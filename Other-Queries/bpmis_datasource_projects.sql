/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  boniboy
 * Created: July 31, 2017
 */
/*Musanze, Rubavu, Huye, Ruszi, Muhanga,Nyagatare Districts one data set all*/
SELECT form_entry.id as plan_id, form_entry.date_of_submission as date_of_submission, form_entry.date_of_response as date_of_response,
mf_invoice.total_amount as Fees,
CASE form_entry.form_id
WHEN 200000 THEN 'Construction Permit' WHEN 201251 THEN 'Construction Permit' WHEN 235992 THEN 'Construction Permit' WHEN 229692 THEN 'Construction Permit' WHEN 60 THEN 'Construction Permit' WHEN 318175 THEN 'Construction Permit'  WHEN 305428 THEN 'Construction Permit' 
WHEN 219480 THEN 'Extension Permit' WHEN 213665 THEN 'Extension Permit' WHEN 239228 THEN 'Extension Permit' WHEN 233480 THEN 'Extension Permit' WHEN 256489 THEN 'Extension Permit' WHEN 314847 THEN 'Extension Permit' WHEN 307681 THEN 'Extension Permit'
WHEN 215327 THEN 'Demolition Permit' WHEN 220384 THEN 'Demolition Permit' WHEN 234966 THEN 'Demolition Permit' WHEN 230688  THEN 'Demolition Permit' WHEN 258173  THEN 'Demolition Permit' WHEN 312093 THEN 'Demolition Permit' WHEN 304297 THEN 'Demolition Permit'
WHEN 214658 THEN 'Refurbishment Without Structural Alteration' WHEN 220023 THEN 'Refurbishment Without Structural Alteration'  WHEN 236928 THEN 'Refurbishment Without Structural Alteration' WHEN 232199 THEN 'Refurbishment Without Structural Alteration' WHEN 257030 THEN 'Refurbishment Without Structural Alteration' WHEN 313852 THEN 'Refurbishment Without Structural Alteration' WHEN 305834 THEN 'Refurbishment Without Structural Alteration'
WHEN 213916 THEN 'Refurbishment With Structural Alteration' WHEN 219859 THEN 'Refurbishment With Structural Alteration' WHEN 244647 THEN 'Refurbishment With Structural Alteration' WHEN 232964 THEN 'Refurbishment With Structural Alteration' WHEN 257320 THEN 'Refurbishment With Structural Alteration' WHEN 311508 THEN 'Refurbishment With Structural Alteration' WHEN 303786 THEN 'Refurbishment With Structural Alteration'
WHEN 202225 THEN 'Occupancy' WHEN 202752 THEN 'Occupancy' WHEN 238289 THEN 'Occupancy' WHEN 234050 THEN 'Occupancy' WHEN 25 THEN 'Occupancy' WHEN 314056 THEN 'Occupancy' WHEN 306752 THEN 'Occupancy'
WHEN 217182 THEN 'Change of Use' WHEN 220990 THEN 'Change of Use' WHEN 235542 THEN 'Change of Use' WHEN 231642 THEN 'Change of Use' WHEN 24 THEN 'Change of Use' WHEN 312723 THEN 'Change of Use' WHEN 304656 THEN 'Change of Use'
ELSE 'Undefined' END AS Type_of_Permit,
sf_guard_user_profile.fullname as Applicant,
CASE sf_guard_user_profile.registeras WHEN 1 THEN 'Property Owner' WHEN 2 THEN 'Engineer' WHEN 3 THEN 'Architect' ELSE 'Undefined' END AS Applicant_type,
CASE mf_invoice.paid WHEN 2 THEN 'Paid' ELSE 'Not Paid' END AS Payment_Status,
CASE form_entry.form_id 
/* Construction Permit */
WHEN 200000 THEN CONCAT(ap_form_200000.element_5_1, ' ', ap_form_200000.element_5_2) WHEN 201251 THEN CONCAT(ap_form_201251.element_5_1, ' ', ap_form_201251.element_5_2) 
WHEN 235992 THEN CONCAT(ap_form_235992.element_5_1, ' ', ap_form_235992.element_5_2) WHEN 229692 THEN CONCAT(ap_form_229692.element_5_1, ' ', ap_form_229692.element_5_2) 
WHEN 318175 THEN CONCAT(ap_form_318175.element_5_1, ' ', ap_form_318175.element_5_2) WHEN 305428 THEN CONCAT(ap_form_305428.element_5_1, ' ', ap_form_305428.element_5_2) 
/* Refurb without and extension */
WHEN 213665 THEN CONCAT(ap_form_213665.element_5_1, ' ', ap_form_213665.element_5_2) WHEN 219480 THEN CONCAT(ap_form_219480.element_5_1, ' ', ap_form_219480.element_5_2) 
WHEN 214658 THEN CONCAT(ap_form_214658.element_5_1, ' ', ap_form_214658.element_5_2) WHEN 220023 THEN CONCAT(ap_form_220023.element_5_1, ' ', ap_form_220023.element_5_2) 
WHEN 314847 THEN CONCAT(ap_form_314847.element_5_1, ' ', ap_form_314847.element_5_2) WHEN 307681 THEN CONCAT(ap_form_307681.element_5_1, ' ', ap_form_307681.element_5_2) 


WHEN 236928 THEN CONCAT(ap_form_213665.element_5_1, ' ', ap_form_213665.element_5_2) WHEN 239228 THEN CONCAT(ap_form_219480.element_5_1, ' ', ap_form_219480.element_5_2) 
WHEN 232199 THEN CONCAT(ap_form_213665.element_5_1, ' ', ap_form_213665.element_5_2) WHEN 233480 THEN CONCAT(ap_form_219480.element_5_1, ' ', ap_form_219480.element_5_2)
WHEN 313852 THEN CONCAT(ap_form_313852.element_5_1, ' ', ap_form_313852.element_5_2) WHEN 305834 THEN CONCAT(ap_form_305834.element_5_1, ' ', ap_form_305834.element_5_2)  
/*Demolition and Refurb with*/
WHEN 215327 THEN CONCAT(ap_form_215327.element_5_1, ' ', ap_form_215327.element_5_2) WHEN 220384 THEN CONCAT(ap_form_220384.element_5_1, ' ', ap_form_220384.element_5_2) 
WHEN 234966 THEN CONCAT(ap_form_234966.element_5_1, ' ', ap_form_234966.element_5_2) WHEN 234647 THEN CONCAT(ap_form_234647.element_5_1, ' ', ap_form_234647.element_5_2) 
WHEN 230688 THEN CONCAT(ap_form_230688.element_5_1, ' ', ap_form_230688.element_5_2) WHEN 232964 THEN CONCAT(ap_form_232964.element_5_1, ' ', ap_form_232964.element_5_2) 
WHEN 213916 THEN CONCAT(ap_form_213916.element_5_1, ' ', ap_form_213916.element_5_2) WHEN 219859 THEN CONCAT(ap_form_219859.element_5_1, ' ', ap_form_219859.element_5_2) 

WHEN 312093 THEN CONCAT(ap_form_312093.element_5_1, ' ', ap_form_312093.element_5_2) WHEN 304297 THEN CONCAT(ap_form_304297.element_5_1, ' ', ap_form_304297.element_5_2) 
WHEN 311508 THEN CONCAT(ap_form_311508.element_5_1, ' ', ap_form_311508.element_5_2) WHEN 303786 THEN CONCAT(ap_form_303786.element_5_1, ' ', ap_form_303786.element_5_2) 


/* Occupation  */
WHEN 234050 THEN CONCAT(ap_form_234050.element_92_1, ' ', ap_form_234050.element_92_2) WHEN 238289 THEN CONCAT(ap_form_238289.element_92_1, ' ', ap_form_238289.element_92_2)
WHEN 314056 THEN CONCAT(ap_form_314056.element_92_1, ' ', ap_form_314056.element_92_2) WHEN 306752 THEN CONCAT(ap_form_306752.element_92_1, ' ', ap_form_306752.element_92_2)
/* Change of User */
WHEN 217182 THEN CONCAT(ap_form_217182.element_5_1, ' ', ap_form_217182.element_5_2) WHEN 220990 THEN CONCAT(ap_form_220990.element_5_1, ' ', ap_form_220990.element_5_2) 
WHEN 235542 THEN CONCAT(ap_form_235542.element_5_1, ' ', ap_form_235542.element_5_2) WHEN 231642 THEN CONCAT(ap_form_231642.element_5_1, ' ', ap_form_231642.element_5_2) 
WHEN 312723 THEN CONCAT(ap_form_312723.element_5_1, ' ', ap_form_312723.element_5_2) WHEN 304656 THEN CONCAT(ap_form_304656.element_5_1, ' ', ap_form_304656.element_5_2) 


ELSE 'Undefined' END AS Owner_name,
agency.name AS Agency,
sub_menus.title as Stage,
saved_permit.date_of_issue AS Permit_Issue_Date,
saved_permit.permit_id AS Permit_Number,
saved_permit.created_by AS Permit_Issued_By,
saved_permit.type_id AS Type_Id,


CASE
when sub_menus.stage_type=6 THEN 'Rejected'
when saved_permit.id IS NOT NULL AND permits.parttype=1 THEN 'Approved'
ELSE 'Pending' END AS Approval_Status,

form_entry.application_id as Application_Number

from form_entry left join mf_invoice on form_entry.id = mf_invoice.app_id
left join sf_guard_user_profile on form_entry.user_id=sf_guard_user_profile.user_id
left join ap_form_200000 on ap_form_200000.id = form_entry.entry_id and form_id = 200000
left join ap_form_201251 on ap_form_201251.id = form_entry.entry_id and form_id = 201251
left join ap_form_219480 on ap_form_219480.id = form_entry.entry_id and form_id = 219480
left join ap_form_213665 on ap_form_213665.id = form_entry.entry_id and form_id = 213665
left join ap_form_215327 on ap_form_215327.id = form_entry.entry_id and form_id = 215327
left join ap_form_220384 on ap_form_220384.id = form_entry.entry_id and form_id = 220384
left join ap_form_214658 on ap_form_214658.id = form_entry.entry_id and form_id = 214658
left join ap_form_220023 on ap_form_220023.id = form_entry.entry_id and form_id = 220023
left join ap_form_213916 on ap_form_213916.id = form_entry.entry_id and form_id = 213916
left join ap_form_219859 on ap_form_219859.id = form_entry.entry_id and form_id = 219859
left join ap_form_202225 on ap_form_202225.id = form_entry.entry_id and form_id = 202225
left join ap_form_202752 on ap_form_202752.id = form_entry.entry_id and form_id = 202752
left join ap_form_217182 on ap_form_217182.id = form_entry.entry_id and form_id = 217182
left join ap_form_220990 on ap_form_220990.id = form_entry.entry_id and form_id = 220990
/* huye */
left join ap_form_235992 on ap_form_235992.id = form_entry.entry_id and form_id = 235992
left join ap_form_235542 on ap_form_235542.id = form_entry.entry_id and form_id = 235542
left join ap_form_239228 on ap_form_239228.id = form_entry.entry_id and form_id = 239228
left join ap_form_234647 on ap_form_234647.id = form_entry.entry_id and form_id = 234647
left join ap_form_236928 on ap_form_236928.id = form_entry.entry_id and form_id = 236928
left join ap_form_238289 on ap_form_238289.id = form_entry.entry_id and form_id = 238289
left join ap_form_234966 on ap_form_234966.id = form_entry.entry_id and form_id = 234966
/* rusizi */
left join ap_form_229692 on ap_form_229692.id = form_entry.entry_id and form_id = 229692
left join ap_form_231642 on ap_form_231642.id = form_entry.entry_id and form_id = 231642
left join ap_form_233480 on ap_form_233480.id = form_entry.entry_id and form_id = 233480
left join ap_form_232964 on ap_form_232964.id = form_entry.entry_id and form_id = 232964
left join ap_form_232199 on ap_form_232199.id = form_entry.entry_id and form_id = 232199
left join ap_form_234050 on ap_form_234050.id = form_entry.entry_id and form_id = 234050
left join ap_form_230688 on ap_form_230688.id = form_entry.entry_id and form_id = 230688
/* Muhanga */
left join ap_form_318175 on ap_form_318175.id = form_entry.entry_id and form_id = 318175
left join ap_form_314847 on ap_form_314847.id = form_entry.entry_id and form_id = 314847
left join ap_form_312093 on ap_form_312093.id = form_entry.entry_id and form_id = 312093
left join ap_form_313852 on ap_form_313852.id = form_entry.entry_id and form_id = 313852
left join ap_form_311508 on ap_form_311508.id = form_entry.entry_id and form_id = 311508
left join ap_form_314056 on ap_form_314056.id = form_entry.entry_id and form_id = 314056
left join ap_form_312723 on ap_form_312723.id = form_entry.entry_id and form_id = 312723
/* Nyagarare */
left join ap_form_305428 on ap_form_305428.id = form_entry.entry_id and form_id = 305428
left join ap_form_307681 on ap_form_307681.id = form_entry.entry_id and form_id = 307681
left join ap_form_304297 on ap_form_304297.id = form_entry.entry_id and form_id = 304297
left join ap_form_305834 on ap_form_305834.id = form_entry.entry_id and form_id = 305834
left join ap_form_303786 on ap_form_303786.id = form_entry.entry_id and form_id = 303786
left join ap_form_306752 on ap_form_306752.id = form_entry.entry_id and form_id = 306752
left join ap_form_304656 on ap_form_304656.id = form_entry.entry_id and form_id = 304656
/* end */

left join sub_menus on sub_menus.id=form_entry.approved
left join menus on menus.id=sub_menus.menu_id
left join agency_menu on agency_menu.menu_id=menus.id
left join agency on agency.id=agency_menu.agency_id
left join ap_forms on ap_forms.form_id=form_entry.form_id
left join form_groups on form_groups.group_id=ap_forms.form_group
left join saved_permit on saved_permit.application_id = form_entry.id and saved_permit.type_id in (select id from permits where parttype=1)
left join permits on permits.id = saved_permit.type_id
where form_entry.form_id in(200000,201251,213665,219480,215327,220384,214658,220023,213916,219859,202225,202752,217182,220990,235992,235542,239228,234647,
236928,238289,234966,229692,231642,233480,232964,232199,234050,230688,318175,314847,312093,313852,311508,314056,312723,305428,307681,304297,305834,303786,306752,304656) and parent_submission=0 and deleted_status = 0 and approved != 0 
and agency.id <> 5/*Do not include Rwanda Housing Authority*/
group by form_entry.id