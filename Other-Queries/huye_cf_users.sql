-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: testbed
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `huye_cf_users`
--

DROP TABLE IF EXISTS `huye_cf_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `huye_cf_users` (
  `nid` int(11) NOT NULL DEFAULT '0',
  `strlastname` text CHARACTER SET utf8 NOT NULL,
  `strfirstname` text CHARACTER SET utf8 NOT NULL,
  `stremail` text CHARACTER SET utf8 NOT NULL,
  `naccesslevel` int(11) NOT NULL DEFAULT '0',
  `struserid` text CHARACTER SET utf8 NOT NULL,
  `strpassword` text CHARACTER SET utf8 NOT NULL,
  `stremail_format` varchar(8) CHARACTER SET utf8 NOT NULL DEFAULT 'HTML',
  `stremail_values` varchar(8) CHARACTER SET utf8 NOT NULL DEFAULT 'IFRAME',
  `nsubstitudeid` int(11) NOT NULL DEFAULT '0',
  `tslastaction` int(11) NOT NULL,
  `bdeleted` int(11) NOT NULL,
  `strstreet` text CHARACTER SET utf8 NOT NULL,
  `strcountry` text CHARACTER SET utf8 NOT NULL,
  `strzipcode` text CHARACTER SET utf8 NOT NULL,
  `strcity` text CHARACTER SET utf8 NOT NULL,
  `strphone_main1` text CHARACTER SET utf8 NOT NULL,
  `strphone_main2` text CHARACTER SET utf8 NOT NULL,
  `strphone_mobile` text CHARACTER SET utf8 NOT NULL,
  `strfax` text CHARACTER SET utf8 NOT NULL,
  `strorganisation` text CHARACTER SET utf8 NOT NULL,
  `strdepartment` text CHARACTER SET utf8 NOT NULL,
  `strcostcenter` text CHARACTER SET utf8 NOT NULL,
  `userdefined1_value` text CHARACTER SET utf8 NOT NULL,
  `userdefined2_value` text CHARACTER SET utf8 NOT NULL,
  `nsubstitutetimevalue` int(11) NOT NULL,
  `strsubstitutetimeunit` text CHARACTER SET utf8 NOT NULL,
  `busegeneralsubstituteconfig` int(11) NOT NULL,
  `busegeneralemailconfig` int(11) NOT NULL,
  `enable_email` int(11) NOT NULL DEFAULT '1',
  `enable_chat` int(11) NOT NULL DEFAULT '1',
  `about_me` text CHARACTER SET utf8,
  `profile_pic` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `address` text CHARACTER SET utf8,
  `twitter` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `facebook` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `youtube` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `linkedin` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `pinterest` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `instagram` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `strtoken` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `strtemppassword` varchar(250) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `huye_cf_users`
--

LOCK TABLES `huye_cf_users` WRITE;
/*!40000 ALTER TABLE `huye_cf_users` DISABLE KEYS */;
INSERT INTO `huye_cf_users` VALUES (200047,'Eric','Ajesimi ','ajesimieric@yahoo.com',1,'ajesimi.eric','$2y$10$uX4ndR0wxnLoHiHIl36u6u0h3fm7wD/.K4VNZMlUyUkEZIk4VirJG','','',0,1488527207,0,'','','','','0788319022','','','','','Huye Building Permit Section OSC','','','',0,'',0,0,0,0,'','','','','','','','','','250bbd997bdde83cf821f60b9ca80215','$2y$10$lJ65F3IO6Ht7B02jr2YCy.v.AEN2jZq4IPE/S/E8ga3oXhE0Es54W'),(200048,' Joselyne','IRAMUKUNDA','joselynei@yahoo.fr',1,'IRAMUKUNDA. Joselyne','$2y$10$KB.ayen25Qwq2UEqAkqJgejxwQqqyyt79xrXJjStYn0DFDgVJTq8.','','',0,1488532464,0,'','','','','0788675561','','','','','Huye Building Permit Section OSC','','','',0,'',0,0,0,0,'','','','','','','','','','',''),(200049,'Jean Pierre',' MUSAFIRI ','musafiripi@gmail.com',1,' MUSAFIRI.Jean','$2y$10$zbuhT1kvyn8KCmvrEaF1MeEUJVvg7FnBzDQz70KCqAUG8K7GBTWVC','','',0,1488527941,0,'','','','','0788440182','','','','','Huye Building Permit Section OSC','','','',0,'',0,0,0,0,'','','','','','','','','','',''),(200050,' Gerasme','NDAYISHIMIYE','Ndageras1@gmail.com',1,'NDAYISHIMIYE.Gerasme','$2y$10$SfLFN44bEAB0O.zct34aReu3NKM2KbCyYo1N2IA9YxmY84TUO8KpS','','',0,1488527151,0,'','','','','0785150432','','','','','Huye Building Permit Section OSC','','','',0,'',0,0,0,0,'','','','','','','','','','',''),(200056,'Nshuti','Jocy','iramukunda7@gmail.com',1,'Jocy','$2y$10$i9pz6FSgn14XoTTy1eOQXeW/F4pm8JwA0tbwXNFdJC1RGOrALS/xi','','',0,1488529460,0,'','','','','','','0727040978','','','Huye Building Permit Section OSC','','','',0,'',0,0,0,0,'','','','','','','','','','',''),(200061,'Pastor','Peter','joy@yahoo.fr',1,'Peter','$2y$10$3S.uoQj0GQK8foxUBJtdDOjwrTvvNifPRO81C.w1aBZOzXes7gvM.','','',0,1488531996,0,'','','','','','','','','','Huye District Onestop Center Coordinator','','','',0,'',0,0,0,0,'','','','','','','','','','','');
/*!40000 ALTER TABLE `huye_cf_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-04 13:15:51
