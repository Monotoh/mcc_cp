/*Query to update ap_forms configuration data values for new form_stage and form_type fields in ap_forms for latest permitflow version*/
UPDATE ap_forms Inner Join ap_column_preferences on ap_forms.form_id=ap_column_preferences.form_id
SET ap_forms.form_stage = ap_column_preferences.starting_point, ap_forms.form_idn = ap_column_preferences.element_name, ap_forms.form_type = 1,
province='Kigali Province'
WHERE ap_forms.form_id in (select form_id from ap_column_preferences);


Update ap_forms SET `form_name` = 'CITY OF KIGALI - ONE STOP CENTER' where form_name like('%one stop center%');
update ext_translations set trl_content='CITY OF KIGALI - ONE STOP CENTER' where table_class='ap_forms' and field_name='form_name'
and field_id in (select form_id from ap_forms where form_name like('%one stop center%'));

Update ap_forms SET `form_name` = 'KICUKIRO DISTRICT - ONE STOP CENTER' where form_name like('%kicukiro%');
update ext_translations set trl_content='KICUKIRO DISTRICT - ONE STOP CENTER' where table_class='ap_forms' and field_name='form_name'
and field_id in (select form_id from ap_forms where form_name like('%kicukiro%'));

Update ap_forms SET `form_name` = 'GASABO DISTRICT - ONE STOP CENTER' where form_name like('%gasabo%');
update ext_translations set trl_content='GASABO DISTRICT - ONE STOP CENTER' where table_class='ap_forms' and field_name='form_name'
and field_id in (select form_id from ap_forms where form_name like('%gasabo%'));

Update ap_forms SET `form_name` = 'NYARUGENGE DISTRICT - ONE STOP CENTER' where form_name like('%nyarugenge%');
update ext_translations set trl_content='NYARUGENGE DISTRICT - ONE STOP CENTER' where table_class='ap_forms' and field_name='form_name'
and field_id in (select form_id from ap_forms where form_name like('%nyarugenge%'));

/* form id 60 is not in table sf_guard_user_categories_forms do an insert */
INSERT INTO `sf_guard_user_categories_forms` (`id`, `categoryid`, 
`formid`, `islinkedto`, `islinkedtitle`) 
VALUES 
(NULL, '2', '60', '0', NULL),
(NULL, '2', '47', '0', NULL),
(NULL, '2', '48', '0', NULL),
(NULL, '2', '49', '0', NULL);

/*Change UPI field to be text*/
update ap_form_elements set element_type='text' where element_id=14 and form_id=60;

/* Kigali construction permit form to construction permit form group.*/
Update ap_forms SET `form_group` = 200000 where form_id in (60,47,48,49);


/*Kigali - Query to setup starting number for application numbers to continue from old system*/
INSERT INTO `ap_number_generator` (`form_id`, `application_number`)
select form_id,max(application_id) from form_entry where approved<>0 and application_id like ('KCPS-%') and parent_submission=0 group by form_id;

/*Update app numbers in number_generator that are for same number sequence, with the maximum among the number sequences*/
Update ap_number_generator update_an 
inner join 
(
select max(application_number) as max_number from ap_number_generator group by substring_index(application_number,'AAA', 1)
) max_an
on substring_index(update_an.application_number,'AAA', 1)=substring_index(max_an.max_number,'AAA', 1)
set update_an.application_number=max_an.max_number;


/* Kigali occupation permit form to occupation permit form group.*/
Update ap_forms SET `form_group` = 200005 where form_id in (25,27,58,59);

/* Kigali change of use permit form to change of use permit form group.*/
Update ap_forms SET `form_group` = 200006 where form_id in (24,29,54,55);

/* Update ap_forms and set form_group value - DO NOT run query with form_group_ids. Query below gets exactly how the form groups were configured.*/
Update ap_forms SET `form_group` = (Select ap_form_groups.group_id from ap_form_groups where form_id=ap_forms.form_id order by ap_form_groups.id desc LIMIT 1)
where form_group IS NULL;


/*Update existing comment sheet to correct type stage and department*/
Update ap_forms set form_department=1, form_department_stage=862, form_type=2 where form_id=1067;

/*Update Departmens that were only saved as string in old version.*/
Insert into department (department_name) select cf_user.strdepartment from cf_user where cf_user.strdepartment not in (select department_name from department) and cf_user.strdepartment<>'';


/*Update configuration of stages*/
/*Submissions Stage Move to In-review on dispatch*/
Update sub_menus set stage_type=8, stage_property=2, stage_type_movement=862 where id =861;
/*In-review Stage Move to Supervisor when tasks are done*/
Update sub_menus set stage_type=2, stage_property=2, stage_type_movement=866 where id =862;



