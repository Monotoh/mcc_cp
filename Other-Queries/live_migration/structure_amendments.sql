SET FOREIGN_KEY_CHECKS = 0; -- Disable foreign key checking.
/*Make room for kigali data where primary keys could conflict and no relations/foreign keys exist*/

/*activity*/
update activity set id=(id+1500);

SELECT `AUTO_INCREMENT`+1500 INTO @newactivityAutoInc
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'rwandacp_v2' AND TABLE_NAME = 'activity';

SET @s:=CONCAT('ALTER TABLE `rwandacp_v2`.`activity` AUTO_INCREMENT=', @newactivityAutoInc);
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

/*audit_trail - note that auto_increment is already set to above 500000 so we only reset the few that had been saved before*/
update audit_trail set id=(id+400000) where id <500000;

/*ap_form_17*/
update ap_form_17 set id=(id+8000);
update mf_user_profile set entry_id=(entry_id+8000) where form_id=17;/*Update the mf_user profile entries to pick from the correct entries since conflict exists with CoK data*/

SELECT `AUTO_INCREMENT`+8000 INTO @newap_form_17AutoInc
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'rwandacp_v2' AND TABLE_NAME = 'ap_form_17';

SET @s:=CONCAT('ALTER TABLE `rwandacp_v2`.`ap_form_17` AUTO_INCREMENT=', @newap_form_17AutoInc);
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

/*communications*/
update communications set id=(id+6000);

SELECT `AUTO_INCREMENT`+6000 INTO @newCommsAutoInc
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'rwandacp_v2' AND TABLE_NAME = 'communications';

SET @s:=CONCAT('ALTER TABLE `rwandacp_v2`.`communications` AUTO_INCREMENT=', @newCommsAutoInc);
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;


/*entry_decline*/
update entry_decline set id=(id+7000);

SELECT `AUTO_INCREMENT`+7000 INTO @newentry_declineAutoInc
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'rwandacp_v2' AND TABLE_NAME = 'entry_decline';

SET @s:=CONCAT('ALTER TABLE `rwandacp_v2`.`entry_decline` AUTO_INCREMENT=', @newentry_declineAutoInc);
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

/*form_entry_shares*/
update form_entry_shares set id=(id+300);

SELECT `AUTO_INCREMENT`+300 INTO @newform_entry_sharesAutoInc
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'rwandacp_v2' AND TABLE_NAME = 'form_entry_shares';

SET @s:=CONCAT('ALTER TABLE `rwandacp_v2`.`form_entry_shares` AUTO_INCREMENT=', @newform_entry_sharesAutoInc);
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;


/*notifications*/
update notifications set id=(id+100);

SELECT `AUTO_INCREMENT`+100 INTO @newnotificationsAutoInc
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'rwandacp_v2' AND TABLE_NAME = 'notifications';

SET @s:=CONCAT('ALTER TABLE `rwandacp_v2`.`notifications` AUTO_INCREMENT=', @newnotificationsAutoInc);
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;



/*notification_history*/
update notification_history set id=(id+26000);

SELECT `AUTO_INCREMENT`+26000 INTO @newnotification_historyAutoInc
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'rwandacp_v2' AND TABLE_NAME = 'notification_history';

SET @s:=CONCAT('ALTER TABLE `rwandacp_v2`.`notification_history` AUTO_INCREMENT=', @newnotification_historyAutoInc);
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;


/*sub_menu_buttons*/
update sub_menu_buttons set id=(id+400);

SELECT `AUTO_INCREMENT`+400 INTO @newsub_menu_buttonsAutoInc
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'rwandacp_v2' AND TABLE_NAME = 'sub_menu_buttons';

SET @s:=CONCAT('ALTER TABLE `rwandacp_v2`.`sub_menu_buttons` AUTO_INCREMENT=', @newsub_menu_buttonsAutoInc);
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

/*sub_menu_tasks*/
update sub_menu_tasks set id=(id+100);

SELECT `AUTO_INCREMENT`+100 INTO @newsub_menu_tasksAutoInc
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'rwandacp_v2' AND TABLE_NAME = 'sub_menu_tasks';

SET @s:=CONCAT('ALTER TABLE `rwandacp_v2`.`sub_menu_tasks` AUTO_INCREMENT=', @newsub_menu_tasksAutoInc);
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;


/*task_forms*/
update task_forms set id=(id+25000);

SELECT `AUTO_INCREMENT`+25000 INTO @newtask_formsAutoInc
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'rwandacp_v2' AND TABLE_NAME = 'task_forms';

SET @s:=CONCAT('ALTER TABLE `rwandacp_v2`.`task_forms` AUTO_INCREMENT=', @newtask_formsAutoInc);
PREPARE stmt FROM @s;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

SET FOREIGN_KEY_CHECKS = 1; -- Enable foreign key checking.

ALTER TABLE `ap_element_options` ADD `opt2` text;
ALTER TABLE `attached_permit` ADD `timestamp` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP;
ALTER TABLE `form_entry` ADD `saved_permit` text;


Update sf_guard_user_profile set registeras=5 where registeras=4;
Update sf_guard_user_categories_forms set categoryid=5 where categoryid=4;
Update sf_guard_user_categories set formid=15,name='SEZAR Design Professional/Architect/Engineer (registration to be verified)',description='Special economic zone staff - RDB' where id=4;
Update ext_translations set trl_content='SEZAR Design Professional/Architect/Engineer (registration to be verified)' where table_class='sf_guard_user_categories' and field_id=4 and field_name='name';
Update ext_translations set trl_content='Special economic zone staff - RDB' where table_class='sf_guard_user_categories' and field_id=4 and field_name='description';


INSERT INTO `sf_guard_user_categories` (`id`, `name`, `description`, `formid`, `orderid`) VALUES (5,'Supervising Firm','Supervising Firm',17,5);
INSERT INTO `ext_translations` (`locale`, `table_class`, `field_name`, `field_id`, `trl_content`) VALUES ('en_US','sf_guard_user_categories','name',5,'Supervising Firm');
INSERT INTO `ext_translations` (`locale`, `table_class`, `field_name`, `field_id`, `trl_content`) VALUES ('en_US','sf_guard_user_categories','description',5,'Supervising Firm');


Delete from mf_user_profile where user_id=0;
Delete from sf_guard_user where id=1;

ALTER TABLE `permits` ADD `content_Part1` text NOT NULL;
ALTER TABLE `permits` ADD `content_Part2` text;
ALTER TABLE `permits` ADD `content_Part3` text;
ALTER TABLE `permits` ADD `content_Part1En` text NOT NULL;
ALTER TABLE `permits` ADD `content_Part2En` text;
ALTER TABLE `permits` ADD `content_Part3En` text;
ALTER TABLE `permits` ADD `contentEn` text NOT NULL;

ALTER TABLE `ap_forms` ADD `bureau` text;
ALTER TABLE `ap_forms` ADD `form_permitType` text;
ALTER TABLE `ap_forms` ADD `payment_stripe_live_secret_key` varchar(50) DEFAULT NULL;
ALTER TABLE `ap_forms` ADD `payment_stripe_live_public_key` varchar(50) DEFAULT NULL;
ALTER TABLE `ap_forms` ADD `payment_stripe_test_secret_key` varchar(50) DEFAULT NULL;
ALTER TABLE `ap_forms` ADD `payment_stripe_test_public_key` varchar(50) DEFAULT NULL;
ALTER TABLE `ap_forms` ADD `payment_stripe_enable_test_mode` int(1) NOT NULL DEFAULT '0';
/*Set different admin username for the most recent bpmis admin COK admin retains admin username*/
Update cf_user set struserid='admin_bpmis' where nid=200000;


/*Copy sf_guard_user from live kigali clone before migration*/
CREATE TABLE kcps_user_before_migration LIKE kcps_beta.sf_guard_user;
INSERT INTO kcps_user_before_migration SELECT * FROM kcps_beta.sf_guard_user;

/*Copy sf_guard_user_profile from live kigali clone before migration*/
CREATE TABLE kcps_user_profile_before_migration LIKE kcps_beta.sf_guard_user_profile;
INSERT INTO kcps_user_profile_before_migration SELECT * FROM kcps_beta.sf_guard_user_profile;

/*Copy sf_guard_user from bpmis before migration*/
CREATE TABLE bpmis_user_before_migration LIKE sf_guard_user;
INSERT INTO bpmis_user_before_migration SELECT * FROM sf_guard_user;

/*Copy sf_guard_user_profile from bpmis before migration*/
CREATE TABLE bpmis_user_profile_before_migration LIKE sf_guard_user_profile;
INSERT INTO bpmis_user_profile_before_migration SELECT * FROM sf_guard_user_profile;

ALTER TABLE `sf_guard_user` ADD `bpmis_username_b4_cok` varchar(128) NOT NULL;
Update sf_guard_user set bpmis_username_b4_cok=username;

delete from kcps_beta.sf_guard_user_profile where user_id in (select id from kcps_beta.sf_guard_user WHERE (last_login IS NULL and created_at < '2017-03-20 00:00:00' and is_super_admin=0) or  (last_login IS NULL and created_at < '2017-01-01 00:00:00' and is_super_admin=1));
delete from kcps_beta.sf_guard_user WHERE (last_login IS NULL and created_at < '2017-03-20 00:00:00' and is_super_admin=0) or  (last_login IS NULL and created_at < '2017-01-01 00:00:00' and is_super_admin=1);

delete from `sf_guard_user_profile`
where user_id in (select id from `sf_guard_user`
WHERE (last_login IS NULL and created_at < '2017-03-20 00:00:00' and is_super_admin=0) or 
(last_login IS NULL and created_at < '2017-01-01 00:00:00' and is_super_admin=1));

delete from `sf_guard_user`
WHERE (last_login IS NULL and created_at < '2017-03-20 00:00:00' and is_super_admin=0) or 
(last_login IS NULL and created_at < '2017-01-01 00:00:00' and is_super_admin=1);

/*delete FROM `sf_guard_user` WHERE last_login IS NULL and created_at < '2017-03-01 00:00:00'
delete FROM `sf_guard_user` WHERE last_login IS NULL;*/

ALTER TABLE `sf_guard_user` ADD `conflicts_with_kcps` int(1) NOT NULL DEFAULT '0';
Update sf_guard_user set conflicts_with_kcps=1 where username in (select username from kcps_beta.sf_guard_user);


Update sf_guard_user set username=concat(username,'_after_migration') where username in (select username from kcps_beta.sf_guard_user);

CREATE TABLE `account_data_transfer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `application_id` int(11) NOT NULL,
  `transferred_to_id` int(11) NOT NULL,
  `transferred_from_id` int(11) NOT NULL,
  `validate` varchar(17) DEFAULT NULL,
  `transfer_complete` tinyint(1) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `email_request_sent` text NULL,
  PRIMARY KEY (`id`),
  KEY `account_data_transfer_application_id_form_entry_id` (`application_id`),
  KEY `account_data_transfer_transferred_to_id_sf_guard_user_id` (`transferred_to_id`),
  KEY `account_data_transfer_transferred_from_id_sf_guard_user_id` (`transferred_from_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



