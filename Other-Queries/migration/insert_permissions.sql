/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  boniboy
 * Created: Apr 13, 2017
 */
INSERT INTO mf_guard_user_group VALUES (200000,6), (200000,7) ;
INSERT INTO mf_guard_user_group VALUES (200000,21), (200000,24) ;
INSERT INTO mf_guard_user_group VALUES (200000,43), (200000,26) ;
INSERT INTO mf_guard_user_group VALUES (200000,45);

/* Permission to view applications is missing for users insert eg for musanze */
insert into mf_guard_group_permission values (200013,187); /* 187 = permission, 200013 = user */

/** Update CoK stuff */
update menus set title = 'CoK One Stop Center' where id = 2 ;
update menus set order_no = 1 where id = 2 ;
update menus set order_no = 2 where id = 8 ;
update menus set order_no = 3 where id = 9 
update menus set order_no = 4 where id = 10;
update menus set order_no = 5 where id = 11;
update menus set order_no = 200002 where id = 200001;
 update menus set order_no = 200003 where id = 200002;
 update menus set order_no = 200001 where id = 200000;
 update menus set order_no = 200004 where id = 200003;
/*  Update forms */
update ap_forms set province = 'Kigali Province', district='City of Kigali One Stop Center' where form_id = 23 ;
update ap_forms set province = 'KIGALI PROVINCE', district='City of Kigali One Stop Center' where form_id = 47 ;
update ap_forms set province = 'KIGALI PROVINCE', district='City of Kigali One Stop Center' where form_id = 49 ;
update ap_forms set province = 'KIGALI PROVINCE', district='City of Kigali One Stop Center' where form_id = 48 ;
update ap_forms set province = 'KIGALI PROVINCE', district='City of Kigali One Stop Center' where form_id = 6086 ;
