/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  boniboy
 * Created: Apr 12, 2017
 */
/* Queries will reset the id and you can export the tables to updates old tables */
/* activity table view */
UPDATE activity SET id = id + 2000 ;
/* agency table view  >> no need Export the data as is */
/* agency_department  >> no need Export the data as is */
/* agency_menu  >> no need Export the data as is */
/* agency_user  >> no need Export the data as is */
/* application_reference */
UPDATE application_reference SET id = id + 200000 ;
/* ap_dependency_logic  >> no need Export the data as is */
/* ap_dependency_logic_conditions  >> no need Export the data as is */
/* ap_dependency_logic_rule_permitted_values >> no need Export the data as is */
/* ap_element_options >> no need Export the data as is */
/* ap_email_logic >> no need Export the data as is */
/* ap_email_logic_conditions */
/* ap_field_logic_conditions */
/* ap_field_logic_elements */
/* ap_forms */
/* ap_form_15 */
/* ap_form_16 */ 
UPDATE ap_form_16 SET id = id + 200 ;
/* ap_form_17 */ 
UPDATE ap_form_17 SET id = id + 15000 ;
/* ap_form_6 */ 
UPDATE ap_form_6 SET id = id + 1000 ;
/* ap_form_elements */
/* ap_form_locks */
/* ap_form_payments */
/* ap_form_sorts */
/* ap_number_generator */
/* ap_permissions */
/* ap_permission_logic_conditions */
/* ap_permission_logic_elements */
/* TRUNCATE ap_settings  and import from new system */
/* ap_workflow_logic_conditions */ 
/* ap_workflow_logic_elements */
/* audit_trail */
UPDATE audit_trail SET id = id + 300000 ;
/* banner */
/* booklet */
/* buttons */
/* cf_config */
/* cf_formslot */
/* cf_user */
/* cf_user_index */
/* communication */
/* communications */
UPDATE communications SET id = id + 5000 ;
/* content */
 UPDATE content SET id = id + 200000 ; 
/* content_type */
 UPDATE content_type SET id = id + 10 ; 
/* countries */
/* currencies */
/* department */
/* entry_decline */
/* truncate ext_locales nd import the new */
/* ext_translations */
/* faq */
UPDATE faq SET id = id + 30 ;
/* favorites */
/* truncate fee  and import new */
/* fee_range */
/* fee_range_condition */
/* form_entry */
/* form_entry_shares */
UPDATE form_entry_shares SET id = id + 1000 ;
/* form_groups */
/* invoicetemplates */
/* menus */
/* merchant */
/* mf_guard_group */
/* mf_guard_group_permission */
/* mf_guard_permission */
/* mf_guard_user_group */
/* mf_invoice */
/* mf_invoice_detail */
/* mf_user_profile */
/* notifications */
UPDATE notifications SET id = id + 500 ;
/* notification_history */
UPDATE notification_history SET id = id + 500000 ;
/* permits */
/* permit_cache */
/*  permit_checker_config */
/* saved_permit */
/* sf_guard_user */
/* sf_guard_user_categories -- update infor to match */
/* sf_guard_user_categories_form */
/* sf_guard_user_profile */
/* sub_menus */
/* sub_menu_buttons */
UPDATE sub_menu_buttons SET id = id + 500 ;
/* sub_menu_tasks */
UPDATE sub_menu_tasks SET id = id + 200 ;
/* task */
/* task_forms */ 
UPDATE task_forms SET id = id + 500000 ;
/* task_queue */
UPDATE task_queue SET id = id + 20000 ;
/* upload_receipt */
UPDATE upload_receipt SET id = id + 100 ;
/* workflow_reviewers */



