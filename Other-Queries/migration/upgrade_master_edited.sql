           /** BORROWED FROM NAIROBI EDITED FOR RWANDA */
CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`agency` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(256) NULL,
  `address` text NULL,
  `logo` varchar(256) NULL,
  `tag_line` varchar(256) NULL,
  `parent_agency` int(10) NULL,
  UNIQUE KEY `name`(`name`),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`ap_number_generator` (
  `form_id` int(11) NOT NULL,
  `application_number` varchar(255) NOT NULL,
  UNIQUE KEY `form_id`(`form_id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`fee_category` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`fee_range` (
  `id` int(11) NOT NULL auto_increment,
  `fee_id` int(11) NULL,
  `name` varchar(100) NOT NULL,
  `range_1` varchar(256) NULL,
  `range_2` varchar(256) NULL,
  `result_value` int(11) NOT NULL,
  `condition_field` int(11) NULL,
  `condition_operator` varchar(100) NULL,
  `condition_value` text NULL,
  `created_by` int(11) NOT NULL,
  `value_type` varchar(64) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_admins` (
  `adminID` int(10) unsigned NOT NULL auto_increment,
  `created` int(10) unsigned NOT NULL,
  `lastactive` int(10) unsigned NOT NULL,
  `status` tinyint(4) NOT NULL,
  `ses` varchar(32) NOT NULL,
  `login` varchar(15) NOT NULL,
  `password` varchar(32) NOT NULL,
  `email` varchar(160) NOT NULL,
  UNIQUE KEY `login`(`login`),
  PRIMARY KEY (`adminID`),
  KEY `ses`(`ses`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_canned` (
  `canID` int(10) unsigned NOT NULL auto_increment,
  `opID` int(10) unsigned NOT NULL,
  `deptID` int(10) unsigned NOT NULL,
  `title` varchar(80) NOT NULL,
  `message` mediumtext NOT NULL,
  PRIMARY KEY (`canID`),
  KEY `deptID`(`deptID`),
  KEY `opID`(`opID`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_canned_auto` (
  `opID` int(10) unsigned NOT NULL,
  `canID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`opID`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_departments` (
  `deptID` int(10) unsigned NOT NULL auto_increment,
  `visible` tinyint(4) NOT NULL,
  `queue` tinyint(4) NOT NULL,
  `tshare` tinyint(4) NOT NULL,
  `texpire` int(10) unsigned NOT NULL,
  `rquestion` tinyint(4) NOT NULL,
  `remail` tinyint(4) NOT NULL,
  `temail` tinyint(4) NOT NULL,
  `temaild` tinyint(4) NOT NULL,
  `rtype` tinyint(4) NOT NULL,
  `rtime` int(10) unsigned NOT NULL,
  `rloop` tinyint(3) unsigned NOT NULL,
  `savem` tinyint(3) unsigned NOT NULL,
  `custom` varchar(255) NOT NULL,
  `smtp` text NOT NULL,
  `lang` varchar(15) NOT NULL,
  `name` varchar(80) NOT NULL,
  `email` varchar(160) NOT NULL,
  `emailt` varchar(160) NOT NULL,
  `emailt_bcc` tinyint(1) NOT NULL,
  `msg_greet` text NOT NULL,
  `msg_offline` text NOT NULL,
  `msg_busy` text NOT NULL,
  `msg_email` text NOT NULL,
  PRIMARY KEY (`deptID`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_dept_ops` (
  `deptID` int(10) unsigned NOT NULL,
  `opID` int(10) unsigned NOT NULL,
  `display` tinyint(4) NOT NULL,
  `visible` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`deptID`,`opID`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_dept_vars` (
  `deptID` int(11) NOT NULL,
  `idle_o` tinyint(3) unsigned NOT NULL,
  `idle_v` tinyint(3) unsigned NOT NULL,
  `trans_f_dept` tinyint(3) unsigned NOT NULL,
  `prechat_form` tinyint(1) NOT NULL DEFAULT 1,
  `greeting_title` varchar(255) NOT NULL,
  `greeting_body` varchar(255) NOT NULL,
  PRIMARY KEY (`deptID`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_ext_ops` (
  `extID` int(10) NOT NULL,
  `opID` int(10) NOT NULL,
  UNIQUE KEY `extID`(`extID`,`opID`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_external` (
  `extID` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(40) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`extID`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_footprints` (
  `created` int(10) unsigned NOT NULL,
  `archive` tinyint(1) NOT NULL,
  `ip` varchar(45) NOT NULL,
  `os` tinyint(1) NOT NULL,
  `browser` tinyint(1) NOT NULL,
  `md5_vis` varchar(32) NOT NULL,
  `md5_page` varchar(32) NULL,
  `onpage` varchar(255) NOT NULL,
  `title` varchar(150) NOT NULL,
  KEY `archive`(`archive`),
  KEY `created`(`created`),
  KEY `ip`(`ip`),
  KEY `md5_page`(`md5_page`),
  KEY `md5_vis`(`md5_vis`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_footprints_u` (
  `footID` int(10) unsigned NOT NULL auto_increment,
  `md5_vis` varchar(32) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `updated` int(10) unsigned NOT NULL,
  `deptID` int(10) unsigned NOT NULL,
  `marketID` int(10) unsigned NOT NULL,
  `chatting` tinyint(1) NOT NULL,
  `os` tinyint(1) NOT NULL,
  `browser` tinyint(1) NOT NULL,
  `footprints` int(10) unsigned NOT NULL,
  `requests` int(10) unsigned NOT NULL,
  `initiates` int(10) unsigned NOT NULL,
  `resolution` varchar(15) NOT NULL,
  `ip` varchar(45) NOT NULL,
  `onpage` varchar(255) NOT NULL,
  `title` varchar(150) NOT NULL,
  `refer` varchar(255) NOT NULL,
  `country` char(2) NOT NULL,
  `region` char(42) NOT NULL,
  `city` char(50) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  UNIQUE KEY `md5_vis`(`md5_vis`),
  PRIMARY KEY (`footID`),
  KEY `created`(`created`),
  KEY `updated`(`updated`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_footstats` (
  `sdate` int(10) unsigned NOT NULL,
  `md5_page` varchar(32) NOT NULL,
  `total` int(10) unsigned NOT NULL,
  `onpage` varchar(255) NOT NULL,
  PRIMARY KEY (`sdate`,`md5_page`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_geo_bloc` (
  `startIpNum` int(10) unsigned NOT NULL,
  `endIpNum` int(10) unsigned NOT NULL,
  `locId` int(10) unsigned NOT NULL,
  `network` mediumint(6) unsigned NOT NULL,
  PRIMARY KEY (`endIpNum`),
  KEY `network`(`network`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_geo_loc` (
  `locId` int(10) unsigned NOT NULL,
  `country` char(2) NOT NULL,
  `region` char(42) NOT NULL,
  `city` varchar(50) NULL,
  `latitude` float NULL,
  `longitude` float NULL,
  PRIMARY KEY (`locId`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_ips` (
  `md5_vis` varchar(32) NOT NULL,
  `ip` varchar(45) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `t_footprints` int(10) unsigned NOT NULL,
  `t_requests` int(10) unsigned NOT NULL,
  `t_initiate` int(11) NOT NULL,
  `i_footprints` int(10) unsigned NOT NULL,
  `i_timestamp` int(10) unsigned NOT NULL,
  `i_initiate` int(10) unsigned NOT NULL,
  PRIMARY KEY (`md5_vis`),
  KEY `created`(`created`),
  KEY `ip`(`ip`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_lang_packs` (
  `lang` varchar(15) NOT NULL,
  `lang_vars` text NOT NULL,
  UNIQUE KEY `lang`(`lang`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_market_c` (
  `sdate` int(10) unsigned NOT NULL,
  `marketID` int(10) unsigned NOT NULL,
  `clicks` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`sdate`,`marketID`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_marketing` (
  `marketID` int(10) unsigned NOT NULL auto_increment,
  `skey` varchar(4) NOT NULL,
  `name` varchar(80) NOT NULL,
  `color` varchar(6) NOT NULL,
  PRIMARY KEY (`marketID`),
  KEY `skey`(`skey`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_marquees` (
  `marqID` int(10) unsigned NOT NULL auto_increment,
  `display` tinyint(4) NOT NULL,
  `deptID` int(10) unsigned NOT NULL,
  `snapshot` varchar(80) NOT NULL,
  `message` varchar(255) NOT NULL,
  PRIMARY KEY (`marqID`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_messages` (
  `messageID` int(10) unsigned NOT NULL auto_increment,
  `created` int(10) unsigned NOT NULL,
  `chat` tinyint(3) unsigned NOT NULL,
  `deptID` int(10) unsigned NOT NULL,
  `footprints` int(10) unsigned NOT NULL,
  `ip` varchar(45) NOT NULL,
  `vname` varchar(80) NOT NULL,
  `vemail` varchar(160) NOT NULL,
  `ces` varchar(32) NOT NULL,
  `md5_vis` varchar(32) NOT NULL,
  `subject` varchar(155) NOT NULL,
  `onpage` varchar(255) NOT NULL,
  `refer` varchar(255) NOT NULL,
  `custom` text NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`messageID`),
  KEY `md5_vis`(`md5_vis`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_op_vars` (
  `opID` int(10) unsigned NOT NULL,
  `canID` int(10) unsigned NOT NULL,
  `sound` tinyint(1) NOT NULL,
  `blink` tinyint(1) NOT NULL,
  `blink_r` tinyint(1) NOT NULL,
  `dn_response` tinyint(1) NOT NULL,
  `dn_request` tinyint(1) NOT NULL,
  `dn_always` tinyint(1) NOT NULL,
  `nsleep` tinyint(1) unsigned NOT NULL DEFAULT 1,
  `shorts` tinyint(1) unsigned NOT NULL,
  `mapp_c` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`opID`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_operators` (
  `opID` int(10) unsigned NOT NULL auto_increment,
  `lastactive` int(10) unsigned NOT NULL,
  `lastrequest` int(11) unsigned NOT NULL,
  `status` tinyint(4) NOT NULL,
  `mapper` tinyint(1) unsigned NOT NULL,
  `mapp` tinyint(1) unsigned NOT NULL,
  `signall` tinyint(4) NOT NULL,
  `rate` tinyint(1) unsigned NOT NULL,
  `op2op` tinyint(1) unsigned NOT NULL,
  `traffic` tinyint(1) unsigned NOT NULL,
  `viewip` tinyint(1) unsigned NOT NULL,
  `nchats` tinyint(1) NOT NULL DEFAULT 1,
  `maxc` tinyint(1) NOT NULL,
  `canID` tinyint(4) NOT NULL,
  `ses` varchar(32) NOT NULL,
  `ces` varchar(32) NOT NULL,
  `rating` tinyint(4) NOT NULL,
  `sms` int(10) unsigned NOT NULL,
  `smsnum` varchar(155) NOT NULL,
  `login` varchar(15) NOT NULL,
  `password` varchar(32) NOT NULL,
  `name` varchar(80) NOT NULL,
  `email` varchar(160) NOT NULL,
  `pic` tinyint(1) unsigned NOT NULL,
  `theme` varchar(15) NOT NULL,
  PRIMARY KEY (`opID`),
  KEY `ses`(`ses`),
  KEY `status`(`status`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_opstatus_log` (
  `created` int(11) NOT NULL,
  `opID` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `mapp` tinyint(1) unsigned NOT NULL,
  KEY `created`(`created`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_refer` (
  `md5_vis` varchar(32) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `archive` tinyint(1) NOT NULL,
  `marketID` int(10) unsigned NOT NULL,
  `md5_page` varchar(32) NOT NULL,
  `refer` varchar(255) NOT NULL,
  PRIMARY KEY (`md5_vis`),
  KEY `archive`(`archive`),
  KEY `created`(`created`),
  KEY `md5_page`(`md5_page`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_referstats` (
  `sdate` int(10) unsigned NOT NULL,
  `md5_page` varchar(32) NOT NULL,
  `total` int(10) unsigned NOT NULL,
  `refer` varchar(255) NOT NULL,
  PRIMARY KEY (`sdate`,`md5_page`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_req_log` (
  `ces` varchar(32) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `ended` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL,
  `archive` tinyint(1) NOT NULL,
  `status_msg` tinyint(1) NOT NULL,
  `initiated` tinyint(1) NOT NULL,
  `deptID` int(11) unsigned NOT NULL,
  `opID` int(11) unsigned NOT NULL,
  `op2op` int(11) NOT NULL,
  `marketID` int(10) NOT NULL,
  `os` tinyint(1) NOT NULL,
  `browser` tinyint(1) NOT NULL,
  `idle_disconnect` tinyint(4) NOT NULL,
  `resolution` varchar(15) NOT NULL,
  `vname` varchar(40) NOT NULL,
  `vemail` varchar(160) NOT NULL,
  `ip` varchar(45) NOT NULL,
  `md5_vis` varchar(32) NOT NULL,
  `sim_ops` varchar(155) NOT NULL,
  `onpage` varchar(255) NOT NULL,
  `title` varchar(150) NOT NULL,
  `custom` text NOT NULL,
  `question` text NOT NULL,
  PRIMARY KEY (`ces`),
  KEY `archive`(`archive`),
  KEY `created`(`created`),
  KEY `md5_vis`(`md5_vis`),
  KEY `opID`(`opID`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_requests` (
  `requestID` int(10) unsigned NOT NULL auto_increment,
  `created` int(10) unsigned NOT NULL,
  `ended` int(10) unsigned NOT NULL,
  `tupdated` int(11) NOT NULL,
  `updated` int(10) unsigned NOT NULL,
  `vupdated` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL,
  `auto_pop` tinyint(4) NOT NULL,
  `initiated` tinyint(1) NOT NULL,
  `deptID` int(11) unsigned NOT NULL,
  `opID` int(11) unsigned NOT NULL,
  `op2op` int(10) unsigned NOT NULL,
  `t_vses` tinyint(2) unsigned NOT NULL,
  `marketID` int(10) NOT NULL,
  `os` tinyint(1) NOT NULL,
  `browser` tinyint(1) NOT NULL,
  `requests` int(10) unsigned NOT NULL,
  `ces` varchar(32) NOT NULL,
  `resolution` varchar(15) NOT NULL,
  `vname` varchar(80) NOT NULL,
  `vemail` varchar(160) NOT NULL,
  `ip` varchar(45) NOT NULL,
  `md5_vis` varchar(32) NOT NULL,
  `md5_vis_` varchar(32) NOT NULL,
  `sim_ops` varchar(155) NOT NULL,
  `sim_ops_` varchar(155) NOT NULL,
  `onpage` varchar(255) NOT NULL,
  `title` varchar(150) NOT NULL,
  `rstring` varchar(255) NOT NULL,
  `refer` varchar(255) NOT NULL,
  `custom` text NOT NULL,
  `question` text NOT NULL,
  UNIQUE KEY `ces`(`ces`),
  PRIMARY KEY (`requestID`),
  KEY `md5_vis`(`md5_vis`),
  KEY `op2op`(`op2op`),
  KEY `opID`(`opID`),
  KEY `status`(`status`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_rstats_depts` (
  `sdate` int(10) unsigned NOT NULL,
  `deptID` int(10) unsigned NOT NULL,
  `requests` int(10) NOT NULL,
  `taken` smallint(5) unsigned NOT NULL,
  `declined` smallint(5) unsigned NOT NULL,
  `message` smallint(5) unsigned NOT NULL,
  `initiated` smallint(5) unsigned NOT NULL,
  `initiated_` smallint(5) unsigned NOT NULL,
  `rateit` smallint(5) unsigned NOT NULL,
  `ratings` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`sdate`,`deptID`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_rstats_ops` (
  `sdate` int(10) unsigned NOT NULL,
  `opID` int(10) unsigned NOT NULL,
  `requests` int(10) NOT NULL,
  `taken` smallint(5) unsigned NOT NULL,
  `declined` smallint(5) unsigned NOT NULL,
  `message` smallint(5) unsigned NOT NULL,
  `initiated` smallint(5) unsigned NOT NULL,
  `initiated_` smallint(5) unsigned NOT NULL,
  `rateit` smallint(5) unsigned NOT NULL,
  `ratings` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`sdate`,`opID`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_socials` (
  `deptID` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL,
  `social` varchar(15) NOT NULL,
  `tooltip` varchar(80) NOT NULL,
  `url` varchar(255) NOT NULL,
  UNIQUE KEY `deptID`(`deptID`,`social`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_transcripts` (
  `ces` varchar(32) NOT NULL,
  `created` int(11) unsigned NOT NULL,
  `ended` int(10) unsigned NOT NULL,
  `deptID` int(11) unsigned NOT NULL,
  `opID` int(11) unsigned NOT NULL,
  `initiated` tinyint(1) NOT NULL,
  `op2op` tinyint(4) NOT NULL,
  `rating` tinyint(1) NOT NULL,
  `encr` tinyint(1) NOT NULL,
  `fsize` mediumint(9) NOT NULL,
  `vname` varchar(80) NOT NULL,
  `vemail` varchar(160) NOT NULL,
  `ip` varchar(45) NOT NULL,
  `md5_vis` varchar(32) NOT NULL,
  `question` mediumtext NOT NULL,
  `formatted` mediumtext NOT NULL,
  `plain` mediumtext NOT NULL,
  PRIMARY KEY (`ces`),
  KEY `created`(`created`),
  KEY `deptID`(`deptID`),
  KEY `encr`(`encr`),
  KEY `md5_vis`(`md5_vis`),
  KEY `op2op`(`op2op`),
  KEY `opID`(`opID`),
  KEY `rating`(`rating`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`p_vars` (
  `code` varchar(10) NOT NULL,
  `position` tinyint(1) NOT NULL,
  `ts_clean` int(10) unsigned NOT NULL,
  `ts_clear` int(10) unsigned NOT NULL,
  `char_set` varchar(155) NOT NULL,
  `profile_pic` tinyint(1) unsigned NOT NULL
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`permit_checker_config` (
  `id` int(11) NOT NULL auto_increment,
  `permit_template_id` int(11) NULL,
  `reference_object` varchar(255) NULL,
  `label_to_show` varchar(255) NULL,
  `value_to_show` varchar(255) NULL,
  `sequence_no` int(11) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`agency_department` (
  `department_id` int(11) NOT NULL DEFAULT '0',
  `agency_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`department_id`,`agency_id`),
  FOREIGN KEY (`agency_id`) REFERENCES `bpmis_kcps_v1`.`agency` (`id`) ON DELETE CASCADE,
  KEY `agency_department_agency_id_agency_id`(`agency_id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`agency_menu` (
  `menu_id` int(11) NOT NULL DEFAULT '0',
  `agency_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menu_id`,`agency_id`),
  FOREIGN KEY (`agency_id`) REFERENCES `bpmis_kcps_v1`.`agency` (`id`) ON DELETE CASCADE,
  KEY `agency_user_agency_id_agency_id`(`agency_id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`agency_user` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `agency_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`agency_id`),
  FOREIGN KEY (`agency_id`) REFERENCES `bpmis_kcps_v1`.`agency` (`id`) ON DELETE CASCADE,
  KEY `agency_user_agency_id_agency_id`(`agency_id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`alerts` (
  `id` int(11) NOT NULL auto_increment,
  `subject` varchar(250) NOT NULL,
  `content` varchar(250) NOT NULL,
  `link` varchar(250) NOT NULL,
  `isread` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `datesent` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `bpmis_kcps_v1`.`announcement` (
  `id` int(11) NOT NULL auto_increment,
  `content` text NOT NULL,
  `start_date` varchar(250) NOT NULL,
  `end_date` varchar(250) NOT NULL,
  `frontend` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

/** ALTER TABLE `bpmis_kcps_v1`.`ap_column_preferences`
  MODIFY COLUMN `starting_point` varchar(255) NOT NULL DEFAULT ''
  , ADD COLUMN `user_id` int(11) NOT NULL DEFAULT '1'; */

/** ALTER TABLE `bpmis_kcps_v1`.`ap_element_options`
  ADD COLUMN `option` text NULL; */

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`ap_element_prices` (
  `aep_id` int(11) unsigned NOT NULL auto_increment,
  `form_id` int(11) NOT NULL,
  `element_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL DEFAULT '0',
  `price` decimal(62,2) NOT NULL DEFAULT 0.00,
  PRIMARY KEY (`aep_id`),
  KEY `element_id`(`element_id`),
  KEY `form_id`(`form_id`)
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`ap_email_logic` (
  `form_id` int(11) NOT NULL,
  `rule_id` int(11) NOT NULL,
  `rule_all_any` varchar(3) NOT NULL DEFAULT 'all',
  `target_email` text NOT NULL,
  `template_name` varchar(15) NOT NULL DEFAULT 'notification' COMMENT 'notification - confirmation - custom',
  `custom_from_name` text NULL,
  `custom_from_email` varchar(255) NOT NULL DEFAULT '',
  `custom_subject` text NULL,
  `custom_content` text NULL,
  `custom_plain_text` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`form_id`,`rule_id`)
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`ap_email_logic_conditions` (
  `aec_id` int(11) unsigned NOT NULL auto_increment,
  `form_id` int(11) NOT NULL,
  `target_rule_id` int(11) NOT NULL,
  `element_name` varchar(50) NOT NULL DEFAULT '',
  `rule_condition` varchar(15) NOT NULL DEFAULT 'is',
  `rule_keyword` varchar(255) NULL,
  PRIMARY KEY (`aec_id`)
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`ap_entries_preferences` (
  `form_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `entries_sort_by` varchar(100) NOT NULL DEFAULT 'id-desc',
  `entries_enable_filter` int(1) NOT NULL DEFAULT '0',
  `entries_filter_type` varchar(5) NOT NULL DEFAULT 'all' COMMENT 'all or any'
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`ap_field_logic_conditions` (
  `alc_id` int(11) unsigned NOT NULL auto_increment,
  `form_id` int(11) NOT NULL,
  `target_element_id` int(11) NOT NULL,
  `element_name` varchar(50) NOT NULL DEFAULT '',
  `rule_condition` varchar(15) NOT NULL DEFAULT 'is',
  `rule_keyword` varchar(255) NULL,
  PRIMARY KEY (`alc_id`)
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`ap_field_logic_elements` (
  `form_id` int(11) NOT NULL,
  `element_id` int(11) NOT NULL,
  `rule_show_hide` varchar(4) NOT NULL DEFAULT 'show',
  `rule_all_any` varchar(3) NOT NULL DEFAULT 'all',
  PRIMARY KEY (`form_id`,`element_id`)
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`ap_fonts` (
  `font_id` int(11) unsigned NOT NULL auto_increment,
  `font_origin` varchar(11) NOT NULL DEFAULT 'google',
  `font_family` varchar(100) NULL,
  `font_variants` text NULL,
  `font_variants_numeric` text NULL,
  PRIMARY KEY (`font_id`),
  KEY `font_family`(`font_family`)
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`ap_form_1` (
  `id` int(11) NOT NULL auto_increment,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_updated` datetime NULL,
  `ip_address` varchar(15) NULL,
  `element_2` text NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM;

 ALTER TABLE `bpmis_kcps_v1`.`ap_form_15`
  ADD COLUMN `element_17` text NULL
  , ADD COLUMN `element_18` text NULL
  , ADD COLUMN `element_19` text NULL ;

/** ALTER TABLE `bpmis_kcps_v1`.`ap_form_16`
  ADD COLUMN `element_6` text NULL
  , ADD COLUMN `status` int(4) unsigned NOT NULL DEFAULT '1'
  , ADD COLUMN `resume_key` varchar(10) NULL;*/

/** ALTER TABLE `bpmis_kcps_v1`.`ap_form_17`
  ADD COLUMN `status` int(4) unsigned NOT NULL DEFAULT '1'
  , ADD COLUMN `resume_key` varchar(10) NULL
  , ADD COLUMN `element_2` text NULL COMMENT 'File Upload'; */


/** ALTER TABLE `bpmis_kcps_v1`.`ap_form_6`
  ADD COLUMN `element_5` text NULL
  , ADD COLUMN `element_4` text NULL
  , ADD COLUMN `status` int(4) unsigned NOT NULL DEFAULT '1'
  , ADD COLUMN `resume_key` varchar(10) NULL; */

/** ALTER TABLE `bpmis_kcps_v1`.`ap_form_7`
  ADD COLUMN `status` int(4) unsigned NOT NULL DEFAULT '1'
  , ADD COLUMN `resume_key` varchar(10) NULL;*/

/** ALTER TABLE `bpmis_kcps_v1`.`ap_form_7_review`
  ADD COLUMN `status` int(4) unsigned NOT NULL DEFAULT '1'
  , ADD COLUMN `resume_key` varchar(10) NULL;*/

ALTER TABLE `bpmis_kcps_v1`.`ap_form_elements`
  MODIFY COLUMN `element_constraint` varchar(255) NULL
  , ADD COLUMN `element_table_name` varchar(200) NULL
  , ADD COLUMN `element_field_value` text NULL
  , ADD COLUMN `element_field_name` varchar(200) NULL
  , ADD COLUMN `element_option_query` text NULL
  , ADD COLUMN `element_existing_form` int(1) NULL
  , ADD COLUMN `element_existing_stage` int(1) NULL
  , ADD COLUMN `element_remote_username` varchar(250) NULL
  , ADD COLUMN `element_remote_password` varchar(250) NULL
  , ADD COLUMN `element_remote_value` text NULL
  , ADD COLUMN `element_remote_server_field` text NULL
  , ADD COLUMN `element_jsondef` text NULL
  , ADD COLUMN `element_remote_post` text NULL
  , ADD COLUMN `element_price_class` text NULL
  , ADD COLUMN `element_field_error_message` text NULL
  , ADD COLUMN `element_mark_file_with_qr_code` int(11) NULL
  , ADD COLUMN `element_file_qr_all_pages` int(11) NULL
  , ADD COLUMN `element_file_qr_page_position` varchar(255) NULL
  , ADD COLUMN `element_file_qr_users` varchar(255) NULL
  , ADD COLUMN `element_notify_contact` int(11) NULL;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`ap_form_filters` (
  `aff_id` int(11) unsigned NOT NULL auto_increment,
  `form_id` int(11) NOT NULL,
  `element_name` varchar(50) NOT NULL DEFAULT '',
  `filter_condition` varchar(15) NOT NULL DEFAULT 'is',
  `filter_keyword` varchar(255) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`aff_id`),
  KEY `form_id`(`form_id`)
) ENGINE=MyISAM;

ALTER TABLE `bpmis_kcps_v1`.`ap_forms`
  ADD COLUMN `logic_permission_enable` tinyint(1) NULL DEFAULT 0
  , ADD COLUMN `logic_workflow_enable` tinyint(1) NULL DEFAULT 0
  , ADD COLUMN `payment_enable_discount` int(11) NULL
  , ADD COLUMN `payment_discount_element_id` int(11) NULL
  , ADD COLUMN `payment_tax_amount` double NULL
  , ADD COLUMN `payment_discount_code` int(11) NULL
  , ADD COLUMN `webhook_enable` int(11) NULL
  , ADD COLUMN `payment_discount_type` int(11) NULL
  , ADD COLUMN `payment_discount_amount` int(11) NULL
  , ADD COLUMN `form_stage` varchar(255) NULL
  , ADD COLUMN `form_group` int(11) NULL
  , ADD COLUMN `form_department` int(11) NULL
  , ADD COLUMN `form_department_stage` int(11) NULL
  , ADD COLUMN `form_idn` varchar(255) NULL
  , ADD COLUMN `form_type` int(11) NULL
  , ADD COLUMN `form_code` varchar(255) NULL
  , ADD COLUMN `payment_cellulant_checkout_url` varchar(50) NULL
  , ADD COLUMN `payment_cellulant_merchant_username` varchar(50) NULL
  , ADD COLUMN `payment_cellulant_merchant_password` varchar(50) NULL
  , ADD COLUMN `payment_cellulant_service_id` varchar(50) NULL
  , ADD COLUMN `payment_jambopay_business` varchar(50) NULL
  , ADD COLUMN `payment_jambopay_shared_key` varchar(250) NULL 
   ,ADD COLUMN `payment_pesapal_live_secret_key` varchar(50) NULL ;
 
/*ALTER TABLE `bpmis_kcps_v1`.`form_groups`
  ADD COLUMN `group_parent` int(11) NOT NULL DEFAULT '0';*/

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`ap_form_groups` (
  `id` int(11) NOT NULL auto_increment,
  `form_id` int(11) NULL,
  `group_id` int(11) NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`form_id`) REFERENCES `bpmis_kcps_v1`.`ap_forms` (`form_id`) ON DELETE CASCADE,
  FOREIGN KEY (`group_id`) REFERENCES `bpmis_kcps_v1`.`form_groups` (`group_id`) ON DELETE CASCADE,
  KEY `ap_form_groups_form_id_idx`(`form_id`),
  KEY `ap_form_groups_group_id_idx`(`group_id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`ap_form_locks` (
  `form_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lock_date` datetime NOT NULL
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`ap_form_sorts` (
  `user_id` int(11) NOT NULL,
  `sort_by` varchar(25) NULL DEFAULT '',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`ap_form_themes` (
  `theme_id` int(11) unsigned NOT NULL auto_increment,
  `status` int(1) NULL DEFAULT '1',
  `theme_has_css` int(1) NOT NULL DEFAULT '0',
  `theme_name` varchar(255) NULL DEFAULT '',
  `theme_built_in` int(1) NOT NULL DEFAULT '0',
  `logo_type` varchar(11) NOT NULL DEFAULT 'default' COMMENT 'default,custom,disabled',
  `logo_custom_image` text NULL,
  `logo_custom_height` int(11) NOT NULL DEFAULT '40',
  `logo_default_image` varchar(50) NULL DEFAULT '',
  `logo_default_repeat` int(1) NOT NULL DEFAULT '0',
  `wallpaper_bg_type` varchar(11) NOT NULL DEFAULT 'color' COMMENT 'color,pattern,custom',
  `wallpaper_bg_color` varchar(11) NULL DEFAULT '',
  `wallpaper_bg_pattern` varchar(50) NULL DEFAULT '',
  `wallpaper_bg_custom` text NULL,
  `header_bg_type` varchar(11) NOT NULL DEFAULT 'color' COMMENT 'color,pattern,custom',
  `header_bg_color` varchar(11) NULL DEFAULT '',
  `header_bg_pattern` varchar(50) NULL DEFAULT '',
  `header_bg_custom` text NULL,
  `form_bg_type` varchar(11) NOT NULL DEFAULT 'color' COMMENT 'color,pattern,custom',
  `form_bg_color` varchar(11) NULL DEFAULT '',
  `form_bg_pattern` varchar(50) NULL DEFAULT '',
  `form_bg_custom` text NULL,
  `highlight_bg_type` varchar(11) NOT NULL DEFAULT 'color' COMMENT 'color,pattern,custom',
  `highlight_bg_color` varchar(11) NULL DEFAULT '',
  `highlight_bg_pattern` varchar(50) NULL DEFAULT '',
  `highlight_bg_custom` text NULL,
  `guidelines_bg_type` varchar(11) NOT NULL DEFAULT 'color' COMMENT 'color,pattern,custom',
  `guidelines_bg_color` varchar(11) NULL DEFAULT '',
  `guidelines_bg_pattern` varchar(50) NULL DEFAULT '',
  `guidelines_bg_custom` text NULL,
  `field_bg_type` varchar(11) NOT NULL DEFAULT 'color' COMMENT 'color,pattern,custom',
  `field_bg_color` varchar(11) NULL DEFAULT '',
  `field_bg_pattern` varchar(50) NULL DEFAULT '',
  `field_bg_custom` text NULL,
  `form_title_font_type` varchar(50) NOT NULL DEFAULT 'Lucida Grande',
  `form_title_font_weight` int(11) NOT NULL DEFAULT '400',
  `form_title_font_style` varchar(25) NOT NULL DEFAULT 'normal',
  `form_title_font_size` varchar(11) NULL DEFAULT '',
  `form_title_font_color` varchar(11) NULL DEFAULT '',
  `form_desc_font_type` varchar(50) NOT NULL DEFAULT 'Lucida Grande',
  `form_desc_font_weight` int(11) NOT NULL DEFAULT '400',
  `form_desc_font_style` varchar(25) NOT NULL DEFAULT 'normal',
  `form_desc_font_size` varchar(11) NULL DEFAULT '',
  `form_desc_font_color` varchar(11) NULL DEFAULT '',
  `field_title_font_type` varchar(50) NOT NULL DEFAULT 'Lucida Grande',
  `field_title_font_weight` int(11) NOT NULL DEFAULT '400',
  `field_title_font_style` varchar(25) NOT NULL DEFAULT 'normal',
  `field_title_font_size` varchar(11) NULL DEFAULT '',
  `field_title_font_color` varchar(11) NULL DEFAULT '',
  `guidelines_font_type` varchar(50) NOT NULL DEFAULT 'Lucida Grande',
  `guidelines_font_weight` int(11) NOT NULL DEFAULT '400',
  `guidelines_font_style` varchar(25) NOT NULL DEFAULT 'normal',
  `guidelines_font_size` varchar(11) NULL DEFAULT '',
  `guidelines_font_color` varchar(11) NULL DEFAULT '',
  `section_title_font_type` varchar(50) NOT NULL DEFAULT 'Lucida Grande',
  `section_title_font_weight` int(11) NOT NULL DEFAULT '400',
  `section_title_font_style` varchar(25) NOT NULL DEFAULT 'normal',
  `section_title_font_size` varchar(11) NULL DEFAULT '',
  `section_title_font_color` varchar(11) NULL DEFAULT '',
  `section_desc_font_type` varchar(50) NOT NULL DEFAULT 'Lucida Grande',
  `section_desc_font_weight` int(11) NOT NULL DEFAULT '400',
  `section_desc_font_style` varchar(25) NOT NULL DEFAULT 'normal',
  `section_desc_font_size` varchar(11) NULL DEFAULT '',
  `section_desc_font_color` varchar(11) NULL DEFAULT '',
  `field_text_font_type` varchar(50) NOT NULL DEFAULT 'Lucida Grande',
  `field_text_font_weight` int(11) NOT NULL DEFAULT '400',
  `field_text_font_style` varchar(25) NOT NULL DEFAULT 'normal',
  `field_text_font_size` varchar(11) NULL DEFAULT '',
  `field_text_font_color` varchar(11) NULL DEFAULT '',
  `border_form_width` int(11) NOT NULL DEFAULT '1',
  `border_form_style` varchar(11) NOT NULL DEFAULT 'solid',
  `border_form_color` varchar(11) NULL DEFAULT '',
  `border_guidelines_width` int(11) NOT NULL DEFAULT '1',
  `border_guidelines_style` varchar(11) NOT NULL DEFAULT 'solid',
  `border_guidelines_color` varchar(11) NULL DEFAULT '',
  `border_section_width` int(11) NOT NULL DEFAULT '1',
  `border_section_style` varchar(11) NOT NULL DEFAULT 'solid',
  `border_section_color` varchar(11) NULL DEFAULT '',
  `form_shadow_style` varchar(25) NOT NULL DEFAULT 'WarpShadow',
  `form_shadow_size` varchar(11) NOT NULL DEFAULT 'large',
  `form_shadow_brightness` varchar(11) NOT NULL DEFAULT 'normal',
  `form_button_type` varchar(11) NOT NULL DEFAULT 'text',
  `form_button_text` varchar(100) NOT NULL DEFAULT 'Submit',
  `form_button_image` text NULL,
  `advanced_css` text NULL,
  `user_id` int(11) NOT NULL DEFAULT '1',
  `theme_is_private` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`theme_id`),
  KEY `theme_name`(`theme_name`)
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`ap_grid_columns` (
  `id` int(11) NOT NULL auto_increment,
  `element_name` varchar(255) NOT NULL,
  `form_id` varchar(255) NOT NULL,
  `chart_id` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`ap_page_logic` (
  `form_id` int(11) NOT NULL,
  `page_id` varchar(15) NOT NULL DEFAULT '',
  `rule_all_any` varchar(3) NOT NULL DEFAULT 'all',
  PRIMARY KEY (`form_id`,`page_id`)
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`ap_page_logic_conditions` (
  `apc_id` int(11) unsigned NOT NULL auto_increment,
  `form_id` int(11) NOT NULL,
  `target_page_id` varchar(15) NOT NULL DEFAULT '',
  `element_name` varchar(50) NOT NULL DEFAULT '',
  `rule_condition` varchar(15) NOT NULL DEFAULT 'is',
  `rule_keyword` varchar(255) NULL,
  PRIMARY KEY (`apc_id`)
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`ap_permission_logic_conditions` (
  `alc_id` int(11) unsigned NOT NULL auto_increment,
  `form_id` int(11) NOT NULL,
  `target_element_id` int(11) NOT NULL,
  `element_name` varchar(50) NOT NULL DEFAULT '',
  `rule_condition` varchar(15) NOT NULL DEFAULT 'is',
  `rule_keyword` varchar(255) NULL,
  PRIMARY KEY (`alc_id`)
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`ap_permission_logic_elements` (
  `form_id` int(11) NOT NULL,
  `element_id` int(11) NOT NULL,
  `rule_show_hide` varchar(4) NOT NULL DEFAULT 'show',
  `rule_all_any` varchar(3) NOT NULL DEFAULT 'all',
  PRIMARY KEY (`form_id`,`element_id`)
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`ap_permissions` (
  `form_id` bigint(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `edit_form` tinyint(1) NOT NULL DEFAULT 0,
  `edit_entries` tinyint(1) NOT NULL DEFAULT 0,
  `view_entries` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`form_id`,`user_id`)
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`ap_report_elements` (
  `access_key` varchar(100) NULL,
  `form_id` int(11) NOT NULL,
  `chart_id` int(11) NOT NULL,
  `chart_position` int(11) NOT NULL DEFAULT '0',
  `chart_status` int(1) NOT NULL DEFAULT '1',
  `chart_datasource` varchar(25) NOT NULL DEFAULT '',
  `chart_type` varchar(25) NOT NULL DEFAULT '',
  `chart_enable_filter` int(1) NOT NULL DEFAULT '0',
  `chart_filter_type` varchar(5) NOT NULL DEFAULT 'all',
  `chart_title` text NULL,
  `chart_title_position` varchar(10) NOT NULL DEFAULT 'top',
  `chart_title_align` varchar(10) NOT NULL DEFAULT 'center',
  `chart_width` int(11) NOT NULL DEFAULT '0',
  `chart_height` int(11) NOT NULL DEFAULT '400',
  `chart_background` varchar(8) NULL,
  `chart_theme` varchar(25) NOT NULL DEFAULT 'blueopal',
  `chart_legend_visible` int(1) NOT NULL DEFAULT '1',
  `chart_legend_position` varchar(10) NOT NULL DEFAULT 'right',
  `chart_labels_visible` int(1) NOT NULL DEFAULT '1',
  `chart_labels_position` varchar(10) NOT NULL DEFAULT 'outsideEnd',
  `chart_labels_template` varchar(255) NOT NULL DEFAULT '#= category #',
  `chart_labels_align` varchar(10) NOT NULL DEFAULT 'circle',
  `chart_tooltip_visible` int(1) NOT NULL DEFAULT '1',
  `chart_tooltip_template` varchar(255) NOT NULL DEFAULT '#= category # - #= dataItem.entry # - #= kendo.format('', percentage)#',
  `chart_gridlines_visible` int(1) NOT NULL DEFAULT '1',
  `chart_bar_color` varchar(8) NULL,
  `chart_is_stacked` int(1) NOT NULL DEFAULT '0',
  `chart_is_vertical` int(1) NOT NULL DEFAULT '0',
  `chart_line_style` varchar(6) NOT NULL DEFAULT 'smooth',
  `chart_axis_is_date` int(1) NOT NULL DEFAULT '0',
  `chart_date_range` varchar(6) NOT NULL DEFAULT 'all' COMMENT 'all,period,custom',
  `chart_date_period_value` int(11) NOT NULL DEFAULT '1',
  `chart_date_period_unit` varchar(5) NOT NULL DEFAULT 'day',
  `chart_date_axis_baseunit` varchar(5) NULL,
  `chart_date_range_start` date NULL,
  `chart_date_range_end` date NULL,
  `chart_grid_page_size` int(11) NOT NULL DEFAULT '10',
  `chart_grid_max_length` int(11) NOT NULL DEFAULT '100',
  PRIMARY KEY (`form_id`,`chart_id`),
  KEY `access_key`(`access_key`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`ap_report_filters` (
  `arf_id` int(11) unsigned NOT NULL auto_increment,
  `form_id` int(11) NOT NULL,
  `chart_id` int(11) NOT NULL,
  `element_name` varchar(50) NOT NULL DEFAULT '',
  `filter_condition` varchar(15) NOT NULL DEFAULT 'is',
  `filter_keyword` varchar(255) NULL,
  PRIMARY KEY (`arf_id`),
  KEY `form_id`(`form_id`, `chart_id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`ap_reports` (
  `form_id` int(11) NOT NULL,
  `report_access_key` varchar(100) NULL,
  PRIMARY KEY (`form_id`),
  KEY `report_access_key`(`report_access_key`)
) ENGINE=InnoDB;


DROP TABLE ap_settings ; 
CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`ap_settings` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `smtp_enable` tinyint(1) NOT NULL DEFAULT 0,
  `smtp_host` varchar(255) NOT NULL DEFAULT 'localhost',
  `smtp_port` int(11) NOT NULL DEFAULT '25',
  `smtp_auth` tinyint(1) NOT NULL DEFAULT 0,
  `smtp_username` varchar(255) NULL,
  `smtp_password` varchar(255) NULL,
  `smtp_secure` tinyint(1) NOT NULL DEFAULT 0,
  `upload_dir` varchar(255) NOT NULL DEFAULT './data',
  `data_dir` varchar(255) NOT NULL DEFAULT './data',
  `upload_dir_web` varchar(255) NULL,
  `data_dir_web` varchar(255) NULL,
  `organisation_name` varchar(255) NOT NULL DEFAULT 'PermitFlow',
  `organisation_email` varchar(255) NULL,
  `organisation_description` varchar(255) NULL,
  `form_manager_max_rows` int(11) NULL DEFAULT '10',
  `admin_image_url` varchar(255) NULL,
  `disable_machform_link` int(1) NULL DEFAULT '0',
  `machform_version` varchar(10) NOT NULL DEFAULT '3.3',
  `admin_theme` varchar(11) NULL,
  `organisation_help` text NULL,
  `organisation_sidebar` text NULL,
  `profile_dir` text NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`ap_users` (
  `user_id` int(11) unsigned NOT NULL auto_increment,
  `user_email` varchar(255) NOT NULL DEFAULT '',
  `user_password` varchar(255) NOT NULL DEFAULT '',
  `user_fullname` varchar(255) NOT NULL DEFAULT '',
  `priv_administer` tinyint(1) NOT NULL DEFAULT 0,
  `priv_new_forms` tinyint(1) NOT NULL DEFAULT 0,
  `priv_new_themes` tinyint(1) NOT NULL DEFAULT 0,
  `last_login_date` datetime NULL,
  `last_ip_address` varchar(15) NULL DEFAULT '',
  `cookie_hash` varchar(255) NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0 - deleted; 1 - active; 2 - suspended',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`ap_workflow_logic_conditions` (
  `alc_id` int(11) unsigned NOT NULL auto_increment,
  `form_id` int(11) NOT NULL,
  `target_element_id` int(11) NOT NULL,
  `element_name` varchar(50) NOT NULL DEFAULT '',
  `rule_condition` varchar(15) NOT NULL DEFAULT 'is',
  `rule_keyword` varchar(255) NULL,
  PRIMARY KEY (`alc_id`)
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`ap_workflow_logic_elements` (
  `form_id` int(11) NOT NULL,
  `element_id` int(11) NOT NULL,
  `rule_show_hide` varchar(4) NOT NULL DEFAULT 'show',
  `rule_all_any` varchar(3) NOT NULL DEFAULT 'all',
  PRIMARY KEY (`form_id`,`element_id`)
) ENGINE=MyISAM;

CREATE TABLE IF NOT EXISTS `bpmis_kcps_v1`.`application_numbers` (
  `form_id` int(11) NOT NULL,
  `stage_id` int(11) NOT NULL,
  `application_number` varchar(255) NOT NULL
) ENGINE=InnoDB;

/** DROP INDEX `slot_id_idx` ON `bpmis_kcps_v1`.`conditions_of_approval`;*/

ALTER TABLE `bpmis_kcps_v1`.`conditions_of_approval`
  ADD COLUMN `permit_id` bigint(20) NULL;

ALTER TABLE `bpmis_kcps_v1`.`form_entry`
  MODIFY COLUMN `approved` int(11) NULL DEFAULT '0'
  , MODIFY COLUMN `declined` int(11) NULL DEFAULT '0'
  , ADD COLUMN `deleted_status` int(11) NOT NULL DEFAULT '0'
  , ADD COLUMN `pdf_path` varchar(250) NOT NULL DEFAULT '0';

CREATE INDEX `entry_id` ON `bpmis_kcps_v1`.`form_entry`(`entry_id`);

CREATE INDEX `user_id` ON `bpmis_kcps_v1`.`form_entry`(`user_id`);

ALTER TABLE `bpmis_kcps_v1`.`form_entry` ADD FOREIGN KEY (`form_id`) REFERENCES `bpmis_kcps_v1`.`ap_forms` (`form_id`) ON DELETE CASCADE;

/** ALTER TABLE `bpmis_kcps_v1`.`approval_condition` ADD FOREIGN KEY (`condition_id`) REFERENCES `bpmis_kcps_v1`.`conditions_of_approval` (`id`) ON DELETE CASCADE;*/

/** ALTER TABLE `bpmis_kcps_v1`.`approval_condition` ADD FOREIGN KEY (`entry_id`) REFERENCES `bpmis_kcps_v1`.`form_entry` (`id`) ON DELETE CASCADE;*/

CREATE TABLE IF NOT EXISTS `attached_permit` (
  `id` int(11) NOT NULL auto_increment,
  `application_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `entry_id` int(11) NOT NULL,
  `attachedby_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM;

/** DROP INDEX `content_type_id_idx` ON `bpmis_kcps_v1`.`content`;*/

ALTER TABLE `audit_trail`
  ADD COLUMN `object_id` int(11) NOT NULL DEFAULT '0'
  , ADD COLUMN `object_name` varchar(250) NULL
  , ADD COLUMN `before_values` text NULL
  , ADD COLUMN `after_values` text NULL
  , ADD COLUMN `ipaddress` varchar(250) NULL
  , ADD COLUMN `http_agent` varchar(250) NULL
  , ADD COLUMN `user_location` varchar(250) NULL;

CREATE INDEX `form_entry_id` ON `bpmis_kcps_v1`.`audit_trail`(`form_entry_id`);

ALTER TABLE `bpmis_kcps_v1`.`cf_formslot`
  ADD COLUMN `strdescription` varchar(250) NOT NULL DEFAULT '0';

ALTER TABLE `cf_user`
  ADD COLUMN `enable_email` int(11) NOT NULL DEFAULT '1'
  , ADD COLUMN `enable_chat` int(11) NOT NULL DEFAULT '1'
  , ADD COLUMN `about_me` text NULL
  , ADD COLUMN `profile_pic` varchar(250) NULL
  , ADD COLUMN `address` text NULL
  , ADD COLUMN `twitter` varchar(250) NULL
  , ADD COLUMN `facebook` varchar(250) NULL
  , ADD COLUMN `youtube` varchar(250) NULL
  , ADD COLUMN `linkedin` varchar(250) NULL
  , ADD COLUMN `pinterest` varchar(250) NULL
  , ADD COLUMN `instagram` varchar(250) NULL
  , ADD COLUMN `strtoken` varchar(100) NULL
  , ADD COLUMN `strtemppassword` varchar(250) NULL;

/** ALTER TABLE `comments`
  ADD COLUMN `resolved` int(11) NOT NULL DEFAULT '0';*/

ALTER TABLE `bpmis_kcps_v1`.`communication`
  ADD COLUMN `application_id` int(11) NULL;

CREATE INDEX `sender` ON `bpmis_kcps_v1`.`communication`(`sender`, `receiver`, `isread`, `created_on`, `application_id`);

ALTER TABLE `content`
  ADD COLUMN `parent_id` int(11) NOT NULL DEFAULT '0'
  , ADD COLUMN `visibility` int(11) NOT NULL DEFAULT '1'
  , ADD COLUMN `deleted` int(11) NOT NULL DEFAULT '0';

ALTER TABLE `bpmis_kcps_v1`.`department`
  ADD COLUMN `department_head` bigint(20) NULL;

ALTER TABLE `bpmis_kcps_v1`.`entry_decline`
  ADD COLUMN `declined_by` int(10) NULL
  , ADD COLUMN `edit_fields` text NULL;

CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) NOT NULL auto_increment,
  `subject` varchar(250) NOT NULL,
  `content` varchar(250) NOT NULL,
  `location` varchar(250) NOT NULL,
  `is_application_related` int(11) NOT NULL DEFAULT '0',
  `related_application` int(11) NULL,
  `user_id` int(11) NOT NULL,
  `datesent` varchar(250) NOT NULL,
  `start_date` varchar(250) NOT NULL,
  `end_date` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

/* CREATE TABLE IF NOT EXISTS `ext_locales` (
  `id` int(11) NOT NULL auto_increment,
  `locale_identifier` varchar(250) NOT NULL,
  `local_title` varchar(250) NOT NULL,
  `locale_description` text NULL,
  `is_default` int(11) NOT NULL DEFAULT '0',
  `text_align` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB; */

ALTER TABLE `ext_locales` ADD COLUMN `text_align` int(11) NOT NULL DEFAULT '0' ;

TRUNCATE ext_locales ;
TRUNCATE fee ;

CREATE TABLE IF NOT EXISTS `ext_translations` (
  `id` int(11) NOT NULL auto_increment,
  `locale` varchar(50) NOT NULL,
  `table_class` varchar(250) NOT NULL,
  `field_name` varchar(250) NOT NULL,
  `field_id` int(11) NOT NULL,
  `trl_content` longtext NOT NULL,
  `option_id` int(11) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `favorites` (
  `id` int(11) NOT NULL auto_increment,
  `subject` varchar(250) NOT NULL,
  `content` varchar(250) NOT NULL,
  `application_id` int(11) NOT NULL,
  `isread` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `datesent` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

ALTER TABLE  `fee`
  ADD COLUMN `fee_code` varchar(100) NULL
  , ADD COLUMN `invoiceid` int(11) NOT NULL DEFAULT '0'
  , ADD COLUMN `fee_category` varchar(256) NULL
  , ADD COLUMN `base_field` int(11) NULL
  , ADD COLUMN `fee_type` varchar(64) NULL
  , ADD COLUMN `percentage` varchar(100) NULL
  , ADD COLUMN `minimum_fee` varchar(256) NULL;

ALTER TABLE `fee` MODIFY COLUMN `amount` text;/* Use amount as the value field and will calculate based on fee type*/

CREATE TABLE IF NOT EXISTS `form_entry_archive` (
  `id` bigint(20) NOT NULL,
  `form_id` int(11) NULL,
  `entry_id` int(11) NULL,
  `user_id` int(11) NULL,
  `circulation_id` int(11) NULL,
  `approved` int(11) NULL DEFAULT '0',
  `application_id` varchar(255) NULL DEFAULT '0',
  `declined` int(11) NULL DEFAULT '0',
  `deleted_status` int(11) NOT NULL DEFAULT '0',
  `saved_permit` text NULL,
  `previous_submission` bigint(20) NOT NULL,
  `parent_submission` bigint(20) NOT NULL,
  `date_of_submission` varchar(250) NOT NULL,
  `date_of_response` varchar(250) NOT NULL,
  `date_of_issue` varchar(250) NOT NULL,
  `observation` text NOT NULL,
  `pdf_path` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `approvedx`(`approved`),
  KEY `entry_id_idx`(`entry_id`),
  KEY `form_id_idx`(`form_id`),
  KEY `user_id_idx`(`user_id`)
) ENGINE=InnoDB;

ALTER TABLE `bpmis_kcps_v1`.`form_entry_links`
  ADD COLUMN `date_of_submission` varchar(200) NOT NULL DEFAULT '0';

CREATE TABLE IF NOT EXISTS `RG_TEMP_1767895174_0` (
  `id` int(11) NOT NULL auto_increment,
  `title` text NOT NULL,
  `order_no` int(11) NOT NULL,
  `app_queuing` varchar(255) NULL DEFAULT 'DESC',
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

INSERT INTO `bpmis_kcps_v1`.`RG_TEMP_1767895174_0`(`id`,`title`,`order_no`,`app_queuing`) SELECT `id`,`title`,`order_no`,NULL FROM `bpmis_kcps_v1`.`menus`;

DROP TABLE `bpmis_kcps_v1`.`menus`;

ALTER TABLE `bpmis_kcps_v1`.`RG_TEMP_1767895174_0` RENAME TO `menus`;

CREATE TABLE IF NOT EXISTS `history` (
  `id` int(11) NOT NULL auto_increment,
  `subject` varchar(250) NOT NULL,
  `content` varchar(250) NOT NULL,
  `link` varchar(250) NOT NULL,
  `isread` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `datesent` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `invoice_api_account` (
  `id` int(11) NOT NULL auto_increment,
  `api_key` text NOT NULL,
  `api_secret` text NOT NULL,
  `mda_name` text NOT NULL,
  `mda_branch` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `invoicetemplates` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL DEFAULT '',
  `applicationform` int(11) NULL,
  `applicationstage` int(11) NULL,
  `content` text NOT NULL,
  `max_duration` varchar(255) NULL DEFAULT '',
  `due_duration` varchar(250) NULL,
  `invoice_number` varchar(250) NULL,
  `expiration_type` int(11) NULL,
  `payment_type` int(11) NULL,
  PRIMARY KEY (`id`),
  KEY `permits_applicationform_idx`(`applicationform`)
) ENGINE=InnoDB;

/**ALTER TABLE `bpmis_kcps_v1`.`mf_guard_group_permission` ADD FOREIGN KEY (`group_id`) REFERENCES `bpmis_kcps_v1`.`mf_guard_group` (`id`) ON DELETE CASCADE;*/

/**CREATE INDEX `mf_guard_user_permission_permission_id_mf_guard_permission_id` ON `bpmis_kcps_v1`.`mf_guard_user_permission`(`permission_id`);*/

/**ALTER TABLE `bpmis_kcps_v1`.`mf_guard_user_permission` ADD FOREIGN KEY (`permission_id`) REFERENCES `bpmis_kcps_v1`.`mf_guard_permission` (`id`) ON DELETE CASCADE;*/

ALTER TABLE `bpmis_kcps_v1`.`mf_invoice`
  ADD COLUMN `expires_at` datetime NULL
  , ADD COLUMN `mda_code` varchar(250) NULL
  , ADD COLUMN `service_code` varchar(250) NULL
  , ADD COLUMN `branch` varchar(250) NULL
  , ADD COLUMN `due_date` varchar(250) NULL
  , ADD COLUMN `payer_id` varchar(250) NULL
  , ADD COLUMN `payer_name` varchar(250) NULL
  , ADD COLUMN `total_amount` double NULL
  , ADD COLUMN `currency` varchar(250) NULL
  , ADD COLUMN `doc_ref_number` varchar(250) NULL
  , ADD COLUMN `template_id` bigint(20) NULL
  , ADD COLUMN `document_key` varchar(255) NULL
  , ADD COLUMN `remote_validate` int(11) NULL
  , ADD COLUMN `pdf_path` varchar(255) NULL;

CREATE INDEX `created_at` ON `bpmis_kcps_v1`.`mf_invoice`(`created_at`);

CREATE INDEX `paid` ON `bpmis_kcps_v1`.`mf_invoice`(`paid`);

CREATE TABLE IF NOT EXISTS `mf_invoice_archive` (
  `id` bigint(20) NOT NULL,
  `app_id` bigint(20) NULL,
  `invoice_number` varchar(255) NULL,
  `paid` bigint(20) NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `expires_at` datetime NULL,
  `mda_code` varchar(250) NULL,
  `service_code` varchar(250) NULL,
  `branch` varchar(250) NULL,
  `due_date` varchar(250) NULL,
  `payer_id` varchar(250) NULL,
  `payer_name` varchar(250) NULL,
  `total_amount` double NULL,
  `currency` varchar(250) NULL,
  `doc_ref_number` varchar(250) NULL,
  `template_id` bigint(20) NULL,
  `document_key` varchar(255) NULL,
  `remote_validate` int(11) NULL,
  `pdf_path` varchar(255) NULL,
  PRIMARY KEY (`id`),
  KEY `app_id_idx`(`app_id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `mf_invoice_detail_archive` (
  `id` bigint(20) NOT NULL,
  `invoice_id` bigint(20) NULL,
  `description` text NULL,
  `amount` varchar(255) NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `invoice_id_idx`(`invoice_id`)
) ENGINE=InnoDB;

ALTER TABLE `bpmis_kcps_v1`.`notification_history`
  MODIFY COLUMN `application_id` int(11) NULL;

CREATE INDEX `application_id` ON `bpmis_kcps_v1`.`notification_history`(`application_id`);

CREATE TABLE IF NOT EXISTS `notification_queue` (
  `id` bigint(20) NOT NULL auto_increment,
  `user_id` int(11) NULL,
  `notification` text NULL,
  `notification_type` varchar(250) NULL,
  `sent_on` varchar(250) NULL,
  `sent` int(11) NULL DEFAULT '0',
  `application_id` int(11) NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_idx`(`user_id`)
) ENGINE=InnoDB;

ALTER TABLE `bpmis_kcps_v1`.`notifications`
  MODIFY COLUMN `form_id` int(11) NULL
  , MODIFY COLUMN `autosend` int(11) NULL DEFAULT '1';

ALTER TABLE `bpmis_kcps_v1`.`permits`
  ADD COLUMN `applicationstage` int(11) NULL
  , ADD COLUMN `footer` text NULL
  , ADD COLUMN `max_duration` varchar(255) NULL DEFAULT ''
  , ADD COLUMN `remote_url` varchar(255) NULL
  , ADD COLUMN `remote_field` varchar(255) NULL
  , ADD COLUMN `remote_username` varchar(255) NULL
  , ADD COLUMN `remote_password` varchar(255) NULL
  , ADD COLUMN `remote_request_type` varchar(255) NULL
  , ADD COLUMN `page_type` varchar(250) NULL
  , ADD COLUMN `page_orientation` varchar(250) NULL;

ALTER TABLE `bpmis_kcps_v1`.`plot`
  MODIFY COLUMN `plot_no` int(11) NULL
  , MODIFY COLUMN `plot_location` varchar(255) NOT NULL DEFAULT '0';

CREATE TABLE IF NOT EXISTS `saved_permit` (
  `id` int(11) NOT NULL auto_increment,
  `type_id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `date_of_issue` varchar(250) NOT NULL,
  `date_of_expiry` varchar(250) NOT NULL,
  `created_by` int(11) NOT NULL,
  `last_updated` varchar(250) NULL,
  `permit_id` varchar(250) NULL,
  `document_key` varchar(250) NULL,
  `remote_result` text NULL,
  `permit_status` int(11) NULL DEFAULT '1',
  `remote_update_uuid` text NULL,
  `pdf_path` varchar(255) NULL,
  PRIMARY KEY (`id`),
  KEY `application_id`(`application_id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `saved_permit_archive` (
  `id` int(11) NOT NULL auto_increment,
  `type_id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `permit` text NOT NULL,
  `date_of_issue` varchar(250) NOT NULL,
  `date_of_expiry` varchar(250) NOT NULL,
  `created_by` int(11) NOT NULL,
  `last_updated` varchar(250) NULL,
  `permit_id` varchar(250) NULL,
  `document_key` varchar(250) NULL,
  `remote_result` text NULL,
  `permit_status` int(11) NULL DEFAULT '1',
  `remote_update_uuid` text NULL,
  `pdf_path` varchar(255) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

ALTER TABLE `bpmis_kcps_v1`.`sf_guard_user_profile`
  MODIFY COLUMN `registeras` int(11) NOT NULL
  , ADD COLUMN `profile_pic` varchar(250) NULL;

/** CREATE INDEX `sf_guard_user_profile_user_id_idx` ON `bpmis_kcps_v1`.`sf_guard_user_profile`(`user_id`); */

/**  CREATE INDEX `sub_menu_buttons_button_id_buttons_id` ON `bpmis_kcps_v1`.`sub_menu_buttons`(`button_id`);

CREATE INDEX `sub_menu_buttons_sub_menu_id_sub_menus_id` ON `bpmis_kcps_v1`.`sub_menu_buttons`(`sub_menu_id`); */

ALTER TABLE `bpmis_kcps_v1`.`sub_menus`
  MODIFY COLUMN `max_duration` int(11) NULL
  , MODIFY COLUMN `deleted` int(11) NULL DEFAULT '0'
  , MODIFY COLUMN `change_identifier` int(11) NULL DEFAULT '0'
  , MODIFY COLUMN `menu_id` int(11) NULL DEFAULT '0'
  , ADD COLUMN `app_queuing` varchar(256) NULL DEFAULT 'default'
  , ADD COLUMN `new_identifier` text NULL
  , ADD COLUMN `new_identifier_start` varchar(250) NULL
  , ADD COLUMN `stage_type` int(11) NULL DEFAULT '0'
  , ADD COLUMN `stage_property` int(11) NULL DEFAULT '0'
  , ADD COLUMN `stage_expired_movement` int(11) NULL DEFAULT '0'
  , ADD COLUMN `stage_type_movement` int(11) NULL DEFAULT '0'
  , ADD COLUMN `stage_type_notification` text NULL
  , ADD COLUMN `stage_type_movement_fail` int(11) NULL DEFAULT '0';

ALTER TABLE `bpmis_kcps_v1`.`task`
  ADD COLUMN `sheet_id` int(11) NULL
  , ADD COLUMN `task_stage` int(11) NULL;

CREATE TABLE IF NOT EXISTS `task_forms` (
  `id` int(11) NOT NULL auto_increment,
  `task_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `entry_id` int(11) NOT NULL,
  `created_on` varchar(250) NOT NULL,
  `updated_on` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `task_forms_settings` (
  `id` int(11) NOT NULL auto_increment,
  `task_type` int(11) NOT NULL,
  `task_department` varchar(250) NOT NULL,
  `task_application_id` int(11) NOT NULL,
  `task_comment_sheet` int(11) NOT NULL,
  `created_on` varchar(250) NOT NULL,
  `updated_on` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;




/* WorkflowCategories **/
CREATE TABLE IF NOT EXISTS `workflow_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/* Foreign key Menus */
/*ALTER TABLE `menus` ADD `app_queuing` INT(4) NULL AFTER `order_no`;*/

/*Update ap_form_{id} status*/
/*ALTER TABLE `ap_form_1` ADD `status` INT(4) NOT NULL;
ALTER TABLE `ap_form_14` ADD `status` INT(4) NOT NULL;
ALTER TABLE `ap_form_18` ADD `status` INT(4) NOT NULL;
ALTER TABLE `ap_form_20` ADD `status` INT(4) NOT NULL;
ALTER TABLE `ap_form_21` ADD `status` INT(4) NOT NULL;*/

/*Update ap_forms configuration data*/

/*Query to update ap_forms configuration data values for new form_stage and form_type fields in ap_forms for latest permitflow version*/
/**UPDATE ap_forms Inner Join ap_column_preferences on ap_forms.form_id=ap_column_preferences.form_id
SET ap_forms.form_id = CONCAT(ap_column_preferences.element_name,ap_column_preferences.starting_point), ap_forms.form_type = 1, ap_forms.payment_currency = 'RWF' 
WHERE ap_forms.form_id in (select form_id from ap_column_preferences where starting_point IS NOT NULL and starting_point <> ''); */

/*UPDATE ap_forms set form_stage=56 where form_id=14;
UPDATE ap_forms set form_stage=16 where form_id=20;*/

/*Update ap_element_options option with value in optioni from old database structure*/
/*Update ap_element_options set `option` = `optioni`; */


/* Update old permissions to reflect new permission structure */

UPDATE `mf_guard_permission` SET `name` = 'managestages' WHERE `mf_guard_permission`.`id` =170;
UPDATE `mf_guard_permission` SET `name` = 'manageinvoices' WHERE `mf_guard_permission`.`id` =195;
UPDATE `mf_guard_permission` SET `name` = 'managepermits' WHERE `mf_guard_permission`.`id` =155;


/** INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES (NULL, 'manageagencies', 'Manage Agencies'); */

/*Sort applications by descending date of submission*/
Update menus set app_queuing = 'DESC';

/*Update Departmens that were only saved as string in old version.*/
/** Insert into department (department_name) select cf_user.strdepartment from cf_user where cf_user.strdepartment not in (select department_name from department) and cf_user.strdepartment<>''; */

/*Permissions for Billing*/
/** INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES (NULL, 'access_billing', 'Access Billing Menu'); */

/*Permission for Manage Web Pages*/
/** insert into mf_guard_permission(name,description) values ('managewebpages', 'Manage Web Pages'); */


/** add missing Form Categories permission new code requires this */
/** INSERT INTO `mf_guard_permission` (`id` ,`name` ,`description`)VALUES ( NULL , 'manageformgroups', 'Manage Form Groups' ); */
/* Missing permission */
/** INSERT INTO `mf_guard_permission` (`name`, `description`) VALUES ('approvepaymentsupport', 'Can cancel invoices');
; */

/*Query to insert permissions/roles from new permitflow into old database.*/
/** INSERT IGNORE INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES
(NULL, 'viewaudittrail', 'View Audit Trail'),
(NULL, 'managedepartments', 'Manage Departments'),
(NULL, 'managepublication', 'Manage Articles'),
(NULL, 'manageusers', 'Manage Registered Members'),
(NULL, 'managegroups', 'Manage Groups'),
(NULL, 'manageroles', 'Manage Roles'),
(NULL, 'manageforms', 'Manage Forms'),
(NULL, 'managefees', 'Manage Fees'),
(NULL, 'managepermits', 'Manage Permits'),
(NULL, 'manageconditions', 'Manage Conditions'),
(NULL, 'manageplots', 'Manage Plots'),
(NULL, 'managereviewers', 'Manage Reviewers'),
(NULL, 'managecommentsheets', 'Manage Comment Sheets'),
(NULL, 'managestages', 'Manage Stages'),
(NULL, 'manageactions', 'Manage Actions'),
(NULL, 'managenotifications', 'Manage Notifications'),
(NULL, 'managewebpages', 'Manage Web Pages'),
(NULL, 'managefaqs', 'Manage FAQs'),
(NULL, 'managenews', 'Manage News Articles'),
(NULL, 'has_hod_access', 'Has Access to Head of Department Functions'),
(NULL, 'transfer_applications', 'Able to transfer ownership of applications between users'),
(NULL, 'editapplication', 'Edit Applications'),
(NULL, 'assigntask', 'Assign tasks'),
(NULL, 'createapplications', 'Create applications from backend'),
(NULL, 'access_applications', 'Access Applications Menu'),
(NULL, 'access_tasks', 'Access Tasks Menu'),
(NULL, 'access_members', 'Access Clients Page'),
(NULL, 'access_reviewers', 'Access Reviewers Page'),
(NULL, 'access_reports', 'Access Reports Menu'),
(NULL, 'access_help', 'Access Help Menu'),
(NULL, 'access_settings', 'Access Settings Menu'),
(NULL, 'access_content', 'Access Content Menu'),
(NULL, 'access_forms', 'Access Forms Menu'),
(NULL, 'access_security', 'Access Security Menu'),
(NULL, 'managelanguages', 'Manage Languages'),
(NULL, 'manageannouncements', 'Manage Announcements'),
(NULL, 'manageformgroups', 'Manage Form Groups'),
(NULL, 'manageinvoices', 'Manage Invoice Templates'),
(NULL, 'managereports', 'Manage Reports'),
(NULL, 'managecategories', 'Manage User Categories'),
(NULL, 'access_workflow', 'Access Workflow Menu'),
(NULL, 'access_billing', 'Access Billing Menu'),
(NULL, 'approvepayments', 'Approve Payments'),
(NULL, 'access_permits', 'Access Permits Menu'),
(NULL, 'sign_permits', 'Can sign permits using EchoSign API'),
(NULL, 'cancel_permits', 'Can cancel permits'); */

/** INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES (NULL, 'managepermitchecker', 'Can Manage Permit Checker'); */

/*Start Translations*/

/*Query to insert default english translation form element fields missing translations*/
INSERT INTO `ext_translations`(locale, table_class, field_name, field_id, trl_content, option_id)
select 'en_US', 'ap_form_elements', 'element_title', ap_form_elements.form_id, ap_form_elements.element_title, ap_form_elements.element_id from ap_form_elements where Concat(ap_form_elements.form_id, "###", ap_form_elements.element_id) not in (select Concat(ext_translations.field_id, "###", ext_translations.option_id) from ext_translations where table_class='ap_form_elements');
/*End Translations*/


/*Start New features*/


/*Option to set invoice number to application number*/
ALTER TABLE `invoicetemplates` ADD `use_application_number` INT(1) NOT NULL;

--
-- OTB: Table structure for table `application_number_history`
--
CREATE TABLE IF NOT EXISTS `application_number_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_entry_id` int(11) DEFAULT NULL,
  `application_number` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


/** INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES (NULL, 'managememberdatabase', 'Manage User Association Membership Databases'); */

ALTER TABLE `sf_guard_user_categories` ADD `member_no_element_id` int(11) NULL;
ALTER TABLE `sf_guard_user_categories` ADD `validation_email_element_id` int(11) NULL;
ALTER TABLE `sf_guard_user_categories` ADD `membership_email_match` int(1) NULL;
ALTER TABLE `sf_guard_user_categories` ADD `member_association_name` varchar(255) NULL;
ALTER TABLE `sf_guard_user_categories` ADD `member_email_verification_message` text NULL;
ALTER TABLE `sf_guard_user_categories` ADD `send_verification_email` int(1) NULL;

/*End New features*/
ALTER TABLE `sub_menus` ADD `stage_expired_invoice_action` INT(4) NOT NULL;
/*Select conditions */
/** INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES (NULL, 'resolvecomment', 'Can Resolve Comments and Conditions of Approval'); */
/* Fields for dynamic member database verification*/

/* OTB Patch -- Departmental Conditions */
ALTER TABLE conditions_of_approval ADD department_id INT(10) NULL AFTER permit_id;
/* Member database setup */
ALTER TABLE `sf_guard_user_categories` ADD `member_database` int(11) NULL;
ALTER TABLE `sf_guard_user_categories` ADD `member_database_member_no_field` int(11) NULL;
ALTER TABLE `sf_guard_user_categories` ADD `member_database_member_email_field` int(11) NULL;
ALTER TABLE `sf_guard_user_categories` ADD `member_database_member_name_field` INT(11) NOT NULL;
/* change application identifier */
ALTER TABLE `sub_menus` ADD `conditional_identifier` VARCHAR(255) NOT NULL;
ALTER TABLE `sub_menus` ADD `change_field_form` INT(11) NOT NULL;
ALTER TABLE `sub_menus` ADD `change_field_element` INT(11) NOT NULL;
ALTER TABLE `sub_menus` ADD `change_field_element_value` VARCHAR(255) NOT NULL;
ALTER TABLE `sub_menus` ADD `change_identifier_condition` INT(1) NOT NULL;
/* form_14 resume key */
ALTER TABLE `ap_form_14` CHANGE `date_created` `date_created` DATETIME NULL DEFAULT NULL;
ALTER TABLE `ap_form_14_review` CHANGE `date_created` `date_created` DATETIME NULL DEFAULT NULL;
ALTER TABLE `ap_form_14` ADD `resume_key` VARCHAR(10) NULL;
ALTER TABLE `ap_form_14_review` ADD `resume_key` VARCHAR(10) NULL;
/* form logic */
ALTER TABLE `ap_field_logic_elements` CHANGE `rule_show_hide` `rule_show_hide` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'show';
/* Submenus multiple conditions for app no change */
/*ALTER TABLE `sub_menus` ADD `change_field_element_value_1` varchar(255) DEFAULT NULL;
ALTER TABLE `sub_menus` ADD `conditional_identifier_1` varchar(255) DEFAULT NULL;
ALTER TABLE `sub_menus` ADD `change_field_element_value_2` varchar(255) DEFAULT NULL;
ALTER TABLE `sub_menus` ADD `conditional_identifier_2` varchar(255) DEFAULT NULL;
ALTER TABLE `sub_menus` ADD `change_field_element_value_3` varchar(255) DEFAULT NULL;
ALTER TABLE `sub_menus` ADD `conditional_identifier_3` varchar(255) DEFAULT NULL; */
 ;
/* Element footprint */
/*ALTER TABLE `ap_form_elements` ADD `element_footprint` INT NULL DEFAULT '0' AFTER `element_notify_contact` ;
ALTER TABLE `ap_form_elements` ADD `element_plotsize` INT NULL DEFAULT '0' AFTER `element_footprint` ;
ALTER TABLE `ap_form_elements` ADD `element_actualplotratio` INT NULL DEFAULT '0' AFTER `element_footprint` ;
ALTER TABLE `ap_form_elements` ADD `element_permittedgroundcoverage` INT NULL DEFAULT '0' AFTER `element_footprint` ;
ALTER TABLE `ap_form_elements` ADD `element_zone` INT NULL DEFAULT '0' AFTER `element_footprint` ;
ALTER TABLE `ap_form_elements` ADD `element_permitteduser` INT NULL DEFAULT '0' AFTER `element_footprint` ;
ALTER TABLE `ap_form_elements` ADD `element_projectcost` INT NULL DEFAULT '0' AFTER `element_footprint` ;
ALTER TABLE `ap_form_elements` ADD `element_groundcoveragereason` INT NULL DEFAULT '0' AFTER `element_footprint` ;
ALTER TABLE `ap_form_elements` ADD `element_plotratioreason` INT NULL DEFAULT '0' AFTER `element_footprint` ;*/
/* Sub menu condition element */
ALTER TABLE `sub_menus` ADD `change_field_element_value_4` VARCHAR(255) NULL , ADD `conditional_identifier_4` VARCHAR(255) NULL ;

/* CUSTOM SPECIFIC FOR RWANDA */
/** feedback module permissions */
/** INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES (NULL, 'accessfeedback', 'Access Feedback module'); */
/** Fee Range */
ALTER TABLE `fee_range` MODIFY COLUMN `result_value` text;

/*Multiple Conditions for fee_range*/
CREATE TABLE IF NOT EXISTS `fee_range_condition` (
`id` int(11) NOT NULL,
  `fee_range_id` int(11) DEFAULT NULL,
  `condition_field` int(11) DEFAULT NULL,
  `condition_operator` varchar(100) DEFAULT NULL,
  `condition_value` text DEFAULT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;
ALTER TABLE `fee_range_condition`
 ADD PRIMARY KEY (`id`);
 ALTER TABLE `fee_range_condition`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

/*Update Conditions from existing fee_ranges*/
/** INSERT INTO `fee_range_condition` (`fee_range_id`, `condition_field`, `condition_operator`, `condition_value`) Select id, condition_field, condition_operator, condition_value from fee_range; */

ALTER TABLE `fee_range` ADD `condition_set_operator` VARCHAR( 50 ) NULL;

/*Payment management from Kiambu Boni*/
/*Currency management*/
CREATE TABLE IF NOT EXISTS `currencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(256) NOT NULL,
  `name` varchar(256) NOT NULL,
  `code` varchar(256) NOT NULL,
  `symbol` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

/** INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES (NULL, 'managecurrencies', 'Manage System Currencies for Form Payments'); */

/*Merchant management*/
CREATE TABLE IF NOT EXISTS `merchant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `description` varchar(256) DEFAULT NULL,
  `link` varchar(256) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `code` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

/** INSERT INTO `countries` (`id`, `name`, `code`) VALUES
(2, 'Andorra', 'AD'),
(3, 'United Arab Emirates', 'AE'),
(4, 'Afghanistan', 'AF'),
(5, 'Antigua and Barbuda', 'AG'),
(6, 'Anguilla', 'AI'),
(7, 'Albania', 'AL'),
(8, 'Armenia', 'AM'),
(9, 'Angola', 'AO'),
(10, 'Antarctica', 'AQ'),
(11, 'Argentina', 'AR'),
(12, 'American Samoa', 'AS'),
(13, 'Austria', 'AT'),
(14, 'Australia', 'AU'),
(15, 'Aruba', 'AW'),
(16, '?land Islands', 'AX'),
(17, 'Azerbaijan', 'AZ'),
(18, 'Bosnia and Herzegovina', 'BA'),
(19, 'Barbados', 'BB'),
(20, 'Bangladesh', 'BD'),
(21, 'Belgium', 'BE'),
(22, 'Burkina Faso', 'BF'),
(23, 'Bulgaria', 'BG'),
(24, 'Bahrain', 'BH'),
(25, 'Burundi', 'BI'),
(26, 'Benin', 'BJ'),
(27, 'Saint Barth?lemy', 'BL'),
(28, 'Bermuda', 'BM'),
(29, 'Brunei Darussalam', 'BN'),
(30, 'Bolivia', 'BO'),
(31, 'Caribbean Netherlands ', 'BQ'),
(32, 'Brazil', 'BR'),
(33, 'Bahamas', 'BS'),
(34, 'Bhutan', 'BT'),
(35, 'Bouvet Island', 'BV'),
(36, 'Botswana', 'BW'),
(37, 'Belarus', 'BY'),
(38, 'Belize', 'BZ'),
(39, 'Canada', 'CA'),
(40, 'Cocos (Keeling) Islands', 'CC'),
(41, 'Congo, Democratic Republic of', 'CD'),
(42, 'Central African Republic', 'CF'),
(43, 'Congo', 'CG'),
(44, 'Switzerland', 'CH'),
(45, 'C?te d''Ivoire', 'CI'),
(46, 'Cook Islands', 'CK'),
(47, 'Chile', 'CL'),
(48, 'Cameroon', 'CM'),
(49, 'China', 'CN'),
(50, 'Colombia', 'CO'),
(51, 'Costa Rica', 'CR'),
(52, 'Cuba', 'CU'),
(53, 'Cape Verde', 'CV'),
(54, 'Cura?ao', 'CW'),
(55, 'Christmas Island', 'CX'),
(56, 'Cyprus', 'CY'),
(57, 'Czech Republic', 'CZ'),
(58, 'Germany', 'DE'),
(59, 'Djibouti', 'DJ'),
(60, 'Denmark', 'DK'),
(61, 'Dominica', 'DM'),
(62, 'Dominican Republic', 'DO'),
(63, 'Algeria', 'DZ'),
(64, 'Ecuador', 'EC'),
(65, 'Estonia', 'EE'),
(66, 'Egypt', 'EG'),
(67, 'Western Sahara', 'EH'),
(68, 'Eritrea', 'ER'),
(69, 'Spain', 'ES'),
(70, 'Ethiopia', 'ET'),
(71, 'Finland', 'FI'),
(72, 'Fiji', 'FJ'),
(73, 'Falkland Islands', 'FK'),
(74, 'Micronesia, Federated States of', 'FM'),
(75, 'Faroe Islands', 'FO'),
(76, 'France', 'FR'),
(77, 'Gabon', 'GA'),
(78, 'United Kingdom', 'GB'),
(79, 'Grenada', 'GD'),
(80, 'Georgia', 'GE'),
(81, 'French Guiana', 'GF'),
(82, 'Guernsey', 'GG'),
(83, 'Ghana', 'GH'),
(84, 'Gibraltar', 'GI'),
(85, 'Greenland', 'GL'),
(86, 'Gambia', 'GM'),
(87, 'Guinea', 'GN'),
(88, 'Guadeloupe', 'GP'),
(89, 'Equatorial Guinea', 'GQ'),
(90, 'Greece', 'GR'),
(91, 'South Georgia and the South Sandwich Islands', 'GS'),
(92, 'Guatemala', 'GT'),
(93, 'Guam', 'GU'),
(94, 'Guinea-Bissau', 'GW'),
(95, 'Guyana', 'GY'),
(96, 'Hong Kong', 'HK'),
(97, 'Heard and McDonald Islands', 'HM'),
(98, 'Honduras', 'HN'),
(99, 'Croatia', 'HR'),
(100, 'Haiti', 'HT'),
(101, 'Hungary', 'HU'),
(102, 'Indonesia', 'ID'),
(103, 'Ireland', 'IE'),
(104, 'Israel', 'IL'),
(105, 'Isle of Man', 'IM'),
(106, 'India', 'IN'),
(107, 'British Indian Ocean Territory', 'IO'),
(108, 'Iraq', 'IQ'),
(109, 'Iran', 'IR'),
(110, 'Iceland', 'IS'),
(111, 'Italy', 'IT'),
(112, 'Jersey', 'JE'),
(113, 'Jamaica', 'JM'),
(114, 'Jordan', 'JO'),
(115, 'Japan', 'JP'),
(116, 'Kenya', 'KE'),
(117, 'Kyrgyzstan', 'KG'),
(118, 'Cambodia', 'KH'),
(119, 'Kiribati', 'KI'),
(120, 'Comoros', 'KM'),
(121, 'Saint Kitts and Nevis', 'KN'),
(122, 'North Korea', 'KP'),
(123, 'South Korea', 'KR'),
(124, 'Kuwait', 'KW'),
(125, 'Cayman Islands', 'KY'),
(126, 'Kazakhstan', 'KZ'),
(127, 'Lao People''s Democratic Republic', 'LA'),
(128, 'Lebanon', 'LB'),
(129, 'Saint Lucia', 'LC'),
(130, 'Liechtenstein', 'LI'),
(131, 'Sri Lanka', 'LK'),
(132, 'Liberia', 'LR'),
(133, 'Lesotho', 'LS'),
(134, 'Lithuania', 'LT'),
(135, 'Luxembourg', 'LU'),
(136, 'Latvia', 'LV'),
(137, 'Libya', 'LY'),
(138, 'Morocco', 'MA'),
(139, 'Monaco', 'MC'),
(140, 'Moldova', 'MD'),
(141, 'Montenegro', 'ME'),
(142, 'Saint-Martin (France)', 'MF'),
(143, 'Madagascar', 'MG'),
(144, 'Marshall Islands', 'MH'),
(145, 'Macedonia', 'MK'),
(146, 'Mali', 'ML'),
(147, 'Myanmar', 'MM'),
(148, 'Mongolia', 'MN'),
(149, 'Macau', 'MO'),
(150, 'Northern Mariana Islands', 'MP'),
(151, 'Martinique', 'MQ'),
(152, 'Mauritania', 'MR'),
(153, 'Montserrat', 'MS'),
(154, 'Malta', 'MT'),
(155, 'Mauritius', 'MU'),
(156, 'Maldives', 'MV'),
(157, 'Malawi', 'MW'),
(158, 'Mexico', 'MX'),
(159, 'Malaysia', 'MY'),
(160, 'Mozambique', 'MZ'),
(161, 'Namibia', 'NA'),
(162, 'New Caledonia', 'NC'),
(163, 'Niger', 'NE'),
(164, 'Norfolk Island', 'NF'),
(165, 'Nigeria', 'NG'),
(166, 'Nicaragua', 'NI'),
(167, 'The Netherlands', 'NL'),
(168, 'Norway', 'NO'),
(169, 'Nepal', 'NP'),
(170, 'Nauru', 'NR'),
(171, 'Niue', 'NU'),
(172, 'New Zealand', 'NZ'),
(173, 'Oman', 'OM'),
(174, 'Panama', 'PA'),
(175, 'Peru', 'PE'),
(176, 'French Polynesia', 'PF'),
(177, 'Papua New Guinea', 'PG'),
(178, 'Philippines', 'PH'),
(179, 'Pakistan', 'PK'),
(180, 'Poland', 'PL'),
(181, 'St. Pierre and Miquelon', 'PM'),
(182, 'Pitcairn', 'PN'),
(183, 'Puerto Rico', 'PR'),
(184, 'Palestine, State of', 'PS'),
(185, 'Portugal', 'PT'),
(186, 'Palau', 'PW'),
(187, 'Paraguay', 'PY'),
(188, 'Qatar', 'QA'),
(189, 'R?union', 'RE'),
(190, 'Romania', 'RO'),
(191, 'Serbia', 'RS'),
(192, 'Russian Federation', 'RU'),
(193, 'Rwanda', 'RW'),
(194, 'Saudi Arabia', 'SA'),
(195, 'Solomon Islands', 'SB'),
(196, 'Seychelles', 'SC'),
(197, 'Sudan', 'SD'),
(198, 'Sweden', 'SE'),
(199, 'Singapore', 'SG'),
(200, 'Saint Helena', 'SH'),
(201, 'Slovenia', 'SI'),
(202, 'Svalbard and Jan Mayen Islands', 'SJ'),
(203, 'Slovakia', 'SK'),
(204, 'Sierra Leone', 'SL'),
(205, 'San Marino', 'SM'),
(206, 'Senegal', 'SN'),
(207, 'Somalia', 'SO'),
(208, 'Suriname', 'SR'),
(209, 'South Sudan', 'SS'),
(210, 'Sao Tome and Principe', 'ST'),
(211, 'El Salvador', 'SV'),
(212, 'Sint Maarten (Dutch part)', 'SX'),
(213, 'Syria', 'SY'),
(214, 'Swaziland', 'SZ'),
(215, 'Turks and Caicos Islands', 'TC'),
(216, 'Chad', 'TD'),
(217, 'French Southern Territories', 'TF'),
(218, 'Togo', 'TG'),
(219, 'Thailand', 'TH'),
(220, 'Tajikistan', 'TJ'),
(221, 'Tokelau', 'TK'),
(222, 'Timor-Leste', 'TL'),
(223, 'Turkmenistan', 'TM'),
(224, 'Tunisia', 'TN'),
(225, 'Tonga', 'TO'),
(226, 'Turkey', 'TR'),
(227, 'Trinidad and Tobago', 'TT'),
(228, 'Tuvalu', 'TV'),
(229, 'Taiwan', 'TW'),
(230, 'Tanzania', 'TZ'),
(231, 'Ukraine', 'UA'),
(232, 'Uganda', 'UG'),
(233, 'United States Minor Outlying Islands', 'UM'),
(234, 'United States', 'US'),
(235, 'Uruguay', 'UY'),
(236, 'Uzbekistan', 'UZ'),
(237, 'Vatican', 'VA'),
(238, 'Saint Vincent and the Grenadines', 'VC'),
(239, 'Venezuela', 'VE'),
(240, 'Virgin Islands (British)', 'VG'),
(241, 'Virgin Islands (U.S.)', 'VI'),
(242, 'Vietnam', 'VN'),
(243, 'Vanuatu', 'VU'),
(244, 'Wallis and Futuna Islands', 'WF'),
(245, 'Samoa', 'WS'),
(246, 'Yemen', 'YE'),
(247, 'Mayotte', 'YT'),
(248, 'South Africa', 'ZA'),
(249, 'Zambia', 'ZM'),
(250, 'Zimbabwe', 'ZW'); */

/*Start Form Logic for field dependancy*/

ALTER TABLE `ap_forms` ADD `logic_dependency_enable` INT(11) NULL;

CREATE TABLE IF NOT EXISTS `ap_dependency_logic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `element_id` int(11) NOT NULL,
  `rule_all_any` varchar(3) NOT NULL DEFAULT 'all',
  `integration_logic` int(1) NOT NULL DEFAULT 0,
  `integration_url` varchar(500) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

--
-- Table structure for table `ap_dependency_logic_rule_permitted_values`
--

CREATE TABLE IF NOT EXISTS `ap_dependency_logic_rule_permitted_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `element_id` int(11) NOT NULL,
  `element_name` varchar(50) NOT NULL,
  `logic_id` int(11) NOT NULL,
  `permitted_value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

--
-- Table structure for table `ap_dependency_logic_conditions`
--

CREATE TABLE IF NOT EXISTS `ap_dependency_logic_conditions` (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `logic_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `target_element_id` int(11) NOT NULL,
  `element_name` varchar(50) NOT NULL DEFAULT '',
  `rule_condition` varchar(15) NOT NULL DEFAULT 'is',
  `rule_keyword` varchar(255) DEFAULT NULL,
  `rule_keyword_option_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
/*End Form Logic for field dependancy*/

/*Task form location*/
ALTER TABLE `task_forms` ADD `longitude` varchar(64) DEFAULT NULL;
ALTER TABLE `task_forms` ADD `latitude` varchar(64) DEFAULT NULL;
/*Task form location*/

/*Onestop BPMIS Reports permissions*/
/*INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES (NULL, 'view_analytical_reports', 'Can view Management Reports');
INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES (NULL, 'manage_analytical_reports', 'Can Add and Configure Management Reports');
*/
/*Added invoice id to ap_form_payments*/
ALTER TABLE `ap_form_payments` ADD `invoice_id` INT(11) DEFAULT NULL;

/*Option for authorized user to set max duration for each permit instance*/
ALTER TABLE `permits` ADD `user_set_duration` INT(1) NOT NULL;
ALTER TABLE `permits` ADD `stage_set_duration` INT(4) NOT NULL;
CREATE TABLE IF NOT EXISTS `permit_cache` (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `application_id` int(11) NOT NULL,
  `template_id` int(11) NOT NULL,
  `period_uom` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `validity_period` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

/* quick solution for arranging service access as per RHA requirements i.e. Construction Permit > Province > District > Apply */
ALTER TABLE `ap_forms` ADD `province` VARCHAR(50) DEFAULT NULL;
Update `ap_forms` set province ='Northern Province' where form_id in (200000,213665,213916,214658,215327,202225,217182,203668,216233);
Update `ap_forms` set province ='Western Province' where form_id in (201251,219480,219859,220023,220384,202752,220990,205668,220200);

/* sms send id for menus table */
ALTER TABLE `menus` ADD `sms_sender` VARCHAR(255) DEFAULT NULL;

/* service code for menus table */
ALTER TABLE `menus` ADD `service_code` VARCHAR(255) DEFAULT NULL;

/*Combining Manual and Digital Payments*/
ALTER TABLE `sub_menus` ADD `stage_payment_confirmation` INT NULL DEFAULT '0';
/** New permission for users to access all available tasks */
/* INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES (NULL, 'accessalltasks', 'Permission for a reviewer to access all reviewers existing tasks '); */
/** Add skipped by **/
ALTER TABLE `task` ADD `skipped_by` INT NULL AFTER `task_stage`;
/** Permission to reset users task */
/* INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES (NULL, 'resettask', 'Permission for a reviewer to reset a task to pending for editing '); */
/** Property to mark a field to have class buildingcategory - used to control what different user categories can view when appliying */
ALTER TABLE `ap_form_elements` ADD `element_buildingcategory` INT NULL DEFAULT '0' AFTER `element_file_qr_users` ;

ALTER TABLE `ap_form_elements` ADD `element_buildingcoverage` INT NULL DEFAULT '0' AFTER `element_title`;
ALTER TABLE `ap_form_elements` ADD `element_plotsize` INT NULL DEFAULT '0' AFTER `element_title`;
ALTER TABLE `ap_form_elements` ADD `element_builtup` INT NULL DEFAULT '0' AFTER `element_title`;
ALTER TABLE `ap_form_elements` ADD `element_no_of_floors` INT NULL DEFAULT '0' AFTER `element_title`;
ALTER TABLE `ap_form_elements` ADD `element_grossfloorarea` INT NULL DEFAULT '0' AFTER `element_title`;
ALTER TABLE `ap_form_elements` ADD `element_floor_ratio` INT NULL DEFAULT '0' AFTER `element_title`;

CREATE TABLE IF NOT EXISTS `booklet` (`id` int(11) NOT NULL, `title` varchar(256) NOT NULL,`content` text NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `booklet` ADD PRIMARY KEY (`id`);
ALTER TABLE `booklet` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

/* Create Leaves Table */
CREATE TABLE IF NOT EXISTS `leave_request` (
  `id` int(10) NOT NULL,
  `reviewer_id` int(10) DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `status` varchar(256) NOT NULL,
  `name` varchar(256) NOT NULL,
  `approved_by` int(10) DEFAULT NULL,
  `approved_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `leave_type` varchar(256) DEFAULT NULL
) ;
ALTER TABLE `leave_request`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `leave_request`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

/* Permission to Approve Leave Requests */
/*INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) 
VALUES (NULL, 'can_approve_leave_requests', 'Permission to Approve Leave Requests.');*/
/* Add column for communication type */
alter table communications add column communication_type varchar(256) null ;
alter table communications add column msg_to_reviewer_id int(10) null ;
/** add district column */
ALTER TABLE ap_forms ADD COLUMN district VARCHAR(256) NULL ;
/* Order number for forms */
ALTER TABLE ap_forms ADD COLUMN order_no INT(10) NULL ;
/* missing payment_pesapal_live_public_key */
ALTER TABLE ap_forms ADD COLUMN payment_pesapal_live_public_key VARCHAR(256) NULL ;
/* payment_pesapal_test_secret_key */
ALTER TABLE ap_forms ADD COLUMN payment_pesapal_test_secret_key VARCHAR(256) NULL ;
/* payment_pesapal_test_public_key */
ALTER TABLE ap_forms ADD COLUMN payment_pesapal_test_public_key VARCHAR(256) NULL ;
/* payment_pesapal_enable_test_mode */
ALTER TABLE ap_forms ADD COLUMN payment_pesapal_enable_test_mode VARCHAR(256) NULL ;
/* TABLES TO TRUNCATE */
TRUNCATE cf_config ;
delete from cf_user where nid = 1 ; /* Delete to insert new */
delete from mf_user_profile where user_id = 0 ; /* Delete to insert new */
delete from sf_guard_user where id = 1 ;  /* Delete to insert new */

