/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  boniboy
 * Created: Jul 31, 2017
 */

select mf_invoice.id as Invoice_Id,
form_entry.application_id as Plan_Number,

CASE form_entry.form_id 
WHEN 200000 THEN CONCAT(ap_form_200000.element_5_1, ' ', ap_form_200000.element_5_2) WHEN 201251 THEN CONCAT(ap_form_201251.element_5_1, ' ', ap_form_201251.element_5_2) 
WHEN 213665 THEN CONCAT(ap_form_213665.element_5_1, ' ', ap_form_213665.element_5_2) WHEN 219480 THEN CONCAT(ap_form_219480.element_5_1, ' ', ap_form_219480.element_5_2) 
WHEN 215327 THEN CONCAT(ap_form_215327.element_5_1, ' ', ap_form_215327.element_5_2) WHEN 220384 THEN CONCAT(ap_form_220384.element_5_1, ' ', ap_form_220384.element_5_2) 
WHEN 214658 THEN CONCAT(ap_form_214658.element_5_1, ' ', ap_form_214658.element_5_2) WHEN 220023 THEN CONCAT(ap_form_220023.element_5_1, ' ', ap_form_220023.element_5_2) 
WHEN 213916 THEN CONCAT(ap_form_213916.element_5_1, ' ', ap_form_213916.element_5_2) WHEN 219859 THEN CONCAT(ap_form_219859.element_5_1, ' ', ap_form_219859.element_5_2) 
WHEN 202225 THEN CONCAT(ap_form_202225.element_92_1, ' ', ap_form_202225.element_92_2) WHEN 202752 THEN CONCAT(ap_form_202752.element_88_1, ' ', ap_form_202752.element_88_2) 
WHEN 217182 THEN CONCAT(ap_form_217182.element_5_1, ' ', ap_form_217182.element_5_2) WHEN 220990 THEN CONCAT(ap_form_220990.element_5_1, ' ', ap_form_220990.element_5_2) 
/** Huye and Rusizi */
WHEN 235992 THEN CONCAT(ap_form_235992.element_5_1, ' ', ap_form_235992.element_5_2) WHEN 229692 THEN CONCAT(ap_form_229692.element_5_1, ' ', ap_form_229692.element_5_2) 
WHEN 239228 THEN CONCAT(ap_form_239228.element_5_1, ' ', ap_form_239228.element_5_2) WHEN 233480 THEN CONCAT(ap_form_233480.element_5_1, ' ', ap_form_233480.element_5_2) 
WHEN 234966 THEN CONCAT(ap_form_234966.element_5_1, ' ', ap_form_234966.element_5_2) WHEN 230688 THEN CONCAT(ap_form_230688.element_5_1, ' ', ap_form_230688.element_5_2) 
WHEN 236928 THEN CONCAT(ap_form_236928.element_5_1, ' ', ap_form_236928.element_5_2) WHEN 232199 THEN CONCAT(ap_form_232199.element_5_1, ' ', ap_form_232199.element_5_2) 
WHEN 234647 THEN CONCAT(ap_form_234647.element_5_1, ' ', ap_form_234647.element_5_2) WHEN 232964 THEN CONCAT(ap_form_232964.element_5_1, ' ', ap_form_232964.element_5_2) 
WHEN 238289 THEN CONCAT(ap_form_238289.element_92_1, ' ', ap_form_238289.element_92_2) WHEN 234050 THEN CONCAT(ap_form_234050.element_92_1, ' ',ap_form_234050.element_92_2) 
WHEN 235542 THEN CONCAT(ap_form_235542.element_5_1, ' ', ap_form_235542.element_5_2) WHEN 231642 THEN CONCAT(ap_form_231642.element_5_1, ' ', ap_form_231642.element_5_2) 
/* Muhanga & Nyagatare */
WHEN 318175 THEN CONCAT(ap_form_318175.element_5_1, ' ', ap_form_318175.element_5_2) WHEN 305428 THEN CONCAT(ap_form_305428.element_5_1, ' ', ap_form_305428.element_5_2) 
WHEN 314847 THEN CONCAT(ap_form_314847.element_5_1, ' ', ap_form_314847.element_5_2) WHEN 307681 THEN CONCAT(ap_form_307681.element_5_1, ' ', ap_form_307681.element_5_2) 
WHEN 313852 THEN CONCAT(ap_form_313852.element_5_1, ' ', ap_form_313852.element_5_2) WHEN 305834 THEN CONCAT(ap_form_305834.element_5_1, ' ', ap_form_305834.element_5_2)  
WHEN 312093 THEN CONCAT(ap_form_312093.element_5_1, ' ', ap_form_312093.element_5_2) WHEN 304297 THEN CONCAT(ap_form_304297.element_5_1, ' ', ap_form_304297.element_5_2) 
WHEN 311508 THEN CONCAT(ap_form_311508.element_5_1, ' ', ap_form_311508.element_5_2) WHEN 303786 THEN CONCAT(ap_form_303786.element_5_1, ' ', ap_form_303786.element_5_2) 
WHEN 312723 THEN CONCAT(ap_form_312723.element_5_1, ' ', ap_form_312723.element_5_2) WHEN 304656 THEN CONCAT(ap_form_304656.element_5_1, ' ', ap_form_304656.element_5_2) 
WHEN 314056 THEN CONCAT(ap_form_314056.element_92_1, ' ', ap_form_314056.element_92_2) WHEN 306752 THEN CONCAT(ap_form_306752.element_92_1, ' ', ap_form_306752.element_92_2)


ELSE 'Undefined' END AS Developer,

sf_guard_user_profile.fullname as Applicant,
mf_invoice.invoice_number as Invoice_Number,
mf_invoice.total_amount as Amount, 
CASE mf_invoice.paid WHEN 1 THEN 'Pending Payment' WHEN 2 THEN 'Paid' WHEN 15 THEN 'Pending Payment Confirmation' END AS Payment_Status, 
mf_invoice.created_at as Invoice_date,
mf_invoice.due_date as Due_date,
mf_invoice.expires_at as Expiry_date,
mf_invoice.service_code as Service_code,
ap_forms.form_name AS District,

CASE form_entry.form_id
WHEN 200000 THEN 'Construction Permit' WHEN 201251 THEN 'Construction Permit' WHEN 318175 THEN 'Construction Permit'  WHEN 305428 THEN 'Construction Permit' 
WHEN 235992 THEN 'Construction Permit' WHEN 229692 THEN 'Construction Permit'
WHEN 219480 THEN 'Extension Permit' WHEN 213665 THEN 'Extension Permit' 
WHEN 239228 THEN 'Extension Permit' WHEN 233480 THEN 'Extension Permit' 
WHEN 314847 THEN 'Extension Permit' WHEN 307681 THEN 'Extension Permit'
WHEN 215327 THEN 'Demolition Permit' WHEN 220384 THEN 'Demolition Permit' 
WHEN 234966 THEN 'Demolition Permit' WHEN 230688 THEN 'Demolition Permit' 
WHEN 312093 THEN 'Demolition Permit' WHEN 304297 THEN 'Demolition Permit'
WHEN 214658 THEN 'Refurbishment Without Structural Alteration' WHEN 220023 THEN 'Refurbishment Without Structural Alteration' 
WHEN 236928 THEN 'Refurbishment Without Structural Alteration' WHEN 232199 THEN 'Refurbishment Without Structural Alteration' 
WHEN 313852 THEN 'Refurbishment Without Structural Alteration' WHEN 305834 THEN 'Refurbishment Without Structural Alteration'
WHEN 213916 THEN 'Refurbishment With Structural Alteration' WHEN 219859 THEN 'Refurbishment With Structural Alteration' 
WHEN 234647 THEN 'Refurbishment With Structural Alteration' WHEN 232964 THEN 'Refurbishment With Structural Alteration' 
WHEN 311508 THEN 'Refurbishment With Structural Alteration' WHEN 303786 THEN 'Refurbishment With Structural Alteration'
WHEN 202225 THEN 'Occupancy' WHEN 202752 THEN 'Occupancy' 
WHEN 238289 THEN 'Occupancy' WHEN 234050 THEN 'Occupancy' 
WHEN 314056 THEN 'Occupancy' WHEN 306752 THEN 'Occupancy'
WHEN 217182 THEN 'Change of Use' WHEN 220990 THEN 'Change of Use'
WHEN 235542 THEN 'Change of Use' WHEN 231642 THEN 'Change of Use'
 WHEN 312723 THEN 'Change of Use' WHEN 304656 THEN 'Change of Use'
ELSE 'Undefined' END AS Type_of_Permit

from
mf_invoice left join form_entry on mf_invoice.app_id=form_entry.id
left join sf_guard_user_profile on form_entry.user_id=sf_guard_user_profile.user_id
left join ap_form_200000 on ap_form_200000.id = form_entry.entry_id and form_id = 200000
left join ap_form_201251 on ap_form_201251.id = form_entry.entry_id and form_id = 201251
left join ap_form_219480 on ap_form_219480.id = form_entry.entry_id and form_id = 219480
left join ap_form_213665 on ap_form_213665.id = form_entry.entry_id and form_id = 213665
left join ap_form_215327 on ap_form_215327.id = form_entry.entry_id and form_id = 215327
left join ap_form_220384 on ap_form_220384.id = form_entry.entry_id and form_id = 220384
left join ap_form_214658 on ap_form_214658.id = form_entry.entry_id and form_id = 214658
left join ap_form_220023 on ap_form_220023.id = form_entry.entry_id and form_id = 220023
left join ap_form_213916 on ap_form_213916.id = form_entry.entry_id and form_id = 213916
left join ap_form_219859 on ap_form_219859.id = form_entry.entry_id and form_id = 219859
left join ap_form_202225 on ap_form_202225.id = form_entry.entry_id and form_id = 202225
left join ap_form_202752 on ap_form_202752.id = form_entry.entry_id and form_id = 202752
left join ap_form_217182 on ap_form_217182.id = form_entry.entry_id and form_id = 217182
left join ap_form_220990 on ap_form_220990.id = form_entry.entry_id and form_id = 220990

/* Huye and rusizi */
left join ap_form_235992 on ap_form_235992.id = form_entry.entry_id and form_id = 235992
left join ap_form_239228 on ap_form_239228.id = form_entry.entry_id and form_id = 239228
left join ap_form_234966 on ap_form_234966.id = form_entry.entry_id and form_id = 234966
left join ap_form_236928 on ap_form_236928.id = form_entry.entry_id and form_id = 236928
left join ap_form_234647 on ap_form_234647.id = form_entry.entry_id and form_id = 234647
left join ap_form_238289 on ap_form_238289.id = form_entry.entry_id and form_id = 238289
left join ap_form_235542 on ap_form_235542.id = form_entry.entry_id and form_id = 235542

left join ap_form_229692 on ap_form_229692.id = form_entry.entry_id and form_id = 229692
left join ap_form_233480 on ap_form_233480.id = form_entry.entry_id and form_id = 233480
left join ap_form_230688 on ap_form_230688.id = form_entry.entry_id and form_id = 230688
left join ap_form_232199 on ap_form_232199.id = form_entry.entry_id and form_id = 232199
left join ap_form_232964 on ap_form_232964.id = form_entry.entry_id and form_id = 232964
left join ap_form_234050 on ap_form_234050.id = form_entry.entry_id and form_id = 234050
left join ap_form_231642 on ap_form_231642.id = form_entry.entry_id and form_id = 231642

/* Muhanga */
left join ap_form_318175 on ap_form_318175.id = form_entry.entry_id and form_id = 318175
left join ap_form_314847 on ap_form_314847.id = form_entry.entry_id and form_id = 314847
left join ap_form_312093 on ap_form_312093.id = form_entry.entry_id and form_id = 312093
left join ap_form_313852 on ap_form_313852.id = form_entry.entry_id and form_id = 313852
left join ap_form_311508 on ap_form_311508.id = form_entry.entry_id and form_id = 311508
left join ap_form_314056 on ap_form_314056.id = form_entry.entry_id and form_id = 314056
left join ap_form_312723 on ap_form_312723.id = form_entry.entry_id and form_id = 312723
/* Nyagarare */
left join ap_form_305428 on ap_form_305428.id = form_entry.entry_id and form_id = 305428
left join ap_form_307681 on ap_form_307681.id = form_entry.entry_id and form_id = 307681
left join ap_form_304297 on ap_form_304297.id = form_entry.entry_id and form_id = 304297
left join ap_form_305834 on ap_form_305834.id = form_entry.entry_id and form_id = 305834
left join ap_form_303786 on ap_form_303786.id = form_entry.entry_id and form_id = 303786
left join ap_form_306752 on ap_form_306752.id = form_entry.entry_id and form_id = 306752
left join ap_form_304656 on ap_form_304656.id = form_entry.entry_id and form_id = 304656

left join ap_forms on ap_forms.form_id=form_entry.form_id
left join form_groups on form_groups.group_id=ap_forms.form_group
where form_entry.form_id in(200000,201251,213665,219480,215327,220384,214658,220023,213916,219859,202225,202752,217182,220990,235992,239228,234966,236928,234647,238289,235542,229692,
233480,230688,232199,232964,234050,231642,318175,314847,312093,313852,311508,314056,312723,
305428,307681,304297,305834,303786,306752,304656) and parent_submission=0 and deleted_status = 0
and mf_invoice.paid in (2)