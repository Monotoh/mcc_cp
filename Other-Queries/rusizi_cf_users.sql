-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: testbed
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `rusizi_cf_users`
--

DROP TABLE IF EXISTS `rusizi_cf_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rusizi_cf_users` (
  `nid` int(11) NOT NULL DEFAULT '0',
  `strlastname` text CHARACTER SET utf8 NOT NULL,
  `strfirstname` text CHARACTER SET utf8 NOT NULL,
  `stremail` text CHARACTER SET utf8 NOT NULL,
  `naccesslevel` int(11) NOT NULL DEFAULT '0',
  `struserid` text CHARACTER SET utf8 NOT NULL,
  `strpassword` text CHARACTER SET utf8 NOT NULL,
  `stremail_format` varchar(8) CHARACTER SET utf8 NOT NULL DEFAULT 'HTML',
  `stremail_values` varchar(8) CHARACTER SET utf8 NOT NULL DEFAULT 'IFRAME',
  `nsubstitudeid` int(11) NOT NULL DEFAULT '0',
  `tslastaction` int(11) NOT NULL,
  `bdeleted` int(11) NOT NULL,
  `strstreet` text CHARACTER SET utf8 NOT NULL,
  `strcountry` text CHARACTER SET utf8 NOT NULL,
  `strzipcode` text CHARACTER SET utf8 NOT NULL,
  `strcity` text CHARACTER SET utf8 NOT NULL,
  `strphone_main1` text CHARACTER SET utf8 NOT NULL,
  `strphone_main2` text CHARACTER SET utf8 NOT NULL,
  `strphone_mobile` text CHARACTER SET utf8 NOT NULL,
  `strfax` text CHARACTER SET utf8 NOT NULL,
  `strorganisation` text CHARACTER SET utf8 NOT NULL,
  `strdepartment` text CHARACTER SET utf8 NOT NULL,
  `strcostcenter` text CHARACTER SET utf8 NOT NULL,
  `userdefined1_value` text CHARACTER SET utf8 NOT NULL,
  `userdefined2_value` text CHARACTER SET utf8 NOT NULL,
  `nsubstitutetimevalue` int(11) NOT NULL,
  `strsubstitutetimeunit` text CHARACTER SET utf8 NOT NULL,
  `busegeneralsubstituteconfig` int(11) NOT NULL,
  `busegeneralemailconfig` int(11) NOT NULL,
  `enable_email` int(11) NOT NULL DEFAULT '1',
  `enable_chat` int(11) NOT NULL DEFAULT '1',
  `about_me` text CHARACTER SET utf8,
  `profile_pic` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `address` text CHARACTER SET utf8,
  `twitter` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `facebook` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `youtube` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `linkedin` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `pinterest` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `instagram` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `strtoken` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `strtemppassword` varchar(250) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rusizi_cf_users`
--

LOCK TABLES `rusizi_cf_users` WRITE;
/*!40000 ALTER TABLE `rusizi_cf_users` DISABLE KEYS */;
INSERT INTO `rusizi_cf_users` VALUES (200051,'Napoleon','NTAWUHARUWE ','napoleon.ntawuharuwe@rusizi.gov.rw',1,'napoleon.ntawuharuwe','$2y$10$YGFpA7QRsLazDWe6mHnbc..TYbnkeP64VvZDgoE6tXBXd5t3PkH3u','','',0,1488527756,0,'','','','','0788791150','','','','','Rusizi Building Permit Section OSC','','','',0,'',0,0,0,0,'','','','','','','','','','8e14bb10c83059da30b1982bc194d54e','$2y$10$uZjdaKMgkJbiPrHaOju6EeNI08XX4hIQJkVwhOB.S7h02TdEOUok6'),(200052,'Laurien','IRAGUHA ','laurien.iraguha@rusizi.gov.rw',1,'laurien.iraguha','$2y$10$CTiKXqZrFKnJSDbSDRTQZ.2ghBVC/0toIdQCYYaW9IT2SyFevlLHS','','',0,1488420640,0,'','','','','0783731880','','','','','Rusizi Building Permit Section OSC','','','',0,'',0,0,0,0,'','','','','','','','','','65ca042898070845cac3e31c977ec853','$2y$10$dHqDf45Z/C/ZRKaTkf62D.4N.jWccC09iP.I13A6FmEs3KFKr4RmC'),(200053,'Jean Damascene','NZABANDORA ','ict@rusizi.gov.rw',1,'ict_rusizi','$2y$10$J9VAWPPKSJDS2YafmB6VSOESqFHd7D4gVqOTk1OCPyPpcFtGXzKDG','','',0,1488532486,0,'','','','','0788551314','','','','','Rusizi Building Permit Section OSC','','','',0,'',0,0,0,0,'','','','','','','','','','',''),(200057,' FRATERNE','UWABABAYEYI','fraterne.uwababayeyi@rusizi.gov.rw',1,'fraterne.uwababayey','$2y$10$mo1e4wOY3cv6eL/2NdU0ueJbpI0r04/m019E3rVvJl/dGiRQn.zxC','','',0,1488528179,0,'','','','','','','0725512312','','','Rusizi Building Permit Section OSC','','','',0,'',0,0,0,0,'','','','','','','','','','',''),(200060,'DONATIEN','GATETE ','donatien.gatete@rusizi.gov.rw',1,'donatien.gatete','$2y$10$QG/bNkTGZqhJMhuaLt4c6O92MrOYKvbyr4/urGnoQ8.Tbdl7p6CX.','','',0,1488532390,0,'','','','','','','','','','Rusizi Building Permit Section OSC','','','',0,'',0,0,0,0,'','','','','','','','','','','');
/*!40000 ALTER TABLE `rusizi_cf_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-04 13:20:21
