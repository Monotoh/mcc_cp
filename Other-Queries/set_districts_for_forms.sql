/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  boniboy
 * Created: Apr 4, 2017
 */

/* Rusizi */
create table rusizi_forms as (select form_id from ap_forms where form_name like '%RUSIZI DISTRICT%');
UPDATE ap_forms set district = 'RUSIZI DISTRICT' WHERE form_id in (select form_id from rusizi_forms where form_name like '%RUSIZI DISTRICT%') ;

/* huye */
create table huye_forms as (select form_id from ap_forms where form_name like '%HUYE DISTRICT%');
UPDATE ap_forms set district = 'HUYE DISTRICT' WHERE form_id in (select form_id from huye_forms where form_name like '%HUYE DISTRICT%') ;

/* musanze */
create table musanze_forms as (select form_id from ap_forms where form_name like '%MUSANZE DISTRICT%');
UPDATE ap_forms set district = 'MUSANZE DISTRICT' WHERE form_id in (select form_id from musanze_forms where form_name like '%MUSANZE DISTRICT%') ;

/* RUBAVU */
create table rubavu_forms as (select form_id from ap_forms where form_name like '%RUBAVU DISTRICT%');
UPDATE ap_forms set district = 'RUBAVU DISTRICT' WHERE form_id in (select form_id from rubavu_forms where form_name like '%RUBAVU DISTRICT%') ;
