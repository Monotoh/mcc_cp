/*
Steps for CPMIS Database Migration:

1. Use red-gate to get compare and synchronize new CPMIS database schema/structure
2. Run data insert and update queries for configuration data
*/

/*Update ap_forms configuration data*/

/*Query to update ap_forms configuration data values for new form_stage and form_type fields in ap_forms for latest permitflow version*/
UPDATE ap_forms Inner Join ap_column_preferences on ap_forms.form_id=ap_column_preferences.form_id
SET ap_forms.form_stage = ap_column_preferences.starting_point, ap_forms.form_idn = ap_column_preferences.element_name, ap_forms.form_type = 1 
WHERE ap_forms.form_id in (select form_id from ap_column_preferences);

/* Query to add fee_category on fee tables */

ALTER TABLE `fee` ADD `fee_category` VARCHAR( 256 ) NULL AFTER `invoiceid`;


/* Update old permissions to reflect new permission structure */

UPDATE `mf_guard_permission` SET `name` = 'managestages' WHERE `mf_guard_permission`.`id` =170;

UPDATE `mf_guard_permission` SET `name` = 'manageinvoices' WHERE `mf_guard_permission`.`id` =195;

UPDATE `mf_guard_permission` SET `name` = 'managepermits' WHERE `mf_guard_permission`.`id` =155;

/* http://stackoverflow.com/questions/2528181/update-multiple-rows-with-one-query */

/* form id 60 is not in table sf_guard_user_categories_forms do an insert */
INSERT INTO `sf_guard_user_categories_forms` (`id`, `categoryid`, 
`formid`, `islinkedto`, `islinkedtitle`) 
VALUES (NULL, '2', '60', '0', NULL);


/* Update ap_forms and set form_group value - DO NOT run query with form_group_ids. Query below gets exactly how the form groups were configured.*/
Update ap_forms SET `form_group` = (Select ap_form_groups.group_id from ap_form_groups where form_id=ap_forms.form_id order by ap_form_groups.id desc LIMIT 1);

/*Update existing comment sheet to correct type stage and department*/
Update ap_forms set form_department=1, form_department_stage=862, form_type=2 where form_id=1067;

/*Update Departmens that were only saved as string in old version.*/
Insert into department (department_name) select cf_user.strdepartment from cf_user where cf_user.strdepartment not in (select department_name from department) and cf_user.strdepartment<>'';


/*Permissions for Billing*/
INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES (NULL, 'access_billing', 'Access Billing Menu');

/*Update configuration of stages*/
/*Submissions Stage Move to In-review on dispatch*/
Update sub_menus set stage_type=8, stage_property=2, stage_type_movement=862 where id =861;
/*In-review Stage Move to Supervisor when tasks are done*/
Update sub_menus set stage_type=2, stage_property=2, stage_type_movement=866 where id =862;

/*Permission for Manage Web Pages*/
insert into mf_guard_permission(name,description) values ('managewebpages', 'Manage Web Pages');

/*Add fee Category*/
CREATE TABLE `fee_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


/*Add RHA form 13 - Inspections form*/

/*Insert form settings - RHA form 13 - Inspections form*/
INSERT INTO `ap_forms` (`form_id`, `form_name`, `form_description`, `form_email`, `form_redirect`, `form_success_message`, `form_password`, `form_unique_ip`, `form_frame_height`, `form_has_css`, `form_captcha`, `form_active`, `form_review`, `form_default_application`, `esl_from_name`, `esl_from_email_address`, `esl_subject`, `esl_content`, `esl_plain_text`, `esr_email_address`, `esr_from_name`, `esr_from_email_address`, `esr_subject`, `esr_content`, `esr_plain_text`, `form_tags`, `form_redirect_enable`, `form_captcha_type`, `form_theme_id`, `form_resume_enable`, `form_limit_enable`, `form_limit`, `form_label_alignment`, `form_language`, `form_page_total`, `form_lastpage_title`, `form_submit_primary_text`, `form_submit_secondary_text`, `form_submit_primary_img`, `form_submit_secondary_img`, `form_submit_use_image`, `form_review_primary_text`, `form_review_secondary_text`, `form_review_primary_img`, `form_review_secondary_img`, `form_review_use_image`, `form_review_title`, `form_review_description`, `form_pagination_type`, `form_schedule_enable`, `form_schedule_start_date`, `form_schedule_end_date`, `form_schedule_start_hour`, `form_schedule_end_hour`, `esl_enable`, `esr_enable`, `payment_enable_merchant`, `payment_merchant_type`, `payment_paypal_email`, `payment_paypal_language`, `payment_currency`, `payment_show_total`, `payment_total_location`, `payment_enable_recurring`, `payment_recurring_cycle`, `payment_recurring_unit`, `payment_price_type`, `payment_price_amount`, `payment_price_name`, `logic_field_enable`, `logic_page_enable`, `payment_enable_trial`, `payment_trial_period`, `payment_trial_unit`, `payment_trial_amount`, `payment_paypal_enable_test_mode`, `payment_enable_invoice`, `payment_invoice_email`, `payment_delay_notifications`, `payment_ask_billing`, `payment_ask_shipping`, `form_disabled_message`, `payment_enable_tax`, `payment_tax_rate`, `logic_email_enable`, `logic_permission_enable`, `logic_workflow_enable`, `payment_enable_discount`, `payment_pesapal_live_secret_key`, `payment_pesapal_live_public_key`, `payment_pesapal_test_secret_key`, `payment_pesapal_test_public_key`, `payment_pesapal_enable_test_mode`, `payment_discount_element_id`, `payment_tax_amount`, `payment_discount_code`, `webhook_enable`, `payment_discount_type`, `payment_discount_amount`, `form_stage`, `form_group`, `form_department`, `form_department_stage`, `form_idn`, `form_type`, `form_code`, `payment_cellulant_checkout_url`, `payment_cellulant_merchant_username`, `payment_cellulant_merchant_password`, `payment_cellulant_service_id`, `payment_jambopay_business`, `payment_jambopay_shared_key`) VALUES
(7968, 'RHA FORM 13', 'SITE/ BUILDING INSPECTION FORM\nBUILDING INSPECTION FORM', NULL, '', 'Success! Your submission has been saved!', '', 0, 6217, 0, 0, 1, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 'r', 0, 0, 0, 0, 'top_label', 'english', 1, 'Untitled Page', 'Submit', 'Previous', '', '', 0, 'Submit', 'Previous', '', '', 0, 'Review Your Entry', 'Please review your entry below. Click Submit button to finish.', 'steps', 0, '0000-00-00', '0000-00-00', '00:00:00', '00:00:00', 0, 0, 0, 'paypal_standard', NULL, 'US', 'USD', 0, 'top', 0, 1, 'month', 'fixed', '0.00', NULL, 0, 0, 0, 1, 'month', '0.00', 0, 0, NULL, 1, 0, 0, NULL, 0, '0.00', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '', 0, 23, 858, '', 2, '', NULL, NULL, NULL, NULL, NULL, NULL);

/*Table structure for table `ap_form_7968` - RHA form 13 - Inspections form*/
CREATE TABLE IF NOT EXISTS `ap_form_7968` (
`id` int(11) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_updated` datetime DEFAULT NULL,
  `ip_address` varchar(15) DEFAULT NULL,
  `status` int(4) unsigned NOT NULL DEFAULT '1',
  `resume_key` varchar(10) DEFAULT NULL,
  `element_3` mediumtext COMMENT 'Paragraph Text',
  `element_4` mediumtext COMMENT 'Paragraph Text',
  `element_6` mediumtext COMMENT 'Paragraph Text',
  `element_7` mediumtext COMMENT 'Paragraph Text',
  `element_9` mediumtext COMMENT 'Paragraph Text',
  `element_10` mediumtext COMMENT 'Paragraph Text',
  `element_12` mediumtext COMMENT 'Paragraph Text',
  `element_13` mediumtext COMMENT 'Paragraph Text',
  `element_15` mediumtext COMMENT 'Paragraph Text',
  `element_16` mediumtext COMMENT 'Paragraph Text',
  `element_18` mediumtext COMMENT 'Paragraph Text',
  `element_19` mediumtext COMMENT 'Paragraph Text',
  `element_21` mediumtext COMMENT 'Paragraph Text',
  `element_22` mediumtext COMMENT 'Paragraph Text',
  `element_25` mediumtext COMMENT 'Paragraph Text',
  `element_24` mediumtext COMMENT 'Paragraph Text',
  `element_27` mediumtext COMMENT 'Paragraph Text',
  `element_28` mediumtext COMMENT 'Paragraph Text',
  `element_30` mediumtext COMMENT 'Paragraph Text',
  `element_31` mediumtext COMMENT 'Paragraph Text',
  `element_34` double DEFAULT NULL COMMENT 'Number',
  `element_35` double DEFAULT NULL COMMENT 'Number',
  `element_36` double DEFAULT NULL COMMENT 'Number',
  `element_37` double DEFAULT NULL COMMENT 'Number',
  `element_38` double DEFAULT NULL COMMENT 'Number',
  `element_44` text COMMENT 'Single Line Text',
  `element_40` text COMMENT 'Single Line Text',
  `element_41` text COMMENT 'Single Line Text',
  `element_42` text COMMENT 'Single Line Text',
  `element_43` text COMMENT 'Single Line Text',
  `element_45` int(4) unsigned NOT NULL DEFAULT '0' COMMENT 'Multiple Choice',
  `element_46` int(4) unsigned NOT NULL DEFAULT '0' COMMENT 'Multiple Choice',
  `element_47` int(4) unsigned NOT NULL DEFAULT '0' COMMENT 'Multiple Choice',
  `element_48` int(4) unsigned NOT NULL DEFAULT '0' COMMENT 'Multiple Choice',
  `element_49` int(4) unsigned NOT NULL DEFAULT '0' COMMENT 'Multiple Choice',
  `element_51_1` int(4) unsigned NOT NULL DEFAULT '0' COMMENT 'Checkbox - 1',
  `element_53_1` int(4) unsigned NOT NULL DEFAULT '0' COMMENT 'Checkbox - 1',
  `element_52` mediumtext COMMENT 'Paragraph Text',
  `element_54` mediumtext COMMENT 'Paragraph Text',
  `element_55_1` int(4) unsigned NOT NULL DEFAULT '0' COMMENT 'Checkbox - 1',
  `element_56` mediumtext COMMENT 'Paragraph Text',
  `element_57_1` int(4) unsigned NOT NULL DEFAULT '0' COMMENT 'Checkbox - 1',
  `element_58` mediumtext COMMENT 'Paragraph Text',
  `element_59` mediumtext COMMENT 'Paragraph Text'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3;

/*Insert form elements - RHA form 13 - Inspections form*/
INSERT INTO `ap_form_elements` (`form_id`, `element_id`, `element_title`, `element_guidelines`, `element_size`, `element_is_required`, `element_is_unique`, `element_is_private`, `element_type`, `element_position`, `element_default_value`, `element_constraint`, `element_total_child`, `element_css_class`, `element_range_min`, `element_range_max`, `element_range_limit_by`, `element_status`, `element_choice_columns`, `element_choice_has_other`, `element_choice_other_label`, `element_time_showsecond`, `element_time_24hour`, `element_address_hideline2`, `element_address_us_only`, `element_date_enable_range`, `element_date_range_min`, `element_date_range_max`, `element_date_enable_selection_limit`, `element_date_selection_max`, `element_date_past_future`, `element_date_disable_past_future`, `element_date_disable_weekend`, `element_date_disable_specific`, `element_date_disabled_list`, `element_file_enable_type_limit`, `element_file_block_or_allow`, `element_file_type_list`, `element_file_as_attachment`, `element_file_enable_advance`, `element_file_auto_upload`, `element_file_enable_multi_upload`, `element_file_max_selection`, `element_file_enable_size_limit`, `element_file_size_max`, `element_matrix_allow_multiselect`, `element_matrix_parent_id`, `element_submit_use_image`, `element_submit_primary_text`, `element_submit_secondary_text`, `element_submit_primary_img`, `element_submit_secondary_img`, `element_page_title`, `element_page_number`, `element_section_display_in_email`, `element_section_enable_scroll`, `element_number_enable_quantity`, `element_number_quantity_link`, `element_table_name`, `element_field_value`, `element_field_name`, `element_option_query`, `element_existing_form`, `element_existing_stage`, `element_remote_username`, `element_remote_password`, `element_remote_value`, `element_remote_server_field`, `element_jsondef`, `element_remote_post`, `element_price_class`, `element_field_error_message`, `element_mark_file_with_qr_code`, `element_file_qr_all_pages`, `element_file_qr_page_position`, `element_file_qr_users`) VALUES (7968, 1, 'INSPECTION OF THE BUILDING', 'This is the description of your section break.', 'medium', 0, 0, 0, 'section', 0, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 2, '1.1 Site Installation', 'This is the description of your section break.', 'medium', 0, 0, 0, 'section', 25, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 3, 'Physical Description (Used Materials, Techniques, Machinery.. )', '', 'medium', 0, 0, 0, 'textarea', 26, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 4, 'Observations (noncompliance/ defect)', '', 'medium', 0, 0, 0, 'textarea', 27, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 5, '1.2 Site fencing and boundary demarcation', 'This is the description of your section break.', 'medium', 0, 0, 0, 'section', 28, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 6, 'Physical Description (Used Materials, Techniques, Machinery.. )', '', 'medium', 0, 0, 0, 'textarea', 29, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 7, 'Observations (noncompliance/ defect)', '', 'medium', 0, 0, 0, 'textarea', 30, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 8, '1.3 Earth works', 'This is the description of your section break.', 'medium', 0, 0, 0, 'section', 31, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 9, 'Physical Description (Used Materials, Techniques, Machinery.. )', '', 'medium', 0, 0, 0, 'textarea', 32, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 10, 'Observations (noncompliance/ defect)', '', 'medium', 0, 0, 0, 'textarea', 33, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 11, '1.4 Excavation', 'This is the description of your section break.', 'medium', 0, 0, 0, 'section', 34, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 12, 'Physical Description (Used Materials, Techniques, Machinery.. )', '', 'medium', 0, 0, 0, 'textarea', 35, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 13, 'Observations (noncompliance/ defect)', '', 'medium', 0, 0, 0, 'textarea', 36, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 14, '1.5 Leveling', 'This is the description of your section break.', 'medium', 0, 0, 0, 'section', 37, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 15, 'Physical Description (Used Materials, Techniques, Machinery.. )', '', 'medium', 0, 0, 0, 'textarea', 38, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 16, 'Observations (noncompliance/ defect)', '', 'medium', 0, 0, 0, 'textarea', 39, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 17, '1.6 Compaction', 'This is the description of your section break.', 'medium', 0, 0, 0, 'section', 40, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 18, 'Physical Description (Used Materials, Techniques, Machinery.. )', '', 'medium', 0, 0, 0, 'textarea', 41, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 19, 'Observations (noncompliance/ defect)', '', 'medium', 0, 0, 0, 'textarea', 42, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 20, '1.7 Embankment', 'This is the description of your section break.', 'medium', 0, 0, 0, 'section', 43, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 21, 'Physical Description (Used Materials, Techniques, Machinery.. )', '', 'medium', 0, 0, 0, 'textarea', 44, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 22, 'Observations (noncompliance/ defect)', '', 'medium', 0, 0, 0, 'textarea', 45, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 23, '1.8 Trenching', 'This is the description of your section break.', 'medium', 0, 0, 0, 'section', 46, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 24, 'Observations (noncompliance/ defect)', '', 'medium', 0, 0, 0, 'textarea', 48, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 25, 'Physical Description (Used Materials, Techniques, Machinery.. )', '', 'medium', 0, 0, 0, 'textarea', 47, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 26, '1.9 Dumping Areas', 'This is the description of your section break.', 'medium', 0, 0, 0, 'section', 49, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 27, 'Physical Description (Used Materials, Techniques, Machinery.. )', '', 'medium', 0, 0, 0, 'textarea', 50, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 28, 'Observations (noncompliance/ defect)', '', 'medium', 0, 0, 0, 'textarea', 51, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 29, '1.10 Transportation', 'This is the description of your section break.', 'medium', 0, 0, 0, 'section', 52, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 30, 'Physical Description (Used Materials, Techniques, Machinery.. )', '', 'medium', 0, 0, 0, 'textarea', 53, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 31, 'Observations (noncompliance/ defect)', '', 'medium', 0, 0, 0, 'textarea', 54, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 32, 'Building Description', 'This is the description of your section break.', 'medium', 0, 0, 0, 'section', 1, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 33, 'Building Capacity', 'This is the description of your section break.', 'medium', 0, 0, 0, 'section', 2, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 34, 'Number of floors', '', 'medium', 0, 0, 0, 'number', 3, '', '', 0, '', 0, 0, 'v', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 35, 'Number of users', '', 'medium', 0, 0, 0, 'number', 4, '', '', 0, '', 0, 0, 'v', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 36, 'Parking capacity (number)', '', 'medium', 0, 0, 0, 'number', 5, '', '', 0, '', 0, 0, 'v', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 37, 'Green area (estimated %)', '', 'medium', 0, 0, 0, 'number', 6, '', '', 0, '', 0, 0, 'v', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 38, 'Built area (estimated %)', '', 'medium', 0, 0, 0, 'number', 7, '', '', 0, '', 0, 0, 'v', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 39, 'Building Status', 'This is the description of your section break.', 'medium', 0, 0, 0, 'section', 9, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 40, 'Completed building', '', 'medium', 0, 0, 0, 'text', 10, '', '', 0, '', 0, 0, 'c', 0, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 41, 'Occupied building', '', 'medium', 0, 0, 0, 'text', 11, '', '', 0, '', 0, 0, 'c', 0, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 42, 'Abandoned site', '', 'medium', 0, 0, 0, 'text', 12, '', '', 0, '', 0, 0, 'c', 0, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 43, 'Suspended site', '', 'medium', 0, 0, 0, 'text', 13, '', '', 0, '', 0, 0, 'c', 0, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 44, 'Collapsed building', '', 'medium', 0, 0, 0, 'text', 8, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 45, 'Completed building', '', 'medium', 0, 0, 0, 'radio', 10, '', '', 0, '', 0, 0, 'c', 1, 1, 0, 'Other', 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);




INSERT INTO `ap_form_elements` (`form_id`, `element_id`, `element_title`, `element_guidelines`, `element_size`, `element_is_required`, `element_is_unique`, `element_is_private`, `element_type`, `element_position`, `element_default_value`, `element_constraint`, `element_total_child`, `element_css_class`, `element_range_min`, `element_range_max`, `element_range_limit_by`, `element_status`, `element_choice_columns`, `element_choice_has_other`, `element_choice_other_label`, `element_time_showsecond`, `element_time_24hour`, `element_address_hideline2`, `element_address_us_only`, `element_date_enable_range`, `element_date_range_min`, `element_date_range_max`, `element_date_enable_selection_limit`, `element_date_selection_max`, `element_date_past_future`, `element_date_disable_past_future`, `element_date_disable_weekend`, `element_date_disable_specific`, `element_date_disabled_list`, `element_file_enable_type_limit`, `element_file_block_or_allow`, `element_file_type_list`, `element_file_as_attachment`, `element_file_enable_advance`, `element_file_auto_upload`, `element_file_enable_multi_upload`, `element_file_max_selection`, `element_file_enable_size_limit`, `element_file_size_max`, `element_matrix_allow_multiselect`, `element_matrix_parent_id`, `element_submit_use_image`, `element_submit_primary_text`, `element_submit_secondary_text`, `element_submit_primary_img`, `element_submit_secondary_img`, `element_page_title`, `element_page_number`, `element_section_display_in_email`, `element_section_enable_scroll`, `element_number_enable_quantity`, `element_number_quantity_link`, `element_table_name`, `element_field_value`, `element_field_name`, `element_option_query`, `element_existing_form`, `element_existing_stage`, `element_remote_username`, `element_remote_password`, `element_remote_value`, `element_remote_server_field`, `element_jsondef`, `element_remote_post`, `element_price_class`, `element_field_error_message`, `element_mark_file_with_qr_code`, `element_file_qr_all_pages`, `element_file_qr_page_position`, `element_file_qr_users`) VALUES
(7968, 46, 'Occupied building', '', 'medium', 0, 0, 0, 'radio', 11, '', '', 0, '', 0, 0, 'c', 1, 1, 0, 'Other', 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 47, 'Abandoned site', '', 'medium', 0, 0, 0, 'radio', 12, '', '', 0, '', 0, 0, 'c', 1, 1, 0, 'Other', 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 48, 'Suspended site', '', 'medium', 0, 0, 0, 'radio', 13, '', '', 0, '', 0, 0, 'c', 1, 1, 0, 'Other', 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 49, 'Collapsed building', '', 'medium', 0, 0, 0, 'radio', 14, '', '', 0, '', 0, 0, 'c', 1, 1, 0, 'Other', 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 50, 'BUILDING USAGE', 'This is the description of your section break.', 'medium', 0, 0, 0, 'section', 15, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 51, 'Residential', '', 'medium', 0, 0, 0, 'checkbox', 16, '', '', 0, '', 0, 0, 'c', 1, 1, 0, 'Other', 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 52, 'Specification', '', 'medium', 0, 0, 0, 'textarea', 17, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 53, 'Commercial', '', 'medium', 0, 0, 0, 'checkbox', 18, '', '', 0, '', 0, 0, 'c', 1, 1, 0, 'Other', 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 54, 'Specification', '', 'medium', 0, 0, 0, 'textarea', 19, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 55, 'Social', '', 'medium', 0, 0, 0, 'checkbox', 20, '', '', 0, '', 0, 0, 'c', 1, 1, 0, 'Other', 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 56, 'Specification', '', 'medium', 0, 0, 0, 'textarea', 21, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 57, 'Industrial', '', 'medium', 0, 0, 0, 'checkbox', 22, '', '', 0, '', 0, 0, 'c', 1, 1, 0, 'Other', 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 58, 'Specification', '', 'medium', 0, 0, 0, 'textarea', 23, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7968, 59, 'Any other (explain)', '', 'medium', 0, 0, 0, 'textarea', 24, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 1, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);



/** Update entry_decline table */
ALTER TABLE `entry_decline` ADD `declined_by` INT( 10 ) NULL AFTER `description` ;
ALTER TABLE `entry_decline` ADD `edit_fields` TEXT NULL AFTER `declined_by` ;
/** add missing Form Categories permission new code requires this */
INSERT INTO `mf_guard_permission` (`id` ,`name` ,`description`)VALUES ( NULL , 'manageformgroups', 'Manage Form Groups' );

/**Update OSC form id = 60 and set plot no as the 1st field */
UPDATE ap_form_elements SET `element_position` = 2 WHERE form_id =60 AND element_id = 1;
UPDATE ap_form_elements SET `element_position` = 3 WHERE form_id =60 AND element_id = 2;
UPDATE ap_form_elements SET `element_position` = 4 WHERE form_id =60 AND element_id = 3;
UPDATE ap_form_elements SET `element_position` = 1 WHERE form_id =60 AND element_id = 14;
/** update plot number field name value for osc form 60 */
UPDATE ap_form_elements SET `element_title` = "Plot No(This should be the Unique Parcel identifier noted on your deed plan UPI)" WHERE form_id = 60 AND element_id = 14 ;
/** update labels for section plot data in osc form 60 */
UPDATE ap_form_elements SET `element_title` = "a).Registered Usage" WHERE form_id = 60 AND element_id = 15 ;
UPDATE ap_form_elements SET `element_title` = "b).Zoning Plan" WHERE form_id = 60 AND element_id = 16 ;
UPDATE ap_form_elements SET `element_title` = "c).Plot Size (in square meters)" WHERE form_id = 60 AND element_id = 17 ;
UPDATE ap_form_elements SET `element_title` = "d).Available Right of Ownership Documents" WHERE form_id = 60 AND element_id = 90 ;


/**Update OSC form id = 23 and set plot no as the 1st field */
UPDATE ap_form_elements SET `element_position` = 2 WHERE form_id =23 AND element_id = 1;
UPDATE ap_form_elements SET `element_position` = 3 WHERE form_id =23 AND element_id = 2;
UPDATE ap_form_elements SET `element_position` = 4 WHERE form_id =23 AND element_id = 3;
UPDATE ap_form_elements SET `element_position` = 5 WHERE form_id =23 AND element_id = 4;
UPDATE ap_form_elements SET `element_position` = 6 WHERE form_id =23 AND element_id = 5;
UPDATE ap_form_elements SET `element_position` = 6 WHERE form_id =23 AND element_id = 7;
UPDATE ap_form_elements SET `element_position` = 9 WHERE form_id =23 AND element_id = 8;
UPDATE ap_form_elements SET `element_position` = 10 WHERE form_id =23 AND element_id = 9;
UPDATE ap_form_elements SET `element_position` = 12 WHERE form_id =23 AND element_id = 11;
UPDATE ap_form_elements SET `element_position` = 13 WHERE form_id =23 AND element_id = 12;
UPDATE ap_form_elements SET `element_position` = 14 WHERE form_id =23 AND element_id = 13;
UPDATE ap_form_elements SET `element_position` = 1 WHERE form_id =23 AND element_id = 14;

/**Update OSC form id = 24 and set plot no as the 1st field */
UPDATE ap_form_elements SET `element_position` = 2 WHERE form_id =24 AND element_id = 1;
UPDATE ap_form_elements SET `element_position` = 3 WHERE form_id =24 AND element_id = 2;
UPDATE ap_form_elements SET `element_position` = 4 WHERE form_id =24 AND element_id = 3;
UPDATE ap_form_elements SET `element_position` = 5 WHERE form_id =24 AND element_id = 4;
UPDATE ap_form_elements SET `element_position` = 6 WHERE form_id =24 AND element_id = 5;
UPDATE ap_form_elements SET `element_position` = 7 WHERE form_id =24 AND element_id = 6;
UPDATE ap_form_elements SET `element_position` = 8 WHERE form_id =24 AND element_id = 7;
UPDATE ap_form_elements SET `element_position` = 9 WHERE form_id =24 AND element_id = 8;
UPDATE ap_form_elements SET `element_position` = 10 WHERE form_id =24 AND element_id = 9;
UPDATE ap_form_elements SET `element_position` = 11 WHERE form_id =24 AND element_id = 10;
UPDATE ap_form_elements SET `element_position` = 12 WHERE form_id =24 AND element_id = 11;
UPDATE ap_form_elements SET `element_position` = 13 WHERE form_id =24 AND element_id = 12;
UPDATE ap_form_elements SET `element_position` = 14 WHERE form_id =24 AND element_id = 13;
UPDATE ap_form_elements SET `element_position` = 1 WHERE form_id =24 AND element_id = 14;

/**Update OSC form id = 25 and set plot no as the 1st field */
UPDATE ap_form_elements SET `element_position` = 2 WHERE form_id =25 AND element_id = 1;
UPDATE ap_form_elements SET `element_position` = 3 WHERE form_id =25 AND element_id = 2;
UPDATE ap_form_elements SET `element_position` = 4 WHERE form_id =25 AND element_id = 3;
UPDATE ap_form_elements SET `element_position` = 1 WHERE form_id =25 AND element_id = 4;

/**Update OSC form id = 22 and set plot no as the 1st field */
UPDATE ap_form_elements SET `element_position` = 2 WHERE form_id =22 AND element_id = 2;
UPDATE ap_form_elements SET `element_position` = 3 WHERE form_id =22 AND element_id = 3;
UPDATE ap_form_elements SET `element_position` = 6 WHERE form_id =22 AND element_id = 5;
UPDATE ap_form_elements SET `element_position` = 1 WHERE form_id =22 AND element_id = 4;
UPDATE ap_form_elements SET `element_position` = 7 WHERE form_id =22 AND element_id = 6;
UPDATE ap_form_elements SET `element_position` = 8 WHERE form_id =22 AND element_id = 7;
UPDATE ap_form_elements SET `element_position` = 9 WHERE form_id =22 AND element_id = 8;
UPDATE ap_form_elements SET `element_position` = 11 WHERE form_id =22 AND element_id = 10;
UPDATE ap_form_elements SET `element_position` = 12 WHERE form_id =22 AND element_id = 11;
UPDATE ap_form_elements SET `element_position` = 13 WHERE form_id =22 AND element_id = 12;
UPDATE ap_form_elements SET `element_position` = 14 WHERE form_id =22 AND element_id = 13;
UPDATE ap_form_elements SET `element_position` = 1 WHERE form_id =22 AND element_id = 14;


/*AP Settings Profile dir field*/

ALTER TABLE `ap_settings` ADD `profile_dir` TEXT NULL;

--
-- OTB: Table structure for table `application_number_history`
--
CREATE TABLE `application_number_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_entry_id` int(11) DEFAULT NULL,
  `application_number` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;




/*Start Translations*/

/*Query to insert default english translation form element fields missing translations*/
INSERT INTO `ext_translations`(locale, table_class, field_name, field_id, trl_content, option_id)
select 'en_US', 'ap_form_elements', 'element_title', ap_form_elements.form_id, ap_form_elements.element_title, ap_form_elements.element_id from ap_form_elements where Concat(ap_form_elements.form_id, "###", ap_form_elements.element_id) not in (select Concat(ext_translations.field_id, "###", ext_translations.option_id) from ext_translations where table_class='ap_form_elements');
/*End Translations*/
ALTER TABLE `ap_form_elements` ADD `element_buildingcoverage` INT NULL DEFAULT '0' AFTER `element_title`;
ALTER TABLE `ap_form_elements` ADD `element_plotsize` INT NULL DEFAULT '0' AFTER `element_title`;
ALTER TABLE `ap_form_elements` ADD `element_builtup` INT NULL DEFAULT '0' AFTER `element_title`;
ALTER TABLE `ap_form_elements` ADD `element_no_of_floors` INT NULL DEFAULT '0' AFTER `element_title`;
ALTER TABLE `ap_form_elements` ADD `element_grossfloorarea` INT NULL DEFAULT '0' AFTER `element_title`;
ALTER TABLE `ap_form_elements` ADD `element_floor_ratio` INT NULL DEFAULT '0' AFTER `element_title`;
/** content type **/
ALTER TABLE `content` ADD `content_type` VARCHAR(256) NULL AFTER `deleted`;

CREATE TABLE `booklet` (`id` int(11) NOT NULL, `title` varchar(256) NOT NULL,`content` text NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `booklet` ADD PRIMARY KEY (`id`);
ALTER TABLE `booklet` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/* Create Leaves Table */
CREATE TABLE `leave_request` (
  `id` int(10) NOT NULL,
  `reviewer_id` int(10) DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `status` varchar(256) NOT NULL,
  `name` varchar(256) NOT NULL,
  `approved_by` int(10) DEFAULT NULL,
  `approved_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `leave_type` varchar(256) DEFAULT NULL
) ;
ALTER TABLE `leave_request`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `leave_request`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
/* Permission to Approve Leave Requests */
INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) 
VALUES (NULL, 'can_approve_leave_requests', 'Permission to Approve Leave Requests.');
/* Add column for communication type */
alter table communications add column communication_type varchar(256) null ;
alter table communications add column msg_to_reviewer_id int(10) null ;
/** add district column */
ALTER TABLE ap_forms ADD COLUMN district VARCHAR(256) NULL ;
/* Order number for forms */
ALTER TABLE ap_forms ADD COLUMN order_no INT(10) NULL ;
