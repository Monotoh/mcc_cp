/*	
COK 
CoK OSC Construction Permit - Already Done
*/
/*
COK OC Extension Permit
*/
INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200049, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200049, '', 'Plot Number', '{upi_plot_fm_element_205}', 4),
(0, 200049, '', 'Developer', '{fm_element_5_1} {fm_element_5_2}', 5),
(0, 200049, '', 'District', '{fm_element_81}', 9),
(0, 200049, '', 'Sector', '{fm_element_82}', 10),
(0, 200049, '', 'Cell', '{fm_element_187}', 11),
(0, 200049, '', 'Project', '{fm_element_2}', 7),
(0, 200049, '', 'Building Type', '{fm_element_199}', 8),
(0, 200049, '', 'Application Number', '{ap_application_id}', 2),
(0, 200049, '', 'UPI', '{fm_element_205}', 3);

/*
CoK OSC Refurbishment With Structural Alterations Permit
*/
INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200050, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200050, '', 'Application Number', '{ap_application_id}', 2),
(0, 200050, '', 'Plot Number', '{upi_plot_fm_element_223}', 4),
(0, 200050, '', 'Developer', '{fm_element_5_1} {fm_element_5_2}', 5),
(0, 200050, '', 'District', '{fm_element_81}', 9),
(0, 200050, '', 'Sector', '{fm_element_82}', 10),
(0, 200050, '', 'Cell', '{fm_element_187}', 11),
(0, 200050, '', 'Project', '{fm_element_2}', 7),
(0, 200050, '', 'Building Type', '{fm_element_208}', 8),
(0, 200050, '', 'UPI', '{fm_element_223}', 3);

/*
COK OSC Demolition Permit
*/
INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200051, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200051, '', 'Application Number', '{ap_application_id}', 2),
(0, 200051, '', 'Plot Number', '{upi_plot_fm_element_220}', 4),
(0, 200051, '', 'Developer', '{fm_element_5_1} {fm_element_5_2}', 5),
(0, 200051, '', 'District', '{fm_element_81}', 9),
(0, 200051, '', 'Sector', '{fm_element_82}', 10),
(0, 200051, '', 'Cell', '{fm_element_187}', 11),
(0, 200051, '', 'Project', '{fm_element_2}', 7),
(0, 200051, '', 'Building Type', '{fm_element_216}', 8),
(0, 200051, '', 'UPI', '{fm_element_220}', 3);

/*
COK OSC Occupancy Permit
*/
INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200052, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200052, '', 'Application Number', '{ap_application_id}', 2),
(0, 200052, '', 'Plot Number', '{upi_plot_fm_element_4}', 4),
(0, 200052, '', 'Developer', '{fm_element_2}', 5),
(0, 200052, '', 'District', '{fm_element_86}', 9),
(0, 200052, '', 'Sector', '{fm_element_77}', 10),
(0, 200052, '', 'Cell', '{fm_element_78}', 11),
(0, 200052, '', 'Project', '{fm_element_2}', 7),

(0, 200052, '', 'UPI', '{{fm_element_4}}', 3);

/*
COK OSC Refurbishment Without Structural Alterations Permit
*/
INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200053, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200053, '', 'Application Number', '{ap_application_id}', 2),
(0, 200053, '', 'Plot Number', '{upi_plot_fm_element_218}', 4),
(0, 200053, '', 'Developer', '{fm_element_5_1} {fm_element_5_2}', 5),
(0, 200053, '', 'District', '{fm_element_81}', 9),
(0, 200053, '', 'Sector', '{fm_element_82}', 10),
(0, 200053, '', 'Cell', '{fm_element_187}', 11),
(0, 200053, '', 'Project', '{fm_element_2}', 7),
(0, 200053, '', 'Building Type', '{fm_element_209}', 8),
(0, 200053, '', 'UPI', '{fm_element_218}', 3);

/*
	COK OSC Change of Use Permit
*/
INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200054, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200054, '', 'Application Number', '{ap_application_id}', 2),
(0, 200054, '', 'Plot Number', '{upi_plot_fm_element_14}', 4),
(0, 200054, '', 'Developer', '{fm_element_5_1} {fm_element_5_2}', 5),
(0, 200054, '', 'District', '{fm_element_9}', 9),
(0, 200054, '', 'Sector', '{fm_element_10}', 10),
(0, 200054, '', 'Cell', '{fm_element_11}', 11),
(0, 200054, '', 'Project', '{fm_element_2}', 7),
(0, 200054, '', 'Building Type', '{fm_element_24}', 8),
(0, 200054, '', 'UPI', '{fm_element_14}', 3);



/*
	CoK Temporary Authorization Permit
*/
INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200055, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200055, '', 'Application Number', '{ap_application_id}', 2),
(0, 200055, '', 'Plot Number', '{upi_plot_fm_element_203}', 4),
(0, 200055, '', 'Developer', '{fm_element_5_1} {fm_element_5_2}', 5),
(0, 200055, '', 'District', '{fm_element_81}', 9),
(0, 200055, '', 'Sector', '{fm_element_82}', 10),
(0, 200055, '', 'Cell', '{fm_element_187}', 11),
(0, 200055, '', 'Project', '{fm_element_2}', 7),
(0, 200055, '', 'Building Type', '{fm_element_198}', 8),
(0, 200055, '', 'UPI', '{fm_element_203}', 3);


/*
		CoK Construction of Fence Fee
*/
INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200083, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200083, '', 'Application Number', '{ap_application_id}', 2),
(0, 200083, '', 'Plot Number', '{upi_plot_fm_element_218}', 4),
(0, 200083, '', 'Developer', '{fm_element_5_1} {fm_element_5_2}', 5),
(0, 200083, '', 'District', '{fm_element_81}', 9),
(0, 200083, '', 'Sector', '{fm_element_82}', 10),
(0, 200083, '', 'Cell', '{fm_element_187}', 11),
(0, 200083, '', 'Project', '{fm_element_2}', 7),
(0, 200083, '', 'UPI', '{fm_element_218}', 3);

/*
	CoK Modifications of Project Permit
*/
INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200089, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200089, '', 'Application Number', '{ap_application_id}', 2),
(0, 200089, '', 'Plot Number', '{upi_plot_fm_element_223}', 4),
(0, 200089, '', 'Developer', '{fm_element_5_1} {fm_element_5_2}', 5),
(0, 200089, '', 'District', '{fm_element_81}', 9),
(0, 200089, '', 'Sector', '{fm_element_82}', 10),
(0, 200089, '', 'Cell', '{fm_element_187}', 11),
(0, 200089, '', 'Project', '{fm_element_2}', 7),
(0, 200089, '', 'UPI', '{fm_element_223}', 3);

/*
	CoK Renewal of Permit
*/
INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200102, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200102, '', 'Application Number', '{ap_application_id}', 2),
(0, 200102, '', 'Plot Number', '{upi_plot_fm_element_14}', 4),
(0, 200102, '', 'Developer', '{fm_element_5_1} {fm_element_5_2}', 5),
(0, 200102, '', 'District', '{fm_element_81}', 9),
(0, 200102, '', 'Sector', '{fm_element_82}', 10),
(0, 200102, '', 'Cell', '{fm_element_187}', 11),
(0, 200102, '', 'Project', '{fm_element_2}', 7),
(0, 200102, '', 'Building Type', '{fm_element_193}', 8),
(0, 200102, '', 'UPI', '{fm_element_14}', 3);


/*

GASABO District
Gasabo OSC Construction Permit
*/

INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200056, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200056, '', 'Application Number', '{ap_application_id}', 2),
(0, 200056, '', 'Plot Number', '{upi_plot_fm_element_14}', 4),
(0, 200056, '', 'Developer', '{fm_element_5_1} {fm_element_5_2}', 5),
(0, 200056, '', 'District', '{fm_element_53}', 9),
(0, 200056, '', 'Sector', '{fm_element_10}', 10),
(0, 200056, '', 'Cell', '{fm_element_11}', 11),
(0, 200056, '', 'Project', '{fm_element_103}', 7),
(0, 200056, '', 'Building Type', '{fm_element_98}', 8),
(0, 200056, '', 'UPI', '{fm_element_14}', 3);

/*
	Gasabo OSC Extension Permit
*/

INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200057, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200057, '', 'Application Number', '{ap_application_id}', 2),
(0, 200057, '', 'Plot Number', '{upi_plot_fm_element_205}', 4),
(0, 200057, '', 'Developer', '{fm_element_5_1} {fm_element_5_2}', 5),
(0, 200057, '', 'District', '{fm_element_81}', 9),
(0, 200057, '', 'Sector', '{fm_element_82}', 10),
(0, 200057, '', 'Cell', '{fm_element_187}', 11),
(0, 200057, '', 'Project', '{fm_element_2}', 7),
(0, 200057, '', 'Building Type', '{fm_element_199}', 8),
(0, 200057, '', 'UPI', '{fm_element_205}', 3);

/*
		Gasabo OSC Refurbishment With Structural Alterations Permit
*/

INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200058, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200058, '', 'Application Number', '{ap_application_id}', 2),
(0, 200058, '', 'Plot Number', '{upi_plot_fm_element_223}', 4),
(0, 200058, '', 'Developer', '{fm_element_5_1} {fm_element_5_2}', 5),
(0, 200058, '', 'District', '{fm_element_81}', 9),
(0, 200058, '', 'Sector', '{fm_element_82}', 10),
(0, 200058, '', 'Cell', '{fm_element_187}', 11),
(0, 200058, '', 'Project', '{fm_element_2}', 7),
(0, 200058, '', 'Building Type', '{fm_element_208}', 8),
(0, 200058, '', 'UPI', '{fm_element_223}', 3);

/*
			Gasabo OSC Demolition Permit
*/

INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200059, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200059, '', 'Application Number', '{ap_application_id}', 2),
(0, 200059, '', 'Plot Number', '{upi_plot_fm_element_220}', 4),
(0, 200059, '', 'Developer', '{fm_element_5_1} {fm_element_5_2}', 5),
(0, 200059, '', 'District', '{fm_element_81}', 9),
(0, 200059, '', 'Sector', '{fm_element_82}', 10),
(0, 200059, '', 'Cell', '{fm_element_187}', 11),
(0, 200059, '', 'Project', '{fm_element_2}', 7),
(0, 200059, '', 'Building Type', '{fm_element_216}', 8),
(0, 200059, '', 'UPI', '{fm_element_220}', 3);

/*
			Gasabo OSC Occupancy Permit
*/

INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200060, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200060, '', 'Application Number', '{ap_application_id}', 2),
(0, 200060, '', 'Plot Number', '{upi_plot_fm_element_9}', 4),
(0, 200060, '', 'Developer', 'fm_element_6', 5),

(0, 200060, '', 'Project', '{fm_element_6}', 7),

(0, 200060, '', 'UPI', '{fm_element_9}', 3);


/*
				Gasabo OSC Refurbishment Without Structural Alterations Permit
*/

INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200061, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200061, '', 'Application Number', '{ap_application_id}', 2),
(0, 200061, '', 'Plot Number', '{upi_plot_fm_element_218}', 4),
(0, 200061, '', 'Developer', '{fm_element_5_1} {fm_element_5_2}', 5),
(0, 200061, '', 'District', '{fm_element_81}', 9),
(0, 200061, '', 'Sector', '{fm_element_82}', 10),
(0, 200061, '', 'Cell', '{fm_element_187}', 11),
(0, 200061, '', 'Project', '{fm_element_2}', 7),
(0, 200061, '', 'Building Type', '{fm_element_209}', 8),
(0, 200061, '', 'UPI', '{fm_element_218}', 3);

/*
Gasabo OSC Change of Use Permit				
*/

INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200062, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200062, '', 'Application Number', '{ap_application_id}', 2),
(0, 200062, '', 'Plot Number', '{upi_plot_fm_element_11}', 4),
(0, 200062, '', 'Developer', 'fm_element_8', 5),

(0, 200062, '', 'Project', '{fm_element_8}', 7),
(0, 200062, '', 'Building Type', '{fm_element_12}', 8),
(0, 200062, '', 'UPI', '{fm_element_11}', 3);

/*
Gasabo Temporary Authorization Permit
*/

INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200063, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200063, '', 'Application Number', '{ap_application_id}', 2),
(0, 200063, '', 'Plot Number', '{upi_plot_fm_element_203}', 4),
(0, 200063, '', 'Developer', '{fm_element_5_1} {fm_element_5_2}', 5),
(0, 200063, '', 'District', '{fm_element_81}', 9),
(0, 200063, '', 'Sector', '{fm_element_82}', 10),
(0, 200063, '', 'Cell', '{fm_element_187}', 11),
(0, 200063, '', 'Project', '{fm_element_2}', 7),
(0, 200063, '', 'Building Type', '{fm_element_198}', 8),
(0, 200063, '', 'UPI', '{fm_element_203}', 3);


/*
Gasabo Construction of Fence Permit
*/

INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200084, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200084, '', 'Application Number', '{ap_application_id}', 2),
(0, 200084, '', 'Plot Number', '{upi_plot_fm_element_9}', 4),
(0, 200084, '', 'Developer', 'fm_element_6', 5),

(0, 200084, '', 'Project', '{fm_element_6}', 7),

(0, 200084, '', 'UPI', '{fm_element_9}', 3);

/*
Gasabo Modifications of Projects Permit
*/

INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200090, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200090, '', 'Application Number', '{ap_application_id}', 2),
(0, 200090, '', 'Plot Number', '{upi_plot_fm_element_218}', 4),
(0, 200090, '', 'Developer', '{fm_element_5_1} {fm_element_5_2}', 5),
(0, 200090, '', 'District', '{fm_element_81}', 9),
(0, 200090, '', 'Sector', '{fm_element_82}', 10),
(0, 200090, '', 'Cell', '{fm_element_187}', 11),
(0, 200090, '', 'Project', '{fm_element_2}', 7),
(0, 200090, '', 'Building Type', '{fm_child_element_194}', 8),
(0, 200090, '', 'UPI', '{fm_element_218}', 3);



/*
Gasabo Renewal of Permit
*/

INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200103, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200103, '', 'Application Number', '{ap_application_id}', 2),
(0, 200103, '', 'Plot Number', '{upi_plot_fm_element_223}', 4),
(0, 200103, '', 'Developer', '{fm_element_5_1} {fm_element_5_2}', 5),
(0, 200103, '', 'District', '{fm_element_81}', 9),
(0, 200103, '', 'Sector', '{fm_element_82}', 10),
(0, 200103, '', 'Cell', '{fm_element_187}', 11),
(0, 200103, '', 'Project', '{fm_element_2}', 7),
(0, 200103, '', 'Building Type', '{fm_element_208}', 8),
(0, 200103, '', 'UPI', '{fm_element_223}', 3);


/*
KICUKIRO District
Kicukiro OSC Construction Permit - Already Done
*/


/*
	Kicukiro Extension Permit
*/

INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200065, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200065, '', 'Application Number', '{ap_application_id}', 2),
(0, 200065, '', 'Plot Number', '{upi_plot_fm_element_205}', 4),
(0, 200065, '', 'Developer', '{fm_element_5_1} {fm_element_5_2}', 5),
(0, 200065, '', 'District', '{fm_element_81}', 9),
(0, 200065, '', 'Sector', '{fm_element_82}', 10),
(0, 200065, '', 'Cell', '{fm_element_187}', 11),
(0, 200065, '', 'Project', '{fm_element_2}', 7),
(0, 200065, '', 'Building Type', '{fm_element_199}', 8),
(0, 200065, '', 'UPI', '{fm_element_205}', 3);

/*
	Kicukiro OSC Refurbishment With Structural Alterations Permit
*/

INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200066, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200066, '', 'Application Number', '{ap_application_id}', 2),
(0, 200066, '', 'Plot Number', '{upi_plot_fm_element_223}', 4),
(0, 200066, '', 'Developer', '{fm_element_5_1} {fm_element_5_2}', 5),
(0, 200066, '', 'District', '{fm_element_81}', 9),
(0, 200066, '', 'Sector', '{fm_element_82}', 10),
(0, 200066, '', 'Cell', '{fm_element_187}', 11),
(0, 200066, '', 'Project', '{fm_element_2}', 7),
(0, 200066, '', 'Building Type', '{fm_element_208}', 8),
(0, 200066, '', 'UPI', '{fm_element_223}', 3);

/*
		Kicukiro OSC Demolition Permit
*/

INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200067, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200067, '', 'Application Number', '{ap_application_id}', 2),
(0, 200067, '', 'Plot Number', '{upi_plot_fm_element_220}', 4),
(0, 200067, '', 'Developer', '{fm_element_5_1} {fm_element_5_2}', 5),
(0, 200067, '', 'District', '{fm_element_81}', 9),
(0, 200067, '', 'Sector', '{fm_element_82}', 10),
(0, 200067, '', 'Cell', '{fm_element_187}', 11),
(0, 200067, '', 'Project', '{fm_element_2}', 7),
(0, 200067, '', 'Building Type', '{fm_element_216}', 8),
(0, 200067, '', 'UPI', '{fm_element_220}', 3);

/*
		Kicuiro OSC Occupancy Permit
*/

INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200068, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200068, '', 'Application Number', '{ap_application_id}', 2),
(0, 200068, '', 'Plot Number', '{upi_plot_fm_element_9}', 4),
(0, 200068, '', 'Developer', 'fm_element_6', 5),

(0, 200068, '', 'Project', '{fm_element_6}', 7),

(0, 200068, '', 'UPI', '{fm_element_9}', 3);

/*
			Kicukiro OSC Refurbishment Without Structural Alterations Permit
*/

INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200069, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200069, '', 'Application Number', '{ap_application_id}', 2),
(0, 200069, '', 'Plot Number', '{upi_plot_fm_element_218}', 4),
(0, 200069, '', 'Developer', '{fm_element_5_1} {fm_element_5_2}', 5),
(0, 200069, '', 'District', '{fm_element_81}', 9),
(0, 200069, '', 'Sector', '{fm_element_82}', 10),
(0, 200069, '', 'Cell', '{fm_element_187}', 11),
(0, 200069, '', 'Project', '{fm_element_2}', 7),
(0, 200069, '', 'Building Type', '{fm_element_209}', 8),
(0, 200069, '', 'UPI', '{fm_element_218}', 3);

/*
			Kicukiro OSC Change of Use Permit
*/

INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200070, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200070, '', 'Application Number', '{ap_application_id}', 2),
(0, 200070, '', 'Plot Number', '{upi_plot_fm_element_11}', 4),
(0, 200070, '', 'Developer', '{fm_element_8}', 5),


(0, 200070, '', 'UPI', '{fm_element_11}', 3);

/*
			Kicukiro OSC Refurbishment Without Structural Alterations Permit
*/

INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200071, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200071, '', 'Application Number', '{ap_application_id}', 2),
(0, 200071, '', 'Plot Number', '{upi_plot_fm_element_203}', 4),
(0, 200071, '', 'Developer', '{fm_element_5_1} {fm_element_5_2}', 5),
(0, 200071, '', 'District', '{fm_element_81}', 9),
(0, 200071, '', 'Sector', '{fm_element_82}', 10),
(0, 200071, '', 'Cell', '{fm_element_187}', 11),
(0, 200071, '', 'Project', '{fm_element_2}', 7),
(0, 200071, '', 'Building Type', '{fm_element_198}', 8),
(0, 200071, '', 'UPI', '{fm_element_203}', 3);

/*
			Kicukiro Construction Fence Permit
*/

INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200087, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200087, '', 'Application Number', '{ap_application_id}', 2),
(0, 200087, '', 'Plot Number', '{upi_plot_fm_element_9}', 4),
(0, 200087, '', 'Developer', '{fm_element_6}', 5),

(0, 200087, '', 'Project', '{fm_element_6}', 7),

(0, 200087, '', 'UPI', '{fm_element_9}', 3);

/*
			Kicukiro Modifications Permit
*/

INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200092, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200092, '', 'Application Number', '{ap_application_id}', 2),
(0, 200092, '', 'Plot Number', '{upi_plot_fm_element_223}', 4),
(0, 200092, '', 'Developer', '{fm_element_5_1} {fm_element_5_2}', 5),
(0, 200092, '', 'District', '{fm_element_81}', 9),
(0, 200092, '', 'Sector', '{fm_element_82}', 10),
(0, 200092, '', 'Cell', '{fm_element_187}', 11),
(0, 200092, '', 'Project', '{fm_element_2}', 7),

(0, 200092, '', 'UPI', '{fm_element_223}', 3);

/*
			Kicukiro Renewal of Permit
*/

INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200106, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200106, '', 'Application Number', '{ap_application_id}', 2),
(0, 200106, '', 'Plot Number', '{upi_plot_fm_element_14}', 4),
(0, 200106, '', 'Developer', '{fm_element_5_1} {fm_element_5_2}', 5),
(0, 200106, '', 'District', '{fm_element_53}', 9),
(0, 200106, '', 'Sector', '{fm_element_10}', 10),
(0, 200106, '', 'Cell', '{fm_element_11}', 11),
(0, 200106, '', 'Project', '{fm_element_106}', 7),
(0, 200106, '', 'Building Type', '{fm_element_96}', 8),
(0, 200106, '', 'UPI', '{fm_element_14}', 3);


/** Nyarugenge Construction Permit **/

INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200072, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200072, '', 'Application Number', '{ap_application_id}', 2),
(0, 200072, '', 'Plot Number', '{upi_plot_fm_element_14}', 4),
(0, 200072, '', 'Developer', '{fm_element_5} {fm_element_5}', 5),
(0, 200072, '', 'District', '{fm_element_53}', 9),
(0, 200072, '', 'Sector', '{fm_element_10}', 10),
(0, 200072, '', 'Cell', '{fm_element_11}', 11),
(0, 200072, '', 'Project', '{fm_element_102}', 7),
(0, 200072, '', 'Building Type', '{fm_element_96}', 8),
(0, 200072, '', 'UPI', '{fm_element_14}', 3);

/** Nyarugenge OSC Extension Permit */
INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200073, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200073, '', 'Application Number', '{ap_application_id}', 2),
(0, 200073, '', 'Plot Number', '{upi_plot_fm_element_205}', 4),
(0, 200073, '', 'Developer', '{fm_element_5} {fm_element_5}', 5),
(0, 200073, '', 'District', '{fm_element_81}', 9),
(0, 200073, '', 'Sector', '{fm_element_82}', 10),
(0, 200073, '', 'Cell', '{fm_element_187}', 11),
(0, 200073, '', 'Project', '{fm_element_2}', 7),
(0, 200073, '', 'Building Type', '{fm_element_199}', 8),
(0, 200073, '', 'UPI', '{fm_element_205}', 3);

/** Nyarugenge Refurbishment With Structural Alterations Permit */
INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200074, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200074, '', 'Application Number', '{ap_application_id}', 2),
(0, 200074, '', 'Plot Number', '{upi_plot_fm_element_223}', 4),
(0, 200074, '', 'Developer', '{fm_element_5} {fm_element_5}', 5),
(0, 200074, '', 'District', '{fm_element_81}', 9),
(0, 200074, '', 'Sector', '{fm_element_82}', 10),
(0, 200074, '', 'Cell', '{fm_element_187}', 11),
(0, 200074, '', 'Project', '{fm_element_2}', 7),
(0, 200074, '', 'Building Type', '{fm_element_208}', 8),
(0, 200074, '', 'UPI', '{fm_element_223}', 3);

/** Nyarugenge OSC Demolition Permit */
INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200074, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200074, '', 'Application Number', '{ap_application_id}', 2),
(0, 200074, '', 'Plot Number', '{upi_plot_fm_element_220}', 4),
(0, 200074, '', 'Developer', '{fm_element_5} {fm_element_5}', 5),
(0, 200074, '', 'District', '{fm_element_81}', 9),
(0, 200074, '', 'Sector', '{fm_element_82}', 10),
(0, 200074, '', 'Cell', '{fm_element_187}', 11),
(0, 200074, '', 'Project', '{fm_element_2}', 7),
(0, 200074, '', 'Building Type', '{fm_element_216}', 8),
(0, 200074, '', 'UPI', '{fm_element_220}', 3);

/** Nyarugenge OSC Occupancy Permit */
INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200076, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200076, '', 'Application Number', '{ap_application_id}', 2),
(0, 200076, '', 'Owner', '{fm_element_6}', 2),
(0, 200076, '', 'Phone', '{fm_element_7}', 4),
(0, 200076, '', 'UPI', '{fm_element_9}', 3);

/** Nyarugenge OSC Refurbishment Without Structural Alterations Permit */
INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200074, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200074, '', 'Application Number', '{ap_application_id}', 2),
(0, 200074, '', 'Plot Number', '{upi_plot_fm_element_218}', 4),
(0, 200074, '', 'Developer', '{fm_element_5} {fm_element_5}', 5),
(0, 200074, '', 'District', '{fm_element_81}', 9),
(0, 200074, '', 'Sector', '{fm_element_82}', 10),
(0, 200074, '', 'Cell', '{fm_element_187}', 11),
(0, 200074, '', 'Project', '{fm_element_2}', 7),
(0, 200074, '', 'Building Type', '{fm_element_209}', 8),
(0, 200074, '', 'UPI', '{fm_element_218}', 3);

/** Nyarugenge OSC Change of Use Permit */
INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200078, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200078, '', 'Application Number', '{ap_application_id}', 2),
(0, 200078, '', 'Developer', '{fm_element_8}', 5),
(0, 200078, '', 'Phone', '{fm_element_9}', 9),
(0, 200078, '', 'UPI', '{fm_element_11}', 3);

/** Nyarugenge Temporary Authorization Permit */
INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200079, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200079, '', 'Application Number', '{ap_application_id}', 2),
(0, 200079, '', 'Plot Number', '{upi_plot_fm_element_203}', 4),
(0, 200079, '', 'Developer', '{fm_element_5} {fm_element_5}', 5),
(0, 200079, '', 'District', '{fm_element_81}', 9),
(0, 200079, '', 'Sector', '{fm_element_82}', 10),
(0, 200079, '', 'Cell', '{fm_element_187}', 11),
(0, 200079, '', 'Project', '{fm_element_214}', 7),
(0, 200079, '', 'Building Type', '{fm_element_198}', 8),
(0, 200079, '', 'UPI', '{fm_element_203}', 3);

/** Nyarungege Constructionof fence Permit Fee */
INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200086, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200086, '', 'Application Number', '{ap_application_id}', 2),
(0, 200086, '', 'Developer', '{fm_element_6}', 5),
(0, 200086, '', 'District', '{fm_element_13}', 9),
(0, 200086, '', 'Sector', '{fm_element_14}', 10),
(0, 200086, '', 'Cell', '{fm_element_15}', 11),
(0, 200086, '', 'Project', '{fm_element_17}', 7),
(0, 200086, '', 'UPI', '{fm_element_9}', 3);

/** Nyarungege Modification of Plans Permit */
INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200091, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200091, '', 'Application Number', '{ap_application_id}', 2),
(0, 200091, '', 'Developer', '{fm_element_5}', 5),
(0, 200091, '', 'District', '{fm_element_81}', 9),
(0, 200091, '', 'Sector', '{fm_element_82}', 10),
(0, 200091, '', 'Cell', '{fm_element_187}', 11),
(0, 200091, '', 'Project', '{fm_element_2}', 7),
(0, 200091, '', 'UPI', '{fm_element_223}', 3);

/** Nyarugenge Renewal of Permit */
INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200105, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200105, '', 'Application Number', '{ap_application_id}', 2),
(0, 200105, '', 'Developer', '{fm_element_5}', 5),
(0, 200105, '', 'District', '{fm_element_53}', 9),
(0, 200105, '', 'Sector', '{fm_element_10}', 10),
(0, 200105, '', 'Cell', '{fm_element_11}', 11),
(0, 200105, '', 'UPI', '{fm_element_14}', 3);

/** NYARUGENGE - ONE STOP CENTER APPLICATION FORM FOR RENOVATION/TRANSFORMATION/ALTERATION PERMIT */
INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200108, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200108, '', 'Application Number', '{ap_application_id}', 2),
(0, 200108, '', 'Developer', '{fm_element_6}', 5),
(0, 200108, '', 'District', '{fm_element_16}', 9),
(0, 200108, '', 'Sector', '{fm_element_19}', 10),
(0, 200108, '', 'Cell', '{fm_element_18}', 11),
(0, 200108, '', 'Project', '{fm_element_20}', 7),
(0, 200108, '', 'UPI', '{fm_element_9}', 3);

/** Renovation / Alteration Nyarugenge Permit */
INSERT INTO `permit_checker_config` (`id`, `permit_template_id`, `reference_object`, `label_to_show`, `value_to_show`, `sequence_no`) VALUES
(0, 200109, '', 'Applicant/Architect/Engineer', '{sf_fullname}', 6),
(0, 200109, '', 'Application Number', '{ap_application_id}', 2),
(0, 200109, '', 'Developer', '{fm_element_8}', 5),
(0, 200109, '', 'District', '{fm_element_21}', 9),
(0, 200109, '', 'Sector', '{fm_element_18}', 10),
(0, 200109, '', 'Cell', '{fm_element_19}', 11),
(0, 200109, '', 'Project', '{fm_element_14}', 7),
(0, 200109, '', 'UPI', '{fm_element_11}', 3);


/*
Not done
-huye
-rusizi
-rubavu

*/