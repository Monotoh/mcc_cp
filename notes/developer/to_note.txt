- Form builder undergoes issues with non-ascii characters as field titles (element_title), especially those copied from word with format data encode inside. Also apostrophes cause issues. If such element_title exists, it will cause all field properties in that form to be inaccessible, because of inability to create json using json_encode
- Install phplive chat on with on the current cp database to avoid errors.

