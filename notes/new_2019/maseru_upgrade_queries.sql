/**
 * Add mising ap_form_elements columns. function mf_get_attachments_details() requires these columns to execute without errors
 * These are columns for QR code
*/
ALTER TABLE `ap_form_elements` ADD `element_mark_file_with_qr_code` INT( 10 ) NULL AFTER `element_status` ;
ALTER TABLE `ap_form_elements` ADD `element_file_qr_all_pages` INT( 10 ) NULL AFTER `element_mark_file_with_qr_code` ;
ALTER TABLE `ap_form_elements` ADD `element_file_qr_page_position` VARCHAR( 256 ) NULL AFTER `element_file_qr_all_pages` ; 
ALTER TABLE `ap_form_elements` ADD `element_file_qr_users` VARCHAR( 256 ) NULL AFTER `element_file_qr_page_position` ; 

/** Update missing columns in entry_decline table */
ALTER TABLE `entry_decline` ADD `declined_by` INT( 10 ) NULL AFTER `description` ;
ALTER TABLE `entry_decline` ADD `edit_fields` TEXT NULL AFTER `declined_by` ;


/* fee category table */
CREATE TABLE IF NOT EXISTS `fee_category` (`Id` int( 11 ) NOT NULL AUTO_INCREMENT ,`title` varchar( 256 ) DEFAULT NULL ,
PRIMARY KEY ( `Id` )) ENGINE = InnoDB DEFAULT CHARSET = latin1 AUTO_INCREMENT =1 ;

/*Fee structure queries*/
ALTER TABLE `fee` ADD `base_field` int(11) DEFAULT NULL;
ALTER TABLE `fee` ADD `fee_type` varchar(64) DEFAULT NULL;
ALTER TABLE `fee` ADD `percentage` varchar(100) DEFAULT NULL;
ALTER TABLE `fee` ADD `minimum_fee` bigint(20) DEFAULT NULL;

CREATE TABLE IF NOT EXISTS `fee_range` (
`id` int(11) NOT NULL,
  `fee_id` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `range_1` int(11) NOT NULL,
  `range_2` int(11) NOT NULL,
  `result_value` int(11) NOT NULL,
  `condition_field` int(11) DEFAULT NULL,
  `condition_operator` varchar(100) DEFAULT NULL,
  `condition_value` text DEFAULT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;
ALTER TABLE `fee_range`
 ADD PRIMARY KEY (`id`);
 ALTER TABLE `fee_range`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

/** For Agenda table - Create this table**/
CREATE TABLE IF NOT EXISTS `agenda` (`id` bigint(20) NOT NULL AUTO_INCREMENT,`agenda_date` varchar(250) NOT NULL,
  `locked` tinyint(4) NOT NULL,`closed` tinyint(4) NOT NULL,PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

/** Approve application permission. To allow reviewers access Agenda Report **/
INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES (NULL, 'approve_applications', 
'Permission to Approve application. This permission is useful for agenda report access');

/*Setting to allow notifying additional contacts when client is notified*/
ALTER TABLE `ap_form_elements` ADD `element_notify_contact` INT(11) NULL;
/** Update missing stage_expired_movement_decline */
ALTER TABLE `sub_menus` ADD `stage_expired_movement_decline` VARCHAR( 256 ) NULL ;
/** Create table reviewer_group_notifications for notifications */
CREATE TABLE IF NOT EXISTS `reviewer_group_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT, `workflow_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/** Addition of missing notification columns */
ALTER TABLE `notifications` ADD `title_reviewer` VARCHAR( 256 ) NULL AFTER `autosend` ,
ADD `content_reviewer` VARCHAR( 256 ) NULL AFTER `title_reviewer` ,
ADD `content_sms_reviewer` VARCHAR( 256 ) NULL AFTER `content_reviewer` ;

/*Permit Checker database table and permission*/
CREATE TABLE `permit_checker_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permit_template_id` int(11) DEFAULT NULL,
  `reference_object` varchar(255) DEFAULT NULL,
  `label_to_show` varchar(255) DEFAULT NULL,
  `value_to_show` varchar(255) DEFAULT NULL,
  `sequence_no` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES (NULL, 'managepermitchecker', 'Can Manage Permit Checker');

/** Currency management permissions */
INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES 
(NULL, 'managecurrencies', 'Manage System Currencies for Form Payments');


/*Add product column to take care of the fee type product */

ALTER TABLE `fee` ADD `product` VARCHAR(100) NOT NULL AFTER `minimum_fee`;


/** Add department_id in conditions of approval **/
ALTER TABLE conditions_of_approval ADD department_id INT(10) NULL AFTER permit_id ;

/** permission to resolve issues **/
INSERT INTO `mf_guard_permission` (`id` ,`name` ,`description`)VALUES (
NULL , 'resolvecomment', 'Can Resolve Comments and Conditions of Approval'
);

/** feedback module permissions */
INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES (NULL, 'accessfeedback', 'Access Feedback module');

/* Addition of application queuing field for menus */
ALTER TABLE `menus` ADD `app_queuing` VARCHAR(255) NULL DEFAULT 'DESC';

/* Addition of application queuing field for sub menus */
ALTER TABLE `sub_menus` ADD `app_queuing` VARCHAR( 256 ) NULL DEFAULT 'default' AFTER `title` ;


/*Start Form Logic for field dependancy*/

ALTER TABLE `ap_forms` ADD `logic_dependency_enable` INT(11) NULL;

--
-- Table structure for table `ap_dependency_logic`
--

CREATE TABLE IF NOT EXISTS `ap_dependency_logic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `element_id` int(11) NOT NULL,
  `rule_all_any` varchar(3) NOT NULL DEFAULT 'all',
  `integration_logic` int(1) NOT NULL DEFAULT 0,
  `integration_url` varchar(500) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

--
-- Table structure for table `ap_dependency_logic_rule_permitted_values`
--

CREATE TABLE IF NOT EXISTS `ap_dependency_logic_rule_permitted_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `element_id` int(11) NOT NULL,
  `element_name` varchar(50) NOT NULL,
  `logic_id` int(11) NOT NULL,
  `permitted_value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

--
-- Table structure for table `ap_dependency_logic_conditions`
--

CREATE TABLE IF NOT EXISTS `ap_dependency_logic_conditions` (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `logic_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `target_element_id` int(11) NOT NULL,
  `element_name` varchar(50) NOT NULL DEFAULT '',
  `rule_condition` varchar(15) NOT NULL DEFAULT 'is',
  `rule_keyword` varchar(255) DEFAULT NULL,
  `rule_keyword_option_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
/*End Form Logic for field dependancy*/

/*Task form location*/
ALTER TABLE `task_forms` ADD `longitude` varchar(64) DEFAULT NULL;
ALTER TABLE `task_forms` ADD `latitude` varchar(64) DEFAULT NULL;
/*Task form location*/

/* Permisision */
INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES (NULL, 'manage_analytical_reports', 'Can Add and Configure Management Reports');
INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES (NULL, 'view_analytical_reports', 'Can view Management Reports');

/*Added invoice id to ap_form_payments*/
ALTER TABLE `ap_form_payments` ADD `invoice_id` INT(11) DEFAULT NULL;

/*Option for authorized user to set max duration for each permit instance*/
ALTER TABLE `permits` ADD `user_set_duration` INT(1) NOT NULL;
ALTER TABLE `permits` ADD `stage_set_duration` INT(4) NOT NULL;
CREATE TABLE IF NOT EXISTS `permit_cache` (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `application_id` int(11) NOT NULL,
  `template_id` int(11) NOT NULL,
  `period_uom` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `validity_period` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


/* sms send id for menus table */
ALTER TABLE `menus` ADD `sms_sender` VARCHAR(255) DEFAULT NULL;

/* service code for menus table */
ALTER TABLE `menus` ADD `service_code` VARCHAR(255) DEFAULT NULL;

/*Combining Manual and Digital Payments*/
ALTER TABLE `sub_menus` ADD `stage_payment_confirmation` INT NULL DEFAULT '0';

/** New permission for users to access all available tasks */
INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES (NULL, 'accessalltasks', 'Permission for a reviewer to access all reviewers existing tasks ');
/** Add skipped by **/
ALTER TABLE `task` ADD `skipped_by` INT NULL AFTER `task_stage`;
/** Permission to reset users task */
INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES (NULL, 'resettask', 'Permission for a reviewer to reset a task to pending for editing ');
/** Property to mark a field to have class buildingcategory - used to control what different user categories can view when appliying */
ALTER TABLE `ap_form_elements` ADD `element_buildingcategory` INT NULL DEFAULT '0' AFTER `element_file_qr_users` ;


/* update password change status */
ALTER TABLE `cf_user` ADD `pass_change` INT NOT NULL DEFAULT '0' AFTER `strpassword`;
/** Special permission to allow System Admins duplicate existing workflow settings */
INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES (NULL, 'duplicateworkflows', 'Permission to Duplicate a Workflow Settings ');


/*Query to insert default english translation form element fields missing translations*/
INSERT INTO `ext_translations`(locale, table_class, field_name, field_id, trl_content, option_id)
select 'en_US', 'ap_form_elements', 'element_title', ap_form_elements.form_id, ap_form_elements.element_title, ap_form_elements.element_id from ap_form_elements where Concat(ap_form_elements.form_id, "###", ap_form_elements.element_id) not in (select Concat(ext_translations.field_id, "###", ext_translations.option_id) from ext_translations where table_class='ap_form_elements');
/*End Translations*/
ALTER TABLE `ap_form_elements` ADD `element_buildingcoverage` INT NULL DEFAULT '0' AFTER `element_title`;
ALTER TABLE `ap_form_elements` ADD `element_plotsize` INT NULL DEFAULT '0' AFTER `element_title`;
ALTER TABLE `ap_form_elements` ADD `element_builtup` INT NULL DEFAULT '0' AFTER `element_title`;
ALTER TABLE `ap_form_elements` ADD `element_no_of_floors` INT NULL DEFAULT '0' AFTER `element_title`;
ALTER TABLE `ap_form_elements` ADD `element_grossfloorarea` INT NULL DEFAULT '0' AFTER `element_title`;
ALTER TABLE `ap_form_elements` ADD `element_floor_ratio` INT NULL DEFAULT '0' AFTER `element_title`;
/** content type **/
ALTER TABLE `content` ADD `content_type` VARCHAR(256) NULL AFTER `deleted`;

CREATE TABLE `booklet` (`id` int(11) NOT NULL, `title` varchar(256) NOT NULL,`content` text NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `booklet` ADD PRIMARY KEY (`id`);
ALTER TABLE `booklet` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/* Create Leaves Table */
CREATE TABLE `leave_request` (
  `id` int(10) NOT NULL,
  `reviewer_id` int(10) DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `status` varchar(256) NOT NULL,
  `name` varchar(256) NOT NULL,
  `approved_by` int(10) DEFAULT NULL,
  `approved_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `leave_type` varchar(256) DEFAULT NULL
) ;
ALTER TABLE `leave_request`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `leave_request`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
/* Permission to Approve Leave Requests */
INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) 
VALUES (NULL, 'can_approve_leave_requests', 'Permission to Approve Leave Requests.');
/* Add column for communication type */
alter table communications add column communication_type varchar(256) null ;
alter table communications add column msg_to_reviewer_id int(10) null ;
/** add district column */
ALTER TABLE ap_forms ADD COLUMN district VARCHAR(256) NULL ;
/* Order number for forms */
ALTER TABLE ap_forms ADD COLUMN order_no INT(10) NULL ;


/*AP Settings Profile dir field*/
ALTER TABLE `ap_settings` ADD `profile_dir` TEXT NULL;

--
-- OTB: Table structure for table `application_number_history`
--
CREATE TABLE `application_number_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_entry_id` int(11) DEFAULT NULL,
  `application_number` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


/* add extra column for extra type for submenus */
ALTER TABLE `sub_menus` ADD `extra_type` INT NULL DEFAULT '0' AFTER `stage_payment_confirmation` ;

/** Property for identifying a text field as a UPI */
ALTER TABLE `ap_form_elements` ADD `element_is_upi` INT NULL DEFAULT '0' AFTER `element_file_qr_users` ;

/** reviewer notification when application is in a stage */
ALTER TABLE `notifications` ADD `title_reviewer` VARCHAR(256) NULL AFTER `autosend`, ADD `content_reviewer` VARCHAR(256) NULL AFTER `title_reviewer`, ADD `content_sms_reviewer` VARCHAR(256) NULL AFTER `content_reviewer`;
CREATE TABLE `reviewer_group_notifications` ( `id` int(11) NOT NULL, `workflow_id` int(11) DEFAULT NULL, `group_id` int(11) DEFAULT NULL ) ENGINE=InnoDB DEFAULT CHARSET=latin1 ;
ALTER TABLE `reviewer_group_notifications` ADD PRIMARY KEY (`id`) ;
ALTER TABLE `reviewer_group_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;

 /** professional membership registration */
CREATE TABLE `members_database` (`id` int(11) NOT NULL,`form_id` int(11) NOT NULL,`entry_id` int(11) NOT NULL,`validate` varchar(256) DEFAULT NULL,`status` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `members_database` ADD PRIMARY KEY(`id`);
ALTER TABLE `members_database` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `sf_guard_user_categories` ADD `member_database` INT(11) NULL AFTER `orderid`, ADD `member_database_member_no_field` INT(11) NULL AFTER `member_database`, ADD `member_database_member_email_field` INT(11) NULL AFTER `member_database_member_no_field`, ADD `member_database_member_name_field` INT(11) NULL AFTER `member_database_member_email_field`, ADD `member_no_element_id` INT(11) NULL AFTER `member_database_member_name_field`, ADD `validation_email_element_id` INT(11) NULL AFTER `member_no_element_id`, ADD `membership_email_match` INT(11) NULL AFTER `validation_email_element_id`, ADD `member_association_name` VARCHAR(256) NULL AFTER `membership_email_match`, ADD `member_email_verification_message` TEXT NULL AFTER `member_association_name`, ADD `send_verification_email` INT(11) NULL AFTER `member_email_verification_message`;
/** Insert membership permission */
insert into mf_guard_permission values(null,'validatearchitects','Validate & Manage Professionals membership');

insert into mf_guard_permission values(null,'can_move_to_any_stage','Can Move to Any Stage');


ALTER TABLE `task` ADD `archived` INT NULL DEFAULT '0';
insert into mf_guard_permission values(null,'director_complete','Permission for the director to complete tasks assigned by supervisors');

/** Allow manual payment for an invoice */
ALTER TABLE `mf_invoice` ADD `allow_online_or_manual_payment` INT NULL DEFAULT '0' AFTER `pdf_path`;
INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES (NULL, 'allow_manual_payment', 'Allow a Client to Pay Manually i.e Attaching a receipt for payment confirmation by Finance Team');
/* Special conditions */
ALTER TABLE `permit_cache` ADD `sp_condition` VARCHAR(256) NULL AFTER `validity_period` ;
/** Custom stage to allow CoK users access old applications with errors */
INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES (NULL, 'access_apps_with_errors', 'CoK users to access apps with errors');
/** Indicator task site visit status 
* 0 = pending
* 1 = done
* 2 = ignored/ not required
*/
ALTER TABLE `task` ADD `site_visit_status` INT NULL DEFAULT '0';
/** Permit approval */
CREATE TABLE `permit_approver_info` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `permit_template_id` INT(10) NOT NULL , 
`approval_text` TEXT NOT NULL , `approval_signature` TEXT NOT NULL , `valid_from` VARCHAR(256) NULL , 
`valid_to` VARCHAR(256) NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
/* permits */
ALTER TABLE `permits` ADD `approver_info_id` INT NULL DEFAULT '0';
ALTER TABLE `saved_permit` ADD `approver_info_id` INT NULL DEFAULT '0';
ALTER TABLE `permit_approver_info` ADD `title`  VARCHAR(256) NULL DEFAULT 'Title';
/** Allow only users with access delayed applications to view number of delayed applications */
INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES (NULL, 'access_delayed_applications', 'Permission to Access Delayed Applications');
insert into mf_guard_permission values(null,'approvepaymentsupport','Permission to Cancel Invoice and Other payment options');
/** Column to insert file names uploaded and shared by reviewers while sending application back */
ALTER TABLE `entry_decline` ADD `attachment` VARCHAR(256) NULL DEFAULT NULL AFTER `resolved`;
/** Option to allow backend users attach documents while sending applications back to architect/ clients */
ALTER TABLE `communications` ADD `attachment` VARCHAR(256) NULL DEFAULT NULL AFTER `msg_to_reviewer_id`;
/** Can move application */
insert into mf_guard_permission values(null,'canmoveplan','Permission to control who can move an application from one stage to another stage. Permission assigned to all directors and supervisors');

/* Start - New tables for multicompany configurations.*/
CREATE TABLE IF NOT EXISTS `agency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `logo` varchar(256) DEFAULT NULL,
  `tag_line` varchar(256) DEFAULT NULL,
  `parent_agency` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE `agency_user` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `agency_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`agency_id`),
  KEY `agency_user_agency_id_agency_id` (`agency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Cascade constraint on delete agency
--
ALTER TABLE `agency_user`
  ADD CONSTRAINT `agency_user_agency_id_agency_id` FOREIGN KEY (`agency_id`) REFERENCES `agency` (`id`) ON DELETE CASCADE;

INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES (NULL, 'manageagencies', 'Manage Agencies');

CREATE TABLE `agency_menu` (
  `menu_id` int(11) NOT NULL DEFAULT '0',
  `agency_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menu_id`,`agency_id`),
  KEY `agency_menu_agency_id_agency_id` (`agency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `agency_menu`
  ADD CONSTRAINT `agency_menu_agency_id_agency_id` FOREIGN KEY (`agency_id`) REFERENCES `agency` (`id`) ON DELETE CASCADE;
 
/** Frontend columns **/
alter table menus add column category_id INT(10) NULL ;
/** Add provinces to remove this **/
alter table ap_forms add column province INT(10) NULL ;

ALTER TABLE `ap_form_elements` ADD `element_parentbutton` INT NULL DEFAULT '0' AFTER `element_file_qr_users` ;

/*Option to set invoice number to application number*/
ALTER TABLE `invoicetemplates` ADD `use_application_number` INT(1) NOT NULL;

/** client specific */
CREATE TABLE IF NOT EXISTS `agency_department` (
  `department_id` int(11) NOT NULL DEFAULT '0',
  `agency_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`department_id`,`agency_id`),
  FOREIGN KEY (`agency_id`) REFERENCES `bpmis_kcps_v1`.`agency` (`id`) ON DELETE CASCADE,
  KEY `agency_department_agency_id_agency_id`(`agency_id`)
) ENGINE=InnoDB;
/** Fees update */
ALTER TABLE `fee_range` ADD `condition_set_operator` VARCHAR( 50 ) NULL;
ALTER TABLE `fee_range` MODIFY COLUMN `result_value` text;
ALTER TABLE `fee_range` ADD `value_type` varchar(64) DEFAULT NULL;
CREATE TABLE IF NOT EXISTS `fee_range_condition` (
`id` int(11) NOT NULL,
  `fee_range_id` int(11) DEFAULT NULL,
  `condition_field` int(11) DEFAULT NULL,
  `condition_operator` varchar(100) DEFAULT NULL,
  `condition_value` text DEFAULT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;
ALTER TABLE `fee_range_condition`
 ADD PRIMARY KEY (`id`);
 ALTER TABLE `fee_range_condition`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

/* Columns for permit duration */
alter table permits modify column user_set_duration INT(10) NULL DEFAULT 0 ;
alter table permits modify column stage_set_duration INT(10) NULL DEFAULT 0 ;