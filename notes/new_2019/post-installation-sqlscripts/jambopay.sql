-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 20, 2016 at 12:28 AM
-- Server version: 5.5.49-0ubuntu0.14.04.1
-- PHP Version: 5.5.35-1+donate.sury.org~trusty+4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kabul_construction_permit`
--

-- --------------------------------------------------------

--
-- Table structure for table `jambopay`
--

CREATE TABLE IF NOT EXISTS `jambopay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_jambopay_business` varchar(256) NOT NULL,
  `payment_jambopay_shared_key` varchar(256) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `jambopay`
--

INSERT INTO `jambopay` (`id`, `payment_jambopay_business`, `payment_jambopay_shared_key`, `merchant_id`) VALUES
(3, 'otbafrica', '12345', 7);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
