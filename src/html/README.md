PermitFlow: Construction Permit Management Information System.


How to Install
###############

Assuming you have already copied the files to your server

1. Open your browser and go to http://your_site/install e.g. "http://localhost/install"

2. Follow the instructions and ensure the cache directory is empty

3. Login as admin and use the configuration wizards on the dashboard to configure the system

N/B: The system will automatically create the database for you as long as the required privileges are available.
