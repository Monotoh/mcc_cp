(function (a) {
    a.fn.outerHTML = function () {
        var b = this[0];
        return !b ? null : b.outerHTML || a("<div />").append(b).html()
    }
})(jQuery);

function select_date(g) {
    var f = g[0].getMonth() + 1;
    var b = g[0].getDate();
    var d = g[0].getFullYear();
    var a = $(this).attr("id").split("_");
    var e = a[1] + "_" + a[2];
    var c = f + "/" + b + "/" + d;
    $("#conditionkeyword_" + e).val(c);
    $("#lifieldrule_" + e).data("rule_condition").keyword = c
}

function select_date_page(g) {
    var f = g[0].getMonth() + 1;
    var b = g[0].getDate();
    var d = g[0].getFullYear();
    var a = $(this).attr("id").split("_");
    var e = a[1] + "_" + a[2];
    var c = f + "/" + b + "/" + d;
    $("#conditionkeyword_" + e).val(c);
    $("#lipagerule_" + e).data("rule_condition").keyword = c
}

function select_date_email(g) {
    var f = g[0].getMonth() + 1;
    var b = g[0].getDate();
    var d = g[0].getFullYear();
    var a = $(this).attr("id").split("_");
    var e = a[1] + "_" + a[2];
    var c = f + "/" + b + "/" + d;
    $("#conditionkeyword_" + e).val(c);
    $("#liemailrule_" + e).data("rule_condition").keyword = c
}
$(function () {
    $(".helpmsg").tooltip({
        position: "bottom center",
        offset: [10, 20],
        effect: "fade",
        opacity: 0.8,
        events: {
            def: "click,mouseout"
        }
    });
    $("#dialog-warning").dialog({
        modal: true,
        autoOpen: false,
        closeOnEscape: false,
        width: 600,
        position: ["center", "center"],
        draggable: false,
        resizable: false,
        open: function () {
            $(this).next().find("button").blur()
        },
        buttons: [{
            text: "OK",
            "class": "bb_button bb_small bb_green",
            click: function () {
                $(this).dialog("close")
            }
        }]
    });
    $("#logic_field_enable").click(function () {
        if ($(this).prop("checked") == true) {
            $("#ls_box_field_rules .ls_box_content").slideDown();
            $(".logic_settings").data("logic_status").logic_field_enable = 1
        } else {
            $("#ls_box_field_rules .ls_box_content").slideUp();
            $(".logic_settings").data("logic_status").logic_field_enable = 0
        }
    });
//Start OTB Africa Patch - Form Logic for field dependency
    $("#logic_dependency_enable").click(function () {
        if ($(this).prop("checked") == true) {
            $("#ls_box_dependency_rules .ls_box_content").slideDown();
            $(".logic_settings").data("logic_status").logic_dependency_enable = 1
        } else {
            $("#ls_box_dependency_rules .ls_box_content").slideUp();
            $(".logic_settings").data("logic_status").logic_dependency_enable = 0
        }
    });
//End OTB Africa Patch - Form Logic for field dependency
    $("#logic_workflow_enable").click(function () {
        if ($(this).prop("checked") == true) {
            $("#ls_box_workflow_rules .ls_box_content").slideDown();
            $(".logic_settings").data("logic_status").logic_workflow_enable = 1
        } else {
            $("#ls_box_workflow_rules .ls_box_content").slideUp();
            $(".logic_settings").data("logic_status").logic_workflow_enable = 0
        }
    });
    $("#logic_permission_enable").click(function () {
        if ($(this).prop("checked") == true) {
            $("#ls_box_permission_rules .ls_box_content").slideDown();
            $(".logic_settings").data("logic_status").logic_permission_enable = 1
        } else {
            $("#ls_box_permission_rules .ls_box_content").slideUp();
            $(".logic_settings").data("logic_status").logic_permission_enable = 0
        }
    });
    $("#ls_select_field_rule").bind("change", function () {
        if ($(this).val() == "") {
            return true
        }
        var a = parseInt($(this).val());
        var c = "";
        var g = "";
        g = $("#ls_fields_lookup").clone(false).attr("id", "conditionfield_" + a + "_1").attr("name", "conditionfield_" + a + "_1").show().outerHTML();
        c += '<li id="lifieldrule_' + a + '" style="display: none"><table width="100%" cellspacing="0"><thead><tr><td><strong>' + $(this).find("option:selected").text() + '</strong><a class="delete_lifieldrule" id="deletelifieldrule_' + a + '" href="#"><img src="/form_builder/images/icons/52_blue_16.png"></a></td></tr></thead><tbody><tr><td><h6><img src="/form_builder/images/icons/arrow_right_blue.png" style="vertical-align: top" /><select style="margin-left: 5px;margin-right: 5px" name="fieldruleshowhide_' + a + '" id="fieldruleshowhide_' + a + '" class="element select rule_show_hide"><option value="show">Show</option><option value="hide">Hide</option><option value="verify">Verify</option></select> this field if <select style="margin-left: 5px;margin-right: 5px" name="fieldruleallany_' + a + '" id="fieldruleallany_' + a + '" class="element select rule_all_any"><option value="all">all</option><option value="any">any</option></select> of the following conditions match:</h6><ul class="ls_field_rules_conditions"><li id="lifieldrule_' + a + '_1"> ' + g + ' <select name="conditiontext_' + a + '_1" id="conditiontext_' + a + '_1" class="element select condition_text" style="width: 120px;display: none"><option value="is">Is</option><option value="is_not">Is Not</option><option value="begins_with">Begins with</option><option value="ends_with">Ends with</option><option value="contains">Contains</option><option value="not_contain">Does not contain</option><option value="unique">Unquie</option></select> <select id="conditionnumber_' + a + '_1" name="conditionnumber_' + a + '_1" style="width: 120px;display: none" class="element select condition_number"><option value="is" selected="selected">Is</option><option value="less_than">Less than</option><option value="greater_than">Greater than</option></select> <select id="conditiondate_' + a + '_1" name="conditiondate_' + a + '_1" style="width: 120px;display: none" class="element select condition_date"><option value="is" selected="selected">Is</option><option value="is_before">Is Before</option><option value="is_after">Is After</option></select> <select id="conditioncheckbox_' + a + '_1" name="conditioncheckbox_' + a + '_1" style="width: 120px;display: none" class="element select condition_checkbox"><option value="is_one">Is Checked</option><option value="is_zero">Is Empty</option></select> \n<span name="conditiontime_' + a + '_1" id="conditiontime_' + a + '_1" class="condition_time" style="display: none"><input name="conditiontimehour_' + a + '_1" id="conditiontimehour_' + a + '_1" type="text" class="element text conditiontime_input" maxlength="2" size="2" value="" placeholder="HH"> : <input name="conditiontimeminute_' + a + '_1" id="conditiontimeminute_' + a + '_1" type="text" class="element text conditiontime_input" maxlength="2" size="2" value="" placeholder="MM">  <span class="conditiontime_second" style="display:none"> : <input name="conditiontimesecond_' + a + '_1" id="conditiontimesecond_' + a + '_1" type="text" class="element text conditiontime_input" maxlength="2" size="2" value="" placeholder="SS"> </span><select class="element select conditiontime_ampm conditiontime_input" name="conditiontimeampm_' + a + '_1" id="conditiontimeampm_' + a + '_1" style="display:none"><option selected="selected" value="AM">AM</option><option value="PM">PM</option></select></span><input type="text" class="element text condition_keyword" value="" id="conditionkeyword_' + a + '_1" name="conditionkeyword_' + a + '_1" style="display: none"> \n<a href="#" id="deletecondition_' + a + '_1" name="deletecondition_' + a + '_1" class="a_delete_condition"><img src="/form_builder/images/icons/51_blue_16.png" /></a>\n</li><li class="ls_add_condition"><a href="#" id="addcondition_' + a + '" class="a_add_condition"><img src="/form_builder/images/icons/49_blue_16.png" /></a></li></ul></td></tr></tbody></table></li>';
        $("#ls_field_rules_group").prepend(c);
        $("#lifieldrule_" + a).hide();
        //$("#conditionfield_" + a + "_1 option[value=element_" + a + "]").remove();
        //$("#conditionfield_" + a + "_1 option[value^=element_" + a + "_]").remove();
        var d = $("#conditionfield_" + a + "_1").eq(0).val();
        var e = $("#ls_fields_lookup").data(d);
        var b = "is";
        if (e == "money" || e == "number") {
            $("#conditionnumber_" + a + "_1").show();
            $("#conditionkeyword_" + a + "_1").show()
        } else {
            if (e == "date" || e == "europe_date") {
                $("#conditiondate_" + a + "_1").show();
                $("#conditionkeyword_" + a + "_1").show();
                $("#lifieldrule_" + a + "_1").addClass("condition_date")
            } else {
                if (e == "time" || e == "time_showsecond" || e == "time_24hour" || e == "time_showsecond24hour") {
                    $("#conditiondate_" + a + "_1").show();
                    $("#conditiontime_" + a + "_1").show();
                    if (e == "time") {
                        $("#conditiontimeampm_" + a + "_1").show()
                    } else {
                        if (e == "time_showsecond") {
                            $("#conditiontimeampm_" + a + "_1").show();
                            $("#conditiontimesecond_" + a + "_1").parent().show()
                        } else {
                            if (e == "time_showsecond24hour") {
                                $("#conditiontimesecond_" + a + "_1").parent().show()
                            }
                        }
                    }
                } else {
                    if (e == "checkbox") {
                        $("#conditioncheckbox_" + a + "_1").show();
                        b = "is_one"
                    } else {
                        $("#conditiontext_" + a + "_1").show();
                        $("#conditionkeyword_" + a + "_1").show()
                    }
                }
            }
        }
        var f = ' <input type="hidden" value="" name="datepicker_' + a + '_1" id="datepicker_' + a + '_1">\n <span style="display:none"><img id="datepickimg_' + a + '_1" alt="Pick date." src="/form_builder/images/icons/calendar.png" class="trigger condition_date_trigger" style="vertical-align: top; cursor: pointer" /></span>';
        $("#conditionkeyword_" + a + "_1").after(f);
        $("#datepicker_" + a + "_1").datepick({
            onSelect: select_date,
            showTrigger: "#datepickimg_" + a + "_1"
        });
        $("#lifieldrule_" + a).slideDown();
        $("#lifieldrule_" + a).data("rule_properties", {
            element_id: a,
            rule_show_hide: "show",
            rule_all_any: "all"
        });
        $("#lifieldrule_" + a + "_1").data("rule_condition", {
            target_element_id: a,
            element_name: d,
            condition: b,
            keyword: ""
        });
        $(this).find("option:selected").remove();
        if ($("#ls_select_field_rule option").length == 1) {
            $("#ls_select_field_rule option").text("No More Fields Available")
        }
    });
//Start OTB Africa Patch - Form Logic for field dependency
	var rule_temp_id = 0;
    $("#ls_select_dependency_rule").bind("change", function () {
		if(rule_temp_id == 0){
			var rule_ids = new Array();
			$(".existing_permitted_values").each(function(i, selected) {
				//rule_temp_id = $(selected).attr("id");//Get logic id for already set logic rules
				rule_ids.push($(selected).attr("id"));
			});
			rule_temp_id = Math.max.apply(null, rule_ids);//Get logic id for already set logic rules
		}
		rule_temp_id++;
        if ($(this).val() == "") {
            return true
        }
        var a = parseInt($(this).val());
        var c = "";
        var g = "";
        var h = "";
        g = $("#ls_dependency_lookup").clone(false).attr("id", "conditiondependency_" + a + "_1").attr("name", "conditiondependency_" + a + "_1").show().outerHTML();
		h = $(".ls_dependency_rule_permitted_values_"+a).clone().attr("id", rule_temp_id).show().outerHTML();//Permitted values selection using form_manager functions
        c += '<li id="lidependencyrule_' + a + '_rule_' + rule_temp_id +'" class="lidependencyrule_' + a + '" style="display: none"><table width="100%" cellspacing="0"><thead><tr><td><strong>' + $(this).find("option:selected").text() + '</strong><a class="delete_lidependencyrule" id="deletelidependencyrule_' + a + '_'+rule_temp_id+'" href="#"><img src="/form_builder/images/icons/52_blue_16.png"></a></td></tr></thead><tbody><tr><td><h6><img src="/form_builder/images/icons/arrow_right_blue.png" style="vertical-align: top" /><ul id="'+rule_temp_id+'" class="ls_dependency_rule_permitted_values">' + h + '</ul> The above selected are permitted value(s) of this field if <select style="margin-left: 5px;margin-right: 5px" name="fieldruleallany_' + a + '" id="fieldruleallany_' + a + '" class="element select rule_all_any"><option value="all">all</option><option value="any">any</option></select> of the following conditions match:</h6><ul id="'+rule_temp_id+'" class="ls_dependency_rules_conditions"><li id="lidependencyrule_' + a + '_1'+ '_rule_' + rule_temp_id+'"> ' + g + ' <select name="conditiontext_' + a + '_1" id="conditiontext_' + a + '_1" class="element select condition_text" style="width: 120px;display: none"><option value="is">Is</option><option value="is_not">Is Not</option><option value="begins_with">Begins with</option><option value="ends_with">Ends with</option><option value="contains">Contains</option><option value="not_contain">Does not contain</option><option value="unique">Unquie</option></select> <select id="conditionnumber_' + a + '_1" name="conditionnumber_' + a + '_1" style="width: 120px;display: none" class="element select condition_number"><option value="is" selected="selected">Is</option><option value="less_than">Less than</option><option value="greater_than">Greater than</option></select> <select id="conditiondate_' + a + '_1" name="conditiondate_' + a + '_1" style="width: 120px;display: none" class="element select condition_date"><option value="is" selected="selected">Is</option><option value="is_before">Is Before</option><option value="is_after">Is After</option></select> <select id="conditioncheckbox_' + a + '_1" name="conditioncheckbox_' + a + '_1" style="width: 120px;display: none" class="element select condition_checkbox"><option value="is_one">Is Checked</option><option value="is_zero">Is Empty</option></select> \n<span name="conditiontime_' + a + '_1" id="conditiontime_' + a + '_1" class="condition_time" style="display: none"><input name="conditiontimehour_' + a + '_1" id="conditiontimehour_' + a + '_1" type="text" class="element text conditiontime_input" maxlength="2" size="2" value="" placeholder="HH"> : <input name="conditiontimeminute_' + a + '_1" id="conditiontimeminute_' + a + '_1" type="text" class="element text conditiontime_input" maxlength="2" size="2" value="" placeholder="MM">  <span class="conditiontime_second" style="display:none"> : <input name="conditiontimesecond_' + a + '_1" id="conditiontimesecond_' + a + '_1" type="text" class="element text conditiontime_input" maxlength="2" size="2" value="" placeholder="SS"> </span><select class="element select conditiontime_ampm conditiontime_input" name="conditiontimeampm_' + a + '_1" id="conditiontimeampm_' + a + '_1" style="display:none"><option selected="selected" value="AM">AM</option><option value="PM">PM</option></select></span><input type="text" class="element text condition_keyword" value="" id="conditionkeyword_' + a + '_1" name="conditionkeyword_' + a + '_1" style="display: none"> \n<a href="#overridedependency" id="deletecondition_' + a + '_1" name="deletecondition_' + a + '_1" class="a_delete_condition"><img src="/form_builder/images/icons/51_blue_16.png" /></a>\n</li><li class="ls_add_condition"><a href="#overridedependency" id="addcondition_' + a + '" class="a_add_condition"><img src="/form_builder/images/icons/49_blue_16.png" /></a></li></ul></td></tr></tbody></table></li>';
        $("#ls_dependency_rules_group").prepend(c);
        $("#lidependencyrule_" + a + '_rule_' + rule_temp_id).hide();
        $("#conditiondependency_" + a + "_1 option[value=element_" + a + "]").remove();
        $("#conditiondependency_" + a + "_1 option[value^=element_" + a + "_]").remove();
        var d = $("#conditiondependency_" + a + "_1").eq(0).val();
        var e = $("#ls_dependency_lookup").data(d);
        var b = "is";
        if (e == "money" || e == "number") {
            $("#conditionnumber_" + a + "_1").show();
            $("#conditionkeyword_" + a + "_1").show()
        } else {
            if (e == "date" || e == "europe_date") {
                $("#conditiondate_" + a + "_1").show();
                $("#conditionkeyword_" + a + "_1").show();
                $("#lidependencyrule_" + a + "_1"+ "_rule_" + rule_temp_id).addClass("condition_date")
            } else {
                if (e == "time" || e == "time_showsecond" || e == "time_24hour" || e == "time_showsecond24hour") {
                    $("#conditiondate_" + a + "_1").show();
                    $("#conditiontime_" + a + "_1").show();
                    if (e == "time") {
                        $("#conditiontimeampm_" + a + "_1").show()
                    } else {
                        if (e == "time_showsecond") {
                            $("#conditiontimeampm_" + a + "_1").show();
                            $("#conditiontimesecond_" + a + "_1").parent().show()
                        } else {
                            if (e == "time_showsecond24hour") {
                                $("#conditiontimesecond_" + a + "_1").parent().show()
                            }
                        }
                    }
                } else {
                    if (e == "checkbox") {
                        $("#conditioncheckbox_" + a + "_1").show();
                        b = "is_one"
                    } else {
                        $("#conditiontext_" + a + "_1").show();
                        $("#conditionkeyword_" + a + "_1").show()
                    }
                }
            }
        }
        var f = ' <input type="hidden" value="" name="datepicker_' + a + '_1" id="datepicker_' + a + '_1">\n <span style="display:none"><img id="datepickimg_' + a + '_1" alt="Pick date." src="/form_builder/images/icons/calendar.png" class="trigger condition_date_trigger" style="vertical-align: top; cursor: pointer" /></span>';
        $("#conditionkeyword_" + a + "_1").after(f);
        $("#datepicker_" + a + "_1").datepick({
            onSelect: select_date,
            showTrigger: "#datepickimg_" + a + "_1"
        });
        $("#lidependencyrule_" + a + '_rule_' + rule_temp_id).slideDown();
        $("#lidependencyrule_" + a + '_rule_' + rule_temp_id).data("rule_properties", {
            element_id: a,
            rule_permitted_values: [],
            rule_temp_id: rule_temp_id,
            rule_all_any: "all"
        });
        $("#lidependencyrule_" + a + "_1" + "_rule_" + rule_temp_id).data("rule_condition", {
            target_element_id: a,
            element_name: d,
            condition: b,
            keyword: "",
            rule_temp_id: rule_temp_id
        });
        //$(this).find("option:selected").remove();
		$(this).prop('selectedIndex',0);
        if ($("#ls_select_dependency_rule option").length == 1) {
            $("#ls_select_dependency_rule option").text("No More Fields Available")
        }
    });
//End OTB Africa Patch - Form Logic for field dependency
    $("#ls_select_workflow_rule").bind("change", function () {
        if ($(this).val() == "") {
            return true
        }
        var a = parseInt($(this).val());
        var c = "";
        var g = "";
        g = $("#ls_workflow_lookup").clone(false).attr("id", "conditionworkflow_" + a + "_1").attr("name", "conditionworkflow_" + a + "_1").show().outerHTML();
        c += '<li id="liworkflowrule_' + a + '" style="display: none"><table width="100%" cellspacing="0"><thead><tr><td><strong>' + $(this).find("option:selected").text() + '</strong><a class="delete_liworkflowrule" id="deleteliworkflowrule_' + a + '" href="#"><img src="/form_builder/images/icons/52_blue_16.png"></a></td></tr></thead><tbody><tr><td><h6><img src="/form_builder/images/icons/arrow_right_blue.png" style="vertical-align: top" /><select style="margin-left: 5px;margin-right: 5px" name="workflowruleshowhide_' + a + '" id="workflowruleshowhide_' + a + '" class="element select rule_show_hide"><option value="show">Show</option><option value="hide">Hide</option></select> this field if <select style="margin-left: 5px;margin-right: 5px" name="workflowruleallany_' + a + '" id="workflowruleallany_' + a + '" class="element select rule_all_any"><option value="all">all</option><option value="any">any</option></select> of the following conditions match:</h6><ul class="ls_workflow_rules_conditions"><li id="liworkflowrule_' + a + '_1"> ' + g + ' <select name="conditiontext_' + a + '_1" id="conditiontext_' + a + '_1" class="element select condition_text" style="width: 120px;display: none"><option value="is">Is</option><option value="is_not">Is Not</option><option value="begins_with">Begins with</option><option value="ends_with">Ends with</option><option value="contains">Contains</option><option value="not_contain">Does not contain</option></select> <select id="conditionnumber_' + a + '_1" name="conditionnumber_' + a + '_1" style="width: 120px;display: none" class="element select condition_number"><option value="is" selected="selected">Is</option><option value="less_than">Less than</option><option value="greater_than">Greater than</option></select> <select id="conditiondate_' + a + '_1" name="conditiondate_' + a + '_1" style="width: 120px;display: none" class="element select condition_date"><option value="is" selected="selected">Is</option><option value="is_before">Is Before</option><option value="is_after">Is After</option></select> <select id="conditioncheckbox_' + a + '_1" name="conditioncheckbox_' + a + '_1" style="width: 120px;display: none" class="element select condition_checkbox"><option value="is_one">Is Checked</option><option value="is_zero">Is Empty</option></select> \n<span name="conditiontime_' + a + '_1" id="conditiontime_' + a + '_1" class="condition_time" style="display: none"><input name="conditiontimehour_' + a + '_1" id="conditiontimehour_' + a + '_1" type="text" class="element text conditiontime_input" maxlength="2" size="2" value="" placeholder="HH"> : <input name="conditiontimeminute_' + a + '_1" id="conditiontimeminute_' + a + '_1" type="text" class="element text conditiontime_input" maxlength="2" size="2" value="" placeholder="MM">  <span class="conditiontime_second" style="display:none"> : <input name="conditiontimesecond_' + a + '_1" id="conditiontimesecond_' + a + '_1" type="text" class="element text conditiontime_input" maxlength="2" size="2" value="" placeholder="SS"> </span><select class="element select conditiontime_ampm conditiontime_input" name="conditiontimeampm_' + a + '_1" id="conditiontimeampm_' + a + '_1" style="display:none"><option selected="selected" value="AM">AM</option><option value="PM">PM</option></select></span><input type="text" class="element text condition_keyword" value="" id="conditionkeyword_' + a + '_1" name="conditionkeyword_' + a + '_1" style="display: none"> \n<a href="#" id="deletecondition_' + a + '_1" name="deletecondition_' + a + '_1" class="a_delete_condition"><img src="/form_builder/images/icons/51_blue_16.png" /></a>\n</li><li class="ls_add_condition"><a href="#" id="addcondition_' + a + '" class="a_add_condition"><img src="/form_builder/images/icons/49_blue_16.png" /></a></li></ul></td></tr></tbody></table></li>';
        $("#ls_workflow_rules_group").prepend(c);
        $("#liworkflowrule_" + a).hide();
        $("#conditionworkflow_" + a + "_1 option[value=element_" + a + "]").remove();
        $("#conditionworkflow_" + a + "_1 option[value^=element_" + a + "_]").remove();
        var d = $("#conditionworkflow_" + a + "_1").eq(0).val();
        var e = $("#ls_workflow_lookup").data(d);
        var b = "is";
        if (e == "money" || e == "number") {
            $("#conditionnumber_" + a + "_1").show();
            $("#conditionkeyword_" + a + "_1").show()
        } else {
            if (e == "date" || e == "europe_date") {
                $("#conditiondate_" + a + "_1").show();
                $("#conditionkeyword_" + a + "_1").show();
                $("#lifieldrule_" + a + "_1").addClass("condition_date")
            } else {
                if (e == "time" || e == "time_showsecond" || e == "time_24hour" || e == "time_showsecond24hour") {
                    $("#conditiondate_" + a + "_1").show();
                    $("#conditiontime_" + a + "_1").show();
                    if (e == "time") {
                        $("#conditiontimeampm_" + a + "_1").show()
                    } else {
                        if (e == "time_showsecond") {
                            $("#conditiontimeampm_" + a + "_1").show();
                            $("#conditiontimesecond_" + a + "_1").parent().show()
                        } else {
                            if (e == "time_showsecond24hour") {
                                $("#conditiontimesecond_" + a + "_1").parent().show()
                            }
                        }
                    }
                } else {
                    if (e == "checkbox") {
                        $("#conditioncheckbox_" + a + "_1").show();
                        b = "is_one"
                    } else {
                        $("#conditiontext_" + a + "_1").show();
                        $("#conditionkeyword_" + a + "_1").show()
                    }
                }
            }
        }
        var f = ' <input type="hidden" value="" name="datepicker_' + a + '_1" id="datepicker_' + a + '_1">\n <span style="display:none"><img id="datepickimg_' + a + '_1" alt="Pick date." src="/form_builder/images/icons/calendar.png" class="trigger condition_date_trigger" style="vertical-align: top; cursor: pointer" /></span>';
        $("#conditionkeyword_" + a + "_1").after(f);
        $("#datepicker_" + a + "_1").datepick({
            onSelect: select_date,
            showTrigger: "#datepickimg_" + a + "_1"
        });
        $("#liworkflowrule_" + a).slideDown();
        $("#liworkflowrule_" + a).data("rule_properties", {
            element_id: a,
            rule_show_hide: "show",
            rule_all_any: "all"
        });
        $("#liworkflowrule_" + a + "_1").data("rule_condition", {
            target_element_id: a,
            element_name: d,
            condition: b,
            keyword: ""
        });
        $(this).find("option:selected").remove();
        if ($("#ls_select_workflow_rule option").length == 1) {
            $("#ls_select_workflow_rule option").text("No More Fields Available")
        }
    });
    $("#ls_select_permission_rule").bind("change", function () {
        if ($(this).val() == "") {
            return true
        }
        var a = parseInt($(this).val());
        var c = "";
        var g = "";
        g = $("#ls_permission_lookup").clone(false).attr("id", "conditionpermission_" + a + "_1").attr("name", "conditionpermission_" + a + "_1").show().outerHTML();
        c += '<li id="lipermissionrule_' + a + '" style="display: none"><table width="100%" cellspacing="0"><thead><tr><td><strong>' + $(this).find("option:selected").text() + '</strong><a class="delete_lipermissionrule" id="deletelipermissionrule_' + a + '" href="#"><img src="/form_builder/images/icons/52_blue_16.png"></a></td></tr></thead><tbody><tr><td><h6><img src="/form_builder/images/icons/arrow_right_blue.png" style="vertical-align: top" /><select style="margin-left: 5px;margin-right: 5px" name="permissionruleshowhide_' + a + '" id="permissionruleshowhide_' + a + '" class="element select rule_show_hide"><option value="show">Show</option><option value="hide">Hide</option></select> this field if <select style="margin-left: 5px;margin-right: 5px" name="permissionruleallany_' + a + '" id="permissionruleallany_' + a + '" class="element select rule_all_any"><option value="all">all</option><option value="any">any</option></select> of the following conditions match:</h6><ul class="ls_permission_rules_conditions"><li id="lipermissionrule_' + a + '_1"> ' + g + ' <select name="conditiontext_' + a + '_1" id="conditiontext_' + a + '_1" class="element select condition_text" style="width: 120px;display: none"><option value="is">Is</option><option value="is_not">Is Not</option><option value="begins_with">Begins with</option><option value="ends_with">Ends with</option><option value="contains">Contains</option><option value="not_contain">Does not contain</option></select> <select id="conditionnumber_' + a + '_1" name="conditionnumber_' + a + '_1" style="width: 120px;display: none" class="element select condition_number"><option value="is" selected="selected">Is</option><option value="less_than">Less than</option><option value="greater_than">Greater than</option></select> <select id="conditiondate_' + a + '_1" name="conditiondate_' + a + '_1" style="width: 120px;display: none" class="element select condition_date"><option value="is" selected="selected">Is</option><option value="is_before">Is Before</option><option value="is_after">Is After</option></select> <select id="conditioncheckbox_' + a + '_1" name="conditioncheckbox_' + a + '_1" style="width: 120px;display: none" class="element select condition_checkbox"><option value="is_one">Is Checked</option><option value="is_zero">Is Empty</option></select> \n<span name="conditiontime_' + a + '_1" id="conditiontime_' + a + '_1" class="condition_time" style="display: none"><input name="conditiontimehour_' + a + '_1" id="conditiontimehour_' + a + '_1" type="text" class="element text conditiontime_input" maxlength="2" size="2" value="" placeholder="HH"> : <input name="conditiontimeminute_' + a + '_1" id="conditiontimeminute_' + a + '_1" type="text" class="element text conditiontime_input" maxlength="2" size="2" value="" placeholder="MM">  <span class="conditiontime_second" style="display:none"> : <input name="conditiontimesecond_' + a + '_1" id="conditiontimesecond_' + a + '_1" type="text" class="element text conditiontime_input" maxlength="2" size="2" value="" placeholder="SS"> </span><select class="element select conditiontime_ampm conditiontime_input" name="conditiontimeampm_' + a + '_1" id="conditiontimeampm_' + a + '_1" style="display:none"><option selected="selected" value="AM">AM</option><option value="PM">PM</option></select></span><input type="text" class="element text condition_keyword" value="" id="conditionkeyword_' + a + '_1" name="conditionkeyword_' + a + '_1" style="display: none"> \n<a href="#" id="deletecondition_' + a + '_1" name="deletecondition_' + a + '_1" class="a_delete_condition"><img src="/form_builder/images/icons/51_blue_16.png" /></a>\n</li><li class="ls_add_condition"><a href="#" id="addcondition_' + a + '" class="a_add_condition"><img src="/form_builder/images/icons/49_blue_16.png" /></a></li></ul></td></tr></tbody></table></li>';
        $("#ls_permission_rules_group").prepend(c);
        $("#lipermissionrule_" + a).hide();
        $("#conditionpermission_" + a + "_1 option[value=element_" + a + "]").remove();
        $("#conditionpermission_" + a + "_1 option[value^=element_" + a + "_]").remove();
        var d = $("#conditionpermission_" + a + "_1").eq(0).val();
        var e = $("#ls_permission_lookup").data(d);
        var b = "is";
        if (e == "money" || e == "number") {
            $("#conditionnumber_" + a + "_1").show();
            $("#conditionkeyword_" + a + "_1").show()
        } else {
            if (e == "date" || e == "europe_date") {
                $("#conditiondate_" + a + "_1").show();
                $("#conditionkeyword_" + a + "_1").show();
                $("#lifieldrule_" + a + "_1").addClass("condition_date")
            } else {
                if (e == "time" || e == "time_showsecond" || e == "time_24hour" || e == "time_showsecond24hour") {
                    $("#conditiondate_" + a + "_1").show();
                    $("#conditiontime_" + a + "_1").show();
                    if (e == "time") {
                        $("#conditiontimeampm_" + a + "_1").show()
                    } else {
                        if (e == "time_showsecond") {
                            $("#conditiontimeampm_" + a + "_1").show();
                            $("#conditiontimesecond_" + a + "_1").parent().show()
                        } else {
                            if (e == "time_showsecond24hour") {
                                $("#conditiontimesecond_" + a + "_1").parent().show()
                            }
                        }
                    }
                } else {
                    if (e == "checkbox") {
                        $("#conditioncheckbox_" + a + "_1").show();
                        b = "is_one"
                    } else {
                        $("#conditiontext_" + a + "_1").show();
                        $("#conditionkeyword_" + a + "_1").show()
                    }
                }
            }
        }
        var f = ' <input type="hidden" value="" name="datepicker_' + a + '_1" id="datepicker_' + a + '_1">\n <span style="display:none"><img id="datepickimg_' + a + '_1" alt="Pick date." src="/form_builder/images/icons/calendar.png" class="trigger condition_date_trigger" style="vertical-align: top; cursor: pointer" /></span>';
        $("#conditionkeyword_" + a + "_1").after(f);
        $("#datepicker_" + a + "_1").datepick({
            onSelect: select_date,
            showTrigger: "#datepickimg_" + a + "_1"
        });
        $("#lipermissionrule_" + a).slideDown();
        $("#lipermissionrule_" + a).data("rule_properties", {
            element_id: a,
            rule_show_hide: "show",
            rule_all_any: "all"
        });
        $("#lipermissionrule_" + a + "_1").data("rule_condition", {
            target_element_id: a,
            element_name: d,
            condition: b,
            keyword: ""
        });
        $(this).find("option:selected").remove();
        if ($("#ls_select_permission_rule option").length == 1) {
            $("#ls_select_permission_rule option").text("No More Fields Available")
        }
    });
    $("#ls_box_field_rules").delegate("select.rule_show_hide", "change", function (b) {
        var a = $(this).attr("id").split("_");
        $("#lifieldrule_" + a[1]).data("rule_properties").rule_show_hide = $(this).val()
    });
//Start OTB Africa Patch - Form Logic for field dependency
    $("#ls_box_dependency_rules").delegate("input.rule_permitted_values", "change", function (b) {
        var a = $(this).attr("id").split("_");
        var rule_temp_id = $(this).parent().attr("id");//Get the temporary id
		var permitted_values = [];
		$("[name='dependencyrulepermittedvalues_"+a[1]+"[]']").each(function(i, selected){ 
			var add_to_permitted = false;
			if($(selected).attr("type") == 'checkbox' && $(selected).val() && $(selected).parent().attr("id") == rule_temp_id){
				if($(selected).prop("checked")){
					add_to_permitted = true;
				}
			}else if($(selected).val() && $(selected).parent().parent().attr("id") == rule_temp_id){
				add_to_permitted = true;			
			}

			if(add_to_permitted){
				permitted_values[i] = $(selected).val();
			}
			permitted_values = permitted_values.filter(function(n){ return n != undefined }); 
		});
		console.log('rule count is ### '+rule_temp_id+' my guy the value for this is #### '+$(this).val()+' and checked is #### '+$(this).prop("checked")+' and permitted values are #### ');
		console.log(permitted_values);
        $("#lidependencyrule_" + a[1] + '_rule_' + rule_temp_id).data("rule_properties").rule_permitted_values = permitted_values;
    });
//End OTB Africa Patch - Form Logic for field dependency
    $("#ls_box_workflow_rules").delegate("select.rule_show_hide", "change", function (b) {
        var a = $(this).attr("id").split("_");
        $("#liworkflowrule_" + a[1]).data("rule_properties").rule_show_hide = $(this).val()
    });
    $("#ls_box_permission_rules").delegate("select.rule_show_hide", "change", function (b) {
        var a = $(this).attr("id").split("_");
        $("#lipermissionrule_" + a[1]).data("rule_properties").rule_show_hide = $(this).val()
    });
    $("#ls_box_field_rules").delegate("select.rule_all_any", "change", function (b) {
        var a = $(this).attr("id").split("_");
        $("#lifieldrule_" + a[1]).data("rule_properties").rule_all_any = $(this).val()
    });
//Start OTB Africa Patch - Form Logic for field dependency
    $("#ls_box_dependency_rules").delegate("select.rule_all_any", "change", function (b) {
        var a = $(this).attr("id").split("_");
        $("#lidependencyrule_" + a[1] +  '_rule_' + a[3]).data("rule_properties").rule_all_any = $(this).val()
    });
//End OTB Africa Patch - Form Logic for field dependency
    $("#ls_box_workflow_rules").delegate("select.rule_all_any", "change", function (b) {
        var a = $(this).attr("id").split("_");
        $("#liworkflowrule_" + a[1]).data("rule_properties").rule_all_any = $(this).val()
    });
    $("#ls_box_permission_rules").delegate("select.rule_all_any", "change", function (b) {
        var a = $(this).attr("id").split("_");
        $("#lipermissionrule_" + a[1]).data("rule_properties").rule_all_any = $(this).val()
    });
    $("#ls_box_field_rules").delegate("select.condition_fieldname", "change", function (c) {
        var a = $(this).val();
        var b = $("#ls_fields_lookup").data(a);
        $(this).parent().find(".condition_text,.condition_time,.condition_number,.condition_date,.condition_checkbox,.condition_keyword").hide();
        $(this).parent().removeClass("condition_date");
        if (b == "money" || b == "number") {
            $(this).parent().find(".condition_number,input.text").show();
            $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_number").val()
        } else {
            if (b == "date" || b == "europe_date") {
                $(this).parent().addClass("condition_date");
                $(this).parent().find(".condition_date,input.text").show();
                $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_date").val()
            } else {
                if (b == "time" || b == "time_showsecond" || b == "time_24hour" || b == "time_showsecond24hour") {
                    $(this).parent().find(".condition_date,.condition_time").show();
                    $(this).parent().find(".condition_time .conditiontime_second,.condition_time .conditiontime_ampm").hide();
                    if (b == "time") {
                        $(this).parent().find(".condition_time .conditiontime_ampm").show()
                    } else {
                        if (b == "time_showsecond") {
                            $(this).parent().find(".condition_time .conditiontime_ampm,.condition_time .conditiontime_second").show()
                        } else {
                            if (b == "time_showsecond24hour") {
                                $(this).parent().find(".condition_time .conditiontime_second").show()
                            }
                        }
                    }
                    $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_date").val()
                } else {
                    if (b == "checkbox") {
                        $(this).parent().find(".condition_checkbox").show();
                        $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_checkbox").val()
                    } else {
                        $(this).parent().find(".condition_text,input.text").show();
                        $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_text").val()
                    }
                }
            }
        }
        $(this).parent().data("rule_condition").element_name = a
    });
//Start OTB Africa Patch - Form Logic for field dependency
    $("#ls_box_dependency_rules").delegate("select.condition_fieldname", "change", function (c) {
        var a = $(this).val();
        var b = $("#ls_dependency_lookup").data(a);
        $(this).parent().find(".condition_text,.condition_time,.condition_number,.condition_date,.condition_checkbox,.condition_keyword").hide();
        $(this).parent().removeClass("condition_date");
        if (b == "money" || b == "number") {
            $(this).parent().find(".condition_number,input.text").show();
            $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_number").val()
        } else {
            if (b == "date" || b == "europe_date") {
                $(this).parent().addClass("condition_date");
                $(this).parent().find(".condition_date,input.text").show();
                $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_date").val()
            } else {
                if (b == "time" || b == "time_showsecond" || b == "time_24hour" || b == "time_showsecond24hour") {
                    $(this).parent().find(".condition_date,.condition_time").show();
                    $(this).parent().find(".condition_time .conditiontime_second,.condition_time .conditiontime_ampm").hide();
                    if (b == "time") {
                        $(this).parent().find(".condition_time .conditiontime_ampm").show()
                    } else {
                        if (b == "time_showsecond") {
                            $(this).parent().find(".condition_time .conditiontime_ampm,.condition_time .conditiontime_second").show()
                        } else {
                            if (b == "time_showsecond24hour") {
                                $(this).parent().find(".condition_time .conditiontime_second").show()
                            }
                        }
                    }
                    $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_date").val()
                } else {
                    if (b == "checkbox") {
                        $(this).parent().find(".condition_checkbox").show();
                        $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_checkbox").val()
                    } else {
                        $(this).parent().find(".condition_text,input.text").show();
                        $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_text").val()
                    }
                }
            }
        }
        $(this).parent().data("rule_condition").element_name = a
    });
//End OTB Africa Patch - Form Logic for field dependency
    $("#ls_box_workflow_rules").delegate("select.condition_fieldname", "change", function (c) {
        var a = $(this).val();
        var b = $("#ls_workflow_lookup").data(a);
        $(this).parent().find(".condition_text,.condition_time,.condition_number,.condition_date,.condition_checkbox,.condition_keyword").hide();
        $(this).parent().removeClass("condition_date");
        if (b == "money" || b == "number") {
            $(this).parent().find(".condition_number,input.text").show();
            $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_number").val()
        } else {
            if (b == "date" || b == "europe_date") {
                $(this).parent().addClass("condition_date");
                $(this).parent().find(".condition_date,input.text").show();
                $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_date").val()
            } else {
                if (b == "time" || b == "time_showsecond" || b == "time_24hour" || b == "time_showsecond24hour") {
                    $(this).parent().find(".condition_date,.condition_time").show();
                    $(this).parent().find(".condition_time .conditiontime_second,.condition_time .conditiontime_ampm").hide();
                    if (b == "time") {
                        $(this).parent().find(".condition_time .conditiontime_ampm").show()
                    } else {
                        if (b == "time_showsecond") {
                            $(this).parent().find(".condition_time .conditiontime_ampm,.condition_time .conditiontime_second").show()
                        } else {
                            if (b == "time_showsecond24hour") {
                                $(this).parent().find(".condition_time .conditiontime_second").show()
                            }
                        }
                    }
                    $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_date").val()
                } else {
                    if (b == "checkbox") {
                        $(this).parent().find(".condition_checkbox").show();
                        $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_checkbox").val()
                    } else {
                        $(this).parent().find(".condition_text,input.text").show();
                        $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_text").val()
                    }
                }
            }
        }
        $(this).parent().data("rule_condition").element_name = a
    });
    $("#ls_box_permission_rules").delegate("select.condition_fieldname", "change", function (c) {
        var a = $(this).val();
        var b = $("#ls_permission_lookup").data(a);
        $(this).parent().find(".condition_text,.condition_time,.condition_number,.condition_date,.condition_checkbox,.condition_keyword").hide();
        $(this).parent().removeClass("condition_date");
        if (b == "money" || b == "number") {
            $(this).parent().find(".condition_number,input.text").show();
            $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_number").val()
        } else {
            if (b == "date" || b == "europe_date") {
                $(this).parent().addClass("condition_date");
                $(this).parent().find(".condition_date,input.text").show();
                $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_date").val()
            } else {
                if (b == "time" || b == "time_showsecond" || b == "time_24hour" || b == "time_showsecond24hour") {
                    $(this).parent().find(".condition_date,.condition_time").show();
                    $(this).parent().find(".condition_time .conditiontime_second,.condition_time .conditiontime_ampm").hide();
                    if (b == "time") {
                        $(this).parent().find(".condition_time .conditiontime_ampm").show()
                    } else {
                        if (b == "time_showsecond") {
                            $(this).parent().find(".condition_time .conditiontime_ampm,.condition_time .conditiontime_second").show()
                        } else {
                            if (b == "time_showsecond24hour") {
                                $(this).parent().find(".condition_time .conditiontime_second").show()
                            }
                        }
                    }
                    $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_date").val()
                } else {
                    if (b == "checkbox") {
                        $(this).parent().find(".condition_checkbox").show();
                        $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_checkbox").val()
                    } else {
                        $(this).parent().find(".condition_text,input.text").show();
                        $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_text").val()
                    }
                }
            }
        }
        $(this).parent().data("rule_condition").element_name = a
    });
    $("#ls_box_field_rules").delegate("select.condition_text,select.condition_number,select.condition_date,select.condition_checkbox", "change", function (a) {
        $(this).parent().data("rule_condition").condition = $(this).val()
    });
//Start OTB Africa Patch - Form Logic for field dependency
    $("#ls_box_dependency_rules").delegate("select.condition_text,select.condition_number,select.condition_date,select.condition_checkbox", "change", function (a) {
        $(this).parent().data("rule_condition").condition = $(this).val()
    });
//End OTB Africa Patch - Form Logic for field dependency
    $("#ls_box_workflow_rules").delegate("select.condition_text,select.condition_number,select.condition_date,select.condition_checkbox", "change", function (a) {
        $(this).parent().data("rule_condition").condition = $(this).val()
    });
    $("#ls_box_field_rules").delegate("input.condition_keyword", "keyup mouseout change", function (a) {
        $(this).parent().data("rule_condition").keyword = $(this).val()
    });
//Start OTB Africa Patch - Form Logic for field dependency
    $("#ls_box_dependency_rules").delegate("input.condition_keyword", "keyup mouseout change", function (a) {
        $(this).parent().data("rule_condition").keyword = $(this).val()
    });
//End OTB Africa Patch - Form Logic for field dependency
    $("#ls_box_workflow_rules").delegate("input.condition_keyword", "keyup mouseout change", function (a) {
        $(this).parent().data("rule_condition").keyword = $(this).val()
    });
    $("#ls_box_permission_rules").delegate("input.condition_keyword", "keyup mouseout change", function (a) {
        $(this).parent().data("rule_condition").keyword = $(this).val()
    });
    $("#ls_box_field_rules").delegate("input.conditiontime_input,select.conditiontime_input", "keyup mouseout change", function (g) {
        var b = $(this).attr("id").split("_");
        var c = parseInt($("#conditiontimehour_" + b[1] + "_" + b[2]).val());
        var d = parseInt($("#conditiontimeminute_" + b[1] + "_" + b[2]).val());
        var a = parseInt($("#conditiontimesecond_" + b[1] + "_" + b[2]).val());
        var f = $("#conditiontimeampm_" + b[1] + "_" + b[2]).val();
        $("#lifieldrule_" + b[1] + "_" + b[2]).data("rule_condition").keyword = c.toString() + ":" + d.toString() + ":" + a.toString() + ":" + f
    });
//Start OTB Africa Patch - Form Logic for field dependency
    $("#ls_box_dependency_rules").delegate("input.conditiontime_input,select.conditiontime_input", "keyup mouseout change", function (g) {
        var b = $(this).attr("id").split("_");
        var c = parseInt($("#conditiontimehour_" + b[1] + "_" + b[2]).val());
        var d = parseInt($("#conditiontimeminute_" + b[1] + "_" + b[2]).val());
        var a = parseInt($("#conditiontimesecond_" + b[1] + "_" + b[2]).val());
        var f = $("#conditiontimeampm_" + b[1] + "_" + b[2]).val();
        $("#lifieldrule_" + b[1] + "_" + b[2]).data("rule_condition").keyword = c.toString() + ":" + d.toString() + ":" + a.toString() + ":" + f
    });
//End OTB Africa Patch - Form Logic for field dependency
    $("#ls_box_workflow_rules").delegate("input.conditiontime_input,select.conditiontime_input", "keyup mouseout change", function (g) {
        var b = $(this).attr("id").split("_");
        var c = parseInt($("#conditiontimehour_" + b[1] + "_" + b[2]).val());
        var d = parseInt($("#conditiontimeminute_" + b[1] + "_" + b[2]).val());
        var a = parseInt($("#conditiontimesecond_" + b[1] + "_" + b[2]).val());
        var f = $("#conditiontimeampm_" + b[1] + "_" + b[2]).val();
        $("#lifieldrule_" + b[1] + "_" + b[2]).data("rule_condition").keyword = c.toString() + ":" + d.toString() + ":" + a.toString() + ":" + f
    });
    $("#ls_box_permission_rules").delegate("input.conditiontime_input,select.conditiontime_input", "keyup mouseout change", function (g) {
        var b = $(this).attr("id").split("_");
        var c = parseInt($("#conditiontimehour_" + b[1] + "_" + b[2]).val());
        var d = parseInt($("#conditiontimeminute_" + b[1] + "_" + b[2]).val());
        var a = parseInt($("#conditiontimesecond_" + b[1] + "_" + b[2]).val());
        var f = $("#conditiontimeampm_" + b[1] + "_" + b[2]).val();
        $("#lifieldrule_" + b[1] + "_" + b[2]).data("rule_condition").keyword = c.toString() + ":" + d.toString() + ":" + a.toString() + ":" + f
    });
    $("#ls_box_field_rules").delegate("a.delete_lifieldrule", "click", function (c) {
        var b = $(this).attr("id").split("_");
        var a = b[1];
        $("#ls_select_field_rule").html($("#ls_select_field_rule_lookup").html());
        $("#lifieldrule_" + a).fadeOut(400, function () {
            $(this).remove();
            $("#ls_field_rules_group > li").each(function () {
                var d = $(this).attr("id").split("_");
                var e = d[1];
                $("#ls_select_field_rule option[value=" + e + "]").remove()
            })
        });
        return false
    });
//Start OTB Africa Patch - Form Logic for field dependency
    $("#ls_box_dependency_rules").delegate("a.delete_lidependencyrule", "click", function (c) {
        var b = $(this).attr("id").split("_");
        var a = b[1];
        var logic_rule_id = b[2];
		console.log("logic_rule_id #### "+logic_rule_id+" and element id is #### "+a);
        $("#ls_select_dependency_rule").html($("#ls_select_dependency_rule_lookup").html());
		$(".lidependencyrule_" + a).each(function () {
			console.log($(this));
			if ($(this).data("rule_properties").rule_temp_id == logic_rule_id){
				$(this).fadeOut(400, function () {
					$(this).remove();
					$("#ls_dependency_rules_group > li").each(function () {
						var d = $(this).attr("id").split("_");
						var e = d[1];
						//$("#ls_select_dependency_rule option[value=" + e + "]").remove()//commented - we do not need to do this
					})
				});
			}
		});
        return false
    });
//End OTB Africa Patch - Form Logic for field dependency
    $("#ls_box_workflow_rules").delegate("a.delete_liworkflowrule", "click", function (c) {
        var b = $(this).attr("id").split("_");
        var a = b[1];
        $("#ls_select_workflow_rule").html($("#ls_select_workflow_rule_lookup").html());
        $("#liworkflowrule_" + a).fadeOut(400, function () {
            $(this).remove();
            $("#ls_workflow_rules_group > li").each(function () {
                var d = $(this).attr("id").split("_");
                var e = d[1];
                $("#ls_select_workflow_rule option[value=" + e + "]").remove()
            })
        });
        return false
    });
    $("#ls_box_permission_rules").delegate("a.delete_lipermissionrule", "click", function (c) {
        var b = $(this).attr("id").split("_");
        var a = b[1];
        $("#ls_select_permission_rule").html($("#ls_select_permission_rule_lookup").html());
        $("#lipermissionrule_" + a).fadeOut(400, function () {
            $(this).remove();
            $("#ls_permission_rules_group > li").each(function () {
                var d = $(this).attr("id").split("_");
                var e = d[1];
                $("#ls_select_permission_rule option[value=" + e + "]").remove()
            })
        });
        return false
    });
    $("#ls_box_field_rules").delegate("a.a_add_condition", "click", function (g) {
        var d = $(this).attr("id").split("_");
        var b = d[1];
        var c = $("#lifieldrule_" + b + " ul > li:not('.ls_add_condition')").length + 1;
        var a = c - 1;
        var f = $("#lifieldrule_" + b + " ul > li:not('.ls_add_condition')").last();
        f.clone(false).data("rule_condition", $.extend("{}", f.data("rule_condition"))).find("*[id],*[name]").each(function () {
            var e = $(this).attr("id").split("_");
            $(this).attr("id", e[0] + "_" + e[1] + "_" + c);
            $(this).attr("name", e[0] + "_" + e[1] + "_" + c)
        }).end().attr("id", "lifieldrule_" + b + "_" + c).insertBefore("#lifieldrule_" + b + " li.ls_add_condition").hide().fadeIn();
        $("#conditionfield_" + b + "_" + c).val($("#conditionfield_" + b + "_" + a).val());
        $("#conditiontext_" + b + "_" + c).val($("#conditiontext_" + b + "_" + a).val());
        $("#conditionnumber_" + b + "_" + c).val($("#conditionnumber_" + b + "_" + a).val());
        $("#conditiondate_" + b + "_" + c).val($("#conditiondate_" + b + "_" + a).val());
        $("#conditioncheckbox_" + b + "_" + c).val($("#conditioncheckbox_" + b + "_" + a).val());
        $("#conditionkeyword_" + b + "_" + c).val("");
        $("#lifieldrule_" + b + "_" + c).data("rule_condition").keyword = "";
        $("#datepicker_" + b + "_" + c).next().next().remove();
        $("#datepicker_" + b + "_" + c).next().remove();
        $("#datepicker_" + b + "_" + c).remove();
        var h = ' <input type="hidden" value="" name="datepicker_' + b + "_" + c + '" id="datepicker_' + b + "_" + c + '"> <span style="display:none"> <img id="datepickimg_' + b + "_" + c + '" alt="Pick date." src="/form_builder/images/icons/calendar.png" class="trigger condition_date_trigger" style="vertical-align: top; cursor: pointer" /></span>';
        $("#conditionkeyword_" + b + "_" + c).after(h);
        $("#datepicker_" + b + "_" + c).datepick({
            onSelect: select_date,
            showTrigger: "#datepickimg_" + b + "_" + c
        });
        return false
    });
//Start OTB Africa Patch - Form Logic for field dependency
    $("#ls_box_dependency_rules").delegate("a.a_add_condition", "click", function (g) {
        var d = $(this).attr("id").split("_");
        var b = d[1];
		var rule_temp_id = $(this).parent().parent().attr("id");
        var c = $("#lidependencyrule_" + b +  "_rule_" + rule_temp_id + " ul > li:not('.ls_add_condition')").length + 1;
        var a = c - 1;
        var f = $("#lidependencyrule_" + b +  "_rule_" + rule_temp_id + " ul > li:not('.ls_add_condition')").last();
        f.clone(false).data("rule_condition", $.extend("{}", f.data("rule_condition"))).find("*[id],*[name]").each(function () {
            var e = $(this).attr("id").split("_");
            $(this).attr("id", e[0] + "_" + e[1] + "_" + c);
            $(this).attr("name", e[0] + "_" + e[1] + "_" + c)
        }).end().attr("id", "lidependencyrule_" + b + "_" + c + "_rule_" + rule_temp_id).insertBefore("#lidependencyrule_" + b +  "_rule_" + rule_temp_id + " li.ls_add_condition").hide().fadeIn();
        $("#conditionfield_" + b + "_" + c).val($("#conditionfield_" + b + "_" + a).val());
        $("#conditiontext_" + b + "_" + c).val($("#conditiontext_" + b + "_" + a).val());
        $("#conditionnumber_" + b + "_" + c).val($("#conditionnumber_" + b + "_" + a).val());
        $("#conditiondate_" + b + "_" + c).val($("#conditiondate_" + b + "_" + a).val());
        $("#conditioncheckbox_" + b + "_" + c).val($("#conditioncheckbox_" + b + "_" + a).val());
        $("#conditionkeyword_" + b + "_" + c).val("");
        $("#lidependencyrule_" + b + "_" + c + "_rule_" + rule_temp_id).data("rule_condition").keyword = "";
        $("#datepicker_" + b + "_" + c).next().next().remove();
        $("#datepicker_" + b + "_" + c).next().remove();
        $("#datepicker_" + b + "_" + c).remove();
        var h = ' <input type="hidden" value="" name="datepicker_' + b + "_" + c + '" id="datepicker_' + b + "_" + c + '"> <span style="display:none"> <img id="datepickimg_' + b + "_" + c + '" alt="Pick date." src="/form_builder/images/icons/calendar.png" class="trigger condition_date_trigger" style="vertical-align: top; cursor: pointer" /></span>';
        $("#conditionkeyword_" + b + "_" + c).after(h);
        $("#datepicker_" + b + "_" + c).datepick({
            onSelect: select_date,
            showTrigger: "#datepickimg_" + b + "_" + c
        });
        return false
    });
//End OTB Africa Patch - Form Logic for field dependency
    $("#ls_box_workflow_rules").delegate("a.a_add_condition", "click", function (g) {
        var d = $(this).attr("id").split("_");
        var b = d[1];
        var c = $("#liworkflowrule_" + b + " ul > li:not('.ls_add_condition')").length + 1;
        var a = c - 1;
        var f = $("#liworkflowrule_" + b + " ul > li:not('.ls_add_condition')").last();
        f.clone(false).data("rule_condition", $.extend("{}", f.data("rule_condition"))).find("*[id],*[name]").each(function () {
            var e = $(this).attr("id").split("_");
            $(this).attr("id", e[0] + "_" + e[1] + "_" + c);
            $(this).attr("name", e[0] + "_" + e[1] + "_" + c)
        }).end().attr("id", "liworkflowrule_" + b + "_" + c).insertBefore("#liworkflowrule_" + b + " li.ls_add_condition").hide().fadeIn();
        $("#conditionfield_" + b + "_" + c).val($("#conditionfield_" + b + "_" + a).val());
        $("#conditiontext_" + b + "_" + c).val($("#conditiontext_" + b + "_" + a).val());
        $("#conditionnumber_" + b + "_" + c).val($("#conditionnumber_" + b + "_" + a).val());
        $("#conditiondate_" + b + "_" + c).val($("#conditiondate_" + b + "_" + a).val());
        $("#conditioncheckbox_" + b + "_" + c).val($("#conditioncheckbox_" + b + "_" + a).val());
        $("#conditionkeyword_" + b + "_" + c).val("");
        $("#lifieldrule_" + b + "_" + c).data("rule_condition").keyword = "";
        $("#datepicker_" + b + "_" + c).next().next().remove();
        $("#datepicker_" + b + "_" + c).next().remove();
        $("#datepicker_" + b + "_" + c).remove();
        var h = ' <input type="hidden" value="" name="datepicker_' + b + "_" + c + '" id="datepicker_' + b + "_" + c + '"> <span style="display:none"> <img id="datepickimg_' + b + "_" + c + '" alt="Pick date." src="/form_builder/images/icons/calendar.png" class="trigger condition_date_trigger" style="vertical-align: top; cursor: pointer" /></span>';
        $("#conditionkeyword_" + b + "_" + c).after(h);
        $("#datepicker_" + b + "_" + c).datepick({
            onSelect: select_date,
            showTrigger: "#datepickimg_" + b + "_" + c
        });
        return false
    });
    $("#ls_box_permission_rules").delegate("a.a_add_condition", "click", function (g) {
        var d = $(this).attr("id").split("_");
        var b = d[1];
        var c = $("#lipermissionrule_" + b + " ul > li:not('.ls_add_condition')").length + 1;
        var a = c - 1;
        var f = $("#lipermissionrule_" + b + " ul > li:not('.ls_add_condition')").last();
        f.clone(false).data("rule_condition", $.extend("{}", f.data("rule_condition"))).find("*[id],*[name]").each(function () {
            var e = $(this).attr("id").split("_");
            $(this).attr("id", e[0] + "_" + e[1] + "_" + c);
            $(this).attr("name", e[0] + "_" + e[1] + "_" + c)
        }).end().attr("id", "lipermissionrule_" + b + "_" + c).insertBefore("#lipermissionrule_" + b + " li.ls_add_condition").hide().fadeIn();
        $("#conditionfield_" + b + "_" + c).val($("#conditionfield_" + b + "_" + a).val());
        $("#conditiontext_" + b + "_" + c).val($("#conditiontext_" + b + "_" + a).val());
        $("#conditionnumber_" + b + "_" + c).val($("#conditionnumber_" + b + "_" + a).val());
        $("#conditiondate_" + b + "_" + c).val($("#conditiondate_" + b + "_" + a).val());
        $("#conditioncheckbox_" + b + "_" + c).val($("#conditioncheckbox_" + b + "_" + a).val());
        $("#conditionkeyword_" + b + "_" + c).val("");
        $("#lifieldrule_" + b + "_" + c).data("rule_condition").keyword = "";
        $("#datepicker_" + b + "_" + c).next().next().remove();
        $("#datepicker_" + b + "_" + c).next().remove();
        $("#datepicker_" + b + "_" + c).remove();
        var h = ' <input type="hidden" value="" name="datepicker_' + b + "_" + c + '" id="datepicker_' + b + "_" + c + '"> <span style="display:none"> <img id="datepickimg_' + b + "_" + c + '" alt="Pick date." src="/form_builder/images/icons/calendar.png" class="trigger condition_date_trigger" style="vertical-align: top; cursor: pointer" /></span>';
        $("#conditionkeyword_" + b + "_" + c).after(h);
        $("#datepicker_" + b + "_" + c).datepick({
            onSelect: select_date,
            showTrigger: "#datepickimg_" + b + "_" + c
        });
        return false
    });
    $("#ls_box_field_rules").delegate("a.a_delete_condition", "click", function (c) {
        var b = $(this).attr("id").split("_");
        var a = b[1];
        if ($("#lifieldrule_" + a + " ul > li:not('.ls_add_condition')").length <= 1) {
            $("#ui-dialog-title-dialog-warning").html("Unable to delete!");
            $("#dialog-warning-msg").html("You can't delete all condition. <br />You must have at least one condition.");
            $("#dialog-warning").dialog("open")
        } else {
            $(this).parent().fadeOut(function () {
                $(this).remove()
            })
        }
        return false
    });
//Start OTB Africa Patch - Form Logic for field dependency
    $("#ls_box_dependency_rules").delegate("a.a_delete_condition", "click", function (c) {
        var b = $(this).attr("id").split("_");
        var a = b[1];
        var rule_temp_id = b[3];
        if ($("#lidependencyrule_" + a +  "_rule_" + rule_temp_id + " ul > li:not('.ls_add_condition')").length <= 1) {
            $("#ui-dialog-title-dialog-warning").html("Unable to delete!");
            $("#dialog-warning-msg").html("You can't delete all condition. <br />You must have at least one condition.");
            $("#dialog-warning").dialog("open")
        } else {
            $(this).parent().fadeOut(function () {
                $(this).remove()
            })
        }
        return false
    });
//End OTB Africa Patch - Form Logic for field dependency
    $("#ls_box_workflow_rules").delegate("a.a_delete_condition", "click", function (c) {
        var b = $(this).attr("id").split("_");
        var a = b[1];
        if ($("#liworkflowrule_" + a + " ul > li:not('.ls_add_condition')").length <= 1) {
            $("#ui-dialog-title-dialog-warning").html("Unable to delete!");
            $("#dialog-warning-msg").html("You can't delete all condition. <br />You must have at least one condition.");
            $("#dialog-warning").dialog("open")
        } else {
            $(this).parent().fadeOut(function () {
                $(this).remove()
            })
        }
        return false
    });
    $("#ls_box_permission_rules").delegate("a.a_delete_condition", "click", function (c) {
        var b = $(this).attr("id").split("_");
        var a = b[1];
        if ($("#lipermissionrule_" + a + " ul > li:not('.ls_add_condition')").length <= 1) {
            $("#ui-dialog-title-dialog-warning").html("Unable to delete!");
            $("#dialog-warning-msg").html("You can't delete all condition. <br />You must have at least one condition.");
            $("#dialog-warning").dialog("open")
        } else {
            $(this).parent().fadeOut(function () {
                $(this).remove()
            })
        }
        return false
    });
    $("#button_save_logics").click(function () {
        if ($("#button_save_logics").text() != "Saving...") {
            $("#button_save_logics").prop("disabled", true);
            $("#button_save_logics").text("Saving...");
            $("#button_save_logics").after("<div class='small_loader_box' style='float: right'><img src='images/loader_small_grey.gif' /></div>");
            var f = $("#ls_field_rules_group > li");
            var h = new Array();
            if (f.length >= 1) {
                f.each(function (m) {
                    h[m] = $(this).data("rule_properties")
                })
            }
            var k = $("#ls_field_rules_group ul.ls_field_rules_conditions > li:not('.ls_add_condition')");
            var e = new Array();
            if (k.length >= 1) {
                k.each(function (m) {
                    e[m] = $(this).data("rule_condition")
                })
            }
//Start OTB Africa Patch - Form Logic for field dependency
            var fd = $("#ls_dependency_rules_group > li");
            var hd = new Array();
            if (fd.length >= 1) {
                fd.each(function (m) {
                    hd[m] = $(this).data("rule_properties")
                })
            }
            var kd = $("#ls_dependency_rules_group ul.ls_dependency_rules_conditions > li:not('.ls_add_condition')");
            var ed = new Array();
            if (kd.length >= 1) {
                kd.each(function (m) {
                    ed[m] = $(this).data("rule_condition")
                })
            }
			console.log("rule properties");
			console.log(hd);
			console.log("rule conditions");
			console.log(ed);
//End OTB Africa Patch - Form Logic for field dependency
            var fw = $("#ls_workflow_rules_group > li");
            var hw = new Array();
            if (fw.length >= 1) {
                fw.each(function (m) {
                    hw[m] = $(this).data("rule_properties")
                })
            }
            var kw = $("#ls_workflow_rules_group ul.ls_workflow_rules_conditions > li:not('.ls_add_condition')");
            var ew = new Array();
            if (kw.length >= 1) {
                kw.each(function (m) {
                    ew[m] = $(this).data("rule_condition")
                })
            }
            var fp = $("#ls_permission_rules_group > li");
            var hp = new Array();
            if (fp.length >= 1) {
                fp.each(function (m) {
                    hp[m] = $(this).data("rule_properties")
                })
            }
            var kp = $("#ls_permission_rules_group ul.ls_permission_rules_conditions > li:not('.ls_add_condition')");
            var ep = new Array();
            if (kp.length >= 1) {
                kp.each(function (m) {
                    ep[m] = $(this).data("rule_condition")
                })
            }
            var b = $("#ls_page_rules_group > li");
            var i = new Array();
            if (b.length >= 1) {
                b.each(function (m) {
                    i[m] = $(this).data("rule_properties")
                })
            }
            var c = $("#ls_page_rules_group ul.ls_page_rules_conditions > li:not('.ls_add_condition')");
            var g = new Array();
            if (c.length >= 1) {
                c.each(function (m) {
                    g[m] = $(this).data("rule_condition")
                })
            }
            var l = $("#ls_email_rules_group > li");
            var d = new Array();
            if (l.length >= 1) {
                l.each(function (m) {
                    d[m] = $(this).data("rule_properties")
                })
            }
            var a = $("#ls_email_rules_group ul.ls_email_rules_conditions > li:not('.ls_add_condition')");
            var j = new Array();
            if (a.length >= 1) {
                a.each(function (m) {
                    j[m] = $(this).data("rule_condition")
                })
            }
            $.ajax({
                type: "POST",
                async: true,
                url: "/backend.php/forms/savelogicsettings",
                data: {
                    form_id: $("#form_id").val(),
                    logic_status: $(".logic_settings").data("logic_status"),
                    field_rule_properties: h,
                    field_rule_conditions: e,
                    workflow_rule_properties: hw,
                    workflow_rule_conditions: ew,
                    permission_rule_properties: hp,
                    permission_rule_conditions: ep,
                    page_rule_properties: i,
                    page_rule_conditions: g,
                    email_rule_properties: d,
                    email_rule_conditions: j,
//Start OTB Africa Patch - Form Logic for field dependency
                    dependency_rule_properties: hd,
                    dependency_rule_conditions: ed
//End OTB Africa Patch - Form Logic for field dependency
                },
                cache: false,
                global: false,
                dataType: "json",
                error: function (o, m, n) {
                    alert("Error! Unable to save logic settings. Please try again.")
                },
                success: function (m) {
                    if (m.status == "ok") {
                        window.location.replace("/backend.php/forms/index")
                    } else {
                        alert("Error! Unable to save logic settings. Please try again.")
                    }
                }
            })
        }
        return false
    });
    $("#ls_box_field_rules .rule_datepicker").each(function (c) {
        var b = $(this).attr("id").split("_");
        var a = b[1] + "_" + b[2];
        $("#datepicker_" + a).datepick({
            onSelect: select_date,
            showTrigger: "#datepickimg_" + a
        })
    });
//Start OTB Africa Patch - Form Logic for field dependency
    $("#ls_box_dependency_rules .rule_datepicker").each(function (c) {
        var b = $(this).attr("id").split("_");
        var a = b[1] + "_" + b[2];
        $("#datepicker_" + a).datepick({
            onSelect: select_date,
            showTrigger: "#datepickimg_" + a
        })
    });
//End OTB Africa Patch - Form Logic for field dependency
    $("#ls_box_page_rules .rule_datepicker").each(function (c) {
        var b = $(this).attr("id").split("_");
        var a = b[1] + "_" + b[2];
        $("#datepicker_" + a).datepick({
            onSelect: select_date_page,
            showTrigger: "#datepickimg_" + a
        })
    });
    $("#ls_box_email_rules .rule_datepicker").each(function (c) {
        var b = $(this).attr("id").split("_");
        var a = b[1] + "_" + b[2];
        $("#datepicker_" + a).datepick({
            onSelect: select_date_email,
            showTrigger: "#datepickimg_" + a
        })
    });
    $("#logic_page_enable").click(function () {
        if ($(this).prop("checked") == true) {
            $("#ls_box_page_rules .ls_box_content").slideDown();
            $(".logic_settings").data("logic_status").logic_page_enable = 1
        } else {
            $("#ls_box_page_rules .ls_box_content").slideUp();
            $(".logic_settings").data("logic_status").logic_page_enable = 0
        }
    });

    $("#logic_workflow_enable").click(function () {
        if ($(this).prop("checked") == true) {
            $("#ls_box_workflow_rules .ls_box_content").slideDown();
            $(".logic_settings").data("logic_status").logic_workflow_enable = 1
        } else {
            $("#ls_box_workflow_rules .ls_box_content").slideUp();
            $(".logic_settings").data("logic_status").logic_workflow_enable = 0
        }
    });

    $("#logic_permission_enable").click(function () {
        if ($(this).prop("checked") == true) {
            $("#ls_box_permission_rules .ls_box_content").slideDown();
            $(".logic_settings").data("logic_status").logic_permission_enable = 1
        } else {
            $("#ls_box_permission_rules .ls_box_content").slideUp();
            $(".logic_settings").data("logic_status").logic_permission_enable = 0
        }
    });

    $("#ls_select_page_rule").bind("change", function () {
        if ($(this).val() == "") {
            return true
        }
        var a = "page" + $(this).val();
        var c = "";
        var g = "";
        g = $("#ls_fields_lookup").clone(false).attr("id", "conditionpage_" + a + "_1").attr("name", "conditionpage_" + a + "_1").show().outerHTML();
        c += '<li id="lipagerule_' + a + '" style="display: none"><table width="100%" cellspacing="0"><thead><tr><td><strong>' + $(this).find("option:selected").text() + '</strong><a class="delete_lipagerule" id="deletelipagerule_' + a + '" href="#"><img src="/form_builder/images/icons/52_red_16.png"></a></td></tr></thead><tbody><tr><td><h6><img src="/form_builder/images/icons/arrow_right_red.png" style="vertical-align: top" /> Go to this page if <select style="margin-left: 5px;margin-right: 5px" name="pageruleallany_' + a + '" id="pageruleallany_' + a + '" class="element select rule_all_any"><option value="all">all</option><option value="any">any</option></select> of the following conditions match:</h6><ul class="ls_page_rules_conditions"><li id="lipagerule_' + a + '_1"> ' + g + ' <select name="conditiontext_' + a + '_1" id="conditiontext_' + a + '_1" class="element select condition_text" style="width: 120px;display: none"><option value="is">Is</option><option value="is_not">Is Not</option><option value="begins_with">Begins with</option><option value="ends_with">Ends with</option><option value="contains">Contains</option><option value="not_contain">Does not contain</option></select> <select id="conditionnumber_' + a + '_1" name="conditionnumber_' + a + '_1" style="width: 120px;display: none" class="element select condition_number"><option value="is" selected="selected">Is</option><option value="less_than">Less than</option><option value="greater_than">Greater than</option></select> <select id="conditiondate_' + a + '_1" name="conditiondate_' + a + '_1" style="width: 120px;display: none" class="element select condition_date"><option value="is" selected="selected">Is</option><option value="is_before">Is Before</option><option value="is_after">Is After</option></select> <select id="conditioncheckbox_' + a + '_1" name="conditioncheckbox_' + a + '_1" style="width: 120px;display: none" class="element select condition_checkbox"><option value="is_one">Is Checked</option><option value="is_zero">Is Empty</option></select> \n<span name="conditiontime_' + a + '_1" id="conditiontime_' + a + '_1" class="condition_time" style="display: none"><input name="conditiontimehour_' + a + '_1" id="conditiontimehour_' + a + '_1" type="text" class="element text conditiontime_input" maxlength="2" size="2" value="" placeholder="HH"> : <input name="conditiontimeminute_' + a + '_1" id="conditiontimeminute_' + a + '_1" type="text" class="element text conditiontime_input" maxlength="2" size="2" value="" placeholder="MM">  <span class="conditiontime_second" style="display:none"> : <input name="conditiontimesecond_' + a + '_1" id="conditiontimesecond_' + a + '_1" type="text" class="element text conditiontime_input" maxlength="2" size="2" value="" placeholder="SS"> </span><select class="element select conditiontime_ampm conditiontime_input" name="conditiontimeampm_' + a + '_1" id="conditiontimeampm_' + a + '_1" style="display:none"><option selected="selected" value="AM">AM</option><option value="PM">PM</option></select></span><input type="text" class="element text condition_keyword" value="" id="conditionkeyword_' + a + '_1" name="conditionkeyword_' + a + '_1" style="display: none"> \n<a href="#" id="deletecondition_' + a + '_1" name="deletecondition_' + a + '_1" class="a_delete_condition"><img src="/form_builder/images/icons/51_red_16.png" /></a>\n</li><li class="ls_add_condition"><a href="#" id="addcondition_' + a + '" class="a_add_condition"><img src="/form_builder/images/icons/49_red_16.png" /></a></li></ul></td></tr></tbody></table></li>';
        $("#ls_page_rules_group").prepend(c);
        $("#lipagerule_" + a).hide();
        $("#conditionpage_" + a + "_1 option[value=element_" + a + "]").remove();
        $("#conditionpage_" + a + "_1 option[value^=element_" + a + "_]").remove();
        var d = $("#conditionpage_" + a + "_1").eq(0).val();
        var e = $("#ls_fields_lookup").data(d);
        var b = "is";
        if (e == "money" || e == "number") {
            $("#conditionnumber_" + a + "_1").show();
            $("#conditionkeyword_" + a + "_1").show()
        } else {
            if (e == "date" || e == "europe_date") {
                $("#conditiondate_" + a + "_1").show();
                $("#conditionkeyword_" + a + "_1").show();
                $("#lipagerule_" + a + "_1").addClass("condition_date")
            } else {
                if (e == "time" || e == "time_showsecond" || e == "time_24hour" || e == "time_showsecond24hour") {
                    $("#conditiondate_" + a + "_1").show();
                    $("#conditiontime_" + a + "_1").show();
                    if (e == "time") {
                        $("#conditiontimeampm_" + a + "_1").show()
                    } else {
                        if (e == "time_showsecond") {
                            $("#conditiontimeampm_" + a + "_1").show();
                            $("#conditiontimesecond_" + a + "_1").parent().show()
                        } else {
                            if (e == "time_showsecond24hour") {
                                $("#conditiontimesecond_" + a + "_1").parent().show()
                            }
                        }
                    }
                } else {
                    if (e == "checkbox") {
                        $("#conditioncheckbox_" + a + "_1").show();
                        b = "is_one"
                    } else {
                        $("#conditiontext_" + a + "_1").show();
                        $("#conditionkeyword_" + a + "_1").show()
                    }
                }
            }
        }
        var f = ' <input type="hidden" value="" name="datepicker_' + a + '_1" id="datepicker_' + a + '_1">\n <span style="display:none"><img id="datepickimg_' + a + '_1" alt="Pick date." src="/form_builder/images/icons/calendar.png" class="trigger condition_date_trigger" style="vertical-align: top; cursor: pointer" /></span>';
        $("#conditionkeyword_" + a + "_1").after(f);
        $("#datepicker_" + a + "_1").datepick({
            onSelect: select_date_page,
            showTrigger: "#datepickimg_" + a + "_1"
        });
        $("#lipagerule_" + a).slideDown();
        $("#lipagerule_" + a).data("rule_properties", {
            page_id: a,
            rule_all_any: "all"
        });
        $("#lipagerule_" + a + "_1").data("rule_condition", {
            target_page_id: a,
            element_name: d,
            condition: b,
            keyword: ""
        });
        $(this).find("option:selected").remove();
        if ($("#ls_select_page_rule option").length == 1) {
            $("#ls_select_page_rule option").text("No More Page Available")
        }
    });
    $("#ls_box_page_rules").delegate("select.rule_all_any", "change", function (b) {
        var a = $(this).attr("id").split("_");
        $("#lipagerule_" + a[1]).data("rule_properties").rule_all_any = $(this).val()
    });
    $("#ls_box_page_rules").delegate("select.condition_fieldname", "change", function (c) {
        var a = $(this).val();
        var b = $("#ls_fields_lookup").data(a);
        $(this).parent().find(".condition_text,.condition_time,.condition_number,.condition_date,.condition_checkbox,.condition_keyword").hide();
        $(this).parent().removeClass("condition_date");
        if (b == "money" || b == "number") {
            $(this).parent().find(".condition_number,input.text").show();
            $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_number").val()
        } else {
            if (b == "date" || b == "europe_date") {
                $(this).parent().addClass("condition_date");
                $(this).parent().find(".condition_date,input.text").show();
                $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_date").val()
            } else {
                if (b == "time" || b == "time_showsecond" || b == "time_24hour" || b == "time_showsecond24hour") {
                    $(this).parent().find(".condition_date,.condition_time").show();
                    $(this).parent().find(".condition_time .conditiontime_second,.condition_time .conditiontime_ampm").hide();
                    if (b == "time") {
                        $(this).parent().find(".condition_time .conditiontime_ampm").show()
                    } else {
                        if (b == "time_showsecond") {
                            $(this).parent().find(".condition_time .conditiontime_ampm,.condition_time .conditiontime_second").show()
                        } else {
                            if (b == "time_showsecond24hour") {
                                $(this).parent().find(".condition_time .conditiontime_second").show()
                            }
                        }
                    }
                    $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_date").val()
                } else {
                    if (b == "checkbox") {
                        $(this).parent().find(".condition_checkbox").show();
                        $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_checkbox").val()
                    } else {
                        $(this).parent().find(".condition_text,input.text").show();
                        $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_text").val()
                    }
                }
            }
        }
        $(this).parent().data("rule_condition").element_name = a
    });
    $("#ls_box_page_rules").delegate("select.condition_text,select.condition_number,select.condition_date,select.condition_checkbox", "change", function (a) {
        $(this).parent().data("rule_condition").condition = $(this).val()
    });
    $("#ls_box_page_rules").delegate("input.condition_keyword", "keyup mouseout change", function (a) {
        $(this).parent().data("rule_condition").keyword = $(this).val()
    });
    $("#ls_box_page_rules").delegate("input.conditiontime_input,select.conditiontime_input", "keyup mouseout change", function (g) {
        var b = $(this).attr("id").split("_");
        var c = parseInt($("#conditiontimehour_" + b[1] + "_" + b[2]).val());
        var d = parseInt($("#conditiontimeminute_" + b[1] + "_" + b[2]).val());
        var a = parseInt($("#conditiontimesecond_" + b[1] + "_" + b[2]).val());
        var f = $("#conditiontimeampm_" + b[1] + "_" + b[2]).val();
        $("#lipagerule_" + b[1] + "_" + b[2]).data("rule_condition").keyword = c.toString() + ":" + d.toString() + ":" + a.toString() + ":" + f
    });
    $("#ls_box_page_rules").delegate("a.delete_lipagerule", "click", function (c) {
        var b = $(this).attr("id").split("_");
        var a = b[1];
        $("#ls_select_page_rule").html($("#ls_select_page_rule_lookup").html());
        $("#lipagerule_" + a).fadeOut(400, function () {
            $(this).remove();
            $("#ls_page_rules_group > li").each(function () {
                var d = $(this).attr("id").split("_");
                var e = d[1];
                e = e.substring(4);
                $("#ls_select_page_rule option[value=" + e + "]").remove()
            })
        });
        return false
    });
    $("#ls_box_page_rules").delegate("a.a_add_condition", "click", function (g) {
        var d = $(this).attr("id").split("_");
        var b = d[1];
        var c = $("#lipagerule_" + b + " ul > li:not('.ls_add_condition')").length + 1;
        var a = c - 1;
        var f = $("#lipagerule_" + b + " ul > li:not('.ls_add_condition')").last();
        f.clone(false).data("rule_condition", $.extend("{}", f.data("rule_condition"))).find("*[id],*[name]").each(function () {
            var e = $(this).attr("id").split("_");
            $(this).attr("id", e[0] + "_" + e[1] + "_" + c);
            $(this).attr("name", e[0] + "_" + e[1] + "_" + c)
        }).end().attr("id", "lipagerule_" + b + "_" + c).insertBefore("#lipagerule_" + b + " li.ls_add_condition").hide().fadeIn();
        $("#conditionpage_" + b + "_" + c).val($("#conditionpage_" + b + "_" + a).val());
        $("#conditiontext_" + b + "_" + c).val($("#conditiontext_" + b + "_" + a).val());
        $("#conditionnumber_" + b + "_" + c).val($("#conditionnumber_" + b + "_" + a).val());
        $("#conditiondate_" + b + "_" + c).val($("#conditiondate_" + b + "_" + a).val());
        $("#conditioncheckbox_" + b + "_" + c).val($("#conditioncheckbox_" + b + "_" + a).val());
        $("#conditionkeyword_" + b + "_" + c).val("");
        $("#lipagerule_" + b + "_" + c).data("rule_condition").keyword = "";
        $("#datepicker_" + b + "_" + c).next().next().remove();
        $("#datepicker_" + b + "_" + c).next().remove();
        $("#datepicker_" + b + "_" + c).remove();
        var h = ' <input type="hidden" value="" name="datepicker_' + b + "_" + c + '" id="datepicker_' + b + "_" + c + '"> <span style="display:none"> <img id="datepickimg_' + b + "_" + c + '" alt="Pick date." src="/form_builder/images/icons/calendar.png" class="trigger condition_date_trigger" style="vertical-align: top; cursor: pointer" /></span>';
        $("#conditionkeyword_" + b + "_" + c).after(h);
        $("#datepicker_" + b + "_" + c).datepick({
            onSelect: select_date_page,
            showTrigger: "#datepickimg_" + b + "_" + c
        });
        return false
    });
    $("#ls_box_page_rules").delegate("a.a_delete_condition", "click", function (c) {
        var b = $(this).attr("id").split("_");
        var a = b[1];
        if ($("#lipagerule_" + a + " ul > li:not('.ls_add_condition')").length <= 1) {
            $("#ui-dialog-title-dialog-warning").html("Unable to delete!");
            $("#dialog-warning-msg").html("You can't delete all condition. <br />You must have at least one condition.");
            $("#dialog-warning").dialog("open")
        } else {
            $(this).parent().fadeOut(function () {
                $(this).remove()
            })
        }
        return false
    });
    $("#logic_email_enable").click(function () {
        if ($(this).prop("checked") == true) {
            $("#ls_box_email_rules .ls_box_content").slideDown();
            $(".logic_settings").data("logic_status").logic_email_enable = 1
        } else {
            $("#ls_box_email_rules .ls_box_content").slideUp();
            $(".logic_settings").data("logic_status").logic_email_enable = 0
        }
    });
    $("#ls_box_email_rules").delegate("select.rule_all_any", "change", function (b) {
        var a = $(this).attr("id").split("_");
        $("#liemailrule_" + a[1]).data("rule_properties").rule_all_any = $(this).val()
    });
    $("#ls_box_email_rules").delegate("select.condition_fieldname", "change", function (c) {
        var a = $(this).val();
        var b = $("#ls_fields_lookup").data(a);
        $(this).parent().find(".condition_text,.condition_time,.condition_number,.condition_date,.condition_checkbox,.condition_keyword").hide();
        $(this).parent().removeClass("condition_date");
        if (b == "money" || b == "number") {
            $(this).parent().find(".condition_number,input.text").show();
            $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_number").val()
        } else {
            if (b == "date" || b == "europe_date") {
                $(this).parent().addClass("condition_date");
                $(this).parent().find(".condition_date,input.text").show();
                $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_date").val()
            } else {
                if (b == "time" || b == "time_showsecond" || b == "time_24hour" || b == "time_showsecond24hour") {
                    $(this).parent().find(".condition_date,.condition_time").show();
                    $(this).parent().find(".condition_time .conditiontime_second,.condition_time .conditiontime_ampm").hide();
                    if (b == "time") {
                        $(this).parent().find(".condition_time .conditiontime_ampm").show()
                    } else {
                        if (b == "time_showsecond") {
                            $(this).parent().find(".condition_time .conditiontime_ampm,.condition_time .conditiontime_second").show()
                        } else {
                            if (b == "time_showsecond24hour") {
                                $(this).parent().find(".condition_time .conditiontime_second").show()
                            }
                        }
                    }
                    $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_date").val()
                } else {
                    if (b == "checkbox") {
                        $(this).parent().find(".condition_checkbox").show();
                        $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_checkbox").val()
                    } else {
                        $(this).parent().find(".condition_text,input.text").show();
                        $(this).parent().data("rule_condition").condition = $(this).parent().find(".condition_text").val()
                    }
                }
            }
        }
        $(this).parent().data("rule_condition").element_name = a
    });
    $("#ls_box_email_rules").delegate("select.condition_text,select.condition_number,select.condition_date,select.condition_checkbox", "change", function (a) {
        $(this).parent().data("rule_condition").condition = $(this).val()
    });
    $("#ls_box_email_rules").delegate("input.condition_keyword", "keyup mouseout change", function (a) {
        $(this).parent().data("rule_condition").keyword = $(this).val()
    });
    $("#ls_box_email_rules").delegate("input.conditiontime_input,select.conditiontime_input", "keyup mouseout change", function (g) {
        var b = $(this).attr("id").split("_");
        var c = parseInt($("#conditiontimehour_" + b[1] + "_" + b[2]).val());
        var d = parseInt($("#conditiontimeminute_" + b[1] + "_" + b[2]).val());
        var a = parseInt($("#conditiontimesecond_" + b[1] + "_" + b[2]).val());
        var f = $("#conditiontimeampm_" + b[1] + "_" + b[2]).val();
        $("#liemailrule_" + b[1] + "_" + b[2]).data("rule_condition").keyword = c.toString() + ":" + d.toString() + ":" + a.toString() + ":" + f
    });
    $("#ls_box_email_rules").delegate("a.delete_liemailrule", "click", function (c) {
        var a = $(this).attr("id").split("_");
        var b = a[1];
        $("#liemailrule_" + b).fadeOut(400, function () {
            $(this).remove()
        });
        return false
    });
    $("#ls_box_email_rules").delegate("a.a_add_condition", "click", function (g) {
        var c = $(this).attr("id").split("_");
        var f = c[1];
        var b = $("#liemailrule_" + f + " ul > li:not('.ls_add_condition')").length + 1;
        var a = b - 1;
        var d = $("#liemailrule_" + f + " ul > li:not('.ls_add_condition')").last();
        d.clone(false).data("rule_condition", $.extend("{}", d.data("rule_condition"))).find("*[id],*[name]").each(function () {
            var e = $(this).attr("id").split("_");
            $(this).attr("id", e[0] + "_" + e[1] + "_" + b);
            $(this).attr("name", e[0] + "_" + e[1] + "_" + b)
        }).end().attr("id", "liemailrule_" + f + "_" + b).insertBefore("#liemailrule_" + f + " li.ls_add_condition").hide().fadeIn();
        $("#conditionemail_" + f + "_" + b).val($("#conditionemail_" + f + "_" + a).val());
        $("#conditiontext_" + f + "_" + b).val($("#conditiontext_" + f + "_" + a).val());
        $("#conditionnumber_" + f + "_" + b).val($("#conditionnumber_" + f + "_" + a).val());
        $("#conditiondate_" + f + "_" + b).val($("#conditiondate_" + f + "_" + a).val());
        $("#conditioncheckbox_" + f + "_" + b).val($("#conditioncheckbox_" + f + "_" + a).val());
        $("#conditionkeyword_" + f + "_" + b).val("");
        $("#liemailrule_" + f + "_" + b).data("rule_condition").keyword = "";
        $("#datepicker_" + f + "_" + b).next().next().remove();
        $("#datepicker_" + f + "_" + b).next().remove();
        $("#datepicker_" + f + "_" + b).remove();
        var h = ' <input type="hidden" value="" name="datepicker_' + f + "_" + b + '" id="datepicker_' + f + "_" + b + '"> <span style="display:none"> <img id="datepickimg_' + f + "_" + b + '" alt="Pick date." src="/form_builder/images/icons/calendar.png" class="trigger condition_date_trigger" style="vertical-align: top; cursor: pointer" /></span>';
        $("#conditionkeyword_" + f + "_" + b).after(h);
        $("#datepicker_" + f + "_" + b).datepick({
            onSelect: select_date_email,
            showTrigger: "#datepickimg_" + f + "_" + b
        });
        return false
    });
    $("#ls_box_email_rules").delegate("a.a_delete_condition", "click", function (c) {
        var a = $(this).attr("id").split("_");
        var b = a[1];
        if ($("#liemailrule_" + b + " ul > li:not('.ls_add_condition')").length <= 1) {
            $("#ui-dialog-title-dialog-warning").html("Unable to delete!");
            $("#dialog-warning-msg").html("You can't delete all condition. <br />You must have at least one condition.");
            $("#dialog-warning").dialog("open")
        } else {
            $(this).parent().fadeOut(function () {
                $(this).remove()
            })
        }
        return false
    });
    $("#ls_box_email_rules").delegate("select.target_email_dropdown", "change", function (b) {
        var a = $(this).attr("id").split("_");
        var c = $(this).val();
        if (c == "custom") {
            $("#targetemailcustomspan_" + a[1]).show();
            c = $("#targetemailcustom_" + a[1]).val()
        } else {
            $("#targetemailcustomspan_" + a[1]).hide()
        }
        $("#liemailrule_" + a[1]).data("rule_properties").target_email = c
    });
    $("#ls_box_email_rules").delegate("input.target_email_custom", "keyup mouseout change", function (b) {
        var a = $(this).attr("id").split("_");
        $("#liemailrule_" + a[1]).data("rule_properties").target_email = $(this).val()
    });
    $("#ls_box_email_rules").delegate("select.template_name", "change", function (b) {
        var a = $(this).attr("id").split("_");
        $("#liemailrule_" + a[1]).data("rule_properties").template_name = $(this).val();
        if ($(this).val() == "custom") {
            $("#ls_email_custom_template_div_" + a[1]).fadeIn()
        } else {
            $("#ls_email_custom_template_div_" + a[1]).fadeOut()
        }
    });
    $("#ls_box_email_rules").delegate("select.custom_from_name_dropdown", "change", function (c) {
        var a = $(this).attr("id").split("_");
        var b = $(this).val();
        if (b == "custom") {
            $("#customfromnamespan_" + a[1]).show();
            b = $("#customfromnameuser_" + a[1]).val()
        } else {
            $("#customfromnamespan_" + a[1]).hide()
        }
        $("#liemailrule_" + a[1]).data("rule_properties").custom_from_name = b
    });
    $("#ls_box_email_rules").delegate("input.custom_from_name_text", "keyup mouseout change", function (b) {
        var a = $(this).attr("id").split("_");
        $("#liemailrule_" + a[1]).data("rule_properties").custom_from_name = $(this).val()
    });
    $("#ls_box_email_rules").delegate("select.custom_from_email_dropdown", "change", function (c) {
        var a = $(this).attr("id").split("_");
        var b = $(this).val();
        if (b == "custom") {
            $("#customfromemailspan_" + a[1]).show();
            b = $("#customfromemailuser_" + a[1]).val()
        } else {
            $("#customfromemailspan_" + a[1]).hide()
        }
        $("#liemailrule_" + a[1]).data("rule_properties").custom_from_email = b
    });
    $("#ls_box_email_rules").delegate("input.custom_from_email_text", "keyup mouseout change", function (b) {
        var a = $(this).attr("id").split("_");
        $("#liemailrule_" + a[1]).data("rule_properties").custom_from_email = $(this).val()
    });
    $("#ls_box_email_rules").delegate("input.custom_email_subject", "keyup mouseout change", function (b) {
        var a = $(this).attr("id").split("_");
        $("#liemailrule_" + a[1]).data("rule_properties").custom_subject = $(this).val()
    });
    $("#ls_box_email_rules").delegate("textarea.custom_email_content", "keyup mouseout change", function (b) {
        var a = $(this).attr("id").split("_");
        $("#liemailrule_" + a[1]).data("rule_properties").custom_content = $(this).val()
    });
    $("#ls_box_email_rules").delegate("input.custom_plain_text", "change", function (b) {
        var a = $(this).attr("id").split("_");
        if ($(this).prop("checked") == true) {
            $("#liemailrule_" + a[1]).data("rule_properties").custom_plain_text = 1
        } else {
            $("#liemailrule_" + a[1]).data("rule_properties").custom_plain_text = 0
        }
    });
    $("#ls_add_email_rule").click(function () {
        var a = $("#ls_email_rules_group > li").last();
        var c = a.attr("id");
        last_rule_id_clean = parseInt(c.replace("liemailrule_email", ""));
        var e = last_rule_id_clean + 1;
        a.clone(false).find("*[id],*[name]").each(function () {
            var g = $(this).attr("id");
            var f = g.replace("email" + last_rule_id_clean, "email" + e);
            $(this).attr("id", f);
            $(this).attr("name", f)
        }).end().attr("id", "liemailrule_email" + e).appendTo("#ls_email_rules_group").hide().fadeIn();
        $("#liemailrule_email" + e + " .rule_title").text("Rule #" + e);
        $("#liemailrule_email" + e).data("rule_properties", $.extend("{}", a.data("rule_properties")));
        $("#liemailrule_email" + e).data("rule_properties").rule_id = e;
        $("#liemailrule_email" + e + " .ls_email_rules_conditions > li:not('.ls_add_condition')").each(function () {
            var l = $(this).attr("id");
            var f = l.replace("email" + e, "email" + last_rule_id_clean);
            $(this).data("rule_condition", $.extend("{}", $("#" + f).data("rule_condition")));
            $(this).data("rule_condition").target_rule_id = "email" + e;
            var h = $(this).find(".hasDatepick");
            var j = h.attr("id");
            h.next().next().remove();
            h.next().remove();
            h.remove();
            var g = j.split("_");
            var i = g[2];
            var k = ' <input type="hidden" value="" name="datepicker_email' + e + "_" + i + '" id="datepicker_email' + e + "_" + i + '"> <span style="display:none"> <img id="datepickimg_email' + e + "_" + i + '" alt="Pick date." src="/form_builder/images/icons/calendar.png" class="trigger condition_date_trigger" style="vertical-align: top; cursor: pointer" /></span>';
            $("#conditionkeyword_email" + e + "_" + i).after(k);
            $("#datepicker_email" + e + "_" + i).datepick({
                onSelect: select_date_email,
                showTrigger: "#datepickimg_email" + e + "_" + i
            })
        });
        var d = $("#liemailrule_email" + last_rule_id_clean).find("select");
        var b = $("#liemailrule_email" + e).find("select");
        b.each(function (f, g) {
            $(g).val(d.eq(f).val())
        });
        return false
    });
    $("#dialog-template-variable").dialog({
        modal: true,
        autoOpen: false,
        closeOnEscape: false,
        width: 400,
        position: ["center", 150],
        draggable: false,
        resizable: false,
        buttons: [{
            text: "Close",
            id: "btn-change-theme-ok",
            "class": "bb_button bb_small bb_green",
            click: function () {
                $(this).dialog("close")
            }
        }]
    });
    $("a.tempvar_link").click(function () {
        $("#dialog-template-variable").dialog("open");
        return false
    });
    $("#tempvar_help_trigger a").click(function () {
        if ($(this).text() == "more info") {
            $(this).text("hide info");
            $("#tempvar_help_content").slideDown();
            $("#tempvar_value").effect("pulsate", {
                times: 3
            }, 1500)
        } else {
            $(this).text("more info");
            $("#tempvar_help_content").slideUp()
        }
        return false
    });
    $("#dialog-template-variable-input").bind("change", function () {
        $("#tempvar_value").text("{" + $(this).val() + "}")
    })
});