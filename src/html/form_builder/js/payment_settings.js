$(function () {
    $(".helpmsg").tooltip({
        position: "bottom center",
        offset: [10, 20],
        effect: "fade",
        opacity: 0.8,
        events: {
            def: "click,mouseout"
        }
    });
    $("#ps_enable_merchant").bind("change", function () {
        if ($(this).prop("checked") == true) {
            $("#ps_main_list").data("payment_properties").enable_merchant = 1;
            $("#ps_main_list li:gt(0)").fadeIn()
        } else {
            $("#ps_main_list").data("payment_properties").enable_merchant = 0;
            $("#ps_main_list li:gt(0)").slideUp()
        }
    });
    $("#ps_select_merchant").bind("change", function () {
        var a = $(this).val();
        $("#ps_main_list").data("payment_properties").merchant_type = a;
        $(".merchant_options,.pesapal_option,.paypal_option").hide();
        $("#ps_currency_paypal_div,#ps_currency_pesapal_div,#ps_currency_cellulant_div,#ps_currency_ecitizen_div,#ps_currency_jambopay_div,#ps_currency_cash_div,#ps_currency_check_div").hide();
        $("#ps_recurring_cycle_unit,#ps_recurring_cycle_unit_month_year").val("month").hide();
        $("#ps_paypal_options").hide();
        $("#ps_pesapal_options,#ps_pesapal_info").hide();
        $("#ps_cellulant_options,#ps_cellulant_info").hide();
        $("#ps_ecitizen_options,#ps_ecitizen_info").hide();
        $("#ps_jambopay_options,#ps_jambopay_info").hide();
        if (a == "paypal_standard") {
            $("#ps_paypal_options").show();
            $(".paypal_option").css("display", "block").show();
            $("#ps_recurring_cycle_unit").show();
            $("#ps_currency_paypal_div").show()
        } else {
            if (a == "pesapal") {
                $("#ps_pesapal_options,#ps_pesapal_info").show();
                $(".pesapal_option").css("display", "block").show();
                $("#ps_recurring_cycle_unit_month_year").show();
                $("#ps_currency_pesapal_div").show()
            } else {
                if (a == "cellulant") {
                    $("#ps_cellulant_options,#ps_cellulant_info").show();
                    $(".cellulant_option").css("display", "block").show();
                    $("#ps_recurring_cycle_unit").show();
                    $("#ps_currency_paypal_div").show()
                }
                else
                {
                    if (a == "ecitizen") {
                        $("#ps_ecitizen_options,#ps_ecitizen_info").show();
                        $(".ecitizen_option").css("display", "block").show();
                        $("#ps_recurring_cycle_unit").show();
                        $("#ps_currency_paypal_div").show()
                    }
                    else
                    {
                        if (a == "jambopay") {
                            $("#ps_jambopay_options,#ps_jambopay_info").show();
                            $(".jambopay_option").css("display", "block").show();
                            $("#ps_recurring_cycle_unit").show();
                            $("#ps_currency_paypal_div").show()
                        }
                        else {
                            if (a == "check") {
                                $("#ps_paypal_options").hide();
                                $("#ps_check_options").show();
                                $("#ps_currency_cash_div").show()
                            }
                            else {
                                if (a == "cash") {
                                    $("#ps_paypal_options").hide();
                                    $("#ps_check_options").show();
                                    $("#ps_currency_cash_div").show()
                                }
                            }
                        }
                    }
                }
            }
        }
    });
    $("#ps_paypal_email").bind("keyup mouseout change", function () {
        $("#ps_main_list").data("payment_properties").paypal_email = $(this).val()
    });
    $("#ps_paypal_language").bind("change", function () {
        $("#ps_main_list").data("payment_properties").paypal_language = $(this).val()
    });
    $("#ps_pesapal_enable_test_mode").bind("change", function () {
        if ($(this).prop("checked") == true) {
            $("#ps_main_list").data("payment_properties").pesapal_enable_test_mode = 1;
            $("#ps_pesapal_live_keys").slideUp();
            $("#ps_pesapal_test_keys_div").slideDown()
        } else {
            $("#ps_main_list").data("payment_properties").pesapal_enable_test_mode = 0;
            $("#ps_pesapal_live_keys").slideDown();
            $("#ps_pesapal_test_keys_div").slideUp()
        }
    });
    $("#ps_paypal_enable_test_mode").bind("change", function () {
        if ($(this).prop("checked") == true) {
            $("#ps_main_list").data("payment_properties").paypal_enable_test_mode = 1
        } else {
            $("#ps_main_list").data("payment_properties").paypal_enable_test_mode = 0
        }
    });

    $("#ps_jambopay_business").bind("keyup mouseout change", function () {
        $("#ps_main_list").data("payment_properties").jambopay_business = $(this).val()
    });

    $("#ps_jambopay_shared_key").bind("keyup mouseout change", function () {
        $("#ps_main_list").data("payment_properties").jambopay_shared_key = $(this).val()
    });

    $("#ps_cellulant_checkout_url").bind("keyup mouseout change", function () {
        $("#ps_main_list").data("payment_properties").cellulant_checkout_url = $(this).val()
    });

    $("#ps_cellulant_username").bind("keyup mouseout change", function () {
        $("#ps_main_list").data("payment_properties").cellulant_merchant_username = $(this).val()
    });

    $("#ps_cellulant_password").bind("keyup mouseout change", function () {
        $("#ps_main_list").data("payment_properties").cellulant_merchant_password = $(this).val()
    });

    $("#ps_cellulant_service_id").bind("keyup mouseout change", function () {
        $("#ps_main_list").data("payment_properties").cellulant_service_id = $(this).val()
    });

    $("#ps_ecitizen_api_id").bind("keyup mouseout change", function () {
        $("#ps_main_list").data("payment_properties").cellulant_checkout_url = $(this).val()
    });

    $("#ps_ecitizen_key").bind("keyup mouseout change", function () {
        $("#ps_main_list").data("payment_properties").cellulant_merchant_username = $(this).val()
    });

    $("#ps_ecitizen_secret").bind("keyup mouseout change", function () {
        $("#ps_main_list").data("payment_properties").cellulant_merchant_password = $(this).val()
    });

    $("#ps_ecitizen_service_id").bind("keyup mouseout change", function () {
        $("#ps_main_list").data("payment_properties").cellulant_service_id = $(this).val()
    });


    $("#ps_pesapal_live_secret_key").bind("keyup mouseout change", function () {
        $("#ps_main_list").data("payment_properties").pesapal_live_secret_key = $(this).val()
    });
    $("#ps_pesapal_live_public_key").bind("keyup mouseout change", function () {
        $("#ps_main_list").data("payment_properties").pesapal_live_public_key = $(this).val()
    });
    $("#ps_pesapal_test_secret_key").bind("keyup mouseout change", function () {
        $("#ps_main_list").data("payment_properties").pesapal_test_secret_key = $(this).val()
    });
    $("#ps_pesapal_test_public_key").bind("keyup mouseout change", function () {
        $("#ps_main_list").data("payment_properties").pesapal_test_public_key = $(this).val()
    });
    $("#ps_currency_paypal,#ps_currency_pesapal,#ps_currency_check,#ps_currency_jambopay,#ps_currency_cash").bind("change", function () {
        $("#ps_main_list").data("payment_properties").currency = $(this).val();
        var a = $(this).find("option:selected").text().split(" - ");
        $(".ps_td_currency,.symbol").html(a[0])
    });
    $("#ps_show_total_amount").bind("change", function () {
        if ($(this).prop("checked") == true) {
            $("#ps_main_list").data("payment_properties").show_total = 1;
            $("#ps_show_total_location_div").slideDown()
        } else {
            $("#ps_main_list").data("payment_properties").show_total = 0;
            $("#ps_show_total_location_div").slideUp()
        }
    });
    $("#ps_enable_tax").bind("change", function () {
        if ($(this).prop("checked") == true) {
            $("#ps_main_list").data("payment_properties").enable_tax = 1;
            $("#ps_tax_rate_div").slideDown()
        } else {
            $("#ps_main_list").data("payment_properties").enable_tax = 0;
            $("#ps_tax_rate_div").slideUp()
        }
    });
    $("#ps_discount_code").bind("keyup mouseout change", function () {
        $("#ps_main_list").data("payment_properties").discount_code = $(this).val()
    });
    $("#ps_tax_rate").bind("keyup mouseout change", function () {
        $("#ps_main_list").data("payment_properties").tax_rate = $(this).val()
    });
    $("#ps_tax_amount").bind("keyup mouseout change", function () {
        $("#ps_main_list").data("payment_properties").tax_amount = $(this).val()
    });
    $("#ps_show_total_location").bind("change", function () {
        $("#ps_main_list").data("payment_properties").total_location = $(this).val()
    });
    $("#ps_enable_recurring").bind("change", function () {
        if ($(this).prop("checked") == true) {
            $("#ps_main_list").data("payment_properties").enable_recurring = 1;
            $("#ps_recurring_div,#ps_trial_div_container").slideDown()
        } else {
            $("#ps_main_list").data("payment_properties").enable_recurring = 0;
            $("#ps_recurring_div,#ps_trial_div_container").slideUp()
        }
    });
    $("#ps_recurring_cycle").bind("change", function () {
        $("#ps_main_list").data("payment_properties").recurring_cycle = parseInt($(this).val())
    });
    $("#ps_recurring_cycle_unit,#ps_recurring_cycle_unit_month_year").bind("change", function () {
        $("#ps_main_list").data("payment_properties").recurring_unit = $(this).val()
    });
    $("#ps_enable_trial").bind("change", function () {
        if ($(this).prop("checked") == true) {
            $("#ps_main_list").data("payment_properties").enable_trial = 1;
            $("#ps_trial_div").slideDown()
        } else {
            $("#ps_main_list").data("payment_properties").enable_trial = 0;
            $("#ps_trial_div").slideUp()
        }
    });
    $("#ps_trial_period").bind("change", function () {
        $("#ps_main_list").data("payment_properties").trial_period = parseInt($(this).val())
    });
    $("#ps_trial_unit").bind("change", function () {
        $("#ps_main_list").data("payment_properties").trial_unit = $(this).val()
    });
    $("#ps_trial_amount").bind("keyup mouseout change", function () {
        $("#ps_main_list").data("payment_properties").trial_amount = $(this).val()
    });
    $("#ps_enable_invoice").bind("change", function () {
        if ($(this).prop("checked") == true) {
            $("#ps_main_list").data("payment_properties").enable_invoice = 1;
            $("#ps_enable_invoice_div").slideDown()
        } else {
            $("#ps_main_list").data("payment_properties").enable_invoice = 0;
            $("#ps_enable_invoice_div").slideUp()
        }
    });
    $("#ps_invoice_email").bind("change", function () {
        $("#ps_main_list").data("payment_properties").invoice_email = $(this).val()
    });
    $("#ps_delay_notifications").bind("change", function () {
        if ($(this).prop("checked") == true) {
            $("#ps_main_list").data("payment_properties").delay_notifications = 1
        } else {
            $("#ps_main_list").data("payment_properties").delay_notifications = 0
        }
    });
    $("#ps_ask_billing").bind("change", function () {
        if ($(this).prop("checked") == true) {
            $("#ps_main_list").data("payment_properties").ask_billing = 1
        } else {
            $("#ps_main_list").data("payment_properties").ask_billing = 0
        }
    });
    $("#ps_ask_shipping").bind("change", function () {
        if ($(this).prop("checked") == true) {
            $("#ps_main_list").data("payment_properties").ask_shipping = 1
        } else {
            $("#ps_main_list").data("payment_properties").ask_shipping = 0
        }
    });
    $("#ps_pricing_type").bind("change", function () {
        if ($(this).val() == "fixed") {
            $("#ps_box_price_variable_amount_div").hide();
            $("#ps_box_price_fixed_amount_div").fadeIn();
            $("#ps_main_list").data("payment_properties").price_type = "fixed"
        } else {
            $("#ps_box_price_fixed_amount_div").hide();
            $("#ps_box_price_variable_amount_div").fadeIn();
            $("#ps_main_list").data("payment_properties").price_type = "variable"
        }
    });
    $("#ps_price_amount").bind("keyup mouseout change", function () {
        var a = parseFloat($(this).val());
        $("#ps_main_list").data("payment_properties").price_amount = a
    });
    $("#ps_price_name").bind("keyup mouseout change", function () {
        $("#ps_main_list").data("payment_properties").price_name = $(this).val()
    });
    $("#ps_select_field_prices").bind("change", function () {
        if ($(this).val() == "") {
            return true
        }
        var a = parseInt($(this).val());
        var c = "";
        if ($("#ps_box_define_prices").data("field_options") != null) {
            c = $("#ps_box_define_prices").data("field_options")[a]
        }
        var d = "";
        var b = $("#ps_currency option:selected").text().split(" - ");
        var f = b[0];
        var g = new Array();
        if ($.isArray(c)) {
            var e = "";
            $.each(c, function (h, i) {
                e += '<tr><td class="ps_td_field_label">' + i.option + '</td><td class="ps_td_field_price"><span class="ps_td_currency">' + f + '</span><input type="text" id="price_' + a + "_" + i.option_id + '" value="0.00" class="element text large"></td></tr>';
                g[parseInt(i.option_id)] = {
                    element_id: a,
                    option_id: parseInt(i.option_id),
                    price: 0,
                    element_type: "multi"
                }
            });
            d = '<li id="liprice_' + a + '" style="display: none"><table cellspacing="0" width="100%"><thead><tr><td colspan="2"><strong>';
            d += $(this).find("option:selected").text();
            d += '</strong><a class="delete_liprice" id="deleteliprice_' + a + '" href="#"><img src="images/icons/53.png" /></a></td></tr></thead><tbody>';
            d += e;
            d += "</tbody></table></li>"
        } else {
            d = '<li id="liprice_' + a + '"><table cellspacing="0" width="100%"><thead><tr><td><strong>' + $(this).find("option:selected").text() + '</strong><a class="delete_liprice" id="deleteliprice_' + a + '" href="#"><img src="images/icons/53.png" /></a></td></tr></thead><tbody><tr><td class="ps_td_field_label">Amount will be entered by the client.</td></tr></tbody></table></li>';
            g.push({
                element_id: a,
                option_id: 0,
                price: 0,
                element_type: "price"
            })
        }
        $("#ps_field_assignment").prepend(d);
        $("#liprice_" + a).fadeIn();
        $("#liprice_" + a).data("field_price_properties", g);
        $(this).find("option:selected").remove();
        if ($("#ps_select_field_prices option").length == 1) {
            $("#ps_select_field_prices option").text("No More Fields Available")
        }
    });
    $("#ps_field_assignment").delegate("input.text", "keyup mouseout change", function (d) {
        var b = $(this).attr("id").split("_");
        var a = parseInt(b[1]);
        var c = parseInt(b[2]);
        $("#liprice_" + a).data("field_price_properties")[c].price = parseFloat($(this).val())
    });
    $("#ps_field_assignment").delegate("a.delete_liprice", "click", function (c) {
        var b = $(this).attr("id").split("_");
        var a = b[1];
        $("#liprice_" + a).fadeOut("slow", function () {
            $(this).remove()
        });
        $("#ps_select_field_prices").append('<option value="' + a + '">' + $(this).prev().text() + "</option>");
        $('#ps_select_field_prices option[value=""]').text("");
        return false
    });
    $("#button_save_payment").click(function () {
        if ($("#button_save_payment").text() != "Saving...") {
            $("#button_save_payment").prop("disabled", true);
            $("#button_save_payment").text("Saving...");
            $("#button_save_payment").after("<div class='small_loader_box' style='float: right'><img src='images/loader_small_grey.gif' /></div>");
            var a = $("#ps_field_assignment li");
            if (a.length >= 1) {
                var b = new Array();
                a.each(function (c) {
                    b[c] = $(this).data("field_price_properties")
                })
            }
            $.ajax({
                type: "POST",
                async: true,
                url: "/backend.php/forms/savepaymentsettings",
                data: {
                    payment_properties: $("#ps_main_list").data("payment_properties"),
                    field_prices: b
                },
                cache: false,
                global: false,
                dataType: "json",
                error: function (f, c, d) {
                    alert("Error! Unable to save payment settings. Please try again.")
                },
                success: function (c) {
                    if (c.status == "ok") {
                        window.location.replace("/backend.php/forms/index")
                    } else {
                        alert("Error! Unable to save payment settings. Please try again.")
                    }
                }
            })
        }
        return false
    })
});
