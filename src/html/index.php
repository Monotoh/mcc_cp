<?php
	require_once(dirname(__FILE__).'/../permitflow_src/config/ProjectConfiguration.class.php');

	$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', 'prod', false);
	sfContext::createInstance($configuration)->dispatch();
