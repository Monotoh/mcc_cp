<?php

	error_reporting(0);

	if($_POST['dbname'] && $_POST['dbhost'] && $_POST['dbuser'])
	{
		$dbconn = mysql_connect($_POST['dbhost'],$_POST['dbuser'],$_POST['dbpass']) or die('<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>'.mysql_error()).'</strong></div>';

		$link = mysql_select_db($_POST['dbname'], $dbconn);

		if($dbconn)
		{
			echo '<div class="alert alert-success">
	            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	            <strong>Database is ok! ';

	        if($link)
	        {
	        	echo 'The database already exists. Ensure it is empty before you continue.';
	        }
	        else
	        {
	        	echo 'The database does not exist. It will be automatically created for you.';
	        }

	        echo '</strong>
	          </div>';
		}
		else
		{
			echo '<div class="alert alert-danger">
	            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	            <strong>Invalid database settings!</strong> Could not connect to the database using the settings above.
	          </div>';
		}
	}
?>
