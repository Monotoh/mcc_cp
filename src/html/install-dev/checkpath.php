<?php

error_reporting(0);

$path = $_POST['path'];

if (file_exists($path)) {
    echo '<div class="alert alert-success">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <strong>Path is ok!</strong></a>.
          </div>';
} else {
    echo '<div class="alert alert-danger">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <strong>Invalid path!</strong> The path you entered does not exist. Check the path and try again.
          </div>';
}

?>
