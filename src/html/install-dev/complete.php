<?php

error_reporting(0);

if($_POST['email'])
{
	require($_POST['path']."/lib/vendor/cp_workflow/config/db_config.inc.php");
  require($_POST['path']."/lib/bcrypt.php");
	$dbconn = mysqli_connect($DATABASE_HOST, $DATABASE_UID, $DATABASE_PWD,$DATABASE_DB);
  if ($dbconn->connect_error) {
    trigger_error('Database connection failed: '  . $dbconn->connect_error, E_USER_ERROR);
  }
  $hash = password_hash($_POST['password'], PASSWORD_BCRYPT);
	$sql = "UPDATE cf_user SET stremail = '".$_POST['email']."', strpassword = '".$hash."' WHERE nid = 1";
	$rs = $dbconn->query($sql);

  if($rs === false) {
    trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $dbconn->error, E_USER_ERROR);
  } else {
    $affected_rows = $dbconn->affected_rows;
  }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="images/favicon.png" type="image/png">

  <title>PermitFlow: Setup Complete!</title>

  <link href="/assets_unified/css/style.default.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="/assets_unified/js/html5shiv.js"></script>
  <script src="/assets_unified/js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="notfound">

<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>

  <div class="notfoundpanel">
    <h3>Your setup is complete. Please delete install folder before you login.</h3>
    <div align="center"><button class="btn btn-primary" onClick="window.location='/backend.php';">Login</button></div>
  </div><!-- notfoundpanel -->

</section>


<script src="/assets_unified/js/jquery-1.10.2.min.js"></script>
<script src="/assets_unified/js/jquery-migrate-1.2.1.min.js"></script>
<script src="/assets_unified/js/bootstrap.min.js"></script>
<script src="/assets_unified/js/modernizr.min.js"></script>
<script src="/assets_unified/js/retina.min.js"></script>

<script src="/assets_unified/js/custom.js"></script>

</body>
</html>
