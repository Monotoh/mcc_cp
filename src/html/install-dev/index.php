<?php

error_reporting(0);

if($_POST['host'])
{
	//Path Settings
	$path = $_POST['path'];

	//Database Settings
	$mysql_host =  $_POST['host'];
	$mysql_database = $_POST['database'];
	$mysql_user = $_POST['username'];
	$mysql_pass = $_POST['password'];

	//Email Settings
	$organisation_name = $_POST['organisationname'];
	$organisation_email = $_POST['organisationemail'];
	$organisation_replyto = $_POST['replyto'];
  $currency = $_POST['currency'];


	//Language Settings
	$locale = $_POST['locale'];

	$success = false;

	//Save Database settings
		// Save to /config/databases.yml
      if(is_file($path.'/config/databases.yml')) {
        unlink($path.'/config/databases.yml');
      }

      //copy databases.yml.dist to databases.yml
      if (!copy($path.'/config/databases.yml.dist', $path.'/config/databases.yml')) {
        echo "failed to copy ".$path.'/config/databases.yml.dist'."...\n";
        exit;
      }

			chmod($path.'/config/databases.yml', 0777);

			//read the entire string
			$str = file_get_contents($path.'/config/databases.yml');
			//replace something in the file string
			$str = str_replace("%mysql_host%", $mysql_host, $str);
			$str = str_replace("%mysql_database%", $mysql_database, $str);
			$str = str_replace("%mysql_user%", $mysql_user, $str);
			$str = str_replace("%mysql_pass%", $mysql_pass, $str);
			file_put_contents($path.'/config/databases.yml', $str);

		// Save to /apps/frontend/config/app.yml
      if(is_file($path.'/apps/frontend/config/app.yml')) {
        unlink($path.'/apps/frontend/config/app.yml');
      }

      //copy app.yml.dist to app.yml
      if (!copy($path.'/apps/frontend/config/app.yml.dist', $path.'/apps/frontend/config/app.yml')) {
        echo "failed to copy ".$path.'/apps/frontend/config/app.yml.dist'."...\n";
        exit;
      }

			chmod($path.'/apps/frontend/config/app.yml', 0777);


				if(is_file($path.'/apps/frontend/config/settings.yml')) {
					unlink($path.'/apps/frontend/config/settings.yml');
				}

				//copy app.yml.dist to app.yml
				if (!copy($path.'/apps/frontend/config/settings.yml.dist', $path.'/apps/frontend/config/settings.yml')) {
					echo "failed to copy ".$path.'/apps/frontend/config/settings.yml.dist'."...\n";
					exit;
				}

				chmod($path.'/apps/frontend/config/settings.yml', 0777);

			//read the entire string
			$str = file_get_contents($path.'/apps/frontend/config/app.yml');
			//replace something in the file string
			$str = str_replace("%mysql_host%", $mysql_host, $str);
			$str = str_replace("%mysql_database%", $mysql_database, $str);
			$str = str_replace("%mysql_user%", $mysql_user, $str);
			$str = str_replace("%mysql_pass%", $mysql_pass, $str);
			file_put_contents($path.'/apps/frontend/config/app.yml', $str);

		// Save to /apps/backend/config/app.yml
      if(is_file($path.'/apps/backend/config/app.yml')) {
        unlink($path.'/apps/backend/config/app.yml');
      }

      //copy app.yml.dist to app.yml
      if (!copy($path.'/apps/backend/config/app.yml.dist', $path.'/apps/backend/config/app.yml')) {
        echo "failed to copy ".$path.'/apps/backend/config/app.yml.dist'."...\n";
        exit;
      }

			chmod($path.'/apps/backend/config/app.yml', 0777);

			// Save to /apps/backend/config/app.yml
			if(is_file($path.'/apps/backend/config/settings.yml')) {
				unlink($path.'/apps/backend/config/settings.yml');
			}

			//copy app.yml.dist to app.yml
			if (!copy($path.'/apps/backend/config/settings.yml.dist', $path.'/apps/backend/config/settings.yml')) {
				echo "failed to copy ".$path.'/apps/backend/config/settings.yml.dist'."...\n";
				exit;
			}

			chmod($path.'/apps/backend/config/settings.yml', 0777);

			//read the entire string
			$str = file_get_contents($path.'/apps/backend/config/app.yml');
			//replace something in the file string
			$str = str_replace("%mysql_host%", $mysql_host, $str);
			$str = str_replace("%mysql_database%", $mysql_database, $str);
			$str = str_replace("%mysql_user%", $mysql_user, $str);
			$str = str_replace("%mysql_pass%", $mysql_pass, $str);
			file_put_contents($path.'/apps/backend/config/app.yml', $str);
    // Save to /lib/vendor/cp_workflow/config/db_config.inc.php
      if(is_file($path.'/lib/vendor/cp_workflow/config/db_config.inc.php')) {
        unlink($path.'/lib/vendor/cp_workflow/config/db_config.inc.php');
      }

      //copy db_config.inc.php.dist to db_config.inc.php
      if (!copy($path.'/lib/vendor/cp_workflow/config/db_config.inc.php.dist', $path.'/lib/vendor/cp_workflow/config/db_config.inc.php')) {
        echo "failed to copy ".$path.'/apps/backend/config/app.yml.dist'."...\n";
        exit;
      }

			chmod($path.'/lib/vendor/cp_workflow/config/db_config.inc.php', 0777);

      //read the entire string
      $str = file_get_contents($path.'/lib/vendor/cp_workflow/config/db_config.inc.php');
      //replace something in the file string
      $str = str_replace("%mysql_host%", $mysql_host, $str);
      $str = str_replace("%mysql_database%", $mysql_database, $str);
      $str = str_replace("%mysql_user%", $mysql_user, $str);
      $str = str_replace("%mysql_pass%", $mysql_pass, $str);
      file_put_contents($path.'/lib/vendor/cp_workflow/config/db_config.inc.php', $str);


	//Save Email Settings
		// Save to /apps/frontend/config/app.yml

			//read the entire string
			$str = file_get_contents($path.'/apps/frontend/config/app.yml');
			//replace something in the file string
			$str = str_replace("%organisation_name%", $organisation_name, $str);
			$str = str_replace("%organisation_email%", $organisation_email, $str);
      $str = str_replace("%currency%", $currency, $str);
			file_put_contents($path.'/apps/frontend/config/app.yml', $str);

		// Save to /apps/backend/config/app.yml

			//read the entire string
			$str = file_get_contents($path.'/apps/backend/config/app.yml');
			//replace something in the file string
			$str = str_replace("%organisation_name%", $organisation_name, $str);
			$str = str_replace("%organisation_email%", $organisation_email, $str);
      $str = str_replace("%currency%", $currency, $str);
			file_put_contents($path.'/apps/backend/config/app.yml', $str);

	//Save Language Settings
		// Save to /apps/frontend/config/app.yml

			//read the entire string
			$str = file_get_contents($path.'/apps/frontend/config/app.yml');
			//replace something in the file string
			$str = str_replace("%locale%", $mysql_host, $str);
			file_put_contents($path.'/apps/frontend/config/app.yml', $str);

			//read the entire string
			$str = file_get_contents($path.'/apps/frontend/config/settings.yml');
			//replace something in the file string
			$str = str_replace("%locale%", $locale, $str);
			file_put_contents($path.'/apps/frontend/config/settings.yml', $str);


		// Save to /apps/backend/config/app.yml

			//read the entire string
			$str = file_get_contents($path.'/apps/backend/config/app.yml');
			//replace something in the file string
			$str = str_replace("%locale%", $locale, $str);
			file_put_contents($path.'/apps/backend/config/app.yml', $str);

			//read the entire string
			$str = file_get_contents($path.'/apps/backend/config/settings.yml');
			//replace something in the file string
			$str = str_replace("%locale%", $locale, $str);
			file_put_contents($path.'/apps/backend/config/settings.yml', $str);

    //clear cache folders if they exist
    if (is_dir($path.'/cache/backend')) {
        rmdir($path.'/cache/backend');
    }
    if (is_dir($path.'/cache/frontend')) {
        rmdir($path.'/cache/frontend');
    }

	if($_POST['install_db'] == "yes")
	{

  	$link = mysql_connect($mysql_host, $mysql_user, $mysql_pass) or die('Error connecting to MySQL server: ' . mysql_error());
  	$db_selected = mysql_select_db($mysql_database, $link);

    if (!$db_selected) {
      // If we couldn't, then it either doesn't exist, or we can't see it.
      $sql = 'CREATE DATABASE '.$mysql_database;

      if (mysql_query($sql, $link)) {
          mysql_select_db($mysql_database) or die('Error selecting MySQL database: ' . mysql_error());
      } else {
          echo 'Error creating database: ' . mysql_error() . "\n";
          exit;
      }
    }

    // Temporary variable, used to store current query
    $templine = '';
    // Read in entire file
    $lines = file("setup.sql");
    // Loop through each line
    foreach ($lines as $line)
    {
        // Skip it if it's a comment
        if (substr($line, 0, 2) == '--' || $line == '')
            continue;

        // Add this line to the current segment
        $templine .= $line;
        // If it has a semicolon at the end, it's the end of the query
        if (substr(trim($line), -1, 1) == ';')
        {
            // Perform the query
            mysql_query($templine) or print('Error performing query \'<strong>' . $templine . '\': ' . mysql_error() . '<br /><br />');
            // Reset temp variable to empty
            $templine = '';
        }
    };

	}

  	$success = true;


	if($success)
	{
		?>
        <!DOCTYPE html>
        <html lang="en">
        <head>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <meta name="description" content="">
          <meta name="author" content="">
          <link rel="shortcut icon" href="/assets_unified/images/favicon.png" type="image/png">

          <title>PermitFlow: Admin Account</title>

          <link href="/assets_unified/css/style.default.css" rel="stylesheet">

          <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
          <!--[if lt IE 9]>
          <script src="/assets_unified/js/html5shiv.js"></script>
          <script src="/assets_unified/js/respond.min.js"></script>
          <![endif]-->
        </head>

        <body class="notfound">

        <!-- Preloader -->
        <div id="preloader">
            <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
        </div>

        <section>

            <div class="lockedpanel">
                <div class="locked">
                    <i class="fa fa-lock"></i>
                </div>
                <div class="loginuser">
                    <img src="/assets_unified/images/photos/loggeduser.png" alt="" />
                </div>
                <div class="logged">
                    <h4>System Administrator</h4>
                    <small class="text-muted"><?php echo $organisation_email; ?></small>
                </div>
                <form method="post" action="complete.php">
                	<input type="hidden" name="path" id="path" value="<?php echo $path; ?>">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Enter your email" style="margin-bottom: 5px;"/>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Enter your password"  style="margin-bottom: 5px;"/>
                    <input type="password" class="form-control" name="confirmpassword" id="confirmpassword" placeholder="Confirm your password" />
                    <button type="submit" class="btn btn-success btn-block" onClick="if((document.getElementById('password').value == document.getElementById('confirmpassword').value) && document.getElementById('password').value != ''){ return true; }else{ alert('Please confirm that your passwords are not blank and that they match'); return false; }">Save Changes</button>
                </form>
            </div><!-- lockedpanel -->

        </section>


        <script src="/assets_unified/js/jquery-1.10.2.min.js"></script>
        <script src="/assets_unified/js/jquery-migrate-1.2.1.min.js"></script>
        <script src="/assets_unified/js/bootstrap.min.js"></script>
        <script src="/assets_unified/js/modernizr.min.js"></script>
        <script src="/assets_unified/js/retina.min.js"></script>

        <script src="/assets_unified/js/custom.js"></script>

        </body>
        </html>
        <?php
		exit;
	}
}
else
{
	?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="shortcut icon" href="/favicon.png" type="image/png">

      <title>PermitFlow: Installation Wizard</title>

      <link href="/assets_unified/css/style.default.css" rel="stylesheet">
      <link href="/assets_unified/css/style.inverse.css" rel="stylesheet">

      <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
      <script src="/assets_unified/js/html5shiv.js"></script>
      <script src="/assets_unified/js/respond.min.js"></script>
      <![endif]-->
    </head>

    <body class="signin">

    <!-- Preloader -->
    <div id="preloader">
        <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
    </div>

    <section>

        <div class="contentpanel">

          <div class="row">

            <div class="col-md-9" style="padding: 50px; margin-left: 13%; margin-right: auto;">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <img src="/assets_unified/images/logo-dark.png">
                  <h4 class="panel-title">PermitFlow: Installation Wizard</h4>
                </div>
                <div class="panel-body panel-body-nopadding">

                  <!-- BASIC WIZARD -->
                  <div id="validationWizard" class="basic-wizard">

                    <ul class="nav nav-pills nav-justified">
                      <li><a href="#vtab0" data-toggle="tab"><span>Step 1:</span> Checklist</a></li>
                      <li><a href="#vtab1" data-toggle="tab"><span>Step 2:</span> Path</a></li>
                      <li><a href="#vtab2" data-toggle="tab"><span>Step 3:</span> Database</a></li>
                      <li><a href="#vtab3" data-toggle="tab"><span>Step 4:</span> Email</a></li>
                      <li><a href="#vtab4" data-toggle="tab"><span>Step 5:</span> Language</a></li>
                    </ul>

                    <form class="form" id="firstForm" name="firstForm" action="index.php" method="post">
                    <div class="tab-content tab-content-nopadding">

                      <div class="tab-pane" id="vtab0" style="padding: 20px;">
                      <?php

            						if (!isset($_SERVER['HTTP_HOST'])) {
            						 	exit('This script cannot be run from the CLI. Run it from a browser.');
            						}

            						require_once dirname(__FILE__).'/SymfonyRequirements.php';

            						$symfonyRequirements = new SymfonyRequirements();

                        $allRequirements = $symfonyRequirements->getRequirements();
            						$majorProblems = $symfonyRequirements->getFailedRequirements();
            						$minorProblems = $symfonyRequirements->getFailedRecommendations();

						          ?>

                    <div class="symfony-block-content">
                        <p>Welcome to your new Permitflow project.</p>
                        <p>
                            This script will guide you through the basic configuration of your project.

                        </p>
                        <?php if (count($allRequirements)): ?>
                            <h2 class="ko">System Requirements</h2>
                            <p>The following are the system requirements to run this application:</p>
                            <ol>
                                <?php foreach ($allRequirements as $problem): ?>
                                    <li><?php echo $problem->getHelpHtml() ?></li>
                                <?php endforeach; ?>
                            </ol>
                        <?php endif; ?>

                        <?php if (count($majorProblems)): ?>
                            <h2 class="ko">Major problems</h2>
                            <p>Major problems have been detected and <strong>must</strong> be fixed before continuing:</p>
                            <ol>
                                <?php foreach ($majorProblems as $problem): ?>
                                    <li><?php echo $problem->getHelpHtml() ?></li>
                                <?php endforeach; ?>
                            </ol>
                        <?php endif; ?>

                        <?php if (count($minorProblems)): ?>
                            <h2>Recommendations</h2>
                            <p>
                                <?php if (count($majorProblems)): ?>Additionally, to<?php else: ?>To<?php endif; ?> enhance your Permitflow experience,
                                it’s recommended that you fix the following:
                            </p>
                            <ol>
                                <?php foreach ($minorProblems as $problem): ?>
                                    <li><?php echo $problem->getHelpHtml() ?></li>
                                <?php endforeach; ?>
                            </ol>
                        <?php endif; ?>

                        <?php if ($symfonyRequirements->hasPhpIniConfigIssue()): ?>
                            <p id="phpini">*
                                <?php if ($symfonyRequirements->getPhpIniConfigPath()): ?>
                                    Changes to the <strong>php.ini</strong> file must be done in "<strong><?php echo $symfonyRequirements->getPhpIniConfigPath() ?></strong>".
                                <?php else: ?>
                                    To change settings, create a "<strong>php.ini</strong>".
                                <?php endif; ?>
                            </p>
                        <?php endif; ?>

                        <?php if (!count($majorProblems) && !count($minorProblems)): ?>
                            <p class="ok">Your configuration looks good to run Symfony.</p>
                        <?php endif; ?>

                    </div>
                      </div>

                      <div class="tab-pane form-bordered form-horizontal pt20" id="vtab1">
                          <div class="form-group">
                            <label class="col-sm-2 control-label">Path to Source Code</label>
                            <div class="col-sm-8">
                              <input type="text" name="path" class="form-control" required placeholder="/var/www/permitflow_src" value="<?php echo __DIR__.'/../../permitflow_src'; ?>"/>
                              <div id="pathresult" name='pathresult'></div>
                            </div>
                          </div>
                      </div>

                      <div class="tab-pane form-bordered form-horizontal pt20" id="vtab2">
                          <div class="form-group">
                            <label class="col-sm-2 control-label">MySQL Host</label>
                            <div class="col-sm-8">
                              <input type="text" id="host" name="host" class="form-control" required />
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="col-sm-2 control-label">MySQL Database</label>
                            <div class="col-sm-8">
                              <input type="text" id="database" name="database" class="form-control" required />
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="col-sm-2 control-label">MySQL Username</label>
                            <div class="col-sm-8">
                              <input type="text" id="username" name="username" class="form-control" required />
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="col-sm-2 control-label">MySQL Password</label>
                            <div class="col-sm-8">
                              <input type="password" id="password" name="password" class="form-control" required />
                            </div>
                          </div>
                          <div id="dbresult"></div>

                      </div>
                      <div class="tab-pane form-bordered form-horizontal pt20" id="vtab3">
                          <div class="form-group">
                            <label class="col-sm-2 control-label">Organisation Name</label>
                            <div class="col-sm-8">
                              <input type="text" name="organisationname" class="form-control" required />
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="col-sm-2 control-label">Organisation Email</label>
                            <div class="col-sm-8">
                              <input type="text" name="organisationemail" class="form-control" required />
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="col-sm-2 control-label">Reply-To Email</label>
                            <div class="col-sm-8">
                              <input type="text" name="replyto" class="form-control" required />
                            </div>
                          </div>

                          <div class="form-group">
                            <label class="col-sm-2 control-label">Currency (e.g. USD, BDT..)</label>
                            <div class="col-sm-8">
                              <input type="text" name="currency" class="form-control" required />
                            </div>
                          </div>

                      </div>

                      <div class="tab-pane form-bordered form-horizontal pt20" id="vtab4">

                          <div class="form-group">
                            <label class="col-sm-2 control-label">Select Default Language</label>
                            <div class="col-sm-8">
														<div class="rdio rdio-primary">
															<input type="radio" value="ar_IQ" id="locale2" name="locale"/>
															<label for="locale2">Arabic</label>
														</div>
														<div class="rdio rdio-primary">
															<input type="radio" value="bn_BD" id="locale2" name="locale"/>
															<label for="locale2">Bengali</label>
														</div>
                              <div class="rdio rdio-primary">
                                <input type="radio" value="en_US" id="locale1" name="locale"/>
                                <label for="locale1">English</label>
                              </div>
                            </div>
                          </div>

													<hr>

													<div class="form-group">
                            <label class="col-sm-2 control-label">Do you want to install the default database?</label>
                            <div class="col-sm-8">
														<div class="rdio rdio-primary">
															<input type="radio" value="yes" id="install_db" name="install_db"/>
															<label for="install_db">Yes</label>
														</div>
														<div class="rdio rdio-primary">
															<input type="radio" value="no" id="ignore_db" name="install_db"/>
															<label for="ignore_db">No</label>
														</div>
                            </div>
                          </div>

                          <div class="form-group">
                            <div class="col-sm-12" align="right">
                              <button type="submit" class="btn btn-success">Finish Setup</button>
                            </div>
                          </div>
                      </div>


                    </div><!-- tab-content -->
                    </form>

                    <ul class="pager wizard">
                        <li class="previous"><a href="javascript:void(0)">Previous</a></li>
                        <li class="next"><a href="javascript:void(0)">Next</a></li>
                      </ul>

                  </div><!-- #validationWizard -->

                </div><!-- panel-body -->
              </div><!-- panel -->
            </div><!-- col-md-6 -->

            <br>

          </div><!-- row -->

        </div><!-- contentpanel -->

    </section>


    <script src="/assets_unified/js/jquery-1.10.2.min.js"></script>
    <script src="/assets_unified/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="/assets_unified/js/bootstrap.min.js"></script>
    <script src="/assets_unified/js/modernizr.min.js"></script>
    <script src="/assets_unified/js/jquery.sparkline.min.js"></script>
    <script src="/assets_unified/js/toggles.min.js"></script>
    <script src="/assets_unified/js/retina.min.js"></script>
    <script src="/assets_unified/js/jquery.cookies.js"></script>

    <script src="/assets_unified/js/bootstrap-wizard.min.js"></script>
    <script src="/assets_unified/js/jquery.validate.min.js"></script>

    <script src="/assets_unified/js/custom.js"></script>
    <script>
    jQuery(document).ready(function(){

      //Check path
      $( "input[name='path']" ).keyup(function() {
        $.ajax({
                type: "POST",
                url: "checkpath.php",
                data: {
                    'path' : $('input:text[name=path]').val()
                },
                dataType: "text",
                success: function(msg){
                      //Receiving the result of search here
                      $("#pathresult").html(msg);
                }
            });
      });

      // Basic Wizard
      jQuery('#basicWizard').bootstrapWizard();

      // Progress Wizard
      $('#progressWizard').bootstrapWizard({
        'nextSelector': '.next',
        'previousSelector': '.previous',
        onNext: function(tab, navigation, index) {
          var $total = navigation.find('li').length;
          var $current = index+1;
          var $percent = ($current/$total) * 100;
          jQuery('#progressWizard').find('.progress-bar').css('width', $percent+'%');
        },
        onPrevious: function(tab, navigation, index) {
          var $total = navigation.find('li').length;
          var $current = index+1;
          var $percent = ($current/$total) * 100;
          jQuery('#progressWizard').find('.progress-bar').css('width', $percent+'%');
        },
        onTabShow: function(tab, navigation, index) {
          var $total = navigation.find('li').length;
          var $current = index+1;
          var $percent = ($current/$total) * 100;
          jQuery('#progressWizard').find('.progress-bar').css('width', $percent+'%');
        }
      });

      // Disabled Tab Click Wizard
      jQuery('#disabledTabWizard').bootstrapWizard({
        tabClass: 'nav nav-pills nav-justified nav-disabled-click',
        onTabClick: function(tab, navigation, index) {
          return false;
        }
      });


      jQuery('#validationWizard').bootstrapWizard({
        tabClass: 'nav nav-pills nav-justified nav-disabled-click',
        onTabClick: function(tab, navigation, index) {
          return false;
        },
        onNext: function(tab, navigation, index) {
          var $valid = jQuery('#firstForm').valid();
          if(!$valid) {
            $validator.focusInvalid();
            return false;
          }
        }
      });

      $("#database").keyup(function () {
        var that = this,
        value = $(this).val();

        if (value.length >= 1 ) {
            $.ajax({
                type: "POST",
                url: "checkdb.php",
                data: {
                    'dbname' : $('input:text[id=database]').val(),
                    'dbuser' : $('input:text[id=username]').val(),
                    'dbpass' : $('input:password[id=password]').val(),
                    'dbhost' : $('input:text[id=host]').val()
                },
                dataType: "text",
                success: function(msg){
                      //Receiving the result of search here
                      $("#dbresult").html(msg);
                }
            });
        }
    });

      $("#username").keyup(function () {
        var that = this,
        value = $(this).val();

        if (value.length >= 1 ) {
            $.ajax({
                type: "POST",
                url: "checkdb.php",
                data: {
                    'dbname' : $('input:text[id=database]').val(),
                    'dbuser' : $('input:text[id=username]').val(),
                    'dbpass' : $('input:password[id=password]').val(),
                    'dbhost' : $('input:text[id=host]').val()
                },
                dataType: "text",
                success: function(msg){
                      //Receiving the result of search here
                      $("#dbresult").html(msg);
                }
            });
        }
    });

      $("#password").keyup(function () {
        var that = this,
        value = $(this).val();

        if (value.length >= 1 ) {
            $.ajax({
                type: "POST",
                url: "checkdb.php",
                data: {
                    'dbname' : $('input:text[id=database]').val(),
                    'dbuser' : $('input:text[id=username]').val(),
                    'dbpass' : $('input:password[id=password]').val(),
                    'dbhost' : $('input:text[id=host]').val()
                },
                dataType: "text",
                success: function(msg){
                    //we need to check if the value is the same
                      $("#dbresult").html(msg);
                }
            });
        }
    });

      $("#host").keyup(function () {
        var that = this,
        value = $(this).val();

        if (value.length >= 1 ) {
            $.ajax({
                type: "POST",
                url: "checkdb.php",
                data: {
                    'dbname' : $('input:text[id=database]').val(),
                    'dbuser' : $('input:text[id=username]').val(),
                    'dbpass' : $('input:password[id=password]').val(),
                    'dbhost' : $('input:text[id=host]').val()
                },
                dataType: "text",
                success: function(msg){
                      //Receiving the result of search here
                      $("#dbresult").html(msg);
                }
            });
        }
    });

    });
    </script>

    </body>
    </html>
	<?php
}
?>
