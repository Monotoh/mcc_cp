ALTER TABLE `audit_trail` ADD `object_id` INT NOT NULL ,
ADD `object_name` VARCHAR( 250 ) NOT NULL ,
ADD `before_values` TEXT NOT NULL ,
ADD `after_values` TEXT NOT NULL ,
ADD `ipaddress` VARCHAR( 250 ) NOT NULL ;