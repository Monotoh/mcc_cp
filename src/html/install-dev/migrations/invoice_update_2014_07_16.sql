ALTER TABLE `mf_invoice` ADD `mda_code` VARCHAR(250) NULL ;
ALTER TABLE `mf_invoice` ADD `service_code` VARCHAR(250) NULL ;
ALTER TABLE `mf_invoice` ADD `branch` VARCHAR(250) NULL ;
ALTER TABLE `mf_invoice` ADD `due_date` VARCHAR(250) NULL ;
ALTER TABLE `mf_invoice` ADD `payer_id` VARCHAR(250) NULL ;
ALTER TABLE `mf_invoice` ADD `payer_name` VARCHAR(250) NULL ;
ALTER TABLE `mf_invoice` ADD `total_amount` DOUBLE NULL ;
ALTER TABLE `mf_invoice` ADD `currency` VARCHAR(250) NULL ;
ALTER TABLE `mf_invoice` ADD `doc_ref_number` VARCHAR(250) NULL ;
ALTER TABLE `mf_invoice` ADD `template_id` bigint(20) NULL ;

ALTER TABLE `invoicetemplates` ADD `due_duration` VARCHAR(250) NULL ;
ALTER TABLE `invoicetemplates` ADD `invoice_number` VARCHAR(250) NULL ;