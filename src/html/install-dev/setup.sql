-- phpMyAdmin SQL Dump
-- version 4.0.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 07, 2014 at 04:43 PM
-- Server version: 5.5.33
-- PHP Version: 5.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `permitflow_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

CREATE TABLE `activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `form_entry_id` int(11) DEFAULT NULL,
  `action` text,
  `action_timestamp` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `alerts`
--

CREATE TABLE `alerts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(250) NOT NULL,
  `content` varchar(250) NOT NULL,
  `link` varchar(250) NOT NULL,
  `isread` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `datesent` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `start_date` varchar(250) NOT NULL,
  `end_date` varchar(250) NOT NULL,
  `frontend` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `application_numbers` (
  `form_id` int(11) NOT NULL,
  `stage_id` int(11) NOT NULL,
  `application_number` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `application_reference`
--

CREATE TABLE `application_reference` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stage_id` int(11) DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `start_date` text NOT NULL,
  `end_date` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `approval_condition`
--

CREATE TABLE `approval_condition` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `entry_id` bigint(20) DEFAULT NULL,
  `condition_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `entry_id_idx` (`entry_id`),
  KEY `condition_id_idx` (`condition_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `app_change`
--

CREATE TABLE `app_change` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stage_id` int(11) DEFAULT NULL,
  `form_id` int(11) DEFAULT NULL,
  `identifier_type` int(11) DEFAULT NULL,
  `app_identifier` text NOT NULL,
  `identifier_start` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_column_preferences`
--

CREATE TABLE `ap_column_preferences` (
  `acp_id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) DEFAULT NULL,
  `element_name` varchar(255) NOT NULL DEFAULT '',
  `position` int(11) NOT NULL DEFAULT '0',
  `starting_point` varchar(255) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`acp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_element_options`
--

CREATE TABLE IF NOT EXISTS `ap_grid_columns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `element_name` varchar(255) NOT NULL,
  `form_id` varchar(255) NOT NULL,
  `chart_id` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;


CREATE TABLE `ap_element_options` (
  `aeo_id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL DEFAULT '0',
  `element_id` int(11) NOT NULL DEFAULT '0',
  `option_id` int(11) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `option` text,
  `option_is_default` int(11) NOT NULL DEFAULT '0',
  `live` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`aeo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4947 ;

--
-- Dumping data for table `ap_element_options`
--

INSERT IGNORE INTO `ap_element_options` (`aeo_id`, `form_id`, `element_id`, `option_id`, `position`, `option`, `option_is_default`, `live`) VALUES
(258, 15, 9, 1, 1, 'Email', 1, 1),
(259, 15, 9, 2, 2, 'SMS', 0, 1),
(295, 15, 9, 3, 3, 'Both Email And SMS', 0, 1),
(523, 15, 11, 1, 1, 'Firm', 0, 1),
(524, 15, 11, 2, 2, 'Individual', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_element_prices`
--

CREATE TABLE `ap_element_prices` (
  `aep_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `element_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL DEFAULT '0',
  `price` decimal(62,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`aep_id`),
  KEY `form_id` (`form_id`),
  KEY `element_id` (`element_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_email_logic`
--

CREATE TABLE `ap_email_logic` (
  `form_id` int(11) NOT NULL,
  `rule_id` int(11) NOT NULL,
  `rule_all_any` varchar(3) NOT NULL DEFAULT 'all',
  `target_email` text NOT NULL,
  `template_name` varchar(15) NOT NULL DEFAULT 'notification' COMMENT 'notification - confirmation - custom',
  `custom_from_name` text,
  `custom_from_email` varchar(255) NOT NULL DEFAULT '',
  `custom_subject` text,
  `custom_content` text,
  `custom_plain_text` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`form_id`,`rule_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_email_logic_conditions`
--

CREATE TABLE `ap_email_logic_conditions` (
  `aec_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `target_rule_id` int(11) NOT NULL,
  `element_name` varchar(50) NOT NULL DEFAULT '',
  `rule_condition` varchar(15) NOT NULL DEFAULT 'is',
  `rule_keyword` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`aec_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_entries_preferences`
--

CREATE TABLE `ap_entries_preferences` (
  `form_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `entries_sort_by` varchar(100) NOT NULL DEFAULT 'id-desc',
  `entries_enable_filter` int(1) NOT NULL DEFAULT '0',
  `entries_filter_type` varchar(5) NOT NULL DEFAULT 'all' COMMENT 'all or any'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_field_logic_conditions`
--

CREATE TABLE `ap_field_logic_conditions` (
  `alc_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `target_element_id` int(11) NOT NULL,
  `element_name` varchar(50) NOT NULL DEFAULT '',
  `rule_condition` varchar(15) NOT NULL DEFAULT 'is',
  `rule_keyword` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`alc_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_field_logic_elements`
--

CREATE TABLE `ap_field_logic_elements` (
  `form_id` int(11) NOT NULL,
  `element_id` int(11) NOT NULL,
  `rule_show_hide` varchar(4) NOT NULL DEFAULT 'show',
  `rule_all_any` varchar(3) NOT NULL DEFAULT 'all',
  PRIMARY KEY (`form_id`,`element_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_workflow_logic_conditions`
--

CREATE TABLE `ap_workflow_logic_conditions` (
  `alc_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `target_element_id` int(11) NOT NULL,
  `element_name` varchar(50) NOT NULL DEFAULT '',
  `rule_condition` varchar(15) NOT NULL DEFAULT 'is',
  `rule_keyword` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`alc_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_workflow_logic_elements`
--

CREATE TABLE `ap_workflow_logic_elements` (
  `form_id` int(11) NOT NULL,
  `element_id` int(11) NOT NULL,
  `rule_show_hide` varchar(4) NOT NULL DEFAULT 'show',
  `rule_all_any` varchar(3) NOT NULL DEFAULT 'all',
  PRIMARY KEY (`form_id`,`element_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Table structure for table `ap_workflow_logic_conditions`
--

CREATE TABLE `ap_permission_logic_conditions` (
  `alc_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `target_element_id` int(11) NOT NULL,
  `element_name` varchar(50) NOT NULL DEFAULT '',
  `rule_condition` varchar(15) NOT NULL DEFAULT 'is',
  `rule_keyword` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`alc_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_workflow_logic_elements`
--

CREATE TABLE `ap_permission_logic_elements` (
  `form_id` int(11) NOT NULL,
  `element_id` int(11) NOT NULL,
  `rule_show_hide` varchar(4) NOT NULL DEFAULT 'show',
  `rule_all_any` varchar(3) NOT NULL DEFAULT 'all',
  PRIMARY KEY (`form_id`,`element_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_field_relations`
--

CREATE TABLE `ap_field_relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field1` int(11) NOT NULL,
  `field2` int(11) NOT NULL,
  `formid` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_fonts`
--

CREATE TABLE `ap_fonts` (
  `font_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `font_origin` varchar(11) NOT NULL DEFAULT 'google',
  `font_family` varchar(100) DEFAULT NULL,
  `font_variants` text,
  `font_variants_numeric` text,
  PRIMARY KEY (`font_id`),
  KEY `font_family` (`font_family`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=445 ;

--
-- Dumping data for table `ap_fonts`
--

INSERT IGNORE INTO `ap_fonts` (`font_id`, `font_origin`, `font_family`, `font_variants`, `font_variants_numeric`) VALUES
(1, 'google', 'Open Sans', '300,300italic,400,400italic,600,600italic,700,700italic,800,800italic', '300,300-italic,400,400-italic,600,600-italic,700,700-italic,800,800-italic'),
(2, 'google', 'Droid Sans', 'regular,bold', '400,700'),
(3, 'google', 'Oswald', 'regular', '400'),
(4, 'google', 'Droid Serif', 'regular,italic,bold,bolditalic', '400,400-italic,700,700-italic'),
(5, 'google', 'Lora', 'regular,italic,bold,bolditalic', '400,400-italic,700,700-italic'),
(6, 'google', 'Yanone Kaffeesatz', '200,300,400,700', '200,300,400,700'),
(7, 'google', 'PT Sans', 'regular,italic,bold,bolditalic', '400,400-italic,700,700-italic'),
(8, 'google', 'Lobster', 'regular', '400'),
(9, 'google', 'Ubuntu', '300,300italic,regular,italic,500,500italic,bold,bolditalic', '300,300-italic,400,400-italic,500,500-italic,700,700-italic'),
(10, 'google', 'Arvo', 'regular,italic,bold,bolditalic', '400,400-italic,700,700-italic'),
(11, 'google', 'Coming Soon', 'regular', '400'),
(12, 'google', 'PT Sans Narrow', 'regular,bold', '400,700'),
(13, 'google', 'The Girl Next Door', 'regular', '400'),
(14, 'google', 'Lato', '100,100italic,300,300italic,400,400italic,700,700italic,900,900italic', '100,100-italic,300,300-italic,400,400-italic,700,700-italic,900,900italic'),
(15, 'google', 'Shadows Into Light', 'regular', '400'),
(16, 'google', 'Dancing Script', 'regular,bold', '400,700'),
(17, 'google', 'Marck Script', '400', '400'),
(18, 'google', 'Cabin', '400,400italic,500,500italic,600,600italic,bold,bolditalic', '400,400-italic,500,500-italic,600,600-italic,700,700-italic'),
(19, 'google', 'Calligraffitti', 'regular', '400'),
(20, 'google', 'Josefin Sans', '100,100italic,300,300italic,400,400italic,600,600italic,700,700italic', '100,100-italic,300,300-italic,400,400-italic,600,600-italic,700,700-italic'),
(21, 'google', 'Nobile', 'regular,italic,bold,bolditalic', '400,400-italic,700,700-italic'),
(22, 'google', 'Crafty Girls', 'regular', '400'),
(23, 'google', 'Rock Salt', 'regular', '400'),
(24, 'google', 'Reenie Beanie', 'regular', '400'),
(25, 'google', 'Bitter', '400,400italic,700', '400,400-italic,700'),
(26, 'google', 'Francois One', 'regular', '400'),
(27, 'google', 'Raleway', '100', '100'),
(28, 'google', 'Cherry Cream Soda', 'regular', '400'),
(29, 'google', 'Syncopate', 'regular,bold', '400,700'),
(30, 'google', 'Tangerine', 'regular,bold', '400,700'),
(31, 'google', 'Molengo', 'regular', '400'),
(32, 'google', 'Play', 'regular,bold', '400,700'),
(33, 'google', 'Pacifico', 'regular', '400'),
(34, 'google', 'Arimo', 'regular,italic,bold,bolditalic', '400,400-italic,700,700-italic'),
(35, 'google', 'Chewy', 'regular', '400'),
(36, 'google', 'Cuprum', 'regular', '400'),
(37, 'google', 'Cantarell', 'regular,italic,bold,bolditalic', '400,400-italic,700,700-italic'),
(38, 'google', 'Walter Turncoat', 'regular', '400'),
(39, 'google', 'Anton', 'regular', '400'),
(40, 'google', 'Luckiest Guy', 'regular', '400'),
(41, 'google', 'Open Sans Condensed', '300,300italic', '300,300-italic'),
(42, 'google', 'Vollkorn', 'regular,italic,bold,bolditalic', '400,400-italic,700,700-italic'),
(43, 'google', 'Josefin Slab', '100,100italic,300,300italic,400,400italic,600,600italic,700,700italic', '100,100-italic,300,300-italic,400,400-italic,600,600-italic,700,700-italic'),
(44, 'google', 'PT Serif', 'regular,italic,bold,bolditalic', '400,400-italic,700,700-italic'),
(45, 'google', 'Homemade Apple', 'regular', '400'),
(46, 'google', 'Copse', 'regular', '400'),
(47, 'google', 'Terminal Dosis', '200,300,400,500,600,700,800', '200,300,400,500,600,700,800'),
(48, 'google', 'Slackey', 'regular', '400'),
(49, 'google', 'Kreon', '300,400,700', '300,400,700'),
(50, 'google', 'Permanent Marker', 'regular', '400'),
(51, 'google', 'Crimson Text', 'regular,400italic,600,600italic,700,700italic', '400,400-italic,600,600-italic,700,700-italic'),
(52, 'google', 'Maven Pro', '400,500,700,900', '400,500,700,900'),
(53, 'google', 'Droid Sans Mono', 'regular', '400'),
(54, 'google', 'Varela Round', 'regular', '400'),
(55, 'google', 'Philosopher', 'regular,italic,bold,bolditalic', '400,400-italic,700,700-italic'),
(56, 'google', 'News Cycle', 'regular', '400'),
(57, 'google', 'Fontdiner Swanky', 'regular', '400'),
(58, 'google', 'Amaranth', 'regular,400italic,700,700italic', '400,400-italic,700,700-italic'),
(59, 'google', 'Covered By Your Grace', 'regular', '400'),
(60, 'google', 'Marvel', '400,400italic,700,700italic', '400,400-italic,700,700-italic'),
(61, 'google', 'Actor', 'regular', '400'),
(62, 'google', 'Nunito', '300,400,700', '300,400,700'),
(63, 'google', 'Paytone One', 'regular', '400'),
(64, 'google', 'Ubuntu Condensed', '400', '400'),
(65, 'google', 'Gloria Hallelujah', 'regular', '400'),
(66, 'google', 'Lobster Two', '400,400italic,700,700italic', '400,400-italic,700,700-italic'),
(67, 'google', 'Bevan', 'regular', '400'),
(68, 'google', 'Merriweather', '300,regular,700,900', '300,400,700,900'),
(69, 'google', 'Old Standard TT', 'regular,italic,bold', '400,400-italic,700'),
(70, 'google', 'Rokkitt', 'regular,700', '400,700'),
(71, 'google', 'PT Sans Caption', 'regular,bold', '400,700'),
(72, 'google', 'Architects Daughter', 'regular', '400'),
(73, 'google', 'Abel', 'regular', '400'),
(74, 'google', 'Neucha', 'regular', '400'),
(75, 'google', 'Istok Web', '400,400italic,700,700italic', '400,400-italic,700,700-italic'),
(76, 'google', 'Allerta', 'regular', '400'),
(77, 'google', 'Questrial', '400', '400'),
(78, 'google', 'Allerta Stencil', 'regular', '400'),
(79, 'google', 'MedievalSharp', 'regular', '400'),
(80, 'google', 'Indie Flower', 'regular', '400'),
(81, 'google', 'Carter One', 'regular', '400'),
(82, 'google', 'Cabin Sketch', 'regular,bold', '400,700'),
(83, 'google', 'Cardo', 'regular,400italic,700', '400,400-italic,700'),
(84, 'google', 'Schoolbell', 'regular', '400'),
(85, 'google', 'Miltonian Tattoo', 'regular', '400'),
(86, 'google', 'Neuton', '200,300,regular,italic,700,800', '200,300,400,400-italic,700,800'),
(87, 'google', 'Muli', '300,300italic,400,400italic', '300,300-italic,400,400-italic'),
(88, 'google', 'Tinos', 'regular,italic,bold,bolditalic', '400,400-italic,700,700-italic'),
(89, 'google', 'Puritan', 'regular,italic,bold,bolditalic', '400,400-italic,700,700-italic'),
(90, 'google', 'Merienda One', 'regular', '400'),
(91, 'google', 'Crushed', 'regular', '400'),
(92, 'google', 'Inconsolata', 'regular', '400'),
(93, 'google', 'Gruppo', 'regular', '400'),
(94, 'google', 'Goudy Bookletter 1911', 'regular', '400'),
(95, 'google', 'Maiden Orange', 'regular', '400'),
(96, 'google', 'Podkova', 'regular,700', '400,700'),
(97, 'google', 'Rancho', '400', '400'),
(98, 'google', 'Signika', '300,400,600,700', '300,400,600,700'),
(99, 'google', 'Waiting for the Sunrise', 'regular', '400'),
(100, 'google', 'Salsa', '400', '400'),
(101, 'google', 'Six Caps', 'regular', '400'),
(102, 'google', 'Didact Gothic', 'regular', '400'),
(103, 'google', 'Sunshiney', 'regular', '400'),
(104, 'google', 'Just Another Hand', 'regular', '400'),
(105, 'google', 'Orbitron', '400,500,700,900', '400,500,700,900'),
(106, 'google', 'Mountains of Christmas', 'regular,700', '400,700'),
(107, 'google', 'Kranky', 'regular', '400'),
(108, 'google', 'IM Fell DW Pica', 'regular,italic', '400,400-italic'),
(109, 'google', 'Jura', '300,400,500,600', '300,400,500,600'),
(110, 'google', 'Unkempt', 'regular,700', '400,700'),
(111, 'google', 'Volkhov', '400,400italic,700,700italic', '400,400-italic,700,700-italic'),
(112, 'google', 'Kristi', 'regular', '400'),
(113, 'google', 'Pinyon Script', 'regular', '400'),
(114, 'google', 'IM Fell English', 'regular,italic', '400,400-italic'),
(115, 'google', 'EB Garamond', 'regular', '400'),
(116, 'google', 'PT Serif Caption', 'regular,italic', '400,400-italic'),
(117, 'google', 'Quattrocento Sans', 'regular', '400'),
(118, 'google', 'Bentham', 'regular', '400'),
(119, 'google', 'Shanti', 'regular', '400'),
(120, 'google', 'Chivo', '400,400italic,900,900italic', '400,400-italic,900,900italic'),
(121, 'google', 'Metrophobic', 'regular', '400'),
(122, 'google', 'Delius', '400', '400'),
(123, 'google', 'Cousine', 'regular,italic,bold,bolditalic', '400,400-italic,700,700-italic'),
(124, 'google', 'Carme', 'regular', '400'),
(125, 'google', 'Changa One', 'regular,italic', '400,400-italic'),
(126, 'google', 'Kameron', '400,700', '400,700'),
(127, 'google', 'Yellowtail', 'regular', '400'),
(128, 'google', 'Special Elite', 'regular', '400'),
(129, 'google', 'Love Ya Like A Sister', 'regular', '400'),
(130, 'google', 'Comfortaa', '300,400,700', '300,400,700'),
(131, 'google', 'Bangers', 'regular', '400'),
(132, 'google', 'Mako', 'regular', '400'),
(133, 'google', 'Quattrocento', 'regular', '400'),
(134, 'google', 'Stardos Stencil', 'regular,bold', '400,700'),
(135, 'google', 'Michroma', 'regular', '400'),
(136, 'google', 'Bowlby One SC', 'regular', '400'),
(137, 'google', 'Leckerli One', 'regular', '400'),
(138, 'google', 'Rosario', 'regular,italic,700,700italic', '400,400-italic,700,700-italic'),
(139, 'google', 'Hammersmith One', 'regular', '400'),
(140, 'google', 'Passion One', '400,700,900', '400,700,900'),
(141, 'google', 'Rochester', 'regular', '400'),
(142, 'google', 'Allan', 'bold', '700'),
(143, 'google', 'Geo', 'regular', '400'),
(144, 'google', 'Varela', 'regular', '400'),
(145, 'google', 'Alice', 'regular', '400'),
(146, 'google', 'Sorts Mill Goudy', '400,400italic', '400,400-italic'),
(147, 'google', 'Corben', '400,bold', '400,700'),
(148, 'google', 'Coda', '400,800', '400,800'),
(149, 'google', 'Andika', 'regular', '400'),
(150, 'google', 'Playfair Display', 'regular,400italic', '400,400-italic'),
(151, 'google', 'Sue Ellen Francisco', 'regular', '400'),
(152, 'google', 'Jockey One', '400', '400'),
(153, 'google', 'Coustard', '400,900', '400,900'),
(154, 'google', 'Patrick Hand', 'regular', '400'),
(155, 'google', 'Cabin Condensed', '400,500,600,700', '400,500,600,700'),
(156, 'google', 'Redressed', 'regular', '400'),
(157, 'google', 'Aclonica', 'regular', '400'),
(158, 'google', 'Poly', '400,400italic', '400,400-italic'),
(159, 'google', 'Quicksand', '300,400,700', '300,400,700'),
(160, 'google', 'Sancreek', '400', '400'),
(161, 'google', 'VT323', 'regular', '400'),
(162, 'google', 'Lekton', '400,italic,700', '400,400-italic,700'),
(163, 'google', 'Antic', '400', '400'),
(164, 'google', 'UnifrakturMaguntia', 'regular', '400'),
(165, 'google', 'Brawler', 'regular', '400'),
(166, 'google', 'Nothing You Could Do', 'regular', '400'),
(167, 'google', 'IM Fell DW Pica SC', 'regular', '400'),
(168, 'google', 'Coda Caption', '800', '800'),
(169, 'google', 'Satisfy', '400', '400'),
(170, 'google', 'Days One', '400', '400'),
(171, 'google', 'Anonymous Pro', 'regular,italic,bold,bolditalic', '400,400-italic,700,700-italic'),
(172, 'google', 'IM Fell English SC', 'regular', '400'),
(173, 'google', 'Over the Rainbow', 'regular', '400'),
(174, 'google', 'Amatic SC', '400,700', '400,700'),
(175, 'google', 'Artifika', 'regular', '400'),
(176, 'google', 'Aldrich', 'regular', '400'),
(177, 'google', 'La Belle Aurore', 'regular', '400'),
(178, 'google', 'Nixie One', 'regular', '400'),
(179, 'google', 'Spinnaker', 'regular', '400'),
(180, 'google', 'Pompiere', 'regular', '400'),
(181, 'google', 'Smythe', 'regular', '400'),
(182, 'google', 'Delius Swash Caps', '400', '400'),
(183, 'google', 'Mate', '400,400italic', '400,400-italic'),
(184, 'google', 'Ultra', 'regular', '400'),
(185, 'google', 'Sansita One', 'regular', '400'),
(186, 'google', 'Damion', 'regular', '400'),
(187, 'google', 'Limelight', 'regular', '400'),
(188, 'google', 'Cedarville Cursive', 'regular', '400'),
(189, 'google', 'IM Fell French Canon SC', 'regular', '400'),
(190, 'google', 'Montez', 'regular', '400'),
(191, 'google', 'Forum', 'regular', '400'),
(192, 'google', 'Aladin', '400', '400'),
(193, 'google', 'Delius Unicase', '400,700', '400,700'),
(194, 'google', 'Hanuman', 'regular,bold', '400,700'),
(195, 'google', 'Wire One', 'regular', '400'),
(196, 'google', 'Expletus Sans', '400,400italic,500,500italic,600,600italic,700,700italic', '400,400-italic,500,500-italic,600,600-italic,700,700-italic'),
(197, 'google', 'Annie Use Your Telescope', 'regular', '400'),
(198, 'google', 'Snippet', 'regular', '400'),
(199, 'google', 'Just Me Again Down Here', 'regular', '400'),
(200, 'google', 'Ubuntu Mono', 'regular,italic,bold,bolditalic', '400,400-italic,700,700-italic'),
(201, 'google', 'Inder', '400', '400'),
(202, 'google', 'Candal', 'regular', '400'),
(203, 'google', 'Adamina', '400', '400'),
(204, 'google', 'Gentium Basic', 'regular,italic,bold,bolditalic', '400,400-italic,700,700-italic'),
(205, 'google', 'IM Fell Great Primer SC', 'regular', '400'),
(206, 'google', 'IM Fell Double Pica SC', 'regular', '400'),
(207, 'google', 'Black Ops One', 'regular', '400'),
(208, 'google', 'Dawning of a New Day', 'regular', '400'),
(209, 'google', 'Buda', '300', '300'),
(210, 'google', 'Kenia', 'regular', '400'),
(211, 'google', 'Cookie', '400', '400'),
(212, 'google', 'UnifrakturCook', 'bold', '700'),
(213, 'google', 'Voltaire', '400', '400'),
(214, 'google', 'Caudex', '400,italic,700,700italic', '400,400-italic,700,700-italic'),
(215, 'google', 'Rationale', 'regular', '400'),
(216, 'google', 'Gentium Book Basic', 'regular,italic,bold,bolditalic', '400,400-italic,700,700-italic'),
(217, 'google', 'Nova Round', 'regular', '400'),
(218, 'google', 'IM Fell Great Primer', 'regular,italic', '400,400-italic'),
(219, 'google', 'Short Stack', '400', '400'),
(220, 'google', 'Federo', 'regular', '400'),
(221, 'google', 'Tenor Sans', 'regular', '400'),
(222, 'google', 'Julee', 'regular', '400'),
(223, 'google', 'Vibur', 'regular', '400'),
(224, 'google', 'Nova Slim', 'regular', '400'),
(225, 'google', 'IM Fell French Canon', 'regular,italic', '400,400-italic'),
(226, 'google', 'Loved by the King', 'regular', '400'),
(227, 'google', 'Viga', '400', '400'),
(228, 'google', 'Gochi Hand', '400', '400'),
(229, 'google', 'Holtwood One SC', 'regular', '400'),
(230, 'google', 'Zeyada', 'regular', '400'),
(231, 'google', 'Contrail One', 'regular', '400'),
(232, 'google', 'Vidaloka', '400', '400'),
(233, 'google', 'Nova Oval', 'regular', '400'),
(234, 'google', 'Montserrat', '400', '400'),
(235, 'google', 'Meddon', 'regular', '400'),
(236, 'google', 'Swanky and Moo Moo', 'regular', '400'),
(237, 'google', 'Nova Script', 'regular', '400'),
(238, 'google', 'Ovo', 'regular', '400'),
(239, 'google', 'Irish Grover', 'regular', '400'),
(240, 'google', 'League Script', '400', '400'),
(241, 'google', 'Petrona', '400', '400'),
(242, 'google', 'Yeseva One', 'regular', '400'),
(243, 'google', 'Squada One', '400', '400'),
(244, 'google', 'Numans', '400', '400'),
(245, 'google', 'Prata', '400', '400'),
(246, 'google', 'Gravitas One', 'regular', '400'),
(247, 'google', 'IM Fell Double Pica', 'regular,italic', '400,400-italic'),
(248, 'google', 'Prociono', '400', '400'),
(249, 'google', 'Astloch', 'regular,bold', '400,700'),
(250, 'google', 'Kelly Slab', 'regular', '400'),
(251, 'google', 'Asset', 'regular', '400'),
(252, 'google', 'Nova Flat', 'regular', '400'),
(253, 'google', 'Judson', '400,400italic,700', '400,400-italic,700'),
(254, 'google', 'Lusitana', '400,bold', '400,700'),
(255, 'google', 'Radley', 'regular,400italic', '400,400-italic'),
(256, 'google', 'Abril Fatface', '400', '400'),
(257, 'google', 'GFS Neohellenic', 'regular,italic,bold,bolditalic', '400,400-italic,700,700-italic'),
(258, 'google', 'Cambo', '400', '400'),
(259, 'google', 'Arapey', '400,400italic', '400,400-italic'),
(260, 'google', 'Tulpen One', 'regular', '400'),
(261, 'google', 'Convergence', '400', '400'),
(262, 'google', 'Rammetto One', '400', '400'),
(263, 'google', 'Alike', 'regular', '400'),
(264, 'google', 'Esteban', '400', '400'),
(265, 'google', 'Modern Antiqua', 'regular', '400'),
(266, 'google', 'Tienne', '400,700,900', '400,700,900'),
(267, 'google', 'Megrim', 'regular', '400'),
(268, 'google', 'Give You Glory', 'regular', '400'),
(269, 'google', 'Monoton', '400', '400'),
(270, 'google', 'Unna', 'regular', '400'),
(271, 'google', 'Mate SC', '400', '400'),
(272, 'google', 'Devonshire', '400', '400'),
(273, 'google', 'Electrolize', '400', '400'),
(274, 'google', 'Geostar', 'regular', '400'),
(275, 'google', 'Andada', '400', '400'),
(276, 'google', 'Handlee', '400', '400'),
(277, 'google', 'Bowlby One', 'regular', '400'),
(278, 'google', 'Wallpoet', 'regular', '400'),
(279, 'google', 'Suwannaphum', 'regular', '400'),
(280, 'google', 'Fanwood Text', '400,400italic', '400,400-italic'),
(281, 'google', 'Sofia', '400', '400'),
(282, 'google', 'Goblin One', 'regular', '400'),
(283, 'google', 'GFS Didot', 'regular', '400'),
(284, 'google', 'Miltonian', 'regular', '400'),
(285, 'google', 'Fjord One', '400', '400'),
(286, 'google', 'Sniglet', '800', '800'),
(287, 'google', 'Lancelot', '400', '400'),
(288, 'google', 'Ruslan Display', 'regular', '400'),
(289, 'google', 'Nova Cut', 'regular', '400'),
(290, 'google', 'Bigshot One', 'regular', '400'),
(291, 'google', 'Duru Sans', '400', '400'),
(292, 'google', 'Nova Mono', 'regular', '400'),
(293, 'google', 'Vast Shadow', 'regular', '400'),
(294, 'google', 'Dorsa', '400', '400'),
(295, 'google', 'Sigmar One', 'regular', '400'),
(296, 'google', 'Nova Square', 'regular', '400'),
(297, 'google', 'Alike Angular', 'regular', '400'),
(298, 'google', 'Linden Hill', '400,400italic', '400,400-italic'),
(299, 'google', 'Monofett', 'regular', '400'),
(300, 'google', 'Patua One', '400', '400'),
(301, 'google', 'Passero One', 'regular', '400'),
(302, 'google', 'Baumans', '400', '400'),
(303, 'google', 'Atomic Age', '400', '400'),
(304, 'google', 'Bad Script', '400', '400'),
(305, 'google', 'Poller One', 'regular', '400'),
(306, 'google', 'Supermercado One', '400', '400'),
(307, 'google', 'Geostar Fill', 'regular', '400'),
(308, 'google', 'Smokum', 'regular', '400'),
(309, 'google', 'Federant', '400', '400'),
(310, 'google', 'Engagement', '400', '400'),
(311, 'google', 'Aubrey', 'regular', '400'),
(312, 'google', 'Boogaloo', 'regular', '400'),
(313, 'google', 'Alfa Slab One', '400', '400'),
(314, 'google', 'Ribeye', '400', '400'),
(315, 'google', 'Signika Negative', '300,400,600,700', '300,400,600,700'),
(316, 'google', 'Quantico', '400,400italic,700,700italic', '400,400-italic,700,700-italic'),
(317, 'google', 'Ruluko', '400', '400'),
(318, 'google', 'Niconne', 'regular', '400'),
(319, 'google', 'Bree Serif', '400', '400'),
(320, 'google', 'Mr Dafoe', '400', '400'),
(321, 'google', 'Crete Round', '400,400italic', '400,400-italic'),
(322, 'google', 'Marmelad', '400', '400'),
(323, 'google', 'Italianno', '400', '400'),
(324, 'google', 'Fredericka the Great', 'regular', '400'),
(325, 'google', 'Trade Winds', '400', '400'),
(326, 'google', 'Magra', '400,bold', '400,700'),
(327, 'google', 'Iceland', '400', '400'),
(328, 'google', 'Stint Ultra Condensed', '400', '400'),
(329, 'google', 'Chelsea Market', '400', '400'),
(330, 'google', 'Bubblegum Sans', '400', '400'),
(331, 'google', 'Trykker', '400', '400'),
(332, 'google', 'Acme', '400', '400'),
(333, 'google', 'Overlock', '400,400italic,700,700italic,900,900italic', '400,400-italic,700,700-italic,900,900italic'),
(334, 'google', 'Armata', '400', '400'),
(335, 'google', 'Playball', '400', '400'),
(336, 'google', 'Habibi', '400', '400'),
(337, 'google', 'Oldenburg', '400', '400'),
(338, 'google', 'Galdeano', '400', '400'),
(339, 'google', 'Dynalight', '400', '400'),
(340, 'google', 'Enriqueta', '400,700', '400,700'),
(341, 'google', 'Concert One', '400', '400'),
(342, 'google', 'Overlock SC', '400', '400'),
(343, 'google', 'Noticia Text', '400,400italic,700,700italic', '400,400-italic,700,700-italic'),
(344, 'google', 'Righteous', '400', '400'),
(345, 'google', 'Cagliostro', '400', '400'),
(346, 'google', 'Arizonia', '400', '400'),
(347, 'google', 'Rouge Script', '400', '400'),
(348, 'google', 'Knewave', '400', '400'),
(349, 'google', 'Miniver', '400', '400'),
(350, 'google', 'Qwigley', '400', '400'),
(351, 'google', 'Flamenco', '300,400', '300,400'),
(352, 'google', 'Asul', '400,bold', '400,700'),
(353, 'google', 'Bokor', 'regular', '400'),
(354, 'google', 'Monsieur La Doulaise', '400', '400'),
(355, 'google', 'Gudea', '400,italic,bold', '400,400-italic,700'),
(356, 'google', 'Flavors', '400', '400'),
(357, 'google', 'Ruda', '400,bold,900', '400,700,900'),
(358, 'google', 'Stoke', '400', '400'),
(359, 'google', 'Spirax', '400', '400'),
(360, 'google', 'Uncial Antiqua', '400', '400'),
(361, 'google', 'Telex', '400', '400'),
(362, 'google', 'Alex Brush', '400', '400'),
(363, 'google', 'Yesteryear', '400', '400'),
(364, 'google', 'Fresca', '400', '400'),
(365, 'google', 'Original Surfer', '400', '400'),
(366, 'google', 'Buenard', '400,bold', '400,700'),
(367, 'google', 'Medula One', '400', '400'),
(368, 'google', 'Ruthie', '400', '400'),
(369, 'google', 'Bilbo', '400', '400'),
(370, 'google', 'Basic', '400', '400'),
(371, 'google', 'Nosifer', '400', '400'),
(372, 'google', 'Fondamento', '400,400italic', '400,400-italic'),
(373, 'google', 'Mrs Sheppards', '400', '400'),
(374, 'google', 'Marko One', '400', '400'),
(375, 'google', 'Caesar Dressing', '400', '400'),
(376, 'google', 'Lemon', '400', '400'),
(377, 'google', 'Metal', 'regular', '400'),
(378, 'google', 'Moulpali', 'regular', '400'),
(379, 'google', 'Balthazar', '400', '400'),
(380, 'google', 'Chicle', '400', '400'),
(381, 'google', 'Spicy Rice', '400', '400'),
(382, 'google', 'Almendra', '400,bold', '400,700'),
(383, 'google', 'Frijole', '400', '400'),
(384, 'google', 'Bilbo Swash Caps', '400', '400'),
(385, 'google', 'Ribeye Marrow', '400', '400'),
(386, 'google', 'Sail', '400', '400'),
(387, 'google', 'Battambang', 'regular,bold', '400,700'),
(388, 'google', 'Wellfleet', '400', '400'),
(389, 'google', 'Jim Nightshade', '400', '400'),
(390, 'google', 'Piedra', '400', '400'),
(391, 'google', 'Eater', '400', '400'),
(392, 'google', 'Belgrano', '400', '400'),
(393, 'google', 'Fugaz One', '400', '400'),
(394, 'google', 'Creepster', 'regular', '400'),
(395, 'google', 'Content', 'regular,bold', '400,700'),
(396, 'google', 'Homenaje', '400', '400'),
(397, 'google', 'Titan One', '400', '400'),
(398, 'google', 'Aguafina Script', '400', '400'),
(399, 'google', 'Fascinate Inline', '400', '400'),
(400, 'google', 'Dr Sugiyama', '400', '400'),
(401, 'google', 'Metamorphous', '400', '400'),
(402, 'google', 'Angkor', 'regular', '400'),
(403, 'google', 'Inika', '400,bold', '400,700'),
(404, 'google', 'Ruge Boogie', '400', '400'),
(405, 'google', 'Alegreya SC', '400,400italic,700,700italic,900,900italic', '400,400-italic,700,700-italic,900,900italic'),
(406, 'google', 'Alegreya', '400,400italic,700,700italic,900,900italic', '400,400-italic,700,700-italic,900,900italic'),
(407, 'google', 'Sarina', '400', '400'),
(408, 'google', 'Lustria', '400', '400'),
(409, 'google', 'Chango', '400', '400'),
(410, 'google', 'Dangrek', 'regular', '400'),
(411, 'google', 'Unlock', 'regular', '400'),
(412, 'google', 'Sonsie One', '400', '400'),
(413, 'google', 'Arbutus', '400', '400'),
(414, 'google', 'Mr De Haviland', '400', '400'),
(415, 'google', 'Plaster', '400', '400'),
(416, 'google', 'Miss Fajardose', '400', '400'),
(417, 'google', 'Amethysta', '400', '400'),
(418, 'google', 'Khmer', 'regular', '400'),
(419, 'google', 'Macondo Swash Caps', '400', '400'),
(420, 'google', 'Fascinate', '400', '400'),
(421, 'google', 'Trochut', '400,italic,bold', '400,400-italic,700'),
(422, 'google', 'Junge', '400', '400'),
(423, 'google', 'Herr Von Muellerhoff', '400', '400'),
(424, 'google', 'Bayon', 'regular', '400'),
(425, 'google', 'Preahvihear', 'regular', '400'),
(426, 'google', 'Ceviche One', '400', '400'),
(427, 'google', 'Freehand', 'regular', '400'),
(428, 'google', 'Nokora', '400,700', '400,700'),
(429, 'google', 'Bonbon', '400', '400'),
(430, 'google', 'Almendra SC', '400', '400'),
(431, 'google', 'Moul', 'regular', '400'),
(432, 'google', 'Sirin Stencil', '400', '400'),
(433, 'google', 'Germania One', '400', '400'),
(434, 'google', 'Montaga', '400', '400'),
(435, 'google', 'Odor Mean Chey', 'regular', '400'),
(436, 'google', 'Macondo', '400', '400'),
(437, 'google', 'Chenla', 'regular', '400'),
(438, 'google', 'Siemreap', 'regular', '400'),
(439, 'google', 'Taprom', 'regular', '400'),
(440, 'google', 'Port Lligat Sans', '400', '400'),
(441, 'google', 'Port Lligat Slab', '400', '400'),
(442, 'google', 'Koulen', 'regular', '400'),
(443, 'google', 'Emblema One', '400', '400'),
(444, 'google', 'Butcherman', '400', '400');

-- --------------------------------------------------------

--
-- Table structure for table `ap_forms`
--

CREATE TABLE `ap_forms` (
  `form_id` int(11) NOT NULL AUTO_INCREMENT,
  `form_name` text,
  `form_description` text,
  `form_email` varchar(255) DEFAULT NULL,
  `form_redirect` text,
  `form_success_message` text,
  `form_password` varchar(100) DEFAULT NULL,
  `form_unique_ip` int(11) NOT NULL DEFAULT '0',
  `form_frame_height` int(11) DEFAULT NULL,
  `form_has_css` int(11) NOT NULL DEFAULT '0',
  `form_captcha` int(11) NOT NULL DEFAULT '0',
  `form_active` int(11) NOT NULL DEFAULT '1',
  `form_review` int(11) NOT NULL DEFAULT '0',
  `form_default_application` int(11) NOT NULL DEFAULT '0',
  `esl_from_name` text,
  `esl_from_email_address` varchar(255) DEFAULT NULL,
  `esl_subject` text,
  `esl_content` text,
  `esl_plain_text` int(11) NOT NULL DEFAULT '0',
  `esr_email_address` varchar(255) DEFAULT NULL,
  `esr_from_name` text,
  `esr_from_email_address` varchar(255) DEFAULT NULL,
  `esr_subject` text,
  `esr_content` text,
  `esr_plain_text` int(11) NOT NULL DEFAULT '0',
  `form_tags` varchar(255) DEFAULT NULL,
  `form_redirect_enable` int(1) NOT NULL DEFAULT '0',
  `form_captcha_type` char(1) NOT NULL DEFAULT 'r',
  `form_theme_id` int(11) NOT NULL DEFAULT '0',
  `form_resume_enable` int(1) NOT NULL DEFAULT '0',
  `form_limit_enable` int(1) NOT NULL DEFAULT '0',
  `form_limit` int(11) NOT NULL DEFAULT '0',
  `form_label_alignment` varchar(11) NOT NULL DEFAULT 'top_label',
  `form_language` varchar(50) DEFAULT NULL,
  `form_page_total` int(11) NOT NULL DEFAULT '1',
  `form_lastpage_title` varchar(255) DEFAULT NULL,
  `form_submit_primary_text` varchar(255) NOT NULL DEFAULT 'Submit',
  `form_submit_secondary_text` varchar(255) NOT NULL DEFAULT 'Previous',
  `form_submit_primary_img` varchar(255) DEFAULT NULL,
  `form_submit_secondary_img` varchar(255) DEFAULT NULL,
  `form_submit_use_image` int(1) NOT NULL DEFAULT '0',
  `form_review_primary_text` varchar(255) NOT NULL DEFAULT 'Submit',
  `form_review_secondary_text` varchar(255) NOT NULL DEFAULT 'Previous',
  `form_review_primary_img` varchar(255) DEFAULT NULL,
  `form_review_secondary_img` varchar(255) DEFAULT NULL,
  `form_review_use_image` int(11) NOT NULL DEFAULT '0',
  `form_review_title` text,
  `form_review_description` text,
  `form_pagination_type` varchar(11) NOT NULL DEFAULT 'steps',
  `form_schedule_enable` int(1) NOT NULL DEFAULT '0',
  `form_schedule_start_date` date DEFAULT NULL,
  `form_schedule_end_date` date DEFAULT NULL,
  `form_schedule_start_hour` time DEFAULT NULL,
  `form_schedule_end_hour` time DEFAULT NULL,
  `esl_enable` tinyint(1) NOT NULL DEFAULT '0',
  `esr_enable` tinyint(1) NOT NULL DEFAULT '0',
  `payment_enable_merchant` int(1) NOT NULL DEFAULT '-1',
  `payment_merchant_type` varchar(25) NOT NULL DEFAULT 'paypal_standard',
  `payment_paypal_email` varchar(255) DEFAULT NULL,
  `payment_paypal_language` varchar(5) NOT NULL DEFAULT 'US',
  `payment_currency` varchar(5) NOT NULL DEFAULT 'USD',
  `payment_show_total` int(1) NOT NULL DEFAULT '0',
  `payment_total_location` varchar(11) NOT NULL DEFAULT 'top',
  `payment_enable_recurring` int(1) NOT NULL DEFAULT '0',
  `payment_recurring_cycle` int(11) NOT NULL DEFAULT '1',
  `payment_recurring_unit` varchar(5) NOT NULL DEFAULT 'month',
  `payment_price_type` varchar(11) NOT NULL DEFAULT 'fixed',
  `payment_price_amount` decimal(62,2) NOT NULL DEFAULT '0.00',
  `payment_price_name` varchar(255) DEFAULT NULL,
  `payment_enable_discount` int(11) DEFAULT NULL,
  `logic_field_enable` tinyint(1) NOT NULL DEFAULT '0',
  `logic_page_enable` tinyint(1) NOT NULL DEFAULT '0',
  `payment_enable_trial` int(1) NOT NULL DEFAULT '0',
  `payment_trial_period` int(11) NOT NULL DEFAULT '1',
  `payment_trial_unit` varchar(5) NOT NULL DEFAULT 'month',
  `payment_trial_amount` decimal(62,2) NOT NULL DEFAULT '0.00',
  `payment_pesapal_live_secret_key` varchar(50) DEFAULT NULL,
  `payment_pesapal_live_public_key` varchar(50) DEFAULT NULL,
  `payment_pesapal_test_secret_key` varchar(50) DEFAULT NULL,
  `payment_pesapal_test_public_key` varchar(50) DEFAULT NULL,
  `payment_pesapal_enable_test_mode` int(1) NOT NULL DEFAULT '0',
  `payment_paypal_enable_test_mode` int(1) NOT NULL DEFAULT '0',
  `payment_enable_invoice` int(1) NOT NULL DEFAULT '0',
  `payment_invoice_email` varchar(255) DEFAULT NULL,
  `payment_delay_notifications` int(1) NOT NULL DEFAULT '1',
  `payment_ask_billing` int(1) NOT NULL DEFAULT '0',
  `payment_ask_shipping` int(1) NOT NULL DEFAULT '0',
  `payment_discount_element_id` int(11) DEFAULT NULL,
  `form_disabled_message` text,
  `payment_enable_tax` int(1) NOT NULL DEFAULT '0',
  `payment_tax_rate` decimal(62,2) NOT NULL DEFAULT '0.00',
  `payment_discount_code` int(11) DEFAULT NULL,
  `webhook_enable` int(11) DEFAULT NULL,
  `payment_discount_type` int(11) DEFAULT NULL,
  `payment_discount_amount` int(11) DEFAULT NULL,
  `logic_email_enable` tinyint(1) NOT NULL DEFAULT '0',
  `form_stage` varchar(255) DEFAULT NULL,
  `form_group` int(11) DEFAULT NULL,
  `form_department` int(11) DEFAULT NULL,
  `form_department_stage` int(11) DEFAULT NULL,
  `form_idn` varchar(255) DEFAULT NULL,
  `form_type` int(11) DEFAULT NULL,
  `form_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`form_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1652 ;

ALTER TABLE `ap_forms` ADD `payment_tax_amount` DOUBLE NULL AFTER `payment_tax_rate` ;

ALTER TABLE `ap_forms` ADD `logic_workflow_enable` BOOLEAN NULL DEFAULT FALSE AFTER `form_id` ;

ALTER TABLE `ap_forms` ADD `logic_permission_enable` BOOLEAN NULL DEFAULT FALSE AFTER `form_id` ;

ALTER TABLE `ap_forms` ADD `payment_cellulant_checkout_url` varchar(50) DEFAULT NULL;
ALTER TABLE `ap_forms` ADD `payment_cellulant_merchant_username` varchar(50) DEFAULT NULL;
ALTER TABLE `ap_forms` ADD `payment_cellulant_merchant_password` varchar(50) DEFAULT NULL;
ALTER TABLE `ap_forms` ADD `payment_cellulant_service_id` varchar(50) DEFAULT NULL;

ALTER TABLE `ap_forms` ADD `payment_jambopay_business` varchar(50) DEFAULT NULL;
ALTER TABLE `ap_forms` ADD `payment_jambopay_shared_key` varchar(250) DEFAULT NULL;

--
-- Dumping data for table `ap_forms`
--

INSERT INTO `ap_forms` (`form_id`, `form_name`, `form_description`, `form_email`, `form_redirect`, `form_success_message`, `form_password`, `form_unique_ip`, `form_frame_height`, `form_has_css`, `form_captcha`, `form_active`, `form_review`, `form_default_application`, `esl_from_name`, `esl_from_email_address`, `esl_subject`, `esl_content`, `esl_plain_text`, `esr_email_address`, `esr_from_name`, `esr_from_email_address`, `esr_subject`, `esr_content`, `esr_plain_text`, `form_tags`, `form_redirect_enable`, `form_captcha_type`, `form_theme_id`, `form_resume_enable`, `form_limit_enable`, `form_limit`, `form_label_alignment`, `form_language`, `form_page_total`, `form_lastpage_title`, `form_submit_primary_text`, `form_submit_secondary_text`, `form_submit_primary_img`, `form_submit_secondary_img`, `form_submit_use_image`, `form_review_primary_text`, `form_review_secondary_text`, `form_review_primary_img`, `form_review_secondary_img`, `form_review_use_image`, `form_review_title`, `form_review_description`, `form_pagination_type`, `form_schedule_enable`, `form_schedule_start_date`, `form_schedule_end_date`, `form_schedule_start_hour`, `form_schedule_end_hour`, `esl_enable`, `esr_enable`, `payment_enable_merchant`, `payment_merchant_type`, `payment_paypal_email`, `payment_paypal_language`, `payment_currency`, `payment_show_total`, `payment_total_location`, `payment_enable_recurring`, `payment_recurring_cycle`, `payment_recurring_unit`, `payment_price_type`, `payment_price_amount`, `payment_price_name`, `payment_enable_discount`, `logic_field_enable`, `logic_page_enable`, `payment_enable_trial`, `payment_trial_period`, `payment_trial_unit`, `payment_trial_amount`, `payment_pesapal_live_secret_key`, `payment_pesapal_live_public_key`, `payment_pesapal_test_secret_key`, `payment_pesapal_test_public_key`, `payment_pesapal_enable_test_mode`, `payment_paypal_enable_test_mode`, `payment_enable_invoice`, `payment_invoice_email`, `payment_delay_notifications`, `payment_ask_billing`, `payment_ask_shipping`, `payment_discount_element_id`, `form_disabled_message`, `payment_enable_tax`, `payment_tax_rate`, `payment_discount_code`, `webhook_enable`, `payment_discount_type`, `payment_discount_amount`, `logic_email_enable`) VALUES
(6, 'Contact form', '', 'support@ecitizen.go.ke', '', 'Success! Your submission has been saved!', '', 0, 360, 1, 1, 1, 0, 0, 'PermitFlow', 'support@ecitizen.go.ke', '{form_name} [#{entry_no}]', '{entry_data}', 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 'r', 0, 0, 0, 0, 'top_label', NULL, 1, NULL, 'Submit', 'Previous', NULL, NULL, 0, 'Submit', 'Previous', NULL, NULL, 0, NULL, NULL, 'steps', 0, NULL, NULL, NULL, NULL, 1, 0, -1, 'paypal_standard', NULL, 'US', 'USD', 0, 'top', 0, 1, 'month', 'fixed', 0.00, NULL, NULL, 0, 0, 0, 1, 'month', 0.00, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, NULL, 'This form is currently inactive.', 0, 0.00, NULL, NULL, NULL, NULL, 0),
(7, 'Feedback Form', '', 'support@ecitizen.go.ke', '', 'Success! Your submission has been saved!', '', 0, 486, 1, 1, 1, 0, 0, 'PermitFlow', 'support@ecitizen.go.ke', '{form_name} [#{entry_no}]', '{entry_data}', 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 'r', 0, 0, 0, 0, 'top_label', NULL, 1, NULL, 'Submit', 'Previous', NULL, NULL, 0, 'Submit', 'Previous', NULL, NULL, 0, NULL, NULL, 'steps', 0, NULL, NULL, NULL, NULL, 1, 0, -1, 'paypal_standard', NULL, 'US', 'USD', 0, 'top', 0, 1, 'month', 'fixed', 0.00, NULL, NULL, 0, 0, 0, 1, 'month', 0.00, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, NULL, 'This form is currently inactive.', 0, 0.00, NULL, NULL, NULL, NULL, 0),
(15, 'Step 2: Client Particulars', 'Additional user information', NULL, '', 'Success! Your submission has been saved!', '', 0, 963, 0, 0, 1, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 'r', 0, 0, 0, 0, 'top_label', NULL, 1, NULL, 'Submit', 'Previous', NULL, NULL, 0, 'Submit', 'Previous', NULL, NULL, 0, NULL, NULL, 'steps', 0, NULL, NULL, NULL, NULL, 0, 0, -1, 'paypal_standard', NULL, 'US', 'USD', 0, 'top', 0, 1, 'month', 'fixed', 0.00, NULL, NULL, 0, 0, 0, 1, 'month', 0.00, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, NULL, 'This form is currently inactive.', 0, 0.00, NULL, NULL, NULL, NULL, 0),
(16, 'Online Payment', '<h4>Attach Proof of Payments</h4>', NULL, '', 'Success! Your payment has been made!', '', 0, 453, 0, 0, 1, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 'r', 0, 0, 0, 0, 'top_label', 'english', 1, NULL, 'Submit', 'Previous', NULL, NULL, 0, 'Submit', 'Previous', NULL, NULL, 0, NULL, NULL, 'steps', 0, NULL, NULL, '02:00:00', '02:00:00', 0, 0, -1, 'paypal_standard', NULL, 'US', 'USD', 0, 'top', 0, 1, 'month', 'fixed', 0.00, NULL, NULL, 0, 0, 0, 1, 'month', 0.00, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, NULL, 'This form is currently inactive.', 0, 0.00, NULL, NULL, NULL, NULL, 0),
(17, 'Attach Permit', '', NULL, '', 'Success! Your submission has been saved!', '', 0, 383, 0, 0, 1, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 'r', 0, 0, 0, 0, 'top_label', 'english', 1, NULL, 'Submit', 'Previous', NULL, NULL, 0, 'Submit', 'Previous', NULL, NULL, 0, '', NULL, 'steps', 0, NULL, NULL, '02:00:00', '02:00:00', 0, 0, -1, 'paypal_standard', NULL, 'US', 'USD', 0, 'top', 0, 1, 'month', 'fixed', 0.00, NULL, NULL, 0, 0, 0, 1, 'month', 0.00, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 0, 0, 0, NULL, 'This form is currently inactive.', 0, 0.00, NULL, NULL, NULL, NULL, 0);
-- --------------------------------------------------------

--
-- Table structure for table `ap_form_1`
--

CREATE TABLE `ap_form_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_updated` datetime DEFAULT NULL,
  `ip_address` varchar(15) DEFAULT NULL,
  `element_2` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_form_6`
--

CREATE TABLE `ap_form_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_updated` datetime DEFAULT NULL,
  `ip_address` varchar(15) DEFAULT NULL,
  `element_2` text,
  `element_3` text,
  `element_5` text,
  `element_4` text,
  `status` int(4) unsigned NOT NULL DEFAULT '1',
  `resume_key` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_form_7`
--

CREATE TABLE `ap_form_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_updated` datetime DEFAULT NULL,
  `ip_address` varchar(15) DEFAULT NULL,
  `element_1_1` varchar(255) DEFAULT NULL,
  `element_1_2` varchar(255) DEFAULT NULL,
  `element_2` text,
  `element_3` text,
  `status` int(4) unsigned NOT NULL DEFAULT '1',
  `resume_key` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_form_7_review`
--

CREATE TABLE `ap_form_7_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_updated` datetime DEFAULT NULL,
  `ip_address` varchar(15) DEFAULT NULL,
  `element_1_1` varchar(255) DEFAULT NULL,
  `element_1_2` varchar(255) DEFAULT NULL,
  `element_2` text,
  `element_3` text,
  `session_id` varchar(100) DEFAULT NULL,
  `status` int(4) unsigned NOT NULL DEFAULT '1',
  `resume_key` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_form_15`
--

CREATE TABLE `ap_form_15` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_updated` datetime DEFAULT NULL,
  `ip_address` varchar(15) DEFAULT NULL,
  `element_1` text,
  `element_3` text,
  `element_4` text,
  `element_6_1` varchar(255) DEFAULT NULL COMMENT ' - Street',
  `element_6_2` varchar(255) DEFAULT NULL COMMENT ' - Line 2',
  `element_6_3` varchar(255) DEFAULT NULL COMMENT ' - City',
  `element_6_4` varchar(255) DEFAULT NULL COMMENT ' - State/Province/Region',
  `element_6_5` varchar(255) DEFAULT NULL COMMENT ' - Zip/Postal Code',
  `element_6_6` varchar(255) DEFAULT NULL COMMENT ' - Country',
  `element_9` int(6) unsigned NOT NULL DEFAULT '0',
  `element_11` int(6) unsigned NOT NULL DEFAULT '0',
  `element_13` text,
  `element_14` text,
  `element_15` text,
  `element_16` text,
  `status` int(4) unsigned NOT NULL DEFAULT '1',
  `resume_key` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ap_form_15`
--

INSERT IGNORE INTO `ap_form_15` (`id`, `date_created`, `date_updated`, `ip_address`, `element_1`, `element_3`, `element_4`, `element_6_1`, `element_6_2`, `element_6_3`, `element_6_4`, `element_6_5`, `element_6_6`, `element_9`, `element_11`, `element_13`, `element_14`, `element_15`, `element_16`, `status`, `resume_key`) VALUES
(1, '2014-04-04 17:51:02', NULL, '127.0.0.1', 'Test', NULL, '25649245', NULL, NULL, NULL, NULL, NULL, NULL, 1, 2, NULL, NULL, NULL, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ap_form_16`
--

CREATE TABLE `ap_form_16` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_updated` datetime DEFAULT NULL,
  `ip_address` varchar(15) DEFAULT NULL,
  `element_6` text,
  `status` int(4) unsigned NOT NULL DEFAULT '1',
  `resume_key` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ap_form_16`
--

INSERT IGNORE INTO `ap_form_16` (`id`, `date_created`, `date_updated`, `ip_address`, `element_6`, `status`, `resume_key`) VALUES
(1, '2014-04-07 12:27:23', NULL, '127.0.0.1', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ap_form_17`
--

CREATE TABLE `ap_form_17` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_updated` datetime DEFAULT NULL,
  `ip_address` varchar(15) DEFAULT NULL,
  `element_1` text,
  `status` int(4) unsigned NOT NULL DEFAULT '1',
  `resume_key` varchar(10) DEFAULT NULL,
  `element_2` text COMMENT 'File Upload',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_form_elements`
--

CREATE TABLE `ap_form_elements` (
  `form_id` int(11) NOT NULL DEFAULT '0',
  `element_id` int(11) NOT NULL DEFAULT '0',
  `element_title` text,
  `element_guidelines` text,
  `element_size` varchar(6) NOT NULL DEFAULT 'medium',
  `element_is_required` int(11) NOT NULL DEFAULT '0',
  `element_is_unique` int(11) NOT NULL DEFAULT '0',
  `element_is_private` int(11) NOT NULL DEFAULT '0',
  `element_type` varchar(50) DEFAULT NULL,
  `element_position` int(11) NOT NULL DEFAULT '0',
  `element_default_value` text,
  `element_constraint` varchar(255) DEFAULT NULL,
  `element_total_child` int(11) NOT NULL DEFAULT '0',
  `element_css_class` varchar(255) NOT NULL DEFAULT '',
  `element_range_min` bigint(11) unsigned NOT NULL DEFAULT '0',
  `element_range_max` bigint(11) unsigned NOT NULL DEFAULT '0',
  `element_range_limit_by` char(1) NOT NULL,
  `element_status` int(1) NOT NULL DEFAULT '2',
  `element_choice_columns` int(1) NOT NULL DEFAULT '1',
  `element_choice_has_other` int(1) NOT NULL DEFAULT '0',
  `element_choice_other_label` text,
  `element_time_showsecond` int(11) NOT NULL DEFAULT '0',
  `element_time_24hour` int(11) NOT NULL DEFAULT '0',
  `element_address_hideline2` int(11) NOT NULL DEFAULT '0',
  `element_address_us_only` int(11) NOT NULL DEFAULT '0',
  `element_date_enable_range` int(1) NOT NULL DEFAULT '0',
  `element_date_range_min` date DEFAULT NULL,
  `element_date_range_max` date DEFAULT NULL,
  `element_date_enable_selection_limit` int(1) NOT NULL DEFAULT '0',
  `element_date_selection_max` int(11) NOT NULL DEFAULT '1',
  `element_date_past_future` char(1) NOT NULL DEFAULT 'p',
  `element_date_disable_past_future` int(1) NOT NULL DEFAULT '0',
  `element_date_disable_weekend` int(1) NOT NULL DEFAULT '0',
  `element_date_disable_specific` int(1) NOT NULL DEFAULT '0',
  `element_date_disabled_list` text CHARACTER SET utf8 COLLATE utf8_bin,
  `element_file_enable_type_limit` int(1) NOT NULL DEFAULT '1',
  `element_file_block_or_allow` char(1) NOT NULL DEFAULT 'b',
  `element_file_type_list` varchar(255) DEFAULT NULL,
  `element_file_as_attachment` int(1) NOT NULL DEFAULT '0',
  `element_file_enable_advance` int(1) NOT NULL DEFAULT '0',
  `element_file_auto_upload` int(1) NOT NULL DEFAULT '0',
  `element_file_enable_multi_upload` int(1) NOT NULL DEFAULT '0',
  `element_file_max_selection` int(11) NOT NULL DEFAULT '5',
  `element_file_enable_size_limit` int(1) NOT NULL DEFAULT '0',
  `element_file_size_max` int(11) DEFAULT NULL,
  `element_matrix_allow_multiselect` int(1) NOT NULL DEFAULT '0',
  `element_matrix_parent_id` int(11) NOT NULL DEFAULT '0',
  `element_submit_use_image` int(1) NOT NULL DEFAULT '0',
  `element_submit_primary_text` varchar(255) NOT NULL DEFAULT 'Continue',
  `element_submit_secondary_text` varchar(255) NOT NULL DEFAULT 'Previous',
  `element_submit_primary_img` varchar(255) DEFAULT NULL,
  `element_submit_secondary_img` varchar(255) DEFAULT NULL,
  `element_page_title` varchar(255) DEFAULT NULL,
  `element_page_number` int(11) NOT NULL DEFAULT '1',
  `element_section_display_in_email` int(1) NOT NULL DEFAULT '0',
  `element_section_enable_scroll` int(1) NOT NULL DEFAULT '0',
  `element_number_enable_quantity` int(1) NOT NULL DEFAULT '0',
  `element_number_quantity_link` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`form_id`,`element_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `ap_form_elements` ADD `element_table_name` VARCHAR(200) NULL , ADD `element_field_value` TEXT NULL , ADD `element_field_name` VARCHAR(200) NULL , ADD `element_option_query` TEXT NULL ;
ALTER TABLE `ap_form_elements` ADD `element_existing_form` int(1) NULL , ADD `element_existing_stage` int(1) NULL;
ALTER TABLE  `ap_form_elements` ADD  `element_remote_username` VARCHAR( 250 ) NULL ,
ADD  `element_remote_password` VARCHAR( 250 ) NULL ;
ALTER TABLE  `ap_form_elements` ADD  `element_remote_value` TEXT NULL;
ALTER TABLE  `ap_form_elements` ADD  `element_remote_server_field` TEXT NULL;
ALTER TABLE  `ap_form_elements` ADD  `element_jsondef` TEXT NULL ;
ALTER TABLE  `ap_form_elements` ADD  `element_remote_post` TEXT NULL ;
ALTER TABLE  `ap_form_elements` ADD  `element_price_class` TEXT NULL ;
ALTER TABLE  `ap_form_elements` ADD  `element_field_error_message` TEXT NULL ;

--
-- Dumping data for table `ap_form_elements`
--

INSERT IGNORE INTO `ap_form_elements` (`form_id`, `element_id`, `element_title`, `element_guidelines`, `element_size`, `element_is_required`, `element_is_unique`, `element_is_private`, `element_type`, `element_position`, `element_default_value`, `element_constraint`, `element_total_child`, `element_css_class`, `element_range_min`, `element_range_max`, `element_range_limit_by`, `element_status`, `element_choice_columns`, `element_choice_has_other`, `element_choice_other_label`, `element_time_showsecond`, `element_time_24hour`, `element_address_hideline2`, `element_address_us_only`, `element_date_enable_range`, `element_date_range_min`, `element_date_range_max`, `element_date_enable_selection_limit`, `element_date_selection_max`, `element_date_past_future`, `element_date_disable_past_future`, `element_date_disable_weekend`, `element_date_disable_specific`, `element_date_disabled_list`, `element_file_enable_type_limit`, `element_file_block_or_allow`, `element_file_type_list`, `element_file_as_attachment`, `element_file_enable_advance`, `element_file_auto_upload`, `element_file_enable_multi_upload`, `element_file_max_selection`, `element_file_enable_size_limit`, `element_file_size_max`, `element_matrix_allow_multiselect`, `element_matrix_parent_id`, `element_submit_use_image`, `element_submit_primary_text`, `element_submit_secondary_text`, `element_submit_primary_img`, `element_submit_secondary_img`, `element_page_title`, `element_page_number`, `element_section_display_in_email`, `element_section_enable_scroll`, `element_number_enable_quantity`, `element_number_quantity_link`) VALUES
(6, 2, 'Email', '', 'medium', 0, 0, 0, 'email', 1, '', '', 0, '', 0, 0, '', 1, 1, 0, NULL, 0, 0, 0, 0, 0, NULL, NULL, 0, 1, 'p', 0, 0, 0, NULL, 1, 'b', NULL, 0, 0, 0, 0, 5, 0, NULL, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL),
(6, 3, 'Your Message', '', 'small', 0, 0, 0, 'textarea', 2, '', '', 0, '', 0, 0, '', 1, 1, 0, NULL, 0, 0, 0, 0, 0, NULL, NULL, 0, 1, 'p', 0, 0, 0, NULL, 1, 'b', NULL, 0, 0, 0, 0, 5, 0, NULL, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL),
(6, 4, 'Name', '', 'medium', 0, 0, 0, 'text', 0, '', '', 0, '', 0, 0, '', 1, 1, 0, NULL, 0, 0, 0, 0, 0, NULL, NULL, 0, 1, 'p', 0, 0, 0, NULL, 1, 'b', NULL, 0, 0, 0, 0, 5, 0, NULL, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL),
(7, 1, 'Name <br>', '', 'small', 1, 0, 0, 'simple_name', 0, '', '', 1, '', 0, 0, '', 1, 1, 0, NULL, 0, 0, 0, 0, 0, NULL, NULL, 0, 1, 'p', 0, 0, 0, NULL, 1, 'b', NULL, 0, 0, 0, 0, 5, 0, NULL, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL),
(7, 2, 'Email', '', 'medium', 0, 0, 0, 'email', 1, '', '', 0, '', 0, 0, '', 1, 1, 0, NULL, 0, 0, 0, 0, 0, NULL, NULL, 0, 1, 'p', 0, 0, 0, NULL, 1, 'b', NULL, 0, 0, 0, 0, 5, 0, NULL, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL),
(7, 3, 'Your Msg', '', 'medium', 0, 0, 0, 'textarea', 2, '', '', 0, '', 0, 0, '', 1, 1, 0, NULL, 0, 0, 0, 0, 0, NULL, NULL, 0, 1, 'p', 0, 0, 0, NULL, 1, 'b', NULL, 0, 0, 0, 0, 5, 0, NULL, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL),
(15, 1, 'If Firm, What is the Business Name', '', 'medium', 0, 0, 0, 'text', 1, '', '', 0, '', 0, 0, '', 1, 1, 0, NULL, 0, 0, 0, 0, 0, NULL, NULL, 0, 1, 'p', 0, 0, 0, NULL, 1, 'b', NULL, 0, 0, 0, 0, 5, 0, NULL, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL),
(15, 3, 'TIN', '', 'medium', 0, 0, 0, 'text', 2, '', '', 0, '', 0, 0, '', 1, 1, 0, NULL, 0, 0, 0, 0, 0, NULL, NULL, 0, 1, 'p', 0, 0, 0, NULL, 1, 'b', NULL, 0, 0, 0, 0, 5, 0, NULL, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL),
(15, 4, 'ID Number', '', 'medium', 0, 0, 0, 'text', 3, '', '', 0, '', 0, 0, '', 1, 1, 0, NULL, 0, 0, 0, 0, 0, NULL, NULL, 0, 1, 'p', 0, 0, 0, NULL, 1, 'b', NULL, 0, 0, 0, 0, 5, 0, NULL, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL),
(15, 6, 'Address', '', 'large', 0, 0, 0, 'address', 5, '', '', 5, '', 0, 0, '', 1, 1, 0, NULL, 0, 0, 0, 0, 0, NULL, NULL, 0, 1, 'p', 0, 0, 0, NULL, 1, 'b', NULL, 0, 0, 0, 0, 5, 0, NULL, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL),
(15, 9, 'Preferred Mode Of Notification', '', 'medium', 0, 0, 0, 'select', 9, '', '', 2, '', 0, 0, '', 1, 1, 0, NULL, 0, 0, 0, 0, 0, NULL, NULL, 0, 1, 'p', 0, 0, 0, NULL, 1, 'b', NULL, 0, 0, 0, 0, 5, 0, NULL, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL),
(15, 11, 'Register as', '', 'medium', 0, 0, 0, 'select', 0, '', '', 1, '', 0, 0, '', 1, 1, 0, NULL, 0, 0, 0, 0, 0, NULL, NULL, 0, 1, 'p', 0, 0, 0, NULL, 1, 'b', NULL, 0, 0, 0, 0, 5, 0, NULL, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL),
(15, 13, 'District', '', 'medium', 0, 0, 0, 'text', 6, '', '', 0, '', 0, 0, '', 1, 1, 0, NULL, 0, 0, 0, 0, 0, NULL, NULL, 0, 1, 'p', 0, 0, 0, NULL, 1, 'b', NULL, 0, 0, 0, 0, 5, 0, NULL, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL),
(15, 14, 'Sector', '', 'medium', 0, 0, 0, 'text', 7, '', '', 0, '', 0, 0, '', 1, 1, 0, NULL, 0, 0, 0, 0, 0, NULL, NULL, 0, 1, 'p', 0, 0, 0, NULL, 1, 'b', NULL, 0, 0, 0, 0, 5, 0, NULL, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL),
(15, 15, 'Cell', '', 'medium', 0, 0, 0, 'text', 8, '', '', 0, '', 0, 0, '', 1, 1, 0, NULL, 0, 0, 0, 0, 0, NULL, NULL, 0, 1, 'p', 0, 0, 0, NULL, 1, 'b', NULL, 0, 0, 0, 0, 5, 0, NULL, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL),
(15, 16, 'Passport Number', '', 'medium', 0, 0, 0, 'text', 4, '', '', 0, '', 0, 0, '', 1, 1, 0, NULL, 0, 0, 0, 0, 0, NULL, NULL, 0, 1, 'p', 0, 0, 0, NULL, 1, 'b', NULL, 0, 0, 0, 0, 5, 0, NULL, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL),
(16, 4, '<div align=''center''><img src=''/assets_frontend/images/MFSlogo.png'' width="100px"> <br>Online mobile money payment coming soon!</div>', '', 'medium', 0, 0, 0, 'section', 0, '', '', 0, '', 0, 0, '', 1, 1, 0, NULL, 0, 0, 0, 0, 0, NULL, NULL, 0, 1, 'p', 0, 0, 0, NULL, 1, 'b', NULL, 0, 0, 0, 0, 5, 0, NULL, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL),
(16, 6, 'Receipt', '', 'medium', 0, 0, 0, 'file', 2, '', '', 0, '', 0, 0, '', 1, 1, 0, NULL, 0, 0, 0, 0, 0, NULL, NULL, 0, 1, 'p', 0, 0, 0, NULL, 1, 'b', '', 0, 1, 1, 1, 5, 0, 0, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL),
(16, 8, 'Attached scanned receipts below.', '', 'medium', 0, 0, 0, 'section', 1, '', '', 0, '', 0, 0, '', 1, 1, 0, NULL, 0, 0, 0, 0, 0, NULL, NULL, 0, 1, 'p', 0, 0, 0, NULL, 1, 'b', NULL, 0, 0, 0, 0, 5, 0, NULL, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL),
(17, 1, 'Permit', '', 'medium', 0, 0, 0, 'file', 0, '', '', 0, '', 0, 0, '', 1, 1, 0, NULL, 0, 0, 0, 0, 0, NULL, NULL, 0, 1, 'p', 0, 0, 0, NULL, 1, 'b', NULL, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL),
(17, 2, 'Additional file', '', 'medium', 0, 0, 0, 'file', 1, '', '', 0, '', 0, 0, 'c', 1, 1, 0, NULL, 0, 0, 0, 0, 0, '0000-00-00', '0000-00-00', 0, 1, 'p', 0, 0, 0, '', 1, 'b', 'php,php3,php4,php5,phtml,exe,pl,cgi,html,htm,js', 0, 0, 1, 1, 5, 0, 2, 0, 0, 0, 'Continue', 'Previous', NULL, NULL, NULL, 1, 0, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ap_form_filters`
--

CREATE TABLE `ap_form_filters` (
  `aff_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `element_name` varchar(50) NOT NULL DEFAULT '',
  `filter_condition` varchar(15) NOT NULL DEFAULT 'is',
  `filter_keyword` varchar(255) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`aff_id`),
  KEY `form_id` (`form_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_form_groups`
--

CREATE TABLE `ap_form_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ap_form_groups_form_id_idx` (`form_id`),
  KEY `ap_form_groups_group_id_idx` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=152 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_form_locks`
--

CREATE TABLE `ap_form_locks` (
  `form_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lock_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_form_locks`
--

INSERT IGNORE INTO `ap_form_locks` (`form_id`, `user_id`, `lock_date`) VALUES
(16, 1, '2013-07-23 18:35:49'),
(6, 1, '2014-02-11 10:10:37'),
(1177, 1, '2013-08-06 06:45:37'),
(61, 1, '2014-01-29 08:36:16'),
(27, 1, '2013-09-09 18:30:25'),
(29, 1, '2013-09-09 18:35:08'),
(17, 1, '2013-08-22 08:19:49'),
(24, 1, '2014-01-15 18:06:30'),
(46, 1, '2013-08-26 23:57:58'),
(871, 1, '2014-04-03 18:04:54');

-- --------------------------------------------------------

--
-- Table structure for table `ap_form_payments`
--

CREATE TABLE `ap_form_payments` (
  `afp_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` int(11) unsigned NOT NULL,
  `record_id` int(11) unsigned NOT NULL,
  `payment_id` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `payment_status` varchar(255) DEFAULT NULL,
  `payment_fullname` varchar(255) DEFAULT NULL,
  `payment_amount` decimal(62,2) NOT NULL DEFAULT '0.00',
  `payment_currency` varchar(3) NOT NULL DEFAULT 'usd',
  `payment_test_mode` int(1) NOT NULL DEFAULT '0',
  `payment_merchant_type` varchar(25) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `billing_street` varchar(255) DEFAULT NULL,
  `billing_city` varchar(255) DEFAULT NULL,
  `billing_state` varchar(255) DEFAULT NULL,
  `billing_zipcode` varchar(255) DEFAULT NULL,
  `billing_country` varchar(255) DEFAULT NULL,
  `same_shipping_address` int(1) NOT NULL DEFAULT '1',
  `shipping_street` varchar(255) DEFAULT NULL,
  `shipping_city` varchar(255) DEFAULT NULL,
  `shipping_state` varchar(255) DEFAULT NULL,
  `shipping_zipcode` varchar(255) DEFAULT NULL,
  `shipping_country` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`afp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_form_sorts`
--

CREATE TABLE `ap_form_sorts` (
  `user_id` int(11) NOT NULL,
  `sort_by` varchar(25) DEFAULT '',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ap_form_sorts`
--

INSERT IGNORE INTO `ap_form_sorts` (`user_id`, `sort_by`) VALUES
(1, 'form_title');

-- --------------------------------------------------------

--
-- Table structure for table `ap_form_themes`
--

CREATE TABLE `ap_form_themes` (
  `theme_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(1) DEFAULT '1',
  `theme_has_css` int(1) NOT NULL DEFAULT '0',
  `theme_name` varchar(255) DEFAULT '',
  `theme_built_in` int(1) NOT NULL DEFAULT '0',
  `logo_type` varchar(11) NOT NULL DEFAULT 'default' COMMENT 'default,custom,disabled',
  `logo_custom_image` text,
  `logo_custom_height` int(11) NOT NULL DEFAULT '40',
  `logo_default_image` varchar(50) DEFAULT '',
  `logo_default_repeat` int(1) NOT NULL DEFAULT '0',
  `wallpaper_bg_type` varchar(11) NOT NULL DEFAULT 'color' COMMENT 'color,pattern,custom',
  `wallpaper_bg_color` varchar(11) DEFAULT '',
  `wallpaper_bg_pattern` varchar(50) DEFAULT '',
  `wallpaper_bg_custom` text,
  `header_bg_type` varchar(11) NOT NULL DEFAULT 'color' COMMENT 'color,pattern,custom',
  `header_bg_color` varchar(11) DEFAULT '',
  `header_bg_pattern` varchar(50) DEFAULT '',
  `header_bg_custom` text,
  `form_bg_type` varchar(11) NOT NULL DEFAULT 'color' COMMENT 'color,pattern,custom',
  `form_bg_color` varchar(11) DEFAULT '',
  `form_bg_pattern` varchar(50) DEFAULT '',
  `form_bg_custom` text,
  `highlight_bg_type` varchar(11) NOT NULL DEFAULT 'color' COMMENT 'color,pattern,custom',
  `highlight_bg_color` varchar(11) DEFAULT '',
  `highlight_bg_pattern` varchar(50) DEFAULT '',
  `highlight_bg_custom` text,
  `guidelines_bg_type` varchar(11) NOT NULL DEFAULT 'color' COMMENT 'color,pattern,custom',
  `guidelines_bg_color` varchar(11) DEFAULT '',
  `guidelines_bg_pattern` varchar(50) DEFAULT '',
  `guidelines_bg_custom` text,
  `field_bg_type` varchar(11) NOT NULL DEFAULT 'color' COMMENT 'color,pattern,custom',
  `field_bg_color` varchar(11) DEFAULT '',
  `field_bg_pattern` varchar(50) DEFAULT '',
  `field_bg_custom` text,
  `form_title_font_type` varchar(50) NOT NULL DEFAULT 'Lucida Grande',
  `form_title_font_weight` int(11) NOT NULL DEFAULT '400',
  `form_title_font_style` varchar(25) NOT NULL DEFAULT 'normal',
  `form_title_font_size` varchar(11) DEFAULT '',
  `form_title_font_color` varchar(11) DEFAULT '',
  `form_desc_font_type` varchar(50) NOT NULL DEFAULT 'Lucida Grande',
  `form_desc_font_weight` int(11) NOT NULL DEFAULT '400',
  `form_desc_font_style` varchar(25) NOT NULL DEFAULT 'normal',
  `form_desc_font_size` varchar(11) DEFAULT '',
  `form_desc_font_color` varchar(11) DEFAULT '',
  `field_title_font_type` varchar(50) NOT NULL DEFAULT 'Lucida Grande',
  `field_title_font_weight` int(11) NOT NULL DEFAULT '400',
  `field_title_font_style` varchar(25) NOT NULL DEFAULT 'normal',
  `field_title_font_size` varchar(11) DEFAULT '',
  `field_title_font_color` varchar(11) DEFAULT '',
  `guidelines_font_type` varchar(50) NOT NULL DEFAULT 'Lucida Grande',
  `guidelines_font_weight` int(11) NOT NULL DEFAULT '400',
  `guidelines_font_style` varchar(25) NOT NULL DEFAULT 'normal',
  `guidelines_font_size` varchar(11) DEFAULT '',
  `guidelines_font_color` varchar(11) DEFAULT '',
  `section_title_font_type` varchar(50) NOT NULL DEFAULT 'Lucida Grande',
  `section_title_font_weight` int(11) NOT NULL DEFAULT '400',
  `section_title_font_style` varchar(25) NOT NULL DEFAULT 'normal',
  `section_title_font_size` varchar(11) DEFAULT '',
  `section_title_font_color` varchar(11) DEFAULT '',
  `section_desc_font_type` varchar(50) NOT NULL DEFAULT 'Lucida Grande',
  `section_desc_font_weight` int(11) NOT NULL DEFAULT '400',
  `section_desc_font_style` varchar(25) NOT NULL DEFAULT 'normal',
  `section_desc_font_size` varchar(11) DEFAULT '',
  `section_desc_font_color` varchar(11) DEFAULT '',
  `field_text_font_type` varchar(50) NOT NULL DEFAULT 'Lucida Grande',
  `field_text_font_weight` int(11) NOT NULL DEFAULT '400',
  `field_text_font_style` varchar(25) NOT NULL DEFAULT 'normal',
  `field_text_font_size` varchar(11) DEFAULT '',
  `field_text_font_color` varchar(11) DEFAULT '',
  `border_form_width` int(11) NOT NULL DEFAULT '1',
  `border_form_style` varchar(11) NOT NULL DEFAULT 'solid',
  `border_form_color` varchar(11) DEFAULT '',
  `border_guidelines_width` int(11) NOT NULL DEFAULT '1',
  `border_guidelines_style` varchar(11) NOT NULL DEFAULT 'solid',
  `border_guidelines_color` varchar(11) DEFAULT '',
  `border_section_width` int(11) NOT NULL DEFAULT '1',
  `border_section_style` varchar(11) NOT NULL DEFAULT 'solid',
  `border_section_color` varchar(11) DEFAULT '',
  `form_shadow_style` varchar(25) NOT NULL DEFAULT 'WarpShadow',
  `form_shadow_size` varchar(11) NOT NULL DEFAULT 'large',
  `form_shadow_brightness` varchar(11) NOT NULL DEFAULT 'normal',
  `form_button_type` varchar(11) NOT NULL DEFAULT 'text',
  `form_button_text` varchar(100) NOT NULL DEFAULT 'Submit',
  `form_button_image` text,
  `advanced_css` text,
  `user_id` int(11) NOT NULL DEFAULT '1',
  `theme_is_private` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`theme_id`),
  KEY `theme_name` (`theme_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `ap_form_themes`
--

INSERT IGNORE INTO `ap_form_themes` (`theme_id`, `status`, `theme_has_css`, `theme_name`, `theme_built_in`, `logo_type`, `logo_custom_image`, `logo_custom_height`, `logo_default_image`, `logo_default_repeat`, `wallpaper_bg_type`, `wallpaper_bg_color`, `wallpaper_bg_pattern`, `wallpaper_bg_custom`, `header_bg_type`, `header_bg_color`, `header_bg_pattern`, `header_bg_custom`, `form_bg_type`, `form_bg_color`, `form_bg_pattern`, `form_bg_custom`, `highlight_bg_type`, `highlight_bg_color`, `highlight_bg_pattern`, `highlight_bg_custom`, `guidelines_bg_type`, `guidelines_bg_color`, `guidelines_bg_pattern`, `guidelines_bg_custom`, `field_bg_type`, `field_bg_color`, `field_bg_pattern`, `field_bg_custom`, `form_title_font_type`, `form_title_font_weight`, `form_title_font_style`, `form_title_font_size`, `form_title_font_color`, `form_desc_font_type`, `form_desc_font_weight`, `form_desc_font_style`, `form_desc_font_size`, `form_desc_font_color`, `field_title_font_type`, `field_title_font_weight`, `field_title_font_style`, `field_title_font_size`, `field_title_font_color`, `guidelines_font_type`, `guidelines_font_weight`, `guidelines_font_style`, `guidelines_font_size`, `guidelines_font_color`, `section_title_font_type`, `section_title_font_weight`, `section_title_font_style`, `section_title_font_size`, `section_title_font_color`, `section_desc_font_type`, `section_desc_font_weight`, `section_desc_font_style`, `section_desc_font_size`, `section_desc_font_color`, `field_text_font_type`, `field_text_font_weight`, `field_text_font_style`, `field_text_font_size`, `field_text_font_color`, `border_form_width`, `border_form_style`, `border_form_color`, `border_guidelines_width`, `border_guidelines_style`, `border_guidelines_color`, `border_section_width`, `border_section_style`, `border_section_color`, `form_shadow_style`, `form_shadow_size`, `form_shadow_brightness`, `form_button_type`, `form_button_text`, `form_button_image`, `advanced_css`, `user_id`, `theme_is_private`) VALUES
(1, 1, 0, 'Green Senegal', 1, 'default', 'http://', 40, 'machform.png', 0, 'color', '#cfdfc5', '', '', 'color', '#bad4a9', '', '', 'color', '#ffffff', '', '', 'color', '#ecf1ea', '', '', 'color', '#ecf1ea', '', '', 'color', '#cfdfc5', '', '', 'Philosopher', 700, 'normal', '', '#80af1b', 'Philosopher', 400, 'normal', '100%', '#80af1b', 'Philosopher', 700, 'normal', '110%', '#80af1b', 'Ubuntu', 400, 'normal', '', '#666666', 'Philosopher', 700, 'normal', '110%', '#80af1b', 'Philosopher', 400, 'normal', '95%', '#80af1b', 'Ubuntu', 400, 'normal', '', '#666666', 1, 'solid', '#bad4a9', 1, 'dashed', '#bad4a9', 1, 'dotted', '#CCCCCC', 'WarpShadow', 'large', 'normal', 'text', 'Submit', 'http://', '', 1, 1),
(2, 1, 0, 'Blue Bigbird', 1, 'default', 'http://', 40, 'machform.png', 0, 'color', '#336699', '', '', 'color', '#6699cc', '', '', 'color', '#ffffff', '', '', 'color', '#ccdced', '', '', 'color', '#6699cc', '', '', 'color', '#ffffff', '', '', 'Open Sans', 600, 'normal', '', '', 'Open Sans', 400, 'normal', '', '', 'Open Sans', 700, 'normal', '100%', '', 'Ubuntu', 400, 'normal', '80%', '#ffffff', 'Open Sans', 600, 'normal', '', '', 'Open Sans', 400, 'normal', '95%', '', 'Open Sans', 400, 'normal', '', '', 1, 'solid', '#336699', 1, 'dotted', '#6699cc', 1, 'dotted', '#CCCCCC', 'WarpShadow', 'large', 'normal', 'text', 'Submit', 'http://', '', 1, 1),
(3, 1, 0, 'Blue Pionus', 1, 'default', 'http://', 40, 'machform.png', 0, 'color', '#556270', '', '', 'color', '#6b7b8c', '', '', 'color', '#ffffff', '', '', 'color', '#99aec4', '', '', 'color', '#6b7b8c', '', '', 'color', '#ffffff', '', '', 'Istok Web', 400, 'normal', '170%', '', 'Maven Pro', 400, 'normal', '100%', '', 'Istok Web', 700, 'normal', '100%', '', 'Maven Pro', 400, 'normal', '95%', '#ffffff', 'Istok Web', 400, 'normal', '110%', '', 'Maven Pro', 400, 'normal', '95%', '', 'Maven Pro', 400, 'normal', '', '', 1, 'solid', '#556270', 1, 'solid', '#6b7b8c', 1, 'dotted', '#CCCCCC', 'WarpShadow', 'large', 'normal', 'text', 'Submit', 'http://', '', 1, 1),
(4, 1, 0, 'Brown Conure', 1, 'default', 'http://', 40, 'machform.png', 0, 'pattern', '#948c75', 'pattern_036.gif', '', 'color', '#b3a783', '', '', 'color', '#ffffff', '', '', 'color', '#e0d0a2', '', '', 'color', '#948c75', '', '', 'color', '#f0f0d8', 'pattern_036.gif', '', 'Molengo', 400, 'normal', '170%', '', 'Molengo', 400, 'normal', '110%', '', 'Molengo', 400, 'normal', '110%', '', 'Nobile', 400, 'normal', '', '#ececec', 'Molengo', 400, 'normal', '130%', '', 'Molengo', 400, 'normal', '100%', '', 'Molengo', 400, 'normal', '110%', '', 1, 'solid', '#948c75', 1, 'solid', '#948c75', 1, 'dotted', '#CCCCCC', 'WarpShadow', 'large', 'normal', 'text', 'Submit', 'http://', '', 1, 1),
(5, 1, 0, 'Yellow Lovebird', 1, 'default', 'http://', 40, 'machform.png', 0, 'color', '#f0d878', '', '', 'color', '#edb817', '', '', 'color', '#ffffff', '', '', 'color', '#f5d678', '', '', 'color', '#f7c52e', '', '', 'color', '#ffffff', '', '', 'Amaranth', 700, 'normal', '170%', '', 'Amaranth', 400, 'normal', '100%', '', 'Amaranth', 700, 'normal', '100%', '', 'Amaranth', 400, 'normal', '', '#444444', 'Amaranth', 400, 'normal', '110%', '', 'Amaranth', 400, 'normal', '95%', '', 'Amaranth', 400, 'normal', '100%', '', 1, 'solid', '#f0d878', 1, 'solid', '#f7c52e', 1, 'dotted', '#CCCCCC', 'WarpShadow', 'large', 'normal', 'text', 'Submit', 'http://', '', 1, 1),
(6, 1, 0, 'Pink Starling', 1, 'default', 'http://', 40, 'machform.png', 0, 'color', '#ff6699', '', '', 'color', '#d93280', '', '', 'color', '#ffffff', '', '', 'color', '#ffd0d4', '', '', 'color', '#f9fad2', '', '', 'color', '#ffffff', '', '', 'Josefin Sans', 600, 'normal', '160%', '', 'Josefin Sans', 400, 'normal', '110%', '', 'Josefin Sans', 700, 'normal', '110%', '', 'Josefin Sans', 600, 'normal', '100%', '', 'Josefin Sans', 700, 'normal', '', '', 'Josefin Sans', 400, 'normal', '110%', '', 'Josefin Sans', 400, 'normal', '130%', '', 1, 'solid', '#ff6699', 1, 'dashed', '#f56990', 1, 'dotted', '#CCCCCC', 'WarpShadow', 'large', 'normal', 'text', 'Submit', 'http://', '', 1, 1),
(8, 1, 0, 'Red Rabbit', 1, 'default', 'http://', 40, 'machform.png', 0, 'color', '#a40802', '', '', 'color', '#800e0e', '', '', 'color', '#ffffff', '', '', 'color', '#ffa4a0', '', '', 'color', '#800e0e', '', '', 'color', '#ffffff', '', '', 'Lobster', 400, 'normal', '', '#000000', 'Ubuntu', 400, 'normal', '100%', '#000000', 'Lobster', 400, 'normal', '110%', '#222222', 'Ubuntu', 400, 'normal', '85%', '#ffffff', 'Lobster', 400, 'normal', '130%', '#000000', 'Ubuntu', 400, 'normal', '95%', '#000000', 'Ubuntu', 400, 'normal', '', '#333333', 1, 'solid', '#a40702', 1, 'solid', '#800e0e', 1, 'dotted', '#CCCCCC', 'WarpShadow', 'large', 'normal', 'text', 'Submit', 'http://', '', 1, 1),
(9, 1, 0, 'Orange Robin', 1, 'default', 'http://', 40, 'machform.png', 0, 'color', '#f38430', '', '', 'color', '#fa6800', '', '', 'color', '#ffffff', '', '', 'color', '#a7dbd8', '', '', 'color', '#e0e4cc', '', '', 'color', '#ffffff', '', '', 'Lucida Grande', 400, 'normal', '', '#000000', 'Nobile', 400, 'normal', '', '#000000', 'Nobile', 700, 'normal', '', '#000000', 'Lucida Grande', 400, 'normal', '', '#444444', 'Nobile', 700, 'normal', '100%', '#000000', 'Nobile', 400, 'normal', '', '#000000', 'Nobile', 400, 'normal', '95%', '#333333', 1, 'solid', '#f38430', 1, 'solid', '#e0e4cc', 1, 'dotted', '#CCCCCC', 'WarpShadow', 'large', 'normal', 'text', 'Submit', 'http://', '', 1, 1),
(10, 1, 0, 'Orange Sunbird', 1, 'default', 'http://', 40, 'machform.png', 0, 'color', '#d95c43', '', '', 'color', '#c02942', '', '', 'color', '#ffffff', '', '', 'color', '#d95c43', '', '', 'color', '#53777a', '', '', 'color', '#ffffff', '', '', 'Lucida Grande', 400, 'normal', '', '#000000', 'Lucida Grande', 400, 'normal', '', '#000000', 'Lucida Grande', 700, 'normal', '', '#222222', 'Lucida Grande', 400, 'normal', '', '#ffffff', 'Lucida Grande', 400, 'normal', '', '#000000', 'Lucida Grande', 400, 'normal', '', '#000000', 'Lucida Grande', 400, 'normal', '', '#333333', 1, 'solid', '#d95c43', 1, 'solid', '#53777a', 1, 'dotted', '#CCCCCC', 'WarpShadow', 'large', 'normal', 'text', 'Submit', 'http://', '', 1, 1),
(11, 1, 0, 'Green Ringneck', 1, 'default', 'http://', 40, 'machform.png', 0, 'color', '#0b486b', '', '', 'color', '#3b8686', '', '', 'color', '#ffffff', '', '', 'color', '#cff09e', '', '', 'color', '#79bd9a', '', '', 'color', '#a8dba8', '', '', 'Delius Swash Caps', 400, 'normal', '', '#000000', 'Delius Swash Caps', 400, 'normal', '100%', '#000000', 'Delius Swash Caps', 400, 'normal', '100%', '#222222', 'Delius', 400, 'normal', '85%', '#ffffff', 'Delius Swash Caps', 400, 'normal', '', '#000000', 'Delius Swash Caps', 400, 'normal', '95%', '#000000', 'Delius', 400, 'normal', '', '#515151', 1, 'solid', '#0b486b', 1, 'solid', '#79bd9a', 1, 'dotted', '#CCCCCC', 'WarpShadow', 'large', 'normal', 'text', 'Submit', 'http://', '', 1, 1),
(12, 1, 0, 'Brown Finch', 1, 'default', 'http://', 40, 'machform.png', 0, 'color', '#774f38', '', '', 'color', '#e08e79', '', '', 'color', '#ffffff', '', '', 'color', '#ece5ce', '', '', 'color', '#c5e0dc', '', '', 'color', '#f9fad2', '', '', 'Arvo', 700, 'normal', '', '#000000', 'Arvo', 400, 'normal', '', '#000000', 'Arvo', 700, 'normal', '', '#222222', 'Arvo', 400, 'normal', '', '#444444', 'Arvo', 400, 'normal', '', '#000000', 'Arvo', 400, 'normal', '85%', '#000000', 'Arvo', 400, 'normal', '', '#333333', 1, 'solid', '#774f38', 1, 'dashed', '#e08e79', 1, 'dotted', '#CCCCCC', 'WarpShadow', 'large', 'normal', 'text', 'Submit', 'http://', '', 1, 1),
(14, 1, 0, 'Brown Macaw', 1, 'default', 'http://', 40, 'machform.png', 0, 'color', '#413e4a', '', '', 'color', '#73626e', '', '', 'pattern', '#ffffff', 'pattern_022.gif', '', 'color', '#f0b49e', '', '', 'color', '#b38184', '', '', 'color', '#ffffff', '', '', 'Cabin', 500, 'normal', '160%', '#000000', 'Cabin', 400, 'normal', '100%', '#000000', 'Cabin', 700, 'normal', '110%', '#222222', 'Lucida Grande', 400, 'normal', '', '#ececec', 'Cabin', 600, 'normal', '', '#000000', 'Cabin', 600, 'normal', '95%', '#000000', 'Cabin', 400, 'normal', '110%', '#333333', 1, 'solid', '#413e4a', 1, 'dotted', '#ff9900', 1, 'dotted', '#CCCCCC', 'WarpShadow', 'large', 'normal', 'text', 'Submit', 'http://', '', 1, 1),
(15, 1, 0, 'Pink Thrush', 1, 'default', 'http://', 40, 'machform.png', 0, 'color', '#ff9f9d', '', '', 'color', '#ff3d7e', '', '', 'color', '#ffffff', '', '', 'color', '#7fc7af', '', '', 'color', '#3fb8b0', '', '', 'color', '#ffffff', '', '', 'Crafty Girls', 400, 'normal', '', '#000000', 'Crafty Girls', 400, 'normal', '100%', '#000000', 'Crafty Girls', 400, 'normal', '100%', '#222222', 'Nobile', 400, 'normal', '80%', '#ffffff', 'Crafty Girls', 400, 'normal', '', '#000000', 'Crafty Girls', 400, 'normal', '95%', '#000000', 'Molengo', 400, 'normal', '110%', '#333333', 1, 'solid', '#ff9f9d', 1, 'solid', '#3fb8b0', 1, 'dotted', '#CCCCCC', 'WarpShadow', 'large', 'normal', 'text', 'Submit', 'http://', '', 1, 1),
(16, 1, 0, 'Yellow Bulbul', 1, 'default', 'http://', 40, 'machform.png', 0, 'color', '#f8f4d7', '', '', 'color', '#f4b26c', '', '', 'color', '#f4dec2', '', '', 'color', '#f2b4a7', '', '', 'color', '#e98976', '', '', 'color', '#ffffff', '', '', 'Special Elite', 400, 'normal', '', '#000000', 'Special Elite', 400, 'normal', '', '#000000', 'Special Elite', 400, 'normal', '95%', '#222222', 'Cousine', 400, 'normal', '80%', '#ececec', 'Special Elite', 400, 'normal', '', '#000000', 'Special Elite', 400, 'normal', '', '#000000', 'Cousine', 400, 'normal', '', '#333333', 1, 'solid', '#f8f4d7', 1, 'solid', '#f4b26c', 1, 'dotted', '#CCCCCC', 'WarpShadow', 'large', 'normal', 'text', 'Submit', 'http://', '', 1, 1),
(17, 1, 0, 'Blue Canary', 1, 'default', 'http://', 40, 'machform.png', 0, 'color', '#81a8b8', '', '', 'color', '#a4bcc2', '', '', 'color', '#ffffff', '', '', 'color', '#e8f3f8', '', '', 'color', '#dbe6ec', '', '', 'color', '#ffffff', '', '', 'PT Sans', 400, 'normal', '', '#000000', 'PT Sans', 400, 'normal', '100%', '#000000', 'PT Sans', 700, 'normal', '100%', '#222222', 'PT Sans', 400, 'normal', '', '#666666', 'PT Sans', 700, 'normal', '', '#000000', 'PT Sans', 400, 'normal', '100%', '#000000', 'PT Sans', 400, 'normal', '110%', '#333333', 1, 'solid', '#81a8b8', 1, 'dashed', '#a4bcc2', 1, 'dotted', '#CCCCCC', 'WarpShadow', 'large', 'normal', 'text', 'Submit', 'http://', '', 1, 1),
(18, 1, 0, 'Red Mockingbird', 1, 'default', 'http://', 40, 'machform.png', 0, 'color', '#6b0103', '', '', 'color', '#a30005', '', '', 'color', '#c21b01', '', '', 'color', '#f03d02', '', '', 'color', '#1c0113', '', '', 'color', '#ffffff', '', '', 'Oswald', 400, 'normal', '', '#ffffff', 'Open Sans', 400, 'normal', '', '#ffffff', 'Oswald', 400, 'normal', '95%', '#ffffff', 'Open Sans', 400, 'normal', '', '#ececec', 'Oswald', 400, 'normal', '', '#ececec', 'Lucida Grande', 400, 'normal', '', '#ffffff', 'Open Sans', 400, 'normal', '', '#333333', 1, 'solid', '#6b0103', 1, 'solid', '#1c0113', 1, 'dotted', '#CCCCCC', 'WarpShadow', 'large', 'normal', 'text', 'Submit', 'http://', '', 1, 1),
(13, 1, 0, 'Green Sparrow', 1, 'default', 'http://', 40, 'machform.png', 0, 'color', '#d1f2a5', '', '', 'color', '#f56990', '', '', 'color', '#ffffff', '', '', 'color', '#ffc48c', '', '', 'color', '#ffa080', '', '', 'color', '#ffffff', '', '', 'Open Sans', 400, 'normal', '', '#000000', 'Open Sans', 400, 'normal', '', '#000000', 'Open Sans', 700, 'normal', '', '#222222', 'Ubuntu', 400, 'normal', '85%', '#f4fce8', 'Open Sans', 600, 'normal', '', '#000000', 'Open Sans', 400, 'normal', '95%', '#000000', 'Open Sans', 400, 'normal', '', '#333333', 10, 'solid', '#f0fab4', 1, 'solid', '#ffa080', 1, 'dotted', '#CCCCCC', 'WarpShadow', 'large', 'normal', 'text', 'Submit', 'http://', '', 1, 1),
(21, 1, 0, 'Purple Vanga', 1, 'default', 'http://', 40, 'machform.png', 0, 'color', '#7b85e2', '', '', 'color', '#7aa6d6', '', '', 'color', '#d1e7f9', '', '', 'color', '#7aa6d6', '', '', 'color', '#fbfcd0', '', '', 'color', '#ffffff', '', '', 'Droid Sans', 400, 'normal', '160%', '#444444', 'Droid Sans', 400, 'normal', '95%', '#444444', 'Open Sans', 700, 'normal', '95%', '#444444', 'Droid Sans', 400, 'normal', '85%', '#444444', 'Droid Sans', 400, 'normal', '110%', '#444444', 'Droid Sans', 400, 'normal', '95%', '#000000', 'Droid Sans', 400, 'normal', '100%', '#333333', 0, 'solid', '#CCCCCC', 1, 'solid', '#fbfcd0', 1, 'dotted', '#CCCCCC', 'WarpShadow', 'large', 'normal', 'text', 'Submit', 'http://', '', 1, 1),
(22, 1, 0, 'Purple Dove', 1, 'default', 'http://', 40, 'machform.png', 0, 'color', '#c0addb', '', '', 'color', '#a662de', '', '', 'pattern', '#ffffff', 'pattern_044.gif', '', 'color', '#a662de', '', '', 'color', '#a662de', '', '', 'color', '#c0addb', '', '', 'Pacifico', 400, 'normal', '180%', '#000000', 'Open Sans', 400, 'normal', '95%', '#000000', 'Pacifico', 400, 'normal', '95%', '#222222', 'Open Sans', 400, 'normal', '80%', '#ececec', 'Pacifico', 400, 'normal', '110%', '#000000', 'Open Sans', 400, 'normal', '95%', '#000000', 'Open Sans', 400, 'normal', '100%', '#333333', 0, 'solid', '#a662de', 1, 'dashed', '#CCCCCC', 1, 'dashed', '#a662de', 'StandShadow', 'large', 'dark', 'text', 'Submit', 'http://', '', 1, 1),
(20, 1, 0, 'Pink Flamingo', 1, 'default', 'http://', 40, 'machform.png', 0, 'color', '#f87d7b', '', '', 'color', '#5ea0a3', '', '', 'color', '#ffffff', '', '', 'color', '#fab97f', '', '', 'color', '#dcd1b4', '', '', 'color', '#ffffff', '', '', 'Lucida Grande', 400, 'normal', '160%', '#b05573', 'Lucida Grande', 400, 'normal', '95%', '#b05573', 'Lucida Grande', 700, 'normal', '95%', '#b05573', 'Lucida Grande', 400, 'normal', '80%', '#444444', 'Lucida Grande', 400, 'normal', '110%', '#b05573', 'Lucida Grande', 400, 'normal', '85%', '#b05573', 'Lucida Grande', 400, 'normal', '100%', '#333333', 0, 'solid', '#f87d7b', 1, 'dotted', '#fab97f', 1, 'dotted', '#CCCCCC', 'WarpShadow', 'large', 'normal', 'text', 'Submit', 'http://', '', 1, 1),
(19, 1, 0, 'Yellow Kiwi', 1, 'default', 'http://', 40, 'machform.png', 0, 'color', '#ffe281', '', '', 'color', '#ffbb7f', '', '', 'color', '#eee9e5', '', '', 'color', '#fad4b2', '', '', 'color', '#ff9c97', '', '', 'color', '#ffffff', '', '', 'Lucida Grande', 400, 'normal', '160%', '#000000', 'Lucida Grande', 400, 'normal', '95%', '#000000', 'Lucida Grande', 700, 'normal', '95%', '#222222', 'Lucida Grande', 400, 'normal', '80%', '#ffffff', 'Lucida Grande', 400, 'normal', '110%', '#000000', 'Lucida Grande', 400, 'normal', '85%', '#000000', 'Lucida Grande', 400, 'normal', '100%', '#333333', 1, 'solid', '#ffe281', 1, 'solid', '#CCCCCC', 1, 'dotted', '#cdcdcd', 'WarpShadow', 'large', 'normal', 'text', 'Submit', 'http://', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ap_page_logic`
--

CREATE TABLE `ap_page_logic` (
  `form_id` int(11) NOT NULL,
  `page_id` varchar(15) NOT NULL DEFAULT '',
  `rule_all_any` varchar(3) NOT NULL DEFAULT 'all',
  PRIMARY KEY (`form_id`,`page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_page_logic_conditions`
--

CREATE TABLE `ap_page_logic_conditions` (
  `apc_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `target_page_id` varchar(15) NOT NULL DEFAULT '',
  `element_name` varchar(50) NOT NULL DEFAULT '',
  `rule_condition` varchar(15) NOT NULL DEFAULT 'is',
  `rule_keyword` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`apc_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_permissions`
--

CREATE TABLE `ap_permissions` (
  `form_id` bigint(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `edit_form` tinyint(1) NOT NULL DEFAULT '0',
  `edit_entries` tinyint(1) NOT NULL DEFAULT '0',
  `view_entries` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`form_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_settings`
--

CREATE TABLE `ap_settings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `smtp_enable` tinyint(1) NOT NULL DEFAULT '0',
  `smtp_host` varchar(255) NOT NULL DEFAULT 'localhost',
  `smtp_port` int(11) NOT NULL DEFAULT '25',
  `smtp_auth` tinyint(1) NOT NULL DEFAULT '0',
  `smtp_username` varchar(255) DEFAULT NULL,
  `smtp_password` varchar(255) DEFAULT NULL,
  `smtp_secure` tinyint(1) NOT NULL DEFAULT '0',
  `upload_dir` varchar(255) NOT NULL DEFAULT './data',
  `data_dir` varchar(255) NOT NULL DEFAULT './data',
  `organisation_name` varchar(255) NOT NULL DEFAULT 'PermitFlow',
  `organisation_email` varchar(255) DEFAULT NULL,
  `organisation_description` varchar(255) DEFAULT NULL,
  `form_manager_max_rows` int(11) DEFAULT '10',
  `admin_image_url` varchar(255) DEFAULT NULL,
  `disable_machform_link` int(1) DEFAULT '0',
  `machform_version` varchar(10) NOT NULL DEFAULT '3.3',
  `admin_theme` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

ALTER TABLE  `ap_settings` ADD  `upload_dir_web` VARCHAR( 255 ) NULL AFTER  `data_dir` ,
ADD  `data_dir_web` VARCHAR( 255 ) NULL AFTER  `upload_dir_web` ;

ALTER TABLE `ap_settings` ADD `organisation_help` text NULL;
ALTER TABLE `ap_settings` ADD `organisation_sidebar` text NULL;

--
-- Dumping data for table `ap_settings`
--

INSERT IGNORE INTO `ap_settings` (`id`, `smtp_enable`, `smtp_host`, `smtp_port`, `smtp_auth`, `smtp_username`, `smtp_password`, `smtp_secure`, `upload_dir`, `data_dir`, `organisation_name`, `organisation_email`, `organisation_description`, `form_manager_max_rows`, `admin_image_url`, `disable_machform_link`, `machform_version`, `admin_theme`) VALUES
(1, 0, 'localhost', 25, 0, '', '', 0, 'asset_data', 'asset_data', 'PermitFlow', 'no-reply@permitflow.com', 'Sample Organisation', 10, '', 0, '3.5', NULL);

-- --------------------------------------------------------



CREATE TABLE `ap_reports` (
  `form_id` int(11) NOT NULL,
  `report_access_key` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`form_id`),
  KEY `report_access_key` (`report_access_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_report_elements`
--

CREATE TABLE `ap_report_elements` (
  `access_key` varchar(100) DEFAULT NULL,
  `form_id` int(11) NOT NULL,
  `chart_id` int(11) NOT NULL,
  `chart_position` int(11) NOT NULL DEFAULT '0',
  `chart_status` int(1) NOT NULL DEFAULT '1',
  `chart_datasource` varchar(25) NOT NULL DEFAULT '',
  `chart_type` varchar(25) NOT NULL DEFAULT '',
  `chart_enable_filter` int(1) NOT NULL DEFAULT '0',
  `chart_filter_type` varchar(5) NOT NULL DEFAULT 'all',
  `chart_title` text,
  `chart_title_position` varchar(10) NOT NULL DEFAULT 'top',
  `chart_title_align` varchar(10) NOT NULL DEFAULT 'center',
  `chart_width` int(11) NOT NULL DEFAULT '0',
  `chart_height` int(11) NOT NULL DEFAULT '400',
  `chart_background` varchar(8) DEFAULT NULL,
  `chart_theme` varchar(25) NOT NULL DEFAULT 'blueopal',
  `chart_legend_visible` int(1) NOT NULL DEFAULT '1',
  `chart_legend_position` varchar(10) NOT NULL DEFAULT 'right',
  `chart_labels_visible` int(1) NOT NULL DEFAULT '1',
  `chart_labels_position` varchar(10) NOT NULL DEFAULT 'outsideEnd',
  `chart_labels_template` varchar(255) NOT NULL DEFAULT '#= category #',
  `chart_labels_align` varchar(10) NOT NULL DEFAULT 'circle',
  `chart_tooltip_visible` int(1) NOT NULL DEFAULT '1',
  `chart_tooltip_template` varchar(255) NOT NULL DEFAULT '#= category # - #= dataItem.entry # - #= kendo.format(''{0:P}'', percentage)#',
  `chart_gridlines_visible` int(1) NOT NULL DEFAULT '1',
  `chart_bar_color` varchar(8) DEFAULT NULL,
  `chart_is_stacked` int(1) NOT NULL DEFAULT '0',
  `chart_is_vertical` int(1) NOT NULL DEFAULT '0',
  `chart_line_style` varchar(6) NOT NULL DEFAULT 'smooth',
  `chart_axis_is_date` int(1) NOT NULL DEFAULT '0',
  `chart_date_range` varchar(6) NOT NULL DEFAULT 'all' COMMENT 'all,period,custom',
  `chart_date_period_value` int(11) NOT NULL DEFAULT '1',
  `chart_date_period_unit` varchar(5) NOT NULL DEFAULT 'day',
  `chart_date_axis_baseunit` varchar(5) DEFAULT NULL,
  `chart_date_range_start` date DEFAULT NULL,
  `chart_date_range_end` date DEFAULT NULL,
  `chart_grid_page_size` int(11) NOT NULL DEFAULT '10',
  `chart_grid_max_length` int(11) NOT NULL DEFAULT '100',
  PRIMARY KEY (`form_id`,`chart_id`),
  KEY `access_key` (`access_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ap_report_filters`
--

CREATE TABLE `ap_report_filters` (
  `arf_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `chart_id` int(11) NOT NULL,
  `element_name` varchar(50) NOT NULL DEFAULT '',
  `filter_condition` varchar(15) NOT NULL DEFAULT 'is',
  `filter_keyword` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`arf_id`),
  KEY `form_id` (`form_id`,`chart_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


--
-- Table structure for table `ap_table_relation`
--

CREATE TABLE `ap_table_relation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) DEFAULT NULL,
  `element_id` int(11) DEFAULT NULL,
  `tbl_name` varchar(250) DEFAULT NULL,
  `tbl_value_fld` varchar(250) DEFAULT NULL,
  `table_status_fld` varchar(250) DEFAULT NULL,
  `exclude_status` varchar(250) DEFAULT NULL,
  `as_numeric` int(11) DEFAULT NULL,
  `as_include` bigint(20) DEFAULT NULL,
  `as_unique` int(11) DEFAULT NULL,
  `max_value` varchar(250) DEFAULT NULL,
  `min_value` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ap_users`
--

CREATE TABLE `ap_users` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_email` varchar(255) NOT NULL DEFAULT '',
  `user_password` varchar(255) NOT NULL DEFAULT '',
  `user_fullname` varchar(255) NOT NULL DEFAULT '',
  `priv_administer` tinyint(1) NOT NULL DEFAULT '0',
  `priv_new_forms` tinyint(1) NOT NULL DEFAULT '0',
  `priv_new_themes` tinyint(1) NOT NULL DEFAULT '0',
  `last_login_date` datetime DEFAULT NULL,
  `last_ip_address` varchar(15) DEFAULT '',
  `cookie_hash` varchar(255) DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 - deleted; 1 - active; 2 - suspended',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ap_users`
--

INSERT IGNORE INTO `ap_users` (`user_id`, `user_email`, `user_password`, `user_fullname`, `priv_administer`, `priv_new_forms`, `priv_new_themes`, `last_login_date`, `last_ip_address`, `cookie_hash`, `status`) VALUES
(1, 'thomasjgx@gmail.com', '$2a$08$StPqSU4mv0nbtPF24Am1OOwDT/iZ.adeCq1mrWx/6ovcNT6xrpsWm', 'Administrator', 1, 1, 1, NULL, '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `attached_permit`
--

CREATE TABLE `attached_permit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `application_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `entry_id` int(11) NOT NULL,
  `attachedby_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `audit_trail`
--

CREATE TABLE `audit_trail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `form_entry_id` int(11) DEFAULT NULL,
  `action` text,
  `action_timestamp` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE `audit_trail` ADD `object_id` INT NOT NULL ,
ADD `object_name` VARCHAR( 250 ) DEFAULT NULL,
ADD `before_values` TEXT DEFAULT NULL,
ADD `after_values` TEXT DEFAULT NULL,
ADD `ipaddress` VARCHAR( 250 ) DEFAULT NULL,
ADD `http_agent` VARCHAR( 250 ) DEFAULT NULL,
ADD `user_location` VARCHAR( 250 ) DEFAULT NULL;

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `image` text NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `buttons`
--

CREATE TABLE `buttons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img` text NOT NULL,
  `title` text NOT NULL,
  `tooltip` mediumtext NOT NULL,
  `link` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cf_config`
--

CREATE TABLE `cf_config` (
  `strcf_server` text NOT NULL,
  `strsmtp_use_auth` text NOT NULL,
  `strsmtp_server` text NOT NULL,
  `strsmtp_port` varchar(8) NOT NULL DEFAULT '',
  `strsmtp_userid` text NOT NULL,
  `strsmtp_pwd` text NOT NULL,
  `strsysreplyaddr` text NOT NULL,
  `strmailaddtextdef` text NOT NULL,
  `strdeflang` char(3) NOT NULL DEFAULT 'en',
  `bdetailseperatewindow` varchar(5) NOT NULL DEFAULT 'true',
  `strdefsortcol` varchar(32) NOT NULL DEFAULT 'COL_CIRCULATION_NAME',
  `bshowposmail` varchar(5) NOT NULL DEFAULT 'true',
  `bfilter_ar_wordstart` varchar(5) NOT NULL DEFAULT 'true',
  `strcirculation_cols` varchar(255) NOT NULL DEFAULT '12345',
  `ndelay_norm` int(11) NOT NULL DEFAULT '7',
  `ndelay_interm` int(11) NOT NULL DEFAULT '10',
  `ndelay_late` int(11) NOT NULL DEFAULT '12',
  `stremail_format` varchar(8) NOT NULL DEFAULT 'HTML',
  `stremail_values` varchar(8) NOT NULL DEFAULT 'IFRAME',
  `nsubstituteperson_hours` int(11) NOT NULL DEFAULT '96',
  `strsubstituteperson_unit` text NOT NULL,
  `nconfigid` int(11) NOT NULL DEFAULT '0',
  `strsortdirection` text NOT NULL,
  `strversion` text NOT NULL,
  `nshowrows` int(11) DEFAULT NULL,
  `nautoreload` int(11) NOT NULL DEFAULT '0',
  `strurlpassword` text NOT NULL,
  `tslastupdate` int(11) NOT NULL,
  `ballowunencryptedrequest` int(11) NOT NULL,
  `userdefined1_title` text NOT NULL,
  `userdefined2_title` text NOT NULL,
  `strdateformat` text NOT NULL,
  `strmailsendtype` text NOT NULL,
  `strmtapath` text NOT NULL,
  `strslotvisibility` varchar(100) NOT NULL,
  `strsmtpencryption` varchar(100) NOT NULL,
  `bsendworkflowmail` int(11) NOT NULL,
  `bsendremindermail` int(11) NOT NULL,
  PRIMARY KEY (`nconfigid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cf_config`
--

INSERT IGNORE INTO `cf_config` (`strcf_server`, `strsmtp_use_auth`, `strsmtp_server`, `strsmtp_port`, `strsmtp_userid`, `strsmtp_pwd`, `strsysreplyaddr`, `strmailaddtextdef`, `strdeflang`, `bdetailseperatewindow`, `strdefsortcol`, `bshowposmail`, `bfilter_ar_wordstart`, `strcirculation_cols`, `ndelay_norm`, `ndelay_interm`, `ndelay_late`, `stremail_format`, `stremail_values`, `nsubstituteperson_hours`, `strsubstituteperson_unit`, `nconfigid`, `strsortdirection`, `strversion`, `nshowrows`, `nautoreload`, `strurlpassword`, `tslastupdate`, `ballowunencryptedrequest`, `userdefined1_title`, `userdefined2_title`, `strdateformat`, `strmailsendtype`, `strmtapath`, `strslotvisibility`, `strsmtpencryption`, `bsendworkflowmail`, `bsendremindermail`) VALUES
('http://localhost', '', '', '25', '', '', '', '', 'en', 'true', 'COL_CIRCULATION_PROCESS_DAYS', 'true', 'true', 'NAME---1---STATION---1---DAYS---1---START---1---SENDER---1---WHOLETIME---1---MAILLIST---0---TEMPLATE---0', 7, 10, 12, 'HTML', 'IFRAME', 1, 'DAYS', 1, 'ASC', '2.11.2', 50, 60, '82456cc0f190e04da86acd61635be578', 1297422619, 0, 'Designation', 'Man Number', 'm-d-Y', 'PHP', '', 'ALL', 'NONE', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cf_formslot`
--

CREATE TABLE `cf_formslot` (
  `nid` int(11) NOT NULL AUTO_INCREMENT,
  `strname` text NOT NULL,
  `strdescription` varchar(250) NOT NULL,
  `ntemplateid` int(11) NOT NULL DEFAULT '0',
  `nslotnumber` int(11) NOT NULL DEFAULT '0',
  `nsendtype` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`nid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cf_substitute`
--

CREATE TABLE `cf_substitute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `substitute_id` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cf_user`
--

CREATE TABLE `cf_user` (
  `nid` int(11) NOT NULL AUTO_INCREMENT,
  `strlastname` text NOT NULL,
  `strfirstname` text NOT NULL,
  `stremail` text NOT NULL,
  `naccesslevel` int(11) NOT NULL DEFAULT '0',
  `struserid` text NOT NULL,
  `strpassword` text NOT NULL,
  `stremail_format` varchar(8) NOT NULL DEFAULT 'HTML',
  `stremail_values` varchar(8) NOT NULL DEFAULT 'IFRAME',
  `nsubstitudeid` int(11) NOT NULL DEFAULT '0',
  `tslastaction` int(11) NOT NULL,
  `bdeleted` int(11) NOT NULL,
  `strstreet` text NOT NULL,
  `strcountry` text NOT NULL,
  `strzipcode` text NOT NULL,
  `strcity` text NOT NULL,
  `strphone_main1` text NOT NULL,
  `strphone_main2` text NOT NULL,
  `strphone_mobile` text NOT NULL,
  `strfax` text NOT NULL,
  `strorganisation` text NOT NULL,
  `strdepartment` text NOT NULL,
  `strcostcenter` text NOT NULL,
  `userdefined1_value` text NOT NULL,
  `userdefined2_value` text NOT NULL,
  `nsubstitutetimevalue` int(11) NOT NULL,
  `strsubstitutetimeunit` text NOT NULL,
  `busegeneralsubstituteconfig` int(11) NOT NULL,
  `busegeneralemailconfig` int(11) NOT NULL,
  `enable_email` int(11) NOT NULL DEFAULT '1',
  `enable_chat` int(11) NOT NULL DEFAULT '1',
  `about_me` text,
  `profile_pic` varchar(250) DEFAULT NULL,
  `address` text,
  `twitter` varchar(250) DEFAULT NULL,
  `facebook` varchar(250) DEFAULT NULL,
  `youtube` varchar(250) DEFAULT NULL,
  `linkedin` varchar(250) DEFAULT NULL,
  `pinterest` varchar(250) DEFAULT NULL,
  `instagram` varchar(250) DEFAULT NULL,
  `strtoken` varchar(100) DEFAULT NULL,
  `strtemppassword` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`nid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

INSERT IGNORE INTO `cf_user` (`nid`, `strlastname`, `strfirstname`, `stremail`, `naccesslevel`, `struserid`, `strpassword`, `stremail_format`, `stremail_values`, `nsubstitudeid`, `tslastaction`, `bdeleted`, `strstreet`, `strcountry`, `strzipcode`, `strcity`, `strphone_main1`, `strphone_main2`, `strphone_mobile`, `strfax`, `strorganisation`, `strdepartment`, `strcostcenter`, `userdefined1_value`, `userdefined2_value`, `nsubstitutetimevalue`, `strsubstitutetimeunit`, `busegeneralsubstituteconfig`, `busegeneralemailconfig`, `enable_email`, `enable_chat`, `about_me`, `profile_pic`, `address`, `twitter`, `facebook`, `youtube`, `linkedin`, `pinterest`, `instagram`) VALUES
(1, 'Administrator (IT Support)', 'System', 'thomasjgx@gmail.com', 1, 'admin', 'b69445142a49250c409e8e41f7021624', '', '', 0, 1395753936, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 1, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cf_user_index`
--

CREATE TABLE `cf_user_index` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `indexe` text NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `checksum`
--

CREATE TABLE `checksum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_id` int(11) DEFAULT NULL,
  `checksum` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `entry_id_idx` (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `circulation_id` int(11) NOT NULL,
  `slot_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `comment` mediumtext NOT NULL,
  `resolved` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `communication`
--

CREATE TABLE `communication` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender` int(11) NOT NULL,
  `receiver` int(11) NOT NULL,
  `message` text,
  `reply` int(11) DEFAULT NULL,
  `isread` int(11) DEFAULT '0',
  `created_on` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE  `communication` ADD  `application_id` INT NULL ;
ALTER TABLE  `communication` ADD INDEX (  `sender` ,  `receiver` ,  `isread` ,  `created_on` ,  `application_id` ) ;

-- --------------------------------------------------------

--
-- Table structure for table `communications`
--

CREATE TABLE `communications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `architect_id` int(11) DEFAULT NULL,
  `reviewer_id` int(11) DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  `messageread` int(11) DEFAULT NULL,
  `content` text,
  `action_timestamp` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `conditions`
--

CREATE TABLE `conditions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `circulation_id` int(11) NOT NULL,
  `slot_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `condition_text` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `conditions_of_approval`
--

CREATE TABLE `conditions_of_approval` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `permit_id` bigint(20) DEFAULT NULL,
  `short_name` varchar(250) DEFAULT NULL,
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `type_id` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `published` tinyint(4) DEFAULT NULL,
  `menu_index` tinyint(4) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `top_article` text,
  `menu_title` varchar(255) DEFAULT NULL,
  `breadcrumb_title` varchar(255) DEFAULT NULL,
  `last_modified_on` datetime DEFAULT NULL,
  `visibility` int(11) NOT NULL DEFAULT '1',
  `deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `type_id_idx` (`type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `parent_id`, `type_id`, `created_on`, `published`, `menu_index`, `url`, `top_article`, `menu_title`, `breadcrumb_title`, `last_modified_on`, `visibility`, `deleted`) VALUES
(1, 0, 1, NULL, 1, 1, NULL, '   <div class="sixteen columns">\r\n      <div class="flexslider">\r\n        <ul class="slides">\r\n          <li> <img src="/permitflow/images/services-slide-pic1.jpg" alt=""> </li>\r\n          <li> <img src="/permitflow/images/services-slide-pic2.jpg" alt="">\r\n            <article class="slide-caption">\r\n              <h3>Logical Planning</h3>\r\n              <p>This is a Slide Caption text example</p>\r\n            </article>\r\n          </li>\r\n          <li> <img src="/permitflow/images/services-slide-pic3.jpg" alt=""> </li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n\r\n<section class="blox dark">\r\n    <div class="container">\r\n      <div class="sixteen columns">\r\n          <hr class="vertical-space1">\r\n        <!-- Start - Icon Boxes -->\r\n      <div class="one_third">\r\n        <article class="icon-box icon-colorx"> <i class="icomoon-marker-2"></i>\r\n          <h4><strong>Web Design</strong></h4>\r\n          <p>Morlem ipsum dolor sit amet, consectetur adipiscing elit Aenean nisl orci aliqua.adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>\r\n        </article>\r\n      </div>\r\n      <div class="one_third">\r\n        <article class="icon-box icon-colorx"> <i class="icomoon-cube4"></i>\r\n          <h4><strong>Web Hosting</strong></h4>\r\n          <p>Morlem ipsum dolor sit amet, consectetur adipiscing elit Aenean nisl orci aliqua.adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>\r\n        </article>\r\n      </div>\r\n      <div class="one_third column-last">\r\n        <article class="icon-box icon-colorx"> <i class="icomoon-mobile"></i>\r\n          <h4><strong>Mobile Marketing</strong></h4>\r\n          <p>Morlem ipsum dolor sit amet, consectetur adipiscing elit Aenean nisl orci aliqua.adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>\r\n        </article>\r\n      </div>\r\n  </div>\r\n  </div>\r\n  </section>\r\n', 'Test Page 1', NULL, '2014-04-04 00:00:00', 1, 0),
(2, 0, NULL, '2014-04-04 00:00:00', 1, 2, NULL, '<p>This is sample content for test page 2</p>\r\n', 'Test Page 2', NULL, '2014-04-04 00:00:00', 1, 0),
(3, 0, NULL, '2014-04-06 00:00:00', 1, 3, NULL, '<p>This is sample content for test page 3</p>\r\n', 'Test Page 3', NULL, '2014-04-06 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `content_type`
--

CREATE TABLE `content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `content_type`
--

INSERT IGNORE INTO `content_type` (`id`, `title`) VALUES
(1, 'Web Article'),
(2, 'Module'),
(3, 'Form');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_name` varchar(255) NOT NULL DEFAULT '',
  `department_head` bigint(20) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `entry_decline`
--

CREATE TABLE `entry_decline` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `entry_id` bigint(20) DEFAULT NULL,
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `resolved` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `entry_id_idx` (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(250) NOT NULL,
  `content` varchar(250) NOT NULL,
  `location` varchar(250) NOT NULL,
  `is_application_related` int(11) NOT NULL DEFAULT '0',
  `related_application` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `datesent` varchar(250) NOT NULL,
  `start_date` varchar(250) NOT NULL,
  `end_date` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ext_locales`
--

CREATE TABLE `ext_locales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale_identifier` varchar(250) NOT NULL,
  `local_title` varchar(250) NOT NULL,
  `locale_description` text,
  `is_default` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE `ext_locales` ADD `text_align` INT NOT NULL DEFAULT '0' ;

INSERT INTO `ext_locales` (`id`, `locale_identifier`, `local_title`, `locale_description`, `is_default`) VALUES
(1, 'en_US', 'English', 'English', 1),
(2, 'bt_BD', 'Arabic', 'Arabic', 0),
(3, 'bt_BD', 'Bangla', 'Bangladesh', 0);
-- --------------------------------------------------------

--
-- Table structure for table `ext_translations`
--

CREATE TABLE `ext_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` varchar(50) NOT NULL,
  `table_class` varchar(250) NOT NULL,
  `field_name` varchar(250) NOT NULL,
  `field_id` int(11) NOT NULL,
  `trl_content` longtext NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(250) NOT NULL,
  `answer` text,
  `published` tinyint(4) DEFAULT '0',
  `posted_by` varchar(80) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `created_on` date NOT NULL DEFAULT '2010-01-01',
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE `favorites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(250) NOT NULL,
  `content` varchar(250) NOT NULL,
  `application_id` int(11) NOT NULL,
  `isread` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `datesent` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fee`
--

CREATE TABLE `fee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fee_code` varchar(100) DEFAULT NULL,
  `description` text NOT NULL,
  `amount` varchar(100) DEFAULT '0',
  `invoiceid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(60) DEFAULT NULL,
  `type` varchar(30) DEFAULT NULL,
  `names` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `message` text,
  `created_on` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `form_entry`
--

CREATE TABLE `form_entry` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) DEFAULT NULL,
  `entry_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `circulation_id` int(11) DEFAULT NULL,
  `approved` int(11) DEFAULT '0',
  `application_id` varchar(255) DEFAULT '0',
  `declined` int(11) DEFAULT '0',
  `deleted_status` int(11) NOT NULL DEFAULT '0',
  `saved_permit` text,
  `previous_submission` bigint(20) NOT NULL,
  `parent_submission` bigint(20) NOT NULL,
  `date_of_submission` varchar(250) NOT NULL,
  `date_of_response` varchar(250) NOT NULL,
  `date_of_issue` varchar(250) NOT NULL,
  `observation` text NOT NULL,
  `pdf_path` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `form_id_idx` (`form_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7450 ;

CREATE TABLE `form_entry_archive` (
  `id` bigint(20) NOT NULL,
  `form_id` int(11) DEFAULT NULL,
  `entry_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `circulation_id` int(11) DEFAULT NULL,
  `approved` int(11) DEFAULT '0',
  `application_id` varchar(255) DEFAULT '0',
  `declined` int(11) DEFAULT '0',
  `deleted_status` int(11) NOT NULL DEFAULT '0',
  `saved_permit` text,
  `previous_submission` bigint(20) NOT NULL,
  `parent_submission` bigint(20) NOT NULL,
  `date_of_submission` varchar(250) NOT NULL,
  `date_of_response` varchar(250) NOT NULL,
  `date_of_issue` varchar(250) NOT NULL,
  `observation` text NOT NULL,
  `pdf_path` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `form_id_idx` (`form_id`),
  KEY `user_id_idx` (`user_id`),
  KEY `entry_id_idx` (`entry_id`),
  KEY `approvedx` (`approved`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7450 ;

-- --------------------------------------------------------

--
-- Table structure for table `form_entry_links`
--

CREATE TABLE `form_entry_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `formentryid` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `entry_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date_of_submission` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `form_entry_shares`
--

CREATE TABLE `form_entry_shares` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `senderid` int(11) NOT NULL,
  `receiverid` int(11) NOT NULL,
  `formentryid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `form_groups`
--

CREATE TABLE `form_groups` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_parent` int(11) NOT NULL DEFAULT '0',
  `group_name` text,
  `group_description` text,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(250) NOT NULL,
  `content` varchar(250) NOT NULL,
  `link` varchar(250) NOT NULL,
  `isread` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `datesent` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;



CREATE TABLE `invoice_api_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `api_key` text NOT NULL,
  `api_secret` text NOT NULL,
  `mda_name` text NOT NULL,
  `mda_branch` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Table structure for table `invoicetemplates`
--

CREATE TABLE `invoicetemplates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `applicationform` int(11) DEFAULT NULL,
  `applicationstage` int(11) DEFAULT NULL,
  `content` text NOT NULL,
  `max_duration` varchar(255) NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `permits_applicationform_idx` (`applicationform`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;


ALTER TABLE `invoicetemplates` ADD `due_duration` VARCHAR(250) NULL ;
ALTER TABLE `invoicetemplates` ADD `invoice_number` VARCHAR(250) NULL ;
ALTER TABLE `invoicetemplates` ADD `expiration_type` int(11) DEFAULT NULL;
ALTER TABLE `invoicetemplates` ADD `payment_type` int(11) DEFAULT NULL;


--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `order_no` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mf_guard_group`
--

CREATE TABLE `mf_guard_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

-- --------------------------------------------------------

--
-- Table structure for table `mf_guard_group_permission`
--

CREATE TABLE `mf_guard_group_permission` (
  `group_id` bigint(20) NOT NULL DEFAULT '0',
  `permission_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mf_guard_permission`
--

CREATE TABLE `mf_guard_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=474 ;

--
-- Dumping data for table `mf_guard_permission`
--

INSERT IGNORE INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES
(48, 'viewaudittrail', 'View Audit Trail'),
(53, 'managedepartments', 'Manage Departments'),
(102, 'managepublication', 'Manage Articles'),
(144, 'manageusers', 'Manage Registered Members'),
(146, 'managegroups', 'Manage Groups'),
(148, 'manageroles', 'Manage Roles'),
(152, 'manageforms', 'Manage Forms'),
(154, 'managefees', 'Manage Fees'),
(156, 'managepermits', 'Manage Permits'),
(158, 'manageconditions', 'Manage Conditions'),
(160, 'manageplots', 'Manage Plots'),
(165, 'managereviewers', 'Manage Reviewers'),
(167, 'managecommentsheets', 'Manage Comment Sheets'),
(171, 'managestages', 'Manage Stages'),
(173, 'manageactions', 'Manage Actions'),
(175, 'managenotifications', 'Manage Notifications'),
(178, 'managewebpages', 'Manage Web Pages'),
(180, 'managefaqs', 'Manage FAQs'),
(182, 'managenews', 'Manage News Articles'),
(191, 'has_hod_access', 'Has Access to Head of Department Functions'),
(200, 'transfer_applications', 'Able to transfer ownership of applications between users'),
(207, 'editapplication', 'Edit Applications'),
(208, 'assigntask', 'Assign tasks'),
(209, 'createapplications', 'Create applications from backend'),
(445, 'access_applications', 'Access Applications Menu'),
(446, 'access_tasks', 'Access Tasks Menu'),
(447, 'access_members', 'Access Clients Page'),
(448, 'access_reviewers', 'Access Reviewers Page'),
(449, 'access_reports', 'Access Reports Menu'),
(450, 'access_help', 'Access Help Menu'),
(451, 'access_settings', 'Access Settings Menu'),
(452, 'access_content', 'Access Content Menu'),
(453, 'access_forms', 'Access Forms Menu'),
(455, 'access_security', 'Access Security Menu'),
(456, 'managelanguages', 'Manage Languages'),
(457, 'manageannouncements', 'Manage Announcements'),
(458, 'manageformgroups', 'Manage Form Groups'),
(459, 'manageinvoices', 'Manage Invoice Templates'),
(460, 'managereports', 'Manage Reports'),
(461, 'managecategories', 'Manage User Categories'),
(462, 'access_workflow', 'Access Workflow Menu'),
(463, 'access_billing', 'Access Billing Menu'),
(464, 'approvepayments', 'Approve Payments'),
(465, 'access_permits', 'Access Permits Menu'),
(466, 'sign_permits', 'Can sign permits using EchoSign API');

INSERT INTO `mf_guard_permission` (`id`, `name`, `description`) VALUES (NULL, 'cancel_permits', 'Can cancel permits');

-- --------------------------------------------------------

--
-- Table structure for table `mf_guard_user_group`
--

CREATE TABLE `mf_guard_user_group` (
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `group_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mf_guard_user_permission`
--

CREATE TABLE `mf_guard_user_permission` (
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `permission_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`permission_id`),
  KEY `mf_guard_user_permission_permission_id_mf_guard_permission_id` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mf_invoice`
--

CREATE TABLE `mf_invoice` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `app_id` bigint(20) DEFAULT NULL,
  `invoice_number` varchar(255) DEFAULT NULL,
  `paid` bigint(20) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `expires_at` datetime NULL,
  PRIMARY KEY (`id`),
  KEY `app_id_idx` (`app_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

ALTER TABLE `mf_invoice` ADD `mda_code` VARCHAR(250) NULL ;
ALTER TABLE `mf_invoice` ADD `service_code` VARCHAR(250) NULL ;
ALTER TABLE `mf_invoice` ADD `branch` VARCHAR(250) NULL ;
ALTER TABLE `mf_invoice` ADD `due_date` VARCHAR(250) NULL ;
ALTER TABLE `mf_invoice` ADD `payer_id` VARCHAR(250) NULL ;
ALTER TABLE `mf_invoice` ADD `payer_name` VARCHAR(250) NULL ;
ALTER TABLE `mf_invoice` ADD `total_amount` DOUBLE NULL ;
ALTER TABLE `mf_invoice` ADD `currency` VARCHAR(250) NULL ;
ALTER TABLE `mf_invoice` ADD `doc_ref_number` VARCHAR(250) NULL ;
ALTER TABLE `mf_invoice` ADD `template_id` bigint(20) NULL ;
ALTER TABLE  `mf_invoice` ADD  `document_key` VARCHAR( 255 ) NULL ;
ALTER TABLE  `mf_invoice` ADD  `remote_validate` INT NULL ;
ALTER TABLE  `mf_invoice` ADD  `pdf_path` VARCHAR( 255 ) NULL ;

CREATE TABLE `mf_invoice_archive` (
  `id` bigint(20) NOT NULL,
  `app_id` bigint(20) DEFAULT NULL,
  `invoice_number` varchar(255) DEFAULT NULL,
  `paid` bigint(20) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `expires_at` datetime NULL,
  PRIMARY KEY (`id`),
  KEY `app_id_idx` (`app_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

ALTER TABLE `mf_invoice_archive` ADD `mda_code` VARCHAR(250) NULL ;
ALTER TABLE `mf_invoice_archive` ADD `service_code` VARCHAR(250) NULL ;
ALTER TABLE `mf_invoice_archive` ADD `branch` VARCHAR(250) NULL ;
ALTER TABLE `mf_invoice_archive` ADD `due_date` VARCHAR(250) NULL ;
ALTER TABLE `mf_invoice_archive` ADD `payer_id` VARCHAR(250) NULL ;
ALTER TABLE `mf_invoice_archive` ADD `payer_name` VARCHAR(250) NULL ;
ALTER TABLE `mf_invoice_archive` ADD `total_amount` DOUBLE NULL ;
ALTER TABLE `mf_invoice_archive` ADD `currency` VARCHAR(250) NULL ;
ALTER TABLE `mf_invoice_archive` ADD `doc_ref_number` VARCHAR(250) NULL ;
ALTER TABLE `mf_invoice_archive` ADD `template_id` bigint(20) NULL ;
ALTER TABLE  `mf_invoice_archive` ADD  `document_key` VARCHAR( 255 ) NULL ;
ALTER TABLE  `mf_invoice_archive` ADD  `remote_validate` INT NULL ;
ALTER TABLE  `mf_invoice_archive` ADD  `pdf_path` VARCHAR( 255 ) NULL ;

-- --------------------------------------------------------

--
-- Table structure for table `mf_invoice_detail`
--

CREATE TABLE `mf_invoice_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invoice_id` bigint(20) DEFAULT NULL,
  `description` text,
  `amount` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `invoice_id_idx` (`invoice_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

CREATE TABLE `mf_invoice_detail_archive` (
  `id` bigint(20) NOT NULL,
  `invoice_id` bigint(20) DEFAULT NULL,
  `description` text,
  `amount` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `invoice_id_idx` (`invoice_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `mf_user_profile`
--

CREATE TABLE `mf_user_profile` (
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `form_id` bigint(20) DEFAULT NULL,
  `entry_id` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `nb_connected`
--

CREATE TABLE `nb_connected` (
  `ip` text NOT NULL,
  `time` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `article` text,
  `created_by` int(11) DEFAULT NULL,
  `created_on` date DEFAULT NULL,
  `published` int(11) DEFAULT NULL,
  `hits` int(11) NOT NULL DEFAULT '0',
  `agency_id` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submenu_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `content` longtext,
  `sms` varchar(255) DEFAULT NULL,
  `form_id` int(11) DEFAULT NULL,
  `autosend` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `submenu_id_idx` (`submenu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `notification_history`
--

CREATE TABLE `notification_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `notification` text,
  `notification_type` varchar(250) DEFAULT NULL,
  `sent_on` varchar(250) DEFAULT NULL,
  `confirmed_receipt` int(11) DEFAULT '0',
  `application_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------


--
-- Table structure for table `notification_queue`
--

CREATE TABLE `notification_queue` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `notification` text,
  `notification_type` varchar(250) DEFAULT NULL,
  `sent_on` varchar(250) DEFAULT NULL,
  `sent` int(11) DEFAULT '0',
  `application_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `permits`
--


CREATE TABLE `permits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `applicationform` int(11) DEFAULT NULL,
  `applicationstage` int(11) DEFAULT NULL,
  `parttype` int(11) DEFAULT NULL,
  `content` text NOT NULL,
  `footer` text NOT NULL,
  `max_duration` varchar(255) NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `permits_applicationform_idx` (`applicationform`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

ALTER TABLE  `permits` ADD  `remote_url` VARCHAR( 255 ) NULL ,
ADD  `remote_field` VARCHAR( 255 ) NULL ,
ADD  `remote_username` VARCHAR( 255 ) NULL ,
ADD  `remote_password` VARCHAR( 255 ) NULL ,
ADD  `remote_request_type` VARCHAR( 255 ) NULL ;

ALTER TABLE `permits` ADD `page_type` VARCHAR(250) NULL ;
ALTER TABLE `permits` ADD `page_orientation` VARCHAR(250) NULL ;


--
-- Table structure for table `plot`
--

CREATE TABLE `plot` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `plot_no` int(11) DEFAULT NULL,
  `plot_type` varchar(250) DEFAULT NULL,
  `plot_status` varchar(250) DEFAULT NULL,
  `plot_size` varchar(250) NOT NULL,
  `plot_lat` varchar(250) NOT NULL,
  `plot_long` varchar(250) NOT NULL,
  `plot_location` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `plot_activity`
--

CREATE TABLE `plot_activity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `plot_id` int(11) DEFAULT NULL,
  `entry_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `plot_id_idx` (`plot_id`),
  KEY `entry_id_idx` (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `publication`
--

CREATE TABLE `publication` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `article` text,
  `created_by` int(11) DEFAULT NULL,
  `created_on` date DEFAULT NULL,
  `published` int(11) DEFAULT NULL,
  `hits` int(11) NOT NULL DEFAULT '0',
  `agency_id` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `form_id` int(11) DEFAULT NULL,
  `title` text NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `report_fields`
--

CREATE TABLE `report_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_id` int(11) DEFAULT NULL,
  `element` text,
  `customheader` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `report_filters`
--

CREATE TABLE `report_filters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_id` int(11) DEFAULT NULL,
  `element_id` int(11) DEFAULT NULL,
  `exclusive` int(11) DEFAULT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `reviewer_comments`
--

CREATE TABLE `reviewer_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commentcontent` text NOT NULL,
  `date_created` text NOT NULL,
  `reviewer_id` int(11) DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `messageread` int(11) NOT NULL DEFAULT '0',
  `application_id` varchar(254) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------


CREATE TABLE `saved_permit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `permit` text NOT NULL,
  `date_of_issue` varchar(250) NOT NULL,
  `date_of_expiry` varchar(250) NOT NULL,
  `created_by` int(11) NOT NULL,
  `last_updated` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

ALTER TABLE `saved_permit` ADD `permit_id` VARCHAR(250) NULL ;
ALTER TABLE `saved_permit` ADD `document_key` VARCHAR(250) NULL ;
ALTER TABLE `saved_permit` ADD `remote_result` TEXT NULL ;
ALTER TABLE  `saved_permit` ADD  `permit_status` INT NULL DEFAULT  '1';
ALTER TABLE `saved_permit` ADD `remote_update_uuid` TEXT NULL ;
ALTER TABLE  `saved_permit` ADD  `pdf_path` VARCHAR( 255 ) NULL ;


CREATE TABLE `saved_permit_archive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `permit` text NOT NULL,
  `date_of_issue` varchar(250) NOT NULL,
  `date_of_expiry` varchar(250) NOT NULL,
  `created_by` int(11) NOT NULL,
  `last_updated` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

ALTER TABLE `saved_permit_archive` ADD `permit_id` VARCHAR(250) NULL ;
ALTER TABLE `saved_permit_archive` ADD `document_key` VARCHAR(250) NULL ;
ALTER TABLE `saved_permit_archive` ADD `remote_result` TEXT NULL ;
ALTER TABLE  `saved_permit_archive` ADD  `permit_status` INT NULL DEFAULT  '1';
ALTER TABLE `saved_permit_archive` ADD `remote_update_uuid` TEXT NULL ;
ALTER TABLE  `saved_permit_archive` ADD  `pdf_path` VARCHAR( 255 ) NULL ;

--
-- Table structure for table `sf_guard_forgot_password`
--

CREATE TABLE `sf_guard_forgot_password` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `unique_key` varchar(255) DEFAULT NULL,
  `expires_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_group`
--

CREATE TABLE `sf_guard_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sf_guard_group`
--

INSERT IGNORE INTO `sf_guard_group` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator group', '2011-02-19 00:35:10', '2011-02-19 00:35:10');

-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_group_permission`
--

CREATE TABLE `sf_guard_group_permission` (
  `group_id` bigint(20) NOT NULL DEFAULT '0',
  `permission_id` bigint(20) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`group_id`,`permission_id`),
  KEY `sf_guard_group_permission_permission_id_sf_guard_permission_id` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sf_guard_group_permission`
--

INSERT IGNORE INTO `sf_guard_group_permission` (`group_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2011-02-19 00:35:10', '2011-02-19 00:35:10');

-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_permission`
--

CREATE TABLE `sf_guard_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sf_guard_permission`
--

INSERT IGNORE INTO `sf_guard_permission` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator permission', '2011-02-19 00:35:10', '2011-02-19 00:35:10');

-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_remember_key`
--

CREATE TABLE `sf_guard_remember_key` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `remember_key` varchar(32) DEFAULT NULL,
  `ip_address` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_idx` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=165 ;

-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_user`
--

CREATE TABLE `sf_guard_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email_address` varchar(255) NOT NULL,
  `username` varchar(128) NOT NULL,
  `algorithm` varchar(128) NOT NULL DEFAULT 'sha1',
  `salt` varchar(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `is_super_admin` tinyint(1) DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `is_active_idx_idx` (`is_active`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1980 ;

-- --------------------------------------------------------

INSERT IGNORE INTO `sf_guard_user` (`id`, `first_name`, `last_name`, `email_address`, `username`, `algorithm`, `salt`, `password`, `is_active`, `is_super_admin`, `last_login`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, '', 'admin', 'sha1', 'a7b879224773436c4dcf84593f75b374', 'd149cbfd384e5c9179e5a35d16f5f54f431fbffc', 0, 0, '2014-03-25 16:04:16', '2011-06-10 09:37:45', '2014-03-25 16:04:16');

--
-- Table structure for table `sf_guard_user_associates`
--

CREATE TABLE `sf_guard_user_associates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `senderid` int(11) NOT NULL,
  `receiverid` int(11) NOT NULL,
  `accepted` int(11) NOT NULL DEFAULT '0',
  `seemyplans` int(11) NOT NULL DEFAULT '0',
  `modifymyplans` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_user_categories`
--

CREATE TABLE `sf_guard_user_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `description` text,
  `formid` int(11) NOT NULL,
  `orderid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


INSERT INTO `sf_guard_user_categories` (`id`, `name`, `description`, `formid`, `orderid`) VALUES
(1, 'Personal', 'Personal', 15, 1);
INSERT INTO `sf_guard_user_categories` (`id`, `name`, `description`, `formid`, `orderid`) VALUES
(2, 'Business', 'Business', 15, 2);
INSERT INTO `sf_guard_user_categories` (`id`, `name`, `description`, `formid`, `orderid`) VALUES
(3, 'Non-Citizen', 'Business', 15, 3);
INSERT INTO `sf_guard_user_categories` (`id`, `name`, `description`, `formid`, `orderid`) VALUES
(4, 'Visitors', 'Visitors', 15, 4);

-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_user_categories_forms`
--

CREATE TABLE `sf_guard_user_categories_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoryid` int(11) NOT NULL,
  `formid` int(11) NOT NULL,
  `islinkedto` int(11) NOT NULL DEFAULT '0',
  `islinkedtitle` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_user_group`
--

CREATE TABLE `sf_guard_user_group` (
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `group_id` bigint(20) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `sf_guard_user_group_group_id_sf_guard_group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------
INSERT IGNORE INTO `sf_guard_user_group` (`user_id`, `group_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2012-03-01 00:00:00', '2012-03-30 00:00:00');

--
-- Table structure for table `sf_guard_user_permission`
--

CREATE TABLE `sf_guard_user_permission` (
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `permission_id` bigint(20) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`user_id`,`permission_id`),
  KEY `sf_guard_user_permission_permission_id_sf_guard_permission_id` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sf_guard_user_profile`
--

CREATE TABLE `sf_guard_user_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `email` varchar(80) DEFAULT NULL,
  `fullname` varchar(80) DEFAULT NULL,
  `validate` varchar(17) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `registeras` int(11) NOT NULL,
  `profile_pic` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_idx` (`user_id`),
  KEY `sf_guard_user_profile_user_id_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sub_menus`
--

CREATE TABLE `sub_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `order_no` int(11) NOT NULL,
  `max_duration` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `change_identifier` int(11) DEFAULT '0',
  `menu_id` int(11) DEFAULT '0',
  `hide_comments` int(11) NOT NULL DEFAULT '0',
  `allow_edit` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE `sub_menus` ADD `stage_type` INT NULL DEFAULT '0' , ADD `stage_property` INT NULL DEFAULT '0' ;
ALTER TABLE `sub_menus` ADD `stage_expired_movement` INT NULL DEFAULT '0' , ADD `stage_type_movement` INT NULL DEFAULT '0' , ADD `stage_type_notification` TEXT NULL ;
ALTER TABLE  `sub_menus` ADD  `new_identifier` TEXT NULL AFTER  `change_identifier` ;
ALTER TABLE  `sub_menus` ADD  `new_identifier_start` VARCHAR( 250 ) NULL AFTER  `new_identifier` ;
ALTER TABLE `sub_menus` ADD `stage_type_movement_fail` INT NULL DEFAULT '0';

-- --------------------------------------------------------

--
-- Table structure for table `sub_menu_buttons`
--

CREATE TABLE `sub_menu_buttons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_menu_id` int(11) NOT NULL,
  `button_id` int(11) NOT NULL,
  `order_no` int(11) NOT NULL,
  `backend` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sub_menu_buttons_sub_menu_id_sub_menus_id` (`sub_menu_id`),
  KEY `sub_menu_buttons_button_id_buttons_id` (`button_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sub_menu_tasks`
--

CREATE TABLE `sub_menu_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_menu_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `creator_user_id` int(11) DEFAULT NULL,
  `owner_user_id` int(11) DEFAULT NULL,
  `sheet_id` int(11) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `start_date` text NOT NULL,
  `end_date` text NOT NULL,
  `priority` int(11) DEFAULT NULL,
  `is_leader` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `last_update` text NOT NULL,
  `date_created` text NOT NULL,
  `remarks` text NOT NULL,
  `application_id` int(11) DEFAULT NULL,
  `task_stage` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `task_comments`
--

CREATE TABLE `task_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commentcontent` text NOT NULL,
  `date_created` text NOT NULL,
  `reviewer_id` int(11) DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `task_forms`
--

CREATE TABLE `task_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `entry_id` int(11) NOT NULL,
  `created_on` varchar(250) NOT NULL,
  `updated_on` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `task_forms_settings`
--

CREATE TABLE `task_forms_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_type` int(11) NOT NULL,
  `task_department` varchar(250) NOT NULL,
  `task_application_id` int(11) NOT NULL,
  `task_comment_sheet` int(11) NOT NULL,
  `created_on` varchar(250) NOT NULL,
  `updated_on` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `task_queue`
--

CREATE TABLE `task_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `current_task_id` int(11) DEFAULT NULL,
  `next_task_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `task_transfer`
--

CREATE TABLE `task_transfer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) DEFAULT NULL,
  `from_user_id` int(11) DEFAULT NULL,
  `to_user_id` int(11) DEFAULT NULL,
  `reason` text NOT NULL,
  `date_created` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `upload_additional`
--

CREATE TABLE `upload_additional` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `application_id` int(11) DEFAULT NULL,
  `form_id` int(11) DEFAULT NULL,
  `entry_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `application_id_idx` (`application_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `upload_receipt`
--

CREATE TABLE `upload_receipt` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) DEFAULT NULL,
  `form_id` int(11) DEFAULT NULL,
  `entry_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `invoice_id_idx` (`invoice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `workflow`
--

CREATE TABLE `workflow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_title` text NOT NULL,
  `workflow_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `workflow_reviewers`
--

CREATE TABLE `workflow_reviewers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_id` int(11) DEFAULT NULL,
  `reviewer_id` int(11) DEFAULT NULL,
  `task_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `ap_number_generator` (
  `form_id` INT NOT NULL ,
  `application_number` VARCHAR(255) NOT NULL ,
  UNIQUE `form_id` (`form_id`)
) ENGINE = InnoDB;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `approval_condition`
--
ALTER TABLE `approval_condition`
  ADD CONSTRAINT `approval_condition_condition_id_conditions_of_approval_id` FOREIGN KEY (`condition_id`) REFERENCES `conditions_of_approval` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `approval_condition_entry_id_form_entry_id` FOREIGN KEY (`entry_id`) REFERENCES `form_entry` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ap_form_groups`
--
ALTER TABLE `ap_form_groups`
  ADD CONSTRAINT `ap_form_groups_form_id_ap_forms_form_id` FOREIGN KEY (`form_id`) REFERENCES `ap_forms` (`form_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ap_form_groups_group_id_form_groups_group_id` FOREIGN KEY (`group_id`) REFERENCES `form_groups` (`group_id`) ON DELETE CASCADE;

--
-- Constraints for table `content`
--
ALTER TABLE `content`
  ADD CONSTRAINT `content_type_id_content_type_id` FOREIGN KEY (`type_id`) REFERENCES `content_type` (`id`);

--
-- Constraints for table `entry_decline`
--
ALTER TABLE `entry_decline`
  ADD CONSTRAINT `entry_decline_entry_id_form_entry_id` FOREIGN KEY (`entry_id`) REFERENCES `form_entry` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `form_entry`
--
ALTER TABLE `form_entry`
  ADD CONSTRAINT `form_entry_form_id_ap_forms_form_id` FOREIGN KEY (`form_id`) REFERENCES `ap_forms` (`form_id`) ON DELETE CASCADE;

--
-- Constraints for table `mf_guard_group_permission`
--
ALTER TABLE `mf_guard_group_permission`
  ADD CONSTRAINT `mf_guard_group_permission_group_id_mf_guard_group_id` FOREIGN KEY (`group_id`) REFERENCES `mf_guard_group` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `mf_guard_user_permission`
--
ALTER TABLE `mf_guard_user_permission`
  ADD CONSTRAINT `mf_guard_user_permission_permission_id_mf_guard_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `mf_guard_permission` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `mf_invoice`
--
ALTER TABLE `mf_invoice`
  ADD CONSTRAINT `mf_invoice_app_id_form_entry_id` FOREIGN KEY (`app_id`) REFERENCES `form_entry` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `mf_invoice_detail`
--
ALTER TABLE `mf_invoice_detail`
  ADD CONSTRAINT `mf_invoice_detail_invoice_id_mf_invoice_id` FOREIGN KEY (`invoice_id`) REFERENCES `mf_invoice` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permits`
--

--
-- Constraints for table `sf_guard_forgot_password`
--
ALTER TABLE `sf_guard_forgot_password`
  ADD CONSTRAINT `sf_guard_forgot_password_user_id_sf_guard_user_id` FOREIGN KEY (`user_id`) REFERENCES `sf_guard_user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sf_guard_group_permission`
--
ALTER TABLE `sf_guard_group_permission`
  ADD CONSTRAINT `sf_guard_group_permission_group_id_sf_guard_group_id` FOREIGN KEY (`group_id`) REFERENCES `sf_guard_group` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sf_guard_group_permission_permission_id_sf_guard_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `sf_guard_permission` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sf_guard_remember_key`
--
ALTER TABLE `sf_guard_remember_key`
  ADD CONSTRAINT `sf_guard_remember_key_user_id_sf_guard_user_id` FOREIGN KEY (`user_id`) REFERENCES `sf_guard_user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sf_guard_user_group`
--
ALTER TABLE `sf_guard_user_group`
  ADD CONSTRAINT `sf_guard_user_group_group_id_sf_guard_group_id` FOREIGN KEY (`group_id`) REFERENCES `sf_guard_group` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sf_guard_user_group_user_id_sf_guard_user_id` FOREIGN KEY (`user_id`) REFERENCES `sf_guard_user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sf_guard_user_permission`
--
ALTER TABLE `sf_guard_user_permission`
  ADD CONSTRAINT `sf_guard_user_permission_permission_id_sf_guard_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `sf_guard_permission` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sf_guard_user_permission_user_id_sf_guard_user_id` FOREIGN KEY (`user_id`) REFERENCES `sf_guard_user` (`id`) ON DELETE CASCADE;


----
---- UPDATES 2015-03-23
----

ALTER TABLE `audit_trail` ADD INDEX ( `form_entry_id` ) ;

ALTER TABLE `saved_permit` ADD INDEX ( `application_id` ) ;

ALTER TABLE `form_entry` ADD INDEX ( `user_id` ) ;

ALTER TABLE `form_entry` ADD INDEX ( `entry_id` ) ;

ALTER TABLE `notification_history` ADD INDEX ( `application_id` ) ;

ALTER TABLE  `saved_permit` DROP  `permit` ;
ALTER TABLE  `form_entry` DROP  `saved_permit` ;

ALTER TABLE `mf_invoice` ADD INDEX ( `created_at` ) ;
ALTER TABLE `mf_invoice` ADD INDEX ( `paid` ) ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
