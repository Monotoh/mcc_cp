<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

      public function __construct(){
                 parent::__construct();
                 $this->load->model('generic_model','generic');
                 $this->load->model('egeneric_model','egeneric');
                 $this->load->library('session');
                 $this->load->library('manager');
                 $this->load->library('pagination');
                 $this->load->library('user_lib');
                 $this->load->model('register_model');
                 $this->load->library('user_lib');
                 $this->load->library('email');

        }




     public function index(){

       $data["graphis"] = "false";
	   $data["hide"] = "true";



       $this->load->view("reports/header",$data);
       $this->load->view("reports/nav");
       $this->load->view("reports/body");
       $this->load->view("reports/footer");
      }

      public function general($date1,$date2){

          $data["graphis"]          =  "general";

          $incoming                 = $this->egeneric->in_coming($date1,$date2);
          $outgoing                 = $this->egeneric->out_going($date1,$date2);
          $data["incoming"]         = $incoming;
          $data["out_going"]        = $outgoing;
          //pass end date

          $data["start_date"]       = $date1;
          $data["end_date"]         = $date2;


          $stages                   = array('Submissions','Resubmissions - Submissions', 'Corrections-Submisstions','Billing', 'Client Invoice Communication',  'Commenting-internal Review', 'Correction-internal','Resubmissions - Internal Review','Comments Review - Internnal Review','Rejections','Commenting - External Review','Comments Review - External Review','Corrections - External Review','Resubmissions - External Review','Issuance');
          $data["stages"]           = $stages;





       $this->load->view("reports/header",$data);
       $this->load->view("reports/nav");
       $this->load->view("reports/body");
       $this->load->view("reports/footer");


      }

      public function monthly($date){

       $data["graphis"]          =  "monthly";

       //


       $this->load->view("reports/header",$data);
       $this->load->view("reports/nav");
       $this->load->view("reports/body");
       $this->load->view("reports/footer");

      }

      public function annually($year){

        $data["graphis"]           =  "yearly";
        $data["months"]            = array("January", "February", "March", "April", "May","June","July","August","September","October","November","December");
        $months                    = array("01","02","03","04","05","06","07","08","09","10","11","12");
        $money_ar                  = array();
        $total_invoices_ar         = array();

        for($i = 0; $i < 12; $i++){

           $day                            = cal_days_in_month(CAL_GREGORIAN,$months[$i],$year);


            $start_date                    =  $year."-".$months[$i]."-01";

            $end_date                      =  $year."-".$months[$i]."-".$day;

           $money = $this->egeneric->annual_total($start_date,$end_date); //paid invoices
           array_push($money_ar,$money);

          $total_invoices = $this->egeneric->total_invoices($start_date,$end_date); //total issued invoices
          array_push($total_invoices_ar,$total_invoices);

        }

         $data["money"]                    =   $money_ar;


        //get money for january



       $this->load->view("reports/header",$data);
       $this->load->view("reports/nav");
       $this->load->view("reports/body");
       $this->load->view("reports/footer");

      }



     public function menu(){

   $this->load->view("reports/header");
   $this->load->view("reports/nav");
   $this->load->view("reports/menu");
   $this->load->view("reports/footer");
 }




    public function move_to(){

                 $report          = $this->input->post("report");
                 $date_filter     = $this->input->post("dates");
                if(isset($report) and $report== "general"){



                 if(!isset($date_filter) ){
                   redirect("reports/tasks/".$date1."/".$date2 );

                 }else{

                     $dates           =    explode("-",$date_filter);
                     $date1           =  $this->manager->dated($dates[0]);
                     $date2           =  $this->manager->dated($dates[1]);

                     $date1           = trim($date1);
                     $date2           = trim($date2);


                     redirect("reports/general/".$date1."/".$date2 );

                 }


         } else if(isset($report) and $report == "in_p"){

                     $dates           =    explode("-",$date_filter);
                     $date1           =  $this->manager->dated($dates[0]);
                     $date2           =  $this->manager->dated($dates[1]);

                     $date1           = trim($date1);
                     $date2           = trim($date2);



                      redirect("reports/tasks/".$date1."/".$date2 );

         } else if(isset($report) and $report == "annual"){
           $year     = $this->input->post("year");
            if(!empty($year)){
             redirect("reports/annually/".$year);
            }else{
             redirect("reports/");
            }


        }
        else if(isset($report) and $report == "monthly"){

        $month     = $this->input->post("month");

        if(!empty($month)){
        $month     =  $this->manager->dated($month);
        redirect("reports/monthly/".$month);
        }else{
          redirect("reports/");
        }



        }else{

          redirect("reports/");


        }






    }


   public function tasks($start_date=0,$end_date=0){

      $array_users     =  array(34,24,28,42,43,53,46,47,36,35,38,37,48);
      $array_names     =  array();

      $total_tasks     =  array();
      $array_complete  =  array();
      $cancelled_task_arr  =  array();
      $pending_task_ar =  array();

      $i               = 0;

      //get name task get total

      while(  $i  <  sizeof($array_users)){

           //get name
             $names         = $this->egeneric->indv_name($array_users[$i]);

             array_push($array_names,$names);//push names

             $total_task    =  $this->egeneric->total_assigned($start_date,$end_date,$array_users[$i]);   //total


             array_push($total_tasks ,$total_task);//push totals tasks

            $complete_task  = $this->egeneric->task_completed($start_date,$end_date,$array_users[$i]); //complte task


            $cancelled_task  = $this->egeneric->cancelled_task($start_date,$end_date,$array_users[$i]); //cancelled task
            array_push($cancelled_task_arr,$cancelled_task);

            array_push($array_complete,$complete_task );//push totals tasks

            //get array task
             $pending_task  = $this->egeneric->pending_task($start_date,$end_date,$array_users[$i]);
             array_push($pending_task_ar,$pending_task);//push totals tasks

         $i++;
       }



          $data["graphis"]            =   "task_person";
          $data["names"]              =    $array_names;
          $data["total"]              =    $total_tasks;
          $data["complete_task"]      =    $array_complete;
          $data["cancelled_task_arr"]=    $cancelled_task_arr;

          $data["array_users"]        =    $array_users;
          $data["start_date"]         =    $start_date;
          $data["end_date"]           =    $end_date;
          $data["pending_task"]       =    $pending_task_ar;


       $this->load->view("reports/header",$data);
       $this->load->view("reports/nav");
       $this->load->view("reports/body");
       $this->load->view("reports/footer");




   }


  public function single($userid,$start_date=0,$end_date=0){





     $array_users      =  array(34,24,28,42,43,53,46,47,36,35,38,37,48);
     $array_names      =  array();

     $total_tasks       =  array();
     $array_complete    =  array();
     $pendind_task_arr  =  array();
     $cancelled_task_arr  =  array();

     $i                 = 0;

      //get name task get total

      while(  $i  <  sizeof($array_users)){

           //get name
             $names     = $this->egeneric->indv_name($array_users[$i]);

             array_push($array_names,$names);//push names

             $total_task =  $this->egeneric->total_assigned($start_date,$end_date,$array_users[$i]);   //total

             array_push($total_tasks ,$total_task);//push totals tasks

            $complete_task  = $this->egeneric->task_completed($start_date,$end_date,$array_users[$i]); //complete task

            $cancelled_task  = $this->egeneric->cancelled_task($start_date,$end_date,$array_users[$i]); //complete task
            array_push($cancelled_task_arr,$cancelled_task);

            $pending_task  = $this->egeneric->pending_task($start_date,$end_date,$array_users[$i]);
            array_push($pendind_task_arr,$pending_task);//push totals tasks

             if($array_users[$i] == $userid){
                $data["ind_pendind"]    = $pending_task;
                $data["ind_complete"]   = $complete_task;
                $data["ind_cancelled"]   = $cancelled_task;
                $data["ind_total_task"] = $total_task;

             }

            array_push($array_complete,$complete_task );//push totals tasks


         $i++;

       }

         $data["userid"]            =    $userid;

         $data["graphis"]           =    "individuals";
         $data["names"]             =    $array_names;
         $data["total"]             =    $total_tasks;
         $data["complete_task"]     =    $array_complete;
         $data["pendind_task_arr"]  =    $pendind_task_arr;
         $data["cancelled_task_arr"]=    $cancelled_task_arr;

        $data["array_users"]        =    $array_users;
        $data["start_date"]         =    $start_date;
        $data["end_date"]           =    $end_date;



 $data["all"]  =  $this->egeneric->all_task_per_user($userid,$start_date,$end_date);
       $this->load->view("reports/header",$data);
       $this->load->view("reports/nav");
       $this->load->view("reports/body");
       $this->load->view("reports/footer");









  }



}
