<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Generic_model extends CI_model{
    //crud creact read update delete

        function create($table,$userdata){
         $data = array(
                'refnum'=> $_SESSION['refnum']
                );
               $row = $this->check_duplicate($table,$data);

          if($row == 0){
              $this->db->insert($table,$userdata);
            }


        }

        function read($table,$array_data){
            $query = $this->db->get_where($table,$array_data);
            return $query->row();
        }

          function read_products($table,$key,$start_from){
           $this->db->select('*');
                  $this->db->from($table);
                  $this->db->where('publication.status',$key);
                  $this->db->limit(15,$start_from);
                  $this->db->order_by('id', 'asc');
                  $query = $this->db->get();
                  return $query->result();


         }

          function read_vacancy($start_from){
            $this->db->select('*');
                  $this->db->from('vacancy_information');
                  $this->db->limit(1,$start_from);
                  $this->db->order_by('date_posted','asc');
                  $query = $this->db->get();
                  return $query->result();


         }

        function read_info($table,$array_data){
            $query = $this->db->get_where($table,$array_data);
            return $query->result();
        }

        function update($table,$userdata){
             $this->db->where('refnum', $_SESSION['refnum']);
             $this->db->update($table,$userdata);

        }


      function  check_duplicate($table,$array_data){
         $query = $this->db->get_where($table,$array_data);
         return $query->num_rows();
        }

         function  count_vacancy($table){
         $query = $this->db->get($table);
         return $query->num_rows();
        }

        public function count($table,$keys){
       return $this->db->count_all($table,$keys);
    }


       public function read_post($category,$start_from){
                  $this->db->select('*');
                  $this->db->from('topic');
                  $this->db->where('category',$category);
                   $this->db->limit(15,$start_from);
                  $query = $this->db->get();
                  return $query->result();

       }
          public function count_product($category){
                  $this->db->select('*');
                  $this->db->from('product');
                  $this->db->where('category',$category);
                  $query = $this->db->get();
                  return $query->num_rows();

       }


     public  function get_category(){
              $query = $this->db->query('SELECT DISTINCT category FROM  product');
              return $query->result();
        }


        public function return_ctry($category){
                  $query = $this->db->query('SELECT DISTINCT pro_location.district FROM  pro_location
                                             join product where
                                             pro_location.id = product.id
                                             and
                                             product.category = "'.$category.'"
                                              ');

                  return $query->result();

       }

        public function return_items($category,$district){
                  $query = $this->db->query('SELECT DISTINCT product.product FROM  product
                                             join pro_location where
                                             product.id = pro_location.id
                                             and
                                             product.category    = "'.$category.'"
                                             and
                                             pro_location.district = "'.$district.'"
                                              ');

                  return $query->result();

       }

         public function filter($category,$district,$item,$start_from){
            $this->db->select('*');
                      $this->db->from('product');
                      $this->db->join('pro_location','product.id = pro_location.id');
                      $this->db->where('product.category',$category);
                      $this->db->where('product.product',$item);
                      $this->db->where('pro_location.district',$district);
                      $this->db->limit(20,$start_from);
                      $query = $this->db->get();
                      return $query->result();

        }

          public function filter_count($category,$district,$item,$start_from){
            $this->db->select('*');
                      $this->db->from('product');
                      $this->db->join('pro_location','product.id = pro_location.id');
                      $this->db->where('product.category',$category);
                      $this->db->where('product.product',$item);
                      $this->db->where('pro_location.district',$district);
                      $query = $this->db->get();
                      return $query->num_rows();

       }



          public function specific_location($city,$village,$category,$start_from){
            $this->db->select('*');
                      $this->db->from('product');
                      $this->db->join('pro_location','product.id = pro_location.id');
                      if($village != "all"){
                      $this->db->where('pro_location.village',$village);
                      }
                      $this->db->where('pro_location.district',$city);
                      $this->db->where('product.category',$category);
                      $this->db->limit(12,$start_from);
                      $query = $this->db->get();
                      return $query->result();

        }

           public function specific_location_count($city,$village,$category,$start_from){
            $this->db->select('*');
                      $this->db->from('product');
                      $this->db->join('pro_location','product.id = pro_location.id');
                      $this->db->where('pro_location.village',$village);
                      $this->db->where('product.category',$category);
                      $this->db->where('pro_location.district',$city);
                      $query = $this->db->get();
                      return $query->num_rows();

        }



            public function filter_service($start_from){
            $this->db->select('*');
                      $this->db->from('pro_user');
                      $this->db->join('user_extra','pro_user.refnum = user_extra.id');
                      $this->db->join('profile','pro_user.refnum= profile.id');
                      $this->db->limit(12,$start_from);
                      $query = $this->db->get();
                      return $query->result();

            }


            public function specific_career($career,$start_from){
            $this->db->select('*');
                      $this->db->from('pro_user');
                      $this->db->join('user_extra','pro_user.refnum = user_extra.id');
                      $this->db->join('profile','pro_user.refnum= profile.id');
                      $this->db->where('user_extra.career',$career);
                      $this->db->limit(12,$start_from);
                      $query = $this->db->get();
                      return $query->result();

            }

              public function specific_career_count($career,$start_from){
              $this->db->select('*');
                      $this->db->from('pro_user');
                      $this->db->join('user_extra','pro_user.refnum = user_extra.id');
                      $this->db->join('profile','pro_user.refnum= profile.id');
                      $this->db->where('user_extra.career',$career);
                      $query = $this->db->get();
                      return $query->num_rows();

            }

            public function filter_products($category,$products,$start_from){
            $this->db->select('*');
                      $this->db->from('product');
                      $this->db->join('pro_location','product.id = pro_location.id');
                      $this->db->where('product.category',$category);
                      $this->db->where('product.product',$products);
                      $this->db->limit(12,$start_from);
                      $query = $this->db->get();
                      return $query->result();

            }

            public function filter_products_count($category,$products,$start_from){
            $this->db->select('*');
                      $this->db->from('product');
                      $this->db->where('category',$category);
                      $this->db->where('product',$products);
                      $this->db->limit(12,$start_from);
                      $query = $this->db->get();
                      return $query->num_rows();

            }

            public function news($start_from){
            $this->db->select('*');
                      $this->db->from('news');
                      $this->db->limit(20,$start_from);
                      $query = $this->db->get();
                      return $query->result();

            }

            public function news_filter($start_from,$token){
            $this->db->select('*');
                      $this->db->from('news');
                      $this->db->where('category',$token);
                      $this->db->limit(20,$start_from);
                      $query = $this->db->get();
                      return $query->result();

            }

            public function news_counter($start_from,$token){
            $this->db->select('*');
                      $this->db->from('news');
                      $this->db->where('category',$token);
                      $query = $this->db->get();
                      return $query->num_rows();

            }

            function read_products12($table,$key,$start_from){
           $this->db->select('*');
                  $this->db->from($table);
                  $this->db->where('publication.status',$key);
                  $this->db->limit(15,$start_from);
                  $this->db->order_by('id', 'asc');
                  $query = $this->db->get();
                  return $query->result();


         }

        public   function delete($table,$keys){
            $this->db->delete($table, $keys);
        }

}