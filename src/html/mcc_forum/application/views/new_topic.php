<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mcc Forum :: New topic</title>

        <!-- Bootstrap -->
        <link href="<?php echo base_url("assets/"); ?>css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom -->
        <link href="<?php echo base_url("assets/"); ?>css/custom.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
          <![endif]-->

        <!-- fonts -->
        <link rel="stylesheet" href="<?php echo base_url("assets/"); ?>font-awesome-4.0.3/css/font-awesome.min.css">

        <!-- CSS STYLE-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/"); ?>css/style.css" media="screen" />

        <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/"); ?>rs-plugin/css/settings.css" media="screen" />

    </head>
    <body>

        <div class="container-fluid">

            <!-- Slider -->
            <div class="tp-banner-container">
                <div class="tp-banner" style="background-image: url(<?php echo base_url("assets/"); ?>images/bg2.png)" >
                    <ul>
                        <!-- SLIDE  -->
                        <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                            <!-- MAIN IMAGE -->
                            <img src="<?php echo base_url("assets/"); ?>images/bg2.png"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                            <!-- LAYERS -->
                        </li>
                    </ul>
                </div>
            </div>
            <!-- //Slider -->

            <div class="headernav">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-1 col-xs-3 col-sm-2 col-md-2 logo "><a href="index.html"><img src="images/logo.jpg" alt=""  /></a></div>
                        <div class="col-lg-3 col-xs-9 col-sm-5 col-md-3 selecttopic">
                           <!-- <div class="dropdown">
                                <a data-toggle="dropdown" href="#">Borderlands 2</a> <b class="caret"></b>
                                <ul class="dropdown-menu" role="menu" >
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Borderlands 1</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-2" href="#">Borderlands 2</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-3" href="#">Borderlands 3</a></li>

                                </ul>
                            </div>-->
                        </div>
                        <div class="col-lg-4 search hidden-xs hidden-sm col-md-3">
                            <div class="wrap">
                                    <?php echo form_open("index.php/post/move_to") ?>
                                    <div class="pull-left txt"><input type="text" name="search_key" class="form-control" placeholder="Search Topics" minlength="10"  maxlength="50" required="required"></div>
                                    <div class="pull-right"><button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button></div>
                                    <div class="clearfix"></div>
                                    <?php echo form_close(); ?>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xs-12 col-sm-5 col-md-4 avt">
                            <div class="stnt pull-left">
                                    <a  href="<?php echo base_url("index.php/post/index"); ?>" class="btn btn-primary">Back to Forum</a>
                            </div>


                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>



            <section class="content">
                <div class="container">
                  <br>
                  <br>
                </div>


                <div class="container">
                    <div class="row">
                       <div class="col-lg-2 col-md-2">
                     </div>
                        <div class="col-lg-8 col-md-8">



                            <!-- POST -->
                            <div class="post">
                               <?php echo form_open(base_url("index.php/post/submit_topic"),"class='form newtopic'"); ?>
                                    <div class="topwrap">
                                        <div class="userinfo pull-left">
                                            <div class="avatar">
                                               <img src="<?php echo base_url("assets/"); ?>images/avatar.png" alt="" />
                                            </div>

                                            <div class="icons">
                                                <img src="images/icon3.jpg" alt="" /><img src="images/icon4.jpg" alt="" /><img src="images/icon5.jpg" alt="" /><img src="images/icon6.jpg" alt="" />
                                            </div>
                                        </div>
                                        <div class="posttext pull-left">

                                            <div>
                                                <input type="text" placeholder="Enter Topic Title" class="form-control"  name="topic_title" required="required" minlength="20"  maxlength="50"  />
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-12 col-md-12">
                                                    <select name="category" id="category"  class="form-control" required="required" >
                                                        <option value="" disabled selected>Select Category</option>
                                                        <option value="op1">Debug</option>
                                                        <option value="op2">Complain</option>
                                                        <option value="op2">Other</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-12 col-md-12">
                                                    <input type="text" name="name" class="form-control" placeholder="Name here.." required="required" minlength="10" maxlength="70" >
                                                </div>
                                                <div class="col-lg-12 col-md-12">
                                                    <input type="email" name="email" class="form-control" placeholder="Email here.." required="required" >
                                                </div>
                                            </div>

                                            <div>
                                                <textarea name="desc" id="desc" placeholder="Description"  class="form-control" required="required" minlength="40" maxlength="200" ></textarea>
                                            </div>
                                            <div class="row newtopcheckbox">
                                                <div class="col-lg-12 col-md-12">
                                                    <div><p>Who can see this?</p></div>
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-4">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="radio" id="everyone" name="target" value="everyone" required="required" /> Everyone
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8 col-md-8">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="radio" id="friends"  name="target" value="mcc"  required="required" /> Mcc Support team only
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>


                                        </div>
                                        <div class="clearfix"></div>
                                    </div>                              
                                    <div class="postinfobot">


                                        <div class="pull-right postreply">
                                            <div class="pull-left"><button type="submit" class="btn btn-primary">Post</button></div>
                                            <div class="clearfix"></div>
                                        </div>


                                        <div class="clearfix"></div>
                                    </div>
                                 <?php echo form_close();  ?>
                            </div><!-- POST -->

                            <!-- POST -->





                        </div>
                        <div class="col-lg-2 col-md-2">


                        </div>
                    </div>
                </div>






            </section>

            <footer>
                <div class="container">
                    <div class="row">
                         <div class="col-lg-8 col-xs-9 col-sm-5 ">Designed by Computer Business Solutions</div>

                    </div>
                </div>
            </footer>
        </div>

        <!-- get jQuery from the google apis -->
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.js"></script>

        <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
        <script type="text/javascript" src="<?php echo base_url("assets/"); ?>rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/"); ?>rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url("assets/"); ?>js/bootstrap.min.js"></script>


        <!-- LOOK THE DOCUMENTATION FOR MORE INFORMATIONS -->
        <script type="text/javascript">

            var revapi;

            jQuery(document).ready(function() {
                "use strict";
                revapi = jQuery('.tp-banner').revolution(
                        {
                            delay: 15000,
                            startwidth: 1200,
                            startheight: 278,
                            hideThumbs: 10,
                            fullWidth: "on"
                        });

            });	//ready

        </script>

        <!-- END REVOLUTION SLIDER -->
    </body>
</html>