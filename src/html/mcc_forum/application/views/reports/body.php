<div class="wrapper">
            <div class="containr-fluid">

                <!-- start page title -->
                <div class="row">
                   <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <div class="page-title-box">
                            <div class="page-title-right">

                            </div>

                            <h4 class="page-title">Dashboard</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                    <?php echo form_open("reports/move_to"); ?>
                      <div class="row">
                      <div class="col-md-1"></div>
                           <div class="col-md-5">
                                  <select class="form-control" id="parent"  name="report"  style="border-radius: 0px;" required="required">
                                   <option value="">Types Of Reports</option>
                                   <option value="general">Applications Processing Report </option>
                                   <option value="in_p"> Individual Perfomance  Report</option>
                                   <option value="annual"> Finacial Report - Annual </option>
                                  <!-- <option value="monthly">Finacial Report - Monthly </option>-->

                                 </select>
                           </div>


                              <div class="col-md-4" id="first">
                                   <input type="text" name="dates"  value=""  class="form-control" />
                              </div>

                              <div class="col-md-4" id="annaul" style="display:none;">
                                   <select name="year" id="year"   class="form-control"  style="display:none;" >
                                        <option value="">Pick a Year</option>
                                        <?php for($i = 2015; $i <= date("Y"); $i++): ?>
                                          <option value="<?=$i; ?>"><?=$i; ?></option>
                                        <?php endfor; ?>
                                   </select>
                              </div>

                              <div class="col-md-4" id="monthly" style="display:none;">
                                   <input type="date" name="month" class="form-control" id="month" style="display:none;">
                              </div>

                             <div class="col-md-1">
                              <button class="btn btn-primary" type="submit">Search</button>
                             </div>

                       </div>
                    <?php echo form_close(); ?>


                <!-- end row-->



                <div class="row" id="print" style=" margin-top: 10px;">
                     <br>
                     <br>
                     <!-- <div style="width :400px;height: 400px; ">
                          <canvas id="myChart"></canvas>
                      </div>-->


                    <div class="col-md-1"></div>

                    <?php if(isset($graphis) and $graphis == "false"): ?>

                                <div class="col-md-10" style=" margin-top: 100px;">

                                  <h1 style="text-align: center;color : rgba(181, 181, 181, 1)">Please filter Reports for Results</h1>

                               </div>


                     <?php endif; ?>


                    <?php  if(isset($graphis) and $graphis == "general"): ?>

                                 <div class="col-lg-5">
                                <div class="card-box">
                                    <h4 class="header-title mb-3">Application Movement Summary</h4>
                                     <table class="table">
                                         <tr>
                                              <th style="text-align: left;">Stage</th>
                                              <th>Incoming </th>
                                              <th>Out going  </th>

                                         </tr>

                                          <?php $i = 0; ?>
                                          <?php if(isset($stages)): ?>
                                               <?php while($i < sizeof($stages)): ?>
                                                 <tr>
                                                      <td style="text-align: left;"><?php echo $stages[$i]; ?> </td>
                                                      <td style="text-align: center;"><?php if(isset($incoming))  echo $incoming[$i]; else  echo 0; ?> </td>
                                                      <td style="text-align: center;"><?php if(isset($out_going)) echo $out_going[$i]; else  echo 0; ?> </td>

                                                </tr>
                                                   <?php $i++; ?>
                                               <?php endwhile; ?>
                                          <?php endif;  ?>
                                     </table>

                                </div> <!-- end card-->
                            </div>

                              <div class="col-md-5">
                                    <div class="card-box">
                                    <h4 class="header-title mb-3">Bar Chart Summary</h4>
                                    <canvas id="myChart1"  height="200"></canvas>
                                   </div>
                               </div>





                     <?php  endif; ?>




                     <?php  if(isset($graphis) and $graphis == "task_person"): ?>

                                <div class="col-lg-5">
                                <div class="card-box">
                                    <h4 class="header-title mb-3">Task Summary</h4>
                                        <table class="table">
                                         <tr>
                                              <th>Name</th>
                                              <th style="text-align: center;">Assigned</th>
                                              <th style="text-align: center;">Completed</th>
                                              <th style="text-align: center;">Cancelled</th>
                                              <th style="text-align: center;">Pending  </th>
                                         </tr>
                                          <?php $i = 0; ?>

                                                <?php if(isset($names)):   ?>
                                                     <?php while($i <  sizeof($names)):  ?>
                                                                <tr>
                                                                     <td> <a href="<?php echo base_url("index.php/reports/single/".$array_users[$i]."/".$start_date."/".$end_date); ?>" ><?php echo $names[$i]; ?></a>    </td>
                                                                     <td style="text-align: center;"><?php if(isset($total[$i]))  echo $total[$i]; else 0;   ?></td>
                                                                     <td style="text-align: center;"><?php if(isset($complete_task[$i]))  echo $complete_task[$i]; else 0;   ?></td>
                                                                     <td style="text-align: center;"><?php if(isset($cancelled_task_arr[$i])) echo  $cancelled_task_arr[$i];else 0;   ?></td>
                                                                     <td style="text-align: center;"> <?php if(isset($pending_task[$i]))  echo $pending_task[$i]; else 0;  ?> </td>

                                                                </tr>
                                                                <?php $i++; ?>
                                                        <?php endwhile; ?>
                                                <?php endif; ?>
                                        </table>


                                </div> <!-- end card-->
                            </div>



                              <div class="col-md-5">
                                    <div class="card-box">
                                    <h4 class="header-title mb-3">Bar Chart</h4>
                                    <canvas id="myChart1"  height="200"></canvas>
                                   </div>
                               </div>





                        <?php  endif; ?>



                         <?php  if(isset($graphis) and $graphis == "individuals"): ?>


                             <div class="col-lg-5">
                                <div class="card-box">

                                    <h4 class="header-title mb-3">Task Summary</h4>
                                        <table class="table">
                                         <tr>
                                              <th>Name</th>
                                              <th style="text-align: center;">Assigned</th>
                                              <th style="text-align: center;">Completed</th>
                                              <th style="text-align: center;">Cancelled</th>
                                              <th style="text-align: center;">Pending  </th>
                                         </tr>
                                          <?php $i = 0; ?>

                                                <?php if(isset($names)):   ?>
                                                     <?php while($i <  sizeof($names)):  ?>
                                                                <tr>
                                                                     <td   > <a href="<?php echo base_url("index.php/reports/single/".$array_users[$i]."/".$start_date."/".$end_date); ?>" <?php if($userid  == $array_users[$i]){  echo "style='color : red;'"; } ?>   ><?php echo $names[$i]; ?></a>    </td>
                                                                     <td style="text-align: center;"><?php if(isset($total[$i]))  echo $total[$i]; else 0;   ?></td>
                                                                     <td style="text-align: center;"><?php if(isset($complete_task[$i]))     echo  $complete_task[$i];    else 0;   ?></td>
                                                                     <td style="text-align: center;"><?php if(isset($cancelled_task_arr[$i])) echo  $cancelled_task_arr[$i];else 0;   ?></td>
                                                                     <td style="text-align: center;"> <?php if(isset($pendind_task_arr[$i])) echo  $pendind_task_arr[$i]; else 0;   ?> </td>

                                                                </tr>
                                                                <?php $i++; ?>
                                                        <?php endwhile; ?>
                                                <?php endif; ?>
                                        </table>


                                </div> <!-- end card-->
                            </div>



                          <div class="col-md-5">
                                    <div class="card-box">
                                    <h4 class="header-title mb-3">Bar Chart</h4>
                                    <canvas id="pie"  height="120"></canvas>
                                    <table class="table">
                                              <tr>
                                                 <td>Reference Number</td>
                                                 <td>Task type</td>
                                                 <td>Created On</td>
                                                 <td>Completed On</td>

                                              </tr>
                                                <?php if(isset($all)): ?>
                                                    <?php foreach($all as $tasks): ?>
                                                      <tr>
                                                           <td><?=$tasks->application_id; ?></td>
                                                         <td>  <?php  if($tasks->type == 2 ) echo "Assessment";  ?>
                                                               <?php  if($tasks->type == 3 ) echo "Invoicing";  ?>
                                                           </td>
                                                        <!--<td> <?=$tasks->strfirstname; ?>&nbsp;<?=$tasks->strlastname; ?> </td>-->

                                                         <td><?=$tasks->date_created;?></td>
                                                         <td><?=$tasks->end_date;?></td>
                                                      </tr>

                                                    <?php endforeach; ?>
                                                <?php endif; ?>


                                         </table
                                   </div>
                            </div>

                         <?php  endif; ?>


                         <?php  if(isset($graphis) and $graphis == "yearly"): ?>


                             <div class="col-lg-5">
                                <div class="card-box">

                                    <h4 class="header-title mb-3">Financial Summary</h4>
                                        <table class="table">
                                         <tr>
                                              <th>Month</th>
                                              <th style="text-align: right;">Cash </th>
                                         </tr>
                                         <?php if(isset($months) and !empty($months)): ?>
                                                <?php $i = 0; ?>
                                                <?php while($i < sizeof($months)): ?>

                                                     <tr>
                                                          <td><?=$months[$i]; ?></td>
                                                          <td style="text-align: right;">M <?=number_format($money[$i],2); ?> </td>
                                                     </tr>

                                                 <?php $i++; ?>
                                                 <?php endwhile; ?>
                                                 <tr>
                                                   <th>Total </th>
                                                   <th style="text-align: right;">M <?php $total = array_sum($money);
                                                               echo  number_format($total,2);
                                                                ?>
                                                    </th>
                                                 </tr>
                                         <?php endif; ?>

                                        </table>


                                </div> <!-- end card-->
                            </div>



                          <div class="col-md-5">
                                    <div class="card-box">
                                    <h4 class="header-title mb-3">Bar Chart</h4>
                                    <canvas id="bar-chart"  height="200"></canvas>
                                   </div>
                            </div>

                         <?php  endif; ?>

                          <?php  if(isset($graphis) and $graphis == "monthly"): ?>


                             <div class="col-lg-5">
                                <div class="card-box">

                                    <h4 class="header-title mb-3">Task Summary</h4>
                                        <table class="table">
                                         <tr>
                                              <th>Name</th>
                                              <th style="text-align: center;">Assigned</th>
                                              <th style="text-align: center;">Completed</th>
                                              <th style="text-align: center;">Pending  </th>
                                         </tr>

                                        </table>


                                </div> <!-- end card-->
                            </div>



                          <div class="col-md-5">
                                    <div class="card-box">
                                    <h4 class="header-title mb-3">Bar Chart</h4>
                                    <canvas id="bar-chart"  height="200"></canvas>
                                   </div>
                            </div>

                         <?php  endif; ?>

                </div>

                <!-- end row -->
                 <br>
				  <?php if(!isset($hide)): ?>
                 <div class="row">
                       <div class="col-md-1"></div>
                        <div class="col-md-10">
                        <button class="btn btn-primary print">Print</button>
                       </div>

                       <br>
                       <br>
                       <br>
                  </div>
                   <?php endif; ?>
            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->