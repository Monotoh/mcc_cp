<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

try {
//do mysql connection
$connection = mysqli_connect("localhost", "root", "rw@nd@bpm15_2016", "testbed");
//statement
$statement1 = "DROP TABLE IF EXISTS project_datasource_cp";
//query resultz
$table_droped = mysqli_query($connection, $statement1) or die("Database Error Droping Table");
//re-create the table with our new fields
$statement_2 = "CREATE TABLE project_datasource_cp AS (SELECT form_entry.id as plan_id, form_entry.date_of_submission as date_of_submission, form_entry.date_of_response as date_of_response,
mf_invoice.total_amount as Fees,
CASE form_entry.form_id
WHEN 200000 THEN 'Construction Permit' WHEN 201251 THEN 'Construction Permit' WHEN 235992 THEN 'Construction Permit' WHEN 229692 THEN 'Construction Permit'
WHEN 219480 THEN 'Extension Permit' WHEN 213665 THEN 'Extension Permit' WHEN 239228 THEN 'Extension Permit' WHEN 233480 THEN 'Extension Permit'
WHEN 215327 THEN 'Demolition Permit' WHEN 220384 THEN 'Demolition Permit' WHEN 234966 THEN 'Demolition Permit' WHEN 230688  THEN 'Demolition Permit'
WHEN 214658 THEN 'Refurbishment Without Structural Alteration' WHEN 220023 THEN 'Refurbishment Without Structural Alteration'  WHEN 236928 THEN 'Refurbishment Without Structural Alteration' WHEN 232199 THEN 'Refurbishment Without Structural Alteration'
WHEN 213916 THEN 'Refurbishment With Structural Alteration' WHEN 219859 THEN 'Refurbishment With Structural Alteration' WHEN 244647 THEN 'Refurbishment With Structural Alteration' WHEN 232964 THEN 'Refurbishment With Structural Alteration'
WHEN 202225 THEN 'Occupancy' WHEN 202752 THEN 'Occupancy' WHEN 238289 THEN 'Occupancy' WHEN 234050 THEN 'Occupancy' 
WHEN 217182 THEN 'Change of Use' WHEN 220990 THEN 'Change of Use' WHEN 235542 THEN 'Change of Use' WHEN 231642 THEN 'Change of Use'
ELSE 'Undefined' END AS Type_of_Permit,
sf_guard_user_profile.fullname as Applicant,
CASE sf_guard_user_profile.registeras WHEN 1 THEN 'Property Owner' WHEN 2 THEN 'Engineer' WHEN 3 THEN 'Architect' ELSE 'Undefined' END AS Applicant_type,
CASE mf_invoice.paid WHEN 2 THEN 'Paid' ELSE 'Not Paid' END AS Payment_Status,
CASE form_entry.form_id 
/* Construction Permit */
WHEN 200000 THEN CONCAT(ap_form_200000.element_5_1, ' ', ap_form_200000.element_5_2) WHEN 201251 THEN CONCAT(ap_form_201251.element_5_1, ' ', ap_form_201251.element_5_2) 
WHEN 235992 THEN CONCAT(ap_form_235992.element_5_1, ' ', ap_form_235992.element_5_2) WHEN 229692 THEN CONCAT(ap_form_229692.element_5_1, ' ', ap_form_229692.element_5_2) 
/* Refurb without and extension */
WHEN 213665 THEN CONCAT(ap_form_213665.element_5_1, ' ', ap_form_213665.element_5_2) WHEN 219480 THEN CONCAT(ap_form_219480.element_5_1, ' ', ap_form_219480.element_5_2) 
WHEN 214658 THEN CONCAT(ap_form_214658.element_5_1, ' ', ap_form_214658.element_5_2) WHEN 220023 THEN CONCAT(ap_form_220023.element_5_1, ' ', ap_form_220023.element_5_2) 

WHEN 236928 THEN CONCAT(ap_form_213665.element_5_1, ' ', ap_form_213665.element_5_2) WHEN 239228 THEN CONCAT(ap_form_219480.element_5_1, ' ', ap_form_219480.element_5_2) 
WHEN 232199 THEN CONCAT(ap_form_213665.element_5_1, ' ', ap_form_213665.element_5_2) WHEN 233480 THEN CONCAT(ap_form_219480.element_5_1, ' ', ap_form_219480.element_5_2) 
/*Demolition and Refurb with*/
WHEN 215327 THEN CONCAT(ap_form_215327.element_5_1, ' ', ap_form_215327.element_5_2) WHEN 220384 THEN CONCAT(ap_form_220384.element_5_1, ' ', ap_form_220384.element_5_2) 
WHEN 234966 THEN CONCAT(ap_form_234966.element_5_1, ' ', ap_form_234966.element_5_2) WHEN 234647 THEN CONCAT(ap_form_234647.element_5_1, ' ', ap_form_234647.element_5_2) 
WHEN 230688 THEN CONCAT(ap_form_230688.element_5_1, ' ', ap_form_230688.element_5_2) WHEN 232964 THEN CONCAT(ap_form_232964.element_5_1, ' ', ap_form_232964.element_5_2) 
WHEN 213916 THEN CONCAT(ap_form_213916.element_5_1, ' ', ap_form_213916.element_5_2) WHEN 219859 THEN CONCAT(ap_form_219859.element_5_1, ' ', ap_form_219859.element_5_2) 

/* Occupation  */
WHEN 234050 THEN CONCAT(ap_form_234050.element_92_1, ' ', ap_form_234050.element_92_2) WHEN 238289 THEN CONCAT(ap_form_238289.element_92_1, ' ', ap_form_238289.element_92_2)
/* Change of User */
WHEN 217182 THEN CONCAT(ap_form_217182.element_5_1, ' ', ap_form_217182.element_5_2) WHEN 220990 THEN CONCAT(ap_form_220990.element_5_1, ' ', ap_form_220990.element_5_2) 
WHEN 235542 THEN CONCAT(ap_form_235542.element_5_1, ' ', ap_form_235542.element_5_2) WHEN 231642 THEN CONCAT(ap_form_231642.element_5_1, ' ', ap_form_231642.element_5_2) 

ELSE 'Undefined' END AS Owner_name,

/* Fill in missing data, Project Name, Location */
/* Project Province */
CASE form_entry.form_id
/* Construction Form */
WHEN 200000 THEN  (SELECT `option` from ap_element_options where form_id=200000 and element_id=204)
WHEN 235992 THEN  (SELECT `option` from ap_element_options where form_id=235992 and element_id=204)
WHEN 229692 THEN  (SELECT `option` from ap_element_options where form_id=229692 and element_id=204)
WHEN 201251 THEN  (SELECT `option` from ap_element_options where form_id=201251 and element_id=204) 
/* Extension Permit - No field available for Province on Musanze,Rubavu, */
WHEN 213665 THEN  'Northern'
WHEN 219480 THEN  'Western'
WHEN 239228 THEN  'Southern'
WHEN 233480 THEN  'Western' 
/* Change of User */
WHEN 217182 THEN  'Northern'
WHEN 231642 THEN  'Western'
WHEN 235542 THEN  'Southern'
WHEN 220990 THEN  'Western'
/* Refurb With */
WHEN 234647 THEN  'Southern'
WHEN 213916 THEN  'Northern'
WHEN 219859 THEN  'Western'
WHEN 232964 THEN  'Western'
/* Refurb without */
WHEN 236928 THEN  'Southern'
WHEN 214658 THEN  'Northern'
WHEN 220023 THEN  'Western'
WHEN 232199 THEN  'Western'
/* Demolition */
WHEN 234966 THEN  'Southern'
WHEN 215327 THEN  'Northern'
WHEN 220384 THEN  'Western'
WHEN 230688 THEN  'Western'
 ELSE 'N/A' END AS Province,
ap_forms.form_name AS District,
/* Project Sector */
CASE form_entry.form_id
/* Construction Form */
WHEN 200000 THEN  (SELECT `option` from ap_element_options where form_id=200000 and element_id=82 and option_id = ap_form_200000.element_82)
WHEN 235992 THEN  (SELECT `option` from ap_element_options where form_id=235992 and element_id=82 and option_id = ap_form_235992.element_82)  
WHEN 229692 THEN  (SELECT `option` from ap_element_options where form_id=229692 and element_id=82 and option_id = ap_form_229692.element_82)  
WHEN 201251 THEN  (SELECT `option` from ap_element_options where form_id=201251 and element_id=82 and option_id = ap_form_201251.element_82)  
/* Extension */
WHEN 239228 THEN  (SELECT `option` from ap_element_options where form_id=239228 and element_id=82 and option_id = ap_form_239228.element_82)
WHEN 213665 THEN  (SELECT `option` from ap_element_options where form_id=213665 and element_id=82 and option_id = ap_form_213665.element_82)  
WHEN 219480 THEN  (SELECT `option` from ap_element_options where form_id=219480 and element_id=82 and option_id = ap_form_219480.element_82)  
WHEN 233480 THEN  (SELECT `option` from ap_element_options where form_id=233480 and element_id=82 and option_id = ap_form_233480.element_82)  
/* Change of User */
WHEN 235542 THEN  (SELECT `option` from ap_element_options where form_id=235542 and element_id=82 and option_id = ap_form_235542.element_82)
WHEN 217182 THEN  (SELECT `option` from ap_element_options where form_id=217182 and element_id=82 and option_id = ap_form_217182.element_82)  
WHEN 220990 THEN  (SELECT `option` from ap_element_options where form_id=220990 and element_id=82 and option_id = ap_form_220990.element_82)  
WHEN 231642 THEN  (SELECT `option` from ap_element_options where form_id=231642 and element_id=82 and option_id = ap_form_231642.element_82)  

/* Refurb With */
WHEN 234647 THEN  (SELECT `option` from ap_element_options where form_id=234647 and element_id=82 and option_id = ap_form_234647.element_82)
WHEN 213916 THEN  (SELECT `option` from ap_element_options where form_id=213916 and element_id=82 and option_id = ap_form_213916.element_82)  
WHEN 219859 THEN  (SELECT `option` from ap_element_options where form_id=219859 and element_id=82 and option_id = ap_form_219859.element_82)  
WHEN 232964 THEN  (SELECT `option` from ap_element_options where form_id=232964 and element_id=82 and option_id = ap_form_232964.element_82)  
/* Refurb Without */
WHEN 236928 THEN  (SELECT `option` from ap_element_options where form_id=236928 and element_id=82 and option_id = ap_form_236928.element_82)
WHEN 214658 THEN  (SELECT `option` from ap_element_options where form_id=214658 and element_id=82 and option_id = ap_form_214658.element_82)  
WHEN 220023 THEN  (SELECT `option` from ap_element_options where form_id=220023 and element_id=82 and option_id = ap_form_220023.element_82)  
WHEN 232199 THEN  (SELECT `option` from ap_element_options where form_id=232199 and element_id=82 and option_id = ap_form_232199.element_82)  
/* Demolition */
WHEN 234966 THEN  (SELECT `option` from ap_element_options where form_id=234966 and element_id=82 and option_id = ap_form_234966.element_82)
WHEN 215327 THEN  (SELECT `option` from ap_element_options where form_id=215327 and element_id=82 and option_id = ap_form_215327.element_82)  
WHEN 220384 THEN  (SELECT `option` from ap_element_options where form_id=220384 and element_id=82 and option_id = ap_form_220384.element_82)  
WHEN 230688 THEN  (SELECT `option` from ap_element_options where form_id=230688 and element_id=82 and option_id = ap_form_230688.element_82)  
ELSE 'N/A' END AS Sector,
/* Project Cell */
CASE form_entry.form_id
/* Construction Form */
WHEN 200000 THEN  (SELECT `option` from ap_element_options where form_id=200000 and element_id=187 and option_id = ap_form_200000.element_187) 
WHEN 235992 THEN  (SELECT `option` from ap_element_options where form_id=235992 and element_id=187 and option_id = ap_form_235992.element_187)
WHEN 229692 THEN   (SELECT `option` from ap_element_options where form_id=229692 and element_id=187 and option_id = ap_form_229692.element_187)
WHEN 201251 THEN  (SELECT `option` from ap_element_options where form_id=201251 and element_id=187 and option_id = ap_form_201251.element_187) 

/* Extension */
WHEN 239228 THEN  (SELECT `option` from ap_element_options where form_id=239228 and element_id=187 and option_id = ap_form_239228.element_187)
WHEN 213665 THEN  (SELECT `option` from ap_element_options where form_id=213665 and element_id=187 and option_id = ap_form_213665.element_187)  
WHEN 219480 THEN  (SELECT `option` from ap_element_options where form_id=219480 and element_id=187 and option_id = ap_form_219480.element_187)  
WHEN 233480 THEN  (SELECT `option` from ap_element_options where form_id=233480 and element_id=187 and option_id = ap_form_233480.element_187)  
/* Change of User */
WHEN 235542 THEN  (SELECT `option` from ap_element_options where form_id=235542 and element_id=187 and option_id = ap_form_235542.element_187)
WHEN 217182 THEN  (SELECT `option` from ap_element_options where form_id=217182 and element_id=187 and option_id = ap_form_217182.element_187)  
WHEN 220990 THEN  (SELECT `option` from ap_element_options where form_id=220990 and element_id=187 and option_id = ap_form_220990.element_187)  
WHEN 231642 THEN  (SELECT `option` from ap_element_options where form_id=231642 and element_id=187 and option_id = ap_form_231642.element_187)  

/* Refurb With */
WHEN 234647 THEN  (SELECT `option` from ap_element_options where form_id=234647 and element_id=187 and option_id = ap_form_234647.element_187)
WHEN 213916 THEN  (SELECT `option` from ap_element_options where form_id=213916 and element_id=187 and option_id = ap_form_213916.element_187)  
WHEN 219859 THEN  (SELECT `option` from ap_element_options where form_id=219859 and element_id=187 and option_id = ap_form_219859.element_187)  
WHEN 232964 THEN  (SELECT `option` from ap_element_options where form_id=232964 and element_id=187 and option_id = ap_form_232964.element_187)  
/* Refurb Without */
WHEN 236928 THEN  (SELECT `option` from ap_element_options where form_id=236928 and element_id=187 and option_id = ap_form_236928.element_187)
WHEN 214658 THEN  (SELECT `option` from ap_element_options where form_id=214658 and element_id=187 and option_id = ap_form_214658.element_187)  
WHEN 220023 THEN  (SELECT `option` from ap_element_options where form_id=220023 and element_id=187 and option_id = ap_form_220023.element_187)  
WHEN 232199 THEN  (SELECT `option` from ap_element_options where form_id=232199 and element_id=187 and option_id = ap_form_232199.element_187)  
/* Demolition */
WHEN 234966 THEN  (SELECT `option` from ap_element_options where form_id=234966 and element_id=187 and option_id = ap_form_234966.element_187)
WHEN 215327 THEN  (SELECT `option` from ap_element_options where form_id=215327 and element_id=187 and option_id = ap_form_215327.element_187)  
WHEN 220384 THEN  (SELECT `option` from ap_element_options where form_id=220384 and element_id=187 and option_id = ap_form_220384.element_187)  
WHEN 230688 THEN  (SELECT `option` from ap_element_options where form_id=230688 and element_id=187 and option_id = ap_form_230688.element_187)  

ELSE 'N/A' END AS Cell,
/* Project Village */
CASE form_entry.form_id
/* Construction Form */
WHEN 200000 THEN  (SELECT `option` from ap_element_options where form_id=200000 and element_id=188 and option_id = ap_form_200000.element_188) 
WHEN 235992 THEN  (SELECT `option` from ap_element_options where form_id=235992 and element_id=188 and option_id = ap_form_235992.element_188) 
WHEN 229692 THEN  (SELECT `option` from ap_element_options where form_id=229692 and element_id=188  and option_id = ap_form_229692.element_188)
WHEN 201251 THEN  (SELECT `option` from ap_element_options where form_id=201251 and element_id=188 and option_id = ap_form_201251.element_188)

/* Extension */
WHEN 239228 THEN  (SELECT `option` from ap_element_options where form_id=239228 and element_id=188 and option_id = ap_form_239228.element_188)
WHEN 213665 THEN  (SELECT `option` from ap_element_options where form_id=213665 and element_id=188 and option_id = ap_form_213665.element_188)  
WHEN 219480 THEN  (SELECT `option` from ap_element_options where form_id=219480 and element_id=188 and option_id = ap_form_219480.element_188)  
WHEN 233480 THEN  (SELECT `option` from ap_element_options where form_id=233480 and element_id=188 and option_id = ap_form_233480.element_188)  
/* Change of User */
WHEN 235542 THEN  (SELECT `option` from ap_element_options where form_id=235542 and element_id=188 and option_id = ap_form_235542.element_188)
WHEN 217182 THEN  (SELECT `option` from ap_element_options where form_id=217182 and element_id=188 and option_id = ap_form_217182.element_188)  
WHEN 220990 THEN  (SELECT `option` from ap_element_options where form_id=220990 and element_id=188 and option_id = ap_form_220990.element_188)  
WHEN 231642 THEN  (SELECT `option` from ap_element_options where form_id=231642 and element_id=188 and option_id = ap_form_231642.element_188)  

/* Refurb With */
WHEN 234647 THEN  (SELECT `option` from ap_element_options where form_id=234647 and element_id=188 and option_id = ap_form_234647.element_188)
WHEN 213916 THEN  (SELECT `option` from ap_element_options where form_id=213916 and element_id=188 and option_id = ap_form_213916.element_188)  
WHEN 219859 THEN  (SELECT `option` from ap_element_options where form_id=219859 and element_id=188 and option_id = ap_form_219859.element_188)  
WHEN 232964 THEN  (SELECT `option` from ap_element_options where form_id=232964 and element_id=188 and option_id = ap_form_232964.element_188)  
/* Refurb Without */
WHEN 236928 THEN  (SELECT `option` from ap_element_options where form_id=236928 and element_id=188 and option_id = ap_form_236928.element_188)
WHEN 214658 THEN  (SELECT `option` from ap_element_options where form_id=214658 and element_id=188 and option_id = ap_form_214658.element_188)  
WHEN 220023 THEN  (SELECT `option` from ap_element_options where form_id=220023 and element_id=188 and option_id = ap_form_220023.element_188)  
WHEN 232199 THEN  (SELECT `option` from ap_element_options where form_id=232199 and element_id=188 and option_id = ap_form_232199.element_188)  
/* Demolition */
WHEN 234966 THEN  (SELECT `option` from ap_element_options where form_id=234966 and element_id=188 and option_id = ap_form_234966.element_188)
WHEN 215327 THEN  (SELECT `option` from ap_element_options where form_id=215327 and element_id=188 and option_id = ap_form_215327.element_188)  
WHEN 220384 THEN  (SELECT `option` from ap_element_options where form_id=220384 and element_id=188 and option_id = ap_form_220384.element_188)  
WHEN 230688 THEN  (SELECT `option` from ap_element_options where form_id=230688 and element_id=188 and option_id = ap_form_230688.element_188)  
ELSE 'N/A' END AS Village,
sub_menus.title as Stage,
saved_permit.date_of_issue AS Permit_Issue_Date,
saved_permit.permit_id AS Permit_Number,
saved_permit.created_by AS Permit_Issued_By

from form_entry left join mf_invoice on form_entry.id = mf_invoice.app_id
left join sf_guard_user_profile on form_entry.user_id=sf_guard_user_profile.user_id
left join ap_form_200000 on ap_form_200000.id = form_entry.entry_id and form_id = 200000
left join ap_form_201251 on ap_form_201251.id = form_entry.entry_id and form_id = 201251
left join ap_form_219480 on ap_form_219480.id = form_entry.entry_id and form_id = 219480
left join ap_form_213665 on ap_form_213665.id = form_entry.entry_id and form_id = 213665
left join ap_form_215327 on ap_form_215327.id = form_entry.entry_id and form_id = 215327
left join ap_form_220384 on ap_form_220384.id = form_entry.entry_id and form_id = 220384
left join ap_form_214658 on ap_form_214658.id = form_entry.entry_id and form_id = 214658
left join ap_form_220023 on ap_form_220023.id = form_entry.entry_id and form_id = 220023
left join ap_form_213916 on ap_form_213916.id = form_entry.entry_id and form_id = 213916
left join ap_form_219859 on ap_form_219859.id = form_entry.entry_id and form_id = 219859
left join ap_form_202225 on ap_form_202225.id = form_entry.entry_id and form_id = 202225
left join ap_form_202752 on ap_form_202752.id = form_entry.entry_id and form_id = 202752
left join ap_form_217182 on ap_form_217182.id = form_entry.entry_id and form_id = 217182
left join ap_form_220990 on ap_form_220990.id = form_entry.entry_id and form_id = 220990
/* huye */
left join ap_form_235992 on ap_form_235992.id = form_entry.entry_id and form_id = 235992
left join ap_form_235542 on ap_form_235542.id = form_entry.entry_id and form_id = 235542
left join ap_form_239228 on ap_form_239228.id = form_entry.entry_id and form_id = 239228
left join ap_form_234647 on ap_form_234647.id = form_entry.entry_id and form_id = 234647
left join ap_form_236928 on ap_form_236928.id = form_entry.entry_id and form_id = 236928
left join ap_form_238289 on ap_form_238289.id = form_entry.entry_id and form_id = 238289
left join ap_form_234966 on ap_form_234966.id = form_entry.entry_id and form_id = 234966
/* rusizi */
left join ap_form_229692 on ap_form_229692.id = form_entry.entry_id and form_id = 229692
left join ap_form_231642 on ap_form_231642.id = form_entry.entry_id and form_id = 231642
left join ap_form_233480 on ap_form_233480.id = form_entry.entry_id and form_id = 233480
left join ap_form_232964 on ap_form_232964.id = form_entry.entry_id and form_id = 232964
left join ap_form_232199 on ap_form_232199.id = form_entry.entry_id and form_id = 232199
left join ap_form_234050 on ap_form_234050.id = form_entry.entry_id and form_id = 234050
left join ap_form_230688 on ap_form_230688.id = form_entry.entry_id and form_id = 230688
/* end */

left join sub_menus on sub_menus.id=form_entry.approved
left join ap_forms on ap_forms.form_id=form_entry.form_id
left join form_groups on form_groups.group_id=ap_forms.form_group
left join ap_form_elements on ap_forms.form_id = ap_form_elements.form_id
left join saved_permit on saved_permit.application_id = form_entry.id and saved_permit.permit_id <> ''
where form_entry.form_id in(200000,201251,213665,219480,215327,220384,214658,220023,213916,219859,202225,202752,217182,220990,235992,235542,239228,234647,
236928,238289,234966,229692,231642,233480,232964,232199,234050,230688) and parent_submission=0 and deleted_status = 0
group by form_entry.id ); 
" ;
//execute
$table_recreated = mysqli_query($connection, $statement1) or die("Database Error Creating Table");

}catch(Exception $ex){
      error_log("Crontab for datasource execution error ".$ex->getMessage());
}