<?php
require_once(dirname(__FILE__).'/../../../permitflow_src/config/ProjectConfiguration.class.php');
require_once(dirname(__FILE__).'/../../../permitflow_src/lib/vendor/symfony/lib/vendor/swiftmailer/swift_required.php');
$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', 'prod', false);
new sfDatabaseManager($configuration);

class PaymentStatusNotification {
    function PaymentStatusNotification($billRefNumber, $transactionId, $statusCode, $agencyCode, $statusMessage, $paymentTxNo, $paymentTxTimestamp, $paymentMethod, $paymentChannel)
    {
        $this->billRefNumber = $billRefNumber;
        $this->transactionId = $transactionId;
        $this->statusCode = $statusCode;
        $this->agencyCode = $agencyCode;
        $this->statusMessage = $statusMessage;
        $this->paymentTxNo = $paymentTxNo;
        $this->paymentTxTimestamp = $paymentTxTimestamp;
        $this->paymentMethod = $paymentMethod;
        $this->paymentChannel = $paymentChannel;
    }
}

class Acknowledgement {
    function Acknowledgement($acknowledgementCode, $acknowledgementDescription, $acknowledgementDateTime)
    {
        $this->acknowledgementCode = $acknowledgementCode;
        $this->acknowledgementDescription = $acknowledgementDescription;
        $this->acknowledgementDateTime = $acknowledgementDateTime;
    }
}

class BPMIS {

    function sendPaymentStatus($paymentStatusNotification) {
		error_log("Live Debug: Received from ROL Irembo, paymentStatusNotification ## ".print_R($paymentStatusNotification, true));
		$q = Doctrine_Query::create()
		   ->from("ApFormPayments a")
		   ->where("a.payment_id = ?", $paymentStatusNotification->billRefNumber);
		$transaction = $q->fetchOne();

		if($transaction){
			if($transaction->getStatus() == 2) {
				throw new SoapFault("ERR02", "Payment already complete for Bill Id: BillRefNumber ".$paymentStatusNotification->billRefNumber);
			}else{
				$acknowledgement = new Acknowledgement("REC", "Message Received", date(DATE_W3C));//Use DATE_W3C date format for ROL
				$invoice_manager = new InvoiceManager();
				$iremboGateway = new IremboGateway();
				$paymentsManager = new PaymentsManager();
				$request_details['billRefNumber'] = $paymentStatusNotification->billRefNumber;
				$request_details['transactionId'] = $paymentStatusNotification->transactionId;
				$request_details['statusCode'] = $paymentStatusNotification->statusCode;
				$request_details['agencyCode'] = $paymentStatusNotification->agencyCode;
				$request_details['statusMessage'] = $paymentStatusNotification->statusMessage;
				$request_details['paymentTxNo'] = $paymentStatusNotification->paymentTxNo;
				$request_details['paymentTxTimestamp'] = $paymentStatusNotification->paymentTxTimestamp;
				$request_details['paymentMethod'] = $paymentStatusNotification->paymentMethod;
				$request_details['paymentChannel'] = $paymentStatusNotification->paymentChannel;

				$result = $paymentsManager->process_validation($transaction->getInvoiceId(), $request_details);
				if($result){
					error_log("ACK sent back to ROL: ".print_R($acknowledgement, true));
					return $acknowledgement;
				}else{
					throw new SoapFault("ERR01", "Service Error");
				}
			}
		}else {
			throw new SoapFault("ERR02", "Invalid Bill Id: BillRefNumber ".$paymentStatusNotification->billRefNumber);
		}
    }

}

try {
	//when in non-wsdl mode the uri option must be specified
	$options=array('uri'=>$_SERVER['REQUEST_URI']);
	//create a new SOAP server
	error_log("Live environment");
	$server = new SoapServer('AGY.ROL.REQ.002.V1_PaymentStatusNotification_V1.wsdl',$options);
	//attach the API class to the SOAP Server
	$server->setClass('BPMIS');
	//start the SOAP requests handler
	$server->handle();
} catch (SoapFault $exc) {
    echo $exc->getTraceAsString();
}
?>
