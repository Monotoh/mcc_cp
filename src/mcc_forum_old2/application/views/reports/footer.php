  <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->

        <!-- Footer Start -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <?php echo date("Y"); ?> &copy;Designed by  <a href="#">Computer Busimess Solutions</a>
                    </div>
                    <div class="col-md-6">

                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->

        <!-- Right Sidebar -->
        <div class="right-bar">
            <div class="rightbar-title">
                <a href="javascript:void(0);" class="right-bar-toggle float-right">
                    <i class="dripicons-cross noti-icon"></i>
                </a>
                <h5 class="m-0 text-white">Settings</h5>
            </div>
            <div class="slimscroll-menu rightbar-content">
                <!-- User box -->
                <div class="user-box">
                    <div class="user-img">
                        <img src="assets/images/users/user-1.jpg" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
                        <a href="javascript:void(0);" class="user-edit"><i class="mdi mdi-pencil"></i></a>
                    </div>

                    <h5><a href="javascript: void(0);">Geneva Kennedy</a> </h5>
                    <p class="text-muted mb-0"><small>Admin Head</small></p>
                </div>

                <!-- Settings -->
                <hr class="mt-0" />
                <h5 class="pl-3">Basic Settings</h5>
                <hr class="mb-0" />

                <div class="p-3">
                    <div class="checkbox checkbox-primary mb-2">
                        <input id="Rcheckbox1" type="checkbox" checked>
                        <label for="Rcheckbox1">
                            Notifications
                        </label>
                    </div>
                    <div class="checkbox checkbox-primary mb-2">
                        <input id="Rcheckbox2" type="checkbox" checked>
                        <label for="Rcheckbox2">
                            API Access
                        </label>
                    </div>
                    <div class="checkbox checkbox-primary mb-2">
                        <input id="Rcheckbox3" type="checkbox">
                        <label for="Rcheckbox3">
                            Auto Updates
                        </label>
                    </div>
                    <div class="checkbox checkbox-primary mb-2">
                        <input id="Rcheckbox4" type="checkbox" checked>
                        <label for="Rcheckbox4">
                            Online Status
                        </label>
                    </div>
                    <div class="checkbox checkbox-primary mb-0">
                        <input id="Rcheckbox5" type="checkbox" checked>
                        <label for="Rcheckbox5">
                            Auto Payout
                        </label>
                    </div>
                </div>

                <!-- Timeline -->
                <hr class="mt-0" />
                <h5 class="pl-3 pr-3">Messages <span class="float-right badge badge-pill badge-danger">25</span></h5>
                <hr class="mb-0" />
                <div class="p-3">
                    <div class="inbox-widget">
                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="assets/images/users/user-2.jpg" class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author"><a href="javascript: void(0);" class="text-dark">Tomaslau</a></p>
                            <p class="inbox-item-text">I've finished it! See you so...</p>
                        </div>
                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="assets/images/users/user-3.jpg" class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author"><a href="javascript: void(0);" class="text-dark">Stillnotdavid</a></p>
                            <p class="inbox-item-text">This theme is awesome!</p>
                        </div>
                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="assets/images/users/user-4.jpg" class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author"><a href="javascript: void(0);" class="text-dark">Kurafire</a></p>
                            <p class="inbox-item-text">Nice to meet you</p>
                        </div>

                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="assets/images/users/user-5.jpg" class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author"><a href="javascript: void(0);" class="text-dark">Shahedk</a></p>
                            <p class="inbox-item-text">Hey! there I'm available...</p>
                        </div>
                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="assets/images/users/user-6.jpg" class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author"><a href="javascript: void(0);" class="text-dark">Adhamdannaway</a></p>
                            <p class="inbox-item-text">This theme is awesome!</p>
                        </div>
                    </div> <!-- end inbox-widget -->
                </div> <!-- end .p-3-->

            </div> <!-- end slimscroll-menu-->
        </div>
        <!-- /Right-bar -->

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- Vendor js -->
         <?php

           if(isset($graphis) and $graphis == "general"){
            if(isset($out_going) and isset($incoming)){

                $obj_incoming = json_encode($incoming);
                $bj_out_going = json_encode($out_going);
            }

           }








          ?>


         <!--<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>  -->
         <script src="<?php echo base_url("globals/") ?>assets/js/charts.js"></script>





          <script>
         <?php if(isset($graphis) and $graphis == "general"): ?>
            <?php if(isset($obj_incoming) and isset($bj_out_going)) :  ?>

            var ctx = document.getElementById('myChart1').getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ['Submissions','Resubmissions - Submissions', 'Corrections-Submisstions','Billing', 'Client Invoice Communication',  'Commenting-internal Review', 'Correction-internal','Resubmissions - Internal Review','Comments Review - Internnal Review','Rejections','Commenting - External Review','Comments Review - External Review','Corrections - External Review','Resubmissions - External Review'],
                    datasets: [{
                        label: 'In Coming Applications',
                        data: <?php echo $obj_incoming; ?>,
                        backgroundColor: [
                            'rgba(10, 207, 151, 1)',
                            'rgba(10, 207, 151, 1)',
                            'rgba(10, 207, 151, 1)',
                            'rgba(10, 207, 151, 1)',
                            'rgba(10, 207, 151, 1)',
                            'rgba(10, 207, 151, 1)',
                            'rgba(10, 207, 151, 1)',
                            'rgba(10, 207, 151, 1)',
                            'rgba(10, 207, 151, 1)',
                            'rgba(10, 207, 151, 1)',
                            'rgba(10, 207, 151, 1)',
                            'rgba(10, 207, 151, 1)',
                            'rgba(10, 207, 151, 1)',
                            'rgba(10, 207, 151, 1)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    },
                     {
                        label: 'Out Going Applications',
                        data: <?php echo $bj_out_going; ?>,
                        backgroundColor: [
                            'rgba(2, 192, 206, 1)',
                            'rgba(2, 192, 206, 1)',
                            'rgba(2, 192, 206, 1)',
                            'rgba(2, 192, 206, 1)',
                            'rgba(2, 192, 206, 1)',
                            'rgba(2, 192, 206, 1)',
                            'rgba(2, 192, 206, 1)',
                            'rgba(2, 192, 206, 1)',
                            'rgba(2, 192, 206, 1)',
                            'rgba(2, 192, 206, 1)',
                            'rgba(2, 192, 206, 1)',
                            'rgba(2, 192, 206, 1)',
                            'rgba(2, 192, 206, 1)',
                            'rgba(2, 192, 206, 1)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }


                    ]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });




            <?php endif; ?>
        <?php endif; ?>


        //start_task person graphis

        <?php if(isset($graphis) and $graphis == "task_person"): ?>

             <?php   $name           = json_encode($names);
                     $total1         = json_encode($total);
                    $complete_task1  = json_encode($complete_task);


             ?>


            var ctx = document.getElementById('myChart1').getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: <?php echo $name; ?>,
                    datasets: [{
                        label: '# of Votes',
                        data: <?php echo $total1; ?>,
                        backgroundColor: [
                            'rgba(10, 207, 151, 1)',
                            'rgba(10, 207, 151, 1)',
                            'rgba(10, 207, 151, 1)',
                            'rgba(10, 207, 151, 1)',
                            'rgba(10, 207, 151, 1)',
                            'rgba(10, 207, 151, 1)',
                            'rgba(10, 207, 151, 1)',
                            'rgba(10, 207, 151, 1)',
                            'rgba(10, 207, 151, 1)',
                            'rgba(10, 207, 151, 1)',
                            'rgba(10, 207, 151, 1)',
                            'rgba(10, 207, 151, 1)',
                            'rgba(10, 207, 151, 1)',
                            'rgba(10, 207, 151, 1)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    },
                     {
                        label: 'Out Going Applications',
                        data: <?php echo $complete_task1; ?>,
                        backgroundColor: [
                            'rgba(2, 192, 206, 1)',
                            'rgba(2, 192, 206, 1)',
                            'rgba(2, 192, 206, 1)',
                            'rgba(2, 192, 206, 1)',
                            'rgba(2, 192, 206, 1)',
                            'rgba(2, 192, 206, 1)',
                            'rgba(2, 192, 206, 1)',
                            'rgba(2, 192, 206, 1)',
                            'rgba(2, 192, 206, 1)',
                            'rgba(2, 192, 206, 1)',
                            'rgba(2, 192, 206, 1)',
                            'rgba(2, 192, 206, 1)',
                            'rgba(2, 192, 206, 1)',
                            'rgba(2, 192, 206, 1)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }


                    ]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });



          <?php endif; ?>
        //
        <?php if(isset($graphis) and $graphis == "monthly"): ?>
              // Bar chart
            new Chart(document.getElementById("bar-chart"), {
                type: 'bar',
                data: {
                  labels: ["Africa", "Asia", "Europe", "Latin America", "North America"],
                  datasets: [
                    {
                      label: "Population (millions)",
                      backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                      data: [2478,5267,734,784,433]
                    }
                  ]
                },
                options: {
                  legend: { display: false },
                  title: {
                    display: true,
                    text: 'Predicted world population (millions) in 2050'
                  }
                }
            });



         <?php endif; ?>

         <?php if(isset($graphis) and $graphis == "yearly"): ?>
                   <?php

                    if(isset($money)){

                    $json_money = json_encode($money);
                    }
                    ?>
              // Bar chart
            new Chart(document.getElementById("bar-chart"), {
                type: 'bar',
                data: {
                  labels: ["January", "February", "March", "April", "May","June","July","August","September","October","November","December"],
                  datasets: [
                    {
                      label: "Population (millions)",
                      backgroundColor: ["#3e95cd", "#3e95cd","#3e95cd","#3e95cd","#3e95cd","#3e95cd","#3e95cd","#3e95cd","#3e95cd","#3e95cd","#3e95cd","#3e95cd"],
                      data: <?php echo $json_money; ?>
                    }
                  ]
                },
                options: {
                  legend: { display: false },
                  title: {
                    display: true,
                    text: 'Amount of money collected for Annual '
                  }
                }
            });


         <?php endif; ?>

        //end graphis person
         <?php if(isset($graphis) and $graphis == "individuals"): ?>
                 <?php

                 //pending_task



                  ?>


        //end individual  person
                   var ctx = document.getElementById('pie').getContext('2d');

                 var myPieChart = new Chart(ctx, {
                    type: 'pie',
                    data:  data = {
                        datasets: [{
                            data: [ <?php echo $ind_total_task; ?> ,  <?php echo $ind_complete; ?> ,  <?php echo $ind_pendind; ?>],backgroundColor:[
                          "red",
						  "Yellow",
					      "Blue"

                                ]
                        }],
                          // These labels appear in the legend and in the tooltips when hovering different arcs
                        labels: [
                            'Assigned Tasks',
                            'Completed Tasks',
                            'Pending Tasks'
                        ]
                    },
                    options:{
			          	responsive: true
		              	}
                });

        //finacial reports

              <?php endif; ?>
        </script>



         <script>

                 var ctx = document.getElementById('mypie').getContext('2d');

                 var myPieChart = new Chart(ctx, {
                    type: 'pie',
                    data:  data = {
                        datasets: [{
                            data: [10, 20, 30],backgroundColor:[
                          "red",
						  "Yellow",
					      "Blue"

                        ]
                        }],
                          // These labels appear in the legend and in the tooltips when hovering different arcs
                        labels: [
                            'Red',
                            'Yellow',
                            'Blue'
                        ]
                    },
                    options:{
			          	responsive: true
		              	}
                });

         </script>




        <script>
       $('input[name="dates"]').daterangepicker();
        </script>













        <script src="<?php echo base_url("globals/") ?>assets/js/vendor.min.js"></script>
         <!-- Plugins js-->
          <script src="<?php echo base_url("globals/") ?>assets/js/pages/form-pickers.init.js"></script>


        <script src="<?php echo base_url("globals/") ?>assets/libs/jquery-knob/jquery.knob.min.js"></script>
        <script src="<?php echo base_url("globals/") ?>assets/libs/jquery-sparkline/jquery.sparkline.min.js"></script>

        <!-- Dashboar 1 init js-->
        <script src="<?php echo base_url("globals/") ?>assets/js/pages/dashboard-1.init.js"></script>

        <!-- App js-->
        <script src="<?php echo base_url("globals/") ?>assets/js/app.min.js"></script>

        <script>
             $("document").ready(function(){

               $("#parent").change(function(){
                   var parent = $(this).val();

                    if(parent == "annual"){
                          $("#first").attr("style","display:none;")
                          $("#monthly").attr("style","display:none;")
                          $("#month").attr("style","display:none;")

                          $("#annaul").removeAttr("style");
                          $("#year").removeAttr("style");


                    }else if(parent == "monthly"){

                          $("#first").attr("style","display:none;")
                          $("#annaul").attr("style","display:none;")
                          $("#year").attr("style","display:none;")

                          $("#monthly").removeAttr("style");
                          $("#month").removeAttr("style");

                    }else{
                          $("#monthly").attr("style","display:none;")
                          $("#annaul").attr("style","display:none;")

                          $("#year").attr("style","display:none;")
                          $("#month").attr("style","display:none;")

                          $("#first").removeAttr("style");


                    }

               });

             });

        </script>

       <script type="text/javascript">

                $("#month").shieldMonthYearPicker();


        </script>


    </body>

<!-- Mirrored from coderthemes.com/ubold/layouts/material-horizontal/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 18 Jun 2019 06:14:45 GMT -->
</html>