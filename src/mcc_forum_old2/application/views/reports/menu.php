        <div class="wrapper">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h5 class="page-title"> Reports Filters </h5>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                      <div class="col-xl-12">
                        <div id="accordion" class="mb-3">
                            <div class="card mb-1">
                                <div class="card-header" id="headingOne">
                                    <h5 class="m-0">
                                        <a class="text-dark collapsed" data-toggle="collapse" href="#collapseOne" aria-expanded="false">
                                            <i class="mdi mdi-help-circle mr-1 text-primary"></i>
                                            Application Processing General Perfomance 
                                        </a>
                                    </h5>
                                </div>

                                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion" style="">
                                    <div class="card-body">
                                         <div class="form-group mb-3">
                                                <!--<label for="example-date">Date</label>-->
                                                 <select class="form-control">
                                                     <option>Permit</option>
                                                     <option>Buiding Permit</option>
                                                     <option>Planninf and Construction Permit</option>
                                                 </select>
                                            </div>

                                            <div class="form-group mb-3">
                                             <!--   <label for="example-date">Date</label>   -->
                                                <input class="form-control" id="example-date" type="date" name="date">
                                            </div>



                                    </div>
                                </div>
                            </div>
                            <div class="card mb-1">
                                <div class="card-header" id="headingTwo">
                                    <h5 class="m-0">
                                        <a class="text-dark collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false">
                                            <i class="mdi mdi-help-circle mr-1 text-primary"></i>
                                            Why use Vakal text here?
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion" style="">
                                    <div class="card-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute,
                                        non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon
                                        tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil
                                        anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan
                                        excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt
                                        you probably haven't heard of them accusamus labore sustainable VHS.
                                    </div>
                                </div>
                            </div>
                            <div class="card mb-1">
                                <div class="card-header" id="headingThree">
                                    <h5 class="m-0">
                                        <a class="text-dark collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false">
                                            <i class="mdi mdi-help-circle mr-1 text-primary"></i>
                                            How many variations exist?
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion" style="">
                                    <div class="card-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute,
                                        non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon
                                        tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil
                                        anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan
                                        excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt
                                        you probably haven't heard of them accusamus labore sustainable VHS.
                                    </div>
                                </div>
                            </div>

                            <div class="card mb-1">
                                <div class="card-header" id="headingFour">
                                    <h5 class="m-0">
                                        <a class="text-dark" data-toggle="collapse" href="#collapseFour" aria-expanded="true">
                                            <i class="mdi mdi-help-circle mr-1 text-primary"></i>
                                            What is Vakal text here?
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseFour" class="collapse show" aria-labelledby="collapseFour" data-parent="#accordion" style="">
                                    <div class="card-body">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute,
                                        non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon
                                        tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil
                                        anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan
                                        excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt
                                        you probably haven't heard of them accusamus labore sustainable VHS.
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end #accordions-->
                    </div>





                <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->