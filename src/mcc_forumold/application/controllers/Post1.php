<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends CI_Controller {

      public function __construct(){
                 parent::__construct();
                 $this->load->model('generic_model','generic');
                 $this->load->model('egeneric_model','egeneric');
                 $this->load->library('session');
                 $this->load->library('manager');
                 $this->load->library('pagination');
                 $this->load->library('user_lib');
                 $this->load->model('register_model');
                 $this->load->library('user_lib');
                 $this->load->library('email');

        }







	public function index() {
     redirect(base_url("index.php/post/readpost/0"));

	}

    	public function readpost($start_from=0) {

               $data["post"] = $this->egeneric->read_more($start_from);
               $total        = $this->egeneric->count_topics();

               $data["total_records"]    =   $total;

                $config['base_url']       = base_url("index.php/post/readpost/");
                $config['total_rows']     =  $total;
                $config['per_page']       = 16;


                $config['first_url']       = 0;



                $config['full_tag_open']    = "<ul class='paginationforum'>";
                $config['full_tag_close']   ="</ul>";
                $config['num_tag_open']     = '<li class="hidden-xs">';
                $config['num_tag_close']    = '</li>';
                $config['cur_tag_open']     = "<li class='disabled'><li class='active'><a href='0'>";
                $config['cur_tag_close']    = "<span class='sr-only'></span></a></li>";
                $config['next_tag_open']    = "<li>";
                $config['next_tagl_close']  = "</li>";
                $config['prev_tag_open']    = "<li>";
                $config['prev_tagl_close']  = "</li>";
                $config['first_tag_open']   = "<li>";
                $config['first_tagl_close'] = "</li>";
                $config['last_tag_open']    = "<li>";
                $config['last_tagl_close']  = "</li>";


                $this->pagination->initialize($config);

                $data["links"]       =  $this->pagination->create_links();






             $this->load->view('index',$data);
	}

    public function topic(){
        $data["nothin"]   = "";
		$this->load->view('new_topic',$data);
	}


     public function submit_topic(){

                 $this->form_validation->set_rules('topic_title','topic title','required|trim');
                 $this->form_validation->set_rules('category','category','required|trim');
                 $this->form_validation->set_rules('name','name','required|trim');
                 $this->form_validation->set_rules('email','email','required|trim');
                 $this->form_validation->set_rules('desc','description','required|trim');
                 $this->form_validation->set_rules('target','target','required|trim');




                if ($this->form_validation->run() == FALSE){
                    $data["feedback"] =  "<div style='color:white;' class='alert alert-danger' >Please Insert All Required Fields</div>";

                }else{

                      $topic_title  =  $this->input->post("topic_title");
                      $category     =  $this->input->post("category");
                      $name         =  $this->input->post("name");
                      $email        =  $this->input->post("email");
                      $desc         =  $this->input->post("desc");
                      $target       =  $this->input->post("target");
                      $table        = "topics";
                      $data         = array(
                                            "topic_title"   => $topic_title,
                                            "category"      => $category,
                                             "name"         => $name,
                                             "email"        => $email,
                                             "description"  => $desc,
                                             "target"       => $target,
                                              "date"        => date("Y-m-d H:i:s")
                                         );


                     $this->egeneric->create($table,$data);
                     $this->send_notification($name,$email,$topic_title,$desc);


                      redirect(base_url("index.php/post/readpost/0"));

                }

	           	$this->load->view('new_topic',$data);



     }

       public function reply($token,$start_from=0){
        $data["nothin"]      = "";
        $table               = "topics";
        $array               = array("id" => $token);
        $data["comment_id"]  = $token;
        $return              =  $this->egeneric->read($table,$array);

        $time                = strtotime($return->date);

        $data["posted"]      =    $this->manager->humanTiming($time).' ago';

        $data["single_post"] =  $return;
        $data["comments"]    = $this->egeneric->read_comments($start_from,$token);
        $total_comments      = $this->egeneric->count_comments($token);
        $data["total"]       =   $total_comments ;

        $config['base_url']       = base_url("index.php//post/reply/".$token);
        $config['total_rows']     =   $total_comments;
        $config['per_page']       = 16;


                $config['first_url']       = 0;



                $config['full_tag_open']    = "<ul class='paginationforum'>";
                $config['full_tag_close']   ="</ul>";
                $config['num_tag_open']     = '<li class="hidden-xs">';
                $config['num_tag_close']    = '</li>';
                $config['cur_tag_open']     = "<li class='disabled'><li class='active'><a href='0'>";
                $config['cur_tag_close']    = "<span class='sr-only'></span></a></li>";
                $config['next_tag_open']    = "<li>";
                $config['next_tagl_close']  = "</li>";
                $config['prev_tag_open']    = "<li>";
                $config['prev_tagl_close']  = "</li>";
                $config['first_tag_open']   = "<li>";
                $config['first_tagl_close'] = "</li>";
                $config['last_tag_open']    = "<li>";
                $config['last_tagl_close']  = "</li>";


                $this->pagination->initialize($config);

                $data["links"]       =  $this->pagination->create_links();




        $this->load->view('reply',$data);
	}

    public  function submit_reply($token){

                $this->form_validation->set_rules('reply','reply','required|trim');

                if ($this->form_validation->run() == FALSE){
                    $data["feedback"] =  "<div style='color:white;' class='alert alert-danger' >Please Insert All Required Fields</div>";

                }else{

                    $datetime    = date("Y-m-d H:i:s") ;
                    $comment     =   $this->input->post("reply");
                    $table       =  " comments";
                     $insert     =   array(
                                          "ref_id"    => $token,
                                          "comments"  => $comment,
                                           "date"     => $datetime
                                            );
                    $this->egeneric->create($table,$insert );
                    redirect(base_url("index.php/post/reply/".$token));




                }




     }

      public function move_to(){
       $search_key = $this->input->post(search_key);
       if(isset($search_key)){
         $converted = $this->manager->convert($search_key);
         redirect(base_url("index.php/post/search/".$converted)."/0");

       }else{


       }


      }

     public function  search($token,$start_from=0){

                $converted  = $this->manager->revese_word($token);
                 $converted =  trim($converted);
                $feedback   = $this->egeneric->search($converted,$start_from);


                $data["post"] = $feedback;

                $total = count((array)$feedback);

                $config['base_url']       = base_url("index.php/post/search/".$token);
                $config['total_rows']     = $total;
                $config['per_page']       = 16;


                $config['first_url']       = 0;



                $config['full_tag_open']    = "<ul class='paginationforum'>";
                $config['full_tag_close']   ="</ul>";
                $config['num_tag_open']     = '<li class="hidden-xs">';
                $config['num_tag_close']    = '</li>';
                $config['cur_tag_open']     = "<li class='disabled'><li class='active'><a href='0'>";
                $config['cur_tag_close']    = "<span class='sr-only'></span></a></li>";
                $config['next_tag_open']    = "<li>";
                $config['next_tagl_close']  = "</li>";
                $config['prev_tag_open']    = "<li>";
                $config['prev_tagl_close']  = "</li>";
                $config['first_tag_open']   = "<li>";
                $config['first_tagl_close'] = "</li>";
                $config['last_tag_open']    = "<li>";
                $config['last_tagl_close']  = "</li>";


                $this->pagination->initialize($config);

                $data["links"]       =  $this->pagination->create_links();






              $this->load->view('index',$data);


    }





    public function mcc_account($start_from=0){

                if(isset($_SESSION["mccsupport"])){
                $data["post"] = $this->egeneric->read_more2($start_from);
                $config['base_url']       = base_url("index.php/post/mcc_account/");
                $config['total_rows']     = $this->egeneric->count_topics();;
                $config['per_page']       = 16;


                $config['first_url']       = 0;



                $config['full_tag_open']    = "<ul class='paginationforum'>";
                $config['full_tag_close']   ="</ul>";
                $config['num_tag_open']     = '<li class="hidden-xs">';
                $config['num_tag_close']    = '</li>';
                $config['cur_tag_open']     = "<li class='disabled'><li class='active'><a href='0'>";
                $config['cur_tag_close']    = "<span class='sr-only'></span></a></li>";
                $config['next_tag_open']    = "<li>";
                $config['next_tagl_close']  = "</li>";
                $config['prev_tag_open']    = "<li>";
                $config['prev_tagl_close']  = "</li>";
                $config['first_tag_open']   = "<li>";
                $config['first_tagl_close'] = "</li>";
                $config['last_tag_open']    = "<li>";
                $config['last_tagl_close']  = "</li>";


                $this->pagination->initialize($config);

                $data["links"]       =  $this->pagination->create_links();

               }


               $data["sd"] = "";

             $this->load->view('mcc',$data);





    }

   public function login(){


          if(($_POST["password"] == "mcc@@2020")   and  ($_POST["email"] == "msetsabi@mcc.org.ls")){

                $credials = array(
                       'mccsupport'  => "mcc@@2019"
                     );
                    $this->session->set_userdata($credials);

                    echo 1;

           }else{
              echo  "<div class='alert alert-danger'>
                       Wrong  Password or Email
                      </div> ";

          }


    }

    public function logout(){

     unset($_SESSION["mccsupport"]);
     redirect(base_url("index.php/post/mcc_account/0"));
    }


      public function delete($id){
                 if(isset($_SESSION["mccsupport"])){
                  $table  = "topics";
                 $keys   = array("id" =>$id);
                 $this->egeneric->delete($table,$keys);
                 $table  = " comments";
                 $keys   = array("ref_id" =>$id);
                 $this->egeneric->delete($table,$keys);
                 redirect(base_url("index.php/post/mcc_account/0"));
                   }else{

                    redirect(base_url("index.php/post/mcc_account/0"));
                   }

            }


    public function send_notification($name,$from_email,$subject,$message){

                      //get the form data

                       //configure email settings
                        $config['protocol'] = 'smtp';
                        $config['smtp_host'] = 'smtp.mcc.org.ls';
                        $config['smtp_port'] = '587';
                        $config['smtp_user'] = 'billing@mcc.org.ls'; // email id
                        $config['smtp_pass'] = 'maseruICUI4CU'; // email password
                        $config['mailtype'] = 'html';
                        $config['wordwrap'] = TRUE;
                        $config['charset'] = 'iso-8859-1';
                        $config['newline'] = "\r\n"; //use double quotes here
                        $this->email->initialize($config);


                        //set to_email id to which you want to receive mails
                        $to_email = 'billing@mcc.org.ls';

                        //send mail
                        $this->email->from($from_email, $name);
                        $this->email->to($to_email);
                        $this->email->subject($subject);
                        $this->email->message($message);
                        if ($this->email->send())
                        {
                            // mail sent
                        return 1;
                        }
                        else
                        {
                            //error
                        return 0;

                        }


                       }



}

