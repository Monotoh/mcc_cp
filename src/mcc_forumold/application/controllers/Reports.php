<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

      public function __construct(){
                 parent::__construct();
                 $this->load->model('generic_model','generic');
                 $this->load->model('egeneric_model','egeneric');
                 $this->load->library('session');
                 $this->load->library('manager');
                 $this->load->library('pagination');
                 $this->load->library('user_lib');
                 $this->load->model('register_model');
                 $this->load->library('user_lib');
                 $this->load->library('email');

        }




     public function index(){

       $data["graphis"] = "false";



       $this->load->view("reports/header",$data);
       $this->load->view("reports/nav");
       $this->load->view("reports/body");
       $this->load->view("reports/footer");
      }

      public function general($date1,$date2){

          $data["graphis"]          =  "general";

          $incoming                 = $this->egeneric->in_coming($date1,$date2);
          $outgoing                 = $this->egeneric->out_going($date1,$date2);
          $data["incoming"]         = $incoming;
          $data["out_going"]        = $outgoing;
          $stages                   = array('Submissions','Resubmissions - Submissions', 'Corrections-Submisstions','Billing', 'Client Invoice Communication',  'Commenting-internal Review', 'Correction-internal','Resubmissions - Internal Review','Comments Review - Internnal Review','Rejections','Commenting - External Review','Comments Review - External Review','Corrections - External Review','Resubmissions - External Review');
          $data["stages"]           = $stages;




       $this->load->view("reports/header",$data);
       $this->load->view("reports/nav");
       $this->load->view("reports/body");
       $this->load->view("reports/footer");


      }


     public function menu(){

   $this->load->view("reports/header");
   $this->load->view("reports/nav");
   $this->load->view("reports/menu");
   $this->load->view("reports/footer");
 }




    public function move_to(){

                 $report          = $this->input->post("report");
                 $date_filter     = $this->input->post("dates");
                if(isset($report) and $report== "general"){



                 if(!isset($date_filter) ){
                   redirect("reports/tasks/".$date1."/".$date2 );

                 }else{

                     $dates           =    explode("-",$date_filter);
                     $date1           =  $this->manager->dated($dates[0]);
                     $date2           =  $this->manager->dated($dates[1]);

                     $date1           = trim($date1);
                     $date2           = trim($date2);


                     redirect("reports/general/".$date1."/".$date2 );

                 }


         } else if(isset($report) and $report == "in_p"){

                     $dates           =    explode("-",$date_filter);
                     $date1           =  $this->manager->dated($dates[0]);
                     $date2           =  $this->manager->dated($dates[1]);

                     $date1           = trim($date1);
                     $date2           = trim($date2);



                      redirect("reports/tasks/".$date1."/".$date2 );

         } else{


           redirect("reports/");

       }




    }


   public function tasks($start_date=0,$end_date=0){

      $array_users     =  array(24,28,42,43,53,46,47,36,35,38,37,48);
      $array_names     = array();

      $total_tasks     = array();
      $array_complete  = array();

      $i               = 0;

      //get name task get total

      while(  $i  <  sizeof($array_users)){

           //get name
             $names = $this->egeneric->indv_name($array_users[$i]);

             array_push($array_names,$names);//push names

             $total_task =  $this->egeneric->total_assigned($start_date,$end_date,$array_users[$i]);   //total


             array_push($total_tasks ,$total_task);//push totals tasks

            $complete_task  = $this->egeneric->task_completed($start_date,$end_date,$array_users[$i]); //complte task

            array_push($array_complete,$complete_task );//push totals tasks


         $i++;
       }



         $data["graphis"]          =  "task_person";
         $data["names"]            =  $array_names;
         $data["total"]            =  $total_tasks;
         $data["complete_task"]    =  $array_complete;



       $this->load->view("reports/header",$data);
       $this->load->view("reports/nav");
       $this->load->view("reports/body");
       $this->load->view("reports/footer");




   }




}
