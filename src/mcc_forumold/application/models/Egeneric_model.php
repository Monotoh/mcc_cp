<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Egeneric_model extends CI_model{
    //crud creact read update delete

        function create($table,$userdata){
              $this->db->insert($table,$userdata);
         }

        function read($table,$array_data){
            $query = $this->db->get_where($table,$array_data);
            return $query->row();
        }

          function read_rows($table,$array_data){
            $query = $this->db->get_where($table,$array_data);
            return $query->result();
        }

           function get_info($table){

            $query = $this->db->get($table);
            return $query->result();
        }

        function update($table,$userdata){
             $this->db->where('refnum',$_SESSION['refnum']);
             $this->db->update($table,$userdata);

        }
         function update_double_key($table,$target_column,$keys){
                   $this->db->where($keys);
                   $this->db->update($table,$target_column);

        }


        function delete($table,$keys){
            $this->db->delete($table, $keys);
        }

      function  check_duplicate($table,$array_data){
         $query = $this->db->get_where($table,$array_data);
         return $query->num_rows();
        }

        function get_max_product_id($table,$table_id){
            $this->db->select_max($table_id);
            $query = $this->db->get($table);
            return $query->row();

        }

        function get_max_product_id_inside($table,$table_id,$key){
            $this->db->select_max($table_id);
             $this->db->where('category',$key);
            $query = $this->db->get($table);
            return $query->row();

        }


          public function read_more($start_from){
            $this->db->select('*');
                      $this->db->from('forum_topics');
                      $this->db->where("target","everyone");
                      $this->db->limit(16,$start_from);
                      $this->db->order_by('id', 'desc');
                      $query = $this->db->get();
                      return $query->result();

       }




           public function read_more1($start_from){
            $this->db->select('*');
                      $this->db->from('subscribers');
                      $this->db->limit(50,$start_from);
                      $query = $this->db->get();
                      return $query->result();

       }

             public function read_more2($start_from){
                       $this->db->select('*');
                       $this->db->from('forum_topics');
                      $this->db->limit(16,$start_from);
                      $query = $this->db->get();
                      return $query->result();

                 }


         public function read_news($start_from){
                      $this->db->select('*');
                      $this->db->from('news');
                      $this->db->where('steps',"3");
                      $this->db->limit(4,$start_from);
                      $query = $this->db->get();
                      return $query->result();

         }


              public function read_comments($start_from,$id){
                      $this->db->select('*');
                      $this->db->from('forum_comments');
                      $this->db->where('ref_id',$id);
                      $this->db->limit(16,$start_from);
                      $query = $this->db->get();
                      return $query->result();

         }







          public function records_count($table,$key){

                  $this->db->select('*');
                  $this->db->from($table);
                  $this->db->where('publication',$key);
                  $query = $this->db->get();
                  return $query->num_rows();

       }

         public function search($token,$start_from){

                  $this->db->select('*');
                  $this->db->from("forum_topics");
                  $this->db->where('target',"everyone");
                  $this->db->like('topic_title',$token ,'both');
                  $query = $this->db->get();
                  return $query->result();

       }

        function favoured_read_products($table,$start_from){
           $this->db->select('*');
                  $this->db->from($table);
                  $this->db->where('priority','high');
                  $this->db->limit(15,$start_from);
                  $this->db->order_by('id', 'asc');
                  $query = $this->db->get();
                  return $query->result();


        }

        function read_documents($start_from){
           $this->db->select('*');
                  $this->db->from("documents");
                  $this->db->limit(16,$start_from);
                  $this->db->order_by('id', 'desc');
                  $query = $this->db->get();
                  return $query->result();


        }

        function read_documents_outside($start_from){
           $this->db->select('*');
                  $this->db->from("documents");
                  $this->db->limit(16,$start_from);
                  $this->db->order_by('id', 'desc');
                  $query = $this->db->get();
                  return $query->result();


        }

          public function general_records_count($table){

                  $this->db->select('*');
                  $this->db->from($table);
                  $query = $this->db->get();
                  return $query->num_rows();

       }

       function general_read_products($table,$start_from){
           $this->db->select('*');
                  $this->db->from($table);
                  $this->db->where('publication !=',"archive");
                  $this->db->limit(15,$start_from);
                  $this->db->order_by('id', 'asc');
                  $query = $this->db->get();
                  return $query->result();


         }

        function category($token){
                  $sql = 'SELECT DISTINCT category from general_information where 	refnum ="'.$token.'"';
                  $query = $this->db->query($sql);
                 return $query->result();
       }




        function sub_categories($token){

              $query = $this->db->query('SELECT DISTINCT general_information.category
                                         from general_information join id_generator_information WHERE
                                          id_generator_information.parent_category = "'.$token.'"
                                          and
                                          id_generator_information.auto_id = general_information.auto_id
                                          and
                                          general_information.step = "3"
                                          and
                                          general_information.status = "yes"
                                          '

                                          );
             if($query->num_rows() > 0){
              return $query->result();
                  }
              else
                return 0;

            }

         function subscribe(){
                   $today = date("Y-m-d");
                   $sql = 'SELECT date_add("'.$today.'", INTERVAL 8760 hour) as date';
                   $query = $this->db->query($sql);
                   return $query->row();
        }

         public function fetch_notification($start_from){
                  $this->db->select('*');
                  $this->db->from('notification_information');
                  $this->db->where('ref_id',$_SESSION['ref_id']);
                  $this->db->limit(18,$start_from);
                  $this->db->order_by('id', 'desc');
                  $query = $this->db->get();
                  return $query->result();

           }


             function read_vacancy($start_from){
            $this->db->select('*');
                  $this->db->from('vacancy');
                  $this->db->limit(16,$start_from);
                  $this->db->order_by('date_posted','desc');
                  $query = $this->db->get();
                  return $query->result();


              }

             public function valid_members(){

                   $this->db->select('*');
                               $this->db->from('aal_users');
                               $this->db->join('valid_subscribers','aal_users.id = valid_subscribers.ref_id');
                               $this->db->where('valid_subscribers.status',"approved");
                               $query = $this->db->get();
                    return $query->num_rows();
           }







        public function profile(){

                   $this->db->select('*');
                               $this->db->from('aal_users');
                               $this->db->join('extra_personal_information','aal_users.id = extra_personal_information.ref_id');
                               $this->db->join('education_information','aal_users.id      = education_information.ref_id');
                               $this->db->join('working_exprience','aal_users.id          = working_exprience.ref_id');
                               $this->db->where('aal_users.id',$_SESSION["ref_id"]);
                               $query = $this->db->get();

                    return  $query->row();



        }

        public function  user_profile($user_id){

               $this->db->select('*');
                               $this->db->from('aal_users');
                               $this->db->join('extra_personal_information','aal_users.id = extra_personal_information.ref_id');
                               $this->db->join('education_information','aal_users.id      = education_information.ref_id');
                               $this->db->join('working_exprience','aal_users.id          = working_exprience.ref_id');
                               $this->db->where('aal_users.id',$user_id);
                               $query = $this->db->get();

                    return  $query->row();


        }









           public function registered_members($limit=0){

                   $this->db->select('*');
                               $this->db->from('aal_users');
                               $this->db->join('extra_personal_information','aal_users.id = extra_personal_information.ref_id');
                               $this->db->join('education_information','aal_users.id      = education_information.ref_id');
                               $this->db->join('working_exprience','aal_users.id          = working_exprience.ref_id');
                               $query = $this->db->get();

                     return $query->result();

           }

                  public function pending_members($limit=0){

                   $this->db->select('*');
                               $this->db->from('aal_users');
                               $this->db->join('extra_personal_information','aal_users.id = extra_personal_information.ref_id');
                               $this->db->join('education_information','aal_users.id      = education_information.ref_id');
                               $this->db->join('working_exprience','aal_users.id          = working_exprience.ref_id');
                               $this->db->join('valid_subscribers','aal_users.id          = valid_subscribers.ref_id');
                               $this->db->where('valid_subscribers.status',"pending");
                               $query = $this->db->get();

                     return $query->result();



                 }


                   public function pending_members_count($limit=0){

                   $this->db->select('*');
                               $this->db->from('aal_users');
                               $this->db->join('extra_personal_information','aal_users.id = extra_personal_information.ref_id');
                               $this->db->join('education_information','aal_users.id      = education_information.ref_id');
                               $this->db->join('working_exprience','aal_users.id          = working_exprience.ref_id');
                               $this->db->join('valid_subscribers','aal_users.id          = valid_subscribers.ref_id');
                               $this->db->where('valid_subscribers.status',"pending");
                               $query = $this->db->get();

                               return $query->num_rows();



                 }


                   public function active_members($limit=0){

                   $this->db->select('*');
                               $this->db->from('aal_users');
                               $this->db->join('extra_personal_information','aal_users.id = extra_personal_information.ref_id');
                               $this->db->join('education_information','aal_users.id      = education_information.ref_id');
                               $this->db->join('working_exprience','aal_users.id          = working_exprience.ref_id');
                               $this->db->join('valid_subscribers','aal_users.id          = valid_subscribers.ref_id');
                               $this->db->where('valid_subscribers.status',"active");
                               $query = $this->db->get();

                     return $query->result();



                 }





                  public function active_members1($limit=0){

                   $this->db->select('*');
                               $this->db->from('aal_users');
                               $this->db->join('extra_personal_information','aal_users.id = extra_personal_information.ref_id');
                               $this->db->join('education_information','aal_users.id      = education_information.ref_id');
                               $this->db->join('working_exprience','aal_users.id          = working_exprience.ref_id');
                               $this->db->join('valid_subscribers','aal_users.id          = valid_subscribers.ref_id');
                               $this->db->where('valid_subscribers.status',"active");
                               $this->db->where('valid_subscribers.status',"active");
                               $query = $this->db->get();

                     return $query->result();



                 }






                public function read_pages($start_from){
                $this->db->select('*');
                      $this->db->from('pages');
                      $this->db->limit(16,$start_from);
                      $query = $this->db->get();
                      return $query->result();

                 }
    //




           public function records($table){

                  $this->db->select('*');
                  $this->db->from($table);
                  $query = $this->db->get();
                  return $query->num_rows();

        }



          public function records_count_tenders($table,$key){

                  $this->db->select('*');
                  $this->db->from($table);
                  $this->db->where('category',$key);
                  $this->db->where('status',"on");
                  $query = $this->db->get();
                  return $query->num_rows();

       }


        public function notification_num(){

                  $this->db->select('*');
                  $this->db->from("notification_information");
                  $this->db->where("ref_id",$_SESSION["ref_id"]);
                  $query = $this->db->get();
                  return $query->num_rows();

        }




        public function records_num($table,$target_column,$key){

                  $this->db->select('*');
                  $this->db->from($table);
                  $this->db->where($target_column,3);
                  $query = $this->db->get();
                  return $query->num_rows();

       }



                    public function get_approvals($limit=0,$token){

                   $this->db->select('*');
                               $this->db->from('aal_users');
                               $this->db->join('extra_personal_information','aal_users.id = extra_personal_information.ref_id');
                               $this->db->join('education_information','aal_users.id      = education_information.ref_id');
                               $this->db->join('working_exprience','aal_users.id          = working_exprience.ref_id');
                               $this->db->join('approval_steps_tracker','aal_users.id     = approval_steps_tracker.ref_id');
                               $this->db->where('approval_steps_tracker.approval',$token);
                               $query = $this->db->get();

                     return $query->result();
                }

                      public function get_decline($limit=0,$token){

                   $this->db->select('*');
                               $this->db->from('aal_users');
                               $this->db->join('extra_personal_information','aal_users.id = extra_personal_information.ref_id');
                               $this->db->join('education_information','aal_users.id      = education_information.ref_id');
                               $this->db->join('working_exprience','aal_users.id          = working_exprience.ref_id');
                               $this->db->join('approval_steps_tracker','aal_users.id     = approval_steps_tracker.ref_id');
                               $this->db->where('approval_steps_tracker.decline',$token);
                               $query = $this->db->get();

                     return $query->result();
                }


    public function today(){
    return date("Y-m-d");
    }

    public function now(){
    return date("H:i:s");
   }



     public function last_updated(){
                  $this->db->select('*');
                  $this->db->from('publisher');
                  $this->db->join('weather_id_auto_generate','publisher.forecast_id = weather_id_auto_generate.	forecast_id');
                  $this->db->order_by('weather_id_auto_generate.date','desc');
                  $query = $this->db->get();
                  return $query->row();

           }

       public function count_comments($ref_id){
                   $this->db->select("*");
                   $this->db->from("forum_comments");
                   $this->db->where("ref_id",$ref_id);
                   $query = $this->db->get();
            return $result = $query->num_rows();
      }

        public function count_topics(){
                   $this->db->select("*");
                   $this->db->from("forum_topics");
                   $this->db->where("target","everyone");
                   $query = $this->db->get();
            return $result = $query->num_rows();
      }

      //mcc report section
    public function in_coming($date1,$date2){

     $i = 0;
     $stages   = array(9,10,52,11,50,13,15,53,14,16,18,19,20,54);
     $results  = array();

             while($i < sizeof($stages)){

                  $sql= "SELECT count(*) count FROM application_reference WHERE stage_id='".$stages[$i]."' AND DATE(start_date) >= '".$date1."' AND DATE(start_date) <= '".$date2."' ";
                  $query = $this->db->query($sql);
                  $rs = $query->row();

                  if(!empty($rs->count)){

                     $value = ((int)$rs->count);
                     array_push($results,$value );

                  }else{
                     array_push($results,0);
                  }



                  $i++;


              }

     return $results;



    }


    public function out_going($date1,$date2){

     $i = 0;
     $stages   = array(9,10,52,11,50,13,15,53,14,16,18,19,20,54);
     $results  = array();

     while($i < sizeof($stages)){

      $sql= "SELECT count(*) total FROM application_reference WHERE stage_id='".$stages[$i]."' AND DATE(end_date) >= '".$date1."' AND DATE(end_date)<='".$date2."'";
      $query = $this->db->query($sql);
      $rs = $query->row();

      echo $stages[$i]." ".$rs->total." ";

      if(!empty($rs->total)){

         $value = ((int)$rs->total);
         array_push($results,$value);

      }else{
         array_push($results,0);
      }


      $i++;


      }
     return $results;
    }



        public function indv_name($user_id){

              $sql= "SELECT strlastname,strfirstname from cf_user  where nid = '".$user_id."' ";
              $query = $this->db->query($sql);
              $rs = $query->row();

              if(isset($rs->strlastname) and isset($rs->strfirstname)){

               return  $rs->strlastname." ".$rs->strfirstname;

              }else{

              return 0;
              }


        }

        public function total_assigned($start_date,$end_date,$user_id){

             $sql= "SELECT count(*) count FROM task WHERE  DATE(date_created) >= '".$start_date."' AND  DATE(date_created)<='".$end_date."' AND owner_user_id = '".$user_id."' ";
              $query = $this->db->query($sql);
              $rs = $query->row();

              if(!empty($rs->count)){

                $value = ((int)$rs->count);

                 return $value;

              }else{
                  return 0;
              }

        }



        public function task_completed($start_date,$end_date,$user_id){

             $sql= "SELECT count(*) count FROM task WHERE  DATE(end_date) >= '".$start_date."' AND  DATE(end_date)<='".$end_date."' AND owner_user_id = '".$user_id."' and status =25 ";
              $query = $this->db->query($sql);
              $rs = $query->row();

              if(!empty($rs->count)){

                $value = ((int)$rs->count);

                 return $value;

              }else{
                  return 0;
              }

        }


    }


