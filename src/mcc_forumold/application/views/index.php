<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Mcc Forum :: home</title>

        <!-- Bootstrap -->
        <link href="<?php echo base_url("assets/"); ?>css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom -->
        <link href="<?php echo base_url("assets/"); ?>css/custom.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
          <![endif]-->

        <!-- fonts -->
          <link rel="stylesheet" href="<?php echo base_url("assets/"); ?>font-awesome-4.0.3/css/font-awesome.min.css">

        <!-- CSS STYLE-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/"); ?>css/style.css" media="screen" />

        <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/"); ?>rs-plugin/css/settings.css" media="screen" />

    </head>
    <body>

        <div class="container-fluid">

            <!-- Slider -->
            <div class="tp-banner-container" style="background-image: url(<?php echo base_url("assets/"); ?>images/bg2.png); background-size : cover;">
                 <div class="tp-banner"   style="background-image: url(<?php echo base_url("assets/"); ?>images/bg2.png); background-size : cover;">

                    <ul>
                        <!-- SLIDE  -->
                        <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                            <!-- MAIN IMAGE -->
                            <!-- LAYERS -->
                        </li>
                    </ul>
                </div>
            </div>
            <!-- //Slider -->

            <div class="headernav">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-1 col-xs-3 col-sm-2 col-md-2 logo "><a href="index.html"><img src="images/logo.jpg" alt=""  /></a></div>
                        <div class="col-lg-3 col-xs-9 col-sm-5 col-md-3 selecttopic">
                           <!-- <div class="dropdown">
                                <a data-toggle="dropdown" href="index.html#" >Borderlands 2</a> <b class="caret"></b>
                                <ul class="dropdown-menu" role="menu">
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="index.html#">Borderlands 1</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-2" href="index.html#">Borderlands 2</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-3" href="index.html#">Borderlands 3</a></li>

                                </ul>
                            </div>-->
                        </div>
                        <div class="col-lg-4 search hidden-xs hidden-sm col-md-3">
                            <div class="wrap">
                                    <?php echo form_open("post/move_to") ?>
                                    <div class="pull-left txt"><input type="text" name="search_key" class="form-control" placeholder="Search Topics" minlength="10"  maxlength="50" required="required"></div>
                                    <div class="pull-right"><button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button></div>
                                    <div class="clearfix"></div>
                                    <?php echo form_close(); ?>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xs-12 col-sm-5 col-md-4 avt">
                            <div class="stnt pull-left">
                                    <a  href="<?php echo base_url("index.php/post/topic"); ?>" class="btn btn-primary">Start New Topic</a>

                            </div>


                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>


            <section class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-2 col-md-2"></div>
                        <div class="col-lg-8 col-xs-12 col-md-8">
                            <?php if(isset($total_records) and $total_records > 16):  ?>
                            <div class="pull-left"><a href="index.html#" class="prevnext"><i class="fa fa-angle-left"></i></a></div>
                            <div class="pull-left">
                              <?php echo $links; ?>
                            </div>
                            <div class="pull-left"><a href="index.html#" class="prevnext last"><i class="fa fa-angle-right"></i></a></div>
                             <?php endif; ?>

                         <div class="clearfix"></div>
                        </div>
                    </div>
                </div>




                <div class="container">
                    <div class="row">
                        <div class="col-lg-2 col-md-2"></div>
                        <div class="col-lg-8 col-md-8">
                            <!-- POST -->
                             <?php  if(isset($post) and !empty($post)): ?>
                                    <?php $this->load->model('egeneric_model','egeneric');  ?>
                                    <?php foreach($post as $vpost): ?>
                                        <div class="post">
                                            <div class="wrap-ut pull-left">
                                                <div class="userinfo pull-left">
                                                    <div class="avatar">
                                                          <img src="<?php echo base_url("assets/"); ?>images/avatar.png" alt="" />
                                                    </div>

                                                    <div class="icons">

                                                    </div>
                                                </div>
                                                <div class="posttext pull-left">
                                                    <h2><a href="<?php echo base_url("index.php/post/reply/".$vpost->id."/0"); ?>"><?php echo $vpost->topic_title; ?></a></h2>
                                                    <p><?php echo $vpost->description; ?></p>
                                                    <p><i style="color : #000000 !important;"><?=$vpost->name; ?></i></p>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="postinfo pull-left">
                                                <div class="comments">
                                                    <div class="commentbg">
                                                        <?php  echo  $total_comments      = $this->egeneric->count_comments($vpost->id);  ?>
                                                        <div class="mark"></div>
                                                    </div>

                                                </div>

                                            </div>
                                            <div class="clearfix"></div>

                                                   <div class="postinfobot">

                                    <div class="likeblock pull-left">
                                        <a href="#" class="up"><i class="fa fa-star"></i></a>
                                        <a href="#" class="up"><i class="fa fa-star"></i></a>
                                        <a href="#" class="up"><i class="fa fa-star"></i></a>
                                    </div>



                                    <div class="posted pull-left"><i class="fa fa-clock-o"></i> Posted  :  <?php
                                                                                                                     $time =  strtotime($vpost->date);
                                                                                                               echo $this->manager->humanTiming($time).' ago';  ?>

                                                                                           </div>
                                   <div class="clearfix"></div>
                                </div>
                                        </div><!-- POST -->
                                   <?php endforeach; ?>
                               <?php endif ?>









                        </div>
                        <div class="col-lg-2 col-md-2"></div>
                    </div>
                </div>



                <div class="container">
                    <div class="row">
                         <div class="col-lg-2 col-md-2"></div>
                        <div class="col-lg-8 col-xs-12">
                            <?php if(isset($total_records) and $total_records > 16):  ?>
                            <div class="pull-left"><a href="#" class="prevnext"><i class="fa fa-angle-left"></i></a></div>
                            <div class="pull-left">
                                  <?php echo $links; ?>
                            </div>
                            <div class="pull-left"><a href="#" class="prevnext last"><i class="fa fa-angle-right"></i></a></div>
                             <?php endif; ?>
                         <div class="clearfix"></div>
                        </div>
                    </div>
                </div>


            </section>

            <footer>
                <div class="container">
                     <div class="row">
                         <div class="col-lg-8 col-xs-9 col-sm-5 ">Designed by Computer Business Solutions</div>

                    </div>
                </div>
            </footer>
        </div>

        <!-- get jQuery from the google apis -->
        <script type="text/javascript" src="<?php echo base_url("assets/"); ?>js/jquery.js"></script>


        <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
        <script type="text/javascript" src="<?php echo base_url("assets/"); ?>rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/"); ?>rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

        <script src="<?php echo base_url("assets/"); ?>js/bootstrap.min.js"></script>


        <!-- LOOK THE DOCUMENTATION FOR MORE INFORMATIONS -->
        <script type="text/javascript">

            var revapi;

            jQuery(document).ready(function() {
                "use strict";
                revapi = jQuery('.tp-banner').revolution(
                        {
                            delay: 15000,
                            startwidth: 1200,
                            startheight: 278,
                            hideThumbs: 10,
                            fullWidth: "on"
                        });

            });	//ready

        </script>

        <!-- END REVOLUTION SLIDER -->
    </body>
</html>