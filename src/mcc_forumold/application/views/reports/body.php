        <div class="wrapper">
            <div class="containr-fluid">

                <!-- start page title -->
                <div class="row">
                   <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <div class="page-title-box">
                            <div class="page-title-right">

                            </div>

                            <h4 class="page-title">Dashboard</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                    <?php echo form_open("reports/move_to"); ?>
                      <div class="row">
                      <div class="col-md-1"></div>
                           <div class="col-md-5">
                                  <select class="form-control"  name="report"  style="border-radius: 0px;" required="required">
                                   <option value="">Types Of Reports</option>
                                   <option value="general">General Performance In all Stages </option>
                                   <option value="in_p"> Individual Perfomance </option>

                                 </select>
                           </div>


                             <div class="col-md-4">
                                   <input type="text" name="dates" placeholder="Pe" value="2019-05-07/2019-06-14"  class="form-control" />
                              </div>

                             <div class="col-md-1">
                              <button class="btn btn-primary" type="submit">Search</button>
                             </div>

                       </div>
                    <?php echo form_close(); ?>


                <!-- end row-->



                <div class="row" style=" margin-top: 10px;">
                     <br>
                     <br>
                     <!-- <div style="width :400px;height: 400px; ">
                          <canvas id="myChart"></canvas>
                      </div>-->


                    <div class="col-md-1"></div>

                    <?php if(isset($graphis) and $graphis == "false"): ?>

                                <div class="col-md-10" style=" margin-top: 100px;">

                                  <h1 style="text-align: center;color : rgba(181, 181, 181, 1)">Please filter Reports for Results</h1>

                               </div>


                     <?php endif; ?>


                    <?php  if(isset($graphis) and $graphis == "general"): ?>

                              <div class="col-md-5">
                                    <div class="card-box">
                                    <h4 class="header-title mb-3">Bar Chart Summary</h4>
                                    <canvas id="myChart1"  height="400"></canvas>
                                   </div>
                               </div>


                             <div class="col-lg-5">
                                <div class="card-box">
                                    <h4 class="header-title mb-3">Application Movement Summary</h4>
                                     <table class="table">
                                         <tr>
                                              <th style="text-align: left;">Stage</th>
                                              <th>Incoming </th>
                                              <th>Out going  </th>

                                         </tr>

                                          <?php $i = 0; ?>
                                          <?php if(isset($stages)): ?>
                                               <?php while($i < sizeof($stages)): ?>
                                                 <tr>
                                                      <td style="text-align: left;"><?php echo $stages[$i]; ?> </td>
                                                      <td style="text-align: center;"><?php if(isset($incoming))  echo $incoming[$i]; else  echo 0; ?> </td>
                                                      <td style="text-align: center;"><?php if(isset($out_going)) echo $out_going[$i]; else  echo 0; ?> </td>

                                                </tr>
                                                   <?php $i++; ?>
                                               <?php endwhile; ?>
                                          <?php endif;  ?>
                                     </table>

                                </div> <!-- end card-->
                            </div>


                     <?php  endif; ?>


                     <?php  if(isset($graphis) and $graphis == "task_person"): ?>

                              <div class="col-md-5">
                                    <div class="card-box">
                                    <h4 class="header-title mb-3">Bar Chart</h4>
                                    <canvas id="myChart1"  height="350"></canvas>
                                   </div>
                               </div>


                             <div class="col-lg-5">
                                <div class="card-box">
                                    <h4 class="header-title mb-3">Task Summary</h4>
                                        <table class="table">
                                         <tr>
                                              <th>Name</th>
                                              <th style="text-align: center;">Assigned</th>
                                              <th style="text-align: center;">Completed</th>
                                              <th style="text-align: center;">Pending  </th>
                                         </tr>
                                          <?php $i = 0; ?>

                                                <?php if(isset($names)):   ?>
                                                     <?php while($i <  sizeof($names)):  ?>
                                                                <tr>
                                                                     <td ><?=$names[$i]; ?></td>
                                                                     <td style="text-align: center;"><?php if(isset($total[$i]))  echo $total[$i]; else 0;   ?></td>
                                                                     <td style="text-align: center;"><?php if(isset($complete_task[$i]))  echo $complete_task[$i]; else 0;   ?></td>
                                                                     <td style="text-align: center;"><?php if(isset($complete_task[$i]) and isset($total[$i]))  echo  ($total[$i] - $complete_task[$i]); else 0;   ?></td>

                                                                </tr>
                                                                <?php $i++; ?>
                                                        <?php endwhile; ?>
                                                <?php endif; ?>
                                        </table>


                                </div> <!-- end card-->
                            </div>


                        <?php  endif; ?>
                </div>

                <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->