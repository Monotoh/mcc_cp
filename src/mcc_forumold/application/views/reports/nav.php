 <!-- Navigation Bar-->
        <header id="topnav">

            <!-- Topbar Start -->
            <div class="navbar-custom">
                <div class="container-fluid">
                    <ul class="list-unstyled topnav-menu float-right mb-0">

                        <li class="dropdown notification-list">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle nav-link">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </li>

                     

                        <li class="dropdown notification-list">
                            <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <img src="assets/images/users/user-1.jpg" alt="user-image" class="rounded-circle">
                                <span class="pro-user-name ml-1">
                                    Geneva <i class="mdi mdi-chevron-down"></i>
                                </span>
                            </a>

                        </li>



                    </ul>

                    <!-- LOGO -->
                    <div class="logo-box">
                        <a href="index.html" class="logo text-center">
                            <span class="logo-lg">
                                <img src="<?php echo base_url("assets/");  ?>images/mcc_logo.png" alt="" style="height : 60px;  " >
                                <span class="logo-lg-text-light" style="color: #7F7F7F">&nbsp;MASERU CITY COUCIL REPORT TOOL</span>
                            </span>
                            <span class="logo-sm">
                                <!-- <span class="logo-sm-text-dark">U</span> -->
                                <img src="<?php echo base_url("assets/");  ?>images/mcc_logo.png" alt="" >
                            </span>
                        </a>
                    </div>

                </div> <!-- end container-fluid-->
            </div>
            <!-- end Topbar -->

            <div class="topbar-menu">
                <div class="container-fluid">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu pull-right" style="width : 300px;">

                            <li class="has-submenu pull-right">
                                <a href="#">
                                    <i class="fe-airplay"></i> Reports For  </a>
                                <ul class="submenu">
                                    <li>
                                        <a href="index.html">Mcc</a>
                                    </li>
                                    <li>
                                        <a href="dashboard-2.html">Wasco </a>
                                    </li>

                                    <li>
                                        <a href="dashboard-2.html">LEC </a>
                                    </li>

                                </ul>
                            </li>
                   </ul>
                        <!-- End navigation menu -->

                        <div class="clearfix"></div>
                    </div>
                    <!-- end #navigation -->
                </div>
                <!-- end container -->
            </div>
            <!-- end navbar-custom -->

        </header>
        <!-- End Navigation Bar-->