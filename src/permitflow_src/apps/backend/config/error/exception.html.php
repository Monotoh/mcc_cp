<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="/assets_unified/images/favicon.png" type="image/png">

  <title>500 Internal Server Error</title>

  <link href="/assets_unified/css/style.default.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="/assets_unified/js/html5shiv.js"></script>
  <script src="/assets_unified/js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="notfound">

<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>
  
  <div class="notfoundpanel">
    <h1>500!</h1>
    <h3>An error occured while processing your request. Please try again.</h3>
    <h4>Please contact your system administrator if you're experiencing problems.</h4>
  </div><!-- notfoundpanel -->
  
</section>


<script src="/assets_unified/js/jquery-1.10.2.min.js"></script>
<script src="/assets_unified/js/jquery-migrate-1.2.1.min.js"></script>
<script src="/assets_unified/js/bootstrap.min.js"></script>
<script src="/assets_unified/js/modernizr.min.js"></script>
<script src="/assets_unified/js/retina.min.js"></script>

<script src="/assets_unified/js/custom.js"></script>

</body>
</html>
