<?php

/**
 * notificationsetup actions.
 *
 * @package    permit
 * @subpackage notificationsetup
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class announcementsActions extends sfActions
{
  public function executeBatch(sfWebRequest $request)
  {
		if($request->getPostParameter('delete'))
		{
			$item = Doctrine_Core::getTable('Announcement')->find(array($request->getPostParameter('delete')));
			if($item)
			{
				$item->delete();
			}
		}
    }
  public function executeIndex(sfWebRequest $request)
  {
    $this->announcements = Doctrine_Core::getTable('announcement')
      ->createQuery('a')
	  ->orderBy('a.id DESC')
      ->execute();
	$this->setLayout("layout-settings");
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new AnnouncementForm();
	$this->setLayout("layout-settings");
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new announcementForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($announcement = Doctrine_Core::getTable('announcement')->find(array($request->getParameter('id'))), sprintf('Object announcement does not exist (%s).', $request->getParameter('id')));
    $this->form = new announcementForm($announcement);
	  $this->setLayout("layout-settings");
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($announcement = Doctrine_Core::getTable('announcement')->find(array($request->getParameter('id'))), sprintf('Object announcement does not exist (%s).', $request->getParameter('id')));
    $this->form = new announcementForm($announcement);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {

    $this->forward404Unless($announcement = Doctrine_Core::getTable('announcement')->find(array($request->getParameter('id'))), sprintf('Object announcement does not exist (%s).', $request->getParameter('id')));
    $announcement->delete();

    $this->redirect('/backend.php/announcements/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
        $announcement = $form->save();

        $this->redirect('/backend.php/announcements/index');
    }
  }
}
