<?php
use_helper("I18N");
?>

<div class="panel panel-dark">
<div class="panel-heading">
  <h3 class="panel-title"><?php echo ($form->getObject()->isNew()?__('New Announcement'):__('Edit Announcement')); ?></div>
  <form id="announcementform" name="announcementform" class="form-bordered form-horizontal" action="<?php echo url_for('/backend.php/announcements/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>

<div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
  <strong><?php echo __('Well done'); ?>!</strong> <?php echo __('You successfully updated this record'); ?></a>.
</div>


<div class="panel-body panel-body-nopadding">

  <?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <?php echo $form->renderGlobalErrors() ?>
	  <?php if(isset($form['_csrf_token'])): ?>
        <?php echo $form['_csrf_token']->render(); ?>
      <?php endif; ?>
            
      <div class="form-group">
        <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Content'); ?></i></label>
         <div class="col-sm-8">
          <?php echo $form['content']->renderError() ?>
          <textarea name="announcements[content]" id="announcements_content" class="form-control"><?php echo $form->getObject()->isNew()?" ":$form->getObject()->getContent(); ?></textarea>
        </div>
      </div>
            
      <div class="form-group">
        <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Start date'); ?></i></label>
         <div class="col-sm-8">
          <?php echo $form['start_date']->renderError() ?>
          <input type="text" name="announcements[start_date]" id="announcements_start_date"  class="form-control"  placeholder="yyyy-mm-dd" value="<?php echo $form->getObject()->isNew()?" ":$form->getObject()->getStartDate(); ?>">
        </div>
      </div>
            
      <div class="form-group">
        <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('End date'); ?></i></label>
         <div class="col-sm-8">
          <?php echo $form['end_date']->renderError() ?>
          <input type="text" name="announcements[end_date]" id="announcements_end_date" class="form-control"  placeholder="yyyy-mm-dd" value="<?php echo $form->getObject()->isNew()?" ":$form->getObject()->getEndDate(); ?>">
        </div>
      </div>
      
      <div class="form-group">
        <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Visibility'); ?></i></label>
         <div class="col-sm-8">
          <?php echo $form['frontend']->renderError() ?>
          <select name="announcements[frontend]" id="announcements_frontend" class="form-control chosen-select">
          	<option value="1" <?php if(!$form->getObject()->isNew()){ if($form->getObject()->getFrontend() == 1){ echo "selected"; } } ?>><?php echo __('Users'); ?></option>
            <option value="2" <?php if(!$form->getObject()->isNew()){ if($form->getObject()->getFrontend() == 2){ echo "selected"; } } ?>><?php echo __('Reviewers'); ?></option>
          </select>
        </div>
      </div>
      
	   </div><!--panel-body-->
        
        <div class="panel-footer">
		<button class="btn btn-danger mr10"><?php echo __('Reset'); ?></button><button id="submitbuttonname" type="submit" class="btn btn-primary" name="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button>
		</div>
</form>
</div><!--panel-body-->
</div>





<script>
jQuery(document).ready(function(){
  
	jQuery( "#submitform" ).click(function() {
		jQuery("#announcementform").submit();
	});
	
	  jQuery('#announcements_start_date').datepicker();
	  jQuery('#announcements_end_date').datepicker();
    
});
</script>
