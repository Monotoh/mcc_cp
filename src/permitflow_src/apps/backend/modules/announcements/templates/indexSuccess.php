<?php
use_helper("I18N");

$audit = new Audit();
$audit->saveAudit("", "Accessed announcement settings");

if($sf_user->mfHasCredential("manageannouncements"))
{
  $_SESSION['current_module'] = "announcements";
  $_SESSION['current_action'] = "index";
  $_SESSION['current_id'] = "";
?>
<div class="contentpanel">
<div class="panel panel-dark">
<div class="panel-heading">
			<h3 class="panel-title"><?php echo __('Announcements'); ?></h3>
				<div class="pull-right">
            <a class="btn btn-primary-alt settings-margin42" id="newannouncement" href="<?php echo public_path(); ?>backend.php/announcements/new"><?php echo __('New Announcement'); ?></a>
</div>
</div>


<div class="panel panel-body panel-body-nopadding ">


<table class="table dt-on-steroids mb0" id="table3">
    <thead>
   <tr>
      <th class="no-sort" style="width: 10px;"><input type='checkbox' name='batchall' onclick="boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.name == 'batch') { if(this.checked == true){ box.checked = true; }else{ box.checked = false; } } } "></th>
      <th width="60">#</th>
 	    <th><?php echo __('Content'); ?></th>
      <th><?php echo __('Start date'); ?></th>
      <th><?php echo __('End date'); ?></th>
      <th class="no-sort"><?php echo __('Visibility'); ?></th>
      <th class="no-sort" width="7%"><?php echo __('Actions'); ?></th>
    </tr>
  </thead>
  <tbody id="tblBdy">
    <?php foreach ($announcements as $announcement): ?>
    <tr id="row_<?php echo $announcement->getId() ?>">
	  <td><input type='checkbox' name='batch' id='batch_<?php echo $announcement->getId() ?>' value='<?php echo $announcement->getId() ?>'></td>
      <td><?php echo $announcement->getId() ?></td>
      <td><?php
		echo $announcement->getContent();
	  ?></td>
      <td><?php echo $announcement->getStartDate() ?></td>
      <td><?php echo $announcement->getEndDate() ?></td>
      <td><?php
	  if($announcement->getFrontend() == 1)
	  {
		  echo __("Users");
	  }
	  else
	  {
		  echo __("Reviewers");
	  }
	  ?></td>
      <td>
		<a id="editannouncement<?php echo $announcement->getId(); ?>" href="<?php echo public_path(); ?>backend.php/announcements/edit/id/<?php echo $announcement->getId(); ?>" title="<?php echo __('Edit'); ?>"><span class="badge badge-primary"><i class="fa fa-pencil"></i></span></a>
		<a id="deleteannouncement<?php echo $announcement->getId(); ?>" href="<?php echo public_path(); ?>backend.php/announcements/delete/id/<?php echo $announcement->getId(); ?>" onClick="if(confirm('Are you sure you want to delete this item?')){ return true; }else{ return false; }" title="<?php echo __('Delete'); ?>"><span class="badge badge-primary"><i class="fa fa-trash-o"></i></span></a>
  </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
	<tfoot>
   <tr><td colspan='7' style='text-align: left;'>
   <select id='batch_action' name='batch_action' onChange="if(this.value != ''){if(confirm('Are you sure?')){ batch('announcements', this.options[this.selectedIndex].text, this.value); document.getElementById('default').selected='selected'; }}">
   <option id='default' value=''><?php echo __('Choose an action'); ?>..</option>
   <option value='delete'><?php echo __('Set As Deleted'); ?></option>
   </select>
   </td></tr>
   </tfoot>
</table>

</div><!--panel-body-->
</div><!--panel-dark-->
</div>
<script>
jQuery(document).ready(function(){
  jQuery('#table3').dataTable({
      "sPaginationType": "full_numbers",

      // Using aoColumnDefs
      "aoColumnDefs": [
      	{ "bSortable": false, "aTargets": [ 'no-sort' ] }
    	]
    });
});
</script>
<?php
}
else
{
  include_partial("settings/accessdenied");
}
?>
