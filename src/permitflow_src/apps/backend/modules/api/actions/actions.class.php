<?php

/**
 * api actions.
 *
 * @package    permitflow
 * @subpackage api
 * @author     Your name here
 * @version    SVN: $Id$
 */
class apiActions extends sfActions
{
    
    /**
     * businessReport
     */
    public function executeBusinessReport(sfWebRequest $request)
    {
    }
    
    /**
     * Fire revenue in sbp
     */
    public function executeFireRevenueInSBP(sfWebRequest $request)
    {
    }

    /**
     * Outdoor revenue in sbp
     */
    public function executeOutdoorRevenueInSBP(sfWebRequest $request)
    {
    }
    
     /**
   * OTB patch - BI reports
   */
    public function executeEmbed(sfWebRequest $request)
    {
        $this->setLayout('layout');
    }
    
    /**
     * Single Business Permits Report
     */
    public function executeSBP(sfWebRequest $request)
    {
    }
    
    /**
     * Dashboard summary
     */
    public function executeSummary(sfWebRequest $request)
    {
    }
    
    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {
        error_log("Was Executed.....") ;
        exit;
    }
    //Metabase redirect
    public function executeReportslink(sfWebRequest $request)
    {
         $this->redirect("http://" . sfConfig::get('app_report_url') . "");
    
         return sfView::NONE;
    }
    //OTB login method
    public function executeLogin(sfWebRequest $request)
    {
        $key = $request->getParameter('key');
        $timestamp = $request->getParameter('timestamp');
        //validate
        //concatinate
        $key_ = sfConfig::get('app_api_key') . $timestamp;
        $hash_key = hash('sha256', $key_);
        //error_log('----Hashed key-----' . $hash_key . '----Key----' . $key . '-----Key combination---' . $key_ . '------Api key---' . sfConfig::get('app_api_key') . '----timestamp-----' . $timestamp);
        if ($hash_key == $key) {
            //login user
            //redirect
            //get report user
            $q = Doctrine_Query::create()
                    ->from('CfUser r')
                    ->where('r.struserid = ?', 'admin');
            $report_user = $q->fetchOne();
            $login_manager = new LoginManager();
            $login_manager->create_session($report_user->getStruserid(), sfConfig::get('app_report_pwd'));
            //send session id
            $session_id = session_id();
            //redirect
            $timestamp_current = time();
            $key_client = sfConfig::get('app_api_key') . $timestamp_current;
            error_log("session id ".$session_id);
            $hash_key_client = hash('sha256', $key_client);
            $this->redirect("http://" . sfConfig::get('app_report_url') . "/auth/login?session_id={$session_id}&key={$hash_key_client}&timestamp={$timestamp_current}");
        } else {
            //handle
            error_log('Error, hash didn\'t match') ;
            echo 'Error, hash didn\'t match';
        }
        return sfView::NONE;
    }

    public function executeUser(sfWebRequest $request)
    {
        //validate and send user data
        $key = $request->getParameter('key');
        $timestamp = $request->getParameter('timestamp');
        $session_id = $request->getParameter('session_id');
        //validate
        //concatinate
        $key_ = sfConfig::get('app_api_key') . $timestamp;
        $hash_key = hash('sha256', $key_);
        if ($hash_key == $key) {
            //valid
            //$reviewer = Functions::current_user();
            //
            $q = Doctrine_Query::create()
                    ->from('CfUser r')
                    ->where('r.struserid = ?', 'admin');
            $reviewer = $q->fetchOne();
            //get user dashboard
            $user_arr = array('username' => $reviewer->getStruserid(), 'first_name' => $reviewer->getStrfirstname(), 'dash_id' => 1, 'email' => $reviewer->getStremail(), 'last_name' => $reviewer->getStrlastname());
            //$this->getResponse()->setContent(json_encode($user_arr));
            //error_log('Respond with the user from session') ;
             
            echo json_encode($user_arr) ;
        } else {
            //handle error
            echo 'User Access Error' ;
        }
        return sfView::NONE;
    }

    public function executeGroup(sfWebRequest $request)
    {
        //validate and send group data
        $key = $request->getParameter('key');
        $timestamp = $request->getParameter('timestamp');
        
        //validate
        //concatinate
        $key_ = sfConfig::get('app_api_key') . $timestamp;
        $hash_key = hash('sha256', $key_);
        if ($hash_key == $key) {
            //valid
            //$reviewer = Functions::current_user();
            
            $q = Doctrine_Query::create()
                    ->from('CfUser r')
                    ->where('r.struserid = ?', 'admin');
            $reviewer = $q->fetchOne();
            
            $group_arr = array();
            foreach ($reviewer->getMfGuardUserGroup() as $group) {
                //get user dashboard
                $group_arr[] = $group->getGroupId();
            }
            //$this->getResponse()->setContent(json_encode($group_arr));
            echo json_encode($group_arr) ;
        } else {
            //handle error
            error_log("Group access error");
        }
        return sfView::NONE;
    }

    /**
     * Verify if an SBP Number Exists
     *
     * @param sfRequest $request A request object
     * form 64301, 3124
     */
    public function executeVerifysbpnumber(sfWebRequest $request)
    {
        $sbp_number = trim($request->getParameter('sbp'));
        $q = Doctrine_Query::create()
            ->select('f.form_id')
            ->from('FormEntry f')
            ->where('f.application_id like ?', $sbp_number)
            ->andWhere('f.form_id = 64301 OR f.form_id = 3124');
        $result = $q->fetchOne();
        if ($q->count() === 1) {
            echo true;
            exit;
        } else {
            echo false;
            exit;
        }
        return sfView::NONE;
    }
}
