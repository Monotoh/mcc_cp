<?php
//ini_set('display_errors', '0');     # don't show any errors...
//error_reporting(E_ALL | E_STRICT);  # ...but do log them
/**
 * Applications actions.
 *
 * Displays submitted applications and all of their review history
 *
 * @package    backend
 * @subpackage applications
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

function GetDaysDiff($startdate, $enddate)
{
	$date1 = new DateTime($startdate);
	$date2 = new DateTime($enddate);

	$diff = $date2->diff($date1)->format("%a");
	return $diff;
}


class applicationsActions extends sfActions
{
    
     //OTB patch - Functions for membership verification
    // to be able to submit new form
    public function executeNew(sfWebRequest $request) {
        $this->form_id = $request->getParameter('form_id');
        $this->setLayout("layout-settings");
    }

    public function executeShowmemberships(sfWebRequest $request) {

        $q = Doctrine_Query::create()
                ->from("ApForms a")
                ->where('a.form_type = ?', 3) // membership form type
                ->andWhere('a.form_active = ? ', 1) //Display only active forms
                ->orderBy("a.form_name ASC");


        $this->apforms = $q->execute();
        $this->setLayout("layout-settings");
    }

    public function executeShowentries(sfWebRequest $request) {

        $form_id = $request->getParameter('form_id');

        //Form name

        $q = Doctrine_Query::create()
                ->select('a.form_id,a.form_name')
                ->from("ApForms a")
                ->where('a.form_id = ?', $form_id)
                ->limit(1);

        $this->apform = $q->fetchArray();

        //Elements

        $q = Doctrine_Query::create()
                ->select('a.element_title,a.element_id')
                ->from("ApFormElements a")
                ->where('a.form_id = ? AND a.element_status = ?', array($form_id, 1));

        $this->form_elements = $q->fetchArray();

        //Entries

        $this->entries = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc("SELECT * FROM ap_form_" . $form_id . " WHERE status = 1");
        //extra paramenter for table name
        $this->from_table = "ap_form_" . $form_id;
        $this->set_form_id = $form_id;
        //
        $this->setLayout("layout-settings");
    }

    //End OTB 

    /**
     * OTB patch 
     *  Function used to delete membership list entries
     */
    public function executeDeletemember(sfWebRequest $request) {
        $id = $request->getParameter('id');
        $form_id = $request->getParameter('form_id');
        $prefix = "ap_form_" . $form_id;
        //
        $q = "DELETE FROM $prefix WHERE id = $id";
        //
        try {
            $conn = Doctrine_Manager::getInstance()->getCurrentConnection()->getDbh();
            $statement = $conn->prepare($q);
            //
            $statement->execute();
            $this->getUser()->setFlash('deleted', 'Record deleted successfuly');
        } catch (Execption $ex) {
            $this->getUser()->setFlash('delete_fail', 'Ooops! Cannot delete record at this time. try again later: ' . $ex->getMessage());
        }
        $this->redirect("/backend.php/applications/showentries/form_id/" . $form_id);
    }

    /**
     * Function to allow a user clear a members database table
     */
    public function executeClearmembers(sfWebRequest $request) {
        $form_id = $request->getParameter('form_id');
        $prefix = "ap_form_" . $form_id;
        $q = "TRUNCATE $prefix";
        try {
            $conn = Doctrine_Manager::getInstance()->getCurrentConnection()->getDbh();
            $statement = $conn->prepare($q);
            //
            $statement->execute();
            $this->getUser()->setFlash('member_deleted', 'Membership database deleted successfuly');
        } catch (Execption $ex) {
            $this->getUser()->setFlash('member_fail', 'Ooops! Cannot delete membership database at this time. try again later: ' . $ex->getMessage());
        }
        $this->redirect("/backend.php/applications/showentries/form_id/" . $form_id);
    }

    
    /**
     * OTB patch - Allow users to download all files as zip
     */
    public function executeDownloadZip(sfWebRequest $request){
        
         $prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";
         require_once($prefix_folder.'config.php');
          require_once($prefix_folder.'includes/db-core.php');
          require_once($prefix_folder.'includes/helper-functions.php');
         require_once($prefix_folder.'includes/entry-functions.php');
         
         $dbh = mf_connect_db();
         $mf_settings = mf_get_settings($dbh);
        
        //$dbh = $request->getParameter('dbh') ;
        $form_id = $request->getParameter('form_id') ;
        $entry_id = $request->getParameter('entry_id') ;
        //get the application number
        $otbhelper = new OTBHelper();
        $app_details = $otbhelper->getAppNumber($form_id, $entry_id);
        $app_number = $app_details->getApplicationId();
        
        
        $param = $request->getParameter('param') ;
         error_log("before get attachment details >>> ".$app_number);
        //
        $attachments_details = mf_get_attachments_files($dbh,$form_id,$entry_id,$param); 
        $field_name = null ;
        $file_hash = null ;
        $files = array();
        ////////////////////
        $tmp_folder = $mf_settings['upload_dir']."/tmp/";
        $zipname = $app_number.".zip";
        $zip = new ZipArchive;
        $zip->open($tmp_folder."/".$zipname, ZipArchive::CREATE);
        //$prefix_html_folder = dirname(__FILE__)."../../../../../../../html/";
        
        foreach($attachments_details as $attach){
            if(!empty($attach['element']))
                {
                //  error_log("Field Name set is ".$attach['element']);
                  $field_name = $attach['element'] ;
                  $file_hash = $attach['hash'] ;
                  //get filename
              $query = "select {$field_name} from `".MF_TABLE_PREFIX."form_{$form_id}` where id=?";
              //OTB patch
             
              $params = array($entry_id);

              $sth = mf_do_query($query,$params,$dbh);
              $row = mf_do_fetch_result($sth);

              $filename_array  = array();
              $filename_array  = explode('|',$row[$field_name]);

              $filename_md5_array = array();
              foreach ($filename_array as $value) {
                      $filename_md5_array[] = md5($value);
              }

              $file_key = array_keys($filename_md5_array,$file_hash);
              if(empty($file_key)){
                      die("Error. File Not Found!");
              }else{
                      $file_key = $file_key[0];
              }

              $complete_filename = $filename_array[$file_key];

              //remove the element_x-xx- suffix we added to all uploaded files
              $file_1 	   	= substr($complete_filename,strpos($complete_filename,'-')+1);
              $filename_only 	= substr($file_1,strpos($file_1,'-')+1);

              $target_file = $mf_settings['upload_dir']."/form_{$form_id}/files/{$complete_filename}";
               error_log("Complete File Name >>>> ".$filename_only);
               
              // $zip->addFile($target_file, basename($target_file));
                $zip->addFile($target_file, $filename_only);
             
             /* if(file_exists($target_file)){
                  //error_log("The file is of Type ".$type);
                    // Send file for download
                     // $zip->addFile("/var/www/rwandacp_test_bed_2/html/".$target_file);
                       // $tmpname = tempnam(sys_get_temp_dir(), 'test');
                       // file_put_contents($tmpname, file_get_contents($target_file));
                        $zip->addFile($target_file, basename($target_file));
                        //$zip->addFile($target_file, $filename_only);
                       /// unlink($tmpname);   
               //   $files[] = $target_file ;
                      // Send file for download
        
         
              }*/
            }
            
        }
        //add the files
//		foreach($files as $file) {
//			$zip->addFile($file,$file);
//		}
       // error_log("Files ".$zip->numFiles.">>> Status ".$zip->status."");
       $ret = $zip->close();
      // error_log("Closed with: " . ($ret ? "true" : "false"));
        //stream the file
        header('Content-Type: application/zip');
        header('Content-disposition: attachment; filename='.$zipname);
        header('Content-Length: ' . filesize($tmp_folder."/".$zipname));
        readfile($tmp_folder."/".$zipname);
        unlink($tmp_folder."/".$zipname) ;
        exit;
    }
    /**
     * Executes 'reversearchive' action
     *
     * reversearchive form
     *
     * @param sfRequest $request A request object
     */
    public function executeReversearchive(sfWebRequest $request)
    {

        $q = Doctrine_Query::create()
		   ->from("FormEntryArchive a")
		   ->where("a.id = ?", $request->getParameter("id"));

		$archived_application = $q->fetchOne();

        //Move Application
		$entry = new FormEntry();
		$entry->setId($archived_application->getId());
		$entry->setFormId($archived_application->getFormId());
		$entry->setEntryId($archived_application->getEntryId());
		$entry->setApproved($archived_application->getApproved());
		$entry->setApplicationId($archived_application->getApplicationId());
		$entry->setUserId($archived_application->getUserId());
		$entry->setParentSubmission($archived_application->getParentSubmission());
		$entry->setDeclined($archived_application->getDeclined());
		$entry->setDateOfSubmission($archived_application->getDateOfSubmission());
		$entry->setDateOfResponse($archived_application->getDateOfResponse());
		$entry->setDateOfIssue($archived_application->getDateOfIssue());
		$entry->setObservation($archived_application->getObservation());
		$entry->save();

		//Move Invoice
		$archive_invoices = $archived_application->getMfInvoiceArchive();
		foreach($archive_invoices as $archive_invoice)
		{
                        $q = Doctrine_Query::create()
                        ->from("MfInvoice a")
                        ->where("a.id = ?", $archive_invoice->getId());
                        if($q->count())
                        {
                                echo "-- skipping reverse archiving invoice for ".$archived_application->getApplicationId()."\n";
                                continue;
                        }
                        else
                        {
                                echo "-- reverse archiving invoice for ".$archived_application->getApplicationId()."\n";
                        }

			$invoice = new MfInvoice();
			$invoice->setId($archive_invoice->getId());
			$invoice->setAppId($archive_invoice->getAppId());
			$invoice->setInvoiceNumber($archive_invoice->getInvoiceNumber());
			$invoice->setTemplateId($archive_invoice->getTemplateId());
			$invoice->setPaid($archive_invoice->getPaid());
			$invoice->setCreatedAt($archive_invoice->getCreatedAt());
			$invoice->setUpdatedAt($archive_invoice->getUpdatedAt());
			$invoice->setDueDate($archive_invoice->getDueDate());
			$invoice->setExpiresAt($archive_invoice->getExpiresAt());
			$invoice->setPayerId($archive_invoice->getPayerId());
			$invoice->setPayerName($archive_invoice->getPayerName());
			$invoice->setDocRefNumber($archive_invoice->getDocRefNumber());
			$invoice->setCurrency($archive_invoice->getCurrency());
			$invoice->setServiceCode($archive_invoice->getServiceCode());
			$invoice->setTotalAmount($archive_invoice->getTotalAmount());
			$invoice->save();

			$archive_invoice_details = $archive_invoice->getMfInvoiceDetailArchive();
			foreach($archive_invoice_details as $archive_invoice_detail)
			{
                                $q = Doctrine_Query::create()
                                ->from("MfInvoiceDetail a")
                                ->where("a.id = ?", $archive_invoice_detail->getId());
                                if($q->count())
                                {
                                        echo "-- skipping reverse archiving invoice fee ".$archived_application->getApplicationId()."\n";
                                        continue;
                                }
                                else
                                {
                                        echo "-- reverse archiving invoice fee for ".$archived_application->getApplicationId()."\n";
                                }

				$invoice_detail = new MfInvoiceDetail();
				$invoice_detail->setId($archive_invoice_detail->getId());
				$invoice_detail->setInvoiceId($archive_invoice_detail->getInvoiceId());
				$invoice_detail->setDescription($archive_invoice_detail->getDescription());
				$invoice_detail->setAmount($archive_invoice_detail->getAmount());
				$invoice_detail->setCreatedAt($archive_invoice_detail->getCreatedAt());
				$invoice_detail->setUpdatedAt($archive_invoice_detail->getUpdatedAt());
				$invoice_detail->save();

				//Delete original detail
				$archive_invoice_detail->delete();
			}

			//Delete original invoice
			$archive_invoice->delete();
		}

		//Move Permit
		$archived_permits = $archived_application->getSavedPermits();
		foreach($archived_permits as $archived_permit)
		{
			$permit = new SavedPermit();
			$permit->setId($archived_permit->getId());
			$permit->setTypeId($archived_permit->getTypeId());
			$permit->setApplicationId($archived_permit->getApplicationId());
			$permit->setDateOfIssue($archived_permit->getDateOfIssue());
			$permit->setDateOfExpiry($archived_permit->getDateOfExpiry());
			$permit->setRemoteResult($archived_permit->getRemoteResult());
			$permit->setCreatedBy($archived_permit->getCreatedBy());
			$permit->setLastUpdated($archived_permit->getLastUpdated());
			$permit->setRemoteUpdateUuid($archived_permit->getRemoteUpdateUuid());
			$permit->save();

			//Delete original permit
			$archived_permit->delete();
		}

		//Delete original application
		$archived_application->delete();

        $this->redirect("/backend.php/frusers/show/id/".$entry->getUserId());

    }

    /**
     * Executes 'searcharchive' action
     *
     * searcharchiveAction form
     *
     * @param sfRequest $request A request object
     */
    public function executeSearcharchive(sfWebRequest $request)
    {
        $q = Doctrine_Query::create()
            ->from('SubMenus a')
            ->where("a.id = ?", $request->getParameter("subgroup", $this->getUser()->getAttribute('current_stage')));
        $submenu = $q->fetchOne();

        if($submenu)
        {
            $group = $submenu->getMenuId();
            $_SESSION['subgroup'] = $submenu->getId();
            $_SESSION['group'] = $group;
            $this->submenu = $submenu;
            $this->stage = $this->submenu->getId();
        }

        $q = Doctrine_Query::create()
		   ->from("FormEntryArchive a")
		   ->where("a.application_id = ?", $request->getPostParameter("applicationid", 0))
		   ->orderBy("a.date_of_submission DESC");
		 $this->pager = new sfDoctrinePager('FormEntry', 20);
		 $this->pager->setQuery($q);
		 $this->pager->setPage($request->getParameter('page', 1));
		 $this->pager->init();
    }

    /**
     * Fetch service details using Application ID
     *
     * Fields: 1. Application ID
     *         2. Service ID (Form ID)
     *         3. JSON Template
     *
     * @param sfRequest $request A request object
     */
    public function executeFetchservice(sfWebRequest $request)
    {
        if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW']))
        {
          $username = $_SERVER['PHP_AUTH_USER'];
          $password = $_SERVER['PHP_AUTH_PW'];

          $q = Doctrine_Query::create()
             ->from("CfUser a")
             ->where("a.struserid = ? OR a.stremail = ?", array($username,$username))
             ->andWhere('a.bdeleted = ?', 0);
          $available_user = $q->fetchOne();

          if($available_user)
          {
            $hash = $available_user->getStrpassword();
            if (password_verify($password, $hash)) {
                if (password_needs_rehash($hash, PASSWORD_BCRYPT, $options)) {
                    $hash = password_hash($password, PASSWORD_BCRYPT, $options);
                    /* Store new hash in db */
                    $available_user->setStrpassword($hash);
                    $available_user->save();
                }

                //Process request
                $application_id = $request->getPostParameter("application_id");
                $service_id = $request->getPostParameter("service_id");
                $template = $request->getPostParameter("template");

                $q = Doctrine_Query::create()
                   ->from("FormEntry a")
                   ->where("a.form_id = ?", $service_id)
                   ->andWhere("a.application_id = ?", $application_id);
                $application = $q->fetchOne();

                if($application)
                {
                  $templateparser = new TemplateParser();
                  $template = $templateparser->parseApplication($application->getId(), $template);

                  echo $template;
                  return sfView::NONE;
                }
                else
                {
                  echo "{'result':'no records found'}";
                  return sfView::NONE;
                }
             }
             else
             {
                echo "{'result':'invalid logins'}";
                return sfView::NONE;
             }
          }
          else
          {
            echo "{'result':'invalid user'}";
            return sfView::NONE;
          }
        }
        else
        {
          $this->getResponse()->setHttpHeader('WWW-Authenticate',  'Basic realm="eCitizen Fetch API"');
          $this->getResponse()->setStatusCode('401');
          $this->sendHttpHeaders();
          return sfView::NONE;
        }
    }

    /**
     * Fetch service details using User's ID Number
     * This will only return the lastest application
     *
     * Fields: 1. User ID
     *         2. Service ID (Form ID)
     *         3. JSON Template
     *
     * @param sfRequest $request A request object
     */
    public function executeFetchservicebyid(sfWebRequest $request)
    {
        if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW']))
        {
          $username = $_SERVER['PHP_AUTH_USER'];
          $password = $_SERVER['PHP_AUTH_PW'];

          $q = Doctrine_Query::create()
             ->from("CfUser a")
             ->where("a.struserid = ? OR a.stremail = ?", array($username,$username))
             ->andWhere('a.bdeleted = ?', 0);
          $available_user = $q->fetchOne();

          if($available_user)
          {
            $hash = $available_user->getStrpassword();
            if (password_verify($password, $hash)) {
                if (password_needs_rehash($hash, PASSWORD_BCRYPT, $options)) {
                    $hash = password_hash($password, PASSWORD_BCRYPT, $options);
                    /* Store new hash in db */
                    $available_user->setStrpassword($hash);
                    $available_user->save();
                }

                  $user_id = $request->getPostParameter("user_id");
                  $service_id = $request->getPostParameter("service_id");
                  $template = $request->getPostParameter("template");

                  $q = Doctrine_Query::create()
                     ->from("SfGuardUser a")
                     ->where("a.username = ?", $user_id);
                  $user = $q->fetchOne();

                  if($user)
                  {
                      $q = Doctrine_Query::create()
                         ->from("FormEntry a")
                         ->where("a.form_id = ?", $service_id)
                         ->andWhere("a.user_id = ?", $user->getId())
                         ->andWhere("a.approved <> 0")
                         ->orderBy("a.id DESC");
                      $application = $q->fetchOne();

                      if($application)
                      {
                        $templateparser = new TemplateParser();
                        $template = $templateparser->parseApplication($application->getId(), $template);

                        echo $template;
                        return sfView::NONE;
                      }
                      else
                      {
                        echo "{'result':'no records found'}";
                        return sfView::NONE;
                      }
                  }
                  else
                  {
                      echo "{'result':'no user found'}";
                      return sfView::NONE;
                  }
               }
               else
               {
                  echo "{'result':'invalid logins'}";
                  return sfView::NONE;
               }
            }
            else
            {
              echo "{'result':'invalid user'}";
              return sfView::NONE;
            }
        }
        else
        {
          $this->getResponse()->setHttpHeader('WWW-Authenticate',  'Basic realm="eCitizen Fetch API"');
          $this->getResponse()->setStatusCode('401');
          $this->sendHttpHeaders();
          return sfView::NONE;
        }
    }


    /**
     * Secure fetch api with http authentication
     *
     * @param sfRequest $request A request object
     */
    public function executeFetchsecure(sfWebRequest $request)
    {

        if (!$this->getUser()->hasAttribute("secure_referer"))
        {
          $this->getUser()->setAttribute("secure_referer", $this->getRequest()->getReferer());
        }

        echo $this->getUser()->getAttribute('secure_referer');

        if (isset($_SERVER['PHP_AUTH_USER']))
        {
            if ($_SERVER['PHP_AUTH_USER'] == "admin" && $_SERVER['PHP_AUTH_PW'] == "3emdi2ijeene84h343")
            {
               $this->getUser()->setAttribute("secure_api_session", true);
               $this->redirect($this->getUser()->getAttribute("secure_referer"));
            }
        }

        $this->getResponse()->setHttpHeader('WWW-Authenticate',  'Basic realm="eCitizen Fetch API"');
        $this->getResponse()->setStatusCode('401');
        $this->sendHttpHeaders();
        return sfView::NONE;
    }


    /**
     * Get
     *
     * @param sfRequest $request A request object
     */
    public function executeGetremote(sfWebRequest $request)
    {
        $form_id = $request->getParameter("form_id");
        $entry_id = $request->getParameter("entry_id");
        $element_id = $request->getParameter("element_id");

        $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

        $sql = "SELECT * FROM ap_form_".$form_id." WHERE id = ".$entry_id;
        $app_results = mysql_query($sql,$dbconn);

        $entry_data = mysql_fetch_assoc($app_results);

        $sql = "SELECT * FROM ap_form_elements WHERE form_id = ".$form_id." AND element_id = ".$element_id;
        $element_results = mysql_query($sql,$dbconn);

        $element = mysql_fetch_assoc($element_results);

        $remote_template = "";

        $remote_url = $element['element_option_query'];
        if(!empty($remote_url))
        {
            $criteria = $element['element_field_name'];
            $remote_template = $element['element_field_value'];
            $remote_value = $element['element_remote_value'];
            $remote_username = $element['element_remote_username'];
            $remote_password = $element['element_remote_password'];


            $ch = curl_init();

            $pos = strpos($remote_url, '$value');

            if($pos === false)
            {
                //dont' do anything
            }
            else
            {
                $remote_url = str_replace('$value', curl_escape($ch, $entry_data['element_'.$element_id]), $remote_url);
            }

            curl_setopt($ch, CURLOPT_URL, $remote_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));

            if(!empty($remote_username) && !empty($remote_password))
            {
                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                curl_setopt($ch, CURLOPT_USERPWD, "$remote_username:$remote_password");
            }

            $results = curl_exec($ch);

            if(curl_error($ch))
            {
                $error = "error:" . curl_error($ch) . "<br />";
            }

            $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            curl_close($ch);

            if(empty($error))
            {
                $values = json_decode($results);
                if($criteria == "records")
                {
                    //If count is = 0, fail
                    if($values->{'count'} == 0)
                    {
                        $error = "No records found on server";
                    }
                }
                else if($criteria == "norecords")
                {
                    //If count is greater than 0, then pass
                    if($values->{'count'} > 0)
                    {
                        $error = "Existing records found on server";
                    }
                }
                else if($criteria == "value")
                {
                    //If count is greater than 0, then pass
                    if($remote_value != $results)
                    {
                        $error = "No matching records found on server";
                    }
                }

                foreach($values->{'records'} as $record)
                {
                    foreach($record as $key => $value)
                    {
                        if(!is_array($value))
                        {
                            //search template
                            $pos = strpos($remote_template, "{".$key."}");
                            if($pos === false)
                            {
                                continue;
                            }
                            else
                            {
                                //parse
                                $remote_template = str_replace('{'.$key.'}', $value , $remote_template);
                            }
                        }
                        else
                        {
                            $result_value = "";
                            foreach($value as $lkey => $lvalue)
                            {
                                $result_value = $result_value.$lvalue;
                            }
                            $remote_template = str_replace('{'.$key.'}', $result_value , $remote_template);
                        }
                    }
                }

            }
        }

        echo $remote_template;

        $this->setLayout(false);
        exit;
    }

	/**
	* Get
	*
	* @param sfRequest $request A request object
	*/
	public function executeGetdropdowns(sfWebRequest $request)
	{
		$q = Doctrine_Query::create()
			->from('ApFormElements a')
			->where('a.form_id = ?', $request->getParameter('formid'))
			->andWhere('a.element_status = ?', 1)
			->andWhere('a.element_type LIKE ?', '%select%')
			->orderBy('a.element_title ASC');
		$elements = $q->execute();

		echo "<select name='form_dropdown_fields' id='form_dropdown_fields' class='form-control'>";
		echo "<option>Choose a dropdown field...</option>";
		foreach($elements as $element)
		{
			echo "<option value='".$element->getElementId()."'>".$element->getElementTitle()."</option>";
		}
		echo "</select>";
		echo '<script language="javascript">
			jQuery(document).ready(function(){
					jQuery("#form_dropdown_fields" ).change(function() {
							var selecteditem = this.value;
							$.ajax({url:"/backend.php/applications/getdropdownvaluefields?elementid=" + selecteditem,success:function(result){
								$("#ajaxdropdownvaluefields").html(result);
							}});
					});
			});
		</script>';
		exit;
	}

	public function executeGetdropdownvaluefields(sfWebRequest $request)
	{
		$q = Doctrine_Query::create()
			->from('ApElementOptions a')
			->where('a.element_id = ?', $request->getParameter('elementid'))
			->andWhere('a.live = 1')
			->orderBy('a.position ASC');
		$options = $q->execute();

		echo "<select name='form_dropdown_value_fields' id='form_dropdown_value_fields' class='form-control' onChange='document.getElementById(\"filter_apps\").submit();'>";
		echo "<option>Filter by an option..</option>";
		foreach($options as $option)
		{
			echo "<option value='".$option->getOptionId()."'>".$option->getOption()."</option>";
		}
		echo "</select>";
		exit;
	}

  /**
  * Executes 'Cronjob' action
  *
  * To be run by cronjobs to check on expired applications
  *
  * @param sfRequest $request A request object
  */
  public function executeCronjob(sfWebRequest $request)
  {
        $invoice_manager = new InvoiceManager();

        $foundexpired = false;
        $q = Doctrine_Query::create()
           ->from("SubMenus a")
           ->where("a.max_duration <> ? AND a.max_duration <> ?", array('', 0));
        $submenus = $q->execute();
        foreach($submenus as $submenu)
        {
            $q = Doctrine_Query::create()
               ->from("FormEntry a")
               ->where("a.approved = ?", $submenu->getId());
            $applications = $q->execute();
            foreach($applications as $application)
            {
                $q = Doctrine_Query::create()
                   ->from("ApplicationReference a")
                   ->where("a.application_id = ?", $application->getId())
                   ->andWhere("a.stage_id = ?", $submenu->getId())
                   ->orderBy("a.id DESC");
                $reference = $q->fetchOne();
                if($reference)
                {
                    if(GetDaysDiff($reference->getStartDate(), date('Y-m-d')) > $submenu->getMaxDuration())
                    {
                        $foundexpired = true;

                        //Check stage for expired stage settings
                        $q = Doctrine_Query::create()
                            ->from("SubMenus a")
                            ->where("a.id = ?", $application->getApproved());
                        $stage = $q->fetchOne();

                        if($stage && $stage->getStageExpiredMovement()) {
                            $application->setApproved($stage->getStageExpiredMovement());
                            $application->save();
                        }
                    }
                }
                else
                {
                    if(GetDaysDiff($application->getDateOfSubmission(), date('Y-m-d')) > $submenu->getMaxDuration())
                    {
                        $foundexpired = true;

                        //Check stage for expired stage settings
                        $q = Doctrine_Query::create()
                            ->from("SubMenus a")
                            ->where("a.id = ?", $application->getApproved());
                        $stage = $q->fetchOne();

                        if($stage && $stage->getStageExpiredMovement()) {
                            $application->setApproved($stage->getStageExpiredMovement());
                            $application->save();
                        }
                    }
                }
            }
        }
        if($foundexpired)
        {
            echo "Found expired applications";
        }
        else
        {
            echo "Did not find expired applications";
        }


        $q = Doctrine_Query::create()
        ->from("MfInvoice a")
        ->where("a.paid <> 2");
        $invoices = $q->execute();

        foreach($invoices as $invoice)
        {
            $invoice = $invoice_manager->update_payment_status($invoice->getId());
            $invoice_manager->update_invoices($invoice->getFormEntry()->getId());
        }

      $prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";

      include_once($prefix_folder.'includes/OAuth.php');
      require_once($prefix_folder.'includes/init.php');

      require_once($prefix_folder.'config.php');
      require_once($prefix_folder.'includes/db-core.php');
      require_once($prefix_folder.'includes/helper-functions.php');
      require_once($prefix_folder.'includes/check-session.php');

      require_once($prefix_folder.'includes/entry-functions.php');
      require_once($prefix_folder.'includes/post-functions.php');
      require_once($prefix_folder.'includes/users-functions.php');


      $dbh = mf_connect_db();

      //Check for duplicate application number and update
      $sql = "SELECT application_id, COUNT(*) FROM form_entry GROUP BY application_id HAVING COUNT(*) > 1";
      $params = array();
      $results = mf_do_query($sql,$params,$dbh);

      while($row = mf_do_fetch_result($results))
      {
         error_log("Duplicate Record Numbers Found by CronJob: ".$row['application_id']);

          //Check for duplicate application numbers and update the latest one
          $q = Doctrine_Query::create()
              ->from("FormEntry a")
              ->where("a.approved <> ?", 0)
              ->andWhere("a.parent_submission = ?", 0)
              ->andWhere("a.deleted_status = ?", 0)
              ->andWhere("a.application_id = ?", $row['application_id'])
              ->orderBy("a.date_of_submission DESC");
          $duplicate_records = $q->execute();

          foreach($duplicate_records as $duplicate_record)
          {
              //Check which application was submitted last, update the number of the latter one
              $q = Doctrine_Query::create()
                  ->from("FormEntry a")
                  ->where("a.approved <> ?", 0)
                  ->andWhere("a.parent_submission = ?", 0)
                  ->andWhere("a.deleted_status = ?", 0)
                  ->andWhere("a.application_id = ?", $duplicate_record->getApplicationId())
                  ->andWhere("a.id <> ?", $duplicate_record->getId())
                  ->orderBy("a.date_of_submission DESC");
              $other_duplicate_records = $q->execute();

              foreach($other_duplicate_records as $other_duplicate_record)
              {
                  $q = Doctrine_Query::create()
                      ->from('FormEntry a')
                      ->where('a.approved <> 0 AND a.declined <> 1 AND a.parent_submission = 0')
                      ->andWhere('a.form_id = ?', $duplicate_record->getFormId())
                      ->orderBy("a.application_id DESC");
                  $last_app = $q->fetchOne();

                  $new_app_id = ""; //submission identifier
                  $identifier_start = ""; //first stage

                  if($last_app)
                  {
                      $new_app_id = $last_app->getApplicationId();
                      $new_app_id = ++$new_app_id;

                      $duplicate_record->setApplicationId($new_app_id);
                      $duplicate_record->save();
                  }

                  error_log("Updating Duplicate Record's Number via CronJob: ".$duplicate_record->getApplicationId());
                  break;
              }
          }
      }

      //Check for duplicate submission on the same form_id and entry_id
      // iterate through each form

      $q = Doctrine_Query::create()
          ->from('ApForms a')
          ->andWhere('a.form_type = 1')
          ->andWhere('a.form_active = 1')
          ->orderBy('a.form_name ASC');
      $forms = $q->execute();

      foreach($forms as $form)
      {

          $sql = "SELECT form_id, entry_id, COUNT(*) FROM form_entry WHERE form_id = ? AND approved <> 0 GROUP BY entry_id HAVING COUNT(*) > 1";
          $params = array($form->getFormId());
          $results = mf_do_query($sql,$params,$dbh);

          while($row = mf_do_fetch_result($results))
          {
              error_log("Duplicate Record Submissions Found by CronJob: ".$row['application_id']);

              //Delete all but the first submission
              $q = Doctrine_Query::create()
                  ->from("FormEntry a")
                  ->where("a.approved <> 0")
                  ->andWhere("a.form_id = ? AND a.entry_id = ?", array($row['form_id'], $row['entry_id']))
                  ->orderBy("a.date_of_submission  ASC");
              $applications = $q->execute();

              $count = 1;
              foreach($applications as $application)
              {
                 if($count == 1)
                 {
                    //ignore, leave this application
                 }
                 else
                 {
                    //save duplicate applications as draft
                     $application->setApproved(0);
                     $application->save();

                     //delete duplicate application invoices
                     $invoices = $application->getMfInvoice();
                     foreach($invoices as $invoice)
                     {
                         $invoice->delete();
                     }

                     //delete duplicate application services
                     $services = $application->getSavedPermits();
                     foreach($services as $service)
                     {
                         $service->delete();
                     }

                 }
                  $count++;
              }
          }

      }

  	exit;
  	$this->setLayout(false);
  }


  /**
  * Executes 'Create' action
  *
  * Create new applications
  *
  * @param sfRequest $request A request object
  */
  public function executeCreate(sfWebRequest $request)
  {
        if($request->getPostParameter("usersearch"))
        {
            $this->filter = $request->getPostParameter("usersearch");
            $this->step = 2;
        }

  		if($request->getPostParameter("users"))
  		{
  			$_SESSION['create_user'] = $request->getPostParameter("users");
  			$this->step = 3;
  		}

  		if($_GET['formid'])
  		{
  			$this->formid = $_GET['formid'];
  			$this->step = 3;
  		}

  		if($_GET['id'])
  		{
  			$this->formid = $_GET['id'];
  			$this->step = 3;
  		}

        if($this->getUser()->getAttribute('current_stage'))
        {
            $this->current_stage = $this->getUser()->getAttribute('current_stage');
        }
  }

    /**
     * Executes 'Payment' action
     *
     * Payment form
     *
     * @param sfRequest $request A request object
     */
    public function executePayment(sfWebRequest $request)
    {
				$q = Doctrine_Query::create()
				   ->from("FormEntry a")
					 ->where("a.form_id = ?", $request->getParameter("id"))
					 ->andWhere("a.entry_id = ?", $request->getParameter("entryid"));
				$application = $q->fetchOne();

				if($application)
				{
					$_SESSION['create_user'] = $application->getUserId();
					$user_id = $application->getUserId();
				}
    }


    /**
     * Executes 'confirmApplication' action
     *
     * confirmApplication form
     *
     * @param sfRequest $request A request object
     */
    public function executeConfirmApplication(sfWebRequest $request)
    {

    }

    /**
     * Executes 'Confirmpayment' action
     *
     * Payment form
     *
     * @param sfRequest $request A request object
     */
    public function executeConfirmpayment(sfWebRequest $request)
    {
        //OTB patch - get invoice
        $this->invoice_id_passed = $request->getParameter('invoice_id_passed');
        $this->setLayout("layoutdashfull");
    }

    /**
     * Executes 'Confirmpayment' action
     *
     * Payment form
     *
     * @param sfRequest $request A request object
     */
    public function executeInvalidpayment(sfWebRequest $request)
    {
        $this->setLayout(false);
    }


    public function executePesapalipn(sfWebRequest $request)
    {
        $this->mailer = $this->getMailer();
        $this->setLayout(false);
    }

   /**
   * Executes 'Batch' action
   *
   * Perform batch actions from the applications list view
   *
   * @param sfRequest $request A request object
   */
   public function executeBatch(sfWebRequest $request)
   {
   		if($request->getParameter("trash"))
   		{
	   		$batchitems = $request->getPostParameter("batchitems");
	   		foreach($batchitems as $batchitem)
	   		{
		   		$q = Doctrine_Query::create()
		   			->from("FormEntry a")
		   			->where("a.id = ?", $batchitem);
		   		$application = $q->fetchOne();
		   		if($application)
		   		{
			   		$application->setDeletedStatus("1");
			   		$application->save();
		   		}
	   		}
   		}
   		if($request->getParameter("archive"))
   		{
	   		$batchitems = $request->getPostParameter("batchitems");
	   		foreach($batchitems as $batchitem)
	   		{
		   		$q = Doctrine_Query::create()
		   			->from("FormEntry a")
		   			->where("a.id = ?", $batchitem);
		   		$application = $q->fetchOne();
		   		if($application)
		   		{
			   		$application->setApproved("-1000");
			   		$application->save();
		   		}
	   		}
   		}
   		if($request->getParameter("spam"))
   		{
	   		$batchitems = $request->getPostParameter("batchitems");
	   		foreach($batchitems as $batchitem)
	   		{
		   		$q = Doctrine_Query::create()
		   			->from("FormEntry a")
		   			->where("a.id = ?", $batchitem);
		   		$application = $q->fetchOne();
		   		if($application)
		   		{
			   		$application->setApproved("-100");
			   		$application->save();
		   		}
	   		}
   		}
   		exit;
   }

   /**
   * Executes 'List' action
   *
   * Displays a list of applications most recent first
   *
   * @param sfRequest $request A request object
   */
   public function executeList(sfWebRequest $request)
   {
		$applications = "";
		$approved = "";
		$submenus = "";

		$q = Doctrine_Query::create()
			->from('SubMenus a');
		$stages = $q->execute();

		$qcount = 0;

		foreach($stages as $stage)
		{
			if($this->getUser()->mfHasCredential('accesssubmenu'.$stage->getId()))
			{
				$qcount++;
				if($qcount == 1)
				{
					$submenus .= "a.approved = ".$stage->getId()." ";
				}
				else
				{
					$submenus .= " OR a.approved = ".$stage->getId()." ";
				}
			}
		}

		$q = Doctrine_Query::create()
		   ->from("FormEntry a")
		   ->where("a.parent_submission = ?", 0)
		   ->andWhere("a.deleted_status = ?", 0)
		   ->andWhere($submenus)
		   ->orderBy("a.date_of_submission DESC");
		 $this->pager = new sfDoctrinePager('FormEntry', 15);
		 $this->pager->setQuery($q);
		 $this->pager->setPage($request->getParameter('page', 1));
		 $this->pager->init();
   }


   /**
   * Executes 'Liststarred' action
   *
   * Displays a list of applications most recent first
   *
   * @param sfRequest $request A request object
   */
   public function executeListstarred(sfWebRequest $request)
   {
		$applications = "";
		$approved = "";
		$submenus = "";

		$q = Doctrine_Query::create()
		   ->from("FormEntry a")
		   ->leftJoin("a.Favorites b")
		   ->where("b.userid = ?", $this->getUser()->getAttribute('userid'))
		   ->andWhere("a.deleted_status = ?", 0)
		   ->orderBy("a.date_of_submission DESC");
		 $this->pager = new sfDoctrinePager('FormEntry', 15);
		 $this->pager->setQuery($q);
		 $this->pager->setPage($request->getParameter('page', 1));
		 $this->pager->init();
   }

   /**
   * Executes 'setstart' action
   *
   * Displays a list of applications most recent first
   *
   * @param sfRequest $request A request object
   */
   public function executeSetstar(sfWebRequest $request)
   {
	   $q = Doctrine_Query::create()
	      ->from("Favorites a")
		  ->where("a.application_id = ?", $request->getParameter("id"));
	   $favorite = $q->fetchOne();
	   if($favorite)
	   {
		   $favorite->delete();
	   }
	   else
	   {
		   $favorite = new Favorites();
		   $favorite->setApplicationId($request->getParameter("id"));
		   $favorite->setUserid($this->getUser()->getAttribute('userid'));
		   $favorite->setDatesent(date("Y-m-d"));
		   $favorite->save();
	   }

	   exit;
   }

   /**
   * Executes 'Listtrash' action
   *
   * Displays a list of applications most recent first
   *
   * @param sfRequest $request A request object
   */
   public function executeListtrash(sfWebRequest $request)
   {
		$applications = "";
		$approved = "";
		$submenus = "";

		$q = Doctrine_Query::create()
			->from('SubMenus a');
		$stages = $q->execute();

		$qcount = 0;
		foreach($stages as $stage)
		{
			if($this->getUser()->mfHasCredential('accesssubmenu'.$stage->getId()))
			{
				$qcount++;
				if($qcount == 1)
				{
					$submenus .= "a.approved = ".$stage->getId()." ";
				}
				else
				{
					$submenus .= " OR a.approved = ".$stage->getId()." ";
				}
			}
		}

		$q = Doctrine_Query::create()
		   ->from("FormEntry a")
		   ->where("a.parent_submission = ?", 0)
		   ->andWhere("a.deleted_status = ?", 1)
		   ->andWhere($submenus)
		   ->orderBy("a.date_of_submission DESC");
		 $this->pager = new sfDoctrinePager('FormEntry', 15);
		 $this->pager->setQuery($q);
		 $this->pager->setPage($request->getParameter('page', 1));
		 $this->pager->init();
   }


   /**
   * Executes 'Listtrash' action
   *
   * Displays a list of applications most recent first
   *
   * @param sfRequest $request A request object
   */
   public function executeListsentback(sfWebRequest $request)
   {
		$applications = "";
		$approved = "";
		$submenus = "";

		$q = Doctrine_Query::create()
			->from('SubMenus a');
		$stages = $q->execute();

		$qcount = 0;
		foreach($stages as $stage)
		{
			if($this->getUser()->mfHasCredential('accesssubmenu'.$stage->getId()))
			{
				$qcount++;
				if($qcount == 1)
				{
					$submenus .= "a.approved = ".$stage->getId()." ";
				}
				else
				{
					$submenus .= " OR a.approved = ".$stage->getId()." ";
				}
			}
		}

		$q = Doctrine_Query::create()
		   ->from("FormEntry a")
		   ->where("a.parent_submission = ?", 0)
		   ->andWhere("a.declined = ?", 1)
		   ->andWhere("a.deleted_status = ?", 0)
		   ->andWhere($submenus)
		   ->orderBy("a.date_of_submission DESC");
		 $this->pager = new sfDoctrinePager('FormEntry', 15);
		 $this->pager->setQuery($q);
		 $this->pager->setPage($request->getParameter('page', 1));
		 $this->pager->init();
   }


   /**
   * Executes 'Listgroup' action
   *
   * Displays a list of applications most recent first
   *
   * @param sfRequest $request A request object
   */
   public function executeListgroup(sfWebRequest $request)
   {
      $application_manager = new ApplicationManager();

      //Check if any filters are set for the application
      $applications = "";
      $approved = "";
      $submenus = "";
      $filters = array();
      //OTB patch - application queuing
      $app_queuing = null ; //latest to oldest - mysql query is set to set default value
      //call helper
      $otb_helper = new OTBHelper();
      $this->export_link = "/backend.php/applications/export/";

      if($request->getPostParameter("application_form"))
      {
        $filters['application_form'] = $request->getPostParameter("application_form");
        $filters['form_dropdown_fields'] = $request->getPostParameter("form_dropdown_fields");
        $filters['form_dropdown_value_fields'] = $request->getPostParameter("form_dropdown_value_fields");
        $this->export_link.= "form/".$this->application_form."/field/".$this->form_dropdown_fields."/value/".$this->form_dropdown_value_fields."/";
      }

      $this->stage = $request->getParameter("subgroup", $this->getUser()->getAttribute('current_stage'));
      $this->export_link.= "stage/".$request->getParameter("subgroup");
      if($request->getParameter("subgroup")) {
           $this->getUser()->setAttribute('current_stage', $request->getParameter("subgroup"));
      }
      $_SESSION['formid'] = "";

      $q = Doctrine_Query::create()
       ->from('SubMenus a')
       ->where("a.id = ?", $request->getParameter("subgroup", $this->getUser()->getAttribute('current_stage')));
      $submenu = $q->fetchOne();

       if($submenu)
       {
           $group = $submenu->getMenuId();
           $_SESSION['subgroup'] = $submenu->getId();
           $_SESSION['group'] = $group;
           $this->submenu = $submenu;
           ////////////////////////// OTB patch
           // get the menu id
           $menu_id = $submenu->getMenuId();
           //
           $query_menu = Doctrine_Query::create()
            ->from('Menus a')
            ->where("a.id = ?", $menu_id);
           $menu = $query_menu->fetchOne();
           //get queuing set for the menu id
           if($menu)
           {        
              $app_queuing = $otb_helper->getAppQueuing($submenu->getAppQueuing(), $menu->getAppQueuing()) ;  
             // error_log("Debug >>> app_queuing >>".$app_queuing);
           }
           else {
               //just incase user forgets to execute updates query - force to DESC
                $app_queuing = 'DESC' ;
           }
           
           //////////////////////////////////
           $this->setLayout('layout-metronic');
           
       }

      $page = $request->getParameter('page', 1);
			$this->page = $page;

      //Check if logic permission is enabled on any form. If it is then filter application for user groups
      if($application_manager->any_logic_permissions_for_stage($request->getParameter("subgroup")))
      {
          if($request->getPostParameter("application_form"))
          {
            $this->application_form = $request->getPostParameter("application_form");

            $this->has_logic = true;
            $this->applications = $application_manager->get_logiced_tabled_applications($request->getPostParameter("application_form"), $page, $request->getParameter("subgroup"), $this->getUser(),$filters);
          }
          else
          {
						if($request->getParameter("form"))
						{
							$this->application_form = $request->getParameter("form");

	            $this->has_logic = true;
	            $this->applications = $application_manager->get_logiced_tabled_applications($request->getParameter("form"), $page, $request->getParameter("subgroup"), $this->getUser(),$filters);
						}
						else
						{
	            $application_form = $application_manager->get_default_form_by_stage($request->getParameter("subgroup"));
	            $this->application_form = $application_form;

	            $this->has_logic = true;
	            $this->applications = $application_manager->get_logiced_tabled_applications($application_form, $page, $request->getParameter("subgroup"), $this->getUser(),$filters);
						}
          }
      }
      else
      {
        $this->has_logic = false;
       // $this->applications = $application_manager->get_applications($page, $request->getParameter("subgroup"), $this->getUser(),$filters);
        //OTB patch - Add queuing request - Call our custom function
         $this->applications = $application_manager->get_applications_listing_custom($page, 
                 $request->getParameter("subgroup"), $this->getUser(),$filters,$app_queuing); 
      }

	//OTB Start Patch - If current stage (first stage of workflow/menus/service) is an archiving stage, redirect to the archive link
	  if($submenu and $submenu->getStageType() == 7){
			$this->redirect("/backend.php/applications/searcharchive/subgroup/".$submenu->getId());
	  }
	//OTB End Patch - If current stage (first stage of workflow/menus/service) is an archiving stage, redirect to the archive link
    $this->setLayout('layout-metronic') ;   
}



public function executeListtoday(sfWebRequest $request)
   {
      
      $application_manager = new ApplicationManager();

      //Check if any filters are set for the application
      $applications = "";
      $approved = "";
      $submenus = "";
      $filters = array();
      //OTB patch - application queuing
      $app_queuing = null ; //latest to oldest - mysql query is set to set default value
      //call helper
      $otb_helper = new OTBHelper();
      $this->export_link = "/backend.php/applications/export/";

      if($request->getPostParameter("application_form"))
      {
        $filters['application_form'] = $request->getPostParameter("application_form");
        $filters['form_dropdown_fields'] = $request->getPostParameter("form_dropdown_fields");
        $filters['form_dropdown_value_fields'] = $request->getPostParameter("form_dropdown_value_fields");
        $this->export_link.= "form/".$this->application_form."/field/".$this->form_dropdown_fields."/value/".$this->form_dropdown_value_fields."/";
      }

      $this->stage = $request->getParameter("subgroup", $this->getUser()->getAttribute('current_stage'));
      $this->export_link.= "stage/".$request->getParameter("subgroup");
      if($request->getParameter("subgroup")) {
           $this->getUser()->setAttribute('current_stage', $request->getParameter("subgroup"));
      }
      $_SESSION['formid'] = "";

      $q = Doctrine_Query::create()
       ->from('SubMenus a')
       ->where("a.id = ?", $request->getParameter("subgroup", $this->getUser()->getAttribute('current_stage')));
      $submenu = $q->fetchOne();

       if($submenu)
       {
           $group = $submenu->getMenuId();
           $_SESSION['subgroup'] = $submenu->getId();
           $_SESSION['group'] = $group;
           $this->submenu = $submenu;
           ////////////////////////// OTB patch
           // get the menu id
           $menu_id = $submenu->getMenuId();
           //
           $query_menu = Doctrine_Query::create()
            ->from('Menus a')
            ->where("a.id = ?", $menu_id);
           $menu = $query_menu->fetchOne();
           //get queuing set for the menu id
           if($menu)
           {        
              $app_queuing = $otb_helper->getAppQueuing($submenu->getAppQueuing(), $menu->getAppQueuing()) ;  
             // error_log("Debug >>> app_queuing >>".$app_queuing);
           }
           else {
               //just incase user forgets to execute updates query - force to DESC
                $app_queuing = 'DESC' ;
           }
           
           //////////////////////////////////
           $this->setLayout('layout-metronic');
           
       }

      $page = $request->getParameter('page', 1);
      $this->page = $page;

      //Check if logic permission is enabled on any form. If it is then filter application for user groups
      if($application_manager->any_logic_permissions_for_stage($request->getParameter("subgroup")))
      {
          if($request->getPostParameter("application_form"))
          {
            $this->application_form = $request->getPostParameter("application_form");

            $this->has_logic = true;
            $this->applications = $application_manager->get_logiced_tabled_applications($request->getPostParameter("application_form"), $page, $request->getParameter("subgroup"), $this->getUser(),$filters);
          }
          else
          {
            if($request->getParameter("form"))
            {
              $this->application_form = $request->getParameter("form");

              $this->has_logic = true;
              $this->applications = $application_manager->get_logiced_tabled_applications($request->getParameter("form"), $page, $request->getParameter("subgroup"), $this->getUser(),$filters);
            }
            else
            {
              $application_form = $application_manager->get_default_form_by_stage($request->getParameter("subgroup"));
              $this->application_form = $application_form;

              $this->has_logic = true;
              $this->applications = $application_manager->get_logiced_tabled_applications($application_form, $page, $request->getParameter("subgroup"), $this->getUser(),$filters);
            }
          }
      }
      else
      {
        $this->has_logic = false;
       // $this->applications = $application_manager->get_applications($page, $request->getParameter("subgroup"), $this->getUser(),$filters);
        //OTB patch - Add queuing request - Call our custom function
         $this->applications = $application_manager->get_applications_listing_custom($page, 
                 $request->getParameter("subgroup"), $this->getUser(),$filters,$app_queuing); 
      }

  //OTB Start Patch - If current stage (first stage of workflow/menus/service) is an archiving stage, redirect to the archive link
    if($submenu and $submenu->getStageType() == 7){
      $this->redirect("/backend.php/applications/searcharchive/subgroup/".$submenu->getId());
    }
  //OTB End Patch - If current stage (first stage of workflow/menus/service) is an archiving stage, redirect to the archive link
    $this->setLayout('layout-metronic') ;   
  }






   /**
   * Executes 'Export' action
   *
   * Displays a list of applications most recent first
   *
   * @param sfRequest $request A request object
   */
   public function executeExport(sfWebRequest $request)
   {
		$applications = "";
		$approved = "";
		$submenus = "";

		$this->export_link = "/backend.php/applications/export/";

		if($request->getParameter("form"))
		{
    		$this->application_form = $request->getParameter("form");
			$this->form_dropdown_fields = $request->getParameter("field");
			$this->form_dropdown_value_fields = $request->getParameter("value");
		}

		$this->group ="";
		$this->subgroup = "";

		if($request->getParameter("stagegroup"))
		{
			$q = Doctrine_Query::create()
				->from('SubMenus a')
				->where("a.menu_id = ?", $request->getParameter("stagegroup"));
			$stages = $q->execute();

			$qcount = 0;
			foreach($stages as $stage)
			{
				if($this->getUser()->mfHasCredential('accesssubmenu'.$stage->getId()))
				{
					$qcount++;
					if($qcount == 1)
					{
						$submenus .= "a.approved = ".$stage->getId()." ";
					}
					else
					{
						$submenus .= " OR a.approved = ".$stage->getId()." ";
					}
				}
			}
			$this->group = $request->getParameter("stagegroup");
		}
		elseif($request->getParameter("stage"))
		{
			$submenus = "a.approved = ".$request->getParameter("stage")." ";

			$q = Doctrine_Query::create()
				->from("SubMenus a")
				->where("a.id = ?", $request->getParameter("stage"));
		    $submenu_item = $q->fetchOne();

		    if($submenu_item)
		    {
			    $this->group = $submenu_item->getMenuId();
		    }

			$this->subgroup = $request->getParameter("stage");
		}

		$this->selectedform = "";
			$_SESSION['group'] = $this->group;

		if($this->application_form)
		{
				$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
				mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

				$sql = "SELECT a.id as id FROM form_entry a LEFT JOIN ap_form_".$this->application_form." b ON a.entry_id = b.id WHERE a.form_id = ".$this->application_form." AND b.element_".$this->form_dropdown_fields." = ".$this->form_dropdown_value_fields." AND (".$submenus.")";
				$results = mysql_query($sql, $dbconn);
				$entries = "";

				$count = 0;
				while($row = mysql_fetch_assoc($results))
				{
					$count++;
					if($count == 1)
					{
						$entries = "a.id = ".$row['id'];
					}
					else
					{
						$entries .= " OR a.id = ".$row['id'];
					}
				}

				if($count == 0)
				{
					$this->q = Doctrine_Query::create()
						->from("FormEntry a")
						->where("a.id = 0")
						->orderBy("a.date_of_submission DESC");
				}
				else
				{
					$this->q = Doctrine_Query::create()
						->from("FormEntry a")
						->where("a.parent_submission = ?", 0)
						->andWhere("a.deleted_status = ?", 0)
						->andWhere($entries)
						->orderBy("a.date_of_submission DESC");
				 }
		}
		else
		{
			$this->q = Doctrine_Query::create()
			   ->from("FormEntry a")
			   ->where("a.parent_submission = ?", 0)
			   ->andWhere("a.deleted_status = ?", 0)
			   ->andWhere($submenus)
			   ->orderBy("a.date_of_submission DESC");
		 }

        $firstapplication =  $this->q->fetchOne();

		$applications = $this->q->execute();

			$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
		    mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

           if($firstapplication)
           {
			$q = Doctrine_Query::create()
	         ->from('ApFormElements a')
	         ->where('a.form_id = ?', $firstapplication->getFormId())
	         ->andWhere('a.element_type <> ? AND a.element_type <> ?', array('section','file'))
	         ->andWhere('a.element_status = 1')
	         ->orderBy('a.element_position ASC');
	       $fields = $q->execute();
	   	   }
	   	   else
	   	   {
	   	   	$fields = array();
	   	   }

			$columns = "";
      		$columns[] = "Service Code";
			$columns[] = "Form Name";
			$columns[] = "Application No";
			$columns[] = "Submitted On";
			$columns[] = "Submitted By";
			$columns[] = "Status";

	        foreach($fields as $field)
	        {
	          $columns[] = $field->getElementTitle();
	        }

			$records = "";

			foreach($applications as $application)
			{
		          $query = "SELECT * FROM ap_form_".$application->getFormId()." WHERE id = '".$application->getEntryId()."'";
		          $apresult = mysql_query($query,$dbconn);
		          $apform = mysql_fetch_assoc($apresult);

					$record_columns = "";
					$q = Doctrine_Query::create()
						 ->from('ApForms a')
						 ->where('a.form_id = ?', $application->getFormId());
					$form = $q->fetchOne();
					if($form)
					{
            			$record_columns[] = $form->getFormCode();
						$record_columns[] = $form->getFormName();
					}
					else
					{
						$record_columns[] =  "-";
					}

					$record_columns[] = $application->getApplicationId();

					$record_columns[] = $application->getDateOfSubmission();

					$q = Doctrine_Query::create()
						 ->from('sfGuardUserProfile a')
						 ->where('a.user_id = ?', $application->getUserId());
					$userprofile = $q->fetchOne();
			          $q = Doctrine_Query::create()
			             ->from('sfGuardUser a')
			             ->where('a.id = ?', $application->getUserId());
			          $user = $q->fetchOne();
					if($userprofile)
					{
						$record_columns[] = $userprofile->getFullname()." (".$user->getUsername().")";
					}
					else
					{
						$record_columns[] = "-";
					}

					 $q = Doctrine_Query::create()
						->from('SubMenus a')
						->where('a.id = ?', $application->getApproved());
					$submenu = $q->fetchOne();
					$record_columns[] = $submenu->getTitle();

			          foreach($fields as $field)
			          {
			             if($field->getElementType() == "select")
			             {
			             	if($field->getElementExistingForm() && $field->getElementExistingStage())
							{
								$q = Doctrine_Query::create()
								   ->from("FormEntry a")
								   ->where("a.id = ?", $apform['element_'.$field->getElementId()]);
								$linked_application = $q->fetchOne();
								if($linked_application)
								{
									$record_columns[] = $linked_application->getApplicationId();
								}
								else
								{
					               $q = Doctrine_Query::create()
					                  ->from('ApElementOptions a')
					                  ->where('a.element_id = ?', $field->getElementId())
					                  ->andWhere('a.option_id = ?', $apform["element_".$field->getElementId()])
					                  ->andWhere('a.form_id = ?', $application->getFormId());
					               $option_value = $q->fetchOne();
					               if($option_value)
					               {
					                 $record_columns[] = $option_value->getOption();
					               }
					               else
					               {
					                 $record_columns[] = "-";
					               }
					            }
					        }
					        else
					        {
					          $q = Doctrine_Query::create()
				                  ->from('ApElementOptions a')
				                  ->where('a.element_id = ?', $field->getElementId())
				                  ->andWhere('a.option_id = ?', $apform["element_".$field->getElementId()])
				                  ->andWhere('a.form_id = ?', $application->getFormId());
				               $option_value = $q->fetchOne();
				               if($option_value)
				               {
				                 $record_columns[] = $option_value->getOption();
				               }
				               else
				               {
				                 $record_columns[] = "-";
				               }
					        }
			             }
			             elseif($field->getElementType() == "checkbox" || $field->getElementType() == "radio")
			             {
			                 $choices = "";

			                 $q = Doctrine_Query::create()
			                    ->from('ApElementOptions a')
			                    ->where('a.element_id = ?', $field->getElementId())
			                    ->andWhere('a.form_id = ?', $application->getFormId());
			                 $options = $q->execute();
			                 foreach($options as $option)
			                 {
			                     if($apform["element_".$field->getElementId()."_".$option->getOptionId()])
			                     {
			                         $choices .= $option->getOption().", ";
			                     }
			                 }

			                 $record_columns[] = $choices;
			             }
			             else
			             {
			               $record_columns[] = $apform["element_".$field->getElementId()];
			             }
			          }

					$records[] = $record_columns;
			}

			$this->ReportGenerator("Applications ".date("Y-m-d"), $columns, $records);
   }

   /**
     * Executes 'ReportGenerator' action
     *
     * Reusable excel generator
     *
     * @param sfRequest $request A request object
     */
    public function ReportGenerator($reportname, $columns, $records)
    {
        date_default_timezone_set('Africa/Nairobi');

        if (PHP_SAPI == 'cli')
            die('This example should only be run from a Web Browser');

        /** Include PHPExcel */
        require_once dirname(__FILE__).'/../../../../../lib/vendor/phpexcel/Classes/PHPExcel.php';

        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("One Stop Center, City Of Kigali")
            ->setTitle($reportname);

        // Add some data
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$alpha_count = "B";
        foreach ($columns as $key => $value)
    	{
			$objPHPExcel->getActiveSheet()->getColumnDimension($alpha_count)->setAutoSize(true);
			$alpha_count++;
		}

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A4', 'No');
        $alpha_count = "B";
        foreach ($columns as $key => $value)
    	{
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($alpha_count.'4', $value);
			$alpha_count++;
		}

		$alpha_count--;

        $objPHPExcel->getActiveSheet()->getStyle('A4')->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->getStyle('A4:'.($alpha_count).'4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A4:'.($alpha_count).'4')->getFill()->getStartColor()->setARGB('46449a');

        $objPHPExcel->getActiveSheet()->getStyle('A4')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);

		$alpha_count = "B";
        foreach ($columns as $key => $value)
    	{
			$objPHPExcel->getActiveSheet()->getStyle($alpha_count.'4')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
			$alpha_count++;
		}

		$alpha_count--;

        $objPHPExcel->getActiveSheet()->getStyle('A1:'.($alpha_count).'1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1:'.($alpha_count).'1')->getFill()->getStartColor()->setARGB('504dc5');
        $objPHPExcel->getActiveSheet()->getStyle('A2:'.($alpha_count).'2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2:'.($alpha_count).'2')->getFill()->getStartColor()->setARGB('504dc5');
        $objPHPExcel->getActiveSheet()->getStyle('A3:'.($alpha_count).'3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A3:'.($alpha_count).'3')->getFill()->getStartColor()->setARGB('504dc5');

        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
        $objDrawing->setPath('./assets_unified/images/logo.png');
        $objDrawing->setHeight(60);
        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

		$alpha_count = "B";
        foreach ($columns as $key => $value)
    	{
			$objPHPExcel->getActiveSheet()->getStyle($alpha_count.'4')->getFont()->setBold(true);
			$alpha_count++;
		}

        /**
         * Fetch all applications linked to the filtered 'type of application' and the 'start date'
         */
        $count = 5;

		// Miscellaneous glyphs, UTF-8
		$alpha_count = "B";

		foreach($records as $record_columns)
		{
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$count, $count-4);
			$alpha_count = "B";
			foreach ($record_columns as $key => $value)
			{
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue($alpha_count.$count, $value);
				$alpha_count++;
			}
			$count++;
		}


        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle($reportname);


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="report1.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    }

   /**
   * Executes 'Move' action
   *
   * Moves an application directly to any stage
   *
   * @param sfRequest $request A request object
   */
   public function executeMove(sfWebRequest $request)
   {
       error_log("Move to called!") ;
		$application = $request->getParameter("id");
		$stage = $request->getParameter("stage");

		//if application is being moved to a decline stage then redirect to decline page to
				   //make the user enter a reason for previous decline
				$q = Doctrine_Query::create()
					 ->from('Buttons a')
					 ->where('a.link LIKE ?', "%decline2?moveto=".$stage."%");
				$buttons = $q->execute();
				foreach($buttons as $button)
				{
						$this->redirect("/backend.php/forms/decline2?moveto=".$stage."&entryid=".$application);
						exit;
				}

		$q = Doctrine_Query::create()
		 ->from('FormEntry a')
		 ->where('a.id = ?', $request->getParameter('id'));
	    $application = $q->fetchOne();

		$application->setApproved($stage);
		$application->save();

		$this->redirect("/backend.php/applications/view/id/".$application->getId());
   }
   /**
   * Executes 'Recall' action
   *
   * Sends the application from the rejection stage to the previous stage
   *
   * @param sfRequest $request A request object
   */
   public function executeRecall(sfWebRequest $request)
   {
           $q = Doctrine_Query::create()
		 ->from('FormEntry a')
		 ->where('a.id = ?', $request->getParameter('id'));
	    $application = $q->fetchOne();

            $q = Doctrine_Query::create()
                 ->from('ApplicationReference a')
                 ->where('a.application_id = ?', $request->getParameter('id'))
                 ->orderBy('a.id DESC');
            $refs = $q->execute();

            $moved = false;

            foreach($refs as $ref)
            {
                if($ref->getStageId() == $application->getApproved())
                {

                }
                else
                {
                        $application->setApproved($ref->getStageId());
                        $application->save();
                        $moved = true;
                        break;
                }
            }

            if($moved == false)
            {
                $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
                mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

                $query = "SELECT * FROM ap_column_preferences WHERE form_id = '".$application->getFormId()."'";
		$optrow = mysql_fetch_assoc(do_query($query));

                $application->setApproved($optrow['starting_point']);
                $application->save();
            }

            $this->redirect("/backend.php/applications/view/id/".$application->getId());
   }

    /**
     * Executes 'Viewjson' action
     *
     * Displays a single application and all of its review history
     *
     * @param sfRequest $request A request object
     */
    public function executeViewjson(sfWebRequest $request)
    {
        $this->executeView($request);
        $this->setLayout(false);
    }
    /**
     * OTB patch - Denial of service
     */
    public function executeDenial(sfWebRequest $request){
        
    }
      public function executeDownloadFile(sfWebRequest $request){
    
    
    $target_file = sfConfig::get("sf_data_dir")."/client_messages/".$request->getParameter('filename') ;
    $filename_only = $request->getParameter('filename') ;
   /* error_log("Target File is ".$target_file);
    $filename_only = $request->getParameter('filename') ;
    //
    $header_file = (strstr($_SERVER['HTTP_USER_AGENT'], 'MSIE')) ? preg_replace('/\./', '%2e', $filename_only, substr_count($filename_only, '.') - 1) : $filename_only;
      //Prepare headers
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: public", false);
    header("Content-Description: File Transfer");
    header("Content-Type: " . $type);
    header("Accept-Ranges: bytes");
    header("Content-Disposition: attachment; filename=\"" . addslashes($header_file) . "\"");
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($target_file));
    exit();*/
//error_log("Target File >>>" .$target_file) ;
    if(file_exists($target_file)){
    //prompt user to download the file
    

    // Get extension of requested file
    $extension = strtolower(substr(strrchr($filename_only, "."), 1));

    // Determine correct MIME type
    switch($extension){
        case "asf":     $type = "video/x-ms-asf";                break;
        case "avi":     $type = "video/x-msvideo";               break;
        case "bin":     $type = "application/octet-stream";      break;
        case "bmp":     $type = "image/bmp";                     break;
        case "cgi":     $type = "magnus-internal/cgi";           break;
        case "css":     $type = "text/css";                      break;
        case "dcr":     $type = "application/x-director";        break;
        case "dxr":     $type = "application/x-director";        break;
        case "dll":     $type = "application/octet-stream";      break;
        case "doc":     $type = "application/msword";            break;
        case "exe":     $type = "application/octet-stream";      break;
        case "gif":     $type = "image/gif";                     break;
        case "gtar":    $type = "application/x-gtar";            break;
        case "gz":      $type = "application/gzip";              break;
        case "htm":     $type = "text/html";                     break;
        case "html":    $type = "text/html";                     break;
        case "iso":     $type = "application/octet-stream";      break;
        case "jar":     $type = "application/java-archive";      break;
        case "java":    $type = "text/x-java-source";            break;
        case "jnlp":    $type = "application/x-java-jnlp-file";  break;
        case "js":      $type = "application/x-javascript";      break;
        case "jpg":     $type = "image/jpeg";                    break;
        case "jpe":     $type = "image/jpeg";                    break;
        case "jpeg":    $type = "image/jpeg";                    break;
        case "lzh":     $type = "application/octet-stream";      break;
        case "mdb":     $type = "application/mdb";               break;
        case "mid":     $type = "audio/x-midi";                  break;
        case "midi":    $type = "audio/x-midi";                  break;
        case "mov":     $type = "video/quicktime";               break;
        case "mp2":     $type = "audio/x-mpeg";                  break;
        case "mp3":     $type = "audio/mpeg";                    break;
        case "mpg":     $type = "video/mpeg";                    break;
        case "mpe":     $type = "video/mpeg";                    break;
        case "mpeg":    $type = "video/mpeg";                    break;
        case "pdf":     $type = "application/pdf";               break;
        case "php":     $type = "application/x-httpd-php";       break;
        case "php3":    $type = "application/x-httpd-php3";      break;
        case "php4":    $type = "application/x-httpd-php";       break;
        case "png":     $type = "image/png";                     break;
        case "ppt":     $type = "application/mspowerpoint";      break;
        case "qt":      $type = "video/quicktime";               break;
        case "qti":     $type = "image/x-quicktime";             break;
        case "rar":     $type = "encoding/x-compress";           break;
        case "ra":      $type = "audio/x-pn-realaudio";          break;
        case "rm":      $type = "audio/x-pn-realaudio";          break;
        case "ram":     $type = "audio/x-pn-realaudio";          break;
        case "rtf":     $type = "application/rtf";               break;
        case "swa":     $type = "application/x-director";        break;
        case "swf":     $type = "application/x-shockwave-flash"; break;
        case "tar":     $type = "application/x-tar";             break;
        case "tgz":     $type = "application/gzip";              break;
        case "tif":     $type = "image/tiff";                    break;
        case "tiff":    $type = "image/tiff";                    break;
        case "torrent": $type = "application/x-bittorrent";      break;
        case "txt":     $type = "text/plain";                    break;
        case "wav":     $type = "audio/wav";                     break;
        case "wma":     $type = "audio/x-ms-wma";                break;
        case "wmv":     $type = "video/x-ms-wmv";                break;
        case "xls":     $type = "application/vnd.ms-excel";      break;
        case "xml":     $type = "application/xml";               break;
        case "7z":      $type = "application/x-compress";        break;
        case "zip":     $type = "application/x-zip-compressed";  break;
        default:        $type = "application/force-download";    break;
    }

    // Fix IE bug [0]
    $header_file = (strstr($_SERVER['HTTP_USER_AGENT'], 'MSIE')) ? preg_replace('/\./', '%2e', $filename_only, substr_count($filename_only, '.') - 1) : $filename_only;

    //Prepare headers
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: public", false);
    header("Content-Description: File Transfer");
    header("Content-Type: " . $type);
    header("Accept-Ranges: bytes");
    header("Content-Disposition: attachment; filename=\"" . addslashes($header_file) . "\"");
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($target_file));

    // Send file for download
    if ($stream = fopen($target_file, 'rb')){
        while(!feof($stream) && connection_status() == 0){
            //reset time limit for big files
            @set_time_limit(0);
            print(fread($stream,1024*8));
            flush();
        }
        fclose($stream);
    }
    }else{
        error_log('File not found!');
    }
    exit();
  }

    /**
  * Executes 'View' action
  *
  * Displays a single application and all of its review history
  *
  * @param sfRequest $request A request object
  */
  public function executeView(sfWebRequest $request)
  {

     //OTB patch - Call helper
//     $otbhelper = new OTBHelper();
     //get parameter for warning incase user tries to move an application with unresolved issues
      $declinewarn = $request->getParameter("declinewarn");
      if($declinewarn == 1)
          {
          $this->getUser()->setFlash("decline_warning", "Decline Warning") ;
      }
     
    $this->getUser()->setAttribute("back_to_tasks", false);

		$this->apppage = $request->getParameter('apppage', 0);

		if($request->getPostParameter("fromdate"))
		{
			$this->fromdate = $request->getPostParameter("fromdate");
			$this->fromtime = $request->getPostParameter("fromtime");
			$this->todate = $request->getPostParameter("todate");
			$this->totime = $request->getPostParameter("totime");
		}

		if($request->getParameter("formid"))
		{
			$this->choosen_form = $request->getParameter("formid");
		}

	$q = Doctrine_Query::create()
		 ->from('FormEntry a')
		 ->where('a.id = ?', $request->getParameter('id'))
     ->limit(1);
	$this->application = $q->fetchOne();

	//If application has a duplicate application number then update the duplicate application number
	if($request->getParameter("refreshid"))
	{
		$q = Doctrine_Query::create()
			 ->from('FormEntry a')
			 ->where('a.application_id = ?', $this->application->getApplicationId())
	     ->limit(1);
         
        $found_draft = strpos($this->application->getApplicationId(), "Draft");
        
        if($found_draft === false)
        {
            $found_draft = false;
        }
        else 
        {
            $found_draft = true;
        }
         
		if($q->count() > 1 || $found_draft)
		{
			$application_manager = new ApplicationManager();
			$this->application->setApplicationId($application_manager->generate_application_number($this->application->getFormId()));
			$this->application->save();
		}
	}

	if($request->getParameter("confirmpayment") && $this->getUser()->mfHasCredential('approvepayments'))
	{
           
		foreach($this->application->getMfInvoice() as $invoice)
		{
                    error_log("Funny code ".$this->application->getFormId()."/".$this->application->getEntryId()."/".sizeof($this->application->getMfInvoice())) ;
			if(md5($invoice->getId()) == $request->getParameter("confirmpayment"))
			{
				$invoice->setPaid(2);
				$invoice->setUpdatedAt(date("Y-m-d"));
                                
				$invoice->save();
                                //OTB patch - Update 
                                $update = Doctrine_Query::create()
                                        ->update('ApFormPayments')
                                        ->set('payment_status','?','paid')
                                        ->set('status', '? ',2)
                                        ->where('record_id = ? ',$this->application->getEntryId() ) ;
                                $update->execute();
                                        

                                $application = $this->application;
                                if($application->getApproved() == 0) {
                                    $application_manager = new ApplicationManager();
                                    $application_manager->publish_draft($application->getId());
                                }
                                        
                                
                       }
		
                     
                }
	}


	if($request->getParameter("messages") == "read")
	{
	    $q = Doctrine_Query::create()
           ->from("Communications a")
           ->Where('a.messageread = ?', '0')
           ->andWhere('a.application_id = ?', $this->application->getId());
        $messages = $q->execute();
        foreach($messages as $message)
        {
            if($message->getReviewerId() == "")
            {
                $message->setMessageread("1");
                $message->save();
            }
        }

        $this->current_tab = "messages";
    }


	if($request->getPostParameter("txtmessage"))
	{
		//If the user is reply then mark the messages as read
		$q = Doctrine_Query::create()
				->from("Communications a")
				->Where('a.messageread = ?', '0')
				->andWhere('a.application_id = ?', $this->application->getId());
			$messages = $q->execute();
			foreach($messages as $message)
			{
					if($message->getReviewerId() == "")
					{
							$message->setMessageread("1");
							$message->save();
					}
			}

		$message = new Communications();
		$message->setReviewerId($_SESSION["SESSION_CUTEFLOW_USERID"]);
		$message->setMessageread("0");
		$message->setContent($request->getPostParameter("txtmessage"));
		$message->setApplicationId($this->application->getId());
		$message->setActionTimestamp(date('Y-m-d'));
		$message->save();

		$q = Doctrine_Query::create()
		   ->from("SfGuardUserProfile a")
		   ->where("a.user_id = ?", $this->application->getUserId());
		$user_profile = $q->fetchOne();

		$q = Doctrine_Query::create()
		   ->from("CfUser a")
		   ->where("a.nid = ?", $_SESSION["SESSION_CUTEFLOW_USERID"]);
		$reviewer = $q->fetchOne();

        $body = "
        Hi ".$user_profile->getFullname().",<br>
        <br>
        You have received a new message on ".$this->application->getApplicationId()." from ".$reviewer->getStrfirstname()." ".$reviewer->getStrlastname()." (".$reviewer->getStrdepartment()."):<br>
        <br><br>
        -------
        <br>
        ".$request->getPostParameter("txtmessage")."
        <br>
        <br>
        Click here to view the application: <br>
        ------- <br>
        <a href='http://".$_SERVER['HTTP_HOST']."/index.php/application/view/id/".$this->application->getId()."'>Link to ".$this->application->getApplicationId()."</a><br>
        ------- <br>

        <br>
        ";

		//$mailnotifications = new mailnotifications();
        //$mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $user_profile->getEmail(),"New Message",$body);
    }

    if($request->getPostParameter("txtmemo"))
	{
		//If the user is reply then mark the messages as read
		$q = Doctrine_Query::create()
				->from("Communications a")
				->Where('a.messageread = ?', '0')
				->andWhere('a.application_id = ?', $this->application->getId());
			$messages = $q->execute();
			foreach($messages as $message)
			{
					if($message->getReviewerId() == "")
					{
							$message->setMessageread("1");
							$message->save();
					}
			}

		$message = new Communication();
		$message->setSender($_SESSION["SESSION_CUTEFLOW_USERID"]);
		$message->setIsread("0");
		$message->setMessage($request->getPostParameter("txtmemo"));
		$message->setApplicationId($this->application->getId());
		$message->setCreatedOn(date('Y-m-d'));
		$message->save();

		$q = Doctrine_Query::create()
		   ->from("SfGuardUserProfile a")
		   ->where("a.user_id = ?", $this->application->getUserId());
		$user_profile = $q->fetchOne();

		$q = Doctrine_Query::create()
		   ->from("CfUser a")
		   ->where("a.nid = ?", $_SESSION["SESSION_CUTEFLOW_USERID"]);
		$reviewer = $q->fetchOne();

        $body = "
        Hi ".$user_profile->getFullname().",<br>
        <br>
        You have received a new message on ".$this->application->getApplicationId()." from ".$reviewer->getStrfirstname()." ".$reviewer->getStrlastname()." (".$reviewer->getStrdepartment()."):<br>
        <br><br>
        -------
        <br>
        ".$request->getPostParameter("txtmessage")."
        <br>
        <br>
        Click here to view the application: <br>
        ------- <br>
        <a href='http://".$_SERVER['HTTP_HOST']."/index.php/application/view/id/".$this->application->getId()."'>Link to ".$this->application->getApplicationId()."</a><br>
        ------- <br>

        <br>
        ";

		//$mailnotifications = new mailnotifications();
        //$mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $user_profile->getEmail(),"New Message",$body);
	}



        //Save Audit Log
        //$audit = new Audit();
        //$audit->saveAudit($this->application->getId(), "Viewed ".$this->application->getApplicationId());

      if($request->getParameter("done"))
      {
          $this->getUser()->setFlash('notice', 'Success! The application has been submitted');
      }

      if($request->getParameter("current_tab") == "application")
      {
          $this->current_tab = "application";
      }
      elseif($request->getParameter("current_tab") == "attachments")
      {
          $this->current_tab = "attachments";
      }
      elseif($request->getParameter("current_tab") == "user")
      {
          $this->current_tab = "user";
      }
      else if($request->getParameter("current_tab") == "review")
      {
          $this->current_tab = "review";
      }
      else if($request->getParameter("current_tab") == "billing")
      {
          $this->current_tab = "billing";
      }
      else if($request->getParameter("current_tab") == "history")
      {
          $this->current_tab = "history";
      }
      else if($request->getParameter("current_tab") == "messages")
      {
          $this->current_tab = "messages";
      }
      else if($request->getParameter("current_tab") == "memo")
      {
          $this->current_tab = "memo";
      }
      else
      {
          $this->current_tab = "application";
      }
      $this->setLayout('layout-metronic');

  }

  /**
  * Executes 'View' action
  *
  * Displays a single application and all of its review history
  *
  * @param sfRequest $request A request object
  */
  public function executeViewarchive(sfWebRequest $request)
  {
        $q = Doctrine_Query::create()
            ->from('FormEntryArchive a')
            ->where('a.id = ?', $request->getParameter('id'))
            ->limit(1);
        $this->application = $q->fetchOne();

        //Save Audit Log
        //$audit = new Audit();
        //$audit->saveAudit($this->application->getId(), "Viewed ".$this->application->getApplicationId());

      if($request->getParameter("done"))
      {
          $this->getUser()->setFlash('notice', 'Success! The application has been submitted');
      }

      if($request->getParameter("current_tab") == "application")
      {
          $this->current_tab = "application";
      }
      elseif($request->getParameter("current_tab") == "attachments")
      {
          $this->current_tab = "attachments";
      }
      elseif($request->getParameter("current_tab") == "user")
      {
          $this->current_tab = "user";
      }
      else if($request->getParameter("current_tab") == "review")
      {
          $this->current_tab = "review";
      }
      else if($request->getParameter("current_tab") == "billing")
      {
          $this->current_tab = "billing";
      }
      else if($request->getParameter("current_tab") == "history")
      {
          $this->current_tab = "history";
      }
      else if($request->getParameter("current_tab") == "messages")
      {
          $this->current_tab = "messages";
      }
      else if($request->getParameter("current_tab") == "memo")
      {
          $this->current_tab = "memo";
      }
      else
      {
          $this->current_tab = "application";
      }

  }


    /**
     * Executes 'Transfer' action
     *
     * Change Ownership of an Application
     *
     * @param sfRequest $request A request object
     */
    public function executeTransfer(sfWebRequest $request)
    {
       if ($this->getUser()->mfHasCredential("transfer_applications")) {      

          $application_id = $request->getParameter('id');
          $q = Doctrine_Query::create()
              ->from('FormEntry a')
              ->where('a.id = ?', $application_id);
          $this->application = $q->fetchOne();
          $this->transferform = new BackendTransferForm();
          $this->setLayout("layout-metronic");

        } else {
          $this->redirect('/backend.php/applications/view/id/'.$request->getParameter('id'));
        }        
    }


      /**
     * Executes 'Accepttransfer' action
     *
     * Cancel Change of Ownership of an Application
     *
     * @param sfRequest $request A request object
     */
    public function executeSuccessfulltransfer(sfWebRequest $request)
    {
        $transfer = $request->getPostParameter("transfer");
        $application_id = $request->getParameter('id');
        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.id = ?', $request->getParameter('id'));
        $this->application = $q->fetchOne();

        $q_u = Doctrine_Query::create()
           ->from("SfGuardUserProfile a")
           ->where("a.email = ?", $transfer['email']);

        $new_user = $q_u->fetchOne();

        $this->application->setUserId($new_user->getUserId());
        $this->application->setCirculationId("");
        $this->application->save();
        $this->getUser()->setFlash('notice', "The request for transfer of {$this->application->getApplicationId()} ownership to {$new_user->getFullname()} has been completed");

        // echo "The request for transfer of {$this->application->getApplicationId()} ownership to {$new_user->getFullname()} has been completed";
        exit();
        return $this->redirect("/backend.php/applications/view/id/".$application_id);
    }

    /**
     *  Execute Check Email
     * Check if the user to transfer the application to exists
     *
      */
    public function executeCheckuser(sfWebRequest $request)
    {
         $q = Doctrine_Query::create()
                   ->from("SfGuardUserProfile a")
                   ->where("a.email = ?",$request->getParameter('email'));

          $new_user = $q->fetchOne();
          if ($new_user) {
               echo 1;
               exit();
          } else {
               echo 0;
            exit();
          }

          exit();
    }

    /**
     * Executes 'Savetransfer' action
     *
     * Cancel Change of Ownership of an Application
     *
     * @param sfRequest $request A request object
     */
    public function executeSavetransfer(sfWebRequest $request)
    {
        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.id = ?', $request->getParameter('id'));
        $this->application = $q->fetchOne();

        $this->form = new BackendTransferForm();
        if($request->isMethod(sfRequest::POST))
        {
            $this->form->bind($request->getParameter($this->form->getName()));

            if ($this->form->isValid())
            {
                $transfer = $request->getPostParameter("transfer");
                $email = $transfer['email'];

                $q = Doctrine_Query::create()
                   ->from("SfGuardUserProfile a")
                   ->where("a.email = ?",$email);

                $new_user = $q->fetchOne();

                $q = Doctrine_Query::create()
                    ->from("SfGuardUserProfile a")
                    ->where("a.user_id = ?",$this->application->getUserId());

                $available_user = $q->fetchOne();

                if($new_user)
                {
                    $this->application->setCirculationId($new_user->getUserId());
                    $this->application->save();

                    $id = $this->application->getId();
                    $data = json_encode(array('id' => $id));
                    $encryptdata = base64_encode($data);

                    //Send email to previous owner for them to approve transfer
                    //Send account recovery email
                    $body = "
                Hi {$available_user->getFullname()}, <br>
                <br>
                A request to transfer your application '".$this->application->getApplicationId()."' to '".$new_user->getFullname()."' has been created.
                <br><br>
                Click the link below to Approve': <br>
                <br>
                <a href='http://".$_SERVER['HTTP_HOST']."/index.php/application/accepttransfer/code/".$encryptdata."'>ACCEPT TRANSFER</a>
                <br><br>
                ------- <br>
                <br>
                Or Click the link below to Reject:<br>
                <a href='http://".$_SERVER['HTTP_HOST']."/index.php/application/canceltransfer/code/".$encryptdata."'>REJECT TRANSFER</a>
                <br>
                <br>
                Thanks,<br>
                ".sfConfig::get('app_organisation_name').".<br>
            ";

                    $mailnotifications = new mailnotifications();
                    $mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $available_user->getEmail(),"Application Transfer",$body);

                    $this->getUser()->setFlash('notice', 'Success! A request for transfer has been sent to the user');
                    $this->redirect("/backend.php/applications/view/id/".$request->getParameter("id"));
                }
                else
                {
                    $data = json_encode(array('id' => $request->getParameter("id")));
                    $encryptdata = base64_encode($data);
                    $this->getUser()->setFlash('error', 'The user you specified does not exist');
                    $this->redirect("/backend.php/applications/transfer/id/".$encryptdata);
                }
            }
            else
            {
                $data = json_encode(array('id' => $request->getParameter("id")));
                $encryptdata = base64_encode($data);
                $this->getUser()->setFlash('error', 'There was a problem with your form');
                $this->redirect("/backend.php/applications/transfer/id/".$encryptdata);
            }
        }
    }




    /**
     * Executes 'Accepttransfer' action
     *
     * Cancel Change of Ownership of an Application
     *
     * @param sfRequest $request A request object
     */
    public function executeAccepttransfer(sfWebRequest $request)
    {
        $data = $request->getParameter('code');
        $data = json_decode(base64_decode($data), true);

        $application_id = $data['id'];

        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.id = ?', $application_id);
        $this->application = $q->fetchOne();

        $previous_owner = $this->application->getCirculationId();

        $this->application->setUserId($this->application->getCirculationId());
        $this->application->setCirculationId("");
        $this->application->save();

        $this->getUser()->setFlash('notice', 'The request for transfer of ownership has been completed');
				return $this->redirect("/backend.php/applications/view/id/".$application_id);
    }

    /**
     * Executes 'Canceltransfer' action
     *
     * Cancel Change of Ownership of an Application
     *
     * @param sfRequest $request A request object
     */
    public function executeCanceltransfer(sfWebRequest $request)
    {
        $data = $request->getParameter('code');
        $data = json_decode(base64_decode($data), true);

        $application_id = $data['id'];

        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.id = ?', $application_id);
        $this->application = $q->fetchOne();

        $this->application->setCirculationId("");
        $this->application->save();

        $this->getUser()->setFlash('notice', 'The request for transfer of ownership has been cancelled');
        return $this->redirect("/backend.php/applications/view/id/".$application_id);
    }


   /**
  * Executes 'Edit' action
  *
  * Displays a form for editing a submitted application (Possibly to make corrections or additions on the client's behalf)
  *
  * @param sfRequest $request A request object
  */
  public function executeEdit(sfWebRequest $request)
  {
      $this->setLayout("layout-metronic");
  }

   /**
  * Executes 'Togglecondition' action
  *
  * Toggles the conditions of approval for this application set during commenting/review in the task. #Probably belongs in the tasks actions.
  *
  * @param sfRequest $request A request object
  */
  public function executeTogglecondition(sfWebRequest $request)
  {
						$q = Doctrine_Query::create()
						   ->from('ApprovalCondition a')
						   ->where('a.entry_id = ?', $request->getParameter("appid"))
						   ->andWhere('a.condition_id = ?', $request->getParameter("id"));
						$condition = $q->fetchOne();
						if(empty($condition))
						{
							$condition = new ApprovalCondition();
							$condition->setEntryId($request->getParameter("appid"));
							$condition->setConditionId($request->getParameter("id"));
							$condition->save();
						}
						else
						{
							$condition->delete();
						}
						exit;
  }

  /**
  * Executes 'Search' action
  *
  * Toggles the conditions of approval for this application set during commenting/review in the task. #Probably belongs in the tasks actions.
  *
  * @param sfRequest $request A request object
  */
  public function executeSearch(sfWebRequest $request)
  {
		$q = Doctrine_Query::create()
		  ->from('ApForms a')
		  ->where('a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ?',array('6','7','15','16','17'))
		  ->orderBy('a.form_id ASC');
        $this->forms = $q->execute();
        //
        $this->setLayout('layout-metronic');
  }

   /**
  * Executes 'Editcolumns' action
  *
  * Displays form to allow the user to set configure a custom layout for the applications list
  *
  * @param sfRequest $request A request object
  */
  public function executeEditcolumns(sfWebRequest $request)
  {
	  if($request->isMethod(sfRequest::POST))
	  {
		  $q = Doctrine_Query::create()
		  ->from('ApForms a')
		  ->where('a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ?',array('6','7','15','16','17'))
		  ->orderBy('a.form_name ASC');
		$forms = $q->execute();

		foreach($forms as $form)
		{
		    //delete existing
			$q = Doctrine_Query::create()
				 ->from('ApplicationOverviewColumns a')
				 ->where('a.form_id = ?', $form->getFormId());
			$existing = $q->execute();
			foreach($existing as $field)
			{
				$field->delete();
			}

			$fields = $request->getPostParameter("fields_".$form->getFormId());
			$headers = $request->getPostParameter("headers_".$form->getFormId());
			$count = 0;
			foreach($fields as $field)
			{
				$reportfield = new ApplicationOverviewColumns();
				$reportfield->setFormId($form->getFormId());
				$reportfield->setElementId($field);
				$reportfield->setCustomHeader($headers[$count]);
				$reportfield->setPosition($count++);
				$reportfield->save();
			}
		}
	  }
  }

   /**
  * Executes 'Sendnotification' action
  *
  * Allows the current user to send a notification and customize the message if neccesary
  *
  * @param sfRequest $request A request object
  */
  public function executeSendnotification(sfWebRequest $request)
  {

		$q = Doctrine_Query::create()
			 ->from('FormEntry a')
			 ->where('a.id = ?', $request->getParameter('application'));
		$this->application = $q->fetchOne();
  }

   /**
  * Executes 'Notificationmail' action
  *
  * Sends notification to the user
  *
  * @param sfRequest $request A request object
  */
  public function executeNotificationmail(sfWebRequest $request)
  {

		$q = Doctrine_Query::create()
			 ->from('FormEntry a')
			 ->where('a.id = ?', $request->getParameter('application'));
		$this->application = $q->fetchOne();



		$notificationmng = new mailnotifications();
		if($request->getPostParameter("email"))
		{
		$notificationmng->sendemail("CCN",$request->getPostParameter("email"),$request->getPostParameter("subject"),$request->getPostParameter("mail"));
		}
		if($request->getPostParameter("phone"))
		{
		$notificationmng->sendsms($request->getPostParameter("phone"), $request->getPostParameter("sms"));
		}
		$this->redirect('/backend.php/applications/view/id/'.$this->application->getId());
  }

  /**
  * Executes 'Print' action
  *
  * Print the application details to a PDF
  *
  * @param sfRequest $request A request object
  */
  public function executePrint(sfWebRequest $request)
  {
	$q = Doctrine_Query::create()
		 ->from('FormEntry a')
		 ->where('a.id = ?', $request->getParameter('id'));
	$application = $q->fetchOne();

	if($request->getParameter("messages") == "read")
	{
	    $q = Doctrine_Query::create()
           ->from("Communications a")
           ->Where('a.messageread = ?', '0')
           ->andWhere('a.application_id = ?', $this->application->getId());
        $messages = $q->execute();
        foreach($messages as $message)
        {
            if($message->getReviewerId() == "")
            {
                $message->setMessageread("1");
                $message->save();
            }
        }
    }


	if($request->getPostParameter("txtmessage"))
	{
		$message = new Communications();
		$message->setReviewerId($_SESSION["SESSION_CUTEFLOW_USERID"]);
		$message->setMessageread("0");
		$message->setContent($request->getPostParameter("txtmessage"));
		$message->setApplicationId($this->application->getId());
		$message->setActionTimestamp(date('Y-m-d'));
		$message->save();
	}


require_once('/var/www/html/tcpdf/config/lang/eng.php');
require_once('/var/www/html/tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);



    $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));

	mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);



	//Check for duplicate application ids and change the lastest (recursive)

	$duplicates = false;

	$sql = "SELECT application_id,

			 COUNT(application_id) AS noduplicates

			FROM form_entry

			GROUP BY application_id

			HAVING ( COUNT(application_id) > 1 );";

	$rows = mysql_query($sql);

	if(mysql_num_rows($rows) > 0)

	{

		$duplicates = true;

	}

	while($duplicates == true)

	{

		$sql = "SELECT application_id,

			 COUNT(application_id) AS noduplicates

			FROM form_entry

			GROUP BY application_id

			HAVING ( COUNT(application_id) > 1 );";

		$rows = mysql_query($sql);

		if(mysql_num_rows($rows) <= 0)

		{

			$duplicates = false;

		}

		else

		{

			while($row = mysql_fetch_assoc($rows))

			{

				$sql = "SELECT * FROM form_entry WHERE application_id = '".$row['application_id']."' ORDER BY id DESC";

				$lastrow = mysql_fetch_assoc(mysql_query($sql));

				//Try to increment until new application id without entries is found

				$app_id = $lastrow['application_id'];

				$new_app_id = ++$app_id;

				if($new_app_id == $app_id)

				{

					$new_app_id = $app_id++;

				}

				$exists = true;

				while($exists == true)

				{

					$sql = "SELECT * FROM form_entry WHERE application_id = '".$new_app_id."' ORDER BY id DESC";

					$existingrows = mysql_query($sql);

					if(mysql_num_rows($existingrows) > 0)

					{

						$prev_app_id = $new_app_id;

						$new_app_id = ++$new_app_id;

						if($prev_app_id == $new_app_id)

						{

							$new_app_id = $new_app_id++;

						}

					}

					else

					{

						$exists = false;

						//save this new_app_id

						$sql = "UPDATE form_entry SET application_id = '".$new_app_id."' WHERE id = ".$lastrow['id'];

						mysql_query($sql);

					}

				}

			}

		}

	}


	function find($needle, $haystack)

	{

		$pos = strpos($haystack, $needle);

		if($pos === false)

		{

			return false;

		}

		else

		{

			return true;

		}

	}





	function parse($form_id, $entry_id, $content, $identifier_type){





	    $sql = "SELECT * FROM ap_form_".$form_id." WHERE id = ".$entry_id;

		$app_results = do_query($sql);



		$apform = mysql_fetch_assoc($app_results);





		if(find('{fm_created_at}', $content))

		{

			if($identifier_type == "0" || $identifier_type == "2")

			{

				$content = str_replace('{fm_created_at}', substr($apform['date_created'], 0, 1), $content);

			}

			else

			{

				$content = str_replace('{fm_created_at}', substr($apform['date_created'], 0, 11), $content);

			}

		}



		if(find('{fm_updated_at}', $content))

		{

			if($identifier_type == "0" || $identifier_type == "2")

			{

				$content = str_replace('{fm_updated_at}', substr($apform['date_updated'], 0, 1), $content);

			}

			else

			{

				$content = str_replace('{fm_updated_at}', substr($apform['date_updated'], 0, 11), $content);

			}

		}



		$sql = "SELECT * FROM ap_form_elements WHERE form_id = ".$form_id;

		$app_form_elements = do_query($sql);



		while($element_row = mysql_fetch_assoc($app_form_elements))

		{

		    $childs = $element_row['element_total_child'];

			if($childs == 0)

			{

			    if(find('{fm_element_'.$element_row['element_id'].'}', $content))

				{

					if($identifier_type == "0" || $identifier_type == "2")

					{

						$content = str_replace('{fm_element_'.$element_row['element_id'].'}',substr($apform['element_'.$element_row['element_id']], 0, 1), $content);

					}

					else

					{

						$content = str_replace('{fm_element_'.$element_row['element_id'].'}',substr($apform['element_'.$element_row['element_id']], 0, 1), $content);

					}

				}

			}

			else

			{

			    if($element_row['element_type'] == "select")

				{

					if(find('{fm_element_'.$element_row['element_id'].'}', $content))

					{



						$sql = "SELECT * FROM ap_element_options WHERE form_id = ".$form_id." AND element_id = ".$element_row['element_id']." AND option_id = ".$apform['element_'.$element_row['element_id']];

						$options = do_query($sql);



						if(mysql_num_rows($options) > 0)

						{

							$option = mysql_fetch_assoc($options);

							if($identifier_type == "0" || $identifier_type == "2")

							{

								$content = str_replace('{fm_element_'.$element_row['element_id'].'}',substr($option['option'],0,1), $content);

							}

							else

							{

								$content = str_replace('{fm_element_'.$element_row['element_id'].'}',$option['option'], $content);

							}

						}

					}

				}

				else

				{

					for($x = 0; $x < ($childs + 1); $x++)

					{

						if(find('{fm_element_'.$element_row['element_id'].'}', $content))

						{

							if($identifier_type == "0" || $identifier_type == "2")

							{

								$content = str_replace('{fm_element_'.$element_row['element_id']."}",substr($apform['element_'.$element_row['element_id']."_".($x+1)],0,1), $content);

							}

							else

							{

								$content = str_replace('{fm_element_'.$element_row['element_id']."}",$apform['element_'.$element_row['element_id']."_".($x+1)], $content);

							}

						}

					}

				}

			}





		}

		return $content;

	}



		function parseFull($form_id, $entry_id, $content){





	    $sql = "SELECT * FROM ap_form_".$form_id." WHERE id = ".$entry_id;

		$app_results = do_query($sql);



		$apform = mysql_fetch_assoc($app_results);



		//Get Form Details (anything starting with fm_ )

		//fm_created_at, fm_updated_at.....fm_element_1





					if(find('{fm_created_at}', $content))

					{

						$content = str_replace('{fm_created_at}', substr($apform['date_created'], 0, 11), $content);

					}

					if(find('{fm_updated_at}', $content))

					{

						$content = str_replace('{fm_updated_at}', substr($apform['date_updated'], 0, 11), $content);

					}

					if(find('{current_date}', $content))

					{

						$content = str_replace('{current_date}',date('Y-m-d'), $content);

					}



				$q = Doctrine_Query::create()

				   ->from('apFormElements a')

				   ->where('a.form_id = ?', $form_id);



				$elements = $q->execute();



				foreach($elements as $element)

				{

					$childs = $element->getElementTotalChild();



					if($childs == 0)

					{

						if(find('{fm_element_'.$element->getElementId().'}', $content))

						{

							$content = str_replace('{fm_element_'.$element->getElementId().'}',$apform['element_'.$element->getElementId()], $content);

						}

					}

					else

					{



						if($element->getElementType() == "select")

						{

							if(find('{fm_element_'.$element->getElementId().'}', $content))

							{

								$opt_value = 0;

								if($apform['element_'.$element->getElementId()] == "0")

								{

									$opt_value++;

								}

								else

								{

									$opt_value = $apform['element_'.$element->getElementId()];

								}



								$q = Doctrine_Query::create()

								   ->from('ApElementOptions a')

								   ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($form_id,$element->getElementId(),$opt_value));

								$option = $q->fetchOne();





								if($option)

								{

									$content = str_replace('{fm_element_'.$element->getElementId().'}',$option->getOption(), $content);

								}

							}

						}

						else

						{

							for($x = 0; $x < ($childs + 1); $x++)

							{

								if(find('{fm_element_'.$element->getElementId().'}', $content))

								{

									$content = str_replace('{fm_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_".($x+1)], $content);

								}

							}





							if(find('{fm_element_'.$element->getElementId().'}', $content))

							{

								$content = str_replace('{fm_element_'.$element->getElementId().'}',$apform['element_'.$element->getElementId()], $content);

							}



						}









					}





				}

		return $content;

	}



    $prefix_folder = dirname(__FILE__)."/../../../../..";



	require($prefix_folder.'/lib/vendor/cp_form/config.php');

	require($prefix_folder.'/lib/vendor/cp_form/includes/check-session.php');

	require($prefix_folder.'/lib/vendor/cp_form/includes/db-core.php');

	require($prefix_folder.'/lib/vendor/cp_form/includes/db-functions.php');

	require($prefix_folder.'/lib/vendor/cp_form/includes/helper-functions.php');

	require($prefix_folder.'/lib/vendor/cp_form/includes/entry-functions.php');



    require_once $prefix_folder.'/lib/vendor/cp_workflow/config/config.inc.php';

	require_once $prefix_folder.'/lib/vendor/cp_workflow/language_files/language.inc.php';

	require_once $prefix_folder.'/lib/vendor/cp_workflow/pages/CCirculation.inc.php';

	require_once $prefix_folder.'/lib/vendor/cp_workflow/pages/version.inc.php';





	connect_db();



	function GetDays($sStartDate, $sEndDate){

		$aDays[] = $start_date;

		$start_date  = $sStartDate;

		$end_date = $sEndDate;

		$current_date = $start_date;

		while(strtotime($current_date) <= strtotime($end_date))

		{

			$aDays[] = gmdate("Y-m-d", strtotime("+1 day", strtotime($current_date)));

			$current_date = gmdate("Y-m-d", strtotime("+2 day", strtotime($current_date)));

		}





	  return $aDays;

	}



	$form_id  = $application->getFormId();

	$entry_id = $application->getEntryId();





	//get form name

	$query = "select form_name from `ap_forms` where form_id='$form_id'";

	$result = do_query($query);

	$row = do_fetch_result($result);

	$form_name = $row['form_name'];



	$entry_details = get_details_only($form_id,$entry_id);

	$document_details = get_document_details($form_id,$entry_id);

	$drawing_details = get_drawing_details($form_id,$entry_id);



	//get entry timestamp

	$query = "select date_created,date_updated,ip_address from `ap_form_{$form_id}` where id='$entry_id'";

	$result = do_query($query);

	$row = do_fetch_result($result);



	$date_created = $row['date_created'];

	if(!empty($row['date_updated'])){

		$date_updated = $row['date_updated'];

	}else{

		$date_updated = '&nbsp;';

	}

	$ip_address   = $row['ip_address'];



	//get ids for navigation buttons

	//older entry id

	$result = do_query("select id from ap_form_{$form_id} where id < $entry_id order by id desc limit 1");

	$row = do_fetch_result($result);

	$older_entry_id = $row['id'];



	//oldest entry id

	$result = do_query("select id from ap_form_{$form_id} order by id asc limit 1");

	$row = do_fetch_result($result);

	$oldest_entry_id = $row['id'];



	//newer entry id

	$result = do_query("select id from ap_form_{$form_id} where id > $entry_id order by id asc limit 1");

	$row = do_fetch_result($result);

	$newer_entry_id = $row['id'];



	//newest entry id

	$result = do_query("select id from ap_form_{$form_id} order by id desc limit 1");

	$row = do_fetch_result($result);

	$newest_entry_id = $row['id'];



	if(($entry_id == $newest_entry_id) && ($entry_id == $oldest_entry_id)){

		$nav_position = 'disabled';

	}elseif($entry_id == $newest_entry_id){

		$nav_position = 'newest';

	}elseif ($entry_id == $oldest_entry_id){

		$nav_position = 'oldest';

	}else{

		$nav_position = 'middle';

	}





	function replaceLinks($value) {

		$linktext = preg_replace('/(([a-zA-Z]+:\/\/)([a-zA-Z0-9?&%.;:\/=+_-]*))/i', "<a href=\"$1\" target=\"_blank\">$1</a>", $value);

		return $linktext;

	}





//Check for identifier change

	$q = Doctrine_Query::create()

	  ->from('AppChange a')

	  ->where('a.stage_id = ? AND a.form_id = ?', array($application->getApproved(),$application->getFormId()));

	$appchange = $q->fetchOne();



if(!empty($appchange))

{

	$app_identifier = $appchange->getAppIdentifier();

	$identifier_type = $appchange->getIdentifierType();

	$identifier_start = $appchange->getIdentifierStart();



	$query = "SELECT * FROM form_entry WHERE application_id LIKE '%INV-%' AND id = ".$application->getId()."";



	$max_results = do_query($query);

	if(mysql_num_rows($max_results) > 0)

	{



    $pos = strpos($application->getFormId(),$app_identifier); //If this application already has the identifier then skip





                    // string needle found in haystack



			if($identifier_type == "0") //Pick First Letter of Field, Increment

			{

				$app_identifier = parse($application->getFormId(), $application->getEntryId(), $app_identifier, $identifier_type);



				$new_app_id = $app_identifier;



				//Get the last form entry record

				$query = "SELECT * FROM form_entry WHERE application_id LIKE '%".$app_identifier."%' ORDER BY application_id DESC LIMIT 1";

				$max_results = do_query($query);

				if(mysql_num_rows($max_results) > 0)

				{

					$last_row = mysql_fetch_assoc($max_results);

					$last_id = $last_row['application_id'];

					$new_app_id = ++$last_id;

				}

				else

				{

					$new_app_id = $new_app_id.$identifier_start;

				}

			}

			if($identifier_type == "1") //Pick Whole Field, Increment

			{

				$app_identifier = parseFull($formentry->getFormId(), $formentry->getEntryId(), $app_identifier);

				$new_app_id = $app_identifier.$identifier_start;

				$new_app_id = ++ $new_app_id;

			}

			if($identifier_type == "2") //Pick First Letter of Field, Don't Increment

			{

				$app_identifier = parse($formentry->getFormId(), $formentry->getEntryId(), $app_identifier, $identifier_type);

				$new_app_id = $app_identifier.$identifier_start;

			}

			if($identifier_type == "3") //Pick Whole Field, Don't Increment

			{

				$app_identifier = parseFull($formentry->getFormId(), $formentry->getEntryId(), $app_identifier);

				$new_app_id = $app_identifier;

			}



			$application->setApplicationId($new_app_id);

			$application->save();



	}

}




// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('www.kcps.gov.rw');
$pdf->SetTitle('Application CheckList');
$pdf->SetSubject('Application CheckList');

// set default header data

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 0, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage();

/* NOTE:
 * *********************************************************
 * You can load external XHTML using :
 *
 * $html = file_get_contents('/path/to/your/file.html');
 *
 * External CSS files will be automatically loaded.
 * Sometimes you need to fix the path of the external CSS.
 * *********************************************************
 */

// define some HTML content with style
$html = "";

$html .'
    <style>

	</style>
';



$html .= ' <br> <br><div class="g12" style="padding-left: 3px;">



							<form id="form" action="#" method="post" autocomplete="off" data-ajax="false">

								';


							$q = Doctrine_Query::create()

								 ->from('ApForms a')

								 ->where('a.form_id = ?', $application->getFormId());

							$form = $q->fetchOne();

							$formtype = $form->getFormName();

			$html .= '<label><h1>'.$formtype.' Details</h1></label><ul>';


									$toggle = false;



									foreach ($entry_details as $data){

										if(strlen($data['element_type'] == "section"))

										{

							$html .= '

								<li style="margin-bottom: 5px;">

									<label for="text_field" style="font-weight: 900; width: 100%;"><b>'.$data['label'].'</b></label><br><br>

								</li>
                                ';


										}

										else

										{

							$html .= '

								<li style="margin-bottom: 5px;">

									<label for="text_field" style="font-weight: 900;"><b>'.$data['label'].': </b></label>

									<br>';

									if($data['value']){ $html .= nl2br($data['value']); }else{ $html .= "-"; }

									$html .= '

								</li>';


										}



							}

							$html .= '

								</ul>



								<ul>

									<li style="margin-bottom: 5px;">

									<label for="text_field" style="font-weight: 900; width: 100%;"><b>Attached Documents</b>

									</li>
									';


									$toggle = false;



									foreach ($document_details as $data){

							$html .= '

								<li style="margin-bottom: 5px;">

									<label for="text_field" style="font-weight: 900;"><b>'.$data['label'].'</b></label>

									<br>';
									if($data['value']){ $html .= nl2br($data['value']); }else{ $html .= "-"; }

									$html .= '

								</li>';

							}

							$html .= '

                             ';


									$toggle = false;



									foreach ($drawing_details as $data){

							$html .= '

								<li style="margin-bottom: 5px;">

									<label for="text_field" style="font-weight: 900;"><b>'.$data['label'].'</b></label>

									<br>';

									if($data['value']){ $html .= nl2br($data['value']); }else{ $html .= "-"; }

									$html .= '

								</li>';

							 }

								$html .= '</ul>


					';


						$q = Doctrine_Query::create()

						   ->from("FormEntryLinks a")

						   ->where("a.formentryid = ?", $application->getId());

						$links = $q->execute();

						foreach($links as $link)

						{

							$q = Doctrine_Query::create()

							   ->from("SfGuardUserCategoriesForms a")

							   ->where("a.formid = ?", $link->getFormId());

							$sharedform = $q->fetchOne();

							if($sharedform)

							{

								$form_id  = $link->getFormId();

								$entry_id = $link->getEntryId();





								//get form name

								$query = "select form_name from `ap_forms` where form_id='$form_id'";

								$result = do_query($query);

								$row = do_fetch_result($result);

								$form_name = $row['form_name'];



								$entry_details = get_details_only($form_id,$entry_id);

								$document_details = get_document_details($form_id,$entry_id);

								$drawing_details = get_drawing_details($form_id,$entry_id);



								//get entry timestamp

								$query = "select date_created,date_updated,ip_address from `ap_form_{$form_id}` where id='$entry_id'";

								$result = do_query($query);

								$row = do_fetch_result($result);



								$date_created = $row['date_created'];

								if(!empty($row['date_updated'])){

									$date_updated = $row['date_updated'];

								}else{

									$date_updated = '&nbsp;';

								}

								$ip_address   = $row['ip_address'];



								//get ids for navigation buttons

								//older entry id

								$result = do_query("select id from ap_form_{$form_id} where id < $entry_id order by id desc limit 1");

								$row = do_fetch_result($result);

								$older_entry_id = $row['id'];



								//oldest entry id

								$result = do_query("select id from ap_form_{$form_id} order by id asc limit 1");

								$row = do_fetch_result($result);

								$oldest_entry_id = $row['id'];



								//newer entry id

								$result = do_query("select id from ap_form_{$form_id} where id > $entry_id order by id asc limit 1");

								$row = do_fetch_result($result);

								$newer_entry_id = $row['id'];



								//newest entry id

								$result = do_query("select id from ap_form_{$form_id} order by id desc limit 1");

								$row = do_fetch_result($result);

								$newest_entry_id = $row['id'];



								if(($entry_id == $newest_entry_id) && ($entry_id == $oldest_entry_id)){

									$nav_position = 'disabled';

								}elseif($entry_id == $newest_entry_id){

									$nav_position = 'newest';

								}elseif ($entry_id == $oldest_entry_id){

									$nav_position = 'oldest';

								}else{

									$nav_position = 'middle';

								}

								$html .= '



                                    <ul>';

                                $q = Doctrine_Query::create()

                                     ->from('ApForms a')

                                     ->where('a.form_id = ?', $link->getFormId());

                                $form = $q->fetchOne();

                                $formtype = $form->getFormName();

                                $html .= '<li><label><h2>'.$formtype.'Details</h2></label></li>';

                                        $toggle = false;



                                        foreach ($entry_details as $data){

                                            if(strlen($data['element_type'] == "section"))

                                            {

                               $html .= '

                                   <li style="margin-bottom: 5px;">

                                        <label for="text_field" style="font-weight: 900; width: 100%;"><b>'.$data['label'].'<b></label>

                                    </li>';

                                            }
                                            else
                                            {


                                    $html .= '
                                    <li style="margin-bottom: 5px;">

                                        <label for="text_field" style="font-weight: 900;"><b>'.$data['label'].'</b></label>

                                        <br>';

										 if($data['value']){ $html .= nl2br($data['value']); }else{ $html .= "-"; }

										$html .= '

                                    </li>';


											}

										}

									$html .= '

									</li>


								<ul>

									<li><label><h2>Submitted By</h2></label></li>
									';


										$q = Doctrine_Query::create()

											 ->from('SfGuardUserProfile a')

											 ->where('a.user_id = ?', $link->getUserId());

										$user_profile = $q->fetchOne();

								$html .= '

								<li style="margin-bottom: 5px;">

									<label for="text_field" style="font-weight: 900;"><b>Name</b></label>

									<br>'.$user_profile->getFullname().'

								</li>

								<li style="margin-bottom: 5px;">

									<label for="text_field" style="font-weight: 900;"><b>Email</b></label>

									<br>'.$user_profile->getEmail().'

								</li>';


									$q = Doctrine_Query::create()

										  ->from('mfUserProfile a')

										  ->where('a.user_id = ?', $link->getUserId());

									$profile = $q->fetchOne();



									if($profile)

									{



									$form_id = $profile->getFormId();

									$entry_id = $profile->getEntryId();



									//get form name

									$query = "select form_name from `ap_forms` where form_id='$form_id'";

									$result = do_query($query);

									$row = do_fetch_result($result);

									$form_name = $row['form_name'];



									$architect_details = get_entry_details($form_id,$entry_id);



									foreach ($architect_details as $data){

							$html .= '

								<li style="margin-bottom: 5px;">

									<label for="text_field" style="font-weight: 900;"><b>'.$data['label'].'</b></label>

									<br>';

									if($data['value']){ $html .= nl2br($data['value']); }else{ $html .= "-"; }

									$html .= '

								</li>';

							 }

								}

							$html .= '

								</ul>

							 ';

							}

						}

					$html .= '


								<ul>

									<li><label><h2>Submitted By</h2></label><br></li>

									';

										$q = Doctrine_Query::create()

											 ->from('SfGuardUserProfile a')

											 ->where('a.user_id = ?', $application->getUserId());

										$user_profile = $q->fetchOne();

								$html .= '

								<li style="margin-bottom: 5px;">

									<label for="text_field" style="font-weight: 900;"><b>Name</b></label>

									<br>'.$user_profile->getFullname().'

								</li>

								<li style="margin-bottom: 5px;">

									<label for="text_field" style="font-weight: 900;"><b>Email</b></label>

									<br>'.$user_profile->getEmail().'

								</li>';




									$q = Doctrine_Query::create()

										  ->from('mfUserProfile a')

										  ->where('a.user_id = ?', $application->getUserId());

									$profile = $q->fetchOne();



									if($profile)

									{



									$form_id = $profile->getFormId();

									$entry_id = $profile->getEntryId();



									//get form name

									$query = "select form_name from `ap_forms` where form_id='$form_id'";

									$result = do_query($query);

									$row = do_fetch_result($result);

									$form_name = $row['form_name'];



									$architect_details = get_entry_details($form_id,$entry_id);



									foreach ($architect_details as $data){

							$html .= '

								<li style="margin-bottom: 5px;">

									<label for="text_field" style="font-weight: 900;"><b>'.$data['label'].'</b></label>

									<br>';

									if($data['value']){ $html .= nl2br($data['value']); }else{ $html .= "-"; }

									$html .= '

								</li>';

							 }

								}

						$html .= '

								</ul>




</div>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// *******************************************************************
// HTML TIPS & TRICKS
// *******************************************************************

// REMOVE CELL PADDING
//
// $pdf->SetCellPadding(0);
//
// This is used to remove any additional vertical space inside a
// single cell of text.

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// REMOVE TAG TOP AND BOTTOM MARGINS
//
// $tagvs = array('p' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)));
// $pdf->setHtmlVSpace($tagvs);
//
// Since the CSS margin command is not yet implemented on TCPDF, you
// need to set the spacing of block tags using the following method.

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// SET LINE HEIGHT
//
// $pdf->setCellHeightRatio(1.25);
//
// You can use the following method to fine tune the line height
// (the number is a percentage relative to font height).

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// CHANGE THE PIXEL CONVERSION RATIO
//
// $pdf->setImageScale(0.47);
//
// This is used to adjust the conversion ratio between pixels and
// document units. Increase the value to get smaller objects.
// Since you are using pixel unit, this method is important to set the
// right zoom factor.
//
// Suppose that you want to print a web page larger 1024 pixels to
// fill all the available page width.
// An A4 page is larger 210mm equivalent to 8.268 inches, if you
// subtract 13mm (0.512") of margins for each side, the remaining
// space is 184mm (7.244 inches).
// The default resolution for a PDF document is 300 DPI (dots per
// inch), so you have 7.244 * 300 = 2173.2 dots (this is the maximum
// number of points you can print at 300 DPI for the given width).
// The conversion ratio is approximatively 1024 / 2173.2 = 0.47 px/dots
// If the web page is larger 1280 pixels, on the same A4 page the
// conversion ratio to use is 1280 / 2173.2 = 0.59 pixels/dots

// *******************************************************************

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
 return $pdf->Output('checklist.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+


  }

  /**
  * Executes 'Viewpermit' action
  *
  * Displays the generated permit that is attached to an application
  *
  * @param sfRequest $request A request object
  */
  public function executeViewpermit(sfWebRequest $request)
  {
	 $q = Doctrine_Query::create()
		 ->from('SavedPermit a')
		 ->where('a.id = ?', $request->getParameter('id'));
	 $savedpermit = $q->fetchOne();

	 if($savedpermit)
	 {
           $permit_manager = new PermitManager();
           $permit_manager->save_to_pdf($savedpermit->getId());
	 }
     else
     {
       echo "Invalid Permit Link";
     }

	 exit;

  }

  /**
  * Executes 'Viewpermit' action
  *
  * Displays the generated permit that is attached to an application
  *
  * @param sfRequest $request A request object
  */
  public function executeViewpermitarchive(sfWebRequest $request)
  {
	 $q = Doctrine_Query::create()
		 ->from('SavedPermitArchive a')
		 ->where('a.id = ?', $request->getParameter('id'));
	 $savedpermit = $q->fetchOne();

	 if($savedpermit)
	 {
           $permit_manager = new PermitManager();
           $permit_manager->save_archive_to_pdf($savedpermit->getId());
	 }
     else
     {
       echo "Invalid Permit Link";
     }

	 exit;

  }

  /**
  * Executes 'Savepermit' action
  *
  * Saves an edited permit that is attached to an application
  *
  * @param sfRequest $request A request object
  */
  public function executeSavepermit(sfWebRequest $request)
  {
		$q = Doctrine_Query::create()
		 ->from('FormEntry a')
		 ->where('a.id = ?', $request->getParameter('id'));
		$application = $q->fetchOne();

		$application->setSavedPermit($request->getPostParameter("permit"));
		$application->save();

		$this->redirect("/backend.php/applications/view/id/".$application->getId());
  }

   /**
  * Executes 'Editpermit' action
  *
  * Displays a form for editing any generated permits attached to an application
  *
  * @param sfRequest $request A request object
  */
  public function executeEditpermit(sfWebRequest $request)
  {
	$q = Doctrine_Query::create()
		 ->from('FormEntry a')
		 ->where('a.id = ?', $request->getParameter('id'));
	$this->application = $q->fetchOne();

	if($request->getParameter("reset") != "")
	{
		$this->reset = $request->getParameter("reset");
	}


	if($request->getPostParameter("txtmessage"))
	{
		$message = new Communications();
		$message->setReviewerId($_SESSION["SESSION_CUTEFLOW_USERID"]);
		$message->setMessageread("0");
		$message->setContent($request->getPostParameter("txtmessage"));
		$message->setApplicationId($this->application->getId());
		$message->setActionTimestamp(date('Y-m-d'));
		$message->save();
	}

  }

   /**
  * Executes 'Editpermit' action
  *
  * Displays a form for editing any generated permits attached to an application
  *
  * @param sfRequest $request A request object
  */
  public function executeUpdatepermit(sfWebRequest $request)
  {
    
    echo "Have Seen You Guys";
  }

   /**
  * Executes 'Ajaxlist' action
  *
  * Displays the default application list format (_list.php partial)
  *
  * @param sfRequest $request A request object
  */
  public function executeAjaxlist(sfWebRequest $request)
  {
		$this->setLayout(false);
  }

  /**
   * Executes 'Formsearch' action
   *
   * Fetch search filters for selected application form
   *
   * @param sfRequest $request A request object
   */
   public function executeFormsearch(sfWebRequest $request)
   {
		$this->setLayout(false);
   }

    /**
     * Executes 'Messages' action
     *
     * Show a list of messages
     *
     * @param sfRequest $request A request object
     */
    public function executeMessages(sfWebRequest $request)
    {
        //OTB patch
        $agency_manager = new AgencyManager();
         $stage_ids = $agency_manager->getAllowedStages($_SESSION['SESSION_CUTEFLOW_USERID']);
				if($request->getParameter("mark"))
				{
					$q = Doctrine_Query::create()
							->from('Communications a')
							->where('a.id = ?', $request->getParameter('mark'));
					$message = $q->fetchOne();

					if($message)
					{
						$message->setMessageread(1);
						$message->save();
					}
				}

        $q = Doctrine_Query::create()
            ->from('Communications a')
            ->leftJoin('a.FormEntry b')
           // ->where('b.approved <> 0')
                ->where('b.approved in ('. implode(',', $stage_ids).') ') //OTB patch
            ->andWhere('a.architect_id <> ?', "")
            ->andWhere('a.messageread = ?', 0)
            ->orderBy('a.id DESC');
        $this->pager = new sfDoctrinePager('Communications', 10);
        $this->pager->setQuery($q);
       $this->pager->setPage($request->getParameter('page', 1));
        $this->pager->init();
        //
        $this->setLayout('layout-metronic') ;
    }

    /**
     * Executes 'Notifications' action
     *
     * Show a list of notifications
     *
     * @param sfRequest $request A request object
     */
    public function executeNotifications(sfWebRequest $request)
    {
        $q = Doctrine_Query::create()
            ->from("Alerts a")
            ->where("a.userid = ?", $this->getUser()->getAttribute('userid'))
            ->andWhere("a.isread = ?", "0")
            ->orderBy("a.id DESC");
        $this->pager = new sfDoctrinePager('Alerts', 10);
        $this->pager->setQuery($q);
        $this->pager->setPage($request->getParameter('page', 1));
        $this->pager->init();
    }

    /**
     * Executes 'viewNotificaiton' action
     *
     * Mark a notification as read and redirect to the link
     *
     * @param sfRequest $request A request object
     */
    public function executeViewnotification(sfWebRequest $request)
    {
        $q = Doctrine_Query::create()
            ->from("Alerts a")
            ->where("a.userid = ?", $this->getUser()->getAttribute('userid'))
            ->andWhere("a.id = ?", $request->getParameter('id'))
            ->orderBy("a.id DESC");
        $alert = $q->fetchOne();
        if($alert)
        {
            $alert->setIsread(1);
            $alert->save();
            $this->redirect($alert->getLink());
        }
        else
        {
            $this->redirect("/backend.php/applications/notifications");
        }
    }
    /**
     * Toggle decline called via ajax
     * 
     */
    public function executeToggledecline(sfWebRequest $request) {
        $entry_id = $request->getParameter("id");
        error_log("Entry decline ID >>>>" . $entry_id);
        $resolved = $request->getParameter("resolved");
        error_log("Resolved Id >>>>" . $resolved);
        $q = Doctrine_Query::create()
                ->update("EntryDecline d")
                ->set("d.resolved ", "?", $resolved)
                ->where("d.id = ? ", $entry_id);
        $q->execute();
        exit();
    }

    /**
     * OTB patch - controller to save special conditions for an application
     */
   //OTB Start - Allow authorized users to send any stage (Previously possible in old system)
	/**
   * Executes 'Move' action
   *
   * Moves an application directly to any stage
   *
   * @param sfRequest $request A request object
   */
   public function executeMoveapp(sfWebRequest $request)
   {
	   $this->forward404Unless($this->getUser()->mfHasCredential('access_settings') or $this->getUser()->mfHasCredential('can_move_to_any_stage'));
		$application = $request->getParameter("id");
		$stage = $request->getParameter("stage");

		$q = Doctrine_Query::create()
		 ->from('FormEntry a')
		 ->where('a.id = ?', $request->getParameter('id'));
	    $application = $q->fetchOne();
		$sub_menu=Doctrine_Core::getTable('SubMenus')->find($stage);
		//Check the stage the app is been moved to
		switch($sub_menu->getStageType()){
			case 5:
				$this->redirect('/backend.php/forms/decline2?moveto='.$stage.'&entryid='.$request->getParameter('id'));
				break;
			case 6:
				$this->redirect('/backend.php/forms/reject?moveto='.$stage.'&entryid='.$request->getParameter('id'));
				break;
			case 10:
				//Check conditions
				$q=Doctrine_Query::create()
					->from('ApprovalCondition a')
					->where('a.entry_id = ?',$request->getParameter('id'));
				$count_conditions=$q->count();
				error_log('----Conditions--'.$count_conditions);
				if($count_conditions <= 0){
					$this->getUser()->setFlash('notice','Application has no Conditions of approval set! Agenda stage requires Conditions to be set.');
					$this->redirect("/backend.php/applications/view/id/".$request->getParameter('id')."/current_tab/review");
				}
				$application->setApproved($stage);
				break;
			default:
				/*$application->setDeclined(0);
				$application->setApproved($stage);*/
				$this->redirect('/backend.php/forms/viewentry?moveto='.$stage.'&entryid='.$request->getParameter('id'));
		}
		$application->save();
		$this->getUser()->setFlash('notice', 'The application <span>'.$application->getApplicationId().'</span> has been Successfully moved and is now in the <span>'.$sub_menu->getTitle().'</span> stage' );
		$this->redirect("/backend.php/applications/view/id/".$application->getId());
   }
   //OTB End - Allow authorized users to send any stage (Previously possible in old system)
}
