<?php
 
class applicationsComponents extends sfComponents
{
    public function executeAppajaxview()
    {
     //OTB Start - Force resolving of decline comments
		if($request->getParameter("decline"))
		{
		  $this->decline_warning = true;
		}
		//OTB End - Force resolving of decline comments
    $this->getUser()->setAttribute("back_to_tasks", false);

		$this->apppage = $request->getParameter('apppage', 0);

		if($request->getPostParameter("fromdate"))
		{
			$this->fromdate = $request->getPostParameter("fromdate");
			$this->fromtime = $request->getPostParameter("fromtime");
			$this->todate = $request->getPostParameter("todate");
			$this->totime = $request->getPostParameter("totime");
		}

		if($request->getParameter("formid"))
		{
			$this->choosen_form = $request->getParameter("formid");
		}

	//$agency_manager = new AgencyManager();//OTB Patch - Multiagency fucntionality, application viewing check
	//if($agency_manager->checkAgencyApplicationAccess($this->getUser()->getAttribute('userid'), $request->getParameter('id')) || $this->getUser()->mfHasCredential('access_apps_with_errors') )
    //    {//OTB Patch - Multiagency fucntionality, application viewing check
		$q = Doctrine_Query::create()
			 ->from('FormEntry a')
			 ->where('a.id = ?', $request->getParameter('id'))
		 ->limit(1);
		$this->application = $q->fetchOne();
	//}//OTB Patch - Multiagency fucntionality, application viewing check
     //   else{
            //
         //   $this->redirect('/backend.php/applications/denial');
       // }

	//If application has a duplicate application number then update the duplicate application number
	if($request->getParameter("refreshid"))
	{
		$q = Doctrine_Query::create()
			 ->from('FormEntry a')
			 ->where('a.application_id = ?', $this->application->getApplicationId())
	     ->limit(1);
         
        $found_draft = strpos($this->application->getApplicationId(), "Draft");
        
        if($found_draft === false)
        {
            $found_draft = false;
        }
        else 
        {
            $found_draft = true;
        }
         
		if($q->count() > 1 || $found_draft)
		{
			$application_manager = new ApplicationManager();
			$this->application->setApplicationId($application_manager->generate_application_number($this->application->getFormId()));
			$this->application->save();
		}
	}

	if($request->getParameter("confirmpayment") && $this->getUser()->mfHasCredential('approvepaymentsupport'))
	{
		foreach($this->application->getMfInvoice() as $invoice)
		{
			if(md5($invoice->getId()) == $request->getParameter("confirmpayment"))
			{
				$invoice->setPaid(2);
				$invoice->setUpdatedAt(date("Y-m-d"));
				$invoice->save();

                $application = $this->application;
                if($application->getApproved() == 0) {
                    $application_manager = new ApplicationManager();
                    $application_manager->publish_draft($application->getId());
                }
			}
		}
	}


	if($request->getParameter("messages") == "read")
	{
	    $q = Doctrine_Query::create()
           ->from("Communications a")
           ->Where('a.messageread = ?', '0')
           ->andWhere('a.application_id = ?', $this->application->getId());
        $messages = $q->execute();
        foreach($messages as $message)
        {
            if($message->getReviewerId() == "")
            {
                $message->setMessageread("1");
                $message->save();
            }
        }

        $this->current_tab = "messages";
    }


	if($request->getPostParameter("txtmessage"))
	{
		//If the user is reply then mark the messages as read
		$q = Doctrine_Query::create()
				->from("Communications a")
				->Where('a.messageread = ?', '0')
				->andWhere('a.application_id = ?', $this->application->getId());
			$messages = $q->execute();
			foreach($messages as $message)
			{
					if($message->getReviewerId() == "")
					{
							$message->setMessageread("1");
							$message->save();
					}
			}

		$message = new Communications();
		$message->setReviewerId($_SESSION["SESSION_CUTEFLOW_USERID"]);
		$message->setMessageread("0");
		$message->setContent($request->getPostParameter("txtmessage"));
		$message->setApplicationId($this->application->getId());
		$message->setActionTimestamp(date('Y-m-d'));
                //OTB patch
                $theFileName = null ;
            foreach($request->getFiles() as $fileName){
                             // error_log("File >>>>>> ".$fileName['name']);
                              $file_name = $fileName['name'] ;
                              $fileSize = $fileName['size'];
                              $fileType = $fileName['type'];
                              $theFileName = $fileName['name'];
                              $uploadDir = sfConfig::get("sf_data_dir");
                            //  $uploadDir =  __DIR__.'/../../html/asset_data' ;
                              //error_log("Upload DIR >>>>>> ".$uploadDir);
                              //error_log("File Name >>>>>> ".$theFileName);
                              $attachment_uploads = $uploadDir.'/client_messages';

                                  if(!is_dir($attachment_uploads))
                                      mkdir($attachment_uploads, 0777);
                                  //
                                  $temp = explode(".", $fileName['name']);
                                  $newfilename = round(microtime(true)) . '.' . end($temp);
                                //  error_log("New File >>>>>>>>>>>>>> ".$newfilename);
                                  move_uploaded_file($fileName['tmp_name'], "$attachment_uploads/$newfilename");

                                  //move_uploaded_file($fileName['tmp_name'], "$attachment_uploads/$theFileName");
                                  $theFileName = $newfilename ;
                          }
                          $message->setAttachment($theFileName);
		$message->save();

		$q = Doctrine_Query::create()
		   ->from("SfGuardUserProfile a")
		   ->where("a.user_id = ?", $this->application->getUserId());
		$user_profile = $q->fetchOne();

		$q = Doctrine_Query::create()
		   ->from("CfUser a")
		   ->where("a.nid = ?", $_SESSION["SESSION_CUTEFLOW_USERID"]);
		$reviewer = $q->fetchOne();

        $body = "
        Hi ".$user_profile->getFullname().",<br>
        <br>
        You have received a new message on ".$this->application->getApplicationId()." from ".$reviewer->getStrfirstname()." ".$reviewer->getStrlastname()." (".$reviewer->getStrdepartment()."):<br>
        <br><br>
        -------
        <br>
        ".$request->getPostParameter("txtmessage")."
        <br>
        <br>
        Click here to view the application: <br>
        ------- <br>
        <a href='http://".$_SERVER['HTTP_HOST']."/index.php/application/view/id/".$this->application->getId()."'>Link to ".$this->application->getApplicationId()."</a><br>
        ------- <br>

        <br>
        ";

		//$mailnotifications = new mailnotifications();
        //$mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $user_profile->getEmail(),"New Message",$body);
    }

    if($request->getPostParameter("txtmemo"))
	{
		//If the user is reply then mark the messages as read
		$q = Doctrine_Query::create()
				->from("Communications a")
				->Where('a.messageread = ?', '0')
				->andWhere('a.application_id = ?', $this->application->getId());
			$messages = $q->execute();
			foreach($messages as $message)
			{
					if($message->getReviewerId() == "")
					{
							$message->setMessageread("1");
							$message->save();
					}
			}

		$message = new Communication();
		$message->setSender($_SESSION["SESSION_CUTEFLOW_USERID"]);
		$message->setIsread("0");
		$message->setMessage($request->getPostParameter("txtmemo"));
		$message->setApplicationId($this->application->getId());
		$message->setCreatedOn(date('Y-m-d'));
		$message->save();

		$q = Doctrine_Query::create()
		   ->from("SfGuardUserProfile a")
		   ->where("a.user_id = ?", $this->application->getUserId());
		$user_profile = $q->fetchOne();

		$q = Doctrine_Query::create()
		   ->from("CfUser a")
		   ->where("a.nid = ?", $_SESSION["SESSION_CUTEFLOW_USERID"]);
		$reviewer = $q->fetchOne();

        $body = "
        Hi ".$user_profile->getFullname().",<br>
        <br>
        You have received a new message on ".$this->application->getApplicationId()." from ".$reviewer->getStrfirstname()." ".$reviewer->getStrlastname()." (".$reviewer->getStrdepartment()."):<br>
        <br><br>
        -------
        <br>
        ".$request->getPostParameter("txtmessage")."
        <br>
        <br>
        Click here to view the application: <br>
        ------- <br>
        <a href='http://".$_SERVER['HTTP_HOST']."/index.php/application/view/id/".$this->application->getId()."'>Link to ".$this->application->getApplicationId()."</a><br>
        ------- <br>

        <br>
        ";

		//$mailnotifications = new mailnotifications();
        //$mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $user_profile->getEmail(),"New Message",$body);
	}



        //Save Audit Log
        //$audit = new Audit();
        //$audit->saveAudit($this->application->getId(), "Viewed ".$this->application->getApplicationId());

      if($request->getParameter("done"))
      {
          $this->getUser()->setFlash('notice', 'Success! The application has been submitted');
      }

      if($request->getParameter("current_tab") == "application")
      {
          $this->current_tab = "application";
      }
      elseif($request->getParameter("current_tab") == "attachments")
      {
          $this->current_tab = "attachments";
      }
      elseif($request->getParameter("current_tab") == "user")
      {
          $this->current_tab = "user";
      }
      else if($request->getParameter("current_tab") == "review")
      {
          $this->current_tab = "review";
      }
      else if($request->getParameter("current_tab") == "billing")
      {
          $this->current_tab = "billing";
      }
      else if($request->getParameter("current_tab") == "history")
      {
          $this->current_tab = "history";
      }
      else if($request->getParameter("current_tab") == "messages")
      {
          $this->current_tab = "messages";
      }
      else if($request->getParameter("current_tab") == "memo")
      {
          $this->current_tab = "memo";
      }
      else
      {
          $this->current_tab = "application";
      }
    }
}