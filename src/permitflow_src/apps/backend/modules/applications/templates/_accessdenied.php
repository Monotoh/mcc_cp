<?php 
 use_helper('I18N');
?>
<div class="notfoundpanel">
 
  <div class="alert alert-danger"> 
    <button type="button" class="close" data-dismiss="alert"> </button>
    <h2><?php echo __('Application Access Denied!') ?></h2>
   <h4><?php echo __('Sorry, you are not allowed to view details of this application !')?></h4>
   <h4><?php echo __('If you are experiencing any trouble, please contact system administrator') ?>  </h4>
   <a class="btn btn-success" href="/backend.php/dashboard"> Go to Dashboard </a>
</div>
</div><!-- notfoundpanel -->
