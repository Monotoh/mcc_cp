<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 13/01/2015
 * Time: 13:48
 */
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">Enter an amount:</h4>
        <p>Enter the amount you wish to pay for now. You can make partial payments for this application but you will not receive the service until all payments are received.</p>
    </div>
    <form class="form-horizontal form-bordered" method="POST" action="<?php echo $_SERVER["PHP_SELF"]."?id=".$_GET['id']."&entryid=".$_GET['entryid'].($_GET['invoiceid']?"&invoiceid=".$_GET['invoiceid']:""); ?>">

        <div class="panel-body panel-body-nopadding">

            <div class="form-group">
                <label class="col-sm-3 control-label">Amount (<?php echo $currency; ?>)</label>
                <div class="col-sm-6">
                    <input type="number" name="partial_amount" placeholder="0.00" class="form-control" value="<?php echo $total_amount_remaining; ?>" max="<?php echo $total_amount_remaining; ?>">
                </div>
            </div>

        </div><!-- panel-body -->

        <div class="panel-footer">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <button type="submit" class="btn btn-primary">Submit</button>&nbsp;
                </div>
            </div>
        </div><!-- panel-footer -->

    </form>

</div><!-- panel -->

