<?php
/**
 * _comments_conditions template.
 *
 * Shows summary of conditions from all reviewers
 *
 * @package    backend
 * @subpackage applications
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

$q = Doctrine_Query::create()
    ->from('CfUser a')
    ->where('a.nid = ?', $_SESSION["SESSION_CUTEFLOW_USERID"]);
$reviewer = $q->fetchOne();

$q = Doctrine_Query::create()
    ->from('Permits a')
    ->where('a.applicationform = ?', $application->getFormId());
$permit = $q->fetchOne();

if($permit)
{
?>
<div class="table-responsive">
  <table class="table dt-on-steroids mb0" id="table3">
    <thead>
      <tr>
        <th>#</th>
        <th style="min-width: 300px;">Description</th>
        <th style="width: 150px;">Selected?</th>
      </tr>
    </thead>
    <tbody>
<?php
    $q = Doctrine_Query::create()
        ->from('ConditionsOfApproval a')
        ->where('a.permit_id = ?', $permit->getId())
        ->orderBy('a.short_name ASC');
    $conditions = $q->execute();

    foreach($conditions as $condition)
    {
        $q = Doctrine_Query::create()
            ->from('ApprovalCondition a')
            ->where('a.entry_id = ?', $application->getId())
            ->andWhere('a.condition_id = ?', $condition->getId());
        $cnd = $q->fetchOne();

        $resolved = "";
        if(empty($cnd)) {
            if($sf_user->mfHasCredential("resolvecomment")) {
                $resolved =  "<a title='Click to Mark as Selected' href='javascript:;' onClick=\"ajaxselect('/backend.php/applications/togglecondition/id/".$condition->getId()."/appid/".$application->getId()."','cn_".$condition->getId()."'); return false;\">";
            }

            $resolved = $resolved."<span class='glyphicon glyphicon-remove'></span>";
            if($sf_user->mfHasCredential("resolvecomment")) {
                $resolved =  $resolved."</a>";
            }
        }
        else
        {
            if($sf_user->mfHasCredential("resolvecomment")) {
                $resolved =  "<a title='Click to Mark as Not Selected.' href='javascript:;' onClick=\"ajaxunselect('/backend.php/applications/togglecondition/id/".$condition->getId()."/appid/".$application->getId()."','cn_".$condition->getId()."');\">";
            }
            
            $resolved = $resolved."<span class='glyphicon glyphicon-ok'></span>";
            if($sf_user->mfHasCredential("resolvecomment")) {
                $resolved =  $resolved."</a>";
            }
        }
?>
      <tr>
<!--OTB Patch - Start: Parse conditions of approval when viewing application-->
<?php
		$templateparser = new Templateparser();
		$cd_short = $condition->getShortName();
		$cd_desc = $condition->getDescription();
		foreach($templateparser->getCommentSheetDetails($application->getId()) as $key=>$comment_element){
			$cd_short = str_replace('{'.$key.'}', $comment_element, $cd_short);
			$cd_desc = str_replace('{'.$key.'}', $comment_element, $cd_desc);
		}
?>
        <td><?php echo $cd_short; ?></td>
        <td><?php echo $cd_desc; ?></td>
<!--OTB Patch - End: Parse conditions of approval when viewing application-->
        <!--<td><?php echo $condition->getShortName() ?></td>
        <td><?php echo $condition->getDescription() ?></td>-->
        <td><div id="cn_<?php echo $condition->getId() ?>"><?php echo $resolved ?></div></td>
      </tr>
<?php
    }
?>
    </tbody>
  </table>
</div>
<?php
}
?>
