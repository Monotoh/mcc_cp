<?php
/**
 * _comments_reviewer template.
 *
 * Shows comments from each reviewer
 *
 * @package    backend
 * @subpackage applications
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");

/** OTB patch addition of missing requuired components */
$prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";
require_once($prefix_folder.'includes/init.php');

header("p3p: CP=\"IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT\"");

$_SESSION['lang'] = $sf_user->getCulture();

require_once($prefix_folder.'config.php');
require_once($prefix_folder.'includes/language.php');
require_once($prefix_folder.'includes/db-core.php');
require_once($prefix_folder.'includes/common-validator.php');
require_once($prefix_folder.'includes/view-functions-task.php');
require_once($prefix_folder.'includes/post-functions.php');
require_once($prefix_folder.'includes/entry-functions.php');
require_once($prefix_folder.'includes/helper-functions.php');
require_once($prefix_folder.'includes/filter-functions.php');
require_once($prefix_folder.'includes/theme-functions.php');
require_once($prefix_folder.'lib/recaptchalib.php');
require_once($prefix_folder.'lib/php-captcha/php-captcha.inc.php');
require_once($prefix_folder.'lib/text-captcha.php');
require_once($prefix_folder.'hooks/custom_hooks.php');

$dbh = mf_connect_db();
$mf_settings = mf_get_settings($dbh);
?>
<div class="panel-group panel-group mb0" id="accordion_inner">
<?php

$taskitems = null;

$skiptasks = array();

$q = Doctrine_Query::create()
    ->from("Task a")
    ->where("a.application_id = ?", $application->getId());
$tasks = $q->execute();

foreach($tasks as $task)
{
    if($skiptasks[$task->getId()]) continue;

    if($task->getStatus() != 25) {
        // Delete duplicate tasks
        $q = Doctrine_Query::create()
            ->from('Task a')
            ->where('a.application_id = ?', $task->getApplicationId())
            ->andWhere('a.status <> 25 AND a.status <> 45 AND a.status <> 55')
            ->andWhere('a.owner_user_id = ?', $task->getOwnerUserId())
            ->andWhere('a.id <> ?', $task->getId());
        $duplicate_tasks = $q->execute();

        foreach($duplicate_tasks as $duplicate_task) {
            if($duplicate_task->getStatus() != 25 && $duplicate_task->getStatus() != 45 && $duplicate_task->getStatus() != 55) {
                $skiptasks[$duplicate_task->getId()] = true;
                $duplicate_task->delete();
            }
        }
    }
    //OTB patch 
    //Get our date formatter
    $otbhelper = new OTBHelper();
    //
    $q = Doctrine_Query::create()
        ->from("TaskForms a")
        ->where("a.task_id = ?", $task->getId());
    $taskforms = $q->execute();

    if(sizeof($taskforms) <= 0)
    {
        $q = Doctrine_Query::create()
            ->from('CfUser a')
            ->where('a.nid = ?', $task->getOwnerUserId());
        $reviewer = $q->fetchOne();
?>
  <div class="panel panel-default">
    <div class="panel-heading panel-heading-noradius">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion_inner" href="#comments<?php echo $task->getId(); ?>">
          <?php echo $reviewer->getStrfirstname()." ".$reviewer->getStrlastname(); ?> - <?php echo $reviewer->getStrdepartment(); ?>
          <p>(<?php echo __("Last updated on"); ?> <?php 
          echo $otbhelper->convertDateToHumanFriendlyMode($task->getLastUpdate()); //OTB patch - get the last date of task update.
          ?>)</p>
        </a>
      </h4>
    </div>
    <div id="comments<?php echo $task->getId(); ?>" class="panel-collapse collapse in">
      <div class="panel-body">
<?php 
        echo $task->getStatusName(); 

        if($task->getStatusName() == "Completed" && $task->getTypeName() == "Invoicing")
            echo __("Check billing tab for new invoices");
?>
      </div>
    </div>
  </div>
<?php
    }

    foreach($taskforms as $taskform)
    {
        $taskitems[] = $task->getId();

        $q = Doctrine_Query::create()
            ->from('CfUser a')
            ->where('a.nid = ?', $task->getOwnerUserId());
        $reviewer = $q->fetchOne();
?>
    <div class="panel-default">
    <div <?php if($task->getIsLeader() == 1 ) { ?> style="background-color:#00FF00 ; color: #FFFFFF;" <?php }?> class="panel-heading panel-heading-noradius">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion_inner" href="#comments<?php echo $task->getId().$taskform->getEntryId(); ?>">
          <?php echo $reviewer->getStrfirstname()." ".$reviewer->getStrlastname(); ?> - <?php echo $reviewer->getStrdepartment(); ?> - <?php if($task->getIsLeader() == 1 ) { ?> <u> (Project Lead Reviewer) </u>  <?php }?>
           <p>(<?php echo __("Last updated on"); ?> <?php 
          echo $otbhelper->convertDateToHumanFriendlyMode($task->getLastUpdate()); //OTB patch - get the last date of task update.
          ?>)</p>
        </a>
      </h4>
    </div>

    <!-- OTB patch Below view is missing -->
    <div id="comments<?php echo $task->getId().$taskform->getEntryId(); ?>" class="panel-collapse collapse collapse">
      <div class="panel-body">
<?php
        $q = Doctrine_Query::create()
            ->from("TaskForms a")
            ->where("a.task_id = ?", $task->getId());
        $taskforms = $q->execute();

        foreach($taskforms as $taskform)
        {
            $form_id  = $taskform->getFormId();
            $entry_id = $taskform->getEntryId();

            //get entry details for particular entry_id
            $param['checkbox_image'] = '/form_builder/images/icons/59_blue_16.png';
            //otb patch user locale settings
            $locale = sfContext::getInstance()->getUser()->getCulture();
            $entry_details = mf_get_entry_details($dbh,$form_id,$entry_id,$param,$locale);

            //Print Out Application Details
            foreach ($entry_details as $data) 
            {
                if(strlen($data['element_type'] == "section"))
                {
?>
        <section>
          <label for="text_field" style="font-weight: 900; width: 100%;"><?php echo $data['label']; ?></label>
        </section>
<?php
                }
                else
                {
                    if(($data['label'] == 'Cell' || $data['label'] == 'Village') && $data['value'] == "&nbsp;")
                    {

                    }
                    else
                    {
?>
        <section>
          <label for="text_field" style="font-weight: 900;"><?php echo $data['label']; ?></label>
          <div>&nbsp;&nbsp;&nbsp;&nbsp;<?php if($data['value']){ echo nl2br($data['value']); }else{ echo "-"; } ?></div>
          <hr>
        </section>
<?php
                    }
                }
            }
        }
?>
      </div>
    </div>
    
    <!--Start OTB: Task Location-->
    <?php
       $q = Doctrine_Query::create()
                ->from("TaskForms a")
                ->where("a.task_id = ?", $task->getId());
        $taskform = $q->fetchOne();
    ?>
    
    <?php if($taskform): ?>
      <div class="panel-body panel-body-nopadding panel-bordered" style="border-top:0;">
        <div id="basicWizard" class="basic-wizard">
                <div class="tab-pane pt20 active" id="ptab7">
                <form class="form-bordered">
                  <?php
		    include_partial('viewlocation', array('taskform' => $taskform));
                  ?>
                  </form>
                </div>
      </div>
    </div>
    <?php endif; ?>
    
  </div>
<?php 
    }
}

//get form id and entry id
$form_id  = $application->getFormId();
$entry_id = $application->getEntryId();

//Iterate through each department, check for assigned reviewers
$q = Doctrine_Query::create()
        ->from('Department a');
$departments = $q->execute();
$count_deps = 0;
foreach($departments as $department)
{
    //Iterate through each reviewer, check if they have tasks assigned to this application
    $q = Doctrine_Query::create()
        ->from('CfUser a')
        ->where('a.strdepartment = ?', $department->getDepartmentName());
    $reviewers = $q->execute();
    $count_tasks = 0;
    foreach($reviewers as $reviewer)
    {
        $q = Doctrine_Query::create()
            ->from('Task a')
            ->where('a.owner_user_id = ?', $reviewer->getNid())
            ->andWhere('a.application_id = ?', $application->getId());
        $count_tasks = $count_tasks + sizeof($q->execute());
    }

    // If this department doesn't have tasks, don't display it.
    if($count_tasks <= 0) continue;

    $count_deps++;

    $q = Doctrine_Query::create()
        ->from('CfUser a')
        ->where('a.strdepartment = ?', $department->getDepartmentName());
    $reviewers = $q->execute();
    foreach($reviewers as $reviewer)
    {
        $q = Doctrine_Query::create()
            ->from('Task a')
            ->where('a.owner_user_id = ?', $reviewer->getNid())
            ->andWhere('a.application_id = ?', $application->getId());
        $tasks = $q->execute();
        foreach($tasks as $task)
        {
            //if(($task->getStatusName() != "Pending" || $task->getStatusName() != "Completed") && in_array($task->getId(),$taskitems))
            if(($task->getStatusName() != "Pending" || $task->getStatusName() != "Completed"))
                break;

            if($task->getIsLeader() == "1") {
                $reviewertype = "Lead Reviewer";
                $style = "color: #4CD014;";
            }
            else {
                $reviewertype = "Support Reviewer";
                $style = "color: #F43535;";
            }

            $tasklink = "/backend.php/tasks/view/id/".$task->getId();
?>
  <div class="panel panel-default">
    <div class="panel-heading panel-heading-noradius">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion_inner" href="#comments<?php echo $task->getId().$taskform->getEntryId(); ?>">
          <?php echo $reviewer->getStrfirstname()." ".$reviewer->getStrlastname(); ?>
          <i>(<?php echo __('Last updated on') ?> <?php echo $otbhelper->convertDateToHumanFriendlyMode($task->getLastUpdate()); ?>)</i>
        </a>
      </h4>
    </div>
    <div id="comments<?php echo $task->getId().$taskform->getEntryId(); ?>" class="panel-collapse collapse">
      <div class="panel-body">
<?php
            //If this task is complete, show the comments
            if($task->getStatusName() == "Completed") {
                include_partial('comments_reviewer_comments', array('application' => $application, 'form_id' => $form_id, 'entry_id' =>  $entry_id, 'task' => $task));
            }

            else if($task->getStatusName() == "Pending" && $task->getCreatorUserId() == $sf_user->getGuardUser()->getId()) {
                include_partial('comments_reviewer_comments', array('application' => $application, 'form_id' => $form_id, 'entry_id' =>  $entry_id, 'task' => $task));
            }
            else {
                echo $task->getStatusName();
            }
?>
      </div>
    </div>
  </div>
<?php
        }
    }
}
?>
</div>
<?php

//If no reviewers have been assigned to work on this application
if($count_deps == 0): ?>
<table class="table mb0">
  <tbody>
    <tr>
      <td><i class="bold-label"><?php echo __("No records found"); ?></i></td>
    </tr>
  </tbody>
</table>
<?php endif; ?>
