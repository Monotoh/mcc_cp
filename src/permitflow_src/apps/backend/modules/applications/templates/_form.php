<?php

/**
 * viewSuccess.php template.
 *
 * Displays a dynamically generated application form
 *
 * @package    frontend
 * @subpackage forms
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

$sf_user->setAttribute('invoice_id', 0);

if($_GET['formid'])
{
    $_GET['id'] = $_GET['formid'];
}

$q = Doctrine_Query::create()
		->from("SfGuardUser a")
		->leftJoin('a.Profile b')
		->where('a.id = b.user_id')
		->andWhere('a.id = ?', $_SESSION['create_user']);
$client = $q->fetchOne();

$_SESSION['applicant_name'] = $client->getProfile()->getFullname();
$_SESSION['applicant_id'] = $client->getUsername();

$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

$application_manager = new ApplicationManager();

//Check if enable_categories is set, if it is then filter application forms
if(sfConfig::get('app_enable_categories') == "yes")
{
    $q = Doctrine_Query::create()
        ->from('sfGuardUserCategoriesForms a')
        ->where('a.categoryid = ?', $client->getProfile()->getRegisteras())
        ->andWhere('a.formid = ?', $_GET['id']);
    $category = $q->count();

    if ($category == 0) {
        exit;
    }
}

//Check if the application form can be auto-submitted by fetching user details
if($application_manager->can_autosubmit_form($_GET['id']) && empty($_GET['done']))
{
    $application_manager->autosubmit_form_backend($_GET['id'], $client->getId());
}
else {
    $_SESSION['edit_entry']['entry_id'] = null;

    $_SESSION['redirect'] = false;
    $_SESSION['redirect_url'] = "";

    $_SESSION['draft_edit'] = false;

    $prefix_folder = dirname(__FILE__) . "/../../../../../lib/vendor/cp_machform/";

    require_once($prefix_folder . 'includes/init.php');

    header("p3p: CP=\"IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT\"");

    require_once($prefix_folder . 'config.php');
    require_once($prefix_folder . 'includes/language.php');
    require_once($prefix_folder . 'includes/db-core.php');
    require_once($prefix_folder . 'includes/common-validator.php');
    require_once($prefix_folder . 'includes/view-functions.php');
    require_once($prefix_folder . 'includes/post-functions.php');
    require_once($prefix_folder . 'includes/filter-functions.php');
    require_once($prefix_folder . 'includes/entry-functions.php');
    require_once($prefix_folder . 'includes/helper-functions.php');
    require_once($prefix_folder . 'includes/theme-functions.php');
    require_once($prefix_folder . 'lib/recaptchalib.php');
    require_once($prefix_folder . 'lib/php-captcha/php-captcha.inc.php');
    require_once($prefix_folder . 'lib/text-captcha.php');
    require_once($prefix_folder . 'hooks/custom_hooks.php');

    $dbh = mf_connect_db();
    $ssl_suffix = mf_get_ssl_suffix();
    $_SESSION["user_email"] = $client->getProfile()->getEmail();

    $_SESSION['applicant_name'] = $client->getProfile()->getFullname();
    $_SESSION['applicant_id'] = $client->getUsername();


    if (mf_is_form_submitted()) { //if form submitted

        if ($_POST['save_as_draft'] || $_POST['save_as_draft2']) {
            $_SESSION['save_as_draft'] = true;
        } else {
            $_SESSION['save_as_draft'] = false;
        }

        $input_array = mf_sanitize($_POST);
        $submit_result = mf_process_form($dbh, $input_array);

        if (!isset($input_array['password'])) { //if normal form submitted

            if ($submit_result['status'] === true) {
                if (!empty($submit_result['form_resume_url'])) { //the user saving a form, display success page with the resume URL
                    $_SESSION['mf_form_resume_url'][$input_array['form_id']] = $submit_result['form_resume_url'];

                    if ($_SESSION['save_as_draft']) {
                        header("Location: http{$ssl_suffix}://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "?id={$input_array['form_id']}&draft={$submit_result['draft']}&done=1");
                    } else {
                        header("Location: http{$ssl_suffix}://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "?id={$input_array['form_id']}&entryid={$submit_result['entry_id']}&done=1");
                    }

                    exit;
                } else if ($submit_result['logic_page_enable'] === true) { //the page has skip logic enable and a custom destination page has been set
                    $target_page_id = $submit_result['target_page_id'];

                    if (is_numeric($target_page_id)) {
                        header("Location: http{$ssl_suffix}://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "?id={$input_array['form_id']}&mf_page={$target_page_id}");
                        exit;
                    } else if ($target_page_id == 'payment') {
                        //redirect to payment page, based on selected merchant
                        $form_properties = mf_get_form_properties($dbh, $input_array['form_id'], array('payment_merchant_type'));

                        if ($form_properties['payment_merchant_type'] == 'stripe') {
                            //allow access to payment page
                            $_SESSION['mf_form_payment_access'][$input_array['form_id']] = true;
                            $_SESSION['mf_payment_record_id'][$input_array['form_id']] = $submit_result['entry_id'];

                            $sf_user->setAttribute('form_id', $input_array['form_id']);
                            $sf_user->setAttribute('entry_id', $submit_result['entry_id']);

                            $form_id = $input_array['form_id'];
                            $new_record_id = $submit_result['entry_id'];

                            //We will use the application manager to create new applications or drafts from form submissions
                            $application_manager = new ApplicationManager();

                            //Check if an application already exists for the form submission to prevent double entry
                            if($application_manager->application_exists($form_id, $new_record_id)) {
                                //If save as draft/resume later was clicked then do nothing
                                $submission = $application_manager->get_application($form_id, $new_record_id);
                            }
                            else {
                                //If save as draft/resume later was clicked then create draft application
                                $submission = $application_manager->create_application($form_id, $new_record_id, $client->getId(), true);
                            }

                            $application_manager->update_invoices($submission->getId());

                            header("Location: http{$ssl_suffix}://" . $_SERVER['HTTP_HOST'] . mf_get_dirname($_SERVER['PHP_SELF']) . "/payment");
                            exit;
                        } else if ($form_properties['payment_merchant_type'] == 'paypal_standard') {
                            echo "<script type=\"text/javascript\">top.location.replace('{$submit_result['form_redirect']}')</script>";
                            exit;
                        }
                    } else if ($target_page_id == 'review') {
                        if (!empty($submit_result['origin_page_number'])) {
                            $page_num_params = '&mf_page_from=' . $submit_result['origin_page_number'];
                        }

                        $_SESSION['review_id'] = $submit_result['review_id'];
                        header("Location: http{$ssl_suffix}://" . $_SERVER['HTTP_HOST'] . mf_get_dirname($_SERVER['PHP_SELF']) . "/confirm?id={$input_array['form_id']}{$page_num_params}");
                        exit;
                    } else if ($target_page_id == 'success') {
                        //redirect to success page
                        if (empty($submit_result['form_redirect'])) {

                            if ($_SESSION['save_as_draft']) {
                                header("Location: http{$ssl_suffix}://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "?id={$input_array['form_id']}&draft={$submit_result['draft']}&done=1");
                            } else {
                                header("Location: http{$ssl_suffix}://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "?id={$input_array['form_id']}&entryid={$submit_result['entry_id']}&done=1");
                            }
                            exit;
                        } else {
                            echo "<script type=\"text/javascript\">top.location.replace('{$submit_result['form_redirect']}')</script>";
                            exit;
                        }
                    }

                } else if (!empty($submit_result['review_id']) && empty($submit_result['entry_id'])) { //redirect to review page

                    if (!empty($submit_result['origin_page_number'])) {
                        $page_num_params = '&mf_page_from=' . $submit_result['origin_page_number'];
                    }

                    if (!empty($submit_result['next_page_number'])) { //redirect to the next page number
                        $_SESSION['mf_form_access'][$input_array['form_id']][$submit_result['next_page_number']] = true;

                        header("Location: http{$ssl_suffix}://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "?id={$input_array['form_id']}&mf_page={$submit_result['next_page_number']}");
                        exit;
                    }

                    $_SESSION['review_id'] = $submit_result['review_id'];
                    if ($_SESSION['save_as_draft']) {
                        header("Location: /backend.php/applications/confirmDraft?id={$input_array['form_id']}&draft={$submit_result['draft']}{$page_num_params}");
                    } else {
                        header("Location: /backend.php/applications/confirmApplication?id={$input_array['form_id']}{$page_num_params}");
                    }
                    exit;
                } else {
                    if (!empty($submit_result['next_page_number'])) { //redirect to the next page number
                        $_SESSION['mf_form_access'][$input_array['form_id']][$submit_result['next_page_number']] = true;

                        header("Location: http{$ssl_suffix}://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "?id={$input_array['form_id']}&mf_page={$submit_result['next_page_number']}");
                        exit;
                    } else { //otherwise display success message or redirect to the custom redirect URL or payment page

                        //Disable payment on submission if enable_invoice is true
                        $form_properties = mf_get_form_properties($dbh, $input_array['form_id'], array('payment_enable_invoice'));

                        if (mf_is_payment_has_value($dbh, $input_array['form_id'], $submit_result['entry_id']) && $form_properties['payment_enable_invoice'] == 0) {
                            //redirect to credit card payment page, if the merchant is being enabled and the amount is not zero

                            //allow access to payment page
                            $_SESSION['mf_form_payment_access'][$input_array['form_id']] = true;
                            $_SESSION['mf_payment_record_id'][$input_array['form_id']] = $submit_result['entry_id'];

                            $sf_user->setAttribute('form_id', $input_array['form_id']);
                            $sf_user->setAttribute('entry_id', $submit_result['entry_id']);

                            $form_id = $input_array['form_id'];
                            $new_record_id = $submit_result['entry_id'];

                            //We will use the application manager to create new applications or drafts from form submissions
                            $application_manager = new ApplicationManager();

                            //Check if an application already exists for the form submission to prevent double entry
                            if($application_manager->application_exists($form_id, $new_record_id)) {
                                //If save as draft/resume later was clicked then do nothing
                                $submission = $application_manager->get_application($form_id, $new_record_id);
                            }
                            else {
                                //If save as draft/resume later was clicked then create draft application
                                $submission = $application_manager->create_application($form_id, $new_record_id, $client->getId(), true);
                            }

                            $application_manager->update_invoices($submission->getId());


                            $q = Doctrine_Query::create()
                                ->from('Invoicetemplates a')
                                ->where("a.applicationform = ?", $input_array['form_id']);
                            $invoicetemplate = $q->fetchOne();
                            if ($invoicetemplate) {
                                header("Location: http{$ssl_suffix}://" . $_SERVER['HTTP_HOST'] . mf_get_dirname($_SERVER['PHP_SELF']) . "/payment");
                                exit;
                            } else {
                                echo "<script type=\"text/javascript\">alert(\"No invoice template exists for this application form. Cannot authorize payment. Please contact system administrator.\"); top.location.replace('/index.php/forms/view?id={$input_array['form_id']}')</script>";
                                exit;
                            }

                        } else {
                            //redirect to success page
                            if (empty($submit_result['form_redirect'])) {

                                $_SESSION['review_id'] = $submit_result['review_id'];

                                if ($_SESSION['save_as_draft']) {
                                    header("Location: http{$ssl_suffix}://" . $_SERVER['HTTP_HOST'] . mf_get_dirname($_SERVER['PHP_SELF']) . "/confirmDraft?id={$input_array['form_id']}&draft={$submit_result['draft']}{$page_num_params}");
                                } else {
                                    header("Location: http{$ssl_suffix}://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "?id={$input_array['form_id']}&entryid={$submit_result['entry_id']}&done=1");
                                }

                                exit;
                            } else {
                                echo "<script type=\"text/javascript\">top.location.replace('{$submit_result['form_redirect']}')</script>";
                                exit;
                            }
                        }
                    }
                }
            } else if ($submit_result['status'] === false) { //there are errors, display the form again with the errors
                $old_values = $submit_result['old_values'];
                $custom_error = @$submit_result['custom_error'];
                $error_elements = $submit_result['error_elements'];

                $form_params = array();
                $form_params['page_number'] = $input_array['page_number'];
                $form_params['populated_values'] = $old_values;
                $form_params['error_elements'] = $error_elements;
                $form_params['custom_error'] = $custom_error;

                $form_params['is_application'] = true;
                $markup = mf_display_form($dbh, $input_array['form_id'], $form_params, $sf_user->getCulture());
            }
        } else { //if password form submitted

            if ($submit_result['status'] === true) { //on success, display the form
                $form_params['is_application'] = true;
                $markup = mf_display_form($dbh, $input_array['form_id'], array(), $sf_user->getCulture());
            } else {
                $custom_error = $submit_result['custom_error']; //error, display the pasword form again

                $form_params = array();
                $form_params['custom_error'] = $custom_error;
                $form_params['is_application'] = true;
                $markup = mf_display_form($dbh, $input_array['form_id'], $form_params, $sf_user->getCulture());
            }
        }
    } else {
        $form_id = (int)trim($_GET['id']);
        $page_number = (int)trim($_GET['mf_page']);

        if ($_GET['linkto']) {
            $_SESSION["main_application"] = $_GET['linkto'];
        }

        $page_number = mf_verify_page_access($form_id, $page_number);

        $resume_key = trim($_GET['mf_resume']);
        if (!empty($resume_key)) {
            $_SESSION['mf_form_resume_key'][$form_id] = $resume_key;
        }

        if (!empty($_GET['done'])) {

            unset($_SESSION['review_id']);

            //We will use the application manager to create new applications or drafts from form submissions
            $application_manager = new ApplicationManager();

            //If the submission is to be attached to an existing application, then create a linked application, else create a normal application
            if ($_SESSION["main_application"]) {
                $submission = $application_manager->create_linked_application($_SESSION["main_application"], $_GET['id'], $_GET['entryid'], $client->getId());

                $markup = mf_display_success($dbh, $form_id);

                $_SESSION["main_application"] = "";
            } else {
                //Check if an application already exists for the form submission to prevent double entry
                if ($application_manager->application_exists($_GET['id'], $_GET['entryid'])) {
                    //If a draft existed, then publish the draft to a live workflow
                    if ($application_manager->is_draft($_GET['id'], $_GET['entryid'])) {
                        $submission = $application_manager->get_application($_GET['id'], $_GET['entryid']);
                        $submission = $application_manager->publish_draft($submission->getId());
                        $_SESSION['just_submitted'] = true;
                        $application_manager->update_services($submission->getId());
                        $markup = mf_display_success($dbh, $form_id);
                    } else {
                        //If the application was already submitted then display warning
                        $submission = $application_manager->get_application($_GET['id'], $_GET['entryid']);
                        $_SESSION['just_submitted'] = true;
                        $application_manager->update_services($submission->getId());
                        $markup = mf_display_already_submitted($dbh, $form_id);
                    }
                } else {
                    //Create new application and publish it to a live workflow
                    $submission = $application_manager->create_application($_GET['id'], $_GET['entryid'], $client->getId(), false);
                    $_SESSION['just_submitted'] = true;
                    $application_manager->update_services($submission->getId());
                    $markup = mf_display_success($dbh, $form_id);
                }

            }

        } else {
            $form_params = array();
            $form_params['page_number'] = $page_number;
            $form_params['is_application'] = true;

            //Check if there is a limit
            $q = Doctrine_Query::create()
                ->from('ApForms a')
                ->where('a.form_id = ?', $form_id);
            $form_settings = $q->fetchOne();
            if ($form_settings && $form_settings->getFormUniqueIp()) {
                $q = Doctrine_Query::create()
                    ->from("FormEntry a")
                    ->where("a.user_id = ?", $client->getId())
                    ->andWhere("a.form_id = ?", $form_id)
                    ->andWhere('a.parent_submission = 0')
                    ->andWhere('a.approved <> 0')
                    ->orderBy("a.id DESC");
                $applications = $q->count();

                if ($applications > 0) {
                    //Check if the application has any expired licenses
                    if ($application_manager->form_entry_limit($form_id, $client->getId())) {
                        $markup = mf_display_unique_entry_warning($dbh, $form_id, $form_params);
                    } else {
                        $markup = mf_display_form($dbh, $form_id, $form_params, $sf_user->getCulture());
                    }
                } else {
                    $markup = mf_display_form($dbh, $form_id, $form_params, $sf_user->getCulture());
                }
            } else {
                $markup = mf_display_form($dbh, $form_id, $form_params, $sf_user->getCulture());
            }
        }
    }

    $q = Doctrine_Query::create()
        ->from('ApForms a')
        ->where('a.form_id = ?', $_GET['id']);
    $formObj = $q->fetchOne();
    $form_name = $formObj->getFormName();
    $form_description = $formObj->getFormDescription();

    $sql = "SELECT * FROM ext_translations WHERE field_id = '" . $_GET['id'] . "' AND field_name = 'form_name' AND table_class = 'ap_forms' AND locale = '" . $sf_user->getCulture() . "'";

    $rows = mysql_query($sql, $dbconn);
    if ($row = mysql_fetch_assoc($rows)) {
        $form_name = $row['trl_content'];
    }
    ?>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i><?php echo __('Application Form'); ?>
            <span> <?php echo __('Submit an Application'); ?></span></h2>

        <div class="breadcrumb-wrapper">

            <ol class="breadcrumb">
                <li><?php echo __('Application Form'); ?></li>
                <li class="active"><?php echo $form_name; ?></li>
            </ol>
        </div>
    </div>


    <div class="contentpanel">
        <div class="row">

            <div class="accordion_custom" id="accordion2">
                <div class="accordion-group whitened">
                    <div class="accordion-body in">
                        <div class="accordion-inner unspaced">
                            <?php
                            header("Content-Type: text/html; charset=UTF-8");
                            echo $markup;
                            ?>
                        </div>
                        <!-- accordion-inner -->
                    </div>
                    <!-- accordion-body -->
                </div>
                <!-- accordion-group -->
            </div>
            <!-- accordion2 -->

        </div>
        <!-- /.row -->
    </div><!-- /.marketing -->



    </div><!-- /.marketing -->

    <?php
    $q = Doctrine_Query::create()
        ->from("FormEntry a")
        ->where("a.user_id = ?", $client->getId())
        ->andWhere("a.approved = ?", 0)
        ->andWhere("a.form_id = ?", $_GET['id'])
        ->andWhere('a.parent_submission = ?', 0)
        ->orderBy("a.id DESC");
    $applications = $q->execute();
    foreach ($applications as $application) {
        ?>

        <!-- Modal -->
        <div class="modal fade" id="draftModal" tabindex="-1" role="dialog" aria-labelledby="draftModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Complete your application!</h4>
                    </div>
                    <div class="modal-body">
                        <p><?php echo __('You have an incomplete application that was saved as a draft'); ?>
                            '<?php echo $application->getApplicationId(); ?>
                            '. <?php echo __('Do you wish to complete this application and submit it?'); ?></p>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-info" type="button"
                                onClick="window.location='/index.php/application/edit?application_id=<?php echo $application->getId(); ?>';"><?php echo __('Yes'); ?></button>
                        <button class="btn btn-white" type="button"
                                onClick="window.location='/index.php/application/view/id/<?php echo $application->getId(); ?>';"><?php echo __('View this draft'); ?></button>
                        <button class="btn btn-white" type="button"
                                onClick="if(confirm('Are you sure?')){ window.location='/index.php/application/deletedraft/id/<?php echo $application->getId(); ?>/redirect/<?php echo $_GET['id']; ?>'; }"><?php echo __('No. Delete this draft'); ?></button>
                    </div>
                </div>
                <!-- modal-content -->
            </div>
            <!-- modal-dialog -->
        </div><!-- modal -->

        <script language="javascript">
            jQuery(document).ready(function () {
                jQuery('#draftModal').modal('show');
            });
        </script>

        <?php
        break;
    }
}
?>
