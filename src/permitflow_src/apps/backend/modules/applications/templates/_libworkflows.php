<?php
	/**
	 * _libcustom.php partial.
	 *
	 * Contains external libraries related to workflow management (Comment sheets, Reviewers and Departments)
	 *
	 * @package    backend
	 * @subpackage applications
	 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
	 */
	 
    $prefix_folder = dirname(__FILE__)."/../../../../..";
	
    require_once $prefix_folder.'/lib/vendor/cp_workflow/config/config.inc.php';
	require_once $prefix_folder.'/lib/vendor/cp_workflow/language_files/language.inc.php';
	require_once $prefix_folder.'/lib/vendor/cp_workflow/pages/CCirculation.inc.php';
	require_once $prefix_folder.'/lib/vendor/cp_workflow/pages/version.inc.php';

	
    $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
	mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
?>