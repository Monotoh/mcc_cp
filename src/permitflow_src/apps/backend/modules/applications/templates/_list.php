<?php
use_helper("I18N");
?>
<!-- OTB -->
<?php 
 $otbhelper = new OTBHelper();
?>
<?php //we will check if this application has a task assigned
  $query_to_check_task = Doctrine_Query::create()
                   ->select('id')
                   ->from('Task')
                   ->where('application_id = ? ',$application->getId() )
                   ->addwhere('task_stage = ?', $application->getApproved())
                   ->addwhere('creator_user_id = ?', $sf_user->getAttribute('userid'))
                   ->addwhere('status = 1')
                   ->limit(1);
             
    $no_of_task =    $query_to_check_task->execute();            
                   
 
 ?>
 
<?php
/*function GetDaysSince($sStartDate, $sEndDate){  
    $start_ts = strtotime($sStartDate);
  $end_ts = strtotime($sEndDate);
  $diff = $end_ts - $start_ts;
  return round($diff / 86400); 
}
*/
              //Check the previous stages through which the application has passed and calculate the days it has been in the process. Skip the times when the application is pending user feedback.
              $q = Doctrine_Query::create()
                 ->from("ApplicationReference a")
                 ->where("a.application_id = ?", $application->getId())
                 ->andWhere("a.stage_id <> 868 AND a.stage_id <> 869 AND a.stage_id <> 865 AND a.stage_id <> 867");
              $references = $q->execute();
              $totaldays = 0;
              /*foreach($references as $reference)
              {
                if($reference->getEndDate() != "")
                {
                  $days = GetDaysSince($reference->getStartDate(), $reference->getEndDate());
                }
                else
                {
                  $days = GetDaysSince($reference->getStartDate(), date("Y-m-d"));
                }
                $totaldays = $totaldays + $days;
              }*/
                                                        
              //OTB patch -- check if this is an approved stage, if so do not highlight applications with colors
            
             $stage_type = $otbhelper->getStageType($application->getApproved());
             error_log("Get stage type >>> ".$stage_type);
             //Stage type id 4 = approved - do not add style color if stage type id is 4
             if($stage_type != 4){
                 
                                                 
              $totaldays = $days;//OTB - will use app reference later
              
              // $dayssince = GetDaysSince($application_form['date_created'], date("Y-m-d"));
              
              if($totaldays > $dayssince)
              {
                $totaldays = ($dayssince - 1);
              }
              
              
              if($totaldays >= 0 && $totaldays <= 10 && $application->getStatusName() != "Archiving")
              {
                  $style = "background-color: #7EF257;";
              }
              else if($totaldays > 10 && $totaldays <= 20 && $application->getStatusName() != "Archiving")
              {
                  $style = "background-color: #FFFF5E;";
              }
              else if($totaldays > 20 && $application->getStatusName() != "Archiving")
              {
                  $style = "background-color: #FF5B5B;";
              }
              else if($application->getStatusName() == "Archiving")
              {
                  $style = "background-color: none;";
              }
                                                     }else {
                                                         $style = "background-color: none;";
                                                     }
                                                        
?>

<tr style="<?php echo $style; ?>">
  <td>
  <div class="ckbox ckbox-success">
    <input type="checkbox" id="batchapp<?php echo $application->getId(); ?>" name="batchapp[<?php echo $application->getId(); ?>]" value="<?php echo $application->getId(); ?>">
    <label for="batchapp<?php echo $application->getId(); ?>"></label>
  </div>
  </td>
  <td>
    <?php
  $q = Doctrine_Query::create()
    ->from("Favorites a")
    ->where("a.application_id = ?", $application->getId())
    ->andWhere("a.userid = ?", $_SESSION['SESSION_CUTEFLOW_USERID']);
  $favorite = $q->fetchOne();
  if($favorite)
  {
  ?>
  <a href="#" class="star star-checked" id="star<?php echo $application->getId(); ?>"><i class="glyphicon glyphicon-star"></i></a>
    <?php
  }
  else
  {
  ?>
    <a href="#" class="star" id="star<?php echo $application->getId(); ?>"><i class="glyphicon glyphicon-star"></i></a>
    <?php
  }
  ?>
    <script language="javascript">
  $(document).ready(function(){
    $("#star<?php echo $application->getId(); ?>").click(function(){
      $.ajax({url:"<?php echo public_path(); ?>backend.php/applications/setstar/id/<?php echo $application->getId(); ?>",success:function(result){
      $("#star<?php echo $application->getId(); ?>").toggleClass("star-checked");
      }});
    });
   });
  </script>
  </td>
   <td>
      <a href="<?php echo public_path(); ?>backend.php/applications/view/id/<?php echo $application->getId(); ?>"><?php echo $application->getApplicationId(); ?></a>
   </td>
    <td>
   <?php echo $application->getDateOfSubmission();
    
     if($stage_type != 4){ // OTB patch - Show counting of days for non approved stage
        echo "<br> ";
        echo "<br>(".$totaldays." days in progress)"; 
     
     }
   ?>
 </td>
 <!-- check if application has a task assigned and check if it was assigned by logged in user -->
   <?php
       $task_value = null ;
       $task_type = null ;
       $owner_id = null ;
      
       
      //
      foreach($no_of_task as $task) 
      {
      
      $task_value =  $task['id'] ;
                  $owner_id = $task['owner_user_id'] ;
                 // $task_type = $task['type'] ;
                  switch($task['type']):
                      case 2 :
                           $task_type = 'Assessment';
                           break;
                      case 3 :
                          $task_type = 'Invoicing';
                          break;
                      case 6:
                          $task_type = 'Inspection' ;
                          break;
                      case 4:
                          $task_type = 'Scanning' ;
                          break;
                      case 5:
                          $task_type = 'Collection' ;
                          break;
                      default:
                          $task_type = 'Unknown' ;

                  endswitch;
                  
    }
   
   ?>
   <?php if($task_value != null): ?>
   <td> <?php echo $application->getStatusName(); ?>  <br/>
       <?php 
            $user = $otbhelper->getReviewerDetails2($owner_id) ;          
       ?>
       <?php
          //show this to hod only
        if($sf_user->mfHasCredential("has_hod_access"))  {
       ?>
       <u><font color="blue"> (<?php echo $task_type ?> Task Assigned to </font>)</u> 
       <br/>
       <ul>
           <li> <font color="green"><?php 
                      if($user){
                        echo $user->getStrfirstname()." ".$user->getStrlastname()  ;
                      }
                      ?>  </font></li>
       </ul>
        <?php } ?>
       
        </td>
   <?php endif; ?>
   
    <?php if($task_value == null): ?>
   <td> <?php echo $application->getStatusName(); ?> </td>
   <?php endif; ?>
  <td>
     <?php
     try
     {
       $q = Doctrine_Query::create()
            ->from('SfGuardUser a')
            ->where('a.id = ?', $application->getUserId());
       $user = $q->fetchOne();

       $q = Doctrine_Query::create()
            ->from('SfGuardUserProfile a')
            ->where('a.user_id = ?', $application->getUserId());
       $user_profile = $q->fetchOne();

       if(empty($user_profile))
       {
         echo "- Could not verify user. Contact System Administrator -"; 
         error_log("FormEntry: Not user found for ".$application->getApplicationId().": ".$ex);  
       }
       else 
       {
        echo $user_profile->getFullname()." (".$user->getUsername().")";
       }
     }catch(Exception $ex)
     {
         error_log("FormEntry: Not user found for ".$application->getApplicationId().": ".$ex);
     }
     ?>
 </td>
  <td align="center">
         <a  title='<?php echo __('View Application'); ?>' href='<?php echo public_path(); ?>backend.php/applications/view/id/<?php echo $application->getId(); ?>'> <span class="badge badge-primary"><i class="fa fa-eye"></i></span></a>
          <?php
          $q = Doctrine_Query::create()
              ->from('SubMenuTasks a')
              ->where('a.sub_menu_id = ?', $application->getApproved());

          $allowedtasks = $q->execute();
          foreach($allowedtasks as $allowedtask)
          {
              if($allowedtask->getTaskId() == "2")
              {
                  ?>
                  <a href="window.location='/backend.php/tasks/commentbeta/id/0/application/<?php echo $application->getId(); ?>" title="<?php echo __('Start');?> <?php echo __('Assessment'); ?>"> <span class="badge badge-primary"><i class="fa fa-play"></i></span></a>
              <?php
              }

              if($allowedtask->getTaskId() == "6")
              {
                  ?>
                  <a href="window.location='/backend.php/tasks/commentbeta/id/0/application/<?php echo $application->getId(); ?>" title="<?php echo __('Start');?> <?php echo __('Inspection'); ?>"> <span class="badge badge-primary"><i class="fa fa-play"></i></span></a>
              <?php
              }

              if($allowedtask->getTaskId() == "3")
              {
                  $q = Doctrine_Query::create()
                      ->from("MfInvoice a")
                      ->where("a.app_id = ?", $application->getId())
                      ->andWhere("a.paid = 15");
                  $invoices = $q->execute();
                  $disabled = false;
                  if(sizeof($invoices) > 0)
                  {
                      $disabled = true;
                  }
                  ?>
                  <a <?php if($disabled){ echo "disabled='disabled'"; } ?> href="/backend.php/tasks/invoice/id/0/application/<?php echo $application->getId(); ?>" title="<?php echo __('Start');?> <?php echo __('Invoicing'); ?>"> <span class="badge badge-primary"><i class="fa fa-play"></i></span></a>
              <?php
              }

              if($allowedtask->getTaskId() == "4")
              {
                  ?>
                  <a href="/backend.php/tasks/commentbeta/id/0/application/<?php echo $application->getId(); ?>" title="<?php echo __('Start');?> <?php echo __('Scanning'); ?>"> <span class="badge badge-primary"><i class="fa fa-play"></i></span></a>
              <?php
              }

              if($allowedtask->getTaskId() == "5")
              {
                  ?>
                  <a href="/backend.php/tasks/commentbeta/id/0/application/<?php echo $application->getId(); ?>" title="<?php echo __('Start');?> <?php echo __('Collection'); ?>"> <span class="badge badge-primary"><i class="fa fa-play"></i></span></a>
              <?php
              }
          }
          ?>
    <!-- <a  title='<?php //echo __('Edit Application'); ?>' href='<?php //echo public_path(); ?>backend.php/applications/edit?form_id=<?php //echo $application->getFormId(); ?>&id=<?php //echo $application->getEntryId(); ?>'><span class="badge badge-primary"><i class="fa fa-pencil"></i></span></a>
        -->    
 <?php
     $q = Doctrine_Query::create()
        ->from("SavedPermit a")
        ->where("a.application_id = ?", $application->getId())
        ->andWhere("a.permit_status <> 0") // Do not show deleted permits
        ->orderBy("a.id DESC")
        ->limit(3);
     $permits = $q->execute();

     foreach($permits as $permit)
     {
       $template = Doctrine_Core::getTable('permits')->find($permit->getTypeId());
       if($template)
       {
         if($permit->getPdfPath())
         {
         ?>
         <a href="<?php echo $permit->getPdfPath(); ?>" title="<?php echo $template->getTitle();?>"> <span class="badge badge-primary"><i class="fa fa-print"></i></span></a>
         <?php
         }
         else
         {
         ?>
         <a href="/backend.php/applications/viewpermit/id/<?php echo $permit->getId(); ?>" title="<?php echo $template->getTitle();?>"> <span class="badge badge-primary"><i class="fa fa-print"></i></span></a>
         <?php
         }
       }
     }
     ?>
  </td>
</tr>
