<?php
use_helper("I18N");
?>
<tr>
  <td>
	<div class="ckbox ckbox-success">
		<input type="checkbox" id="batchapp<?php echo $application->getId(); ?>" name="batchapp[<?php echo $application->getId(); ?>]" value="<?php echo $application->getId(); ?>">
		<label for="batchapp<?php echo $application->getId(); ?>"></label>
	</div>
  </td>
  <td>
  	<?php
	$q = Doctrine_Query::create()
		->from("Favorites a")
		->where("a.application_id = ?", $application->getId())
		->andWhere("a.userid = ?", $_SESSION['SESSION_CUTEFLOW_USERID']);
	$favorite = $q->fetchOne();
	if($favorite)
	{
	?>
	<a href="#" class="star star-checked" id="star<?php echo $application->getId(); ?>"><i class="glyphicon glyphicon-star"></i></a>
    <?php
	}
	else
	{
	?>
    <a href="#" class="star" id="star<?php echo $application->getId(); ?>"><i class="glyphicon glyphicon-star"></i></a>
    <?php
	}
	?>
    <script language="javascript">
	$(document).ready(function(){
		$("#star<?php echo $application->getId(); ?>").click(function(){
		  $.ajax({url:"<?php echo public_path(); ?>backend.php/applications/setstar/id/<?php echo $application->getId(); ?>",success:function(result){
			$("#star<?php echo $application->getId(); ?>").toggleClass("star-checked");
		  }});
		});
	 });
	</script>
  </td>
   <td>
			<a href="<?php echo public_path(); ?>backend.php/applications/viewarchive/id/<?php echo $application->getId(); ?>"><?php echo $application->getApplicationId(); ?></a>
   </td>
    <td>
   <?php echo $application->getDateOfSubmission(); ?>
 </td>
  <td>
     <?php
       $q = Doctrine_Query::create()
            ->from('SfGuardUser a')
            ->where('a.id = ?', $application->getUserId());
       $user = $q->fetchOne();

       $q = Doctrine_Query::create()
            ->from('SfGuardUserProfile a')
            ->where('a.user_id = ?', $application->getUserId());
       $user_profile = $q->fetchOne();

       echo $user_profile->getFullname()." (".$user->getUsername().")";
     ?>
 </td>
  <td align="center">
         <a  title='<?php echo __('View Application'); ?>' href='<?php echo public_path(); ?>backend.php/applications/viewarchive/id/<?php echo $application->getId(); ?>'> <span class="badge badge-primary"><i class="fa fa-eye"></i></span></a>
  </td>
</tr>
