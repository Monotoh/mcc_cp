<div class="col-lg-2">
 <div class="panel panel-default">
<?php
if($sf_user->mfHasCredential('createapplications'))
{
?>
<button class="btn btn-danger btn-block btn-compose-email" onClick="window.location='/backend.php/applications/create';"><?php echo ("Create New"); ?></button>
<?php
}

if($sf_user->mfHasCredential('assigntask'))
{
    ?>
    <a name="newtaskbtn" id="newtaskbtn" class="btn btn-primary btn-block btn-compose-email"  data-toggle="modal" data-target="#myModal"><?php echo ("Assign Applications"); ?></a>
<?php
}

function bd_nice_number($n) {
	// first strip any formatting;
	$n = (0+str_replace(",","",$n));

	// is this a number?
	if(!is_numeric($n)) return false;

	// now filter it;
	if($n>1000000000000) return round(($n/1000000000000),1).' T';
	else if($n>1000000000) return round(($n/1000000000),1).' B';
	else if($n>1000000) return round(($n/1000000),1).' M';
	else if($n>10000) return round(($n/10000),1).' K';

	return number_format($n);
}

	$q = Doctrine_Query::create()
        ->from('SubMenus a')
        ->where("a.id = ?", $stage);
    $submenu = $q->fetchOne();

    if($submenu) {
        $group = $submenu->getMenuId();
    }
?>
    <h5 class="subtitle"><?php echo ("Approval Stages"); ?></h5>
    <ul class="nav nav-pills nav-stacked nav-email">
        <?php
          $q = Doctrine_Query::create()
	        ->from('SubMenus a')
	        ->where("a.menu_id = ?", $group)
	        ->andWhere("a.deleted <> 1")
	        ->orderBy('a.order_no ASC');
	    $submenus = $q->execute();
           
	    foreach($submenus as $submenu)
	    {
	        if($sf_user->mfHasCredential('accesssubmenu'.$submenu->getId()))
	        {
                if($submenu->getStageType() == 7)
                {
                ?>
        <li <?php if($submenu->getId() == $stage){ echo "class='active'"; } ?>><a  href="<?php echo public_path(); ?>backend.php/applications/searcharchive/subgroup/<?php echo $submenu->getId(); if($form_id){ echo "/form/".$form_id; } ?>">
                    <span class="badge pull-right"><?php echo $applications; ?></span>
                    <i class="fa fa-caret-right"></i>
                    <?php echo $submenu->getTitle(); ?></a></li>
                <?php
                }
                else
                {
                     //OTB patch - Number of application in a stage
                      $apps_r = Doctrine_Query::create()
                            ->from("FormEntry f")
                              ->where("f.deleted_status = ?", 0)
                            ->andWhere("f.approved = ?", $submenu->getId())
                              ->andWhere("f.parent_submission = ? ",0)->count();
                   //
                    ?>
                    <li <?php if($submenu->getId() == $stage){ echo "class='active'"; } ?>><a  href="<?php echo public_path(); ?>backend.php/applications/listgroup/subgroup/<?php echo $submenu->getId(); if($form_id){ echo "/form/".$form_id; } ?>">
                    <span class="badge pull-right"><?php echo $applications; ?></span>
                    <span class=" pull-right badge badge-warning badge-round"><?php echo $apps_r ?></span>
                    <i class="fa fa-caret-right"></i>
                    <?php echo $submenu->getTitle(); ?></a>
                    
                    </li>
                <?php
                }
	        }
	    }
        ?>
    </ul>
    <div class="mb20"></div>
    </div>
</div><!-- col-sm-3 -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form" action="<?php echo public_path(); ?>backend.php/tasks/save" method="post" autocomplete="off">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo ('New Task'); ?></h4>
                </div>
                <div class="modal-body modal-body-nopadding" id="newtask">
                    <img src="/assets_unified/images/loaders/loader10.gif" alt="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo ('Close'); ?></button>
                    <button type="submit" class="btn btn-primary"><?php echo ('Save changes'); ?></button>
                </div>
            </form>
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->


<script language="javascript">
    jQuery(document).ready(function() {
        jQuery("#myModal").on('shown.bs.modal', function () {
            jQuery("#newtask").load("<?php echo public_path(); ?>backend.php/tasks/new/submenu/<?php echo $_SESSION['subgroup']; ?>");
        });
    });
</script>
