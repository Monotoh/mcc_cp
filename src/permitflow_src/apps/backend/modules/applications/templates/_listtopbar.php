<div class="btn-group mr10">
                                <button id="archive" class="btn btn-white tooltips" type="button" data-toggle="tooltip" title="Archive"><i class="glyphicon glyphicon-hdd"></i></button>
                                <script language="javascript">
								$( "#archive").click(function() {
								   var batchapps = new Array();
								   var count = 0;
								   $('input[type=checkbox]').each(function () {
								   		if(this.checked == true)
								   		{
									   		batchapps[count] = this.value;
									   		count++;
								   		}
								   });
								   
								   $.ajax({
									   url: "<?php echo public_path(); ?>backend.php/applications/batch",
									   type: "POST",
									   
									   data:{'batchitems':batchapps,'archive':'true'},
								
									   success:function(returndata){
											$("input[type='checkbox']").each(function(){
												var chkval = 0
												if($(this).is(":checked")){
													$(this).prop('checked', false);
													//convert crosses to ticks
													count++;
												}
											});
									   },error:function(errordata){
											alert("Could not finish your request. Please try again.");
									   }
									 });
				
								   
								});
								</script>
                                <button id="spam" class="btn btn-white tooltips" type="button" data-toggle="tooltip" title="Report Spam"><i class="glyphicon glyphicon-exclamation-sign"></i></button>
                                <script language="javascript">
								$( "#spam").click(function() {
								   var batchapps = new Array();
								   var count = 0;
								   $('input[type=checkbox]').each(function () {
								   		if(this.checked == true)
								   		{
									   		batchapps[count] = this.value;
									   		count++;
								   		}
								   });
								   
								   $.ajax({
									   url: "<?php echo public_path(); ?>backend.php/applications/batch",
									   type: "POST",
									   
									   data:{'batchitems':batchapps,'spam':'true'},
								
									   success:function(returndata){
											$("input[type='checkbox']").each(function(){
												var chkval = 0
												if($(this).is(":checked")){
													$(this).prop('checked', false);
													//convert crosses to ticks
													count++;
												}
											});
									   },error:function(errordata){
											alert("Could not finish your request. Please try again.");
									   }
									 });
				
								   
								});
								</script>
                                <button id="trash" class="btn btn-white tooltips" type="button" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-trash"></i></button>
                                <script language="javascript">
								$( "#trash").click(function() {
								   var batchapps = new Array();
								   var count = 0;
								   $('input[type=checkbox]').each(function () {
								   		if(this.checked == true)
								   		{
									   		batchapps[count] = this.value;
									   		count++;
								   		}
								   });
								   
								   $.ajax({
									   url: "<?php echo public_path(); ?>backend.php/applications/batch",
									   type: "POST",
									   
									   data:{'batchitems':batchapps,'trash':'true'},
								
									   success:function(returndata){
											$("input[type='checkbox']").each(function(){
												var chkval = 0
												if($(this).is(":checked")){
													$(this).prop('checked', false);
													//convert crosses to ticks
													count++;
												}
											});
									   },error:function(errordata){
											alert("Could not finish your request. Please try again.");
									   }
									 });
				
								   
								});
								</script>
</div>

<div class="btn-group mr10">
    <div class="btn-group nomargin">
        <button data-toggle="dropdown" class="btn btn-white dropdown-toggle tooltips" type="button" title="Move to Stage">
          <i class="glyphicon glyphicon-folder-close mr5"></i>
          <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" style="overflow:auto; height: 300px;">
        <?php
		$q = Doctrine_Query::create()
			->from('Menus a')
			->orderBy('a.order_no ASC');
		$stagegroups = $q->execute();
		?>
        <?php foreach($stagegroups as $group): ?>
          <li><a href="#"><i class="glyphicon glyphicon-folder-open mr5"></i> <?php echo $group->getTitle(); ?></a>
          <?php
            $q = Doctrine_Query::create()
                ->from('SubMenus a')
                ->where('a.menu_id = ?', $group->getId())
                ->orderBy('a.order_no ASC');
            $stages = $q->execute();
            if(sizeof($stages) > 0)
            {
                echo "<ul>";
            }
            foreach($stages as $stage)
            {
                echo "<li><a href='#' id='moveto".$stage->getId()."'>".$stage->getTitle()."</a></li>";
				?>
                <script language="javascript">
				$( "#moveto<?php echo $stage->getId(); ?>").click(function() {
				   var batchapps = new Array();
				   var count = 0;
				   $('input[type=checkbox]').each(function () {
				   		if(this.checked == true)
				   		{
					   		batchapps[count] = this.value;
					   		count++;
				   		}
				   });
				   
				   $.ajax({
					   url: "<?php echo public_path(); ?>backend.php/applications/batch",
					   type: "POST",
					   
					   data:{'batchitems':batchapps,'moveto':'true'},
				
					   success:function(returndata){
							$("input[type='checkbox']").each(function(){
								var chkval = 0
								if($(this).is(":checked")){
									$(this).prop('checked', false);
									//convert crosses to ticks
									count++;
								}
							});
					   },error:function(errordata){
							alert("Could not finish your request. Please try again.");
					   }
					 });

				   
				});
				</script>
                <?php
            }
            if(sizeof($stages) > 0)
            {
                echo "</ul>";
            }
          ?>
          </li>
          <?php endforeach; ?>
        </ul>
    </div>
    <div class="btn-group nomargin">
        <button data-toggle="dropdown" class="btn btn-white dropdown-toggle tooltips" type="button" title="Filter by Stage">
          <i class="glyphicon glyphicon-tag mr5"></i>
          <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" style="overflow:auto; height: 300px;">
        <?php
		$q = Doctrine_Query::create()
			->from('Menus a')
			->orderBy('a.order_no ASC');
		$stagegroups = $q->execute();
		?>
        <?php foreach($stagegroups as $group): ?>
          <li><a href="#"><i class="glyphicon glyphicon-tag mr5"></i> <?php echo $group->getTitle(); ?></a>
          <?php
            $q = Doctrine_Query::create()
                ->from('SubMenus a')
                ->where('a.menu_id = ?', $group->getId())
                ->orderBy('a.order_no ASC');
            $stages = $q->execute();
            if(sizeof($stages) > 0)
            {
                echo "<ul>";
            }
            foreach($stages as $stage)
            {
                echo "<li><a href='".public_path()."backend.php/applications/listgroup/subgroup/".$stage->getId()."'>".$stage->getTitle()."</a></li>";
            }
            if(sizeof($stages) > 0)
            {
                echo "</ul>";
            }
          ?>
          </li>
          <?php endforeach; ?>
        </ul>
    </div>
</div>