<?php
	/**
	 * _viewattachments.php partial.
	 *
	 * Displays application details with only the attachments
	 *
	 * @package    backend
	 * @subpackage applications
	 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
	 */
	
	//get form id and entry id
	$form_id  = $application->getFormId();
	$entry_id = $application->getEntryId();

    $prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";


    require_once($prefix_folder.'config.php');
    require_once($prefix_folder.'includes/db-core.php');
    require_once($prefix_folder.'includes/helper-functions.php');

    require_once($prefix_folder.'includes/entry-functions.php');

$nav = trim($_GET['nav']);

if(empty($form_id) || empty($entry_id)){
    die("Invalid Request");
}

$dbh = mf_connect_db();
$mf_settings = mf_get_settings($dbh);


//get entry details for particular entry_id
$param['checkbox_image'] = '/assets_unified/images/59_blue_16.png';
$attachments_details = mf_get_attachments_details($dbh,$form_id,$entry_id,$param);

?>
<br>
<?php
    
    //Print Out Application Details 
    foreach ($attachments_details as $data){
    ?>  
    <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo $data['label']; ?></i></label>
        <div class="col-sm-8"><?php if($data['value']){ echo nl2br($data['value']); }else{ echo "-"; } ?></div>
    </div>
<?php } ?>  