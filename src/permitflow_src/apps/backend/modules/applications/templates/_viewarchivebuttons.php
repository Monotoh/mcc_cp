<?php
	/**
	 * _viewbuttons.php partial.
	 *
	 * Display control buttons that manipulate the application
	 *
	 * @package    backend
	 * @subpackage applications
	 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
	 */

	/**
	*
	* Function to get all the days between a period of time
	*
	* @param String $sStartDate Start date to begin fetching dates from
	* @param String $sEndDate End date where to stop fetching dates from
	*
	* @return String[]
	*/
	use_helper('I18N');
?>


<div class="btn-group mr5 pull-left">
  <button type="button" class="btn btn-success">Download</button>
  <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu" role="menu">
		<?php
		$q = Doctrine_Query::create()
			->from("SavedPermitArchive a")
			->where("a.application_id = ?", $application->getId())
			->andWhere("a.permit_status <> 3");
		$permits = $q->execute();

		foreach($permits as $permit)
		{
			$q = Doctrine_Query::create()
					->from('Permits a')
					->where('a.id = ?', $permit->getTypeId());
			$permittype = $q->fetchOne();

			if($permittype->getPartType() == 2 && $permit->getDocumentKey() == "")
			{
					continue;
			}
			?>
			<li><a target="_blank" onClick="window.location = '/backend.php/permits/viewarchive/id/<?php echo $permit->getId(); ?>';"><?php echo $permittype->getTitle()." (".$permit->getDateOfIssue().")"; ?></a></li>
			<?php
		}
		?>
  </ul>
</div>