<?php
/**
 * _viewdetails.php partial.
 *
 * Displays application details
 *
 * @package    backend
 * @subpackage applications
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

//get form id and entry id
$form_id  = $application->getFormId();
$entry_id = $application->getEntryId();

$prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";

require_once($prefix_folder.'config.php');
require_once($prefix_folder.'includes/db-core.php');
require_once($prefix_folder.'includes/helper-functions.php');

require_once($prefix_folder.'includes/entry-functions.php');


$nav = trim($_GET['nav']);

    $dbh = mf_connect_db();

//get entry details for particular entry_id
    $param['checkbox_image'] = '/assets_unified/images/59_blue_16.png';
    $entry_details = mf_get_entry_details($dbh, $form_id, $entry_id, $param, $sf_user->getCulture());

//Print Out Application Details
    foreach ($entry_details as $data) {
        ?>
        <?php
        if ($data['element_type'] == "section") {
            ?>
            <div class="form-group">
                <label class="col-sm-12"><?php echo $data['label']; ?></label>
            </div>
        <?php
        } elseif ($data['element_type'] == "page_break") {

        } else {
            ?>
            <div class="form-group">
                <label class="col-sm-2 control-label"><i class="bold-label-2"><?php echo $data['label']; ?></i></label>

                <div class="col-sm-8"><?php if ($data['value']) {
                        echo nl2br($data['value']);
                    } else {
                        echo "-";
                    } ?></div>
            </div>
        <?php
        }
    }
?>
