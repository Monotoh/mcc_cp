<?php
	/**
	 * _viewinvoices.php partial.
	 *
	 * Displays any invoices attached to an application
	 *
	 * @package    backend
	 * @subpackage applications
	 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
	 */
    use_helper("I18N");
    
    $q = Doctrine_Query::create()
        ->from("ApSettings a");
    $settings = $q->fetchOne();
?>
<div class="panel-group panel-group-dark mb0" id="accordion1">
<?php
    $q = Doctrine_Query::create()
       ->from("MfInvoiceArchive a")
       ->where("a.app_id = ?", $application->getId())
       ->orderBy("a.id DESC");
    $invoices = $q->execute();

    foreach($invoices as $invoice)
    {
		//Display information about each invoice
        $invcount++;
        
        //Generated PDF document
        try {
            if($invoice->getPdfPath() == "")
            {
                $invoice_manager = new InvoiceManager();
    
                $filename = $invoice_manager->save_archive_to_pdf_locally($invoice->getId());
                $invoice->setPdfPath($filename);
                $invoice->save();
            }
        }catch(Exception $ex)
        {
            error_log("Debug-p: ".$ex);
        }
        ?>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" class="collapsed" data-parent="#accordion1" href="#collapseTwo<?php echo $invoice->getId(); ?>">
                    <?php echo __("Invoice"); ?> <?php echo ((sizeof($invoices) + 1) - $invcount); ?> (<?php
                    $expired = false;

                    $db_date_event = str_replace('/', '-', $invoice->getExpiresAt());

                    $db_date_event = strtotime($db_date_event);

                    if (time() > $db_date_event && !($invoice->getPaid() == "15" || $invoice->getPaid() == "2" || $invoice->getPaid() == "3"))
                    {
                         $expired = true;
                    }

                    if($expired)
                    {
                        echo "Expired";
                    }
                    else
                    {
                        if($invoice->getPaid() == "1"){
                           echo __("Pending");
                        }else if($invoice->getPaid() == "15"){
                          echo "Part Payment";
                        }elseif($invoice->getPaid() == "2"){
                          echo __("Paid");
                        }elseif($invoice->getPaid() == "3"){
                            echo __("Cancelled");
                        }
                    }
                    ?>) <?php if($invoice->getExpiresAt()){ echo "Expires on ".$invoice->getExpiresAt(); } ?>
                  </a>
                </h4>
              </div>
              <div id="collapseTwo<?php echo $invoice->getId(); ?>" class="panel-collapse collapse <?php if($invcount == 1){ ?> in <?php } ?>">
              <div align="right">
                  <div class="text-right btn-invoice" style="padding: 10px;">
                        <?php
        
                        if(($invoice->getPaid() == "1" && $sf_user->mfHasCredential('approvepaymentsupport')) || ($invoice->getPaid() == "15" && $sf_user->mfHasCredential('approvepayments')) || ($invoice->getPaid() == "2" && $application->getApproved() == 0  && $sf_user->mfHasCredential('approvepaymentsupport')))
                        {
                        ?>
                        <a onClick="if(confirm('Are you sure you want to confirm this invoice?')){ return true; }else{ return false; }" href="/backend.php/applications/view/id/<?php echo $application->getId(); ?>/confirmpayment/<?php echo md5($invoice->getId()); ?>" class="btn btn-white" id="printinvoice" type="button"><i class="fa fa-print mr5"></i> <?php echo __("Confirm Payment"); ?></a>
                        <?php
                        }
        
                        if($invoice->getPaid() == 3 && $sf_user->mfHasCredential('approvepaymentsupport'))
                        {
                            ?>
                            <a class="btn btn-white" id="makepayment" onClick="if(confirm('Are you sure you want to uncancel this invoice?')){ return true; }else{ return false; }" href="/backend.php/invoices/view/id/<?php echo $invoice->getId(); ?>/confirm/<?php echo md5($invoice->getId()); ?>"><i class="fa fa-print mr5"></i> <?php echo __('UnCancel Payment'); ?></a>
                        <?php
                        }
        
        
        
                        if($invoice->getPaid() <> 2 && $invoice->getPaid() <> 3 && $sf_user->mfHasCredential('approvepayments')){
                            $sf_user->setAttribute('form_id', $invoice->getFormEntry()->getFormId());
                            $sf_user->setAttribute('entry_id', $invoice->getFormEntry()->getEntryId());
                            $sf_user->setAttribute('invoice_id', $invoice->getId());
                            ?>
                            <button class="btn btn-white" id="makepayment" type="button" onClick="window.location='/backend.php/applications/payment';"><i class="fa fa-print mr5"></i> <?php echo __('Add Payment'); ?></button>
                        <?php
                        }
        
                        if($invoice->getPaid() <> 3 && $sf_user->mfHasCredential('approvepayments'))
                        {
                            ?>
                            <a class="btn btn-white" id="makepayment" onClick="if(confirm('Are you sure you want to cancel this invoice?')){ return true; }else{ return false; }" href="/backend.php/invoices/view/id/<?php echo $invoice->getId(); ?>/cancel/<?php echo md5($invoice->getId()); ?>"><i class="fa fa-print mr5"></i> <?php echo __('Cancel Payment'); ?></a>
                        <?php
                        }
                        ?>
                    </div>
              </div>
                <div class="panel-body" align="center">
                    <?php
                    $pdf_path = $invoice->getPdfPath();
                    
                    if(substr($settings->getUploadDir(), 0, 8) == "/mnt/gv0")
                    {
                        $pdf_path = "/uploads/".$pdf_path;
                    }
                    else
                    {
                        $pdf_path = $settings->getUploadDir()."/".$pdf_path;
                    }
                    
                    if(substr($pdf_path, 1) != "/")
                    {
                        $pdf_path  = "/".$pdf_path;
                    }
                    ?>
                    <iframe src = "/ViewerJS/#..<?php echo $pdf_path; ?>" width='1024' height='900' allowfullscreen webkitallowfullscreen></iframe>
                    
                </div>
              </div>
            </div>
        <?php
    }

	//If no invoices have been attached to this application
	if($invcount == 0)
	{
		echo "<table class=\"table mb0\">
			<tbody>
			<tr>
			<td><i class=\"bold-label\">".__('No records found')."</i></td>
			</tr>
			</tbody>
			</table>
";
	}

?>
</div>
