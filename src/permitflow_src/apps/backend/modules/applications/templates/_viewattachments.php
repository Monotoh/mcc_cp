<?php
	/**
	 * _viewattachments.php partial.
	 *
	 * Displays application details with only the attachments
	 *
	 * @package    backend
	 * @subpackage applications
	 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
	 */
	
	//get form id and entry id
	$form_id  = $application->getFormId();
	$entry_id = $application->getEntryId();

    $prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";


    require_once($prefix_folder.'config.php');
    require_once($prefix_folder.'includes/db-core.php');
    require_once($prefix_folder.'includes/helper-functions.php');

    require_once($prefix_folder.'includes/entry-functions.php');

$nav = trim($_GET['nav']);

if(empty($form_id) || empty($entry_id)){
    die("Invalid Request");
}

$dbh = mf_connect_db();
$mf_settings = mf_get_settings($dbh);
$otbhelper = new OTBHelper();

//get entry details for particular entry_id
$param['checkbox_image'] = '/assets_unified/images/59_blue_16.png';
$attachments_details = mf_get_attachments_details($dbh,$form_id,$entry_id,$param);

?>
<?php 
  $entry_id2 = $otbhelper->getPreviousSubmittedAppEntryId($entry_id, $form_id);
  $attachments_details2 = mf_get_attachments_details($dbh,$form_id,$entry_id2,$param);
?>
<div class="panel panel-default">
    <div class="panel-heading panel-heading-noradius">
      <h5 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion_inner" href="#attachments1">
            
             <?php if($entry_id2 != 0){ ?>
              <?php echo __('Click to View Revised Attachments') ?>  
              <button class="btn btn-small btn-success">(<?php echo __('New files') ?>*)</button>
            <?php } else { ?>
               <?php echo __('Click to View & Download Attachments') ?>  
            <?php } ?>
        </a>
      </h5>
    </div>
    <div id="attachments1" class="panel-collapse panel-bordered collapse collapse">
        <div class="panel-body">
            <a href="<?php echo public_path() ?>backend.php/applications/downloadZip?form_id=<?php echo $form_id ?>&entry_id=<?php echo $entry_id ?>&param=<?php echo $param ?>"class="btn btn-primary"> Download All Attachments </a>
<br>

<div class="table-scrollable">
    <table class="table table-hover">
    <thead>
    <tr>
        <th>
            Details
        </th>
        <th>
             
        </th>
        
    </tr>
    </thead>
    <tbody>
    <?php
    
    //Print Out Application Details 
    foreach ($attachments_details as $data){
    ?>  
    <tr>
        <td> <label class="col-sm-4"><i class="bold-label"><?php echo $data['label']; ?></i></label></td>
       <td> <?php if($data['value']){ echo nl2br($data['value']); }else{ echo "-"; } ?></td>
    </tr>
<?php } ?>
    </tbody>
    </table>
            </div>

        </div>
     </div>
</div>

 <?php if($entry_id2 != 0 ): ?>
<div class="panel panel-default">
    <div class="panel-heading panel-heading-noradius">
      <h5 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion_inner" href="#attachments2">
            <?php echo __('Click to View Old Attachments') ?> 
            
            <button class="btn btn-small btn-danger">(<?php echo __('Old files') ?>*) </button>
        </a>
      </h5>
    </div>
    <div id="attachments2" class="panel-collapse panel-bordered collapse collapse">
        <div class="panel-body">
            <a href="<?php echo public_path() ?>backend.php/applications/downloadZip?form_id=<?php echo $form_id ?>&entry_id=<?php echo $entry_id2 ?>&param=<?php echo $param ?>"class="btn btn-primary"> <?php echo __('Download All') ?> </a>
<br>

    <div class="table-scrollable">
    <table class="table table-hover">
    <thead>
    <tr>
        <th>
            Details
        </th>
        <th>
             
        </th>
        
    </tr>
    </thead>
    <tbody>
    <?php
    //Print Out Application Details 
    foreach ($attachments_details2 as $data2){
    ?>  
   <tr>
       <td> <label class="col-sm-4"><i class="bold-label"><?php //echo $data2['label']; ?></i></label> </td>
        <td><?php if($data2['value']){ echo nl2br($data2['value']); }else{ echo "-"; } ?></td>
    </tr>
<?php } ?>

   </tbody>
								</table>
							</div>





        </div>





     </div>
</div>
<?php endif; ?>

  