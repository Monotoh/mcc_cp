<?php
	/**
	 * _viewbuttons.php partial.
	 *
	 * Display control buttons that manipulate the application
	 *
	 * @package    backend
	 * @subpackage applications
	 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
	 */

	/**
	*
	* Function to get all the days between a period of time
	*
	* @param String $sStartDate Start date to begin fetching dates from
	* @param String $sEndDate End date where to stop fetching dates from
	*
	* @return String[]
	*/
	use_helper('I18N');
?>
<!-- OTB patch- Check parent submission and hide action buttons for revisions -->
<?php if($application->getParentSubmission() == 0) : ?>

<div class="btn-group mr5 pull-left">
  <button type="button" class="btn btn-primary">Choose Action</button>
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu" role="menu">
		<?php
		if($application->getParentSubmission() == "0")
		{
			 $rejected = false;

	     if($application->getDeclined())
	     {
	        $rejected = true;
	     }

        $q = Doctrine_Query::create()
             ->from('SubMenuButtons a')
             ->where('a.sub_menu_id = ?', $application->getApproved());
        $submenubuttons = $q->execute();
        foreach($submenubuttons as $submenubutton)
        {
            $q = Doctrine_Query::create()
                 ->from('Buttons a')
                 ->where('a.id = ?', $submenubutton->getButtonId());
            $buttons = $q->execute();

            foreach($buttons as $button)
            {
                if($sf_user->mfHasCredential("accessbutton".$button->getId()))
                {
                    if($button->getLink() == "/backend.php/circulations/generatepermit?language=en&permit=1&")
                    {
                        ?>
                        <li><a onClick="if(confirm('Are you sure?')){ window.location='<?php echo $button->getLink(); ?>&entryid=<?php echo $application->getId(); ?>&form_id=<?php echo $form_id; ?>&id=<?php echo $entry_id; ?>'; }else{ return false; }"><?php echo $button->getTitle(); ?></a></li>
                        <?php
                        if($application->getApproved() != "850")
                        {
                        ?>
												<li><a class="btn btn-primary" onClick="if(confirm('Are you sure?')){ window.location='/backend.php/applications/editpermit/id/<?php echo $application->getId(); ?>'; }else{ return false; }"><?php echo __('Edit and Save Permit'); ?></a></li>
                        <?php
                        }

                    }
                    else
                    {
                        $pos = strpos($button->getLink(), "decline");
                        if($pos === false)
                        {

													$pos = strpos($button->getTitle(), "delete");
													if($pos === false)
													{
														?>
														<li><a  onClick="if(confirm('Are you sure?')){window.location='<?php echo $button->getLink(); ?>&entryid=<?php echo $application->getId(); ?>&form_id=<?php echo $form_id; ?>&id=<?php echo $entry_id; ?>'; }else{ return false; }"><?php echo $button->getTitle(); ?></a></li>
														<?php
													}
													else
													{
														?>
														<li><a  onClick="if(confirm('Are you sure?')){ window.location='<?php echo $button->getLink(); ?>&entryid=<?php echo $application->getId(); ?>&form_id=<?php echo $form_id; ?>&id=<?php echo $entry_id; ?>'; }else{ return false; }"><?php echo $button->getTitle(); ?></a></li>
														<?php
													}

                        }
                        else
                        {
                            ?>
														<li><a  onClick="if(confirm('Are you sure?')){ window.location='<?php echo $button->getLink(); ?>&entryid=<?php echo $application->getId(); ?>&form_id=<?php echo $form_id; ?>&id=<?php echo $entry_id; ?>'; }else{ return false; }"><?php echo $button->getTitle(); ?></a></li>
                            <?php
                        }


                    }

                }

            }

        }
		}
	?>
  </ul>
</div>

<!--OTB Start - Allow authorized users to send any stage (Previously possible in old system)-->
<div class="btn-group mr5 pull-left">
  <button type="button" class="btn btn-warning">Move To</button>
  <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu" role="menu">
<?php
if($sf_user->mfHasCredential('access_settings') or $sf_user->mfHasCredential('can_move_to_any_stage'))
{
	//Get list of all stages, starting with parent stages
	$q = Doctrine_Query::create()
	  ->from('SubMenus a')
	  ->where('a.id = ?', $application->getApproved());
	$current_stage = $q->fetchOne();

	$q = Doctrine_Query::create()
	  ->from('Menus a')
	  ->where('a.id = ?', $current_stage->getMenuId())
	  ->orderBy('a.order_no ASC');
	$stagegroups = $q->execute();
	foreach($stagegroups as $stagegroup)
	{
	   //Filter only stages that the current user has privileges for
		//error_log("Debug: Move to debug get menu title is >> ".$stagegroup->getTitle());
		//exit;
	   if($sf_user->mfHasCredential('accessmenu'.$stagegroup->getId()))
	   {
		   
			$q = Doctrine_Query::create()
			  ->from('SubMenus a')
			  ->where('a.menu_id = ? AND a.deleted = 0', $stagegroup->getId())
			  ->orderBy('a.order_no ASC');
			$stages = $q->execute();
			
			$stagecontent = "";
			
			//Get of list of all child stages under this parent stage
			foreach($stages as $stage)
			{
				//error_log("Debug: Stage to debug get menu title is >> ".$stage->getTitle());
			   //Filter only stages that the current user has privileges for
			   if($sf_user->mfHasCredential('accesssubmenu'.$stage->getId()))
			   {
					$selected = "";
					//Highlighted currently filtered stage as selected
					if($_GET['filter'] != "" && $_GET['filter'] == $stage->getId())
					{
						$selected = "selected";
					}
					?>
					<!--<li><a  onClick="if(confirm('Are you sure you want to move this application to \'<?php //echo $stage->getTitle();?>\'?')){ window.location = '/backend.php/applications/moveapp/id/<?php //echo $application->getId(); ?>/stage/<?php //echo $stage->getId();?>'; }"><?php //echo $stage->getTitle(); ?></a></li> -->
					<li> <a  href="/backend.php/applications/moveapp/id/<?php echo $application->getId(); ?>/stage/<?php echo $stage->getId();?>" > 
					<?php echo $stage->getTitle(); ?>
					 </a> </li>
					<?php
				}
			}
			
			//This prevents parent stages from showing if they don't have any child stages
			/*if($stagecontent != "")
			{
				echo "<optgroup label='".$stagegroup->getTitle()."'>";
					
				echo $stagecontent;
					
				echo "</optgroup>";
			}*/
			
		}
	}
}
?>
  </ul>
</div>
<!--OTB End - Allow authorized users to send any stage (Previously possible in old system)-->


<div class="btn-group mr5 pull-left">
  <button type="button" class="btn btn-success">Download</button>
  <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu" role="menu">
		<?php
		//OTB Patch Start - For Old Permits in Kigali System, show link to download them
		$q = Doctrine_Query::create()
			 ->from('AttachedPermit a')
			 ->where('a.application_id = ?', $application->getId());
		$attachedpermits = $q->execute();

		foreach($attachedpermits as $attachedpermit)
		{
			?>
			<li><a  onClick="window.location = '/backend.php/forms/viewentry2?id=<?php echo $attachedpermit->getFormId(); ?>&entryid=<?php echo $attachedpermit->getEntryId(); ?>';"><?php echo $application->getApplicationId()." ".__('Permit'); ?></a></li>
			<?php
		}
		//OTB Patch End - For Old Permits in Kigali System, show link to download them
		$q = Doctrine_Query::create()
			->from("SavedPermit a")
			->where("a.application_id = ?", $application->getId())
			->andWhere("a.permit_status <> 0");
		$permits = $q->execute();

		foreach($permits as $permit)
		{
			$q = Doctrine_Query::create()
					->from('Permits a')
					->where('a.id = ?', $permit->getTypeId());
			$permittype = $q->fetchOne();

			?>
			<li><a  onClick="window.location = '/backend.php/permits/view/id/<?php echo $permit->getId(); ?>';"><?php echo /*Start OTB PATCH - Cancelled Permit*/($permit->getPermitStatus()==3 ? "Cancelled >> " : "")./*Start OTB PATCH - Cancelled Permit*/$permittype->getTitle()." (".$permit->getDateOfIssue().")"; ?></a></li>
			<?php
		}
		?>
  </ul>
</div>

<div class="btn-group pull-right">

			<?php

			        if($submenu)
			        {
			            if($sf_user->mfHasCredential("sendnotification"))
			            {
			                $q = Doctrine_Query::create()
			                    ->from('Notifications a')
			                    ->where('a.submenu_id = ?', $submenu->getId());
			                $notification = $q->fetchOne();

			                if($notification)
			                {
			                    if($notification->getAutoSend() == "0")
			                    {
			                        ?>
			                        <a  class="btn btn-primary" onClick="window.location='/backend.php/applications/sendnotification/application/<?php echo $application->getId(); ?>';"><?php echo __('Send Notification'); ?></a>
			                        <?php
			                    }
			                }
			            }
			        }
        ?>
            <?php 
    //OTB patch - Allow a user to reset task status 
    $otbhelper = new OTBHelper();
    if($sf_user->mfHasCredential("resettask")):
    ?>
    <?php if($otbhelper->checkAppHasTasksCompletedAndIsInReview($application->getId())): ?>
       <a title="Reset All tasks to pending for reviewers to edit. Note: For specific tasks, kindly go to Tasks menu and search the task from Completed tasks tab" class="btn btn-warning" href="/backend.php/tasks/resetAllTasks?app_id=<?php echo $application->getId() ?> " >
        <?php echo __("Reset Tasks"); ?>
    </a>
    <?php endif; ?> <?php endif; ?>                                       
                                                
                                                
	<?php
	if($sf_user->mfHasCredential("editapplication"))
	{
	?>
		<a  title="<?php echo __('Edit Application'); ?>" class="btn btn-primary" onClick="window.location='/backend.php/applications/edit?form_id=<?php echo $application->getFormId(); ?>&id=<?php echo $application->getEntryId(); ?>';"><span class="glyphicon glyphicon-edit"></span> Edit Application</a>
        <?php
            $q = Doctrine_Query::create()
               ->from("FormEntryLinks a")
               ->where("a.formentryid = ?", $application->getId());
            $links = $q->execute();
            foreach($links as $link)
            {
                $q = Doctrine_Query::create()
                   ->from("SfGuardUserCategoriesForms a")
                   ->where("a.formid = ?", $link->getFormId());
                $sharedform = $q->fetchOne();
                //if($sharedform)
                if(false)
                {
                    ?>
                    <a  title="<?php echo __('Edit'); ?> <?php echo $sharedform->getIslinkedtitle(); ?>" class="btn btn-primary" onClick="window.location='/backend.php/applications/shareedit/id/<?php echo $application->getId(); ?>';"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                    <?php

                }
            }
        }
        ?>
	<?php

					if($sf_user->mfHasCredential("transfer_applications")) {
                    ?>
                    <div class="btn-group pull-right">
              <a class="btn btn-primary" onClick="window.location='/backend.php/applications/transfer?form_id=<?php echo $application->getFormId(); ?>&id=<?php echo $application->getId(); ?>';"><?php echo __('Transfer Application'); ?></a>
            </div>
         <?php }

	  $taskmanager = new TasksManager();

		$is_free = $taskmanager->can_pick_task($application->getId(), $_SESSION["SESSION_CUTEFLOW_USERID"]);

	  $q = Doctrine_Query::create()
		->from('SubMenuTasks a')
		->where('a.sub_menu_id = ?', $application->getApproved());

	 $allowedtasks = $q->execute();
	 foreach($allowedtasks as $allowedtask)
	 {
			if($allowedtask->getTaskId() == "2" && $is_free || $sf_user->mfHasCredential("assigntask"))
			{
				?>
                    <a  title="click this button to make comments on this project. Clicking this button will create a task that you can work on and submit comments without assigning tasks" class="btn btn-danger" onClick="window.location='/backend.php/tasks/starttask/application/<?php echo $application->getId(); ?>/type/2';"><?php echo __('Start');?> <?php echo __('Assessment'); ?></a>
				<?php
			}

			if($allowedtask->getTaskId() == "6" && $is_free)
			{
				?>
				<a  class="btn btn-primary" onClick="window.location='/backend.php/tasks/starttask/application/<?php echo $application->getId(); ?>/type/6';"><?php echo __('Start');?> <?php echo __('Inspection'); ?></a>
				<?php
			}

			//if($allowedtask->getTaskId() == "3" && $is_free)
			if($allowedtask->getTaskId() == "3")//OTB Patch - Allow invoicing to be done regardless of whether a user can pick a task or not.
			{
                $q = Doctrine_Query::create()
                   ->from("MfInvoice a")
                   ->where("a.app_id = ?", $application->getId())
                   ->andWhere("a.paid = 15");
                $invoices = $q->execute();
                $disabled = false;
                if(sizeof($invoices) > 0)
                {
                   $disabled = true;
                }
				?>
				<!--<a class="btn btn-primary" <?php if($disabled){ echo "disabled='disabled'"; } ?> onClick="window.location='/backend.php/tasks/starttask/application/<?php echo $application->getId(); ?>/type/3';"><?php echo __('Start');?> <?php echo __('Invoicing'); ?></a>-->
				<a  class="btn btn-primary" <?php if($disabled){ echo "disabled='disabled'"; } ?> onClick="window.location='/backend.php/tasks/invoice/id/0/application/<?php echo $application->getId(); ?>';"><?php echo __('Start');?> <?php echo __('Invoicing'); ?></a><!--OTB Patch - Use same invoicing URL as that of the invoicing icon in list of applications.-->
				<?php
			}

			if($allowedtask->getTaskId() == "4" && $is_free)
			{
				?>
				<a  class="btn btn-primary" onClick="window.location='/backend.php/tasks/starttask/application/<?php echo $application->getId(); ?>/type/4';"><?php echo __('Start');?> <?php echo __('Scanning'); ?></a>
				<?php
			}

			if($allowedtask->getTaskId() == "5" && $is_free)
			{
				?>
				<a  class="btn btn-primary" onClick="window.location='/backend.php/tasks/starttask/application/<?php echo $application->getId(); ?>/type/5';"><?php echo __('Start');?> <?php echo __('Collection'); ?></a>
				<?php
			}
		}

		$q = Doctrine_Query::create()
			->from('SubMenus a')
			->where('a.id = ?', $application->getApproved());

		$stage = $q->fetchOne();

	 if($sf_user->mfHasCredential("assigntask") && $rejected == false && ($stage->getStageType() == 2 || $stage->getStageType() == 8))
	 {
		?>
		<a class="btn btn-primary" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-user"></span><?php echo __('Assign Application'); ?></a>
		<?php
	 }
?>
</div>
<?php endif; ?>