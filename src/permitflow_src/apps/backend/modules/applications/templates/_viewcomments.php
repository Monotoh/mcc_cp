<?php
/**
 * _viewcomments.php partial.
 *
 * Display comments submitted by reviewers #may eventually decide to merge this with the _viewreviewers if i manage to seperate individual reviewer comments
 *
 * @package    backend
 * @subpackage applications
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");

//get form id and entry id
$form_id  = $application->getFormId();
$entry_id = $application->getEntryId();
?>
<div class="panel-group mb0" id="accordion">
	<div class="panel panel-default">
		<div class="panel-heading  panel-heading-noradius">
        	<h4 class="panel-title">
            	<a data-toggle="collapse" data-parent="#accordion" href="#commentsReviewers">
            	<?php echo __("Reviewer Comments"); ?>
                </a>
                </h4>
              </div>
              <div id="commentsReviewers" class="panel-collapse collapse in">
                <div class="panel-body panel-body-nopadding">
        				<?php
        				//Show comments submitted by individual reviewers #Categorized
        				include_partial('applications/comments_reviewer', array('application' => $application, 'form_id' => $form_id, 'entry_id' =>  $entry_id));
        				?>
        				</div>
              </div>
            </div>

  <div class="panel panel-default">
    <div class="panel-heading panel-heading-noradius">
          <h4 class="panel-title">
              <a data-toggle="collapse" class="collapsed"  data-parent="#accordion" href="#commentsDeclines">
              <?php echo __("Previous Reasons for Decline"); ?>
                </a>
                </h4>
              </div>
              <div id="commentsDeclines" class="panel-collapse collapse">
                <div class="panel-body">
        <?php
        //Check if this application has been previously declined before
        include_partial('applications/comments_declines', array('application' => $application, 'form_id' => $form_id, 'entry_id' =>  $entry_id));
        ?>
        </div>
              </div>
            </div>

  <div class="panel panel-default">
    <div class="panel-heading panel-heading-noradius">
          <h4 class="panel-title">
              <a data-toggle="collapse" class="collapsed"  data-parent="#accordion" href="#commentsConditions">
              <?php echo __("Conditions of Approval"); ?>
                </a>
                </h4>
              </div>
              <div id="commentsConditions" class="panel-collapse collapse">
                <div class="panel-body">
        <?php
        //Check if this application has been previously declined before
        include_partial('applications/comments_conditions', array('application' => $application, 'form_id' => $form_id, 'entry_id' =>  $entry_id));
        ?>
        </div>
              </div>
            </div>
  <div class="panel panel-default">
    <div class="panel-heading  panel-heading-noradius">
          <h4 class="panel-title">
              <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#commentsSummary">
              <?php echo __("Comments Summary"); ?>
                </a>
                </h4>
              </div>
              <div id="commentsSummary" class="panel-collapse collapse">
                <div class="panel-body panel-body-nopadding">
        <?php
        //Summarize all comments and show all negative comments that may make this application become declined
        include_partial('applications/comments_summary', array('application' => $application, 'form_id' => $form_id, 'entry_id' =>  $entry_id));
        ?>
        </div>
              </div>
            </div>
<!--OTB Start: Show Approval Validity-->
<?php
        $q = Doctrine_Query::create()
            ->from('PermitCache a')
            ->where('a.application_id = ?', $application->getId());
        $cache_permits = $q->execute();
?>
  <div class="panel panel-default">
    <div class="panel-heading  panel-heading-noradius">
          <h4 class="panel-title">
              <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#ApprovalValidity">
              <?php echo __("Approval Validity Period"); ?>
                </a>
                </h4>
              </div>
              <div id="ApprovalValidity" class="panel-collapse collapse">
                <div class="panel-body panel-body-nopadding">
				<table class="table table-bordered">
				<tr>
				<th><?php echo __("Permit"); ?></th>
				<th><?php echo __("Recommended Validity Period"); ?></th>
				<th><?php echo __("Unit"); ?></th>
				<th><?php echo __("Permit Generation Status"); ?></th>
				</tr>
        <?php
		foreach($cache_permits as $validity){
		?><tr style="background-color: <?php echo $validity->getStatus()=="done" ? "#f4fef4" : "#ffe5e5";?>;">
		<td><?php echo $validity->getPermitTemplate()->getTitle();?></td>
		<td><?php echo $validity->getValidityPeriod();?></td>
		<td><?php echo $validity->getPeriodUom();?></td>
		<td><?php echo $validity->getStatus();?></td>
		</tr><?php
		}
        ?>
		</table>
        </div>
              </div>
            </div>
<!--OTB End: Show Approval Validity-->
</div>
