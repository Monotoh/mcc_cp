<?php
/**
 * _viewdetails.php partial.
 *
 * Displays application details
 *
 * @package    backend
 * @subpackage applications
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

//get form id and entry id
$form_id  = $application->getFormId();
$entry_id = $application->getEntryId();

$prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";

require_once($prefix_folder.'config.php');
require_once($prefix_folder.'includes/db-core.php');
require_once($prefix_folder.'includes/helper-functions.php');

require_once($prefix_folder.'includes/entry-functions.php');


$nav = trim($_GET['nav']);

if(empty($form_id) || empty($entry_id)){
    echo '<div class="alert alert-error fade in nomargin">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
          <h4>Submission Error!</h4>
          <p>An error occurred during the submission of this application. Use the below recovery tool to recreate the application with the help of the client or tell the client to edit the application from their account.</p>
        </div>';
    error_log("Debug-e: Application without ID");

    $application_manager = new ApplicationManager();

    if($application->getFormId())
    {
      $form = $application_manager->create_adhoc_edit_by_form($application->getId(), $application->getFormId());
      echo $form;
    }
    elseif($application->getApproved() != 0) {
      $form = $application_manager->create_adhoc_edit_by_stage($application->getId(), $application->getApproved());
      echo $form;
    }
    else
    {
      if($choosen_form)
      {
        $form = $application_manager->create_adhoc_edit_by_form($application->getId(), $choosen_form);
      }
      else
      {
        $form = $application_manager->create_adhoc_chooser($application->getId());
        echo $form;
      }
    }
}
else {
    $dbh = mf_connect_db();

//get entry details for particular entry_id
    $param['checkbox_image'] = '/assets_unified/images/59_blue_16.png';
    $entry_details = mf_get_entry_details($dbh, $form_id, $entry_id, $param, $sf_user->getCulture());
 ?>
<div class="row">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box default">
						
						<div class="portlet-body">
							<div class="table-scrollable">
								<table class="table table-hover">
								<thead>
								<tr>
									<th>
										Details
									</th>
									<th>
										 
									</th>
									
								</tr>
								</thead>
								<tbody>
								
                                    
                                <?php
                            //Print Out Application Details
                                foreach ($entry_details as $data) {
                                    ?>
                                    <tr>
                                    <?php
                                    if ($data['element_type'] == "section") {
                                        ?>
                                        <td>
                                        
                                            <label class="col-sm-12"><u> <?php echo $data['label']; ?> </u></label>
                                        </td>

                                    <?php
                                    } elseif ($data['element_type'] == "page_break") {

                                    } else {
                                        ?>
                                         <td>
                                            <label class="col-sm-2 control-label"><i class="bold-label-2"><?php echo $data['label'].":"; ?></i></label>

                                            <div class="col-sm-8"><?php if ($data['value']) {
                                                    echo nl2br($data['value']);
                                                } else {
                                                    echo "-";
                                                } ?></div>
                                        </td>
                                        </tr>
                                    <?php
                                    }
                                } ?>
									
								
								
								</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
                </div> 
                
</div>



<?php }
?>
