<?php
/**
 * _viewcomments.php partial.
 *
 * Display comments submitted by reviewers #may eventually decide to merge this with the _viewreviewers if i manage to seperate individual reviewer comments
 *
 * @package    backend
 * @subpackage applications
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");

//get form id and entry id
$form_id  = $application->getFormId();
$entry_id = $application->getEntryId();
?>
<div style="margin: 10px;">

   <button class="btn btn-primary pull-right" type="button" style="margin-top: -15px;" onClick="window.location='/backend.php/applications/view/id/<?php echo $application->getId(); ?>'">
   Reset
   </button>
   <button class="btn btn-primary pull-right" type="button" style="margin-top: -15px; margin-right: 5px;" data-toggle="modal" data-target="#auditModal">
   Filter
   </button>

<div class="mb30"></div>
<p>This is a security log of activity on this application.</p>
    <?php

   if($fromdate)
   {
     $q = Doctrine_Query::create()
        ->from("AuditTrail a")
        ->where("a.form_entry_id = ?", $application->getId())
        ->andWhere("a.action_timestamp BETWEEN ? AND ?", array($fromdate." ".$fromtime, $todate." ".$totime))
        ->orderBy("a.id DESC");
   }
   else
   {
     $q = Doctrine_Query::create()
        ->from("AuditTrail a")
        ->where("a.form_entry_id = ?", $application->getId())
        ->orderBy("a.id DESC");
    }

    $apppager = new sfDoctrinePager('AuditTrail', 500);
    $apppager->setQuery($q);
    $apppager->setPage($apppage);
    $apppager->init();
   ?>
    <table class="table table-bordered" id="datatable-responsive">
   <thead>
    <th><?php echo __('Client IP') ?></th>
    <th><?php echo __('Reviewer') ?></th>	
    <th><?php echo __('Activity') ?></th>
    <th><?php echo __('Date') ?></th> 
    <th><?php echo __('Device') ?></th>
   </thead>
   <tbody>
   <?php foreach($apppager->getResults() as $audit){ ?>
    <tr>
    <td><?php echo $audit->getIpaddress(); ?></td>
    <td><?php
      $q = Doctrine_Query::create()
         ->from("CfUser a")
         ->where("a.nid = ?", $audit->getUserId());
      $reviewer = $q->fetchOne();
      if($reviewer)
      {
        echo $reviewer->getStrfirstname()." ".$reviewer->getStrlastname();
      }
     ?></td>
    <td style="pointer-events: none; cursor: default;"><?php echo ucfirst($audit->getAction()); ?></td>
    <td><?php echo $audit->getActionTimestamp(); ?></td>
     <td><?php echo $audit->getHttpAgent(); ?></td> 
    </tr>
    <?php } ?>
       </tbody>
   </table>
<!--OTB Start: Movement History -->
<div class="mb30"></div>
<p>This is a log of movement of this application.</p>
	<?php
		 $q = Doctrine_Query::create()
			->from("ApplicationReference a")
			->where("a.application_id = ?", $application->getId())
			->orderBy("a.id DESC");
		$move_history = $q->execute();
	?>
   <table class="table table-bordered">
   <thead>
   <tr><th>Movement</th><th>Client/Reviewer</th>	<th>Date</th> </tr>
   </thead>
   <tbody>
   <?php foreach($move_history as $ref){ ?>
	<tr>
	<td><?php
	if($ref->getApprovedBy()){
		  $q = Doctrine_Query::create()
			 ->from("CfUser a")
			 ->where("a.nid = ?", $ref->getApprovedBy());
		  $reviewer = $q->fetchOne();
		  if($reviewer)
		  {
			echo $reviewer->getStrfirstname()." ".$reviewer->getStrlastname();
		  }
	  }else{
		  $q = Doctrine_Query::create()
			 ->from("SfGuardUserProfile a")
			 ->where("a.user_id = ?", $application->getUserId());
		  $applicant = $q->fetchOne();
		  if($applicant)
		  {
			echo $applicant->getFullname()." (Applicant)";
		  }		  
	  }
	 ?></td>
		<?php $q = Doctrine_Query::create()
			->from("SubMenus a")
			->where("a.id = ?", $ref->getStageId())
			->orderBy("a.id DESC");
		$stage = $q->fetchOne();?>
	<td><?php echo "Moved to ".$stage->getTitle(); ?></td>
	<td><?php echo $ref->getStartDate(); ?></td>
	</tr>
   <?php } ?>
	</tbody>
	</table>
<!--OTB End: Movement History -->
</div>


<!-- Modal -->
<div class="modal fade" id="auditModal" tabindex="-1" role="dialog" aria-labelledby="auditModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="#" method="post" enctype="multipart/form-data">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Filter Activity Log</h4>
      </div>
      <div class="modal-body">
          <label>From</label>
          <div class="input-group">
            <input type="text" id="from-multiple" name="fromdate" placeholder="mm/dd/yyyy" class="form-control">
            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
          </div>
          <div class="input-group mb15">
            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
            <div class="bootstrap-timepicker"><input id="timepicker1" name="fromtime" type="text" class="form-control"/></div>
          </div>
          <label>To</label>
          <div class="input-group">
            <input type="text" id="to-multiple" name="todate" placeholder="mm/dd/yyyy" class="form-control">
            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
          </div>
          <div class="input-group mb15">
            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
            <div class="bootstrap-timepicker"><input id="timepicker2" name="totime" type="text" class="form-control"/></div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </form>
    </div><!-- modal-content -->
  </div><!-- modal-dialog -->
</div><!-- modal -->

<?php include_component('dashboard', 'javascriptsdatatable'); ?>
<script>
jQuery(document).ready(function(){

  jQuery('#from-multiple').datepicker({
    numberOfMonths: 3,
    showButtonPanel: true
  });

  jQuery('#to-multiple').datepicker({
    numberOfMonths: 3,
    showButtonPanel: true
  });

  // Time Picker
  jQuery('#timepicker1').timepicker({showMeridian: false});
  jQuery('#timepicker2').timepicker({showMeridian: false});


});
</script>
