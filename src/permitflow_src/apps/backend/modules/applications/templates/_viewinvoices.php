<?php
	/**
	 * _viewinvoices.php partial.
	 *
	 * Displays any invoices attached to an application
	 *
	 * @package    backend
	 * @subpackage applications
	 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
	 */
    use_helper("I18N");

	//get form id and entry id
	$form_id  = $application->getFormId();
	$entry_id = $application->getEntryId();

?>
<div class="panel-group panel-group-dark mb0" id="accordion1">
<?php
	//Iterate through any invoices attached to this application
    $invcount = 0;
    $invtotal = 0;

    $invoice_manager = new InvoiceManager();

    $q = Doctrine_Query::create()
       ->from("MfInvoice a")
       ->where("a.app_id = ?", $application->getId())
       ->orderBy("a.id DESC");
    $invoices = $q->execute();

    foreach($invoices as $invoice)
    {
		//Display information about each invoice
        $invcount++;
        ?>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" class="collapsed" data-parent="#accordion1" href="#collapseTwo<?php echo $invoice->getId(); ?>">
                    <?php echo __("Invoice"); ?> <?php echo ((sizeof($invoices) + 1) - $invcount); ?> (<?php
                    $expired = false;

                    $db_date_event = str_replace('/', '-', $invoice->getExpiresAt());

                    $db_date_event = strtotime($db_date_event);

                    if (time() > $db_date_event && !($invoice->getPaid() == "15" || $invoice->getPaid() == "2" || $invoice->getPaid() == "3"))
                    {
                        var_dump("AM Hear");
                         $expired = true;
                    }

                    if($expired)
                    {
                        echo "Expired";
                    }
                    else
                    {
                        if($invoice->getPaid() == "1"){
                           echo __("Pending");
                        }else if($invoice->getPaid() == "15"){
                          echo "Pending Confirmation";
                        }elseif($invoice->getPaid() == "2"){
                          echo __("Paid");
                        }elseif($invoice->getPaid() == "3"){
                            echo __("Cancelled");
                        }
                    }
                    ?>) <?php if($invoice->getExpiresAt()){ echo "Expires on ".$invoice->getExpiresAt(); } ?>
                  </a>
                </h4>
              </div>
              <div id="collapseTwo<?php echo $invoice->getId(); ?>" class="panel-collapse collapse <?php if($invcount == 1){ ?> in <?php } ?>">
                <div class="panel-body">

                <?php
                    try {
                        $html = $invoice_manager->generate_invoice_template($invoice->getId(), false);
                    }
                    catch(Exception $ex)
                    {
                        error_log("Debug-t: Invoice Parse Error: ".$ex);

                        $html = $invoice_manager->generate_invoice_template_old_parser($invoice->getId(), false);
                    }

          			echo $html;
          		?>
                <h4><u>Invoice Details</u></h4>

                <form class="form-bordered">
                <!--<div class="form-group">
                    <label class="col-sm-4">
                    <i class="bold-label">
                        MDA Code
                    </i></label>
                    <div class="col-sm-8">
                        <?php //echo $invoice->getMdaCode(); ?>
                    </div>
                </div>-->
                <div class="form-group">
                    <label class="col-sm-4">
                    <i class="bold-label">
                        Service Code
                    </i></label>
                    <div class="col-sm-8">
                        <?php echo $invoice->getServiceCode(); ?>
                    </div>
                </div>
                <!--<div class="form-group">
                    <label class="col-sm-4">
                    <i class="bold-label">
                        Branch
                    </i></label>
                    <div class="col-sm-8">
                        <?php //echo $invoice->getBranch(); ?>
                    </div>
                </div>-->
                <div class="form-group">
                    <label class="col-sm-4">
                    <i class="bold-label">
                        Invoice Number
                    </i></label>
                    <div class="col-sm-8">
                        <?php echo $invoice->getInvoiceNumber(); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">
                    <i class="bold-label">
                        Due Date
                    </i></label>
                    <div class="col-sm-8">
                        <?php echo $invoice->getDueDate(); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">
                    <i class="bold-label">
                        Expires On
                    </i></label>
                    <div class="col-sm-8">
                        <?php echo $invoice->getExpiresAt(); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">
                    <i class="bold-label">
                        Payer ID
                    </i></label>
                    <div class="col-sm-8">
                        <?php echo $invoice->getPayerId(); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">
                    <i class="bold-label">
                        Payer Name
                    </i></label>
                    <div class="col-sm-8">
                        <?php echo $invoice->getPayerName(); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">
                    <i class="bold-label">
                        Total Amount
                    </i></label>
                    <div class="col-sm-8">
                        <?php echo $invoice->getTotalAmount(); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">
                        <i class="bold-label">
                            Balance Due
                        </i></label>
                    <div class="col-sm-8">
                        <?php echo $invoice_manager->get_invoice_total_owed($invoice->getId()); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">
                    <i class="bold-label">
                        Currency
                    </i></label>
                    <div class="col-sm-8">
                        <?php
                        $currency = "";

                        $q = Doctrine_Query::create()
                            ->from("Invoicetemplates a")
                            ->where("a.applicationform = ?", $application->getFormId())
                            ->limit(1);
                        $invoice_template = $q->fetchOne();

                        $q = Doctrine_Query::create()
                            ->from("ApForms a")
                            ->where("a.form_id = ?", $invoice_template->getApplicationform());
                        $form = $q->fetchOne();

                        if($form)
                        {
                          $currency = $form->getPaymentCurrency();
                        }
                        else
                        {
                          $currency = sfConfig::get("app_currency");
                        }

                        echo $currency;
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">
                    <i class="bold-label">
                        Document Reference Number
                    </i></label>
                    <div class="col-sm-8">
                        <?php echo $invoice->getDocRefNumber(); ?>
                    </div>
                </div>
				<!--OTB Start - Include Transaction Reference Number if present-->
                <div class="form-group">
                    <label class="col-sm-4">
                    <i class="bold-label text-capitalize">
                        <?php echo $form->getPaymentMerchant_Type()." ".__('Reference Number'); ?>
                    </i></label>
                    <div class="col-sm-8">
                        <?php
                        $currency = "";

                        $q = Doctrine_Query::create()
                            ->from("ApFormPayments a")
                            ->where("a.form_id = ?", $application->getFormId())
                            ->andWhere("a.record_id = ? and a.payment_status in ('pending','paid')", $application->getEntryId())
							->orderBy('a.afp_id, a.payment_date desc')
                            ->limit(1);
                        $transaction = $q->fetchOne();
                        echo $transaction ? $transaction->getPaymentId() : "n/a";
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">
                    <i class="bold-label text-capitalize">
                        <?php echo $form->getPaymentMerchant_Type()." ".__('Payment Status'); ?>
                    </i></label>
                    <div class="col-sm-8">
                        <?php
                        echo $transaction ? $transaction->getPaymentStatus() : "Transaction not yet posted";
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">
                    <i class="bold-label text-capitalize">
                        <?php echo $form->getPaymentMerchant_Type()." ".__('Payment Date'); ?>
                    </i></label>
                    <div class="col-sm-8">
                        <?php
                        echo $transaction ? $transaction->getPaymentDate() : "Transaction not yet posted";
                        ?>
                    </div>
                </div>
				<!--OTB End - Include Transaction Reference Number if present-->
              </form>


            <div class="btn-invoice row col-lg-12">
                <?php
                if($invoice->getDocumentKey())
                {
                    ?>
                    <button class="btn btn-primary" id="printinvoice" type="button"
                            onClick="window.location='<?php echo $invoice->getDocumentKey(); ?>';">
                        <i class="fa fa-print mr5"></i> <?php echo __('Download Invoice'); ?>
                    </button>
                <?php
                }
                else {
                    ?>
                    <a class="btn btn-primary" id="printinvoice"
                       href="/backend.php/invoices/print/id/<?php echo $invoice->getId(); ?>"><i
                            class="fa fa-print mr5"></i> <?php echo __("Print Invoice"); ?></a>
                <?php
                }

                if( ($invoice->getPaid() == "15" && $sf_user->mfHasCredential('approvepaymentoverride')))
                {
                ?>
                <!-- OTB patch - Check if Invoice Expired -->
                <?php if(!($expired)): ?>
                <!--<a onClick="if(confirm('Are you sure you want to confirm this invoice?')){ return true; }else{ return false; }" href="/backend.php/applications/confirmpayment/<?php //echo md5($invoice->getId()); ?>" class="btn btn-success" id="printinvoice" type="button"><i class="fa fa-paypal mr5"></i> <?php //echo __("Confirm Payment"); ?></a> -->
                <!-- OTB patch - Quick fix - not sure why use md5 on the invoice by web masters -->
                  <a onClick="if(confirm('Are you sure you want to confirm this invoice?')){ return true; }else{ return false; }" href="/backend.php/applications/confirmpayment/invoice_id_passed/<?php echo $invoice->getId(); ?>" class="btn btn-success" id="printinvoice" type="button"><i class="fa fa-paypal mr5"></i> <?php echo __("Confirm Payment"); ?></a>
                <?php endif; ?>
                <?php
                }

                if($invoice->getPaid() == 3 && $sf_user->mfHasCredential('approvepaymentoverride'))
                {
                    ?>
                    <a class="btn btn-white" id="makepayment" onClick="if(confirm('Are you sure you want to uncancel this invoice?')){ return true; }else{ return false; }" href="/backend.php/invoices/view/id/<?php echo $invoice->getId(); ?>/confirm/<?php echo md5($invoice->getId()); ?>"><i class="fa fa-print mr5"></i> <?php echo __('UnCancel Payment'); ?></a>
                <?php
                }



               /* if($invoice->getPaid() <> 2 && $invoice->getPaid() <> 3 && $sf_user->mfHasCredential('approvepaymentoverride')){
                    $sf_user->setAttribute('form_id', $invoice->getFormEntry()->getFormId());
                    $sf_user->setAttribute('entry_id', $invoice->getFormEntry()->getEntryId());
                    $sf_user->setAttribute('invoice_id', $invoice->getId());
                    ?>
                     <?php if(!($expired)): //OTB patch Do not pay expired invoices ?>
                    <button class="btn btn-white" id="makepayment" type="button" onClick="window.location='/backend.php/applications/payment';"><i class="fa fa-print mr5"></i> <?php echo __('Add Payment'); ?></button>
                    <?php endif; ?>
                        <?php
                }*/

                if($invoice->getPaid() <> 3 && $sf_user->mfHasCredential('approvepaymentsupport'))
                {
                    ?>
                    <a class="btn btn-danger" id="makepayment" onClick="if(confirm('Are you sure you want to cancel this invoice?')){ return true; }else{ return false; }" href="/backend.php/invoices/view/id/<?php echo $invoice->getId(); ?>/cancel/<?php echo md5($invoice->getId()); ?>"><i class="fa fa-print mr5"></i> <?php echo __('Cancel Payment'); ?></a>
                <?php
                }
                ?>
               <!-- OTB patch Button to allow support for Manual & Online Payment -->
            <?php if($invoice->getPaid() == 1 && $sf_user->mfHasCredential('allow_manual_payment')) : //allow only for pending invoices ?> 
                <!-- check if invoice expired -->
                <?php if($expired){ ?>
                   <div class="alert alert-danger">
                    <?php echo "Warning: This Invoice has expired! Send this application back 
                    to invoicing,re-generate the invoice and send the new invoice to the client for payment." ?>
                   </div>               
                <?php } else { ?>
                     <?php if($invoice->getAllowOnlineOrManualPayment() == 0){ ?>
                     <a href="/backend.php/invoices/manualOnline/invoice_id/<?php echo $invoice->getId() ?>/app_id/<?php  echo $application->getId()  ?>" class="btn btn-success"> <?php echo __("Set Payment to Manual") ?> </a> 
                     <?php  }else { ?> 
                    <a href="/backend.php/invoices/resetManualOnline/invoice_id/<?php  echo $invoice->getId() ?>/app_id/<?php  echo $application->getId()  ?>" class="btn btn-warning"> <?php echo __("Reset Payment to Online") ?> </a> 
                     <?php }
                     } 
                ?> 
            <?php endif; ?>
            </div>

            <div class="mb40"></div>


            </div>
           </div>
          </div>




            <div class="btn-invoice row col-lg-12">

                <ul>

                <?php
                    //Iterate through all receipts attached to this invoice
                    $count = 0;
                    foreach($invoice->getUploadReceipt() as $receipt)
                    {
                        $count++;
                        echo "<li><font style='font-weight: 900;'>".__('Receipt')." ".$count."</font><br> <a target='_blank' href='/backend.php/invoices/viewreceipt?form_id=".$receipt->getFormId()."&id=".$receipt->getEntryId()."'>(".__('View Receipt').")</a></li>";
                    }

                ?>

                </ul>
        </div>

        <?php

    }

	//If no invoices have been attached to this application
	if($invcount == 0)
	{
		echo "<table class=\"table mb0\">
			<tbody>
			<tr>
			<td><i class=\"bold-label\">".__('No records found')."</i></td>
			</tr>
			</tbody>
			</table>
";
	}

?>
</div>
