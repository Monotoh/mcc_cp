<?php
/**
 * _viewmessages.php partial.
 *
 * Displays a message trail between the client and the reviewers
 *
 * @package    backend
 * @subpackage applications
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
 use_helper("I18N");

?>

<!--<img src="http://maps.google.com/maps/api/staticmap?center=<?php //echo $taskform->getLatitude();?>,<?php //echo $taskform->getLongitude();?>&zoom=8&size=400x300&sensor=false" style="width: 400px; height: 400px;" />-->

<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyASFeM8hNHuFCGYp-XmtBavu1oicLX5uOs"></script>
<?php if($taskform->getLatitude() && $taskform->getLongitude()): ?>   
     <div id="tasklocationmap" style="width: 800px; height: 250px"></div> 
<?php endif; ?>
   <script type="text/javascript"> 
      var mapOptions = {
         zoom: 8,
         center: new google.maps.LatLng(<?php echo $taskform->getLatitude();?>, <?php echo $taskform->getLongitude();?>),
         mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      var map = new google.maps.Map(document.getElementById("tasklocationmap"), mapOptions);
		var infoWindow = new google.maps.InfoWindow;
                
     var marker = new google.maps.Marker({
          position: mapOptions.center,
          map: map,
          title: 'Task Location'
        });
        //var marker_position = { lat: -1.922247, lng: 29.838867 };
        //
       // addMarker(new google.maps.LatLng(marker_position, map));

      // Change this depending on the name of your PHP file
      downloadUrl("phpsqlajax_genxml.php", function(data) {
        var xml = data.responseXML;
        var markers = xml.documentElement.getElementsByTagName("marker");
        for (var i = 0; i < markers.length; i++) {
          var name = markers[i].getAttribute("name");
          var address = markers[i].getAttribute("address");
          var type = markers[i].getAttribute("type");
          var point = new google.maps.LatLng(
              parseFloat(markers[i].getAttribute("lat")),
              parseFloat(markers[i].getAttribute("lng")));
          var html = "<b>" + name + "</b> <br/>" + address;
          var icon = customIcons[type] || {};
          var marker = new google.maps.Marker({
            map: map,
            position: point,
            icon: icon.icon
          });
          bindInfoWindow(marker, map, infoWindow, html);
        }
      });

    function bindInfoWindow(marker, map, infoWindow, html) {
      google.maps.event.addListener(marker, 'click', function() {
        infoWindow.setContent(html);
        infoWindow.open(map, marker);
      });
    }

    function downloadUrl(url, callback) {
      var request = window.ActiveXObject ?
          new ActiveXObject('Microsoft.XMLHTTP') :
          new XMLHttpRequest;

      request.onreadystatechange = function() {
        if (request.readyState == 4) {
          request.onreadystatechange = doNothing;
          callback(request, request.status);
        }
      };

      request.open('GET', url, true);
      request.send(null);
    }

    function doNothing() {}

   </script> 