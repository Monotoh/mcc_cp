<?php
/**
 * _viewmessages.php partial.
 *
 * Displays a message trail between the client and the reviewers
 *
 * @package    backend
 * @subpackage applications
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
 use_helper("I18N");

//get form id and entry id
$form_id  = $application->getFormId();
$entry_id = $application->getEntryId();

//Get all messages attached to this application
$q = Doctrine_Query::create()
  ->from('Communication a')
  ->where('a.application_id = ?', $application->getId())
  ->orderBy('a.id ASC');
$memos = $q->execute();

//if($sf_user->mfHasCredential('incomingmessages'))
{
?>

    <form>
	<?php
	if(sizeof($memos) <= 0){
	?>
		<table class="table mb0">
			<tbody>
			<tr>
			<td><i class="bold-label">No internal memos found</i></td>
			</tr>
			</tbody>
			</table>
	<?php
	}
	else
	{
	?>
	<div class="panel panel-default panel-messages mt0 scrollable" data-height="450">
     <?php
		//Iterate through each memos and display them
	 	foreach($memos as $memo){
			if($memo->getSender() != "")
			{
				$q = Doctrine_Query::create()
				  ->from('CfUser a')
				  ->where('a.nid = ?', $memo->getSender())
				  ->orderBy('a.nid DESC');
				$member = $q->fetchOne();
				$name = $member->getStrfirstname()." ".$member->getStrlastname();
			?>
			  <div class="media media-client">
			        <span class="arrow"></span>
                    <div class="media-body">
                        <h4 class="text-primary"><?php echo $name; ?> </h4>
                        <?php echo html_entity_decode($memo->getMessage()); ?>
                        <small class="text-muted pull-right"><?php echo $memo->getCreatedOn(); ?></small>
                    </div>
                    <span class="clearfix"></span>
                </div><!-- media -->
      <?php
            }
		}
    ?></div><?php
	}
	?>
	 </form>

        <!-- Display form for reviewer to send memo/reply to other reviewers -->
        <form action="/backend.php/applications/view/id/<?php echo $application->getId(); ?>/memos/read" method="post"  autocomplete="off" data-ajax="false">
              <div class="panel panel-default timeline-post mb0">
                 <div class="panel-body pt0">
                      <textarea name='txtmemo' id="memo_wysiwyg" placeholder="<?php echo __("Type in your Memo Here"); ?>..." class="form-control" rows="10" data-autogrow="true"></textarea>
                 </div>
              </div>
               <div class="panel-footer">
                      <button type='submit' class="btn btn-primary pull-right"><?php echo __("Send"); ?> </button>
             </div>
         </form>
<?php
}
?>


<script src="<?php echo public_path(); ?>assets_unified/js/msg_ckeditor/ckeditor.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/msg_ckeditor/adapters/jquery.js"></script>

<script>
jQuery(document).ready(function(){

  // CKEditor
  jQuery('#memo_wysiwyg').ckeditor();


});
</script>
