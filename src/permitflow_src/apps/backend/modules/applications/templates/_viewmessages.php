<?php
/**
 * _viewmessages.php partial.
 *
 * Displays a message trail between the client and the reviewers
 *
 * @package    backend
 * @subpackage applications
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
 use_helper("I18N");

//get form id and entry id
$form_id  = $application->getFormId();
$entry_id = $application->getEntryId();

//Get all messages attached to this application
$q = Doctrine_Query::create()
  ->from('Communications a')
  ->where('a.application_id = ?', $application->getId())
  ->orderBy('a.id ASC');
$messages = $q->execute();

//if($sf_user->mfHasCredential('incomingmessages'))
{
?>

    <form>
	<?php
	if(sizeof($messages) <= 0){
	?>
		<table class="table mb0">
			<tbody>
			<tr>
			<td><i class="bold-label">No records found</i></td>
			</tr>
			</tbody>
			</table>
	<?php
	}
	else
	{
	?>
	<div class="panel panel-default panel-messages mt0 scrollable" data-height="450">
     <?php
		//Iterate through each message and display them
	 	foreach($messages as $message){
			if($message->getReviewerId() != "")
			{
				$q = Doctrine_Query::create()
				  ->from('CfUser a')
				  ->where('a.nid = ?', $message->getReviewerId())
				  ->orderBy('a.nid DESC');
				$member = $q->fetchOne();
				$name = $member->getStrfirstname()." ".$member->getStrlastname();
			}
			else if($message->getArchitectId() != "")
			{
				$q = Doctrine_Query::create()
				  ->from('SfGuardUserProfile a')
				  ->where('a.user_id = ?', $message->getArchitectId())
				  ->orderBy('a.id DESC');
				$member = $q->fetchOne();
				$name = $member->getFullname()." (Client)";
			}
			?>

			 <?php
			if($message->getArchitectId() != "")
			{
			?>
			  <div class="media media-client">
			  	<?php
			}
			else
			{
			?>
			 <div class="media  media-staff">
			 <?php
			}
			?>
			        <span class="arrow"></span>
                    <div class="media-body">
                        <h4 class="text-primary"><?php echo $name; ?> </h4>
                        <?php echo html_entity_decode($message->getContent()); ?>
                        <?php if(!empty($message->getAttachment())): ?>
                        <a class="btn btn-success" href="/backend.php/applications/downloadFile/filename/<?php echo $message->getAttachment() ?>"> Download Attachment </a>
                        <?php endif; ?>
                        <small class="text-muted pull-right"><?php echo $message->getActionTimestamp(); ?></small>
                    </div>
                    <span class="clearfix"></span>
                </div><!-- media -->



		<?php
		 }
     ?></div><?php
	}
	?>
	 </form>

        <!-- Display form for reviewer to send message/reply to architect -->
        <form enctype="multipart/form-data" action="/backend.php/applications/view/id/<?php echo $application->getId(); ?>/messages/read" method="post"  autocomplete="off" data-ajax="false">
              <div class="panel panel-default timeline-post mb0">
                 <div class="panel-body pt0">
                     <?php echo __('Type your Message') ?> 
                      <textarea name='txtmessage' id="msg_wysiwyg" placeholder="<?php echo __("Type in your Message Here"); ?>..." class="form-control ckeditor" rows="10" data-autogrow="true"></textarea>
                 </div>
                  <!-- OTB patch - Add option for attaching a file -->
                   <div class="panel-body pt0">
                        <label>
                        <h4><?php echo __("Attach a file (Optional) "); ?>:</h4>
                      </label>
                        <input type="file" name="txt_file" id="txt_file" />
                    </div>
              </div>
               <div class="panel-footer">
                      <button type='submit' class="btn btn-primary pull-right"><?php echo __("Send Message"); ?> </button>
             </div>
         </form>
<?php
}
?>


<script src="<?php echo public_path(); ?>assets_unified/js/msg_ckeditor/ckeditor.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/msg_ckeditor/adapters/jquery.js"></script>

<script>
jQuery(document).ready(function(){

  // CKEditor
  jQuery('#memo_wysiwyg').ckeditor();

});
</script>
