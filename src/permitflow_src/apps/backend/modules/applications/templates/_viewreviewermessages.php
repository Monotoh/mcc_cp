<?php
/**
 * _viewreviewermessages.php partial.
 *
 * Displays a message trail between the client and the reviewers
 *
 * @package    backend
 * @subpackage applications
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
 use_helper("I18N");

//get form id and entry id
$form_id  = $application->getFormId();
$entry_id = $application->getEntryId();

//Get all messages attached to this application
$q = Doctrine_Query::create()
  ->from('Communications a')
  ->where('a.application_id = ?', $application->getId())
    /*OTB Start - only show messages meant for reviewer*/
  ->andWhere('a.communication_type IS NULL')
    /*OTB End - only show messages meant for reviewer*/
  ->orderBy('a.id ASC');
$messages = $q->execute();

//if($sf_user->mfHasCredential('incomingmessages'))
{
?>

    <form>
	<?php
	if(sizeof($messages) <= 0){
	?>
		<table class="table mb0">
			<tbody>
			<tr>
			<td><i class="bold-label">No records found</i></td>
			</tr>
			</tbody>
			</table>
	<?php
	}
	else
	{
	?>
	<div class="panel panel-default panel-messages mt0 scrollable" data-height="450">
     <?php       
		//Iterate through each message and display them
	 	foreach($messages as $message){
			if($message->getReviewerId() != "")
			{
				$q = Doctrine_Query::create()
				  ->from('CfUser a')
				  ->where('a.nid = ?', $message->getReviewerId())
				  ->orderBy('a.nid DESC');
				$member = $q->fetchOne();
				$name = $member->getStrfirstname()." ".$member->getStrlastname();
			}
			else if($message->getArchitectId() != "")
			{
				$q = Doctrine_Query::create()
				  ->from('SfGuardUserProfile a')
				  ->where('a.user_id = ?', $message->getArchitectId())
				  ->orderBy('a.id DESC');
				$member = $q->fetchOne();
				$name = $member->getFullname()." (Client)";
			}
			if($message->getMsgToReviewerId() != "" && $message->getCommunicationType() != "client")
			{
				$q = Doctrine_Query::create()
				  ->from('CfUser a')
				  ->where('a.nid = ?', $message->getMsgToReviewerId())
				  ->orderBy('a.nid DESC');
				$member = $q->fetchOne();
				if($member){
					$name_sent_to = $member->getStrfirstname()." ".$member->getStrlastname();
				}else{
					$name_sent_to = "";
				}
			}
			?>
			
			 <?php
			if($message->getArchitectId() != "")
			{
			?>
			  <div class="media media-client">
			  	<?php
			}
			else
			{
			?>
			 <div class="media  media-staff">
			 <?php
			}
			?>
			        <span class="arrow"></span>
                    <div class="media-body">
                        <h4 class="text-primary"><b>Message From:</b> <?php echo $name; ?> </h4>
                        <?php if($name_sent_to !=""){ ?><h4 class="text-primary"><b>Message To:</b> <?php echo $name_sent_to; ?> </h4><?php } ?>
                        <?php echo html_entity_decode($message->getContent()); ?>
                        <small class="text-muted pull-right"><?php echo $message->getActionTimestamp(); ?></small>
                    </div>
                    <span class="clearfix"></span>
                </div><!-- media -->
		
		
              
		<?php
		}?>
                </div><!-- panel -->
	<?php
	}
	?>
	 </form>

        <!-- Display form for reviewer to send message/reply to other reviewers -->
        <form action="/backend.php/applications/view/id/<?php echo $application->getId(); ?>/reviewer_messages/read" method="post"  autocomplete="off" data-ajax="false">
              <div class="panel panel-default timeline-post mb0">
                 <div class="panel-body pt0">
				<label><?php echo __('Message to: '); ?></label>
				<select id='user_msgto' name='user_msgto' onChange="ajaxSearchform1(this.value);" class="chosen">
							<option value="0"><?php echo __('Select user'); ?>....</option>
							<?php
								//Get list of all available applications categorized by groups
								$q = Doctrine_Query::create()
								  ->from('CfUser a')
								  ->orderBy('a.strfirstname ASC');
								$users = $q->execute(); 
                                                                
                                                               
                                                        $otbhelper = new OTBHelper();
                                                        //
                                                       /** $logged_reviewer_id = $_SESSION["SESSION_CUTEFLOW_USERID"] ;
                                                        //error_log("Reviewer Id >>> ".$logged_reviewer_id);
                                                        //get allowed agencies
                                                        $alllowed_agencies = $otbhelper->getUserAllowedAgencies($logged_reviewer_id) ;           
                                                        //
                                                        $agencies_id = implode(',', $alllowed_agencies) ;
                                                       //
                                                        $admins = $otbhelper->getAdminRHAUsers() ;        
                                                        $user_ids = implode(',', $admins) ;
                                                        //
                                                        $u = Doctrine_Query::create()
                                                                ->from('AgencyUser u')
                                                                ->where('u.agency_id in ('.$agencies_id.')')
                                                                ->addWhere('u.user_id not in ('.$user_ids.')');
                                                        $users = $u->execute(); */
           

								if(sizeof($users) > 0)
								{
									foreach($users as $user)
									{
											echo "<option value='".$user->getNId()."' ".$selected.">".$otbhelper->getReviewerNames($user->getNId())."</option>";
									}
								}
								else
								{
								}
								?>
						</select>
						<br/><br/>
                      <textarea name='txtmessage' id="msg_wysiwyg_reviewer" placeholder="<?php echo __("Type in your Message Here"); ?>..." class="form-control" rows="10" data-autogrow="true"></textarea>
					  <input type="hidden" name="reviewer_message" value="true">
                 </div>
              </div>
               <div class="panel-footer">
                      <button type='submit' class="btn btn-primary pull-right"><?php echo __("Send"); ?> </button>
             </div>
         </form>
<?php
}
?>


<script src="<?php echo public_path(); ?>assets_unified/js/msg_ckeditor/ckeditor.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/msg_ckeditor/adapters/jquery.js"></script>

<script>
jQuery(document).ready(function(){

  // CKEditor
  jQuery('#msg_wysiwyg_reviewer').ckeditor();

});
</script>