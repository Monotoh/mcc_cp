<?php
/**
 * _viewreviewers.php partial.
 *
 * Display information about the application's current progression through the workflow
 *
 * @package    backend
 * @subpackage applications
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
 
/**
*
* Function to get all the number of days between a period of time
*
* @param String $sStartDate Start date to begin fetching dates from
* @param String $sEndDate End date where to stop fetching dates from
*
* @return int
*/
function GetHistoryDays($sStartDate, $sEndDate){  
    $start_ts = strtotime($sStartDate);
	$end_ts = strtotime($sEndDate);
	$diff = $end_ts - $start_ts;
	return round($diff / 86400); 
} 

//get form id and entry id
$form_id  = $application->getFormId();
$entry_id = $application->getEntryId();
?>
<form id="form" action="#" autocomplete="off" data-ajax="false">
	<fieldset>
        <label>Reviewers</label>
		<?php
			$count_tasks = 0;
			$q = Doctrine_Query::create()
			   ->from('Task a')
			   ->where('a.application_id = ?', $application->getId())
                           ->orderBy('a.date_created ASC');
			$tasks = $q->execute();
			foreach($tasks as $task)
			{
                                $q = Doctrine_Query::create()
					->from('CfUser a')
				 	->where('a.nid = ?', $task->getOwnerUserId());
				$reviewer = $q->fetchOne();
				$count_tasks++;
				if($task->getIsLeader() == "1")
				{
					$reviewertype = "Lead Reviewer";
					$style = "color: #4CD014;";
				}
				else
				{
					$reviewertype = "Support Reviewer";
					$style = "color: #F43535;";
				}
				
				$tasklink = "/backend.php/tasks/view/id/".$task->getId();
				?>
				 <section>
					<label for="text_field" style="font-weight: 900; <?php echo $style; ?>"><?php echo $reviewer->getStrfirstname()." ".$reviewer->getStrlastname(); ?><br /><font style='font-weight: 100; font-size: 10px;'>(<?php echo $reviewertype; ?>)</font></label>
					<div><?php echo $task->getTypeName(); ?> task assigned on <?php echo $task->getDateCreated(); ?> by
                     <?php 
					    //Display information on who created the task
						$q = Doctrine_Query::create()
							->from("CfUser a")
							->where("a.nid = ?", $task->getCreatorUserId());
					    $creator = $q->fetchOne();
						echo $creator->getStrfirstname()." ".$creator->getStrlastname();
 					 ?>. 
                     <?php
					 	//Display information on when the task is due or when it was completed.
					 	if($task->getEndDate() != "" && $task->getStatusName() == "Pending")
						{
							echo "Due on ".$task->getEndDate().".";
						}
						else if($task->getEndDate() != "" && $task->getStatusName() == "Completed")
						{
							echo "Completed on ".$task->getEndDate().".";
						}

						echo " <b>".$task->getStatusName()." </b>.";
					 ?>
                    
                    <br /><br />
                    <?php
						if($task->getRemarks() != "")
						{
							?>
                            <b><u>Description:</u></b><br />
                            <?php echo $task->getRemarks(); ?>
                            <?php
						}
					?>
                    </div>
				</section>
				<?php
		
			}
            ?>

    <?php
	//If no reviewers have been assigned to work on this application
    if($count_tasks == 0)
    {
        ?>
        <section><label style="width: 100%;">No Reviewers</label></section>
        <?php
    }
    ?>

    </fieldset>
    </form>

    
    <form id="form" action="#" autocomplete="off" data-ajax="false">
    <fieldset>
        <label>Supervisors</label>
        <?php
			//Iterate through all actions taken on this application (Actions that move it between stages)
            $q = Doctrine_Query::create()
                 ->from('ApplicationReference a')
                 ->where('a.application_id = ?', $application->getId())
                 ->orderBy('a.start_date ASC');
            $refs = $q->execute();
            foreach($refs as $ref)
            {
				//Get name of stage where the application was sent to
                $q = Doctrine_Query::create()
                     ->from('SubMenus a')
                     ->where('a.id = ?', $ref->getStageId());
                $submenu = $q->fetchOne();
				
				//Get name of supervisor that performed this action
                $q = Doctrine_Query::create()
                     ->from('CfUser a')
                     ->andWhere('a.nid = ?', $ref->getApprovedBy());
                $sender = $q->fetchOne();

                $sender_name = "-";
				//If the user was deleted or is no longer existing in the DB, skip
                if($sender)
                {
                    $sender_name = $sender->getStrfirstname()." ".$sender->getStrlastname();
                }
				else
				{	
					continue;
				}
				
				//Get information about which groups the user belongs to
				$q = Doctrine_Query::create()
				   ->from("MfGuardUserGroup a")
				   ->where("a.user_id = ?", $sender->getNid());
			    $usergroupids = $q->execute();
				
				$groups = null;
				foreach($usergroupids as $groupid)
				{
					$q = Doctrine_Query::create()
					   ->from("MfGuardGroup a")
					   ->where("a.id = ?", $groupid->getGroupId());
					$group = $q->fetchOne();
					$groups[] = $group;
				}
				
				$groupstring = "<br> <font style='font-weight: 100; font-size: 10px;'>(";
				foreach($groups as $group)
				{
					if($group)
					{
						$groupstring .= $group->getName().", ";
					}
				}
				$groupstring .= ")</font>";

        		//Display the information
				if($submenu)
				{
					echo "<section><label style='font-weight: 900;'>".$sender_name.$groupstring."</label><div>Sent this application to ".$submenu->getTitle()." on ".$ref->getStartDate()."</div></section>";
				}
				else
				{
					echo "<section><label style='font-weight: 900;'>".$sender_name.$groupstring."</label><div> ".$ref->getStartDate()."</div></section>";
				}

            }

    

        ?>

        </fieldset>
        </form>
        
        <form id="form" action="#" autocomplete="off" data-ajax="false">
    <fieldset>
        <label>History</label>
        <?php
			$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
			mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
			$query = "SELECT * FROM ap_form_".$application->getFormId()." WHERE id = '".$application->getEntryId()."'";
			$result = mysql_query($query,$dbconn);

			$application_form = mysql_fetch_assoc($result);
		
			//Iterate through all actions taken on this application (Actions that move it between stages)
            $q = Doctrine_Query::create()
                 ->from('ApplicationReference a')
                 ->where('a.application_id = ?', $application->getId())
                 ->orderBy('a.start_date ASC');
            $refs = $q->execute();
			
			$refssize = sizeof($refs);
			$count = 0;

			
			
            foreach($refs as $ref)
            {
				$count++;
				
				if($count == 1)
				{
					//Get the first stage and check if its in the application ref, if not add it and set start
					// date as date_created and end_date as ref->getStartDate()
					//Get list of all stages, starting with parent stages
					 $firststage = "";
						$q = Doctrine_Query::create()
						  ->from('Menus a')
						  ->orderBy('a.order_no ASC');
						$stagegroups = $q->execute();
						foreach($stagegroups as $stagegroup)
						{
						   //Filter only stages that the current user has privileges for
						   if($sf_user->mfHasCredential('accessmenu'.$stagegroup->getId()))
						   {
							   
								$q = Doctrine_Query::create()
								  ->from('SubMenus a')
								  ->where('a.menu_id = ?', $stagegroup->getId())
								  ->orderBy('a.order_no ASC');
								$stages = $q->execute();
								
								$stagecontent = "";
								
								//Get of list of all child stages under this parent stage
								foreach($stages as $stage)
								{
								   //Filter only stages that the current user has privileges for
								   if($sf_user->mfHasCredential('accesssubmenu'.$stage->getId()))
								   {
									   $firststage = $stage;
									   break;
								   }
								}
						   }
						   if($firststage != "")
						   {
							 break;  
						   }
						}

					if($firststage)
					{
					
					$q = Doctrine_Query::create()
						 ->from('ApplicationReference a')
						 ->where('a.application_id = ?', $application->getId())
						 ->andWhere('a.stage_id = ?', $firststage->getId())
						 ->orderBy('a.id ASC');
					$aprefs = $q->execute();
					
					if(sizeof($aprefs) > 0)
					{
						//ignore
					}
					else
					{
						
						
						$appref = new ApplicationReference();
						$appref->setApplicationId($application->getId());
						$appref->setStageId($firststage->getId());
						$appref->setStartDate($application_form['date_created']);
						$appref->setEndDate($ref->getStartDate());
						$appref->setApprovedBy(0);
						$appref->save();
						
						$totaldays = 0;
						$daystring = "<br> <font style='font-weight: 100; font-size: 10px;'>";
						$startdate = $application_form['date_created'];
						$enddate = $ref->getStartDate();
						 
						$days = GetHistoryDays($startdate, $enddate);
						 
						$totaldays = $totaldays + $days;
						$daystring .= "From ".$startdate." to ".$enddate."<br>(".$totaldays." days";
						$daystring .= ")</font>";
		
						//Display the informatio
							echo "<section><label style='font-weight: 900;'>".$firststage->getTitle().$daystring."</label><div></div></section>";
					}
				}
				}
				
				//Get name of stage where the application was sent to
                $q = Doctrine_Query::create()
                     ->from('SubMenus a')
                     ->where('a.id = ?', $ref->getStageId());
                $submenu = $q->fetchOne();
				
				//Get name of supervisor that performed this action
                $q = Doctrine_Query::create()
                     ->from('CfUser a')
                     ->Where('a.nid = ?', $ref->getApprovedBy());
                $sender = $q->fetchOne();

                $sender_name = "-";
				//If the user was deleted or is no longer existing in the DB, skip
                if($sender)
                {
                    $sender_name = $sender->getStrfirstname()." ".$sender->getStrlastname();
                }
				else
				{	
					if($ref->getApprovedBy() == 0)
					{
						$sender_name = "Client";
					}
					else
					{
						continue;
					}
				}
				
				
				
				$totaldays = 0;
				$daystring = "<br> <font style='font-weight: 100; font-size: 10px;'>";
				$startdate = "";
				$enddate = "";
				  if($ref->getEndDate() != "")
				  {
					  //if this is not the archiving stage and it has an enddate, disregard and consider today's date
					  if($submenu->getTitle() != "Archiving" && $refssize == $count)
					  {
						$days = GetHistoryDays($ref->getStartDate(), date("Y-m-d"));
						$startdate = $ref->getStartDate();
						$enddate = date("Y-m-d");
					  }
					  else if($submenu->getTitle() == "Archiving")
					  {
						$days = GetHistoryDays($ref->getStartDate(), $ref->getStartDate());
						$startdate = $ref->getStartDate();
						$enddate = $ref->getStartDate();
					  }
					  else
					  {
						  $days = GetHistoryDays($ref->getStartDate(), $ref->getEndDate());
						  $startdate = $ref->getStartDate();
						  $enddate = $ref->getEndDate();
					  }
				  }
				  else
				  {
					$days = GetHistoryDays($ref->getStartDate(), date("Y-m-d"));
					$startdate = $ref->getStartDate();
					$enddate = date("Y-m-d");
				  }
				  $totaldays = $totaldays + $days;
				  $daystring .= "From ".$startdate." to ".$enddate."<br>(".$totaldays." days";
				$daystring .= ")</font>";

        		//Display the information
				if($submenu)
				{
					echo "<section><label style='font-weight: 900;'>".$submenu->getTitle().$daystring."</label><div>".$sender_name." sent this application to ".$submenu->getTitle()." on ".$ref->getStartDate()."</div></section>";
				}

            }

    

        ?>

        </fieldset>
        </form>
