<?php
/**
 * ajaxalternatelist template.
 *
 * Displays list of applications. This is only used by ajax. No styling exists here. _list.php partial is called to introduce template.
 *
 * @package    backend
 * @subpackage applications
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

	$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
	mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
	
	//import pagination libraries
	$prefix_folder = dirname(__FILE__)."/../../../../../..";
	if(include ($prefix_folder.'/html/assets_backend/plugins/pagination/function.php'))
        {
            //Loaded pagination correctly, do nothing.
        }
        else
        {
            //Didn't load pagination, try public_html instead
           include ($prefix_folder.'/public_html/assets_backend/plugins/pagination/function.php');
        }
	
	//Check if filter is applied for status or form
	$filter = $_GET['filter'];
	if(!empty($filter))
	{
		$filter = " WHERE b.approved = '".$filter."'";
	}
	
	$selectedform = $_GET['form'];
	
?>
<?php
//Initialize pagination variables
$page = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
$limit = 5;
$startpoint = ($page * $limit) - $limit;

//Generate filter queries if filter was applied 
$pars = "";
if(empty($filter))
{
	$pars = "WHERE ((";
	$q = Doctrine_Query::create()
	->from('Menus a')
	->orderBy('a.order_no ASC');
	$stagegroups = $q->execute();
	$qcount = 0;
	foreach($stagegroups as $stagegroup)
	{
		//Check if user has prilige to view certain stages
		if($sf_user->mfHasCredential('accessmenu'.$stagegroup->getId()))
		{
			$q = Doctrine_Query::create()
			->from('SubMenus a')
			->where('a.menu_id = ?', $stagegroup->getId())
			->orderBy('a.order_no ASC');
			$stages = $q->execute();
			
			foreach($stages as $stage)
			{
				if($sf_user->mfHasCredential('accesssubmenu'.$stage->getId()))
				{
					$qcount++;
					if($qcount == 1)
					{
						$pars .= "b.approved = ".$stage->getId()." ";
					}
					else
					{
						$pars .= " OR b.approved = ".$stage->getId()." ";
					}
				}
			}
		}
	}
	$pars .= ") AND b.parent_submission = 0)";
}
else
{
	$pars = " AND b.parent_submission = 0 ";	
}

//If form filter is applied then show only the selected application form types
if(!empty($selectedform))
{
	$filterform = " AND b.form_id = '".$selectedform."'";
}

//Combine filter queries into final query
$applications = null;
if($_GET['form'])
{
	$statement = "SELECT b.id AS entryid, b.approved as approved FROM ap_form_".$_GET['form']." a LEFT OUTER JOIN form_entry b ON a.id = b.entry_id ".$filter.$pars.$filterform." ORDER BY b.date_of_submission DESC";
	$query = "SELECT b.id AS entryid, b.approved as approved, a.date_created as date_created FROM ap_form_".$_GET['form']." a LEFT OUTER JOIN form_entry b ON a.id = b.entry_id ".$filter.$pars.$filterform." ORDER BY b.date_of_submission DESC LIMIT {$startpoint}, {$limit}";
}
else
{
	$statement = "SELECT b.id AS entryid, b.approved as approved FROM form_entry b ".$filter.$filterform." ".$pars."  ORDER BY b.id DESC";
	$query = "SELECT b.id AS entryid, b.approved as approved FROM form_entry b ".$filter.$filterform." ".$pars."  ORDER BY b.id DESC LIMIT {$startpoint}, {$limit}";
}


$applications = null;

//Iterate through result and filter out stages that the user is not allowed to see
$results = mysql_query($query,$dbconn);
while($row = mysql_fetch_assoc($results))
{
		if($sf_user->mfHasCredential('accesssubmenu'.$row['approved']))
		{
				$applications['all'][] = $row;
		}
}
?>
		<div style="margin: 0 0 0 0; padding: 0 0 0 0;">   
            <!-- Call the _list.php partial to output template -->
			<?php include_partial('list', array('applications' => $applications['all'], 'application_form' => $selectedform)) ?>
		</div>
<?php

?>
<br>
<?php

//Prepare filters for the pagination function
$chfilter = "";
if(!empty($_GET['filter']))
{
	$chfilter = "list?filter=".$_GET['filter']."&";
}
if(!empty($_GET['form']))
{
	if($chfilter == "")
	{
		$chfilter .= "list?form=".$_GET['form']."&";
	}
	else
	{
		$chfilter .= "&form=".$_GET['form']."&";
	}
}
if($chfilter == "")
{
	$chfilter = "?";
}
//Call the pagination function to generate the page numbers
echo pagination($statement,$limit,$page,$chfilter);
?>
