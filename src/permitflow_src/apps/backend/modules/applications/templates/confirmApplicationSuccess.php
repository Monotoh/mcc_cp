<?php
    /**
     * confirmApplicationSuccess.php template.
     *
     * Displays confirmation/review page for a submitted application
     *
     * @package    frontend
     * @subpackage forms
     * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
     */
    $prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";

    require($prefix_folder.'includes/init.php');

    require($prefix_folder.'config.php');
    require($prefix_folder.'includes/db-core.php');
    require($prefix_folder.'includes/helper-functions.php');

    require($prefix_folder.'includes/language.php');
    require($prefix_folder.'includes/common-validator.php');
    require($prefix_folder.'includes/view-functions.php');
    require($prefix_folder.'includes/theme-functions.php');
    require($prefix_folder.'includes/post-functions.php');
    require($prefix_folder.'includes/entry-functions.php');
    require($prefix_folder.'hooks/custom_hooks.php');

    $q = Doctrine_Query::create()
        ->from("SfGuardUser a")
        ->leftJoin('a.Profile b')
        ->where('a.id = b.user_id')
        ->andWhere('a.id = ?', $_SESSION['create_user']);
    $client = $q->fetchOne();

    //get data from database
    $dbh        = mf_connect_db();
    $ssl_suffix = mf_get_ssl_suffix();

    $form_id    = (int) trim($_REQUEST['id']);

    $application_manager = new ApplicationManager();

    if(!empty($_POST['review_submit']) || !empty($_POST['review_submit_x'])){ //if form submitted

        //commit data from review table to actual table
        //however, we need to check if this form has payment enabled or not

        //if the form doesn't have any payment enabled, continue with commit and redirect to success page
        $form_properties = mf_get_form_properties($dbh,$form_id,array('payment_enable_merchant','payment_delay_notifications','payment_merchant_type','payment_enable_invoice'));
        error_log("Payment Debug-x 1: ".$form_properties['payment_enable_merchant']." - ".$form_properties['payment_enable_invoice']);
        if($form_properties['payment_enable_merchant'] != 1 || $form_properties['payment_enable_invoice'] == 1){
            $record_id     = $_SESSION['review_id'];
            $commit_result = mf_commit_form_review($dbh,$form_id,$record_id);

            error_log("Debug-x 2: ".$client->getProfile()->getFullname());

            if(empty($commit_result['form_redirect'])){
                if($_POST['draft'] == 1)
                {
                    header("Location: http{$ssl_suffix}://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."?id={$form_id}&draft=1&entryid={$commit_result['record_insert_id']}&done=1");
                }
                else
                {
                    header("Location: http{$ssl_suffix}://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."?id={$form_id}&entryid={$commit_result['record_insert_id']}&done=1");
                }
                error_log("Debug-x 3: ".$client->getProfile()->getFullname());
                exit;
            }else{
                error_log("Debug-x 4: ".$client->getProfile()->getFullname());
                echo "<script type=\"text/javascript\">top.location.replace('{$commit_result['form_redirect']}')</script>";
                exit;
            }
        }else{
            error_log("Debug-x 5: ".$client->getProfile()->getFullname()." - ".$_SESSION['review_id']);
            //if the form has payment enabled, continue commit and redirect to payment page
            $record_id      = $_SESSION['review_id'];
            $commit_options = array();

            //delay notifications only available on stripe
            if(!empty($form_properties['payment_delay_notifications']) && $form_properties['payment_merchant_type'] == 'stripe'){
                $commit_options['send_notification'] = false;
            }

            $new_record_id = null;

            //Check if review record exists
            if($application_manager->review_entry_exists($form_id,$record_id))
            {
                error_log("Debug-x 5.1: ".$client->getProfile()->getFullname()." - Commit Review");
                $commit_result = mf_commit_form_review($dbh, $form_id, $record_id, $commit_options);

                $new_record_id = $commit_result['record_insert_id'];

                $_SESSION['new_record_id'] = $new_record_id;
            }
            elseif($application_manager->new_entry_exists($form_id,$_SESSION['new_record_id']))
            {
                error_log("Debug-x 5.1: ".$client->getProfile()->getFullname()." - Session Review - ".$_SESSION['new_record_id']);
                $new_record_id = $_SESSION['new_record_id'];
            }
            else
            {
                ?>
                We encountered an error on submission. You are being redirected...

                <a href="/index.php/forms/view?id=<?php echo $form_id; ?>">Click the link here to continue.</a>
                <script language="javascript">
                    window.location = "/backend.php/applications/create";
                </script>
                <?php
            }

            error_log("Debug-x 6: ".$client->getProfile()->getFullname());

            //We will use the application manager to create new applications or drafts from form submissions
            $application_manager = new ApplicationManager();

            //Check if an application already exists for the form submission to prevent double entry
            if($application_manager->application_exists($form_id, $new_record_id)) {
                //If save as draft/resume later was clicked then do nothing
                $submission = $application_manager->get_application($form_id, $new_record_id);
            }
            else {
                //If save as draft/resume later was clicked then create draft application
                $submission = $application_manager->create_application($form_id, $new_record_id, $client->getId(), true);
            }

            $application_manager->update_invoices($submission->getId());

            $sf_user->setAttribute('form_id', $form_id);
            $sf_user->setAttribute('entry_id', $new_record_id);

            header("Location: http{$ssl_suffix}://".$_SERVER['HTTP_HOST']."/backend.php/applications/payment");
            exit;

        }

    }elseif (!empty($_POST['review_back']) || !empty($_POST['review_back_x'])){
        error_log("Debug-x 7: ".$client->getProfile()->getFullname());
        //go back to form
        $origin_page_num = (int) $_POST['mf_page_from'];
        header("Location: http{$ssl_suffix}://".$_SERVER['HTTP_HOST'].mf_get_dirname($_SERVER['PHP_SELF'])."/create?id={$form_id}&mf_page={$origin_page_num}");
        exit;
    }else{

        error_log("Debug-x 8: ".$client->getProfile()->getFullname());

        if(empty($form_id)){
            die('ID required.');
        }

        if(!empty($_GET['done']) && !empty($_SESSION['mf_form_completed'][$form_id])){

            //We will use the application manager to create new applications or drafts from form submissions
            $application_manager = new ApplicationManager();

            //If the submission is to be attached to an existing application, then create a linked application, else create a normal application
            if($_SESSION["main_application"])
            {
                error_log("Debug-x 9: ".$client->getProfile()->getFullname());
                $submission = $application_manager->create_linked_application($_SESSION["main_application"], $form_id, $_GET['entryid'], $client->getId());

                $_SESSION["main_application"] = "";
            }
            else
            {
                error_log("Debug-x 10: ".$client->getProfile()->getFullname());
                //Check if an application already exists for the form submission to prevent double entry
                if($application_manager->application_exists($form_id, $_GET['entryid'])) {
                    //If a draft existed, then publish the draft to a live workflow
                    if($application_manager->is_draft($form_id, $_GET['entryid'])) {
                        $submission = $application_manager->get_application($_GET['id'], $_GET['entryid']);
                        $submission = $application_manager->publish_draft($submission->getId());
                    }
                    else {
                        //If the application was already submitted then display warning
                        $submission = $application_manager->get_application($form_id, $_GET['entryid']);
                        $markup = mf_display_already_submitted($dbh,$form_id);
                    }
                }
                else {
                    //Create new application and publish it to a live workflow
                    $submission = $application_manager->create_application($form_id, $_GET['entryid'], $client->getId(), false);
                    $markup = mf_display_success($dbh,$form_id);
                }

            }

            $markup = mf_display_success($dbh,$form_id);


        }else{
            if(empty($_SESSION['review_id'])){
                die("Your session has been expired. Please <a href='view?id={$form_id}'>click here</a> to start again.");
            }else{
                $record_id = $_SESSION['review_id'];
            }

            $from_page_num = (int) $_GET['mf_page_from'];
            if(empty($from_page_num)){
                $form_page_num = 1;
            }

            $markup = mf_display_form_review($dbh,$form_id,$record_id,$from_page_num);
        }
    }
?>

<div class="pageheader">
       <h2><i class="fa fa-edit"></i>Application Form<span> Submit an Application</span></h2>
      <div class="breadcrumb-wrapper">

        <ol class="breadcrumb">
          <li><a href="">Application Form</a></li>
          <li class="active"><?php echo $form_name; ?></li>
        </ol>
      </div>
    </div>

<div class="contentpanel">
<div class="row">
                                <?php
    header("Content-Type: text/html; charset=UTF-8");
    echo $markup;

    ?>

</div><!-- /.row -->
</div><!-- /.marketing -->
