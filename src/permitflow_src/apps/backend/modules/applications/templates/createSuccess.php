<?php
use_helper("I18N");

if($step == 1)
{
    $sf_user->setAttribute('invoice_id', 0);
}
elseif($step == 2)
{
    $sf_user->setAttribute('invoice_id', 0);
}

if($sf_user->mfHasCredential('createapplications')) {
    $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
    mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
    ?>

    <div class="pageheader">
        <h2><i class="fa fa-home"></i> Create Application <span>Security</span></h2>

        <div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
                <li><a href="<?php echo public_path(); ?>backend.php">Home</a></li>
                <li>Applications</li>
                <li class="active">Create</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
    <div class="row">
    <div class="col-md-12">

    <!-- BASIC WIZARD -->
    <div id="progressWizard" class="basic-wizard">

    <ul class="nav nav-pills nav-justified">
        <li <?php if ($step == 1){ ?>class="active"<?php } ?>><a href="#ptab1"
                                                                 <?php if ($step == 1){ ?>data-toggle="tab"<?php } ?>><span>Step 1:</span>
                Search for User</a></li>
        <li <?php if ($step == 2){ ?>class="active"<?php } ?>><a href="#ptab2"
                                                                 <?php if ($step == 2){ ?>data-toggle="tab"<?php } ?>><span>Step 2:</span>
                Confirm User Details</a></li>
        <li <?php if ($step == 3){ ?>class="active"<?php } ?>><a href="#ptab3"
                                                                 <?php if ($step == 3){ ?>data-toggle="tab"<?php } ?>><span>Step 3:</span>
                Create Application</a></li>
    </ul>

    <div class="tab-content">
    <div class="tab-pane <?php if ($step == 1) { ?>active<?php } ?>" id="ptab1">
        <form class="form-horizontal form-bordered" method="post"
              action="<?php echo public_path(); ?>backend.php/applications/create">
            <input type="hidden" name="step" value="1"/>

            <div class="panel-body panel-body-nopadding" style="border-top:none;">
                <div class="form-group">
                    <label class="col-sm-2"><?php echo __('Search for a user by name, user id or email'); ?></label>

                    <div class="col-sm-8">
                        <input class="form-control" type='text' name='usersearch' id='usersearch' style="width:80%;"
                               value="<?php echo $_POST['usersearch']; ?>">
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <button class="btn btn-primary" type="submit" name="submitbuttonname"
                        value="submitbuttonvalue"><?php echo __('Search'); ?></button>
            </div>
        </form>
    </div>
    <div class="tab-pane <?php if ($step == 2) { ?>active<?php } ?>" id="ptab2">
        <?php
        if($step == 2) {
            ?>
            <form class="form-horizontal form-bordered" method="post"
                  action="<?php echo public_path(); ?>backend.php/applications/create">
                <input type="hidden" name="step" value="2"/>

                <div class="panel-body panel-body-nopadding">
                    <div class="table-responsive">
                        <table class="table dt-on-steroids mb0" id="table3">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th width="60">ID</th>
                                <th width="20%">Full Name</th>
                                <th width="20%">User ID</th>
                                <th width="20%">Email</th>
                                <th>Last Login</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $architects = null;

                            if (sfConfig::get('app_enable_categories') != "yes") {
                                $q = Doctrine_Query::create()
                                    ->from("SfGuardUser a")
                                    ->leftJoin('a.Profile b')
                                    ->where('a.id = b.user_id')
                                    ->andWhere('b.fullname LIKE ? OR a.username LIKE ? OR b.email = ?', array($filter . "%", $filter . "%", $filter));
                                $users = $q->execute();
                            } else {
                                $q = Doctrine_Query::create()
                                    ->from("SfGuardUser a")
                                    ->leftJoin('a.Profile b')
                                    ->where('a.id = b.user_id')
                                    ->andWhere('b.fullname LIKE ? OR a.username LIKE ? OR b.email = ?', array($filter . "%", $filter . "%", $filter))
                                    ->andWhere("a.is_active = ?", 1);
                                $users = $q->execute();
                            }
                            $count = 0;
                            ?>
                            <?php foreach ($users as $sf_guard_user): ?>
                                <?php
                                //Skip id 1, id 1 is used for authenticating backend users
                                if ($sf_guard_user->getId() == "1") {
                                    continue;
                                }

                                $count++;
                                ?>
                                <tr id="row_<?php echo $sf_guard_user->getId() ?>">
                                    <td><input type="radio" name="users" id="users<?php $sf_guard_user->getId(); ?>"
                                               value="<?php echo $sf_guard_user->getId(); ?>"></td>
                                    <td><?php echo $count; ?></td>
                                    <td><?php echo ucwords(strtolower($sf_guard_user->getProfile()->getFullname())); ?></td>
                                    <td><?php echo ucwords(strtolower($sf_guard_user->getUsername())); ?></td>
                                    <td><?php echo strtolower($sf_guard_user->getProfile()->getEmail()); ?></td>

                                    <td><?php echo $sf_guard_user->getLastLogin() ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>

                    </div>
                </div>
                <br>
                <button type="submit" class="btn btn-danger" id="submitpermissions" style="float: left;"
                        onClick="window.location='/backend.php/applications/create';">Back
                </button>
                <button type="submit" class="btn btn-success" id="submitpermissions" style="float: right;">Next</button>
                <br>
                <br>
            </form>
        <?php
        }
        ?>
    </div>
    <div class="tab-pane <?php if ($step == 3 || $_SESSION['formid']) { ?>active<?php } ?>" id="ptab3">
        <input type="hidden" name="step" value="3"/>
        <?php
        if($step == 3 || $_SESSION['formid']) {
            //Get list of all stages in current stage. If more than one form
            // form is linked to the stages then show a list. If only one form
            // then display the form directly

            $form_count = 0;

            if ($current_stage && $step == 3) {
                $q = Doctrine_Query::create()
                    ->from("SubMenus a")
                    ->where("a.id = ?", $current_stage);
                $stage = $q->fetchOne();

                $q = Doctrine_Query::create()
                    ->from("SubMenus a")
                    ->where("a.menu_id = ?", $stage->getMenuId());
                $substages = $q->execute();

                $last_form_id = null;
                $forms_array = array();

                foreach ($substages as $substage) {
                    $q = Doctrine_Query::create()
                        ->from('ApForms a')
                        ->where('a.form_stage = ?', $substage->getId())
                        ->andWhere('a.form_type = 1')
                        ->andWhere('a.form_active = 1')
                        ->orderBy('a.form_name ASC');
                    $applications = $q->execute();
                    foreach ($applications as $apform) {
                        $last_form_id = $apform->getFormId();
                        $forms_array[] = $last_form_id;
                        $form_count++;
                    }
                }


                $q = Doctrine_Query::create()
                    ->from('ApForms a')
                    ->andWhere('a.form_type = 1')
                    ->andWhere('a.form_active = 1')
                    ->orderBy('a.form_name ASC');
                $applications = $q->execute();
                foreach ($applications as $apform) {
                    //Check for forms linked via logic
                    $sql = "SELECT * FROM ap_workflow_logic_conditions WHERE form_id = " . $apform->getFormId();
                    $stage_result = mysql_query($sql, $dbconn);

                    while ($stage_values = mysql_fetch_assoc($stage_result)) {
                        if ($stage_values) {
                            $stage_value = $stage_values['element_name'];

                            $identifier_start = $stage_value;

                            foreach ($substages as $substage) {
                                if ($substage->getId() == $identifier_start) {
                                    $last_form_id = $apform->getFormId();
                                    if(!in_array($last_form_id, $forms_array))
                                    {
                                        $forms_array[] = $last_form_id;
                                        $form_count++;
                                    }
                                }
                            }
                        }
                    }
                }

                if($_GET['formid'])
                {
                    $last_form_id = $_GET['formid'];
                }

                if($_GET['id'])
                {
                    $last_form_id = $_GET['id'];
                }

                if ($_GET['formid'] || $_GET['id']) {
                    $_GET['formid'] = $last_form_id;
                    $_SESSION['formid'] = $last_form_id;
                    include_partial("form", array("formid" => $last_form_id));
                } elseif (sizeof($forms_array) > 1) {
                    ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion"
                                   href="#collapse1">
                                    Application Forms
                                </a>
                            </h4>
                        </div>
                        <div id="collapse1"
                             class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped mb30">
                                        <thead>
                                        <tr>
                                            <th width="30%">Form Name</th>
                                            <th width="70%">Form Description</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $count = 0;
                                        foreach ($forms_array as $form) {
                                            $q = Doctrine_Query::create()
                                                ->from('ApForms a')
                                                ->where('a.form_id = ?', $form);
                                            $apform = $q->fetchOne();
                                            $count++;
                                            ?>
                                            <tr>
                                                <td>
                                                    <a href="/backend.php/applications/create?formid=<?php echo $apform->getFormId(); ?>"><?php echo $apform->getFormName(); ?></a>
                                                </td>
                                                <td>
                                                    <a href="/backend.php/applications/create?formid=<?php echo $apform->getFormId(); ?>"><?php echo $apform->getFormDescription(); ?></a>
                                                </td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                }
            }

            if ($_SESSION['formid'] && empty($form_count)) {
                $_GET['id'] = $_SESSION['formid'];
                include_partial("form", array("formid" => $_SESSION['formid']));
            }

            if ((empty($current_stage) || $form_count == 0) && $_SESSION['formid'] == "") {

                if ($_GET['formid']) {
                    include_partial("form", array("formid" => $_GET['formid']));
                } elseif ($_GET['id']) {
                    include_partial("form", array("formid" => $_GET['id']));
                } else {
                    ?>

                    <div class="panel-group" id="accordion">

                        <?php
                        $q = Doctrine_Query::create()
                            ->from('FormGroups a')
                            ->orderBy('a.group_name ASC');
                        $groups = $q->execute();
                        $groupcount = 0;
                        foreach ($groups as $group) {
                            $groupcount++;
                            $group_name = $group->getGroupName();
                            $group_description = $group->getGroupDescription();
                            ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion"
                                           href="#collapse<?php echo $count; ?>">
                                            <?php echo $group_name; ?>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse<?php echo $count; ?>"
                                     class="panel-collapse collapse <?php if ($groupcount) { ?>in<?php } ?>">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-striped mb30">
                                                <thead>
                                                <tr>
                                                    <th width="30%">Form Name</th>
                                                    <th width="70%">Form Description</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                $count = 0;
                                                $q = Doctrine_Query::create()
                                                    ->from('ApForms a')
                                                    ->where('a.form_group = ?', $group->getGroupId())
                                                    ->andWhere('a.form_type = 1')
                                                    ->andWhere('a.form_active = 1')
                                                    ->orderBy('a.form_name ASC');
                                                $applications = $q->execute();
                                                foreach ($applications as $apform) {
                                                    $count++;
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <a href="/backend.php/applications/create?formid=<?php echo $apform->getFormId(); ?>"><?php echo $apform->getFormName(); ?></a>
                                                        </td>
                                                        <td>
                                                            <a href="/backend.php/applications/create?formid=<?php echo $apform->getFormId(); ?>"><?php echo $apform->getFormDescription(); ?></a>
                                                        </td>
                                                    </tr>
                                                <?php
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
                        }


                        $q = Doctrine_Query::create()
                            ->from("ApForms a")
                            ->where("a.form_type = 1")
                            ->andWhere("a.form_group = 0 or a.form_group = ''")
                            ->andWhere('a.form_active = 1');
                        $apforms = $q->execute();
                        $count = 0;
                        foreach ($apforms as $apform) {
                            ?>
                            <div class="panel panel-dark">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion"
                                           href="#collapseap<?php echo $count; ?>">
                                            <?php echo $apform->getFormName(); ?>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseap<?php echo $count; ?>"
                                     class="panel-collapse collapse <?php if ($count == 0) { ?>in<?php } ?>">
                                    <div class="panel-body">
                                        <a class="btn btn-primary"
                                           href="/backend.php/applications/create?formid=<?php echo $apform->getFormId(); ?>">Apply
                                            Now</a>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $count++;
                        }
                        ?>

                    </div>

                <?php
                }
            }
            ?>
            <button type="submit" class="btn btn-danger" id="submitpermissions" style="float: left;"
                    onClick="window.location='/backend.php/applications/create';">Back
            </button>
            <br>
            <br>
        <?php
        }
        ?>
    </div>

    </div>
    <!-- tab-content -->

    </div>
    <!-- #basicWizard -->

    </div>
    </div>
    </div>



    <script>
        jQuery(document).ready(function () {

            // Basic Wizard
            jQuery('#basicWizard').bootstrapWizard();

            // Progress Wizard
            $('#progressWizard').bootstrapWizard({
                'nextSelector': '.next',
                'previousSelector': '.previous',
                onNext: function (tab, navigation, index) {
                    var $total = navigation.find('li').length;
                    var $current = index + 1;
                    var $percent = ($current / $total) * 100;
                    jQuery('#progressWizard').find('.progress-bar').css('width', $percent + '%');
                },
                onPrevious: function (tab, navigation, index) {
                    var $total = navigation.find('li').length;
                    var $current = index + 1;
                    var $percent = ($current / $total) * 100;
                    jQuery('#progressWizard').find('.progress-bar').css('width', $percent + '%');
                },
                onTabShow: function (tab, navigation, index) {
                    var $total = navigation.find('li').length;
                    var $current = index + 1;
                    var $percent = ($current / $total) * 100;
                    jQuery('#progressWizard').find('.progress-bar').css('width', $percent + '%');
                }
            });

            // Disabled Tab Click Wizard
            jQuery('#disabledTabWizard').bootstrapWizard({
                tabClass: 'nav nav-pills nav-justified nav-disabled-click',
                onTabClick: function (tab, navigation, index) {
                    return false;
                }
            });

            // With Form Validation Wizard
            var $validator = jQuery("#firstForm").validate({
                highlight: function (element) {
                    jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                success: function (element) {
                    jQuery(element).closest('.form-group').removeClass('has-error');
                }
            });

            jQuery('#validationWizard').bootstrapWizard({
                tabClass: 'nav nav-pills nav-justified nav-disabled-click',
                onTabClick: function (tab, navigation, index) {
                    return false;
                },
                onNext: function (tab, navigation, index) {
                    var $valid = jQuery('#firstForm').valid();
                    if (!$valid) {

                        $validator.focusInvalid();
                        return false;
                    }
                }
            });


        });
    </script>
<?php
}
?>