<?php
	/**
	 * editcolumns template.
	 *
	 * Displays form to allow the user to set configure a custom layout for the applications list
	 *
	 * @package    backend
	 * @subpackage applications
	 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
	 */
    $prefix_folder = dirname(__FILE__)."/../../../../..";

	require($prefix_folder.'/lib/vendor/cp_form/config.php');
	require($prefix_folder.'/lib/vendor/cp_form/includes/check-session.php');
	require($prefix_folder.'/lib/vendor/cp_form/includes/db-core.php');
	require($prefix_folder.'/lib/vendor/cp_form/includes/db-functions.php');
	require($prefix_folder.'/lib/vendor/cp_form/includes/helper-functions.php');
	require($prefix_folder.'/lib/vendor/cp_form/includes/entry-functions.php');

    require_once $prefix_folder.'/lib/vendor/cp_workflow/config/config.inc.php';
	require_once $prefix_folder.'/lib/vendor/cp_workflow/language_files/language.inc.php';
	require_once $prefix_folder.'/lib/vendor/cp_workflow/pages/CCirculation.inc.php';
	require_once $prefix_folder.'/lib/vendor/cp_workflow/pages/version.inc.php';
	
	connect_db();				
?>
	
		

<div class="g12">
    <form>
    	<label>Edit Columns</label>
    </form>
	<form action='/backend.php/applications/editcolumns' method='post' >
	<?php
	    //Display list of all application forms and allow the user to select which fields will make up the custom table layout for each
		$q = Doctrine_Query::create()
			->from('ApForms a')
			->where('a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ?',array('6','7','15','16','17'))
			->orderBy('a.form_name ASC');
		$forms = $q->execute();
		
		//Iterate through all forms				
		foreach($forms as $form)
		{
			$formid = $form->getFormId();
	?> 
			<fieldset>
				<label><?php echo $form->getFormName(); ?></label>
					<section>
						<label>Content</label>
				 			<div>
								<?php
                                if($formid != "")
                                {
                                ?>
									<select name="fields_<?php echo $form->getFormId(); ?>" id="fields_<?php echo $form->getFormId(); ?>" multiple>
									<?php
                                    $selected = "";
                                    $q = Doctrine_Query::create()
                                         ->from('ApplicationOverviewColumns a')
                                         ->where('a.form_id = ?', $formid)
                                         ->andWhere('a.element_id = ?', "{sf_email}");
                                    $existing = $q->execute();
                                    if(sizeof($existing) > 0)
                                    {
                                        $selected = "selected";
                                    }
                                    ?>
                                    <option value='{sf_email}' <?php echo $selected; ?>>Applicant's Email</option>
                                    <?php
                                    $selected = "";
                                    $q = Doctrine_Query::create()
                                         ->from('ApplicationOverviewColumns a')
                                         ->where('a.form_id = ?', $formid)
                                         ->andWhere('a.element_id = ?', "{sf_fullname}");
                                    $existing = $q->execute();
                                    if(sizeof($existing) > 0)
                                    {
                                        $selected = "selected";
                                    }
                                    ?>
                                    <option value='{sf_fullname}' <?php echo $selected; ?>>Applicant's Fullname</option>
                                    <?php
                                    $selected = "";
                                    $q = Doctrine_Query::create()
                                         ->from('ApplicationOverviewColumns a')
                                         ->where('a.form_id = ?', $formid)
                                         ->andWhere('a.element_id = ?', "{sf_username}");
                                    $existing = $q->execute();
                                    if(sizeof($existing) > 0)
                                    {
                                        $selected = "selected";
                                    }
                                    ?>
                                    <option value='{sf_username}' <?php echo $selected; ?>>Applicant's Username</option>
                                    <?php
                                            $q = Doctrine_Query::create()
                                               ->from('apFormElements a')
                                               ->where('a.form_id = ?', 15);
                                               
                                            $elements = $q->execute();
                                            
                                            foreach($elements as $element)
                                            {
                                                $childs = $element->getElementTotalChild();
                                                if($childs == 0)
                                                {
                                                   $selected = "";
                                                   $q = Doctrine_Query::create()
                                                         ->from('ApplicationOverviewColumns a')
                                                         ->where('a.form_id = ?', $formid)
                                                         ->andWhere('a.element_id = ?', "{sf_element_".$element->getElementId()."}");
                                                    $existing = $q->execute();
                                                    if(sizeof($existing) > 0)
                                                    {
                                                        $selected = "selected";
                                                    }
                                                   echo "<option value='{sf_element_".$element->getElementId()."}' ".$selected.">".$element->getElementTitle()."</option>";
                                                }
                                                else
                                                {
                                                    if($element->getElementType() == "select")
                                                    {
                                                         $selected = "";
                                                           $q = Doctrine_Query::create()
                                                                 ->from('ApplicationOverviewColumns a')
                                                                 ->where('a.form_id = ?', $formid)
                                                                 ->andWhere('a.element_id = ?', "{sf_element_".$element->getElementId()."}");
                                                            $existing = $q->execute();
                                                            if(sizeof($existing) > 0)
                                                            {
                                                                $selected = "selected";
                                                            }
                                                        echo "<option value='{sf_element_".$element->getElementId()."}' ".$selected.">".$element->getElementTitle()."</option>";
                                                    }
                                                    else
                                                    {
                                                        for($x = 0; $x < ($childs + 1); $x++)
                                                        {
                                                            $selected = "";
                                                               $q = Doctrine_Query::create()
                                                                     ->from('ApplicationOverviewColumns a')
                                                                     ->where('a.form_id = ?', $formid)
                                                                     ->andWhere('a.element_id = ?', "{sf_element_".$element->getElementId()."_".($x+1)."}");
                                                                $existing = $q->execute();
                                                                if(sizeof($existing) > 0)
                                                                {
                                                                    $selected = "selected";
                                                                }
                                                            echo "<option value='{sf_element_".$element->getElementId()."_".($x+1)."}' ".$selected.">".$element->getElementTitle()."</option>";
                                                        }
                                                    }
                                                }
                                            }
                                    ?>
                                    <?php 
                                    //Get Application Information (anything starting with ap_ )
                                               //ap_application_id
                                            
                                    //Get Form Details (anything starting with fm_ )
                                               //fm_created_at, fm_updated_at.....fm_element_1
                                               
                                               $selected = "";
                                               $q = Doctrine_Query::create()
                                                     ->from('ApplicationOverviewColumns a')
                                                     ->where('a.form_id = ?', $formid)
                                                     ->andWhere('a.element_id = ?', "{ap_application_id}");
                                                $existing = $q->execute();
                                                if(sizeof($existing) > 0)
                                                {
                                                    $selected = "selected";
                                                }
                                    ?>
                                    <option value='{ap_application_id}' <?php echo $selected; ?>>Application Number</option>
                                    <?php
                                    $selected = "";
                                    $q = Doctrine_Query::create()
                                         ->from('ApplicationOverviewColumns a')
                                         ->where('a.form_id = ?', $formid)
                                         ->andWhere('a.element_id = ?', "{fm_created_at}");
                                    $existing = $q->execute();
                                    if(sizeof($existing) > 0)
                                    {
                                        $selected = "selected";
                                    }
                                    ?>
                                    <option value='{fm_created_at}' <?php echo $selected; ?>>Submitted On</option>
                                    <?php
        
                                            $q = Doctrine_Query::create()
                                               ->from('apFormElements a')
                                               ->where('a.form_id = ?', $formid);
                                               
                                            $elements = $q->execute();
                                            
                                            foreach($elements as $element)
                                            {
                                                $childs = $element->getElementTotalChild();
                                                if($childs == 0)
                                                {
                                                    $selected = "";
                                                    $q = Doctrine_Query::create()
                                                         ->from('ApplicationOverviewColumns a')
                                                         ->where('a.form_id = ?', $formid)
                                                         ->andWhere('a.element_id = ?', "{fm_element_".$element->getElementId()."}");
                                                    $existing = $q->execute();
                                                    if(sizeof($existing) > 0)
                                                    {
                                                        $selected = "selected";
                                                    }
                                                   echo "<option value='{fm_element_".$element->getElementId()."}' ".$selected.">".$element->getElementTitle()."</option>";
                                                }
                                                else
                                                {
                                                    if($element->getElementType() == "select")
                                                    {
                                                        $selected = "";
                                                        $q = Doctrine_Query::create()
                                                             ->from('ApplicationOverviewColumns a')
                                                             ->where('a.form_id = ?', $formid)
                                                             ->andWhere('a.element_id = ?', "{fm_element_".$element->getElementId()."}");
                                                        $existing = $q->execute();
                                                        if(sizeof($existing) > 0)
                                                        {
                                                            $selected = "selected";
                                                        }
                                                        echo "<option value='{fm_element_".$element->getElementId()."}' ".$selected.">".$element->getElementTitle()."</option>";
                                                    }
                                                    else
                                                    {
                                                        for($x = 0; $x < ($childs + 1); $x++)
                                                        {
                                                            $selected = "";
                                                            $q = Doctrine_Query::create()
                                                                 ->from('ApplicationOverviewColumns a')
                                                                 ->where('a.form_id = ?', $formid)
                                                                 ->andWhere('a.element_id = ?', "{fm_element_".$element->getElementId()."_".($x+1)."}");
                                                            $existing = $q->execute();
                                                            if(sizeof($existing) > 0)
                                                            {
                                                                $selected = "selected";
                                                            }
                                                            echo "<option value='{fm_element_".$element->getElementId()."_".($x+1)."}' ".$selected.">".$element->getElementTitle()."</option>";
                                                        }
                                                    }
                                                }
                                            }
                                            
                                    $selected = "";
                                    $q = Doctrine_Query::create()
                                         ->from('ApplicationOverviewColumns a')
                                         ->where('a.form_id = ?', $formid)
                                         ->andWhere('a.element_id = ?', "{ca_conditions}");
                                    $existing = $q->execute();
                                    if(sizeof($existing) > 0)
                                    {
                                        $selected = "selected";
                                    }		
                                    ?>
                                    <option value="{ca_conditions}" <?php echo $selected; ?>>Conditions of Approval</option>
                                    <?php
                                    $selected = "";
                                    $q = Doctrine_Query::create()
                                         ->from('ApplicationOverviewColumns a')
                                         ->where('a.form_id = ?', $formid)
                                         ->andWhere('a.element_id = ?', "{mini_ca_conditions}");
                                    $existing = $q->execute();
                                    if(sizeof($existing) > 0)
                                    {
                                        $selected = "selected";
                                    }	
                                    ?>
                                    <option value="{mini_ca_conditions}" <?php echo $selected; ?>>Minimized Conditions Approval</option>
                                    <?php
                                    $selected = "";
                                    $q = Doctrine_Query::create()
                                         ->from('ApplicationOverviewColumns a')
                                         ->where('a.form_id = ?', $formid)
                                         ->andWhere('a.element_id = ?', "{other_conditions}");
                                    $existing = $q->execute();
                                    if(sizeof($existing) > 0)
                                    {
                                        $selected = "selected";
                                    }	
                                    ?>
                                    <option value="{other_conditions}" <?php echo $selected; ?>>Other Conditions</option>
                                    <?php
                                    $selected = "";
                                    $q = Doctrine_Query::create()
                                         ->from('ApplicationOverviewColumns a')
                                         ->where('a.form_id = ?', $formid)
                                         ->andWhere('a.element_id = ?', "{in_total}");
                                    $existing = $q->execute();
                                    if(sizeof($existing) > 0)
                                    {
                                        $selected = "selected";
                                    }	
                                    ?>
                                    <option value="{in_total}" <?php echo $selected; ?>>Invoice Total Amount</option>
                            </select>
                                <?php
					}
					?>
					</div>
			  	</section>
			  <section>
			  <label>Custom Headers</label>
			   	<div>
				<table>
                    <thead>
                    <tr>
                   	 <th>Field</th><th>Custom Name</th>
                    </tr>
                    </thead>
					<tbody>
					<?php
                    $parser = new Templateparser();
                    
                    $q = Doctrine_Query::create()
                     ->from('ApplicationOverviewColumns a')
                     ->where('a.form_id = ?', $formid);
                    $fields = $q->execute();
                    foreach($fields as $field)
                    {
                    ?>
                        <tr><td><?php echo $parser->parseHeaders($formid,$field->getElementId()); ?></td><td><input type='text' name='headers_<?php echo $form->getFormId(); ?>[]' value='<?php echo $field->getCustomHeader(); ?>'></td></tr>
                    <?php
                    }
                    ?>
					</tbody>
				</table>
				</div>
			  </section>
			  </fieldset>
              <?php
                }
			  ?>
			 <fieldset>
			 <section>
					<div><button class="reset">Reset</button><button class="submit" name="submitbuttonname" value="submitbuttonvalue">Submit</button></div>
			 </section>
			 </fieldset>
			</form>
			<p><br><b>Warning!</b> Changing this will result in you being unable to list all 
			applications in one view but instead you will need to filter to access the different applications.</p>
			<br>
</div>
