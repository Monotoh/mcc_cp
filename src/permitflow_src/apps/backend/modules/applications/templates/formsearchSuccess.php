<?php

$formid = $_POST['formid'];
?>
<div   style='height: 250px; overflow-y: auto;'>
<fieldset>
<?php
  $q = Doctrine_Query::create()
	  ->from('ApFormElements a')
	  ->where('a.form_id = ?', $formid)
	  ->andWhere('a.element_type <> ? AND a.element_type <> ? AND a.element_type <> ?', array('section','file','section'))
	  ->andWhere('a.element_status = ?', 1)
	  ->orderBy('a.element_position ASC');
	$fields = $q->execute();
	foreach($fields as $field)
	{
		?>
		<section>
		<label><?php echo $field->getElementTitle(); ?></label>
		<div>
		<?php
		if($field->getElementType() == "select")
		{
			$q = Doctrine_Query::create()
			  ->from('ApElementOptions a')
			  ->where('a.element_id = ?', $field->getElementId())
			  ->andWhere('a.form_id = ?', $formid)
			  ->andWhere('a.live = ?', 1)
			  ->orderBy('a.position ASC');
			$options = $q->execute();
			echo "<select name='element_".$field->getElementId()."' id='element_".$field->getElementId()."'>";
			echo "<option value=''></option>";
			foreach($options as $option)
			{
				echo "<option value='".$option->getOptionId()."'>".$option->getOption()."</option>";
			}
			echo "</select>";
		}
		else
		{
		?>
			<input type='text' name='element_<?php echo $field->getElementId(); ?>' id='element_<?php echo $field->getElementId(); ?>'>
		<?php
		}
		?>
		</div>
		</section>
		<?php
	}
?>
</fieldset>
</div>
