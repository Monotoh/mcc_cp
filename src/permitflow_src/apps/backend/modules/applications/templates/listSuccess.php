<?php
/**
 * list template.
 *
 * Displays a list of applications ordered by most recent first
 *
 * @package    backend
 * @subpackage applications
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>
<div class="pageheader">
  <h2><i class="fa fa-envelope"></i> Applications <span>List of submitted applications</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>backend.php">Home</a></li>
      <li class="active">Applications</li>
    </ol>
  </div>
</div>





<div class="contentpanel panel-email">

    <div class="row">
        <?php include_partial('listsidebar', array('applications_size' => $pager->getNbResults())) ?>
        <div class="col-lg-10">
            
            <div class="panel panel-dark">
                   <div class="panel-heading">
                    <h3 class="panel-title">Applications</h3>
                    <p class="text-muted">Showing<?php if($pager->getPage() == 1){ echo "1"; }else{ $from = ($pager->getPage() - 1)*15; echo $from; } ?> - <?php if($pager->getPage() == 1){ echo "15"; }else{ $from = ($pager->getPage() - 1)*15; echo ($from+15); } ?> of <?php echo $pager->getNbResults(); ?> Applications</p>
                    </div>
           <div class="panel-body panel-body-nopadding">   
                    <div class="table-responsive">
                        <table class="table dt-on-steroids mb0" id="table2">
                        <thead>
                        <th  class="no-sort"></th>
                        <th  class="no-sort"></th>
                        <th>Application No.</th>
                        <th  class="no-sort" width="40%">Progress</th>
                        <th>Submission Date</th>
                        <th  class="no-sort" width="5%">Actions</th>
                        </thead>
                          <tbody>
                          <?php foreach ($pager->getResults() as $application): ?>
                            <?php include_partial('list', array('application' => $application)) ?>
                          <?php endforeach; ?>
                          </tbody>
                        </table>
                    </div><!-- table-responsive -->
                    
                </div><!-- panel-body -->
            </div><!-- panel -->
            
        </div><!-- col-sm-9 -->
        
    </div><!-- row -->
</div>
