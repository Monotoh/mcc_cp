<?php
/**
 * list template.
 *
 * Displays a list of applications ordered by most recent first
 *
 * @package    backend
 * @subpackage applications
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");

include_partial('dashboard/checksession');

function GetDaysSince($sStartDate, $sEndDate){
	//$start_ts = strtotime($sStartDate);
        $start_ts = strtotime(date("Y-m-d", strtotime($sStartDate)));
	$end_ts = strtotime($sEndDate);
        //today 
        $today = date('Y-m-d');
	$diff = $end_ts - $start_ts;
        //prevent -ve values because of missing entries in Application Reference
        if($diff < 0 ){
            
            $diff = strtotime($today) - $start_ts ;
        }
	return round($diff / 86400);
}

if($sf_user->mfHasCredential("access_applications"))
{
?>
<div class="pageheader">
  <h2><i class="fa fa-envelope"></i><?php echo __("Applications"); ?></h2>
  <div class="breadcrumb-wrapper">
    <span class="label"><?php echo __("You are here"); ?>:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>backend.php"><?php echo __("Home"); ?></a></li>
      <li><a href="<?php echo public_path(); ?>backend.php/applications/list/get/your"><?php echo __("Applications"); ?></a></li>
      <li class="active">Stages</li>
    </ol>
  </div>
</div>

<div class="contentpanel panel-email">

    <div class="row">
       
        <?php include_partial('listsidebar', array('form_id'=> $application_form,'stage' => $stage,'has_logic' => $has_logic)) ?>
        
        <div class="col-lg-10">
           <div class="panel panel-default">
			<label style="height: 18px;">
				<div style="float: right; font-size: 11px;">
				 <img src='<?php echo public_path(); ?>asset_img/greenkey.png' /> <?php echo __("0 -10 days") ?>
				 <img src='<?php echo public_path(); ?>asset_img/yellowkey.png' /> <?php echo __("10 - 20 days") ?>
				 <img src='<?php echo public_path(); ?>asset_img/redkey.png' /> <?php echo __("More than 20 days")?>
				 <img src='<?php echo public_path(); ?>asset_img/greykey.png' /> <?php echo __("Archived") ?>
                                 |<img src='' alt="<?php echo __("None: Approved") ?>" /> 
				</div>
			</label>
           <div class="panel-heading">
            <h3 class="panel-title"><?php echo __("Applications"); ?></h3>
            <!--<p class="text-muted"><?php echo __("Showing"); ?> <?php echo count($applications); ?> <?php echo __("Applications"); ?> <br> <a href='<?php echo $export_link; ?>'>Export to Excel</a></p>
            <div class="pull-right" style="margin-top: -45px;">
			<form id="filter_apps" name="filter_apps" action="#" method="post"> -->
            <?php
            	/*$link = "";
            	if($stage != "")
            	{
                	$link = "/backend.php/applications/listgroup/subgroup/".$stage;
            	}
            	else
            	{
                	$link = "/backend.php/applications/listgroup/group/".$group;
            	}

            if(!$has_logic) {
                ?>
                <select id='application_form' name='application_form' class='form-control' class="pull-right">
                    <option value="0">Filter By Form...</option>
                    <?php
                    //Get list of all available applications categorized by groups
                    $q = Doctrine_Query::create()
                        ->from('FormGroups a');
                    $groups = $q->execute();

                    if (sizeof($groups) > 0) {
                        foreach ($groups as $group) {
                            echo "<optgroup label='" . $group->getGroupName() . "'>";

                            $q = Doctrine_Query::create()
                                ->from('ApForms a')
                                ->where('a.form_group = ?', $group->getGroupId())
                                ->andWhere('a.form_type = ?', 1)
                                ->andWhere('a.form_active = ?', 1);
                            $forms = $q->execute();

                            $count = 0;

                            foreach ($forms as $form) {
                                $selected = "";

                                if ($application_form != "" && $application_form == $form->getFormId()) {
                                    $selected = "selected='selected'";
                                    $_GET['form'] = $form;
                                }

                                echo "<option value='" . $form->getFormId() . "' " . $selected . ">" . $form->getFormName() . "</option>";

                                $count++;
                            }
                            echo "</optgroup>";
                        }
                    } else {
                        $q = Doctrine_Query::create()
                            ->from('ApForms a')
                            ->where('a.form_type = ?', '1')
                            ->andWhere('a.form_active = ?', 1)
                            ->orderBy('a.form_id ASC');
                        $forms = $q->execute();

                        $count = 0;

                        echo "<optgroup label='Application Forms'>";

                        foreach ($forms as $form) {

                            $selected = "";

                            if ($application_form != "" && $application_form == $form->getFormId()) {
                                $selected = "selected='selected'";
                            }


                            echo "<option value='" . $form->getFormId() . "' " . $selected . ">" . $form->getFormName() . "</option>";

                            $count++;
                        }

                        echo "</optgroup>";
                    } */
                    ?>
                <!--</select>

                <div id="ajaxdropdownvaluefields" class="pull-right">-->
                    <?php
                   /* if ($application_form) {
                        $q = Doctrine_Query::create()
                            ->from('ApFormElements a')
                            ->where('a.form_id = ?', $application_form)
                            ->andWhere('a.element_status = ?', 1)
                            ->andWhere('a.element_type LIKE ?', '%select%')
                            ->orderBy('a.element_title ASC');
                        $elements = $q->execute();

                        echo "<select name='form_dropdown_fields' id='form_dropdown_fields' class='form-control'>";
                        echo "<option>Choose a dropdown field...</option>";
                        foreach ($elements as $element) {
                            $selected = "";
                            if ($form_dropdown_fields == $element->getElementId()) {
                                $selected = "selected='selected'";
                            }
                            echo "<option value='" . $element->getElementId() . "' " . $selected . ">" . $element->getElementTitle() . "</option>";
                        }
                        echo "</select>";
                    }*/
                    ?>
               <!-- </div>

                <div id="filterdropdown" class="pull-right"> -->
                    <?php
                    /*if ($form_dropdown_fields) {
                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.element_id = ?', $form_dropdown_fields)
                            ->andWhere('a.live = 1')
                            ->orderBy('a.position ASC');
                        $options = $q->execute();

                        echo "<select name='form_dropdown_value_fields' id='form_dropdown_value_fields' class='form-control' onChange='document.getElementById(\"filter_apps\").submit();'>";
                        echo "<option>Filter by an option..</option>";
                        foreach ($options as $option) {
                            $selected = "";
                            if ($form_dropdown_value_fields == $option->getOptionId()) {
                                $selected = "selected='selected'";
                            }
                            echo "<option value='" . $option->getOptionId() . "' " . $selected . ">" . $option->getOption() . "</option>";
                        }
                        echo "</select>"; */
                   // }
                    ?>
                <!--</div> -->

                <!--<script language="javascript">
                    jQuery(document).ready(function () {
                        jQuery("#application_form").change(function () {
                            var selecteditem = this.value;
                            $.ajax({
                                url: "/backend.php/applications/getdropdowns?formid=" + selecteditem,
                                success: function (result) {
                                    $("#filterdropdown").html(result);
                                }
                            });
                        });
                    });
                </script> -->
            <?php
           /* }
						else
						{
							?>
							<select id='application_form_logic' name='application_form_logic' class='form-control' class="pull-right" onChange="window.location='/backend.php/applications/listgroup/subgroup/<?php echo $stage; ?>/form/' + this.value;">
									<option value="0">Filter By Form...</option>
									<?php
									//Get list of all available applications categorized by groups
									$q = Doctrine_Query::create()
											->from('FormGroups a');
									$groups = $q->execute();

									if (sizeof($groups) > 0) {
											foreach ($groups as $group) {
													echo "<optgroup label='" . $group->getGroupName() . "'>";

													$q = Doctrine_Query::create()
															->from('ApForms a')
															->where('a.form_group = ?', $group->getGroupId())
															->andWhere('a.form_type = ?', 1)
															->andWhere('a.form_active = ?', 1);
													$forms = $q->execute();

													$count = 0;

													foreach ($forms as $form) {
															$selected = "";

															if ($application_form != "" && $application_form == $form->getFormId()) {
																	$selected = "selected='selected'";
																	$_GET['form'] = $form;
															}

															echo "<option value='" . $form->getFormId() . "' " . $selected . ">" . $form->getFormName() . "</option>";

															$count++;
													}
													echo "</optgroup>";
											}
									} else {
											$q = Doctrine_Query::create()
													->from('ApForms a')
													->where('a.form_type = ?', '1')
													->andWhere('a.form_active = ?', 1)
													->orderBy('a.form_id ASC');
											$forms = $q->execute();

											$count = 0;

											echo "<optgroup label='Application Forms'>";

											foreach ($forms as $form) {

													$selected = "";

													if ($application_form != "" && $application_form == $form->getFormId()) {
															$selected = "selected='selected'";
													}


													echo "<option value='" . $form->getFormId() . "' " . $selected . ">" . $form->getFormName() . "</option>";

													$count++;
											}

											echo "</optgroup>";
									}
									?>
							</select>
							<?php
						} */
                ?>
				<!--</form>
            </div>
       </div>-->
               <?php 
                function GetLastDayWorkedOn($application){//OTB - Use last day of action on application. Work can still continue after a permit has been issued
							$q = Doctrine_Query::create()
								->from('ApplicationReference b')
								->where('b.application_id = ?', $application->getId())
								->andWhere('b.start_date IS NOT NULL')
								->andWhere('b.end_date IS NOT NULL')
								->andWhere('b.start_date <= b.end_date')
								->orderBy("b.id DESC");
							$application_reference_list = $q->execute();
                                                        error_log("Application >>> ".$application->getApplicationId(). " End date ".$application_reference_list[0]->getEndDate());
							return $application_reference_list[0]->getEndDate();
						}
               ?>
       <div class="panel-body panel-body-nopadding">
		 <!-- OTB patch -- list apps data-table -->
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
					
							<div class="portlet-body">
								<table class="table table-hover" id="bonie">
								<thead>
								<tr>
									<th>
									    <?php echo __("Application No"); ?>
									</th>
									<th>
									<?php echo __("Submission Date"); ?>
									</th>
									<th>
									<?php echo __("Approval Stage") ?>
									</th>
									<th>
										 <?php echo __("Submitted By") ?>
									</th>
									<th>
									   <?php echo __("Actions"); ?>
									</th>
								</tr>
								</thead>
								<tbody>
									
								<?php foreach ($applications->getResults() as $application): ?>
								
									<?php
										$days = GetDaysSince($application->getDateOfSubmission(), date("Y-m-d"));
									?>
									<?php include_partial('list2', array('application' => $application, 'days' => $days)) ?>
								<?php endforeach; ?>
								
								
								</tbody>
								</table>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
						
		 <!-- end -->

	    
        </div><!-- panel -->

        </div><!-- col-sm-9 -->

    </div><!-- row -->
	</div>
	<?php
}
else
{
	include_partial("settings/accessdenied");
}
?>
<script src="<?php echo public_path(); ?>metronicv3/assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script>
	jQuery(document).ready(function(){
var table = $('#bonie');

/* Formatting function for row details */
function fnFormatDetails(oTable, nTr) {
	var aData = oTable.fnGetData(nTr);
	var sOut = '<table>';
	sOut += '<tr><td>Application No:</td><td>' + aData[1] + '</td></tr>';
	sOut += '<tr><td> Submission Date :</td><td>' + aData[2] + '</td></tr>';
	sOut += '<tr><td>Approval Stage:</td><td>' + aData[3] + '</td></tr>';
	sOut += '<tr><td>Submitted By:</td><td>' + aData[4] + '</td></tr>';
	sOut += '</table>';

	return sOut;
}

/*
 * Insert a 'details' column to the table
 */
var nCloneTh = document.createElement('th');
nCloneTh.className = "table-checkbox";

var nCloneTd = document.createElement('td');
nCloneTd.innerHTML = '<span class="row-details row-details-close"></span>';

table.find('thead tr').each(function () {
	this.insertBefore(nCloneTh, this.childNodes[0]);
});

table.find('tbody tr').each(function () {
	this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
});

/*
 * Initialize DataTables, with no sorting on the 'details' column
 */
var oTable = table.dataTable({
	"columnDefs": [{
		"orderable": false,
		"targets": [0]
	}],
	"order": [
		[1, 'asc']
	],
	"lengthMenu": [
		[5, 15, 20, -1],
		[5, 15, 20, "All"] // change per page values here
	],
	// set the initial value
	"pageLength": 10,
});
var tableWrapper = $('#bonie_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

/* Add event listener for opening and closing details
 * Note that the indicator for showing which row is open is not controlled by DataTables,
 * rather it is done here
 */
table.on('click', ' tbody td .row-details', function () {
	var nTr = $(this).parents('tr')[0];
	if (oTable.fnIsOpen(nTr)) {
		/* This row is already open - close it */
		$(this).addClass("row-details-close").removeClass("row-details-open");
		oTable.fnClose(nTr);
	} else {
		/* Open this row */
		$(this).addClass("row-details-open").removeClass("row-details-close");
		oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
	}
});
});
</script>
