<?php
/**
 * list template.
 *
 * Displays a list of applications ordered by most recent first
 *
 * @package    backend
 * @subpackage applications
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>
<div class="pageheader">
      <h2><i class="fa fa-envelope"></i> Applications <span>List of submitted applications</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo public_path(); ?>backend.php">Home</a></li>
          <li><a href="<?php echo public_path(); ?>backend.php/applications/list/get/your">Applications</a></li>
          <li class="active">Trash</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel panel-email">

        <div class="row">
            <?php include_partial('listsidebar', array('applications_size' => $pager->getNbResults())) ?>
            
            <div class="col-lg-10">
                
                <div class="panel panel-default">
                    <div class="panel-body">
                        
                        <div class="pull-right">
                            <?php include_partial('listtopbar') ?>
                            <?php if($pager->haveToPaginate()): ?>
                            <div class="btn-group mr10">
                                <button class="btn btn-white" type="button" onclick="window.location='<?php echo public_path(); ?>backend.php/applications/list/get/your/page/<?php echo $pager->getPreviousPage(); ?>';"><i class="glyphicon glyphicon-chevron-left"></i></button>
                                <button class="btn btn-white" type="button" onclick="window.location='<?php echo public_path(); ?>backend.php/applications/list/get/your/page/<?php echo $pager->getNextPage(); ?>';"><i class="glyphicon glyphicon-chevron-right"></i></button>
                            </div>
                            <?php endif; ?>
                        </div><!-- pull-right -->
                        
                        <h5 class="subtitle mb5">Deleted Applications</h5>
                        <p class="text-muted">Showing <?php if($pager->getPage() == 1){ echo "1"; }else{ $from = ($pager->getPage() - 1)*15; echo $from; } ?> - <?php if($pager->getPage() == 1){ echo "15"; }else{ $from = ($pager->getPage() - 1)*15; echo ($from+15); } ?> of <?php echo $pager->getNbResults(); ?> deleted Applications</p>
                        
                        <div class="table-responsive">
                            <table class="table table-email">
                              <tbody>
                              <?php foreach ($pager->getResults() as $application): ?>
                              	<?php include_partial('list', array('application' => $application)) ?>
                              <?php endforeach; ?>
                              </tbody>
                            </table>
                        </div><!-- table-responsive -->
                        
                    </div><!-- panel-body -->
                </div><!-- panel -->
                
            </div><!-- col-sm-9 -->
            
        </div><!-- row -->
    
    </div>