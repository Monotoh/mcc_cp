<?php
    $prefix_folder = dirname(__FILE__)."/../../../../..";

	require($prefix_folder.'/lib/vendor/cp_form/config.php');
	require($prefix_folder.'/lib/vendor/cp_form/includes/check-session.php');
	require($prefix_folder.'/lib/vendor/cp_form/includes/db-core.php');
	require($prefix_folder.'/lib/vendor/cp_form/includes/db-functions.php');
	require($prefix_folder.'/lib/vendor/cp_form/includes/helper-functions.php');
	require($prefix_folder.'/lib/vendor/cp_form/includes/entry-functions.php');

    require_once $prefix_folder.'/lib/vendor/cp_workflow/config/config.inc.php';
	require_once $prefix_folder.'/lib/vendor/cp_workflow/language_files/language.inc.php';
	require_once $prefix_folder.'/lib/vendor/cp_workflow/pages/CCirculation.inc.php';
	require_once $prefix_folder.'/lib/vendor/cp_workflow/pages/version.inc.php';
	

	connect_db();
	
	
	$form_id  = $application->getFormId();
	$entry_id = $application->getEntryId();
	
	
	//get form name
	$query = "select form_name from `ap_forms` where form_id='$form_id'";
	$result = do_query($query);
	$row = do_fetch_result($result);
	$form_name = $row['form_name'];
	
	$entry_details = get_details_only($form_id,$entry_id);
	$document_details = get_document_details($form_id,$entry_id);
	$drawing_details = get_drawing_details($form_id,$entry_id);
	
	//get entry timestamp
	$query = "select date_created,date_updated,ip_address from `ap_form_{$form_id}` where id='$entry_id'";
	$result = do_query($query);
	$row = do_fetch_result($result);
	
	$date_created = $row['date_created'];
	if(!empty($row['date_updated'])){
		$date_updated = $row['date_updated'];
	}else{
		$date_updated = '&nbsp;';
	}
	$ip_address   = $row['ip_address'];
	
	//get ids for navigation buttons
	//older entry id
	$result = do_query("select id from ap_form_{$form_id} where id < $entry_id order by id desc limit 1");
	$row = do_fetch_result($result);
	$older_entry_id = $row['id'];
	
	//oldest entry id
	$result = do_query("select id from ap_form_{$form_id} order by id asc limit 1");
	$row = do_fetch_result($result);
	$oldest_entry_id = $row['id'];
	
	//newer entry id
	$result = do_query("select id from ap_form_{$form_id} where id > $entry_id order by id asc limit 1");
	$row = do_fetch_result($result);
	$newer_entry_id = $row['id'];
	
	//newest entry id
	$result = do_query("select id from ap_form_{$form_id} order by id desc limit 1");
	$row = do_fetch_result($result);
	$newest_entry_id = $row['id'];
	
	if(($entry_id == $newest_entry_id) && ($entry_id == $oldest_entry_id)){
		$nav_position = 'disabled';
	}elseif($entry_id == $newest_entry_id){
		$nav_position = 'newest';
	}elseif ($entry_id == $oldest_entry_id){
		$nav_position = 'oldest';
	}else{
		$nav_position = 'middle';
	}
	
	
	function replaceLinks($value) {
		$linktext = preg_replace('/(([a-zA-Z]+:\/\/)([a-zA-Z0-9?&%.;:\/=+_-]*))/i', "<a href=\"$1\" target=\"_blank\">$1</a>", $value);
		return $linktext;
	}
											
?>
    <div style="float: left;">
		<ul class="breadcrumb">
					<li><a href="#">Applications </a></li>
					<li><a href="#">View Application</a></li>
		</ul>
	</div>
	
	<div style="float: right;">
		<?php
			//List tasks by task type here
			$q = Doctrine_Query::create()
				 ->from('Task a')
				 ->where('a.owner_user_id = ?', $_SESSION["SESSION_CUTEFLOW_USERID"])
				 ->andWhere('a.application_id = ?', $application->getId())
				 ->andWhere('a.status = ? OR a.status = ? OR a.status = ? OR a.status = ? OR a.status = ?', array('1','2','3','4','5'));
			$pendingtasks = $q->execute();
			
			$reviewtasks = 0;
			$commenttasks = 0;
			$inspecttasks = 0;
			$invoicetasks = 0;
			$scantasks = 0;
			
			foreach($pendingtasks as $task)
			{
				?>
				<button onClick="var dialog = $.alert('Your Task');
					dialog.setHeader('What would you like to do?');
					dialog.setBody('<div style=\'float:left;\'><button onClick=\'window.location=&quot;<?php echo public_path(); ?>backend.php/tasks/<?php if($task->getType() == "1")
						{
						    $reviewtasks++;
							echo "review";
						}
						if($task->getType() == "2")
						{
						    $commenttasks++;
							echo "comment";
						}
						if($task->getType() == "6")
						{
						    $inspecttasks++;
							echo "inspect";
						}
						if($task->getType() == "3")
						{
						    $invoicetasks++;
							echo "invoice";
						}
						if($task->getType() == "4")
						{
						    $scantasks++;
							echo "scan";
						}
						if($task->getType() == "5")
						{
							echo "stamp";
						}; ?>/id/<?php echo $task->getId(); ?>&quot;;\'>Start <?php
						if($task->getType() == "1")
						{
							echo "Review";
						}
						if($task->getType() == "2")
						{
							echo "Commenting";
						}
						if($task->getType() == "6")
						{
							echo "Inspection";
						}
						if($task->getType() == "3")
						{
							echo "Invoicing";
						}
						if($task->getType() == "4")
						{
							echo "Scanning";
						}
						if($task->getType() == "5")
						{
							echo "Stamping";
						}
				?></button></div><div style=\'float:right;\'><button onClick=\'window.location=&quot;<?php echo public_path(); ?>backend.php/tasks/view/id/<?php echo $task->getId(); ?>&quot;;\'>View Task</button></div>');" class="btn <?php
						if($task->getType() == "1")
						{
							echo "red";
						}
						if($task->getType() == "2")
						{
							echo "green";
						}
						if($task->getType() == "6")
						{
							echo "yellow";
						}
						if($task->getType() == "3")
						{
							echo "blue";
						}
						if($task->getType() == "4")
						{
							echo "pink";
						}
						if($task->getType() == "5")
						{
							echo "purple";
						}
				?>"><?php
						if($task->getType() == "1")
						{
							echo "Review";
						}
						if($task->getType() == "2")
						{
							echo "Commenting";
						}
						if($task->getType() == "6")
						{
							echo "Inspection";
						}
						if($task->getType() == "3")
						{
							echo "Invoicing";
						}
						if($task->getType() == "4")
						{
							echo "Scanning";
						}
						if($task->getType() == "5")
						{
							echo "Stamping";
						}
				?> Task</button>
				<?php
			}
			
		//$reviewtasks 
		//$commenttasks
		//$inspecttasks 
		//$invoicetasks
		//$scantasks

		//Check for allowable tasks for this stage
		
		$q = Doctrine_Query::create()
		   ->from('SubMenuTasks a')
		   ->where('a.sub_menu_id = ?', $application->getApproved());
		$stagetasks = $q->execute();
		
		foreach($stagetasks as $stagetask)
		{
			if($reviewtasks == 0 && $stagetask->getTaskId() == "1")
			{
				?>
				<button OnClick="window.location='/backend.php/tasks/review/id/0/application/<?php echo $application->getId(); ?>';">Start Reviewing</button>
				<?php
			}
			if($commenttasks == 0 && $stagetask->getTaskId() == "2")
			{
				?>
				<button OnClick="window.location='/backend.php/tasks/comment/id/0/application/<?php echo $application->getId(); ?>';">Start Commenting</button>
				<?php
			}
			if($inspecttasks == 0 && $stagetask->getTaskId() == "6")
			{
				?>
				<button OnClick="window.location='/backend.php/tasks/inspect/id/0/application/<?php echo $application->getId(); ?>';">Start Inspecting</button>
				<?php
			}
			if($invoicetasks == 0 && $stagetask->getTaskId() == "3")
			{
				?>
				<button OnClick="window.location='/backend.php/tasks/invoice/id/0/application/<?php echo $application->getId(); ?>';">Start Invoicing</button>
				<?php
			}
			if($scantasks == 0 && $stagetask->getTaskId() == "4")
			{
				?>
				<button OnClick="window.location='/backend.php/tasks/scan/id/0/application/<?php echo $application->getId(); ?>';">Start Scanning</button>
				<?php
			}
		}
			
		?>
		<?php
		if($sf_user->mfHasCredential("editapplication"))
		{
		?>
		<button class="i_pencil icon" onClick="window.location='/backend.php/applications/edit/id/<?php echo $application->getId(); ?>';">Edit Application</button>
		<?php
		}
		?>
		<?php
		if($sf_user->mfHasCredential("assigntask"))
		{
		?>
		<button class="i_calendar_day icon" onClick="window.location='/backend.php/tasks/new/application/<?php echo $application->getId(); ?>';">New Task</button>
		<?php
		}
		?>
		
		
		
	</div>
	
	<div style="clear: both;"></div>
	<div>
		<?php
		
				$q = Doctrine_Query::create()
					 ->from('SubMenuButtons a')
					 ->where('a.sub_menu_id = ?', $application->getApproved());
				$submenubuttons = $q->execute();
				
				foreach($submenubuttons as $submenubutton)
				{
					$q = Doctrine_Query::create()
						 ->from('Buttons a')
						 ->where('a.id = ?', $submenubutton->getButtonId());
					$buttons = $q->execute();
					foreach($buttons as $button)
					{
						if($sf_user->mfHasCredential("accessbutton".$button->getId()))
						{
						
							if($button->getLink() == "/backend.php/circulations/generatepermit?language=en&permit=1&")
							{
									?>
						
									<button onClick="window.location='<?php echo $button->getLink(); ?>&entryid=<?php echo $application->getId(); ?>&form_id=<?php echo $form_id; ?>&id=<?php echo $entry_id; ?>'"><?php echo $button->getTitle(); ?></button>
									
									<?php
									
								if($application->getApproved() != "850")
								{
									?>
						
									<button onClick="window.location='/backend.php/applications/editpermit/id/<?php echo $application->getId(); ?>';">Edit &amp; Save Permit</button>
									
									<?php
								}
							}
							else
							{
						?>
						
						<button onClick="window.location='<?php echo $button->getLink(); ?>&entryid=<?php echo $application->getId(); ?>&form_id=<?php echo $form_id; ?>&id=<?php echo $entry_id; ?>'"><?php echo $button->getTitle(); ?></button>
						
						<?php
							}
						}
					}
				}
		?>
		<?php
		if($sf_user->mfHasCredential("sendnotification"))
		{
		?>
		<button class="i_mail icon" onClick="window.location='/backend.php/applications/sendnotification/application/<?php echo $application->getId(); ?>';">Send Notification</button>
		<?php
		}
		?>
	</div>

<div class="g12">
			<h1><?php echo $application->getApplicationId(); ?></h1>
			<p><b style='font-weight: 900;'>Status: </b><?php
				$q = Doctrine_Query::create()
					 ->from('SubMenus a')
					 ->where('a.id = ?', $application->getApproved());
				$submenu = $q->fetchOne();
				
				echo $submenu->getTitle();
				
			?>
			<br>
			<?php
			$q = Doctrine_Query::create()
					 ->from('EntryDecline a')
					 ->where('a.entry_id = ?', $application->getId());
			$decline = $q->fetchOne();
			if($decline)
			{
			 echo "<h3>Previous Reasons for Decline: </h3> ".$decline->getDescription();
			}
			?>
			</p>
			<br>
			
			<?php
				$q = Doctrine_Query::create()
				   ->from('Notifications a')
				   ->where('a.submenu_id = ?', $application->getApproved());
				$notification = $q->fetchOne();
			?>
			
			<form action='/backend.php/applications/sendnotification/application/<?php echo $application->getId(); ?>' method='post' >
			 <fieldset>
			 <label>Send Notification</label>
			 <section>
			 <label>Subject</label>
			 <div>
				<input type='text' name='subject' id='subject' value='<?php echo $notification->getTitle(); ?>'>
			 </div>
			 </section>
			 <section>
			 <label>Email</label>
			 <div>
			  <textarea name='email' id='email'><?php echo $notification->getContent(); ?></textarea>
			 </div>
			 </section>
			 <section>
			 <label>SMS</label>
			 <div>
			  <textarea name='sms' id='sms'><?php echo $notification->getSms(); ?></textarea>
			 </div>
			 </section>
			 <section>
					<div><button class="reset">Reset</button><button class="submit" name="submitbuttonname" value="submitbuttonvalue">Submit</button></div>
			 </section>
			 </fieldset>
			</form>
			
</div>
