<?php
  use_helper('I18N');
?>
<div class="pageheader">
  <h2><i class="fa fa-home"></i> <?php echo __('Payment'); ?> <span><?php echo __('Pay for your application'); ?></span></h2>
  <div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
      <li><a href="#"><?php echo __('Home'); ?></a></li>
      <li class="active"><?php echo __('Payment'); ?></li>
    </ol>
  </div>
</div>

<div class="contentpanel">
    <div class="row">
        <div class="col-md-12" style="height: 100%">
        <?php
          $form_id    = $sf_user->getAttribute('form_id');
          $record_id   = $sf_user->getAttribute('entry_id');

          $application_manager = new ApplicationManager();
          $invoice_manager = new InvoiceManager();
          $payments_manager = new PaymentsManager();

          if(empty($record_id) && $_SESSION['mf_payment_record_id'][$form_id])
          {
              $record_id = $_SESSION['mf_payment_record_id'][$form_id];
          }

          //Check if an application already exists for the form submission to prevent double entry
          if ($application_manager->application_exists($form_id, $record_id)) {
              //If save as draft/resume later was clicked then do nothing
              $submission = $application_manager->get_application($form_id, $record_id);
          } else {
              //If save as draft/resume later was clicked then create draft application
              $submission = $application_manager->create_application($form_id, $record_id, $_SESSION['create_user'], true);
          }

          $application_manager->update_invoices($submission->getId());

          if ($invoice_manager->can_make_partial_payment($submission->getId()) && $_SESSION['partial_amount'] == false) {
              //Display a form the user can use to select amount they want to pay
              $invoice = "";
              if ($sf_user->getAttribute('invoice_id')) {
                  $invoice = $invoice_manager->get_invoice_by_id($sf_user->getAttribute('invoice_id'));
              } else {
                  $invoice = $invoice_manager->get_invoice_by_reference($submission->getFormId() . "/" . $submission->getEntryId() . "/1");

                  if($invoice)
                  {
                      $sf_user->setAttribute('invoice_id', $invoice->getId());
                  }
              }
              $total_amount_remaining = $invoice_manager->get_invoice_total_owed($invoice->getId());
              $currency = $invoice->getCurrency();
              include_partial('chooseamount', array('total_amount_remaining' => $total_amount_remaining, 'currency' => $currency));
          } else {

              $invoice = $invoice_manager->get_invoice_by_id($sf_user->getAttribute('invoice_id'));

              if($invoice)
              {
                  $invoice = $invoice_manager->get_invoice_by_id($sf_user->getAttribute('invoice_id'));

                  if($invoice->getPaid() == 2)
                  {
                      header("Location: /backend.php/invoices/view/id/".$invoice->getId());
                      exit;
                  }
                  else
                  {
                    echo $payments_manager->display_checkout($invoice->getId(), true);
                  }
              }
              else
              {
                  $invoice = $invoice_manager->get_invoice_by_reference($form_id . "/" . $record_id . "/1");

                  if($invoice)
                  {
                      $sf_user->setAttribute('invoice_id', $invoice->getId());

                      echo $payments_manager->display_checkout($invoice, true);
                  }
                  else
                  {
                      echo "<h3>No Invoice</h3>";
                  }
              }
          }
        ?>
        </div>
    </div>
</div>
