<?php
use_helper("I18N");
?>

<div class="pageheader">
  <h2><i class="fa fa-envelope"></i> <?php echo __('Search'); ?></h2>
  <div class="breadcrumb-wrapper">
    <span class="label"><?php echo __('You are here'); ?>:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>backend.php"><?php echo __('Home'); ?></a></li>
      <li><a href="<?php echo public_path(); ?>backend.php/applications/search"><?php echo __('Search'); ?></a></li>
    </ol>
  </div>
</div>

<div class="contentpanel">
	<div class="row">
		
<div id='search_adv' name='search_adv' class="g12" style='padding-top: 4px;<?php 
	if($_POST['search_posted'] == '1'){ echo "display: none;"; } ?>'>
	<form autocomplete="off" data-ajax="false">
	<fieldset>
	<input type='hidden' name='search_posted' id='search_posted' value='1'>
	</fieldset>
	</form>
	<div class="panel-group" id="accordion">
	<?php 
	if($_POST['application_form'])
	{
	
	}
	else
	{
	?>
		<div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
					  <?php echo __('Quick Search'); ?>
				  </a>
                </h4>
              </div>
              <div id="collapse3" class="panel-collapse collapse <?php if(empty($_GET['search'])){ ?>in<?php } ?>">
              <div class="panel-body panel-body-nopadding">
							<form class="form-bordered" method='post' action="<?php echo public_path(); ?>backend.php/applications/search" autocomplete="off" data-ajax="false">
							<div class="panel-body panel-body-nopadding" style="border-top:none;">
							 <div class="form-group">
							<label class="col-sm-2"><?php echo __('Application No'); ?></label>
							<div class="col-sm-8">
							<input class="form-control" type='text' name='applicationid' id='applicationid' style="width:80%;" value="<?php echo $_POST['applicationid']; ?>">
							</div>
							</div>
							</div>
							<div class="panel-footer">
							 <button class="btn btn-primary" type="submit" name="submitbuttonname" value="submitbuttonvalue"><?php echo __('Search'); ?></button>
							</div>
							
							</form>
							
							<?php
								if($_POST['applicationid'])
								{
									?>
									<div style="border-top:2px solid #d2d2d2;">
									<h3 class="subtitle mt20 ml10"><?php echo __('Search Results'); ?>:</h3>
									<div class="table-responsive">
									<table class="table dt-on-steroids mb0" id="table3">
									<thead>
										<tr>
										<th width="215px;"><?php echo __('Type'); ?></th><th width="86px"><?php echo __('No'); ?></th><th width="135px"><?php echo __('Submitted On'); ?></th><th width="106px"><?php echo __('Submitted By'); ?></th><th width="86px"><?php echo __('Status'); ?></th><th width="38px"><?php echo __('Actions'); ?></th>
										</tr>
									</thead>
									<?php
									    $q = Doctrine_Query::create()
										   ->from('FormEntry a')
										   ->where('a.application_id LIKE ?', '%'.$_POST['applicationid'].'%')
										   ->andWhere('a.parent_submission = 0');
										$applications = $q->execute();
										foreach($applications as $application)
										{
											$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
											mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
											$query = "SELECT * FROM ap_form_".$application->getFormId()." WHERE id = '".$application->getEntryId()."'";
											$result = mysql_query($query,$dbconn);

											$application_form = mysql_fetch_assoc($result);
										?>
										<tr id="row_<?php echo $application->getId() ?>">
										
										<td width="215px"><?php 
										$q = Doctrine_Query::create()
											 ->from('ApForms a')
											 ->where('a.form_id = ?', $application->getFormId());
										$form = $q->fetchOne();
										
										?></td>
										<td width="86px"><?php echo $application->getApplicationId(); ?></td>
										<td width="135px"><?php
											echo $application_form['date_created'];
										?></td>
										<td class="c" width="106px">
										<?php
											$q = Doctrine_Query::create()
											 ->from('sfGuardUserProfile a')
											 ->where('a.user_id = ?', $application->getUserId());
										$userprofile = $q->fetchOne();
										if($userprofile)
										{
											echo $userprofile->getFullname();
										}
										else
										{
											echo "-";
										}
										?>
										</td>
										<td class="c" width="180px">
										<?php
											 $q = Doctrine_Query::create()
												->from('SubMenus a')
												->where('a.id = ?', $application->getApproved());
											$submenu = $q->fetchOne();
											
											if($submenu)
											{
												echo $submenu->getTitle();
											}
											else
											{
												echo "-";
											}
										?>
										</td>
										<td class="c" width="38px">
										<a title='<?php echo __('View Application'); ?>' href='<?php echo public_path(); ?>backend.php/applications/view/id/<?php echo $application->getId(); ?>'><span class="badge badge-primary"><i class="fa fa-eye"></i></span></a>
										</td>
									</tr>
								<?php
										}
								?>
								</table>
								</div>
								</div>
									<?php
								}
							?>
							
			</div>
				<?php
				}
				?>
					</div>
        </div>

		<div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
					  <?php echo __('Advanced Search'); ?>
				  </a>
                </h4>
              </div>
              <div id="collapse4" class="panel-collapse collapsed  <?php if($_GET['search']){ ?>in<?php } ?>">
              <div class="panel-body">
							<form class="form-bordered"  method='post' action="<?php echo public_path(); ?>backend.php/applications/search" autocomplete="off" data-ajax="false">
						
							<label><?php echo __('Type Of Application'); ?></label>
							<br>
							
							<select id='application_form' name='application_form' onChange="ajaxSearchform1(this.value);">
										<option value="0"><?php echo __('Select form'); ?>....</option>
										<?php
											//Get list of all available applications categorized by groups
											$q = Doctrine_Query::create()
											  ->from('FormGroups a');
											$groups = $q->execute();

											if(sizeof($groups) > 0)
											{
												foreach($groups as $group)
												{
													echo "<optgroup label='".$group->getGroupName()."'>";

													$q = Doctrine_Query::create()
													  ->from('ApForms a')
													  ->where('a.form_group = ?', $group->getGroupId())
														->andWhere('a.form_type = ?', 1)
														->andWhere('a.form_active = ?', 1);
													$forms = $q->execute();

													$count = 0;

													foreach($forms as $form)
													{
														$selected = "";

														if($application_form != "" && $application_form == $form->getFormId())
														{
															$selected = "selected='selected'";
															$_GET['form'] = $form;
														}

														echo "<option value='".$form->getFormId()."' ".$selected.">".$form->getFormName()."</option>";

														$count++;
													}
												echo "</optgroup>";
												}
											}
											else
											{
													$q = Doctrine_Query::create()
													  ->from('ApForms a')
													  ->where('a.form_type = ?', '1')
														->andWhere('a.form_active = ?', 1)
													  ->orderBy('a.form_id ASC');
													$forms = $q->execute();

													$count = 0;

													echo "<optgroup label='Application Forms'>";

													foreach($forms as $form)
													{

														$selected = "";

														if($application_form != "" && $application_form == $form->getFormId())
														{
															$selected = "selected='selected'";
														}


														echo "<option value='".$form->getFormId()."' ".$selected.">".$form->getFormName()."</option>";

														$count++;
													}

													echo "</optgroup>";
											}
											?>
									</select>
							
							
						    
						
						    
							<div id='ajaxsearchform' name='ajaxsearchform'>
							
							
						
					</div></div>
									<div class="panel-footer"><button class="reset btn btn-danger mr10"><?php echo __('Reset'); ?></button><button class="submit btn btn-primary" name="submitbuttonname" value="submitbuttonvalue"><?php echo __('Search'); ?></button>
								
						
							
							</form>
							
					
				
							
							<?php 
	if($_POST['application_form'])
	{
	?>
			<?php
			$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
			mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
			$query = "SELECT * FROM ap_form_".$_POST['application_form']." WHERE ";
			
			 $q = Doctrine_Query::create()
			  ->from('ApFormElements a')
			  ->where('a.form_id = ?', $_POST['application_form'])
			  ->andWhere('a.element_type <> ? AND a.element_type <> ? AND a.element_type <> ?', array('section','file','section'))
			  ->andWhere('a.element_status = ?', 1)
			  ->orderBy('a.element_position ASC');
			$fields = $q->execute();
			$count = 0;
			foreach($fields as $field)
			{
				if($_POST['element_'.$field->getElementId()] != "")
				{
					if($count > 0)
					{
					     $query = $query." OR ";
					}
					
					if($field->getElementType() == "simple_name")
					{
						$query = $query."CONCAT(element_".$field->getElementId()."_1, ' ', element_".$field->getElementId()."_2) LIKE '%".$_POST['element_'.$field->getElementId()]."%'";
					}
					else
					{
				    	$query = $query."element_".$field->getElementId()." LIKE '%".$_POST['element_'.$field->getElementId()]."%'";
				    }

				    $count++;

				}
			}
			
			$results = mysql_query($query,$dbconn);
			
			echo "&nbsp; &nbsp; <br>";
			?>
			<div class="panel-body">
			<div class="table-responsive">
			<table class="table dt-on-steroids mb0">
				<thead>
					<tr><th><?php echo __('Type'); ?></th><th><?php echo __('No'); ?></th><th><?php echo __('Submitted On'); ?></th><th><?php echo __('Submitted By'); ?></th><th><?php echo __('Status'); ?></th><th style="background: none;"><?php echo __('Actions'); ?></th>
					</tr>
				</thead>
				<tbody>
			<?php
			while($row = mysql_fetch_assoc($results))
			{
				$q = Doctrine_Query::create()
				  ->from('FormEntry a')
				  ->where('a.form_id = ?', $_POST['application_form'])
				  ->andWhere('a.entry_id = ?', $row['id']);
				$application = $q->fetchOne();
                                
                if(empty($application))
				{
					continue;	
				}
				?>
				<tr id="row_<?php echo $application->getId() ?>">
						  
						  
						<td><?php 
						$q = Doctrine_Query::create()
						     ->from('ApForms a')
							 ->where('a.form_id = ?', $application->getFormId());
					    $form = $q->fetchOne();
						if($form)
						{
							echo $form->getFormName();
						}
						else
						{
							echo "-";
						}
						?></td>
						<td><?php echo $application->getApplicationId(); ?></td>
						<td><?php
							echo $row['date_created'];
						?></td>
						<td class="c">
						<?php
							$q = Doctrine_Query::create()
						     ->from('sfGuardUserProfile a')
							 ->where('a.user_id = ?', $application->getUserId());
					    $userprofile = $q->fetchOne();
						if($userprofile)
						{
							echo $userprofile->getFullname();
						}
						else
						{
							echo "-";
						}
						?>
						</td>
						<td class="c">
						<?php
							 $q = Doctrine_Query::create()
								->from('SubMenus a')
								->where('a.id = ?', $application->getApproved());
							$submenu = $q->fetchOne();
							
							if($submenu)
							{
								echo $submenu->getTitle();
							}
							else
							{
								echo "-";
							}
						?>
						</td>
						<td class="c">
						<a title='<?php echo __('View Application'); ?>' href='<?php echo public_path(); ?>backend.php/applications/view/id/<?php echo $application->getId(); ?>'><span class="badge badge-primary"><i class="fa fa-eye"></i></span></a>
						</td>
					</tr>
				<?php
			}
			?>
			</tbody>
			</table>
			</div>
			</div>
	<?php
	}
	?>
							
							
					</div>
				</div>
	
	
</div>

	</div>
</div>


	
