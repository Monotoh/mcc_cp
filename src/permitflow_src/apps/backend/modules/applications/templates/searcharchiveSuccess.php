<?php
/**
 * list template.
 *
 * Displays a list of applications ordered by most recent first
 *
 * @package    backend
 * @subpackage applications
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");

include_partial('dashboard/checksession');

function GetDaysSince($sStartDate, $sEndDate){
	$start_ts = strtotime($sStartDate);
	$end_ts = strtotime($sEndDate);
	$diff = $end_ts - $start_ts;
	return round($diff / 86400);
}

if($sf_user->mfHasCredential("access_applications"))
{
?>
<div class="pageheader">
  <h2><i class="fa fa-envelope"></i><?php echo __("Archives"); ?></h2>
  <div class="breadcrumb-wrapper">
    <span class="label"><?php echo __("You are here"); ?>:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>backend.php"><?php echo __("Home"); ?></a></li>
      <li><a href="<?php echo public_path(); ?>backend.php/applications/list/get/your"><?php echo __("Applications"); ?></a></li>
      <li class="active">Archives</li>
    </ol>
  </div>
</div>

<div class="contentpanel panel-email">

    <div class="row">
        <?php include_partial('listsidebar', array('form_id'=> $application_form,'stage' => $stage,'has_logic' => $has_logic)) ?>

        <div class="col-lg-10">
           <div class="panel panel-dark">
           <div class="panel-heading">
            <h3 class="panel-title"><?php echo __("Archives"); ?></h3>
            <p class="text-muted">Search for an application in the archives<br></p>
       </div>
       <div class="panel-body panel-body-nopadding">
		   		<form class="form-bordered" method='post' autocomplete="off" data-ajax="false">
							<div class="panel-body panel-body-nopadding" style="border-top:none;">
							 <div class="form-group">
							<label class="col-sm-2"><h4><?php echo __('Application No'); ?></h4></label>
							<div class="col-sm-8">
							<input class="form-control" type='text' name='applicationid' id='applicationid' style="width:80%;" value="<?php echo $_POST['applicationid']; ?>">
							</div>
							</div>
							</div>
							<div class="panel-footer">
							 <button class="btn btn-primary" type="submit" name="submitbuttonname" value="submitbuttonvalue"><?php echo __('Search'); ?></button>
							</div>
          </form>
          
          
          <table class="table dt-on-steroids mb0">
	        <thead>
	        <th  class="no-sort"></th>
	        <th  class="no-sort"></th>
	        <th><?php echo __("Application No"); ?></th>
	        <th><?php echo __("Submission Date"); ?></th>
	        <th>Submitted By</th>
	        <th class="no-sort" width="10%"><?php echo __("Actions"); ?></th>
	        </thead>
	              <tbody>
	              	<?php foreach ($pager->getResults() as $application): ?>
	              	<?php
                    $days =  GetDaysSince($application->getDateOfSubmission(), date("Y-m-d H:i:s"));
                  ?>
	              	<?php include_partial('listarchive', array('application' => $application, 'days' => $days)) ?>
	              <?php endforeach; ?>
	              </tbody>
			  </table>
          
	    </div>
            </div><!-- panel -->

        </div><!-- col-sm-9 -->

    </div><!-- row -->
	</div>
	<?php
}
else
{
	include_partial("settings/accessdenied");
}
?>
