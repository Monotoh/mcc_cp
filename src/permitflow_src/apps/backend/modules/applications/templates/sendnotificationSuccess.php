
<?php
    $prefix_folder = dirname(__FILE__)."/../../../../..";

	require($prefix_folder.'/lib/vendor/cp_form/config.php');
	require($prefix_folder.'/lib/vendor/cp_form/includes/check-session.php');
	require($prefix_folder.'/lib/vendor/cp_form/includes/db-core.php');
	require($prefix_folder.'/lib/vendor/cp_form/includes/db-functions.php');
	require($prefix_folder.'/lib/vendor/cp_form/includes/helper-functions.php');
	require($prefix_folder.'/lib/vendor/cp_form/includes/entry-functions.php');

    require_once $prefix_folder.'/lib/vendor/cp_workflow/config/config.inc.php';
	require_once $prefix_folder.'/lib/vendor/cp_workflow/language_files/language.inc.php';
	require_once $prefix_folder.'/lib/vendor/cp_workflow/pages/CCirculation.inc.php';
	require_once $prefix_folder.'/lib/vendor/cp_workflow/pages/version.inc.php';
	

	connect_db();
	
	
	$form_id  = $application->getFormId();
	$entry_id = $application->getEntryId();
	
	
	//get form name
	$query = "select form_name from `ap_forms` where form_id='$form_id'";
	$result = do_query($query);
	$row = do_fetch_result($result);
	$form_name = $row['form_name'];
	
	$entry_details = get_details_only($form_id,$entry_id);
	$document_details = get_document_details($form_id,$entry_id);
	$drawing_details = get_drawing_details($form_id,$entry_id);
	
	//get entry timestamp
	$query = "select date_created,date_updated,ip_address from `ap_form_{$form_id}` where id='$entry_id'";
	$result = do_query($query);
	$row = do_fetch_result($result);
	
	$date_created = $row['date_created'];
	if(!empty($row['date_updated'])){
		$date_updated = $row['date_updated'];
	}else{
		$date_updated = '&nbsp;';
	}
	$ip_address   = $row['ip_address'];
	
	//get ids for navigation buttons
	//older entry id
	$result = do_query("select id from ap_form_{$form_id} where id < $entry_id order by id desc limit 1");
	$row = do_fetch_result($result);
	$older_entry_id = $row['id'];
	
	//oldest entry id
	$result = do_query("select id from ap_form_{$form_id} order by id asc limit 1");
	$row = do_fetch_result($result);
	$oldest_entry_id = $row['id'];
	
	//newer entry id
	$result = do_query("select id from ap_form_{$form_id} where id > $entry_id order by id asc limit 1");
	$row = do_fetch_result($result);
	$newer_entry_id = $row['id'];
	
	//newest entry id
	$result = do_query("select id from ap_form_{$form_id} order by id desc limit 1");
	$row = do_fetch_result($result);
	$newest_entry_id = $row['id'];
	
	if(($entry_id == $newest_entry_id) && ($entry_id == $oldest_entry_id)){
		$nav_position = 'disabled';
	}elseif($entry_id == $newest_entry_id){
		$nav_position = 'newest';
	}elseif ($entry_id == $oldest_entry_id){
		$nav_position = 'oldest';
	}else{
		$nav_position = 'middle';
	}
	
	
	function replaceLinks($value) {
		$linktext = preg_replace('/(([a-zA-Z]+:\/\/)([a-zA-Z0-9?&%.;:\/=+_-]*))/i', "<a href=\"$1\" target=\"_blank\">$1</a>", $value);
		return $linktext;
	}
											
?>
    <div style="float: left;">
		<ul class="breadcrumb">
					<li><a href="#">Applications </a></li>
					<li><a href="#">View Application</a></li>
		</ul>
	</div>
	
	<div style="float: right;">
		<?php
			//List tasks by task type here
			$q = Doctrine_Query::create()
				 ->from('Task a')
				 ->where('a.owner_user_id = ?', $_SESSION["SESSION_CUTEFLOW_USERID"])
				 ->andWhere('a.application_id = ?', $application->getId())
				 ->andWhere('a.status = ? OR a.status = ? OR a.status = ? OR a.status = ? OR a.status = ?', array('1','2','3','4','5'));
			$pendingtasks = $q->execute();
			
			$reviewtasks = 0;
			$commenttasks = 0;
			$inspecttasks = 0;
			$invoicetasks = 0;
			$scantasks = 0;
			
			foreach($pendingtasks as $task)
			{
				?>
				<button onClick="var dialog = $.alert('Your Task');
					dialog.setHeader('What would you like to do?');
					dialog.setBody('<div style=\'float:left;\'><button onClick=\'window.location=&quot;<?php echo public_path(); ?>backend.php/tasks/<?php if($task->getType() == "1")
						{
						    $reviewtasks++;
							echo "review";
						}
						if($task->getType() == "2")
						{
						    $commenttasks++;
							echo "comment";
						}
						if($task->getType() == "6")
						{
						    $inspecttasks++;
							echo "inspect";
						}
						if($task->getType() == "3")
						{
						    $invoicetasks++;
							echo "invoice";
						}
						if($task->getType() == "4")
						{
						    $scantasks++;
							echo "scan";
						}
						if($task->getType() == "5")
						{
							echo "stamp";
						}; ?>/id/<?php echo $task->getId(); ?>&quot;;\'>Start <?php
						if($task->getType() == "1")
						{
							echo "Review";
						}
						if($task->getType() == "2")
						{
							echo "Commenting";
						}
						if($task->getType() == "6")
						{
							echo "Inspection";
						}
						if($task->getType() == "3")
						{
							echo "Invoicing";
						}
						if($task->getType() == "4")
						{
							echo "Scanning";
						}
						if($task->getType() == "5")
						{
							echo "Stamping";
						}
				?></button></div><div style=\'float:right;\'><button onClick=\'window.location=&quot;<?php echo public_path(); ?>backend.php/tasks/view/id/<?php echo $task->getId(); ?>&quot;;\'>View Task</button></div>');" class="btn <?php
						if($task->getType() == "1")
						{
							echo "red";
						}
						if($task->getType() == "2")
						{
							echo "green";
						}
						if($task->getType() == "6")
						{
							echo "yellow";
						}
						if($task->getType() == "3")
						{
							echo "blue";
						}
						if($task->getType() == "4")
						{
							echo "pink";
						}
						if($task->getType() == "5")
						{
							echo "purple";
						}
				?>"><?php
						if($task->getType() == "1")
						{
							echo "Review";
						}
						if($task->getType() == "2")
						{
							echo "Commenting";
						}
						if($task->getType() == "6")
						{
							echo "Inspection";
						}
						if($task->getType() == "3")
						{
							echo "Invoicing";
						}
						if($task->getType() == "4")
						{
							echo "Scanning";
						}
						if($task->getType() == "5")
						{
							echo "Stamping";
						}
				?> Task</button>
				<?php
			}
			
		//$reviewtasks 
		//$commenttasks
		//$inspecttasks 
		//$invoicetasks
		//$scantasks

		//Check for allowable tasks for this stage
		
		$q = Doctrine_Query::create()
		   ->from('SubMenuTasks a')
		   ->where('a.sub_menu_id = ?', $application->getApproved());
		$stagetasks = $q->execute();
		
		foreach($stagetasks as $stagetask)
		{
			if($reviewtasks == 0 && $stagetask->getTaskId() == "1")
			{
				?>
				<button OnClick="window.location='/backend.php/tasks/review/id/0/application/<?php echo $application->getId(); ?>';">Start Reviewing</button>
				<?php
			}
			if($commenttasks == 0 && $stagetask->getTaskId() == "2")
			{
				?>
				<button OnClick="window.location='/backend.php/tasks/comment/id/0/application/<?php echo $application->getId(); ?>';">Start Commenting</button>
				<?php
			}
			if($inspecttasks == 0 && $stagetask->getTaskId() == "6")
			{
				?>
				<button OnClick="window.location='/backend.php/tasks/inspect/id/0/application/<?php echo $application->getId(); ?>';">Start Inspecting</button>
				<?php
			}
			if($invoicetasks == 0 && $stagetask->getTaskId() == "3")
			{
				?>
				<button OnClick="window.location='/backend.php/tasks/invoice/id/0/application/<?php echo $application->getId(); ?>';">Start Invoicing</button>
				<?php
			}
			if($scantasks == 0 && $stagetask->getTaskId() == "4")
			{
				?>
				<button OnClick="window.location='/backend.php/tasks/scan/id/0/application/<?php echo $application->getId(); ?>';">Start Scanning</button>
				<?php
			}
		}
			
		?>
		<?php
		if($sf_user->mfHasCredential("editapplication"))
		{
		?>
		<button class="i_pencil icon" onClick="window.location='/backend.php/applications/edit/id/<?php echo $application->getId(); ?>';">Edit Application</button>
		<?php
		}
		?>
		<?php
		if($sf_user->mfHasCredential("assigntask"))
		{
		?>
		<button class="i_calendar_day icon" onClick="window.location='/backend.php/tasks/new/application/<?php echo $application->getId(); ?>';">New Task</button>
		<?php
		}
		?>
		
		
		
	</div>
	
	<div style="clear: both;"></div>
	<div>
		<?php
		
				$q = Doctrine_Query::create()
					 ->from('SubMenuButtons a')
					 ->where('a.sub_menu_id = ?', $application->getApproved());
				$submenubuttons = $q->execute();
				
				foreach($submenubuttons as $submenubutton)
				{
					$q = Doctrine_Query::create()
						 ->from('Buttons a')
						 ->where('a.id = ?', $submenubutton->getButtonId());
					$buttons = $q->execute();
					foreach($buttons as $button)
					{
						if($sf_user->mfHasCredential("accessbutton".$button->getId()))
						{
						
							if($button->getLink() == "/backend.php/circulations/generatepermit?language=en&permit=1&")
							{
									?>
						
									<button onClick="window.location='<?php echo $button->getLink(); ?>&entryid=<?php echo $application->getId(); ?>&form_id=<?php echo $form_id; ?>&id=<?php echo $entry_id; ?>'"><?php echo $button->getTitle(); ?></button>
									
									<?php
									
								if($application->getApproved() != "850")
								{
									?>
						
									<button onClick="window.location='/backend.php/applications/editpermit/id/<?php echo $application->getId(); ?>';">Edit &amp; Save Permit</button>
									
									<?php
								}
							}
							else
							{
						?>
						
						<button onClick="window.location='<?php echo $button->getLink(); ?>&entryid=<?php echo $application->getId(); ?>&form_id=<?php echo $form_id; ?>&id=<?php echo $entry_id; ?>'"><?php echo $button->getTitle(); ?></button>
						
						<?php
							}
						}
					}
				}
		?>
		<?php
		if($sf_user->mfHasCredential("sendnotification"))
		{
		?>
		<button class="i_mail icon" onClick="window.location='/backend.php/applications/sendnotification/application/<?php echo $application->getId(); ?>';">Send Notification</button>
		<?php
		}
		?>
	</div>

<div class="g12">
			<h1><?php echo $application->getApplicationId(); ?></h1>
			<p><b style='font-weight: 900;'>Status: </b><?php
				$q = Doctrine_Query::create()
					 ->from('SubMenus a')
					 ->where('a.id = ?', $application->getApproved());
				$submenu = $q->fetchOne();
				
				echo $submenu->getTitle();
				
			?>
			<br>
			<?php
			$q = Doctrine_Query::create()
					 ->from('EntryDecline a')
					 ->where('a.entry_id = ?', $application->getId());
			$decline = $q->fetchOne();
			if($decline)
			{
			 echo "<h3>Previous Reasons for Decline: </h3> ".$decline->getDescription();
			}
			?>
			</p>
			<br>
			
			<div class="tab">
					<ul>
						<li><a href="#tabs-8">Send Notification</a></li>
						<li><a href="#tabs-1">Application Details</a></li>
						<li><a href="#tabs-2">Attachments</a></li>
						<li><a href="#tabs-3">Submitted By</a></li>
						<li><a href="#tabs-4">Reviewers</a></li>
						<li><a href="#tabs-5">Comments</a></li>
					</ul>
					<div id="tabs-8">
					<?php
						$q = Doctrine_Query::create()
						   ->from('Notifications a')
						   ->where('a.submenu_id = ?', $application->getApproved());
						$notification = $q->fetchOne();
					?>
					
					<?php 
						$q = Doctrine_Query::create()
							 ->from('SfGuardUserProfile a')
							 ->where('a.user_id = ?', $application->getUserId());
						$user_profile = $q->fetchOne(); 
					?>  
					<form action='/backend.php/applications/notificationmail/application/<?php echo $application->getId(); ?>' method='post' >
					 <fieldset>
					 <label>Send Notification</label>
					 <section>
					 <label>Email</label>
					 <div>
					  <input type='text' name='email' id='email' value='<?php echo $user_profile->getEmail(); ?>'>
					 </div>
					 </section>
					 <section>
					 <label>Subject</label>
					 <div>
						<input type='text' name='subject' id='subject' value='<?php echo $notification->getTitle(); ?>'>
					 </div>
					 </section>
					 <section>
					 <label>Mail</label>
					 <div>
					  <textarea name='mail' id='mail'><?php echo $notification->getContent(); ?></textarea>
					 </div>
					 </section>
					 <section>
					 <label>Phone No</label>
					 <div>
					  <input type='text' name='phone' id='phone' value=''>
					 </div>
					 </section>
					 <section>
					 <label>SMS</label>
					 <div>
					  <textarea name='sms' id='sms'><?php echo $notification->getSms(); ?></textarea>
					 </div>
					 </section>
					 <section>
							<div><button class="reset">Reset</button><button class="submit" name="submitbuttonname" value="submitbuttonvalue">Submit</button></div>
					 </section>
					 </fieldset>
					</form>
					</div>
					<div id="tabs-1">
							<form id="form" action="#" method="post" autocomplete="off" data-ajax="false">
								<fieldset>
						    <?php
							$q = Doctrine_Query::create()
								 ->from('ApForms a')
								 ->where('a.form_id = ?', $application->getFormId());
							$form = $q->fetchOne();
							$formtype = $form->getFormName();
							?>
									<label><?php echo $formtype ?> Details</label>
							<?php 
									$toggle = false;
									
									foreach ($entry_details as $data){ 
							?>  
								<section>
									<label for="text_field" style="font-weight: 900;"><?php echo $data['label']; ?></label>
									<div><?php if($data['value']){ echo nl2br($data['value']); }else{ echo "-"; } ?></div>
								</section>
							<?php } ?>  
								</fieldset>
							</form>
					</div>
					<div id="tabs-2">
							<form id="form" action="#" method="post" autocomplete="off" data-ajax="false">
								<fieldset>
									<label>Attached Documents</label>
							<?php 
									$toggle = false;
									
									foreach ($document_details as $data){ 
							?>  
								<section>
									<label for="text_field" style="font-weight: 900;"><?php echo $data['label']; ?></label>
									<div><?php if($data['value']){ echo nl2br($data['value']); }else{ echo "-"; } ?></div>
								</section>
							<?php } ?>  
								</fieldset>
							</form>
							
							<form id="form" action="#" method="post" autocomplete="off" data-ajax="false">
								<fieldset>
									<label>Attached Drawings</label>
							<?php 
									$toggle = false;
									
									foreach ($drawing_details as $data){ 
							?>  
								<section>
									<label for="text_field" style="font-weight: 900;"><?php echo $data['label']; ?></label>
									<div><?php if($data['value']){ echo nl2br($data['value']); }else{ echo "-"; } ?></div>
								</section>
							<?php } ?>  
								</fieldset>
							</form>
					</div>
					<div id="tabs-3">
						<form id="form" action="#" method="post" autocomplete="off" data-ajax="false">
								<fieldset>
									<label>Basic Details</label>
								<section>
									<label for="text_field" style="font-weight: 900;">Name</label>
									<div><?php echo $user_profile->getFullname(); ?></div>
								</section>
								<section>
									<label for="text_field" style="font-weight: 900;">Email</label>
									<div><?php echo $user_profile->getEmail(); ?></div>
								</section>
								</fieldset>
							</form>
							<form id="form" action="#" method="post" autocomplete="off" data-ajax="false">
								<fieldset>
									<label>Additional Details</label>
							<?php 
									$q = Doctrine_Query::create()
										  ->from('mfUserProfile a')
										  ->where('a.user_id = ?', $application->getUserId());
									$profile = $q->fetchOne();
									
									if($profile)
									{
									
									$form_id = $profile->getFormId();
									$entry_id = $profile->getEntryId();
									
									//get form name
									$query = "select form_name from `ap_forms` where form_id='$form_id'";
									$result = do_query($query);
									$row = do_fetch_result($result);
									$form_name = $row['form_name'];
									
									$architect_details = get_entry_details($form_id,$entry_id);
									
									foreach ($architect_details as $data){ 
							?>  
								<section>
									<label for="text_field" style="font-weight: 900;"><?php echo $data['label']; ?></label>
									<div><?php if($data['value']){ echo nl2br($data['value']); }else{ echo "-"; } ?></div>
								</section>
							<?php } 
							
							}?>  
								</fieldset>
							</form>
					</div>
					<div id="tabs-4">
						<div class="accordion">
						<table>
								<thead>
									<tr>
										<th>Reviewer</th>
										<th>Task</th>
										<th>Start Date</th>
										<th>End Date</th>
										<th>Duration</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
						<?php
						$q = Doctrine_Query::create()
						   ->from('Department a');
						$departments = $q->execute();
						$count_deps = 0;
						foreach($departments as $department)
						{
							//Check if this department has any tasks
								$q = Doctrine_Query::create()
								   ->from('CfUser a')
								   ->where('a.strdepartment = ?', $department->getDepartmentName());
								$reviewers = $q->execute();
								$count_tasks = 0;
								foreach($reviewers as $reviewer)
								{
									$q = Doctrine_Query::create()
									   ->from('Task a')
									   ->where('a.owner_user_id = ?', $reviewer->getNid())
									   ->andWhere('a.application_id = ?', $application->getId());
									$count_tasks = $count_tasks + sizeof($q->execute());
								}
								if($count_tasks <= 0)
								{
									continue;
								}
								$count_deps++;
						?>
							<tr><td colspan='6' style="text-align:left; padding-left: 15px;"><h3><?php echo $department->getDepartmentName(); ?></h3></td></tr>
							<tr>
								<?php
								$q = Doctrine_Query::create()
								   ->from('CfUser a')
								   ->where('a.strdepartment = ?', $department->getDepartmentName());
								$reviewers = $q->execute();
								foreach($reviewers as $reviewer)
								{
									$q = Doctrine_Query::create()
									   ->from('Task a')
									   ->where('a.owner_user_id = ?', $reviewer->getNid());
									$tasks = $q->execute();
									foreach($tasks as $task)
									{
										$tasktype = "";
										if($task->getType() == "1")
										{
											$tasktype = "Review";
										}
										if($task->getType() == "2")
										{
											$tasktype = "Commenting";
										}
										if($task->getType() == "6")
										{
											$tasktype = "Inspection";
										}
										if($task->getType() == "3")
										{
											$tasktype = "Invoicing";
										}
										if($task->getType() == "4")
										{
											$tasktype = "Scanning";
										}
										if($task->getType() == "5")
										{
											$tasktype = "Stamping";
										}
										echo "<td>".$reviewer->getStrfirstname()." ".$reviewer->getStrlastname()."</td>";
										echo "<td>".$tasktype."</td>";
										echo "<td>".$task->getStartDate()."</td>";
										echo "<td>".$task->getEndDate()."</td>";
										echo "<td>0</td>";
										echo "<td>";			
										if($task->getStatus() == "1")
										{
											echo "Pending";
										}
										else if($task->getStatus() == "2")
										{
											echo "Completing";
										}
										else if($task->getStatus() == "25")
										{
											echo "Completed";
										}
										else if($task->getStatus() == "5")
										{
											echo "Cancelling";
										}
										else if($task->getStatus() == "55")
										{
											echo "Cancelled";
										}
										else if($task->getStatus() == "3")
										{
											echo "PostPoned";
										}
										else if($task->getStatus() == "4")
										{
											echo "Transferring";
										}
										else if($task->getStatus() == "45")
										{
											echo "Transferred";
										}
										echo "</td></tr>";
									}
								}
								?>
						<?php
							
						}
						?>
						
								</tbody>
							</table>
						<?php
						if($count_deps == 0)
						{
							?>
							<h4><a href="#">No Reviewers</a></h4>
							<?php
						}
						?>
						</div>
					</div>
					<div id="tabs-5">
						<div class="accordion">
						<h4><a href="#">Comments Summary</a></h4>
						<div>
						    <ul>
							<?php
							    $comment_count = 0;
								$q = Doctrine_Query::create()
								   ->from('CfFormslot a');
								$slots = $q->execute();
								foreach($slots as $slot)
								{
												$q = Doctrine_Query::create()
												   ->from('Comments a')
												   ->where('a.circulation_id = ?', $application->getCirculationId())
												   ->andWhere('a.slot_id = ?', $slot->getNid());
												$comments = $q->execute();
												if(sizeof($comments) > 0)
												{
													$comment_count++;
												?>
													<li><?php echo $slot->getStrname(); ?>
														<ul>
															<?php
															foreach($comments as $comment)
															{
																	$q = Doctrine_Query::create()
																	   ->from('CfInputfield a')
																	   ->where('a.nid = ?', $comment->getFieldId());
																	$field = $q->fetchOne();
																	echo "<li><b style='color:#000; font-weight: 700;'>";
																	echo $field->getStrname()."</b><br>&nbsp;&nbsp;&nbsp;&nbsp;";
																	echo $comment->getComment();
																	echo "</li>";
															}
															?>
														</ul>
													</li>
												<?php
												}
								}
							?>
							
							<?php
								if($comment_count <= 0)
								{
									echo "<li><i class=\"bold-label\">No Comments</i></li>";
								}
							?>
							</ul>
						</div>
						
						<h4><a href="#">Conditions Of Approval Summary</a></h4>
						<div>
						    <ul>
							<?php
												$q = Doctrine_Query::create()
												   ->from('ApprovalCondition a')
												   ->where('a.entry_id = ?', $application->getId());
												$conditions = $q->execute();
												if(sizeof($conditions) > 0)
												{
													$conditions_count++;
												?>
															<?php
															foreach($conditions as $condition)
															{
																	$q = Doctrine_Query::create()
																	   ->from('ConditionsOfApproval a')
																	   ->where('a.id = ?', $condition->getConditionId());
																	$condition = $q->fetchOne();
																	echo "<li><b style='color:#000; font-weight: 700;'>";
																	echo $condition->getShortName()."</b> - ";
																	echo $condition->getDescription();
																	echo "</li>";
															}
															?>
												<?php
												}
							?>
							
							<?php
								if($conditions_count <= 0)
								{
									echo "<li>No Conditions</li>";
								}
							?>
							</ul>
						</div>
						
						<h4><a href="#">Subject To Summary</a></h4>
						<div>
						    <ul>
							<?php
							    $comment_count = 0;
								$q = Doctrine_Query::create()
								   ->from('CfFormslot a');
								$slots = $q->execute();
								foreach($slots as $slot)
								{
												$q = Doctrine_Query::create()
												   ->from('Conditions a')
												   ->where('a.circulation_id = ?', $application->getCirculationId())
												   ->andWhere('a.slot_id = ?', $slot->getNid());
												$conditions = $q->execute();
												if(sizeof($conditions) > 0)
												{
													$conditions_count++;
												?>
													<li><?php echo $slot->getStrname(); ?>
														<ul>
															<?php
															foreach($conditions as $condition)
															{
																	$q = Doctrine_Query::create()
																	   ->from('CfInputfield a')
																	   ->where('a.nid = ?', $condition->getFieldId());
																	$field = $q->fetchOne();
																	echo "<li><b style='color:#000; font-weight: 700;'>";
																	echo $field->getStrname()."</b><br>&nbsp;&nbsp;&nbsp;&nbsp;";
																	echo $condition->getConditionText();
																	echo "</li>";
															}
															?>
														</ul>
													</li>
												<?php
												}
								}
							?>
							
							<?php
								if($conditions_count <= 0)
								{
									echo "<li>No Conditions</li>";
								}
							?>
							</ul>
						</div>
						
						<?php
						if($application->getCirculationId())
						{
							$objMyCirculation = new CCirculation();
							
							 //--- open database
							$nConnection = mysql_connect($DATABASE_HOST, $DATABASE_UID, $DATABASE_PWD);
							if ($nConnection)
							{
								if (mysql_select_db($DATABASE_DB, $nConnection))
								{
									//--- get the single circulation form
									$query = "select * from cf_circulationform WHERE nid=".$application->getCirculationId();
									$nResult = mysql_query($query, $nConnection);

									if ($nResult)
									{
										if (mysql_num_rows($nResult) > 0)
										{
											$arrCirculationForm = mysql_fetch_array($nResult);
										}
									}
									$nSenderUserId = $arrCirculationForm['nsenderid'];
									//-----------------------------------------------
									//--- get history (all revisions)
									//-----------------------------------------------
									$arrHistoryData = array();
									$nMaxRevisionId = 0;
									$strQuery = "SELECT * FROM cf_circulationhistory WHERE ncirculationformid=".$application->getCirculationId()." ORDER BY nrevisionnumber DESC";
									$nResult = mysql_query($strQuery, $nConnection);
									if ($nResult)
									{
										if (mysql_num_rows($nResult) > 0)
										{
											while (	$arrRow = mysql_fetch_array($nResult))
											{
												if ($nMaxRevisionId == 0)
												{
													$nMaxRevisionId = $arrRow["nid"];	
												}
												$arrHistoryData[$arrRow["nid"]] = $arrRow;
											}
										}
									}
									
									if ($_REQUEST['nRevisionId'] == '')
									{
										$_REQUEST['nRevisionId'] = $nMaxRevisionId;
									}
									
									//-----------------------------------------------
									//--- get all users
									//-----------------------------------------------
									$arrUsers = array();
									$strQuery = "SELECT * FROM cf_user  WHERE bdeleted <> 1";
									$nResult = mysql_query($strQuery, $nConnection);
									if ($nResult)
									{
										if (mysql_num_rows($nResult) > 0)
										{
											while (	$arrRow = mysql_fetch_array($nResult))
											{
												$arrUsers[$arrRow["nid"]] = $arrRow;
											}
										}
									}
									
									//-----------------------------------------------
									//--- get the mailing list
									//-----------------------------------------------
									$query = "select * from cf_mailinglist WHERE nid=".$arrCirculationForm["nmailinglistid"];
									$nResult = mysql_query($query, $nConnection);
									if ($nResult)
									{
										if (mysql_num_rows($nResult) > 0)
										{
											$arrMailingList = mysql_fetch_array($nResult);
										}
									}

									$nMailingListID = $arrMailingList['nid'];
									
									//-----------------------------------------------
									//--- get the template
									//-----------------------------------------------	            
									$strQuery = "SELECT * FROM cf_formtemplate WHERE nid=".$arrMailingList["ntemplateid"];
									$nResult = mysql_query($strQuery, $nConnection);
									if ($nResult)
									{
										if (mysql_num_rows($nResult) > 0)
										{
											$arrTemplate = mysql_fetch_array($nResult);
											$strTemplateName = $arrTemplate["strname"];
										}
									}
									
									//-----------------------------------------------
									//--- get the form slots
									//-----------------------------------------------	            
									$arrSlots = array();
									$strQuery = "SELECT * FROM cf_formslot WHERE ntemplateid=".$arrMailingList["ntemplateid"]."  ORDER BY nslotnumber ASC";
									$nResult = mysql_query($strQuery, $nConnection);
									if ($nResult)
									{
										if (mysql_num_rows($nResult) > 0)
										{
											while (	$arrRow = mysql_fetch_array($nResult))
											{
												$arrSlots[] = $arrRow;
											}
										}
									}
									
									//-----------------------------------------------
									//--- get the field values
									//-----------------------------------------------	
												
									$arrValues = array();
									$strQuery = "SELECT * FROM cf_fieldvalue WHERE nformid=".$application->getCirculationId();
									$nResult = mysql_query($strQuery, $nConnection);
									if ($nResult)
									{
										if (mysql_num_rows($nResult) > 0)
										{
											while (	$arrRow = mysql_fetch_array($nResult))
											{
												$arrValues[$arrRow["ninputfieldid"]."_".$arrRow["nslotid"]] = $arrRow;
											}
										}
									}
									
									//-----------------------------------------------
									//--- get the form process detail
									//-----------------------------------------------	            
									$arrProcessInformation = array();
									$arrProcessInformationSubstitute = array();
									
									$strQuery = "SELECT * FROM cf_circulationprocess WHERE ncirculationformid=".$application->getCirculationId()." ORDER BY dateinprocesssince";
									$nResult = mysql_query($strQuery, $nConnection) or die ($strQuery."<br>".mysql_error());
									
									
									if ($nResult)
									{
										if (mysql_num_rows($nResult) > 0)
										{
											$nPosInSlot = -1;
											$nLastSlotId = -1;
											while (	$arrRow = mysql_fetch_array($nResult))
											{
												if ($arrRow["nissubstitiuteof"] != 0)
												{
													if ($arrRow["nslotid"] != $nLastSlotId)
													{
														$nLastSlotId = $arrRow["nslotid"];	
														$nPosInSlot = -1;
													}
													//$nPosInSlot++;
													$arrProcessInformationSubstitute[$arrRow["nissubstitiuteof"]] = $arrRow;
												}
												else
												{
													if ($arrRow["nslotid"] != $nLastSlotId)
													{
														$nLastSlotId = $arrRow["nslotid"];	
														$nPosInSlot = -1;
													}
													$nPosInSlot++;
													$arrProcessInformation[$arrRow["nuserid"]."_".$arrRow["nslotid"]."_".$nPosInSlot] = $arrRow;
												}
											}    				
										}
									}
								}
							}
							
							$nConnection = mysql_connect($DATABASE_HOST, $DATABASE_UID, $DATABASE_PWD);
							if ($nConnection)
							{
								if (mysql_select_db($DATABASE_DB, $nConnection))
								{
									foreach ($arrSlots as $arrSlot)
									{
										$q = Doctrine_Query::create()
											 ->from('CfFieldvalue a')
											 ->where('a.nslotid = ?', $arrSlot['nid'])
											 ->andWhere('a.nformid = ?', $application->getCirculationId());
										$fielddata = $q->execute();
										if(sizeof($fielddata) > 0)
										{
										?>
										<h4><a href="#"><?php echo $arrSlot['strname']; ?></a></h4>
										<div>
											<table width="100%">
											<tr><td style="font-weight: bold;background: #666666; color: #fff;  height: 40px;" colspan="5"><h1 style="margin: 0px; padding: 0px; font-size: 18px; font-family:'Palatino Linotype', 'Book Antiqua', Palatino, serif;"><?php echo $arrSlot['strname']; ?></h1></td></tr>
											<tr>
											<?php
												$strQuery = "SELECT * FROM cf_inputfield INNER JOIN cf_slottofield ON cf_inputfield.nid = cf_slottofield.nfieldid WHERE cf_slottofield.nslotid = ".$arrSlot["nid"]."  ORDER BY cf_slottofield.nposition ASC";
												$nResult = mysql_query($strQuery, $nConnection) or die ($strQuery."<br>".mysql_error());
												if ($nResult)
												{
													if (mysql_num_rows($nResult) > 0)
													{
														$nRunningCounter = 1;
														while (	$arrRow = mysql_fetch_array($nResult))
														{
															if($arrRow["ntype"] == "10")
																							{
																								echo "<td width=\"100%\" valign=\"top\" align=\"left\" style=\" text-align: left;\"><h2 style='margin: 0px; padding: 5px; font-size: 17px; font-family:\"Palatino Linotype\", \"Book Antiqua\", Palatino, serif;'>".htmlentities($arrRow["strname"])."</h2>";
																							}
																							else if($arrRow["ntype"] == "11")
																							{
																								echo "<td width=\"100%\" valign=\"top\" align=\"left\" style=\" text-align: left;\"><h3 style='margin: 0px; padding: 5px; font-size: 16px; font-family:\"Palatino Linotype\", \"Book Antiqua\", Palatino, serif;'>".htmlentities($arrRow["strname"])."</h3>";
																							}
																							else
																							{
																								echo "<td width=\"100%\" valign=\"top\" align=\"left\" style='text-align: left;background-color: #CCCCCC; border: 1px solid silver; padding: 5px;'><div style='margin-bottom: -2px; font-size: 14px; font-family:\"Palatino Linotype\", \"Book Antiqua\", Palatino, serif;'><b>".htmlentities($arrRow["strname"])."</b></div><br>";
																							}
																							
																							
															if ($arrRow["ntype"] == 1)
															{
																if ($arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"]!='')
																{
																	$arrValue = split('rrrrr',$arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"]);
																	
																	
																	$output = replaceLinks($arrValue[0]); 
																	if ($arrRow['strbgcolor'] != "") {
																		$output = '<span style="background-color: #'.$arrRow['strbgcolor'].'">'.$output.'<span>';
																	}																
																	echo $output; 
																}
																else
																{
																	$arrValue = split('rrrrr',$arrRow['strstandardvalue']);
																	
																	$output = replaceLinks($arrValue[0]); 
																	if ($arrRow['strbgcolor'] != "") {
																		$output = '<span style="background-color: #'.$arrRow['strbgcolor'].'">'.$output.'<span>';
																	}																
																	echo $output;
																}
															}
															else if ($arrRow["ntype"] == 2)
															{
																if ($arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"] != "on")
																{
																	$state = "inactive";
																}
																else
																{
																	$state = "active";
																}
																
																echo "<img src=\"/asset_misc/assets_backend/images/$state.gif\" height=\"16\" width=\"16\">";
															}
															else if ($arrRow["ntype"] == 3)
															{
																if ($arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"]!='')
																{
																	$arrValue = split('xx',$arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"]);								
																	$nNumGroup 	= $arrValue[1];														
																	$arrValue1 = split('rrrrr',$arrValue[2]);														
																	$strMyValue	= $arrValue1[0];
																}
																else
																{
																	$arrValue = split('xx',$arrRow['strstandardvalue']);								
																	$nNumGroup 	= $arrValue[1];														
																	$arrValue1 = split('rrrrr',$arrValue[2]);														
																	$strMyValue	= $arrValue1[0];
																}
																$output = replaceLinks($strMyValue); 
																if ($arrRow['strbgcolor'] != "") {
																	$output = '<span style="background-color: #'.$arrRow['strbgcolor'].'">'.$output.'<span>';
																}																
																echo $output;
															}
															else if ($arrRow["ntype"] == 4)
															{
																if ($arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"]!='')
																{
																	$arrValue = split('xx',$arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"]);
																	$nDateGroup 	= $arrValue[1];
																	$arrValue2 = split('rrrrr',$arrValue[2]);
																	$strMyValue 	= $arrValue2[0];
																}
																else
																{
																	$arrValue 		= split('xx',$arrRow['strstandardvalue']);
																	$nDateGroup 	= $arrValue[1];
																	$arrValue2 		= split('rrrrr',$arrValue[2]);
																	$strMyValue 	= $arrValue2[0];
																}
																$output = replaceLinks($strMyValue); 
																if ($arrRow['strbgcolor'] != "") {
																	$output = '<span style="background-color: #'.$arrRow['strbgcolor'].'">'.$output.'<span>';
																}																
																echo $output;
															}
															else if ($arrRow["ntype"] == 5)
															{
																if ($arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"]!='')
																{
																	echo replaceLinks($arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"]);
																}
																else
																{
																	echo replaceLinks($arrRow['strstandardvalue']);
																}
															}
															else if ($arrRow["ntype"] == 6)
															{
																if ($arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"]!='')
																{
																	$strValue = $arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"];
																	$arrMySplit = split('---', $strValue);
																	
																	if ($arrMySplit[1] > 1)
																	{	// edited field values
																		
																		$strValue = '';
																		$nMax = (sizeof($arrMySplit));
																		for ($nIndex = 3; $nIndex < $nMax; $nIndex = $nIndex + 2)
																		{
																			$strValue .= $arrMySplit[$nIndex].'---';
																		}
																		$keyId = rand(1, 150);
																	}
																	else
																	{	// we have to use the standard value
																		$strValue = $arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"];
																		$keyId = rand(1, 150);
																	}
																}
																else
																{
																	$strValue = $arrRow['strstandardvalue'];
																}
																
																$nInputfieldID 	= $arrRow["nfieldid"];
																$bIsEnabled 	= 0;
																
																$strEcho = $objMyCirculation->getRadioGroup2($nInputfieldID,$application->getCirculationId(),$arrSlot['nid'], $strValue, $bIsEnabled, $keyId, $nRunningCounter);
																
																echo $strEcho;
															}
															else if ($arrRow["ntype"] == 7)
															{
																if ($arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"]!='')
																{
																$strValue = $arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"];
																	$arrMySplit = split('---', $strValue);
																	
																	if ($arrMySplit[1] > 1)
																	{	// edited field values
																		
																		$strValue = '';
																		$nMax = (sizeof($arrMySplit));
																		for ($nIndex = 3; $nIndex < $nMax; $nIndex = $nIndex + 2)
																		{
																			$strValue .= $arrMySplit[$nIndex].'---';
																		}
																		$keyId = rand(1, 150);
																	}
																	else
																	{	// we have to use the standard value
																		$strValue = $arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"];
																		$keyId = rand(1, 150);
																	}
																}
																else
																{
																	$strValue = $arrRow['strstandardvalue'];
																}
																
																$nInputfieldID 	= $arrRow["nfieldid"];
																$bIsEnabled 	= 0;
																
																
																$strEcho = $objMyCirculation->getCheckboxGroup2($nInputfieldID,$application->getCirculationId(),$arrSlot['nid'], $strValue, $bIsEnabled, $keyId, $nRunningCounter);
																
																echo $strEcho;										
															}
															elseif($arrRow["ntype"] == 8)
															{
																if ($arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"]!='')
																{
																	$strValue = $arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"];
																	$arrMySplit = split('---', $strValue);
																	
																	if ($arrMySplit[1] > 1)
																	{	// edited field values
																		
																		$strValue = '';
																		$nMax = (sizeof($arrMySplit));
																		for ($nIndex = 3; $nIndex < $nMax; $nIndex = $nIndex + 2)
																		{
																			$strValue .= $arrMySplit[$nIndex].'---';
																		}
																		$keyId = rand(1, 150);
																	}
																	else
																	{	// we have to use the standard value
																		$strValue = $arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"];
																		$keyId = rand(1, 150);
																	}
																}
																else
																{
																	$strValue = $arrRow['strstandardvalue'];
																}
																
																$nInputfieldID 	= $arrRow["nfieldid"];
																$bIsEnabled 	= 0;
																
																
																$strEcho = $objMyCirculation->getComboBoxGroup2($nInputfieldID,$application->getCirculationId(),$arrSlot['nid'], $strValue, $bIsEnabled, $keyId, $nRunningCounter);
																
																echo $strEcho;
															}
															elseif($arrRow["ntype"] == 9)
															{
																if ($arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"]!='')
																{
																	$arrSplit = split('---',$arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"]);
																}
																else
																{
																	$arrSplit = split('---',$arrRow['strstandardvalue']);
																}
																
																$nNumberOfUploads 	= $arrSplit[1];
																$strDirectory		= $arrSplit[2].'_'.$nNumberOfUploads;
																
																$arrValue22 = split('rrrrr',$arrSplit[3]);
																
																$strFilename		= $arrValue22[0];
																
																$strUploadPath 		= '/uploads/';
																$strLink			= $strUploadPath.$strDirectory.'/'.$strFilename;
																
																echo "<a href=\"$strLink\" target=\"_blank\">$strFilename</a>";
															}
															
															echo "</td></tr>\n<tr>";
																								
															
															
															$nRunningCounter++;
														}
														echo "<td></td>";
													}
												}
												
												?>
											</tr>
											</table>
										</div>
										<?php
										}
									}
								}
							}
						}
						?>
					</div>
					</div>
				</div>
</div>