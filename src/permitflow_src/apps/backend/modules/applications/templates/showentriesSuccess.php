<?php
use_helper("I18N");

$audit = new Audit();
$audit->saveAudit("", "Accessed membership form");
?>

<div class="contentpanel panel-email">
    <div class="panel panel-dark">

        <div class="panel-heading">
                <h3 class="panel-title"><?php echo $apform[0]['form_name'] ?></h3>
                
                    
                
                <div class="pull-right">    
                       <a class="btn btn-success-alt settings-margin42" href="<?php echo public_path() ?>backend.php/import/index?from_table=<?php echo $from_table  ?>&&form_id=<?php echo $set_form_id  ?>"> <?php echo __('Import Data') ?></a>
                       <a class="btn btn-primary-alt settings-margin42" href="<?php echo public_path(); ?>backend.php/applications/new/form_id/<?php echo $apform[0]['form_id'] ?>">New <?php echo ucfirst($apform[0]['form_name']) ?> Entry</a>
                        <a class="btn btn-danger-alt settings-margin42" href="<?php echo public_path() ?>backend.php/applications/clearmembers?form_id=<?php echo $set_form_id  ?>"> <?php echo __('Clear Table') ?></a> 
		</div>
        </div>

		<div class="panel panel-body panel-body-nopadding ">
                  <?php if ($sf_user->hasFlash('updated_professionals')) : ?>
                    <div class="alert alert-success">
                        <?php echo __('Successfuly updated professional database!') ; ?>
                    </div>
                 <?php endif; ?>
                 
                <?php if ($sf_user->hasFlash('error_professionals')) : ?>
                    <div class="alert alert-danger">
                        <?php echo __('Error uploading data') ; ?> -- <?php echo $sf_user->getFlash('error_professionals') ?>
                    </div>
                 <?php endif; ?>
                    
                <?php if ($sf_user->hasFlash('unknown_professionals')) : ?>
                    <div class="alert alert-success">
                        <?php echo __('File Error. Cannot update database at this time. Try again later!') ; ?>
                    </div>
                 <?php endif; ?>
                    
                    <?php if ($sf_user->hasFlash('deleted')) : ?>
                    <div class="alert alert-success">
                        <?php echo $sf_user->getFlash('deleted') ; ?>
                    </div>
                 <?php endif; ?>  
                    <?php if ($sf_user->hasFlash('delete_fail')) : ?>
                    <div class="alert alert-success">
                        <?php echo $sf_user->getFlash('delete_fail') ; ?>
                    </div>
                 <?php endif; ?>  
                    
                    <?php if ($sf_user->hasFlash('member_deleted')) : ?>
                    <div class="alert alert-success">
                        <?php echo $sf_user->getFlash('member_deleted') ; ?>
                    </div>
                 <?php endif; ?>  
                    <?php if ($sf_user->hasFlash('member_fail')) : ?>
                    <div class="alert alert-success">
                        <?php echo $sf_user->getFlash('member_fail') ; ?>
                    </div>
                 <?php endif; ?>  
                    
                    
		<div class="table-responsive">
		<table class="table dt-on-steroids mb0" id="table3">
			<thead>
		   
			  <th>#</th>
			  <?php foreach($form_elements as $element): ?>
			      <th><?php echo $element['element_title'] ?></th>
			  <?php endforeach; ?>
			  <th class="no-sort"><?php echo __('Actions'); ?></th>
		
		  </thead>
		  <tbody>
			<?php foreach($entries as  $entry): ?>
			<tr>
				<td><?php echo $entries['id']; ?></td>
				<?php foreach($form_elements as $e): ?>
                                    <td>
                                    <?php echo $entry['element_'.$e['element_id']] ?>
                                    </td>
				<?php endforeach; ?>
			   <td>
				<a title="<?php echo __('Edit'); ?>" href="<?php echo public_path(); ?>backend.php/applications/edit?form_id=<?php echo $apform[0]['form_id']; ?>&id=<?php echo $entry['id']; ?>"><span class="badge badge-primary"><i class="fa fa-pencil"></i></span></a>
                                <a href="<?php echo public_path(); ?>backend.php/applications/deletemember?form_id=<?php echo $apform[0]['form_id']; ?>&id=<?php echo $entry['id']; ?>" title="<?php echo __('Delete') ?>"> <span class="badge badge-danger"><i class="fa fa-recycle"></i></span></a>
                                
                           </td>
                           
			</tr>

			<?php endforeach; ?>

		</tbody>
		</table>
		</div>
		</div><!--panel-body-->
    </div>
</div>
<script>
  jQuery(document).ready(function() {
      jQuery('#table3').dataTable({
          "sPaginationType": "full_numbers",

          // Using aoColumnDefs
          "aoColumnDefs": [
          	{ "bSortable": false, "aTargets": [ 'no-sort' ] }
        	]
        });
    });
</script>
