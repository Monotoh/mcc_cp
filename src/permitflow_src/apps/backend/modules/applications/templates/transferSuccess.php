<?php
use_helper("I18N");

include_partial('dashboard/checksession');

//if($sf_user->mfHasCredential("accesssubmenu".$application->getApproved()))
{
?>
<?php
/**
 * view template.
 *
 * Displays a single application and all of its review history
 *
 * @package    backend
 * @subpackage applications
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

$q = Doctrine_Query::create()
    ->from("FormEntry a")
    ->where("a.parent_submission = ?", $application->getId())
    ->orderBy("a.id DESC");
$prevsubmissions = $q->execute();

?>
<div class="pageheader">
    <h2><i class="fa fa-envelope"></i> <?php echo __('Applications'); ?></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><?php echo __('You are here'); ?>:</span>
        <ol class="breadcrumb">
            <li><a href="<?php echo public_path("backend.php"); ?>"><?php echo __('Home'); ?></a></li>
            <li><a href="<?php echo public_path("backend.php/applications/list/get/your"); ?>"><?php echo __('Applications'); ?></a></li>
        </ol>
    </div>
</div>

<div class="contentpanel">
<div class="row">

<div class="col-sm-3">
    <div class="blog-item pt20 pb5">

        <h5 class="subtitle mt20 ml20"><?php echo __('Application Summary'); ?></h5>

        <ul class="profile-social-list">
            <li><a href=""><?php
                    $q = Doctrine_Query::create()
                        ->from("SubMenus a")
                        ->where("a.id = ?", $application->getApproved());
                    $stage = $q->fetchOne();
                    if($stage)
                    {
                    $q = Doctrine_Query::create()
                        ->from("Menus a")
                        ->where("a.id = ?", $stage->getMenuId());
                    $parentstage = $q->fetchOne();
                    ?><?php echo $parentstage->getTitle(); ?></a></li><?php
            }

            $q = Doctrine_Query::create()
                ->from("SfGuardUserProfile a")
                ->where("a.user_id = ?", $application->getUserId());
            $architect = $q->fetchOne();
            ?>
            <li><?php echo __('Submitted by'); ?> <a href=""> <?php echo $architect->getFullname(); ?></a></li>
            <li><?php echo __('Date of Submission'); ?> <a><?php echo str_replace(" ", " @ ", $application->getDateOfSubmission()); ?></a></li>
            <?php
            if($application->getDateOfResponse())
            {
                ?>
                <li><?php echo __('Date of Approval'); ?> <a><?php echo str_replace(" ", " @ ", $application->getDateOfResponse()); ?></a></li>
            <?php
            }
            ?>
            <?php
            function GetDaysSince($sStartDate, $sEndDate){
                $start_ts = strtotime($sStartDate);
                $end_ts = strtotime($sEndDate);
                $diff = $end_ts - $start_ts;
                return round($diff / 86400);
            }
            $days =  GetDaysSince($application->getDateOfSubmission(), date("Y-m-d H:i:s"));

            $days_color = "";
            $maximum_duration = 0;

            //get maximum duration of current stage
            $q = Doctrine_Query::create()
                ->from("SubMenus a")
                ->where("a.id = ?", $application->getApproved());
            $current_stage = $q->fetchOne();
            if($current_stage)
            {
                $maximum_duration = $current_stage->getMaxDuration();
            }

            if($days < $maximum_duration || $maximum_duration == 0){
                $days_color = "success";
            }
            elseif($days >= $maximum_duration){
                $days_color = "danger";
            }
            ?>
            <li><?php echo __('Days in progress'); ?> <span

                    class="badge badge-<?php echo $days_color; ?>"

                    ><strong>
                        <?php
                        echo $days." ".__('days');
                        ?>
                    </strong>
            </span>
            </li>
        </ul>

        <ul class="profile-social-list">
            <li><?php echo __('Application Status'); ?><a href=""> <?php echo $stage->getTitle(); ?> </a></li>
            <?php
            $q = Doctrine_Query::create()
                ->from("SubMenus a")
                ->where("a.id = ?", $application->getApproved());
            $current = $q->fetchOne();

            $current_stage_no = 0;

            if($current)
            {
                $parent_menu = $current->getMenuId();

                $q = Doctrine_Query::create()
                    ->from("SubMenus a")
                    ->where("a.menu_id = ?", $parent_menu)
                    ->orderBy("a.order_no ASC");
                $stages = $q->execute();

                $countstages = 0;
                foreach($stages as $stage)
                {
                    $countstages++;
                    if($stage->getId() == $current->getId())
                    {
                        $current_stage_no = $countstages;
                    }
                }
            }

            if($countstages == 0)
            {
                $percentage = 0;
            }
            else
            {
                $percentage = ($current_stage_no/$countstages) * 100;
            }
            ?>
            <li>
                <span class="badge badge-<?php echo $days_color; ?> pull-left mr5"><strong><?php echo round($percentage, 0); ?>%</strong></span>
                <div class="progress">
                    <div class="progress-bar progress-bar-<?php echo $days_color; ?>" role="progressbar" aria-valuenow="<?php echo $percentage; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $percentage; ?>%">
                        <span class="sr-only"><?php echo $percentage; ?>% Complete</span>
                    </div>
                </div>
            </li>
        </ul>
        <div class="mb10"></div>
        <?php
        if($application->getParentSubmission() == "0")
        {
            ?>
            <h5 class="subtitle ml20"><?php echo __('Revisions'); ?></h5>
        <?php
        }
        ?>

        <ul class="profile-social-list">
            <?php
            foreach($prevsubmissions as $currentapplication)
            {
                ?>
                <li><a target="_blank" href="<?php echo public_path('backend.php/applications/view/id/'.$currentapplication->getId()); ?>"><?php echo $architect->getFullname(); ?> (<?php echo $currentapplication->getDateOfSubmission(); ?>)</a></li>
            <?php
            }
            if(sizeof($prevsubmissions) <= 0)
            {
                ?>
                <li><?php echo __('No Revisions Found'); ?></li>
            <?php
            }
            ?>
        </ul>
        <div class="mb20"></div>
        <?php
        //Display control buttons that manipulate the application
        ?>
    </div><!-- blog-item -->
</div><!-- col-sm-3 -->
<div class="col-sm-9">
<?php
if($sf_user->hasFlash('notice'))
{
    ?>
    <div class="alert alert-success">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <strong><?php echo $sf_user->getFlash('notice'); ?></strong>
    </div>
<?php
}
?>
<div class="panel panel-dark">
    <div class="panel-heading">
        <h4 class="panel-title">Transfer ownership of <?php echo $application->getApplicationId(); ?> to another user
            <span class="badge"><?php

                $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
                mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);


                if($application->getParentSubmission() != 0)
                {
                    echo "<strong style=\"font-size:13px\">".__('Revision')."</strong>";
                }
                ?></span>
        </h4>
        <p>
            <?php
            $q = Doctrine_Query::create()
                ->from("ApForms a")
                ->where("a.form_id = ?", $application->getFormId());
            $form = $q->fetchOne();
            if($form)
            {
                $name = $form->getFormName();

                $sql = "SELECT * FROM ext_translations WHERE field_id = '".$form->getFormId()."' AND field_name = 'form_name' AND table_class = 'ap_forms' AND locale = '".$sf_user->getCulture()."'";

                $rows = mysql_query($sql, $dbconn);
                if($row = mysql_fetch_assoc($rows))
                {
                    $name = $row['trl_content'];
                }
                echo $name;
            }
            ?>
        </p>
    </div>
    <div class="col-lg-12">
        <div class="transfer_success">

        </div>
    </div>
    <div class="panel-body panel-body-nopadding">
        <!-- BASIC WIZARD -->
        <div id="basicWizard" class="basic-wizard">
            <form id="bannerform" class="form-bordered form-horizontal" action="/backend.php/applications/successfulltransfer/id/<?php echo $application->getId(); ?>" method="post" <?php $transferform->isMultipart() and print 'enctype="multipart/form-data" ' ?>   autocomplete="off" data-ajax="false">
                <div class="panel-body panel-body-nopadding">

                    <?php echo $transferform->renderGlobalErrors() ?>
                    <?php if(isset($transferform['_csrf_token'])): ?>
                        <?php echo $transferform['_csrf_token']->render(); ?>
                    <?php endif; ?>

                    <?php
                    if($sf_user->hasFlash('error'))
                    {
                        ?>
                        <div class="alert alert-danger">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <strong><?php echo $sf_user->getFlash('error'); ?></strong>.
                        </div>
                    <?php
                    }
                    ?>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Enter email address of the new owner'); ?></i></label>
                        <div class="col-sm-8">
                            <?php echo $transferform['email']->renderError() ?>
                            <?php echo $transferform['email'] ?>
                        </div>
                    </div>


                </div><!--panel-body-->

                <div class="panel-footer"><button id="submitbuttonname" type="submit" class="btn btn-primary" name="submitbuttonname" value="submitbuttonvalue"><?php echo __('Transfer'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
</div>


<?php
}
/*else
{
	include_partial("settings/accessdenied");
}*/
?>
 <script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {

        $('form#bannerform').on('submit', function (form) {
            form.preventDefault();

            let email = $("#transfer_email").val();
            let srvRqst = $.ajax({
                url: '/backend.php/applications/checkuser',
                type: 'post',
                data: {email: email},
                datatype: 'text',
            });

            srvRqst.done(function (response) {
                if (response == 1) {
                    let post_url = "/backend.php/applications/successfulltransfer/id/<?php echo $application->getId(); ?>";
                    
                     $.post(post_url, $('form#bannerform').serialize(), function (data) {
                        data = "<p class='alert alert-danger'> Successful Transfer of the application</p>"
                        $('div.transfer_success').html(data);
                        url = "/backend.php/applications/view/id/<?php echo $application->getId(); ?>/current_tab/user";
                        setTimeout(function(){window.location = url;}, 1000);
                    });
                } else {
                    data = "<p class='alert alert-danger'>The Email address, Doesn't Exist</p>"
                        $('div.transfer_success').html(data);
                }
            });
           
        });
    });
</script>