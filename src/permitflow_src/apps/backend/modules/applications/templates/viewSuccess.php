<?php
/**
 * view template.
 *
 * Displays a single application and all of its review history
 *
 * @package    backend
 * @subpackage applications
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");

include_partial('dashboard/checksession');?>
<script src="<?php echo public_path(); ?>metronicv3/assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="http://code.jquery.com/jquery-1.11.3.js" type="text/javascript"></script>
<?php
$invoice_manager = new InvoiceManager();
$invoice_manager->update_invoices($application->getId());

$permit_manager = new PermitManager();
if($permit_manager->needs_permit_for_current_stage($application->getId()))
{
    $permit_manager->create_permit($application->getId());
}
//OTB patch 
$otbhelper = new OTBHelper();
?>

<div class="pageheader">
  <h2><i class="fa fa-envelope"></i> <?php echo __('Applications'); ?></h2>
  <div class="breadcrumb-wrapper">
    <span class="label"><?php echo __('You are here'); ?>:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>backend.php"><?php echo __('Home'); ?></a></li>
      <li><a href="<?php echo public_path(); ?>backend.php/applications/list/get/your"><?php echo __('Applications'); ?></a></li>
    </ol>
  </div>
</div>

<div class="contentpanel">


   <?php
      	 if($sf_user->hasFlash('decline_warning'))
      	 {
      	?>
      	<div class="alert alert-danger">
      		<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
      		<?php echo __("Warning: You still have an unresolved reason for decline. Please mark it as resolved to continue processing this application") ?>
                <?php echo __("Click on Review History tab, then click link Previous Reasons for Decline and mark issues as resolved") ?>
      	</div>
      	<?php
      	 }
      	?>

        <?php if($sf_user->hasFlash('manual_online_success')): ?>
    <div class="alert alert-success">
        <?php echo $sf_user->getFlash('manual_online_success') ?>
    </div>
    <?php endif; ?>
     <?php if($sf_user->hasFlash('manual_online_error')): ?>
    <div class="alert alert-danger">
        <?php echo $sf_user->getFlash('manual_online_error') ?>
    </div>
    <?php endif; ?>
    <!-- Success or Error message -->
      <?php if($sf_user->hasFlash('reset_invoice')): ?>
    <div class="alert alert-success">
        <?php echo $sf_user->getFlash('reset_invoice') ?>
    </div>
    <?php endif; ?>
     <?php if($sf_user->hasFlash('reset_invoice_error')): ?>
    <div class="alert alert-danger">
        <?php echo $sf_user->getFlash('reset_invoice_error') ?>
    </div>
    <?php endif; ?>
    
    <!-- OTB patch -- Task Reset message -->
      <?php if($sf_user->hasFlash('task_reset_all')): ?>
      <div class="alert alert-success">         
            <?php echo $sf_user->getFlash('task_reset_all',ESC_RAW); ?>      
      </div>
      <?php endif; ?>    
       <?php if($sf_user->hasFlash('task_reset_error_all')): ?>
      <div class="alert alert-danger">
          <?php echo $sf_user->getFlash('task_reset_error_all',ESC_RAW); ?>
      </div>
      <?php endif; ?>
    
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
    <!-- Action Buttons -->
       <?php
        if($sf_user->mfHasCredential("accesssubmenu".$application->getApproved()))
        {
            //Display control buttons that manipulate the application

            //Hide actions if this is an invoicing stage and the application is not paid for
            $q = Doctrine_Query::create()
                ->from('SubMenus a')
                ->where('a.id = ?', $application->getApproved())
                ->limit(1);
            $stage = $q->fetchOne();

            if($stage && $stage->getStageType() == 3) {
                //search for unpaid invoices
                $paid = true;
                $invoices = $application->getMfInvoice();
                $db_date_expiry = null ; //Check if invoice expired
                foreach($invoices as $invoice)
                {
                    if($invoice->getPaid() != 2 && $invoice->getPaid() != 3)
                    {
                        $paid = false;
                        //OTB patch
                        $db_date_event = str_replace('/', '-', $invoice->getExpiresAt());
                        $db_date_expiry = strtotime($db_date_event);
                    }
                }
                $expired_invoice = false ;
                if(time() > $expired_invoice){
                    $expired_invoice = true;
                }
                

                //if no pending invoices are found then display actions
                if($paid || $expired_invoice) {
                    include_partial('viewbuttons', array('application' => $application));
                }
            }
            else
            {
                include_partial('viewbuttons', array('application' => $application));
            }
        }
        else
        {
          //view edit button to edit drafts
          if($sf_user->mfHasCredential("editapplication"))
        	{
          	?>
            <div class="btn-group pull-right">
          		<a class="btn btn-primary" onClick="window.location='/backend.php/applications/edit?form_id=<?php echo $application->getFormId(); ?>&id=<?php echo $application->getEntryId(); ?>';"><?php echo __('Edit Application'); ?></a>
            </div>
            <?php
          }
        }
        ?>

    
    </div>
    </div><!-- col-sm-12 -->
    <br/><br/>
    <div class="col-sm-3">
         <div class="panel panel-default">
   	 <div class="blog-item pt20 pb5">
    <h5 class="subtitle mt20 ml20"><?php echo __('Application Summary'); ?></h5>

    <ul class="profile-social-list">
     	<li><a href=""><?php
        		$q = Doctrine_Query::create()
        			->from("SubMenus a")
        			->where("a.id = ?", $application->getApproved())
                    ->limit(1);
        		$stage = $q->fetchOne();
        		if($stage)
        		{
        			$q = Doctrine_Query::create()
          			->from("Menus a")
          			->where("a.id = ?", $stage->getMenuId())
                        ->limit(1);
          		$parentstage = $q->fetchOne();
                ?><?php if($parentstage) {echo $parentstage->getTitle();} ?></a></li><?php
          	}

            $q = Doctrine_Query::create()
                ->from("SfGuardUserProfile a")
                ->where("a.user_id = ?", $application->getUserId())
                ->limit(1);
            $architect = $q->fetchOne();
            
            if(empty($architect))
            {
                error_log("FormEntry: Not user found for ".$application->getApplicationId().": ".$ex); 
                ?>
                <li>- Could not verify user. Contact System Administrator -</a></li>
                <?php 
            }
            else 
            {
                ?>
                <li><?php echo __('Submitted by'); ?> <a href="/backend.php/frusers/show/id/<?php echo $application->getUserId(); ?>"> <?php echo $architect->getFullname(); ?></a></li>
                <?php 
            }
            ?>
            <li><?php echo __('Date of Submission'); ?> <a><?php echo str_replace(" ", " @ ", $application->getDateOfSubmission()); ?></a></li>
            <?php
            if($application->getDateOfResponse())
            {
            ?>
            <li><?php echo __('Date of Approval'); ?> <a><?php echo str_replace(" ", " @ ", $application->getDateOfResponse()); ?></a></li>
            <?php
            }
            ?>
           <?php
						//Mombasa OTB Start - System now only accounts for the number of days within the working days of the week. i.e, excludes weekends
						function getWorkingDays($startDate,$endDate,$holidays){
							// do strtotime calculations just once
							$endDate = strtotime($endDate);
							$startDate = strtotime($startDate);


							//The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
							//We add one to inlude both dates in the interval.
							$days = ($endDate - $startDate) / 86400 + 1;

							$no_full_weeks = floor($days / 7);
							$no_remaining_days = fmod($days, 7);

							//It will return 1 if it's Monday,.. ,7 for Sunday
							$the_first_day_of_week = date("N", $startDate);
							$the_last_day_of_week = date("N", $endDate);

							//---->The two can be equal in leap years when february has 29 days, the equal sign is added here
							//In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
							if ($the_first_day_of_week <= $the_last_day_of_week) {
								if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
								if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
							}
							else {
								// (edit by Tokes to fix an edge case where the start day was a Sunday
								// and the end day was NOT a Saturday)

								// the day of the week for start is later than the day of the week for end
								if ($the_first_day_of_week == 7) {
									// if the start date is a Sunday, then we definitely subtract 1 day
									$no_remaining_days--;

									if ($the_last_day_of_week == 6) {
										// if the end date is a Saturday, then we subtract another day
										$no_remaining_days--;
									}
								}
								else {
									// the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
									// so we skip an entire weekend and subtract 2 days
									$no_remaining_days -= 2;
								}
							}

							//The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
						//---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
						   $workingDays = $no_full_weeks * 5;
							if ($no_remaining_days > 0 )
							{
							  $workingDays += $no_remaining_days;
							}

							//We subtract the holidays
							foreach($holidays as $holiday){
								$time_stamp=strtotime($holiday);
								//If the holiday doesn't fall in weekend
								if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
									$workingDays--;
							}

							return $workingDays;
						}
//						//Mombasa OTB End - System now only accounts for the number of days within the working days of the week. i.e, excludes weekends
//						function GetDaysSince($sStartDate, $sEndDate){
//                                                        //Fix -ve on display of days application with district
//                                                       $start_t = strtotime($sStartDate) ;
//                                                       $end_t = strtotime($sEndDate) ;
//                                                       
//                                                       //
//							$working_days = round(getWorkingDays($sStartDate,$sEndDate));// -1;//Do not include start day
//							$working_days = $working_days>0?$working_days-1:$working_days;
//							error_log("otb sStartDate: ".$sStartDate." end date: ".$sEndDate." Working days: ".$working_days);
//							return floor($working_days);//Mombasa OTB - System now only accounts for the number of days within the working days of the week. i.e, excludes weekends
//							/*$start_ts = strtotime($sStartDate);
//							$end_ts = strtotime($sEndDate);
//							$diff = $end_ts - $start_ts;
//							return round($diff / 86400);*/
//						}
                                                function GetDaysSince($sStartDate, $sEndDate){
                                                            //$start_ts = strtotime($sStartDate);
                                                            $start_ts = strtotime(date("Y-m-d", strtotime($sStartDate)));
                                                            $end_ts = strtotime($sEndDate);
                                                            //today 
                                                            $today = date('Y-m-d');
                                                            $diff = $end_ts - $start_ts;
                                                            //prevent -ve values because of missing entries in Application Reference
                                                            if($diff < 0 ){

                                                                $diff = strtotime($today) - $start_ts ;
                                                            }
                                                            return round($diff / 86400);
                                                    }

						function GetLastDayWorkedOn($application){//OTB - Use last day of action on application. Work can still continue after a permit has been issued
							$q = Doctrine_Query::create()
								->from('ApplicationReference b')
								->where('b.application_id = ?', $application->getId())
								->andWhere('b.start_date IS NOT NULL')
								->andWhere('b.end_date IS NOT NULL')
								->andWhere('b.start_date <= b.end_date')
								->orderBy("b.id DESC");
							$application_reference_list = $q->execute();
							return $application_reference_list[0]->getEndDate();
						}

                       $days = 0;
//                       if($application->getDateOfResponse())
//                       {
//                           //$days = GetDaysSince($application->getDateOfSubmission(), $application->getDateOfResponse());
//                           $days = GetDaysSince($application->getDateOfSubmission(), GetLastDayWorkedOn($application));//OTB - Use last day of action on application. Work can still continue after a permit has been issued
//						   error_log("ot b last day #".GetLastDayWorkedOn($application)." difference ".$days);
//                       }
//                       else {
//                          
//                       }
                        $days = GetDaysSince($application->getDateOfSubmission(), date("Y-m-d H:i:s"));

						$days_color = "";
						$maximum_duration = 0;

						//get maximum duration of current stage
						$q = Doctrine_Query::create()
							->from("SubMenus a")
							->where("a.id = ?", $application->getApproved());
						$current_stage = $q->fetchOne();
						if($current_stage)
						{
							$maximum_duration = $current_stage->getMaxDuration();
						}

						if($days < $maximum_duration || $maximum_duration == 0){
								$days_color = "success";
						}
						elseif($days >= $maximum_duration){
								$days_color = "danger";
						}
					   ?>
            <li><?php echo __('Days in progress'); ?> <span

            class="badge badge-<?php echo $days_color; ?>"

            ><strong>
            <?php
						echo $days." ".__('days');
						?>
            </strong>
            </span>
        </li>
      </ul>

      <ul class="profile-social-list">
        <li><?php echo __('Application Status'); ?><a href=""> <?php if($stage){

            $q = Doctrine_Query::create()
                ->from("Menus a")
                ->where("a.id = ?", $stage->getMenuId())
                ->limit(1);
            $parent_stage = $q->fetchOne();
            if($parent_stage)
            {
                echo "<br>".$parent_stage->getTitle()." &gt; ";
            }
           if($stage){
           echo $stage->getTitle(); 
           
           }

        }else{ echo "Drafts"; } ?> </a></li>
    </ul>
<!--OTB Start - Show previous application numbers if any-->
      <!--<ul class="profile-social-list">
        <li><?php //echo __('Previous Application Numbers'); ?><a href=""> <?php 
          //$previous_numbers = Doctrine_Manager::getInstance()->getCurrentConnection()->execute("SELECT * FROM application_number_history WHERE form_entry_id = " . $application->getId());

         /* foreach($previous_numbers as $rec)
          {
                echo "<br>".$rec['application_number'];
		  } */
		?> </a></li>
    </ul>-->
<!--OTB End - Show previous application numbers if any-->

	<!-- Start OTB Patch - Show Plot Info from LAIS for Rwanda -->
<?php 
		$appdatas = Doctrine_Manager::getInstance()->getCurrentConnection()->execute("SELECT * FROM ap_form_".$application->getFormId()." WHERE id = " . $application->getEntryId());
		$formmanager = new FormManager();
		$lais_integration_logic = $formmanager->getLogicFields(False, $application->getFormId(),True);
		//error_log("the integration_logic array we have is ##### ".print_R($lais_integration_logic, true));
		$element_data = array_shift($lais_integration_logic);
          foreach($appdatas as $appdata)
          {
?>
<script type="text/javascript">

jQuery(document).ready(function(){
var upi = '<?php echo $appdata['element_'.$element_data['element_id']];?>';
$.post('/index.php/jquery/checkupi/', {upi:upi}, function(plot){
	if ( plot == 'false' ) {
		$( "#lais_plot_details" ).append( "<br/><p style='color:red;'><b>Please Note! Plot data could not be fetched for the given UPI in LAIS.</b></p>");
	}else if ( plot && plot.return ) {
		//alert('Please Note! The plot data has been pre-filled for UPI '+plot.return.UPI+' of plot size '+plot.return.size);
		$( "#lais_plot_details" ).append( "<br/><p>Owner First Name: <b>" + (plot.return.owner[0] ? plot.return.owner[0].owner.givenName : plot.return.owner.owner.givenName) +"</b></p>");
		$( "#lais_plot_details" ).append( "<br/><p>Owner Surname: <b>" + (plot.return.owner[0] ? plot.return.owner[0].owner.surname : plot.return.owner.owner.surname) +"</b></p>");
		$( "#lais_plot_details" ).append( "<br/><p>Size: <b>" + plot.return.size + "</b></p>");
		$( "#lais_plot_details" ).append( "<br/><p>Sector: <b>" + plot.return.address.sector + "</b></p>");
		$( "#lais_plot_details" ).append( "<br/><p>Cell: <b>" + plot.return.address.cell + "</b></p>");
		$( "#lais_plot_details" ).append( "<br/><p>Village: <b>" + plot.return.address.village + "</b></p>");
	}else if (upi) {
		$( "#lais_plot_details" ).append( "<br/><p style='color:red;'><b>Please Note! Plot data could not be fetched for the given UPI in LAIS.</b></p>");
	}
}, 'json').fail(function() {
	$( "#lais_plot_details" ).append( "<br/><p style='color:red;'><b>Please Note! The plot data could not be fetched at this time.</b></p>");
	});

});
</script>
	<!--<ul class="profile-social-list" style="background-color:#f4fef4;">
		<li><b><?php echo __('LAIS Plot Information'); ?></b>
		<br/><?php echo __('UPI'); ?>: <b><?php echo $appdata['element_'.$element_data['element_id']];?></b>
		<div id="lais_plot_details">
		</div>
		</li>
	</ul>-->
	<?php } ?>
	<!-- End OTB Patch - Show Plot Info from LAIS for Rwanda -->
    <div class="mb10"></div>
			<?php
      if($application->getParentSubmission() == "0")
      {
        ?>
          <h5 class="subtitle ml20"><?php echo __('Revisions'); ?></h5>
        <?php
			}
      ?>

      <ul class="profile-social-list">
         <?php
           //OTB patch - Get Application previously details - Revisons
             $old_id = $application->getPreviousSubmission();
             //error_log("Old ID >> ".$old_id) ;
             $q = Doctrine_Query::create()
                    ->from('FormEntry f')
                    ->where('f.id = ? ',$old_id) ;
             $q_r = $q->fetchOne();
             if($q_r){ // if record exists ?>
            <li>
                <a target="_blank" href="<?php echo public_path(); ?>backend.php/applications/view/id/<?php echo $q_r->getId(); ?>">
                 <?php echo $q_r->getDateOfSubmission() ?>
                </a>
            </li>
                 <?php
             }else { ?>
                 	<li><?php echo __('No Revisions Found'); ?></li>
            <?php }
         ?>
          
      </ul>
    <div class="mb20"></div>
    </div><!-- blog-item -->
   </div>
  </div><!-- col-sm-3 -->
  <div class="col-sm-9">

	 <div class="panel panel-default">
      <div class="panel-heading">
        	<h4 class="panel-title"><a href="/backend.php/applications/view/id/<?php echo $application->getId(); ?>/refreshid/true"><?php echo $application->getApplicationId(); ?></a>
              <span class="badge"><?php

							//$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
							//mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);


						  if($application->getParentSubmission() != 0)
						  {
							  echo "<strong style=\"font-size:13px\">".__('Revision')."</strong>";
						  }
						  ?></span>
			        </h4>
              <p>
              <?php
              $q = Doctrine_Query::create()
              	->from("ApForms a")
              	->where("a.form_id = ?", $application->getFormId())
                  ->limit(1);
              $form = $q->fetchOne();
              if($form)
              {
	              $name = $form->getFormName();

								$sql = "SELECT * FROM ext_translations WHERE field_id = '".$form->getFormId()."' AND field_name = 'form_name' AND table_class = 'ap_forms' AND locale = '".$sf_user->getCulture()."'";

								$rows = mysqli_query($otbhelper->getDB(),$sql);
								if($row = mysqli_fetch_assoc($rows))
								{
										$name = $row['trl_content'];
								}
									echo $name;
              }
              ?>
    			</p>
    </div>

    <div class="panel-body panel-body-nopadding">
    	<?php
      	 if($sf_user->hasFlash('notice'))
      	 {
      	?>
      	<div class="alert alert-success">
      		<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
      		<?php echo $sf_user->getFlash('notice',ESC_RAW); ?>.
      	</div>
      	<?php
      	 }
      	?>

				<!-- BASIC WIZARD -->
			  <div id="basicWizard" class="basic-wizard">

			     <ul class="nav nav-pills nav-justified">
		       <?php
            if($application->getParentSubmission() == "0")
            {
              ?>
        			<li class="<?php if($current_tab == "application"){ echo "active"; } ?>"><a class="" href="/backend.php/applications/view/id/<?php echo $application->getId(); ?>/current_tab/application"><?php echo __('Application Details'); ?></a></li>

        			<li class="<?php if($current_tab == "attachments"){ echo "active"; } ?>"><a class="" href="/backend.php/applications/view/id/<?php echo $application->getId(); ?>/current_tab/attachments"><?php echo __('More Details'); ?></a></li>
              <?php
						}
            else
            {
                ?>
	          		<li style="display: none;"><a href="/backend.php/applications/view/id/<?php echo $application->getId(); ?>?current_tab=application" data-toggle="tab"><?php echo __('Application Details'); ?></a></li>
                <?php
						}
						?>
           <?php
            $q = Doctrine_Query::create()
               ->from("FormEntryLinks a")
               ->where("a.formentryid = ?", $application->getId());
            $links = $q->execute();
            if(sizeof($links) > 0)
            {
            	?>
                  <li><a href="#ptabadd" data-toggle="tab">Additional Details</a></li>
                  <?php
            }

						if($application->getParentSubmission() == "0")
						{
							?>
                 <!-- <li <?php //if($current_tab == "user"){ echo "class='active'"; } ?>><a class="" href="/backend.php/applications/view/id/<?php// echo $application->getId(); ?>/current_tab/user"><?php //echo __("Applicant Details"); ?></a></li>-->
		            <li <?php if($current_tab == "review"){ echo "class='active'"; } ?>><a class="" href="/backend.php/applications/view/id/<?php echo $application->getId(); ?>/current_tab/review"><?php echo __('Review History'); ?></a></li>
		            <li <?php if($current_tab == "billing"){ echo "class='active'"; } ?>><a class="" href="/backend.php/applications/view/id/<?php echo $application->getId(); ?>/current_tab/billing"><?php echo __('Billing History'); ?></a></li>
		            <li <?php if($current_tab == "history"){ echo "class='active'"; } ?>><a class="" href="/backend.php/applications/view/id/<?php echo $application->getId(); ?>/current_tab/history"><?php echo __('Application History'); ?></a></li>
                    <li <?php if($current_tab == "messages"){ echo "class='active'"; } ?>><a class="" href="/backend.php/applications/view/id/<?php echo $application->getId(); ?>/current_tab/messages"><?php echo __('Client Messages'); ?></a></li>
                   <li <?php if($current_tab == "memo"){ echo "class='active'"; } ?>><a class="" href="/backend.php/applications/view/id/<?php echo $application->getId(); ?>/current_tab/memo"><?php echo __('Staff Messages'); ?></a></li>
             <?php
						}
						?>
			        </ul>
			        <div class="tab-content tab-content-nopadding">
                  <?php  if($current_tab == "application"){ ?>
			              <div class="tab-pane <?php if($current_tab == "application"){ echo "active"; } ?>" id="ptab1">
    									<?php
    		                if($application->getParentSubmission() == "0")
    		                {
    		                ?>

    				            <?php
    										}
    		                else
    		                {
    		                ?>
    			           		<h4><strong style="text-transform:uppercase;"><?php echo __('Application details'); ?></strong></h4>
                          <?php
    										}
    										?>
    			              <?php
      				                //Display control buttons that manipulate the application
      				                include_partial('viewdetails', array('application' => $application, 'choosen_form' => $choosen_form));
    				             ?>
    			            </div>

        		          <div class="tab-pane" id="ptabadd">
        		          <?php
	                     if($links)
                       {
                          ?>
                           <div class="panel-group panel-group-dark mb0" id="accordion1">
                          <?php
                              $q = Doctrine_Query::create()
                                 ->from("FormEntryLinks a")
                                 ->where("a.formentryid = ?", $application->getId());
                              $links = $q->execute();
                              $count = 0;
                              foreach($links as $link)
                              {
                                  $count++;
                                  $q = Doctrine_Query::create()
                                     ->from("ApForms a")
                                     ->where("a.form_id = ?", $link->getFormId())
                                     ->limit(1);
                                  $linkedform = $q->fetchOne();
                                  if($linkedform)
                                  {
                                          ?>
                                               <?php
                                                  //Display control buttons that manipulate the application
                                                  include_partial('viewformlinks', array('link' => $link, 'count' => $count));
                                                ?>
                                          <?php
                                  }
                              }
                              ?>
                              </div>
                                    <?php
                        }
                        ?>
                        </div>
                        <?php } ?>
                        <?php  if($current_tab == "attachments"){ ?>
			              <div class="tab-pane <?php if($current_tab == "attachments"){ echo "active"; } ?>" id="ptab1">
    									<?php
    		                if($application->getParentSubmission() == "0")
    		                {
    		                ?>

    				            <?php
    										}
    		                else
    		                {
    		                ?>
    			           		<h4><strong style="text-transform:uppercase;"><?php echo __('Attachments'); ?></strong></h4>
                          <?php
    										}
    										?>
    			              <?php
      				                //Display control buttons that manipulate the application
      				                include_partial('viewattachments', array('application' => $application, 'choosen_form' => $choosen_form));
    				             ?>
    			            </div>
                      <?php } ?>
                        <?php  if($current_tab == "user"){  ?>
        			          <div class="tab-pane active" id="ptab2">
        			            <form class="form-bordered">
        			            	<?php
                               //Display information about the user that submitted this application
                               //include_partial('viewclient', array('application' => $application));//Check time for loading of library scripts
                            ?>
        			            </form>
        			          </div>
                        <?php } ?>
                        <?php  if($current_tab == "review"){ ?>
        			          <div class="tab-pane active" id="ptab3">
        			            <form class="form-bordered">
        			            <?php
        			            	   //Display comments submitted by reviewers #may eventually decide to merge this with the _viewreviewers if i manage to seperate individual reviewer comments
        			             	   include_partial('viewcomments', array('application' => $application));//Check time for loading of library scripts
        			            ?>
        			            </form>
        			          </div>
                        <?php } ?>
                        <?php  if($current_tab == "billing"){ ?>
			                 <div class="tab-pane active" id="ptab4">
			                 <?php  if($application->getMfInvoice()){ ?>
                        <form class="form-bordered">
                        <?php
                                //Displays any information attached to this application
                                include_partial('viewinvoices', array('application' => $application));
                        ?>
                        </form>
                        <?php } ?>
          		          </div>
                        <?php } ?>
                        <?php  if($current_tab == "history"){ ?>
                             <div class="tab-pane active" id="ptab5">
                            <?php
                                //Display scanned files attached on the comment sheet using a scanning task
                                include_partial('viewhistory', array('application' => $application,'fromdate' => $fromdate,'fromtime' => $fromtime,'todate' => $todate,'totime' => $totime));//Check time for loading of library scripts
                            ?>
                             </div>
                        <?php } ?>
                        <?php  if($current_tab == "messages"){ ?>
			                  <div class="tab-pane pt20 active" id="ptab6">
      			            <form class="form-bordered">
      			              <?php
                                //Displays a message trail between the client and the reviewers
                                include_partial('viewmessages', array('application' => $application));//Check time for loading of library scripts
      				            ?>
      			            </form>
                        </div>
                        <?php } ?>
                        <?php  if($current_tab == "memo"){ ?>
                        <div class="tab-pane pt20 active" id="ptab7">
                             <b>Messages to Other Reviewers</b>
      			            <form class="form-bordered">
      			              <?php
                                //Displays a message trail between reviewers
                               // include_partial('viewmemos', array('application' => $application));//Check time for loading of library scripts
                               include_partial('viewreviewermessages', array('application' => $application));
                              ?>
                        </form>
                        </div>
                        <?php } ?>

			        	</div><!-- tab-content -->
			      	</div><!-- #basicWizard -->
          	</div>
            </div>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <form class="form" action="<?php echo public_path(); ?>backend.php/tasks/save" method="post" autocomplete="off">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo __('New Task'); ?></h4>
      </div>
      <div class="modal-body modal-body-nopadding" id="newtask">
        Content goes here...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close'); ?></button>
        <button type="submit" class="btn btn-primary"><?php echo __('Assign Task'); ?></button>
      </div>
    </form>
    </div><!-- modal-content -->
  </div><!-- modal-dialog -->
</div><!-- modal -->


<script language="javascript">
	$("#newtask").load("<?php echo public_path(); ?>backend.php/tasks/new/application/<?php echo $application->getId(); ?>");
    <?php
    if($decline_warning == true)
    {
        ?> 
		$('#commentsReviewers').toggle();
		$('#commentsDeclines').toggle();
		alert("You still have an unresolved reason for decline. Please click ok and mark it as resolved in the page that appears."); 
		<?php
    }
	?>
</script>