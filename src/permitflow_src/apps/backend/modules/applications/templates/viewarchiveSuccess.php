<?php
/**
 * view template.
 *
 * Displays a single application and all of its review history
 *
 * @package    backend
 * @subpackage applications
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");

include_partial('dashboard/checksession');

?>
<div class="pageheader">
  <h2><i class="fa fa-envelope"></i> <?php echo __('Applications'); ?></h2>
  <div class="breadcrumb-wrapper">
    <span class="label"><?php echo __('You are here'); ?>:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>backend.php"><?php echo __('Home'); ?></a></li>
      <li><a href="<?php echo public_path(); ?>backend.php/applications/list/get/your"><?php echo __('Applications'); ?></a></li>
    </ol>
  </div>
</div>

<div class="contentpanel">
	<div class="row">
    <div class="col-sm-12">
    <!-- Action Buttons -->
       <?php
        if($sf_user->mfHasCredential("accesssubmenu".$application->getApproved()))
        {
            //Display control buttons that manipulate the application

            //Hide actions if this is an invoicing stage and the application is not paid for
            $q = Doctrine_Query::create()
                ->from('SubMenus a')
                ->where('a.id = ?', $application->getApproved())
                ->limit(1);
            $stage = $q->fetchOne();

            if($stage && $stage->getStageType() == 3) {
                //search for unpaid invoices
                $paid = true;
                $invoices = $application->getMfInvoice();
                foreach($invoices as $invoice)
                {
                    if($invoice->getPaid() != 2 && $invoice->getPaid() != 3)
                    {
                        $paid = false;
                    }
                }

                //if no pending invoices are found then display actions
                if($paid) {
                    include_partial('viewarchivebuttons', array('application' => $application));
                }
            }
            else
            {
                include_partial('viewarchivebuttons', array('application' => $application));
            }
        }
        ?>

    <hr class="mb10 mt0">
    </div><!-- col-sm-12 -->
    <div class="col-sm-3">
   	 <div class="blog-item pt20 pb5">

      <?php
          $q = Doctrine_Query::create()
               ->from('SfGuardUser a')
               ->where('a.id = ?', $application->getUserId());
          $user = $q->fetchOne();

          if(sfConfig::get('app_enable_categories') == "no")
          {
            $account_type = "";

            if($user->getProfile()->getRegisteras() == 1)
            {
              $account_type = "citizen";
            }
            elseif($user->getProfile()->getRegisteras() == 3)
            {
              $account_type = "alien";
            }
            elseif($user->getProfile()->getRegisteras() == 4)
            {
              $account_type = "visitor";
            }

            ?>
            <div class="form-group" align="center">
              <img src="https://account.ecitizen.go.ke/profile-picture/<?php echo $user->getUsername(); ?>?t=<?php echo $account_type; ?>" class="thumbnail img-responsive mb20" alt="" />
            </div>
            <?php
          }
      ?>

    <h5 class="subtitle mt20 ml20"><?php echo __('Application Summary'); ?></h5>

    <ul class="profile-social-list">
     	<li><a href=""><?php
        		$q = Doctrine_Query::create()
        			->from("SubMenus a")
        			->where("a.id = ?", $application->getApproved())
                    ->limit(1);
        		$stage = $q->fetchOne();
        		if($stage)
        		{
        			$q = Doctrine_Query::create()
          			->from("Menus a")
          			->where("a.id = ?", $stage->getMenuId())
                        ->limit(1);
          		$parentstage = $q->fetchOne();
        	?><?php echo $parentstage->getTitle(); ?></a></li><?php
          	}

					$q = Doctrine_Query::create()
						->from("SfGuardUserProfile a")
						->where("a.user_id = ?", $application->getUserId())
                        ->limit(1);
					$architect = $q->fetchOne();
                ?>
            <li><?php echo __('Submitted by'); ?> <a href="/backend.php/frusers/show/id/<?php echo $application->getUserId(); ?>"> <?php echo $architect->getFullname(); ?></a></li>
            <li><?php echo __('Date of Submission'); ?> <a><?php echo str_replace(" ", " @ ", $application->getDateOfSubmission()); ?></a></li>
            <?php
            if($application->getDateOfResponse())
            {
            ?>
            <li><?php echo __('Date of Approval'); ?> <a><?php echo str_replace(" ", " @ ", $application->getDateOfResponse()); ?></a></li>
            <?php
            }
            ?>
           <?php
						function GetDaysSince($sStartDate, $sEndDate){
							$start_ts = strtotime($sStartDate);
							$end_ts = strtotime($sEndDate);
							$diff = $end_ts - $start_ts;
							return round($diff / 86400);
						}

                       $days = 0;
                       if($application->getDateOfResponse())
                       {
                           $days = GetDaysSince($application->getDateOfSubmission(), $application->getDateOfResponse());
                       }
                       else {
                           $days = GetDaysSince($application->getDateOfSubmission(), date("Y-m-d H:i:s"));
                       }

						$days_color = "";
						$maximum_duration = 0;

						//get maximum duration of current stage
						$q = Doctrine_Query::create()
							->from("SubMenus a")
							->where("a.id = ?", $application->getApproved())
                            ->limit(1);
						$current_stage = $q->fetchOne();
						if($current_stage)
						{
							$maximum_duration = $current_stage->getMaxDuration();
						}

						if($days < $maximum_duration || $maximum_duration == 0){
								$days_color = "success";
						}
						elseif($days >= $maximum_duration){
								$days_color = "danger";
						}
					   ?>
            <li><?php echo __('Days in progress'); ?> <span

            class="badge badge-<?php echo $days_color; ?>"

            ><strong>
            <?php
						echo $days." ".__('days');
						?>
            </strong>
            </span>
        </li>
      </ul>

      <ul class="profile-social-list">
        <li><?php echo __('Application Status'); ?><a href=""> <?php if($stage){

            $q = Doctrine_Query::create()
                ->from("Menus a")
                ->where("a.id = ?", $stage->getMenuId())
                ->limit(1);
            $parent_stage = $q->fetchOne();
            if($parent_stage)
            {
                echo "<br>".$parent_stage->getTitle()." &gt; ";
            }

           echo $stage->getTitle();

        }else{ echo "Drafts"; } ?> </a></li>
    </ul>
    <div class="mb10"></div>
			<?php
      if($application->getParentSubmission() == "0")
      {
        ?>
          <h5 class="subtitle ml20"><?php echo __('Revisions'); ?></h5>
        <?php
			}
      ?>

      <ul class="profile-social-list">
      <?php
		   foreach($prevsubmissions as $currentapplication)
			{
		  ?>
            <li><a target="_blank" href="<?php echo public_path(); ?>backend.php/applications/view/id/<?php echo $currentapplication->getId(); ?>"><?php echo $architect->getFullname(); ?> (<?php echo $currentapplication->getDateOfSubmission(); ?>)</a></li>
          <?php
		   }
		   if(sizeof($prevsubmissions) <= 0)
		   {
		   	?>
		   	<li><?php echo __('No Revisions Found'); ?></li>
		   	<?php
		   }
		  ?>
    </ul>
    <div class="mb20"></div>
    </div><!-- blog-item -->
  </div><!-- col-sm-3 -->
  <div class="col-sm-9">

	 <div class="panel panel-dark">
      <div class="panel-heading">
        	<h4 class="panel-title"><?php echo $application->getApplicationId(); ?>
              <span class="badge"><?php

							$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
							mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);


						  if($application->getParentSubmission() != 0)
						  {
							  echo "<strong style=\"font-size:13px\">".__('Revision')."</strong>";
						  }
						  ?></span>
			        </h4>
              <p>
              <?php
              $q = Doctrine_Query::create()
              	->from("ApForms a")
              	->where("a.form_id = ?", $application->getFormId())
                  ->limit(1);
              $form = $q->fetchOne();
              if($form)
              {
	              $name = $form->getFormName();

								$sql = "SELECT * FROM ext_translations WHERE field_id = '".$form->getFormId()."' AND field_name = 'form_name' AND table_class = 'ap_forms' AND locale = '".$sf_user->getCulture()."'";

								$rows = mysql_query($sql, $dbconn);
								if($row = mysql_fetch_assoc($rows))
								{
										$name = $row['trl_content'];
								}
									echo $name;
              }
              ?>
    			</p>
    </div>

    <div class="panel-body panel-body-nopadding">
    	<?php
      	 if($sf_user->hasFlash('notice'))
      	 {
      	?>
      	<div class="alert alert-success">
      		<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
      		<?php echo $sf_user->getFlash('notice',ESC_RAW); ?>.
      	</div>
      	<?php
      	 }
      	?>

				<!-- BASIC WIZARD -->
			  <div id="basicWizard" class="basic-wizard">

			     <ul class="nav nav-pills nav-justified">
		       <?php
            if($application->getParentSubmission() == "0")
            {
              ?>
        			<li class="<?php if($current_tab == "application"){ echo "active"; } ?>"><a href="/backend.php/applications/viewarchive/id/<?php echo $application->getId(); ?>/current_tab/application"><?php echo __('Application Details'); ?></a></li>
              
        			<li class="<?php if($current_tab == "attachments"){ echo "active"; } ?>"><a href="/backend.php/applications/viewarchive/id/<?php echo $application->getId(); ?>/current_tab/attachments"><?php echo __('Attachments'); ?></a></li>
              <?php
						}
            else
            {
                ?>
	          		<li style="display: none;"><a href="/backend.php/applications/viewarchive/id/<?php echo $application->getId(); ?>?current_tab=application" data-toggle="tab"><?php echo __('Application Details'); ?></a></li>
                <?php
						}
						?>
           <?php
            $q = Doctrine_Query::create()
               ->from("FormEntryLinks a")
               ->where("a.formentryid = ?", $application->getId());
            $links = $q->execute();
            if(sizeof($links) > 0)
            {
            	?>
                  <li><a href="#ptabadd" data-toggle="tab">Additional Details</a></li>
                  <?php
            }

						if($application->getParentSubmission() == "0")
						{
							?>
		            <li <?php if($current_tab == "user"){ echo "class='active'"; } ?>><a href="/backend.php/applications/viewarchive/id/<?php echo $application->getId(); ?>/current_tab/user"><?php echo __("User's Details"); ?></a></li>
		            <li <?php if($current_tab == "review"){ echo "class='active'"; } ?>><a href="/backend.php/applications/viewarchive/id/<?php echo $application->getId(); ?>/current_tab/review"><?php echo __('Review History'); ?></a></li>
		            <li <?php if($current_tab == "billing"){ echo "class='active'"; } ?>><a href="/backend.php/applications/viewarchive/id/<?php echo $application->getId(); ?>/current_tab/billing"><?php echo __('Billing History'); ?></a></li>
		            <li <?php if($current_tab == "history"){ echo "class='active'"; } ?>><a href="/backend.php/applications/viewarchive/id/<?php echo $application->getId(); ?>/current_tab/history"><?php echo __('Application History'); ?></a></li>
                <li <?php if($current_tab == "messages"){ echo "class='active'"; } ?>><a href="/backend.php/applications/viewarchive/id/<?php echo $application->getId(); ?>/current_tab/messages"><?php echo __('Messages'); ?></a></li>
                <li <?php if($current_tab == "memo"){ echo "class='active'"; } ?>><a href="/backend.php/applications/viewarchive/id/<?php echo $application->getId(); ?>/current_tab/memo"><?php echo __('Memo'); ?></a></li>
             <?php
						}
						?>
			        </ul>
			        <div class="tab-content tab-content-nopadding">
                  <?php  if($current_tab == "application"){ ?>
			              <div class="tab-pane <?php if($current_tab == "application"){ echo "active"; } ?>" id="ptab1">
    									<?php
    		                if($application->getParentSubmission() == "0")
    		                {
    		                ?>

    				            <?php
    										}
    		                else
    		                {
    		                ?>
    			           		<h4><strong style="text-transform:uppercase;"><?php echo __('Application details'); ?></strong></h4>
                          <?php
    										}
    										?>
    			              <?php
      				                //Display control buttons that manipulate the application
      				                include_partial('viewarchivedetails', array('application' => $application, 'choosen_form' => $choosen_form));
    				             ?>
    			            </div>
                        <?php } ?>
                        <?php  if($current_tab == "attachments"){ ?>
			              <div class="tab-pane <?php if($current_tab == "attachments"){ echo "active"; } ?>" id="ptab1">
    									<?php
    		                if($application->getParentSubmission() == "0")
    		                {
    		                ?>

    				            <?php
    										}
    		                else
    		                {
    		                ?>
    			           		<h4><strong style="text-transform:uppercase;"><?php echo __('Attachments'); ?></strong></h4>
                          <?php
    										}
    										?>
    			              <?php
      				                //Display control buttons that manipulate the application
      				                include_partial('viewarchiveattachments', array('application' => $application, 'choosen_form' => $choosen_form));
    				             ?>
    			            </div>
                      <?php } ?>
                        <?php  if($current_tab == "user"){  ?>
        			          <div class="tab-pane active" id="ptab2">
        			            <form class="form-bordered">
        			            	<?php
                               //Display information about the user that submitted this application
                               include_partial('viewclient', array('application' => $application));//Check time for loading of library scripts
                            ?>
        			            </form>
        			          </div>
                        <?php } ?>
                        <?php  if($current_tab == "review"){ ?>
        			          <div class="tab-pane active" id="ptab3">
        			            <form class="form-bordered">
        			            <?php
        			            	   //Display comments submitted by reviewers #may eventually decide to merge this with the _viewreviewers if i manage to seperate individual reviewer comments
        			             	   include_partial('viewcomments', array('application' => $application));//Check time for loading of library scripts
        			            ?>
        			            </form>
        			          </div>
                        <?php } ?>
                        <?php  if($current_tab == "billing"){ ?>
			                 <div class="tab-pane active" id="ptab4">
			                 <?php  if($application->getMfInvoiceArchive()){ ?>
                        <form class="form-bordered">
                        <?php
                                //Displays any information attached to this application
                                include_partial('viewarchiveinvoices', array('application' => $application));
                        ?>
                        </form>
                        <?php } ?>
          		          </div>
                        <?php } ?>
                        <?php  if($current_tab == "history"){ ?>
                             <div class="tab-pane active" id="ptab5">
                            <?php
                                //Display scanned files attached on the comment sheet using a scanning task
                                include_partial('viewhistory', array('application' => $application,'fromdate' => $fromdate,'fromtime' => $fromtime,'todate' => $todate,'totime' => $totime));//Check time for loading of library scripts
                            ?>
                             </div>
                        <?php } ?>
                        <?php  if($current_tab == "messages"){ ?>
			                  <div class="tab-pane pt20 active" id="ptab6">
      			            <form class="form-bordered">
      			              <?php
                                //Displays a message trail between the client and the reviewers
                                include_partial('viewmessages', array('application' => $application));//Check time for loading of library scripts
      				            ?>
      			            </form>
                        </div>
                        <?php } ?>
                        <?php  if($current_tab == "memo"){ ?>
                        <div class="tab-pane pt20 active" id="ptab7">
      			            <form class="form-bordered">
      			              <?php
                                //Displays a message trail between reviewers
                                include_partial('viewmemos', array('application' => $application));//Check time for loading of library scripts
                           ?>
                        </form>
                        </div>
                        <?php } ?>

			        	</div><!-- tab-content -->
			      	</div><!-- #basicWizard -->
          	</div>
            </div>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <form class="form" action="<?php echo public_path(); ?>backend.php/tasks/save" method="post" autocomplete="off">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo __('New Task'); ?></h4>
      </div>
      <div class="modal-body modal-body-nopadding" id="newtask">
        Content goes here...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close'); ?></button>
        <button type="submit" class="btn btn-primary"><?php echo __('Save changes'); ?></button>
      </div>
    </form>
    </div><!-- modal-content -->
  </div><!-- modal-dialog -->
</div><!-- modal -->


<script language="javascript">
	$("#newtask").load("<?php echo public_path(); ?>backend.php/tasks/new/application/<?php echo $application->getId(); ?>");
</script>