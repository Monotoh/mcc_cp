<?php

/**
 * banner actions.
 *
 * @package    permit
 * @subpackage banner
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class bannerActions extends sfActions
{
   public function executeBatch(sfWebRequest $request)
  {

        $selectedItems = $request->getPostParameter('sf_admin_batch_selection', array());
		try
		{
		  foreach($selectedItems as $item)
		  {
			  $content = Doctrine_Core::getTable('Banner')->find($item);
			  if($content)
			  {
				  $content->delete();
			  }
		  }
		}
		catch (PropelException $e)
		{

		}

        return $this->redirect('/backend.php/banner/index');

  }


  public function executeIndex(sfWebRequest $request)
  {
    $q = Doctrine_Query::create()
       ->from('Banner a')
	   ->orderBy('a.id DESC');
     $this->pager = new sfDoctrinePager('Banner', 20);
	 $this->pager->setQuery($q);
	 $this->pager->setPage($request->getParameter('page', 1));
	 $this->pager->init();


	$this->setLayout("layout-settings");
  }

  public function executeShow(sfWebRequest $request)
  {
    $this->banner = Doctrine_Core::getTable('Banner')->find(array($request->getParameter('id')));
    $this->forward404Unless($this->banner);
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new BannerForm();
  	$this->setLayout("layout-settings");
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new BannerForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($banner = Doctrine_Core::getTable('Banner')->find(array($request->getParameter('id'))), sprintf('Object banner does not exist (%s).', $request->getParameter('id')));
    $this->form = new BannerForm($banner);
	$this->setLayout("layout-settings");
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($banner = Doctrine_Core::getTable('Banner')->find(array($request->getParameter('id'))), sprintf('Object banner does not exist (%s).', $request->getParameter('id')));
    $this->form = new BannerForm($banner);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {

    $this->forward404Unless($banner = Doctrine_Core::getTable('Banner')->find(array($request->getParameter('id'))), sprintf('Object banner does not exist (%s).', $request->getParameter('id')));


      $audit = new Audit();
      $audit->saveAudit("", "deleted banner image of id ".$banner->getId());

    $banner->delete();

    $this->redirect('/backend.php/banner/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {

      $banner = $form->save();

      $audit = new Audit();
      $audit->saveAudit("", "<a href=\"/backend.php/banner/edit?id=".$banner->getId()."\">updated banner image</a>");

      $this->redirect('/backend.php/banner/index');
    }
  }
}
