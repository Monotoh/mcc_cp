<?php
use_helper("I18N");
?>

<div class="panel panel-dark">
<div class="panel-heading">
  <h3 class="panel-title"><?php echo ($form->getObject()->isNew()?__('New Banner'):__('Edit Banner')); ?></div>
  <form id="bannerform" class="form-bordered form-horizontal" action="/backend.php/banner/<?php echo ($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '') ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>   autocomplete="off" data-ajax="false">

<div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
  <strong><?php echo __('Well done'); ?>!</strong> <?php echo __('You successfully updated this record'); ?></a>.
</div>


<div class="panel-body panel-body-nopadding">

<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
<?php echo $form->renderGlobalErrors() ?>
      <?php if(isset($form['_csrf_token'])): ?>
      <?php echo $form['_csrf_token']->render(); ?>
      <?php endif; ?>

        <div class="form-group">
        <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Title'); ?></i></label>
         <div class="col-sm-8 rogue-input">
          <?php echo $form['title']->renderError() ?>
          <?php echo $form['title'] ?>
        </div>
      </div>

			 <div class="form-group">
       <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Description'); ?></i></label>
        <div class="col-sm-8 rogue-input">
         <?php echo $form['description']->renderError() ?>
         <?php echo $form['description'] ?>
       </div>
     </div>

        <div class="form-group">
          <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Upload Image'); ?></i></label>
           <div class="col-sm-8">
            <?php echo $form['image']->renderError() ?>
            <?php echo $form['image'] ?>
          </div>
        </div>


  </div><!--panel-body-->

    <div class="panel-footer">
  <button class="btn btn-danger mr10"><?php echo __('Reset'); ?></button><button id="submitbuttonname" type="submit" class="btn btn-primary" name="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button>
  </div>
</form>
</div><!--panel-body-->
</div>



<script>
jQuery(document).ready(function(){

  jQuery( "#submitform" ).click(function() {
    jQuery("#bannerform").submit();
  });

});
</script>
