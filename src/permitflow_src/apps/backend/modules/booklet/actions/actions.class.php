<?php

/**
 * booklet actions.
 *
 * @package    symfony
 * @subpackage booklet
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class bookletActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $q = Doctrine_Core::getTable('Booklet')
      ->createQuery('a')
      ->execute();
    $this->results = $q ;
    $this->setLayout('layout-settings');
            
    
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new BookletForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new BookletForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($booklet = Doctrine_Core::getTable('Booklet')->find(array($request->getParameter('id'))), sprintf('Object booklet does not exist (%s).', $request->getParameter('id')));
    $this->form = new BookletForm($booklet);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($booklet = Doctrine_Core::getTable('Booklet')->find(array($request->getParameter('id'))), sprintf('Object booklet does not exist (%s).', $request->getParameter('id')));
    $this->form = new BookletForm($booklet);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    //$request->checkCSRFProtection();

    $this->forward404Unless($booklet = Doctrine_Core::getTable('Booklet')->find(array($request->getParameter('id'))), sprintf('Object booklet does not exist (%s).', $request->getParameter('id')));
    $booklet->delete();

   return $this->redirect("/backend.php/booklet/index");
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $booklet = $form->save();

      $this->redirect("/backend.php/booklet/index");
    }
  }
}
