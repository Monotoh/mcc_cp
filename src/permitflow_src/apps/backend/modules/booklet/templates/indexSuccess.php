<?php use_helper('I18N', 'Date') ?>
<?php
$audit = new Audit();
$audit->saveAudit("", "Accessed Booklet settings");

if($sf_user->mfHasCredential("managewebpages"))
{
  $_SESSION['current_module'] = "booklet";
  $_SESSION['current_action'] = "index";
  $_SESSION['current_id'] = "";
?>
<div class="contentpanel">
<div class="panel panel-dark">

<div class="panel-heading">
    <h3 class="panel-title"><?php echo __('Booklet'); ?></h3>

<div style="float:right;">
 <a class="btn btn-primary-alt settings-margin42" id="newbanner" href="<?php echo public_path(); ?>backend.php/booklet/new"><?php echo __('New Booklet'); ?></a>
</div>
</div>



<div id='notifications' name='notifications'>
</div>
<div class="table-responsive">
<table class="table dt-on-steroids mb0" id="table3">
  <thead>
   <tr>
      <th style="width: 10px;"><input type='checkbox' name='batchall' onclick="boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.name == 'batch') { if(this.checked == true){ box.checked = false; }else{ box.checked = true; } } } "></th>
      
      <th>Title</th>
      <th style="background:none;">Content</th>
      <th style="background:none;">Actions</th>
    </tr>
  </thead>
    <tbody>
    <?php foreach ($results as $booklet): ?>
    <tr id="row_<?php echo $booklet->getId() ?>">
	  <td><input type='checkbox' name='batch' id='batch_<?php echo $booklet->getId() ?>' value='<?php echo $booklet->getId() ?>'></td>
      <td><?php echo $booklet->getTitle() ?></td>
       <td><?php echo $booklet->getContent() ?></td>
      <td>
		<a id="editbanner<?php echo $booklet->getId(); ?>" href="<?php echo public_path(); ?>backend.php/booklet/edit/id/<?php echo $booklet->getId(); ?>" title="<?php echo __('Edit'); ?>"><span class="badge badge-primary"><i class="fa fa-pencil"></i></span></a>
    <a id="deletebanner<?php echo $booklet->getId(); ?>" onClick="if(confirm('Are you sure you want to delete this item?')){ return true; }else{ return false; }" href="<?php echo public_path(); ?>backend.php/booklet/delete/id/<?php echo $booklet->getId(); ?>" title="<?php echo __('Delete'); ?>"><span class="badge badge-primary"><i class="fa fa-trash-o"></i></span></a>
  </td>
    </tr>
    <?php endforeach; ?>
    </tbody>
<tfoot>
 <tr><td colspan='8' style='text-align: left;'>
 <select id='batch_action' name='batch_action' onChange="if(this.value != ''){if(confirm('Are you sure?')){ batch('booklet', this.options[this.selectedIndex].text, this.value); document.getElementById('default').selected='selected'; }}">
 <option id='default' value=''><?php echo __('Choose an action'); ?>..</option>
 <option value='delete'><?php echo __('Set As Deleted'); ?></option>
 </select>
 </td></tr>
 </tfoot>
</table>
</div>
</div>
</div>
<script language='javascript'>
jQuery('#table3').dataTable({
    "sPaginationType": "full_numbers",

    // Using aoColumnDefs
    "aoColumnDefs": [
      { "bSortable": false, "aTargets": [ 'no-sort' ] }
    ]
  });
</script>

<?php
}
else
{
  include_partial("settings/accessdenied");
}
?>
