<?php

/**
 * conditions actions.
 *
 * @package    permit
 * @subpackage conditions
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class conditionsActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $this->approval_conditions = Doctrine_Core::getTable('ApprovalCondition')
      ->createQuery('a')
      ->execute();
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new ApprovalConditionForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new ApprovalConditionForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($approval_condition = Doctrine_Core::getTable('ApprovalCondition')->find(array($request->getParameter('id'))), sprintf('Object approval_condition does not exist (%s).', $request->getParameter('id')));
    $this->form = new ApprovalConditionForm($approval_condition);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($approval_condition = Doctrine_Core::getTable('ApprovalCondition')->find(array($request->getParameter('id'))), sprintf('Object approval_condition does not exist (%s).', $request->getParameter('id')));
    $this->form = new ApprovalConditionForm($approval_condition);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($approval_condition = Doctrine_Core::getTable('ApprovalCondition')->find(array($request->getParameter('id'))), sprintf('Object approval_condition does not exist (%s).', $request->getParameter('id')));
    $approval_condition->delete();

    $this->redirect('conditions/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $approval_condition = $form->save();

      $this->redirect('conditions/edit?id='.$approval_condition->getId());
    }
  }
}
