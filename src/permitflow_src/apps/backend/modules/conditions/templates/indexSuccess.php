<h1>Approval conditions List</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Entry</th>
      <th>Condition</th>
      <th>Created at</th>
      <th>Updated at</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($approval_conditions as $approval_condition): ?>
    <tr>
      <td><a href="<?php echo url_for('conditions/edit?id='.$approval_condition->getId()) ?>"><?php echo $approval_condition->getId() ?></a></td>
      <td><?php echo $approval_condition->getEntryId() ?></td>
      <td><?php echo $approval_condition->getConditionId() ?></td>
      <td><?php echo $approval_condition->getCreatedAt() ?></td>
      <td><?php echo $approval_condition->getUpdatedAt() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

  <a href="<?php echo url_for('conditions/new') ?>">New</a>
