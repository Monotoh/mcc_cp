<?php

/**
 * content actions.
 *
 * @package    permit
 * @subpackage content
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class contentActions extends sfActions
{

  public function executeBatch(sfWebRequest $request)
  {
		if($request->getPostParameter('delete'))
		{
			$item = Doctrine_Core::getTable('Content')->find(array($request->getPostParameter('delete')));
			if($item)
			{
				$item->delete();
			}
		}
    }

    public function executeChangelogo(sfWebRequest $request)
    {
      $form = new ApSettingsForm();
      $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
      if ($form->isValid())
      {

        $logo = $form->save();

        $audit = new Audit();
        $audit->saveAudit("", "<a href=\"/backend.php/settings/content\">Updated Agency Logo</a>");

        $this->redirect('/backend.php/settings/content');
      }
      else
      {
        $this->redirect('/backend.php/settings/content?error=logo');
      }
    }

	/**
	 * Executes 'Checkname' action
	 *
	 * Ajax used to check existence of name
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeCheckname(sfWebRequest $request)
	{
	    // add new user
	    $q = Doctrine_Query::create()
	       ->from("Content a")
	       ->where('a.menu_title = ?', $request->getPostParameter('name'));
	    $existinggroup = $q->execute();
	    if(sizeof($existinggroup) > 0)
	    {
	          echo '<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Title is already in use!</strong></div>';
	          exit;
	    }
	    else
	    {
	          echo '<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Title is available!</strong></div>';
	          exit;
	    }
	}


  public function executeIndex(sfWebRequest $request)
  {
	if($request->getParameter("ptoggle"))
	 {
		 $content = Doctrine_Core::getTable('Content')->find(array($request->getParameter('ptoggle')));
		 if($content->getPublished() == "1")
		 {
			 $content->setPublished("0");
		 }
		 else
		 {
			 $content->setPublished("1");
		 }
		 $content->save();
	 }

	 if($request->getParameter("ltoggle"))
	 {
		 $content = Doctrine_Core::getTable('Content')->find(array($request->getParameter('ltoggle')));
		 if($content->getVisibility() == "1")
		 {
			 $content->setVisibility("0");
		 }
		 else
		 {
			 $content->setVisibility("1");
		 }
		 $content->save();
	 }

    $q = Doctrine_Query::create()
       ->from('Content a')
	   ->orderBy('a.menu_index ASC');
     $this->contents = $q->execute();

     $this->form = new ApSettingsForm();

	 $this->setLayout("layout-settings");

  }

  /**
   * Push order up.
   *
   * @return Response
   */
  public function executeOrderup(sfWebRequest $request)
  {
    $this->forward404Unless($content = Doctrine_Core::getTable('Content')->find(array($request->getParameter('id'))), sprintf('Object menus does not exist (%s).', $request->getParameter('id')));

    $q = Doctrine_Query::create()
         ->from("Content a")
         ->where("a.menu_index < ?", $content->getMenuIndex())
         ->orderBy('a.menu_index DESC');
    $previous_content = $q->fetchOne();

    if($previous_content)
    {
      $current_order = $content->getMenuIndex();
      $previous_order = $previous_content->getMenuIndex();

      $previous_content->setMenuIndex(-1); //temporary set page order to prevent conflict
      $previous_content->save();

      $content->setMenuIndex($previous_order);
      $content->save();

      $previous_content->setMenuIndex($current_order);
      $previous_content->save();
    }

    return $this->redirect("/backend.php/content/index");
  }

  /**
   * Push order up.
   *
   * @return Response
   */
  public function executeOrderdown(sfWebRequest $request)
  {
    $this->forward404Unless($content = Doctrine_Core::getTable('Content')->find(array($request->getParameter('id'))), sprintf('Object menus does not exist (%s).', $request->getParameter('id')));

    $q = Doctrine_Query::create()
         ->from("Content a")
         ->where("a.menu_index > ?", $content->getMenuIndex())
         ->orderBy('a.menu_index ASC');
    $previous_content = $q->fetchOne();

    if($previous_content)
    {
      $current_order = $content->getMenuIndex();
      $previous_order = $previous_content->getMenuIndex();

      $previous_content->setMenuIndex(-1); //temporary set page order to prevent conflict
      $previous_content->save();

      $content->setMenuIndex($previous_order);
      $content->save();

      $previous_content->setMenuIndex($current_order);
      $previous_content->save();
    }

    return $this->redirect("/backend.php/content/index");
  }

  public function executeShow(sfWebRequest $request)
  {
    $this->content = Doctrine_Core::getTable('Content')->find(array($request->getParameter('id')));
    $this->forward404Unless($this->content);
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new ContentForm();
	$this->setLayout("layout-settings");
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

  	$content = new Content();
    $content->setTopArticle($request->getPostParameter('top_article_txt'));
  	$content->setMenuTitle($request->getPostParameter('menu_title'));
  	$content->setLastModifiedOn(date('Y-m-d'));
  	$content->setPublished($request->getPostParameter('published'));
  	$content->setParentId($request->getPostParameter('parent'));
  	$content->setVisibility($request->getPostParameter('visibility'));
  	$content->setUrl($request->getPostParameter('url'));
  	$content->setCreatedOn(date("Y-m-d"));
        //OTB patch
        $content->setContentType($request->getParameter('content_content_type'));
  	$content->setDeleted(0);
  	$content->save();


  	$content->setMenuIndex($content->getId());
  	$content->save();

	if($this->getUser()->getCulture())
	{
		$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
		mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

		//Set Menu Title
		$sql = "SELECT * FROM ext_translations WHERE field_id = '".$content->getId()."' AND field_name = 'menu_title' AND table_class = 'content' AND locale = '".$this->getUser()->getCulture()."'";
		$rows = mysql_query($sql);
		if($row = mysql_fetch_assoc($rows))
		{
			$sql = "UPDATE ext_translations SET trl_content = '".mysql_real_escape_string($request->getPostParameter('menu_title'))."' WHERE locale = '".$this->getUser()->getCulture()."' AND table_class='content' AND field_name='menu_title' AND field_id = ".$content->getId();
			mysql_query($sql);
		}
		else
		{
			$sql = "INSERT INTO ext_translations(trl_content, locale, table_class, field_name, field_id) VALUES('".mysql_real_escape_string($request->getPostParameter('menu_title'))."', '".$this->getUser()->getCulture()."', 'content', 'menu_title', '".$content->getId()."')";
			mysql_query($sql);
		}

		//Set Breadcrumb Title
		$sql = "SELECT * FROM ext_translations WHERE field_id = '".$content->getId()."' AND field_name = 'menu_title' AND table_class = 'content' AND locale = '".$this->getUser()->getCulture()."'";
		$rows = mysql_query($sql);
		if($row = mysql_fetch_assoc($rows))
		{
			$sql = "UPDATE ext_translations SET trl_content = '".mysql_real_escape_string($request->getPostParameter('breadcrumb_title'))."' WHERE locale = '".$this->getUser()->getCulture()."' AND table_class='content' AND field_name='breadcrumb_title' AND field_id = ".$content->getId();
			mysql_query($sql);
		}
		else
		{
			$sql = "INSERT INTO ext_translations(trl_content, locale, table_class, field_name, field_id) VALUES('".mysql_real_escape_string($request->getPostParameter('breadcrumb_title'))."', '".$this->getUser()->getCulture()."', 'content', 'breadcrumb_title', '".$content->getId()."')";
			mysql_query($sql);
		}

		//Set Top Article
		$sql = "SELECT * FROM ext_translations WHERE field_id = '".$content->getId()."' AND field_name = 'top_article' AND table_class = 'content' AND locale = '".$this->getUser()->getCulture()."'";
		$rows = mysql_query($sql);
		if($row = mysql_fetch_assoc($rows))
		{
			$sql = "UPDATE ext_translations SET trl_content = '".mysql_real_escape_string($request->getPostParameter('top_article_txt'))."' WHERE locale = '".$this->getUser()->getCulture()."' AND table_class='content' AND field_name='top_article' AND field_id = ".$content->getId();
			mysql_query($sql);
		}
		else
		{
			$sql = "INSERT INTO ext_translations(trl_content, locale, table_class, field_name, field_id) VALUES('".mysql_real_escape_string($request->getPostParameter('top_article_txt'))."', '".$this->getUser()->getCulture()."', 'content', 'top_article', '".$content->getId()."')";
			mysql_query($sql);
		}

		//Set Top Sidebar Article
		$sql = "SELECT * FROM ext_translations WHERE field_id = '".$content->getId()."' AND field_name = 'top_sidebar' AND table_class = 'content' AND locale = '".$this->getUser()->getCulture()."'";
		$rows = mysql_query($sql);
		if($row = mysql_fetch_assoc($rows))
		{
			$sql = "UPDATE ext_translations SET trl_content = '".mysql_real_escape_string($request->getPostParameter('top_sidebar_txt'))."' WHERE locale = '".$this->getUser()->getCulture()."' AND table_class='content' AND field_name='top_sidebar' AND field_id = ".$content->getId();
			mysql_query($sql);
		}
		else
		{
			$sql = "INSERT INTO ext_translations(trl_content, locale, table_class, field_name, field_id) VALUES('".mysql_real_escape_string($request->getPostParameter('top_sidebar_txt'))."', '".$this->getUser()->getCulture()."', 'content', 'top_sidebar', '".$content->getId()."')";
			mysql_query($sql);
		}

		//Set Bottom Article
		$sql = "SELECT * FROM ext_translations WHERE field_id = '".$content->getId()."' AND field_name = 'bottom_article' AND table_class = 'content' AND locale = '".$this->getUser()->getCulture()."'";
		$rows = mysql_query($sql);
		if($row = mysql_fetch_assoc($rows))
		{
			$sql = "UPDATE ext_translations SET trl_content = '".mysql_real_escape_string($request->getPostParameter('top_bottom_txt'))."' WHERE locale = '".$this->getUser()->getCulture()."' AND table_class='content' AND field_name='bottom_article' AND field_id = ".$content->getId();
			mysql_query($sql);
		}
		else
		{
			$sql = "INSERT INTO ext_translations(trl_content, locale, table_class, field_name, field_id) VALUES('".mysql_real_escape_string($request->getPostParameter('top_bottom_txt'))."', '".$this->getUser()->getCulture()."', 'content', 'bottom_article', '".$content->getId()."')";
			mysql_query($sql);
		}

		//Set Bottom Sidebar Article
		$sql = "SELECT * FROM ext_translations WHERE field_id = '".$content->getId()."' AND field_name = 'bottom_sidebar' AND table_class = 'content' AND locale = '".$this->getUser()->getCulture()."'";
		$rows = mysql_query($sql);
		if($row = mysql_fetch_assoc($rows))
		{
			$sql = "UPDATE ext_translations SET trl_content = '".mysql_real_escape_string($request->getPostParameter('bottom_sidebar_txt'))."' WHERE locale = '".$this->getUser()->getCulture()."' AND table_class='content' AND field_name='bottom_sidebar' AND field_id = ".$content->getId();
			mysql_query($sql);
		}
		else
		{
			$sql = "INSERT INTO ext_translations(trl_content, locale, table_class, field_name, field_id) VALUES('".mysql_real_escape_string($request->getPostParameter('bottom_sidebar_txt'))."', '".$this->getUser()->getCulture()."', 'content', 'bottom_sidebar', '".$content->getId()."')";
			mysql_query($sql);
		}


		//Set Custom Footer
		$sql = "SELECT * FROM ext_translations WHERE field_id = '".$content->getId()."' AND field_name = 'custom_footer' AND table_class = 'content' AND locale = '".$this->getUser()->getCulture()."'";
		$rows = mysql_query($sql);
		if($row = mysql_fetch_assoc($rows))
		{
			$sql = "UPDATE ext_translations SET trl_content = '".mysql_real_escape_string($request->getPostParameter('custom_footer_txt'))."' WHERE locale = '".$this->getUser()->getCulture()."' AND table_class='content' AND field_name='custom_footer' AND field_id = ".$content->getId();
			mysql_query($sql);
		}
		else
		{
			$sql = "INSERT INTO ext_translations(trl_content, locale, table_class, field_name, field_id) VALUES('".mysql_real_escape_string($request->getPostParameter('custom_footer_txt'))."', '".$this->getUser()->getCulture()."', 'content', 'custom_footer', '".$content->getId()."')";
			mysql_query($sql);
		}
	}

    $this->redirect('/backend.php/content/index');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($content = Doctrine_Core::getTable('Content')->find(array($request->getParameter('id'))), sprintf('Object content does not exist (%s).', $request->getParameter('id')));
    $this->form = new ContentForm($content);
	$this->content = $content;
	$this->setLayout("layout-settings");
  }

  public function executeUpdateorder(sfWebRequest $request)
  {
            $this->forward404Unless($content = Doctrine_Core::getTable('Content')->find(array($request->getPostParameter('pageid'))), sprintf('Object content does not exist (%s).', $request->getPostParameter('pageid')));
            $content->setMenuIndex($request->getPostParameter('order'));
            $content->save();
            return true;
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($content = Doctrine_Core::getTable('Content')->find(array($request->getParameter('id'))), sprintf('Object content does not exist (%s).', $request->getParameter('id')));

	if($this->getUser()->getCulture())
	{
		$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
		mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

		//Set Menu Title
		$sql = "SELECT * FROM ext_translations WHERE field_id = '".$content->getId()."' AND field_name = 'menu_title' AND table_class = 'content' AND locale = '".$this->getUser()->getCulture()."'";
		$rows = mysql_query($sql);
		if($row = mysql_fetch_assoc($rows))
		{
			$sql = "UPDATE ext_translations SET trl_content = '".$request->getPostParameter('menu_title')."' WHERE locale = '".$this->getUser()->getCulture()."' AND table_class='content' AND field_name='menu_title' AND field_id = ".$content->getId();
			mysql_query($sql);
		}
		else
		{
			$sql = "INSERT INTO ext_translations(trl_content, locale, table_class, field_name, field_id) VALUES('".$request->getPostParameter('menu_title')."', '".$this->getUser()->getCulture()."', 'content', 'menu_title', '".$content->getId()."')";
			mysql_query($sql);
		}

		//Set Breadcrumb Title
		$sql = "SELECT * FROM ext_translations WHERE field_id = '".$content->getId()."' AND field_name = 'menu_title' AND table_class = 'content' AND locale = '".$this->getUser()->getCulture()."'";
		$rows = mysql_query($sql);
		if($row = mysql_fetch_assoc($rows))
		{
			$sql = "UPDATE ext_translations SET trl_content = '".$request->getPostParameter('breadcrumb_title')."' WHERE locale = '".$this->getUser()->getCulture()."' AND table_class='content' AND field_name='breadcrumb_title' AND field_id = ".$content->getId();
			mysql_query($sql);
		}
		else
		{
			$sql = "INSERT INTO ext_translations(trl_content, locale, table_class, field_name, field_id) VALUES('".$request->getPostParameter('breadcrumb_title')."', '".$this->getUser()->getCulture()."', 'content', 'breadcrumb_title', '".$content->getId()."')";
			mysql_query($sql);
		}

		//Set Top Article
		$sql = "SELECT * FROM ext_translations WHERE field_id = '".$content->getId()."' AND field_name = 'top_article' AND table_class = 'content' AND locale = '".$this->getUser()->getCulture()."'";
		$rows = mysql_query($sql);
		if($row = mysql_fetch_assoc($rows))
		{
			$sql = "UPDATE ext_translations SET trl_content = '".$request->getPostParameter('top_article_txt')."' WHERE locale = '".$this->getUser()->getCulture()."' AND table_class='content' AND field_name='top_article' AND field_id = ".$content->getId();
			mysql_query($sql);
		}
		else
		{
			$sql = "INSERT INTO ext_translations(trl_content, locale, table_class, field_name, field_id) VALUES('".$request->getPostParameter('top_article_txt')."', '".$this->getUser()->getCulture()."', 'content', 'top_article', '".$content->getId()."')";
			mysql_query($sql);
		}

		//Set Top Sidebar Article
		$sql = "SELECT * FROM ext_translations WHERE field_id = '".$content->getId()."' AND field_name = 'top_sidebar' AND table_class = 'content' AND locale = '".$this->getUser()->getCulture()."'";
		$rows = mysql_query($sql);
		if($row = mysql_fetch_assoc($rows))
		{
			$sql = "UPDATE ext_translations SET trl_content = '".$request->getPostParameter('top_sidebar_txt')."' WHERE locale = '".$this->getUser()->getCulture()."' AND table_class='content' AND field_name='top_sidebar' AND field_id = ".$content->getId();
			mysql_query($sql);
		}
		else
		{
			$sql = "INSERT INTO ext_translations(trl_content, locale, table_class, field_name, field_id) VALUES('".$request->getPostParameter('top_sidebar_txt')."', '".$this->getUser()->getCulture()."', 'content', 'top_sidebar', '".$content->getId()."')";
			mysql_query($sql);
		}

		//Set Bottom Article
		$sql = "SELECT * FROM ext_translations WHERE field_id = '".$content->getId()."' AND field_name = 'bottom_article' AND table_class = 'content' AND locale = '".$this->getUser()->getCulture()."'";
		$rows = mysql_query($sql);
		if($row = mysql_fetch_assoc($rows))
		{
			$sql = "UPDATE ext_translations SET trl_content = '".$request->getPostParameter('top_bottom_txt')."' WHERE locale = '".$this->getUser()->getCulture()."' AND table_class='content' AND field_name='bottom_article' AND field_id = ".$content->getId();
			mysql_query($sql);
		}
		else
		{
			$sql = "INSERT INTO ext_translations(trl_content, locale, table_class, field_name, field_id) VALUES('".$request->getPostParameter('top_bottom_txt')."', '".$this->getUser()->getCulture()."', 'content', 'bottom_article', '".$content->getId()."')";
			mysql_query($sql);
		}

		//Set Bottom Sidebar Article
		$sql = "SELECT * FROM ext_translations WHERE field_id = '".$content->getId()."' AND field_name = 'bottom_sidebar' AND table_class = 'content' AND locale = '".$this->getUser()->getCulture()."'";
		$rows = mysql_query($sql);
		if($row = mysql_fetch_assoc($rows))
		{
			$sql = "UPDATE ext_translations SET trl_content = '".$request->getPostParameter('bottom_sidebar_txt')."' WHERE locale = '".$this->getUser()->getCulture()."' AND table_class='content' AND field_name='bottom_sidebar' AND field_id = ".$content->getId();
			mysql_query($sql);
		}
		else
		{
			$sql = "INSERT INTO ext_translations(trl_content, locale, table_class, field_name, field_id) VALUES('".$request->getPostParameter('bottom_sidebar_txt')."', '".$this->getUser()->getCulture()."', 'content', 'bottom_sidebar', '".$content->getId()."')";
			mysql_query($sql);
		}


		//Set Custom Footer
		$sql = "SELECT * FROM ext_translations WHERE field_id = '".$content->getId()."' AND field_name = 'custom_footer' AND table_class = 'content' AND locale = '".$this->getUser()->getCulture()."'";
		$rows = mysql_query($sql);
		if($row = mysql_fetch_assoc($rows))
		{
			$sql = "UPDATE ext_translations SET trl_content = '".$request->getPostParameter('custom_footer_txt')."' WHERE locale = '".$this->getUser()->getCulture()."' AND table_class='content' AND field_name='custom_footer' AND field_id = ".$content->getId();
			mysql_query($sql);
		}
		else
		{
			$sql = "INSERT INTO ext_translations(trl_content, locale, table_class, field_name, field_id) VALUES('".$request->getPostParameter('custom_footer_txt')."', '".$this->getUser()->getCulture()."', 'content', 'custom_footer', '".$content->getId()."')";
			mysql_query($sql);
		}
	}
		$content->setTopArticle($request->getPostParameter('top_article_txt'));
		$content->setMenuTitle($request->getPostParameter('menu_title'));
		$content->setLastModifiedOn(date('Y-m-d'));
		$content->setPublished($request->getPostParameter('published'));
		$content->setParentId($request->getPostParameter('parent'));
		$content->setVisibility($request->getPostParameter('visibility'));
		$content->setUrl($request->getPostParameter('url'));
		$content->setCreatedOn(date("Y-m-d"));
		$content->save();

	$client_ip = $_SERVER["REMOTE_ADDR"];
	if($client_ip == "::1" || $client_ip == "")
	{
		$client_ip = "127.0.0.1";
	}

	//Save Uploaded Banners
	$banners = $request->getPostParameter("banner_upload[]");
	foreach($banners as $bannernm)
	{
		$banner = new Banner();
		$banner->setImage($client_ip.$bannernm);
		$banner->setPageId($content->getId());
		$banner->save();
	}

    $this->redirect('/backend.php/content/index');
  }

  public function executeDelete(sfWebRequest $request)
  {

    $this->forward404Unless($content = Doctrine_Core::getTable('Content')->find(array($request->getParameter('id'))), sprintf('Object content does not exist (%s).', $request->getParameter('id')));

    $audit = new Audit();
    $audit->saveAudit("", "deleted content of id ".$content->getId());

    $content->delete();

    $this->redirect('/backend.php/content/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $content = $form->save();

	  if($content->getUrl() == "")
	  {
		  $content->setUrl("/index.php?id=".$content->getId());
		  $content->save();
	  }

	  if($content->getMenuIndex() == "")
	  {
		  $content->setMenuIndex($content->getId());
		  $content->save();
	  }

	  if($content->getPublished() == "")
	  {
		  $content->setPublished("1");
		  $content->save();
	  }

      $audit = new Audit();
      $audit->saveAudit("", "<a href=\"/backend.php/content/edit?id=".$content->getId()."&language=en\">updated content</a>");

      $this->redirect('/backend.php/content/index');
    }
  }
}
