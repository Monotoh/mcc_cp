<?php
	use_helper("I18N");
	$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
	mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
	$locale = '';
?>


<form id="webpageform" name="webpageform"  action="/backend.php/content/<?php echo ($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : ''); ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>   autocomplete="off" data-ajax="false" class="form-bordered">
<div class="panel panel-dark">
<div class="panel-heading">


           <h3 class="panel-title"><?php echo ($form->getObject()->isNew()?__('New WebPage'):__('Edit WebPage'));
		    if($sf_user->getCulture()){
				$sql = "SELECT * FROM ext_locales WHERE locale_identifier = '".$sf_user->getCulture()."'";
				$rows = mysql_query($sql);
				$locale = mysql_fetch_assoc($rows);
				echo " </h3><p>(".$locale['local_title']." Translation)</p> ";
			}
			?>




		   <?php if (!$form->getObject()->isNew()): ?>
			<input type="hidden" name="sf_method" value="put" />
			<?php endif; ?>

            <?php if(isset($form['_csrf_token'])): ?>
            <?php echo $form['_csrf_token']->render(); ?>
            <?php endif; ?>


			<?php echo $form->renderGlobalErrors() ?>

</div>


<div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
  <strong><?php echo __('Well done'); ?>!</strong> <?php echo __('You successfully updated this record'); ?></a>.
</div>

	<div class="panel-body panel-body-nopadding">
		<div class="form-group">
        	<div class="col-sm-12">
                <?php echo $form['menu_title']->renderError() ?>
          <input type="text" name="menu_title" id="menu_title" class="form-control" placeholder="<?php echo __('Page Title'); ?>" value="<?php
					$sql = "SELECT * FROM ext_translations WHERE field_id = '".$form->getObject()->getId()."' AND field_name = 'menu_title' AND table_class = 'content' AND locale = '".$sf_user->getCulture()."'";
					$rows = mysql_query($sql);
					if($row = mysql_fetch_assoc($rows))
					{
						echo $row['trl_content'];
					}
					else
					{
						echo $form->getObject()->getMenuTitle();
					}
				?>">
        

	  <div id="nameresult" name="nameresult"></div>

      <script language="javascript">
        $('document').ready(function(){
          $('#menu_title').keyup(function(){
            $.ajax({
                      type: "POST",
                      url: "/backend.php/content/checkname",
                      data: {
                          'name' : $('input:text[id=menu_title]').val()
                      },
                      dataType: "text",
                      success: function(msg){
                            //Receiving the result of search here
                            $("#nameresult").html(msg);
                      }
                  });
              });
        });
      </script>
       	 </div>
	    </div>
        <div class="form-group">
        	<div class="col-sm-12">
                <textarea name="top_article_txt" id="top_article_txt" class="form-control"><?php
					$sql = "SELECT * FROM ext_translations WHERE field_id = '".$form->getObject()->getId()."' AND field_name = 'top_article' AND table_class = 'content' AND locale = '".$sf_user->getCulture()."'";
					$rows = mysql_query($sql);
					if($row = mysql_fetch_assoc($rows))
					{
						echo $row['trl_content'];
					}
					else
					{
						echo $form->getObject()->getTopArticle();
					}
				 ?></textarea>
		</div>
    </div>
	<div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Status'); ?></label><br>
        <div class="col-sm-12">
            <select name="published" id="published" class="form-control">
				<option value="1" <?php if($content){ if($content->getPublished() == "1"){ echo "selected"; } } ?>><?php echo __('Published'); ?></option>
				<option value="0" <?php if($content){ if($content->getPublished() == "0"){ echo "selected"; } } ?>><?php echo __('UnPublished'); ?></option>
			</select>
		</div>
	</div>
	<div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Visibility'); ?></label><br>
        <div class="col-sm-12">
            <select name="visibility" id="visibility" class="form-control">
				<option value="1" <?php if($content){ if($content->getVisibility() == "1"){ echo "selected"; } } ?>><?php echo __('Public'); ?></option>
				<option value="0" <?php if($content){ if($content->getVisibility() == "0"){ echo "selected"; } } ?>><?php echo __('Registered Users Only'); ?></option>
			</select>
		</div>
	</div>
        <!-- OTB patch - option for content type -->
        <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Content Type'); ?></label><br>
        <div class="col-sm-12">
            <?php echo $form['content_type']->render() ?>
	</div>
        <!-- end -->
	<div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Parent'); ?></label><br>
        <div class="col-sm-12">
            <select name="parent" id="parent" class="form-control">
            	<option value="0"><?php echo __('Choose Parent'); ?></option>
            <?php
            $q = Doctrine_Query::create()
               ->from("Content a")
               ->where("a.deleted = 0")
               ->where("a.parent_id = 0")
               ->orderBy("a.menu_title ASC");
            $pages = $q->execute();
            foreach($pages as $page)
            {
            ?>
				<option value="<?php echo $page->getId() ?>" <?php if(!$form->getObject()->isNew() && $content->getParentId() == $page->getId()){ echo "selected"; } ?>><?php echo $page->getMenuTitle(); ?></option>
			<?php
			}
			?>
			</select>
		</div>
	</div>
</div><!-- panel-body -->
   <div class="panel-footer">
		<button class="btn btn-danger mr10"><?php echo __('Reset'); ?></button><button type="submit" class="btn btn-primary" name="submitbuttonname" id="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button>
  </div>
 </div><!-- panel-default -->
</form>



<script src="<?php echo public_path(); ?>assets_unified/js/ckeditor/ckeditor.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/ckeditor/adapters/jquery.js"></script>

<script>
jQuery(document).ready(function(){

  // CKEditor
  jQuery('#top_article_txt').ckeditor();

});
</script>
