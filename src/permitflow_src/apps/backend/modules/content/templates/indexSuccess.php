<?php
use_helper("I18N");

$audit = new Audit();
$audit->saveAudit("", "Accessed Web Pages settings");

if($sf_user->mfHasCredential("managewebpages"))
{
  $_SESSION['current_module'] = "content";
  $_SESSION['current_action'] = "index";
  $_SESSION['current_id'] = "";
?>
<div class="contentpanel">
<div class="panel panel-dark">

<div class="panel-heading">
		<h3 class="panel-title"><?php echo __('Web Pages'); ?></h3>

        <div class="pull-right">
                   <a class="btn btn-primary-alt settings-margin42" id="newpage" href="<?php echo public_path(); ?>backend.php/content/new" ><?php echo __('New WebPage'); ?></a>
       </div>
</div>



<div id='notifications' name='notifications'>
			</div>
<div class="table-responsive">
<table class="table dt-on-steroids mb0" id="table30">
    <thead>
      <tr>
      <th><input type='checkbox' name='batchall' onclick="boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.name == 'batch') { if(this.checked == true){ box.checked = true; }else{ box.checked = false; } } } "></th>
      <th class="no-sort">#</th>
      <th><?php echo __('Menu Title'); ?></th>
      <th><?php echo __('Order'); ?></th>
      <th width="60"><?php echo __('Published'); ?></th>
      <th width="120"><?php echo __('Requires Login?'); ?></th>
      <th width="60"class="no-sort"><?php echo __('Actions'); ?></th>
    </tr>
    </thead>
    <tbody>
	<?php
		$count = 1;
	?>
    <?php foreach($contents as $content): ?>
    <tr id="row_<?php echo $content->getId() ?>">
	  <td><input type='checkbox' name='batch' id='batch_<?php echo $content->getId() ?>' value='<?php echo $content->getId() ?>'></td>
      <td><?php echo $count++; ?></td>
	  <td><?php
    $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
    mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

    $menutitle = "";

    $sql = "SELECT * FROM ext_translations WHERE field_id = '".$content->getId()."' AND field_name = 'menu_title' AND table_class = 'content' AND locale = '".$sf_user->getCulture()."'";
    $rows = mysql_query($sql);
    if($row = mysql_fetch_assoc($rows))
    {
      $menutitle = $row['trl_content'];
    }
    else
    {
      $menutitle = $content->getMenuTitle();
    }

    echo $menutitle;

    ?></td>
      <td>
        <a href="/backend.php/content/orderup/id/<?php echo $content->getId(); ?>"><span class="glyphicon glyphicon-circle-arrow-up"></span></a>
      <a href="/backend.php/content/orderdown/id/<?php echo $content->getId(); ?>"><span class="glyphicon glyphicon-circle-arrow-down"></span></a>
      </td>
      <td align="center"><a id="publish<?php echo $content->getId(); ?>" href="<?php echo public_path().'backend.php/content/index?ptoggle='.$content->getId() ?>">
	  <?php
	  if($content->getPublished() == "1")
	  {
		  echo "<span class='badge-round badge-success'><span class='fa fa-check'></span></span>";
	  }
	  else
	  {
		  echo "<span class='badge-round badge-danger'><span class='fa fa-times'></span></span>";
	  }

	  ?>
      </a></td>
      <td align="center"><a id="access<?php echo $content->getId(); ?>" href="<?php echo public_path().'backend.php/content/index?ltoggle='.$content->getId() ?>">
	  <?php
	  if($content->getVisibility() == "0")
	  {
		  echo "<span class='badge-round badge-success'><span class='fa fa-check'></span></span>";
	  }
	  else
	  {
		  echo "<span class='badge-round badge-danger'><span class='fa fa-times'></span></span>";
	  }
	  ?>
      </a></td>
     <td align="center">
		<a id="editpage<?php echo $content->getId(); ?>" href="<?php echo public_path(); ?>backend.php/content/edit/id/<?php echo $content->getId(); ?>" title="<?php echo __('Edit'); ?>"><span class="badge badge-primary"><i class="fa fa-pencil"></i></span></a>
		<?php
    if($content->getPublished() == 0)
    {
    ?>
      	<a id="deletepage<?php echo $content->getId(); ?>"  onClick="if(confirm('Are you sure you want to delete this item?')){ return true; }else{ return false; }" href="<?php echo public_path(); ?>backend.php/content/delete/id/<?php echo $content->getId(); ?>" title="<?php echo __('Delete'); ?>"><span class="badge badge-primary"><i class="fa fa-trash-o"></i></span></a>
    <?php
    }
    ?>
	</td>
    </tr>
    <?php endforeach; ?>
    </tbody>
	<tfoot>
   <tr><td colspan='8' style='text-align: left;'>
   <select id='batch_action' name='batch_action' onChange="if(this.value != ''){if(confirm('Are you sure?')){ batch('content', this.options[this.selectedIndex].text, this.value); document.getElementById('default').selected='selected'; }}">
   <option id='default' value=''><?php echo __('Choose an action'); ?>..</option>
   <option value='delete'><?php echo __('Set As Deleted'); ?></option>
   </select>
   </td></tr>
   </tfoot>
</table>
</div>
</div>
</div>

<script language='javascript'>
  jQuery('#table30').dataTable({
      "sPaginationType": "full_numbers",

      // Using aoColumnDefs
      "aoColumnDefs": [
      	{ "bSortable": false, "aTargets": [ 'no-sort' ] }
    	]
    });




   function updateOrder(pageid, order)
   {
            var xmlHttpReq1 = false;
            var self = this;
            // Mozilla/Safari

            if (window.XMLHttpRequest) {
                self.xmlHttpReq1 = new XMLHttpRequest();
            }
            // IE
            else if (window.ActiveXObject) {
                self.xmlHttpReq1 = new ActiveXObject("Microsoft.XMLHTTP");
            }
            self.xmlHttpReq1.open('POST', '/backend.php/content/updateorder', true);
            self.xmlHttpReq1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            self.xmlHttpReq1.onreadystatechange = function() {
                if (self.xmlHttpReq1.readyState == 4) {
                    document.getElementById('page_' + pageid + '_order_status').innerHTML = 'Done!';
                }
                else
                {
                    document.getElementById('page_' + pageid + '_order_status').innerHTML = 'Updating...';
                }
            }

            self.xmlHttpReq1.send('pageid=' + pageid + '&order=' + order);
   }
</script>

<?php
}
else
{
  include_partial("settings/accessdenied");
}
?>
