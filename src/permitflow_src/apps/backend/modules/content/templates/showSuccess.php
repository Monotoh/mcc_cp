
<?php

$prefix_folder = dirname(__FILE__)."/../../../../..";

 require($prefix_folder.'/lib/vendor/cp_form/includes/check-session.php'); ?>
<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $content->getId() ?></td>
    </tr>
    <tr>
      <th>Type:</th>
      <td><?php echo $content->getTypeId() ?></td>
    </tr>
    <tr>
      <th>Title:</th>
      <td><?php echo $content->getTitle() ?></td>
    </tr>
    <tr>
      <th>Longtitle:</th>
      <td><?php echo $content->getLongtitle() ?></td>
    </tr>
    <tr>
      <th>Parent:</th>
      <td><?php echo $content->getParent() ?></td>
    </tr>
    <tr>
      <th>Summary:</th>
      <td><?php echo $content->getSummary() ?></td>
    </tr>
    <tr>
      <th>Article:</th>
      <td><?php echo $content->getArticle() ?></td>
    </tr>
    <tr>
      <th>Created on:</th>
      <td><?php echo $content->getCreatedOn() ?></td>
    </tr>
    <tr>
      <th>Published:</th>
      <td><?php echo $content->getPublished() ?></td>
    </tr>
    <tr>
      <th>Menu index:</th>
      <td><?php echo $content->getMenuIndex() ?></td>
    </tr>
    <tr>
      <th>Url:</th>
      <td><?php echo $content->getUrl() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('content/edit?id='.$content->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('content/index') ?>">List</a>
