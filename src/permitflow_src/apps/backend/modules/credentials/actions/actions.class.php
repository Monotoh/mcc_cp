<?php

/**
 * credentials actions.
 *
 * @package    permit
 * @subpackage credentials
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class credentialsActions extends sfActions
{
    /**
     * Executes 'Checkname' action
     *
     * Ajax used to check existence of name
     *
     * @param sfRequest $request A request object
     */
    public function executeCheckname(sfWebRequest $request)
    {
        // add new user
        $q = Doctrine_Query::create()
           ->from("MfGuardPermission a")
           ->where('a.name = ?', $request->getPostParameter('name'));
        $existingrole = $q->execute();
        if(sizeof($existingrole) > 0)
        {
              echo '<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Name is already in use!</strong></div><script language="javascript">document.getElementById("submitbuttonname").disabled = true;</script>';
              exit;
        }
        else
        {
              echo '<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Name is available!</strong></div><script language="javascript">document.getElementById("submitbuttonname").disabled = false;</script>';
              exit;
        }
    }
  
  public function executeBatch(sfWebRequest $request)
  {
		if($request->getPostParameter('delete'))
		{
			$item = Doctrine_Core::getTable('MfGuardPermission')->find(array($request->getPostParameter('delete')));
			if($item)
			{
				$item->delete();
			}
		}
    }

  public function executeIndex(sfWebRequest $request)
  {
	$q = Doctrine_Query::create()
       ->from('mfGuardPermission a')
	   ->orderBy('a.id DESC');
    $this->roles = $q->execute();
	 
	 
	$this->setLayout("layout-settings");
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new mfGuardPermissionForm();
  $this->setLayout("layout-settings");
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new mfGuardPermissionForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($mf_guard_permission = Doctrine_Core::getTable('mfGuardPermission')->find(array($request->getParameter('id'))), sprintf('Object mf_guard_permission does not exist (%s).', $request->getParameter('id')));
    $this->form = new mfGuardPermissionForm($mf_guard_permission);
  $this->setLayout("layout-settings");
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($mf_guard_permission = Doctrine_Core::getTable('mfGuardPermission')->find(array($request->getParameter('id'))), sprintf('Object mf_guard_permission does not exist (%s).', $request->getParameter('id')));
    $this->form = new mfGuardPermissionForm($mf_guard_permission);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {

    $this->forward404Unless($mf_guard_permission = Doctrine_Core::getTable('mfGuardPermission')->find(array($request->getParameter('id'))), sprintf('Object mf_guard_permission does not exist (%s).', $request->getParameter('id')));

    $audit = new Audit();
    $audit->saveAudit("", "deleted credential of id ".$mf_guard_permission->getId());

    $mf_guard_permission->delete();

    $this->redirect('/backend.php/credentials/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $mf_guard_permission = $form->save();

     $audit = new Audit();
     $audit->saveAudit("", "<a href=\"/backend.php/credentials/edit?id=".$mf_guard_permission->getId()."&language=en\">updated a credential</a>");

      $this->redirect('/backend.php/credentials/index');
    }
  }
}
