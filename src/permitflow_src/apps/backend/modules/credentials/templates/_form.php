<?php
use_helper("I18N");
?>

<div class="panel panel-dark">
<div class="panel-heading">
 <h3 class="panel-title"><?php echo ($form->getObject()->isNew()?__('New Role'):__('Edit Role')); ?></h3>
 </div>
<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
  <strong><?php echo __('Well done'); ?>!</strong> <?php echo __('You successfully updated this role'); ?></a>.
</div>

<div class="panel-body panel-body-nopadding">


<form id="roleform" action="<?php echo url_for('/backend.php/credentials/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> autocomplete="off" data-ajax="false" class="form-bordered form-horizontal">


	<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>

   <?php if(isset($form['_csrf_token'])): ?>
            <?php echo $form['_csrf_token']->render(); ?>
            <?php endif; ?>
      <?php echo $form->renderGlobalErrors() ?>
      <div class="form-group">
        <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Name'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['name']->renderError() ?>
          <?php echo $form['name']->render(array('required' => 'required')); ?>
        </div>
      </div>

      <div id="nameresult" name="nameresult"></div>

      <script language="javascript">
        $('document').ready(function(){
          $('#mf_guard_permission_name').keyup(function(){
            $.ajax({
                      type: "POST",
                      url: "/backend.php/credentials/checkname",
                      data: {
                          'name' : $('input:text[id=mf_guard_permission_name]').val()
                      },
                      dataType: "text",
                      success: function(msg){
                            //Receiving the result of search here
                            $("#nameresult").html(msg);
                      }
                  });
              });
        });
      </script>

      <div class="form-group">
        <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Description'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['description']->renderError() ?>
          <?php echo $form['description']->render(array('required' => 'required')); ?>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Groups'); ?></i></label>
        <div class="col-sm-8">
          <?php echo $form['groups_list']->renderError() ?>
          <?php echo $form['groups_list']->render(array('required' => 'required')); ?>
        </div>
      </div>
      
      </div>
   
	  <div class="panel-footer">
			<button class="btn btn-danger mr10"><?php echo __('Reset'); ?></button><button type="submit" class="btn btn-primary" name="submitbuttonname" id="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button>
	  </div>
</form>
</div>
<script type="text/javascript" src="<?php echo public_path(); ?>assets_unified/js/jquery.bootstrap-duallistbox.js"></script>

<script>
jQuery(document).ready(function(){
  
  // CKEditor
  var list1 = jQuery('select[name="mf_guard_permission[groups_list][]"]').bootstrapDualListbox();

});
</script>
