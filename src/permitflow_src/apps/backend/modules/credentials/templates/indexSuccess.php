<?php
use_helper("I18N");

$audit = new Audit();
$audit->saveAudit("", "Accessed roles settings");

if($sf_user->mfHasCredential("manageroles"))
{
  $_SESSION['current_module'] = "roles";
  $_SESSION['current_action'] = "index";
  $_SESSION['current_id'] = "";
?>
<div class="contentpanel">
<div class="panel panel-dark">
<div class="panel-heading">
  <h3 class="panel-title"><?php echo __('Roles'); ?></h3>
  <div class="pull-right">
  <a  id="newrole" class="btn btn-primary-alt tooltips pull-right" type="button" data-toggle="tooltip" href="<?php echo public_path(); ?>backend.php/credentials/new" title="New Role" style="margin-top:-28px;"><?php echo __('New Role'); ?></a>

</div>
  </div>

<div class="panel-body panel-body-nopadding">
<div class="table-responsive">

<table id="table3" class="table mb0 dt-on-steroids">
   <thead>
   <tr>
      <th style="width: 10px;"><input type='checkbox' name='batchall' onclick="boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.name == 'batch') { if(this.checked == true){ box.checked = true; }else{ box.checked = false; } } } "></th>
      <th width="60">#</th>
      <th><?php echo __('Name'); ?></th>
      <th><?php echo __('Description'); ?></th>
      <th width="5%"><?php echo __('Actions'); ?></th>
    </tr>
    </thead>
    <tbody>
	<?php
		$count = 1;
	?>
    <?php foreach ($roles as $mf_guard_permission): ?>
    <tr id="row_<?php echo $mf_guard_permission->getId() ?>">
	  <td><input type='checkbox' name='batch' id='batch_<?php echo $mf_guard_permission->getId() ?>' value='<?php echo $mf_guard_permission->getId() ?>'></td>
	  <td><?php echo $count++; ?></td>
      <td><?php echo $mf_guard_permission->getName() ?></td>
      <td><?php echo $mf_guard_permission->getDescription() ?></td>
      <td>
		    <a id="editrole<?php echo $mf_guard_permission->getId(); ?>" href="<?php echo public_path(); ?>backend.php/credentials/edit/id/<?php echo $mf_guard_permission->getId(); ?>" title="<?php echo __('Edit'); ?>"><span class="badge badge-primary"><i class="fa fa-pencil"></i></span></a>
       
        <a id="deleterole<?php echo $mf_guard_permission->getId(); ?>" onClick="if(confirm('Are you sure you want to delete this item?')){ return true; }else{ return false; }"  href="<?php echo public_path(); ?>backend.php/credentials/delete/id/<?php echo $mf_guard_permission->getId(); ?>" title="<?php echo __('Delete'); ?>"><span class="badge badge-primary"><i class="fa fa-trash-o"></i></span></a>
       
      </td>
    </tr>
    <?php endforeach; ?>
    </tbody>
	<tfoot>
   <tr><td colspan='7' style='text-align: left;'>
   <select id='batch_action' name='batch_action' onChange="if(this.value != ''){if(confirm('Are you sure?')){ batch('credentials', this.options[this.selectedIndex].text, this.value); document.getElementById('default').selected='selected'; }}">
   <option id='default' value=''><?php echo __('Choose an action'); ?>..</option>
   <option value='delete'><?php echo __('Set As Deleted'); ?></option>
   </select>
   </td></tr>
   </tfoot>
</table>
</div>
</div>
</div>
</div>

<script language='javascript'>
    jQuery(document).ready(function() {

    		jQuery('#table3').dataTable({
           "sPaginationType": "full_numbers"
         });

	});
</script>
<?php
}
else
{
  include_partial("settings/accessdenied");
}
?>
