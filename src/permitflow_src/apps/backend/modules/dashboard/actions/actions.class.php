<?php
/**
 * Dashboard actions.
 *
 * Displays a summary of all application related information for the reviewers
 *
 * @package    backend
 * @subpackage dashboard
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
class dashboardActions extends sfActions
{
  /** OTB patch 
     * Add method to count users async
     */
    public function executeClientsRegistered(sfWebRequest $request){
         $q = Doctrine_Query::create()
                ->from("SfGuardUser a")
                 ->where('a.is_active = ? ', 1)
                 ->andWhere('a.is_super_admin = ?',1);
                $clients = $q->count();
                //
                echo $clients ;
                exit();
    }
    /**
     * Clients New 
     */
    public function executeClientsNew(sfWebRequest $request){
            $q = Doctrine_Query::create()
                ->from("SfGuardUser a")
                ->where("a.created_at LIKE ?", "%".date("Y-m-d")."%");
            $new_clients = $q->count();
            echo $new_clients; 
            exit();
    }
    
     /**
     * Clients Signed in today
     */
    public function executeSignedInToday(sfWebRequest $request){
          $q = Doctrine_Query::create()
            ->from("SfGuardUser a")
            ->where("a.last_login LIKE ?", "%".date("Y-m-d")."%")
            ->andWhere("a.id <> 1");
        $clients_signed_in = $q->count();
        echo $clients_signed_in ;
        exit();
    }
    
    /**
     * Applications Received today
     */
    public function executeAppsReceivedToday(sfWebRequest $request){
       
         //OTB patch - Allowed user stages
          $agency = new AgencyManager();
          //$stage_ids_to_check = $agency->getAllowedStages($_SESSION["SESSION_CUTEFLOW_USERID"]);
          
          
       
        $q_to_res = Doctrine_Query::create()
            ->from("FormEntry a")
            ->where("a.date_of_submission LIKE ?", "%".date("Y-m-d")."%")
            ->andWhere("a.parent_submission = ? ", 0) // dont show revisions
            ->andWhere("a.approved <> ? ", 0) ;// dont count drafts
           // ->andWhere("a.approved  ");
        $apps = $q_to_res->execute();
        //
        $applications_submitted = $q_to_res->count();
        echo $applications_submitted ; exit();
    }
    /**
     * Applications approved today
     */
    public function executeApprovedToday(sfWebRequest $request){
        //OTB patch - Allowed user stages
          //$agency = new AgencyManager();
          //$stage_ids_to_check = $agency->getAllowedStages($_SESSION["SESSION_CUTEFLOW_USERID"]);
          //TODO - Retrieve a list of applications in approved stage.
        $approved_stage = 21 ;
        $q = Doctrine_Query::create()
            ->from("FormEntry a")
            ->where("a.date_of_response LIKE ?", "%".date("Y-m-d")."%") 
            ->andWhere("a.approved = ?", $approved_stage); //
            //>andWhere("a.saved_permit <> NULL");
        
        $applications_approved = $q->count();
        //
        echo $applications_approved; exit();
    }
    /**
     * Tasks assigned today
     */
    public function executeTaskAssignedToday(sfWebRequest $request){
         $q = Doctrine_Query::create()
            ->from("Task a")
            ->leftJoin("a.Application c")
            ->where("a.owner_user_id = ?", $_SESSION["SESSION_CUTEFLOW_USERID"])
            ->andWhere("a.date_created LIKE ?","%".date("Y-m-d")."%")
            ->andWhere("c.id = a.application_id");
        $task_assigned = $q->count();
        echo $task_assigned; exit;
    }
    /**
     * Task completed
     */
    public function executeTaskCompleted(sfWebRequest $request){
         $q = Doctrine_Query::create()
            ->from("Task a")
            ->leftJoin("a.Application c")
            ->where("a.owner_user_id = ?", $_SESSION["SESSION_CUTEFLOW_USERID"])
            ->andWhere("a.status = ?", "25")
            ->andWhere("a.last_update LIKE ?","%".date("Y-m-d")."%")
            ->andWhere("c.id = a.application_id");
        $task_completed = $q->count();
        echo $task_completed ; exit;
    }
    
    /**
     * Invoices sent today
     */
    public function executeInvoiceSentToday(sfWebRequest $request){
          //OTB patch - Allowed user stages
          $agency = new AgencyManager();
          $stage_ids_to_check = $agency->getAllowedStages($_SESSION["SESSION_CUTEFLOW_USERID"]);
          //
         $q = Doctrine_Query::create()
            ->from("MfInvoice a")
            ->leftJoin("a.FormEntry f")
            ->where("a.created_at LIKE ?", '%'.date("Y-m-d").'%')
            ->andWhere("f.approved in (".implode(',',$stage_ids_to_check).") ");
        $invoices_sent = $q->count();
        echo $invoices_sent ; exit;
    }
    /**
     * Invoices paid
     */
    public function executeInvoicesPaid(sfWebRequest $request){
         //OTB patch - Allowed user stages
          $agency = new AgencyManager();
          $stage_ids_to_check = $agency->getAllowedStages($_SESSION["SESSION_CUTEFLOW_USERID"]);
          $q = Doctrine_Query::create()
            ->from("MfInvoice a")
             ->leftJoin("a.FormEntry f")
            ->where("a.paid = ? AND a.updated_at LIKE ?", array('2','%'.date("Y-m-d").'%'))
            ->andWhere("f.approved in (".implode(',',$stage_ids_to_check).") ");
        $invoices_paid = $q->count();
       echo $invoices_paid ; exit;
    }
    /**
     * Apps delayed
     */
    public function executeDelayedApps(sfWebRequest $request){
        
        /* $otbhelper = new OTBHelper(); 
         //
         $agency = new AgencyManager();
         $stage_ids_to_check = $agency->getAllowedStages($_SESSION["SESSION_CUTEFLOW_USERID"]);
         //
        //Delayed applications
            $applications_delayed = array();
            //first select all applications this user is allowed to access
            $q_all_apps = Doctrine_Query::create()
                    ->from('FormEntry f')
                    ->where('f.approved in ('. implode(',', $stage_ids_to_check).')')
                    ->andWhere('f.parent_submission = ? ', 0)
                    ->andWhere('f.deleted_status = ? ', 0)
                    ;
            $q_all_apps_res = $q_all_apps->execute();
            foreach($q_all_apps_res as $r){
                ///
                $app_menu_ids = $otbhelper->getMenuId($r['approved']);
                $stage_approved = $otbhelper->getStageWithSetStageType(4, $app_menu_ids->getMenuId());
                //check if application approved
                 if($otbhelper->checkApplicationApproved($stage_approved->getId(), $r['id'])){
                     //ignore if approved
                 }else {
                     // not approved, use this

                        $today = date('Y-m-d H:i:s') ;
                        $submission_date = $r['date_of_submission'];
                        ///
                        $diff = $today - strtotime($submission_date) ;
                        $diff_days = round($diff/86400) ;
                        //service charter time is 30 days - Make dynamic
                        if($diff_days > 30){
                            array_push($applications_delayed, $r['id']) ; // push the application


                        }
                 }

            }
           // error_log("Delayed apps >>> ".count($applications_delayed));
            $res = count($applications_delayed);
            echo $res; exit; */
        echo 0 ; exit;
    }
    /**
     * Sent back appliations
     */
    public function executeSentBackApplications(sfWebRequest $request){
         /////////////////////Sent back for Corrections
        $agency = new AgencyManager();
        $q_selected_corrections = Doctrine_Query::create()
                ->from('SubMenus s')
                ->where('s.stage_type = ? ', 5); //Sent for Corrections
         $q_selected_corrections_res = $q_selected_corrections->execute();
         $stage_c_ids = array() ;
         foreach($q_selected_corrections_res as $res){
             //Only push menus that the user has access for
            /* if($sf_user->mfHasCredential('accesssubmenu'.$res['id'])) {
               array_push($stage_c_ids, $res['id']) ;
             }*/
              array_push($stage_c_ids, $res['id']) ;

        }
       // error_log("Stage ids >>>> ".implode(",",$stage_c_ids));
       $q_corrections = Doctrine_Query::create()
                ->from('FormEntry f')
                ->where('f.approved in ('.implode(",",$stage_c_ids).')')
                ->andWhere('f.parent_submission = ? ', 0)
                ->andWhere('f.deleted_status = ? ', 0)
               ; 
        $q_corrections_res = $q_corrections->count();
        echo $q_corrections_res ; exit;
    }
    /**
     * Declined applications
     */
   public function executeDeclinedApplications(sfWebRequest $request){
       //select all stage types is 6 - rejected
        $agency = new AgencyManager();
        $q_stages_reject = Doctrine_Query::create()
                ->from('SubMenus s')
                ->where('s.stage_type = ? ', 6); //Rejected
        $q_stages_res = $q_stages_reject->execute();
        $stage_ids = array();
        //
        foreach($q_stages_res as $res){
            //Only push menus that the user has access for
            array_push($stage_ids, $res['id']) ;
           
        }

        //
        $q_declined = Doctrine_Query::create()
                ->from('FormEntry f')
                ->where('f.approved in ('.implode(',',$stage_ids).')')
                ->andWhere('f.parent_submission = ? ', 0)
                ->andWhere('f.deleted_status = ? ', 0)
                ; 
        $q_declined_res = $q_declined->count();
        echo $q_declined_res ; exit;
       // echo 0 ; exit;
                                       
   }
    /**
     * Executes 'Index' action
     *
     * Displays a summary of all application related information for the reviewers
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {
        //OTB Start - Single signon for reporting tool
		$agency_manager = new AgencyManager();
        if ($request->getGetParameter("url_onestop_analytics")){
                $q = Doctrine_Query::create()
                                 ->from('CfUser a')
                                 ->where('a.nid = ?', $_SESSION["SESSION_CUTEFLOW_USERID"]);
                $logged_reviewer = $q->fetchOne();
                $data = array();
                $data['email'] = $logged_reviewer->getStremail();
                $data['first_name'] = $logged_reviewer->getStrfirstname();
                $data['last_name'] = $logged_reviewer->getStrlastname();
                $data['username'] = $logged_reviewer->getStruserid();
                $data['department'] = $logged_reviewer->getStrdepartment();
                $data['manage_analytical_reports'] = $this->getUser()->mfHasCredential("manage_analytical_reports") ? "yes" : "no";
                $data['user_perms'] = $agency_manager->getAllowedAgencies($_SESSION["SESSION_CUTEFLOW_USERID"]);//For Rwanda, send list of allowed agencies to BI tool
                $signon_data = base64_encode(json_encode($data));
                return $this->redirect($request->getGetParameter("url_onestop_analytics").'/?data='.$signon_data);
        }
        //OTB End - Single signon for reporting tool

        $this->invoice_form = new InvoicetemplatesForm();
        $this->permit_form = new permitsForm();

        if($request->getParameter("mark"))
        {
          $q = Doctrine_Query::create()
              ->from('Communications a')
              ->where('a.id = ?', $request->getParameter('mark'));
          $message = $q->fetchOne();

          if($message)
          {
            $message->setMessageread(1);
            $message->save();
          }
        }
        //
        $this->setLayout('layout-metronic') ;
    }
    
    /**
     * Execute custom dashboard with statisctics
     */
    public function executeStats(sfWebRequest $request)
    {
        $this->setLayout('layout-metronic');
    }
    
    /**
     * Executes 'Statistics' action
     *
     * Displays a summary of all application related information for the reviewers
     *
     * @param sfRequest $request A request object
     */
    public function executeStatistics(sfWebRequest $request)
    {

    }

    public function executeFetchyear(sfWebRequest $request)
    {
        $this->year = $request->getParameter("year");
        $this->currency = $request->getParameter("currency");
        $this->setLayout(false);
    }

    public function executeFetchmonth(sfWebRequest $request)
    {
        $this->month = $request->getParameter("month");
        $this->currency = $request->getParameter("currency");
        $this->setLayout(false);
    }
}
