<?php
/**
 * Dashboard components.
 *
 * Contains code snippets that can be inserted into the layout
 *
 * @package    backend
 * @subpackage dashboard
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
class dashboardComponents extends sfComponents
{
      /** custom stles for metronic 4 */
      public function executeStylesheetmetronic(sfWebRequest $request){

      }
        /** custom metronic v4  */
    public function executeJavascriptsmetronic(sfWebRequest $request){}
    
    /**
     * Executes 'Sidemenu' component
     *
     * Displays the sidemenu located on the left of the screen
     *
     */
    public function executeSidemenu(sfWebRequest $request)
    {
          $this->module = $request->getParameter('module');
          $this->action = $request->getParameter('action');
    }

    /**
     * Executes 'Sidemenu' component
     *
     * Displays the sidemenu located on the left of the screen
     *
     */
    public function executeSettingssidemenu(sfWebRequest $request)
    {
        $this->module = $request->getParameter('module');
        $this->action = $request->getParameter('action');
    }
	
    /**
     * Executes 'Rightbar' component
     *
     * Displays the rightbar located on the right of the screen
     *
     */
    public function executeRightbar(sfWebRequest $request)
    {
          $this->module = $request->getParameter('module');
          $this->action = $request->getParameter('action');
    }
    
    /**
     * Executes 'Header' component
     *
     * Displays the header component
     *
     */
    public function executeHeader(sfWebRequest $request)
    {
          $this->module = $request->getParameter('module');
          $this->action = $request->getParameter('action');
          //OTB patch for getting logged in user profile image
          $user_id = $this->getUser()->getAttribute('userid') ;
          //reviewr info
           $q = Doctrine_Query::create()
                         ->from('CfUser a')
                         ->where('a.nid = ?', $user_id);
                 $this->reviewer = $q->fetchOne();
         //upload directory settings
          $q2 = Doctrine_Query::create()
                  ->from("ApSettings a")
                  ->where("a.id = 1")
                  ->orderBy("a.id DESC");
         $this->aplogo = $q2->fetchOne();
                 
    }
    /**
     * Custom Header 2
     */
     public function executeHeader2(sfWebRequest $request)
    {
          $this->module = $request->getParameter('module');
          $this->action = $request->getParameter('action');
          //OTB patch for getting logged in user profile image
          $user_id = $this->getUser()->getAttribute('userid') ;
          //reviewr info
           $q = Doctrine_Query::create()
                         ->from('CfUser a')
                         ->where('a.nid = ?', $user_id);
                 $this->reviewer = $q->fetchOne();
         //upload directory settings
          $q2 = Doctrine_Query::create()
                  ->from("ApSettings a")
                  ->where("a.id = 1")
                  ->orderBy("a.id DESC");
         $this->aplogo = $q2->fetchOne();
                 
    }

    /**
     * Executes 'Header' component
     *
     * Displays the header component
     *
     */
    public function executeSettingsheader(sfWebRequest $request)
    {
        $this->module = $request->getParameter('module');
        $this->action = $request->getParameter('action');
    }
    
    /**
     * Executes 'Footer' component
     *
     * Displays the Footer component
     *
     */
    public function executeFooter(sfWebRequest $request)
    {
          $this->module = $request->getParameter('module');
          $this->action = $request->getParameter('action');
    }
    
    /**
     * Executes 'Checksession' component
     *
     * Checks whether the user has logged in correctly
     *
     */
    public function executeChecksession(sfWebRequest $request)
    {
          $this->module = $request->getParameter('module');
          $this->action = $request->getParameter('action');
    }

    /**
     * Executes 'Stylesheets' component
     *
     * Displays the stylesheets component
     *
     */
    public function executeStylesheets(sfWebRequest $request)
    {
          $this->module = $request->getParameter('module');
          $this->action = $request->getParameter('action');
    }

    /**
     * Executes 'Javascripts' action
     *
     * Displays the javascripts component
     *
     */
    public function executeJavascripts(sfWebRequest $request)
    {
          $this->module = $request->getParameter('module');
          $this->action = $request->getParameter('action');
    }
    /**
     * Javascripts for dataTables backend
     */
    public function executeJavascriptsdatatable(sfWebRequest $request){
        
    }
}
