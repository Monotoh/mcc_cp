<?php
/**
 * _checksession.php template.
 *
 * Checks whether the user is logged in correctly
 *
 * @package    backend
 * @subpackage dashboard
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

//Check if backend user is logged in, if so log them out
if($sf_user->isAuthenticated() && $sf_user->getAttribute('userid') != ""){
	
    $q = Doctrine_Query::create()
       ->from("CfUser a")
       ->where("a.nid = ?", $sf_user->getAttribute('userid'));
    $user = $q->fetchOne();
	if(empty($user))
	{
		header(public_path().'backend.php/login/logout');
	}

	if($sf_user->getCulture() == "")
	{
		$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
		mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
		
		$sql = "SELECT * FROM ext_locales";
		$rows = mysql_query($sql);
		
		$setlocale = $sf_user->getCulture();
		
		
		while($row = mysql_fetch_assoc($rows))
		{
			$selected = "";
			if($setlocale == $row['locale_identifier'])
			{
				$sf_user->setCulture($row['locale_identifier']);
			}
			else if($setlocale == "" && $row['is_default'] == "1")
			{
				$sf_user->setCulture($row['locale_identifier']);
			}
		}
	}
}
else
{
	header("Location: ".public_path().'backend.php/login/logout');
	exit;
}
?>
