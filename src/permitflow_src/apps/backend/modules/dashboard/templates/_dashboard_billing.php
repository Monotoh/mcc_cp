<?php if($sf_user->mfHasCredential("access_billing")){

//Check if the system has forms with different currencies and active payments
$q = Doctrine_Query::create()
    ->from('ApForms a')
    ->where('a.payment_enable_merchant = ?', 1)
    ->andWhere('a.form_type = 1')
    ->andWhere('a.form_active = 1')
    ->groupBy('a.payment_currency');

$currencies = $q->count();

if($currencies)
{
  $currency_forms = $q->execute();
  foreach($currency_forms as $form)
  {
    $currency = $form->getPaymentCurrency();

    include_partial('dashboard_currency', array("currency" => $currency));
  }
}
else
{
?>
    <div class="row">
    <div class="col-sm-12 col-md-12">

    <div class="panel panel-default">

    <div class="panel-body">
    <div class="row">
    <div class="col-sm-12">
    <h5 class="subtitle mb5">Revenue</h5>
    <p class="mb15">The following is a summary of today's income from paid invoices:</p>

    <ul class="nav nav-tabs nav-tab-special">
        <li class="<?php if($_GET['tab'] == "thisyear"){ echo "active"; } ?>"><a href="/backend.php/dashboard/statistics?tab=thisyear"> Annual</a></li>
        <li class="<?php if($_GET['tab'] == "thismonth"){ echo "active"; } ?>"><a href="/backend.php/dashboard/statistics?tab=thismonth"> Monthly</a></li>
        <li class="<?php if($_GET['tab'] == "thisweek"){ echo "active"; } ?>"><a href="/backend.php/dashboard/statistics?tab=thisweek"> This Week</a></li>
        <li class="<?php if($_GET['tab'] == "today"){ echo "active"; } ?>"><a href="/backend.php/dashboard/statistics?tab=today"> Today</a></li>

    </ul>
    <div class="panel-body">

    <!-- Tab panes -->
    <div class="tab-content tab-content-nopadding">

    <?php if($_GET['tab'] == "today"){  ?>
    <div id="today" class="tab-pane active">
        <script>
            jQuery(document).ready(function(){
                /***** BAR CHART *****/

                    <?php
                      $earnings = null;

                      $search = null;
                      $total_earnings = 0;

                      $hours = null;

                      for($hour = 0; $hour <= 24; $hour++)
                      {
                        $search = $hour;
                        if($hour < 10)
                        {
                          $search = "0".$hour;
                        }
                        $hours[] = $search;

                        $q = Doctrine_Query::create()
                           ->select("SUM(b.amount) AS total")
                           ->from("MfInvoice a")
                           ->leftJoin("a.mfInvoiceDetail b")
                           ->where("a.created_at LIKE ?", "%".date('Y-m-d')." ".$search."%")
                           ->andWhere("a.paid = 2")
                           ->andWhere("b.description LIKE ? OR b.description LIKE ?", array("%total%", "%submission fee%"));
                        $invoice = $q->fetchOne();
                        $count = 0;
                        if($invoice)
                        {
                           $count++;
                           $earnings['hour'.$search] = $invoice->getTotal();
                           if(empty($earnings['hour'.$search]))
                           {
                              $earnings['hour'.$search] = 0;
                           }
                           $total_earnings += $invoice->getTotal();
                        }
                      }
                    ?>

                var bardata = [ <?php $count = 0; foreach($hours as $hour){ ?>["<?php echo $hour; ?>hrs", <?php echo $earnings['hour'.$hour]; ?>], <?php } ?> ];

                jQuery.plot("#barchart4", [ bardata ], {
                    series: {
                        lines: {
                            lineWidth: 1
                        },
                        bars: {
                            show: true,
                            barWidth: 0.5,
                            align: "center",
                            lineWidth: 0,
                            fillColor: "#428BCA"
                        }
                    },
                    grid: {
                        borderColor: '#ddd',
                        borderWidth: 1,
                        labelMargin: 10
                    },
                    xaxis: {
                        mode: "categories",
                        tickLength: 0
                    }
                });

                function labelFormatter(label, series) {
                    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
                }

            });
        </script>

        <div class="tinystat mr20">
            <div class="datainfo">
                <span class="text-muted">Total</span>
                <h4><?php echo sfConfig::get('app_currency').". ".$total_earnings; ?></h4>
            </div>
        </div>

        <div id="barchart4" style="width: 100%; height: 300px"></div>


        <div style="margin-top:20px"></div>

    </div>
    <?php } ?>
    <?php if($_GET['tab'] == "thisweek"){ ?>
    <div id="thisweek" class="tab-pane active">

        <script>
            jQuery(document).ready(function(){
                /***** BAR CHART *****/

                    <?php
                      $earnings = null;

                      $search = null;
                      $total_earnings = 0;

                      $week_start = new DateTime();
                      $week_start->setISODate(date('Y'),date('W', strtotime(date('Y-m-d'))));

                      $start_date = $week_start->format('Y-m-d');
                      $days = null;
                      $days[] = $start_date;
                      $count = 0;
                      while($count < 6)
                      {
                         $count++;
                         $days[] = date('Y-m-d', strtotime( date( "Y-m-d", strtotime( $start_date ) ) . "+".$count." day" ) );
                      }

                      foreach($days as $day)
                      {
                        $q = Doctrine_Query::create()
                           ->select("SUM(b.amount) AS total")
                           ->from("MfInvoice a")
                           ->leftJoin("a.mfInvoiceDetail b")
                           ->where("a.created_at LIKE ?", "%".$day."%")
                           ->andWhere("a.paid = 2")
                           ->andWhere("b.description LIKE ? OR b.description LIKE ?", array("%total%", "%submission fee%"));
                        $invoice = $q->fetchOne();
                        $count = 0;
                        if($invoice)
                        {
                           $count++;
                           $earnings['day'.$day] = $invoice->getTotal();
                           if(empty($earnings['day'.$day]))
                           {
                              $earnings['day'.$day] = 0;
                           }
                           $total_earnings += $invoice->getTotal();
                        }
                      }
                    ?>

                var bardata = [ <?php $count = 0; foreach($days as $day){
                          $count++;

                          $label = "";
                          switch($count)
                          {
                            case 1:
                              $label = "Mon";
                              break;
                            case 2:
                              $label = "Tue";
                               break;
                            case 3:
                              $label = "Wed";
                              break;
                            case 4:
                              $label = "Thu";
                              break;
                            case 5:
                              $label = "Fri";
                              break;
                            case 6:
                              $label = "Sat";
                              break;
                            case 7:
                              $label = "Sun";
                              break;
                          }

                      ?>["<?php echo $label; ?>", <?php echo $earnings['day'.$day]; ?>], <?php } ?> ];

                jQuery.plot("#barchart2", [ bardata ], {
                    series: {
                        lines: {
                            lineWidth: 1
                        },
                        bars: {
                            show: true,
                            barWidth: 0.5,
                            align: "center",
                            lineWidth: 0,
                            fillColor: "#428BCA"
                        }
                    },
                    grid: {
                        borderColor: '#ddd',
                        borderWidth: 1,
                        labelMargin: 10
                    },
                    xaxis: {
                        mode: "categories",
                        tickLength: 0
                    }
                });

                function labelFormatter(label, series) {
                    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
                }

            });
        </script>


        <div class="tinystat mr20">
            <div class="datainfo">
                <span class="text-muted">THIS WEEKS Total</span>
                <h4><?php echo sfConfig::get('app_currency').". ".$total_earnings; ?></h4>
            </div>
        </div>

        <div id="barchart2" style="width: 100%; height: 300px"></div>


        <div style="margin-top:20px"></div>

    </div>
    <?php } ?>
    <?php if($_GET['tab'] == "thismonth"){ ?>
    <div id="thismonth" class="tab-pane active">

        <h5 class="subtitle mb5">Month:
            <select name='choosemonth' id='choosemonth'>
                <?php
                for($x = 1; $x <= 12; $x++)
                {
                    $selected = "";
                    if($x == date("m"))
                    {
                        $selected = "selected='selected'";
                    }
                    echo "<option value='".$x."' ".$selected.">".$x."</option>";
                }
                ?>
            </select>
            <script>
                jQuery(document).ready(function(){
                    $("#choosemonth").on('change', function() {
                        $("#loadmonth").load("/backend.php/dashboard/fetchmonth/month/" + this.value);
                    });
                });
            </script>
        </h5>
        <div id="loadmonth" name="loadmonth">
            <div id="barchart1" style="width: 100%; height: 300px"></div>
            <script>
                jQuery(document).ready(function(){
                    /***** BAR CHART *****/

                        <?php
                          $earnings = null;

                          $search = null;
                          $total_earnings = 0;

                          for($day = 1; $day <= 31; $day++)
                          {
                            if($day < 10)
                            {
                                $search = date('Y')."-".date('m')."-0".$day;
                            }
                            else
                            {
                                $search = date('Y')."-".date('m')."-".$day;
                            }

                            $q = Doctrine_Query::create()
                               ->select("SUM(b.amount) AS total")
                               ->from("MfInvoice a")
                               ->leftJoin("a.mfInvoiceDetail b")
                               ->where("a.created_at LIKE ?", "%".$search."%")
                               ->andWhere("a.paid = 2")
                               ->andWhere("b.description LIKE ? OR b.description LIKE ?", array("%total%", "%submission fee%"));
                            $invoice = $q->fetchOne();
                            if($invoice)
                            {
                               $earnings['day'.$day] = $invoice->getTotal();
                               if(empty($earnings['day'.$day]))
                               {
                                  $earnings['day'.$day] = 0;
                               }
                               $total_earnings += $invoice->getTotal();
                            }
                          }
                        ?>

                    var bardata = [ <?php for($day = 1; $day <= 31; $day++){ ?>["<?php echo $day; ?>", <?php echo $earnings['day'.$day]; ?>], <?php } ?> ];

                    jQuery.plot("#barchart1", [ bardata ], {
                        series: {
                            lines: {
                                lineWidth: 1
                            },
                            bars: {
                                show: true,
                                barWidth: 0.5,
                                align: "center",
                                lineWidth: 0,
                                fillColor: "#428BCA"
                            }
                        },
                        grid: {
                            borderColor: '#ddd',
                            borderWidth: 1,
                            labelMargin: 10
                        },
                        xaxis: {
                            mode: "categories",
                            tickLength: 0
                        }
                    });

                    function labelFormatter(label, series) {
                        return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
                    }

                });
            </script>
            <div class="tinystat mr20" style="margin-top:40px">

                <div class="datainfo">
                    <span class="text-muted">Total</span>
                    <h4><?php echo sfConfig::get('app_currency').". ".$total_earnings; ?></h4>
                </div>
            </div>

        </div>


    </div>
    <?php } ?>
    <?php if($_GET['tab'] == "thisyear"){ ?>
    <div id="thisyear" class="tab-pane active">

        <?php $year = date("Y"); ?>
        <h5 class="subtitle mt10">Year:
            <select name='chooseyear' id='chooseyear'>
                <?php
                for($x = $year; $x > 2008; $x--)
                {
                    echo "<option value='".$x."'>".$x."</option>";
                }
                ?>
            </select>
            <script>
                jQuery(document).ready(function(){
                    $("#chooseyear").on('change', function() {
                        $("#loadyear").load("/backend.php/dashboard/fetchyear/year/" + this.value);
                    });
                });
            </script>
        </h5>
        <div id="loadyear" name="loadyear">
            <div id="barchart" style="width: 100%; height: 300px"></div>

            <script>
                jQuery(document).ready(function(){
                    /***** BAR CHART *****/

                        <?php
                          $earnings = null;
                          $earnings['month1'] = 0;
                          $earnings['month2'] = 0;
                          $earnings['month3'] = 0;
                          $earnings['month4'] = 0;
                          $earnings['month5'] = 0;
                          $earnings['month6'] = 0;
                          $earnings['month7'] = 0;
                          $earnings['month8'] = 0;
                          $earnings['month9'] = 0;
                          $earnings['month10'] = 0;
                          $earnings['month11'] = 0;
                          $earnings['month12'] = 0;

                          $search = null;
                          $total_earnings = 0;

                          for($month = 1; $month <= 12; $month++)
                          {
                            if($month < 10)
                            {
                              $search = $year."-0".$month."%";
                            }
                            else
                            {
                              $search = $year."-".$month."%";
                            }

                            $q = Doctrine_Query::create()
                               ->select("SUM(b.amount) AS total")
                               ->from("MfInvoice a")
                               ->leftJoin("a.mfInvoiceDetail b")
                               ->leftJoin("a.FormEntry c")
                               ->where("a.created_at LIKE ?", $search)
                               ->andWhere("a.paid = 2")
                               ->andWhere("b.description LIKE ? OR b.description LIKE ?", array("%total%", "%submission fee%"));
                            $invoice = $q->fetchOne();
                            if($invoice)
                            {
                               $earnings['month'.$month] = $invoice->getTotal();
                               if(empty($earnings['month'.$month]))
                               {
                                  $earnings['month'.$month] = 0;
                               }
                               $total_earnings += $invoice->getTotal();
                            }
                          }
                        ?>

                    var bardata = [ ["Jan", <?php echo $earnings['month1']; ?>], ["Feb", <?php echo $earnings['month2']; ?>], ["Mar", <?php echo $earnings['month3']; ?>], ["Apr", <?php echo $earnings['month4']; ?>], ["May", <?php echo $earnings['month5']; ?>], ["Jun", <?php echo $earnings['month6']; ?>], ["Jul", <?php echo $earnings['month7']; ?>], ["Aug", <?php echo $earnings['month8']; ?>], ["Sep", <?php echo $earnings['month9']; ?>], ["Oct", <?php echo $earnings['month10']; ?>], ["Nov", <?php echo $earnings['month11']; ?>], ["Dec", <?php echo $earnings['month12']; ?>] ];

                    jQuery.plot("#barchart", [ bardata ], {
                        series: {
                            lines: {
                                lineWidth: 1
                            },
                            bars: {
                                show: true,
                                barWidth: 0.5,
                                align: "center",
                                lineWidth: 0,
                                fillColor: "#428BCA"
                            }
                        },
                        grid: {
                            borderColor: '#ddd',
                            borderWidth: 1,
                            labelMargin: 10
                        },
                        xaxis: {
                            mode: "categories",
                            tickLength: 0
                        }
                    });

                    function labelFormatter(label, series) {
                        return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
                    }

                });
            </script>

            <div class="tinystat mr20" style="margin-top:40px">
                <div class="datainfo">
                    <span class="text-muted">Total</span>
                    <h4><?php echo sfConfig::get('app_currency').". ".$total_earnings; ?></h4>
                </div>
            </div>



        </div>

    </div>
    <?php } ?>
    </div>

    </div></div></div>



    </div><!-- panel-body -->
    </div><!-- panel-default-special -->
    </div><!-- cotentpanel -->
</div><!-- row -->
<?php
  }
}
?>
