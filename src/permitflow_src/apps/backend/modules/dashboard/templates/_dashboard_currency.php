<?php
  $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
  mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
?>
<div class="row">
<div class="col-sm-12 col-md-12">

<div class="panel panel-default">

<div class="panel-body">
<div class="row">
<div class="col-sm-12">
<h5 class="subtitle mb5">Revenue</h5>
<p class="mb15">The following is a summary of today's income from paid invoices:</p>

<ul class="nav nav-tabs nav-tab-special">
    <li class="<?php if($_GET['tab'] == "thisyear"){ echo "active"; } ?>"><a href="/backend.php/dashboard/statistics?tab=thisyear"> Annual</a></li>
    <li class="<?php if($_GET['tab'] == "thismonth"){ echo "active"; } ?>"><a href="/backend.php/dashboard/statistics?tab=thismonth"> Monthly</a></li>
    <li class="<?php if($_GET['tab'] == "thisweek"){ echo "active"; } ?>"><a href="/backend.php/dashboard/statistics?tab=thisweek"> This Week</a></li>
    <li class="<?php if($_GET['tab'] == "today"){ echo "active"; } ?>"><a href="/backend.php/dashboard/statistics?tab=today"> Today</a></li>
</ul>
<div class="panel-body">

<!-- Tab panes -->
<div class="tab-content tab-content-nopadding">

<?php if($_GET['tab'] == "today"){  ?>
<div id="today<?php echo $currency; ?>" class="tab-pane active">
    <script>
        jQuery(document).ready(function(){
            /***** BAR CHART *****/

                <?php
                  $earnings = null;

                  $search = null;
                  $total_earnings = 0;

                  $hours = null;

                  for($hour = 0; $hour <= 24; $hour++)
                  {
                    $search = $hour;
                    if($hour < 10)
                    {
                      $search = "0".$hour;
                    }
                    $hours[] = $search;

                    $sql = "SELECT SUM(a.payment_amount) AS total_amount, COUNT(*) AS total_apps
                            FROM ap_form_payments a
                            WHERE a.payment_date LIKE '%".date('Y-m-d')." ".$search."%' AND a.status = 2 AND a.payment_currency = '".$currency."'";
                    $results = mysql_query($sql, $dbconn);

                    $sub_totals = 0;

                    while($row = mysql_fetch_assoc($results))
                    {
                       if($currency == "KES" && sfConfig::get('app_sso_secret'))
                       {
                          $sub_totals += $row['total_amount'] - ($row['total_apps'] * 50);
                       }
                       elseif($currency == "USD" && sfConfig::get('app_sso_secret'))
                       {
                          $sub_totals += $row['total_amount'] - ($row['total_apps'] * 1);
                       }
                       else
                       {
                         $sub_totals += $row['total_amount'];
                       }
                    }

                     $earnings['hour'.$search] = $sub_totals;
                     if(empty($earnings['hour'.$search]))
                     {
                        $earnings['hour'.$search] = 0;
                     }
                     $total_earnings += $sub_totals;
                  }
                ?>

            var bardata = [ <?php $count = 0; foreach($hours as $hour){ ?>["<?php echo $hour; ?>hrs", <?php echo $earnings['hour'.$hour]; ?>], <?php } ?> ];

            jQuery.plot("#barchart<?php echo $currency; ?>", [ bardata ], {
                series: {
                    lines: {
                        lineWidth: 1
                    },
                    bars: {
                        show: true,
                        barWidth: 0.5,
                        align: "center",
                        lineWidth: 0,
                        fillColor: "#428BCA"
                    }
                },
                grid: {
                    borderColor: '#ddd',
                    borderWidth: 1,
                    labelMargin: 10
                },
                xaxis: {
                    mode: "categories",
                    tickLength: 0
                }
            });

            function labelFormatter(label, series) {
                return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
            }

        });
    </script>

    <div class="tinystat mr20">
        <div class="datainfo">
            <span class="text-muted">Total</span>
            <h4><?php echo $currency.". ".$total_earnings; ?></h4>
        </div>
    </div>

    <div id="barchart<?php echo $currency; ?>" style="width: 100%; height: 300px"></div>



    <div style="margin-top:20px"></div>

</div>
<?php } ?>
<?php if($_GET['tab'] == "thisweek"){ ?>
<div id="thisweek<?php echo $currency; ?>" class="tab-pane active">
    <script>
        jQuery(document).ready(function(){
            /***** BAR CHART *****/

                <?php
                  $earnings = null;

                  $search = null;
                  $total_earnings = 0;

                  $week_start = new DateTime();
                  $week_start->setISODate(date('Y'),date('W', strtotime(date('Y-m-d'))));

                  $start_date = $week_start->format('Y-m-d');
                  $days = null;
                  $days[] = $start_date;
                  $count = 0;
                  while($count < 6)
                  {
                     $count++;
                     $days[] = date('Y-m-d', strtotime( date( "Y-m-d", strtotime( $start_date ) ) . "+".$count." day" ) );
                  }

                  foreach($days as $day)
                  {
                    $sql = "SELECT SUM(a.payment_amount) AS total_amount, COUNT(*) AS total_apps
                            FROM ap_form_payments a
                            WHERE a.payment_date LIKE '%".$day."%' AND a.status = 2 AND a.payment_currency = '".$currency."'";
                    $results = mysql_query($sql, $dbconn);

                    $sub_totals = 0;

                    while($row = mysql_fetch_assoc($results))
                    {
                       if($currency == "KES" && sfConfig::get('app_sso_secret'))
                       {
                          $sub_totals += $row['total_amount'] - ($row['total_apps'] * 50);
                       }
                       elseif($currency == "USD" && sfConfig::get('app_sso_secret'))
                       {
                          $sub_totals += $row['total_amount'] - ($row['total_apps'] * 1);
                       }
                       else
                       {
                         $sub_totals += $row['total_amount'];
                       }
                    }

                     $count++;
                     $earnings['day'.$day] = $sub_totals;
                     if(empty($earnings['day'.$day]))
                     {
                        $earnings['day'.$day] = 0;
                     }
                     $total_earnings += $sub_totals;
                  }
                ?>

            var bardata = [ <?php $count = 0; foreach($days as $day){
                      $count++;

                      $label = "";
                      switch($count)
                      {
                        case 1:
                          $label = "Mon";
                          break;
                        case 2:
                          $label = "Tue";
                           break;
                        case 3:
                          $label = "Wed";
                          break;
                        case 4:
                          $label = "Thu";
                          break;
                        case 5:
                          $label = "Fri";
                          break;
                        case 6:
                          $label = "Sat";
                          break;
                        case 7:
                          $label = "Sun";
                          break;
                      }

                  ?>["<?php echo $label; ?>", <?php echo $earnings['day'.$day]; ?>], <?php } ?> ];

            jQuery.plot("#barchart2<?php echo $currency; ?>", [ bardata ], {
                series: {
                    lines: {
                        lineWidth: 1
                    },
                    bars: {
                        show: true,
                        barWidth: 0.5,
                        align: "center",
                        lineWidth: 0,
                        fillColor: "#428BCA"
                    }
                },
                grid: {
                    borderColor: '#ddd',
                    borderWidth: 1,
                    labelMargin: 10
                },
                xaxis: {
                    mode: "categories",
                    tickLength: 0
                }
            });

            function labelFormatter(label, series) {
                return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
            }

        });
    </script>


    <div class="tinystat mr20">
        <div class="datainfo">
            <span class="text-muted">THIS WEEKS Total</span>
            <h4><?php echo $currency.". ".$total_earnings; ?></h4>
        </div>
    </div>


    <div id="barchart2<?php echo $currency; ?>" style="width: 100%; height: 300px"></div>


    <div style="margin-top:20px"></div>

</div>
<?php } ?>
<?php if($_GET['tab'] == "thismonth"){ ?>
<div id="thismonth<?php echo $currency; ?>" class="tab-pane active">

    <h5 class="subtitle mb5">Month:
        <select name='choosemonth<?php echo $currency; ?>' id='choosemonth<?php echo $currency; ?>'>
            <?php
            for($x = 1; $x <= 12; $x++)
            {
                $selected = "";
                if($x == date("m"))
                {
                    $selected = "selected='selected'";
                }
                echo "<option value='".$x."' ".$selected.">".$x."</option>";
            }
            ?>
        </select>
        <script>
            jQuery(document).ready(function(){
                $("#choosemonth<?php echo $currency; ?>").on('change', function() {
                    $("#loadmonth<?php echo $currency; ?>").load("/backend.php/dashboard/fetchmonth/month/" + this.value + "/currency/<?php echo $currency; ?>");
                });
            });
        </script>
    </h5>
    <div id="loadmonth<?php echo $currency; ?>" name="loadmonth<?php echo $currency; ?>">
        <div id="barchart1<?php echo $currency; ?>" style="width: 100%; height: 300px"></div>
        <script>
            jQuery(document).ready(function(){
                /***** BAR CHART *****/

                    <?php
                      $earnings = null;

                      $search = null;
                      $total_earnings = 0;

                      for($day = 1; $day <= 31; $day++)
                      {
                        if($day < 10)
                        {
                            $search = date('Y')."-".date('m')."-0".$day;
                        }
                        else
                        {
                            $search = date('Y')."-".date('m')."-".$day;
                        }

                        $sql = "SELECT SUM(a.payment_amount) AS total_amount, COUNT(*) AS total_apps
                                FROM ap_form_payments a
                                WHERE a.payment_date LIKE '%".$search."%' AND a.status = 2 AND a.payment_currency = '".$currency."'";
                        $results = mysql_query($sql, $dbconn);

                        $sub_totals = 0;

                        while($row = mysql_fetch_assoc($results))
                        {
                           if($currency == "KES" && sfConfig::get('app_sso_secret'))
                           {
                              $sub_totals += $row['total_amount'] - ($row['total_apps'] * 50);
                           }
                           elseif($currency == "USD" && sfConfig::get('app_sso_secret'))
                           {
                              $sub_totals += $row['total_amount'] - ($row['total_apps'] * 1);
                           }
                           else
                           {
                             $sub_totals += $row['total_amount'];
                           }
                        }

                         $earnings['day'.$day] = $sub_totals;
                         if(empty($earnings['day'.$day]))
                         {
                            $earnings['day'.$day] = 0;
                         }
                         $total_earnings += $sub_totals;
                      }
                    ?>

                var bardata = [ <?php for($day = 1; $day <= 31; $day++){ ?>["<?php echo $day; ?>", <?php echo $earnings['day'.$day]; ?>], <?php } ?> ];

                jQuery.plot("#barchart1<?php echo $currency; ?>", [ bardata ], {
                    series: {
                        lines: {
                            lineWidth: 1
                        },
                        bars: {
                            show: true,
                            barWidth: 0.5,
                            align: "center",
                            lineWidth: 0,
                            fillColor: "#428BCA"
                        }
                    },
                    grid: {
                        borderColor: '#ddd',
                        borderWidth: 1,
                        labelMargin: 10
                    },
                    xaxis: {
                        mode: "categories",
                        tickLength: 0
                    }
                });

                function labelFormatter(label, series) {
                    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
                }

            });
        </script>
        <div class="tinystat mr20" style="margin-top:40px">

            <div class="datainfo">
                <span class="text-muted">Total</span>
                <h4><?php echo $currency.". ".$total_earnings; ?></h4>
            </div>
        </div>



    </div>

</div>
<?php } ?>
<?php if($_GET['tab'] == "thisyear"){ ?>
<div id="thisyear<?php echo $currency; ?>" class="tab-pane active">

    <?php $year = date("Y"); ?>
    <h5 class="subtitle mt10">Year:
        <select name='chooseyear<?php echo $currency; ?>' id='chooseyear<?php echo $currency; ?>'>
            <?php
            for($x = $year; $x > 2008; $x--)
            {
                echo "<option value='".$x."'>".$x."</option>";
            }
            ?>
        </select>
        <script>
            jQuery(document).ready(function(){
                $("#chooseyear<?php echo $currency; ?>").on('change', function() {
                    $("#loadyear<?php echo $currency; ?>").load("/backend.php/dashboard/fetchyear/year/" + this.value + "/currency/<?php echo $currency; ?>");
                });
            });
        </script>
    </h5>
    <div id="loadyear<?php echo $currency; ?>" name="loadyear<?php echo $currency; ?>">
        <div id="barcharty<?php echo $currency; ?>" style="width: 100%; height: 300px"></div>

        <script>
            jQuery(document).ready(function(){
                /***** BAR CHART *****/

                    <?php
                      $earnings = null;
                      $earnings['month1'] = 0;
                      $earnings['month2'] = 0;
                      $earnings['month3'] = 0;
                      $earnings['month4'] = 0;
                      $earnings['month5'] = 0;
                      $earnings['month6'] = 0;
                      $earnings['month7'] = 0;
                      $earnings['month8'] = 0;
                      $earnings['month9'] = 0;
                      $earnings['month10'] = 0;
                      $earnings['month11'] = 0;
                      $earnings['month12'] = 0;

                      $search = null;
                      $total_earnings = 0;

                      for($month = 1; $month <= 12; $month++)
                      {
                        if($month < 10)
                        {
                          $search = $year."-0".$month."%";
                        }
                        else
                        {
                          $search = $year."-".$month."%";
                        }

                        $sql = "SELECT SUM(a.payment_amount) AS total_amount, COUNT(*) AS total_apps
                                FROM ap_form_payments a
                                WHERE a.payment_date LIKE '".$search."' AND a.status = 2 AND a.payment_currency = '".$currency."'";
                        $results = mysql_query($sql, $dbconn);

                        $sub_totals = 0;

                        while($row = mysql_fetch_assoc($results))
                        {
                           if($currency == "KES" && sfConfig::get('app_sso_secret'))
                           {
                              $sub_totals += $row['total_amount'] - ($row['total_apps'] * 50);
                           }
                           elseif($currency == "USD" && sfConfig::get('app_sso_secret'))
                           {
                              $sub_totals += $row['total_amount'] - ($row['total_apps'] * 1);
                           }
                           else
                           {
                             $sub_totals += $row['total_amount'];
                           }
                        }

                         $earnings['month'.$month] = $sub_totals;
                         if(empty($earnings['month'.$month]))
                         {
                            $earnings['month'.$month] = 0;
                         }
                         $total_earnings += $sub_totals;
                      }
                    ?>

                var bardata = [ ["Jan", <?php echo $earnings['month1']; ?>], ["Feb", <?php echo $earnings['month2']; ?>], ["Mar", <?php echo $earnings['month3']; ?>], ["Apr", <?php echo $earnings['month4']; ?>], ["May", <?php echo $earnings['month5']; ?>], ["Jun", <?php echo $earnings['month6']; ?>], ["Jul", <?php echo $earnings['month7']; ?>], ["Aug", <?php echo $earnings['month8']; ?>], ["Sep", <?php echo $earnings['month9']; ?>], ["Oct", <?php echo $earnings['month10']; ?>], ["Nov", <?php echo $earnings['month11']; ?>], ["Dec", <?php echo $earnings['month12']; ?>] ];

                jQuery.plot("#barcharty<?php echo $currency; ?>", [ bardata ], {
                    series: {
                        lines: {
                            lineWidth: 1
                        },
                        bars: {
                            show: true,
                            barWidth: 0.5,
                            align: "center",
                            lineWidth: 0,
                            fillColor: "#428BCA"
                        }
                    },
                    grid: {
                        borderColor: '#ddd',
                        borderWidth: 1,
                        labelMargin: 10
                    },
                    xaxis: {
                        mode: "categories",
                        tickLength: 0
                    }
                });

                function labelFormatter(label, series) {
                    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
                }

            });
        </script>

        <div class="tinystat mr20" style="margin-top:40px">
            <div class="datainfo">
                <span class="text-muted">Total</span>
                <h4><?php echo $currency.". ".$total_earnings; ?></h4>
            </div>
        </div>



    </div>


</div>
<?php } ?>
</div>


</div></div></div>



</div><!-- panel-body -->
</div><!-- panel-default-special -->
</div><!-- cotentpanel -->
</div><!-- row -->
