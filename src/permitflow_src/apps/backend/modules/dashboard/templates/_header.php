<?php
/**
 * _header.php template.
 *
 * Displays the header on the layout
 *
 * @package    backend
 * @subpackage dashboard
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
 use_helper('I18N');
?>
<div class="leftpanel">
<!--OTB Start Patch - Multiagency fucntionality, stage listing check-->
<?php
$agency_manager = new AgencyManager();
$logo = $agency_manager->getLogo($sf_user->getAttribute('userid'));
?>
<!--OTB End Patch - Multiagency fucntionality, stage listing check-->

  <div class="logopanel">
      <h1> <img src="<?php echo $logo; ?>" alt="" /></h1><!--OTB Patch - Multiagency fucntionality, stage listing check-->
  </div><!-- logopanel -->

  
  <?php
  //Displays the sidemenu
  include_component('dashboard', 'sidemenu');
  ?>

  <div class="mainpanel">
    <div class="headerbar">

      <a class="menutoggle"><i class="fa fa-bars"></i></a>

      <form class="searchform" action="<?php echo public_path(); ?>backend.php/applications/search" method="post">
        <input type="text" class="form-control" name="applicationid" placeholder="<?php echo __('Search here...'); ?>" />
      </form>

      <form class="searchform" action="<?php echo public_path(); ?>backend.php/applications/search" method="post">
        <a href="<?php echo public_path(); ?>backend.php/applications/search?search=adv" class="pull-left" style="padding: 15px;">Advanced Search</a>
      </form>
      <div class="header-right">
        <ul class="headermenu">
          <!-- OTB patch - addition of below section from mombasa cp -->
        <ul class="headermenu">
         <li>
           <?php
           //$agency_manager = new AgencyManager(); OTB patch
           $stage_ids = $agency_manager->getAllowedStages($sf_user->getAttribute('userid'));
		  $q = Doctrine_Query::create()
		    ->from('Communications a')
        ->leftJoin('a.FormEntry b')
       // ->where('b.approved <> 0 ')
        ->where('b.approved in ('. implode(',', $stage_ids).') ')
		    ->andWhere('a.architect_id <> ?', "")
                   // ->andWhere('b.approved in('. implode(',', $stage_ids).') ')
            ->andWhere('a.messageread = ?', 0);
		  $newmessages = $q->count();
		  ?>
            <div class="btn-group">
              <button class="btn btn-default dropdown-toggle tp-icon" data-toggle="dropdown" onClick="window.location='<?php echo public_path("/backend.php/applications/messages"); ?>';">
                <i class="glyphicon glyphicon-envelope"></i>
                <?php if($newmessages){ ?>
                <span class="badge"><?php echo $newmessages; ?></span>
                <?php } ?>
              </button>
            </div>
          </li>
          <li>
          <?php
		  $q = Doctrine_Query::create()
		  	->from("Alerts a")
			->where("a.userid = ?", $sf_user->getAttribute('userid'))
			->andWhere("a.isread = ?", "0")
			->orderBy("a.id DESC");
		  $newalerts = $q->count();
		  ?>
            <div class="btn-group">
              <button class="btn btn-default dropdown-toggle tp-icon" data-toggle="dropdown" onClick="window.location='<?php echo public_path("/backend.php/applications/notifications"); ?>';">
                <i class="glyphicon glyphicon-globe"></i>
                <?php
				if($newalerts)
				{
				?>
                	<span class="badge"><?php echo $newalerts; ?></span>
                <?php
				}
				?>
              </button>
            </div>
          </li>
          <li>
            <div class="btn-group">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                <?php
                $q = Doctrine_Query::create()
                         ->from('CfUser a')
                         ->where('a.nid = ?', $sf_user->getAttribute('userid'));
                $logged_reviewer = $q->fetchOne();

                if(empty($logged_reviewer))
                {
                        ?>
                        <script language='javascript'>
                         window.location="/backend.php/login/logout";
                        </script>
                        <?php
                        exit;
                }

                if($logged_reviewer->getProfilePic())
                {
                    $q = Doctrine_Query::create()
                       ->from("ApSettings a")
                       ->where("a.id = 1")
                       ->orderBy("a.id DESC");
                    $aplogo = $q->fetchOne();
                    if($aplogo && $aplogo->getProfileDir())
                    {
                      ?>
                      <!-- <img src="<?php // echo public_path() ?><?php //echo $aplogo->getUploadDir() ?>/reviewer/profile/<?php //echo $logged_reviewer->getStruserid()  ?>/<?php //echo $logged_reviewer->getProfilePic(); ?>" width="200px;" class="thumbnail img-responsive mb0" alt="No image" />
    		        -->
                        <?php echo __('Account Settings') ?>
                      <?php
                    }
                }
                else
                {
				?>
                <img src="<?php echo public_path(); ?>assets_unified/images/photos/loggeduser.png" alt="" />
                <b><?php  echo $logged_reviewer->getStrfirstname()." ".$logged_reviewer->getStrlastname(); ?></b>
                <?php
                }
                ?>
                
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                <li><a href="<?php echo public_path(); ?>backend.php/users/viewuser/userid/<?php echo $sf_user->getAttribute('userid'); ?>"><i class="glyphicon glyphicon-cog"></i> <?php echo __('My Account'); ?></a></li>
                <li><a href="/backend.php/login/forgot"><i class="glyphicon glyphicon-adjust"></i> <?php echo __('Change Password'); ?></a></li>
                <li><a href="<?php echo public_path(); ?>backend.php/help/index"><i class="glyphicon glyphicon-question-sign"></i> <?php echo __('Help'); ?></a></li>
                <li><a href="<?php echo public_path(); ?>backend.php/login/logout"><i class="glyphicon glyphicon-log-out"></i> <?php echo __('Log Out'); ?></a></li>
              </ul>
            </div>
          </li>
          <li>
            <button id="chatview" class="btn btn-default tp-icon chat-icon">
                <i class="glyphicon glyphicon-comment"></i>
            </button>
          </li>
        </ul>
      </div><!-- header-right -->

    </div><!-- headerbar -->
