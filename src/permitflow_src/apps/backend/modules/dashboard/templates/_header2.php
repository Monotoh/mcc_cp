<?php
 use_helper('I18N');
$agency_manager = new AgencyManager();
$logo = $agency_manager->getLogo($sf_user->getAttribute('userid'));
?>
<!-- BEGIN HEADER -->
<div style="background-color:#fff;" class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a style="margin-top: 0px;" href="#">
			<img width="150px;" height="30px" src="<?php echo public_path(); ?>assets_unified/images/mcclogo2.png" alt="logo" class="logo-default"/>
			</a>
			<div class="menu-toggler sidebar-toggler">
				<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
			</div>
		</div>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		
		<!-- BEGIN PAGE TOP -->
		<div class="page-top">
			<!-- BEGIN HEADER SEARCH BOX -->
			<!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->
			<form class="search-form search-form-expanded" action="<?php echo public_path(); ?>backend.php/applications/search" method="post">
				<div style="background-color:#fff;" class="input-group">
					<input type='text' name='applicationid' id='applicationid' type="text" class="form-control" placeholder="Search..." name="query">
					<span class="input-group-btn">
					<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
					</span>
				</div>
			</form>
             <!-- Advanced search -->
			 <form class="searchform" action="<?php echo public_path(); ?>backend.php/applications/search" method="post">
                                                <a href="<?php echo public_path(); ?>backend.php/applications/search?search=adv" class="pull-left" style="padding: 15px;">Advanced Search</a>
            </form>
			<!-- END HEADER SEARCH BOX -->
			<!-- BEGIN TOP NAVIGATION MENU -->
			<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
                              <?php
                                //$agency_manager = new AgencyManager(); OTB patch
                                $stage_ids = $agency_manager->getAllowedStages($sf_user->getAttribute('userid'));
                                       $q = Doctrine_Query::create()
                                         ->from('Communications a')
                             ->leftJoin('a.FormEntry b')
                            ->where('b.approved <> 0 ')
                            // ->where('b.approved in ('. implode(',', $stage_ids).') ')
                                         ->andWhere('a.architect_id <> ?', "")
                                        // ->andWhere('b.approved in('. implode(',', $stage_ids).') ')
                                 ->andWhere('a.messageread = ?', 0);
                                       $newmessages = $q->count();
                                       //
                                       $results = $q->execute();
		             ?>
				<!-- BEGIN NOTIFICATION DROPDOWN -->
				<li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<i class="icon-bell"></i>
					<span class="badge badge-default">
					<?php echo $newmessages; ?> </span>
					</a>
					<ul class="dropdown-menu">
						<li>
							<p>
								 You have <?php echo $newmessages; ?> new notifications
							</p>
						</li>
						
						<li class="external">
							<a  href="/backend.php/applications/messages">
							Go to notifications <i class="m-icon-swapright"></i>
							</a>
						</li>
					</ul>
				</li>
				<!-- END NOTIFICATION DROPDOWN -->
				
				
				<!-- BEGIN USER LOGIN DROPDOWN -->
				<li class="dropdown dropdown-user">
                                   <?php
                                    $q = Doctrine_Query::create()
                                            ->from('CfUser a')
                                            ->where('a.nid = ?', $sf_user->getAttribute('userid'));
                                   $logged_reviewer = $q->fetchOne();
                                   ?>
					<a style="background-color:#fff;" href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<img alt="" class="img-circle hide1" src="<?php echo public_path(); ?>assets_unified/images/photos/loggeduser.png"/>
					<span class="username username-hide-on-mobile">
					<?php  echo $logged_reviewer->getStrfirstname()." ".$logged_reviewer->getStrlastname(); ?> </span>
					<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu">
						<li><a href="<?php echo public_path(); ?>backend.php/users/viewuser/userid/<?php echo $sf_user->getAttribute('userid'); ?>"><i class="glyphicon glyphicon-cog"></i> <?php echo __('My Account'); ?></a></li>
                                                <li><a href="/backend.php/login/forgot"><i class="glyphicon glyphicon-adjust"></i> <?php echo __('Change Password'); ?></a></li>
                                                <li><a href="<?php echo public_path(); ?>backend.php/help/index"><i class="glyphicon glyphicon-question-sign"></i> <?php echo __('Help'); ?></a></li>
                                                <li><a href="<?php echo public_path(); ?>backend.php/login/logout"><i class="glyphicon glyphicon-log-out"></i> <?php echo __('Log Out'); ?></a></li>
					</ul>
				</li>
				<!-- END USER LOGIN DROPDOWN -->
				
			</ul>
			</div>
			<!-- END TOP NAVIGATION MENU -->
		</div>
		<!-- END PAGE TOP -->
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->