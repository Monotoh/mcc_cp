<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<script src="<?php echo public_path(); ?>asset_unified/js/jquery.min.js"></script>

<!-- Datatables-->
<script src="<?php echo public_path(); ?>asset_unified/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo public_path(); ?>asset_unified/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="<?php echo public_path(); ?>asset_unified/plugins/datatables/dataTables.buttons.min.js"></script>

<!-- Datatable init js -->
<script src="<?php echo public_path(); ?>asset_unified/pages/datatables.init.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({keys: true});
        $('#datatable-responsive').DataTable();
        $('#datatable-scroller').DataTable({ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true});
        var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
    });
    TableManageButtons.init();

</script>
