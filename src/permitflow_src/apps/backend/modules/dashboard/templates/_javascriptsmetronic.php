<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo public_path(); ?>metronicv3/assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="<?php echo public_path(); ?>metronicv3/assets/global/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!--[if lt IE 9]>
<script src="<?php echo public_path(); ?>metronicv3/assets/global/plugins/respond.min.js"></script>
<script src="<?php echo public_path(); ?>metronicv3/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->


<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo public_path(); ?>metronicv3/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?php echo public_path(); ?>metronicv3/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo public_path(); ?>metronicv3/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo public_path(); ?>metronicv3/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo public_path(); ?>metronicv3/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo public_path(); ?>metronicv3/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo public_path(); ?>metronicv3/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo public_path(); ?>metronicv3/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script type="text/javascript" src="<?php echo public_path(); ?>metronicv3/assets/global/plugins/select2/select2.min.js"></script>
<script src="<?php echo public_path(); ?>metronicv3/assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo public_path(); ?>metronicv3/assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo public_path(); ?>metronicv3/assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="<?php echo public_path(); ?>metronicv3/assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<!-- table data -->
<script type="text/javascript" src="<?php echo public_path(); ?>metronicv3/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo public_path(); ?>metronicv3/assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo public_path(); ?>metronicv3/assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo public_path(); ?>metronicv3/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<!-- Editor tools --> 
<script src="<?php echo public_path(); ?>assets_unified/js/wysihtml5-0.3.0.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/bootstrap-wysihtml5.js"></script>

<!-- chart ploting --> 
<script src="<?php echo public_path(); ?>assets_unified/js/flot/jquery.flot.min.js"></script>

<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core components
Layout.init(); // init current layout
QuickSidebar.init(); // init quick sidebar
Demo.init(); // init demo features

  // $('.page-sidebar .ajaxify.start').click() // load the content for the dashboard page.

  //alert("Good work!") ;
});
</script>
<!-- From old view -->
<?php
if($sf_context->getModuleName() == "tasks")
{
?>
<script src="<?php echo public_path(); ?>assets_unified/js/slide-in/js/main.js"></script> <!-- Resource jQuery NEW JS -->
<script src="<?php echo public_path(); ?>assets_unified/js/slide-in/js/modernizr.js"></script> <!-- Modernizr NEW JS -->
<?php
}
?>

<?php
if(($sf_context->getModuleName() == "applications" && $sf_context->getActionName() == "view") || ($sf_context->getModuleName() == "permits" && $sf_context->getActionName() == "view") || ($sf_context->getModuleName() == "invoices" && $sf_context->getActionName() == "view"))
{
?>
<!-- In production, only one script (pdf.js) is necessary -->
<!-- In production, change the content of PDFJS.workerSrc below -->
<script src="<?php echo public_path(); ?>assets_unified/pdfjs/src/shared/util.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/pdfjs/src/display/api.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/pdfjs/src/display/metadata.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/pdfjs/src/display/canvas.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/pdfjs/src/display/webgl.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/pdfjs/src/display/pattern_helper.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/pdfjs/src/display/font_loader.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/pdfjs/src/display/annotation_helper.js"></script>

<script>
  // Specify the main script used to create a new PDF.JS web worker.
  // In production, leave this undefined or change it to point to the
  // combined `pdf.worker.js` file.
  PDFJS.workerSrc = '<?php echo public_path(); ?>assets_unified/pdfjs/src/worker_loader.js';
</script>
<?php
}
?>


<script src="<?php echo public_path(); ?>elm/elm.js"></script>


<script type="text/javascript" src="<?php echo public_path(); ?>assets_unified/js/jquery.dualListBox.js"></script>


<script>
jQuery(document).ready(function(){

    "use strict";

    // Select2
  jQuery(".select2").select2({
    width: '100%',
    minimumResultsForSearch: -1
  });

});
</script>

<script>
jQuery(document).ready(function(){

  // HTML5 WYSIWYG Editor
  jQuery('#wysiwyg').wysihtml5({color: true,html:true});
 

});
</script>


<script>
  jQuery(document).ready(function() {

    jQuery('#table1').dataTable();

  	jQuery('#table2').dataTable({
      "sPaginationType": "full_numbers",

      // Using aoColumnDefs
      "aoColumnDefs": [
      	{ "bSortable": false, "aTargets": [ 'no-sort' ] }
    	]
    });
    
    jQuery('#table3').dataTable({
      "sPaginationType": "full_numbers",

      // Using aoColumnDefs
      "aoColumnDefs": [
      	{ "bSortable": false, "aTargets": [ 'no-sort' ] }
    	]
    });
    
     jQuery('#table4').dataTable({
      "sPaginationType": "full_numbers",

      // Using aoColumnDefs
      "aoColumnDefs": [
      	{ "bSortable": false, "aTargets": [ 'no-sort' ] }
    	]
    });

  jQuery('#table5').dataTable({
      "sPaginationType": "full_numbers",

      // Using aoColumnDefs
      "aoColumnDefs": [
      	{ "bSortable": false, "aTargets": [ 'no-sort' ] }
    	]
    });
    
    jQuery('#table6').dataTable({
      "sPaginationType": "full_numbers",

      // Using aoColumnDefs
      "aoColumnDefs": [
      	{ "bSortable": false, "aTargets": [ 'no-sort' ] }
    	]
    });


    // Delete row in a table
    jQuery('.delete-row').click(function(){
      var c = confirm("Continue delete?");
      if(c)
        jQuery(this).closest('tr').fadeOut(function(){
          jQuery(this).remove();
        });

        return false;
    });

    // Show aciton upon row hover
    jQuery('.table-hidaction tbody tr').hover(function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 1});
    },function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 0});
    });


  });
</script>

<script>
  jQuery(document).ready(function(){

    //jQuery("a[rel^='prettyPhoto']").prettyPhoto();

    //Replaces data-rel attribute to rel.
    //We use data-rel because of w3c validation issue
    jQuery('a[data-rel]').each(function() {
        jQuery(this).attr('rel', jQuery(this).data('rel'));
    });

     // Basic Wizard
	 //jQuery('#basicWizard').bootstrapWizard();


	  // Date Picker
	  jQuery('#from_dateblt1').datepicker();
	  jQuery('#to_date').datepicker();
	  jQuery('#from_date1').datepicker();
	  jQuery('#to_date1').datepicker();
	  jQuery('#from_date8').datepicker();
	  jQuery('#to_date8').datepicker();
	  jQuery('#from_date10').datepicker();
	  jQuery('#to_date10').datepicker();
	  jQuery('#from_date12').datepicker();
	  jQuery('#to_date12').datepicker();
	  jQuery('#from_date_r2').datepicker();
	  jQuery('#to_date_r2').datepicker();
	  jQuery('#from_date_r3').datepicker();
	  jQuery('#to_date_r3').datepicker();
	  jQuery('#from_date_r5').datepicker();
	  jQuery('#to_date_r5').datepicker();
	  jQuery('#from_date_r6').datepicker();
	  jQuery('#to_date_r6').datepicker();
	  jQuery('#from_date_r8').datepicker();
	  jQuery('#to_date_r8').datepicker();
	  jQuery('#from_date_r9').datepicker();
	  jQuery('#to_date_r9').datepicker();
	  jQuery('#from_date_r10').datepicker();
	  jQuery('#to_date_r10').datepicker();
    jQuery('#from_date17').datepicker();
    jQuery('#to_date17').datepicker();
    jQuery('#from_date18').datepicker();
    jQuery('#to_date18').datepicker();
    jQuery('#from_date19').datepicker();
    jQuery('#to_date19').datepicker();
	  jQuery('#announcements_start_date').datepicker();
	  jQuery('#announcements_end_date').datepicker();

  });

function batch(module, title, action)
{
        var inputs = document.getElementsByTagName("input"); //or document.forms[0].elements;
        var cbs = []; //will contain all checkboxes
        var checked = []; //will contain all checked checkboxes
        for (var i = 0; i < inputs.length; i++) {
          if (inputs[i].type == "checkbox") {
                cbs.push(inputs[i]);
                if (inputs[i].checked) {
                    var url = '/backend.php/' + module + '/batch';
                    ajaxupdate(url, action, inputs[i].value);
                }
          }
        }
        var nbCbs = cbs.length; //number of checkboxes
        var nbChecked = checked.length; //number of checked checkboxes
}

function ajaxupdate(strURL, action, id) {
        var xmlHttpReq1 = false;
        var self1 = this;
        // Mozilla/Safari

        if (window.XMLHttpRequest) {
                self.xmlHttpReq1 = new XMLHttpRequest();
        }
        // IE
        else if (window.ActiveXObject) {
                self.xmlHttpReq1 = new ActiveXObject("Microsoft.XMLHTTP");
        }
        self.xmlHttpReq1.open('POST', strURL, true);
        self.xmlHttpReq1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        self.xmlHttpReq1.onreadystatechange = function() {

        }
        self.xmlHttpReq1.send(action + '=' + id);
        if(action == 'delete'){
          document.getElementById('row_' + id).style.display = 'none';
        }
        else
        {
          location.reload();
        }
        // document.getElementById('notifications').innerHTML = "<div class='alert success'>Selected entries have been successfully updated.</div>";

}

function ajaxselect(strURL, div) {
    var xmlHttpReq1 = false;
    var self1 = this;
    // Mozilla/Safari

    if (window.XMLHttpRequest) {
      self.xmlHttpReq1 = new XMLHttpRequest();
    }
    // IE
    else if (window.ActiveXObject) {
      self.xmlHttpReq1 = new ActiveXObject("Microsoft.XMLHTTP");
    }
    self.xmlHttpReq1.open('POST', strURL, true);
    self.xmlHttpReq1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    self.xmlHttpReq1.onreadystatechange = function() {
      if (self.xmlHttpReq1.readyState == 4) {
        document.getElementById(div).innerHTML = "<span class='glyphicon glyphicon-ok'></span>";
      }
      else
      {
        document.getElementById(div).innerHTML = "<img src='/assets_unified/images/loaders/loader1.gif' alt=''> Loading, Please wait...";
      }
    }
    self.xmlHttpReq1.send();
  }

function ajaxunselect(strURL, div) {
    var xmlHttpReq1 = false;
    var self1 = this;
    // Mozilla/Safari

    if (window.XMLHttpRequest) {
      self.xmlHttpReq1 = new XMLHttpRequest();
    }
    // IE
    else if (window.ActiveXObject) {
      self.xmlHttpReq1 = new ActiveXObject("Microsoft.XMLHTTP");
    }
    self.xmlHttpReq1.open('POST', strURL, true);
    self.xmlHttpReq1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    self.xmlHttpReq1.onreadystatechange = function() {
      if (self.xmlHttpReq1.readyState == 4) {
        document.getElementById(div).innerHTML = "<span class='glyphicon glyphicon-remove'></span>";
      }
      else
      {
        document.getElementById(div).innerHTML = "<img src='/assets_unified/images/loaders/loader1.gif' alt=''> Loading, Please wait...";
      }
    }
    self.xmlHttpReq1.send();
  }


  function ajaxresolve(strURL, div) {
    var xmlHttpReq1 = false;
    var self1 = this;
    // Mozilla/Safari

    if (window.XMLHttpRequest) {
      self.xmlHttpReq1 = new XMLHttpRequest();
    }
    // IE
    else if (window.ActiveXObject) {
      self.xmlHttpReq1 = new ActiveXObject("Microsoft.XMLHTTP");
    }
    self.xmlHttpReq1.open('POST', strURL, true);
    self.xmlHttpReq1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    self.xmlHttpReq1.onreadystatechange = function() {
      if (self.xmlHttpReq1.readyState == 4) {
        document.getElementById(div).innerHTML = "<span class='glyphicon glyphicon-ok'></span> Resolved.";
      }
      else
      {
        document.getElementById(div).innerHTML = "<img src='/assets_unified/images/loaders/loader1.gif' alt=''> Loading, Please wait...";
      }
    }
    self.xmlHttpReq1.send();
  }

  function ajaxunresolve(strURL, div) {
    var xmlHttpReq1 = false;
    var self1 = this;
    // Mozilla/Safari

    if (window.XMLHttpRequest) {
      self.xmlHttpReq1 = new XMLHttpRequest();
    }
    // IE
    else if (window.ActiveXObject) {
      self.xmlHttpReq1 = new ActiveXObject("Microsoft.XMLHTTP");
    }
    self.xmlHttpReq1.open('POST', strURL, true);
    self.xmlHttpReq1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    self.xmlHttpReq1.onreadystatechange = function() {
      if (self.xmlHttpReq1.readyState == 4) {
        document.getElementById(div).innerHTML = "<span class='glyphicon glyphicon-remove'></span> Not Resolved.";
      }
      else
      {
        document.getElementById(div).innerHTML = "<img src='/assets_unified/images/loaders/loader1.gif' alt=''> Loading, Please wait...";
      }
    }
    self.xmlHttpReq1.send();
  }

  function ajaxSearchform1(formid) {
    var xmlHttpReq1 = false;
    var self1 = this;
    // Mozilla/Safari

    if (window.XMLHttpRequest) {
        self.xmlHttpReq1 = new XMLHttpRequest();
    }
    // IE
    else if (window.ActiveXObject) {
        self.xmlHttpReq1 = new ActiveXObject("Microsoft.XMLHTTP");
    }
    self.xmlHttpReq1.open('POST', '/backend.php/applications/formsearch/formid/' + formid, true);
    self.xmlHttpReq1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    self.xmlHttpReq1.onreadystatechange = function() {
        if (self.xmlHttpReq1.readyState == 4) {
            document.getElementById('ajaxsearchform').innerHTML = self.xmlHttpReq1.responseText;
        }
        else
        {
            document.getElementById('ajaxsearchform').innerHTML = '<img src="/asset_pics/loading.gif">';

        }
    }

    self.xmlHttpReq1.send('formid=' + formid);

}

</script>
<!-- END JAVASCRIPTS -->