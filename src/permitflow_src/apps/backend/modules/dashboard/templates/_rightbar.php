
<div class="rightpanel">
<!-- Nav tabs -->
<ul class="nav nav-tabs nav-justified">
    <li class="active"><a href="#rp-alluser" data-toggle="tab"><i class="fa fa-users"></i></a></li>
    <li><a href="#rp-favorites" data-toggle="tab"><i class="fa fa-heart"></i></a></li>
    <li><a href="#rp-history" data-toggle="tab"><i class="fa fa-clock-o"></i></a></li>
    <li><a href="#rp-settings" data-toggle="tab"><i class="fa fa-gear"></i></a></li>
</ul>
    
<!-- Tab panes -->
<div class="tab-content">
    <div class="tab-pane active" id="rp-alluser">
        <h5 class="sidebartitle">Online Users</h5>
        <ul class="chatuserlist">
        <?php
		 $q = Doctrine_Query::create()
			->from('CfUser a')
			->where('a.nid = ?', $sf_user->getAttribute('userid'));
		 $logged_reviewer = $q->fetchOne();
		
		 $q = Doctrine_Query::create()
			->from("CfUser a")
			->where("a.bdeleted = ?", "0")
			->andWhere("a.strdepartment LIKE ?", "%".$logged_reviewer->getStrdepartment()."%")
			->orderBy("a.strfirstname ASC");
	     $allusers = $q->execute();
		 
		 foreach($allusers as $user)
		 {
			 if(($user->getTslastaction() + 7200) > time())
			 {
			 ?>
				<li class="online">
					<div class="media">
						<a href="#" class="pull-left media-thumb">
                        <?php
                             if($user->getProfilePic())
                             {
                             ?>
                             <img src="/asset_uplds/photos/<?php echo $user->getProfilePic(); ?>" class="media-object"/>
                             <?php
                             }
                             else
                             {
                             ?>
							 <img alt="" src="<?php echo public_path(); ?>assets_unified/images/photos/userprofile.png" class="media-object">
						     <?php
                             }
                        ?>
                        </a>
						<div class="media-body">
							<strong><?php echo $user->getStrfirstname()." ".$user->getStrlastname(); ?></strong>
							<small><?php echo $user->getStrdepartment(); ?></small>
						</div>
					</div><!-- media -->
				</li>
			<?php
			 }
		 }
		 ?>
        </ul>
        
        <div class="mb30"></div>
        
        <h5 class="sidebartitle">Offline Users</h5>
        <ul class="chatuserlist">
        	<?php
			$q = Doctrine_Query::create()
				->from("CfUser a")
				->where("a.bdeleted = ?", "0")
				->andWhere("a.strdepartment LIKE ?", "%".$logged_reviewer->getStrdepartment()."%")
				->orderBy("a.strfirstname ASC");
			 $allusers = $q->execute();
			 
			 foreach($allusers as $user)
			 {
				 if(!(($user->getTslastaction() + 7200) > time()))
				 {
				 ?>
				<li>
					<div class="media">
						<a href="#" class="pull-left media-thumb">
                        <?php
                         if($user->getProfilePic())
                         {
                         ?>
                             <img src="/asset_uplds/photos/<?php echo $user->getProfilePic(); ?>" class="media-object"/>
                         <?php
                         }
                         else
                         {
                         ?>
							<img alt="" src="<?php echo public_path(); ?>assets_unified/images/photos/user5.png" class="media-object">
                         <?php
                         }
                         ?>
                        </a>
						<div class="media-body">
							<strong><?php echo $user->getStrfirstname()." ".$user->getStrlastname(); ?></strong>
							<small><?php echo $user->getStrdepartment(); ?></small>
						</div>
					</div><!-- media -->
				</li>
				<?php
				 }
			 }
			 ?>
        </ul>
    </div>
    <div class="tab-pane" id="rp-favorites">
        <h5 class="sidebartitle">Starred</h5>
        <ul class="chatuserlist">
        <?php 
		$q = Doctrine_Query::create()
		   ->from("Favorites a")
		   ->where("a.userid = ?", $sf_user->getAttribute('userid'))
		   ->orderBy("a.id ASC");
		 $favorites = $q->execute();
		   
		 foreach($favorites as $favorite)
		 {
			$q = Doctrine_Query::create()
				->from("FormEntry a")
				->where("a.id = ?", $favorite->getApplicationId());
			$application = $q->fetchOne();
			if($application)
			{
				$q = Doctrine_Query::create()
					->from("ApForms a")
					->where("a.form_id = ?", $application->getFormId());
			    $form = $q->fetchOne();
				if($form)
				{
		?>
            <li class="online">
                <div class="media">
                    <a href="<?php echo public_path(); ?>backend.php/favorites/view/id/<?php echo $favorite->getId(); ?>" class="pull-left media-thumb">
                        <img alt="" src="<?php echo public_path(); ?>assets_unified/images/photos/user2.png" class="media-object">
                    </a>
                    <div class="media-body">
                        <strong><a href="<?php echo public_path(); ?>backend.php/applications/view/id/<?php echo $application->getId(); ?>"><?php echo $application->getApplicationId(); ?></a></strong>
                        <small><?php echo $form->getFormDescription(); ?></small>
                    </div>
                </div><!-- media -->
            </li>
          <?php
				}
			}
		   }
		  ?>
        </ul>
    </div>
    <div class="tab-pane" id="rp-history">
        <h5 class="sidebartitle">History</h5>
        <ul class="chatuserlist">
        <?php 
		$q = Doctrine_Query::create()
		   ->from("History a")
		   ->where("a.userid = ?", $_SESSION['SESSION_CUTEFLOW_USERID'])
		   ->orderBy("a.id DESC");
		 $historys = $q->execute();
		   
		 $count = 0;
		 foreach($historys as $history)
		 {
			 $count++;
		?>
            <li class="online">
                <div class="media">
                    <a href="<?php echo public_path(); ?>backend.php/history/view/id/<?php echo $history->getId(); ?>" class="pull-left media-thumb">
                    <?php
                    if($logged_reviewer->getProfilePic())
                    {
                        ?>
                        <img src="/asset_uplds/photos/<?php echo $user->getProfilePic(); ?>" class="media-object"/>
                    <?php
                    }
                    else
                    {
                    ?>
                        <img alt="" src="<?php echo public_path(); ?>assets_unified/images/photos/user4.png" class="media-object">
                    <?php
                    }
                   ?>
                    </a>
                    <div class="media-body">
                        <strong><?php echo $history->getContent(); ?></strong>
                        <small><?php echo $history->getDatesent(); ?></small>
                    </div>
                </div><!-- media -->
            </li>
         <?php
		 	if($count >= 10)
			{
				break;
			}
		 }
		 ?>
        </ul>
    </div>
    <div class="tab-pane pane-settings" id="rp-settings">
        <?php
		$email_notifications = false;
		$online_chat = false;
		
		if($logged_reviewer->getEnableEmail())
		{
			$email_notifications = true;
		}
		else
		{
			$email_notifications = false;
		}
		
		if($logged_reviewer->getEnableChat())
		{
			$online_chat = true;
		}
		else
		{
			$online_chat = false;
		}
		?>
        <h5 class="sidebartitle mb20">Settings</h5>
        <div class="form-group">
            <label class="col-xs-8 control-label">Email Notifications</label>
            <div class="col-xs-4 control-label">
                <div class="toggle<?php if(!$email_notifications){ echo "-chat1"; } ?> toggle-success"></div>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-xs-8 control-label">Online Chat</label>
            <div class="col-xs-4 control-label">
                <div class="toggle<?php if(!$online_chat){ echo "-chat1"; } ?> toggle-success"></div>
            </div>
        </div>
        
        
    </div><!-- tab-pane -->
    
</div><!-- tab-content -->
</div><!-- rightpanel -->