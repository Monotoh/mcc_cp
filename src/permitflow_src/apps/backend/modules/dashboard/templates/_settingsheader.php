<?php
/**
 * _header.php template.
 *
 * Displays the header on the layout
 *
 * @package    backend
 * @subpackage dashboard
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper('I18N');
?>
<div class="leftpanel">
<!--OTB Start Patch - Multiagency fucntionality, stage listing check-->
<?php
$agency_manager = new AgencyManager();
$logo = $agency_manager->getLogo($sf_user->getAttribute('userid'));
?>
<!--OTB End Patch - Multiagency fucntionality, stage listing check-->

  <div class="logopanel">
      <h1> <img src="<?php echo $logo; ?>" alt="" /></h1><!--OTB Patch - Multiagency fucntionality, stage listing check-->
  </div><!-- logopanel -->
<?php
//Displays the sidemenu
include_component('dashboard', 'settingssidemenu');
?>

<div class="mainpanel">

<div class="headerbar">

<a class="menutoggle"><i class="fa fa-bars"></i></a>

<form class="searchform" action="<?php echo public_path(); ?>backend.php/applications/search" method="post">
    <input type="text" class="form-control" name="applicationid" placeholder="<?php echo __('Search here...'); ?>" />
</form>

<form class="searchform" action="<?php echo public_path(); ?>backend.php/applications/search" method="post">
    <a href="<?php echo public_path(); ?>backend.php/applications/search?search=adv" class="pull-left" style="padding: 15px;">Advanced Search</a>
</form>
<div class="header-right">
<ul class="headermenu">
<?php
if($sf_user->mfHasCredential('access_members'))
{
    ?>
    <li>
        <div class="btn-group">
            <button class="btn btn-default dropdown-toggle tp-icon" data-toggle="dropdown">
                <i class="glyphicon glyphicon-user"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-head pull-right">
                <h5 class="title"><?php echo __('Recently Registered Users'); ?></h5>
                <ul class="dropdown-list user-list">
                    <?php

                    $q = Doctrine_Query::create()
                        ->from("SfGuardUser a")
                        ->where("a.id <> 1")
                        ->orderBy("a.id DESC")
                        ->limit(5);
                    $allusers = $q->execute();

                    $count = 0;
                    foreach($allusers as $user)
                    {
                        if($user->getIsActive() == "1")
                        {
                            ?>
                            <li>
                                <div class="thumb"><a href="<?php echo public_path(); ?>backend.php/frusers/show/id/<?php echo $user->getId(); ?>"><img src="<?php echo public_path(); ?>assets_unified/images/photos/user2.png" alt="" /></a></div>
                                <div class="desc">
                                    <h5><a href="<?php echo public_path(); ?>backend.php/frusers/show/id/<?php echo $user->getId(); ?>"><?php echo $user->getProfile()->getFullname(); ?> (<?php echo $user->getProfile()->getEmail(); ?>)</a></h5>
                                </div>
                            </li>
                        <?php
                        }
                        else
                        {
                            ?>
                            <li class="new">
                                <div class="thumb"><a href="<?php echo public_path(); ?>backend.php/frusers/show/id/<?php echo $user->getId(); ?>"><img src="<?php echo public_path(); ?>assets_unified/images/photos/user2.png" alt="" /></a></div>
                                <div class="desc">
                                    <h5><a href="<?php echo public_path(); ?>backend.php/frusers/show/id/<?php echo $user->getId(); ?>"><?php echo $user->getProfile()->getFullname(); ?> (<?php echo $user->getProfile()->getEmail(); ?>)</a> <span class="badge badge-success">new</span></h5>
                                </div>
                            </li>
                        <?php
                        }
                    }
                    ?>
                    <li class="new"><a href="<?php echo public_path(); ?>backend.php/frusers/index"><?php echo __('See All Users'); ?></a></li>
                </ul>
            </div>
        </div>
    </li>
<?php
}
?>
<li>
    <?php
    $q = Doctrine_Query::create()
        ->from('ReviewerComments a')
        ->where('a.reviewer_id = ?', $sf_user->getAttribute('userid'))
        ->andWhere('a.messageread = ?', '0')
        ->orderBy('a.id DESC');
    $newmessages = $q->execute();
    ?>
    <div class="btn-group">
        <button class="btn btn-default dropdown-toggle tp-icon" data-toggle="dropdown">
            <i class="glyphicon glyphicon-envelope"></i>
            <?php if(sizeof($messages)){ ?>
                <span class="badge"><?php echo sizeof($newmessages); ?></span>
            <?php } ?>
        </button>
        <div class="dropdown-menu dropdown-menu-head pull-right">
            <h5 class="title">You Have <?php echo sizeof($newmessages); ?> New Private Messages</h5>
            <ul class="dropdown-list gen-list">
                <?php
                $q = Doctrine_Query::create()
                    ->from('ReviewerComments a')
                    ->where('a.reviewer_id = ?', $sf_user->getAttribute('userid'))
                    ->orderBy('a.id DESC');
                $allmessages = $q->execute();

                $count = 0;
                foreach($allmessages as $message)
                {
                    if($message->getMessageread() == "0")
                    {
                        ?>
                        <li class="new">
                            <a href="<?php echo public_path(); ?>backend.php/messages/view/id/<?php echo $message->getId(); ?>">
                                <span class="thumb"><img src="<?php echo public_path(); ?>assets_unified/images/photos/user1.png" alt="" /></span>
                        <span class="desc">
                          <span class="name">
						  <?php
                          $q = Doctrine_Query::create()
                              ->from('CfUser a')
                              ->where('a.nid = ?', $message->getSenderId() );
                          $reviewer = $q->fetchOne();
                          echo $reviewer->getStrfirstname()." ".$reviewer->getStrlastname();
                          ?> <span class="badge badge-success">new</span></span>
                          <span class="msg">
                          <?php
                          $words = explode(" ",$message->getCommentcontent());
                          echo implode(" ",array_splice($words,0,5))."....";
                          echo "<br>";
                          echo "<i><b>Sent ".$message->getDateCreated()."</b></i>";
                          ?>
                          </span>
                        </span>
                            </a>
                        </li>
                    <?php
                    }
                    else
                    {
                        ?>
                        <li>
                            <a href="<?php echo public_path(); ?>backend.php/messages/view/id/<?php echo $message->getId(); ?>">
                                <span class="thumb"><img src="<?php echo public_path(); ?>assets_unified/images/photos/user2.png" alt="" /></span>
                        <span class="desc">
                          <span class="name">
						  <?php
                          $q = Doctrine_Query::create()
                              ->from('CfUser a')
                              ->where('a.nid = ?', $message->getSenderId() );
                          $reviewer = $q->fetchOne();
                          echo $reviewer->getStrfirstname()." ".$reviewer->getStrlastname();
                          ?></span>
                          <span class="msg">
                          <?php
                          $words = explode(" ",$message->getCommentcontent());
                          echo implode(" ",array_splice($words,0,5))."....";
                          echo "<br>";
                          echo "<i><b>Sent ".$message->getDateCreated()."</b></i>";
                          ?>
                          </span>
                        </span>
                            </a>
                        </li>
                    <?php
                    }
                    if($count >= 5)
                    {
                        break;
                    }
                }
                ?>
                <li class="new"><a href="<?php echo public_path(); ?>backend.php/messages/index">Read All Messages</a></li>
            </ul>
        </div>
    </div>
</li>
<li>
    <?php
    $q = Doctrine_Query::create()
        ->from("Alerts a")
        ->where("a.userid = ?", $sf_user->getAttribute('userid'))
        ->andWhere("a.isread = ?", "0")
        ->orderBy("a.id DESC");
    $newalerts = $q->execute();
    ?>
    <div class="btn-group">
        <button class="btn btn-default dropdown-toggle tp-icon" data-toggle="dropdown">
            <i class="glyphicon glyphicon-globe"></i>
            <?php
            if(sizeof($newalerts) > 0)
            {
                ?>
                <span class="badge"><?php echo sizeof($newalerts); ?></span>
            <?php
            }
            ?>
        </button>
        <div class="dropdown-menu dropdown-menu-head pull-right">
            <h5 class="title"><?php echo __('Notifications'); ?></h5>
            <ul class="dropdown-list gen-list">
                <?php
                $q = Doctrine_Query::create()
                    ->from("Alerts a")
                    ->where("a.userid = ?", $sf_user->getAttribute('userid'))
                    ->orderBy("a.id DESC");
                $alerts = $q->execute();

                foreach($alerts as $alert)
                {
                    if($alert->getIsread() == "0")
                    {
                        ?>
                        <li>
                            <a href="<?php echo public_path().$alert->getLink(); ?>">
                                <span class="thumb"><img src="<?php echo public_path(); ?>assets_unified/images/photos/user4.png" alt="" /></span>
						<span class="desc">
						  <span class="name"><?php echo $alert->getSubject(); ?></span>
						  <span class="msg"><?php echo $alert->getContent(); ?></span>
						</span>
                            </a>
                        </li>
                    <?php
                    }
                    else
                    {
                        ?>
                        <li class="new">
                            <a href="<?php echo public_path().$alert->getLink(); ?>">
                                <span class="thumb"><img src="<?php echo public_path(); ?>assets_unified/images/photos/user4.png" alt="" /></span>
						<span class="desc">
						  <span class="name"><?php echo $alert->getSubject(); ?></span>
						  <span class="msg"><?php echo $alert->getContent(); ?></span>
						</span>
                            </a>
                        </li>
                    <?php
                    }
                }
                ?>
                <li class="new"><a href="<?php echo public_path(); ?>backend.php/alerts/list"><?php echo __('See All Notifications'); ?></a></li>
            </ul>
        </div>
    </div>
</li>
<li>
    <div class="btn-group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
            <?php
            $q = Doctrine_Query::create()
                ->from('CfUser a')
                ->where('a.nid = ?', $sf_user->getAttribute('userid'));
            $logged_reviewer = $q->fetchOne();

            if(empty($logged_reviewer))
            {
                ?>
                <script language='javascript'>
                    window.location="/backend.php/login/logout";
                </script>
                <?php
                exit;
            }

            if($logged_reviewer->getProfilePic())
            {
                $q = Doctrine_Query::create()
                   ->from("ApSettings a")
                   ->where("a.id = 1")
                   ->orderBy("a.id DESC");
                $aplogo = $q->fetchOne();
                if($aplogo && $aplogo->getUploadDirWeb())
                {
                ?>
                <img src="<?php echo $aplogo->getUploadDirWeb(); ?><?php echo $logged_reviewer->getProfilePic(); ?>" alt="" />
                <?php
              }
            }
            else
            {
                ?>
                <img src="<?php echo public_path(); ?>assets_unified/images/photos/loggeduser.png" alt="" />
            <?php
            }
            ?>
            <?php echo $logged_reviewer->getStrfirstname()." ".$logged_reviewer->getStrlastname(); ?>
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
            <li><a href="<?php echo public_path(); ?>backend.php/users/viewuser/userid/<?php echo $sf_user->getAttribute('userid'); ?>"><i class="glyphicon glyphicon-cog"></i> <?php echo __('My Account'); ?></a></li>
            <li><a href="<?php echo public_path(); ?>backend.php/help/index"><i class="glyphicon glyphicon-question-sign"></i> <?php echo __('Help'); ?></a></li>
            <li><a href="<?php echo public_path(); ?>backend.php/login/logout"><i class="glyphicon glyphicon-log-out"></i> <?php echo __('Log Out'); ?></a></li>
        </ul>
    </div>
</li>
<li>
    <button id="chatview" class="btn btn-default tp-icon chat-icon">
        <i class="glyphicon glyphicon-comment"></i>
    </button>
</li>
</ul>
</div><!-- header-right -->

</div><!-- headerbar -->
