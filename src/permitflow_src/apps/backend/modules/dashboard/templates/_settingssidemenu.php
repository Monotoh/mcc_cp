<?php
/**
 * _sidemenu.php template.
 *
 * Displays the main menu that is located on the left of the screen
 *
 * @package    backend
 * @subpackage dashboard
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

use_helper('I18N');
?>
<div class="leftpanelinner">

<?php
$q = Doctrine_Query::create()
    ->from('CfUser a')
    ->where('a.nid = ?', $sf_user->getAttribute('userid'));
$logged_reviewer = $q->fetchOne();
?>


<ul class="nav nav-pills nav-stacked nav-bracket">

    <?php
  if($sf_user->mfHasCredential("access_settings")) {
    if ($sf_user->mfHasCredential("access_content")) {
        ?>
        <li class="nav-parent  <?php if ($sf_context->getModuleName() == "content" || $sf_context->getModuleName() == "banner" || $sf_context->getModuleName() == "faq" || $sf_context->getModuleName() == "news" || $sf_context->getModuleName() == "languages" || $sf_context->getModuleName() == "announcements" || $sf_context->getModuleName() == "siteconfig") {
            echo "nav-active active";
        } ?>"><a href=""><i class="fa fa-list-alt"></i> <span><?php echo __('Content'); ?></span></a>
            <ul class="children"  <?php if ($sf_context->getModuleName() == "content" || $sf_context->getModuleName() == "banner" || $sf_context->getModuleName() == "faq" || $sf_context->getModuleName() == "news" || $sf_context->getModuleName() == "languages" || $sf_context->getModuleName() == "announcements" || $sf_context->getModuleName() == "siteconfig") {
                echo "style='display: block'";
            } ?>>
                <?php
                 if ($sf_user->mfHasCredential('managewebpages')) {
                 ?>
                    <li <?php if ($sf_context->getModuleName() == "content") {
                        echo "class='active'";
                    } ?>>
                        <a href="<?php echo public_path(); ?>backend.php/content/index"><i
                                class="fa fa-caret-right"></i>Web Pages</a>
                    </li>
                    

                    <li <?php if ($sf_context->getModuleName() == "banner") {
                        echo "class='active'";
                    } ?>>
                        <a href="<?php echo public_path(); ?>backend.php/banner/index"><i
                                class="fa fa-caret-right"></i>Banners</a>
                    </li>
                    <?php
                }

                if($sf_user->mfHasCredential("managefaqs")) {
                ?>
                <li <?php if ($sf_context->getModuleName() == "faq") {
                    echo "class='active'";
                } ?>>
                    <a href="<?php echo public_path(); ?>backend.php/faq/index"><i
                            class="fa fa-caret-right"></i>FAQs</a>
                </li>
                <?php
                }

                if($sf_user->mfHasCredential("managenews")) {
                ?>
                <li <?php if ($sf_context->getModuleName() == "news") {
                    echo "class='active'";
                } ?>>
                    <a href="<?php echo public_path(); ?>backend.php/news/index"><i
                            class="fa fa-caret-right"></i>News</a>
                </li>
                <?php
                }

                if($sf_user->mfHasCredential("managelanguages")) {
                ?>
                <li <?php if ($sf_context->getModuleName() == "languages") {
                    echo "class='active'";
                } ?>>
                    <a href="<?php echo public_path(); ?>backend.php/languages/index"><i
                            class="fa fa-caret-right"></i>Languages</a>
                </li>
                <?php
                }

                if($sf_user->mfHasCredential("manageannouncements")) {
                ?>
                <li <?php if ($sf_context->getModuleName() == "announcements") {
                    echo "class='active'";
                } ?>>
                    <a href="<?php echo public_path(); ?>backend.php/announcements/index"><i
                            class="fa fa-caret-right"></i>Announcements</a>
                </li>
                <?php
                }
                ?>
            </ul>
        </li>
    <?php
    }

    if($sf_user->mfHasCredential("access_workflow")) {
    ?>
    <li <?php if ($sf_context->getModuleName() == "services") {
        echo "class='active'";
    } ?>>
        <a href="<?php echo public_path(); ?>backend.php/services/index"><i
                class="fa fa-puzzle-piece"></i>Services</a>
    </li>
    <?php
    }
    
    if ($sf_user->mfHasCredential("access_forms")) {
        ?>
        <li class="nav-parent  <?php if ($sf_context->getModuleName() == "forms" || $sf_context->getModuleName() == "formgroups" || $sf_context->getModuleName() == "fees" || $sf_context->getModuleName() == "invoicetemplates" || $sf_context->getModuleName() == "permits" || $sf_context->getModuleName() == "jsonreports") {
            echo "nav-active active";
        } ?>"><a href=""><i class="fa fa-magic"></i> <span><?php echo __('Other Settings'); ?></span></a>
            <ul class="children"  <?php if ($sf_context->getModuleName() == "department" || $sf_context->getModuleName() == "formgroups" || $sf_context->getModuleName() == "fees" || $sf_context->getModuleName() == "invoicetemplates" || $sf_context->getModuleName() == "permits" || $sf_context->getModuleName() == "jsonreports") {
                echo "style='display: block'";
            } ?>>
                <?php
               if($sf_user->mfHasCredential("manageformgroups")) {
                ?>
                <li <?php if ($sf_context->getModuleName() == "formgroups") {
                    echo "class='active'";
                } ?>>
                    <a href="<?php echo public_path(); ?>backend.php/formgroups/index"><i
                            class="fa fa-caret-right"></i>Form Categories</a>
                </li>
                <?php
                }
               

                if($sf_user->mfHasCredential("managedepartments")) {
                ?>
                <li <?php if ($sf_context->getModuleName() == "department") {
                    echo "class='active'";
                } ?>>
                    <a href="<?php echo public_path(); ?>backend.php/department/index"><i
                            class="fa fa-caret-right"></i>Departments</a>
                </li>
                <?php
                }
//OTB Africa Start Patch - Multiagency fucntionality
                /*if($sf_user->mfHasCredential("manageagencies")) {
                ?>
                <li <?php if ($sf_context->getModuleName() == "agency") {
                    echo "class='active'";
                } ?>>
                    <a href="<?php echo public_path(); ?>backend.php/agency/index"><i
                            class="fa fa-caret-right"></i><?php echo __('Agencies') ?></a>
                </li>
                <?php
                }*/
//OTB Africa End Patch - Multiagency fucntionality
                
                
                //Manage Lais
                 // if($sf_user->mfHasCredential("manageagencies")) {
                ?>
               <!-- <li <?php //if ($sf_context->getModuleName() == "dependency") {
                   // echo "class='active'";
               // } ?>>
                    <a href="<?php //echo public_path(); ?>backend.php/dependency/index"><i
                            class="fa fa-caret-right"></i><?php //echo __('LAIS Dependency') ?></a>
                </li> -->
                <?php
               // }

               if($sf_user->mfHasCredential("managefees")) {
                ?>
                <li <?php if ($sf_context->getModuleName() == "fees") {
                    echo "class='active'";
                } ?>>
                    <a href="<?php echo public_path(); ?>backend.php/fees/settingsindex/filter/0"><i
                            class="fa fa-caret-right"></i>Fees Configurations</a>
                </li>
                <?php
                }

                if($sf_user->mfHasCredential("manageinvoices")) {
                ?>
                <li <?php if ($sf_context->getModuleName() == "invoiceapiaccounts") {
                    echo "class='active'";
                } ?>>
                    <a href="<?php echo public_path(); ?>backend.php/invoiceapiaccounts/index"><i
                            class="fa fa-caret-right"></i>Invoice API Accounts</a>
                </li>
                <?php
                }

                if($sf_user->mfHasCredential("manageformgroups")) {
                    ?>
                    <li <?php if ($sf_context->getModuleName() == "jsonreports") {
                        echo "class='active'";
                    } ?>>
                        <a href="<?php echo public_path(); ?>backend.php/jsonreports/index"><i
                                class="fa fa-caret-right"></i>JSON Reports</a>
                    </li>
                    <?php
                }

                ?>
            </ul>
        </li>
    <?php
    }
     //Start OTB patch- Making payments Configurable

        if ($sf_user->mfHasCredential("access_forms")) {

        ?>

        <li class="nav-parent  <?php if ($sf_context->getModuleName() == "merchant" || $sf_context->getModuleName() == "currencies" || $sf_context->getModuleName() == "jambopay") {
            echo "nav-active active";
        } ?>"><a href=""><i class="fa fa-magic"></i> <span><?php echo __('Payment Settings'); ?></span></a>
            <ul class="children"  <?php if ($sf_context->getModuleName() == "merchant" || $sf_context->getModuleName() == "currencies" || $sf_context->getModuleName() == "jambopay") {
                echo "style='display: block'";

            } ?>>
                <?php
                //OTB patch - Currency management
                if($sf_user->mfHasCredential("managecurrencies")) {
                ?>
                <li <?php if ($sf_context->getModuleName() == "merchant") {
                    echo "class='active'";
                } ?>>
                    <a href="<?php echo public_path(); ?>backend.php/merchant/index"><i
                            class="fa fa-caret-right"></i><?php echo __("Available Merchants")?></a>
                </li>
                <?php
                }
                if($sf_user->mfHasCredential("managecurrencies")) {

                ?>

                <li <?php if ($sf_context->getModuleName() == "currencies") {

                    echo "class='active'";

                } ?>>

                    <a href="<?php echo public_path(); ?>backend.php/currencies/index"><i

                            class="fa fa-caret-right"></i><?php echo __("Manage Currencies")?></a>

                </li>

                <?php

                }

            

               ?>

                

           

            </ul>

        </li>

    <?php

    }
     //End OTB patch- Making payments Configurable

    if ($sf_user->mfHasCredential("access_security")) {
        ?>
        <li class="nav-parent  <?php if ($sf_context->getModuleName() == "usercategories" || $sf_context->getModuleName() == "groups" || $sf_context->getModuleName() == "credentials" || ($sf_context->getModuleName() == "wizard" && $sf_context->getActionName() == "security")) {
            echo "nav-active active";
        } ?>"><a href=""><i class="fa fa-unlock-alt"></i> <span><?php echo __('Security'); ?></span></a>
            <ul class="children"  <?php if ($sf_context->getModuleName() == "usercategories" || $sf_context->getModuleName() == "groups" || $sf_context->getModuleName() == "credentials" || ($sf_context->getModuleName() == "wizard" && $sf_context->getActionName() == "security")) {
                echo "style='display: block'";
            } ?>>
                <?php
               if($sf_user->mfHasCredential("managecategories")) {
                ?>
                <li <?php if ($sf_context->getModuleName() == "usercategories") {
                    echo "class='active'";
                } ?>>
                    <a href="<?php echo public_path(); ?>backend.php/usercategories/index"><i
                            class="fa fa-caret-right"></i>User Categories</a>
                </li>
                <?php
                }

                if($sf_user->mfHasCredential("managegroups")) {
                ?>
                <li <?php if ($sf_context->getModuleName() == "groups") {
                    echo "class='active'";
                } ?>>
                    <a href="<?php echo public_path(); ?>backend.php/groups/index"><i
                            class="fa fa-caret-right"></i>Groups</a>
                </li>
                <?php
                }


                if($sf_user->mfHasCredential("manageroles")) {
                ?>
                <li <?php if ($sf_context->getModuleName() == "credentials") {
                    echo "class='active'";
                } ?>>
                    <a href="<?php echo public_path(); ?>backend.php/credentials/index"><i
                            class="fa fa-caret-right"></i>Roles</a>
                </li>
                <?php
                }

                if($sf_user->mfHasCredential("access_security")) {
                ?>
                <li <?php if ($sf_context->getModuleName() == "wizard" && $sf_context->getActionName() == "security") {
                    echo "class='active'";
                } ?>>
                    <a href="<?php echo public_path(); ?>backend.php/wizard/security"><i
                            class="fa fa-caret-right"></i>Security Wizard</a>
                </li>
                <?php
                }
                ?>
            </ul>
        </li>
    <?php
    }



    if($sf_user->mfHasCredential("managewebpages")) {
    ?>
    <li <?php if ($sf_context->getModuleName() == "siteconfig") {
        echo "class='active'";
    } ?>>
        <a href="<?php echo public_path(); ?>backend.php/siteconfig/index"><i
                class="fa fa-wrench"></i>Site Config</a>
    </li>
    <?php
    }

  }

    if($sf_user->mfHasCredential("access_help"))
    {
        /**
        ?>
        <li <?php if($sf_context->getModuleName() == "help" && $sf_context->getActionName() == "index"){ echo "class='active'"; } ?>><a href="<?php echo public_path(); ?>backend.php/help/index"><i class="fa fa-info"></i> <span><?php echo __('Help'); ?></span></a></li>
        <?php
         **/
    }

    if($sf_user->mfHasCredential("access_help"))
    {
        /**
        ?>
        <li <?php if($sf_context->getModuleName() == "help" && $sf_context->getActionName() == "video"){ echo "class='active'"; } ?>><a href="<?php echo public_path(); ?>backend.php/help/video"><i class="fa fa-video-camera"></i> <span><?php echo __('Video Tutorials'); ?></span></a></li>
        <?php
         **/
    }

    ?>



</ul>

    <div class="infosummary">
        <h5 class="sidebartitle"><?php echo __('Administration'); ?></h5>
            <ul>
                 <li>
                    <a class="btn btn-default-alt btn-block" href="<?php echo public_path(); ?>backend.php/dashboard"><i class="fa fa-dashboard"> </i> Back to Dashboard</a>
                </li>
            </ul>
    </div>

</div><!-- leftpanelinner -->
</div><!-- leftpanel -->
