<?php
/**
 * _sidemenu.php template.
 *
 * Displays the main menu that is located on the left of the screen
 *
 * @package    backend
 * @subpackage dashboard
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

 use_helper('I18N');
?>
<!-- BEGIN SIDEBAR1 -->
	<div class="page-sidebar-wrapper">
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<div class="page-sidebar navbar-collapse collapse">
			<!-- BEGIN SIDEBAR MENU1 -->
			<ul class="page-sidebar-menu" data-slide-speed="200" data-auto-scroll="true" data-auto-scroll="true" data-slide-speed="200">
				
				<li class="start">
					<a class=" start" href="<?php echo public_path(); ?>backend.php/dashboard">
					<i class="fa fa-home"></i>
					<span class="title">
					Dashboard</span>
					<span class="selected">
					</span>
					</a>
				</li>
                                
                                
                                <?php
        if($sf_user->mfHasCredential("access_applications"))
        {
        ?>
                  <li class="<?php if($sf_context->getActionName() == "listgroup" || $sf_context->getModuleName() == "applications"){ echo "nav-active active"; } ?>"><a href="javascript:;"><i class="fa fa-edit"></i> <span class="title"><?php echo __('Applications'); ?></span>
                      <span class="selected"></span>
                      <span class="arrow"></span>
                      </a>
		        <ul class="sub-menu"  <?php if($sf_context->getActionName() == "listgroup" || $sf_context->getModuleName() == "applications"){ echo "style='display: block'"; } ?>>
			<?php
		            $q = Doctrine_Query::create()
				        ->from('Menus a')
				        ->orderBy('a.order_no ASC');
				    $stagegroups = $q->execute();
				    foreach($stagegroups as $stagegroup)
				    {
						//error_log('---->menu_ID'.$stagegroup->getId());
				        if($sf_user->mfHasCredential('accessmenu'.$stagegroup->getId()))
				        {
							 $q = Doctrine_Query::create()
								->from('SubMenus a')
								->where("a.menu_id = ?", $stagegroup->getId())
                                ->andWhere("a.deleted = 0")
								->orderBy('a.order_no ASC');
							$submenus = $q->execute();
                                                        //---- get submenu ids of each user
                            $sub = "";
                            foreach($submenus as $submenu)
                            {
                                if($sf_user->mfHasCredential('accesssubmenu'.$submenu->getId()))
                                {
                                    $sub .=$submenu->getId().",";
                                }
                            }
                                
                            
                            
							foreach($submenus as $submenu)
							{
								error_log('---->submenu_ID'.$submenu->getId());
                                if($sf_user->mfHasCredential('accesssubmenu'.$submenu->getId()))
                                {
                                   
                                     $html ="<span class='pull-right badge badge-success badge-round'>".$q_response."</span>";
                				    ?>
                                        <li <?php if(($sf_context->getActionName() == "listgroup" || $sf_context->getModuleName() == "applications") && $stagegroup->getId() == $_SESSION['group']){ echo "class='active'"; } ?>><a  href="<?php echo public_path(); ?>backend.php/applications/listgroup/subgroup/<?php echo $submenu->getId(); ?>"> <i class="fa fa-caret-right"> </i><?php echo $html ?><?php echo $stagegroup->getTitle(); ?>
                                                           
                                                            </a>
                                                         
                                                        </li>
                				    <?php
                                    break;
                                }
							}
				        }
				    }
		            ?>
		          </ul>
		        </li>
       
                  <?php
        }
        if($sf_user->mfHasCredential("access_tasks"))
        {
        ?>
            <li <?php if($sf_context->getModuleName() == "tasks"){ echo "class='active'"; } ?>><a class="" href="<?php echo public_path(); ?>backend.php/tasks/list"><i class="fa fa-tasks"></i> <span><?php echo __('Tasks'); ?></span></a></li>
            <!-- <li <?php //if($sf_context->getModuleName() == "help" && $sf_context->getActionName() == "searchUPI"){ echo "class='active'"; } ?>><a class="" href="<?php// echo public_path(); ?>backend.php/help/searchUPI"><i class="fa fa-automobile"></i> <span><?php //echo __('Search UPI Details'); ?></span></a></li> -->

        <?php
        }
       

        if($sf_user->mfHasCredential("access_permits"))
        {
        ?>
        <li <?php if($sf_context->getModuleName() == "permits"){ echo "class='active'"; } ?>><a class="" href="<?php echo public_path(); ?>backend.php/permits/list"><i class="fa fa-certificate"></i> <span><?php echo __('Issued Permits'); ?></span></a></li>

        <?php
        }

        if($sf_user->mfHasCredential("access_billing"))
        {
        ?>
        <li class="<?php if ($sf_context->getModuleName() == "invoices") {
            echo "nav-active active";
        } ?>"><a href="<?php echo public_path(); ?>backend.php/invoices/index"><i class="fa fa-money"></i> <span><?php echo __('Billing'); ?></span>
            <span class="selected"></span>
            <span class="arrow"></span>
            </a>
		<!--OTB Patch Start - Seperate viewing of invoices from viewing of payments-->
		<ul class="sub-menu" <?php if ($sf_context->getModuleName() == "invoices") {
                echo "style='display: block'";
            } ?>>
			<li <?php if ($sf_context->getModuleName() == "invoices" and $sf_context->getActionName() == "index") {
                    echo "class='active'";
                } ?>>
				<a class="" href="<?php echo public_path(); ?>backend.php/invoices/index"><i
						class="fa fa-caret-right"></i><?php echo __('Invoices'); ?></a>
			</li>
			<!--<li <?php //if ($sf_context->getModuleName() == "invoices" and $sf_context->getActionName() == "transactions") {
                    //echo "class='active'";
                //} ?>>
				<a class="" href="<?php //echo public_path(); ?>backend.php/invoices/transactions"><i
						class="fa fa-caret-right"></i><?php //echo __('Payments'); ?></a>
			</li> -->
		</ul>
		<!--OTB Patch End - Seperate viewing of invoices from viewing of payments-->
		</li>

        <?php
        }


        if($sf_user->mfHasCredential("access_messages"))
        {
        ?>
        <li <?php if($sf_context->getModuleName() == "messages"){ echo "class='active'"; } ?>><a class="" href="<?php echo public_path(); ?>backend.php/messages/index"><i class="fa fa-envelope"></i> <span><?php echo __('Messages'); ?></span></a></li>

        <?php
        }

        if($sf_user->mfHasCredential("access_members"))
        {
        ?>
        <li <?php if($sf_context->getModuleName() == "frusers"){ echo "class='active'"; } ?>><a class="" href="<?php echo public_path(); ?>backend.php/frusers/index"><i class="fa fa-users"></i> <span><?php echo __('Users'); ?></span></a></li>

        <?php
        }
    // OTB patch - Improve membership validation process 
      //  error_log('I was executed....before') ;
            if($sf_user->mfHasCredential("validatearchitects")) {
               // error_log('I was executed....after') ;
           ?>
           <li>
              <a class="" href="<?php echo public_path(); ?>backend.php/applications/showmemberships"><i
                                                   class="fa fa-caret-right"></i><?php echo __('Membership database') ?></a>

           </li>
           <?php
           }

        if($sf_user->mfHasCredential("access_reviewers"))
        {
        ?>
        <li <?php if($sf_context->getModuleName() == "users"){ echo "class='active'"; } ?>><a class="" href="<?php echo public_path(); ?>backend.php/users/index"><i class="fa fa-group"></i> <span><?php echo __('Reviewers'); ?></span></a></li>

        <?php
        } ?>
        
        <!--<li <?php //if($sf_context->getModuleName() == "leaves"){ echo "class='active'"; } ?> class="">
            <a href="/backend.php/leaves/index"><i class="fa fa-anchor"></i> <span> Reviewers Leave </span>
            <span class="selected"></span>
            <span class="arrow"></span>
            </a>
            <ul class="sub-menu"  <?php //if ($sf_context->getModuleName() == "leaves") {
               // echo "style='display: block'";
            //} ?>>
                <li <?php //if ($sf_context->getModuleName() == "leaves" and $sf_context->getActionName() == "index") {
                   // echo "class='active'";
                //} ?>
                    > <a class="" href="/backend.php/leaves/index"> Requests </a></li>
              
            </ul>
        </li> -->
        <?php
        
        if($sf_user->mfHasCredential("accessfeedback"))
        {
        ?>
        <li <?php if($sf_context->getModuleName() == "feedback"){ echo "class='active'"; } ?>><a class="" href="<?php echo public_path(); ?>backend.php/feedback/index"><i class="fa fa-group"></i> <span><?php echo __('Clients Feedback'); ?></span></a></li>

        <?php
        }

        if($sf_user->mfHasCredential("access_reports"))
        {
        ?>
        <li <?php if($sf_context->getModuleName() == "reports"){ echo "class='active'"; } ?>><a class="" href="<?php echo public_path(); ?>backend.php/reports/list"><i class="fa fa-bar-chart-o"></i> <span><?php echo __('Reports'); ?></span></a></li>
<!--OTB Patch Start - BI Link-->
        <?php
        }

        if($sf_user->mfHasCredential("view_analytical_reports"))
        {
        ?>

         <li class="<?php if ($sf_context->getModuleName() == "api") {
            echo "nav-active active";
        } ?>"><a href="#"><i class="fa fa-bar-chart-o"></i> <span><?php echo __('Management Reports'); ?></span>
            <span class="selected"></span>
            <span class="arrow"></span>
            </a>
		
		<ul class="sub-menu" <?php if ($sf_context->getModuleName() == "api") {
                echo "style='display: block'";
            } ?>>
			<li <?php if ($sf_context->getModuleName() == "api") {
                    echo "class='active'";
                } ?>>
				<a target="_blank" href="http://129.232.240.226:3000"><i
						class="fa fa-caret-right"></i><?php echo __('Dashboard'); ?></a>
			</li>
			
		</ul>
		<!--OTB Patch End - Seperate viewing of invoices from viewing of payments-->
		</li>
        
<!--OTB Patch Start - BI Link-->
<!--OTB Patch Start - Live Chat-->
        <?php
        }
		//$livechat = new LiveChat();

       // if($livechat->isOperator($_SESSION['SESSION_CUTEFLOW_USERID']))
       // {
        ?>
       <!-- <li <?php //if($sf_context->getModuleName() == "livechat"){ echo "class='active'"; } ?>><a class="" href="<?php //echo public_path(); ?>backend.php/livechat"><i class="fa fa-comment"></i> <span><?php //echo __('Live Chat'); ?></span></a></li>-->
<!--OTB Patch End - Live Chat-->
        <?php
        //}

        if($sf_user->mfHasCredential("access_help"))
        {
            
        ?>
      
      
          <li <?php if($sf_context->getModuleName() == "help" && $sf_context->getActionName() == "index"){ echo "class='active'"; } ?>><a class="" href="<?php echo public_path(); ?>backend.php/help/index"><i class="fa fa-info"></i> <span><?php echo __('Help'); ?></span></a></li>
           <?php
            
        }

        if($sf_user->mfHasCredential("access_help"))
        {
            /**
        ?>
        <li <?php if($sf_context->getModuleName() == "help" && $sf_context->getActionName() == "video"){ echo "class='active'"; } ?>><a href="<?php echo public_path(); ?>backend.php/help/video"><i class="fa fa-video-camera"></i> <span><?php echo __('Video Tutorials'); ?></span></a></li>
        <?php
            **/
        }

        ?>
          <?php
    if($sf_user->mfHasCredential("access_settings"))
    {
    ?>
          <li>  <a href="#" class="btn blue ">Settings and Configuration</a> </li>
           <li>
              <a target="_blank" href="<?php echo public_path(); ?>backend.php/settings/index"><i class="fa fa-cog"> </i> System Settings</a>
         </li>
         <?php
    }
    ?>
  </ul>
    
		</div>
	</div>
	<!-- END SIDEBAR1 -->