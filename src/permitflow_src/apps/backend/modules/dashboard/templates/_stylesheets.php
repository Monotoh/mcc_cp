<?php
/**
 * _stylesheets.php template.
 *
 * Displays stylesheets on the layout
 *
 * @package    backend
 * @subpackage dashboard
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

$translation = new Translation();
if($translation->IsLeftAligned())
{
	?>
	<script src="<?php echo public_path(); ?>metronicv3/assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
	<script src="http://code.jquery.com/jquery-1.11.3.js" type="text/javascript"></script>
	<link href="<?php echo public_path(); ?>assets_unified/css/style.default.css" rel="stylesheet">
	<link href="<?php echo public_path(); ?>assets_unified/css/bootstrap-override.css" rel="stylesheet">
	<link href="<?php echo public_path(); ?>assets_unified/css/bootstrap.min.css" rel="stylesheet">
	


	<link href="<?php echo public_path(); ?>assets_unified/css/fullcalendar.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo public_path(); ?>assets_unified/css/bootstrap-timepicker.min.css" />
	<link href="<?php echo public_path(); ?>assets_unified/css/dropzone.css" />
	<link href="<?php echo public_path(); ?>assets_unified/css/jquery.datatables.css" rel="stylesheet">
	<link href="<?php echo public_path(); ?>assets_unified/css/bootstrap-duallistbox.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo public_path(); ?>assets_unified/css/bootstrap-wysihtml5.css" />

    <link rel="stylesheet" href="<?php echo public_path(); ?>assets_unified/css/ectzn/style.ecitizenbackend.css" />
	<link rel="stylesheet" href="<?php echo public_path(); ?>assets_unified/css/ectzn/custom.css" />

	<link rel="stylesheet" href="<?php echo public_path(); ?>assets_unified/js/slide-in/css/reset.css"> <!-- CSS reset NEW CSS -->
	<link rel="stylesheet" href="<?php echo public_path(); ?>assets_unified/js/slide-in/css/style.css"> <!-- Resource style NEW CSS-->
	

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
	<?php
}
else
{ //If language is right aligned
?>
    <link href="<?php echo public_path(); ?>assets_unified/css/style.default.css" rel="stylesheet">
	<link href="<?php echo public_path(); ?>assets_unified/css/style.default-rtl.css" rel="stylesheet">
	<link href="<?php echo public_path(); ?>assets_unified/css/bootstrap-override-rtl.css" rel="stylesheet">
	<link href="<?php echo public_path(); ?>assets_unified/css/bootsrap-rtl.min.css" rel="stylesheet">

	<link href="<?php echo public_path(); ?>assets_unified/css/fullcalendar.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo public_path(); ?>assets_unified/css/bootstrap-timepicker.min.css" />
	<link href="<?php echo public_path(); ?>assets_unified/css/dropzone.css" />
	<link href="<?php echo public_path(); ?>assets_unified/css/jquery.datatables.css" rel="stylesheet">
	<link href="<?php echo public_path(); ?>assets_unified/css/bootstrap-duallistbox.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo public_path(); ?>assets_unified/css/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" href="<?php echo public_path(); ?>assets_unified/css/ectzn/style.ecitizenbackend.css" />
    <link rel="stylesheet" href="<?php echo public_path(); ?>assets_unified/css/ectzn/custom.css" />

		<link rel="stylesheet" href="<?php echo public_path(); ?>assets_unified/js/slide-in/css/reset.css"> <!-- CSS reset NEW CSS -->
		<link rel="stylesheet" href="<?php echo public_path(); ?>assets_unified/js/slide-in/css/style.css"> <!-- Resource style NEW CSS-->


	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
<?php
}
?>
<!-- OTB patch - Include our styles -->
<link rel="stylesheet" href="<?php echo public_path(); ?>otb_assets/custom.css">
