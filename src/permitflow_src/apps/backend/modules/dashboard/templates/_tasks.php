<?php
  //Get a list of all stages with automatic assignments
  $q = Doctrine_Query::create()
      ->from("SubMenuTasks a");
  $task_stages = $q->execute();

  //Filter out stages that we don't have permissions for
  $allowed_stages = "";

  $count = 0;
  foreach($task_stages as $task_stage)
  {
    if($sf_user->mfHasCredential('accesssubmenu'.$task_stage->getSubMenuId()))
    {
      $count++;
      if($count == 1)
      {
        $allowed_stages .= "a.approved = ".$task_stage->getSubMenuId();
      }
      else
      {
        $allowed_stages .= " OR a.approved = ".$task_stage->getSubMenuId();
      }
    }
  }

  //Display applications without any tasks
  $q = "";

  if($allowed_stages <> "")
  {
    $q = Doctrine_Query::create()
        ->from("FormEntry a")
        ->leftJoin("a.Task b")
        ->where($allowed_stages)
        ->andWhere("b.owner_user_id IS NULL")
        ->orderBy("a.date_of_submission ASC");
  }
  else {
    $q = Doctrine_Query::create()
        ->from("FormEntry a")
        ->where("a.approved = -1")
        ->orderBy("a.date_of_submission ASC");
  }

  $pending_page = 1;

  if($_GET["apage"])
  {
    $pending_page = $_GET["apage"];
  }

  $pending_pager = new sfDoctrinePager('Task', 10);
  $pending_pager->setQuery($q);
  $pending_pager->setPage($pending_page);
  $pending_pager->init();
?>
<div style="margin-bottom: 20px;">
        <p><strong><u><?php echo $pending_pager->getNbResults(); ?></u></strong> tasks available for you to work on</p>

        <?php
        if($_GET['late'])
        {
        ?>
        <div class="alert alert-danger">
      		<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
      		Sorry! The application you selected has already been assigned.
      	</div>
        <?php
        }
        ?>

        <div class="table-responsive">
            <form action="/backend.php/tasks/batchpick" method="post">
                <table class="table table-primary">
                <thead>
                <tr>
                    <th style="background-color: #428BCA;">#</th>
                    <th style="background-color: #428BCA;"><?php echo __('Service') ?></th>
                    <th style="background-color: #428BCA;"><?php echo __('Application') ?></th>
                    <th style="background-color: #428BCA;"><?php echo __('Submitted On') ?></th>
                    <th style="background-color: #428BCA;"><?php echo __('Submitted By') ?></th>
                    <th style="background-color: #428BCA;"><?php echo __('Status') ?></th>
                    <th style="background-color: #428BCA;"></th>
                </tr>
                </thead>
                <tbody>
                  <?php foreach ($pending_pager->getResults() as $application): ?>
                  <tr>
                      <td><input type="checkbox" name="batch_pick[]" id="batch_<?php echo $application->getId(); ?>" value="<?php echo $application->getId(); ?>"></td>
                      <td><?php echo $application->getForm()->getFormName(); ?></td>
                      <td><a href="/backend.php/tasks/pick/id/<?php echo $application->getId(); ?>"><?php echo $application->getApplicationId(); ?></a></td>
                      <td><?php echo $application->getDateOfSubmission(); ?></td>
                      <td><?php
                      /**
                       * OTB patch
                       * Fix bug with retrival of the fullname object
                       * Some plans miss submitted by field value hence getFullname() will through an exception
                       */
                         /* $q = Doctrine_Query::create()
                             ->from('SfGuardUser a')
                             ->where('a.id = ?', $application->getUserId());
                        $user = $q->fetchOne(); */

                        $q = Doctrine_Query::create()
                             ->from('SfGuardUserProfile a')
                             ->where('a.user_id = ?', $application->getUserId());
                        $user_profile = $q->fetchOne();
                       //Note : This is a fix but not the best approach but $user_profile->getFullname() throws a php fatal error if the value is null
                       $user_id = $application->getUserId() ;
                       $usr_id = null ;
                       ////////////////////////////////////////
                        $query_username = "SELECT * FROM sf_guard_user WHERE id = $user_id " ;
                        $query_username_res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($query_username);
                        foreach($query_username_res as $res){
                            $usr_id = intval($res['id']);
                        }
                       /////////////////////////////////////////////
                      //  echo $user_profile->getFullname()." (".$user->getUsername().")";
                       $full_name = null ;
                       $query_fullname = "SELECT * FROM sf_guard_user_profile WHERE user_id = '$usr_id' " ;
                       $query_fullname_res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($query_fullname);
                        foreach($query_fullname_res as $res)
                        {
                            $full_name = $res['fullname'];
                            echo $full_name;
                           
                        }
                      ?></td>
                      <td><?php echo $application->getStatusName(); ?></td>
                      <td>
                          <a  title='<?php echo __('Pick Task'); ?>' href='<?php echo public_path("backend.php/tasks/pick/id/".$application->getId()); ?>'> <span class="badge badge-primary"><i class="fa fa-eye"></i></span></a>
                      </td>
                  </tr>
                  <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="7"><button type="submit" class="btn btn-primary">Pick Tasks</button></td>
                    </tr>
                </tfoot>
                </table>
            </form>
        </div><!-- table-responsive -->

        <?php if ($pending_pager->haveToPaginate()): ?>
            <ul class="pagination pagination-sm mb0 mt0 pull-right">
                <li> <a href="/backend.php/tasks/list">
                        <i class="fa fa-angle-left"></i>
                    </a></li>

                <li> <a href="/backend.php/tasks/list?apage=<?php echo $pending_pager->getPreviousPage() ?>">
                        <i class="fa fa-angle-left"></i>
                    </a></li>

                <?php foreach ($pending_pager->getLinks() as $page): ?>
                    <?php if ($page == $pending_pager->getPage()): ?>
                        <li class="active"><a href=""><?php echo $page ?></a>
                    <?php else: ?>
                        <li><a href="/backend.php/tasks/list?apage=<?php echo $page ?>"><?php echo $page ?></a></li>
                    <?php endif; ?>
                <?php endforeach; ?>

                <li> <a href="/backend.php/tasks/list?apage=<?php echo $pending_pager->getNextPage() ?>">
                        <i class="fa fa-angle-right"></i>
                    </a></li>

                <li> <a href="/backend.php/tasks/list?apage=<?php echo $pending_pager->getLastPage() ?>">
                        <i class="fa fa-angle-right"></i>
                    </a></li>
            </ul>
        <?php endif; ?>
</div>
