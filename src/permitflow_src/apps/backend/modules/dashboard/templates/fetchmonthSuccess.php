<div id="barchart1" style="width: 100%; height: 300px"></div>
  <script>
  jQuery(document).ready(function(){
      /***** BAR CHART *****/

      <?php
        if($month < 10)
        {
          $month = "0".$month;
        }

        $earnings = null;

        $search = null;
        $total_earnings = 0;

        for($day = 1; $day <= 31; $day++)
        {
          if($day < 10)
          {
              $search = date('Y')."-".$month."-0".$day;
          }
          else
          {
              $search = date('Y')."-".$month."-".$day;
          }

          $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
          mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

          $sql = "SELECT SUM(c.amount) AS total_amount
                  FROM form_entry a
                    LEFT JOIN mf_invoice b ON a.id = b.app_id
                    LEFT JOIN mf_invoice_detail c ON b.id = c.invoice_id
                    LEFT JOIN ap_forms d ON a.form_id = d.form_id
                  WHERE b.updated_at LIKE '%".$search."%' AND b.paid = 2 AND c.description LIKE '%Submission fee' AND d.payment_currency = '".$currency."'";

          $results = mysql_query($sql, $dbconn);

          $sub_totals = 0;

          while($row = mysql_fetch_assoc($results))
          {
             $sub_totals += $row['total_amount'];
          }

          $earnings['day'.$day] = $sub_totals;
          if(empty($earnings['day'.$day]))
          {
            $earnings['day'.$day] = 0;
          }
          $total_earnings += $sub_totals;
        }
      ?>

      var bardata = [ <?php for($day = 1; $day <= 31; $day++){ ?>["<?php echo $day; ?>", <?php echo $earnings['day'.$day]; ?>], <?php } ?> ];

     jQuery.plot("#barchart1", [ bardata ], {
        series: {
              lines: {
                lineWidth: 1
              },
          bars: {
            show: true,
            barWidth: 0.5,
            align: "center",
                 lineWidth: 0,
                 fillColor: "#428BCA"
          }
        },
          grid: {
              borderColor: '#ddd',
              borderWidth: 1,
              labelMargin: 10
        },
        xaxis: {
          mode: "categories",
          tickLength: 0
        }
     });

     function labelFormatter(label, series) {
      return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
    }

  });
  </script>
<div class="panel-special-pad">
<div class="tinystat mr20">
	<div class="datainfo">
	  <span class="text-muted">Total</span>
	  <h4><?php echo $currency.". ".$total_earnings; ?></h4>
	</div>
</div>
</div>
