<div id="barchart" style="width: 100%; height: 300px"></div>
    <script>
    jQuery(document).ready(function(){
        /***** BAR CHART *****/

        <?php
          $earnings = null;
          $earnings['month1'] = 0;
          $earnings['month2'] = 0;
          $earnings['month3'] = 0;
          $earnings['month4'] = 0;
          $earnings['month5'] = 0;
          $earnings['month6'] = 0;
          $earnings['month7'] = 0;
          $earnings['month8'] = 0;
          $earnings['month9'] = 0;
          $earnings['month10'] = 0;
          $earnings['month11'] = 0;
          $earnings['month12'] = 0;

          $search = null;
          $total_earnings = 0;

          for($month = 1; $month <= 12; $month++)
          {
            if($month < 10)
            {
              $search = $year."-0".$month."%";
            }
            else
            {
              $search = $year."-".$month."%";
            }


            $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
            mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

            $sql = "SELECT SUM(c.amount) AS total_amount
                    FROM form_entry a
                      LEFT JOIN mf_invoice b ON a.id = b.app_id
                      LEFT JOIN mf_invoice_detail c ON b.id = c.invoice_id
                      LEFT JOIN ap_forms d ON a.form_id = d.form_id
                    WHERE b.updated_at LIKE '%".$search."%' AND b.paid = 2 AND c.description LIKE '%Submission fee' AND d.payment_currency = '".$currency."'";

            $results = mysql_query($sql, $dbconn);

            $sub_totals = 0;

            while($row = mysql_fetch_assoc($results))
            {
               $sub_totals += $row['total_amount'];
            }

             $earnings['month'.$month] = $sub_totals;
             if(empty($earnings['month'.$month]))
             {
                $earnings['month'.$month] = 0;
             }
             $total_earnings += $sub_totals;
          }
        ?>

        var bardata = [ ["Jan", <?php echo $earnings['month1']; ?>], ["Feb", <?php echo $earnings['month2']; ?>], ["Mar", <?php echo $earnings['month3']; ?>], ["Apr", <?php echo $earnings['month4']; ?>], ["May", <?php echo $earnings['month5']; ?>], ["Jun", <?php echo $earnings['month6']; ?>], ["Jul", <?php echo $earnings['month7']; ?>], ["Aug", <?php echo $earnings['month8']; ?>], ["Sep", <?php echo $earnings['month9']; ?>], ["Oct", <?php echo $earnings['month10']; ?>], ["Nov", <?php echo $earnings['month11']; ?>], ["Dec", <?php echo $earnings['month12']; ?>] ];

       jQuery.plot("#barchart", [ bardata ], {
          series: {
                lines: {
                  lineWidth: 1
                },
            bars: {
              show: true,
              barWidth: 0.5,
              align: "center",
                   lineWidth: 0,
                   fillColor: "#428BCA"
            }
          },
            grid: {
                borderColor: '#ddd',
                borderWidth: 1,
                labelMargin: 10
          },
          xaxis: {
            mode: "categories",
            tickLength: 0
          }
       });

       function labelFormatter(label, series) {
        return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
      }

    });
    </script>

    <div class="panel-special-pad">
  					<div class="tinystat mr20">
			    		<div class="datainfo">
						  <span class="text-muted">Total</span>
						  <h4><?php echo $currency.". ".$total_earnings; ?></h4>
						</div>
          			</div>
          			</div>
