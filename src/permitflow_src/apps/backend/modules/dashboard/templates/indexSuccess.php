<?php
/**
 * indexSuccess.php template.
 *
 * Displays a summary of all application/reviewer related information for the reviewers
 *
 * @package    backend
 * @subpackage dashboard
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper('I18N');

//Ensure the reviewer session is authentic and does not conflict with frontend sessions
include_partial('checksession');

//If this is the first run after installation then display the wizard, else display the dashboard
$wizard_manager = new WizardManager();

if($wizard_manager->is_first_run())
{
    include_partial('dashboard_wizard', array('wizard_manager' => $wizard_manager,'invoice_form' => $invoice_form,'permit_form' => $permit_form));
}
else
{
   include_partial('dashboard_home');
}
?>
