<?php
/**
 * indexSuccess.php template.
 *
 * Displays a summary of all application/reviewer related information for the reviewers
 *
 * @package    backend
 * @subpackage dashboard
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper('I18N');
?>
<br>

<div>
  <div class="col-sm-12 col-md-12">
    <div class="panel panel-default panel-bordered">
      <div class="panel-body">
      <?php
      include_partial('dashboard_billing');
      ?>
      </div>
    </div>
  </div>
</div>

<?php
if($sf_user->mfHasCredential('access_applications'))
{
   $q = Doctrine_Query::create()
     ->from('FormGroups a')
   ->where('a.group_parent = ?', 0)
     ->orderBy('a.group_name ASC');
   $formgroups = $q->execute();
  
	//OTB Start - Multiagency fucntionality
	$agency_manager = new AgencyManager();
	//OTB End - Multiagency fucntionality

   foreach($formgroups as $formgroup)
   {
	   //OTB Start - Multiagency fucntionality
		$allowed_stages = $agency_manager->getAllowedStages($sf_user->getAttribute('userid'));
       $q = Doctrine_Query::create()
         ->from('ApForms a')
       ->where('a.form_group = ?', $formgroup->getGroupId());
       $appforms = $q->execute();
	   $form_group_stages = array();
	   foreach($appforms as $appform){
			array_push($form_group_stages,$appform->getFormStage()); 
	   }
		if(array_intersect($allowed_stages, $form_group_stages)){//OTB End - Multiagency fucntionality
   ?>
     <div>
       <div class="col-sm-8 col-md-9">
         <div class="panel panel-default panel-bordered">
           <div class="panel-body">
         <div class="row">
               <div class="col-sm-12">
                 <h5 class="subtitle mb5"><?php echo $formgroup->getGroupName(); ?></h5>
                 <p class="mb15"><?php echo $formgroup->getGroupDescription(); ?></p>
                 <div id="flotgroup<?php echo $formgroup->getGroupId(); ?>" style="width: 100%; height: 300px; margin-bottom: 20px"></div>
                 <script>
         jQuery(document).ready(function(){
             <?php
           $date = "";
           $day = "";
           $day[1] = 0; $date[1] = date('Y-m-d', strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-6 day" ) );
           $day[2] = 0; $date[2] = date('Y-m-d', strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-5 day" ) );
           $day[3] = 0; $date[3] = date('Y-m-d', strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-4 day" ) );
           $day[4] = 0; $date[4] = date('Y-m-d', strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-3 day" ) );
           $day[5] = 0; $date[5] = date('Y-m-d', strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-2 day" ) );
           $day[6] = 0; $date[6] = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 day" ) );
           $day[7] = 0; $date[7] = date("Y-m-d");

           for($x = 1; $x <= 7; $x++)
           {
             $q = Doctrine_Query::create()
                 ->from('FormGroups a')
                 ->where('a.group_parent = ?', $formgroup->getGroupId())
                 ->orderBy('a.group_name ASC');
               $groups = $q->execute();

               if(sizeof($groups) > 0)
               {
                 foreach($groups as $group)
                 {
                   foreach($groups as $group)
                   {
                     $total_applications = 0;

                     $q = Doctrine_Query::create()
                       ->from('ApFormGroups a')
                       ->where('a.group_id = ?', $group->getGroupId());
                     $apformgroups = $q->execute();

                     foreach($apformgroups as $apformgroup)
                     {
                       $q = Doctrine_Query::create()
                         ->from('ApForms a')
                         ->where('a.form_id = ?', $apformgroup->getFormId());
                       $form = $q->fetchOne();

                       if($form)
                       {
                           $q = Doctrine_Query::create()
                              ->from("FormEntry a")
                              ->where("a.date_of_submission LIKE ?", "%".$date[$x]."%")
                                                    ->andWhere("a.parent_submission = 0")
                              ->andWhere("a.approved <> 0")
                              ->andWhere("a.form_id = ?", $form->getFormId());
                           $results = $q->count();
                         $day[$x] = $day[$x] + $results;
                       }
                     }
                   }
                 }
               }
               else
               {
                                 $q = Doctrine_Query::create()
                                     ->from('ApForms a')
                                     ->where('a.form_group = ?', $formgroup->getGroupId());
                                 $forms = $q->execute();

                                 foreach($forms as $form)
                                 {
                                     $q = Doctrine_Query::create()
                                        ->from("FormEntry a")
                                        ->where("a.date_of_submission LIKE ?", "%".$date[$x]."%")
                                        ->andWhere("a.parent_submission = 0")
                                        ->andWhere("a.approved <> 0")
                                        ->andWhere("a.form_id = ?", $form->getFormId());
                                     $results = $q->count();
                                     $day[$x] = $day[$x] + $results;
                                 }
               }
           }
           ?>
           var submissions = [["<?php echo $date[1]; ?>", <?php echo $day[1]; ?>], ["<?php echo $date[2]; ?>", <?php echo $day[2]; ?>], ["<?php echo $date[3]; ?>",<?php echo $day[3]; ?>], ["<?php echo $date[4]; ?>", <?php echo $day[4]; ?>], ["<?php echo $date[5]; ?>", <?php echo $day[5]; ?>], ["<?php echo $date[6]; ?>", <?php echo $day[6]; ?>], ["<?php echo $date[7]; ?>", <?php echo $day[7]; ?>]];


                         jQuery.plot("#flotgroup<?php echo $formgroup->getGroupId(); ?>", [ submissions ], {
                             series: {
                                 lines: {
                                     lineWidth: 1
                                 },
                                 bars: {
                                     show: true,
                                     barWidth: 0.5,
                                     align: "center",
                                     lineWidth: 0,
                                     fillColor: "#428BCA"
                                 }
                             },
                             grid: {
                                 borderColor: '#ddd',
                                 borderWidth: 1,
                                 labelMargin: 10
                             },
                             xaxis: {
                                 mode: "categories",
                                 tickLength: 0
                             }
                         });


         });
         </script>
               </div><!-- col-sm-12 -->
             </div><!-- row -->
           </div><!-- panel-body -->
         </div><!-- panel -->
       </div><!-- col-sm-9 -->

       <div class="col-sm-4 col-md-3">

         <div class="panel panel-default panel-bordered">
           <div class="panel-body">
           <h5 class="subtitle mb5"><?php echo __('Applications'); ?></h5>
                             <p class="mb15"><?php echo __('Summary of the status of your system.'); ?></p>
                             <?php
         $q = Doctrine_Query::create()
           ->from('FormGroups a')
           ->where('a.group_parent = ?', $formgroup->getGroupId())
           ->orderBy('a.group_name ASC');
         $groups = $q->execute();

         if(sizeof($groups) > 0)
         {
           foreach($groups as $group)
           {
             $total_applications = 0;

             $q = Doctrine_Query::create()
               ->from('ApFormGroups a')
               ->where('a.group_id = ?', $group->getGroupId());
             $apformgroups = $q->execute();

             foreach($apformgroups as $apformgroup)
             {
               $q = Doctrine_Query::create()
                 ->from('ApForms a')
                 ->where('a.form_id = ?', $apformgroup->getFormId());
               $form = $q->fetchOne();

               if($form){

                                 $q = Doctrine_Query::create()
                                     ->from("FormEntry a")
                                     ->where("a.approved <> 0")
                                     ->andWhere("a.parent_submission = 0")
                                     ->andWhere("a.form_id = ?", $form->getFormId());
                                 $applications = $q->count();
                 $total_applications = $total_applications + $applications;
               }
             }

             ?>
                         <hr><span class="sublabel"><?php echo $group->getGroupName(); ?> <br><b>(<?php echo $total_applications." submissions"; ?>)</b></span>
                            <?php
           }

         }
         else
         {

             $q = Doctrine_Query::create()
               ->from('ApForms a')
               ->where('a.form_group = ?', $formgroup->getGroupId());
             $forms = $q->execute();

             foreach($forms as $form){
               $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
               mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);


                             $q = Doctrine_Query::create()
                                 ->from("FormEntry a")
                                 ->where("a.approved <> 0")
                                 ->andWhere("a.parent_submission = 0")
                                 ->andWhere("a.form_id = ?", $form->getFormId());
                             $applications = $q->count();

               $applicationsize = $applications;

             ?>
                             <hr><span class="sublabel"><?php echo $form->getFormName(); ?> <br><b>(<?php echo $applicationsize." submissions"; ?>)</b></span>
               <?php
           }
         }
         ?>
           </div><!-- panel-body -->
         </div><!-- panel -->

       </div><!-- col-sm-3 -->

     </div><!-- row -->
     <?php
		 }//OTB Start - Multiagency fucntionality
   }
}
?>
