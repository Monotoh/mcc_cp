<?php
/**
 * indexSuccess.php template.
 *
 * Displays a summary of all application related information for the reviewers
 *
 * @package    backend
 * @subpackage dashboard
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper('I18N');

include_partial('checksession');

function bd_nice_number($n) {
    // first strip any formatting;
    $n = (0+str_replace(",","",$n));

    // is this a number?
    if(!is_numeric($n)) return false;

    // now filter it;
    if($n>1000000000000) return round(($n/1000000000000),1).' T';
    else if($n>1000000000) return round(($n/1000000000),1).' B';
    else if($n>1000000) return round(($n/1000000),1).' M';

    return number_format($n);
}

$q = Doctrine_Query::create()
    ->from('CfUser a')
    ->where('a.nid = ?', $sf_user->getAttribute('userid'));
$thisuser = $q->fetchOne();
?>

<?php 
   $otbhelper = new OTBHelper();                         
?>

<?php
    $agency = new AgencyManager();
    $stage_ids_to_check = $agency->getAllowedStages($_SESSION["SESSION_CUTEFLOW_USERID"]);
?>

<div class="pageheader">
    <h2><i class="fa fa-home"></i> <?php echo __('Dashboard'); ?> <span><?php echo __('Summary of site activities'); ?></span></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><?php echo __('You are here'); ?>:</span>
        <ol class="breadcrumb">
            <li><a href="<?php echo public_path("backend.php"); ?>"><?php echo __('Home'); ?></a></li>
            <li class="active"><?php echo __('Dashboard'); ?></li>
        </ol>
    </div>
</div>

<div class="contentpanel">

    <?php
    $q = Doctrine_Query::create()
        ->from("Announcement a")
        ->where("'".date("m/d/Y")."' BETWEEN a.start_date AND a.end_date")
        ->andWhere("a.frontend = 2");
    $announcements = $q->execute();
    foreach($announcements as $announcement)
    {
        ?>
        <div class="row">
            <div class="col-sm-12">

                <div class="alert alert-info fade in nomargin">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                    <h4><?php echo __('Announcement'); ?></h4>
                    <p><?php echo $announcement->getContent(); ?></p>
                </div>

                <div class="mb15"></div>
            </div><!-- col-sm-6 -->
        </div><!-- row -->
    <?php
    }
    ?>

    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12">                         
                            <p class="mb15">The following is a summary of today's activities:</p>

                            <!-- Statistics on number of clients that have logged in today -->
                            <div class="col-sm-6 col-md-3">
                                <div class="panel panel-success panel-stat">
                                    <div class="panel-heading">
                                        <?php
                                        $q = Doctrine_Query::create()
                                            ->from("SfGuardUser a");
                                        $clients = $q->count();
                                        ?>
                                        <div class="stat">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <img src="<?php echo public_path(); ?>assets_unified/images/is-user.png" alt="" />
                                                </div>
                                                <div class="col-xs-8">
                                                    <small class="stat-label"><?php echo __('Clients Registered'); ?></small>
                                                            <h3>
                                                         <span id="total_clients_registered">
                                                        <?php 
                                                          // echo bd_nice_number($clients);
                                                      echo ("loading..") ;
                                                        ?>
                                                        </span>
                                                            </h3>
                                                     <script type="text/javascript"> 
                                                                                       //var ajax_call0 = function() 
                                                                                       //{
                                                                                            //your jQuery ajax code
                                                                                            $.ajax({
                                                                                                type : 'POST' ,
                                                                                                url: '<?php echo public_path() ?>backend.php/dashboard/clientsRegistered' ,
                                                                                                data: {                                                                              
		                                                                                },        
                                                                                                success: function(response) 
                                                                                                {
                                                                                                   
                                                                                                     var parsed_data = JSON.parse(response);
                                                                                                     var n = parsed_data ;
                                                                                                     
                                                                                                   
                                                                                                      $("#total_clients_registered").text(n.toLocaleString('en-US', {minimumFractionDigits: 0}));
                                                                                                   // alert("Success");
                                                                                                   //$("#auto_refresh").val(JSON.parse(response.responseText));
                                                                                                } ,
                                                                                                error: function(error) 
                                                                                                {
                                                                                                    console.log("Error PDF Printing ") ;
                                                                                                }
        
                
                
                
                
                                                                                             }) ;
                                                                                       //};
                                                                                       //var interval = 1000 * 60 * 1; // Every 1 minutes
                                                                                       //setInterval(ajax_call0, interval );
                                                                                    </script>
                                                </div>
                                            </div><!-- row -->

                                            <div class="mb15"></div>
                                            <?php
                                           
                                            ?>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <small class="stat-label"><?php echo __('Signed In'); ?></small>
                                                    <h4>
                                                        <span id="signed_in_today">
                                                            <?php echo "loading..." ?>
                                                        </span>
                                                    </h4>
                                                     <script type="text/javascript"> 
                                                                                       //var ajax_call1 = function() 
                                                                                       //{
                                                                                            //your jQuery ajax code
                                                                                            $.ajax({
                                                                                                type : 'POST' ,
                                                                                                url: '<?php echo public_path() ?>backend.php/dashboard/signedInToday' ,
                                                                                                data: {                                                                              
		                                                                                },        
                                                                                                success: function(response) 
                                                                                                {
                                                                                                   
                                                                                                     var parsed_data = JSON.parse(response);
                                                                                                     var n = parsed_data ;
                                                                                                     
                                                                                                   
                                                                                                      $("#signed_in_today").text(n.toLocaleString('en-US', {minimumFractionDigits: 0}));
                                                                                                   // alert("Success");
                                                                                                   //$("#auto_refresh").val(JSON.parse(response.responseText));
                                                                                                } ,
                                                                                                error: function(error) 
                                                                                                {
                                                                                                    console.log("Error signed_in_today ") ;
                                                                                                }
        
                
                
                
                
                                                                                             }) ;
                                                                                       //};
                                                                                       //var interval = 1000 * 60 * 1; // Every 1 minutes
                                                                                       //setInterval(ajax_call1, interval );
                                                                                    </script>
                                                </div>

                                                <div class="col-xs-6">
                                                    <small class="stat-label"><?php echo __('Signed Up'); ?></small>
                                                    <h4> <span id="signed_up_today">
                                                            loading..
                                                         </span></h4>
                                                    <script type="text/javascript"> 
                                                                                       //var ajax_call5 = function() 
                                                                                       //{
                                                                                            //your jQuery ajax code
                                                                                            $.ajax({
                                                                                                type : 'POST' ,
                                                                                                url: '<?php echo public_path() ?>backend.php/dashboard/clientsNew' ,
                                                                                                data: {                                                                              
		                                                                                },        
                                                                                                success: function(response) 
                                                                                                {
                                                                                                   
                                                                                                     var parsed_data = JSON.parse(response);
                                                                                                     var n = parsed_data ;
                                                                                                     
                                                                                                   
                                                                                                      $("#signed_up_today").text(n.toLocaleString('en-US', {minimumFractionDigits: 0}));
                                                                                                   // alert("Success");
                                                                                                   //$("#auto_refresh").val(JSON.parse(response.responseText));
                                                                                                } ,
                                                                                                error: function(error) 
                                                                                                {
                                                                                                    console.log("Error signed_up_today ") ;
                                                                                                }
        
                
                
                
                
                                                                                             }) ;
                                                                                       //};
                                                                                       //var interval = 1000 * 60 * 1; // Every 1 minutes
                                                                                       //setInterval(ajax_call5, interval );
                                                                                    </script>
                                                </div>
                                            </div><!-- row -->
                                        </div><!-- stat -->

                                    </div><!-- panel-heading -->
                                </div><!-- panel -->
                            </div><!-- col-sm-6 -->

                            <!-- Statistics on number of applications that have submitted today -->
                          
                            <div class="col-sm-6 col-md-3">
                                <div class="panel panel-danger panel-stat">
                                    <div class="panel-heading">
                                        <div class="stat">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <img src="<?php echo public_path(); ?>assets_unified/images/is-document.png" alt="" />
                                                </div>
                                                <div class="col-xs-8">
                                                    <small class="stat-label"><?php echo __('Plans Received Today'); ?></small>
                                                    <h3>
                                                       
                                                        <span id="app_received_today">
                                                            <?php echo "loading.." ?>
                                                        </span>
                                                    </h3>
                                                    <?php  //error_log(json_encode($stage_ids_to_check)) ?>
                                                    <script type="text/javascript"> 
                                                                                       //var ajax_call2 = function() 
                                                                                       //{
                                                                                            //your jQuery ajax code
                                                                                            $.ajax({
                                                                                                type : 'POST' ,
                                                                                                url: '<?php echo public_path() ?>backend.php/dashboard/appsReceivedToday' ,
                                                                                                data: {  
                                                                                                    
		                                                                                },        
                                                                                              
                                                                                                success: function(response) 
                                                                                                {
                                                                                                   
                                                                                                     var parsed_data = JSON.parse(response);
                                                                                                     var n = parsed_data ;
                                                                                                 
                                                                                                      $("#app_received_today").text(n.toLocaleString('en-US', {minimumFractionDigits: 0}));
                                                                                                   // alert("Success");
                                                                                                   //$("#auto_refresh").val(JSON.parse(response.responseText));
                                                                                                } ,
                                                                                                error: function(error) 
                                                                                                {
                                                                                                    console.log("Error app_received_today ") ;
                                                                                                }
        
                
                
                
                
                                                                                             }) ;
                                                                                       //};
                                                                                       //var interval = 1000 * 60 * 0.5; // Every 1 minutes
                                                                                       //setInterval(ajax_call2, interval );
                                                                                    </script>
                                                </div>
                                            </div><!-- row -->

                                            <div class="mb15"></div>
                                            <?php
                                            
                                            ?>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <small class="stat-label"><?php echo __('Approved'); ?></small>
                                                    <h4>
                                                        <span id="approved_today">loading..</span>
                                                    </h4>
                                                     <script type="text/javascript"> 
                                                    //var ajax_call3 = function() 
                                                    //{
                                                         //your jQuery ajax code
                                                         $.ajax({
                                                             type : 'POST' ,
                                                             url: '<?php echo public_path() ?>backend.php/dashboard/approvedToday' ,
                                                             data: {  
                                                                
                                                             },        

                                                             success: function(response) 
                                                             {

                                                                  var parsed_data = JSON.parse(response);
                                                                  var n = parsed_data ;

                                                                   $("#approved_today").text(n.toLocaleString('en-US', {minimumFractionDigits: 0}));
                                                                // alert("Success");
                                                                //$("#auto_refresh").val(JSON.parse(response.responseText));
                                                             } ,
                                                             error: function(error) 
                                                             {
                                                                 console.log("Error approved_today ") ;
                                                             }





                                                          }) ;
                                                    //};
                                                    //var interval = 1000 * 60 * 0.5; // Every 1 minutes
                                                    //setInterval(ajax_call3, interval );
                                                 </script>
                                                </div>
                                            </div><!-- row -->

                                        </div><!-- stat -->

                                    </div><!-- panel-heading -->
                                </div><!-- panel -->
                            </div><!-- col-sm-6 -->

                            <!-- Statistics on number of tasks that have issued today -->
                            <div class="col-sm-6 col-md-3">
                                <div class="panel panel-primary panel-stat">
                                    <div class="panel-heading">
                                        <?php
                                       
                                        ?>
                                        <div class="stat">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <img src="<?php echo public_path(); ?>assets_unified/images/is-document.png" alt="" />
                                                </div>
                                                <div class="col-xs-8">
                                                    <small class="stat-label"><?php echo __('Tasks Assigned Today'); ?></small>
                                                    <h3>
                                                        <span id="task_assigned_today">
                                                            loading...
                                                        </span>
                                                    </h3>
                                                    <script type="text/javascript"> 
                                                    //var ajax_call6 = function() 
                                                    //{
                                                         //your jQuery ajax code
                                                         $.ajax({
                                                             type : 'POST' ,
                                                             url: '<?php echo public_path() ?>backend.php/dashboard/taskAssignedToday' ,
                                                             data: {  
                                                                
                                                             },        

                                                             success: function(response) 
                                                             {

                                                                  var parsed_data = JSON.parse(response);
                                                                  var n = parsed_data ;

                                                                   $("#task_assigned_today").text(n.toLocaleString('en-US', {minimumFractionDigits: 0}));
                                                                // alert("Success");
                                                                //$("#auto_refresh").val(JSON.parse(response.responseText));
                                                             } ,
                                                             error: function(error) 
                                                             {
                                                                 console.log("Error TaskAssignedToday ") ;
                                                             }





                                                          }) ;
                                                    //};
                                                    //var interval = 1000 * 60 * 0.5; // Every 1 minutes
                                                    //setInterval(ajax_call6, interval );
                                                 </script>
                                                </div>
                                            </div><!-- row -->

                                            <div class="mb15"></div>
                                           
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <small class="stat-label"><?php echo __('Completed'); ?></small>
                                                            <h4><span id="task_completed">
                                                        loading...
                                                        </span></h4>
                                                    <script type="text/javascript"> 
                                                    //var ajax_call7 = function() 
                                                    //{
                                                         //your jQuery ajax code
                                                         $.ajax({
                                                             type : 'POST' ,
                                                             url: '<?php echo public_path() ?>backend.php/dashboard/taskCompleted' ,
                                                             data: {  
                                                                
                                                             },        

                                                             success: function(response) 
                                                             {

                                                                  var parsed_data = JSON.parse(response);
                                                                  var n = parsed_data ;

                                                                   $("#task_completed").text(n.toLocaleString('en-US', {minimumFractionDigits: 0}));
                                                                // alert("Success");
                                                                //$("#auto_refresh").val(JSON.parse(response.responseText));
                                                             } ,
                                                             error: function(error) 
                                                             {
                                                                 console.log("Error task_completed ") ;
                                                             }





                                                          }) ;
                                                    //};
                                                    //var interval = 1000 * 60 * 0.5; // Every 1 minutes
                                                    //setInterval(ajax_call7, interval );
                                                 </script>
                                                </div>
                                            </div><!-- row -->

                                        </div><!-- stat -->

                                    </div><!-- panel-heading -->
                                </div><!-- panel -->
                            </div><!-- col-sm-6 -->

                            <!-- Statistics on number of invoices that have issued today -->
                            <div class="col-sm-6 col-md-3">
                                <div class="panel panel-warning panel-stat">
                                    <div class="panel-heading">
                                      
                                        <div class="stat">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                 <img src="<?php echo public_path(); ?>assets_unified/images/is-document.png" alt="" /> </div>
                                                <div class="col-xs-8">
                                                    <small class="stat-label"><?php echo __('Invoices Sent Today'); ?></small>
                                                            <h3><span id="invoices_sent">
                                                        loading...
                                                        </span></h3>
                                                     <script type="text/javascript"> 
                                                    //var ajax_call8 = function() 
                                                    //{
                                                         //your jQuery ajax code
                                                         $.ajax({
                                                             type : 'POST' ,
                                                             url: '<?php echo public_path() ?>backend.php/dashboard/invoiceSentToday' ,
                                                             data: {  
                                                                
                                                             },        

                                                             success: function(response) 
                                                             {

                                                                  var parsed_data = JSON.parse(response);
                                                                  var n = parsed_data ;

                                                                   $("#invoices_sent").text(n.toLocaleString('en-US', {minimumFractionDigits: 0}));
                                                                // alert("Success");
                                                                //$("#auto_refresh").val(JSON.parse(response.responseText));
                                                             } ,
                                                             error: function(error) 
                                                             {
                                                                 console.log("Error InvoiceSentToday ") ;
                                                             }





                                                          }) ;
                                                    //};
                                                    //var interval = 1000 * 60 * 0.5; // Every 1 minutes
                                                    //setInterval(ajax_call8, interval );
                                                 </script>
                                                </div>
                                            </div><!-- row -->

                                            <div class="mb15"></div>

                                            <div class="row">

                                                <div class="col-xs-6">
                                                    <small class="stat-label"><?php echo __('Invoices Paid'); ?></small>
                                                    <h4>
                                                        <span id="invoices_paid">
                                                        loading...
                                                        </span>
                                                    </h4>
                                                    <script type="text/javascript"> 
                                                    //var ajax_call9 = function() 
                                                    //{
                                                         //your jQuery ajax code
                                                         $.ajax({
                                                             type : 'POST' ,
                                                             url: '<?php echo public_path() ?>backend.php/dashboard/invoicesPaid' ,
                                                             data: {  
                                                                
                                                             },        

                                                             success: function(response) 
                                                             {

                                                                  var parsed_data = JSON.parse(response);
                                                                  var n = parsed_data ;

                                                                   $("#invoices_paid").text(n.toLocaleString('en-US', {minimumFractionDigits: 0}));
                                                                // alert("Success");
                                                                //$("#auto_refresh").val(JSON.parse(response.responseText));
                                                             } ,
                                                             error: function(error) 
                                                             {
                                                                 console.log("Error invoices_paid ") ;
                                                             }





                                                          }) ;
                                                    //};
                                                    //var interval = 1000 * 60 * 0.5; // Every 1 minutes
                                                    //setInterval(ajax_call9, interval );
                                                 </script>
                                                </div>
                                            </div><!-- row -->

                                        </div><!-- stat -->

                                    </div><!-- panel-heading -->
                                </div><!-- panel -->
                            </div><!-- col-sm-6 -->                         
                        </div><!-- col-sm-8 -->
                    </div><!-- row -->
                </div><!-- panel-body -->
            </div><!-- panel -->
        </div><!-- col-sm-9 -->

    </div><!-- row -->
    
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <h5 class="subtitle mb5">Overall Summary of Denied & Sent back for Corrections</h5>
                            <p class="mb15">Below is a an overall summary for denied/declined applications,total number of applications sent back to clients for corrections:</p>
                           <!-- OTB patch - Denied Applications -->
                            <div class="col-sm-6 col-md-3">
                                <div class="panel panel-success panel-stat">
                                    <div class="panel-heading">
                                       
                                        <div class="stat">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                   <img src="<?php echo public_path(); ?>assets_unified/images/is-document.png" alt="" />
                                                </div>
                                                <div class="col-xs-8">
                                                    <small class="stat-label"><?php echo __('Declined Applications'); ?></small>
                                                    <h3>
                                                        <span id="declined_apps">
                                                            loading...
                                                        </span>
                                                    </h3>
                                                     <script type="text/javascript"> 
                                                    //var ajax_call12 = function() 
                                                    //{
                                                         //your jQuery ajax code
                                                         $.ajax({
                                                             type : 'POST' ,
                                                             url: '<?php echo public_path() ?>backend.php/dashboard/declinedApplications' ,
                                                             data: {  
                                                                
                                                             },        

                                                             success: function(response) 
                                                             {

                                                                  var parsed_data = JSON.parse(response);
                                                                  var n = parsed_data ;

                                                                   $("#declined_apps").text(n.toLocaleString('en-US', {minimumFractionDigits: 0}));
                                                                // alert("Success");
                                                                //$("#auto_refresh").val(JSON.parse(response.responseText));
                                                             } ,
                                                             error: function(error) 
                                                             {
                                                                 console.log("Error declined_apps ") ;
                                                             }





                                                          }) ;
                                                    //};
                                                    //var interval = 1000 * 60 * 1; // Every 1 minutes
                                                    //setInterval(ajax_call12, interval );
                                                 </script>
                                                </div>
                                            </div><!-- row -->

                                            <div class="mb15"></div>

                                            <div class="row">

                                                <div class="col-xs-6">
                                                    <small class="stat-label"><?php echo __('Denied Applications'); ?></small>
                                                   
                                                </div>
                                            </div><!-- row -->

                                        </div><!-- stat -->

                                    </div><!-- panel-heading -->
                                </div><!-- panel -->
                            </div><!-- col-sm-6 -->
                            
                             <!-- OTB patch - Sent Back for Corrections Applications -->
                            <div class="col-sm-6 col-md-3">
                                <div class="panel panel-primary panel-stat">
                                    <div class="panel-heading">
                                       
                                        <div class="stat">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                   <img src="<?php echo public_path(); ?>assets_unified/images/is-document.png" alt="" />
                                                </div>
                                                <div class="col-xs-8">
                                                    <small class="stat-label"><?php echo __('Sent Back to Client'); ?></small>
                                                    <h3>
                                                        <span id="sent_back_to_architect">
                                                            loading...
                                                        </span>
                                                    </h3>
                                                     <script type="text/javascript"> 
                                                    //var ajax_call10 = function() 
                                                    //{
                                                         //your jQuery ajax code
                                                         $.ajax({
                                                             type : 'POST' ,
                                                             url: '<?php echo public_path() ?>backend.php/dashboard/sentBackApplications' ,
                                                             data: {  
                                                                
                                                             },        

                                                             success: function(response) 
                                                             {

                                                                  var parsed_data = JSON.parse(response);
                                                                  var n = parsed_data ;

                                                                   $("#sent_back_to_architect").text(n.toLocaleString('en-US', {minimumFractionDigits: 0}));
                                                                // alert("Success");
                                                                //$("#auto_refresh").val(JSON.parse(response.responseText));
                                                             } ,
                                                             error: function(error) 
                                                             {
                                                                 console.log("Error sent_back_to_architect ") ;
                                                             }





                                                          }) ;
                                                    //};
                                                    //var interval = 1000 * 60 * 1; // Every 1 minutes
                                                    //setInterval(ajax_call10, interval );
                                                 </script>
                                                </div>
                                            </div><!-- row -->

                                            <div class="mb15"></div>

                                            <div class="row">

                                                <div class="col-xs-6">
                                                    <small class="stat-label"><?php echo __('Applications With clients'); ?></small>
                                                   
                                                </div>
                                            </div><!-- row -->

                                        </div><!-- stat -->

                                    </div><!-- panel-heading -->
                                </div><!-- panel -->
                            </div><!-- col-sm-6 -->
                            
                            <?php  if($sf_user->mfHasCredential("access_delayed_applications")): ?>
                             <!-- OTB patch - Delayed Applications -->
                            <div class="col-sm-6 col-md-3">
                                <div class="panel panel-info panel-stat">
                                    <div class="panel-heading">
                                       
                                        <div class="stat">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <img src="<?php echo public_path(); ?>assets_unified/images/is-document.png" alt="" />
                                                </div>
                                                <div class="col-xs-8">
                                                    <small class="stat-label"><?php echo __('Delayed Applications'); ?></small>
                                                    <h3>
                                                        <span id="delayed_applications">
                                                            loading...
                                                        </span>
                                                    </h3>
                                                    <script type="text/javascript"> 
                                                    //var ajax_call11 = function() 
                                                    //{
                                                         //your jQuery ajax code
                                                         $.ajax({
                                                             type : 'POST' ,
                                                             url: '<?php echo public_path() ?>backend.php/dashboard/delayedApps' ,
                                                             data: {  
                                                                
                                                             },        

                                                             success: function(response) 
                                                             {

                                                                  var parsed_data = JSON.parse(response);
                                                                  var n = parsed_data ;

                                                                   $("#delayed_applications").text(n.toLocaleString('en-US', {minimumFractionDigits: 0}));
                                                                // alert("Success");
                                                                //$("#auto_refresh").val(JSON.parse(response.responseText));
                                                             } ,
                                                             error: function(error) 
                                                             {
                                                                 console.log("Error delayed_applications ") ;
                                                             }





                                                          }) ;
                                                    //};
                                                    //var interval = 1000 * 60 * 1; // Every 1 minutes
                                                    //setInterval(ajax_call11, interval );
                                                 </script>
                                                </div>
                                            </div><!-- row -->

                                            <div class="mb15"></div>

                                            <div class="row">

                                                <div class="col-xs-6">
                                                    <small class="stat-label"><?php echo __('Delayed Applications'); ?></small>
                                                  
                                                </div>
                                            </div><!-- row -->

                                        </div><!-- stat -->

                                    </div><!-- panel-heading -->
                                </div><!-- panel -->
                            </div><!-- col-sm-6 -->
                            
                            <?php endif; ?>
                        </div>
                        
                    </div>
                </div>
            </div>
       </div> 
    </div>
                    



    <?php if($sf_user->mfHasCredential("access_billing")){ ?>
    <div class="row">
    <div class="col-sm-12 col-md-12">

    <div class="panel panel-default">

    <div class="panel-body">
    <div class="row">
    <div class="col-sm-12">
    <h5 class="subtitle mb5">Revenue</h5>
    <p class="mb15">The following is a summary of today's income from paid invoices:</p>

    <ul class="nav nav-tabs nav-tab-special">
        <li class=""><a data-toggle="tab" href="#thisyear"> Annual</a></li>
        <li class=""><a data-toggle="tab" href="#thismonth"> Monthly</a></li>
        <li class=""><a data-toggle="tab" href="#thisweek"> This Week</a></li>
        <li class="active"><a data-toggle="tab" href="#today"> Today</a></li>

    </ul>
    <div class="panel-body">

    <!-- Tab panes -->
    <div class="tab-content tab-content-nopadding">
    <div id="today" class="tab-pane  active">





        <script>
            jQuery(document).ready(function(){
                /***** BAR CHART *****/

                    <?php
                      $earnings = null;

                      $search = null;
                      $total_earnings = 0;

                      $hours = null;

                      for($hour = 0; $hour <= 24; $hour++)
                      {
                        $search = $hour;
                        if($hour < 10)
                        {
                          $search = "0".$hour;
                        }
                        $hours[] = $search;

                        $q = Doctrine_Query::create()
                           ->select("SUM(b.amount) AS total")
                           ->from("MfInvoice a")
                           ->leftJoin("a.mfInvoiceDetail b")
                           ->where("a.created_at LIKE ?", "%".date('Y-m-d')." ".$search."%")
                           ->andWhere("a.paid = 2 or a.paid = 1")//OTB - In accounting terms, revenue should include both paid and unpaid invoices
                           ->andWhere("b.description LIKE ? OR b.description LIKE ?", array("%total%", "%submission fee%"));
                        $invoice = $q->fetchOne();
                        $count = 0;
                        if($invoice)
                        {
                           $count++;
                           $earnings['hour'.$search] = $invoice->getTotal();
                           if(empty($earnings['hour'.$search]))
                           {
                              $earnings['hour'.$search] = 0;
                           }
                           $total_earnings += $invoice->getTotal();
                        }
                      }
                    ?>

                var bardata = [ <?php $count = 0; foreach($hours as $hour){ ?>["<?php echo $hour; ?>hrs", <?php echo $earnings['hour'.$hour]; ?>], <?php } ?> ];

                jQuery.plot("#barchart4", [ bardata ], {
                    series: {
                        lines: {
                            lineWidth: 1
                        },
                        bars: {
                            show: true,
                            barWidth: 0.5,
                            align: "center",
                            lineWidth: 0,
                            fillColor: "#428BCA"
                        }
                    },
                    grid: {
                        borderColor: '#ddd',
                        borderWidth: 1,
                        labelMargin: 10
                    },
                    xaxis: {
                        mode: "categories",
                        tickLength: 0
                    }
                });

                function labelFormatter(label, series) {
                    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
                }

            });
        </script>

        <div class="tinystat mr20">
            <div class="datainfo">
                <span class="text-muted">Total</span>
                <h4><?php echo sfConfig::get('app_currency').". ".$total_earnings; ?></h4>
            </div>
        </div>

        <div id="barchart4" style="width: 100%; height: 300px"></div>



        <div style="margin-top:20px"></div>











    </div>
    <div id="thisweek" class="tab-pane">














        <script>
            jQuery(document).ready(function(){
                /***** BAR CHART *****/

                    <?php
                      $earnings = null;

                      $search = null;
                      $total_earnings = 0;

                      $week_start = new DateTime();
                      $week_start->setISODate(date('Y'),date('W', strtotime(date('Y-m-d'))));

                      $start_date = $week_start->format('Y-m-d');
                      $days = null;
                      $days[] = $start_date;
                      $count = 0;
                      while($count < 6)
                      {
                         $count++;
                         $days[] = date('Y-m-d', strtotime( date( "Y-m-d", strtotime( $start_date ) ) . "+".$count." day" ) );
                      }

                      foreach($days as $day)
                      {
                        $q = Doctrine_Query::create()
                           ->select("SUM(b.amount) AS total")
                           ->from("MfInvoice a")
                           ->leftJoin("a.mfInvoiceDetail b")
                           ->where("a.created_at LIKE ?", "%".$day."%")
                           ->andWhere("a.paid = 2 or a.paid = 1")//OTB - In accounting terms, revenue should include both paid and unpaid invoices
                           ->andWhere("b.description LIKE ? OR b.description LIKE ?", array("%total%", "%submission fee%"));
                        $invoice = $q->fetchOne();
                        $count = 0;
                        if($invoice)
                        {
                           $count++;
                           $earnings['day'.$day] = $invoice->getTotal();
                           if(empty($earnings['day'.$day]))
                           {
                              $earnings['day'.$day] = 0;
                           }
                           $total_earnings += $invoice->getTotal();
                        }
                      }
                    ?>

                var bardata = [ <?php $count = 0; foreach($days as $day){
                          $count++;

                          $label = "";
                          switch($count)
                          {
                            case 1:
                              $label = "Mon";
                              break;
                            case 2:
                              $label = "Tue";
                               break;
                            case 3:
                              $label = "Wed";
                              break;
                            case 4:
                              $label = "Thu";
                              break;
                            case 5:
                              $label = "Fri";
                              break;
                            case 6:
                              $label = "Sat";
                              break;
                            case 7:
                              $label = "Sun";
                              break;
                          }

                      ?>["<?php echo $label; ?>", <?php echo $earnings['day'.$day]; ?>], <?php } ?> ];

                jQuery.plot("#barchart2", [ bardata ], {
                    series: {
                        lines: {
                            lineWidth: 1
                        },
                        bars: {
                            show: true,
                            barWidth: 0.5,
                            align: "center",
                            lineWidth: 0,
                            fillColor: "#428BCA"
                        }
                    },
                    grid: {
                        borderColor: '#ddd',
                        borderWidth: 1,
                        labelMargin: 10
                    },
                    xaxis: {
                        mode: "categories",
                        tickLength: 0
                    }
                });

                function labelFormatter(label, series) {
                    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
                }

            });
        </script>


        <div class="tinystat mr20">
            <div class="datainfo">
                <span class="text-muted">THIS WEEKS Total</span>
                <h4><?php echo sfConfig::get('app_currency').". ".$total_earnings; ?></h4>
            </div>
        </div>


        <div id="barchart2" style="width: 100%; height: 300px"></div>


        <div style="margin-top:20px"></div>








    </div>
    <div id="thismonth" class="tab-pane">


















        <h5 class="subtitle mb5">Month:
            <select name='choosemonth' id='choosemonth'>
                <?php
                for($x = 1; $x <= 12; $x++)
                {
                    $selected = "";
                    if($x == date("m"))
                    {
                        $selected = "selected='selected'";
                    }
                    echo "<option value='".$x."' ".$selected.">".$x."</option>";
                }
                ?>
            </select>
            <script>
                jQuery(document).ready(function(){
                    $("#choosemonth").on('change', function() {
                        $("#loadmonth").load("/backend.php/dashboard/fetchmonth/month/" + this.value);
                    });
                });
            </script>
        </h5>
        <div id="loadmonth" name="loadmonth">
            <div id="barchart1" style="width: 100%; height: 300px"></div>
            <script>
                jQuery(document).ready(function(){
                    /***** BAR CHART *****/

                        <?php
                          $earnings = null;

                          $search = null;
                          $total_earnings = 0;

                          for($day = 1; $day <= 31; $day++)
                          {
                            if($day < 10)
                            {
                                $search = date('Y')."-".date('m')."-0".$day;
                            }
                            else
                            {
                                $search = date('Y')."-".date('m')."-".$day;
                            }

                            $q = Doctrine_Query::create()
                               ->select("SUM(b.amount) AS total")
                               ->from("MfInvoice a")
                               ->leftJoin("a.mfInvoiceDetail b")
                               ->where("a.created_at LIKE ?", "%".$search."%")
                               ->andWhere("a.paid = 2 or a.paid = 1")//OTB - In accounting terms, revenue should include both paid and unpaid invoices
                               ->andWhere("b.description LIKE ? OR b.description LIKE ?", array("%total%", "%submission fee%"));
                            $invoice = $q->fetchOne();
                            if($invoice)
                            {
                               $earnings['day'.$day] = $invoice->getTotal();
                               if(empty($earnings['day'.$day]))
                               {
                                  $earnings['day'.$day] = 0;
                               }
                               $total_earnings += $invoice->getTotal();
                            }
                          }
                        ?>

                    var bardata = [ <?php for($day = 1; $day <= 31; $day++){ ?>["<?php echo $day; ?>", <?php echo $earnings['day'.$day]; ?>], <?php } ?> ];

                    jQuery.plot("#barchart1", [ bardata ], {
                        series: {
                            lines: {
                                lineWidth: 1
                            },
                            bars: {
                                show: true,
                                barWidth: 0.5,
                                align: "center",
                                lineWidth: 0,
                                fillColor: "#428BCA"
                            }
                        },
                        grid: {
                            borderColor: '#ddd',
                            borderWidth: 1,
                            labelMargin: 10
                        },
                        xaxis: {
                            mode: "categories",
                            tickLength: 0
                        }
                    });

                    function labelFormatter(label, series) {
                        return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
                    }

                });
            </script>
            <div class="tinystat mr20" style="margin-top:40px">

                <div class="datainfo">
                    <span class="text-muted">Total</span>
                    <h4><?php echo sfConfig::get('app_currency').". ".$total_earnings; ?></h4>
                </div>
            </div>



        </div>














    </div>



    <div id="thisyear" class="tab-pane">













        <?php $year = date("Y"); ?>
        <h5 class="subtitle mt10">Year:
            <select name='chooseyear' id='chooseyear'>
                <?php
                for($x = $year; $x > 2008; $x--)
                {
                    echo "<option value='".$x."'>".$x."</option>";
                }
                ?>
            </select>
            <script>
                jQuery(document).ready(function(){
                    $("#chooseyear").on('change', function() {
                        $("#loadyear").load("/backend.php/dashboard/fetchyear/year/" + this.value);
                    });
                });
            </script>
        </h5>
        <div id="loadyear" name="loadyear">
            <div id="barchart" style="width: 100%; height: 300px"></div>

            <script>
                jQuery(document).ready(function(){
                    /***** BAR CHART *****/

                        <?php
                          $earnings = null;
                          $earnings['month1'] = 0;
                          $earnings['month2'] = 0;
                          $earnings['month3'] = 0;
                          $earnings['month4'] = 0;
                          $earnings['month5'] = 0;
                          $earnings['month6'] = 0;
                          $earnings['month7'] = 0;
                          $earnings['month8'] = 0;
                          $earnings['month9'] = 0;
                          $earnings['month10'] = 0;
                          $earnings['month11'] = 0;
                          $earnings['month12'] = 0;

                          $search = null;
                          $total_earnings = 0;

                          for($month = 1; $month <= 12; $month++)
                          {
                            if($month < 10)
                            {
                              $search = $year."-0".$month."%";
                            }
                            else
                            {
                              $search = $year."-".$month."%";
                            }

                            $q = Doctrine_Query::create()
                               ->select("SUM(b.amount) AS total")
                               ->from("MfInvoice a")
                               ->leftJoin("a.mfInvoiceDetail b")
                               ->leftJoin("a.FormEntry c")
                               ->where("a.created_at LIKE ?", $search)
                               ->andWhere("a.paid = 2 or a.paid = 1")//OTB - In accounting terms, revenue should include both paid and unpaid invoices
                               ->andWhere("b.description LIKE ? OR b.description LIKE ?", array("%total%", "%submission fee%"));
                            $invoice = $q->fetchOne();
                            if($invoice)
                            {
                               $earnings['month'.$month] = $invoice->getTotal();
                               if(empty($earnings['month'.$month]))
                               {
                                  $earnings['month'.$month] = 0;
                               }
                               $total_earnings += $invoice->getTotal();
                            }
                          }
                        ?>

                    var bardata = [ ["Jan", <?php echo $earnings['month1']; ?>], ["Feb", <?php echo $earnings['month2']; ?>], ["Mar", <?php echo $earnings['month3']; ?>], ["Apr", <?php echo $earnings['month4']; ?>], ["May", <?php echo $earnings['month5']; ?>], ["Jun", <?php echo $earnings['month6']; ?>], ["Jul", <?php echo $earnings['month7']; ?>], ["Aug", <?php echo $earnings['month8']; ?>], ["Sep", <?php echo $earnings['month9']; ?>], ["Oct", <?php echo $earnings['month10']; ?>], ["Nov", <?php echo $earnings['month11']; ?>], ["Dec", <?php echo $earnings['month12']; ?>] ];

                    jQuery.plot("#barchart", [ bardata ], {
                        series: {
                            lines: {
                                lineWidth: 1
                            },
                            bars: {
                                show: true,
                                barWidth: 0.5,
                                align: "center",
                                lineWidth: 0,
                                fillColor: "#428BCA"
                            }
                        },
                        grid: {
                            borderColor: '#ddd',
                            borderWidth: 1,
                            labelMargin: 10
                        },
                        xaxis: {
                            mode: "categories",
                            tickLength: 0
                        }
                    });

                    function labelFormatter(label, series) {
                        return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
                    }

                });
            </script>

            <div class="tinystat mr20" style="margin-top:40px">
                <div class="datainfo">
                    <span class="text-muted">Total</span>
                    <h4><?php echo sfConfig::get('app_currency').". ".$total_earnings; ?></h4>
                </div>
            </div>



        </div>















    </div>
    </div>






    </div></div></div>



    </div><!-- panel-body -->
    </div><!-- panel-default-special -->
    </div><!-- cotentpanel -->
</div><!-- row -->
<?php } ?>


    <div class="row">
        <div class="col-sm-8 col-md-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <h5 class="subtitle mb5">Tasks</h5>
                            <p class="mb15">The following is a summary of tasks you need to work on:</p>
                            <div class="table-responsive">
                                <table class="table table-primary table-buglist">
                                    <?php
                                    $q = Doctrine_Query::create()
                                        ->from("Task a")
                                        ->leftJoin("a.Owner b")
                                        ->leftJoin("a.Application c")
                                        ->leftJoin("a.Creator d")
                                        ->where("a.owner_user_id = ? and a.status = ? and c.id = a.application_id", array($_SESSION["SESSION_CUTEFLOW_USERID"], 1))
                                        ->orWhere("a.creator_user_id = ? and a.status = ? and c.id = a.application_id", array($_SESSION["SESSION_CUTEFLOW_USERID"], 2))
                                        ->orderBy("a.id DESC");

                                    $pager = new sfDoctrinePager('Task', 10);
                                    $pager->setQuery($q);
                                    $pager->setPage(1);
                                    $pager->init();

                                    if($pager->getNbResults() > 0) {
                                        ?>
                                        <thead>
                                        <tr>
                                            <th style="background-color: #428BCA;">Type</th>
                                            <th style="background-color: #428BCA;">Key</th>
                                            <th style="background-color: #428BCA;">Description</th>
                                            <th style="background-color: #428BCA;">Priority</th>
                                            <th style="background-color: #428BCA;">Status</th>
                                            <th style="background-color: #428BCA;"></th>
                                        </tr>
                                        </thead>
                                    <?php
                                    }
                                    ?>
                                    <tbody>
                                    <?php
                                    //construct today's date
                                    $today = date("Y-m-d");
                                    $today_time = strtotime($today);
                                    foreach ($pager->getResults() as $task): ?>
                                    <!-- OTB Fix -->
                                    <!-- If a Task is overdue - mark the task with a red color -->
                                     <?php    $expire_time = strtotime($task->getEndDate()); ?>
                                        <?php if(  $expire_time < $today_time ) { ?>
                                        <tr>
                                            <td style="background-color: #A94442; font-color: #FFFFFF;"><i class="fa fa-tasks tooltips" data-toggle="tooltip" title="Bug"></i></td>
                                            <td style="background-color: #A94442; font-color: #FFFFFF;"><a href="/backend.php/tasks/view/id/<?php echo $task->getId(); ?>"><?php echo $task->getApplication()->getApplicationId(); ?></a></td>
                                            <td style="background-color: #A94442; font-color: #FFFFFF;"><a href="/backend.php/tasks/view/id/<?php echo $task->getId(); ?>"><?php echo ($task->getRemarks()?$task->getRemarks():$task->getTypeName()." task") ?></a></td>
                                            <td><?php echo $task->getPriorityName(); ?></td>
                                            <td><?php echo $task->getStatusName(); ?></td>
                                            <td>
                                                <a  title='<?php echo __('View Task'); ?>' href='<?php echo public_path("backend.php/tasks/view/id/".$task->getId()); ?>'> <span class="badge badge-primary"><i class="fa fa-eye"></i></span></a>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                        
                                        <?php if(  $expire_time > $today_time ) { ?>
											 <tr>
                                            <td><i class="fa fa-tasks tooltips" data-toggle="tooltip" title="Bug"></i></td>
                                            <td><a href="/backend.php/tasks/view/id/<?php echo $task->getId(); ?>"><?php echo $task->getApplication()->getApplicationId(); ?></a></td>
                                            <td><a href="/backend.php/tasks/view/id/<?php echo $task->getId(); ?>"><?php echo ($task->getRemarks()?$task->getRemarks():$task->getTypeName()." task") ?></a></td>
                                            <td><?php echo $task->getPriorityName(); ?></td>
                                            <td><?php echo $task->getStatusName(); ?></td>
                                            <td>
                                                <a  title='<?php echo __('View Task'); ?>' href='<?php echo public_path("backend.php/tasks/view/id/".$task->getId()); ?>'> <span class="badge badge-primary"><i class="fa fa-eye"></i></span></a>
                                            </td>
                                        </tr>
									    <?php } ?>		
                                    <?php
                                    endforeach;
                                    ?>
                                    </tbody>
                                </table>
                                <div align="center">
                                    <?php
                                    if($pager->getNbResults() > 0) {
                                        ?>
                                       <!-- <a  href="/backend.php/tasks/list" class="btn btn-primary" name="submitbuttonname" id="submitbutton"  value="submitbuttonvalue"><php echo __('View More'); ?></a> -->
                                    <?php
                                    }
                                    else
                                    {
                                        ?>
                                        <h4>No Tasks Found</h4>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div><!-- table-responsive -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-4 col-md-4">
            <div class="panel panel-default panel-alt widget-messaging">
                <div class="panel-body">
                    <h5 class="subtitle mb5">Messages</h5>
                    <p class="mb15">List of recent messages from users:</p>
                    <ul>
                        <?php
                        $q = Doctrine_Query::create()
                            ->from('Communications a')
                            ->leftJoin('a.FormEntry b')
                            ->where('b.approved <> 0')
                            ->andWhere('a.architect_id <> ?', "")
                            ->andWhere('a.messageread = ?', 0)
                            ->orderBy("a.id DESC");

                        $pager = new sfDoctrinePager('Communications', 5);
                        $pager->setQuery($q);
                        $pager->setPage(1);
                        $pager->init();

                        foreach ($pager->getResults() as $message): ?>
                        <li><a href="<?php echo public_path(); ?>backend.php/applications/view/id/<?php echo $message->getApplicationId(); ?>/messages/read">
                            <small class="pull-right"><?php echo $message->getActionTimestamp(); ?></small>
                            <h4 class="sender">
                                <?php
                                $q = Doctrine_Query::create()
                                    ->from("SfGuardUserProfile a")
                                    ->where("a.user_id = ?", $message->getArchitectId());
                                $sender = $q->fetchOne();

                                echo $sender->getFullname();
                                ?>
                            </h4>
                            <small><?php
                                $words = explode(" ", html_entity_decode($message->getContent()));
                                echo implode(" ",array_splice($words,0,5))."....";
                            ?></small></a>
                        </li>
                        <?php
                        endforeach;
                        ?>
                    </ul>
                    <div align="center">
                        <?php
                        if($pager->getNbResults() > 0) {
                            ?>
                            <a  href="/backend.php/applications/messages" class="btn btn-primary" name="submitbuttonname" id="submitbutton"  value="submitbuttonvalue"><?php echo __('View More'); ?></a>
                        <?php
                        }
                        else
                        {
                            ?>
                            <h4>No Messages Found</h4>
                        <?php
                        }
                        ?>
                    </div>
                </div><!-- panel-body -->
            </div>
        </div>
    </div>


</div><!-- contentpanel -->
