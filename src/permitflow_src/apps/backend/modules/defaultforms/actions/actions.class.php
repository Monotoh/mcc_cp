<?php

/**
 * defaultforms actions.
 *
 * @package    permit
 * @subpackage defaultforms
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class defaultformsActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $this->forward404Unless($default_forms = Doctrine_Core::getTable('DefaultForms')->find(array(1)), sprintf('Object default_forms does not exist (%s).', $request->getParameter('id')));
    $this->form = new DefaultFormsForm($default_forms);
	
	$this->setLayout("layout");
  }


  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($default_forms = Doctrine_Core::getTable('DefaultForms')->find(array($request->getParameter('id'))), sprintf('Object default_forms does not exist (%s).', $request->getParameter('id')));
    $this->form = new DefaultFormsForm($default_forms);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }


  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $default_forms = $form->save();

      $audit = new Audit();
      $audit->saveAudit("", "<a href=\"/backend.php/defaultforms/index?id=".$default_forms->getId()."&language=en\">updated default forms settings</a>");

      $this->redirect('/backend.php/defaultforms/index?id='.$default_forms->getId());
    }
  }
}
