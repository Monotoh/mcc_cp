<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<form action="<?php echo url_for('/backend.php/defaultforms/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <table width="100%">
    <tfoot>
      <tr>
        <td colspan="2">
          <?php echo $form->renderHiddenFields(false) ?>
          <input type="submit" value="Save" class="btn"/>
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th>Occupancy Permit Application Form</th>
        <td>
          <?php echo $form['occupancy_form_id']->renderError() ?>
          <select id="default_forms_occupancy_form_id" name="default_forms[occupancy_form_id]">
          <?php
		  $q = Doctrine_Query::create()
		     ->from('ApForms a');
		  $forms = $q->execute();
		  foreach($forms as $formi)
		  {
		    $selected = "";
			if($form->getObject()->getOccupancyFormId() == $formi->getFormId())
			{
				$selected = "selected='selected'";
			}
		  	echo "<option value='".$formi->getFormId()."' ".$selected.">".$formi->getFormName()."</option>";
		  }
		  ?>
          </select>
        </td>
      </tr>
      <tr>
        <th>Construction Permit Application Form</th>
        <td>
          <?php echo $form['application_form_id']->renderError() ?>
          <select id="default_forms_application_form_id" name="default_forms[application_form_id]">
          <?php
		  $q = Doctrine_Query::create()
		     ->from('ApForms a');
		  $forms = $q->execute();
		  foreach($forms as $formi)
		  {
		    $selected = "";
			if($form->getObject()->getApplicationFormId() == $formi->getFormId())
			{
				$selected = "selected='selected'";
			}
		  	echo "<option value='".$formi->getFormId()."' ".$selected.">".$formi->getFormName()."</option>";
		  }
		  ?>
          </select>
        </td>
      </tr>
      <tr>
        <th>Contact Us Form</th>
        <td>
          <?php echo $form['contact_form_id']->renderError() ?>
          <select id="default_forms_contact_form_id" name="default_forms[contact_form_id]">
          <?php
		  $q = Doctrine_Query::create()
		     ->from('ApForms a');
		  $forms = $q->execute();
		  foreach($forms as $formi)
		  {
		    $selected = "";
			if($form->getObject()->getContactFormId() == $formi->getFormId())
			{
				$selected = "selected='selected'";
			}
		  	echo "<option value='".$formi->getFormId()."' ".$selected.">".$formi->getFormName()."</option>";
		  }
		  ?>
          </select>
        </td>
      </tr>
      <tr>
        <th>FAQ Feedback Form</th>
        <td>
          <?php echo $form['feedback_form_id']->renderError() ?>
          <select id="default_forms_feedback_form_id" name="default_forms[feedback_form_id]">
          <?php
		  $q = Doctrine_Query::create()
		     ->from('ApForms a');
		  $forms = $q->execute();
		  foreach($forms as $formi)
		  {
		    $selected = "";
			if($form->getObject()->getFeedbackFormId() == $formi->getFormId())
			{
				$selected = "selected='selected'";
			}
		  	echo "<option value='".$formi->getFormId()."' ".$selected.">".$formi->getFormName()."</option>";
		  }
		  ?>
          </select>
        </td>
      </tr>
      <tr>
        <th>Architects Additional Info Form</th>
        <td>
          <?php echo $form['profile_form_id']->renderError() ?>
          <select id="default_forms_profile_form_id" name="default_forms[profile_form_id]">
          <?php
		  $q = Doctrine_Query::create()
		     ->from('ApForms a');
		  $forms = $q->execute();
		  foreach($forms as $formi)
		  {
		    $selected = "";
			if($form->getObject()->getProfileFormId() == $formi->getFormId())
			{
				$selected = "selected='selected'";
			}
		  	echo "<option value='".$formi->getFormId()."' ".$selected.">".$formi->getFormName()."</option>";
		  }
		  ?>
          </select>
        </td>
      </tr>
      <tr>
        <th>Payment Form</th>
        <td>
          <?php echo $form['payment_form_id']->renderError() ?>
          <select id="default_forms_payment_form_id" name="default_forms[payment_form_id]">
          <?php
		  $q = Doctrine_Query::create()
		     ->from('ApForms a');
		  $forms = $q->execute();
		  foreach($forms as $formi)
		  {
		    $selected = "";
			if($form->getObject()->getPaymentFormId() == $formi->getFormId())
			{
				$selected = "selected='selected'";
			}
		  	echo "<option value='".$formi->getFormId()."' ".$selected.">".$formi->getFormName()."</option>";
		  }
		  ?>
          </select>
        </td>
      </tr>
      <tr>
        <th>Additional Documents Form</th>
        <td>
          <?php echo $form['additional_docs_form_id']->renderError() ?>
          <select id="default_forms_additional_docs_form_id" name="default_forms[additional_docs_form_id]">
          <?php
		  $q = Doctrine_Query::create()
		     ->from('ApForms a');
		  $forms = $q->execute();
		  foreach($forms as $formi)
		  {
		    $selected = "";
			if($form->getObject()->getAdditionalDocsFormId() == $formi->getFormId())
			{
				$selected = "selected='selected'";
			}
		  	echo "<option value='".$formi->getFormId()."' ".$selected.">".$formi->getFormName()."</option>";
		  }
		  ?>
          </select>
        </td>
      </tr>
    </tbody>
  </table>
</form>
