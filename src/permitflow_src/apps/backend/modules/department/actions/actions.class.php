<?php

/**
 * department actions.
 *
 * @package    permit
 * @subpackage department
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class departmentActions extends sfActions
{
    /**
     * Executes 'Checkname' action
     *
     * Ajax used to check existence of name
     *
     * @param sfRequest $request A request object
     */
    public function executeCheckname(sfWebRequest $request)
    {
        // add new user
        $q = Doctrine_Query::create()
           ->from("department a")
           ->where('a.department_name = ?', $request->getPostParameter('name'));
        $existinggroup = $q->execute();
        if(sizeof($existinggroup) > 0)
        {
              echo '<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Name is already in use!</strong></div>';
              exit;
        }
        else
        {
              echo '<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Name is available!</strong></div>';
              exit;
        }
    }

    /**
     * Executes 'Checkname' action
     *
     * Ajax used to check existence of name
     *
     * @param sfRequest $request A request object
     */
    public function executeChecknamemin(sfWebRequest $request)
    {
        // add new user
        $q = Doctrine_Query::create()
           ->from("department a")
           ->where('a.department_name = ?', $request->getPostParameter('name'));
        $existinggroup = $q->execute();
        if(sizeof($existinggroup) > 0)
        {
              echo 'fail';
              exit;
        }
        else
        {
              echo 'pass';
              exit;
        }
    }
  
  public function executeBatch(sfWebRequest $request)
  {
		if($request->getPostParameter('delete'))
		{
			$item = Doctrine_Core::getTable('department')->find(array($request->getPostParameter('delete')));
			if($item)
			{
			    $dep_name = $item->getDepartmentName();
				$item->delete();
				
        //Backwards compatibility. To be removed when cuteflow modules are removed.
				$q = Doctrine_Query::create()
				  ->from('CfFormslot a')
				  ->where('a.strname LIKE ?', '%'.$dep_name.'%');
				$slots = $q->execute();
				foreach($slots as $slot)
				{
					$slot->delete();
				}
				
			}
		}
   }
	
  public function executeIndex(sfWebRequest $request)
  {
    $this->departments = Doctrine_Core::getTable('department')
      ->createQuery('a')
      ->execute();
      $this->setLayout("layout-settings");
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->setLayout("layout-settings");
    $this->form = new departmentForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new departmentForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->setLayout("layout-settings");
    $this->forward404Unless($department = Doctrine_Core::getTable('department')->find(array($request->getParameter('id'))), sprintf('Object department does not exist (%s).', $request->getParameter('id')));
    $this->form = new departmentForm($department);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($department = Doctrine_Core::getTable('department')->find(array($request->getParameter('id'))), sprintf('Object department does not exist (%s).', $request->getParameter('id')));
    $this->form = new departmentForm($department);
	
	$old_name = $department->getDepartmentName();
	
	$new_name = $request->getPostParameter("department[department_name]");
	
	$q = Doctrine_Query::create()
	   ->from('cfUser a')
	   ->where('a.strdepartment = ?', $old_name);
	$users = $q->execute();
	foreach($users as $user)
	{
		$user->setStrDepartment($new_name);
		$user->save();
	}
	
	$q = Doctrine_Query::create()
	   ->from('cfFormslot a')
	   ->where('a.strname = ?', $old_name);
	$slots = $q->execute();
	foreach($slots as $slot)
	{
		$slot->setStrname($new_name);
		$slot->save();
	}

    $this->processForm($request, $this->form);

    $this->redirect('/backend.php/department/index');
  }

  public function executeDelete(sfWebRequest $request)
  {

    $this->forward404Unless($department = Doctrine_Core::getTable('department')->find(array($request->getParameter('id'))), sprintf('Object department does not exist (%s).', $request->getParameter('id')));
    $department->delete();

    $this->redirect('/backend.php/department/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
	  
       $department = $form->save();

       $department->setDepartmentHead($request->getPostParameter("department_head"));
       $department->save();

       $q = Doctrine_Query::create()
          ->from("CfUser a")
          ->where("a.strdepartment = ?", $department->getDepartmentName());
       $reviewers = $q->execute();
       foreach($reviewers as $reviewer)
       {
          $reviewer->setStrdepartment("");
          $reviewer->save();
       }

       $assigned_reviewers = $request->getPostParameter('department_reviewers');

       foreach($assigned_reviewers as $assigned_reviewer)
       {
          $q = Doctrine_Query::create()
             ->from("CfUser a")
             ->where("a.nid = ?", $assigned_reviewer);
          $reviewer = $q->fetchOne();
          if($reviewer)
          {
            $reviewer->setStrdepartment($department->getDepartmentName());
            $reviewer->save();
          }
       }
	  

       $this->redirect('/backend.php/department/index');
    }
  }
}
