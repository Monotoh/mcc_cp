<?php
use_helper("I18N");
?>

<div class="panel-heading">
<h3 class="panel-title"><?php echo ($form->getObject()->isNew()?__('New Department'):__('Edit Department')); ?></h3>
</div>
<div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
  <strong><?php echo __('Well done'); ?>!</strong> <?php echo __('You successfully updated this department'); ?></a>.
</div>

<div class="panel-body panel-body-nopadding">
<form id="departmentform" class="form-bordered form-horizontal"  action="<?php echo url_for('/backend.php/department/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>  autocomplete="off" data-ajax="false">


		   <?php if (!$form->getObject()->isNew()): ?>
<input class="form-control" type="hidden" name="sf_method" value="put" />
<?php endif; ?>

            <?php if(isset($form['_csrf_token'])): ?>
            <?php echo $form['_csrf_token']->render(); ?>
            <?php endif; ?>
			
      <?php echo $form->renderGlobalErrors() ?>
      <div class="form-group">
        <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Name'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['department_name']->renderError() ?>
          <?php echo $form['department_name']->render(array('required' => 'required')); ?>
		</div>
	  </div>

	  <div id="nameresult" name="nameresult"></div>

      <script language="javascript">
        $('document').ready(function(){
          $('#department_department_name').keyup(function(){
            $.ajax({
                      type: "POST",
                      url: "/backend.php/department/checkname",
                      data: {
                          'name' : $('input:text[id=department_department_name]').val()
                      },
                      dataType: "text",
                      success: function(msg){
                            //Receiving the result of search here
                            $("#nameresult").html(msg);
                      }
                  });
              });
        });
      </script>

      <div class="form-group">
        <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Head of Department'); ?></i></label>
        <div class="col-sm-8">

          <select name="department_head" id="department_head" class="form-control">
          	<option value="0"><?php echo __('Choose head of department'); ?></option>
	          <?php 
	          	$reviewers = array();

	          	if(!$form->getObject()->isNew())
	          	{
	           		$q = Doctrine_Query::create()
	           		   ->from("CfUser a")
	           		   ->where("a.strdepartment = ?", $form->getObject()->getDepartmentName())
	           		   ->andWhere("a.bdeleted = 0")
	           		   ->orderBy("a.strfirstname ASC");
	           		$reviewers = $q->execute();
	           	}

	           foreach($reviewers as $reviewer)
	           {
	           	  $selected = "";
	           	  if($reviewer->getNid() == $form->getObject()->getDepartmentHead())
	           	  {
	           	  	$selected = "selected";
	           	  }
	           	  echo "<option value='".$reviewer->getNid()."' ".$selected.">".$reviewer->getStrfirstname()." ".$reviewer->getStrlastname()."</option>";
	           }
	          ?>
          </select>
		</div>
	  </div>

      <div class="form-group">
        <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Reviewers'); ?></i></label>
        <div class="col-sm-8">

          <select name="department_reviewers[]" id="department_reviewers" class="form-control" multiple="multiple">
          	<?php
          	$q = Doctrine_Query::create()
          	   ->from("CfUser a")
          	   ->where("a.bdeleted = 0")
          	   ->orderBy("a.strfirstname ASC");
          	$reviewers = $q->execute();
          	foreach($reviewers as $reviewer)
          	{
          		$selected = "";

          		if(!$form->getObject()->isNew())
          		{
          			if($reviewer->getStrdepartment() == $form->getObject()->getDepartmentName())
          			{
          				$selected = "selected='selected'";
          			}
          		}

          		echo "<option value='".$reviewer->getNid()."' ".$selected.">".$reviewer->getStrfirstname()." ".$reviewer->getStrlastname()." (".$reviewer->getStruserid().")</option>";
          	}
          	?>
          </select>
		</div>
	  </div>

</div><!--panel-body-->
	  <div class="panel-footer">
			<button class="btn btn-danger mr10"><?php echo __('Reset'); ?></button><button type="submit" class="btn btn-primary" name="submitbuttonname" id="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button>
	  </div>

</form>
<script type="text/javascript" src="<?php echo public_path(); ?>assets_unified/js/jquery.bootstrap-duallistbox.js"></script>

<script>
jQuery(document).ready(function(){
  
  // CKEditor
  var list1 = jQuery('select[name="department_reviewers[]"]').bootstrapDualListbox();

});
</script>
