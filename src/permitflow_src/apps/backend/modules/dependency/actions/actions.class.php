<?php

class dependencyActions extends sfActions
{
  public function executeBatch(sfWebRequest $request)
  {
		if($request->getPostParameter('delete'))
		{
			$item = Doctrine_Core::getTable('ApDependencyLogic')->find(array($request->getPostParameter('delete')));
			if($item)
			{
				$item->delete();
			}
		}
    }

  public function executeIndex(sfWebRequest $request)
  {
     $q = Doctrine_Query::create()
       ->from('ApDependencyLogic a')
	     ->orderBy('a.id DESC');
     $this->dependency_logic = $q->execute();
	   $this->filter = $request->getParameter('filter');
	 
	   $this->setLayout("layout-settings");
  }



  public function executeNew(sfWebRequest $request)
  {
    $this->form = new ApDependencyLogicForm();
    if($request->getParameter('filter'))
    {
    	$this->filter = $request->getParameter('filter');
    }
    else
    {
    	$this->filter = 0;
    }
    $this->filter = $request->getParameter('filter');
	$this->setLayout("layout-settings");
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new ApDependencyLogicForm();


    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($logic = Doctrine_Core::getTable('ApDependencyLogic')->find(array($request->getParameter('id'))), sprintf('Object logic does not exist (%s).', $request->getParameter('id')));
    $this->form = new ApDependencyLogicForm($logic);
     $this->filter = $request->getParameter('filter');
	  $this->setLayout("layout-settings");
	  $this->setSfFormChoiceWidgetOptionsFromApFormElements($logic->getFormId(), 'element_id');//OTB Patch - Dynamic Element selection
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($logic = Doctrine_Core::getTable('ApDependencyLogic')->find(array($request->getParameter('id'))), sprintf('Object logic does not exist (%s).', $request->getParameter('id')));
    $this->form = new ApDependencyLogicForm($logic);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {

    $this->forward404Unless($logic = Doctrine_Core::getTable('ApDependencyLogic')->find(array($request->getParameter('id'))), sprintf('Object logic does not exist (%s).', $request->getParameter('id')));

    $audit = new Audit();
    $audit->saveAudit("", "deleted logic of id ".$logic->getId());

    $logic->delete();

    $this->redirect('/backend.php/dependenct/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $dependency = $form->save();

      $audit = new Audit();
      $audit->saveAudit("", "<a href=\"/backend.php/dependency/edit?id=".$dependency->getId()."&language=en\">deleted fixed fee</a>");

      $this->redirect('/backend.php/dependency/index');
    }
  }


  public function executeChangefields(sfWebRequest $request)
  {
	  $ap_form = Doctrine_Core::getTable('ap_forms')->find(array($request->getParameter('form_id')));
		  if($ap_form){
		  $elements = Doctrine_Core::getTable('ApFormElements')->getAllFields($ap_form->getFormId());
		  foreach($elements as $key => $value){
			$new_options .= "<option value='".$key."'>".$value."</option>";
		  }
		  echo $new_options;
	  }
	  exit();
  }
  protected function setSfFormChoiceWidgetOptionsFromApFormElements($apform_id, $widget_name)
  {
	$widget = $this->form->getWidget($widget_name);
	$widget->setOptions(array('choices' => Doctrine_Core::getTable('ApFormElements')->getAllFields($apform_id)));
	//error_log($apform_id." ## configure this form values ### ".print_R($widget->getChoices(), true));
  }

	//Conditions
	public function executeDependencycondition(sfWebRequest $request)
	{
		$this->filter = $request->getParameter("filter");
		$q_range=Doctrine_Query::create()
			->from('ApDependencyLogicConditions f')
			->where('f.logic_id = ?', $this->filter);
		$this->dependency_conditions=$q_range->execute();
	}
  public function executeDependencyconditionform(sfWebRequest $request)
  {
	$this->filter = $request->getParameter("filter");
	if($request->getParameter('id')){
		$condition=Doctrine_Core::getTable('ApDependencyLogicConditions')->find(array($request->getParameter('id')));
		$this->form=new ApDependencyLogicConditionsForm($condition);
	}else{
		$this->form=new ApDependencyLogicConditionsForm();
	}
	#$logic = Doctrine_Core::getTable('ApDependencyLogic')->find(array($this->filter));
	#$this->setSfFormChoiceWidgetOptionsFromApFormElements($logic->getFormId(), 'element_id');
  }
  public function executeNewdependencycondition(sfWebRequest $request)
  {
	$this->forward404Unless($request->isMethod(sfRequest::POST));
	$this->form=new ApDependencyLogicConditionForm();
    $this->form->bind($request->getParameter($this->form->getName()), $request->getFiles($this->form->getName()));
    if ($this->form->isValid())
    {
		$this->form->save();
	}
	$this->getUser()->setFlash('save_notice', sprintf('Condition has been saved'));
	$this->redirect('/backend.php/dependency/dependencycondition');
	$this->setTemplate('dependencycondition');
  }
  public function executeUpdatedependencycondition(sfWebRequest $request)
  {
	$this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
	$this->forward404Unless($dependencycondition=Doctrine_Core::getTable('ApDependencyLogicConditions')->find($request->getParameter('id')));
	$form= new ApDependencyLogicConditionForm($dependencycondition);
	$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
		$form->save();
	}
	$this->getUser()->setFlash('save_notice', sprintf('Condition has been saved'));
	$this->redirect('/backend.php/dependency/dependencycondition');
	$this->setTemplate('dependencycondition');
  }
  public function executeDeletedependencycondition(sfWebRequest $request)
  {
    $this->forward404Unless($dependency_condition = Doctrine_Core::getTable('ApDependencyLogicConditions')->find(array($request->getParameter('id'))), sprintf('Object condition does not exist (%s).', $request->getParameter('id')));

    $audit = new Audit();
    $audit->saveAudit("", "deleted condition of id ".$dependency_condition->getId());
	
	$logic_id = $dependency_condition->getLogicId();

    $dependency_condition->delete();

      #$this->redirect('/backend.php/fees/dependency/filter/'.$logic_id);
  }
	//Start Permitted Values
	public function executeDependencypermitted(sfWebRequest $request)
	{
		$this->filter = $request->getParameter("filter");
		$q_range=Doctrine_Query::create()
			->from('ApDependencyLogicRulePermittedValues f')
			->where('f.logic_id = ?', $this->filter);
		$this->dependency_permitted_values=$q_range->execute();
	}
  public function executeDependencypermittedform(sfWebRequest $request)
  {
	$this->filter = $request->getParameter("filter");
	if($request->getParameter('id')){
		$permitted=Doctrine_Core::getTable('ApDependencyLogicRulePermittedValues')->find(array($request->getParameter('id')));
		$this->form=new ApDependencyLogicRulePermittedValuesForm($permitted);
	}else{
		$this->form=new ApDependencyLogicRulePermittedValuesForm();
	}
	$logic = Doctrine_Core::getTable('ApDependencyLogic')->find(array($this->filter));
	$this->setSfFormChoiceWidgetOptionsFromApFormElements($logic->getFormId(), 'element_id');
  }
  public function executeNewdependencypermittedvalues(sfWebRequest $request)
  {
	$this->forward404Unless($request->isMethod(sfRequest::POST));
	$this->form=new ApDependencyLogicRulePermittedValuesForm();
    $this->form->bind($request->getParameter($this->form->getName()), $request->getFiles($this->form->getName()));
    if ($this->form->isValid())
    {
		$this->form->save();
	}
	$this->getUser()->setFlash('save_notice', sprintf('Permitted Value has been saved'));
	$this->redirect('/backend.php/dependency/dependencypermitted');
	$this->setTemplate('dependencypermitted');
  }
  public function executeUpdatedependencypermittedvalues(sfWebRequest $request)
  {
	$this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
	$this->forward404Unless($dependencypermitted=Doctrine_Core::getTable('ApDependencyLogicRulePermittedValues')->find($request->getParameter('id')));
	$form= new ApDependencyLogicRulePermittedValuesForm($dependencypermitted);
	$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
		$form->save();
	}
	$this->getUser()->setFlash('save_notice', sprintf('Permitted Value has been saved'));
	$this->redirect('/backend.php/dependency/dependencypermitted');
	$this->setTemplate('dependencypermitted');
  }
  public function executeDeletedependencypermitted(sfWebRequest $request)
  {
    $this->forward404Unless($dependencypermitted = Doctrine_Core::getTable('ApDependencyLogicRulePermittedValues')->find(array($request->getParameter('id'))), sprintf('Object condition does not exist (%s).', $request->getParameter('id')));

    $audit = new Audit();
    $audit->saveAudit("", "deleted permiited of id ".$dependencypermitted->getId());
	
	$logic_id = $dependencypermitted->getLogicId();

    $dependencypermitted->delete();

      #$this->redirect('/backend.php/fees/dependency/filter/'.$logic_id);
  }
	//End Permitted Values
}
