<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php
use_helper("I18N");
if($sf_user->mfHasCredential("access_settings")):
?>
<div class="contentpanel">
<form id="feeform" class="form-bordered" action="<?php echo url_for('/backend.php/dependency/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId()."&filter=".$filter : '?filter='.$filter )) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>  autocomplete="off" data-ajax="false">

<div class="panel panel-dark">
<div class="panel-heading">
<h3 class="panel-title"><?php echo __('Dependency Logic'); ?></h3>
</div>

<div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
  <strong><?php echo __('Well done'); ?>!</strong> <?php echo __('You successfully updated this record'); ?></a>.
</div>


<div class="panel-body panel-body-nopadding">

   <?php echo $form['_csrf_token']->render(); ?> 
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
      <?php echo $form->renderGlobalErrors() ?>
      <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Form'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['form_id']->renderError() ?>
          <?php echo $form['form_id'] ?>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Element'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['element_id']->renderError() ?>
          <?php echo $form['element_id'] ?>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Conditions Logic'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['rule_all_any']->renderError() ?>
          <?php echo $form['rule_all_any'] ?>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Call Remote Web Service?'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['integration_logic']->renderError() ?>
          <?php echo $form['integration_logic'] ?>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Remote Web Service URL?'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['integration_url']->renderError() ?>
          <?php echo $form['integration_url'] ?>
        </div>
      </div>
      <div class="form-group">
		<div class="col-sm-12 alignright">
		  <button type="button" class="btn btn-primary" data-target="#fieldsModal" data-toggle="modal">View available user/form fields</button>
		</div>
	  </div>
	  <!--Start Load conditions area -->
	<div class="form-group">
	        <div class="col-sm-12" id="loaddependencyconditions" name="loaddependencyconditions">

	        </div>
	        <?php
	          $dependency_id = 0;
			  if(!$form->getObject()->isNew())
			  {
			  		$dependency_id = $form->getObject()->getId();
			  }
	        ?>
			<script language="javascript">
			 jQuery(document).ready(function(){
		        $("#loaddependencyconditions").load("<?php echo public_path(); ?>backend.php/dependency/dependencycondition/filter/<?php echo $dependency_id; ?>");
				 $('#ap_dependency_logic_form_id').change(function(){
					var value = this.value ;
					 $.ajax({
						url: '<?php echo url_for('/backend.php/dependency/changefields/form_id/'); ?>'+value,
						cache: false,
						type: 'POST',
						data : $('#bform').serialize(),
						success: function(json) {
							$('#ap_dependency_logic_element_id').empty().append(json);
						}
					});
				})
		     });
			 </script>
	      </div>
	  <!--End Load conditions area -->
	  <!--Start Permitted Values area -->
	<div class="form-group">
	        <div class="col-sm-12" id="loaddependencypermitted" name="loaddependencypermitted">

	        </div>
	        <?php
	          $dependency_id = 0;
			  if(!$form->getObject()->isNew())
			  {
			  		$dependency_id = $form->getObject()->getId();
			  }
	        ?>
			<script language="javascript">
			 jQuery(document).ready(function(){
		        $("#loaddependencypermitted").load("<?php echo public_path(); ?>backend.php/dependency/dependencypermitted/filter/<?php echo $dependency_id; ?>");
		     });
			 </script>
	      </div>
	  <!--End Permitted Values area -->
	   <div class="panel-footer">
			<button class="btn btn-danger mr10"><?php echo __('Reset'); ?></button><button type="submit" class="btn btn-primary" name="submitbuttonname" id="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button>
	  </div>
</form>
</div>

		  <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="fieldsModal" class="modal fade" style="display: none;">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
			        <h4 id="myModalLabel" class="modal-title">View available user/form fields</h4>
			      </div>
			      <div class="modal-body">
			        <div class="form-group">
		<?php
			//Get User Information (anything starting with sf_ )
					   //sf_email, sf_fullname, sf_username, ... other fields in the dynamic user profile form e.g sf_element_1
			?>
			<table class="table dt-on-steroids mb0">
			<thead><tr><th width="50%"><?php echo __('User Details'); ?></th><th><?php echo __('Tag'); ?></th></tr></thead>
			<tbody>
			<tr><td><?php echo __('User ID'); ?></td><td>{sf_username}</td></tr>
            <tr><td><?php echo __('Mobile Number'); ?></td><td>{sf_mobile}</td></tr>
			<tr><td><?php echo __('Email'); ?></td><td>{sf_email}</td></tr>
			<tr><td><?php echo __('Full Name'); ?></td><td>{sf_fullname}</td></tr>
			<tr><td><?php echo __('User Category'); ?></td><td>{sf_user_category}</td></tr>
			<?php
					$q = Doctrine_Query::create()
					   ->from('apFormElements a')
					   ->where('a.form_id = ?', 15)
             ->andWhere('a.element_status = ?', 1)
             ->orderBy('a.element_position ASC');

					$elements = $q->execute();

					foreach($elements as $element)
					{
						$childs = $element->getElementTotalChild();
						if($childs == 0)
						{
						   echo "<tr><td>".$element->getElementTitle()."</td><td>{sf_element_".$element->getElementId()."}</td></tr>";
						}
						else
						{
							if($element->getElementType() == "select")
							{
								echo "<tr><td>".$element->getElementTitle()."</td><td>{sf_element_".$element->getElementId()."}</td></tr>";
							}
							else
							{
								for($x = 0; $x < ($childs + 1); $x++)
								{
									echo "<tr><td>".$element->getElementTitle()."</td><td>{sf_element_".$element->getElementId()."_".($x+1)."}</td></tr>";
								}
							}
						}
					}
			?>
			</tbody>
			</table>


			<table class="table dt-on-steroids mb0">
			<thead><tr><th width="50%"><?php echo __('Application Details'); ?></th><th><?php echo __('Tag'); ?></th></tr></thead>
			<tbody>
			     <tr><td><?php echo __('User Profile Picture'); ?></td><td>{profile_pic}</td></tr>
            <tr><td><?php echo __('Bar Code'); ?></td><td>{bar_code}</td></tr>
            <tr><td><?php echo __('Bar Code (Small)'); ?></td><td>{bar_code_small}</td></tr>
			<tr><td><?php echo __('Application Number'); ?></td><td>{ap_application_id}</td></tr>
			<tr><td><?php echo __('Created At'); ?></td><td>{fm_created_at}</td></tr>
			<tr><td><?php echo __('Approved At'); ?></td><td>{fm_updated_at}</td></tr>
      <tr><td><?php echo __('Permit ID'); ?></td><td>{ap_permit_id}</td></tr>
			<tr><td><?php echo __('Permit Issued At'); ?></td><td>{ap_issue_date}</td></tr>
      		<tr><td><?php echo __('Permit Expires At'); ?></td><td>{ap_expire_date}</td></tr>
          <tr><td><?php echo __('Permit UUID'); ?></td><td>{uuid}</td></tr>
			<?php
			//Get Application Information (anything starting with ap_ )
					   //ap_application_id

			//Get Form Details (anything starting with fm_ )
					   //fm_created_at, fm_updated_at.....fm_element_1
			?>
		 <?php
		  if(!$form->getObject()->isNew())
		  {
		    $appform = $form->getObject()->getFormId();
		  ?>
			<?php

					$q = Doctrine_Query::create()
					   ->from('apFormElements a')
					   ->where('a.form_id = ?', $appform)
             ->andWhere('a.element_status = ?', 1)
             ->orderBy('a.element_position ASC');

					$elements = $q->execute();

					foreach($elements as $element)
					{
						$childs = $element->getElementTotalChild();
						if($childs == 0)
						{
               if($element->getElementType() == "select")
               {
                 if($element->getElementExistingForm() && $element->getElementExistingStage())
                 {
                   $q = Doctrine_Query::create()
                    ->from("ApForms a")
                    ->where("a.form_id = ?", $element->getElementExistingForm());
                   $child_form = $q->fetchOne();

                   echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."} ";
                     echo '<table class="table dt-on-steroids mb0">
                     <thead><tr><th width="50%">'.__($child_form->getFormName().' Details').'</th><th>'.__('Tag').'</th></tr></thead>
                     <tbody>';

                      ?>
                      <tr><td><?php echo __('Application Number'); ?></td><td>{ap_child_application_id}</td></tr>
                      <tr><td><?php echo __('Created At'); ?></td><td>{fm_child_created_at}</td></tr>
                      <tr><td><?php echo __('Approved At'); ?></td><td>{fm_child_updated_at}</td></tr>
                      <?php
                        $q = Doctrine_Query::create()
                           ->from("Permits a")
                           ->where("a.applicationform = ?", $element->getElementExistingForm());
                        $permits = $q->execute();

                        foreach($permits as $permit)
                        {
                            echo "<tr><td>".$permit->getTitle()." ID</td><td>{ap_permit_id_".$permit->getId()."_element_child}</td></tr>";
                        }

                      $q = Doctrine_Query::create()
                         ->from('apFormElements a')
                         ->where('a.form_id = ?', $element->getElementExistingForm())
                         ->andWhere('a.element_status = ?', 1)
                         ->orderBy('a.element_position ASC');

                      $child_elements = $q->execute();

                      foreach($child_elements as $child_element)
                      {

                          //START CHILD ELEMENTS
                          $childs = $child_element->getElementTotalChild();
                          if($childs == 0)
                          {
                             if($child_element->getElementType() == "select")
                             {
                               if($child_element->getElementExistingForm() && $child_element->getElementExistingStage())
                               {

                                 $q = Doctrine_Query::create()
                                  ->from("ApForms a")
                                  ->where("a.form_id = ?", $child_element->getElementExistingForm());
                                 $grand_form = $q->fetchOne();

                                   echo "<tr><td>".$child_element->getElementTitle()."</td><td>{fm_child_element_".$child_element->getElementId()."} ";
                                   echo '<table class="table dt-on-steroids mb0">
                                   <thead><tr><th width="50%">'.__($grand_form->getFormName().' Details').'</th><th>'.__('Tag').'</th></tr></thead>
                                   <tbody>';

                                    $q = Doctrine_Query::create()
                                       ->from('apFormElements a')
                                       ->where('a.form_id = ?', $child_element->getElementExistingForm())
                                       ->andWhere('a.element_status = ?', 1)
                                       ->orderBy('a.element_position ASC');

                                    $grand_child_elements = $q->execute();

                                    foreach($grand_child_elements as $grand_child_element)
                                    {
                                      ?>
                                      <tr><td><?php echo __('Application Number'); ?></td><td>{ap_grand_child_application_id}</td></tr>
                                      <tr><td><?php echo __('Created At'); ?></td><td>{fm_grand_child_created_at}</td></tr>
                                      <tr><td><?php echo __('Approved At'); ?></td><td>{fm_grand_child_updated_at}</td></tr>
                                      <?php
                                       $q = Doctrine_Query::create()
                                          ->from("Permits a")
                                          ->where("a.applicationform = ?", $child_element->getElementExistingForm());
                                       $permits = $q->execute();

                                       foreach($permits as $permit)
                                       {
                                           echo "<tr><td>".$permit->getTitle()." ID</td><td>{ap_permit_id_".$permit->getId()."_element_grand_child}</td></tr>";
                                       }

                                        //START GRAND CHILD ELEMENTS
                                        $childs = $grand_child_element->getElementTotalChild();
                                        if($childs == 0)
                                        {
                                           if($grand_child_element->getElementType() == "select")
                                           {
                                               echo "<tr><td>".$grand_child_element->getElementTitle()."</td><td>{fm_grand_child_element_".$grand_child_element->getElementId()."}</td></tr>";
                                           }
                                           else
                                           {
                                              echo "<tr><td>".$grand_child_element->getElementTitle()."</td><td>{fm_grand_child_element_".$grand_child_element->getElementId()."}</td></tr>";
                                           }
                                        }
                                        else
                                        {
                                            for($x = 0; $x < ($childs + 1); $x++)
                                            {
                                              echo "<tr><td>".$grand_child_element->getElementTitle()."</td><td>{fm_grand_child_element_".$grand_child_element->getElementId()."_".($x+1)."}</td></tr>";
                                            }
                                        }
                                        //END GRAND CHILD ELEMENTS
                                    }

                                   echo '</tbody></table>';
                                 echo "</td></tr>";
                               }
                               else
                               {
                                 echo "<tr><td>".$child_element->getElementTitle()."</td><td>{fm_child_element_".$child_element->getElementId()."}</td></tr>";
                               }
                             }
                             else
                             {
                                echo "<tr><td>".$child_element->getElementTitle()."</td><td>{fm_child_element_".$child_element->getElementId()."}</td></tr>";
                             }
                          }
                          else
                          {
                              for($x = 0; $x < ($childs + 1); $x++)
                              {
                                echo "<tr><td>".$child_element->getElementTitle()."</td><td>{fm_child_element_".$child_element->getElementId()."_".($x+1)."}</td></tr>";
                              }
                          }
                          //END CHILD ELEMENTS
                      }

                     echo '</tbody></table>';
                   echo "</td></tr>";
                 }
                 else
                 {
                   echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."}</td></tr>";
                 }
               }
               else
               {
                  echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."}</td></tr>";
               }
            }
						else
						{
								for($x = 0; $x < ($childs + 1); $x++)
								{
									echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."_".($x+1)."}</td></tr>";
								}
						}
					}

          ?>
		  <?php
		  }
		  ?>
			</tbody>
			</table>
            <table class="table dt-on-steroids mb0">
                <thead><tr><th width="50%"><?php echo __('Invoice Details'); ?></th><th><?php echo __('Tag'); ?></th></tr></thead>
                <tbody>
                <tr><td><?php echo __('Invoice No'); ?></td><td>{inv_no}</td></tr>
                <tr><td><?php echo __('Invoice Date'); ?></td><td>{inv_date_created}</td></tr>
                <tr><td><?php echo __('Invoice Expiry Date'); ?></td><td>{inv_expires_at}</td></tr>
                <tr><td><?php echo __('List of Fees'); ?></td><td>{inv_fee_table}</td></tr>
                <tr><td><?php echo __('Total'); ?></td><td>{inv_total}</td></tr>
                <tr><td><?php echo __('Payment Reference Number'); ?></td><td>{inv_payment_id}</td></tr>
                <tr><td><?php echo __('Payment Transaction Id'); ?></td><td>{inv_transaction_id}</td></tr>
                </tbody>
            </table>
			<table class="table dt-on-steroids mb0">
			<thead><tr><th width="50%"><?php echo __('Conditions Of Approval'); ?></th><th><?php echo __('Tag'); ?></th></tr></thead>
			<tbody>
			<tr><td><?php echo __('Conditions Of Approval'); ?></td><td>{ca_conditions}</td></tr>
			</tbody>
			</table>
		</div>
	      <div class="modal-footer">
	        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
	      </div>
	    </div><!-- modal-content -->
	  </div><!-- modal-dialog -->
	</div>
	</div>
<?php else:
	include_partial("accessdenied");
endif; ?>