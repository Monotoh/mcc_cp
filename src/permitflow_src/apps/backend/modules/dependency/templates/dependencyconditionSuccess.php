<?php use_helper("I18N"); 
if($sf_user->mfHasCredential("access_settings")):
?>
<div class="panel panel-dark">
<div class="panel-heading">
	<h3 class="panel-title"><?php echo __('Logic Conditions'); ?></h3>
	<div class="pull-right">
	<a class="btn btn-primary-alt settings-margin42" id="newcond" href="#newcond"><?php echo __('New Condition'); ?></a>
            <script language="javascript">
            jQuery(document).ready(function(){
              $( "#newcond" ).click(function() {
                  $("#loaddependencyconditions").load("<?php echo public_path(); ?>backend.php/dependency/dependencyconditionform/filter/<?php echo $filter; ?>");
              });
            });
            </script>
	</div>
</div>

<?php use_helper("I18N"); ?>
<?php if($sf_user->hasFlash('save_notice')): ?>
	<div><?php echo $sf_user->getFlash('save_notice') ?></div>
<?php endif; ?>
  <div class="pageheader">
       <h2><i class="fa fa-edit"></i><?php echo __('Conditons'); ?></h2>
      <div class="breadcrumb-wrapper">
        
        <ol class="breadcrumb">
          <li><a href=""><?php echo __('Conditons'); ?></a></li>
          <li class="active"><?php echo __('This page list all the conditons that will be applied to applications'); ?></li>
        </ol>
      </div>
    </div>
		<div class="panel-body panel-body-nopadding">
			<div class="table-responsive">
				<table class="table dt-on-steroids mb0" id="table_dependencycondition">
					<thead>
						<th><?php echo __('Element Name') ?></th>
						<th><?php echo __('Rule Condition') ?></th>
						<th><?php echo __('Rule Keyword') ?></th>
						<th><?php echo __('Rule Keyword Option Id') ?></th>
						<th><?php echo __('Action') ?></th>
					</thead>
					<tbody>
						<?php foreach($dependency_conditions as $dependency_condition): ?>
							<tr class="unread">
								<td><?php echo $dependency_condition->getElementName(); ?></td>
								<td><?php echo $dependency_condition->getRuleCondition(); ?></td>
								<td><?php echo $dependency_condition->getRuleKeyword(); ?></td>
								<td><?php echo $dependency_condition->getRuleKeywordOptionId(); ?></td>
								<td>                
								<a id="editcondition<?php echo $dependency_condition->getId() ?>" href="#editcondition" title="<?php echo __('Edit'); ?>"><span class="badge badge-primary"><i class="fa fa-pencil"></i></span></a>
								<a id="deletecondition<?php echo $dependency_condition->getId() ?>" href="#deletecondition" title="<?php echo __('Delete'); ?>"><span class="badge badge-primary"><i class="fa fa-trash-o"></i></span></a>
								<script language="javascript">
								jQuery(document).ready(function(){
								  $( "#editcondition<?php echo $dependency_condition->getId() ?>" ).click(function() {
									  $("#loaddependencyconditions").load("<?php echo public_path(); ?>backend.php/dependency/dependencyconditionform/id/<?php echo $dependency_condition->getId(); ?>/filter/<?php echo $filter; ?>");
								  });
								  $( "#deletecondition<?php echo $dependency_condition->getId() ?>" ).click(function() {
									  if(confirm('Are you sure you want to delete this range?')){
										$("#loaddependencyconditions").load("<?php echo public_path(); ?>backend.php/dependency/deletedependencycondition/id/<?php echo $dependency_condition->getId(); ?>/filter/<?php echo $filter; ?>");
									  }
									  else
									  {
										return false;
									  }
								  });
								});
								</script>
								</td>

							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
</div>
	<script>
		jQuery('#table_dependencycondition').dataTable();
	</script>
<?php else:
	include_partial("accessdenied");
endif; ?>