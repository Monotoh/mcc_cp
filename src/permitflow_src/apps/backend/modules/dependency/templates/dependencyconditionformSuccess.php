<?php
use_helper("I18N");
?>
<div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
  <strong><?php echo __('Well done'); ?>!</strong> <?php echo __('You successfully updated this record'); ?></a>.
</div>
<div class="panel-heading">
<h3 class="panel-title"><?php echo ($form->getObject()->isNew()?__('New Condition'):__('Edit Condition')); ?></h3>
</div>

<?php if($sf_user->hasFlash('save_notice')): ?>
	<div><?php echo $sf_user->getFlash('save_notice') ?></div>
<?php endif; ?>
<form id="bform" class="form-bordered form-horizontal" action="<?php echo url_for('/backend.php/dependency/'.($form->getObject()->isNew() ? 'newdependencycondition' : 'updatedependencycondition').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>   autocomplete="off" data-ajax="false">
    <div class="panel-body panel-body-nopadding">

          <?php echo $form->renderGlobalErrors() ?>
          <?php if(isset($form['_csrf_token'])): ?>
          <?php echo $form['_csrf_token']->render(); ?>
          <?php endif; ?>


		<input type="hidden" name="ap_dependency_logic_conditions[logic_id]" value="<?php echo $filter; ?>" id="ap_dependency_logic_conditions_logic_id" /><!--Automatically save logic_id of opened logic-->

          <!--<div class="form-group">
            <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Logic Rule'); ?></i></label>
             <div class="col-sm-8">
              <?php echo $form['logic_id']->renderError() ?>
              <?php echo $form['logic_id'] ?>
            </div>
          </div>-->


          <div class="form-group">
            <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Element Name'); ?></i></label>
             <div class="col-sm-8">
              <?php echo $form['element_name']->renderError() ?>
              <?php echo $form['element_name'] ?>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Key Word'); ?></i></label>
             <div class="col-sm-8">
              <?php echo $form['rule_keyword']->renderError() ?>
              <?php echo $form['rule_keyword'] ?>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Rule Condition'); ?></i></label>
             <div class="col-sm-8">
              <?php echo $form['rule_condition']->renderError() ?>
              <?php echo $form['rule_condition'] ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Key Word Opiton Id (For select field types)'); ?></i></label>
             <div class="col-sm-8">
              <?php echo $form['rule_keyword_option_id']->renderError() ?>
              <?php echo $form['rule_keyword_option_id'] ?>
            </div>
          </div>
<div class="panel-footer" align="right">
            <a id="backbuttonname" name="backbuttonname" class="btn btn-success"><?php echo __('Back'); ?></a> <button type="submit" class="btn btn-primary" name="submitbuttonname" id="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button>
       </div>
</div>
<script language="javascript">
 jQuery(document).ready(function(){
	$("#submitbuttonname").click(function() {
		 $.ajax({
			url: '<?php echo url_for('/backend.php/dependency/'.($form->getObject()->isNew() ? 'newdependencycondition' : 'updatedependencycondition').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>',
			cache: false,
			type: 'POST',
			data : $('#bform').serialize(),
			success: function(json) {
				$('#alertdiv').attr("style", "display: block;");
		        $("#loaddependencyconditions").load("<?php echo public_path(); ?>backend.php/dependency/dependencycondition/filter/<?php echo $filter; ?>");
			}
		});
		return false;
	 });

	  $( "#backbuttonname" ).click(function() {
		        $("#loaddependencyconditions").load("<?php echo public_path(); ?>backend.php/dependency/dependencycondition/filter/<?php echo $filter; ?>");
	  });

	});
</script>

</form>
