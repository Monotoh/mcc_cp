<?php use_helper("I18N"); 
if($sf_user->mfHasCredential("access_settings")):
?>
<div class="panel panel-dark">
<div class="panel-heading">
	<h3 class="panel-title"><?php echo __('Logic Permitted Values'); ?></h3>
	<div class="pull-right">
	<a class="btn btn-primary-alt settings-margin42" id="newperm" href="#newperm"><?php echo __('New Value'); ?></a>
            <script language="javascript">
            jQuery(document).ready(function(){
              $( "#newperm" ).click(function() {
                  $("#loaddependencypermitted").load("<?php echo public_path(); ?>backend.php/dependency/dependencypermittedform/filter/<?php echo $filter; ?>");
              });
            });
            </script>
	</div>
</div>

<?php use_helper("I18N"); ?>
<?php if($sf_user->hasFlash('save_notice')): ?>
	<div><?php echo $sf_user->getFlash('save_notice') ?></div>
<?php endif; ?>
  <div class="pageheader">
       <h2><i class="fa fa-edit"></i><?php echo __('Values'); ?></h2>
      <div class="breadcrumb-wrapper">
        
        <ol class="breadcrumb">
          <li><a href=""><?php echo __('Values'); ?></a></li>
          <li class="active"><?php echo __('This page list all the values that will be applied to applications'); ?></li>
        </ol>
      </div>
    </div>
		<div class="panel-body panel-body-nopadding">
			<div class="table-responsive">
				<table class="table dt-on-steroids mb0" id="table_dependencycondition">
					<thead>
						<th><?php echo __('Element Name') ?></th>
						<th><?php echo __('Permitted Value') ?></th>
						<th><?php echo __('Action') ?></th>
					</thead>
					<tbody>
						<?php foreach($dependency_permitted_values as $dependency_permitted): ?>
							<tr class="unread">
								<td><?php echo $dependency_permitted->getElementName(); ?></td>
								<td><?php echo $dependency_permitted->getPermittedValue(); ?></td>
								<td>                
								<a id="editpermitted<?php echo $dependency_permitted->getId() ?>" href="#editpermitted" title="<?php echo __('Edit'); ?>"><span class="badge badge-primary"><i class="fa fa-pencil"></i></span></a>
								<a id="deletepermitted<?php echo $dependency_permitted->getId() ?>" href="#deletepermitted" title="<?php echo __('Delete'); ?>"><span class="badge badge-primary"><i class="fa fa-trash-o"></i></span></a>
								<script language="javascript">
								jQuery(document).ready(function(){
								  $( "#editpermitted<?php echo $dependency_permitted->getId() ?>" ).click(function() {
									  $("#loaddependencypermitted").load("<?php echo public_path(); ?>backend.php/dependency/dependencypermittedform/id/<?php echo $dependency_permitted->getId(); ?>/filter/<?php echo $filter; ?>");
								  });
								  $( "#deletepermitted<?php echo $dependency_permitted->getId() ?>" ).click(function() {
									  if(confirm('Are you sure you want to delete this range?')){
										$("#loaddependencypermitted").load("<?php echo public_path(); ?>backend.php/dependency/deletedependencypermitted/id/<?php echo $dependency_permitted->getId(); ?>/filter/<?php echo $filter; ?>");
									  }
									  else
									  {
										return false;
									  }
								  });
								});
								</script>
								</td>

							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
</div>
	<script>
		jQuery('#table_dependencycondition').dataTable();
	</script>
<?php else:
	include_partial("accessdenied");
endif; ?>