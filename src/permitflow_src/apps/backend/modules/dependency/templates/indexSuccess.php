<?php
use_helper("I18N");
if($sf_user->mfHasCredential("managefees"))
{
?>
<?php use_helper('I18N', 'Date') ?>

<div class="panel panel-dark">
<div class="panel-heading">
			<h3 class="panel-title"><?php echo __('Logic'); ?></h3>
				<div class="pull-right">
            <a class="btn btn-primary-alt settings-margin42" id="newfee" href="#end"><?php echo __('New Logic'); ?></a>
</div>
</div>
		
 
<div class="panel panel-body panel-body-nopadding ">

<div class="table-responsive">
<table class="table dt-on-steroids mb0" id="table6">
    <thead>
   <tr>
      <th width="1%">#</th>
      <th><?php echo __('Form'); ?></th>
      <th><?php echo __('Element'); ?></th>
      <th><?php echo __('Integration Logic'); ?></th>
      <th class="no-sort" width="7%"><?php echo __('Actions'); ?></th>
    </tr>
  </thead>
    <tbody>
    <?php foreach ($dependency_logic as $logic): ?>
    <tr id="row_<?php echo $logic->getId() ?>">
		<td><a href="<?php echo url_for('/backend.php/dependency/edit?id='.$logic->getId()) ?>"><?php echo $logic->getId() ?></a></td>
		<td><a href="<?php echo url_for('/backend.php/dependency/edit?id='.$logic->getId()) ?>"><?php echo $logic->getFormId() ?></a></td>
		<td><a href="<?php echo url_for('/backend.php/dependency/edit?id='.$logic->getId()) ?>"><?php echo $logic->getElementId() ?></a></td>
		<td><a href="<?php echo url_for('/backend.php/dependency/edit?id='.$logic->getId()) ?>"><?php echo $logic->getIntegrationLogic() ?></a></td>
    <td>					
	  <a id="editlogic<?php echo $logic->getId() ?>" href="<?php echo public_path(); ?>backend.php/dependency/edit?id=<?php echo $logic->getId(); ?>&filter=0" alt="Edit" title="<?php echo __('Edit'); ?>"><span class="badge badge-primary"><i class="fa fa-pencil"></i></span></a>
     
      <a id="deletelogic<?php echo $logic->getId() ?>" onClick="if(confirm('Are you sure you want to delete this item?')){ return true; }else{ return false; }" href="<?php echo public_path(); ?>backend.php/dependency/delete?id=<?php echo $logic->getId(); ?>&filter=0" alt="Delete" title="<?php echo __('Delete'); ?>"><span class="badge badge-primary"><i class="fa fa-trash-o"></i></span></a>
   </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
</div>
<a name="end"></a>

</div><!--panel-body-->
</div><!--panel-dark-->

<script>
  jQuery('#table3').dataTable({
      "sPaginationType": "full_numbers",
     
      // Using aoColumnDefs
      "aoColumnDefs": [
      	{ "bSortable": false, "aTargets": [ 'no-sort' ] }
    	]
    });
    
</script>
<?php
}
else
{
  include_partial("settings/accessdenied"); //OTB patch
}
?>
