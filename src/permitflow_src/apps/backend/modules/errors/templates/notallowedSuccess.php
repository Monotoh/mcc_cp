<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="/assets_unified/images/favicon.png" type="image/png">

  <title>403 Forbidden </title>

  <link href="/assets_unified/css/style.default.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="/assets_unified/js/html5shiv.js"></script>
  <script src="/assets_unified/js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="notfound">

<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>
  
  <div class="notfoundpanel">
                 <section id="content">
		<h1>403</h1>
		<h2>Forbidden</h2>
		<hr>
                <p style="color:red;">You dont have the permission to access this action and/or page.</p>
		<p style="color:red;">Please contact your system administrator if you're experiencing problems.</p>
		
		</section>
    <a class="btn btn-primary" href="/backend.php/dashboard"> Back to Dashboard  </a>
  </div><!-- notfoundpanel -->
  
</section>


<script src="/assets_unified/js/jquery-1.10.2.min.js"></script>
<script src="/assets_unified/js/jquery-migrate-1.2.1.min.js"></script>
<script src="/assets_unified/js/bootstrap.min.js"></script>
<script src="/assets_unified/js/modernizr.min.js"></script>
<script src="/assets_unified/js/retina.min.js"></script>

<script src="/assets_unified/js/custom.js"></script>

</body>
</html>
