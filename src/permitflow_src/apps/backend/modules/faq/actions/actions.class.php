<?php

/**
 * faq actions.
 *
 * @package    permit
 * @subpackage faq
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class faqActions extends sfActions
{
  public function executeBatch(sfWebRequest $request)
  {
		if($request->getPostParameter('delete'))
		{
			$item = Doctrine_Core::getTable('faq')->find(array($request->getPostParameter('delete')));
			if($item)
			{
				$item->delete();
			}
		}
    }

  /**
   * Executes 'Checkname' action
   *
   * Ajax used to check existence of name
   *
   * @param sfRequest $request A request object
   */
  public function executeCheckname(sfWebRequest $request)
  {
      // add new user
      $q = Doctrine_Query::create()
         ->from("Faq a")
         ->where('a.question = ?', $request->getPostParameter('name'));
      $existinggroup = $q->execute();
      if(sizeof($existinggroup) > 0)
      {
            echo '<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Question already exists!</strong></div>';
            exit;
      }
      else
      {
            echo '<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Question is available!</strong></div>';
            exit;
      }
  }
  public function executeIndex(sfWebRequest $request)
  {
    $this->faqs = Doctrine_Core::getTable('faq')
      ->createQuery('a')
      ->execute();
	$this->setLayout("layout-settings");
  }

  public function executeShow(sfWebRequest $request)
  {
    $this->faq = Doctrine_Core::getTable('faq')->find(array($request->getParameter('id')));
    $this->forward404Unless($this->faq);
	$this->setLayout("layout");
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new faqForm();
	$this->setLayout("layout-settings");
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new faqForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($faq = Doctrine_Core::getTable('faq')->find(array($request->getParameter('id'))), sprintf('Object faq does not exist (%s).', $request->getParameter('id')));
    $this->form = new faqForm($faq);
	$this->setLayout("layout-settings");
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($faq = Doctrine_Core::getTable('faq')->find(array($request->getParameter('id'))), sprintf('Object faq does not exist (%s).', $request->getParameter('id')));
    $this->form = new faqForm($faq);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {

    $this->forward404Unless($faq = Doctrine_Core::getTable('faq')->find(array($request->getParameter('id'))), sprintf('Object faq does not exist (%s).', $request->getParameter('id')));
    $faq->delete();

    
    $this->redirect('/backend.php/faq/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
		
      	$faq = $form->save();
      
    $this->redirect('/backend.php/faq/index');
    }
  }
}
