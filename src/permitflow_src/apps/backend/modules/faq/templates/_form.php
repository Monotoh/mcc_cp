<?php
	use_helper("I18N");
	$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
	mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
	$locale = '';
?>




<form id="faqform" name="faqform" action="<?php echo url_for('/backend.php/faq/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>  autocomplete="off" data-ajax="false" >
<div class="panel panel-dark">
<div class="panel-heading">



           <h3 class="panel-title"><?php echo ($form->getObject()->isNew()?__('New Question'):__('Edit Question')); ?><?php
 if($_SESSION['locale']){ 
    $sql = "SELECT * FROM ext_locales WHERE locale_identifier = '".$_SESSION['locale']."'";
    $rows = mysql_query($sql);
    $locale = mysql_fetch_assoc($rows);
    echo " <p></h3> (".$locale['local_title']." Translation)</p>";
 } 
?>

</div>

<div class="panel-body panel-body-nopadding form-bordered">

<div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
  <strong><?php echo __('Well done'); ?>!</strong> <?php echo __('You successfully updated this web page'); ?></a>.
</div>


		   <?php if (!$form->getObject()->isNew()): ?>
			<input type="hidden" name="sf_method" value="put" />
			<?php endif; ?>

          <?php echo $form->renderHiddenFields(false) ?>
         
           <?php echo $form->renderGlobalErrors() ?>
		<div class="form-group">
        <label class="col-sm-4" for="text_field"><i class="bold-label"><?php echo __('Question'); ?></i></label>
        <div class="col-sm-12">
          <?php echo $form['question']->renderError() ?>
      
            <input type="text" name="faq[question]" id="faq_question" class="form-control"  value="<?php 
				$sql = "SELECT * FROM ext_translations WHERE field_id = '".$form->getObject()->getId()."' AND field_name = 'faq_question' AND table_class = 'faq' AND locale = '".$_SESSION['locale']."'";
				$rows = mysql_query($sql);
				if($row = mysql_fetch_assoc($rows))
				{
					echo $row['trl_content'];
				}
				else
				{
					echo $form->getObject()->getQuestion();	
				}
			?>">
        </div>
      </div>

	  <div id="nameresult" name="nameresult"></div>

      <script language="javascript">
        $('document').ready(function(){
          $('#faq_question').keyup(function(){
            $.ajax({
                      type: "POST",
                      url: "/backend.php/faq/checkname",
                      data: {
                          'name' : $('input:text[id=faq_question]').val()
                      },
                      dataType: "text",
                      success: function(msg){
                            //Receiving the result of search here
                            $("#nameresult").html(msg);
                      }
                  });
              });
        });
      </script>
      <div class="form-group">
        <label class="col-sm-4" for="text_field"><i class="bold-label"><?php echo __('Answer'); ?></i></label>
           <div class="col-sm-12">
          <?php echo $form['answer']->renderError() ?>
                <textarea name="faq[answer]" id="top_article_txt" class="form-control"><?php 
					$sql = "SELECT * FROM ext_translations WHERE field_id = '".$form->getObject()->getId()."' AND field_name = 'faq_answer' AND table_class = 'faq' AND locale = '".$_SESSION['locale']."'";
					$rows = mysql_query($sql);
					if($row = mysql_fetch_assoc($rows))
					{
						echo $row['trl_content'];
					}
					else
					{
						echo $form->getObject()->getAnswer();	
					}
				?></textarea>
				 
        </div>
      </div>
	  
      </div>
	  
		<div class="panel-footer">
			<div><button class="btn btn-danger mr10"><?php echo __('Reset'); ?></button><button type="submit" class="btn btn-primary" name="submitbuttonname" id="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button></div>
		</div>



</div>
</form>



<script>
jQuery(document).ready(function(){
  
  // CKEditor
  jQuery('#top_article_txt').ckeditor();

});
</script>
