<?php
use_helper("I18N");

$audit = new Audit();
$audit->saveAudit("", "Accessed FAQ settings");

if($sf_user->mfHasCredential("managefaqs"))
{
  $_SESSION['current_module'] = "faq";
  $_SESSION['current_action'] = "index";
  $_SESSION['current_id'] = "";
?>
<div class="contentpanel">
<div class="panel panel-dark">

<div class="panel-heading">
<h3 class="panel-title"><?php echo __('Frequently Asked Questions'); ?></h3>

    <div class="pull-right">
           <a class="btn btn-primary-alt settings-margin42" id="newfaq"  href="<?php echo public_path(); ?>backend.php/faq/new"><?php echo __('New Question'); ?></a>
    </div>
</div>


<div class="table-responsive">
<table class="table dt-on-steroids mb0" id="table3">
  <thead>
    <tr>
      <th class="no-sort" style="width: 10px;"><input type='checkbox' name='batchall' onclick="boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.name == 'batch') { if(this.checked == true){ box.checked = true; }else{ box.checked = false; } } } "></th>
      <th width="60">#</th>
      <th><?php echo __('Question'); ?></th>
	  <th class="no-sort" style="width:7%;"><?php echo __('Actions'); ?></th>
    </tr>
  </thead>
  <tbody id="tblBdy">
    <?php
		$count = 1;
	?>
    <?php foreach ($faqs as $faq): ?>
    <tr id="row_<?php echo $faq->getId() ?>">
	    <td><input type='checkbox' name='batch' id='batch_<?php echo $faq->getId() ?>' value='<?php echo $faq->getId() ?>'></td>
      <td><?php echo $count++; ?></td>
      <td><?php echo $faq->getQuestion() ?></td>
	  <td>
		<a id="editfaq<?php echo $faq->getId() ?>" title="<?php echo __('Edit'); ?>" href="<?php echo public_path(); ?>backend.php/faq/edit/id/<?php echo $faq->getId(); ?>"><span class="badge badge-primary"><i class="fa fa-pencil"></i></span></a>

      	<a id="deletefaq<?php echo $faq->getId() ?>" onClick="if(confirm('Are you sure you want to delete this item?')){ return true; }else{ return false; }" title="<?php echo __('Delete'); ?>" href="<?php echo public_path(); ?>backend.php/faq/delete/id/<?php echo $faq->getId(); ?>"><span class="badge badge-primary"><i class="fa fa-trash-o"></i></span></a>

  </td>
    </tr>
    <?php endforeach; ?>
    </tbody>
	<tfoot>
   <tr><td colspan='7' style='text-align: left;'>
   <select id='batch_action' name='batch_action' onChange="if(this.value != ''){if(confirm('Are you sure?')){ batch('faq', this.options[this.selectedIndex].text, this.value); document.getElementById('default').selected='selected'; }}">
   <option id='default' value=''><?php echo __('Choose an action'); ?>..</option>
   <option value='delete'><?php echo __('Set As Deleted'); ?></option>
   </select>
   </td></tr>
   </tfoot>
</table>
</div>

</div>
</div>
<script>
  jQuery('#table3').dataTable({
      "sPaginationType": "full_numbers",

      // Using aoColumnDefs
      "aoColumnDefs": [
      	{ "bSortable": false, "aTargets": [ 'no-sort' ] }
    	]
    });

</script>
<?php
}
else
{
  include_partial("settings/accessdenied");
}
?>
