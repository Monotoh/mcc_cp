<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $faq->getId() ?></td>
    </tr>
    <tr>
      <th>Question:</th>
      <td><?php echo $faq->getQuestion() ?></td>
    </tr>
    <tr>
      <th>Answer:</th>
      <td><?php echo $faq->getAnswer() ?></td>
    </tr>
    <tr>
      <th>Published:</th>
      <td><?php echo $faq->getPublished() ?></td>
    </tr>
    <tr>
      <th>Posted by:</th>
      <td><?php echo $faq->getPostedBy() ?></td>
    </tr>
    <tr>
      <th>Email:</th>
      <td><?php echo $faq->getEmail() ?></td>
    </tr>
    <tr>
      <th>Created on:</th>
      <td><?php echo $faq->getCreatedOn() ?></td>
    </tr>
    <tr>
      <th>Deleted:</th>
      <td><?php echo $faq->getDeleted() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('faq/edit?id='.$faq->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('faq/index') ?>">List</a>
