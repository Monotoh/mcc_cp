<?php

/**
 * fees actions.
 *
 * @package    permit
 * @subpackage fees
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class feesActions extends sfActions
{
  public function executeGetfee(sfWebRequest $request)
  {
    $string =  $request->getPostParameter("code");
    
    $code = "";
    
    $tok = strtok($string, ":");

	while ($tok !== false) {
		$code = $tok;
		break;
	}
    $q = Doctrine_Query::create()
       ->from("Fee a")
       ->where("a.fee_code = ?", $code);
    $fee = $q->fetchOne();
    if($fee)
    {
    	echo $fee->getAmount();
    }
  	exit;
  }
  public function executeBatch(sfWebRequest $request)
  {
		if($request->getPostParameter('delete'))
		{
			$item = Doctrine_Core::getTable('Fee')->find(array($request->getPostParameter('delete')));
			if($item)
			{
				$item->delete();
			}
		}
    }

  public function executeIndex(sfWebRequest $request)
  {
     $q = Doctrine_Query::create()
       ->from('Fee a')
       ->where("a.invoiceid = ?", $request->getParameter('filter'))
	     ->orderBy('a.id DESC');
     $this->fees = $q->execute();
	   $this->filter = $request->getParameter('filter');
	 
	   $this->setLayout(false);
  }

  public function executeSettingsindex(sfWebRequest $request)
  {
     $q = Doctrine_Query::create()
       ->from('Fee a')
	     ->orderBy('a.id DESC');
     $this->fees = $q->execute();
	   $this->filter = 0;
	 
	   $this->setLayout("layout-settings");
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new feeForm();
    if($request->getParameter('filter'))
    {
    	$this->filter = $request->getParameter('filter');
    }
    else
    {
    	$this->filter = 0;
    }
    $this->filter = $request->getParameter('filter');
	$this->setLayout("layout-settings");
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new feeForm();


    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($fee = Doctrine_Core::getTable('fee')->find(array($request->getParameter('id'))), sprintf('Object fee does not exist (%s).', $request->getParameter('id')));
    $this->form = new feeForm($fee);
     $this->filter = $request->getParameter('filter');
	  $this->setLayout("layout-settings");
	  $this->setSfFormChoiceWidgetOptionsFromApFormElements($fee->getInvoicetemplate()->getApplicationform(), 'base_field');//OTB Patch - Dynamic Base Field selection For Implementing Finance Bills
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($fee = Doctrine_Core::getTable('fee')->find(array($request->getParameter('id'))), sprintf('Object fee does not exist (%s).', $request->getParameter('id')));
    $this->form = new feeForm($fee);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
    $this->setLayout('layout-metronic');
  }

  public function executeDelete(sfWebRequest $request)
  {

    $this->forward404Unless($fee = Doctrine_Core::getTable('fee')->find(array($request->getParameter('id'))), sprintf('Object fee does not exist (%s).', $request->getParameter('id')));

    $audit = new Audit();
    $audit->saveAudit("", "deleted fixed fee of id ".$fee->getId());

    $fee->delete();

    $this->redirect('/backend.php/fees/settingsindex');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $fee = $form->save();
      if($request->getParameter("filter"))
      {
      	$fee->setInvoiceid($request->getParameter("filter"));
        $fee->save();
      }

      $audit = new Audit();
      $audit->saveAudit("", "<a href=\"/backend.php/fees/edit?id=".$fee->getId()."&language=en\">deleted fixed fee</a>");

      $this->redirect('/backend.php/fees/settingsindex');
    }
  }

	   //OTB Start Patch - For Implementing Finance Bills
	public function executeFeerangeindex(sfWebRequest $request)
	{
		$this->filter = $request->getParameter("filter");
		$q_range=Doctrine_Query::create()
			->from('FeeRange f')
			->where('f.fee_id = ?', $this->filter);
		$this->fee_ranges=$q_range->execute();
	}
  public function executeFeerange(sfWebRequest $request)
  {
	$this->filter = $request->getParameter("filter");
	if($request->getParameter('id')){
		$range=Doctrine_Core::getTable('FeeRange')->find(array($request->getParameter('id')));
		$this->form=new FeeRangeForm($range);
	}else{
		$this->form=new FeeRangeForm();
	}
	$fee = Doctrine_Core::getTable('fee')->find(array($this->filter));
	//$this->setSfFormChoiceWidgetOptionsFromApFormElements($fee->getInvoicetemplate()->getApplicationform(), 'condition_field');//OTB Patch - Dynamic Condition Fields selection For Implementing Finance Bills
  }
  public function executeNewfeerange(sfWebRequest $request)
  {
	$this->forward404Unless($request->isMethod(sfRequest::POST));
	$this->form=new FeeRangeForm();
    $this->form->bind($request->getParameter($this->form->getName()), $request->getFiles($this->form->getName()));
    if ($this->form->isValid())
    {
		$this->form->save();
	}
	$this->getUser()->setFlash('save_notice', sprintf('Fee range has been saved'));
	$this->redirect('/backend.php/fees/feerangeindex');
	$this->setTemplate('feerange');
  }
  public function executeUpdatefeerange(sfWebRequest $request)
  {
	$this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
	$this->forward404Unless($range=Doctrine_Core::getTable('FeeRange')->find($request->getParameter('id')));
	$form= new FeeRangeForm($range);
	$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
		$form->save();
	}
	$this->getUser()->setFlash('save_notice', sprintf('Fee range has been updated'));
	$this->redirect('/backend.php/fees/feerangeindex');
	$this->setTemplate('feerange');
  }
  public function executeDeleterange(sfWebRequest $request)
  {
    $this->forward404Unless($fee_range = Doctrine_Core::getTable('FeeRange')->find(array($request->getParameter('id'))), sprintf('Object fee_range does not exist (%s).', $request->getParameter('id')));

    $audit = new Audit();
    $audit->saveAudit("", "deleted fee_range of id ".$fee_range->getId());
	
	$fee_id = $fee_range->getFeeId();

    $fee_range->delete();

      $this->redirect('/backend.php/fees/feerangeindex/filter/'.$fee_id);
  }

  public function executeChangebasefield(sfWebRequest $request)
  {
	  $invoicetemplate = Doctrine_Core::getTable('invoicetemplates')->find(array($request->getParameter('invoicetemplate_id')));
		  if($invoicetemplate){
		  $elements = Doctrine_Core::getTable('ApFormElements')->getAllFields($invoicetemplate->getApplicationform());
		  foreach($elements as $key => $value){
			$new_options .= "<option value='".$key."'>".$value."</option>";
		  }
		  echo $new_options;
	  }
	  exit();
  }
  protected function setSfFormChoiceWidgetOptionsFromApFormElements($apform_id, $widget_name)
  {
	$widget = $this->form->getWidget($widget_name);
	$widget->setOptions(array('choices' => Doctrine_Core::getTable('ApFormElements')->getAllFields($apform_id)));
	//error_log($apform_id." ## configure this form values ### ".print_R($widget->getChoices(), true));
  }

	//Conditions
	public function executeRangeconditions(sfWebRequest $request)
	{
		$this->filter = $request->getParameter("filter");
		$q_range=Doctrine_Query::create()
			->from('FeeRangeCondition f')
			->where('f.fee_range_id = ?', $this->filter);
		$this->fee_range_conditions=$q_range->execute();
	}
  public function executeRangeconditionform(sfWebRequest $request)
  {
	$this->filter = $request->getParameter("filter");
	error_log("the filter ya range condition form #### ".$this->filter);
	if($request->getParameter('id')){
		$rangecondition=Doctrine_Core::getTable('FeeRangeCondition')->find(array($request->getParameter('id')));
		$this->form=new FeeRangeConditionForm($rangecondition);
	}else{
		$this->form=new FeeRangeConditionForm();
	}
	$fee_range = Doctrine_Core::getTable('feerange')->find(array($this->filter));
	$fee = Doctrine_Core::getTable('fee')->find(array($fee_range->getFeeId()));
	$this->setSfFormChoiceWidgetOptionsFromApFormElements($fee->getInvoicetemplate()->getApplicationform(), 'condition_field');//OTB Patch - Dynamic Condition Fields selection For Implementing Finance Bills
  }
  public function executeNewfeerangecondition(sfWebRequest $request)
  {
	$this->forward404Unless($request->isMethod(sfRequest::POST));
	$this->form=new FeeRangeConditionForm();
    $this->form->bind($request->getParameter($this->form->getName()), $request->getFiles($this->form->getName()));
    if ($this->form->isValid())
    {
		$this->form->save();
	}
	$this->getUser()->setFlash('save_notice', sprintf('Fee range has been saved'));
	$this->redirect('/backend.php/fees/rangeconditions');
	$this->setTemplate('feerangecondition');
  }
  public function executeUpdatefeerangecondition(sfWebRequest $request)
  {
	$this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
	$this->forward404Unless($rangecondition=Doctrine_Core::getTable('FeeRangeCondition')->find($request->getParameter('id')));
	$form= new FeeRangeConditionForm($rangecondition);
	$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
	error_log("is valid ###### ".$form->isValid());
    if ($form->isValid())
    {
		$form->save();
	}
	$this->getUser()->setFlash('save_notice', sprintf('Fee range condition has been updated'));
	$this->redirect('/backend.php/fees/rangeconditions');
	$this->setTemplate('rangeconditions');
  }
  public function executeDeleterangecondition(sfWebRequest $request)
  {
    $this->forward404Unless($fee_range_condition = Doctrine_Core::getTable('FeeRangeCondition')->find(array($request->getParameter('id'))), sprintf('Object fee_range_condition does not exist (%s).', $request->getParameter('id')));

    $audit = new Audit();
    $audit->saveAudit("", "deleted fee_range_condition of id ".$fee_range_condition->getId());
	
	$fee_range_id = $fee_range_condition->getFeeRangeId();

    $fee_range_condition->delete();

      $this->redirect('/backend.php/fees/rangeconditions/filter/'.$fee_range_id);
  }

	   //OTB End Patch - For Implementing Finance Bills
}
