<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php
use_helper("I18N");
?>
<div class="contentpanel">
<form id="feeform" class="form-bordered" action="<?php echo url_for('/backend.php/fees/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId()."&filter=".$filter : '?filter='.$filter )) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>  autocomplete="off" data-ajax="false">

<div class="panel panel-dark">
<div class="panel-heading">
<h3 class="panel-title"><?php echo __('Fee Details'); ?></h3>
</div>

<div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
  <strong><?php echo __('Well done'); ?>!</strong> <?php echo __('You successfully updated this record'); ?></a>.
</div>


<div class="panel-body panel-body-nopadding">

   <?php echo $form['_csrf_token']->render(); ?> 
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
      <?php echo $form->renderGlobalErrors() ?>
      <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Invoice'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['invoiceid']->renderError() ?>
          <?php echo $form['invoiceid'] ?>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Fee Code'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['fee_code']->renderError() ?>
          <?php echo $form['fee_code'] ?>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Description'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['description']->renderError() ?>
          <?php echo $form['description'] ?>
        </div>
      </div>
	  <!--OTB Start Patch - For Implementing Finance Bills -->
      <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Fee Type'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['fee_type']->renderError() ?>
          <?php echo $form['fee_type'] ?>
        </div>
		<script language="javascript">
			jQuery(document).ready(function(){
				//default is fixed so first hide percentage and range areas
				get_fields_to_show($('#fee_fee_type').val());
				 $('#fee_fee_type').change(function(){
					var value = this.value ;
					get_fields_to_show(value);
				})
				
				function get_fields_to_show(value){
					if(value == "percentage"){
							$('#fixed_amount_div').show();
							$("#loadranges").hide();
							//$('#percentage_div').show();
							$('#base_field').show();
					}else if(value == "fixed") {
							$('#fixed_amount_div').show();
							$("#loadranges").hide();
							//$('#percentage_div').hide();
							$('#base_field').hide();
					}else if(value == "range" || value == "range_percentage"){
							$('#fixed_amount_div').hide();
							//$('#percentage_div').hide();
							$('#base_field').show();
							$("#loadranges").show();
					}else if(value == "formula") {
							$('#fixed_amount_div').show();
							$("#loadranges").hide();
							$('#base_field').show();
					}
				}
			});
		</script>
      </div>
      <!--<div id="percentage_div" class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Percentage(%)'); ?></i></label>
        <div class="col-sm-8">
          <?php echo $form['percentage']->renderError() ?>
          <?php echo $form['percentage'] ?>
        </div>
      </div>-->
      <div id="base_field" class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Base field'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['base_field']->renderError() ?>
          <?php echo $form['base_field'] ?>
        </div>
      </div>
	  <!--OTB End Patch - For Implementing Finance Bills -->
      <div class="form-group">
		<div class="col-sm-12 alignright">
		  <button type="button" class="btn btn-primary" data-target="#fieldsModal" data-toggle="modal">View available user/form fields</button>
		</div>
	  </div>
      <div id="fixed_amount_div" class="form-group"><!--OTB Patch - Added fixed_amount_div For Implementing Finance Bills -->
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Value'); ?></i></label><!--OTB Patch - rename to Value-->
        <div class="col-sm-8 rogue-input">
          <?php echo $form['amount']->renderError() ?>
          <?php echo $form['amount'] ?>
        </div>
      </div>
	  <!--OTB Start Patch - For Implementing Finance Bills. Load fee structures area -->
	<div class="form-group">
	        <div class="col-sm-12" id="loadranges" name="loadranges">

	        </div>
	        <?php
	          $feeid = 0;
			  if(!$form->getObject()->isNew())
			  {
			  		$feeid = $form->getObject()->getId();
			  }
	        ?>
			<script language="javascript">
			 jQuery(document).ready(function(){
		        $("#loadranges").load("<?php echo public_path(); ?>backend.php/fees/feerangeindex/filter/<?php echo $feeid; ?>");
				 $('#fee_invoiceid').change(function(){
					var value = this.value ;
					 $.ajax({
						url: '<?php echo url_for('/backend.php/fees/changebasefield/invoicetemplate_id/'); ?>'+value,
						cache: false,
						type: 'POST',
						data : $('#bform').serialize(),
						success: function(json) {
							$('#fee_base_field').empty().append(json);
						}
					});
				})
		     });
			 </script>
	      </div>
	  <!--OTB End Patch - For Implementing Finance Bills. Load fee structures area -->
	   <div class="panel-footer">
			<button class="btn btn-danger mr10"><?php echo __('Reset'); ?></button><button type="submit" class="btn btn-primary" name="submitbuttonname" id="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button>
	  </div>
</form>
</div>

		  <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="fieldsModal" class="modal fade" style="display: none;">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
			        <h4 id="myModalLabel" class="modal-title">View available user/form fields</h4>
			      </div>
			      <div class="modal-body">
			        <div class="form-group">
		<?php
			//Get User Information (anything starting with sf_ )
					   //sf_email, sf_fullname, sf_username, ... other fields in the dynamic user profile form e.g sf_element_1
			?>
			<table class="table dt-on-steroids mb0">
			<thead><tr><th width="50%"><?php echo __('User Details'); ?></th><th><?php echo __('Tag'); ?></th></tr></thead>
			<tbody>
			<tr><td><?php echo __('User ID'); ?></td><td>{sf_username}</td></tr>
            <tr><td><?php echo __('Mobile Number'); ?></td><td>{sf_mobile}</td></tr>
			<tr><td><?php echo __('Email'); ?></td><td>{sf_email}</td></tr>
			<tr><td><?php echo __('Full Name'); ?></td><td>{sf_fullname}</td></tr>
			<tr><td><?php echo __('User Category'); ?></td><td>{sf_user_category}</td></tr>
			<?php
					$q = Doctrine_Query::create()
					   ->from('apFormElements a')
					   ->where('a.form_id = ?', 15)
             ->andWhere('a.element_status = ?', 1)
             ->orderBy('a.element_position ASC');

					$elements = $q->execute();

					foreach($elements as $element)
					{
						$childs = $element->getElementTotalChild();
						if($childs == 0)
						{
						   echo "<tr><td>".$element->getElementTitle()."</td><td>{sf_element_".$element->getElementId()."}</td></tr>";
						}
						else
						{
							if($element->getElementType() == "select")
							{
								echo "<tr><td>".$element->getElementTitle()."</td><td>{sf_element_".$element->getElementId()."}</td></tr>";
							}
							else
							{
								for($x = 0; $x < ($childs + 1); $x++)
								{
									echo "<tr><td>".$element->getElementTitle()."</td><td>{sf_element_".$element->getElementId()."_".($x+1)."}</td></tr>";
								}
							}
						}
					}
			?>
			</tbody>
			</table>


			<table class="table dt-on-steroids mb0">
			<thead><tr><th width="50%"><?php echo __('Application Details'); ?></th><th><?php echo __('Tag'); ?></th></tr></thead>
			<tbody>
			     <tr><td><?php echo __('User Profile Picture'); ?></td><td>{profile_pic}</td></tr>
            <tr><td><?php echo __('Bar Code'); ?></td><td>{bar_code}</td></tr>
            <tr><td><?php echo __('Bar Code (Small)'); ?></td><td>{bar_code_small}</td></tr>
			<tr><td><?php echo __('Application Number'); ?></td><td>{ap_application_id}</td></tr>
			<tr><td><?php echo __('Created At'); ?></td><td>{fm_created_at}</td></tr>
			<tr><td><?php echo __('Approved At'); ?></td><td>{fm_updated_at}</td></tr>
      <tr><td><?php echo __('Permit ID'); ?></td><td>{ap_permit_id}</td></tr>
			<tr><td><?php echo __('Permit Issued At'); ?></td><td>{ap_issue_date}</td></tr>
      		<tr><td><?php echo __('Permit Expires At'); ?></td><td>{ap_expire_date}</td></tr>
          <tr><td><?php echo __('Permit UUID'); ?></td><td>{uuid}</td></tr>
			<?php
			//Get Application Information (anything starting with ap_ )
					   //ap_application_id

			//Get Form Details (anything starting with fm_ )
					   //fm_created_at, fm_updated_at.....fm_element_1
			?>
		 <?php
		  if(!$form->getObject()->isNew())
		  {
		    $appform = $form->getObject()->getInvoicetemplate()->getApplicationform();
		  ?>
			<?php

					$q = Doctrine_Query::create()
					   ->from('apFormElements a')
					   ->where('a.form_id = ?', $appform)
             ->andWhere('a.element_status = ?', 1)
             ->orderBy('a.element_position ASC');

					$elements = $q->execute();

					foreach($elements as $element)
					{
						$childs = $element->getElementTotalChild();
						if($childs == 0)
						{
               if($element->getElementType() == "select")
               {
                 if($element->getElementExistingForm() && $element->getElementExistingStage())
                 {
                   $q = Doctrine_Query::create()
                    ->from("ApForms a")
                    ->where("a.form_id = ?", $element->getElementExistingForm());
                   $child_form = $q->fetchOne();

                   echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."} ";
                     echo '<table class="table dt-on-steroids mb0">
                     <thead><tr><th width="50%">'.__($child_form->getFormName().' Details').'</th><th>'.__('Tag').'</th></tr></thead>
                     <tbody>';

                      ?>
                      <tr><td><?php echo __('Application Number'); ?></td><td>{ap_child_application_id}</td></tr>
                      <tr><td><?php echo __('Created At'); ?></td><td>{fm_child_created_at}</td></tr>
                      <tr><td><?php echo __('Approved At'); ?></td><td>{fm_child_updated_at}</td></tr>
                      <?php
                        $q = Doctrine_Query::create()
                           ->from("Permits a")
                           ->where("a.applicationform = ?", $element->getElementExistingForm());
                        $permits = $q->execute();

                        foreach($permits as $permit)
                        {
                            echo "<tr><td>".$permit->getTitle()." ID</td><td>{ap_permit_id_".$permit->getId()."_element_child}</td></tr>";
                        }

                      $q = Doctrine_Query::create()
                         ->from('apFormElements a')
                         ->where('a.form_id = ?', $element->getElementExistingForm())
                         ->andWhere('a.element_status = ?', 1)
                         ->orderBy('a.element_position ASC');

                      $child_elements = $q->execute();

                      foreach($child_elements as $child_element)
                      {

                          //START CHILD ELEMENTS
                          $childs = $child_element->getElementTotalChild();
                          if($childs == 0)
                          {
                             if($child_element->getElementType() == "select")
                             {
                               if($child_element->getElementExistingForm() && $child_element->getElementExistingStage())
                               {

                                 $q = Doctrine_Query::create()
                                  ->from("ApForms a")
                                  ->where("a.form_id = ?", $child_element->getElementExistingForm());
                                 $grand_form = $q->fetchOne();

                                   echo "<tr><td>".$child_element->getElementTitle()."</td><td>{fm_child_element_".$child_element->getElementId()."} ";
                                   echo '<table class="table dt-on-steroids mb0">
                                   <thead><tr><th width="50%">'.__($grand_form->getFormName().' Details').'</th><th>'.__('Tag').'</th></tr></thead>
                                   <tbody>';

                                    $q = Doctrine_Query::create()
                                       ->from('apFormElements a')
                                       ->where('a.form_id = ?', $child_element->getElementExistingForm())
                                       ->andWhere('a.element_status = ?', 1)
                                       ->orderBy('a.element_position ASC');

                                    $grand_child_elements = $q->execute();

                                    foreach($grand_child_elements as $grand_child_element)
                                    {
                                      ?>
                                      <tr><td><?php echo __('Application Number'); ?></td><td>{ap_grand_child_application_id}</td></tr>
                                      <tr><td><?php echo __('Created At'); ?></td><td>{fm_grand_child_created_at}</td></tr>
                                      <tr><td><?php echo __('Approved At'); ?></td><td>{fm_grand_child_updated_at}</td></tr>
                                      <?php
                                       $q = Doctrine_Query::create()
                                          ->from("Permits a")
                                          ->where("a.applicationform = ?", $child_element->getElementExistingForm());
                                       $permits = $q->execute();

                                       foreach($permits as $permit)
                                       {
                                           echo "<tr><td>".$permit->getTitle()." ID</td><td>{ap_permit_id_".$permit->getId()."_element_grand_child}</td></tr>";
                                       }

                                        //START GRAND CHILD ELEMENTS
                                        $childs = $grand_child_element->getElementTotalChild();
                                        if($childs == 0)
                                        {
                                           if($grand_child_element->getElementType() == "select")
                                           {
                                               echo "<tr><td>".$grand_child_element->getElementTitle()."</td><td>{fm_grand_child_element_".$grand_child_element->getElementId()."}</td></tr>";
                                           }
                                           else
                                           {
                                              echo "<tr><td>".$grand_child_element->getElementTitle()."</td><td>{fm_grand_child_element_".$grand_child_element->getElementId()."}</td></tr>";
                                           }
                                        }
                                        else
                                        {
                                            for($x = 0; $x < ($childs + 1); $x++)
                                            {
                                              echo "<tr><td>".$grand_child_element->getElementTitle()."</td><td>{fm_grand_child_element_".$grand_child_element->getElementId()."_".($x+1)."}</td></tr>";
                                            }
                                        }
                                        //END GRAND CHILD ELEMENTS
                                    }

                                   echo '</tbody></table>';
                                 echo "</td></tr>";
                               }
                               else
                               {
                                 echo "<tr><td>".$child_element->getElementTitle()."</td><td>{fm_child_element_".$child_element->getElementId()."}</td></tr>";
                               }
                             }
                             else
                             {
                                echo "<tr><td>".$child_element->getElementTitle()."</td><td>{fm_child_element_".$child_element->getElementId()."}</td></tr>";
                             }
                          }
                          else
                          {
                              for($x = 0; $x < ($childs + 1); $x++)
                              {
                                echo "<tr><td>".$child_element->getElementTitle()."</td><td>{fm_child_element_".$child_element->getElementId()."_".($x+1)."}</td></tr>";
                              }
                          }
                          //END CHILD ELEMENTS
                      }

                     echo '</tbody></table>';
                   echo "</td></tr>";
                 }
                 else
                 {
                   echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."}</td></tr>";
                 }
               }
               else
               {
                  echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."}</td></tr>";
               }
            }
						else
						{
								for($x = 0; $x < ($childs + 1); $x++)
								{
									echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."_".($x+1)."}</td></tr>";
								}
						}
					}

          ?>
          <tr><td><strong>Comment Sheets</strong></td><td><strong>Tag</strong></td></tr>
          <?php

          //Comment Sheets
          $q = Doctrine_Query::create()
             ->from('SubMenus a')
             ->where('a.id = ?', $form->getObject()->getInvoicetemplate()->getApplicationstage());
          $stage = $q->fetchOne();

          if($stage)
          {

		  //OTB Patch Start - Show comment sheet tags for the whole workflow
          $q = Doctrine_Query::create()
             ->from('Menus a')
             ->where('a.category_id = ?', $stage->getMenus()->getCategoryId());
          $workflows = $q->execute();
			$workflow_ids = array();
			foreach($workflows as $workflow){
				array_push($workflow_ids, $workflow->getId());
			}
		  //OTB Patch End - Show comment sheet tags for the whole workflow
		  
          $q = Doctrine_Query::create()
            ->from('SubMenus a')
            //->where('a.menu_id = ?', $stage->getMenuId())
            ->whereIn('a.menu_id', $workflow_ids)//OTB Patch - Show comment sheet tags for the whole workflow
            ->orderBy('a.order_no ASC');
          $stages = $q->execute();

          $filstages = "";

          $filtags = "";

          $count = 0;

          foreach($stages as $stage)
          {
              $filstages[] = $stage->getId();
              if($count == 0)
              {
                $filtags = $filtags."a.form_department_stage = ? ";
              }
              else
              {
                $filtags = $filtags."OR a.form_department_stage = ? ";
              }
            $count++;
          }

          $q = Doctrine_Query::create()
             ->from("ApForms a")
             ->where($filtags, $filstages);
          $forms = $q->execute();

          foreach($forms as $apform)
          {
            echo "<tr><td><strong>".$apform->getFormName()." details</strong></td><td><strong>Tag</strong></td></tr>";
             $q = Doctrine_Query::create()
                ->from('apFormElements a')
                ->where('a.form_id = ?', $apform->getFormId())
                ->andWhere('a.element_status = ?', 1)
                ->orderBy('a.element_position ASC');

             $elements = $q->execute();

             foreach($elements as $element)
             {
               $childs = $element->getElementTotalChild();
               if($childs == 0)
               {
                  if($element->getElementType() == "select")
                  {
                      echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_c".$apform->getFormId()."_element_".$element->getElementId()."}</td></tr>";
                  }
                  else
                  {
                     echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_c".$apform->getFormId()."_element_".$element->getElementId()."}</td></tr>";
                  }
               }
               else
               {
                   for($x = 0; $x < ($childs + 1); $x++)
                   {
                     echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_c".$apform->getFormId()."_element_".$element->getElementId()."_".($x+1)."}</td></tr>";
                   }
               }
             }
          }
        }

			?>
		  <?php
		  }
		  ?>
			</tbody>
			</table>
            <table class="table dt-on-steroids mb0">
                <thead><tr><th width="50%"><?php echo __('Invoice Details'); ?></th><th><?php echo __('Tag'); ?></th></tr></thead>
                <tbody>
                <tr><td><?php echo __('Invoice No'); ?></td><td>{inv_no}</td></tr>
                <tr><td><?php echo __('Invoice Date'); ?></td><td>{inv_date_created}</td></tr>
                <tr><td><?php echo __('Invoice Expiry Date'); ?></td><td>{inv_expires_at}</td></tr>
                <tr><td><?php echo __('List of Fees'); ?></td><td>{inv_fee_table}</td></tr>
                <tr><td><?php echo __('Total'); ?></td><td>{inv_total}</td></tr>
                <tr><td><?php echo __('Payment Reference Number'); ?></td><td>{inv_payment_id}</td></tr>
                <tr><td><?php echo __('Payment Transaction Id'); ?></td><td>{inv_transaction_id}</td></tr>
                </tbody>
            </table>
			<table class="table dt-on-steroids mb0">
			<thead><tr><th width="50%"><?php echo __('Conditions Of Approval'); ?></th><th><?php echo __('Tag'); ?></th></tr></thead>
			<tbody>
			<tr><td><?php echo __('Conditions Of Approval'); ?></td><td>{ca_conditions}</td></tr>
			</tbody>
			</table>
		</div>
	      <div class="modal-footer">
	        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
	      </div>
	    </div><!-- modal-content -->
	  </div><!-- modal-dialog -->
	</div>
	</div>