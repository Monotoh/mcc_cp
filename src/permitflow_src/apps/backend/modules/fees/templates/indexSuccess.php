<?php
use_helper("I18N");
if($sf_user->mfHasCredential("managefees"))
{
?>
<?php use_helper('I18N', 'Date') ?>

<div class="panel panel-dark">
<div class="panel-heading">
			<h3 class="panel-title"><?php echo __('Fees'); ?></h3>
				<div class="pull-right">
            <a class="btn btn-primary-alt settings-margin42" id="newfee" href="#end"><?php echo __('New Fee'); ?></a>

            <script language="javascript">
            jQuery(document).ready(function(){
              $( "#newfee" ).click(function() {
                  $("#loadinner").load("<?php echo public_path(); ?>backend.php/fees/new/filter/<?php echo $filter; ?>");
              });
            });
            </script>
</div>
</div>
		
 
<div class="panel panel-body panel-body-nopadding ">

<div class="table-responsive">
<table class="table dt-on-steroids mb0" id="table3">
    <thead>
   <tr>
      <th width="1%">#</th>
      <th><?php echo __('Code'); ?></th>
      <th><?php echo __('Description'); ?></th>
      <th><?php echo __('Amount'); ?></th>
      <th class="no-sort" width="7%"><?php echo __('Actions'); ?></th>
    </tr>
  </thead>
    <tbody>
    <?php foreach ($fees as $fee): ?>
    <tr id="row_<?php echo $fee->getId() ?>">
		<td><a href="<?php echo url_for('/backend.php/fees/edit?id='.$fee->getId()) ?>"><?php echo $fee->getId() ?></a></td>
		<td><a href="<?php echo url_for('/backend.php/fees/edit?id='.$fee->getId()) ?>"><?php echo $fee->getFeeCode() ?></a></td>
		<td><a href="<?php echo url_for('/backend.php/fees/edit?id='.$fee->getId()) ?>"><?php echo $fee->getDescription() ?></a></td>
		<td><a href="<?php echo url_for('/backend.php/fees/edit?id='.$fee->getId()) ?>"><?php echo $fee->getAmount() ?></a></td>
    <td>					
	  <a id="editfee<?php echo $fee->getId() ?>" href="#end" alt="Edit" title="<?php echo __('Edit'); ?>"><span class="badge badge-primary"><i class="fa fa-pencil"></i></span></a>
     
      <a id="deletefee<?php echo $fee->getId() ?>" href="#end" alt="Delete" title="<?php echo __('Delete'); ?>"><span class="badge badge-primary"><i class="fa fa-trash-o"></i></span></a>
	 

            <script language="javascript">
            jQuery(document).ready(function(){
              $( "#editfee<?php echo $fee->getId() ?>" ).click(function() {
                  $("#loadinner").load("<?php echo public_path(); ?>backend.php/fees/edit?id=<?php echo $fee->getId(); ?>&filter=<?php echo $filter; ?>");
              });
              $( "#deletefee<?php echo $fee->getId() ?>" ).click(function() {
                  if(confirm('Are you sure you want to delete this fee?')){
                   $("#loadinner").load("<?php echo public_path(); ?>backend.php/fees/delete?id=<?php echo $fee->getId(); ?>&filter=<?php echo $filter; ?>");
                   $("#loadinner").load("<?php echo public_path(); ?>backend.php/fees/index/filter/<?php echo $filter; ?>");
                  }
              });
            });
            </script>
   </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
</div>
<a name="end"></a>

</div><!--panel-body-->
</div><!--panel-dark-->

<script>
  jQuery('#table3').dataTable({
      "sPaginationType": "full_numbers",
     
      // Using aoColumnDefs
      "aoColumnDefs": [
      	{ "bSortable": false, "aTargets": [ 'no-sort' ] }
    	]
    });
    
</script>
<?php
}
else
{
  include_partial("settings/accessdenied"); //OTB patch
}
?>
