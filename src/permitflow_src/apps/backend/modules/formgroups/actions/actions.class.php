<?php

/**
 * formgroups actions.
 *
 * @package    permit
 * @subpackage formgroups
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class formgroupsActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $this->ap_form_groupss = Doctrine_Core::getTable('FormGroups')
      ->createQuery('a')
      ->execute();
            $this->setLayout("layout-settings");
  }

  public function executeBatch(sfWebRequest $request)
  {
    if($request->getPostParameter('delete'))
    {
      $item = Doctrine_Core::getTable('FormGroups')->find(array($request->getPostParameter('delete')));
      if($item)
      {
        $item->delete();
      }
    }
  }
    
  /**
   * Executes 'Checkname' action
   *
   * Ajax used to check existence of name
   *
   * @param sfRequest $request A request object
   */
  public function executeCheckname(sfWebRequest $request)
  {
      // add new user
      $q = Doctrine_Query::create()
         ->from("FormGroups a")
         ->where('a.group_name = ?', $request->getPostParameter('name'));
      $existingcategory = $q->execute();
      if(sizeof($existingcategory) > 0)
      {
            echo '<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Name is already in use!</strong></div><script language="javascript">document.getElementById("submitbuttonname").disabled = true;</script>';
            exit;
      }
      else
      {
            echo '<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Name is available!</strong></div><script language="javascript">document.getElementById("submitbuttonname").disabled = false;</script>';
            exit;
      }
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new ApFormGroupsForm();
    $this->setLayout("layout-settings");
  }

  public function executeCreate(sfWebRequest $request)
  {
	$this->forward404Unless($request->isMethod(sfRequest::POST));

	$fgroup = new FormGroups();
	$fgroup->setGroupName($request->getPostParameter("group_name"));
	$fgroup->setGroupParent($request->getPostParameter("group_parent"));
	$fgroup->setGroupDescription($request->getPostParameter("group_description"));
	$fgroup->save();

	$translation = new translation();
	$translation->setTranslation("form_groups","group_name",$fgroup->getGroupId(),$request->getPostParameter("group_name"));
	$translation->setTranslation("form_groups","group_description",$fgroup->getGroupId(),$request->getPostParameter("group_description"));
	
	$apforms = $request->getPostParameter("ap_forms");

  $q = Doctrine_Query::create()
      ->from('ApForms a')
      ->where('a.form_group = ?', $fgroup->getGroupId());
  $existing_forms = $q->execute();

  foreach($existing_forms as $existing_form)
  {
    $existing_form->setFormGroup("0");
    $existing_form->save();
  }

  foreach($apforms as $apform)
  {
    $q = Doctrine_Query::create()
      ->from('ApForms a')
      ->where('a.form_id = ?', $apform);
    $form = $q->fetchOne();
    $form->setFormGroup($fgroup->getGroupId());
    $form->save();
  }
	
	$this->redirect('/backend.php/formgroups/index');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($ap_form_groups = Doctrine_Core::getTable('FormGroups')->find(array($request->getParameter('id'))), sprintf('Object ap_form_groups does not exist (%s).', $request->getParameter('id')));
    $this->form = new FormGroupsForm($ap_form_groups);
    $this->setLayout("layout-settings");
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($ap_form_groups = Doctrine_Core::getTable('FormGroups')->find(array($request->getParameter('id'))), sprintf('Object ap_form_groups does not exist (%s).', $request->getParameter('id')));
    
	
    $fgroup = Doctrine_Core::getTable('FormGroups')->find(array($request->getParameter('id')));
    $fgroup->setGroupName($request->getPostParameter("group_name"));
    $fgroup->setGroupDescription($request->getPostParameter("group_description"));
    $fgroup->setGroupParent($request->getPostParameter("group_parent"));
    $fgroup->save();

	$translation = new translation();
	$translation->setTranslation("form_groups","group_name",$fgroup->getGroupId(),$request->getPostParameter("group_name"));
	$translation->setTranslation("form_groups","group_description",$fgroup->getGroupId(),$request->getPostParameter("group_description"));
	

  $q = Doctrine_Query::create()
      ->from('ApForms a')
      ->where('a.form_group = ?', $fgroup->getGroupId());
  $existing_forms = $q->execute();

  foreach($existing_forms as $existing_form)
  {
    $existing_form->setFormGroup("0");
    $existing_form->save();
  }

	$apforms = $request->getPostParameter("ap_forms");
	foreach($apforms as $apform)
	{
    $q = Doctrine_Query::create()
      ->from('ApForms a')
      ->where('a.form_id = ?', $apform);
    $form = $q->fetchOne();
    $form->setFormGroup($fgroup->getGroupId());
    $form->save();
	}
	
	$this->redirect('/backend.php/formgroups/index');
	
  }

  public function executeDelete(sfWebRequest $request)
  {
    $this->forward404Unless($ap_form_groups = Doctrine_Core::getTable('FormGroups')->find(array($request->getParameter('id'))), sprintf('Object ap_form_groups does not exist (%s).', $request->getParameter('id')));
    
    $q = Doctrine_Query::create()
        ->from('ApForms a')
        ->where('a.form_group = ?', $ap_form_groups->getGroupId());
    $existing_forms = $q->execute();

    foreach($existing_forms as $existing_form)
    {
      $existing_form->setFormGroup("0");
      $existing_form->save();
    }

    $ap_form_groups->delete();

    $this->redirect('/backend.php/formgroups/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $ap_form_groups = $form->save();

      $this->redirect('/backend.php/formgroups/index');
    }
  }
}
