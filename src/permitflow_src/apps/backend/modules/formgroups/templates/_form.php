<?php
use_helper("I18N");
?>
<div class="panel-body panel-body-nopadding">

<form id="groupform" class="form-bordered form-horizontal" action="<?php echo public_path(); ?>backend.php/formgroups/<?php echo ($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getGroupId() : ''); ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>

<div class="form-group"><label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Name'); ?></i></label>
<?php
    $translation = new translation();

	$group_name = "";

	if(!$form->getObject()->isNew())
	{
	   $group_name = $form->getObject()->getGroupName();
	   if($translation->getTranslation('form_groups','group_name',$form->getObject()->getGroupId()))
       {
          $group_name = $translation->getTranslation('form_groups','group_name',$form->getObject()->getGroupId());
       }
	}
?>
<div class="col-sm-8"><input class="form-control" type='text' name='group_name' id='group_name' value='<?php echo ($form->getObject()->isNew() ? '' : $group_name); ?>'></div>
</div>

  <div id="nameresult" name="nameresult"></div>

  <script language="javascript">
    $('document').ready(function(){
      $('#group_name').keyup(function(){
        $.ajax({
                  type: "POST",
                  url: "/backend.php/formgroups/checkname",
                  data: {
                      'name' : $('input:text[id=group_name]').val()
                  },
                  dataType: "text",
                  success: function(msg){
                        //Receiving the result of search here
                        $("#nameresult").html(msg);
                  }
              });
          });
    });
  </script>

<div class="form-group"><label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Parent Group'); ?></i></label>
<div class="col-sm-8">
	<select name='group_parent' id='group_parent'>
	   <option value='0'><?php echo __('Choose a Parent'); ?></option>
	   <?php
		   $q = Doctrine_Query::create()
		   ->from('FormGroups a')
		   ->where('a.group_parent = ?', '0')
		   ->orderBy('a.group_name ASC');
		   $formgroups = $q->execute();
		   foreach($formgroups as $group)
		   {
			    $selected = "";
				if(!$form->getObject()->isNew())
				{
					if($form->getObject()->getGroupParent() == $group->getGroupId())
					{
						$selected = 'selected';
					}
				}
			   ?>
			   <option value='<?php echo $group->getGroupId(); ?>' <?php echo $selected; ?>><?php echo $group->getGroupName(); ?></option>
			   <?php
		   }
	   ?>
	</select>
</div>
</div>

<div class="form-group"><label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Description'); ?></i></label>
<?php
	$group_description = "";
	if(!$form->getObject()->isNew())
	{
	   $group_description = $form->getObject()->getGroupDescription();
	   if($translation->getTranslation('form_groups','group_description',$form->getObject()->getGroupId()))
       {
          $group_description = $translation->getTranslation('form_groups','group_description',$form->getObject()->getGroupId());
       }
	}
?>
<div class="col-sm-8"><textarea name='group_description' class="form-control" id='top_article_txt'><?php echo ($form->getObject()->isNew() ? '' : $group_description); ?></textarea></div>
</div>


<div class="form-group"><label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Forms'); ?></i></label>
<div class="col-sm-8">
<select id='ap_forms' name='ap_forms[]' multiple="multiple">
<?php
  $q = Doctrine_Query::create()
     ->from('ApForms a')
	  ->where('a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ?',array('6','7','15','16','17'))
	  ->andWhere("a.form_active = 1")
	  ->andWhere("a.form_type = 1")
	  ->orderBy('a.form_name ASC');
  $forms = $q->execute();
  foreach($forms as $apform)
  {
  	$selected = "";
	if(!$form->getObject()->isNew())
	{
		if($form->getObject()->getGroupId() == $apform->getFormGroup())
		{
			$selected = "selected=\"selected\"";
		}
	}
	echo "<option value='".$apform->getFormId()."' ".$selected.">".$apform->getFormName()."-".$apform->getFormDescription()."</option>";
  }
?>
</select>
        
</div>
</div>
</div><!--Panel-body-->


<div class="panel-footer">
<button type='submit' class='btn btn-primary' id="submitbuttonname"><?php echo __('Submit'); ?></button>
</div>
</form>

<script type="text/javascript" src="<?php echo public_path(); ?>assets_unified/js/jquery.bootstrap-duallistbox.js"></script>


<script>
jQuery(document).ready(function(){
  
  // CKEditor
  var list1 = jQuery('select[name="ap_forms[]"]').bootstrapDualListbox();

});
</script>