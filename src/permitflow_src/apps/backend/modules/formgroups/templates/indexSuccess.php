<?php
use_helper("I18N");

$audit = new Audit();
$audit->saveAudit("", "Accessed form groups settings");

if($sf_user->mfHasCredential("manageformgroups"))
{
  $_SESSION['current_module'] = "formgroups";
  $_SESSION['current_action'] = "index";
  $_SESSION['current_id'] = "";
?>
<div class="contentpanel">
<div class="panel panel-dark">
<div class="panel-heading">
			<h3 class="panel-title"><?php echo __('Form Groups'); ?></h3>
				<div class="pull-right">
            <a class="btn btn-primary-alt settings-margin42" id="newgroup" href="<?php echo public_path(); ?>backend.php/formgroups/new"><?php echo __('New Form Group'); ?></a>
</div>
</div>


<div class="panel panel-body panel-body-nopadding ">

<div class="table-responsive">
<table class="table dt-on-steroids mb0" id="table3">
  <thead>
    <tr>
      <th class="no-sort"><input type='checkbox' name='batchall' onclick="boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.name == 'batch') { if(this.checked == true){ box.checked = true; }else{ box.checked = false; } } } "></th>
      <th width="60px">#</th>
      <th><?php echo __('Name'); ?></th>
      <th class="no-sort" width="60px"><?php echo __('Actions'); ?></th>
    </tr>
  </thead>
  <tbody>
    <?php
    $translation = new translation();
    ?>
    <?php foreach ($ap_form_groupss as $ap_form_groups): ?>
    <tr id="row_<?php echo $ap_form_groups->getGroupId(); ?>">
      <td><input type='checkbox' name='batch' id='batch_<?php echo $ap_form_groups->getGroupId(); ?>' value='<?php echo $ap_form_groups->getGroupId(); ?>'></td>
      <td><a href="<?php echo public_path().url_for('backend.php/formgroups/edit?id='.$ap_form_groups->getGroupId()) ?>"><?php echo $ap_form_groups->getGroupId() ?></a></td>
      <td><?php
       if($translation->getTranslation('form_groups','group_name',$ap_form_groups->getGroupId()))
       {
          echo $translation->getTranslation('form_groups','group_name',$ap_form_groups->getGroupId());
       }
       else
       {
          echo $ap_form_groups->getGroupName();
       }
       ?></td>
      <td>
	       <a id="editgroup<?php echo $ap_form_groups->getGroupId(); ?>" href="<?php echo public_path(); ?>backend.php/formgroups/edit/id/<?php echo $ap_form_groups->getGroupId(); ?>" title="<?php echo __('Edit'); ?>"><span class="badge badge-primary"><i class="fa fa-pencil"></i></span></a>
		     
      	 <a id="deletegroup<?php echo $ap_form_groups->getGroupId(); ?>" onClick="if(confirm('Are you sure you want to delete this item?')){ return true; }else{ return false; }" href="<?php echo public_path(); ?>backend.php/formgroups/delete/id/<?php echo $ap_form_groups->getGroupId(); ?>" title="<?php echo __('Delete'); ?>"><span class="badge badge-primary"><i class="fa fa-trash-o"></i></span></a>
	        

	  </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
  <tfoot>
   <tr><td colspan='7' style='text-align: left;'>
   <select id='batch_action' name='batch_action' onChange="if(this.value != ''){if(confirm('Are you sure?')){ batch('formgroups', this.options[this.selectedIndex].text, this.value); document.getElementById('default').selected='selected'; }}">
   <option id='default' value=''><?php echo __('Choose an action'); ?>..</option>
   <option value='delete'><?php echo __('Set As Deleted'); ?></option>
   </select>
   </td></tr>
   </tfoot>
</table>
</div>
</div><!--panel-body-->
</div><!--panel-dark-->
</div>
<script>
  jQuery(document).ready(function() {
    jQuery('#table3').dataTable({
        "sPaginationType": "full_numbers",

        // Using aoColumnDefs
        "aoColumnDefs": [
        	{ "bSortable": false, "aTargets": [ 'no-sort' ] }
      	]
      });
  });
</script>
<?php
}
else
{
  include_partial("settings/accessdenied");
}
?>
