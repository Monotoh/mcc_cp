<?php
use_helper("I18N");
?>
<div class="contentpanel">
<div class="panel panel-dark">
<div class="panel-heading">
<h3 class="panel-title"><?php echo __('New Form Group'); ?></h3>
</div>
<div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
  <strong><?php echo __('Well done'); ?>!</strong> <?php echo __('You successfully updated this record'); ?></a>.
</div>

<?php include_partial('form', array('form' => $form)) ?>
</div>
</div>