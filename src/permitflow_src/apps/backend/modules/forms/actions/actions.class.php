<?php
/**
 * Forms actions.
 *
 * Form Management Service
 *
 * @package    backend
 * @subpackage forms
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
class formsActions extends sfActions
{
    /**
    * Executes 'Index' action
    *
    * Displays list of dynamically generated forms
    *
    * @param sfRequest $request A request object
    */
    public function executeIndex(sfWebRequest $request)
    {
            $this->setLayout("layout-settings");
    }

    /**
    * Executes 'Ajaxindex' action
    *
    * Displays list of dynamically generated forms
    *
    * @param sfRequest $request A request object
    */
    public function executeAjaxindex(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    /**
    * Executes 'Ajaxeditform' action
    *
    * Allows for editing of a dynamic form
    *
    * @param sfRequest $request A request object
    */
    public function executeAjaxeditform(sfWebRequest $request)
    {
            $this->setLayout(false);
    }

    public function executeAjaxdeleteform(sfWebRequest $request)
    {
        $this->id = $request->getParameter("id");
        $this->setLayout(false);
    }

    public function executeAjaxduplicateform(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    /**
    * Executes 'Grid Data Source' action
    *
    * Data source for entry reports
    *
    * @param sfRequest $request A request object
    */
    public function executeGriddatasource(sfWebRequest $request)
    {
            $this->setLayout(false);
    }

  public function executeBatch(sfWebRequest $request)
  {
    if($request->getPostParameter('delete'))
    {
      $prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";
      require($prefix_folder.'includes/init.php');

      require($prefix_folder.'config.php');
      require($prefix_folder.'includes/db-core.php');
      require($prefix_folder.'includes/helper-functions.php');
      require($prefix_folder.'includes/check-session.php');

      require($prefix_folder.'includes/filter-functions.php');
      require($prefix_folder.'includes/theme-functions.php');
      require($prefix_folder.'includes/users-functions.php');

      $dbh = mf_connect_db();
      $mf_settings = mf_get_settings($dbh);

      if(empty($request->getPostParameter('delete'))){
        die("Error! You can't open this file directly");
      }

      $form_id = $request->getPostParameter('delete');

      //check permission, is the user allowed to access this page?
      if(empty($_SESSION['mf_user_privileges']['priv_administer'])){
        $user_perms = mf_get_user_permissions($dbh,$form_id,$_SESSION['mf_user_id']);

        //this page need edit_form permission
        if(empty($user_perms['edit_form'])){
          die("Access Denied. You don't have permission to delete this form.");
        }
      }

      //depends on the config file
      //when deleting the form, we can simply set the status of the form_active to '9' which means deleted
      //or delete the form data completely
      if(MF_CONF_TRUE_DELETE === true){ //true deletion

        //remove from ap_forms
        $query = "delete from ".MF_TABLE_PREFIX."forms where form_id=?";
        $params = array($form_id);
        mf_do_query($query,$params,$dbh);

        //remove from ap_form_elements
        $query = "delete from ".MF_TABLE_PREFIX."form_elements where form_id=?";
        $params = array($form_id);
        mf_do_query($query,$params,$dbh);

        //remove from ap_element_options
        $query = "delete from ".MF_TABLE_PREFIX."element_options where form_id=?";
        $params = array($form_id);
        mf_do_query($query,$params,$dbh);

        //remove from ap_column_preferences
        $query = "delete from ".MF_TABLE_PREFIX."column_preferences where form_id=?";
        $params = array($form_id);
        mf_do_query($query,$params,$dbh);

        //remove from ap_entries_preferences
        $query = "delete from ".MF_TABLE_PREFIX."entries_preferences where form_id=?";
        $params = array($form_id);
        mf_do_query($query,$params,$dbh);

        //remove from ap_form_locks
        $query = "delete from ".MF_TABLE_PREFIX."form_locks where form_id=?";
        $params = array($form_id);
        mf_do_query($query,$params,$dbh);

        //remove from ap_element_prices
        $query = "delete from ".MF_TABLE_PREFIX."element_prices where form_id=?";
        $params = array($form_id);
        mf_do_query($query,$params,$dbh);

        //remove from ap_field_logic_elements table
        $query = "delete from ".MF_TABLE_PREFIX."field_logic_elements where form_id=?";
        $params = array($form_id);
        mf_do_query($query,$params,$dbh);

        //remove from ap_field_logic_conditions table
        $query = "delete from ".MF_TABLE_PREFIX."field_logic_conditions where form_id=?";
        $params = array($form_id);
        mf_do_query($query,$params,$dbh);

        //remove from ap_page_logic table
        $query = "delete from ".MF_TABLE_PREFIX."page_logic where form_id=?";
        $params = array($form_id);
        mf_do_query($query,$params,$dbh);

        //remove from ap_page_logic_conditions table
        $query = "delete from ".MF_TABLE_PREFIX."page_logic_conditions where form_id=?";
        $params = array($form_id);
        mf_do_query($query,$params,$dbh);

        //remove review table
        $query = "drop table if exists `".MF_TABLE_PREFIX."form_{$form_id}_review`";
        $params = array();
        mf_do_query($query,$params,$dbh);

        //remove the actual form table
        $query = "drop table if exists `".MF_TABLE_PREFIX."form_{$form_id}`";
        $params = array();
        mf_do_query($query,$params,$dbh);

        //remove form folder
        @mf_full_rmdir($mf_settings['upload_dir']."/form_{$form_id}");
        if($mf_settings['upload_dir'] != $mf_settings['data_dir']){
          @mf_full_rmdir($mf_settings['data_dir']."/form_{$form_id}");
        }

      }else{ //safe deletion
        $query = "update ".MF_TABLE_PREFIX."forms set form_active=9 where form_id=?";
        $params = array($form_id);
        mf_do_query($query,$params,$dbh);
      }

      //delete entries from ap_permissions table, regardless of the config
      $query = "delete from ".MF_TABLE_PREFIX."permissions where form_id=?";
      $params = array($form_id);
      mf_do_query($query,$params,$dbh);
    }
  }

    /**
    * Executes 'Editform' action
    *
    * Allows for editing of a dynamic form
    *
    * @param sfRequest $request A request object
    */
    public function executeEditform(sfWebRequest $request)
    {
            $this->setLayout("layout-settings");
    }



    /**
     * Executes 'Trasnlateform' action
     *
     * Allows for editing of a dynamic form
     *
     * @param sfRequest $request A request object
     */
    public function executeTranslateform(sfWebRequest $request)
    {
        $this->setLayout("layout-settings");
    }

    /**
     * Executes 'Savetranslate' action
     *
     * Allows for editing of a dynamic form
     *
     * @param sfRequest $request A request object
     */
    public function executeSavetranslate(sfWebRequest $request)
    {
        $form_id = $request->getParameter("id");
        $filter = $request->getParameter("service");

        $translator = new translation();

        $q = Doctrine_Query::create()
            ->from("ExtLocales a")
            ->orderBy("a.local_title ASC");
        $languages = $q->execute();

        //Save Element Translations

        $q = Doctrine_Query::create()
            ->from("ApFormElements a")
            ->where("a.form_id = ?", $form_id)
            ->orderBy("a.element_position ASC");
        $elements = $q->execute();

        foreach($elements as $element)
        {
            foreach($languages as $language)
            {
                //Set the language names
                $lang_values = $request->getPostParameter("locale_".$language->getLocaleIdentifier());

                $tablename = "ap_form_elements";
                $fieldname = "element_title";
                $fieldid = $form_id;
                $optionid = $element->getElementId();
                $translator->setOptionTranslationManual($tablename,$fieldname,$fieldid,$optionid,$lang_values[$element->getElementId()], $language->getLocaleIdentifier());
                ////////////////////////
                 
                //Set the element guidelines
                $guideline_values = $request->getPostParameter("locale_guideline_".$language->getLocaleIdentifier());

                $tablename = "ap_form_elements";
                $fieldname = "element_guidelines";
                $fieldid = $form_id;
                $optionid = $element->getElementId();
                $translator->setOptionTranslationManual($tablename,$fieldname,$fieldid,$optionid,$guideline_values[$element->getElementId()], $language->getLocaleIdentifier());

                //Set the option values
                $option_values = $request->getPostParameter("locale_option_".$element->getElementId()."_".$language->getLocaleIdentifier());

                if($option_values)
                {
                    $q = Doctrine_Query::create()
                        ->from("ApElementOptions a")
                        ->where("a.form_id = ?", $form_id)
                        ->andWhere("a.element_id = ?", $element->getElementId())
                        ->andWhere("a.live = 1");
                    $options = $q->execute();

                    foreach ($options as $option) {
                        $tablename = "ap_element_options";
                        $fieldname = "option";
                        $fieldid = $option->getAeoId();
                        $optionid = $option->getOptionId();
                        $translator->setOptionTranslationManual($tablename,$fieldname,$fieldid,$optionid,$option_values[$option->getAeoId()], $language->getLocaleIdentifier());
                    }
                }
            }
        }

        //Save Logic Translations

        $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

        $sql = "SELECT * FROM ap_field_logic_conditions WHERE form_id = ".$form_id;
        $conditions = mysql_query($sql, $dbconn) or die($sql);

        while($condition = mysql_fetch_assoc($conditions)) {
            foreach($languages as $language) {
                $lang_value = $request->getPostParameter("condition_locale_" . $language->getLocaleIdentifier()."_" . $condition['alc_id']);

                $translator->setTranslationManual("field_logic_conditions", "rule_keyword", $condition['alc_id'], $lang_value, $language->getLocaleIdentifier());
            }
        }
        
        //OTB Patch  
        ////Reconcile for loops in one foreach statement . There is no need of having each 
        foreach($languages as $language) 
         {
            //Form Success Message
             $translator->setTranslationManual("ap_forms", "form_success_message", $form_id, $request->getPostParameter("locale_".$language->getLocaleIdentifier()."_form_success_message_".$language->getLocaleIdentifier()), $language->getLocaleIdentifier());
             //For Form Review Title
             $translator->setTranslationManual("ap_forms", "form_review_title", $form_id, $request->getPostParameter("locale_".$language->getLocaleIdentifier()."_form_review_title_".$language->getLocaleIdentifier()), $language->getLocaleIdentifier());
             //Form Review Description
               $translator->setTranslationManual("ap_forms", "form_review_description", $form_id, $request->getPostParameter("locale_".$language->getLocaleIdentifier()."_form_review_description_".$language->getLocaleIdentifier()), $language->getLocaleIdentifier());
             //Form Description
             $translator->setTranslationManual("ap_forms", "form_description", $form_id, $request->getPostParameter("locale_".$language->getLocaleIdentifier()."_form_description_".$language->getLocaleIdentifier()), $language->getLocaleIdentifier());
             ////Form Name
             $translator->setTranslationManual("ap_forms", "form_name", $form_id, $request->getPostParameter("locale_".$language->getLocaleIdentifier()."_form_name_".$language->getLocaleIdentifier()), $language->getLocaleIdentifier());
             
        }

        $this->redirect("/backend.php/forms/translateform?id=".$form_id."&filter=".$filter);
    }

    public function executeAddfield(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeAddmatrixrow(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeCaptcha(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeChangepaymentstatus(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeChangetheme(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeDisableform(sfWebRequest $request)
    {
        $this->setLayout("layout-settings");
    }

    public function executeEnableform(sfWebRequest $request)
    {
        $this->setLayout("layout-settings");
    }

    public function executeChangeuserstatus(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeClearfilter(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeColumnspreference(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeConfirmembed(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeConfirm(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeDeletedraftfield(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeDeleteentries(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeDeletefileupload(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeDeleteform(sfWebRequest $request)
    {
		$this->id = $request->getParameter("id");
        $this->setLayout("layout-settings");
    }

    public function executeManagereport(sfWebRequest $request)
    {
        $this->id = $request->getParameter("id");
        $this->setLayout("layout-settings");
    }

    public function executeSavewidgetsposition(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeWidget(sfWebRequest $request)
    {
        $this->setLayout(false);
    }


    public function executeAddwidget(sfWebRequest $request)
    {
        $this->setLayout("layout-settings");
    }

    public function executeEditwidget(sfWebRequest $request)
    {
        $this->setLayout("layout-settings");
    }

    public function executeDeletelivefield(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeDeletematrixrow(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeDownload(sfWebRequest $request)
    {
        $this->setLayout(false);
    }


    public function executeDuplicateform(sfWebRequest $request)
    {
        $this->setLayout("layout-settings");
    }

    public function executeEditentry(sfWebRequest $request)
    {
    }

    public function executeEdittheme(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeEmailentry(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeEmbedcode(sfWebRequest $request)
    {
        $this->setLayout("layout-settings");
    }

    public function executeExportentries(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeFormlocked(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeGetfontlist(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeLogicsettings(sfWebRequest $request)
    {
        $this->setLayout("layout-settings");
    }

    public function executeManageentries(sfWebRequest $request)
    {
        $this->setLayout("layout-settings");
    }

    public function executeManageforms(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeNotificationsettings(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executePaymentembed(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executePaymentsettings(sfWebRequest $request)
    {
        //OTB patch - Get Configured Merchants
        $m_query = Doctrine_Query::create()
                ->from('Merchant m')
                ->where('m.status = ? ',1) ; // Get only active merchants
        //
        $this->merchants = $m_query->execute();
        //Select Set settings from app forms
        $form_payments_q = Doctrine_Query::create()
                ->from('ApForms f')
                ->where('f.form_id = ? ',$request->getParameter('id')) ; 
        $results = $form_payments_q->execute();
        $this->merchant = 'Not Set ' ;
        $this->currency = 'Not Set' ;
        //
        foreach($results as $r){
             $this->set_merchant = $r->getPaymentMerchantType();
             $this->set_currency= $r->getPaymentCurrency();
        }
        $this->setLayout("layout-settings");
    }

    public function executePaymentsubmitstripe(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executePayment(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executePaypalipn(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executePing(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeRestricted(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeSaveColumnspreference(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeSavefilter(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeSavelogicsettings(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeSavepaymentsettings(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeSavetags(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeSavetheme(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeSignature(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeStringtotime(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeSynchfields(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeToggleform(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeUploadthemeimages(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeViewentry(sfWebRequest $request)
    {
        $q = Doctrine_Query::create()
        ->from("FormEntry a")
        ->where("a.id = ?", $request->getParameter('entryid'));
   $application = $q->fetchOne();
   error_log("Debug: XXXXXXXXXXXXXXXXXXXXXXXXXXXX");




           foreach($application as $app){


               $approvedstg=$app->getApproved ;

               if($approvedstg!=51 && $approvedstg!=52 && $approvedstg!=53  && $approvedstg!=54){
           //error_log("App Id >>>> ".$application->getId());
           //If application has an unresolved decline entry then redirect and warn the user
           //OTB patch - Commented out as this is still under development
           $q = Doctrine_Query::create()
               ->from("EntryDecline a")
               ->where("a.entry_id = ?", $application->getId())
               ->andWhere("a.resolved = 0");
           $declined = $q->count();

           if($declined > 0)
           {
               
           //  $this->redirect("/backend.php/tasks/view/id/".$this->getUser()->getAttribute("task")."/decline/warning");
               $this->redirect("/backend.php/applications/view/id/".$application->getId()."/declinewarn/1") ; // 
           } 
       }
   }
  
   if(!empty($request->getParameter('moveto')) AND $request->getParameter('moveto') != "")
   {   
       error_log("DEBUG::::::::::::::::::::::::::::::::::::::::::::::::::::::COMPLETING ALL OF USER".$_SESSION["SESSION_CUTEFLOW_USERID"]." TASKS ON: ".$application->getId());
      //Complete all of current user's tasks
       $q = Doctrine_Query::create()
          ->from("Task a")
          ->where("a.owner_user_id = ?", $_SESSION["SESSION_CUTEFLOW_USERID"])
          ->andWhere("a.application_id = ?", $application->getId())
          ->andWhere("a.status = ? OR a.status = ?", array(1, 2));
       $tasks = $q->execute();

       foreach($tasks as $task)
       {
           $task->setStatus(25);
           $task->setEndDate(date('Y-m-d H:i:s'));
           $task->save();
           error_log("DEBUG:::::::::::::::::::::::::::::::::::::::COMPLETED TASK".$task->getId());
       } 
       
       
       //CBS
       //check if there is still any unattended tasks so that we abort them
       $q = Doctrine_Query::create()
       ->from('Task a')
       ->where('a.application_id = ?', $application->getId())
       ->andWhere('a.status = 1 OR a.status = 2 OR a.status = 3 OR a.status = 4 OR a.status = 5');
       $unattended_tasks = $q->execute();
       $number_of_unattended_tasks=$q->count();
       
       error_log("DEBUG:::::::::::::::::::::::::::::::::::::::NUMBER OF UNATTENDED TASKS: ".$number_of_unattended_tasks);
       //if there are still any pending,queued etc tasks, delete them
       if($unattended_tasks && $number_of_unattended_tasks>0){
           
           foreach($unattended_tasks as $u_task){
           error_log("DEBUG:::::::::::::::::::: CANCELLING TASK: ".$u_task->getId()." ON APLLICATION: ".$u_task->getApplicationId()." Status: ".$u_task->getStatus());
           $u_task->setStatus(55);
           $u_task->setEndDate(date('Y-m-d H:i:s'));
           $u_task->save();
       }
   }
   else{
       error_log("DEBUG:::::::::::::::::::: NO PENDING TASKS");

   }

       error_log("Debug: Set Approved >>>>> ".$request->getParameter('moveto')) ;
       $application->setApproved($request->getParameter('moveto'));
       $application->save();
/*
       $q = Doctrine_Query::create()
                ->from('ApplicationReference a')
                ->where('a.application_id = ?', $application->getId())
                ->andWhere('a.end_date = ?', "");
       $oldappref = $q->fetchOne();

       if($oldappref)
       {
               $oldappref->setEndDate(date('Y-m-d H:i:s'));
               $oldappref->save();
       }*/

       $q = Doctrine_Query::create()
        ->from("SubMenus a")
        ->where("a.id = ?", $request->getParameter('moveto'));
      $stage = $q->fetchOne();

       //Save Audit
       $audit = new Audit();
       $audit->saveAudit($application->getId(), "Moved application to ".$stage->getTitle());


       $this->getUser()->setFlash('notice', 'The application <span>'.$application->getApplicationId().'</span> has been Successfully moved and is now in the <span>'.$stage->getTitle().'</span> stage' );
   }
   else {
       error_log("Debug: moveto is null");
   }

   if($this->getUser()->getAttribute("back_to_tasks") == true)
   {
       $this->redirect("/backend.php/dashboard");
   }
   else
   {
       if($this->getUser()->mfHasCredential("accesssubmenu".$application->getApproved())){
           $this->redirect("/backend.php/applications/view/id/" . $application->getId());
       }
       else
       {
           $this->redirect("/backend.php/dashboard");
       }
   }
    }

    public function executeView(sfWebRequest $request)
    {

    }
    /**
     * Executes 'Save' action
     *
     * Save the form
     *
     * @param sfRequest $request A request object
     */
    public function executeSaveform(sfWebRequest $request)
    {
        $this->setLayout(false);
    }


 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeManagerelations(sfWebRequest $request)
  {
	  $this->setLayout("layout");
	  $this->formid = $request->getParameter("id");
  }

  public function executePermitview(sfWebRequest $request)
  {
      $this->layout = Doctrine_Core::getTable("WebLayout")->find($request->getParameter("id"));
  	  $this->setLayout('layout');
  }
  public function executePermitedit(sfWebRequest $request)
  {
      $this->layout = Doctrine_Core::getTable("WebLayout")->find($request->getParameter("id"));
  }
  public function executePermitsave(sfWebRequest $request)
  {
      $layout = Doctrine_Core::getTable("WebLayout")->find($request->getPostParameter("layoutid"));
	  $layout->setTitle($request->getPostParameter("title"));
	  $layout->setContent($request->getPostParameter("content2"));
	  $layout->save();

	  $this->redirect("/backend.php/forms/index");
  }
  public function executePermitlist(sfWebRequest $request)
  {
      $q = Doctrine_Query::create()
	     ->from('WebLayout a');
	  $this->layouts = $q->execute();
  	  $this->setLayout(false);
  }
  public function executeDecline(sfWebRequest $request)
  {
		$this->setLayout('layout-metronic');
		if($request->getPostParameter("reason") && $request->getPostParameter("reason") != "")
		{
			$entry = Doctrine_Core::getTable('FormEntry')->find(array($request->getPostParameter("entryid")));
			if($entry)
			{
				$decline = new EntryDecline();
				$decline->setEntryId($entry->getId());
				$decline->setDescription($request->getPostParameter("reason"));
				$decline->setDeclinedBy($_SESSION["SESSION_CUTEFLOW_USERID"]);
				$decline->setEditFields(json_encode($_POST['edit_fields']));
				$decline->save();
				$this->decline = $decline;



				$entry->setApproved($this->getUser()->getAttribute('moveto', $entry->getApproved()));
				$entry->setDeclined("1");
				$entry->save();


				$appref = new ApplicationReference();
				$appref->setStageId($this->getUser()->getAttribute('moveto', $entry->getApproved()));
				$appref->setApplicationId($entry->getId());
				$appref->setApprovedBy($_SESSION["SESSION_CUTEFLOW_USERID"]);
				$appref->setStartDate(date('Y-m-d'));
				$appref->setEndDate("");
				$appref->save();
                                //OTB patch get user culture
                                $user_culture = $this->getUser()->getCulture() ;

				//Save Audit
				$audit = new Audit();
				$audit->saveAudit($entry->getId(), "Declined an application");

        //Complete all of current user's tasks
        $q = Doctrine_Query::create()
           ->from("Task a")
           ->where("a.owner_user_id = ?", $_SESSION["SESSION_CUTEFLOW_USERID"])
           ->andWhere("a.application_id = ?", $entry->getId())
           ->andWhere("a.status = ? OR a.status = ?", array(1, 2));
        $tasks = $q->execute();

        foreach($tasks as $task)
        {
            $task->setStatus(25);
            $task->save();
        }

				$this->notifier = new notifications($this->getMailer());

        if($this->getUser()->getAttribute("back_to_tasks") == true)
        {
            $this->redirect("/backend.php/dashboard");
            $this->back_to_tasks = true;
        }
			}
		}
		else
		{
			$entry = Doctrine_Core::getTable('FormEntry')->find(array($_GET['entryid']));
			$former_state = $entry->getApproved();
			if($entry)
			{
				$this->entry = $entry;
				$this->getUser()->setAttribute('moveto', $request->getGetParameter("moveto"));


			}
		}
  }
  public function executeReject(sfWebRequest $request)
  {
        $this->setLayout('layout');
        if($request->getPostParameter("reason"))
        {
            $entry = Doctrine_Core::getTable('FormEntry')->find(array($request->getPostParameter("entryid")));
            if($entry)
            {
                $decline = new EntryDecline();
                $decline->setEntryId($entry->getId());
                $decline->setDescription($request->getPostParameter("reason"));
        				$decline->setDeclinedBy($_SESSION["SESSION_CUTEFLOW_USERID"]);
                $decline->save();
                $this->decline = $decline;



                $entry->setApproved($this->getUser()->getAttribute('moveto', $entry->getApproved()));
                $entry->setDeclined("2");
                $entry->save();


                $appref = new ApplicationReference();
                $appref->setStageId($this->getUser()->getAttribute('moveto', $entry->getApproved()));
                $appref->setApplicationId($entry->getId());
                $appref->setApprovedBy($_SESSION["SESSION_CUTEFLOW_USERID"]);
                $appref->setStartDate(date('Y-m-d'));
                $appref->setEndDate("");
                $appref->save();

        $q = Doctrine_Query::create()
           ->from('CfUser a')
           ->where('a.bdeleted = 0');
        $reviewers = $q->execute();
        foreach($reviewers as $reviewer)
        {
            $q = Doctrine_Query::create()
                ->from('mfGuardUserGroup a')
                ->leftJoin('a.Group b')
                ->leftJoin('b.mfGuardGroupPermission c') //Left Join group permissions
                ->leftJoin('c.Permission d') //Left Join permissions
                ->where('a.user_id = ?', $reviewer->getNid())
                ->andWhere('d.name = ?', "accesssubmenu".$entry->getApproved());
            $usergroups = $q->execute();
            if(sizeof($usergroups) > 0)
            {
                $body = "
                Hi ".$reviewer->getStrfirstname()." ".$reviewer->getStrlastname().",</br>
                </br>
                Application ".$entry->getApplicationId()." has been moved to ".$entry->getStatusName().":</br>

                <br>
                Click here to view the application: </br>
                ------- </br>
                <a href='http://".$_SERVER['HTTP_HOST']."/backend.php/applications/view/id/".$entry->getId()."'>Link to ".$entry->getApplicationId()."</a></br>
                ------- </br>

                </br>
                ";

                $mailnotifications = new mailnotifications();
                $mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $reviewer->getStremail(),$entry->getApplicationId()." moved to ".$entry->getStatusName(),$body);
            }
        }

                //Save Audit
                $audit = new Audit();
                $audit->saveAudit($entry->getId(), "Declined an application");

                //Complete all of current user's tasks
                $q = Doctrine_Query::create()
                   ->from("Task a")
                   ->where("a.owner_user_id = ?", $_SESSION["SESSION_CUTEFLOW_USERID"])
                   ->andWhere("a.application_id = ?", $entry->getId())
                   ->andWhere("a.status = ?", 1);
                $tasks = $q->execute();

                foreach($tasks as $task)
                {
                    $task->setStatus(25);
                    $task->save();
                }

                $this->notifier = new notifications($this->getMailer());

                if($this->getUser()->getAttribute("back_to_tasks") == true)
                {
                    $this->redirect("/backend.php/tasks/list");
                    $this->back_to_tasks = true;
                }
            }
        }
        else
        {
            $entry = Doctrine_Core::getTable('FormEntry')->find(array($_GET['entryid']));
            $former_state = $entry->getApproved();
            if($entry)
            {
                $this->entry = $entry;
                $this->getUser()->setAttribute('moveto', $request->getGetParameter("moveto"));


            }
        }
  }
  public function executeApprove(sfWebRequest $request)
  {
    $this->setLayout('layout');

    $entry = Doctrine_Core::getTable('FormEntry')->find(array($request->getParameter("entryid")));
    if($entry)
    {
      $q = Doctrine_Query::create()
         ->from('Permits a')
         ->where("a.applicationform = ?", $entry->getFormId())
         ->andWhere("a.applicationstage = ?", $request->getParameter("moveto"));
      $permittemplates = $q->execute();

      if($permittemplates)
      {
        //continue because it has found a permit already linked to the approved stage
      }
      else
      {
        //assign a default permit because no permit is assigned to this stage
        $q = Doctrine_Query::create()
           ->from('Permits a')
           ->where("a.applicationform = ?", $entry->getFormId())
           ->andWhere("a.applicationstage = ?", $entry->getApproved());
        $permittemplates = $q->execute();
        if($permittemplates)
        {
            //continue
        }
        else
        {
            //Before defaulting to the only permit, check if clone workflows might apply
            $found = false;

            $q = Doctrine_Query::create()
                ->from('SubMenus a')
                ->where("a.id = ?", $request->getParameter("moveto"));
            $next_stage = $q->fetchOne();
            if($next_stage)
            {
                $title = $next_stage->getTitle();
                $q = Doctrine_Query::create()
                    ->from('SubMenus a')
                    ->where("a.title = ?", $title);
                $stages = $q->execute();
                foreach($stages as $stage)
                {
                    $q = Doctrine_Query::create()
                        ->from('Permits a')
                        ->where("a.applicationform = ?", $entry->getFormId())
                        ->andWhere("a.applicationstage = ?", $stage->getId());
                    $permittemplates = $q->execute();

                    if($permittemplates)
                    {
                        //continue because it has found a permit already linked to the approved stage
                        $found = true;
                        break;
                    }
                }
            }

            if($found == false)
            {
                $q = Doctrine_Query::create()
                   ->from('Permits a')
                   ->where("a.applicationform = ?", $entry->getFormId());
                $permittemplates = $q->execute();
            }
        }
      }

      $date_of_response = date("Y-m-d H:i:s");
      $date_of_issue = $date_of_response;

      $entry->setDateOfResponse(date("Y-m-d H:i:s"));
      $entry->setDateOfIssue(date("Y-m-d H:i:s"));

      $q = Doctrine_Query::create()
          ->from("SubMenus a")
          ->where("a.id = ?", $request->getParameter("moveto"));
      $existingmenu = $q->fetchOne();

      if($existingmenu)
      {
        $entry->setApproved($request->getParameter("moveto"));
      }

      $entry->save();

      /**
      $q = Doctrine_Query::create()
         ->from('CfUser a')
         ->where('a.bdeleted = 0');
      $reviewers = $q->execute();
      foreach($reviewers as $reviewer)
      {
          $q = Doctrine_Query::create()
              ->from('mfGuardUserGroup a')
              ->leftJoin('a.Group b')
              ->leftJoin('b.mfGuardGroupPermission c') //Left Join group permissions
              ->leftJoin('c.Permission d') //Left Join permissions
              ->where('a.user_id = ?', $reviewer->getNid())
              ->andWhere('d.name = ?', "accesssubmenu".$entry->getApproved());
          $usergroups = $q->execute();
          if(sizeof($usergroups) > 0)
          {
              $body = "
              Hi ".$reviewer->getStrfirstname()." ".$reviewer->getStrlastname().",</br>
              </br>
              A new application ".$entry->getApplicationId()." has been submitted to ".$entry->getStatusName().":</br>

              <br>
              Click here to view the application: </br>
              ------- </br>
              <a href='http://".$_SERVER['HTTP_HOST']."/backend.php/applications/view/id/".$entry->getId()."'>Link to ".$entry->getApplicationId()."</a></br>
              ------- </br>

              </br>
              ";

              $mailnotifications = new mailnotifications();
              $mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $reviewer->getStremail(),$entry->getApplicationId()." moved to ".$entry->getStatusName(),$body);
          }
      }
      **/
//CBS new
      foreach($permittemplates as $permittemplate ){
        //New code to handle more than one permit per application
        error_log(" MAKE NEW PERMIT************************************************");
        $newpermit = new SavedPermit();
        $newpermit->setTypeId($permittemplate->getId());
        $newpermit->setApplicationId($entry->getId());
        $newpermit->setDateOfIssue($date_of_issue);
  
        $q = Doctrine_Query::create()
           ->from("Permits a")
           ->where("a.applicationform = ?", $entry->getFormId());
        $template = $q->fetchOne();
        if($template)
        {
           if($template->getMaxDuration() > 0)
           {
              $date = strtotime("+".$template->getMaxDuration()." day");
              $newpermit->setDateOfExpiry(date('Y-m-d', $date));
           }
        }
  
        //if permit template has its own unique identifier then assign it
        if($permittemplate->getFooter())
        {error_log("HAS OWN FOOTER************************************************");
           $q = Doctrine_Query::create()
              ->from('SavedPermit a')
              ->where('a.type_id = ?', $permittemplate->getId())
              ->orderBy("a.permit_id DESC");
           $last_permit = $q->fetchOne();
  
           $new_permit_id = ""; //submission identifier
           $identifier_start = ""; //first stage
  
           if($last_permit && $last_permit->getPermitId())
           {
               $new_permit_id = $last_permit->getPermitId();
               $new_permit_id = ++$new_permit_id;
           }
           else
           {
               $new_permit_id = $permittemplate->getFooter();
           }
  
           $newpermit->setPermitId($new_permit_id);
        }
        else
        {error_log("NO FOOTER************************************************");
           $newpermit->setPermitId($entry->getApplicationId());
        }
  
        $newpermit->setCreatedBy($_SESSION["SESSION_CUTEFLOW_USERID"]);
        $newpermit->setLastUpdated($date_of_issue);
        $newpermit->save();
  
        //Save Audit
        $audit = new Audit();
        $audit->saveAudit($entry->getId(), "approved application");
  
        $this->notifier = new notifications($this->getMailer());
  
        //Complete all of current user's tasks
        $q = Doctrine_Query::create()
           ->from("Task a")
           ->where("a.owner_user_id = ?", $_SESSION["SESSION_CUTEFLOW_USERID"])
           ->andWhere("a.application_id = ?", $entry->getId())
           ->andWhere("a.status = ? OR a.status = ?", array(1, 2));
        $tasks = $q->execute();
  
        foreach($tasks as $task)
        {
            $task->setStatus(25);
            $task->save();
        }
      }


      //OTB OLD
/*
      //New code to handle more than one permit per application
      $newpermit = new SavedPermit();
      $newpermit->setTypeId($permittemplate->getId());
      $newpermit->setApplicationId($entry->getId());
      $newpermit->setDateOfIssue($date_of_issue);

      $q = Doctrine_Query::create()
         ->from("Permits a")
         ->where("a.applicationform = ?", $entry->getFormId());
      $template = $q->fetchOne();
      if($template)
      {
         if($template->getMaxDuration() > 0)
         {
            $date = strtotime("+".$template->getMaxDuration()." day");
            $newpermit->setDateOfExpiry(date('Y-m-d', $date));
         }
      }

      //if permit template has its own unique identifier then assign it
      if($permittemplate->getFooter())
      {
         $q = Doctrine_Query::create()
            ->from('SavedPermit a')
            ->where('a.type_id = ?', $permittemplate->getId())
            ->orderBy("a.permit_id DESC");
         $last_permit = $q->fetchOne();

         $new_permit_id = ""; //submission identifier
         $identifier_start = ""; //first stage

         if($last_permit && $last_permit->getPermitId())
         {
             $new_permit_id = $last_permit->getPermitId();
             $new_permit_id = ++$new_permit_id;
         }
         else
         {
             $new_permit_id = $permittemplate->getFooter();
         }

         $newpermit->setPermitId($new_permit_id);
      }
      else
      {
         $newpermit->setPermitId($entry->getApplicationId());
      }

      $newpermit->setCreatedBy($_SESSION["SESSION_CUTEFLOW_USERID"]);
      $newpermit->setLastUpdated($date_of_issue);
      $newpermit->save();

      $q = Doctrine_Query::create()
         ->from('ApplicationReference a')
         ->where('a.application_id = ?', $entry->getId())
         ->andWhere('a.end_date = ?', "");
      $oldappref = $q->fetchOne();

      if($oldappref)
      {
        $oldappref->setEndDate(date('Y-m-d'));
        $oldappref->save();
      }

      $appref = new ApplicationReference();
      $appref->setStageId($request->getParameter("moveto"));
      $appref->setApplicationId($entry->getId());
      $appref->setApprovedBy($_SESSION["SESSION_CUTEFLOW_USERID"]);
      $appref->setStartDate(date('Y-m-d'));
      $appref->setEndDate("");
      $appref->save();

      //Save Audit
      $audit = new Audit();
      $audit->saveAudit($entry->getId(), "approved application");

      $this->notifier = new notifications($this->getMailer());

      //Complete all of current user's tasks
      $q = Doctrine_Query::create()
         ->from("Task a")
         ->where("a.owner_user_id = ?", $_SESSION["SESSION_CUTEFLOW_USERID"])
         ->andWhere("a.application_id = ?", $entry->getId())
         ->andWhere("a.status = ? OR a.status = ?", array(1, 2));
      $tasks = $q->execute();

      foreach($tasks as $task)
      {
          $task->setStatus(25);
          $task->save();
      }*/

      if($this->getUser()->getAttribute("back_to_tasks") == true)
      {
        $this->redirect("/backend.php/dashboard");
      }
      else
      {
          if($this->getUser()->mfHasCredential("accesssubmenu".$entry->getApproved())){
              $this->redirect("/backend.php/applications/view/id/" . $entry->getId());
          }
          else
          {
              $this->redirect("/backend.php/dashboard");
          }
      }
    }
  }
  public function executeDecline2(sfWebRequest $request)
  {
		$this->setLayout('layout');
		if($request->getPostParameter("reason"))
		{
			$entry = Doctrine_Core::getTable('FormEntry')->find(array($request->getPostParameter("entryid")));
			if($entry)
			{
				$decline = new EntryDecline();
				$decline->setEntryId($entry->getId());
				$decline->setDescription($request->getPostParameter("reason"));
				$decline->setDeclinedBy($_SESSION["SESSION_CUTEFLOW_USERID"]);
				$decline->save();
				$this->decline = $decline;


				$entry->setApproved($this->getUser()->getAttribute('moveto', $entry->getApproved()));
				$entry->setDeclined("1");
				$entry->save();

				$q = Doctrine_Query::create()
					 ->from('ApplicationReference a')
					 ->where('a.application_id = ?', $entry->getId())
					 ->andWhere('a.end_date = ?', "");
				$oldappref = $q->fetchOne();

				if($oldappref)
				{
					$oldappref->setEndDate(date('Y-m-d'));
					$oldappref->save();
				}

				$appref = new ApplicationReference();
				$appref->setStageId($this->getUser()->getAttribute('moveto', $entry->getApproved()));
				$appref->setApplicationId($entry->getId());
				$appref->setApprovedBy($_SESSION["SESSION_CUTEFLOW_USERID"]);
				$appref->setStartDate(date('Y-m-d'));
				$appref->setEndDate("");
				$appref->save();

				//Save Audit
				$audit = new Audit();
				$audit->saveAudit($entry->getId(), "rejected_request");

                                //Complete all of current user's tasks
                                $q = Doctrine_Query::create()
                                   ->from("Task a")
                                   ->where("a.owner_user_id = ?", $_SESSION["SESSION_CUTEFLOW_USERID"])
                                   ->andWhere("a.application_id = ?", $entry->getId())
                                   ->andWhere("a.status = ?", 1);
                                $tasks = $q->execute();

                                foreach($tasks as $task)
                                {
                                    $task->setStatus(25);
                                    $task->save();
                                }

				$this->notifier = new notifications($this->getMailer());
			}
		}
		else
		{
			$entry = Doctrine_Core::getTable('FormEntry')->find(array($_GET['entryid']));
			$former_state = $entry->getApproved();
			if($entry)
			{
				$this->entry = $entry;
				$this->getUser()->setAttribute('moveto', $request->getGetParameter("moveto"));
			}
        }
        $this->setLayout('layout-metronic');

  }
  public function executeManageform(sfWebRequest $request)
  {
     if($request->getParameter("delete"))
     {
		$this->deleteform = $request->getParameter("delete");
     }
	 if($request->getParameter("duplicate"))
     {
		$this->duplicate = $request->getParameter("duplicate");
     }
     $this->setLayout("layout");
  }
  public function executeAjaxformactivation(sfWebRequest $request)
  {

  }
  public function executeFetchrelates(sfWebRequest $request)
  {
	$this->setLayout(false);
  }
  public function executeSaverelates(sfWebRequest $request)
  {
	  $this->formidd = $request->getPostParameter("formid");
  }

  public function executeSearchentries(sfWebRequest $request)
  {

      $this->module = $request->getParameter('module');
      $this->action = $request->getParameter('action');
  }
  public function executeEditcss(sfWebRequest $request)
  {

  }
  public function executeEmailsettings(sfWebRequest $request)
  {
	  $this->formid = $request->getParameter("id");
  }
  public function executeViewentry2(sfWebRequest $request)
  {
	  $this->setLayout("layout");

      $this->module = $request->getParameter('module');
      $this->action = $request->getParameter('action');

	  $this->notifier = new notifications($this->getMailer());
  }
  public function executeTemplatevariables(sfWebRequest $request)
  {

  }
  public function executeEmbed(sfWebRequest $request)
  {
        $this->setLayout(false);
  }
  public function executeDeleteelementoption(sfWebRequest $request)
  {

  }
  public function executeDeleteelement(sfWebRequest $request)
  {

  }
  public function executeDeletewidget(sfWebRequest $request)
  {
      $this->setLayout(false);
  }
  public function executeSharereport(sfWebRequest $request)
  {
      $this->setLayout(false);
  }
  public function executeUnsharereport(sfWebRequest $request)
  {
      $this->setLayout(false);
  }
  public function executeReport(sfWebRequest $request)
  {
      $this->setLayout(false);
  }
  public function executeWidgetcode(sfWebRequest $request)
  {

  }
  public function executeSavewidgetsettings(sfWebRequest $request)
  {
      $this->setLayout(false);
  }
}
