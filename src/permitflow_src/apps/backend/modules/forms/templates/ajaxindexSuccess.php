<?php
use_helper("I18N");

$audit = new Audit();
$audit->saveAudit("", "Accessed form settings");

if($sf_user->mfHasCredential("manageforms"))
{
  $_SESSION['current_module'] = "forms";
  $_SESSION['current_action'] = "index";
  $_SESSION['current_id'] = "";
?>
<?php
    $prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";

    require($prefix_folder.'includes/init.php');

    require($prefix_folder.'config.php');
    require($prefix_folder.'includes/db-core.php');
    require($prefix_folder.'includes/helper-functions.php');
    require($prefix_folder.'includes/check-session.php');
    require($prefix_folder.'includes/users-functions.php');

    $dbh 		 = mf_connect_db();

    $mf_settings = mf_get_settings($dbh);
    $selected_form_id = (int) $_GET['id'];

    $user_permissions = mf_get_user_permissions_all($dbh,$_SESSION['mf_user_id']);

    if(!empty($_GET['hl'])){
        $highlight_selected_form_id = true;
    }else{
        $highlight_selected_form_id = false;
    }

    //determine the sorting order
    $form_sort_by = 'date_created'; //the default sort order

    if(!empty($_GET['sortby'])){
        $form_sort_by = strtolower(trim($_GET['sortby'])); //the user select a new sort order

        //save the sort order into ap_form_sorts table
        $query = "delete from ".MF_TABLE_PREFIX."form_sorts where user_id=?";
        $params = array($_SESSION['mf_user_id']);
        mf_do_query($query,$params,$dbh);

        $query = "insert into ".MF_TABLE_PREFIX."form_sorts(user_id,sort_by) values(?,?)";
        $params = array($_SESSION['mf_user_id'],$form_sort_by);
        mf_do_query($query,$params,$dbh);

    }else{ //load the previous saved sort order

        $query = "select sort_by from ".MF_TABLE_PREFIX."form_sorts where user_id=?";
        $params = array($_SESSION['mf_user_id']);

        $sth = mf_do_query($query,$params,$dbh);
        $row = mf_do_fetch_result($sth);
        if(!empty($row)){
            $form_sort_by = $row['sort_by'];
        }
    }

    $query_order_by_clause = '';

    if($form_sort_by == 'form_title'){
        $query_order_by_clause = " ORDER BY form_id DESC";
        $sortby_title = 'Form Title';
    }else if($form_sort_by == 'form_tags'){
        $query_order_by_clause = " ORDER BY form_tags ASC";
        $sortby_title = 'Form Tags';
    }else if($form_sort_by == 'today_entries'){
        $sortby_title = "Today's Entries";


    }else if($form_sort_by == 'total_entries'){
        $sortby_title = "Total Entries";


    }else{ //the default date created sort
        $query_order_by_clause = " ORDER BY form_id ASC";
        $sortby_title = "Date Created";
    }

    //the number of forms being displayed on each page
    $rows_per_page = $mf_settings['form_manager_max_rows'];

    //get the list of the form, put them into array
    $query = "SELECT
                        form_name,
                        form_description,
                        form_code,
                        form_id,
                        form_tags,
                        form_active,
                        form_disabled_message,
                        form_theme_id
                    FROM
                        ".MF_TABLE_PREFIX."forms
                    WHERE
                        form_active=1 AND form_id > 17
                        {$query_order_by_clause}";
    $params = array();
    $sth = mf_do_query($query,$params,$dbh);

    $form_list_array = array();
    $i=0;
    while($row = mf_do_fetch_result($sth)){

        //check user permission to this form
        if(empty($_SESSION['mf_user_privileges']['priv_administer']) && empty($user_permissions[$row['form_id']])){
            continue;
        }

        $form_list_array[$i]['form_id']   	  = $row['form_id'];

        if(!empty($row['form_name'])){
            $form_list_array[$i]['form_name'] = str_replace('&nbsp;','',$row['form_name']);
        }else{
            $form_list_array[$i]['form_name'] = '-Untitled Form- (#'.$row['form_id'].')';
        }

        $form_list_array[$i]['form_description']   		= $row['form_description'];
        $form_list_array[$i]['form_code']        = $row['form_code'];
        $form_list_array[$i]['form_active']   			= $row['form_active'];
        $form_list_array[$i]['form_disabled_message']   = $row['form_disabled_message'];
        $form_list_array[$i]['form_theme_id'] 			= $row['form_theme_id'];

        $form_disabled_message = json_encode($row['form_disabled_message']);
        $jquery_data_code .= "\$('#liform_{$row['form_id']}').data('form_disabled_message',{$form_disabled_message});\n";

        //get todays entries count
        $sub_query = "select count(*) today_entry from `".MF_TABLE_PREFIX."form_{$row['form_id']}` where `status`=1 and date_created >= date_format(curdate(),'%Y-%m-%d 00:00:00') ";
        $sub_sth = mf_do_query($sub_query,array(),$dbh);
        $sub_row = mf_do_fetch_result($sub_sth);

        $form_list_array[$i]['today_entry'] = $sub_row['today_entry'];

        //get latest entry timing
        if(!empty($sub_row['today_entry'])){
            $sub_query = "select date_created from `".MF_TABLE_PREFIX."form_{$row['form_id']}` order by id desc limit 1";
            $sub_sth = mf_do_query($sub_query,array(),$dbh);
            $sub_row = mf_do_fetch_result($sub_sth);

            $form_list_array[$i]['latest_entry'] = mf_relative_date($sub_row['date_created']);
        }

        //get total entries count
        if($form_sort_by == 'total_entries'){
            $sub_query = "select count(*) total_entry from `".MF_TABLE_PREFIX."form_{$row['form_id']}` where `status`=1";
            $sub_sth = mf_do_query($sub_query,array(),$dbh);
            $sub_row = mf_do_fetch_result($sub_sth);

            $form_list_array[$i]['total_entry'] = $sub_row['total_entry'];
        }


        //get form tags and split them into array
        if(!empty($row['form_tags'])){
            $form_tags_array = explode(',',$row['form_tags']);
            array_walk($form_tags_array, 'mf_trim_value');
            $form_list_array[$i]['form_tags'] = $form_tags_array;
        }

        $i++;
    }


    if($form_sort_by == 'today_entries'){
        usort($form_list_array, 'sort_by_today_entry');
    }

    if($form_sort_by == 'total_entries'){
        usort($form_list_array, 'sort_by_total_entry');
    }


    if(empty($selected_form_id)){ //if there is no preference for which form being displayed, display the first form
        $selected_form_id = $form_list_array[0]['form_id'];
    }

    $selected_page_number = 1;

    //build pagination markup
    $total_rows = count($form_list_array);
    $total_page = ceil($total_rows / $rows_per_page);

    if($total_page > 1){

        $start_form_index = 0;
        $pagination_markup = '<ul id="mf_pagination" class="pages green small">'."\n";

        for($i=1;$i<=$total_page;$i++){

            //attach the data code into each pagination button
            $end_form_index = $start_form_index + $rows_per_page;
            $liform_ids_array = array();

            for ($j=$start_form_index;$j<$end_form_index;$j++) {
                if(!empty($form_list_array[$j]['form_id'])){
                    $liform_ids_array[] = '#liform_'.$form_list_array[$j]['form_id'];

                    //put the page number into the array
                    $form_list_array[$j]['page_number'] = $i;

                    //we need to determine on which page the selected_form_id being displayed
                    if($selected_form_id == $form_list_array[$j]['form_id']){
                        $selected_page_number = $i;
                    }
                }
            }

            $liform_ids_joined = implode(',',$liform_ids_array);
            $start_form_index = $end_form_index;

            $jquery_data_code .= "\$('#pagebtn_{$i}').data('liform_list','{$liform_ids_joined}');\n";


            if($i == $selected_page_number){
                if($selected_page_number > 1){
                    $pagination_markup = str_replace('current_page','',$pagination_markup);
                }

                $pagination_markup .= '<li id="pagebtn_'.$i.'" class="page current_page">'.$i.'</li>'."\n";
            }else{
                $pagination_markup .= '<li id="pagebtn_'.$i.'" class="page">'.$i.'</li>'."\n";
            }

        }

        $pagination_markup .= '</ul>';
    }else{
        //if there is only 1 page, set the page_number property for each form to 1
        foreach ($form_list_array as $key=>$value){
            $form_list_array[$key]['page_number'] = 1;
        }
    }

    //get the available tags
    $query = "select form_tags from ".MF_TABLE_PREFIX."forms where form_tags is not null and form_tags <> ''";
    $params = array();

    $sth = mf_do_query($query,$params,$dbh);
    $raw_tags = array();
    while($row = mf_do_fetch_result($sth)){
        $raw_tags = array_merge(explode(',',$row['form_tags']),$raw_tags);
    }

    $all_tagnames = array_unique($raw_tags);
    sort($all_tagnames);

    $jquery_data_code .= "\$('#dialog-enter-tagname-input').data('available_tags',".json_encode($all_tagnames).");\n";

    //get the available custom themes
    if(!empty($_SESSION['mf_user_privileges']['priv_administer'])){
        $query = "SELECT theme_id,theme_name FROM ".MF_TABLE_PREFIX."form_themes WHERE theme_built_in=0 and status=1 ORDER BY theme_name ASC";
        $params = array();
    }else{
        $query = "SELECT
                            theme_id,
                            theme_name
                        FROM
                            ".MF_TABLE_PREFIX."form_themes
                       WHERE
                            (theme_built_in=0 and status=1 and user_id=?) OR
                            (theme_built_in=0 and status=1 and user_id <> ? and theme_is_private=0)
                    ORDER BY
                            theme_name ASC";
        $params = array($_SESSION['mf_user_id'],$_SESSION['mf_user_id']);
    }

    $sth = mf_do_query($query,$params,$dbh);

    $theme_list_array = array();
    while($row = mf_do_fetch_result($sth)){
        $theme_list_array[$row['theme_id']] = htmlspecialchars($row['theme_name']);
    }

    //get built-in themes
    $query = "SELECT theme_id,theme_name FROM ".MF_TABLE_PREFIX."form_themes WHERE theme_built_in=1 and status=1 ORDER BY theme_name ASC";

    $params = array();
    $sth = mf_do_query($query,$params,$dbh);

    $theme_builtin_list_array = array();
    while($row = mf_do_fetch_result($sth)){
        $theme_builtin_list_array[$row['theme_id']] = htmlspecialchars($row['theme_name']);
    }


    $current_nav_tab = 'manage_forms';
    ?>

<div class="panel panel-dark">
<div class="panel-heading">
			<h3 class="panel-title"><?php echo __('Forms'); ?></h3>
				<div class="pull-right">
            <a class="btn btn-primary-alt settings-margin42" id="newforms" href="#"><?php echo __('New Form'); ?></a>

            <script language="javascript">
            jQuery(document).ready(function(){
              $( "#newforms" ).click(function() {
                  $("#contentload").load("<?php echo public_path(); ?>backend.php/forms/ajaxeditform");
              });
            });
            </script>
</div>
</div>


<div class="panel panel-body panel-body-nopadding ">

<div class="table-responsive">
<table class="table dt-on-steroids mb0" id="table3">
    <thead>
   <tr>
      <th class="no-sort"><input type='checkbox' name='batchall' onclick="boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.name == 'batch') { if(this.checked == true){ box.checked = true; }else{ box.checked = false; } } } "></th>
      <th width="60">#</th>
      <th><?php echo __('Title'); ?></th>
      <th class="no-sort"><?php echo __('Product/Service Code'); ?></th>
      <th class="no-sort" width="250"><?php echo __('Actions'); ?></th>
    </tr>
  </thead>
  <tbody>
                <?php

                $row_num = 1;

                foreach ($form_list_array as $form_data){
                    $form_name   	 = htmlspecialchars($form_data['form_name']);
                    $form_description = htmlspecialchars($form_data['form_description']);
                    $form_code       = htmlspecialchars($form_data['form_code']);
                    $form_id   	 	 = $form_data['form_id'];
                    $today_entry 	 = $form_data['today_entry'];
                    $total_entry 	 = $form_data['total_entry'];
                    $latest_entry 	 = $form_data['latest_entry'];
                    $theme_id		 = (int) $form_data['form_theme_id'];

                    if(!empty($form_data['form_tags'])){
                        $form_tags_array = array_reverse($form_data['form_tags']);
                    }else{
                        $form_tags_array = array();
                    }


                    $form_class = array();
                    $form_class_tag = '';

                    if($form_id == $selected_form_id){
                        $form_class[] = 'form_selected';
                    }

                    if(empty($form_data['form_active'])){
                        $form_class[] = 'form_inactive';
                    }

                    if($selected_page_number == $form_data['page_number']){
                        $form_class[] = 'form_visible';
                    }

                    $form_class_joined = implode(' ',$form_class);
                    $form_class_tag	   = 'class="'.$form_class_joined.'"';


                    ?>
					<tr id="row_<?php echo $form_id; ?>">
	 					<td><input type='checkbox' name='batch' id='batch_<?php echo $form_id; ?>' value='<?php echo $form_id; ?>'></td>
						<td>
						<?php echo $form_id; ?>
						</td>
						<td>
            <?php
            $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
            mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
            $sql = "SELECT * FROM ext_translations WHERE field_id = ".$form_id." AND field_name = 'form_name' AND table_class = 'ap_forms' AND locale = '".$sf_user->getCulture()."'";
            $translation_sth = mysql_query($sql,$dbconn);
            $translation_row = mysql_fetch_assoc($translation_sth);
            if($translation_row)
            {
              $form_name = $translation_row['trl_content'];
            }
            echo $form_name;
            ?>
						</td>
                        <td>
                        <?php
                            echo $form_code;
                        ?>
                        </td>
					   <td>
                        <?php
                            $q = Doctrine_Query::create()
                                ->from("FormEntry a")
                                ->where("a.form_id = ?", $form_id);
                            $applications =  $q->execute();

                            $q = Doctrine_Query::create()
                               ->from("TaskForms a")
                               ->where("a.form_id = ?", $form_id);
                            $taskforms = $q->execute();

                            $q = Doctrine_Query::create()
                               ->from("MfUserProfile a")
                               ->where("a.form_id = ?", $form_id);
                            $userforms = $q->execute();

                        if($sf_user->mfHasCredential("manageforms") && sizeof($applications) <= 0 && sizeof($taskforms) <= 0 && sizeof($userforms) <= 0 && $form_id >17)
                        {
                        ?>
                            <a title="<?php echo __('Delete Form'); ?>" id="delete<?php echo $form_id; ?>" href="#"> <span class="badge badge-primary"><i class="fa fa-trash-o"></i></span></a>
                        <?php
                        }
                        ?>

                        <?php if($sf_user->mfHasCredential("manageforms")){ ?>
                                <a title="<?php echo __('Duplicate Form'); ?>" id="duplicate<?php echo $form_id; ?>" href="#"> <span class="badge badge-primary"><i class="fa fa-paste"></i></span></a>
                        <?php } ?>

                        <?php if($sf_user->mfHasCredential("manageforms")){ ?>
                                <a title="<?php echo __('Edit Form'); ?>" id="edit<?php echo $form_id; ?>" href="#"><span class="badge badge-primary"><i class="fa fa-pencil"></i></span></a>
                        <?php } ?>

                        <?php if($sf_user->mfHasCredential("manageforms")){ ?>
                                <?php
                                if(empty($form_data['form_active'])){
                                    //echo '<a title="Enable Form" id="enable'.$form_id.'" href="#"><span class="badge badge-primary"><i class="fa fa-check-square"></i></span></a>';
                                }else{
                                    //echo '<a title="Disable Form" id="disable'.$form_id.'" href="#"><span class="badge badge-primary"><i class="fa fa-power-off"></i></span></a>';
                                }
                                ?>
                        <?php } ?>

    <script language="javascript">
    jQuery(document).ready(function(){
      $( "#edit<?php echo $form_id; ?>" ).click(function() {
          $("#contentload").load("<?php echo public_path(); ?>backend.php/forms/ajaxeditform?id=<?php echo $form_id; ?>");
      });
      $( "#duplicate<?php echo $form_id; ?>" ).click(function() {
          $("#contentload").load("<?php echo public_path(); ?>backend.php/forms/ajaxduplicateform?id=<?php echo $form_id; ?>");
      });
      $( "#delete<?php echo $form_id; ?>" ).click(function() {
        if(confirm('Are you sure you want to delete this form?')){
          $("#contentload").load("<?php echo public_path(); ?>backend.php/forms/ajaxdeleteform/id/<?php echo $form_id; ?>");
        }
        else
        {
          return false;
        }
      });
    });
    </script>
						</td>
                    </tr>

                    <?php
                    $row_num++;
                }//end foreach $form_list_array
                ?>

</tbody>
	<tfoot>
   <tr><td colspan='7' style='text-align: left;'>
   <select id='batch_action' name='batch_action' onChange="if(this.value != ''){if(confirm('Are you sure?')){ batch('forms', this.options[this.selectedIndex].text, this.value); document.getElementById('default').selected='selected'; }}">
   <option id='default' value=''><?php echo __('Choose an action'); ?>..</option>
   <option value='delete'><?php echo __('Set As Deleted'); ?></option>
   </select>
   </td></tr>
   </tfoot>
</table>
</div>
</div><!--panel-body-->
</div><!--panel-dark-->
<script>
  jQuery(document).ready(function() {
      jQuery('#table3').dataTable({
          "sPaginationType": "full_numbers",

          // Using aoColumnDefs
          "aoColumnDefs": [
          	{ "bSortable": false, "aTargets": [ 'no-sort' ] }
        	]
        });
    });
</script>

<?php
}
else
{
  include_partial("settings/accessdenied");
}
?>
