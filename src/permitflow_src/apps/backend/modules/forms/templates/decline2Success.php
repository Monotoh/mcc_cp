<?php
use_helper("I18N");
?>
<div class="pageheader">
   <h2><i class="fa fa-home"></i> <?php echo __("Application"); ?> <span><?php echo __("Decline"); ?></span></h2>
   <div class="breadcrumb-wrapper">
     <span class="label"><?php echo __("You are here"); ?>:</span>
     <ol class="breadcrumb">
       <li><a href="<?php echo public_path(); ?>backend.php"><?php echo __("Home"); ?></a></li>
       <li class="active"><?php echo __("Applications"); ?></li>
     </ol>
   </div>
 </div>

<div class="contentpanel">
<div class="row">
  <div class="panel panel-dark">
  <div class="panel-heading">
    <h3 class="panel-title"><?php echo __("Decline Application"); ?></h3>
    <p><?php echo __("Send back for corrections"); ?></p>
  </div>
  <div class="panel-body panel-body-nopadding">
    <div class="col-md-12">
       <br>
       <h1><?php echo __("Send Application Back To User"); ?></h1>
       <br>
        <?php
        if($decline)
        {
           echo __("The application has been sent to the user for re-submission");
        }
        else
        {
          ?>
          <form action="/backend.php/forms/decline" method="POST"  autocomplete="off" data-ajax="false">
            <fieldset>
              <input type="hidden" name="entryid" value="<?php echo $entry->getId(); ?>">
              <label>
              <?php echo __("Reason"); ?>:
              </label>
              <div><textarea class="form-control" name="reason" cols="50" rows="10" required></textarea></div>
              </fieldset>
              <fieldset>
              <section>
                <div>
                  <button class="btn btn-default" type="submit" class="submit"><?php echo __("Send Comments"); ?></button>
                </div>
              </section>
            </fieldset>
          </form>
          <?php
        }
        ?>
        </div>
  		</div>
	</div>
</div>
</div>
