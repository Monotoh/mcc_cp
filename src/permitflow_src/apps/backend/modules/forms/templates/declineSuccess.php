<?php
use_helper("I18N");

//ini_set('display_errors', '1');     # don't show any errors...
//error_reporting(E_ALL | E_STRICT);

?>
<div class="pageheader">
   <h2><i class="fa fa-home"></i> <?php echo __("Application"); ?> <span><?php echo __("Decline"); ?></span></h2>
   <div class="breadcrumb-wrapper">
     <span class="label"><?php echo __("You are here"); ?>:</span>
     <ol class="breadcrumb">
       <li><a href="<?php echo public_path(); ?>backend.php"><?php echo __("Home"); ?></a></li>
       <li class="active"><?php echo __("Applications"); ?></li>
     </ol>
   </div>
 </div>

<div class="contentpanel">
<div class="panel panel-dark">
<div class="panel-heading">
  <h3 class="panel-title"><?php echo __("Decline Application"); ?></h3>
  <p><?php echo __("Send back for corrections"); ?></p>
  <div class="pull-right">

  </div>
 </div>
<div class="panel-body panel-body-nopadding">
      <div class="col-md-12">

<?php
if($decline)
{
  $q = Doctrine_Query::create()
     ->from("FormEntry a")
     ->where("a.id = ?", $_POST['entryid']);
  $application = $q->fetchOne();
  error_log("Application id ".$application->getId()) ;
  if($application)
  {
   ?>
   <div align='center'>
   <h4 class="mt20"><?php echo __("The application has been sent to the user for re-submission"); ?><h4>

   <button type="button" class="btn btn-default" onClick="window.location='/backend.php/applications/view/id/<?php echo $application->getId() ?>';"><?php echo __("Back To Application"); ?></button>
   <?php
        if($back_to_tasks == true)
        {
            ?>
            <button type="button" class="btn btn-default" OnClick="window.location='<?php echo public_path(); ?>backend.php/tasks/list';"><?php echo __("List Of Pending Tasks"); ?></button>
            <?php
        }
        else
        {
            ?>
            <button type="button" class="btn btn-default" OnClick="window.location='<?php echo public_path(); ?>backend.php/applications/listgroup/subgroup/<?php echo $application->getApproved() ?>';"><?php echo __("List Of Applications"); ?></button>
            <?php
        }
    }
	?>
   <br>
   <br>
   </div>
   <?php

}
else
{
?>
<form action="/backend.php/forms/decline" method="POST"  autocomplete="off" data-ajax="false">
  <fieldset>
  <input type="hidden" name="entryid" value="<?php echo $entry->getId(); ?>">
    <label>
      <h4><?php echo __("Reason"); ?>:</h4>
    </label>
    <div>
      <textarea class="form-control" name="reason" cols="50" rows="10" required></textarea>
    </div>
  </fieldset>
  <fieldset>
  <fieldset>
    <input type="hidden" name="entryid" value="<?php echo $entry->getId(); ?>">
    <label>
    <h4><?php echo __("Select the fields you want the client to edit"); ?>:</h4>
    </label>
      <div>
        <?php
          $q = Doctrine_Query::create()
            ->from("ApFormElements a")
            ->where("a.form_id = ?", $entry->getFormId())
            ->andWhere("a.element_status = ?", 1)
            ->orderBy("a.element_position ASC");
          $fields = $q->execute();

          foreach($fields as $field)
          {
            if($field->getElementType() == "section")
            {
              ?>
              <h4><?php  echo $field->getElementTitle(); ?></h4>
              <?php
            }
            else {
              
                //OTB patch add support for of translations 
                //Test for existance of translation
                 $translation = new Translation();
                 //get form id
               //  $form_id = $translation->getFieldFormId('ap_form_elements','element_title',$field->getElementId()) ;
                 
                 if($translation->getFieldTranslation('ap_form_elements','element_title',$entry->getFormId(),$field->getElementId()) && $translation->getFieldTranslation('ap_form_elements','element_title',$entry->getFormId(),$field->getElementId()) != "") {
                  ?>            
              <div class="checkbox block"><label><input type="checkbox" name="edit_fields[]" value="<?php echo $field->getElementId(); ?>"> <?php echo $translation->getFieldTranslation('ap_form_elements','element_title',$entry->getFormId(),$field->getElementId()); ?></label></div>
              <?php
                 }
                 else { ?>
                     <div class="checkbox block"><label><input type="checkbox" name="edit_fields[]" value="<?php echo $field->getElementId(); ?>"> <?php echo $field->getElementTitle(); ?></label></div>
                <?php }
               }
          }
        ?>
      </div>
  </fieldset>
  <fieldset>
    <section>
      <div>
        <button id="sendComments" class="btn btn-default mt10" type="submit" class="submit"><?php echo __("Send Comments"); ?></button>
      </div>
    </section>
  </fieldset>
</form>

  <script type="text/javascript">
  $(document).ready(function () {
      $('#sendComments').click(function() {
        checked = $("input[type=checkbox]:checked").length;

        if(!checked) {
          alert("You must check at least one checkbox.");
          return false;
        }

      });
  });

  </script>
<br>
<br>
<?php
}
?>


</div>
</div>
