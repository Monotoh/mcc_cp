<?php
/********************************************************************************
 MachForm
  
 Copyright 2007-2012 Appnitro Software. This code cannot be redistributed without
 permission from http://www.appnitro.com/
 
 More info at: http://www.appnitro.com/
 ********************************************************************************/
    $prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";
    require($prefix_folder.'includes/init.php');

	require($prefix_folder.'config.php');
	require($prefix_folder.'includes/db-core.php');
	require($prefix_folder.'includes/helper-functions.php');
	
	//get query string and parse it, query string is base64 encoded
	$query_string = trim($_GET['q']);
	parse_str(base64_decode($query_string),$params);
	
	$form_id 	= $params['form_id'];
	$id      	= $params['id'];
	$field_name = $params['el'];
	$file_hash  = $params['hash'];
	$element_mark_file_with_qr_code  = $params['element_mark_file_with_qr_code'];//OTB Africa Add QR on attachments
	$element_file_qr_all_pages  = $params['element_file_qr_all_pages'];//OTB Africa Add QR on attachments
	$element_file_qr_page_position  = $params['element_file_qr_page_position'];//OTB Africa Add QR on attachments
	
	$dbh = mf_connect_db();
	$mf_settings = mf_get_settings($dbh);

	//get filename
	$query 	= "select {$field_name} from `".MF_TABLE_PREFIX."form_{$form_id}` where id=?";
	$params = array($id);

	$sth = mf_do_query($query,$params,$dbh);
	$row = mf_do_fetch_result($sth);

	$filename_array  = array();
	$filename_array  = explode('|',$row[$field_name]);
				
	$filename_md5_array = array();
	foreach ($filename_array as $value) {
		$filename_md5_array[] = md5($value);
	}

	$file_key = array_keys($filename_md5_array,$file_hash);
	if(empty($file_key)){
		die("Error. File Not Found!");
	}else{
		$file_key = $file_key[0];
	}
	
	$complete_filename = $filename_array[$file_key];
	
	//remove the element_x-xx- suffix we added to all uploaded files
	$file_1 	   	= substr($complete_filename,strpos($complete_filename,'-')+1);
	$filename_only 	= substr($file_1,strpos($file_1,'-')+1);
	 
	$target_file = $mf_settings['upload_dir']."/form_{$form_id}/files/{$complete_filename}";
	
	if(file_exists($target_file) and !$element_mark_file_with_qr_code){//OTB Africa Add QR on attachments
		//prompt user to download the file
		
		// Get extension of requested file
        $extension = strtolower(substr(strrchr($filename_only, "."), 1));
        
        // Determine correct MIME type
        switch($extension){
 				case "asf":     $type = "video/x-ms-asf";                break;
                case "avi":     $type = "video/x-msvideo";               break;
                case "bin":     $type = "application/octet-stream";      break;
                case "bmp":     $type = "image/bmp";                     break;
                case "cgi":     $type = "magnus-internal/cgi";           break;
                case "css":     $type = "text/css";                      break;
                case "dcr":     $type = "application/x-director";        break;
                case "dxr":     $type = "application/x-director";        break;
                case "dll":     $type = "application/octet-stream";      break;
                case "doc":     $type = "application/msword";            break;
                case "exe":     $type = "application/octet-stream";      break;
                case "gif":     $type = "image/gif";                     break;
				case "gtar":    $type = "application/x-gtar";            break;
				case "gz":      $type = "application/gzip";              break;
				case "htm":     $type = "text/html";                     break;
				case "html":    $type = "text/html";                     break;
				case "iso":     $type = "application/octet-stream";      break;
				case "jar":     $type = "application/java-archive";      break;
				case "java":    $type = "text/x-java-source";            break;
				case "jnlp":    $type = "application/x-java-jnlp-file";  break;
				case "js":      $type = "application/x-javascript";      break;
				case "jpg":     $type = "image/jpeg";                    break;
				case "jpe":     $type = "image/jpeg";                    break;
				case "jpeg":    $type = "image/jpeg";                    break;
				case "lzh":     $type = "application/octet-stream";      break;
				case "mdb":     $type = "application/mdb";               break;
				case "mid":     $type = "audio/x-midi";                  break;
				case "midi":    $type = "audio/x-midi";                  break;
				case "mov":     $type = "video/quicktime";               break;
				case "mp2":     $type = "audio/x-mpeg";                  break;
				case "mp3":     $type = "audio/mpeg";                    break;
				case "mpg":     $type = "video/mpeg";                    break;
				case "mpe":     $type = "video/mpeg";                    break;
				case "mpeg":    $type = "video/mpeg";                    break;
				case "pdf":     $type = "application/pdf";               break;
				case "php":     $type = "application/x-httpd-php";       break;
				case "php3":    $type = "application/x-httpd-php3";      break;
				case "php4":    $type = "application/x-httpd-php";       break;
				case "png":     $type = "image/png";                     break;
				case "ppt":     $type = "application/mspowerpoint";      break;
				case "qt":      $type = "video/quicktime";               break;
				case "qti":     $type = "image/x-quicktime";             break;
				case "rar":     $type = "encoding/x-compress";           break;
				case "ra":      $type = "audio/x-pn-realaudio";          break;
				case "rm":      $type = "audio/x-pn-realaudio";          break;
				case "ram":     $type = "audio/x-pn-realaudio";          break;
				case "rtf":     $type = "application/rtf";               break;
				case "swa":     $type = "application/x-director";        break;
				case "swf":     $type = "application/x-shockwave-flash"; break;
				case "tar":     $type = "application/x-tar";             break;
				case "tgz":     $type = "application/gzip";              break;
				case "tif":     $type = "image/tiff";                    break;
				case "tiff":    $type = "image/tiff";                    break;
				case "torrent": $type = "application/x-bittorrent";      break;
				case "txt":     $type = "text/plain";                    break;
				case "wav":     $type = "audio/wav";                     break;
				case "wma":     $type = "audio/x-ms-wma";                break;
				case "wmv":     $type = "video/x-ms-wmv";                break;
				case "xls":     $type = "application/vnd.ms-excel";      break;
				case "xml":     $type = "application/xml";               break;
				case "7z":      $type = "application/x-compress";        break;
				case "zip":     $type = "application/x-zip-compressed";  break;
				default:        $type = "application/force-download";    break; 
         }
         
         // Fix IE bug [0]
         $header_file = (strstr($_SERVER['HTTP_USER_AGENT'], 'MSIE')) ? preg_replace('/\./', '%2e', $filename_only, substr_count($filename_only, '.') - 1) : $filename_only;
         
         //Prepare headers
         header("Pragma: public");
         header("Expires: 0");
         header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
         header("Cache-Control: public", false);
         header("Content-Description: File Transfer");
         header("Content-Type: " . $type);
         header("Accept-Ranges: bytes");
         header("Content-Disposition: attachment; filename=\"" . addslashes($header_file) . "\"");
         header("Content-Transfer-Encoding: binary");
         header("Content-Length: " . filesize($target_file));
         
               
         // Send file for download
         if ($stream = fopen($target_file, 'rb')){
         	while(!feof($stream) && connection_status() == 0){
            	//reset time limit for big files
                @set_time_limit(0);
                print(fread($stream,1024*8));
                flush();
             }
             fclose($stream);
         }
	}
	//OTB Africa - Start mark PDF with QR Code
	else if(file_exists($target_file) and $element_mark_file_with_qr_code){
		$prefix_folder_fpdf = dirname(__FILE__)."/../../../../../lib/vendor/otbafrica/fpdf/";
		require_once($prefix_folder_fpdf.'fpdf181/fpdf.php');
		require_once($prefix_folder_fpdf.'fpdf/fpdi.php');
        require_once dirname(__FILE__)."/../../../../../lib/vendor/phpqrcode/qrlib.php";

        $PNG_TEMP_DIR = dirname(__FILE__)."/../../../../../../html/asset_uplds/temp/";
		if (!file_exists($PNG_TEMP_DIR))
			mkdir($PNG_TEMP_DIR);
        $filename = $PNG_TEMP_DIR.'test.png';

		//remember to sanitize user input in real-life solution !!!
		$errorCorrectionLevel = 'L';
		if (isset($_REQUEST['level']) && in_array($_REQUEST['level'], array('L','M','Q','H')))
			$errorCorrectionLevel = $_REQUEST['level'];
		$matrixPointSize = 2;
		if (isset($_REQUEST['size']))
			$matrixPointSize = min(max((int)$_REQUEST['size'], 1), 10);
		$ssl_suffix = mf_get_ssl_suffix();
		#$link = "http".$ssl_suffix."://".$_SERVER[HTTP_HOST]."/".$target_file;
                $link = "http".$ssl_suffix."://".$_SERVER[HTTP_HOST]."/backend.php/forms/download?q=".$_GET['q'];//Show original file with login required

		$filename = $PNG_TEMP_DIR.'test'.md5($link.'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';

		QRcode::png($link, $filename, $errorCorrectionLevel, $matrixPointSize, 2);

		$pieces = explode("/", $filename);

		$actual_filename = $pieces[sizeof($pieces) - 1];

		$pdf = new FPDI();

		$pages = $pdf->setSourceFile($target_file);
		$qr_code_image = dirname(__FILE__)."/../../../../../../html/asset_uplds/temp/".$actual_filename;
		$size_qr_code_image = getimagesize($qr_code_image);
		$width_qr_code_image = $size_qr_code_image[0];
		$height_qr_code_image = $size_qr_code_image[1];
		$x_qr_pos = 0;
		$y_qr_pos = 0;
		//mark all pages with QR code if set on form builder
		$page_count = 1;
		while($page_count <= $pages){
			$tplIdx = $pdf->importPage(1, '/MediaBox');
			$page_size = $pdf->getTemplatesize($tplIdx);
			//$pdf->addPage();
			//Add new page based on the original file's width, height and orientation
			if ($page_size['w'] > $page_size['h']){
				$pdf->addPage('L',array($page_size['w'], $page_size['h']));
			}else{
				$pdf->addPage('P',array($page_size['w'], $page_size['h']));
			}
			$pdf->useTemplate($tplIdx, 0, 0, 0, 0, true);
			if($element_file_qr_page_position == "top_left"){//Set position of QR code
			}else if($element_file_qr_page_position == "top_right"){
				$x_qr_pos = ($page_size['w'])-$width_qr_code_image;
				$y_qr_pos = 0;
			}else if($element_file_qr_page_position == "bottom_left"){
				$x_qr_pos = 0;
				$y_qr_pos = ($page_size['w'])-$height_qr_code_image;
			}else if($element_file_qr_page_position == "bottom_right"){
				$x_qr_pos = ($page_size['w'])-$width_qr_code_image;
				$y_qr_pos = ($page_size['w'])-$height_qr_code_image;
			}

			$pdf->SetFont('Arial','BU');
			$pdf->SetTextColor(34, 139, 34);
			$pdf->SetXY(23, 10);

			if($element_file_qr_all_pages != 1){//If this file field is not set to mark qr on all pages, set pasges to first page only
				if($page_count == 1){
					$pdf->Write(0,"Scan QR code to confirm authenticity");
					$pdf->Image($qr_code_image,$x_qr_pos,$y_qr_pos);
				}
			}else{
				$pdf->Write(0,"Scan QR code to confirm authenticity");
				$pdf->Image($qr_code_image,$x_qr_pos,$y_qr_pos);
			}
			$page_count++;
		}
		//remove the element_x-xx- suffix we added to all uploaded files
		$file_1 	   	= substr($complete_filename,strpos($complete_filename,'-')+1);
		$filename_only 	= substr($file_1,strpos($file_1,'-')+1);
		$pdf->Output("D", $filename_only);
		exit;
	}
	//OTB Africa - End mark PDF with QR Code
	else{
		echo 'File not found!';
	}

?>
