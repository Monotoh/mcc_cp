<?php
/********************************************************************************
 MachForm
  
 Copyright 2007-2012 Appnitro Software. This code cannot be redistributed without
 permission from http://www.appnitro.com/
 
 More info at: http://www.appnitro.com/
 ********************************************************************************/
    $prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";
    require($prefix_folder.'includes/init.php');
	
	require($prefix_folder.'config.php');
	require($prefix_folder.'includes/db-core.php');
	require($prefix_folder.'includes/helper-functions.php');
	require($prefix_folder.'includes/check-session.php');

	require($prefix_folder.'includes/language.php');
	require($prefix_folder.'includes/common-validator.php');
	require($prefix_folder.'includes/post-functions.php');
	require($prefix_folder.'includes/filter-functions.php');
	require($prefix_folder.'includes/entry-functions.php');
	require($prefix_folder.'includes/view-functions.php');
	require($prefix_folder.'includes/users-functions.php');
	
	$form_id  = (int) trim($_GET['form_id']);
	$entry_id = (int) trim($_GET['entry_id']);
	$nav = trim($_GET['nav']);

	$dbh = mf_connect_db();
	$mf_settings = mf_get_settings($dbh);

	//check permission, is the user allowed to access this page?
	if(empty($_SESSION['mf_user_privileges']['priv_administer'])){
		$user_perms = mf_get_user_permissions($dbh,$form_id,$_SESSION['mf_user_id']);

		//this page need edit_entries permission
		if(empty($user_perms['edit_entries'])){
			$_SESSION['MF_DENIED'] = "You don't have permission to edit this entry.";

			$ssl_suffix = mf_get_ssl_suffix();						
			header("Location: http{$ssl_suffix}://".$_SERVER['HTTP_HOST'].mf_get_dirname($_SERVER['PHP_SELF'])."/restricted.php");
			exit;
		}
	}
	
	//get form name
	$query 	= "select 
					 form_name
			     from 
			     	 ".MF_TABLE_PREFIX."forms 
			    where 
			    	 form_id = ?";
	$params = array($form_id);
	
	$sth = mf_do_query($query,$params,$dbh);
	$row = mf_do_fetch_result($sth);
	
	if(!empty($row)){
		$form_name = htmlspecialchars($row['form_name']);
	}

	//if there is "nav" parameter, we need to determine the correct entry id and override the existing entry_id
	if(!empty($nav)){
		$all_entry_id_array = mf_get_filtered_entries_ids($dbh,$form_id);
		$entry_key = array_keys($all_entry_id_array,$entry_id);
		$entry_key = $entry_key[0];

		if($nav == 'prev'){
			$entry_key--;
		}else{
			$entry_key++;
		}

		$entry_id = $all_entry_id_array[$entry_key];

		//if there is no entry_id, fetch the first/last member of the array
		if(empty($entry_id)){
			if($nav == 'prev'){
				$entry_id = array_pop($all_entry_id_array);
			}else{
				$entry_id = $all_entry_id_array[0];
			}
		}
	}

	if(mf_is_form_submitted()){ //if form submitted
		$input_array   = mf_sanitize($_POST);
		$submit_result = mf_process_form($dbh,$input_array);
		
		if($submit_result['status'] === true){
			$_SESSION['MF_SUCCESS'] = 'Entry #'.$input_array['edit_id'].' has been updated.';

			$ssl_suffix = mf_get_ssl_suffix();						
			header("Location: http{$ssl_suffix}://".$_SERVER['HTTP_HOST'].mf_get_dirname($_SERVER['PHP_SELF'])."/viewentry2?id={$input_array['form_id']}&entryid={$input_array['edit_id']}");
			exit;
		}else if($submit_result['status'] === false){ //there are errors, display the form again with the errors
			$old_values 	= $submit_result['old_values'];
			$custom_error 	= @$submit_result['custom_error'];
			$error_elements = $submit_result['error_elements'];
				
			$form_params = array();
			$form_params['populated_values'] = $old_values;
			$form_params['error_elements']   = $error_elements;
			$form_params['custom_error'] 	 = $custom_error;
			$form_params['edit_id']			 = $input_array['edit_id'];
			$form_params['integration_method'] = 'php';
			$form_params['page_number'] = 0; //display all pages (if any) as a single page

			$form_markup = mf_display_form($dbh,$input_array['form_id'],$form_params);
		}

	}else{ //otherwise, display the form with the values
		//set session value to override password protected form
		$_SESSION['user_authenticated'] = $form_id;
		
		//set session value to bypass unique checking
		$_SESSION['edit_entry']['form_id']  = $form_id;
		$_SESSION['edit_entry']['entry_id'] = $entry_id;

		$form_values = mf_get_entry_values($dbh,$form_id,$entry_id);
		
		$form_params = array();
		$form_params['populated_values'] = $form_values;
		$form_params['edit_id']			 = $entry_id;
		$form_params['integration_method'] = 'php';
		$form_params['page_number'] = 0; //display all pages (if any) as a single page

		$form_markup = mf_display_form($dbh,$form_id,$form_params);
	}
	

	$header_data =<<<EOT
EOT;

	$current_nav_tab = 'manage_forms';
	
?>


		<div id="content" >
			<div class="post edit_entry">

				<?php mf_show_message(); ?>

				<div class="content_body">
					<div id="ve_details" data-formid="<?php echo $form_id; ?>" data-entryid="<?php echo $entry_id; ?>">
						<?php echo $form_markup; ?>
					</div>
					<div id="ve_actions">
						<div id="ve_entry_navigation">
							<a href="<?php echo "edit_entry.php?form_id={$form_id}&entry_id={$entry_id}&nav=prev"; ?>" title="Previous Entry"><img src="images/icons/16_left.png" /></a>
							<a href="<?php echo "edit_entry.php?form_id={$form_id}&entry_id={$entry_id}&nav=next"; ?>" title="Next Entry" style="margin-left: 5px"><img src="images/icons/16_right.png" /></a>
						</div>
					</div>
				</div> <!-- /end of content_body -->	
			
			</div><!-- /.post -->
		</div><!-- /#content -->

<div id="dialog-confirm-entry-delete" title="Are you sure you want to delete this entry?" class="buttons" style="display: none">
	<img src="images/icons/hand.png" title="Confirmation" /> 
	<p id="dialog-confirm-entry-delete-msg">
		This action cannot be undone.<br/>
		<strong id="dialog-confirm-entry-delete-info">Data and files associated with this entry will be deleted.</strong><br/><br/>
		If you are sure with this, you can continue with the deletion.<br /><br />
	</p>				
</div>

<div id="dialog-email-entry" title="Email entry #<?php echo $entry_id; ?> to:" class="buttons" style="display: none"> 
	<form id="dialog-email-entry-form" class="dialog-form" style="padding-left: 10px;padding-bottom: 10px">	
		<ul>
			<li>
				<div>
					<input type="text" value="" class="text" name="dialog-email-entry-input" id="dialog-email-entry-input" />
				</div> 
				<div class="infomessage" style="padding-top: 5px;padding-bottom: 0px">Use commas to separate email addresses.</div>
			</li>
		</ul>
	</form>
</div>

<div id="dialog-entry-sent" title="Success!" class="buttons" style="display: none">
	<img src="images/icons/62_green_48.png" title="Success" /> 
	<p id="dialog-entry-sent-msg">
			The entry has been sent.
	</p>
</div>
 