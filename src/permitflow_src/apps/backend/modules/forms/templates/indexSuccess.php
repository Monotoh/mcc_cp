<?php
use_helper("I18N");

$audit = new Audit();
$audit->saveAudit("", "Accessed form settings");

if($sf_user->mfHasCredential("manageforms"))
{
  $_SESSION['current_module'] = "forms";
  $_SESSION['current_action'] = "index";
  $_SESSION['current_id'] = "";
    ?>

<div class="contentpanel panel-email">

<div class="panel panel-dark">
<div class="panel-heading">
			<h3 class="panel-title"><?php echo __('Forms'); ?></h3>
				<div class="pull-right">
            <a class="btn btn-primary-alt settings-margin42" id="newforms" href="<?php echo public_path(); ?>backend.php/forms/editform"><?php echo __('New Form'); ?></a>

</div>
</div>


<div class="panel panel-body panel-body-nopadding ">

<div class="table-responsive">
<table class="table dt-on-steroids mb0" id="table3">
    <thead>
   <tr>
      <th class="no-sort"><input type='checkbox' name='batchall' onclick="boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.name == 'batch') { if(this.checked == true){ box.checked = true; }else{ box.checked = false; } } } "></th>
      <th width="60">#</th>
      <th><?php echo __('Title'); ?></th>
      <th class="no-sort"><?php echo __('Service Code'); ?></th>
      <th class="no-sort" width="250"><?php echo __('Actions'); ?></th>
    </tr>
  </thead>
  <tbody>
                <?php

                $q = Doctrine_Query::create()
                   ->from("ApForms a")
                   ->where("a.form_active = ?", 1)
                   ->orderBy("a.form_name ASC");
                $apforms = $q->execute();

                foreach ($apforms as $form){
                    $form_id = $form->getFormId();
                    ?>
                    <tr id="row_<?php echo $form_id; ?>">
                            <td><input type='checkbox' name='batch' id='batch_<?php echo $form_id; ?>' value='<?php echo $form_id; ?>'></td>
                            <td>
                            <?php echo $form_id; ?>
                            </td>
                            <td>
                        <?php
                        $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
                        mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
                        $sql = "SELECT * FROM ext_translations WHERE field_id = ".$form_id." AND field_name = 'form_name' AND table_class = 'ap_forms' AND locale = '".$sf_user->getCulture()."'";
                        $translation_sth = mysql_query($sql,$dbconn);
                        $translation_row = mysql_fetch_assoc($translation_sth);
                        if($translation_row)
                        {
                          $form_name = $translation_row['trl_content'];
                        }
                        echo $form_name = $form->getFormName();
                        ?>
						</td>
                        <td>
                        <?php
                            echo $form_code;
                        ?>
                        </td>
					   <td>
                        <?php

                        if($sf_user->mfHasCredential("manageforms") && sizeof($applications) <= 0 && sizeof($taskforms) <= 0 && sizeof($userforms) <= 0 && $form_id >17)
                        {
                        ?>
                            <a onClick="if(confirm('Are you sure you want to delete this item?')){ return true; }else{ return false; }" title="<?php echo __('Delete Form'); ?>" id="delete<?php echo $form_id; ?>" href="<?php echo public_path(); ?>backend.php/forms/deleteform/id/<?php echo $form_id; ?>"> <span class="badge badge-primary"><i class="fa fa-trash-o"></i></span></a>
                        <?php
                        }
                        ?>

                        <?php if($sf_user->mfHasCredential("manageforms")){ ?>
                                <a title="<?php echo __('Duplicate Form'); ?>" id="duplicate<?php echo $form_id; ?>" href="<?php echo public_path(); ?>backend.php/forms/duplicateform?id=<?php echo $form_id; ?>"> <span class="badge badge-primary"><i class="fa fa-paste"></i></span></a>
                        <?php } ?>

                        <?php if($sf_user->mfHasCredential("manageforms")){ ?>
                                <a title="<?php echo __('View Entries'); ?>" id="entries<?php echo $form_id; ?>" href="<?php echo public_path(); ?>backend.php/forms/manageentries?id=<?php echo $form_id; ?>"><span class="badge badge-primary"><i class="fa  fa-folder-open"></i></span></a>
                        <?php } ?>

                        <?php if($sf_user->mfHasCredential("manageforms")){ ?>
                                <a title="<?php echo __('Edit Form'); ?>" id="edit<?php echo $form_id; ?>" href="<?php echo public_path(); ?>backend.php/forms/editform?id=<?php echo $form_id; ?>"><span class="badge badge-primary"><i class="fa fa-pencil"></i></span></a>
                        <?php } ?>

                        <?php if($sf_user->mfHasCredential("manageforms")){ ?>
                               <a title="Edit Payment" id="payment<?php echo $form_id; ?>" href="<?php echo public_path(); ?>backend.php/forms/paymentsettings?id=<?php echo $form_id; ?>"><span class="badge badge-primary"><i class="fa fa-credit-card"></i></span></a>
                        <?php } ?>

                        <?php if($sf_user->mfHasCredential("manageforms")){ ?>
                               <a title="View Code" id="code<?php echo $form_id; ?>" href="<?php echo public_path(); ?>backend.php/forms/embedcode?id=<?php echo $form_id; ?>"><span class="badge badge-primary"><i class="fa fa-cog"></i></span></a>
                        <?php } ?>

                        <?php if($sf_user->mfHasCredential("manageforms")){ ?>
                                <a title="<?php echo __('Edit Logic'); ?>" id="logic<?php echo $form_id; ?>" href="<?php echo public_path(); ?>backend.php/forms/logicsettings?id=<?php echo $form_id; ?>"><span class="badge badge-primary"><i class="fa fa-cogs"></i></span></a>
                        <?php } ?>

                       <?php if($sf_user->mfHasCredential("manageforms")){ ?>
                           <a title="<?php echo __('Edit Language Labels'); ?>" id="logic<?php echo $form_id; ?>" href="<?php echo public_path(); ?>backend.php/forms/translateform?id=<?php echo $form_id; ?>"><span class="badge badge-primary"><i class="fa fa-language"></i></span></a>
                       <?php } ?>

                        <?php if($sf_user->mfHasCredential("manageforms")){ ?>
                                <?php
                                if(empty($form_data['form_active'])){
                                    //echo '<a title="Enable Form" id="enable'.$form_id.'" href="#"><span class="badge badge-primary"><i class="fa fa-check-square"></i></span></a>';
                                }else{
                                    //echo '<a title="Disable Form" id="disable'.$form_id.'" href="#"><span class="badge badge-primary"><i class="fa fa-power-off"></i></span></a>';
                                }
                                ?>
                        <?php } ?>

						</td>
                    </tr>

                    <?php
                    $row_num++;
                }//end foreach $form_list_array
                ?>

</tbody>
	<tfoot>
   <tr><td colspan='7' style='text-align: left;'>
   <select id='batch_action' name='batch_action' onChange="if(this.value != ''){if(confirm('Are you sure?')){ batch('forms', this.options[this.selectedIndex].text, this.value); document.getElementById('default').selected='selected'; }}">
   <option id='default' value=''><?php echo __('Choose an action'); ?>..</option>
   <option value='delete'><?php echo __('Set As Deleted'); ?></option>
   </select>
   </td></tr>
   </tfoot>
</table>
</div>
</div><!--panel-body-->
</div><!--panel-dark-->
</div>
<script>
  jQuery(document).ready(function() {
      jQuery('#table3').dataTable({
          "sPaginationType": "full_numbers",

          // Using aoColumnDefs
          "aoColumnDefs": [
          	{ "bSortable": false, "aTargets": [ 'no-sort' ] }
        	]
        });
    });
</script>

<?php
}
else
{
  include_partial("settings/accessdenied");
}
?>
