<?php
	use_helper("I18N");

    $prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";
	require($prefix_folder.'includes/init.php');
	
	require($prefix_folder.'config.php');
	require($prefix_folder.'includes/db-core.php');
	require($prefix_folder.'includes/helper-functions.php');
	require($prefix_folder.'includes/check-session.php');
	require($prefix_folder.'includes/users-functions.php');

	//set this value to 'false' to turn off all charts
	//useful if you have large amount of charts and need to load the report page faster
	$display_widgets = true;
	
	$form_id = $id;
	
	$dbh = mf_connect_db();
	$mf_settings = mf_get_settings($dbh);

	//check permission, is the user allowed to access this page?
	if(empty($_SESSION['mf_user_privileges']['priv_administer'])){
		$user_perms = mf_get_user_permissions($dbh,$form_id,$_SESSION['mf_user_id']);

		//this page need edit_form permission
		if(empty($user_perms['edit_form'])){
			$_SESSION['MF_DENIED'] = "You don't have permission to edit this form.";

			$ssl_suffix = mf_get_ssl_suffix();						
			header("Location: http{$ssl_suffix}://".$_SERVER['HTTP_HOST'].mf_get_dirname($_SERVER['PHP_SELF'])."/restricted.php");
			exit;
		}
	}
	
	
	$query 	= "select 
					 form_name 
			     from 
			     	 ".MF_TABLE_PREFIX."forms 
			    where 
			    	 form_id = ?";
	$params = array($form_id);
	
	$sth = mf_do_query($query,$params,$dbh);
	$row = mf_do_fetch_result($sth);
	
	if(!empty($row)){
		$row['form_name'] = mf_trim_max_length($row['form_name'],55);
		$form_name = htmlspecialchars($row['form_name']);		
	}

	//get the list of widgets, put them into  array
	$query = "SELECT 
					access_key,
					chart_id,
					chart_title,
					chart_height,
					chart_type 
				FROM 
					".MF_TABLE_PREFIX."report_elements
				WHERE 
					chart_status = 1 and 
					access_key <> '' and 
					form_id = ?
			ORDER BY 
					chart_position,chart_id desc";
	
	$params = array($form_id);
	$sth = mf_do_query($query,$params,$dbh);
	
	$report_widgets_array = array();
	$i=0;
	while($row = mf_do_fetch_result($sth)){
		$report_widgets_array[$i]['chart_id'] 	 = $row['chart_id'];
		$report_widgets_array[$i]['access_key']  = $row['access_key'];

		$chart_type = $row['chart_type'];
		$chart_title = $row['chart_title'];

		$report_widgets_array[$i]['chart_height'] = (int) $row['chart_height'];
		if($chart_type == 'grid' && !empty($chart_title)){
			$report_widgets_array[$i]['chart_height'] += 30; //if the grid is having title, add 30px to the height
		}


		if(empty($report_widgets_array[$i]['chart_height']) || $report_widgets_array[$i]['chart_height'] > 600){
			//the maximum chart height is 600px for chart and 630px for grid (30px is for grid title)
			if($chart_type == 'grid'){
				$report_widgets_array[$i]['chart_height'] = 630; 
			}else{
				$report_widgets_array[$i]['chart_height'] = 600;	
			}
		}

		$report_widgets_array[$i]['chart_title'] = htmlspecialchars($row['chart_title']);
		if(empty($report_widgets_array[$i]['chart_title'])){
			$report_widgets_array[$i]['chart_title'] = '-Untitled Widget-';
		}

		$i++;
	}

	//get report access key
	$query 	= "select 
					 report_access_key 
			     from 
			     	 ".MF_TABLE_PREFIX."reports 
			    where 
			    	 form_id = ?";
	$params = array($form_id);
	
	$sth = mf_do_query($query,$params,$dbh);
	$row = mf_do_fetch_result($sth);
	
	if(!empty($row)){
		$report_access_key  = $row['report_access_key'];
		$report_shared_link = "<a href=\"{$mf_settings['base_url']}report.php?key={$report_access_key}\" target=\"blank\">{$mf_settings['base_url']}report?key={$report_access_key}</a>";
	}

			$header_data =<<<EOT
<link type="text/css" href="/form_builder/css/dropui.css" rel="stylesheet" />
<link type="text/css" href="/form_builder/js/jquery-ui/themes/base/jquery.ui.all.css" rel="stylesheet" />
EOT;

	$current_nav_tab = 'manage_forms';
	require($prefix_folder.'includes/header.php'); 
	
?>

<div class="contentpanel">
					<div class="panel panel-dark">
					<div class="panel-heading">
					  <h3 class="panel-title"><?php echo $form_name; ?></h3>
					  <p><?php echo __('Edit, share and publish your form report'); ?></p>
					  <div class="pull-right">
					  <a href="<?php echo public_path(); ?>backend.php/forms/addwidget?id=<?php echo $form_id; ?>" id="add_widget" name="add_widget" class="btn btn-primary-alt tooltips pull-right settings-margin42" data-original-title="New Architect" data-toggle="tooltip"><?php echo __('Add Widget'); ?></a>
					  
					  </div>
					 </div>
					<div class="panel-body panel-body-nopadding">
				
				<div class="content_body">
					<div id="mr_report_shared" style="display: <?php if(!empty($report_shared_link)){ echo 'block'; }else{ echo 'none'; } ?>">
						Report shared at &#8674; <span id="mr_report_shared_span"><?php echo $report_shared_link; ?><span>
					</div>
					<?php
						if(!empty($report_widgets_array)){
							$ssl_suffix = mf_get_ssl_suffix();

							//display the sortable widget list
							echo '<ul id="widget_list_sortable" data-formid="'.$form_id.'" style="clear: both;display: none">';
							foreach ($report_widgets_array as $value) {
								echo "<li id=\"widget_{$value['chart_id']}\" class=\"gradient_blue\">[#{$value['chart_id']}] {$value['chart_title']}</li>\n";
							}
							echo '</ul>';
					?>

							<div id="report_sort_pane_apply" style="display: none">
									<input type="button" id="mr_report_sort_pane_submit" value="Save Changes" class="button_text"> <span id="cancel_report_sort_pane_span">or <a href="#" id="report_sort_pane_cancel"><?php echo __('Cancel'); ?></a></span>
							</div>

					<?php
							//display the widgets
							echo "<ul id=\"mr_report_list\">\n";
							foreach ($report_widgets_array as $value) {
								$widget_url = '/backend.php/forms/widget?key='.$value['access_key'];
					?>

								<li id="li_<?php echo $value['chart_id']; ?>">
									<?php if($display_widgets == true){ ?>
									<iframe 
											height="<?php echo $value['chart_height']; ?>" 
											allowTransparency="true" 
											frameborder="0" 
											scrolling="no" 
											style="width:100%;border:none" 
											src="<?php echo $widget_url; ?>" 
											title="Report Form">
										 <a href="<?php echo $widget_url; ?>" title="<?php echo $value['chart_title']; ?>"><?php echo $value['chart_title']; ?></a>
									</iframe>
									
									<?php 
										}else{
											echo  "<h3>#{$value['chart_id']} {$value['chart_title']}</h3>";
										}
									?>
									
									<div class="report_toolbar">
										<div>
											<span class="chart_idnum">#<?php echo $value['chart_id']; ?></span>
											<a id="widgetedit_<?php echo $value['chart_id']; ?>" href="<?php echo "/backend.php/forms/editwidget?key={$value['access_key']}"; ?>"><span class="icon-pencil" style="margin-right: 5px;font-size: 120%"></span><?php echo __('Edit'); ?></a>
											<a id="widgetcode_<?php echo $value['chart_id']; ?>" href="<?php echo "/backend.php/forms/widgetcode?key={$value['access_key']}"; ?>" style="margin-left: 10px"><span class="icon-paste" style="margin-right: 5px;font-size: 120%"></span><?php echo __('Widget Code'); ?></a>
										
											<a id="widgetdelete_<?php echo $value['chart_id']; ?>" href="#" class="delete_icon"><span class="icon-remove" style="margin-right: 5px;font-size: 120%"></span><?php echo __('Delete'); ?></a>
											<a href="<?php echo "/backend.php/forms/widget?key={$value['access_key']}"; ?>" target="_blank" class="open_icon"><span class="icon-popout" style="margin-right: 5px;font-size: 120%"></span><?php echo __('Open'); ?></a>
										</div>
									</div>
								</li>

					<?php
							}
							echo "</ul>";

						}else{
					?>
					
						<div id="report_manager_empty">
							<h2><?php echo __('Report Empty'); ?></h2>
							<h3><?php echo __('Add widgets to your report by clicking the button above'); ?>.</h3>
						</div>

					<?php } ?>

					<!-- start dialog boxes -->
					<div id="dialog-delete-widget" title="Are you sure you want to delete this widget?" class="buttons" style="display: none">
						<span class="icon-bubble-notification"></span> 
						<p>
							<?php echo __('This action cannot be undone'); ?>.<br/>
							<strong><?php echo __('Only this widget will be deleted. Any related data will remain intact'); ?>.</strong><br/><br/>
							
						</p>	
					</div>
					<div id="dialog-share-report" title="Are you sure you want to share this report?" class="buttons" style="display: none">
						<span class="icon-bubble-notification"></span>  
						<p>
							<strong><?php echo __('This report will be made public.Anyone who has the link can access. No sign-in required'); ?>.</strong><br/><br/>
							<?php echo __('You can unshare it later at anytime you want'); ?>.<br /><br />
						</p>	
					</div>
					<div id="dialog-unshare-report" title="Are you sure you want to unshare this report?" class="buttons" style="display: none">
						<span class="icon-bubble-notification"></span> 
						<p>
							<strong><?php echo __('This report will be made private. Nobody else will be able to view it anymore'); ?>.</strong><br/><br/>
							<?php echo __('You can share it again later at anytime you want'); ?>.<br /><br />
						</p>	
					</div>
					<!-- end dialog boxes -->

				</div> <!-- /end of content_body -->	
			
			</div><!-- /.post -->
		</div><!-- /#content -->

</div>
<?php
	$footer_data =<<<EOT
<script type="text/javascript" src="/form_builder/js/jquery-ui/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="/form_builder/js/jquery-ui/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="/form_builder/js/jquery-ui/ui/jquery.ui.tabs.js"></script>
<script type="text/javascript" src="/form_builder/js/jquery-ui/ui/jquery.ui.mouse.js"></script>
<script type="text/javascript" src="/form_builder/js/jquery-ui/ui/jquery.ui.sortable.js"></script>
<script type="text/javascript" src="/form_builder/js/jquery-ui/ui/jquery.ui.draggable.js"></script>
<script type="text/javascript" src="/form_builder/js/jquery-ui/ui/jquery.ui.position.js"></script>
<script type="text/javascript" src="/form_builder/js/jquery-ui/ui/jquery.ui.dialog.js"></script>
<script type="text/javascript" src="/form_builder/js/manage_report.js"></script>
EOT;

	require($prefix_folder.'includes/footer.php'); 
?>