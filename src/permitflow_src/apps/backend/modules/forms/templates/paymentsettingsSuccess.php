<?php
/********************************************************************************
 MachForm

 Copyright 2007-2012 Appnitro Software. This code cannot be redistributed without
 permission from http://www.appnitro.com/

 More info at: http://www.appnitro.com/
 ********************************************************************************/
 $audit = new Audit();
 $audit->saveAudit("", "Accessed form payment settings");
use_helper("I18N");
  $prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";
  require($prefix_folder.'includes/init.php');

	require($prefix_folder.'config.php');
	require($prefix_folder.'includes/db-core.php');
	require($prefix_folder.'includes/helper-functions.php');
	require($prefix_folder.'includes/check-session.php');
	require($prefix_folder.'includes/users-functions.php');

	$form_id = (int) trim($_REQUEST['id']);

	$dbh = mf_connect_db();
	$mf_settings = mf_get_settings($dbh);

	//check permission, is the user allowed to access this page?
	if(empty($_SESSION['mf_user_privileges']['priv_administer'])){
		$user_perms = mf_get_user_permissions($dbh,$form_id,$_SESSION['mf_user_id']);

		//this page need edit_form permission
		if(empty($user_perms['edit_form'])){
			$_SESSION['MF_DENIED'] = "You don't have permission to edit this form.";

			$ssl_suffix = mf_get_ssl_suffix();
			header("Location: http{$ssl_suffix}://".$_SERVER['HTTP_HOST'].mf_get_dirname($_SERVER['PHP_SELF'])."/restricted.php");
			exit;
		}
	}

	//load the payment settings property from ap_forms table
	$payment_properties = new stdClass();

	$query 	= "select
					 form_name,
					 payment_enable_merchant,
					 payment_merchant_type,
					 ifnull(payment_paypal_email,'') payment_paypal_email,
					 payment_paypal_language,
					 payment_currency,
					 payment_show_total,
					 payment_total_location,
					 payment_enable_recurring,
					 payment_recurring_cycle,
					 payment_recurring_unit,
					 payment_enable_trial,
					 payment_trial_period,
					 payment_trial_unit,
					 payment_trial_amount,
					 payment_price_type,
					 payment_price_amount,
					 payment_price_name,
					 payment_cellulant_checkout_url,
					 payment_cellulant_merchant_username,
					 payment_cellulant_merchant_password,
					 payment_cellulant_service_id,
					 payment_jambopay_shared_key,
					 payment_jambopay_business,
					 payment_pesapal_live_secret_key,
					 payment_pesapal_live_public_key,
					 payment_pesapal_test_secret_key,
					 payment_pesapal_test_public_key,
					 payment_pesapal_enable_test_mode,
					 payment_paypal_enable_test_mode,
					 payment_enable_invoice,
					 payment_invoice_email,
					 payment_delay_notifications,
					 payment_ask_billing,
					 payment_ask_shipping,
					 payment_enable_tax,
					 payment_tax_rate,
					 payment_discount_code,
           payment_tax_amount
			     from
			     	 ".MF_TABLE_PREFIX."forms
			    where
			    	 form_id = ?";
	$params = array($form_id);

	$sth = mf_do_query($query,$params,$dbh);
	$row = mf_do_fetch_result($sth);

	if(!empty($row)){

		$form_name = htmlspecialchars($row['form_name']);

		$payment_properties->form_id = $form_id;
		$payment_properties->enable_merchant = (int) $row['payment_enable_merchant'];
		$payment_properties->merchant_type 	 = $row['payment_merchant_type'];
		$payment_properties->paypal_email 	 = $row['payment_paypal_email'];
		$payment_properties->paypal_language = $row['payment_paypal_language'];

		$payment_properties->currency 		  = $row['payment_currency'];
		$payment_properties->show_total 	  = (int) $row['payment_show_total'];
		$payment_properties->total_location   = $row['payment_total_location'];
		$payment_properties->enable_recurring = (int) $row['payment_enable_recurring'];
		$payment_properties->recurring_cycle  = (int) $row['payment_recurring_cycle'];
		$payment_properties->recurring_unit   = $row['payment_recurring_unit'];

		$payment_properties->enable_trial = (int) $row['payment_enable_trial'];
		$payment_properties->trial_period = (int) $row['payment_trial_period'];
		$payment_properties->trial_unit   = $row['payment_trial_unit'];
		$payment_properties->trial_amount = $row['payment_trial_amount'];

		$payment_properties->enable_tax = (int) $row['payment_enable_tax'];
		$payment_properties->tax_rate 	= $row['payment_tax_rate'];
    $payment_properties->tax_amount 	= $row['payment_tax_amount'];
    $payment_properties->discount_code 	= $row['payment_discount_code'];


        $payment_properties->cellulant_checkout_url   = trim($row['payment_cellulant_checkout_url']);
        $payment_properties->cellulant_merchant_username   = trim($row['payment_cellulant_merchant_username']);
        $payment_properties->cellulant_merchant_password   = trim($row['payment_cellulant_merchant_password']);
        $payment_properties->cellulant_service_id   = trim($row['payment_cellulant_service_id']);

        $payment_properties->jambopay_shared_key   = trim($row['payment_jambopay_shared_key']);
        $payment_properties->jambopay_business   = trim($row['payment_jambopay_business']);

		$payment_properties->pesapal_live_secret_key   = trim($row['payment_pesapal_live_secret_key']);
		$payment_properties->pesapal_live_public_key   = trim($row['payment_pesapal_live_public_key']);
		$payment_properties->pesapal_test_secret_key   = trim($row['payment_pesapal_test_secret_key']);
		$payment_properties->pesapal_test_public_key   = trim($row['payment_pesapal_test_public_key']);
		$payment_properties->pesapal_enable_test_mode  = (int) $row['payment_pesapal_enable_test_mode'];

		$payment_properties->paypal_enable_test_mode  = (int) $row['payment_paypal_enable_test_mode'];

		$payment_properties->enable_invoice  	  = (int) $row['payment_enable_invoice'];
		$payment_properties->delay_notifications  = (int) $row['payment_delay_notifications'];
		$payment_properties->ask_billing  		  = (int) $row['payment_ask_billing'];
		$payment_properties->ask_shipping  		  = (int) $row['payment_ask_shipping'];
		$payment_properties->invoice_email 		  = $row['payment_invoice_email'];

		$payment_properties->price_type   = $row['payment_price_type'];
		$payment_properties->price_amount = (float) $row['payment_price_amount'];
		$payment_properties->price_name   = $row['payment_price_name'];

		if(empty($payment_properties->price_name)){
			$payment_properties->price_name = $form_name.' Fee';
		}

		//payment_enable_merchant has 3 possible values:
		// -1 : disabled
		//  0 : disabled
		//  1 : enabled
		//the -1 is the default for all newly created form
		//once the user save the payment settings page, the only possible values are 0 or 1
		//we put -1 as an option, so that when the first time user load the payment settings page, it will enable the payment setting by default
		if($payment_properties->enable_merchant === -1){
			$payment_properties->enable_merchant = 1;
		}
	}

	$json_payment_properties = json_encode($payment_properties);
	$jquery_data_code = "\$('#ps_main_list').data('payment_properties',{$json_payment_properties});\n";

	//when certain fields (checkboxes, radio buttons, dropdown) has an option being deleted
	//the related ap_element_prices records are not being updated
	//thus we need to do a cleanup here, before loading the prices
	$query = "select element_id,option_id from ".MF_TABLE_PREFIX."element_options where form_id = ? and live = 0";
	$params = array($form_id);

	$sth = mf_do_query($query,$params,$dbh);
	$deleted_element_options = array();
	$i=0;
	while($row = mf_do_fetch_result($sth)){
		$deleted_element_options[$i]['element_id'] = $row['element_id'];
		$deleted_element_options[$i]['option_id']  = $row['option_id'];
		$i++;
	}

	foreach ($deleted_element_options as $value) {
		$query = "delete from ".MF_TABLE_PREFIX."element_prices where form_id=? and element_id=? and option_id=?";

		$params = array($form_id,$value['element_id'],$value['option_id']);
		mf_do_query($query,$params,$dbh);
	}

	//when certain fields (checkboxes, radio buttons, dropdown) has new option being added
	//the related ap_element_prices records are not being updated
	//thus we need to add them here, before loading the prices
	$query = "SELECT
					C.element_id,
					C.option_id,
					C.price
			    FROM (
						SELECT
							  A.element_id,
							  A.option_id,
							  B.price
						  FROM
							  ".MF_TABLE_PREFIX."element_options A left join ".MF_TABLE_PREFIX."element_prices B
							ON
							  (A.form_id = B.form_id and A.element_id = B.element_id and A.option_id = B.option_id)
						 WHERE
						   	  A.form_id = ? and A.`live` = 1
					 ) C
			   WHERE
			   		C.price IS NULL
			ORDER BY
					element_id,option_id ASC";
	$params = array($form_id);

	$sth = mf_do_query($query,$params,$dbh);
	$new_element_options = array();

	$i=0;
	while($row = mf_do_fetch_result($sth)){
		$new_element_options[$i]['element_id'] = $row['element_id'];
		$new_element_options[$i]['option_id']  = $row['option_id'];
		$i++;
	}

	//get existing price-enabled elements from ap_element_prices
	$existing_price_elements_id = array();
	$query = "select element_id from ".MF_TABLE_PREFIX."element_prices where form_id = ? group by element_id asc";
	$params = array($form_id);

	$sth = mf_do_query($query,$params,$dbh);
	while($row = mf_do_fetch_result($sth)){
		$existing_price_elements_id[] = $row['element_id'];
	}

	//if existing_price_elements_id is empty, we don't need to insert into the table, because this means the form is new
	//and this is the first time the user loading this page
	if(!empty($new_element_options) && !empty($existing_price_elements_id)){
		//insert into ap_element_prices and set the price to 0
		foreach ($new_element_options as $value) {

			//if this item doesn't have any friends within the same element_id, we don't need to insert, because this mean the field is new
			if(!in_array($value['element_id'], $existing_price_elements_id)){
				continue;
			}

			$query = "INSERT INTO ".MF_TABLE_PREFIX."element_prices(form_id,element_id,option_id,`price`) VALUES(?,?,?,?)";

			$params = array($form_id,$value['element_id'],$value['option_id'],0);
			mf_do_query($query,$params,$dbh);
		}
	}

	//get price-ready fields for this form and put them into array
	//price-ready fields are the following types: price, checkboxes, multiple choice, dropdown
	$query = "select
					element_title,
					element_id,
					element_type
				from
					".MF_TABLE_PREFIX."form_elements
			   where
			   		form_id=? and
			   		element_status=1 and
			   		element_is_private=0 and
			   		element_type in('radio','money','select','checkbox')
		    order by
		    		element_title asc";
	$params = array($form_id);

	$sth = mf_do_query($query,$params,$dbh);
	$price_field_array = array();
	$price_field_options_array = array();
	$price_field_options_lookup = array();

	while($row = mf_do_fetch_result($sth)){
		$element_id = $row['element_id'];
		$price_field_array[$element_id]['element_title'] = htmlspecialchars(strip_tags($row['element_title']));
		$price_field_array[$element_id]['element_type'] = $row['element_type'];

		if(strlen($price_field_array[$element_id]['element_title']) > 45){
			$price_field_array[$element_id]['element_title'] = substr($price_field_array[$element_id]['element_title'], 0, 45).'...';
		}

		if($row['element_type'] != 'money'){
			//get the choices for the field
			$sub_query = "select
								option_id,
								`option`
							from
								".MF_TABLE_PREFIX."element_options
						   where
						   		form_id=? and
						   		live=1 and
						   		element_id=?
						order by
								`position` asc";
			$sub_params = array($form_id,$element_id);
			$sub_sth = mf_do_query($sub_query,$sub_params,$dbh);
			$i=0;
			while($sub_row = mf_do_fetch_result($sub_sth)){
				$price_field_options_array[$element_id][$i]['option_id'] = $sub_row['option_id'];
				$price_field_options_array[$element_id][$i]['option'] = htmlspecialchars($sub_row['option']);
				$price_field_options_lookup[$element_id][$sub_row['option_id']] = htmlspecialchars($sub_row['option']);
				$i++;
			}

		}
	}

	if(!empty($price_field_options_array)){
		$json_price_field_options = json_encode($price_field_options_array);
		$jquery_data_code .= "\$('#ps_box_define_prices').data('field_options',{$json_price_field_options});\n";
	}

	//load existing data from ap_element_prices table
	$query = "select
					A.element_id,
					A.option_id,
					A.`price`,
					B.`position`
				from
					".MF_TABLE_PREFIX."element_prices A left join ".MF_TABLE_PREFIX."element_options B
				  on
				  	(A.form_id=B.form_id and A.element_id=B.element_id and A.option_id=B.option_id)
   			   where
   			   		A.form_id = ?
   			order by
   					A.element_id,B.position asc";
	$params = array($form_id);

	$sth = mf_do_query($query,$params,$dbh);
	$current_price_settings = array();

	while($row = mf_do_fetch_result($sth)){
		$element_id = (int) $row['element_id'];
		$option_id = (int) $row['option_id'];
		$current_price_settings[$element_id][$option_id]  = $row['price'];
	}


	//prepare the dom data for the prices fields
	foreach ($current_price_settings as $element_id=>$values){
		if($price_field_array[$element_id]['element_type'] == 'money'){ //if this is 'price' field
			$price_values = new stdClass();

			$price_values->element_id = $element_id;
			$price_values->option_id  = 0;
			$price_values->price      = 0;
			$price_values->element_type = 'price';

			$json_price_values = json_encode($price_values);
			$jquery_data_code .= "\$('#liprice_{$element_id}').data('field_price_properties',{$json_price_values});\n";
		}else{


			$price_values_array = array();
			foreach ($values as $option_id=>$price){
				$price_values = new stdClass();

				$price_values->element_id = $element_id;
				$price_values->option_id  = $option_id;
				$price_values->price      = $price;
				$price_values->element_type = 'multi';

				$price_values_array[$option_id] = $price_values;
			}

			$json_price_values = json_encode($price_values_array);
			$jquery_data_code .= "\$('#liprice_{$element_id}').data('field_price_properties',{$json_price_values});\n";
		}
	}

	//get email fields for this form
	$query = "select
					element_id,
					element_title
				from
					`".MF_TABLE_PREFIX."form_elements`
			   where
			   		form_id=? and element_type='email' and element_is_private=0 and element_status=1
			order by
					element_title asc";
	$params = array($form_id);
	$sth = mf_do_query($query,$params,$dbh);

	$i=1;
	$email_fields = array();
	while($row = mf_do_fetch_result($sth)){
		$email_fields[$i]['label'] = $row['element_title'];
		$email_fields[$i]['value'] = $row['element_id'];
		$i++;
	}


	$current_nav_tab = 'manage_forms';
	require($prefix_folder.'includes/header.php');

?>
<div class="contentpanel">

		<div id="content" class="panel panel-default">
			<div class="panel panel-heading">
              <div class="panel-btns">
                  <a target="_blank" href="<?php echo public_path(); ?>backend.php/merchant/index" class="btn btn-warning"> <?php echo __('Configure Merchants') ?> </a>  <a href="#" id="button_save_payment" class="btn btn-success"><?php echo __('Save Settings') ?></a>
              </div><!-- panel-btns -->
              <h3 class="panel-title"><?php echo "<a href='manage_forms.php?id={$form_id}'>".$form_name.'</a>'; ?> <img src="/form_builder/images/icons/resultset_next.gif" /> <?php echo __('Payment Settings') ?></h3>
              <p>Configure payment options for your form</p>
            </div>
				<div class=" panel-body">
				<ul id="ps_main_list"  style="padding:0">

						<li id="ps_box_merchant_settings" class="panel panel-default widget-payment">
							<div class="panel-body">
					  <div id="ps_box_merchant_settings">
						<div class="col-xs-12 temp">
						  <h5 class="ml20">1. Merchant Settings</h5>
						</div>
						<div class="col-xs-12 weather">
						<div class="form-bordered form-horizontal" style="border:1px solid #d2d2d2 !important">
						             <div class="form-group">
										<span class="col-sm-12">
										   <div class="checkbox block"><label class="choice" for="ps_enable_merchant"><input id="ps_enable_merchant" class="checkbox" value="" type="checkbox" <?php if(!empty($payment_properties->enable_merchant)){ echo 'checked="checked"'; } ?>> Enable Merchant <img class="helpmsg" src="/form_builder/images/icons/68_blue.png" title="Disabling this option will turn off the payment functionality of your form."/></label></div>
										</span>
									 </div>
									 <div class="form-group">
										<label class="description col-sm-4" for="ps_select_merchant">
											Select a Merchant
											<img class="helpmsg" src="/form_builder/images/icons/68_blue.png" title="A merchant will process transactions on your form and authorize the payments."/>
										</label>
										<div class="col-sm-8">
											<select class="select medium" id="ps_select_merchant" autocomplete="off">
											 <?php foreach($merchants as $m): ?>
                                                                                             <option value=""> <?php echo __('Change') ?></option>
                                                                                            <option value="<?php echo $m->getName() ?>"> <?php echo $m->getDescription() ?></option>
                                                                                           <?php endforeach ; ?> 
											</select>
                                                                                    
										</div>
									</div>
                                                    <!-- otb patch -->
                                                                     <div class="form-group">
                                                                         <div class="col-sm-8">
                                                                             <div class="alert alert-success">
                                                                                 <h5> <u> <?php echo __('Current Settings') ?> </u> </h5>
                                                                                  <?php echo __('Payment Merchant')  ?> : <?php echo $set_merchant ?><br/>
                                                                                  <?php echo __('Merchant Currency')  ?> : <?php echo $set_currency ?><br/>
                                                                             </div>
                                                                            
                                                                         </div>
                                                                       </div>
									 
					   </div><!-- form-horizontal -->
				       </div>
                      </div>
                    </div>
                 		</li>   <!--End section 1-->
						<li class="ps_arrow" <?php if($payment_properties->enable_merchant === 0){ echo 'style="display: none;"'; } ?>><img src="/form_builder/images/icons/33_orange.png" /></li>
						<li <?php if($payment_properties->enable_merchant === 0){ echo 'style="display: none;"'; } ?>>
							<div id="ps_box_payment_options" class="panel panel-default widget-payment">
										<div  class="col-xs-12 temp">
						 					<h5 class="ml20">2. Payment Options</h5>
										</div>
								<div  class="col-xs-12 weather">
								<div class="form-horizontal form-bordered"  style="border:1px solid #d2d2d2 !important">
								     	
										<div id="ps_optional_settings">
									        <div class="form-group nopadding">
										<label class="description col-sm-12 panel-heading-inline panel-heading-inline-gray section_breakpanel-heading-inline panel-heading-inline-gray">
										<h3 class="panel-title">Optional Settings</h3>
										</label>
									</div>
									        <div class="form-group">
									<div class="col-sm-12">
										<div class="checkbox block"><label class="choice" for="ps_show_total_amount"><input id="ps_show_total_amount" <?php if(!empty($payment_properties->show_total)){ echo 'checked="checked"'; } ?> class="checkbox" value="" type="checkbox"> Show Total Amount <img class="helpmsg" src="/form_builder/images/icons/68_red.png" title="Shows the total amount of the payment to the client as they are filling out the form. You can also select the location of the total amount placement within the form."/></label></div>
									</div>
									</div>
											<div class="form-group" id="ps_show_total_location_div" <?php if(empty($payment_properties->show_total)){ echo 'style="display: none"'; } ?>>
												<label class="col-sm-4">Display at</label>
												<div class="col-sm-8">
													<select class="select medium" id="ps_show_total_location" name="ps_show_total_location" autocomplete="off">
														<option <?php if($payment_properties->total_location == 'top'){ echo 'selected="selected"'; } ?> id="ps_location_top" value="top">top</option>
														<option <?php if($payment_properties->total_location == 'bottom'){ echo 'selected="selected"'; } ?> id="ps_location_bottom" value="bottom">bottom</option>
														<option <?php if($payment_properties->total_location == 'top-bottom'){ echo 'selected="selected"'; } ?> id="ps_location_top_bottom" value="top-bottom">top and bottom</option>
													</select>
												</div>
											</div>
										    <div class="form-group" <?php if(!in_array($payment_properties->merchant_type,array('pesapal','paypal_standard','cellulant','jambopay','ecitizen'))){ echo 'style="display: none"'; } ?>>
										<div class="col-sm-12">
											<div class="checkbox block"><label class="choice" for="ps_enable_tax"><input id="ps_enable_tax" <?php if(!empty($payment_properties->enable_tax)){ echo 'checked="checked"'; } ?> class="checkbox" value="" type="checkbox"> Enable Convenience Fee<img class="helpmsg" src="/form_builder/images/icons/68_red.png" style="vertical-align: top" title="Upon checkout, sales tax will automatically be added to the order total. You can define the tax rate here."/></label></div>
											<div id="ps_tax_rate_div" class="form-groups" <?php if(empty($payment_properties->enable_tax)){ echo 'style="display: none"'; } ?>>
                                                    Convenience Service Code:
                                                    <input id="ps_discount_code" name="ps_discount_code" class="element text" style="width: 200px"  value="<?php echo htmlspecialchars($payment_properties->discount_code); ?>" type="text">
                                                    <br><br>
													Convenience fee (%):
													<input id="ps_tax_rate" name="ps_tax_rate" class="element text" style="width: 40px"  value="<?php echo htmlspecialchars($payment_properties->tax_rate); ?>" type="text"> %
											    <br><br>
                          Convenience fee (Fixed Amount):
                          <input id="ps_tax_amount" name="ps_tax_amount" class="element text" style="width: 40px"  value="<?php echo htmlspecialchars($payment_properties->tax_amount); ?>" type="text">
                      </div>
										</div>
										</div>
										    <div class="paypal_option pesapal_option form-group" <?php if(!in_array($payment_properties->merchant_type,array('pesapal','paypal_standard','cellulant','jambopay','ecitizen'))){ echo 'style="display: none"'; } ?>>
										<div class="col-sm-12">
						             	    <div class="checkbox block"><label class="choice" for="ps_enable_recurring"><input id="ps_enable_recurring" <?php if(!empty($payment_properties->enable_recurring)){ echo 'checked="checked"'; } ?> class="checkbox" value="" type="checkbox"> Enable Recurring Payments <img class="helpmsg" src="/form_builder/images/icons/68_red.png" style="vertical-align: top" title="If enabled, your clients will be charged automatically for every period of time, until they cancel the subscription from their PayPal account."/></label></div>
											<div id="ps_recurring_div" <?php if(empty($payment_properties->enable_recurring)){ echo 'style="display: none"'; } ?>>
											 	<label class="description" style="margin-top: 5px">Charge Payment Every:</label>
												<select id="ps_recurring_cycle">
													<?php
														for($i=1;$i<=10;$i++){
															if($i == $payment_properties->recurring_cycle){
																echo '<option value="'.$i.'" selected="selected">'.$i.'</option>';
															}else{
																echo '<option value="'.$i.'">'.$i.'</option>';
															}
														}
													?>
												</select>
												<select id="ps_recurring_cycle_unit" <?php if(!in_array($payment_properties->merchant_type,array('pesapal','paypal_standard','cellulant','jambopay','ecitizen'))){ echo 'style="display: none"'; }; ?>>
													<option value="day" <?php if($payment_properties->recurring_unit == 'day'){ echo 'selected="selected"'; } ?>>Day(s)</option>
													<option value="week" <?php if($payment_properties->recurring_unit == 'week'){ echo 'selected="selected"'; } ?>>Week(s)</option>
													<option value="month" <?php if($payment_properties->recurring_unit == 'month'){ echo 'selected="selected"'; } ?>>Month(s)</option>
													<option value="year" <?php if($payment_properties->recurring_unit == 'year'){ echo 'selected="selected"'; } ?>>Year(s)</option>
												</select>
												<select id="ps_recurring_cycle_unit_month_year" <?php if(!in_array($payment_properties->merchant_type,array('pesapal','paypal_standard','cellulant','jambopay','ecitizen'))){ echo 'style="display: none"'; }; ?>>
													<option value="month" <?php if($payment_properties->recurring_unit == 'month'){ echo 'selected="selected"'; } ?>>Month(s)</option>
													<option value="year" <?php if($payment_properties->recurring_unit == 'year'){ echo 'selected="selected"'; } ?>>Year(s)</option>
												</select>
											</div>
										</div>
										</div>
											<div class="paypal_option pesapal_option  form-group" id="ps_trial_div_container" <?php if(empty($payment_properties->enable_recurring)){ echo 'style="display: none"'; } ?>>
										   <div class="col-sm-12">
										    <div class="checkbox block"><label class="choice" for="ps_enable_trial"><input id="ps_enable_trial" <?php if(!empty($payment_properties->enable_trial)){ echo 'checked="checked"'; } ?> class="checkbox" value="" type="checkbox"> Enable Trial Periods <img class="helpmsg" src="/form_builder/images/icons/68_red.png" style="vertical-align: top" title="You can enable trial periods to let your clients to try your subscription service before their regular subscription begin. You can set the price and duration of trial periods independently of the regular subscription price and billing cycle. Enter '0' to offer free trial."/></label></div>
											<div id="ps_trial_div" <?php if(empty($payment_properties->enable_trial)){ echo 'style="display: none"'; } ?>>
												<label class="description" style="margin-top: 5px">Trial Period:</label>
												<select id="ps_trial_period">
													<?php
														for($i=1;$i<=10;$i++){
															if($i == $payment_properties->trial_period){
																echo '<option value="'.$i.'" selected="selected">'.$i.'</option>';
															}else{
																echo '<option value="'.$i.'">'.$i.'</option>';
															}
														}
													?>
												</select>
												<select id="ps_trial_unit">
													<option value="day" <?php if($payment_properties->trial_unit == 'day'){ echo 'selected="selected"'; } ?>>Day(s)</option>
													<option value="week" <?php if($payment_properties->trial_unit == 'week'){ echo 'selected="selected"'; } ?>>Week(s)</option>
													<option value="month" <?php if($payment_properties->trial_unit == 'month'){ echo 'selected="selected"'; } ?>>Month(s)</option>
													<option value="year" <?php if($payment_properties->trial_unit == 'year'){ echo 'selected="selected"'; } ?>>Year(s)</option>
												</select>
												<label class="description" style="margin-top: 5px">Trial Price:</label>
												<span class="symbol">$</span><span><input id="ps_trial_amount" name="ps_trial_amount" class="element text medium" value="<?php echo htmlspecialchars($payment_properties->trial_amount); ?>" type="text"></span>
											</div>
											</div>
										</div>

										<div class="form-group">
                                          <div class="col-sm-12">

                                            <span>
                                                <div class="checkbox block"><input id="ps_enable_invoice" <?php if(!empty($payment_properties->enable_invoice)){ echo 'checked="checked"'; } ?> class="checkbox" value="" type="checkbox" style="margin-left: 0px">
                                                <label class="choice" for="ps_enable_invoice">Disable payment on submission. Only pay after invoicing.</label>
                                                <img class="helpmsg" src="/form_builder/images/icons/68_red.png" style="vertical-align: top" title="When enabled, the user will only be redirected to payments page after an invoice has been manually generated after submission."/>
												</div>
                                            </span>

                                          </div>
                                         </div>

                                          <div class="form-group ps_options_span">
                                          <div class="col-sm-12">
                                            <div class="checkbox block ps_options_span pesapal_option" <?php if(!in_array($payment_properties->merchant_type,array('pesapal','paypal_standard','cellulant','jambopay','ecitizen'))){ echo 'style="display: block"'; } ?>><label class="choice" for="ps_delay_notifications"><input id="ps_delay_notifications" <?php if(!empty($payment_properties->delay_notifications)){ echo 'checked="checked"'; } ?> class="checkbox" value="" type="checkbox"> Delay Notifications Until Paid <img class="helpmsg" src="/form_builder/images/icons/68_red.png" title="By default, notification emails are being sent when payment is made successfully. If you disable this option (not recommended), notification emails are being sent immediately upon form submission, regardless of payment status."/></label></div>
                                            </div>
                                            </div>

                                          <div class="form-group ps_options_span">
                                          <div class="col-sm-12">
                                            <div id="ask_billing_span" class="checkbox block ps_options_span pesapal_option" <?php if(!in_array($payment_properties->merchant_type,array('pesapal','paypal_standard','cellulant','jambopay','ecitizen'))){ echo 'style="display: block"'; } ?>><label class="choice" for="ps_ask_billing"><input id="ps_ask_billing" <?php if(!empty($payment_properties->ask_billing)){ echo 'checked="checked"'; } ?> class="checkbox" value="" type="checkbox"> Ask for Billing Address <img class="helpmsg" src="/form_builder/images/icons/68_red.png" title="If enabled, the payment page will prompt your clients to enter their billing address."/></label></div>
                                            </div>
                                            </div>

                                          <div class="form-group ps_options_span">
                                          <div class="col-sm-12">
                                            <div id="ask_shipping_span" class="checkbox block ps_options_span pesapal_option" <?php if(!in_array($payment_properties->merchant_type,array('pesapal','paypal_standard','cellulant','jambopay','ecitizen'))){ echo 'style="display: block"'; } ?>><label class="choice" for="ps_ask_shipping"><input id="ps_ask_shipping" <?php if(!empty($payment_properties->ask_shipping)){ echo 'checked="checked"'; } ?> class="checkbox" value="" type="checkbox"> Ask for Shipping Address <img class="helpmsg" src="/form_builder/images/icons/68_red.png" style="vertical-align: top" title="If enabled, the payment page will prompt your clients to enter their shipping address."/></label></div>
                                            </div>
                                            </div>


										</span>
										</div>
									</div>
								</div>
							</div>
						</li><!--End section 2-->
						<li class="ps_arrow" <?php if($payment_properties->enable_merchant === 0){ echo 'style="display: none;"'; } ?>><img src="/form_builder/images/icons/33_orange.png" /></li>
						<li <?php if($payment_properties->enable_merchant === 0){ echo 'style="display: none;"'; } ?>>
							<div id="ps_box_define_prices" class="panel panel-default widget-payment">
										<div  class="col-xs-12 temp">
						 					<h5 class="ml20">3. Define Prices</h5>
										</div>

								<div class="col-sm-12 weather"  style="border:1px solid #d2d2d2 !important">
								<div class="form-horizontal form-bordered">
									<div id="ps_box_price_selector" class="form-group">
									  <div class="col-sm-12">
										<select id="ps_pricing_type">
												<option value="fixed" <?php if($payment_properties->price_type == 'fixed'){ echo 'selected="selected"'; } ?>>Fixed Amount</option>
												<option value="variable" <?php if($payment_properties->price_type == 'variable'){ echo 'selected="selected"'; } ?>>Variable Amount</option>
										</select>
									 </div>
									</div>

									<div id="ps_box_price_fields" class="col-xs-12">
										<div id="ps_box_price_fixed_amount_div" <?php if($payment_properties->price_type == 'variable'){ echo 'style="display: none;"'; } ?>>
											<label class="description" for="ps_price_amount" style="margin-top: 0px">Price Amount <span class="required">*</span> <img class="helpmsg" src="/form_builder/images/icons/68_green.png" style="vertical-align: top" title="Enter the amount to be charged to your client."/></label>
											<span><input id="ps_price_amount" name="ps_price_amount" class="element text medium form-control" value="<?php echo $payment_properties->price_amount; ?>" type="text"></span>

											<label class="description" for="ps_price_name">Price Name <span class="required">*</span> <img class="helpmsg" src="/form_builder/images/icons/68_green.png" style="vertical-align: top" title="Enter a descriptive name for the price. This will be displayed into PayPal pages and the receipt email being sent to your client."/></label>
											<input id="ps_price_name" name="ps_price_name" class="element text large form-control" value="<?php echo $payment_properties->price_name; ?>" type="text">

											<p><img class="helpmsg" src="/form_builder/images/icons/70_green2.png" style="vertical-align: top" /> Fixed Amount - Your clients will be charged a fixed amount per form submission.</p>
										</div>
										<div id="ps_box_price_variable_amount_div" <?php if($payment_properties->price_type == 'fixed'){ echo 'style="display: none;"'; } ?>>
											<?php if(!empty($price_field_array)){ ?>

											<label class="description" for="ps_select_field_prices">
											Add a Field To Set Prices
											<img class="helpmsg" src="/form_builder/images/icons/68_green.png" title="Add one or more field from this list to set the prices. A field can have one or more prices, depends on the type. When your client select any of the field you've set here, he will be charged the amount being assigned for the selected field. Supported fields: Checkboxes, Drop Down, Multiple Choice, Price"/>
											</label>
											<select class="select large" id="ps_select_field_prices" name="ps_select_field_prices" autocomplete="off">
												<option value=""></option>
												<?php
													foreach ($price_field_array as $element_id=>$data){

														if($data['element_type'] == 'radio'){
															$element_type = 'Multiple Choice';
														}else if($data['element_type'] == 'money'){
															$element_type = 'Price';
														}else if($data['element_type'] == 'select'){
															$element_type = 'Drop Down';
														}else if($data['element_type'] == 'checkbox'){
															$element_type = 'Checkboxes';
														}

														$price_field_array[$element_id]['complete_title'] = $data['element_title'].' ('.$element_type.')';
														$price_field_array[$element_id]['element_type']   = $data['element_type'];

														if(empty($current_price_settings[$element_id])){
															echo "<option value=\"{$element_id}\">{$data['element_title']} ({$element_type})</option>";
														}
													}
												?>
											</select>
											<ul id="ps_field_assignment">
												<?php

													if(!empty($current_price_settings)){

														//get the currency symbol first
														switch($payment_properties->currency){
															case 'IQD' : $currency_symbol = 'IQD';break;
															case 'KES' : $currency_symbol = 'KES';break;
															case 'EUR' : $currency_symbol = '&#8364;';break;
															case 'GBP' : $currency_symbol = '&#163;';break;
															case 'AUD' : $currency_symbol = 'A&#36;';break;
															case 'CAD' : $currency_symbol = 'C&#36;';break;
															case 'JPY' : $currency_symbol = '&#165;';break;
															case 'THB' : $currency_symbol = '&#3647;';break;
															case 'HUF' : $currency_symbol = '&#70;&#116;';break;
															case 'CHF' : $currency_symbol = 'CHF';break;
															case 'CZK' : $currency_symbol = '&#75;&#269;';break;
															case 'SEK' : $currency_symbol = 'kr';break;
															case 'DKK' : $currency_symbol = 'kr';break;
															case 'NOK' : $currency_symbol = 'kr';break;
															case 'PHP' : $currency_symbol = '&#36;';break;
															case 'MYR' : $currency_symbol = 'RM';break;
															case 'ZAR' : $currency_symbol = 'R';break;
															case 'PLN' : $currency_symbol = '&#122;&#322;';break;
															case 'BRL' : $currency_symbol = 'R&#36;';break;
															case 'HKD' : $currency_symbol = 'HK&#36;';break;
															case 'MXN' : $currency_symbol = 'Mex&#36;';break;
															case 'TWD' : $currency_symbol = 'NT&#36;';break;
															case 'TRY' : $currency_symbol = 'TL';break;
														}

														foreach ($current_price_settings as $element_id=>$data){
															$liprice_markup = '';

															if($price_field_array[$element_id]['element_type'] == 'money'){ //if this is price field
																$liprice_markup = '<li id="liprice_'.$element_id.'">'.
																	'<table width="100%" cellspacing="0">'.
																		'<thead>'.
																			'<tr><td>'.
																				'<strong>'.$price_field_array[$element_id]['complete_title'].'</strong>'.
																				'<a href="#" id="deleteliprice_'.$element_id.'" class="delete_liprice"><img src="/form_builder/images/icons/53.png"></a>'.
																			'</td></tr>'.
																		'</thead>'.
																		'<tbody>'.
																			'<tr><td class="ps_td_field_label">Amount will be entered by the client.</td></tr>'.
																		'</tbody>'.
																	'</table>'.
																	'</li>';
															}else{

																$liprice_markup = '<li style="" id="liprice_'.$element_id.'">'.
																	'<table width="100%" cellspacing="0">'.
																		'<thead>'.
																			'<tr><td colspan="2">'.
																				'<strong>'.$price_field_array[$element_id]['complete_title'].'</strong>'.
																				'<a href="#" id="deleteliprice_'.$element_id.'" class="delete_liprice"><img src="/form_builder/images/icons/53.png"></a>'.
																			'</td></tr>'.
																		'</thead>'.
																		'<tbody>';

																foreach ($data as $option_id=>$price){
																	$liprice_markup .=	'<tr>'.
																				'<td class="ps_td_field_label">'.$price_field_options_lookup[$element_id][$option_id].'</td>'.
																				'<td class="ps_td_field_price">'.
																					'<span class="ps_td_currency">'.$currency_symbol.'</span>'.
																					'<input type="text" class="element text large" value="'.$price.'" id="price_'.$element_id.'_'.$option_id.'">'.
																				'</td>'.
																			'</tr>';
																}

																$liprice_markup .= '</tbody></table></li>';
															}

															echo $liprice_markup;
														}
													}
												?>
											</ul>

											<?php } else { ?>
												<div id="ps_no_price_fields">
													<h6>No Available Fields Found</h6>
													<p>To set variable amount prices, you need to add one or more of the following field types into your form: <span style="font-weight: 700">Checkboxes, Drop Down, Multiple Choice, Price.</span></p>
												</div>
											<?php } ?>

											<p><img class="helpmsg" src="/form_builder/images/icons/70_green2.png" style="vertical-align: top" /> Variable Amount - Your clients will be charged a certain amount depends on their selection.</p>
										</div>
								</div>
							</div>
							</div>
						</li>

					</ul>


				</div> <!-- /end of content_body -->

			</div><!-- /.post -->
		</div><!-- /#content -->
</div>

<?php
	$footer_data =<<<EOT
<script type="text/javascript">
	$(function(){
		{$jquery_data_code}
    });
</script>
<script type="text/javascript" src="/form_builder/js/jquery.tools.min.js"></script>
<script type="text/javascript" src="/form_builder/js/payment_settings.js"></script>
EOT;

	require($prefix_folder.'includes/footer.php');
?>
