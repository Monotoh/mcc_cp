<div class="pageheader">
   <h2><i class="fa fa-home"></i> Application <span>Decline</span></h2>
   <div class="breadcrumb-wrapper">
     <span class="label">You are here:</span>
     <ol class="breadcrumb">
       <li><a href="<?php echo public_path(); ?>backend.php">Home</a></li>
       <li class="active">Applications</li>
     </ol>
   </div>
 </div>

<div class="contentpanel">
<div class="row">
<div class="panel panel-dark">
<div class="panel-heading">
  <h3 class="panel-title">Reject Application</h3>
  <p>Send back for corrections</p>
  <div class="pull-right">
  
  </div>
 </div>
<div class="panel-body panel-body-nopadding">
      <div class="col-md-12">

<?php
if($decline)
{
   ?>
   <div align='center'>
   <h4 class="mt20">The application has been sent to the user for re-submission</h4>
   
   <button type="button" class="btn btn-primary" onClick="window.location='/backend.php/applications/view/id/<?php echo $_POST['entryid'] ?>';">Back To Application</button> 
   <?php
	   
			?>
			
	        <button type="button" class="btn btn-default" OnClick="window.location='<?php echo public_path(); ?>backend.php/applications/list/get/your';">List Of Applications</button>
			<?php
	?>

   </div>
   <?php
   
}
else
{
?>
<form action="/backend.php/forms/reject" method="POST"  autocomplete="off" data-ajax="false">
<fieldset>
<input type="hidden" name="entryid" value="<?php echo $entry->getId(); ?>">
<label>
<h4>Reason:</h4>
</label>
<div><textarea class="form-control" name="reason" cols="50" rows="10" required></textarea></div>
</fieldset>
<fieldset>
<section>
<div><button class="btn btn-default mt10" type="submit" class="submit">Send Comments</button></div>
</section>
</fieldset>
<br>
<br>
<?php
}
?>


          </div>
      </div>
   </div>
</div>
</div>