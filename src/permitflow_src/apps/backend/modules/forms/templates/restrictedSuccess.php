<?php
/********************************************************************************
 MachForm
  
 Copyright 2007-2012 Appnitro Software. This code cannot be redistributed without
 permission from http://www.appnitro.com/
 
 More info at: http://www.appnitro.com/
 ********************************************************************************/
    $prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";
    require($prefix_folder.'includes/init.php');
	
	require($prefix_folder.'config.php');
	require($prefix_folder.'includes/db-core.php');
	require($prefix_folder.'includes/helper-functions.php');
	require($prefix_folder.'includes/check-session.php');
	
	require($prefix_folder.'includes/header.php');
	
	$deny_message = "You don't have permission to access this page.";
	if(!empty($_SESSION['MF_DENIED'])){
		$deny_message = $_SESSION['MF_DENIED'];
		$_SESSION['MF_DENIED'] = '';
	}
?>


		<div id="content" class="full">
			<div class="post access_denied">
				<div class="content_header">
					&nbsp;
				</div>
				<div class="content_body">
					<div id="access_denied_body">
						<img src="images/icons/hand.png" />
						<h2>Access Denied.</h2>
						<h3><?php echo $deny_message; ?></h3>
					</div>	
				</div> <!-- /end of content_body -->	
			
			</div><!-- /.post -->
		</div><!-- /#content -->

 
<?php

	require($prefix_folder.'includes/footer.php');
?>