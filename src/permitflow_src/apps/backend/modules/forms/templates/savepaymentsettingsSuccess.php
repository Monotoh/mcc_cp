<?php
/********************************************************************************
 MachForm

 Copyright 2007-2012 Appnitro Software. This code cannot be redistributed without
 permission from http://www.appnitro.com/

 More info at: http://www.appnitro.com/
 ********************************************************************************/
//ini_set('display_errors', '1');     # don't show any errors...
//error_reporting(E_ALL | E_STRICT);  # ...but do log them

    $prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";
    require($prefix_folder.'includes/init.php');

	require($prefix_folder.'config.php');
	require($prefix_folder.'includes/db-core.php');
	require($prefix_folder.'includes/helper-functions.php');
	require($prefix_folder.'includes/check-session.php');

	require($prefix_folder.'includes/filter-functions.php');
	require($prefix_folder.'includes/users-functions.php');

	$dbh = mf_connect_db();
        //OTB patch - Instantiate our helper class
        $otbhelper = new OTBHelper() ;
        //defaut currency iso code
        $curreny_iso_code = 'KES' ;

        //


	if(empty($_POST['payment_properties'])){
		die("Error! You can't open this file directly");
	}

	$payment_properties = mf_sanitize($_POST['payment_properties']);
	$field_prices = mf_sanitize($_POST['field_prices']);

	$form_id = (int) $payment_properties['form_id'];
	unset($payment_properties['form_id']);

	//check permission, is the user allowed to access this page?
	if(empty($_SESSION['mf_user_privileges']['priv_administer'])){
		$user_perms = mf_get_user_permissions($dbh,$form_id,$_SESSION['mf_user_id']);

		//this page need edit_form permission
		if(empty($user_perms['edit_form'])){
			die("Access Denied. You don't have permission to edit this form.");
		}
	}

	//save payment properties into ap_forms table
	foreach ($payment_properties as $key=>$value){
		$form_input['payment_'.$key] = $value;
                 //OTB Fix Bug - Payment Settings is Wrong!! The form needs a re-design. !!
                //Currency value sent is always USD because the system never checks the specific currency value set for a merchant_type
                //The repitive way currency is set is not a good programming practice. We should have one an option to set the merchant currency once somewhere
                if($key == 'merchant_type')
                {
                  //  error_log("Test merchant_type >>>>>>. ".$value) ;
                    //Lets do a quick fix. 
                    //First check the type of currency set for a specific merchant_type
                    if($value == 'cash'){
                         //for cash from the handcode currency on paymentsettingSuccess >> this is bad !!!! get the 
                        //We get the Currency for the selected Merchant type
                      //  error_log('Nice >>>>>>>>>>>>>>>> '.) ;
                        //merchant currency id 
                        $currency_id = $otbhelper->getMerchantCurrency($value) ;
                        $curreny_iso_code = $otbhelper->getCurrencyISOCode($currency_id); // ISO code
                    }
                     //do above for all other merchants
                else if($value == 'cellulant')
                    {
                         $currency_id = $otbhelper->getMerchantCurrency($value) ;
                        $curreny_iso_code = $otbhelper->getCurrencyISOCode($currency_id); // ISO code
                }
                else if($value == 'ecitizen')
                    {
                         $currency_id = $otbhelper->getMerchantCurrency($value) ;
                        $curreny_iso_code = $otbhelper->getCurrencyISOCode($currency_id); // ISO code
                }
                else if($value == 'jambopay')
                    {
                         $currency_id = $otbhelper->getMerchantCurrency($value) ;
                        $curreny_iso_code = $otbhelper->getCurrencyISOCode($currency_id); // ISO code
                }
                  else
                    {
                       $value = 'jambopay' ;
                       ///
                         $currency_id = $otbhelper->getMerchantCurrency($value) ;
                        $curreny_iso_code = $otbhelper->getCurrencyISOCode($currency_id); // ISO code
                }  
                }

	}
        
	//if the merchant type is "check/cash" we need to make sure to disable the tax calculation
	if($form_input['payment_merchant_type'] == 'check'){
		$form_input['payment_enable_tax'] = 0;
    $form_input['payment_enable_amount'] = 0;
	}
       //OTB patch Force currency value
        $form_input['payment_currency'] = $curreny_iso_code ;
	mf_ap_forms_update($form_id,$form_input,$dbh);

	//save field prices into ap_element_prices table
	$query = "delete from ".MF_TABLE_PREFIX."element_prices where form_id=?";
	$params = array($form_id);
	mf_do_query($query,$params,$dbh);
         
	if(!empty($field_prices)){
           
		foreach ($field_prices as $element_data){
			if($element_data['element_type'] == 'price'){ //if this is price field
				$query = "insert into ".MF_TABLE_PREFIX."element_prices(form_id,element_id,option_id,`price`) values(?,?,?,?)";
				$params = array($form_id,$element_data['element_id'],$element_data['option_id'],$element_data['price']);
				mf_do_query($query,$params,$dbh);
			}else{
				foreach($element_data as $values){
					$element_id = (int) $values['element_id'];

					if(!empty($element_id)){
						$query = "insert into ".MF_TABLE_PREFIX."element_prices(form_id,element_id,option_id,`price`) values(?,?,?,?)";
						$params = array($form_id,$values['element_id'],$values['option_id'],$values['price']);
						mf_do_query($query,$params,$dbh);
					}
				}
			}
		}
	}
        error_log("Debug test >>>>>>>>>> ") ;
	$_SESSION['MF_SUCCESS'] = 'Payment settings has been saved.';

   	echo '{ "status" : "ok", "form_id" : "'.$form_id.'" }';

?>
