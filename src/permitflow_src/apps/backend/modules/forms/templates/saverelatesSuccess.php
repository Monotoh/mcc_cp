<?php

$formid = $_POST['formrelateid'];
$field1 = $_POST['field1'];
$field2 = $_POST['field2'];
$relatetype = $_POST['relate_type'];
$startingpoint = $_POST['first_stage'];

$q = Doctrine_Query::create()
   ->from('ApForms a')
   ->where('a.form_id = ?', $formid);
$apform = $q->fetchOne();

$apform->setFormDefaultApplication($startingpoint);
$apform->save();


$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

if($relatetype == "1" || $relatetype == "2")
{
$sql = "DELETE FROM ap_field_relations WHERE formid='".$formid."' AND field1 = '".$field1."' AND field2 = '".$field2."'";
mysql_query($sql,$dbconn);


$sql="
DROP TABLE IF EXISTS `ap_form_".$formid."_".$field1."_".$field2."`;
";

if(mysql_query($sql,$dbconn))
{
   //echo "DROPPED EXISTING TABLE.....<br><br>";
}

if($_POST['relate_clear'] && $_POST['relate_clear'] == "1")
{
	header("Location: /backend.php/forms/manageform");
}

$sql = "INSERT ap_field_relations (field1,field2,formid) VALUES('".$field1."','".$field2."','".$formid."')";
mysql_query($sql,$dbconn);

$sql="
CREATE TABLE IF NOT EXISTS `ap_form_".$formid."_".$field1."_".$field2."` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `element_".$field1."` varchar(250) NOT NULL,
  `element_".$field2."` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;
";

if(mysql_query($sql,$dbconn))
{
   //echo "CREATED TABLE.....<br><br>";
}


$query = "SELECT * FROM ap_element_options WHERE form_id='".$formid."' AND element_id='".$field2."' AND live='1'";
$results = mysql_query($query,$dbconn);
while($row = mysql_fetch_assoc($results))
{
	if($_POST['element_'.$row['option_id']])
	{
	    if($_POST['relatetype'] == "2")
		{
			$query = "INSERT INTO `ap_form_".$formid."_".$field1."_".$field2."` (element_".$field1.", element_".$field2.") VALUES('".$row['option_id']."','".$_POST['element_'.$row['option_id']]."')";
			$result1 = mysql_query($query,$dbconn);

		}
		else
		{
			$query = "INSERT INTO `ap_form_".$formid."_".$field1."_".$field2."` (element_".$field1.", element_".$field2.") VALUES('".$_POST['element_'.$row['option_id']]."','".$row['option_id']."')";
			$result1 = mysql_query($query,$dbconn);
		}
	}
}

}
else if($relatetype == "3")
{	
	$sql="
CREATE TABLE IF NOT EXISTS `ap_form_".$formid."_".$field1."` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `element_id` varchar(250) NOT NULL,
  `status` varchar(250) NOT NULL,
  `showvalue` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;
";

if(mysql_query($sql,$dbconn))
{
   //echo "CREATED TABLE.....<br><br>";
}

	//Show/Hide
	$sql = "INSERT ap_form_".$formid."_".$field1."(element_id,status,showvalue) VALUES('".$field2."','1', '".$_POST['showvalue_element']."')";
	mysql_query($sql,$dbconn) or die(mysql_error());

	

}
else if($relatetype == "4")
{
	//Table Relation
	$sql = "DELETE FROM ap_table_relation WHERE element_id = '".$field1."'";
	mysql_query($sql, $dbconn);
	
	$sql = "INSERT INTO ap_table_relation(form_id, element_id, tbl_name, tbl_value_fld, tbl_status_fld, exclude_status) VALUES('".$formid."','".$field1."','".$_POST['table_name']."','".$_POST['value_field']."','".$_POST['status_field']."','".$_POST['exclude_status']."')";
	mysql_query($sql, $dbconn);
}
else if($relatetype == "5")
{
	
	$sql="
	CREATE TABLE IF NOT EXISTS `clusters` (
	  `id` bigint(20) NOT NULL AUTO_INCREMENT,
	  `form_id` varchar(250) NOT NULL,
	  `field1` varchar(250) NOT NULL,
	  `field2` varchar(250) NOT NULL,
	  PRIMARY KEY (`id`)
	) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;
	";
	$rs = mysql_query($sql,$dbconn);
	
	//Clusters
	$sql = "DELETE FROM clusters WHERE form_id = '".$formid."' AND field1 = '".$field1."' AND field2 = '".$field2."'";
	$rs = mysql_query($sql,$dbconn);
	
	$sql = "INSERT INTO clusters(form_id, field1, field2) VALUES('".$formid."','".$field1."','".$field2."')";
	$rs = mysql_query($sql,$dbconn);
}

 $q = Doctrine_Query::create()
	  ->from('ApColumnPreferences a')
	  ->where('a.form_id = ?', $formid);
 $application_identifier = $q->fetchOne();
 
 if(empty($application_identifier))
 {
	$application_identifier = new ApColumnPreferences();
 }
 
 $application_identifier->setFormId($formid);
 $application_identifier->setElementName($_POST['application_identifier']);
 $application_identifier->setStartingPoint($startingpoint);
 $application_identifier->setPosition($_POST['identifier_type']);
 $application_identifier->save();


echo "Successfully saved relationships.";

?>
<script language="javascript">
window.location = "/backend.php/forms/manageform";
</script>
