<?php
use_helper("I18N");

$audit = new Audit();
$audit->saveAudit("", "Accessed translation files");

$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

$translator = new translation();

if($sf_user->mfHasCredential("managelanguages"))
{
    $_SESSION['current_module'] = "languages";
    $_SESSION['current_action'] = "translate";
    $_SESSION['current_id'] = "";
    ?>
    <?php
    /**
     * indexSuccess.php template.
     *
     * Displays list of available languages
     *
     * @package    backend
     * @subpackage languages
     * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
     */
    ?>
    <div class="contentpanel">
        <div class="panel panel-dark">



            <form id="languageform" name="languageform"  action="/backend.php/forms/savetranslate/id/<?php echo $_GET['id']; ?>/service/<?php echo $_GET['filter'] ?>" method="post" enctype="multipart/form-data"  autocomplete="off" data-ajax="false">
                <input type="hidden" name="filter" value="<?php echo $filter; ?>">

                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo __('Translate Labels'); ?></h3>

                    <div class="pull-right">
                        <button type="submit" class="btn btn-primary-alt settings-margin42"><?php echo __('Save Translations'); ?></button>
                        <a class="btn btn-primary-alt settings-margin42" href="<?php echo public_path(); ?>backend.php/forms/index/filter/<?php echo $_GET['filter'] ?>"><?php echo __('Back to Forms'); ?></a>
                    </div>
                </div>

                <div class="panel panel-dark">

                    <div class="panel-body panel-body-nopadding form-bordered form-horizontal">

                        <?php
                        $q = Doctrine_Query::create()
                            ->from("ExtLocales a")
                            ->orderBy("a.local_title ASC");
                        $languages = $q->execute();

                        $q = Doctrine_Query::create()
                            ->from("ApFormElements a")
                            ->where("a.form_id = ?", $_GET['id'])
                            ->andWhere("a.element_status = 1")
                            ->orderBy("a.element_position ASC");
                        $elements = $q->execute();

                        $q = Doctrine_Query::create()
                            ->from("ApForms a")
                            ->where("a.form_id = ?", $_GET['id']);
                        $form = $q->fetchOne();
                        ?>
                        <div class="table-responsive">
                            <hr>
                            <h3 style="padding: 10px;"><?php echo __('Form Properties'); ?></h3>
                            <hr>
                            <table class="table dt-on-steroids mb0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th width="15%">Key</th>
                                    <?php
                                    foreach($languages as $language)
                                    {
                                        echo "<th>".$language->getLocalTitle()."</th>";
                                    }
                                    ?>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>
                                            <textarea name='form_key_locales[]' readonly='readonly'><?php echo $form->getFormName(); ?></textarea>
                                        </td>
                                        <?php
                                        foreach($languages as $language)
                                        {
                                            $sql = "SELECT * FROM ext_translations WHERE field_id = ".$_GET['id']." AND field_name = 'form_name' AND table_class = 'ap_forms' AND locale = '".$language->getLocaleIdentifier()."'";
                                            $rows = mysql_query($sql, $dbconn) or die($sql);

                                            if($row = mysql_fetch_assoc($rows))
                                            {
                                                echo '<td><textarea name="locale_' . $language->getLocaleIdentifier() . '_form_name_'.$language->getLocaleIdentifier().'" class="form-control">'.$row['trl_content'].'</textarea></td>';
                                            }
                                            else {
                                                echo '<td><textarea name="locale_' . $language->getLocaleIdentifier() . '_form_name_'.$language->getLocaleIdentifier().'" class="form-control"></textarea></td>';
                                            }
                                        }
                                        ?>
                                    </tr>

                                    <tr>
                                        <td>2</td>
                                        <td>
                                            <textarea name='form_key_locales[]' readonly='readonly'><?php echo $form->getFormDescription(); ?></textarea>
                                        </td>
                                        <?php
                                        foreach($languages as $language)
                                        {
                                            $sql = "SELECT * FROM ext_translations WHERE field_id = ".$_GET['id']." AND field_name = 'form_description' AND table_class = 'ap_forms' AND locale = '".$language->getLocaleIdentifier()."'";
                                            $rows = mysql_query($sql, $dbconn) or die($sql);

                                            if($row = mysql_fetch_assoc($rows))
                                            {
                                                echo '<td><textarea name="locale_' . $language->getLocaleIdentifier() . '_form_description_'.$language->getLocaleIdentifier().'" class="form-control">'.$row['trl_content'].'</textarea></td>';
                                            }
                                            else {
                                                echo '<td><textarea name="locale_' . $language->getLocaleIdentifier() . '_form_description_'.$language->getLocaleIdentifier().'" class="form-control"></textarea></td>';
                                            }
                                        }
                                        ?>
                                    </tr>

                                    <tr>
                                        <td>3</td>
                                        <td>
                                            <textarea name='form_key_locales[]' readonly='readonly'><?php echo $form->getFormSuccessMessage(); ?></textarea>
                                        </td>
                                        <?php
                                        foreach($languages as $language)
                                        {
                                            $sql = "SELECT * FROM ext_translations WHERE field_id = ".$_GET['id']." AND field_name = 'form_success_message' AND table_class = 'ap_forms' AND locale = '".$language->getLocaleIdentifier()."'";
                                            $rows = mysql_query($sql, $dbconn) or die($sql);

                                            if($row = mysql_fetch_assoc($rows))
                                            {
                                                echo '<td><textarea name="locale_' . $language->getLocaleIdentifier() . '_form_success_message_'.$language->getLocaleIdentifier().'" class="form-control">'.$row['trl_content'].'</textarea></td>';
                                            }
                                            else {
                                                echo '<td><textarea name="locale_' . $language->getLocaleIdentifier() . '_form_success_message_'.$language->getLocaleIdentifier().'" class="form-control"></textarea></td>';
                                            }
                                        }
                                        ?>
                                    </tr>
                                     <tr>
                                        <td>4</td>
                                        <td>
                                            <textarea name='form_key_locales[]' readonly='readonly'><?php echo $form->getFormReviewTitle(); ?></textarea>
                                        </td>
                                        <?php
                                        foreach($languages as $language)
                                        {
                                            $sql = "SELECT * FROM ext_translations WHERE field_id = ".$_GET['id']." AND field_name = 'form_review_title' AND table_class = 'ap_forms' AND locale = '".$language->getLocaleIdentifier()."'";
                                           
                                            $rows = mysql_query($sql, $dbconn) or die($sql);

                                            if($row = mysql_fetch_assoc($rows))
                                            {
                                                echo '<td><textarea name="locale_' . $language->getLocaleIdentifier() . '_form_review_title_'.$language->getLocaleIdentifier().'" class="form-control">'.$row['trl_content'].'</textarea></td>';
                                            }
                                            else {
                                                echo '<td><textarea name="locale_' . $language->getLocaleIdentifier() . '_form_review_title_'.$language->getLocaleIdentifier().'" class="form-control"></textarea></td>';
                                            }
                                        }
                                        ?>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>
                                            <textarea name='form_key_locales[]' readonly='readonly'><?php echo $form->getFormReviewDescription(); ?></textarea>
                                        </td>
                                        <?php
                                        foreach($languages as $language)
                                        {
                                            $sql = "SELECT * FROM ext_translations WHERE field_id = ".$_GET['id']." AND field_name = 'form_review_description' AND table_class = 'ap_forms' AND locale = '".$language->getLocaleIdentifier()."'";
                                           
                                            $rows = mysql_query($sql, $dbconn) or die($sql);

                                            if($row = mysql_fetch_assoc($rows))
                                            {
                                                echo '<td><textarea name="locale_' . $language->getLocaleIdentifier() . '_form_review_description_'.$language->getLocaleIdentifier().'" class="form-control">'.$row['trl_content'].'</textarea></td>';
                                            }
                                            else {
                                                echo '<td><textarea name="locale_' . $language->getLocaleIdentifier() . '_form_review_description_'.$language->getLocaleIdentifier().'" class="form-control"></textarea></td>';
                                            }
                                        }
                                        ?>
                                    </tr>
                                </tbody>
                            </table>
                            <hr>
                            <h3 style="padding: 10px;"><?php echo __('Form Elements'); ?></h3>
                            <hr>
                            <table class="table dt-on-steroids mb0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th width="15%">Key</th>
                                    <?php
                                    foreach($languages as $language)
                                    {
                                        echo "<th>".$language->getLocalTitle()."</th>";
                                    }
                                    ?>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $count = 0;

                                foreach($elements as $element)
                                {
                                    $sql = "SELECT * FROM ap_form_elements where element_id = ".$element->getElementId()." AND form_id = ".$_GET['id'];
                                    $element_result = mysql_query($sql, $dbconn) or die($sql);
                                    $element_row = mysql_fetch_assoc($element_result);
                                    
                                    //Display element title
                                    ?>
                                    <tr>
                                        <td><?php echo $count+1; ?></td>
                                        <td>
                                            <textarea name='key_locales[]' readonly='readonly'><?php echo $element_row['element_title']; ?></textarea>
                                        </td>
                                        <?php
                                        foreach($languages as $language)
                                        {
                                            $sql = "SELECT * FROM ext_translations WHERE option_id = ".$element->getElementId()." AND field_id = ".$_GET['id']." AND field_name = 'element_title' AND table_class = 'ap_form_elements' AND locale = '".$language->getLocaleIdentifier()."'";
                                            $rows = mysql_query($sql, $dbconn) or die($sql);

                                            if($row = mysql_fetch_assoc($rows))
                                            {
                                                echo '<td><textarea name="locale_' . $language->getLocaleIdentifier() . '[' . $element->getElementId() . ']" class="form-control">'.$row['trl_content'].'</textarea></td>';
                                            }
                                            else {
                                                echo '<td><textarea name="locale_' . $language->getLocaleIdentifier() . '[' . $element->getElementId() . ']" class="form-control"></textarea></td>';
                                            }
                                        }
                                        ?>
                                    </tr>
                                    <?php
                                    $count++;

                                    //Display element guideline if not empty
                                    if($element->getElementGuidelines()) {
                                        $sql = "SELECT * FROM ap_form_elements where element_id = ".$element->getElementId()." AND form_id = ".$_GET['id'];
                                        $element_result = mysql_query($sql, $dbconn) or die($sql);
                                        $element_row = mysql_fetch_assoc($element_result);
                                        ?>
                                        <tr>
                                            <td><?php echo $count + 1; ?></td>
                                            <td>
                                                <textarea name='key_locales[]' readonly='readonly'><?php echo $element_row['element_guidelines']; ?></textarea>
                                            </td>
                                            <?php
                                            foreach ($languages as $language) {
                                                $sql = "SELECT * FROM ext_translations WHERE option_id = ".$element->getElementId()." AND field_id = ".$_GET['id']." AND field_name = 'element_guidelines' AND table_class = 'ap_form_elements' AND locale = '".$language->getLocaleIdentifier()."'";
                                                $rows = mysql_query($sql, $dbconn) or die($sql);

                                                if($row = mysql_fetch_assoc($rows)){
                                                    echo '<td><textarea name="locale_guideline_' . $language->getLocaleIdentifier() . '[' . $element->getElementId() . ']" class="form-control">' . $row['trl_content'] . '</textarea></td>';
                                                } else {
                                                    echo '<td><textarea name="locale_guideline_' . $language->getLocaleIdentifier() . '[' . $element->getElementId() . ']" class="form-control"></textarea></td>';
                                                }
                                            }
                                            ?>
                                        </tr>
                                        <?php
                                        $count++;
                                        }

                                        //Display element options
                                        $q = Doctrine_Query::create()
                                            ->from("ApElementOptions a")
                                            ->where("a.form_id = ?", $_GET['id'])
                                            ->andWhere("a.element_id = ?", $element->getElementId())
                                            ->andWhere("a.live = 1");
                                        $options = $q->execute();

                                        foreach ($options as $option) {
                                            $sql = "SELECT * FROM ap_element_options where aeo_id = ".$option->getAeoId();
                                            $option_result = mysql_query($sql, $dbconn) or die($sql);
                                            $option_row = mysql_fetch_assoc($option_result);
                                            ?>
                                            <tr>
                                                <td><?php echo $count + 1; ?></td>
                                                <td>
                                                    <input type='text' name='key_locales[]'
                                                           value="<?php echo $option_row['option']; ?>"
                                                           readonly="readonly">
                                                </td>
                                                <?php
                                                foreach ($languages as $language) {
                                                    $sql = "SELECT * FROM ext_translations WHERE option_id = ".$option->getOptionId()." AND field_id = ".$option->getAeoId()." AND field_name = 'option' AND table_class = 'ap_element_options' AND locale = '".$language->getLocaleIdentifier()."'";
                                                    $rows = mysql_query($sql, $dbconn) or die($sql);

                                                    if($row = mysql_fetch_assoc($rows)){
                                                        echo '<td><input type="text" name="locale_option_' . $element->getElementId() . '_' . $language->getLocaleIdentifier() . '[' . $option->getAeoId() . ']" class="form-control" value="' . $row['trl_content'] . '"></td>';
                                                    } else {
                                                        echo '<td><input type="text" name="locale_option_' . $element->getElementId() . '_' . $language->getLocaleIdentifier() . '[' . $option->getAeoId() . ']" class="form-control" value=""></td>';
                                                    }
                                                }
                                                ?>
                                            </tr>
                                            <?php
                                            $count++;
                                        }
                                }
                                ?>
                                </tbody>
                            </table>
                            <hr>
                            <h3 style="padding: 10px;"><?php echo __('Logic Settings'); ?></h3>
                            <hr>
                            <table class="table dt-on-steroids mb0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th width="15%">Key</th>
                                    <?php
                                    foreach($languages as $language)
                                    {
                                        echo "<th>".$language->getLocalTitle()."</th>";
                                    }
                                    ?>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $count = 0;

                                $sql = "SELECT * FROM ap_field_logic_conditions WHERE form_id = ".$_GET['id'];
                                $conditions = mysql_query($sql, $dbconn) or die($sql);

                                while($condition = mysql_fetch_assoc($conditions)) {
                                    $count++;
                                    //Display element title
                                    ?>
                                    <tr>
                                        <td><?php echo $count; ?></td>
                                        <td>
                                            <textarea name='condition_key_locales[<?php echo $condition['alc_id']; ?>]'
                                                      readonly='readonly'><?php echo $condition['rule_keyword']; ?></textarea>
                                        </td>
                                        <?php
                                        foreach ($languages as $language) {
                                            $sql = "SELECT * FROM ext_translations WHERE field_id = '" . $condition['alc_id'] . "' AND field_name = 'rule_keyword' AND table_class = 'field_logic_conditions' AND locale = '" . $language->getLocaleIdentifier() . "'";
                                            $rows = mysql_query($sql, $dbconn) or die($sql);

                                            if ($row = mysql_fetch_assoc($rows)) {
                                                echo '<td><textarea name="condition_locale_' . $language->getLocaleIdentifier() . '_' . $condition['alc_id'] . '" class="form-control">' . $row['trl_content'] . '</textarea></td>';
                                            } else {
                                                echo '<td><textarea name="condition_locale_' . $language->getLocaleIdentifier() . '_' . $condition['alc_id'] . '" class="form-control"></textarea></td>';
                                            }
                                        }
                                        ?>
                                    </tr>
                                <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>

    <?php
}
else
{
    include_partial("settings/accessdenied");
}
?>
