<?php

        $q = Doctrine_Query::create()
             ->from("FormEntry a")
             ->where("a.id = ?", $_REQUEST['entryid']);
        $application = $q->fetchOne();
        
        $form_id2  = (int) trim($application->getFormId());
        $entry_id2 = (int) trim($application->getEntryId());
        
        if(empty($form_id2) || empty($entry_id2)){
                $form_id2  = (int) trim($_GET['form_id']);
                $entry_id2 = (int) trim($_GET['id']);
        }
        
        
        if(!empty($_GET['moveto']) AND $_GET['moveto'] != "")
        {
                $q = Doctrine_Query::create()
                                           ->from('FormEntry a')
                                           ->where('a.form_id = ?', $form_id2)
                                           ->andWhere('a.entry_id = ?', $entry_id2);
                $formentries = $q->execute();
                foreach($formentries as $formentry)
                {
                        $formentry->setApproved($_GET['moveto']);
                        $formentry->save();

                        $q = Doctrine_Query::create()
                             ->from('CfUser a')
                             ->where('a.bdeleted = 0');
                          $reviewers = $q->execute();
                          foreach($reviewers as $reviewer)
                          {
                              $q = Doctrine_Query::create()
                                  ->from('mfGuardUserGroup a')
                                  ->leftJoin('a.Group b')
                                  ->leftJoin('b.mfGuardGroupPermission c') //Left Join group permissions
                                  ->leftJoin('c.Permission d') //Left Join permissions
                                  ->where('a.user_id = ?', $reviewer->getNid())
                                  ->andWhere('d.name = ?', "accesssubmenu".$formentry->getApproved());
                              $usergroups = $q->execute();
                              if(sizeof($usergroups) > 0)
                              {
                                  $body = "
                                  Hi ".$reviewer->getStrfirstname()." ".$reviewer->getStrlastname().",<br>
                                  <br>
                                  Application ".$formentry->getApplicationId()." has been moved to ".$formentry->getStatusName().":<br>
                                  
                                  <br>
                                  Click here to view the application: <br>
                                  ------- <br>
                                  <a href='http://".$_SERVER['HTTP_HOST']."/backend.php/applications/view/id/".$formentry->getId()."'>Link to ".$entry->getApplicationId()."</a><br>
                                  ------- <br>

                                  <br>
                                  ";

                                  $mailnotifications = new mailnotifications();
                                  $mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $reviewer->getStremail(),$formentry->getApplicationId()." moved to ".$formentry->getStatusName(),$body);
                              }
                          }
                        
                        $q = Doctrine_Query::create()
                                 ->from('ApplicationReference a')
                                 ->where('a.application_id = ?', $formentry->getId())
                                 ->andWhere('a.end_date = ?', "");
                        $oldappref = $q->fetchOne();
                        
                        if($oldappref)
                        {
                                $oldappref->setEndDate(date('Y-m-d'));
                                $oldappref->save();
                        }
                        
                        
                        //If there are 'Previous reasons of decline', clear them
                        if(sizeof($formentry->getEntryDecline()) > 0)
                        {
                                $declines  = $formentry->getEntryDecline();
                                foreach($declines as $decline)
                                {
                                        //$decline->delete();
                                }
                        }
                        
                }
        }
        
?>
<script language="javascript">
        window.location="/backend.php/applications/view/id/<?php echo $formentry->getId(); ?>";
</script>
