<?php
use_helper("I18N");

if($sf_user->mfHasCredential("manageusers"))
{
  $_SESSION['current_module'] = "users";
  $_SESSION['current_action'] = "index";
  $_SESSION['current_id'] = "";
?>
<?php
/**
 * indexSuccess.php template.
 *
 * Displays list of all registered clients
 *
 * @package    backend
 * @subpackage frusers
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>
<div class="pageheader">
  <h2><i class="fa fa-home"></i><?php echo __('Users'); ?><span>List of registered users</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label"><?php echo __('You are here'); ?>:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>backend.php"><?php echo __('Home'); ?></a></li>
      <li class="active"><?php echo __('Users'); ?></li>
    </ol>
  </div>
</div>

<div class="contentpanel">

  

  <div class="mb30"></div>

  <div class="row">

  <div class="alert alert-success" id="notifications" name="notifications" style="display: none;">
    <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
    <strong><?php echo __('Well done'); ?>!</strong> <?php echo __('You successfully updated this user'); ?>.
  </div>

  </div>

   <div class="panel panel-bordered">
  <div class="panel-body panel-body-nopadding">
  <?php if ($pager->getResults()): ?>
  <table class="table mb0 border-left-0 border-right-0 border-bottom-0 panel-table radius-tl radius-tr">
      <thead class="form-horizontal">
      <tr>
      <th  style="width:10%;" class="border-bottom-1 radius-tl">
      <?php
      if($sf_user->mfHasCredential("manageusers")) {
      ?>
      <a class="" href="<?php echo public_path(); ?>backend.php/frusers/new"
         class="btn btn-primary tooltips table-btn" data-original-title="New User"
         data-toggle="tooltip"><i class="fa fa-plus"></i> <span class="hidden-xs"><?php echo __('Add User'); ?></span></a>
      <?php
      }
      ?>
      </th>
          <!--<form method="post" action="/backend.php/frusers/index/filter/<?php //echo $filter; ?><?php //if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">
              <th class="border-bottom-1" style="width:50%;">
                      <input name="search" value="<?php //echo $filter; ?>" placeholder="Search" type="text" class="form-control p10">
              </th>

              <?php
              //if(!sfConfig::get('app_sso_secret')) {
                  ?>
                  <th class="border-bottom-1 radius-tr">
                          <select size="1" name="filter_status" aria-controls="table2"
                                  class="select2"
                                  onChange="window.location='/backend.php/frusers/index/filterstatus/' + this.value;">
                              <option value="1">Select Status</option>
                              <option value="1" <?php //if ($filterstatus == "1") {
                                  //echo "selected='selected'";
                             // } ?>>Active
                              </option>
                              <option value="0" <?php //if ($filterstatus == "0") {
                                 // echo "selected='selected'";
                              //} ?>>Inactive
                              </option>
                          </select>
                  </th>
              <?php
            //  }
              ?>
          </form> -->
      </tr>
      </thead>
  </table>
  <div class="table-responsive">
  <table class="table table-striped table-bordered table-hover" id="bonie">
  <thead>
  <tr class="main-tr">
      <th>#</th>
      <th>Full Names</th>
      <th>Email Address</th>
      <th>User ID</th>
      <th>Created On</th>
      <th>Last Login</th>
      <?php
      if(sfConfig::get('app_enable_categories') == "yes") {
          ?>
          <th>Status</th>
      <?php
      }
      ?>
      <th class="aligncenter">Action</th>
  </tr>
  </thead>
  <tbody>
  <?php
  $count = 0;

  if($pager->getPage() > 1)
  {
    $count = 10 * ($pager->getPage() - 1);
  }

  foreach($pager->getResults() as $user)
  {
      $userprofile = $user->getProfile();
      if($userprofile)
      {
          $count++;
          ?>
          <tr>
              <td><?php echo $count; ?></td>
              <td><a class="" title="View User" href="<?php echo public_path(); ?>backend.php/frusers/show/id/<?php echo $user->getId(); ?>"><?php echo $userprofile->getFullname(); ?></a></td>
              <td><a class="" title="View User" href="<?php echo public_path(); ?>backend.php/frusers/show/id/<?php echo $user->getId(); ?>"><?php echo $userprofile->getEmail(); ?></a></td>
              <td><a class="" title="View User" href="<?php echo public_path(); ?>backend.php/frusers/show/id/<?php echo $user->getId(); ?>"><?php echo $user->getUsername(); ?></a></td>
              <td><?php echo $user->getCreatedAt(); ?></td>
              <td><?php echo $user->getLastLogin(); ?></td>
              <?php
              if(sfConfig::get('app_enable_categories') == "yes") {
                  if($user->getIsActive() != 1) {
                      ?>
                      <td>
                          <span class="label label-danger">Inactive</span>
                      </td>
                  <?php
                  }
                  else {
                      ?>
                      <td>
                          <span class="label label-success">Active</span>
                      </td>
                  <?php
                  }
                      ?>
              <?php
              }
              ?>
              <td class="aligncenter">
                  <a title="View User" href="<?php echo public_path(); ?>backend.php/frusers/show/id/<?php echo $user->getId(); ?>"><span class="badge badge-primary"><i class="fa fa-eye"></i></span></a>
                  <?php
                  if($sf_user->mfHasCredential("manageusers")) {
                      if ($user->getIsActive() != 1) {
                        ?>
                          <a class="" title="Activate User" href="<?php echo public_path(); ?>backend.php/frusers/activate/id/<?php echo $user->getId(); ?>"><span class="badge badge-primary"><i class="fa fa-check-circle"></i></span></a>
                        <?php
                      }
                      else
                      {
                        ?>
                          <a class="" title="Deactivate User" href="<?php echo public_path(); ?>backend.php/frusers/deactivate/id/<?php echo $user->getId(); ?>"><span class="badge badge-primary"><i class="fa fa-times-circle-o"></i></span></a>
                        <?php
                      }

                      if ($user->getIsSuperAdmin() != 1) {
                        ?>
                          <a class="" title="Validate Email" href="<?php echo public_path(); ?>backend.php/frusers/validate/id/<?php echo $user->getId(); ?>"><span class="badge badge-primary"><i class="fa fa-check-circle"></i></span></a>
                        <?php
                      }
                      else
                      {
                        ?>
                          <a class="" title="UnValidate Email" href="<?php echo public_path(); ?>backend.php/frusers/unvalidate/id/<?php echo $user->getId(); ?>"><span class="badge badge-primary"><i class="fa fa-times-circle-o"></i></span></a>
                        <?php
                      }
                  }
                  ?>
              </td>
          </tr>
          <?php
      }
  }
  ?>
  </tbody>
  
  </table>
  </div><!-- table-responsive -->
  <?php else: ?>
      <div class="table-responsive">
          <table class="table dt-on-steroids mb0">
              <tbody>
              <tr><td>
                      No Records Found
                  </td></tr>
              </tbody>
          </table>
      </div>
  <?php endif; ?>
  </div>
</div>


    </div>

    </div><!-- panel -->


</div>


<?php
}
else
{
   include_partial("settings/accessdenied");
}
?>


<script>
    jQuery(document).ready(function() {  
var table = $('#bonie');

/* Formatting function for row details */
function fnFormatDetails(oTable, nTr) {
	var aData = oTable.fnGetData(nTr);
	var sOut = '<table>';
	sOut += '<tr><td>Full Name:</td><td>' + aData[1] + '</td></tr>';
	sOut += '<tr><td> Email Address :</td><td>' + aData[2] + '</td></tr>';
	sOut += '<tr><td>User Id:</td><td>' + aData[3] + '</td></tr>';
	sOut += '<tr><td>Created On:</td><td>' + aData[4] + '</td></tr>';
	sOut += '</table>';

	return sOut;
}

/*
 * Insert a 'details' column to the table
 */
var nCloneTh = document.createElement('th');
nCloneTh.className = "table-checkbox";

var nCloneTd = document.createElement('td');
nCloneTd.innerHTML = '<span class="row-details row-details-close"></span>';

table.find('thead tr').each(function () {
	this.insertBefore(nCloneTh, this.childNodes[0]);
});

table.find('tbody tr').each(function () {
	this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
});

/*
 * Initialize DataTables, with no sorting on the 'details' column
 */
var oTable = table.dataTable({
	"columnDefs": [{
		"orderable": false,
		"targets": [0]
	}],
	"order": [
		[1, 'asc']
	],
	"lengthMenu": [
		[5, 15, 20, -1],
		[5, 15, 20, "All"] // change per page values here
	],
	// set the initial value
	"pageLength": 10,
});
var tableWrapper = $('#bonie_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

/* Add event listener for opening and closing details
 * Note that the indicator for showing which row is open is not controlled by DataTables,
 * rather it is done here
 */
table.on('click', ' tbody td .row-details', function () {
	var nTr = $(this).parents('tr')[0];
	if (oTable.fnIsOpen(nTr)) {
		/* This row is already open - close it */
		$(this).addClass("row-details-close").removeClass("row-details-open");
		oTable.fnClose(nTr);
	} else {
		/* Open this row */
		$(this).addClass("row-details-open").removeClass("row-details-close");
		oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
	}
});
});
</script>