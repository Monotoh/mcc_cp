<?php
if($sf_user->mfHasCredential("manageusers"))
{
  $_SESSION['current_module'] = "users";
  $_SESSION['current_action'] = "index";
  $_SESSION['current_id'] = "";
?>
<?php
/**
 * indexSuccess.php template.
 *
 * Displays list of all registered clients
 *
 * @package    backend
 * @subpackage frusers
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>

<div class="row">
<div class="panel panel-dark">
<div class="panel-heading">
  <h3 class="panel-title">Applicants</h3>
  <p>List of all applicants who have signed up</p>
  
    <div class="pull-right">
  <a  class="btn btn-primary-alt tooltips pull-right settings-margin42" id="newarchitect" type="button" data-toggle="tooltip" title="New Architect">Add New Architect</a>
     		<script language="javascript">
            jQuery(document).ready(function(){
              $( "#newarchitect" ).click(function() {
                  $("#contentload").load("<?php echo public_path(); ?>backend.php/frusers/settingsnew");
              });
            });
            </script>
  </div>
 </div>
<div class="panel-body panel-body-nopadding">
<div class="table-responsive">
          <table class="table mb0" id="table2">
    <thead>
   <tr>
      <th width="60">ID</th>
      <th>Full Name</th>
      <th class="no-sort">Active?</th>
      <th class="no-sort">Validated? </th>
      <th>Last Login</th>
      <th class="no-sort" width="7%">Actions</th>
    </tr>
    </thead>
    <tbody>
	<?php
		$count = 1;
	?>
    <?php foreach ($pager->getResults() as $sf_guard_user): ?>
    <?php
	//Skip id 1, id 1 is used for authenticating backend users
	if($sf_guard_user->getId() == "1")
	{
		continue;
	}
	?>
    <tr id="row_<?php echo $sf_guard_user->getId() ?>">
	  <td><?php echo $sf_guard_user->getId(); ?></td>
      <td><a  id="editarchitect2<?php echo $sf_guard_user->getId(); ?>" href="#" alt="Edit"><?php echo ucwords(strtolower($sf_guard_user->getProfile()->getFullname())); ?></a></td>
      <td align="center"><a id="toggleactivate<?php echo $sf_guard_user->getId(); ?>" href="#">
      <?php  
	  if($sf_guard_user->getIsActive() == "1")
	  {
		  echo "<span class='badge-round badge-success'><span class='fa fa-check'></span></span>";
	  }
	  else
	  {
		  echo "<span class='badge-round badge-danger'><span class='fa fa-times'></span></span>";
	  }
	  ?></a>
      </td>
      <td align="center"><a id="toggleconfirm<?php echo $sf_guard_user->getId(); ?>" href="#">
      <?php 
	  if($sf_guard_user->getIsSuperAdmin() == "1")
	  {
		  echo "<span class='badge-round badge-success'><span class='fa fa-check'></span></span>";
	  }
	  else
	  {
		  echo "<span class='badge-round badge-danger'><span class='fa fa-times'></span></span>";
	  }
	  ?></a></td>
      <td><?php echo $sf_guard_user->getLastLogin() ?></td>
      <td>
	  
	 <span class="badge badge-primary"><i class='fa fa-search-plus'></i><a id="editarchitect<?php echo $sf_guard_user->getId(); ?>" href="#" alt="Edit"></a></span>
	  
	  <?php
	    $q = Doctrine_Query::create()
			 ->from('FormEntry a')
			 ->where('a.user_id = ?', $sf_guard_user->getId());
		$applications = $q->execute();
	    if(sizeof($applications) == 0)
		{
	  ?>
	  <span class="badge badge-primary"><i class='fa fa-trash-o'></i><a id="deletearchitect<?php echo $sf_guard_user->getId(); ?>" href="#"></a></span>
	  <?php
	    }
	  ?>
		
      
      
            <script language="javascript">
            jQuery(document).ready(function(){
              $( "#editarchitect<?php echo $sf_guard_user->getId(); ?>" ).click(function() {
                  $("#contentload").load("<?php echo public_path(); ?>backend.php/frusers/settingsedit/id/<?php echo $sf_guard_user->getId(); ?>");
              });
              $( "#editarchitect2<?php echo $sf_guard_user->getId(); ?>" ).click(function() {
                  $("#contentload").load("<?php echo public_path(); ?>backend.php/frusers/settingsedit/id/<?php echo $sf_guard_user->getId(); ?>");
              });
              $( "#deletearchitect<?php echo $sf_guard_user->getId(); ?>" ).click(function() {
				  if(confirm('Are you sure you want to delete this member?'))
				  {
					  $("#contentload").load("<?php echo public_path(); ?>backend.php/frusers/settingsdelete/id/<?php echo $sf_guard_user->getId(); ?>");
				  }
				  else
				  {
                  	  return false;
				  }
              });
			  $( "#toggleactivate<?php echo $sf_guard_user->getId(); ?>" ).click(function() {
				  $("#contentload").load("<?php echo public_path(); ?>backend.php/frusers/settingsindex?atoggle=<?php echo $sf_guard_user->getId(); ?>");
			  });
			  $( "#toggleconfirm<?php echo $sf_guard_user->getId(); ?>" ).click(function() {
				  $("#contentload").load("<?php echo public_path(); ?>backend.php/frusers/settingsindex?stoggle=<?php echo $sf_guard_user->getId(); ?>");
			  });
            });
            </script>
	  </td>
    </tr>
    <?php endforeach; ?>
   </tbody>
   </table>
   
			<?php if ($pager->haveToPaginate()): ?>
         <div class="pagination pull-right" style="margin:0;" >
			<ul style="list-style-type: none; display:inline;">
				<?php
			    $filter = "";
				if($_GET['filter'])
				{
					$filter = "&filter=".$_GET['filter'];
				}
			  ?>
			  <li style="display:inline;"><?php echo "<a id='first' title='First' href='#'>&laquo;</a>"; ?></li>
			  <li style="display:inline;"><?php echo "<a id='previous' title='Previous' href='#'>&laquo;</a>"; ?></li>
			  
			  <?php foreach ($pager->getLinks() as $page): ?>
			    <?php
				if($pager->getPage() == $page)
				{
					?>
					<li style="display:inline;"><a><b><i><?php echo $page; ?></i></b></a></li>
					<?php
				}
				else
				{
				?>
					<li style="display:inline;"><?php echo "<a id='page".$page."' title='Page ".$page."' href='#'>".$page."</a>"; ?></li>
                    <script language="javascript">
  					jQuery(document).ready(function(){
  						$( "#page<?php echo $page; ?>" ).click(function() {
  							$("#contentload").load("<?php echo public_path(); ?>backend.php/frusers/settingsindex?page=<?php echo $page; ?>");
						});
					});
					</script>
				<?php
				}
				?>
			  <?php endforeach; ?>
			
			 <li style="display:inline;"> <?php echo "<a id='next' title='Next' href='#'>&raquo;</a>"; ?></li>
			 <li style="display:inline;"> <?php echo "<a id='last' title='Last' href='#'>&raquo;</a>"; ?></li>
			
			</ul>
		</div><!-- /.pagination -->
		
		        <script language="javascript">
  					jQuery(document).ready(function(){
  						$( "#first" ).click(function() {
  							$("#contentload").load("<?php echo public_path(); ?>backend.php/frusers/settingsindex?page=1");
						});
  						$( "#previous" ).click(function() {
  							$("#contentload").load("<?php echo public_path(); ?>backend.php/frusers/settingsindex?page=<?php echo $pager->getPreviousPage(); ?>");
						});
  						$( "#next" ).click(function() {
  							$("#contentload").load("<?php echo public_path(); ?>backend.php/frusers/settingsindex?page=<?php echo $pager->getNextPage(); ?>");
						});
  						$( "#last" ).click(function() {
  							$("#contentload").load("<?php echo public_path(); ?>backend.php/frusers/settingsindex?page=<?php echo $pager->getLastPage(); ?>");
						});
					});
				</script>
                
			<?php endif; ?>
   </div>
</div>
</div>


<script src="<?php echo public_path(); ?>assets_unified/js/jquery-1.10.2.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/bootstrap.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/modernizr.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/jquery.sparkline.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/toggles.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/retina.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/jquery.cookies.js"></script>

<script src="<?php echo public_path(); ?>assets_unified/js/flot/flot.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/flot/flot.resize.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/morris.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/raphael-2.1.0.min.js"></script>

<script src="<?php echo public_path(); ?>assets_unified/js/jquery.datatables.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/chosen.jquery.min.js"></script>

<script src="<?php echo public_path(); ?>assets_unified/js/jquery.autogrow-textarea.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/bootstrap-fileupload.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/bootstrap-timepicker.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/jquery.maskedinput.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/jquery.tagsinput.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/jquery.mousewheel.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/dropzone.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/colorpicker.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/fullcalendar.min.js"></script>

<script src="<?php echo public_path(); ?>assets_unified/js/custom.js"></script>

<script src="<?php echo public_path(); ?>assets_unified/js/jquery.prettyPhoto.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/holder.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/bootstrap-wizard.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/jquery.validate.min.js"></script>

<script>
  jQuery(document).ready(function(){
  
  	// Chosen Select
  	jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
    
    jQuery("a[rel^='prettyPhoto']").prettyPhoto();
    
    //Replaces data-rel attribute to rel.
    //We use data-rel because of w3c validation issue
    jQuery('a[data-rel]').each(function() {
        jQuery(this).attr('rel', jQuery(this).data('rel'));
    });
    
     // Basic Wizard
	 jQuery('#basicWizard').bootstrapWizard();
	 
	 jQuery("#nosort").attr('class', '');
    
  });
</script>
<?php
}
else
{
  include_partial("accessdenied");
}
?>
