<?php
/**
 * newSuccess.php template.
 *
 * Allows creation of a new client account
 *
 * @package    backend
 * @subpackage frusers
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

?>

<div class="g12" style="margin-top: -5px;">
			
<?php include_partial('settingsform', array('form' => $form)) ?>

</div>
