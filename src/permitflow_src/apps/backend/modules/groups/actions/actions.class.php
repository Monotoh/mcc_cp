<?php

/**
 * groups actions.
 *
 * @package    permit
 * @subpackage groups
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class groupsActions extends sfActions
{
    /**
     * Executes 'Checkname' action
     *
     * Ajax used to check existence of name
     *
     * @param sfRequest $request A request object
     */
    public function executeCheckname(sfWebRequest $request)
    {
        // add new user
        $q = Doctrine_Query::create()
           ->from("MfGuardGroup a")
           ->where('a.name = ?', $request->getPostParameter('name'));
        $existinggroup = $q->execute();
        if(sizeof($existinggroup) > 0)
        {
              echo '<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Name is already in use!</strong></div><script language="javascript">document.getElementById("submitbuttonname").disabled = true;</script>';
              exit;
        }
        else
        {
              echo '<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Name is available!</strong></div><script language="javascript">document.getElementById("submitbuttonname").disabled = false;</script>';
              exit;
        }
    }

    /**
     * Executes 'Checkname' action
     *
     * Ajax used to check existence of name
     *
     * @param sfRequest $request A request object
     */
    public function executeChecknamemin(sfWebRequest $request)
    {
        // add new user
        $q = Doctrine_Query::create()
           ->from("MfGuardGroup a")
           ->where('a.name = ?', $request->getPostParameter('name'));
        $existinggroup = $q->execute();
        if(sizeof($existinggroup) > 0)
        {
              echo 'fail';
              exit;
        }
        else
        {
              echo 'pass';
              exit;
        }
    }

  public function executeBatch(sfWebRequest $request)
  {
		if($request->getPostParameter('delete'))
		{
			$item = Doctrine_Core::getTable('MfGuardGroup')->find(array($request->getPostParameter('delete')));
			if($item)
			{
				$item->delete();
			}
		}
    }

  public function executeIndex(sfWebRequest $request)
  {
	  
	$q = Doctrine_Query::create()
       ->from('mfGuardGroup a')
	   ->orderBy('a.id DESC');
    $this->groups = $q->execute();
	 
	$this->setLayout("layout-settings");
  }
  
  public function executeDuplicate(sfWebRequest $request)
  {
        $q = Doctrine_Query::create()
             ->from('mfGuardGroup a')
	     ->where('a.id = ?', $request->getParameter("id"));
        $group = $q->fetchOne();
        
        $roles = $group->getPermissions();
        
        $newgroup = new MfGuardGroup();
        $newgroup->setName($group->getName()." Copy Group");
        $newgroup->setDescription($group->getDescription()." (Copy)");
        $newgroup->save();
        
        $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
        
        foreach($roles as $role)
        {
            //$newgroup->setMfGuardGroupPermission($credential, new mfGuardGroupPermission());
            $sql = "INSERT INTO mf_guard_group_permission(group_id, permission_id) VALUES('".$newgroup->getId()."','".$role->getId()."')";
            mysql_query($sql,$dbconn);
        }
  
        $this->redirect("/backend.php/settings/security?load=groups");
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new mfGuardGroupForm();
	$this->setLayout("layout-settings");
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new mfGuardGroupForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($mf_guard_group = Doctrine_Core::getTable('mfGuardGroup')->find(array($request->getParameter('id'))), sprintf('Object mf_guard_group does not exist (%s).', $request->getParameter('id')));
    $this->form = new mfGuardGroupForm($mf_guard_group);
	$this->setLayout("layout-settings");
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($mf_guard_group = Doctrine_Core::getTable('mfGuardGroup')->find(array($request->getParameter('id'))), sprintf('Object mf_guard_group does not exist (%s).', $request->getParameter('id')));
    $this->form = new mfGuardGroupForm($mf_guard_group);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $this->forward404Unless($mf_guard_group = Doctrine_Core::getTable('mfGuardGroup')->find(array($request->getParameter('id'))), sprintf('Object mf_guard_group does not exist (%s).', $request->getParameter('id')));
    

    $audit = new Audit();
    $audit->saveAudit("", "deleted group of id ".$mf_guard_group->getId());

    $mf_guard_group->delete();

    $this->redirect('/backend.php/groups/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $mf_guard_group = $form->save();

      $audit = new Audit();
      $audit->saveAudit("", "<a href=\"/backend.php/groups/edit?id=".$mf_guard_group->getId()."&language=en\">updated group</a>");

      $this->redirect('/backend.php/groups/index');
    }
  }
}
