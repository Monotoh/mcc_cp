<?php
use_helper("I18N");

$audit = new Audit();
$audit->saveAudit("", "Accessed reviewer groups settings");

if($sf_user->mfHasCredential("managegroups"))
{
  $_SESSION['current_module'] = "groups";
  $_SESSION['current_action'] = "index";
  $_SESSION['current_id'] = "";
?>
<div class="contentpanel">
<div class="panel panel-dark">
<div class="panel-heading">
  <h3 class="panel-title"><?php echo __('Groups'); ?></h3>

    <div class="pull-right">
  <a  id="newgroup" class="btn btn-primary-alt tooltips pull-right" type="button" data-toggle="tooltip" title="New Group" style="margin-top:-28px;" href="<?php echo public_path(); ?>backend.php/groups/new"><?php echo __('New Group'); ?></a>

</div>
  </div>

<div class="panel-body panel-body-nopadding">
<div class="table-responsive">

<table id="table3" class="table mb0 dt-on-steroids">
    <thead>
   <tr>
      <th><input type='checkbox' name='batchall' onclick="boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.name == 'batch') { if(this.checked == true){ box.checked = true; }else{ box.checked = false; } } } "></th>
      <th width="60">#</th>
      <th><?php echo __('Name'); ?></th>
      <th><?php echo __('Description'); ?></th>
      <th width="10%"><?php echo __('Actions'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($groups as $mf_guard_group): ?>
    <tr id="row_<?php echo $mf_guard_group->getId() ?>">
	  <td><input type='checkbox' name='batch' id='batch_<?php echo $mf_guard_group->getId() ?>' value='<?php echo $mf_guard_group->getId() ?>'></td>
      <td ><a href="<?php echo url_for('/backend.php/groups/edit?id='.$mf_guard_group->getId()) ?>"><?php echo $mf_guard_group->getId() ?></a></td>
      <td ><?php echo $mf_guard_group->getName() ?></td>
      <td ><?php echo $mf_guard_group->getDescription() ?></td>
      <td>
		<a id="editgroup<?php echo $mf_guard_group->getId(); ?>" href="<?php echo public_path(); ?>backend.php/groups/edit/id/<?php echo $mf_guard_group->getId(); ?>" title="<?php echo __('Edit'); ?>"><span class="badge badge-primary"><i class="fa fa-pencil"></i></span></a>

		<a id="duplicategroup<?php echo $mf_guard_group->getId(); ?>" href="<?php echo public_path(); ?>backend.php/groups/duplicate/id/<?php echo $mf_guard_group->getId(); ?>" title="<?php echo __('Duplicate'); ?>"><span class="badge badge-primary"><i class="fa fa-paste"></i></span></a>

      	<a id="deletegroup<?php echo $mf_guard_group->getId(); ?>"  onClick="if(confirm('Are you sure you want to delete this item?')){ return true; }else{ return false; }" href="<?php echo public_path(); ?>backend.php/groups/delete/id/<?php echo $mf_guard_group->getId(); ?>" title="<?php echo __('Delete'); ?>"><span class="badge badge-primary"><i class="fa fa-trash-o"></i></span></a>

      </td>
    </tr>
    <?php endforeach; ?>
     </tbody>
	<tfoot>
   <tr><td colspan='7' style='text-align: left;'>
   <select id='batch_action' name='batch_action' onChange="if(this.value != ''){if(confirm('Are you sure?')){ batch('groups', this.options[this.selectedIndex].text, this.value); document.getElementById('default').selected='selected'; }}">
   <option id='default' value=''><?php echo __('Choose an action'); ?>..</option>
   <option value='delete'><?php echo __('Set As Deleted'); ?></option>
   </select>
   </td></tr>
   </tfoot>
</table>
 </div>
 </div>
 </div>
  </div>
<script language='javascript'>
    jQuery(document).ready(function() {

    		jQuery('#table3').dataTable({
           "sPaginationType": "full_numbers"
         });

	});
</script>
<?php
}
else
{
  include_partial("settings/accessdenied");
}
?>
