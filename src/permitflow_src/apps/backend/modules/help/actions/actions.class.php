<?php
/**
 * Help actions.
 *
 * Displays help manuals and videos for the reviewers.
 *
 * @package    backend
 * @subpackage help
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */  
class helpActions extends sfActions
{
    /**
    * Executes 'Index' action 
    * 
    * Display backend user manual
    *
    * @param sfRequest $request A request object
    */
    public function executeIndex(sfWebRequest $request)
    {
        $this->setLayout('layout-metronic');
    }
    
    /**
    * Executes 'Video' action 
    * 
    * Display backend user manual
    *
    * @param sfRequest $request A request object
    */
    public function executeVideo(sfWebRequest $request)
    {

    }
    /**
     * OTB patch
     * Search UPI
     */
    public function executeSearchUPI(sfWebRequest $request){
        
    }
}
