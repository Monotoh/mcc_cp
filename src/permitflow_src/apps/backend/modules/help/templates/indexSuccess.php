<?php
/**
 * indexSuccess.php partial.
 *
 * Displays backend user manual
 *
 * @package    backend
 * @subpackage help
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper('I18N');
?>
<div class="pageheader">
  <h2><i class="fa fa-file-text"></i> Help <span>Download Resources for Reviewers and System Admins</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>backend.php/dashboard">Home</a></li>
      <li>Help</li>
    </ol>
  </div>
</div>

<div class="contentpanel">
  
  <div class="row blog-content">
  <div class="col-sm-9">
    
    <div class="panel panel-default panel-blog">
      <div class="panel-body">
        <h3 class="blogsingle-title">Resources</h3>
        
        <ol>
          <li> <a href="downloads/CP_System_User_Manual_Administrator.pdf">  <?php echo __('Admin User Manual') ?> </a></li>
          <li> <a href="assets_unified/downloads/CP_System_User_Manual_Reviewers.pdf">  <?php echo __('Reviewers User Manual') ?> </a></li>
        </ol>
        
        <br />
        
      </div><!-- panel-body -->
    </div><!-- panel -->
    
  </div>
  
  
  
</div>
</div>