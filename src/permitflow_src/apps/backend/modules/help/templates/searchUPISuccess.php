<?php
/**
 * indexSuccess.php partial.
 *
 * Displays backend user manual
 *
 * @package    backend
 * @subpackage help
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper('I18N');
?>
<div class="pageheader">
  <h2><i class="fa fa-file-text"></i> Help <span>Search UPI details</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>backend.php/dashboard">Home</a></li>
      <li>UPI</li>
    </ol>
  </div>
</div>

<div class="contentpanel">
  
  <div class="row blog-content">
  <div class="col-sm-12">
    
    <div class="panel panel-default panel-blog">
      <div class="panel-body">
        <h3 class="blogsingle-title">UPI Details - LAIS BPMIS Integration</h3>
        <form action="#" method="post">
            <label> Enter UPI Detail: </label>
            <input size="30px" type="text" name="upi" id="upi" value="e.g 1/02/10/04/0000" />
            <button id="ajax_upi" name="ajax_upi" class="btn btn-primary"> Get Details </button> <br/>
            <div id="info"> 
             
            </div>
            <div id="lais_plot_details"> 
             
            </div>
            
        </form>
        
      </div><!-- panel-body -->
    </div><!-- panel -->
    
  </div>
  
  
  
</div>
</div>
<script type="text/javascript">
   
jQuery(document).ready(function(){
     $("#ajax_upi").click(function(e) {
         var upi = $("#upi").val();
         $('#info').append("<div class='alert alert-success' id='connect'> Connecting to LAIS ........Please Wait ........</div>");
         $('#error1').remove();
         $('#error2').remove();
         $('#error3').remove();
         $('#upi_details').remove();
        $.post('/index.php/jquery/checkupi/', {upi:upi}, function(plot){
	if ( plot === 'false' ) {
             $('#connect').remove();$('#upi_details').remove();
		$( "#info" ).append( "<div id='error1' class='alert alert-danger'> Oops! Plot data could not be fetched for the given UPI in LAIS.</div>");
	}else if ( plot && plot.return ) {
            $('#connect').remove();
		//alert('Please Note! The plot data has been pre-filled for UPI '+plot.return.UPI+' of plot size '+plot.return.size);
		$( "#lais_plot_details" ).append( "<div id='upi_details'> <p>Owner First Name: <b>" + (plot.return.owner[0] ? plot.return.owner[0].owner.givenName : plot.return.owner.owner.givenName) +"</b></p>");
		$( "#lais_plot_details" ).append( "<br/><p>Owner Surname: <b>" + (plot.return.owner[0] ? plot.return.owner[0].owner.surname : plot.return.owner.owner.surname) +"</b></p>");
		$( "#lais_plot_details" ).append( "<br/><p>Size: <b>" + plot.return.size + "</b></p>");
		$( "#lais_plot_details" ).append( "<br/><p>Sector: <b>" + plot.return.address.sector + "</b></p>");
		$( "#lais_plot_details" ).append( "<br/><p>Cell: <b>" + plot.return.address.cell + "</b></p>");
		$( "#lais_plot_details" ).append( "<br/><p>Village: <b>" + plot.return.address.village + "</b></p> </div>");
	}else if (upi) {
             
		$( "#info" ).append( "<div id='error2' class='alert alert-danger'> Ooops! Plot data could not be fetched for the given UPI in LAIS.</div>");
	        $('#connect').remove();
                $('#upi_details').remove();
        }
}, 'json').fail(function() {
        $('#connect').remove();
        $('#upi_details').remove();
	$( "#info" ).append( "<div id='error3' class='alert alert-danger'> Ooops! Plot data could not be fetched at this time.</div>");
	});
         e.preventDefault();
     }) ;

});
</script>