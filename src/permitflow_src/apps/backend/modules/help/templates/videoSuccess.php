<?php
/**
 * indexSuccess.php partial.
 *
 * Displays backend user manual
 *
 * @package    backend
 * @subpackage help
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>
<div class="pageheader">
  <h2><i class="fa fa-file-text"></i> Video Tutorials <span>Video Tutorials for Reviewers</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>backend.php/dashboard">Home</a></li>
      <li>Help</li>
      <li>Videos</li>
    </ol>
  </div>
</div>

<div class="contentpanel">
  
  <div class="row blog-content">
  <div class="col-sm-9">
    
    <div class="panel panel-default panel-blog">
      <div class="panel-body">
        <h3 class="blogsingle-title">Getting Started With MasterCPMIS</h3>
        
        <ul class="blog-meta">
          <li>Jan 02, 2014</li>
        </ul>
        
        <br />
        <div class="mb20"></div>
        
        
        <a id="myaccount"><h4 class="blogsingle-title">My Account</h4></a>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
         
        <a id="dashboard"><h4 class="blogsingle-title">Dashboard</h4></a>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        
        <a id="applications"><h4 class="blogsingle-title">Applications</h4></a>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        
        <a id="tasks"><h4 class="blogsingle-title">Tasks</h4></a>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        
        <a id="messages"><h4 class="blogsingle-title">Messages</h4></a>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        
        <a id="architects"><h4 class="blogsingle-title">Architects</h4></a>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        
        <a id="reviewers"><h4 class="blogsingle-title">Reviewers</h4></a>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        
        <a id="reports"><h4 class="blogsingle-title">Reports</h4></a>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        
        <a id="help"><h4 class="blogsingle-title">Help</h4></a>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        
        <a id="videotutorials"><h4 class="blogsingle-title">Video Tutorials</h4></a>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      
      </div><!-- panel-body -->
    </div><!-- panel -->
    
  </div>
  
  <div class="col-sm-3">
        <div class="blog-sidebar">
          
          <h5 class="subtitle">Categories</h5>
          <ul class="sidebar-list">
            <li><a href="#myaccount"><i class="fa fa-angle-right"></i> My Account</a></li>
            <li><a href="#dashboard"><i class="fa fa-angle-right"></i> Dashboard</a></li>
            <li><a href="#applications"><i class="fa fa-angle-right"></i> Applications</a></li>
            <li><a href="#tasks"><i class="fa fa-angle-right"></i> Tasks</a></li>
            <li><a href="#messages"><i class="fa fa-angle-right"></i> Messages</a></li>
            <li><a href="#architects"><i class="fa fa-angle-right"></i> Architects</a></li>
            <li><a href="#reviewers"><i class="fa fa-angle-right"></i> Reviewers</a></li>
            <li><a href="#reports"><i class="fa fa-angle-right"></i> Reports</a></li>
            <li><a href="#help"><i class="fa fa-angle-right"></i> Help</a></li>
            <li><a href="#videotutorials"><i class="fa fa-angle-right"></i> Video Tutorials</a></li>
          </ul>
          
        </div><!-- blog-sidebar -->
        
   </div><!-- col-sm-4 -->
  
</div>
</div>