<?php

/**
 * invoiceapiaccounts actions.
 *
 * @package    permit
 * @subpackage invoiceapiaccounts
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class invoiceapiaccountsActions extends sfActions
{
  public function executeBatch(sfWebRequest $request)
  {
		if($request->getPostParameter('delete'))
		{
			$item = Doctrine_Core::getTable('InvoiceApiAccount')->find(array($request->getPostParameter('delete')));
			if($item)
			{
				$item->delete();
			}
		}
  }

  public function executeIndex(sfWebRequest $request)
  {
    $this->invoice_api_acoounts = Doctrine_Core::getTable('InvoiceApiAccount')
      ->createQuery('a')
	  ->orderBy('a.id DESC')
      ->execute();
	$this->setLayout("layout-settings");
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new InvoiceApiAccountForm();
	$this->setLayout("layout-settings");
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new InvoiceApiAccountForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($invoice_api_account = Doctrine_Core::getTable('InvoiceApiAccount')->find(array($request->getParameter('id'))), sprintf('Object invoice api account does not exist (%s).', $request->getParameter('id')));
    $this->form = new InvoiceApiAccountForm($invoice_api_account);
	  $this->setLayout("layout-settings");
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($invoice_api_account = Doctrine_Core::getTable('InvoiceApiAccount')->find(array($request->getParameter('id'))), sprintf('Object invoice api account does not exist (%s).', $request->getParameter('id')));
    $this->form = new InvoiceApiAccountForm($invoice_api_account);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {

    $this->forward404Unless($invoice_api_account = Doctrine_Core::getTable('InvoiceApiAccount')->find(array($request->getParameter('id'))), sprintf('Object invoice api account does not exist (%s).', $request->getParameter('id')));
    $invoice_api_account->delete();

    $this->redirect('/backend.php/invoiceapiaccounts/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
        $invoice_api_account = $form->save();

        $this->redirect('/backend.php/invoiceapiaccounts/index');
    }
  }
}
