<?php
use_helper("I18N");
?>

<div class="panel panel-dark">
<div class="panel-heading">
  <h3 class="panel-title"><?php echo ($form->getObject()->isNew()?__('New API Account'):__('Edit API Account')); ?></div>
  <form id="accountform" name="accountform" class="form-bordered form-horizontal" action="<?php echo url_for('/backend.php/invoiceapiaccounts/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>

<div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
  <strong><?php echo __('Well done'); ?>!</strong> <?php echo __('You successfully updated this record'); ?></a>.
</div>


<div class="panel-body panel-body-nopadding">

  <?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <?php echo $form->renderGlobalErrors() ?>
	  <?php if(isset($form['_csrf_token'])): ?>
        <?php echo $form['_csrf_token']->render(); ?>
      <?php endif; ?>
            
      <div class="form-group">
        <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Name'); ?></i></label>
         <div class="col-sm-8">
          <?php echo $form['mda_name']->renderError() ?>
          <input type="text" name="invoiceapiaccount[mda_name]" id="invoiceapiaccount_mda_name"  class="form-control" value="<?php echo $form->getObject()->isNew()?" ":$form->getObject()->getMdaName(); ?>">
        </div>
      </div>
            
      <div class="form-group">
        <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Branch'); ?></i></label>
         <div class="col-sm-8">
          <?php echo $form['mda_branch']->renderError() ?>
          <input type="text" name="invoiceapiaccount[mda_branch]" id="invoiceapiaccount_mda_branch" class="form-control" value="<?php echo $form->getObject()->isNew()?" ":$form->getObject()->getMdaBranch(); ?>">
        </div>
      </div>

        <div class="form-group">
            <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('API Key'); ?></i></label>
            <div class="col-sm-8">
                <?php echo $form['api_key']->renderError() ?>
                <input type="text" name="invoiceapiaccount[api_key]" id="invoiceapiaccount_api_key"  class="form-control" value="<?php echo $form->getObject()->isNew()?" ":$form->getObject()->getApiKey(); ?>">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('API Secret'); ?></i></label>
            <div class="col-sm-8">
                <?php echo $form['api_secret']->renderError() ?>
                <input type="text" name="invoiceapiaccount[api_secret]" id="invoiceapiaccount_api_secret" class="form-control" value="<?php echo $form->getObject()->isNew()?" ":$form->getObject()->getApiSecret(); ?>">
            </div>
        </div>
      
	   </div><!--panel-body-->
        
        <div class="panel-footer">
		<button class="btn btn-danger mr10"><?php echo __('Reset'); ?></button><button id="submitbuttonname" type="submit" class="btn btn-primary" name="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button>
		</div>
</form>
</div><!--panel-body-->
</div>

