<?php
use_helper("I18N");

$audit = new Audit();
$audit->saveAudit("", "Accessed Invoice API Account settings");

if($sf_user->mfHasCredential("manageinvoices"))
{
  $_SESSION['current_module'] = "invoiceapiaccounts";
  $_SESSION['current_action'] = "index";
  $_SESSION['current_id'] = "";
?>
<div class="contentpanel">
<div class="panel panel-dark">
<div class="panel-heading">
			<h3 class="panel-title"><?php echo __('Invoice API Accounts'); ?></h3>
				<div class="pull-right">
            <a class="btn btn-primary-alt settings-margin42" id="newinvoiceapiaccount" href="<?php echo public_path(); ?>backend.php/invoiceapiaccounts/new"><?php echo __('New API Account'); ?></a>
</div>
</div>


<div class="panel panel-body panel-body-nopadding ">


<table class="table dt-on-steroids mb0" id="table3">
    <thead>
   <tr>
      <th class="no-sort" style="width: 10px;"><input type='checkbox' name='batchall' onclick="boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.name == 'batch') { if(this.checked == true){ box.checked = true; }else{ box.checked = false; } } } "></th>
      <th width="60">#</th>
 	    <th><?php echo __('Name'); ?></th>
      <th><?php echo __('Branch'); ?></th>
      <th class="no-sort" width="7%"><?php echo __('Actions'); ?></th>
    </tr>
  </thead>
  <tbody id="tblBdy">
    <?php foreach ($invoice_api_acoounts as $account): ?>
    <tr id="row_<?php echo $account->getId() ?>">
	  <td><input type='checkbox' name='batch' id='batch_<?php echo $account->getId() ?>' value='<?php echo $account->getId() ?>'></td>
      <td><?php echo $account->getId(); ?></td>
      <td><?php echo $account->getMdaName(); ?></td>
      <td><?php echo $account->getMdaBranch(); ?></td>
      <td>
		<a id="editaccount<?php echo $account->getId(); ?>" href="<?php echo public_path(); ?>backend.php/invoiceapiaccounts/edit/id/<?php echo $account->getId(); ?>" title="<?php echo __('Edit'); ?>"><span class="badge badge-primary"><i class="fa fa-pencil"></i></span></a>
		<a id="deleteaccount<?php echo $account->getId(); ?>" href="<?php echo public_path(); ?>backend.php/invoiceapiaccounts/delete/id/<?php echo $account->getId(); ?>" onClick="if(confirm('Are you sure you want to delete this item?')){ return true; }else{ return false; }" title="<?php echo __('Delete'); ?>"><span class="badge badge-primary"><i class="fa fa-trash-o"></i></span></a>
  </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
	<tfoot>
   <tr><td colspan='7' style='text-align: left;'>
   <select id='batch_action' name='batch_action' onChange="if(this.value != ''){if(confirm('Are you sure?')){ batch('invoiceapiaccount', this.options[this.selectedIndex].text, this.value); document.getElementById('default').selected='selected'; }}">
   <option id='default' value=''><?php echo __('Choose an action'); ?>..</option>
   <option value='delete'><?php echo __('Set As Deleted'); ?></option>
   </select>
   </td></tr>
   </tfoot>
</table>

</div><!--panel-body-->
</div><!--panel-dark-->
</div>
<script>
jQuery(document).ready(function(){
  jQuery('#table3').dataTable({
      "sPaginationType": "full_numbers",

      // Using aoColumnDefs
      "aoColumnDefs": [
      	{ "bSortable": false, "aTargets": [ 'no-sort' ] }
    	]
    });
});
</script>
<?php
}
else
{
  include_partial("settings/accessdenied");
}
?>
