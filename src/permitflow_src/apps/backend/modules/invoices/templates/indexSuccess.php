<?php
/**
 * indexSuccess.php template.
 *
 * Displays billing stats
 *
 * @package    frontend
 * @subpackage invoices
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");

//For all eCitizen Services the following must be deducted from the total:
 // - KES - Deduct KSH. 50
 // - USB - Deduct USD. 1
 $main_currency = "LSL";
 $agency_manager = new AgencyManager();//OTB - Managing agency access
?>

<div class="pageheader">
  <h2><i class="fa fa-home"></i><?php echo __('Billing'); ?></h2>
  <div class="breadcrumb-wrapper">
    <span class="label"><?php echo __('You are here'); ?>:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>backend.php"><?php echo __('Home'); ?></a></li>
      <li class="active"><?php echo __('Billing'); ?></li>
    </ol>
  </div>
</div>

<div class="contentpanel">
	<div class="panel panel-bordered radius-all">
		<div class="panel-body panel-body-nopadding">

	   <?php if ($pager->getResults()): ?>

	   <table style="border:0px;" class="table mb0 border-left-0 border-right-0 border-bottom-0 panel-table radius-tl  radius-tr">
			<thead class="form-horizontal">
				  <tr>
					   <th class="form-group border-bottom-0 radius-tl" style="width:50%; border:0px;">
						   <form method="post">
							  <input type="text" class="form-control p10" name="search" id="search" placeholder="Search..." value="<?php echo $search; ?>"/>
						   </form>
					  	</th>
					   <th  style="border:0px;" class="form-group border-bottom-0">
							<select size="1" name="table2_length" aria-controls="table2" class="select2" onChange="window.location='/backend.php/invoices/index/filter/' + this.value;">
								<option value="0" selected="selected">Select Product/Service</option>
								<?php
								$q = Doctrine_Query::create()
										->from('ApForms a')
										->where('a.form_id <> 15 AND a.form_id <> 16 AND a.form_id <> 17 AND  a.form_id <> 6 AND a.form_id <> 7')
										->andWhere('a.form_active = 1 AND a.form_type = 1')
										->andWhere('a.payment_enable_merchant = 1')
										->orderBy('a.form_name ASC');
								$applicationforms = $q->execute();
								foreach($applicationforms as $applicationform)
								{
									//if($agency_manager->checkAgencyStageAccess($sf_user->getAttribute('userid'), $applicationform->getFormStage())){//OTB - Managing agency access
										$selected = "";
										if($applicationform->getFormId() == $filter)
										{
											$selected = "selected='selected'";
											$main_currency = $applicationform->getPaymentCurrency();
										}

										echo "<option value='".$applicationform->getFormId()."' ".$selected.">".$applicationform->getFormCode()." - ".$applicationform->getFormName()."</option>";
								//	}
								}
								?>
					  		</select>
					   </th>

					  <th style="width:10%; border:0px;" class="form-group border-bottom-0 radius-tr">
						  <div><form style="margin:0" action="/backend.php/invoices/index<?php if($filter){ echo "/filter/".$filter; } ?><?php if($fromdate){ echo "/fromdate/".$fromdate."/todate/".$todate; } ?><?php if($filter_status){ echo "/filterstatus/".$filter_status; } ?>/export/1" method="post"><input type='hidden' name='export' id='export' value='1'><button class="btn btn-default btn-xs table-billing-btn btn-excell" type="submit">E<span class="hidden-xs">xport</span></button></form></div>
					  </th>
				  </tr>
			</thead>
		</table>


	 <table class="table table-striped table-hover mb0 border-top-0 border-left-0 border-right-0 panel-table">
		 <thead class="form-horizontal">
		  <tr>
		   <form method="post" action="/backend.php/invoices/index/filter/<?php echo $filter; ?>">
		   <th class="border-bottom-1" style="width:40%;">
			  <select style="width:40%;" size="1" name="filter_status" aria-controls="table2" class="select2" <!--onChange="window.location='/backend.php/invoices/index<?php if($filter){ echo "/filter/".$filter; } ?>/filterstatus/' + this.value;"-->>
					<option value="2">Select Status</option>
					<option value="1" <?php if($filter_status == "1"){ echo "selected='selected'"; } ?>>Pending</option>
					<!--<option value="15" <?php if($filter_status == "15"){ echo "selected='selected'"; } ?>>Part Payment</option>-->
					<option value="15" <?php if($filter_status == "15"){ echo "selected='selected'"; } ?>><?php echo __('Pending Confirmation'); ?></option><!--OTB Patch >> Pending Confirmation-->
					<option value="2" <?php if($filter_status == "2"){ echo "selected='selected'"; } ?>>Paid</option>
					<option value="3" <?php if($filter_status == "3"){ echo "selected='selected'"; } ?>>Cancelled</option>
			  </select>
		   </th>
		   <th class="border-bottom-1">
			  <input name="fromdate" value="<?php echo $fromdate; ?>" type="text" class="form-control  p10" placeholder="Starting From" id="datepicker1">
		   </th>
			<th class="border-bottom-1">

			  <input name="todate" value="<?php echo $todate; ?>" type="text" class="form-control  p10" placeholder="Ending" id="datepicker2">
				</th>
				<th style="width:10%;" class="border-bottom-1">
				<button type="submit" class="btn table-billing-btn btn-default">G<span class="hidden-xs">O</span></button>
			   </th>
			   </form>
		  </tr>
		</thead>
	</table>



<div class="table-responsive">

		<table class="table table-striped table-hover mb0 radius-bl radius-br ">
    	<thead>
      	<tr class="main-tr">
		  <th>#</th>
          <th>Date Of Issue</th>
		  <th>Invoice No.</th>
          <th>Application</th>
          <th>User</th>
          <th width="108px">Fee</th>
          <th width="108px">Paid Amount</th>
          <!--<th>Reference Number</th>--><!--OTB - Comment this out for invoices-->
          <th class="aligncenter">Payment Mode</th>
          <th class="aligncenter">Payment Date</th>
          <th width="160px" class="aligncenter">Payment Status</th>
          <th width="10px" class="aligncenter">RC</th>
		  <th class="aligncenter">Action</th>
      </tr>
	 </thead>
	<tbody>
			<?php
			  $count = 0;
			  $invoice_manager = new InvoiceManager();
				foreach($pager->getResults() as $invoice)
				{
					$q = Doctrine_Query::create()
					   ->from("FormEntry a")
					   ->where("a.id = ?", array($invoice->getAppId()));
					$application = $q->fetchOne();
					if($application) {
						$merchant_reference = $invoice_manager->get_merchant_reference($invoice->getId());

						$q = Doctrine_Query::create()
								->from("ApFormPayments a")
								->where("a.payment_id = ?", $merchant_reference)
								->orWhere("a.invoice_id = ?", $invoice->getId());
						$payment = $q->fetchOne();
						?>
						<tr class="main-tr">
							<td><?php echo $invoice->getId(); ?></td>
							<td><?php echo $invoice->getCreatedAt(); ?></td>
							<td><?php if($invoice){ echo $invoice->getInvoiceNumber(); }else{ echo "#"; } ?></td>
							<td><a href="/backend.php/applications/view/id/<?php echo $application->getId(); ?>"><?php echo $application->getApplicationId(); ?></a></td>
							<td><?php echo $application->getUser()->getFullname(); ?></td>
							<td width="108px"><?php echo $invoice->getCurrency(); ?> <?php echo $invoice->getTotalAmount(); ?></td>
							<td width="108px"><?php echo $payment ? $payment->getPaymentCurrency() : False; ?> <?php echo $payment ? $payment->getPaymentAmount() : "No Payment"; ?></td>
							<!--<td><?php echo $payment ? $payment->getPaymentId() : False; ?></td>--><!--OTB - Comment this out for invoices-->
							<td class="aligncenter"><?php echo $payment ? $payment->getPaymentMerchantType() : False; ?></td>
							<td class="aligncenter"><?php echo $payment ? $payment->getPaymentDate() : False; ?></td>
							<td width="160px" class="aligncenter">
							<?php
								if($invoice->getPaid() == "1")
								{
									echo "<span class='badge badge-danger'><strong>Pending</strong></span>";
								}
								elseif($invoice->getPaid() == "15")
								{
									//echo "<span class='badge badge-Info'><strong>Part Payment</span>";
									echo "<span class='badge badge-Info'><strong>Pending Confirmation</span>";//OTB Patch >> Pending Confirmation
								}
								elseif($invoice->getPaid() == "2")
								{
									echo "<a class='badge badge-success'><strong>Paid</strong></a>";
								}
								elseif($invoice->getPaid() == "3")
								{
									echo "<a class='badge btn-danger-alt'>Cancelled</a>";
								}
								?>
							</td>
							<td width="10px" class="aligncenter">
								<?php
								if($invoice)
								{
									if($invoice->getRemoteValidate())
									{
										?>
										<span class="fa fa-flag"></span>
										<?php
									}
									else
									{
										?>
										<span class="fa fa-flag-o"></span>
										<?php
									}
								}
								?>
							</td>
							<td class="aligncenter">
								<?php
								if($invoice) {
									?>
									<a title="View Invoice"
									   href="<?php echo public_path(); ?>backend.php/invoices/view/id/<?php echo $invoice->getId(); ?>"><span
												class="badge badge-primary"><i class="fa fa-eye"></i></span></a>
									<?php
								}
								?>
							</td>
						</tr>
						<?php
					}
	   		 	}
	        ?>
	</tbody>
	<tfoot>
                <tr>
                <th class="radius-bl radius-br" colspan="12">
     <p class="table-showing pull-left"><strong><?php echo count($pager) ?></strong> Invoices

                                            <?php if ($pager->haveToPaginate()): ?>
                                              - page <strong><?php echo $pager->getPage() ?>/<?php echo $pager->getLastPage() ?></strong>
                                            <?php endif; ?></p>




        <?php if ($pager->haveToPaginate()): ?>
                <ul class="pagination pagination-sm mb0 mt0 pull-right">
                  <li><a href="/backend.php/invoices/index/page/1<?php if($filter){ echo "/filter/".$filter; } ?><?php if($fromdate){ echo "/fromdate/".$fromdate."/todate/".$todate; } ?><?php if($filter_status){ echo "/filterstatus/".$filter_status; } ?>">
                      <i class="fa fa-angle-left"></i>
                  </a></li>

                  <li><a href="/backend.php/invoices/index/page/<?php echo $pager->getPreviousPage() ?><?php if($filter){ echo "/filter/".$filter; } ?><?php if($fromdate){ echo "/fromdate/".$fromdate."/todate/".$todate; } ?><?php if($filter_status){ echo "/filterstatus/".$filter_status; } ?>">
                      <i class="fa fa-angle-left"></i>
                  </a></li>

                  <?php foreach ($pager->getLinks() as $page): ?>
                    <?php if ($page == $pager->getPage()): ?>
                      <li class="active"><a href=""><?php echo $page ?></li></a>
                    <?php else: ?>
                      <li><a href="/backend.php/invoices/index/page/<?php echo $page ?><?php if($filter){ echo "/filter/".$filter; } ?><?php if($fromdate){ echo "/fromdate/".$fromdate."/todate/".$todate; } ?><?php if($filter_status){ echo "/filterstatus/".$filter_status; } ?>"><?php echo $page ?></a></li>
                    <?php endif; ?>
                  <?php endforeach; ?>

                  <li><a href="/backend.php/invoices/index/page/<?php echo $pager->getNextPage() ?><?php if($filter){ echo "/filter/".$filter; } ?><?php if($fromdate){ echo "/fromdate/".$fromdate."/todate/".$todate; } ?><?php if($filter_status){ echo "/filterstatus/".$filter_status; } ?>">
                      <i class="fa fa-angle-right"></i>
                  </a></li>

                  <li><a href="/backend.php/invoices/index/page/<?php echo $pager->getLastPage() ?><?php if($filter){ echo "/filter/".$filter; } ?><?php if($fromdate){ echo "/fromdate/".$fromdate."/todate/".$todate; } ?><?php if($filter_status){ echo "/filterstatus/".$filter_status; } ?>">
                      <i class="fa fa-angle-right"></i>
                  </a>
                 </li>
                </ul>
              <?php endif; ?>
                </th>
                </tr>
                </tfoot>
              </table>
             </div><!-- table-responsive -->
			   <div class="table-responsive">
				   <table class="table table-striped table-hover mb0 radius-bl radius-br ">
					   <tbody>
					   <tr><td class="radius-bl radius-br" align="center">
							   <?php
								if($filter) {
									$total_amount = $total->total;

									if($main_currency == "KES")
									{
										$total_amount = $total_amount - (count($pager) * 50);
									}
									elseif($main_currency == "USD")
									{
										$total_amount = $total_amount - (count($pager) * 1);
									}

									?>
									<h1><?php echo $main_currency; ?>. <?php echo $total_amount; ?></h1>
									<?php
								}
							   ?>
						   </td></tr>
					   </tbody>
				   </table>
			   </div>
			<?php else: ?>
			  <div class="table-responsive">
				  <table class="table table-striped table-hover mb0 radius-bl radius-br ">
					<tbody>
					  <tr><td class="radius-bl radius-br">
					No Records Found
					</td></tr>
				  </tbody>
				</table>
			  </div>
			<?php endif; ?>
	</div>
	</div>




</div><!-- panel -->

<script>
jQuery(document).ready(function(){
	// Date Picker
	jQuery('#datepicker1').datepicker();
	jQuery('#datepicker2').datepicker();
});
</script>
