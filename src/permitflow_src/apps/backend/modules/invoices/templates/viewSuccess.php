<?php
/**
 * viewSuccess.php template.
 *
 * Displays a full invoice
 *
 * @package    frontend
 * @subpackage invoices
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");
$application = $invoice->getFormEntry();


$prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";
include_once($prefix_folder.'includes/OAuth.php');

require($prefix_folder.'includes/init.php');

require($prefix_folder.'config.php');
require($prefix_folder.'includes/db-core.php');
require($prefix_folder.'includes/helper-functions.php');
require($prefix_folder.'includes/check-session.php');

require($prefix_folder.'includes/entry-functions.php');
require($prefix_folder.'includes/post-functions.php');
require($prefix_folder.'includes/users-functions.php');

$dbh = mf_connect_db();
$mf_settings = mf_get_settings($dbh);

$invoice_manager = new InvoiceManager();

$invoice_manager->update_invoices($application->getId());

$query = "select * from ".MF_TABLE_PREFIX."form_payments where form_id = ? and record_id = ?";
$params = array($application->getFormId(),$application->getEntryId());
$sth = mf_do_query($query,$params,$dbh);
$count = 0;
while($row = mf_do_fetch_result($sth))
{
    $count++;
    if(!empty($row)){
    $paid = true;

    if($row['billing_state'] != "" && $invoice->getPaid() != 2 && $invoice->getPaid() != 3)
    {
        $invoice = $invoice_manager->update_payment_status($invoice->getId());
    }
}
}
 ?>
 <div class="pageheader">
  <h2><i class="fa fa-envelope"></i> <?php echo __('Billing'); ?></h2>
  <div class="breadcrumb-wrapper">
    <span class="label"><?php echo __('You are here'); ?>:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>index.php"><?php echo __('Home'); ?></a></li>
      <li><a href="<?php echo public_path(); ?>index.php/permits/index"><?php echo __('Billing'); ?></a></li>
    </ol>
  </div>
</div>

<div class="contentpanel">
    <div class="row">

<div class="col-sm-3">
    <div class="blog-item pt20 pb5">

        <h5 class="subtitle mt20 ml20"><?php echo __('Application Summary'); ?></h5>

          <ul class="profile-social-list">
            <li><a href=""><?php
                    $q = Doctrine_Query::create()
                        ->from("SubMenus a")
                        ->where("a.id = ?", $application->getApproved());
                    $stage = $q->fetchOne();
                    if($stage)
                    {
                        $q = Doctrine_Query::create()
                            ->from("Menus a")
                            ->where("a.id = ?", $stage->getMenuId());
                        $parentstage = $q->fetchOne();
                ?><?php echo $parentstage->getTitle(); ?></a></li><?php
                    }

                    $q = Doctrine_Query::create()
                        ->from("SfGuardUserProfile a")
                        ->where("a.user_id = ?", $application->getUserId());
                    $architect = $q->fetchOne();
                ?>
            <li><?php echo __('Submitted by'); ?> <a href="/backend.php/frusers/show/id/<?php echo $application->getUserId(); ?>"> <?php echo $architect->getFullname(); ?></a></li>
            <li><?php echo __('Date of Submission'); ?> <a><?php echo str_replace(" ", " @ ", $application->getDateOfSubmission()); ?></a></li>
            <?php
            if($application->getDateOfResponse())
            {
            ?>
            <li><?php echo __('Date of Approval'); ?> <a><?php echo str_replace(" ", " @ ", $application->getDateOfResponse()); ?></a></li>
            <?php
            }
            ?>
           <?php
            function GetDaysSince($sStartDate, $sEndDate){
                $start_ts = strtotime($sStartDate);
                $end_ts = strtotime($sEndDate);
                $diff = $end_ts - $start_ts;
                return round($diff / 86400);
            }
            $days =  GetDaysSince($application->getDateOfSubmission(), date("Y-m-d H:i:s"));

            $days_color = "";

            if($days < 10){
                    $days_color = "success";
            }
            elseif($days >= 10 && $days < 20){
                    $days_color = "primary";
            }
            elseif($days >= 20 && $days < 30){
                    $days_color = "warning";
            }
            elseif($days >= 30){
                    $days_color = "danger";
            }
           ?>
            <li><?php echo __('Days in progress'); ?> <span

            class="badge badge-<?php echo $days_color; ?>"

            ><strong>
            <?php
            echo $days." ".__('days');
            ?>
            </strong>
            </span>
            </li>
          </ul>

          <div class="mb20"></div>

          <ul class="profile-social-list">
            <li><?php echo __('Application Status'); ?><a href=""> <?php echo $application->getStatusName();  ?> </a></li>
          </ul>

          <div class="mb20"></div>

          <ul class="profile-social-list">
            <li><a href=""> <?php
            $expired = $invoice_manager->is_invoice_expired($invoice->getId());

            if($expired)
            {
                echo "Expired";
            }
            else
            {
              if($invoice->getPaid() == 1)
              {
                  echo __("Pending");
              }
              elseif($invoice->getPaid() == 15)
              {
                  echo __("Part Payment");
              }
              elseif($invoice->getPaid() == 2)
              {
                  echo __("Paid");
              }
            }
                         ?> </a></li>
          </ul>

       </div><!-- blog-item -->

        </div><!-- col-sm-3 -->
        <div class="col-sm-9">

<ul id="myTab" class="nav nav-tabs" style="margin-top:20px; margin-right:20px;">
            <li class="active"><a href="#tabs-1" data-toggle="tab"><?php echo __('Invoice'); ?></a></li>
            <li><a href="#tabs-2" data-toggle="tab"><?php echo __('Payment Details'); ?></a></li>
</ul>
    <div id="myTabContent" class="tab-content" style=" margin-right:20px;">
          <div class="tab-pane fade in active" id="tabs-1">
            <?php
                $invoice_manager = new InvoiceManager();

                try {
                    $html = $invoice_manager->generate_invoice_template($invoice->getId(), false);
                }
                catch(Exception $ex)
                {
                    error_log("Debug-t: Invoice Parse Error: ".$ex);

                    $html = $invoice_manager->generate_invoice_template_old_parser($invoice->getId(), false);
                }

                echo $html;
            ?>

            <div class="text-right btn-invoice" style="padding-right: 10px; padding-top: 10px; padding-bottom: 10px;">
            <?php
            if($invoice->getDocumentKey())
            {
            ?>
                <a class="btn btn-white" id="printinvoice" href="<?php echo $invoice->getDocumentKey(); ?>"><i class="fa fa-print mr5"></i> <?php echo __('Download Invoice'); ?></a>
            <?php
            }
            else {
                ?>
                <a class="btn btn-white" id="printinvoice"
                   href="/backend.php/invoices/print/id/<?php echo $invoice->getId(); ?>"><i
                        class="fa fa-print mr5"></i> <?php echo __("Print Invoice"); ?></a>
            <?php
            }

                if($invoice->getPaid() <> 2 && $invoice->getPaid() <> 3 && $sf_user->mfHasCredential('approvepaymentoverride'))
                {
            ?>
                <a onClick="if(confirm('Are you sure you want to confirm this invoice?')){ return true; }else{ return false; }" class="btn btn-white" id="makepayment" href="/backend.php/invoices/view/id/<?php echo $invoice->getId(); ?>/confirm/<?php echo md5($invoice->getId()); ?>"><i class="fa fa-print mr5"></i> <?php echo __('Confirm Payment'); ?></a>
            <?php
                }

                if($invoice->getPaid() == 3 && $sf_user->mfHasCredential('approvepaymentoverride'))
                {
                    ?>
                    <a class="btn btn-white" id="makepayment" onClick="if(confirm('Are you sure you want to uncancel this invoice?')){ return true; }else{ return false; }" href="/backend.php/invoices/view/id/<?php echo $invoice->getId(); ?>/confirm/<?php echo md5($invoice->getId()); ?>"><i class="fa fa-print mr5"></i> <?php echo __('UnCancel Payment'); ?></a>
                    <?php
                }

                if($invoice->getPaid() <> 3 && $sf_user->mfHasCredential('approvepaymentoverride'))
                {
                    ?>
                    <a class="btn btn-white" id="makepayment" onClick="if(confirm('Are you sure you want to cancel this invoice?')){ return true; }else{ return false; }" href="/backend.php/invoices/view/id/<?php echo $invoice->getId(); ?>/cancel/<?php echo md5($invoice->getId()); ?>"><i class="fa fa-print mr5"></i> <?php echo __('Cancel Payment'); ?></a>
                    <?php
                }
                
                if($invoice->getPaid() <> 2 && $sf_user->mfHasCredential('approvepaymentbyreference'))
                {
                    ?>
                    <a class="btn btn-white" id="makepayment" href="#" data-toggle="modal" data-target="#referenceModal"><i class="fa fa-print mr5"></i> <?php echo __('Attach Reference'); ?></a>
                    <?php
                }
            ?>
            </div>

          </div>
          <div class="tab-pane fade" id="tabs-2">
            <?php

            $paid = false;

            ?>
                <h4><u>Invoice Details</u></h4>

                <form class="form-bordered">
                <div class="form-group">
                    <label class="col-sm-4">
                    <i class="bold-label">
                        MDA Code
                    </i></label>
                    <div class="col-sm-8">
                        <?php echo $invoice->getMdaCode(); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">
                    <i class="bold-label">
                        Service Code
                    </i></label>
                    <div class="col-sm-8">
                        <?php echo $invoice->getServiceCode(); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">
                    <i class="bold-label">
                        Branch
                    </i></label>
                    <div class="col-sm-8">
                        <?php echo $invoice->getBranch(); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">
                    <i class="bold-label">
                        Invoice Number
                    </i></label>
                    <div class="col-sm-8">
                        <?php echo $invoice->getInvoiceNumber(); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">
                    <i class="bold-label">
                        Due Date
                    </i></label>
                    <div class="col-sm-8">
                        <?php echo $invoice->getDueDate(); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">
                    <i class="bold-label">
                        Expires On
                    </i></label>
                    <div class="col-sm-8">
                        <?php echo $invoice->getExpiresAt(); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">
                    <i class="bold-label">
                        Payer ID
                    </i></label>
                    <div class="col-sm-8">
                        <?php echo $invoice->getPayerId(); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">
                    <i class="bold-label">
                        Payer Name
                    </i></label>
                    <div class="col-sm-8">
                        <?php echo $invoice->getPayerName(); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">
                    <i class="bold-label">
                        Total Amount
                    </i></label>
                    <div class="col-sm-8">
                        <?php echo $invoice->getTotalAmount(); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">
                        <i class="bold-label">
                            Balance Due
                        </i></label>
                    <div class="col-sm-8">
                        <?php echo $invoice_manager->get_invoice_total_owed($invoice->getId()); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">
                    <i class="bold-label">
                        Currency
                    </i></label>
                    <div class="col-sm-8">
                        <?php
                        $currency = "";

                        $q = Doctrine_Query::create()
                            ->from("Invoicetemplates a")
                            ->where("a.applicationform = ?", $application->getFormId())
                            ->limit(1);
                        $invoice_template = $q->fetchOne();

                        $q = Doctrine_Query::create()
                            ->from("ApForms a")
                            ->where("a.form_id = ?", $invoice_template->getApplicationform());
                        $form = $q->fetchOne();

                        if($form)
                        {
                          $currency = $form->getPaymentCurrency();
                        }
                        else
                        {
                          $currency = sfConfig::get("app_currency");
                        }

                        echo $currency; ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">
                    <i class="bold-label">
                        Document Reference Number
                    </i></label>
                    <div class="col-sm-8">
                        <?php echo $invoice->getDocRefNumber(); ?>
                    </div>
                </div>
				<!--OTB Start - Include Transaction Reference Number if present-->
                <div class="form-group">
                    <label class="col-sm-4">
                    <i class="bold-label text-capitalize">
                        <?php echo $form->getPaymentMerchant_Type()." ".__('Reference Number'); ?>
                    </i></label>
                    <div class="col-sm-8">
                        <?php
                        $currency = "";

                        $q = Doctrine_Query::create()
                            ->from("ApFormPayments a")
                            ->where("a.form_id = ?", $application->getFormId())
                            ->andWhere("a.record_id = ? and a.payment_status in ('pending','paid')", $application->getEntryId())
							->orderBy('a.afp_id, a.payment_date desc')
                            ->limit(1);
                        $transaction = $q->fetchOne();
                        echo $transaction ? $transaction->getPaymentId() : "n/a";
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">
                    <i class="bold-label text-capitalize">
                        <?php echo $form->getPaymentMerchant_Type()." ".__('Payment Status'); ?>
                    </i></label>
                    <div class="col-sm-8">
                        <?php
                        echo $transaction ? $transaction->getPaymentStatus() : "Transaction not yet posted";
                        ?>
                    </div>
                </div>
				<!--OTB End - Include Transaction Reference Number if present-->
              </form>
			  <h3><u><?php echo __("Payment(s)"); ?></u></h3><!--OTB Patch - Name below accordingly as payments-->
            <?php
            function get_ordinal($number)
            {
                $ends = array('th','st','nd','rd','th','th','th','th','th','th');
                if (($number %100) >= 11 && ($number%100) <= 13)
                    $abbreviation = $number. 'th';
                else
                    $abbreviation = $number. $ends[$number % 10];
                return $abbreviation;
            }

            $q = Doctrine_Query::create()
                ->from("Invoicetemplates a")
                ->where("a.applicationform = ?", $application->getFormId())
                ->limit(1);
            $invoice_template = $q->fetchOne();

            if($invoice_template && $invoice_template->getPaymentType() == 1)
            {
                  //$query = "select * from ".MF_TABLE_PREFIX."form_payments where form_id = ? and record_id = ? order by afp_id desc";
                  $query = "select * from ".MF_TABLE_PREFIX."form_payments where form_id = ? and record_id = ? and payment_status='paid' order by afp_id desc";//OTB Patch - Only show paid transactions
                  $params = array($application->getFormId(),$application->getEntryId());
                  $sth = mf_do_query($query,$params,$dbh);
                  $count = 0;
                  while($row = mf_do_fetch_result($sth))
                  {
                      $count++;
                  if(!empty($row)){
                      ?>
                      <h4><u><?php echo get_ordinal($count); ?> Installment:</u></h4>

                      <form class="form-bordered">
                      <div class="form-group">
                          <label class="col-sm-4">
                          <i class="bold-label">
                              Date Of Payment
                          </i></label>
                          <div class="col-sm-8">
                              <?php echo $row['payment_date']; ?>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-4">
                          <i class="bold-label">
                              Mode of Payment
                          </i></label>
                          <div class="col-sm-8">
                              <?php echo ucfirst($row['payment_merchant_type']); ?>
                          </div>
                      </div>
                          <div class="form-group">
                              <label class="col-sm-4">
                              <i class="bold-label">
                                  Payment Status
                              </i></label>
                              <div class="col-sm-8">
                                  <?php echo ucfirst($row['payment_status']); ?>
                              </div>
                          </div>
                      <div class="form-group">
                          <label class="col-sm-4">
                          <i class="bold-label">
                              Reference Number
                          </i></label>
                          <div class="col-sm-8">
                              <?php echo $row['payment_id']; ?>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-4">
                          <i class="bold-label">
                              Amount Paid
                          </i></label>
                          <div class="col-sm-8">
                              <?php echo sfConfig::get('app_currency'); ?> <?php echo $row['payment_amount']; ?>
                          </div>
                      </div>
                    </form>
                      <?php
                      $paid = true;
                  }

                  break;
              }
            }
            else
            {
              //$query = "select * from ".MF_TABLE_PREFIX."form_payments where form_id = ? and record_id = ?";
              $query = "select * from ".MF_TABLE_PREFIX."form_payments where form_id = ? and record_id = ? and payment_status='paid'";//OTB Patch - Only show paid transactions
              $params = array($application->getFormId(),$application->getEntryId());
              $sth = mf_do_query($query,$params,$dbh);
              $count = 0;
              while($row = mf_do_fetch_result($sth))
              {
                  $count++;
              if(!empty($row)){
                  ?>
                  <h4><u><?php echo get_ordinal($count); ?> Installment:</u></h4>

                  <form class="form-bordered">
                  <div class="form-group">
                      <label class="col-sm-4">
                      <i class="bold-label">
                          Date Of Payment
                      </i></label>
                      <div class="col-sm-8">
                          <?php echo $row['payment_date']; ?>
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-4">
                      <i class="bold-label">
                          Mode of Payment
                      </i></label>
                      <div class="col-sm-8">
                          <?php echo ucfirst($row['payment_merchant_type']); ?>
                      </div>
                  </div>
                      <div class="form-group">
                          <label class="col-sm-4">
                          <i class="bold-label">
                              Payment Status
                          </i></label>
                          <div class="col-sm-8">
                              <?php echo ucfirst($row['payment_status']); ?>
                          </div>
                      </div>
                  <div class="form-group">
                      <label class="col-sm-4">
                      <i class="bold-label">
                          Reference Number
                      </i></label>
                      <div class="col-sm-8">
                          <?php echo $row['payment_id']; ?>
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-4">
                      <i class="bold-label">
                          Amount Paid
                      </i></label>
                      <div class="col-sm-8">
                          <?php echo sfConfig::get('app_currency'); ?> <?php echo $row['payment_amount']; ?>
                      </div>
                  </div>
                </form>
                  <?php
                  $paid = true;
              }
          }
        }
            $receipts = $invoice->getUploadReceipt();

            if(sizeof($receipts) > 0)
            {

                $count = 0;
                foreach($receipts as $receipt)
                {
                  $count++;
                ?>
                <h4><a href="#"><?php echo __('Receipt'); ?> <?php echo $count; ?></a></h4>
                <div>
                  <p style="font-size: 12px; font-family:Times New Roman,'Georgia',Serif;">
                                <?php echo "<a target='_blank' href='/backend.php/invoices/viewreceipt?form_id=".$receipt->getFormId()."&id=".$receipt->getEntryId()."'>(".__("View Receipt").")</a>"; ?>
                  </p>
                </div>
                <?php

                }
            }

            if(sizeof($receipts) == 0 && $paid == false)
            {
            ?>
            <h4><a href="#"><?php echo __('Receipts'); ?></a></h4>
                <div>
                    <p style="font-size: 12px; font-family:Times New Roman,'Georgia',Serif;">

                        <div class="table-responsive">
                        <table class="table table-bordered mb0">
                          <tbody>
                                <tr>
                                <td colspan="4" class="aligned">
                                <h4><?php echo __('No Payments Made'); ?></h4>

                                </td>
                              </tr>
                          </tbody>
                        </table>
                        </div><!--Responsive-table-->
                    </p>
                </div>
            <?php
            }
            ?>
            </div>

</div>

</div>

</div><!-- /.row -->



</div><!-- /.marketing -->

<!-- Modal -->
<div class="modal fade" id="referenceModal" tabindex="-1" role="dialog" aria-labelledby="referenceModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <form class="form" action="<?php echo public_path(); ?>backend.php/invoices/view/id/<?php echo $invoice->getId(); ?>" method="post" autocomplete="off">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="referenceModalLabel"><?php echo __('Attach reference number to invoice'); ?></h4>
      </div>
      <div class="modal-body modal-body-nopadding" id="newtask">
            <div class="form-group">
                <label class="col-sm-4"><?php echo __('Reference'); ?></label>
                <div class="col-sm-8 pull-right">
                    <?php echo $invoice->getFormEntry()->getFormId(); ?> / <?php echo $invoice->getFormEntry()->getEntryId(); ?> / <input type="text" class="form-control" name="remote_reference" id="remote_reference" style="width: 150px;"> 
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close'); ?></button>
        <button type="submit" class="btn btn-primary"><?php echo __('Update Invoice'); ?></button>
      </div>
    </form>
    </div><!-- modal-content -->
  </div><!-- modal-dialog -->
</div><!-- modal -->
