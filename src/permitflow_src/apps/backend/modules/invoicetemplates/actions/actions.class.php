<?php

/**
 * invoicetemplates actions.
 *
 * @package    invoicetemplates
 * @subpackage invoicetemplates
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class invoicetemplatesActions extends sfActions
{
    /**
     * Executes 'Checkname' action
     *
     * Ajax used to check existence of name
     *
     * @param sfRequest $request A request object
     */
    public function executeCheckname(sfWebRequest $request)
    {
        // add new user
        $q = Doctrine_Query::create()
           ->from("invoicetemplates a")
           ->where('a.title = ?', $request->getPostParameter('name'));
        $existinggroup = $q->execute();
        if(sizeof($existinggroup) > 0)
        {
              echo '<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Name is already in use!</strong></div><script language="javascript">document.getElementById("submitbuttonname").disabled = true;</script>';
              exit;
        }
        else
        {
              echo '<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Name is available!</strong></div><script language="javascript">document.getElementById("submitbuttonname").disabled = false;</script>';
              exit;
        }
    }

  public function executeBatch(sfWebRequest $request)
  {
		if($request->getPostParameter('delete'))
		{
			$item = Doctrine_Core::getTable('invoicetemplates')->find(array($request->getPostParameter('delete')));
			if($item)
			{
				$item->delete();
			}
		}
    }

    public function executeAjaxnew(sfWebRequest $request)
    {
        $this->form = new InvoicetemplatesForm();
        $this->setLayout(false);
    }

    public function executeAjaxcreate(sfWebRequest $request)
    {
        $this->forward404Unless($request->isMethod(sfRequest::POST));

        $this->form = new InvoicetemplatesForm();

        $this->ajaxprocessForm($request, $this->form);

        $this->setTemplate('new');
    }

    public function executeAjaxedit(sfWebRequest $request)
    {
        $this->forward404Unless($invoicetemplates = Doctrine_Core::getTable('invoicetemplates')->find(array($request->getParameter('id'))), sprintf('Object permits does not exist (%s).', $request->getParameter('id')));
        $this->form = new InvoicetemplatesForm($invoicetemplates);
        $this->setLayout(false);
    }

    public function executeAjaxindex(sfWebRequest $request)
    {
        $this->templates = Doctrine_Core::getTable('invoicetemplates')
            ->createQuery('a')
            ->execute();
        $this->setLayout(false);
    }

    public function executeAjaxupdate(sfWebRequest $request)
    {
        $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
        $this->forward404Unless($invoicetemplates = Doctrine_Core::getTable('invoicetemplates')->find(array($request->getParameter('id'))), sprintf('Object permits does not exist (%s).', $request->getParameter('id')));
        $this->form = new InvoicetemplatesForm($invoicetemplates);

        $this->ajaxprocessForm($request, $this->form);

        $this->setTemplate('edit');
    }

  public function executeIndex(sfWebRequest $request)
  {
    if($request->getParameter("filter"))
    {
      //Save filter to session
      $this->getUser()->setAttribute('filter', $request->getParameter("filter"));
      //
      $q = Doctrine_Query::create()
       ->from("SubMenus a")
       ->where("a.menu_id = ?", $request->getParameter("filter", $this->getUser()->getAttribute("filter")));
    $stages = $q->execute();

    $stages_array = array();

    foreach($stages as $stage)
    {
        $stages_array[] = "a.applicationstage = ".$stage->getId();
    }

    $stages_query = implode(" OR ", $stages_array);

    $q = Doctrine_Query::create()
       ->from("Invoicetemplates a")
       ->where($stages_query);
    $this->templates = $q->execute();
    } //OTB patch - Show all invoices
    else {
        $this->getUser()->setAttribute('filter', 'allinvoices'); // Show all
         $q = Doctrine_Query::create()
        ->from("Invoicetemplates a");
       $this->templates = $q->execute();
    }

    

    $this->setLayout("layout-settings");
  }


    public function executeGetsettings(sfWebRequest $request)
    {
        $this->form = $request->getParameter('id');
        $this->setLayout(false);
    }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new InvoicetemplatesForm();
    $this->setLayout("layout-settings");
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new InvoicetemplatesForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($invoicetemplates = Doctrine_Core::getTable('invoicetemplates')->find(array($request->getParameter('id'))), sprintf('Object permits does not exist (%s).', $request->getParameter('id')));
    $this->form = new InvoicetemplatesForm($invoicetemplates);
    $this->setLayout("layout-settings");
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($invoicetemplates = Doctrine_Core::getTable('invoicetemplates')->find(array($request->getParameter('id'))), sprintf('Object permits does not exist (%s).', $request->getParameter('id')));
    $this->form = new InvoicetemplatesForm($invoicetemplates);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {

    $this->forward404Unless($invoicetemplates = Doctrine_Core::getTable('invoicetemplates')->find(array($request->getParameter('id'))), sprintf('Object permits does not exist (%s).', $request->getParameter('id')));
    $invoicetemplates->delete();

    $this->redirect('/backend.php/invoicetemplates/index');
  }

    protected function ajaxprocessForm(sfWebRequest $request, sfForm $form)
    {
        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
        if ($form->isValid())
        {
            $invoicetemplates = $form->save();
        }
        else
        {
            echo "Invalid Form";
        }

        $this->redirect('/backend.php/dashboard');
    }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $invoicetemplates = $form->save();
    }
    else
    {
      echo "Invalid Form";
    }
    $this->redirect('/backend.php/invoicetemplates/index');
  }
}
