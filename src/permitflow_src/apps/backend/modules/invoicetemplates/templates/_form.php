<?php
use_helper("I18N");
$_SESSION['current_module'] = "invoicetemplates";
$_SESSION['current_action'] = "index";
$_SESSION['current_id'] = "";
?>
<div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
  <strong><?php echo __('Well done'); ?>!</strong> <?php echo __('You successfully updated this record'); ?></a>.
</div>

<form id="invoicetemplatesform" class="form-bordered" action="/backend.php/<?php echo 'invoicetemplates/'.($form->getObject()->isNew() ? 'create' : 'update'.(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post">
<div class="panel-body panel-body-nopadding">
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
<?php echo $form->renderHiddenFields(false) ?>
<?php echo $form->renderGlobalErrors() ?>

      <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Invoice Title'); ?></i></label><br>
        <div class="col-sm-12 rogue-input">
          <?php echo $form['title']->renderError() ?>
          <?php echo $form['title'] ?>
        </div>
      </div>

	  <div id="nameresult" name="nameresult"></div>

      <script language="javascript">
        $('document').ready(function(){
          $('#invoicetemplates_title').keyup(function(){
            $.ajax({
                      type: "POST",
                      url: "/backend.php/invoicetemplates/checkname",
                      data: {
                          'name' : $('input:text[id=invoicetemplates_title]').val()
                      },
                      dataType: "text",
                      success: function(msg){
                            //Receiving the result of search here
                            $("#nameresult").html(msg);
                      }
                  });
              });
        });
      </script>
      <div class="form-group">
        <div class="col-sm-12">
          <label class="col-sm-4"><i class="bold-label"><?php echo __('Application Form'); ?></i></label><br>
          <?php echo $form['applicationform']->renderError() ?>
          <select name="invoicetemplates[applicationform]" class="form-control" id="invoicetemplates_applicationform"  onChange="ajaxFetchPermitsettings(this.value);">
			<option value=""></option>
			<?php
              $q = Doctrine_Query::create()
                 ->from("SubMenus a") ;
                      //OTB patch - show all stages
                    if($sf_user->getAttribute('filter') != 'allinvoices'){
                         $q->where("a.menu_id = ?", $sf_user->getAttribute('filter'));
                    }
                
              $stages = $q->execute();

              $stages_array = array();

              foreach($stages as $stage)
              {
                  $stages_array[] = "a.form_stage = ".$stage->getId();
              }

              $stages_query = implode(" OR ", $stages_array);

			       $q = Doctrine_Query::create()
                  ->from('ApForms a')
				           ->where('a.form_id <> 15 AND a.form_id <> 16 AND a.form_id <> 17 AND  a.form_id <> 6 AND a.form_id <> 7')
                  ->andWhere('a.form_active = 1 AND a.form_type = 1')
                  ->andWhere($stages_query)
                  ->orderBy('a.form_name ASC');
              $applicationforms = $q->execute();
              foreach($applicationforms as $appform)
              {
                  error_log("This is a bug here ") ;
				  if($form->getObject()->isNew())
				  {
					echo "<option value='".$appform->getFormId()."'>".$appform->getFormName().'('.$appform->getFormDescription().')'."</option>";
				  }
				  else
				  {
				    if($form->getObject()->getApplicationform() == $appform->getFormId())
					{
						echo "<option value='".$appform->getFormId()."' selected>".$appform->getFormName().'('.$appform->getFormDescription().')'."</option>";
					}
					else
					{
						echo "<option value='".$appform->getFormId()."'>".$appform->getFormName().'('.$appform->getFormDescription().')'."</option>";
					}
				  }
			  }
			?>
		</select>
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-12">
          <label class="col-sm-4"><i class="bold-label"><?php echo __('Application Stage'); ?></i></label><br>
          <?php echo $form['applicationstage']->renderError() ?>
          <select name="invoicetemplates[applicationstage]" class="form-control" id="invoicetemplates_applicationstage">
			<option><?php echo __('Choose a stage'); ?></option>
				<?php
				$q = Doctrine_Query::create()
				   ->from("Menus a")
           //->where("a.id = ?", $sf_user->getAttribute('filter'))
				   ->orderBy("a.title ASC");
                                 //OTB patch - show all stages
                    if($sf_user->getAttribute('filter') != 'allinvoices'){
                        $q->where("a.id = ?", $sf_user->getAttribute('filter')) ;
                    }
				$stage_groups = $q->execute();

				foreach($stage_groups as $stage_group)
				{
					?>
					<optgroup label="<?php echo $stage_group->getTitle(); ?>">
					<?php
						$q = Doctrine_Query::create()
						   ->from("SubMenus a")
						   ->where("a.menu_id = ?", $stage_group->getId())
						   ->andWhere("a.deleted = 0")
						   ->orderBy("a.order_no ASC");
						$stages = $q->execute();

						foreach($stages as $stage)
						{
							$selected = "";
							if(!$form->getObject()->isNew())
							{
								if($stage->getId() == $form->getObject()->getApplicationstage())
								{
									$selected = "selected='selected'";
								}
							}
							?>
							<option value="<?php echo $stage->getId(); ?>" <?php echo $selected; ?>><?php echo $stage->getTitle(); ?></option>
							<?php
						}
					?>
					</optgroup>
					<?php
				}
				?>
		</select>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-12"><i class="bold-label"><?php echo __('Invoice number of the first invoice'); ?></i></label><br>
        <div class="col-sm-12">
          <?php echo $form['invoice_number']->renderError() ?>
          <input type="text" class="form-control" name="invoicetemplates[invoice_number]" id="invoicetemplates_invoice_number" value="<?php if(!$form->getObject()->isNew()){ echo $form->getObject()->getInvoiceNumber(); }else{ echo "0"; } ?>">
        </div>
      </div>
		<!--OTB Start - option to set invoice number to application number-->
        <div class="form-group">
            <label class="col-sm-12"><i class="bold-label"><?php echo __('Set Invoice Number of invoices to their corresponding application numbers?'); ?></i></label><br>
            <div class="col-sm-12">
                <?php echo $form['use_application_number']->renderError() ?>
                <select class="form-control" name="invoicetemplates[use_application_number]" id="invoicetemplates_use_application_number">
                    <?php
                    $selected = "";
                    if(!$form->getObject()->isNew())
                    {
                        if($form->getObject()->getUseApplicationNumber() == 1)
                        {
                            $selected = "selected='selected'";
                        }
                    }
                    ?>
                    <option value="1" <?php echo $selected; ?>>Yes</option>
                    <?php
                    $selected = "";
                    if(!$form->getObject()->isNew())
                    {
                        if($form->getObject()->getUseApplicationNumber() == 0)
                        {
                            $selected = "selected='selected'";
                        }
                    }
                    ?>
                    <option value="0" <?php echo $selected; ?>>No</option>
                </select>
            </div>
        </div>
		<!--OTB End - option to set invoice number to application number-->
        <div class="form-group">
            <label class="col-sm-12"><i class="bold-label"><?php echo __('Type of payment'); ?></i></label><br>
            <div class="col-sm-12">
                <?php echo $form['max_duration']->renderError() ?>
                <select class="form-control" name="invoicetemplates[payment_type]" id="invoicetemplates_payment_type">
                    <?php
                    $selected = "";
                    if(!$form->getObject()->isNew())
                    {
                        if($form->getObject()->getPaymentType() == "1")
                        {
                            $selected = "selected='selected'";
                        }
                    }
                    ?>
                    <option value="1" <?php echo $selected; ?>>Allow only full payments</option>
                    <?php
                    $selected = "";
                    if(!$form->getObject()->isNew())
                    {
                        if($form->getObject()->getPaymentType() == "2")
                        {
                            $selected = "selected='selected'";
                        }
                    }
                    ?>
                    <option value="2" <?php echo $selected; ?>>Allow partial payments</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-12"><i class="bold-label"><?php echo __('Type of expiration'); ?></i></label><br>
            <div class="col-sm-12">
                <?php echo $form['max_duration']->renderError() ?>
                <select class="form-control" name="invoicetemplates[expiration_type]" id="invoicetemplates_expiration_type">
                    <?php
                    $selected = "";
                    if(!$form->getObject()->isNew())
                    {
                        if($form->getObject()->getExpirationType() == "1")
                        {
                            $selected = "selected='selected'";
                        }
                    }
                    ?>
                    <option value="1" <?php echo $selected; ?>>Expires after a specified number of days</option>
                    <?php
                    $selected = "";
                    if(!$form->getObject()->isNew())
                    {
                        if($form->getObject()->getExpirationType() == "2")
                        {
                            $selected = "selected='selected'";
                        }
                    }
                    ?>
                    <option value="2" <?php echo $selected; ?>>Expires at the end of each month</option>
                    <?php
                    $selected = "";
                    if(!$form->getObject()->isNew())
                    {
                        if($form->getObject()->getExpirationType() == "3")
                        {
                            $selected = "selected='selected'";
                        }
                    }
                    ?>
                    <option value="3" <?php echo $selected; ?>>Expires at the end of each year</option>
                </select>
            </div>
        </div>
      <div class="form-group">
        <label class="col-sm-12"><i class="bold-label"><?php echo __('Maximum number of days before expiration'); ?></i></label><br>
        <div class="col-sm-12">
          <?php echo $form['max_duration']->renderError() ?>
          <input type="text" class="form-control" name="invoicetemplates[max_duration]" id="invoicetemplates_max_duration" value="<?php if(!$form->getObject()->isNew()){ echo $form->getObject()->getMaxDuration(); }else{ echo "0"; } ?>">
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-12"><i class="bold-label"><?php echo __('Maximum number of days till due date'); ?></i></label><br>
        <div class="col-sm-12">
          <?php echo $form['due_duration']->renderError() ?>
          <input type="text" class="form-control" name="invoicetemplates[due_duration]" id="invoicetemplates_due_duration" value="<?php if(!$form->getObject()->isNew()){ echo $form->getObject()->getDueDuration(); }else{ echo "0"; } ?>">
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Content'); ?></i></label><br>
        <div class="col-sm-12">
          <?php echo $form['content']->renderError() ?>
		  <?php
		  if($form->getObject()->isNew())
		  {
		  ?>
          <textarea class="form-control" rows="30" name="invoicetemplates[content]" id="invoicetemplates_content" ></textarea>
		  <?php
		  }
		  else
		  {
		  ?>
          <textarea class="form-control" rows="30" name="invoicetemplates[content]" id="invoicetemplates_content" ><?php echo $form->getObject()->getContent(); ?></textarea>
		  <?php
		  }
		  ?>
      <?php
      if(sfConfig::get('app_disable_ckeditor') != "true")
      {
      ?>
      <script src="<?php echo public_path(); ?>assets_unified/js/ckeditor/ckeditor.js"></script>
      <script src="<?php echo public_path(); ?>assets_unified/js/ckeditor/adapters/jquery.js"></script>

      <script>
      jQuery(document).ready(function(){

        // CKEditor
        jQuery('#invoicetemplates_content').ckeditor();

      });
      </script>
      <?php
		  }
		  ?>
        </div>
      <div class="form-group">
		<div class="col-sm-12 alignright">
		  <button type="button" class="btn btn-primary" data-target="#fieldsModal" data-toggle="modal">View available user/form fields</button>
		</div>
		  <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="fieldsModal" class="modal fade" style="display: none;">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
			        <h4 id="myModalLabel" class="modal-title">View available user/form fields</h4>
			      </div>
			      <div class="modal-body">
			        <div class="form-group">
		 <?php
		  if(!$form->getObject()->isNew())
		  {
		    $appform = $form->getObject()->getApplicationform();
		  ?>
			<?php
			//Get User Information (anything starting with sf_ )
					   //sf_email, sf_fullname, sf_username, ... other fields in the dynamic user profile form e.g sf_element_1
			?>

			<table class="table dt-on-steroids mb0">
			<thead><tr><th width="50%"><?php echo __('Applicant Details'); ?></th><th><?php echo __('Tag'); ?></th></tr></thead>
			<tbody>
            <tr><td><?php echo __('User ID'); ?></td><td>{sf_username}</td></tr>
            <tr><td><?php echo __('Mobile Number'); ?></td><td>{sf_mobile}</td></tr>
			<tr><td><?php echo __('Email'); ?></td><td>{sf_email}</td></tr>
			<tr><td><?php echo __('Full Name'); ?></td><td>{sf_fullname}</td></tr>
			<?php
					$q = Doctrine_Query::create()
					   ->from('apFormElements a')
					   ->where('a.form_id = ?', 15)
             ->andWhere('a.element_status = ?', 1);

					$elements = $q->execute();

					foreach($elements as $element)
					{
						$childs = $element->getElementTotalChild();
						if($childs == 0)
						{
						   echo "<tr><td>".$element->getElementTitle()."</td><td>{sf_element_".$element->getElementId()."}</td></tr>";
						}
						else
						{
							if($element->getElementType() == "select")
							{
								echo "<tr><td>".$element->getElementTitle()."</td><td>{sf_element_".$element->getElementId()."}</td></tr>";
							}
							else
							{
								for($x = 0; $x < ($childs + 1); $x++)
								{
									echo "<tr><td>".$element->getElementTitle()."</td><td>{sf_element_".$element->getElementId()."_".($x+1)."}</td></tr>";
								}
							}
						}
					}
			?>
			</tbody>
			</table>
			<?php
			//Get Application Information (anything starting with ap_ )
					   //ap_application_id

			//Get Form Details (anything starting with fm_ )
					   //fm_created_at, fm_updated_at.....fm_element_1
			?>
			<table class="table dt-on-steroids mb0">
			<thead><tr><th width="50%"><?php echo __('Application Details'); ?></th><th><?php echo __('Tag'); ?></th></tr></thead>
			<tbody>
			<tr><td><?php echo __('Application Number'); ?></td><td>{ap_application_id}</td></tr>
			<tr><td><?php echo __('Created At'); ?></td><td>{fm_created_at}</td></tr>
			<tr><td><?php echo __('Approved At'); ?></td><td>{fm_updated_at}</td></tr>
     <?php
      if(!$form->getObject()->isNew())
      {
        $appform = $form->getObject()->getApplicationform();
      ?>
      <?php

          $q = Doctrine_Query::create()
             ->from('apFormElements a')
             ->where('a.form_id = ?', $appform)
             ->andWhere('a.element_status = ?', 1)
             ->orderBy('a.element_position ASC');

          $elements = $q->execute();

          foreach($elements as $element)
          {
            $childs = $element->getElementTotalChild();
            if($childs == 0)
            {
               if($element->getElementType() == "select")
               {
                 if($element->getElementExistingForm() && $element->getElementExistingStage())
                 {
                   $q = Doctrine_Query::create()
                    ->from("ApForms a")
                    ->where("a.form_id = ?", $element->getElementExistingForm());
                   $child_form = $q->fetchOne();

                   echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."} ";
                     echo '<table class="table dt-on-steroids mb0">
                     <thead><tr><th width="50%">'.__($child_form->getFormName().' Details').'</th><th>'.__('Tag').'</th></tr></thead>
                     <tbody>';

                      ?>
                      <tr><td><?php echo __('Application Number'); ?></td><td>{ap_child_application_id}</td></tr>
                      <tr><td><?php echo __('Created At'); ?></td><td>{fm_child_created_at}</td></tr>
                      <tr><td><?php echo __('Approved At'); ?></td><td>{fm_child_updated_at}</td></tr>
                      <?php
                        $q = Doctrine_Query::create()
                           ->from("Permits a")
                           ->where("a.applicationform = ?", $element->getElementExistingForm());
                        $permits = $q->execute();

                        foreach($permits as $permit)
                        {
                            echo "<tr><td>".$permit->getTitle()." ID</td><td>{ap_permit_id_".$permit->getId()."_element_child}</td></tr>";
                        }

                      $q = Doctrine_Query::create()
                         ->from('apFormElements a')
                         ->where('a.form_id = ?', $element->getElementExistingForm())
                         ->andWhere('a.element_status = ?', 1)
                         ->orderBy('a.element_position ASC');

                      $child_elements = $q->execute();

                      foreach($child_elements as $child_element)
                      {

                          //START CHILD ELEMENTS
                          $childs = $child_element->getElementTotalChild();
                          if($childs == 0)
                          {
                             if($child_element->getElementType() == "select")
                             {
                               if($child_element->getElementExistingForm() && $child_element->getElementExistingStage())
                               {

                                 $q = Doctrine_Query::create()
                                  ->from("ApForms a")
                                  ->where("a.form_id = ?", $child_element->getElementExistingForm());
                                 $grand_form = $q->fetchOne();

                                   echo "<tr><td>".$child_element->getElementTitle()."</td><td>{fm_child_element_".$child_element->getElementId()."} ";
                                   echo '<table class="table dt-on-steroids mb0">
                                   <thead><tr><th width="50%">'.__($grand_form->getFormName().' Details').'</th><th>'.__('Tag').'</th></tr></thead>
                                   <tbody>';

                                    $q = Doctrine_Query::create()
                                       ->from('apFormElements a')
                                       ->where('a.form_id = ?', $child_element->getElementExistingForm())
                                       ->andWhere('a.element_status = ?', 1)
                                       ->orderBy('a.element_position ASC');

                                    $grand_child_elements = $q->execute();

                                    foreach($grand_child_elements as $grand_child_element)
                                    {
                                      ?>
                                      <tr><td><?php echo __('Application Number'); ?></td><td>{ap_grand_child_application_id}</td></tr>
                                      <tr><td><?php echo __('Created At'); ?></td><td>{fm_grand_child_created_at}</td></tr>
                                      <tr><td><?php echo __('Approved At'); ?></td><td>{fm_grand_child_updated_at}</td></tr>
                                      <?php
                                       $q = Doctrine_Query::create()
                                          ->from("Permits a")
                                          ->where("a.applicationform = ?", $child_element->getElementExistingForm());
                                       $permits = $q->execute();

                                       foreach($permits as $permit)
                                       {
                                           echo "<tr><td>".$permit->getTitle()." ID</td><td>{ap_permit_id_".$permit->getId()."_element_grand_child}</td></tr>";
                                       }

                                        //START GRAND CHILD ELEMENTS
                                        $childs = $grand_child_element->getElementTotalChild();
                                        if($childs == 0)
                                        {
                                           if($grand_child_element->getElementType() == "select")
                                           {
                                               echo "<tr><td>".$grand_child_element->getElementTitle()."</td><td>{fm_grand_child_element_".$grand_child_element->getElementId()."}</td></tr>";
                                           }
                                           else
                                           {
                                              echo "<tr><td>".$grand_child_element->getElementTitle()."</td><td>{fm_grand_child_element_".$grand_child_element->getElementId()."}</td></tr>";
                                           }
                                        }
                                        else
                                        {
                                            for($x = 0; $x < ($childs + 1); $x++)
                                            {
                                              echo "<tr><td>".$grand_child_element->getElementTitle()."</td><td>{fm_grand_child_element_".$grand_child_element->getElementId()."_".($x+1)."}</td></tr>";
                                            }
                                        }
                                        //END GRAND CHILD ELEMENTS
                                    }

                                   echo '</tbody></table>';
                                 echo "</td></tr>";
                               }
                               else
                               {
                                 echo "<tr><td>".$child_element->getElementTitle()."</td><td>{fm_child_element_".$child_element->getElementId()."}</td></tr>";
                               }
                             }
                             else
                             {
                                echo "<tr><td>".$child_element->getElementTitle()."</td><td>{fm_child_element_".$child_element->getElementId()."}</td></tr>";
                             }
                          }
                          else
                          {
                              for($x = 0; $x < ($childs + 1); $x++)
                              {
                                echo "<tr><td>".$child_element->getElementTitle()."</td><td>{fm_child_element_".$child_element->getElementId()."_".($x+1)."}</td></tr>";
                              }
                          }
                          //END CHILD ELEMENTS
                      }

                     echo '</tbody></table>';
                   echo "</td></tr>";
                 }
                 else
                 {
                   echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."}</td></tr>";
                 }
               }
               else
               {
                  echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."}</td></tr>";
               }
            }
            else
            {
                for($x = 0; $x < ($childs + 1); $x++)
                {
                  echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."_".($x+1)."}</td></tr>";
                }
            }
          }
      ?>
      <?php
      }
      ?>
			</tbody>
			</table>

			<table class="table dt-on-steroids mb0">
			<thead><tr><th width="50%"><?php echo __('Conditions Of Approval'); ?></th><th><?php echo __('Tag'); ?></th></tr></thead>
			<tbody>
			<tr><td><?php echo __('Conditions Of Approval'); ?></td><td>{ca_conditions}</td></tr>
			</tbody>
			</table>

		  <?php
		  }
		  ?>
			<table class="table dt-on-steroids mb0">
			<thead><tr><th width="50%"><?php echo __('Invoice Details'); ?></th><th><?php echo __('Tag'); ?></th></tr></thead>
			<tbody>
			<tr><td><?php echo __('Invoice No'); ?></td><td>{inv_no}</td></tr>
			<tr><td><?php echo __('Invoice Date'); ?></td><td>{inv_date_created}</td></tr>
      		<tr><td><?php echo __('Invoice Expiry Date'); ?></td><td>{inv_expires_at}</td></tr>
			<tr><td><?php echo __('List of Fees'); ?></td><td>{inv_fee_table}</td></tr>
			<tr><td><?php echo __('Total'); ?></td><td>{inv_total}</td></tr>
            <tr><td><?php echo __('Balance Due'); ?></td><td>{inv_balance}</td></tr>
            <tr><td><?php echo __('Invoice Status'); ?></td><td>{inv_status}</td></tr>
            <tr><td><?php echo __('Payment Mode'); ?></td><td>{inv_payment_merchant_type}</td></tr>
            <tr><td><?php echo __('Payment Reference Number'); ?></td><td>{inv_payment_id}</td></tr>
            <tr><td><?php echo __('Bar Code'); ?></td><td>{bar_code}</td></tr>
            <tr><td><?php echo __('Bar Code (Small)'); ?></td><td>{bar_code_small}</td></tr>
			</tbody>
			</table>
    </div>
	      <div class="modal-footer">
	        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
	      </div>
	    </div><!-- modal-content -->
	  </div><!-- modal-dialog -->
	</div>
		  </div>
		  <div class="form-group">
	        <div class="col-sm-12" id="loadinner" name="loadinner">

	        </div>

	        <?php
	          $invoiceid = 0;
			  if(!$form->getObject()->isNew())
			  {
			  		$invoiceid = $form->getObject()->getId();
			  }
	        ?>
	      </div>

<div class="panel-footer">
            <button class='btn btn-danger mr10'><?php echo __('Reset'); ?></button><button type="submit" class='btn btn-primary' name="submitbuttonname" id="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button></div>
</div>
</div>
</form>
