<?php
use_helper("I18N");

$audit = new Audit();
$audit->saveAudit("", "Accessed invoice template settings");
?>
<div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
  <strong><?php echo __('Well done'); ?>!</strong> <?php echo __('You successfully updated this record'); ?></a>.
</div>

<?php
if($sf_user->mfHasCredential("manageinvoices"))
{
  $_SESSION['current_module'] = "invoicetemplates";
  $_SESSION['current_action'] = "index";
  $_SESSION['current_id'] = "";
?>
<div class="panel panel-dark">
<div class="panel-heading">
			<h3 class="panel-title"><?php echo __('Invoice Templates'); ?></h3>
				<div class="pull-right">
            <a class="btn btn-primary-alt settings-margin42" id="newtemplate" href="#"><?php echo __('New Template'); ?></a>

            <script language="javascript">
            jQuery(document).ready(function(){
              $( "#newtemplate" ).click(function() {
                  $("#contentload").load("<?php echo public_path(); ?>backend.php/invoicetemplates/ajaxnew");
              });
            });
            </script>
</div>
</div>


<div class="panel panel-body panel-body-nopadding ">

<div class="table-responsive">
<table class="table dt-on-steroids mb0" id="table3">
  <thead>
    <tr>
      <th class="no-sort"><input type='checkbox' name='batchall' onclick="boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.name == 'batch') { if(this.checked == true){ box.checked = true; }else{ box.checked = false; } } } "></th>
      <th width="60">#</th>
      <th><?php echo __('Title'); ?></th>
      <th class="no-sort"><?php echo __('Application Form'); ?></th>
      <th class="no-sort"><?php echo __('Application Stage'); ?></th>
      <th class="no-sort" width="7%"><?php echo __('Actions'); ?></th>
    </tr>
  </thead>
  <tbody>
    <?php
		$count = 1;
	?>
    <?php foreach ($templates as $template): ?>
    <tr id="row_<?php echo $template->getId() ?>">
      <td><input type='checkbox' name='batch' id='batch_<?php echo $template->getId() ?>' value='<?php echo $template->getId() ?>'></td>

      <td><?php echo $count++; ?></td>
      <td><?php echo $template->getTitle() ?></td>
      <td><?php
	    $q = Doctrine_Query::create()
		  ->from('ApForms a')
		  ->where('a.form_id = ?', $template->getApplicationform());
		$form = $q->fetchOne();
    if($form)
    {
  		echo $form->getFormName();
    }
	  ?></td>
      <td><?php
      $q = Doctrine_Query::create()
      ->from('SubMenus a')
      ->where('a.id = ?', $template->getApplicationstage());
    $stage = $q->fetchOne();
    if($stage)
    {
      echo $stage->getTitle();
    }
    ?></td>
        <td>
            <a id="edittemplate<?php echo $template->getId(); ?>" href="#" title="<?php echo __('Edit'); ?>"><span class="badge badge-primary"><i class="fa fa-pencil"></i></span></a>

            <a id="deletetemplate<?php echo $template->getId(); ?>" href="#" title="<?php echo __('Delete'); ?>"><span class="badge badge-primary"><i class="fa fa-trash-o"></i></span></a>


            <script language="javascript">
            jQuery(document).ready(function(){
              $( "#edittemplate<?php echo $template->getId(); ?>" ).click(function() {
                  $("#contentload").load("<?php echo public_path(); ?>backend.php/invoicetemplates/ajaxedit/id/<?php echo $template->getId(); ?>");
              });
              $( "#deletetemplate<?php echo $template->getId(); ?>" ).click(function() {
                  if(confirm('Are you sure you want to delete this permit?')){
                    $("#contentload").load("<?php echo public_path(); ?>backend.php/invoicetemplates/ajaxdelete/id/<?php echo $template->getId(); ?>");
                  }
                  else
                  {
                    return false;
                  }
              });
            });
            </script>
        </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
        <tfoot>
        <tr><td colspan='7' style='text-align: left;'>
            <select id='batch_action' name='batch_action' onChange="if(this.value != ''){if(confirm('Are you sure?')){ batch('invoicetemplates', this.options[this.selectedIndex].text, this.value); document.getElementById('default').selected='selected'; }}">
                <option id='default' value=''><?php echo __('Choose an action'); ?>..</option>
                <option value='delete'><?php echo __('Set As Deleted'); ?></option>
            </select>
        </td></tr>
        </tfoot>
</table>
</div>
</div>
<script>
jQuery(document).ready(function(){
  jQuery('#table3').dataTable({
      "sPaginationType": "full_numbers",

      // Using aoColumnDefs
      "aoColumnDefs": [
      	{ "bSortable": false, "aTargets": [ 'no-sort' ] }
    	]
  });
});
</script>
<?php
}
else
{
  include_partial("accessdenied");
}
?>
