<div id='results' name='results' style='height: 250px; overflow-y: auto;' align="right">
<?php
//Get User Information (anything starting with sf_ )
		   //sf_email, sf_fullname, sf_username, ... other fields in the dynamic user profile form e.g sf_element_1
?>
<table>
<thead><tr><th>User Details</th><th>Tag</th></tr></thead>
<tbody>
<tr><td>Username</td><td>{sf_username}</td></tr>
<tr><td>Email</td><td>{sf_email}</td></tr>
<tr><td>Full Name</td><td>{sf_fullname}</td></tr>
<?php
        $q = Doctrine_Query::create()
		   ->from('apFormElements a')
		   ->where('a.form_id = ?', 15);
		   
		$elements = $q->execute();
		
		foreach($elements as $element)
		{
		    $childs = $element->getElementTotalChild();
			if($childs == 0)
			{
			   echo "<tr><td>".$element->getElementTitle()."</td><td>{sf_element_".$element->getElementId()."}</td></tr>";
			}
			else
			{
			    if($element->getElementType() == "select")
				{
					echo "<tr><td>".$element->getElementTitle()."</td><td>{sf_element_".$element->getElementId()."}</td></tr>";
				}
				else
				{
					for($x = 0; $x < ($childs + 1); $x++)
					{
						echo "<tr><td>".$element->getElementTitle()."</td><td>{sf_element_".$element->getElementId()."_".($x+1)."}</td></tr>";
					}
				}
			}
		}
?>
</tbody>
</table>
<?php 
//Get Application Information (anything starting with ap_ )
		   //ap_application_id
		
//Get Form Details (anything starting with fm_ )
		   //fm_created_at, fm_updated_at.....fm_element_1
?>
<table>
<thead><tr><th>Application Details</th><th>Tag</th></tr></thead>
<tbody>
<tr><td>Plan Registration Number</td><td>{ap_application_id}</td></tr>
<tr><td>Created At</td><td>{fm_created_at}</td></tr>
<tr><td>Approved At</td><td>{fm_updated_at}</td></tr>
<?php

        $q = Doctrine_Query::create()
		   ->from('apFormElements a')
		   ->where('a.form_id = ?', $form);
		   
		$elements = $q->execute();
		
		foreach($elements as $element)
		{
		    $childs = $element->getElementTotalChild();
			if($childs == 0)
			{
			   echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."}</td></tr>";
			}
			else
			{
			    if($element->getElementType() == "select")
				{
					echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."}</td></tr>";
				}
				else
				{
					for($x = 0; $x < ($childs + 1); $x++)
					{
						echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."_".($x+1)."}</td></tr>";
					}
				}
			}
		}
?>
</tbody>
</table>

<table>
<thead><tr><th>Conditions Details</th><th>Tag</th></tr></thead>
<tbody>
<tr><td>Conditions Of Approval</td><td>{ca_conditions}</td></tr>
<tr><td>Other Conditions</td><td>{other_conditions}</td></tr>
</tbody>
</table>

<table>
<thead><tr><th>Invoice Details</th><th>Tag</th></tr></thead>
<tbody>
<tr><td>Total</td><td>{in_total}</td></tr>
</tbody>
</table>

</div>