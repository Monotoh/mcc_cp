<?php
/**
 * jsonreports actions.
 *
 * Generates in-built reports on various types of applications submitted by clients and the history of their review process.
 * Also includes components for creating and generating custom reports.
 *
 * @package    backend
 * @subpackage reports
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
class jsonreportsActions extends sfActions
{
 /**
  * Executes index action
  *
  * Shows a list of custom generated reports
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
	  $q = Doctrine_Query::create()
			  ->from('Reports a')
			  ->orderBy('a.title ASC');
	$this->reports = $q->execute();
	$this->setLayout("layout-settings");
  }


 /**
  * Executes New action
  *
  * Displays form for creating a new custom report
  *
  * @param sfRequest $request A request object
  */
   public function executeNew(sfWebRequest $request)
  {
	if($request->getParameter("formid") != "")
	{
		$this->formid = $request->getParameter("formid");
        $this->selectedform = $request->getParameter("formid");
        $this->application_form = $request->getParameter("formid");
        $this->type = $request->getParameter("type");
		$this->title = $request->getParameter("title");
	}
	  $this->setLayout("layout-settings");
  }


 /**
  * Executes Edit action
  *
  * Displays form for editing an existing custom report
  *
  * @param sfRequest $request A request object
  */
   public function executeEdit(sfWebRequest $request)
   {
		$q = Doctrine_Query::create()
			 ->from('Reports a')
			 ->where('a.id = ?', $request->getParameter('id'));
		$this->report = $q->fetchOne();
		$this->formid = $this->report->getFormId();
        $this->selectedform = $this->report->getFormId();
        $this->application_form = $this->report->getFormId();
		$this->type = $this->report->getType();
		$this->title = $this->report->getTitle();
		if($request->getParameter("formid") != "")
		{
			$this->formid = $request->getParameter("formid");
			$this->type = $request->getParameter("type");
			$this->title = $request->getParameter("title");
		}
	   $this->setLayout("layout-settings");
  }

	/**
	 * Executes View action
	 *
	 * Displays form for editing an existing custom report
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeView(sfWebRequest $request)
	{
		$q = Doctrine_Query::create()
				->from('Reports a')
				->where('a.id = ?', $request->getParameter('id'));
		$this->report = $q->fetchOne();

		if($_GET['print'])
		{
			$report_content = $this->report->getContent();

			if($_POST['from_date_filter'])
			{
				$from_date = date('Y-m-d', strtotime($_POST['from_date_filter']));
				$to_date = date('Y-m-d', strtotime($_POST['to_date_filter']));
			}
			else {
				$from_date = date('Y-m-d', strtotime($_GET['from_date_filter']));
				$to_date = date('Y-m-d', strtotime($_GET['to_date_filter']));
			}

			$stage =  1;

			$q = Doctrine_Query::create()
					->from('ReportFilters a')
					->where('a.report_id = ?', $this->report->getId());
			$filter = $q->fetchOne();

			if($filter)
			{
				$stage = $filter->getValue();
			}

			$dbconn = mysql_connect(sfConfig::get('app_mysql_host'), sfConfig::get('app_mysql_user'), sfConfig::get('app_mysql_pass'));
			mysql_select_db(sfConfig::get('app_mysql_db'), $dbconn);

			$sql = "SELECT * FROM form_entry a LEFT JOIN ap_form_" . $this->report->getFormId() . " b ON a.entry_id = b.id WHERE a.form_id = " . $this->report->getFormId() . " AND a.approved = " . $stage . " AND a.date_of_response BETWEEN '" . $from_date . "' AND '" . $to_date . "'";
			$results = mysql_query($sql, $dbconn);
			$rows = array();

			function find($needle, $haystack)
			{
				$pos = strpos($haystack, $needle);
				if ($pos === false) {
					return false;
				} else {
					return true;
				}
			}

			while ($row = mysql_fetch_assoc($results)) {
				$num_records = $row['total_rows'];
				foreach ($row as $key => $value) {
					if (find("element_", $key)) {
						$element_id = str_replace("element_", "", $key);

						$q = Doctrine_Query::create()
								->from("ApFormElements a")
								->where("a.form_id = ? AND a.element_id = ?", array($this->report->getFormId(), $element_id))
								->andWhere("a.element_type = 'select' OR a.element_type = 'radio'");
						$element = $q->fetchOne();

						if ($element) {
							$q = Doctrine_Query::create()
									->from("ApElementOptions a")
									->where("a.form_id = ? AND a.element_id = ?", array($this->report->getFormId(), $element_id))
									->andWhere("a.option_id = ?", $value);
							$option = $q->fetchOne();

							if ($option) {
								$row[$key] = $option->getOption();
							} else {
								$row[$key] = "";
							}
						}
					}
				}
				$rows[] = $row;
			}
			$parse['rows'] = $rows;

			$template_parser = new templateparser();
			$report_content = $template_parser->parseWithDust($report_content, $parse);

			require_once(dirname(__FILE__)."/../../../../../lib/vendor/dompdf/dompdf_config.inc.php");

			$dompdf = new DOMPDF();
			$dompdf->load_html($report_content);
			$dompdf->set_paper("A4", "landscape");
			$dompdf->render();
			$dompdf->stream($this->report->getTitle().".pdf");
		}
	}

 /**
  * Executes create action
  *
  * Processes POST data submitted when user is creating a new custom report
  *
  * @param sfRequest $request A request object
  */
   public function executeCreate(sfWebRequest $request)
  {
	  if($request->isMethod(sfRequest::POST))
	  {
		   $report = new Reports();
		   $report->setType($request->getPostParameter("rpttype"));
		   $report->setFormId($request->getPostParameter("application_form"));
		   $report->setTitle($request->getPostParameter("title"));
		   $report->setContent($request->getPostParameter("rptcontent"));
		   $report->save();

		   if($report->getId() != "")
		   {
				if($request->getPostParameter("application_filter") != "0")
				{
					$filter = new ReportFilters();
					$filter->setReportId($report->getId());
					$filter->setElementId($request->getPostParameter("application_actions"));
					$filter->setValue($request->getPostParameter("application_filter"));
					$filter->save();
				}

				$fields = $request->getPostParameter("fields");
				foreach($fields as $field)
				{
					$reportfield = new ReportFields();
					$reportfield->setReportId($report->getId());
					$reportfield->setElement($field);
					$reportfield->save();
				}

		   }
	  }

	  $this->redirect("/backend.php/jsonreports/index");
  }



 /**
  * Executes update action
  *
  * Processes POST data submitted when user is editing an existing custom report
  *
  * @param sfRequest $request A request object
  */
   public function executeUpdate(sfWebRequest $request)
  {
	  if($request->isMethod(sfRequest::POST))
	  {
	       $q = Doctrine_Query::create()
			 ->from('Reports a')
			 ->where('a.id = ?', $request->getParameter('id'));
		   $report = $q->fetchOne();
		   $report->setType($request->getPostParameter("rpttype"));
		   $report->setFormId($request->getPostParameter("application_form"));
		   $report->setTitle($request->getPostParameter("title"));
		   $report->setContent($request->getPostParameter("rptcontent"));
		   $report->save();

		   if($report->getId() != "")
		   {
				if($request->getPostParameter("application_filter") != "0")
				{
					$q = Doctrine_Query::create()
					 ->from('ReportFilters a')
					 ->where('a.report_id = ?', $request->getParameter('id'));
				   $filters = $q->execute();
				   foreach($filters as $filter)
				   {
						$filter->delete();
				   }

					$filter = new ReportFilters();
					$filter->setReportId($report->getId());
					$filter->setElementId($request->getPostParameter("application_actions"));
					$filter->setValue($request->getPostParameter("application_filter"));
					$filter->save();
				}

				$q = Doctrine_Query::create()
				 ->from('ReportFields a')
				 ->where('a.report_id = ?', $request->getParameter('id'));
			   $fields = $q->execute();
			   foreach($fields as $field)
			   {
					$field->delete();
			   }

				$fields = $request->getPostParameter("fields");
				$headers = $request->getPostParameter("headers");
				$count = 0;
				foreach($fields as $field)
				{
					$reportfield = new ReportFields();
					$reportfield->setReportId($report->getId());
					$reportfield->setElement($field);
					$reportfield->setCustomheader($headers[$count]);
					$reportfield->save();
					$count++;
				}

		   }
	  }

	  $this->redirect("/backend.php/jsonreports/index");
  }


 /**
  * Executes delete action
  *
  * Deletes an existing custom report
  *
  * @param sfRequest $request A request object
  */
  public function executeDelete(sfWebRequest $request)
  {
		$q = Doctrine_Query::create()
		   ->from('Reports a')
		   ->where('a.id = ?', $request->getParameter("id"));
		$report = $q->fetchOne();

		$report->delete();

		$this->redirect("/backend.php/jsonreports/index");
  }

}
