<?php
use_helper("I18N");
/**
 * new template.
 *
 * Displays form for creating a new custom report 
 *
 * @package    backend
 * @subpackage reports
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>

<div class="contentpanel">
	<div class="panel panel-dark">
		<div class="panel-heading">
			<h3 class="panel-title"><?php echo __('Custom JSON Reports'); ?></h3>
		</div>


		<div class="panel panel-body panel-body-nopadding ">

		<form id="reportform" class="form-bordered" action="/backend.php/jsonreports/create" method="post" enctype="multipart/form-data" autocomplete="off" data-ajax="false">

					 <div class="form-group">
						<label class="col-sm-4"><i class="bold-label">Title</i></label>
						<div class="col-sm-8">
						  <input class="form-control" type="text" name="title" id="title" value="<?php if($title){ echo $title; } ?>">
						</div>
					  </div>
					 <div class="form-group">
						<label class="col-sm-4"><i class="bold-label">Type Of Application</i></label>
						<div class="col-sm-8">
							<select id='application_form' name='application_form' onChange="window.location='/backend.php/jsonreports/new/formid/' + (this.value) + '/type/' + document.getElementById('rpttype').value + '/title/' + document.getElementById('title').value ;">
							<option value="0">Choose an application form...</option>
							<?php

							$q = Doctrine_Query::create()
								->from('ApForms a')
								->where('a.form_id <> 6 AND a.form_id <> 7 AND a.form_id <> 15 AND a.form_id <> 16 AND a.form_id <> 17 AND a.form_id <> 18 AND a.form_id <> 19');
							$forms = $q->execute();
							?>
							<?php
									//Get list of all available applications categorized by groups
									$q = Doctrine_Query::create()
										->from('FormGroups a');
									$groups = $q->execute();

									if(sizeof($groups) > 0)
									{
										foreach($groups as $group)
										{
											echo "<optgroup label='".$group->getGroupName()."'>";

											$q = Doctrine_Query::create()
												->from('ApForms a')
												->where('a.form_group = ?', $group->getGroupId());
											$forms = $q->execute();

											$count = 0;

											foreach($forms as $apform)
											{
												$selected = "";

												if($application_form != "" && $application_form == $apform->getFormId())
												{
													$selected = "selected";
													$_GET['form'] = $application_form;
												}


												echo "<option value='".$apform->getFormId()."' ".$selected.">".$apform->getFormName()."</option>";

												$count++;
											}
											echo "</optgroup>";
										}
									}
									else
									{
										$q = Doctrine_Query::create()
											->from('ApForms a')
											->where('a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ?',array('6','7','15','16','17'))
											->orderBy('a.form_id ASC');
										$forms = $q->execute();

										$count = 0;

										echo "<optgroup label='Application Forms'>";

										foreach($forms as $apform)
										{

											$selected = "";

											if($application_form != "" && $application_form == $apform->getFormId())
											{
												$selected = "selected";
												$_GET['form'] = $application_form;
											}

											if($selectedform != "" && $selectedform == $apform->getFormId())
											{
												$selected = "selected";
												$_GET['form'] = $selectedform;
											}



											echo "<option value='".$apform->getFormId()."' ".$selected.">".$apform->getFormName()."</option>";

											$count++;
										}

										echo "</optgroup>";
									}
							?>>
						</select>
						</div>
					  </div>
					  <div class="form-group">
						<label class="col-sm-4"><i class="bold-label">Report Type</i></label>
						<div class="col-sm-8">
						 <select name="rpttype" id="rpttype">
							<option value="2" <?php if($type != "" && $type == 2){ echo "selected"; }?>>Single Entry Summary</option>
							<option value="1" <?php if($type != "" && $type == 1){ echo "selected"; }?>>Multiple Entry List</option>
						 </select>
						</div>
					  </div>
					  <div class="form-group">
						<label class="col-sm-4"><i class="bold-label">Content</i></label>
						<div class="col-sm-8">
						 <div id='ajaxContent' name='ajaxContent'>
						 <textarea name="rptcontent" id="rptcontent" rows="10" cols="120"></textarea>
						   <div id="ajaxTags" name="ajaxTags" style='height: 250px; overflow-y: auto;' align="right">
						   <?php
							if($formid != "")
							{
						   ?>
								<table class="table">
									<thead><tr><th>User Details</th><th>Tag</th></tr></thead>
									<tbody>
									<tr><td>Username</td><td>{username}</td></tr>
									<tr><td>Email</td><td>{email}</td></tr>
									<tr><td>Full Name</td><td>{fullname}</td></tr>

									</tbody>
									</table>
									<?php
									//Get Application Information (anything starting with ap_ )
											   //ap_application_id

									//Get Form Details (anything starting with fm_ )
											   //fm_created_at, fm_updated_at.....fm_element_1
									?>
									<table class="table">
									<thead><tr><th>Application Details</th><th>Tag</th></tr></thead>
									<tbody>
									<tr><td>Application Number</td><td>{application_id}</td></tr>
									<tr><td>Created At</td><td>{date_of_submission}</td></tr>
									<tr><td>Approved At</td><td>{date_of_response}</td></tr>
									<?php

											$q = Doctrine_Query::create()
											   ->from('apFormElements a')
											   ->where('a.form_id = ?', $formid);

											$elements = $q->execute();

											foreach($elements as $element)
											{
												$childs = $element->getElementTotalChild();
												if($childs == 0)
												{
												   echo "<tr><td>".$element->getElementTitle()."</td><td>{element_".$element->getElementId()."}</td></tr>";
												}
												else
												{
													if($element->getElementType() == "select")
													{
														echo "<tr><td>".$element->getElementTitle()."</td><td>{element_".$element->getElementId()."}</td></tr>";
													}
													else
													{
														for($x = 0; $x < ($childs + 1); $x++)
														{
															echo "<tr><td>".$element->getElementTitle()."</td><td>{element_".$element->getElementId()."_".($x+1)."}</td></tr>";
														}
													}
												}
											}
									?>
									</tbody>
									</table>

							<?php
							}
							?>
						   </div>
						 </div>
						</div>
					  </div>
					   <div class="form-group">
						<label class="col-sm-4"><i class="bold-label">Show Actions Column</i></label>
						<div class="col-sm-8">
							<select id='application_actions' name='application_actions'>
							<option value="0">No</option>
							<option value="1">Yes</option>
						</select>
						</div>
					  </div>

			  <div class="panel-footer">
					<button type="submit" class="btn btn-primary" name="submitbuttonname" id="submitbuttonname" value="submitbuttonvalue">Submit</button>
			  </div>
		</fieldset>

		</form>

		</div
	</div>
</div>
