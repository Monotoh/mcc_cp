<?php
use_helper("I18N");
/**
 * edit template.
 *
 * Displays form for editing an existing custom report
 *
 * @package    backend
 * @subpackage reports
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>

<div class="contentpanel">
    <div class="panel panel-dark">

            <?php
                $report_content = $report->getContent();

                if($_POST['from_date_filter'])
                {
                    $from_date = date('Y-m-d', strtotime($_POST['from_date_filter']));
                    $to_date = date('Y-m-d', strtotime($_POST['to_date_filter']));
                }
                else {
                    $from_date = date('Y-m-d', strtotime($_GET['from_date_filter']));
                    $to_date = date('Y-m-d', strtotime($_GET['to_date_filter']));
                }

                $form_id =  $report->getFormId();
                $stage =  1;

                $q = Doctrine_Query::create()
                    ->from('ReportFilters a')
                    ->where('a.report_id = ?', $report->getId());
                $filter = $q->fetchOne();

                if($filter)
                {
                    $stage = $filter->getValue();
                }

        ?>
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $report->getTitle(); ?></h3>

            <div class="pull-right">
                <a class="btn btn-primary-alt settings-margin42" href="<?php echo public_path(); ?>backend.php/jsonreports/view?id=<?php echo $_GET['id']."&page=".$_GET['page']."&from_date_filter=$from_date&to_date_filter=$to_date&print=1"; ?>"><?php echo __('Print Report'); ?></a>
            </div>
        </div>


        <div class="panel panel-body panel-body-nopadding ">
        <?php
        if($_GET['print']) {
            echo "<div align='center'><h1>Your report has been printed.</h1></div>";
        }
        else
        {
            $dbconn = mysql_connect(sfConfig::get('app_mysql_host'), sfConfig::get('app_mysql_user'), sfConfig::get('app_mysql_pass'));
            mysql_select_db(sfConfig::get('app_mysql_db'), $dbconn);

            $rec_limit = 20;

            if (isset($_GET{'page'})) {
                $page = $_GET{'page'};
                $offset = ($page - 1) * $rec_limit;
            } else {
                $page = 0;
                $offset = 0;
            }

            $sql = "SELECT * FROM form_entry a LEFT JOIN ap_form_" . $report->getFormId() . " b ON a.entry_id = b.id WHERE a.form_id = " . $report->getFormId() . " AND a.approved = " . $stage . " AND a.date_of_response BETWEEN '" . $from_date . "' AND '" . $to_date . "' limit $offset, $rec_limit";
            $results = mysql_query($sql, $dbconn);
            $rows = array();

            function find($needle, $haystack)
            {
                $pos = strpos($haystack, $needle);
                if ($pos === false) {
                    return false;
                } else {
                    return true;
                }
            }

            while ($row = mysql_fetch_assoc($results)) {
                $num_records = $row['total_rows'];
                foreach ($row as $key => $value) {
                    if (find("element_", $key)) {
                        $element_id = str_replace("element_", "", $key);

                        $q = Doctrine_Query::create()
                            ->from("ApFormElements a")
                            ->where("a.form_id = ? AND a.element_id = ?", array($report->getFormId(), $element_id))
                            ->andWhere("a.element_type = 'select' OR a.element_type = 'radio'");
                        $element = $q->fetchOne();

                        if ($element) {
                            $q = Doctrine_Query::create()
                                ->from("ApElementOptions a")
                                ->where("a.form_id = ? AND a.element_id = ?", array($report->getFormId(), $element_id))
                                ->andWhere("a.option_id = ?", $value);
                            $option = $q->fetchOne();

                            if ($option) {
                                $row[$key] = $option->getOption();
                            } else {
                                $row[$key] = "";
                            }
                        }
                    }
                }
                $rows[] = $row;
            }
            $parse['rows'] = $rows;

            $template_parser = new templateparser();
            $report_content = $template_parser->parseWithDust($report_content, $parse);

            echo html_entity_decode($report_content);

            $sql = "SELECT COUNT(*) as total_rows FROM form_entry a WHERE a.form_id = " . $report->getFormId() . " AND a.approved = " . $stage . " AND a.date_of_response BETWEEN '" . $from_date . "' AND '" . $to_date . "'";
            $results = mysql_query($sql, $dbconn);
            $num_record_row = mysql_fetch_assoc($results);
            $num_records = $num_record_row['total_rows'];

            echo "<br><br><div style='float:left;'><strong>&nbsp; &nbsp;Page $page / " . ceil($num_records / $rec_limit) . " of $num_records Records</strong></div><div align='center'>";

            $page++;
            if ($page > 1) {
                $last = $page - 2;
                if ($page != 2) {
                    echo "<a href = \"/backend.php/jsonreports/view?id=" . $_GET['id'] . "&page=$last&from_date_filter=$from_date&to_date_filter=$to_date\">Last 10 Records</a> |";
                }
                echo "<a href = \"/backend.php/jsonreports/view?id=" . $_GET['id'] . "&page=$page&from_date_filter=$from_date&to_date_filter=$to_date\">Next 10 Records</a>";
            } else if ($page == 1) {
                $page = 2;
                echo "<a href = \"/backend.php/jsonreports/view?id=" . $_GET['id'] . "&page=$page&from_date_filter=$from_date&to_date_filter=$to_date\">Next 10 Records</a>";
            } else if ($left_rec < $rec_limit) {
                $last = $page - 2;
                echo "<a href = \"/backend.php/jsonreports/view?id=" . $_GET['id'] . "&page=$last&from_date_filter=$from_date&to_date_filter=$to_date\">Last 10 Records</a>";
            }

            echo "</div>";
        }
        ?>

        </div
    </div>
</div>
