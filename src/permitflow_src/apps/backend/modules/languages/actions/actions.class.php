<?php
/**
 * Languages actions.
 *
 * Language Management Service
 *
 * @package    backend
 * @subpackage languages
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

class languagesActions extends sfActions
{

    /**
     * Executes 'Batch' action
     *
     * Deletes selected languages
     *
     * @param sfRequest $request A request object
     */
    public function executeBatch(sfWebRequest $request)
    {
      if($request->getPostParameter('delete'))
      {
        $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
                  mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

          try
          {
                    $sql = "DELETE FROM `ext_locales` WHERE id = ".$request->getParameter('delete');
                    mysql_query($sql);
          }
          catch (PropelException $e)
          {
          }
      }

    }
    /**
     * Executes 'Checkname' action
     *
     * Ajax used to check existence of name
     *
     * @param sfRequest $request A request object
     */
    public function executeChecklocale(sfWebRequest $request)
    {
        $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

        $sql = "SELECT * FROM ext_locales WHERE locale_identifier = '".$request->getPostParameter('name')."'";
        $rows = mysql_query($sql);
        
        if(mysql_num_rows($rows) > 0)
        {
              echo '<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Locale is already in use!</strong></div>';
              exit;
        }
        else
        {
              echo '<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Locale is available!</strong></div>';
              exit;
        }
    }
    /**
     * Executes 'Checkname' action
     *
     * Ajax used to check existence of name
     *
     * @param sfRequest $request A request object
     */
    public function executeChecktitle(sfWebRequest $request)
    {
        $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

        $sql = "SELECT * FROM ext_locales WHERE locale_title = '".$request->getPostParameter('name')."'";
        $rows = mysql_query($sql);
        
        if(mysql_num_rows($rows) > 0)
        {
              echo '<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Title is already in use!</strong></div>';
              exit;
        }
        else
        {
              echo '<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Title is available!</strong></div>';
              exit;
        }
    }

    /**
     * Executes 'Index' action
     *
     * Displays list of available languages
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {

          $this->setLayout("layout-settings");
    }

    /**
     * Executes 'New' action
     *
     * Create new language
     *
     * @param sfRequest $request A request object
     */
    public function executeNew(sfWebRequest $request)
    {
          $this->setLayout("layout-settings");
    }

    /**
     * Executes 'Create' action
     *
     * Saves new language details to the database
     *
     * @param sfRequest $request A request object
     */
    public function executeCreate(sfWebRequest $request)
    {
            $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
            mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

            $sql = "INSERT INTO `ext_locales`(`locale_identifier`, `local_title`, `locale_description`, `is_default`, `text_align`) VALUES('".$request->getParameter('locale_identifier')."','".$request->getPostParameter('locale_title')."','".$request->getPostParameter('locale_description')."','".$request->getPostParameter('is_default')."','".$request->getPostParameter('text_align')."')";


            $rows = mysql_query($sql);

            $this->redirect('/backend.php/languages/index');
    }

    /**
     * Executes 'Edit' action
     *
     * Edit selected language details
     *
     * @param sfRequest $request A request object
     */
    public function executeEdit(sfWebRequest $request)
    {
          $this->rowid = $request->getParameter("id");
          $this->setLayout("layout-settings");
    }

    /**
     * Executes 'Update' action
     *
     * Saves language details to the database
     *
     * @param sfRequest $request A request object
     */
    public function executeUpdate(sfWebRequest $request)
    {

            $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
            mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

            $sql = "UPDATE `ext_locales` SET `locale_identifier` = '".$request->getParameter('locale_identifier')."', `local_title` = '".$request->getPostParameter('locale_title')."', `locale_description` = '".$request->getPostParameter('locale_description')."', `is_default` = '".$request->getPostParameter('is_default')."', `text_align` = '".$request->getPostParameter('text_align')."' WHERE id = ".$request->getPostParameter("rowid");

            $rows = mysql_query($sql);

        $this->redirect('/backend.php/languages/index');
    }

    /**
     * Executes 'Delete' action
     *
     * Deletes a currently existing language
     *
     * @param sfRequest $request A request object
     */
    public function executeDelete(sfWebRequest $request)
    {
          $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
                  mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

          try
                  {
                            $sql = "DELETE FROM `ext_locales` WHERE id = ".$request->getParameter('id');
                            mysql_query($sql);
                  }
                  catch (PropelException $e)
                  {
                  }

      $this->redirect('/backend.php/languages/index');
    }

    /**
     * Executes 'Setlocale' action
     *
     * Change the display language for the currently logged in reviewer
     *
     * @param sfRequest $request A request object
     */
    public function executeSetlocale(sfWebRequest $request)
    {
      $this->getUser()->setCulture($request->getParameter("code"));
      $this->redirect($request->getReferer());
    }


}
