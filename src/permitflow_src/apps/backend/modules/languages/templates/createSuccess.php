<?php
/**
 * createSuccess.php template.
 *
 * Saves new language details to the database
 *
 * @package    backend
 * @subpackage languages
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");
?>
<div class="contentpanel">
<div class="g12">
<form action="/backend.php/languages/create" method="post" enctype="multipart/form-data"  autocomplete="off" data-ajax="false">
<fieldset>
           <label><?php echo __('New Language'); ?> <div style="float: right; margin-top: -12px;">
							<button style="height: 34px; font-size: 12px;" onClick="window.location='<?php echo public_path(); ?>backend.php/languages/index';"><?php echo __('Back to Languages'); ?></button>
</div></label>


<section><label for="text_field"><?php echo __('Locale'); ?></label>
	<div>
		<input type='text' name='locale_identifier' id='locale_identifier' />
	</div>
</section>

<div id="localeresult" name="localeresult"></div>

<script language="javascript">
$('document').ready(function(){
  $('#locale_identifier').keyup(function(){
    $.ajax({
              type: "POST",
              url: "/backend.php/languages/checklocale",
              data: {
                  'name' : $('input:text[id=locale_identifier]').val()
              },
              dataType: "text",
              success: function(msg){
                    //Receiving the result of search here
                    $("#localeresult").html(msg);
              }
          });
      });
});
</script>
<section><label for="text_field"><?php echo __('Title'); ?></label>
	<div>
		<input type='text' name='locale_title' id='locale_title' />
	</div>
</section>

<section><label for="text_field"><?php echo __('Description'); ?></label>
	<div>
		<input type='text' name='locale_description' id='locale_description' />
	</div>
</section>
<section><label for="text_field"><?php echo __('Is Default?'); ?></label>
	<div>
		<select name='is_default' id='is_default'>
        	<option value='0'><?php echo __('No'); ?></option>
            <option value='1'><?php echo __('Yes'); ?></option>
        </select>
	</div>
</section>
<section><label for="text_field"><?php echo __('Language is Left Aligned?'); ?></label>
	<div>
		<select name='text_align' id='text_align'>
        	<option value='0'><?php echo __('Yes'); ?></option>
            <option value='1'><?php echo __('No'); ?></option>
        </select>
	</div>
</section>
	  
		<section>
			<div><button class="reset"><?php echo __('Reset'); ?></button><button class="submit" name="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button></div>
		</section>
		</fieldset>
</form>
</div>
</div>