<?php
/**
 * editSuccess.php template.
 *
 * Edit selected language details
 *
 * @package    backend
 * @subpackage languages
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");
?>
<div class="contentpanel">

<div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
  <strong><?php echo __('Well done'); ?>!</strong> <?php echo __('You successfully updated this web page'); ?></a>.
</div>

<form id="languageform" name="languageform"  action="/backend.php/languages/update" method="post" enctype="multipart/form-data"  autocomplete="off" data-ajax="false">
<div class="panel panel-dark">
<div class="panel-heading">
 <h3 class="panel-title"><?php echo __('Edit Language'); ?></h3>
</div>
<div class="panel-body panel-body-nopadding form-bordered form-horizontal">
<input type='hidden' name='rowid' id='rowid' value='<?php echo $rowid; ?>' />

<?php
$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

$sql = "SELECT * FROM ext_locales WHERE id = ".$rowid;
$results = mysql_query($sql);
$row = mysql_fetch_assoc($results);

?>
<div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Locale'); ?></i></label>
		<div class="col-sm-8">
			<input type='text' class="form-control" name='locale_identifier' id='locale_identifier' value='<?php echo $row['locale_identifier']; ?>'/>
		</div>
	</div>
<div id="localeresult" name="localeresult"></div>
<script language="javascript">
$('document').ready(function(){
  $('#locale_identifier').keyup(function(){
    $.ajax({
              type: "POST",
              url: "/backend.php/languages/checklocale",
              data: {
                  'name' : $('input:text[id=locale_identifier]').val()
              },
              dataType: "text",
              success: function(msg){
                    //Receiving the result of search here
                    $("#localeresult").html(msg);
              }
          });
      });
});
</script>
<div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Title'); ?></i></label>
	<div class="col-sm-8">
		<input type='text' class="form-control" name='locale_title' id='locale_title'  value='<?php echo $row['local_title']; ?>'/>
	</div>
</div>
<div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Description'); ?></i></label>
	<div class="col-sm-8">
		<input type='text' class="form-control" name='locale_description' id='locale_description'  value='<?php echo $row['locale_description']; ?>'/>
	</div>
</div>
<div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Is Default?'); ?></i></label>
	<div class="col-sm-8">
		<select name='is_default' id='is_default' class="form-control">
        	<option value='0' <?php if($row['is_default'] == "0"){ echo "selected"; } ?>><?php echo __('No'); ?></option>
            <option value='1' <?php if($row['is_default'] == "1"){ echo "selected"; } ?>><?php echo __('Yes'); ?></option>
        </select>
	</div>
</div>
<div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Language is Left Aligned?'); ?></i></label>
  <div class="col-sm-8">
    <select name='text_align' id='text_align' class="form-control">
          <option value='0' <?php if($row['text_align'] == "0"){ echo "selected"; } ?>><?php echo __('Yes'); ?></option>
            <option value='1' <?php if($row['text_align'] == "1"){ echo "selected"; } ?>><?php echo __('No'); ?></option>
        </select>
  </div>
</div>
<div class="panel-footer">
			<div><button class="btn btn-danger mr10" type="reset"><?php echo __('Reset'); ?></button><button type="submit" class="btn btn-primary" name="submitbuttonname" id="submitbuttonname"><?php echo __('Submit'); ?></button></div>
		</div>
</fieldset>
</div>
</form>

</div>

