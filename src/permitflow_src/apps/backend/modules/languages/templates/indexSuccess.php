<?php
use_helper("I18N");

$audit = new Audit();
$audit->saveAudit("", "Accessed language settings");

if($sf_user->mfHasCredential("managelanguages"))
{
  $_SESSION['current_module'] = "languages";
  $_SESSION['current_action'] = "index";
  $_SESSION['current_id'] = "";
?>
<?php
/**
 * indexSuccess.php template.
 *
 * Displays list of available languages
 *
 * @package    backend
 * @subpackage languages
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>
<div class="contentpanel">
<div class="panel panel-dark">

<div class="panel-heading">
<h3 class="panel-title"><?php echo __('Languages'); ?></h3>

    <div class="pull-right">
           <a class="btn btn-primary-alt settings-margin42" id="newlanguage" href="<?php echo public_path(); ?>backend.php/languages/new"><?php echo __('New Language'); ?></a>
    </div>
</div>

<div class="table-responsive">
<table class="table dt-on-steroids mb0" id="table3">
    <thead>
	<tr>
      <th class="no-sort" style="width: 10px;"><input type='checkbox' name='batchall' onclick="boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.name == 'batch') { if(this.checked == true){ box.checked = true; }else{ box.checked = false; } } } "></th>
      <th width="60">#</th>
      <th class="no-sort"><?php echo __('Locale'); ?></th>
      <th class="no-sort"><?php echo __('Title'); ?></th>
      <th class="no-sort"><?php echo __('Is Default?'); ?></th>
      <th class="no-sort" width="7%"><?php echo __('Actions'); ?></th>
    </tr>
    </thead>
    <tbody>
	<?php
		$count = 1;

		$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
		mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

		$sql = "SELECT * FROM ext_locales";
	    $rows = mysql_query($sql);
	?>
    <?php while($row = mysql_fetch_assoc($rows)){ ?>
    <tr id="row_<?php echo $row['id']; ?>">
	  <td><input type='checkbox' name='batch' id='batch_<?php echo $row['id']; ?>' value='<?php echo $row['id']; ?>'></td>
	  <td><?php echo $count++; ?></td>
      <td><?php echo $row['locale_identifier']; ?></td>
       <td><?php echo $row['local_title']; ?></td>
       <td><?php if($row['is_default'] == "1"){ echo "Yes"; }else{ echo "No"; } ?></td>
     <td>
		<a id="editlanguage<?php echo $row['id']; ?>" href="<?php echo public_path(); ?>backend.php/languages/edit/id/<?php echo $row['id']; ?>" title="<?php echo __('Edit'); ?>"><span class="badge badge-primary"><i class="fa fa-pencil"></i></span></a>
		<?php
    if($row['is_default'] == "0")
    {
    ?>
    <a id="deletelanguage<?php echo $row['id']; ?>" onClick="if(confirm('Are you sure you want to delete this item?')){ return true; }else{ return false; }" href="<?php echo public_path(); ?>backend.php/languages/delete/id/<?php echo $row['id']; ?>" title="<?php echo __('Delete'); ?>"><span class="badge badge-primary"><i class="fa fa-trash-o"></i></span></a>
    <?php
    }
    ?>
  </td>
    </tr>
    <?php } ?>
    </tbody>
	<tfoot>
   <tr><td colspan='7' style='text-align: left;'>
   <select id='batch_action' name='batch_action' onChange="if(this.value != ''){if(confirm('Are you sure?')){ batch('languages', this.options[this.selectedIndex].text, this.value); document.getElementById('default').selected='selected'; }}">
   <option id='default' value=''><?php echo __('Choose an action'); ?>..</option>
   <option value='delete'><?php echo __('Set As Deleted'); ?></option>
   </select>
   </td></tr>
   </tfoot>
</table>
</div>
</div>
</div>
<script>
  jQuery('#table3').dataTable({
      "sPaginationType": "full_numbers",

      // Using aoColumnDefs
      "aoColumnDefs": [
      	{ "bSortable": false, "aTargets": [ 'no-sort' ] }
    	]
    });

</script>
<?php
}
else
{
  include_partial("settings/accessdenied");
}
?>
