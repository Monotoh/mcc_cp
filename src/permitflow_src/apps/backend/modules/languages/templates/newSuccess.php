<?php
/**
 * newSuccess.php template.
 *
 * Create new language
 *
 * @package    backend
 * @subpackage languages
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");
?>
<div class="contentpanel">

<div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
  <strong><?php echo __('Well done'); ?>!</strong> <?php echo __('You successfully updated this web page'); ?></a>.
</div>

<form id="languageform" name="languageform"  action="/backend.php/languages/create" method="post" enctype="multipart/form-data"  autocomplete="off" data-ajax="false">
<div class="panel panel-dark">
<div class="panel-heading">
 <h3 class="panel-title"><?php echo __('New Language'); ?></h3>
</div>

<div class="panel-body panel-body-nopadding form-bordered form-horizontal">

<div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Locale'); ?></i></label>
	<div class="col-sm-8">
		<input type='text' name='locale_identifier' id='locale_identifier' class="form-control"/>
	</div>
</div>
<div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Title'); ?></i></label>
	<div class="col-sm-8">
		<input type='text' name='locale_title' id='locale_title' class="form-control" />
	</div>
</div>
<div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Description'); ?></i></label>
	<div class="col-sm-8">
		<input type='text' name='locale_description' id='locale_description' class="form-control" />
	</div>
</div>
<div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Is Default?'); ?></i></label>
	<div class="col-sm-8">
		<select name='is_default' id='is_default'>
        	<option value='0'><?php echo __('No'); ?></option>
            <option value='1'><?php echo __('Yes'); ?></option>
        </select>
		 </div>
      </div>
	  
<div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Language is Left Aligned?'); ?></i></label>
  <div class="col-sm-8">
    <select name='text_align' id='text_align' class="form-control">
          <option value='0'><?php echo __('Yes'); ?></option>
            <option value='1'><?php echo __('No'); ?></option>
        </select>
  </div>
</div>

      </div>
	  
		<div class="panel-footer">
			<button class="btn btn-danger mr10"><?php echo __('Reset'); ?></button><button type="submit" class="btn btn-primary" name="submitbuttonname" id="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button>
		</div>
		
    </fieldset>
</form>
</div>
</div>

