<?php

/**
 * leaves actions.
 *
 * @package    symfony
 * @subpackage leaves
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class leavesActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
      if($this->getUser()->mfHasCredential("can_approve_leave_requests")){
          //view All user Requests
       //check user district and only retrieve records from the said user district.
       $otbhelper = new OTBHelper();
       $agencies_allowed = $otbhelper->getUserAllowedAgencies($_SESSION["SESSION_CUTEFLOW_USERID"]) ;
       //error_log("Allowed Agencies ");
       //error_log(print_R($agencies_allowed,True));
       $agencies_sorted = implode(', ', $agencies_allowed);
       //
     /*  $sql = "select * from leave_request AS l left join agency_user "
               . "AS a on l.reviewer_id = a.user_id where a.agency_id in ($agencies_sorted); " ; */
       //We use doctrine query manager to take care of query formatting and sql injections!!
      // $this->leave_requests = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($sql) ;   
      // error_log("Query >>> ".$sql);
       $this->leave_requests = Doctrine_Core::getTable('LeaveRequest')
      ->createQuery('a')
      ->leftJoin('a.User u')
      ->leftJoin('u.AgencyUser y')
      ->where('y.agency_id in ('.$agencies_sorted.')')
      ->execute(); 
       
      }else {
          //view only logged user requests
          $q = Doctrine_Query::create()
                 ->from('LeaveRequest r')
                  ->where('r.reviewer_id = ? ',$_SESSION["SESSION_CUTEFLOW_USERID"]) ;
           $this->leave_requests = $q->execute();
      }
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new LeaveRequestForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new LeaveRequestForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($leave_request = Doctrine_Core::getTable('LeaveRequest')->find(array($request->getParameter('id'))), sprintf('Object leave_request does not exist (%s).', $request->getParameter('id')));
    $this->form = new LeaveRequestForm($leave_request);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($leave_request = Doctrine_Core::getTable('LeaveRequest')->find(array($request->getParameter('id'))), sprintf('Object leave_request does not exist (%s).', $request->getParameter('id')));
    $this->form = new LeaveRequestForm($leave_request);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }
  
  /**
   * 
   * @param sfWebRequest $request
   * approve Leave Request
   */
  public function executeDone(sfWebRequest $request){
      $leave_id = $request->getParameter('id') ;
      $q = Doctrine_Query::create()
              ->update('LeaveRequest r')
              ->set('r.status','?','done')
              ->set('r.approved_by','?',$_SESSION["SESSION_CUTEFLOW_USERID"])
              ->set('r.approved_at','?',date('Y-m-d H:i:s'))
              ->where('r.id = ?',$leave_id) ;
      $res = $q->execute();
      $this->getUser()->setFlash('notice', sprintf('Request Successfuly Updated')) ;
      $this->redirect('/backend.php/leaves/index');
  }
   /**
   * 
   * @param sfWebRequest $request
   * approve Leave Request
   */
  public function executeApprove(sfWebRequest $request){
      $leave_id = $request->getParameter('id') ;
      $q = Doctrine_Query::create()
              ->update('LeaveRequest r')
              ->set('r.status','?','ongoing')
              ->set('r.approved_by','?',$_SESSION["SESSION_CUTEFLOW_USERID"])
              ->set('r.approved_at','?',date('Y-m-d H:i:s'))
              ->where('r.id = ?',$leave_id) ;
      $res = $q->execute();
      $this->getUser()->setFlash('notice', sprintf('Request Successfuly Approved')) ;
      $this->redirect('/backend.php/leaves/index');
  }
  /**
   * 
   * @param sfWebRequest $request
   * Reset Leave Request
   */
  public function executeReset(sfWebRequest $request){
      $leave_id = $request->getParameter('id') ;
      $q = Doctrine_Query::create()
              ->update('LeaveRequest r')
              ->set('r.status','?','pending')
              ->set('r.approved_by','?',$_SESSION["SESSION_CUTEFLOW_USERID"])
              ->set('r.approved_at','?',date('Y-m-d H:i:s'))
              ->where('r.id = ?',$leave_id) ;
      $res = $q->execute();
      $this->redirect('/backend.php/leaves/index');
  }
   /**
   * 
   * @param sfWebRequest $request
   * Reject Leave Request
   */
  public function executeReject(sfWebRequest $request){
      $leave_id = $request->getParameter('id') ;
      $q = Doctrine_Query::create()
              ->update('LeaveRequest r')
              ->set('r.status','?','rejected')
              ->set('r.approved_by','?',$_SESSION["SESSION_CUTEFLOW_USERID"])
              ->set('r.approved_at','?',date('Y-m-d H:i:s'))
              ->where('r.id = ?',$leave_id) ;
      $res = $q->execute();
      $this->getUser()->setFlash('notice', sprintf('Request Successfuly rejected')) ;
      $this->redirect('/backend.php/leaves/index');
  }
  /**
   * 
   * @param sfWebRequest $request
   * Cancel Leave Request
   */
  public function executeCancel(sfWebRequest $request){
      $leave_id = $request->getParameter('id') ;
      $q = Doctrine_Query::create()
              ->update('LeaveRequest r')
              ->set('r.status','?','cancelled')
              ->set('r.approved_by','?',$_SESSION["SESSION_CUTEFLOW_USERID"])
              ->set('r.approved_at','?',date('Y-m-d H:i:s'))
              ->where('r.id = ?',$leave_id) ;
      $res = $q->execute();
      $this->getUser()->setFlash('notice', sprintf('Request Successfuly Cancelled')) ;
      $this->redirect('/backend.php/leaves/index');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($leave_request = Doctrine_Core::getTable('LeaveRequest')->find(array($request->getParameter('id'))), sprintf('Object leave_request does not exist (%s).', $request->getParameter('id')));
    $leave_request->delete();

     $this->redirect('/backend.php/leaves/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    $otbhelper = new OTBHelper();
    if ($form->isValid())
    {
      //set variables
      $form_vals = $this->form->getValues();
      //$start_date = $form['start_date'] ;
      //error_log("Submitted Start Date >>>> ".$form_vals['start_date']);
      if($otbhelper->validateLeaveRequestDates($_SESSION["SESSION_CUTEFLOW_USERID"],$form_vals['start_date'],$form_vals['end_date'])){
          $leave_request = $form->save();
          $this->getUser()->setFlash('notice', sprintf('Request submitted successful.Wait for Approval from your manager/director')) ;
      }
      else {
           $this->getUser()->setFlash('error', sprintf('Sorry cannot submit your request. Check Your dates selection. '
                  . 'Start Date must be greater than or equal to today and less than end date.Also the reviewer must not have any ongoing leave for the selected duration.')) ;
          
      }
       $this->redirect('/backend.php/leaves/index');
      
    }
  }
}
