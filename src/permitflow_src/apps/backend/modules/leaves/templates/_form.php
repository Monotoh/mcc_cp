<div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
  <strong><?php echo __('Well done'); ?>!</strong> <?php echo __('You successfully updated this record'); ?></a>.
</div>

<form id="permitform" class="form-bordered" action="/backend.php/<?php echo 'leaves/'.($form->getObject()->isNew() ? 'create' : 'update'.(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post"  autocomplete="off">
<div class="panel-body panel-body-nopadding">
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
<?php echo $form->renderHiddenFields(false) ?>
<?php echo $form->renderGlobalErrors() ?>
    
       <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Reviewer'); ?></i></label><br>
        <div class="col-sm-12 rogue-input">
          <?php echo $form['reviewer_id']->renderError() ?>
            <?php
              $otbhelper = new OTBHelper();
              //
              $logged_reviewer_id = $_SESSION["SESSION_CUTEFLOW_USERID"] ;
              //error_log("Reviewer Id >>> ".$logged_reviewer_id);
              //get allowed agencies
              $alllowed_agencies = $otbhelper->getUserAllowedAgencies($logged_reviewer_id) ;           
              //
              $agencies_id = implode(',', $alllowed_agencies) ;
             //
              $admins = $otbhelper->getAdminRHAUsers() ;        
              $user_ids = implode(',', $admins) ;
              //
              $users = Doctrine_Query::create()
                      ->from('AgencyUser u')
                      ->where('u.agency_id in ('.$agencies_id.')')
                      ->addWhere('u.user_id not in ('.$user_ids.')');
              $users_r = $users->execute();
            ?>
            <select class="form-control" name="leave_request[reviewer_id]" id="leave_request_reviewer_id">
                <option value="<?php echo $_SESSION['SESSION_CUTEFLOW_USERID'] ?>"> <?php echo $otbhelper->getReviewerNames($_SESSION["SESSION_CUTEFLOW_USERID"]) ?> </option>   
            </select>
        </div>
      </div>

       <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Leave Type'); ?></i></label><br>
        <div class="col-sm-12 rogue-input">
          <?php echo $form['leave_type']->renderError() ?>
            <!-- http://hrdailyadvisor.blr.com/2015/04/27/employee-leave-101-what-types-of-leave-are-available/ -->
            <select class="form-control" name="leave_request[leave_type]" id="leave_request_leave_type">
                <option value="sick_leave">Sick Leave</option>
                <option value="vacation_leave">Vacation Leave</option>
                <option value="administrative_leave">Administrative Leave</option>
                <option value="maternity">Maternity/Paternity</option>
                <option value="others">Others</option>              
            </select>
        </div>
      </div>

      	<div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Description'); ?></i></label><br>
        <div class="col-sm-12 rogue-input">
          <?php echo $form['name']->renderError() ?>
          <?php echo $form['name'] ?>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-12"><i class="bold-label"><?php echo __('Start Date'); ?></i></label><br>
        <div class="col-sm-12">
          <?php echo $form['start_date']->renderError() ?>
          <input placeholder="yyyy-mm-dd" type="text" class="form-control" name="leave_request[start_date]" id="leave_request_start_date">
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-12"><i class="bold-label"><?php echo __('End Date'); ?></i></label><br>
        <div class="col-sm-12">
          <?php echo $form['end_date']->renderError() ?>
          <input placeholder="yyyy-mm-dd" type="text" class="form-control" name="leave_request[end_date]" id="leave_request_end_date">
        </div>
      </div>


<div class="panel-footer">
   <button type="submit" class='btn btn-primary' name="submitbuttonname" id="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit Request'); ?></button></div>
</div>
</div>
</form>

<script>
jQuery(document).ready(function(){
   //Date Pickers
  jQuery('#leave_request_start_date').datepicker();
  jQuery('#leave_request_end_date').datepicker();


});
</script>
