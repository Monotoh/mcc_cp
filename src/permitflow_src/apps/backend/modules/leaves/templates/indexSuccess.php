<?php
/**
 * indexSuccess.php template.
 *
 * Allow Reviewers to request for Job Leaves
 *
 * @package    frontend
 * @subpackage leaves
 * @author    Boniface Irungu - OTBAfrica boniface@otbafrica.com
 */
use_helper("I18N");
?>


 <div class="pageheader">
    <h2><i class="fa fa-certificate"></i><?php echo __('Work Leaves'); ?></h2>
  <div class="breadcrumb-wrapper">

    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>backend.php"><?php echo __('Home'); ?></a></li>
      <li><a href="<?php echo public_path(); ?>backend.php/leaves/index"><?php echo __('Leaves'); ?></a></li>
    </ol>
  </div>
</div>

<div class="contentpanel">
    <div class="row">
    <?php if($sf_user->hasFlash('notice')): ?>
        <div class="alert alert-success">
            <?php echo $sf_user->getFlash('notice') ?>
        </div>
    <?php endif; ?>
        <?php if($sf_user->hasFlash('error')): ?>
        <div class="alert alert-danger">
            <?php echo $sf_user->getFlash('error') ?>
        </div>
    <?php endif; ?>
 <div class="panel panel-dark widget-btns">
   <div class="panel-heading">
     <h3 class="panel-title"><?php echo __('Staff Leaves'); ?></h3>
      
    </div>
     <div class="pull-right">
          <a class="btn btn-success" href="/backend.php/leaves/new">
              <?php echo __('Submit New Request') ?>
          </a>
    </div>
   <div id="leave_requests" class="table-responsive">
          <?php if(count($leave_requests) > 0 ) {   ?>
           <table class="table mb0">
           <thead>      
              <th><?php echo __('Type'); ?></th>
             <th><?php echo __('Comments'); ?></th>
             <th><?php echo __('Leave Start Date'); ?></th>
             <th><?php echo __('Leave End Date'); ?></th>
             <th><?php echo __('Reviewer') ?></th>
             <th><?php echo __('Approval Status'); ?></th>
            <!-- <th><?php //echo __('Date Requested'); ?></th> -->
             <th><?php echo __('Actions') ?></th>
           </thead>
                <tbody>
                    <?php foreach($leave_requests as $leaves) {?>
                    <tr>
                        <td><?php 
                        $myleave = $leaves->getLeaveType();
                        switch($myleave):
                            case 'sick_leave':
                                echo __('Sick Leave') ;
                                break;
                            case 'vacation_leave':
                                echo __('Vacation Leave');
                                  break;
                            case 'administrative_leave':
                                echo __('Administrative Leave');
                                  break;
                            case 'maternity':
                                echo __('Maternity/Paternity') ;
                                  break;
                            default:
                                echo __('Others') ;
                                  break;
                        endswitch;
                        
                        ?></td>
                        <td><?php echo $leaves->getName() ?></td>
                        <td><?php echo $leaves->getStartDate() ?></td>
                        <td><?php echo $leaves->getEndDate() ?></td>
                        <td><?php echo $leaves->getUser()->getStrFirstName()." ".$leaves->getUser()->getStrLastName() ?></td>
                        <td>
                            
                                <span style="background-color: blue; color: white;">
                                     <?php echo $leaves->getStatus() ?>
                                </span>
                                </td>
                                
                            
                                                 
                        <!--<td><?php //echo $leaves->getCreatedAt() ?></td> -->
                        <td>
                            <?php if($sf_user->mfHasCredential("can_approve_leave_requests")){ ?>
                                    <?php if($leaves->getStatus() == 'pending'): ?>
                                    <a class="btn btn-sm btn-danger" href="/backend.php/leaves/reject/id/<?php echo $leaves->getId() ?>">
                                        Reject
                                    </a>
                                     <a class="btn btn-sm btn-success" href="/backend.php/leaves/approve/id/<?php echo $leaves->getId() ?>">
                                        Approve
                                    </a>
                                     <a class="btn btn-sm btn-warning" href="/backend.php/leaves/cancel/id/<?php echo $leaves->getId() ?>">
                                        Cancel
                                    </a>
                                    <?php endif; ?>
                            
                                    <?php if($leaves->getStatus() == 'cancelled' || $leaves->getStatus() == 'rejected'): ?>
                                    <a class="btn btn-sm btn-black" href="/backend.php/leaves/reset/id/<?php echo $leaves->getId() ?>">
                                        Reset
                                    </a>
                                    <?php endif; ?>
                                    <?php if($leaves->getStatus() == 'ongoing'): ?>
                                    <a class="btn btn-sm btn-black" href="/backend.php/leaves/done/id/<?php echo $leaves->getId() ?>">
                                        Set as Done
                                    </a>
                                    <?php endif; ?>
                            <?php } else { ?>
                                <?php echo ('N/A') ; ?>
                            <?php } ?>
                            
                            
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
           </table>
          <?php } else { ?>
       <div class="alert alert-warning">
           <?php echo ('You have zero leave requests !') ?>
           <?php echo ('Please submit new request by clicking Submit New Request button') ?> 
       </div>
          <?php } ?>


      </div><!-- panel-body-nopadding pt10 -->
  </div><!-- panel -->

 	</div><!--panel-row-->
</div><!--contentpanel-->

<script>
jQuery(document).ready(function(){
  // Date Picker
  jQuery('#datepicker1').datepicker();
  jQuery('#datepicker2').datepicker();
});
</script>
