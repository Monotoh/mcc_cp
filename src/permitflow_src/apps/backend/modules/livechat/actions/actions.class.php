<?php

class liveChatActions extends sfActions
{
    public function executeIndex(sfWebRequest $request)
    {
		$livechat = new LiveChat();
		$livechat->updateOperator($_SESSION['SESSION_CUTEFLOW_USERID']);
		$this->chat_url = $livechat->PHPLiveLogin();
	}
}
