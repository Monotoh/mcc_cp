<?php
/**
 * Login actions.
 *
 * Manages reviewer login and logout.
 *
 * @package    backend
 * @subpackage login
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
class loginActions extends sfActions
{
  /**
   * Executes 'Index' action
   *
   * Display login and form and manages reviewer login
   *
   * @param sfRequest $request A request object
   */
  public function executeIndex(sfWebRequest $request)
  {
    $this->form = new BackendSigninForm();
    if($request->isMethod(sfRequest::POST))
    {
      $this->form->bind($request->getParameter($this->form->getName()));

      if ($this->form->isValid())
      {
          $logins = $request->getPostParameter("login");
          $username = $logins['username'];
          $password = $logins['password'];

          $q = Doctrine_Query::create()
             ->from("CfUser a")
             ->where("a.struserid = ? OR a.stremail = ?", array($username,$username))
             ->andWhere('a.bdeleted = ?', 0);
          $available_user = $q->fetchOne();

          if($available_user)
          {
            $hash = $available_user->getStrpassword();
            if (password_verify($password, $hash)) {
                if (password_needs_rehash($hash, PASSWORD_BCRYPT, $options)) {
                    $hash = password_hash($password, PASSWORD_BCRYPT, $options);
                    /* Store new hash in db */
                    //$available_user->setStrpassword($hash);
                    //$available_user->save();
                }

                $this->getUser()->setAttribute('username', $username);
                $this->getUser()->setAttribute('userid', $available_user->getNid());
                $this->getUser()->setAttribute('logintime', date("Y-m-d g:i:s"));
                $this->getUser()->setAuthenticated(true);

                //Backward compatibility after improving login for backend
                  //To be deprecated after all cuteflow modules have been deleted
                $_SESSION["SESSION_CUTEFLOW_USERID"] = $available_user->getNid();

                $available_user->setTslastaction(strtotime(date("Y-m-d g:i:s")));
                $available_user->save();

                //Save Audit Log
                //$audit = new Audit();
                //$audit->saveFullAudit("Logged into of system",$available_user->getNid(),"cf_user","","");

                //Add credentials to user

                //redirect to dashboard
                //return $this->redirect("/backend.php/dashboard");

		//Redirect to referer
		$signinUrl = sfConfig::get('app_sf_guard_plugin_success_signin_url', $request->getReferer($request->getReferer()));
	        return $this->redirect('' != $signinUrl ? $signinUrl : '/backend.php/dashboard');
                $this->loginerror = "You are already logged in.";
            }
            else
            {
              $this->loginerror = "Invalid Username/Password.";
              //Save Audit Log
              $audit = new Audit();
              $audit->saveFullAudit("Failed login attempt","","cf_user","username: ".$username,"");
            }
          }
          else
          {
             $this->loginerror = "Invalid Username/Password. ";
             //Save Audit Log
             $audit = new Audit();
             $audit->saveFullAudit("Failed login attempt","","cf_user","username: ".$username,"");
          }
      }
    }
	  $this->setLayout(false);
  }

  /**
   * Executes 'Logout' action
   *
   * Manages reviewer logout
   *
   * @param sfRequest $request A request object
   */
  public function executeLogout(sfWebRequest $request)
  {
    //Save Audit Log
    $audit = new Audit();
    $audit->saveFullAudit("Logged out of system",$_SESSION["SESSION_CUTEFLOW_USERID"],"cf_user","","");

    $this->getUser()->getAttributeHolder()->clear();
    $this->getUser()->clearCredentials();
    $this->getUser()->setAuthenticated(false);
    $_SESSION["SESSION_CUTEFLOW_USERID"] = false;
      $_SESSION['create_user'] = "";

	  $this->redirect("/backend.php/login");
  }


 public function executeForgot(sfWebRequest $request)
 {
    $this->form = new BackendForgotForm();
    if($request->isMethod(sfRequest::POST))
    {
      $this->form->bind($request->getParameter($this->form->getName()));

      if ($this->form->isValid())
      {
          $forgot = $request->getPostParameter("forgot");
          $email = $forgot['email'];

          $q = Doctrine_Query::create()
             ->from("CfUser a")
             ->where("a.stremail = ?", $email)
             ->andWhere('a.bdeleted = ?', 0);
          $available_user = $q->fetchOne();

          if($available_user)
          {
              $random_pass = rand(10000, 1000000);
              $random_code = rand(10000, 1000000000);

              $temp_pass = password_hash($random_pass, PASSWORD_BCRYPT);
              $temp_code = md5($random_code);

              $available_user->setStrtemppassword($temp_pass);
              $available_user->setStrtoken($temp_code);

              $available_user->save();

              //Send account recovery email
              $body = "
                Hi {$available_user->getStrfirstname()} {$available_user->getStrlastname()}, <br>
                <br>
                You have requested to reset your account password. Use the link below to reset it now: <br>
                <br>
                Temporary Password: {$random_pass}
                <br>
                ---- <br>
                http://".$_SERVER['HTTP_HOST']."/backend.php/login/recover/code/{$temp_code} <br>
                ---- <br>
                <br>
                Thanks,<br>
                ".sfConfig::get('app_organisation_name').".<br>
            ";

            $mailnotifications = new mailnotifications();
            $mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $available_user->getStremail(),"Password Reset",$body);

            //Save Audit Log
            $audit = new Audit();
            $audit->saveFullAudit("Submitted request to reset password",$available_user->getNid(),"cf_user","","");

              return $this->redirect("/backend.php/login/notification");
          }
          else
          {
            //Save Audit Log
            $audit = new Audit();
            $audit->saveFullAudit("Failed attempt to reset password","","cf_user","","");
          }
      }
      else
      {
        $this->forgoterror = "Invalid form";
      }
    }
    $this->setLayout(false);
 }


  public function executeRecover(sfWebRequest $request)
  {
      $this->form = new BackendRecoverForm();
      $this->token = $request->getParameter("code");
      if($request->isMethod(sfRequest::POST))
      {
        $this->form->bind($request->getParameter($this->form->getName()));

        if ($this->form->isValid())
        {
          $recovery = $request->getPostParameter("recovery");
          $password = $recovery['password'];
          $token = $this->token;

          $q = Doctrine_Query::create()
             ->from("CfUser a")
             ->where("a.strtoken = ?", $token)
             ->andWhere('a.bdeleted = ?', 0);
          $available_user = $q->fetchOne();

          if($available_user)
          {
            if(password_verify($password, $available_user->getStrtemppassword()))
            {
              //Change code to protect the recovery form
              $random_code = rand(10000, 1000000000);
              $temp_code = md5($random_code);

              //Remove temporary password since its now useless because we changed the token
              $available_user->setStrtoken($temp_code);
              $available_user->setStrtemppassword("");
              $available_user->save();

              //Save Audit Log
              $audit = new Audit();
              $audit->saveFullAudit("Reset Password",$available_user->getNid(),"cf_user","","");

              return $this->redirect("/backend.php/login/reset/code/".$temp_code);

            }
            else
            {
              $this->recoveryerror = "Invalid password. Check your email and confirm your using the same password.";

              //Save Audit Log
              $audit = new Audit();
              $audit->saveFullAudit("Failed attempt to reset password due to invalid password","","cf_user","","");
            }
          }
          else
          {
            $this->recoveryerror = "Invalid token. Check your email and confirm you using the same link.";

            //Save Audit Log
            $audit = new Audit();
            $audit->saveFullAudit("Failed attempt to reset password due to invalid token","","cf_user","","");
          }
        }
        else
        {
          $this->recoveryerror = "Invalid Form";
          //Save Audit Log
          $audit = new Audit();
          $audit->saveFullAudit("Failed attempt to reset password due to invalid form","","cf_user","","");
        }
      }
      $this->setLayout(false);
  }


  public function executeReset(sfWebRequest $request)
  {
      $this->form = new BackendResetForm();
      $this->token = $request->getParameter("code");
      if($request->isMethod(sfRequest::POST))
      {
        $this->form->bind($request->getParameter($this->form->getName()));

        if ($this->form->isValid())
        {
          $recovery = $request->getPostParameter("reset");
          $password = $recovery['password1'];
          $token = $this->token;

          $q = Doctrine_Query::create()
             ->from("CfUser a")
             ->where("a.strtoken = ?", $token)
             ->andWhere('a.bdeleted = ?', 0);
          $available_user = $q->fetchOne();

          if($available_user)
          {
              $new_password_hash = password_hash($password, PASSWORD_BCRYPT);

              //set new password hash
              $available_user->setStrpassword($new_password_hash);
              $available_user->save();

              //Remove temporary password and token
              $available_user->setStrtoken("");
              $available_user->setStrtemppassword("");
              $available_user->save();

              //Send account recovery email
              $body = "<br>
             Hi {$available_user->getStrfirstname()} {$available_user->getStrlastname()},<br>
             <br>
              Your account has been successfully reset.<br>
              <br>
              Thanks,<br>
              ".sfConfig::get('app_organisation_name').".<br>
              <br>
              ---- <br>
              If you did not authorize this, please contact us and let us know. <br>
          ";

           $mailnotifications = new mailnotifications();
           $mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $available_user->getStremail(),"Password Reset",$body);

              return $this->redirect("/backend.php/login/resetdone");
          }
          else
          {
            $this->recoveryerror = "Invalid token. Check your email and confirm you using the same link.";

            //Save Audit Log
            $audit = new Audit();
            $audit->saveFullAudit("Failed attempt to reset password due to invalid code","","cf_user","","");
          }
        }
        else
        {
          $this->recoveryerror = "Invalid Form";
          //Save Audit Log
          $audit = new Audit();
          $audit->saveFullAudit("Failed attempt to reset password due to invalid form","","cf_user","","");
        }
      }
      $this->setLayout(false);
  }


  public function executeResetdone(sfWebRequest $request)
  {
      $this->setLayout(false);
  }

  public function executeNotification(sfWebRequest $request)
  {
      $this->setLayout(false);
  }


 public function executeSetlocale(sfWebRequest $request)
 {
   $this->getUser()->setCulture($request->getParameter("code"));
   $this->redirect($request->getReferer());
 }
}
