<?php
  use_helper('I18N');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="WebmastersAfrica">
  <link rel="shortcut icon" href="<?php echo public_path(); ?>assets_unified/images/favicon.png" type="image/png">

  <title><?php echo sfConfig::get('app_organisation_name'); ?> - <?php echo __('Sign In'); ?></title>

  <?php
  $translation = new Translation();
  if($translation->IsLeftAligned())
  {
  ?>
  <link href="<?php echo public_path(); ?>assets_unified/css/style.default.css" rel="stylesheet">
  <link href="<?php echo public_path(); ?>assets_unified/css/bootstrap-override.css" rel="stylesheet">
  <link href="<?php echo public_path(); ?>assets_unified/css/bootstrap.min" rel="stylesheet">
  <link href="<?php echo public_path(); ?>assets_unified/css/ectzn/custom.css" rel="stylesheet">
  <?php
  }
  else
  {
  ?>
    <link href="<?php echo public_path(); ?>assets_unified/css/style.default.css" rel="stylesheet">
  <link href="<?php echo public_path(); ?>assets_unified/css/style.default-rtl.css" rel="stylesheet">
  <link href="<?php echo public_path(); ?>assets_unified/css/bootstrap-override-rtl.css" rel="stylesheet">
  <link href="<?php echo public_path(); ?>assets_unified/css/bootstrap-rtl.min" rel="stylesheet">

  <link href="<?php echo public_path(); ?>assets_unified/css/ectzn/custom.css" rel="stylesheet">

  <?php
  }
  ?>
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->

  <?php
  if(sfConfig::get('app_zopim_key'))
  {
  ?>
  <!--Start of Zopim Live Chat Script-->
  <script type="text/javascript">
  window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
  d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
  _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
  $.src="//v2.zopim.com/?<?php echo sfConfig::get('app_zopim_key'); ?>";z.t=+new Date;$.
  type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
  </script>
  <!--End of Zopim Live Chat Script-->
  <?php
  }
  ?>
</head>
<body class="signin">

<!-- Preloader
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>
-->

<section>

    <div class="signinpanel">

        <div class="row">

            <?php
            	$disabled = "";
            	if(is_dir(dirname(__FILE__)."/../../../../../../html/install"))
            	{
            		?>
            		<div align="center">
            		<h1 style="color: red;"><?php echo __('Please delete install folder before you login.'); ?></h1>
            		<br><br>
            		</div>
            		<?php
            	}
            ?>

            <div class="col-md-7">

                <div class="signin-info">
                    <div class="logopanel">
                          <h1><img src="<?php echo public_path(); ?>assets_unified/images/mcclogo2.png"  alt="Construction Permit Management Information System" /></h1>
                    </div><!-- logopanel -->

                    <div class="mb20"></div>

                    <h5><strong><?php echo __('Construction Permit Management Information System') ?></strong></h5>
                    <ul>
                        <li><i class="fa fa-arrow-circle-o-right mr5"></i> <?php echo __('Review submitted applications'); ?></li>
                        <li><i class="fa fa-arrow-circle-o-right mr5"></i> <?php echo __('Send invoices to clients'); ?></li>
                        <li><i class="fa fa-arrow-circle-o-right mr5"></i> <?php echo __('Share comments between reviewers'); ?></li>
                        <li><i class="fa fa-arrow-circle-o-right mr5"></i> <?php echo __('Generate permits for approved applications'); ?></li>
                        <li><i class="fa fa-arrow-circle-o-right mr5"></i> <?php echo __('and much more...'); ?></li>
                    </ul>
                    <div class="mb20"></div>
                    <strong><?php echo __('Not a member? Please contact your System Administrator'); ?></strong>
                </div><!-- signin0-info -->

            </div><!-- col-sm-7 -->

            <div class="col-md-5">

            	<?php
            	$disabled = "";
            	if(is_dir(dirname(__FILE__)."/../../../../../../html/install"))
            	{
            		$disabled = "disabled='disabled'";
            	}
            	?>
              <?php
              if(!$sf_user->isAuthenticated())
              {
              ?>
                <form method="post" action="/backend.php/login/index" autocomplete="off">
                    <h4 class="nomargin"><?php echo __('Sign In'); ?></h4>
                      <?php if ($form->isCSRFProtected()) : ?>
                          <?php echo $form['_csrf_token']->render(); ?>
                      <?php endif; ?>
                      <?php echo $form->renderGlobalErrors() ?>
                      <?php
            					    if($loginerror)
            					    {
                            echo $loginerror;
                					 ?>
                					  <p class="mt5 mb20" style="color: #F00;">
                					  <?php echo __('The username or password you entered is incorrect.'); ?>
                					  </p>
                					  <?php
              					  }
              					  else
              					  {
              					  ?>
                          <p class="mt5 mb20"><?php echo __('Login to access your account.'); ?></p>
                          <?php
              					  }
              			?>
                    <input type="text" id="login_username" placeholder="Username or Email" class="form-control uname" size="60" name="login[username]">
                    <input type="password" id="login_password" placeholder="Password" class="form-control pword" size="60" name="login[password]">
                    <a href="/backend.php/login/forgot"><small><?php echo __('Forgot Your Password / Change your password') ?> </small></a>
                    <button class="btn btn-success btn-block" type="submit" <?php echo $disabled; ?>><?php echo __('Sign In'); ?></button>

                    <?php
                    $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
                    mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);



                    $sql = "SELECT * FROM ext_locales";
                    $rows = mysql_query($sql);

                    $setlocale = $sf_user->getCulture();

                    if(mysql_num_rows($rows) > 1)
                    {
                    ?>
                    <br>
                    <select style="width: 150px; margin-bottom:0" name='locale' id='locale' onChange="window.location='/backend.php/languages/setlocale/code/' + this.value;">
                        <?php
                        while($row = mysql_fetch_assoc($rows))
                        {
                          $selected = "";
                          if($setlocale == $row['locale_identifier'])
                          {
                              $selected = "selected";
                          }
                        ?>
                        <option value='<?php echo $row['locale_identifier']; ?>' <?php echo $selected; ?>><?php echo $row['local_title']; ?></option>
                          <?php
                    }
                    ?>
                    </select>
                    <?php
                    }
                    ?>

                  </div>
                </form>
                <?php
                }
                else
                {
                  ?>
                  <form>
                  <a href="/backend.php/dashboard">Click here to go back to dashboard</a>
                  <hr>
                  <a href="/backend.php/login/logout">Click here to logout</a>
                  </form>
                  <?php
                }
                ?>
            </div><!-- col-sm-5 -->

        <div class="signup-footer">
            <div class="pull-left">
                &copy; <?php echo date("Y"); ?>. <?php echo __('All Rights Reserved.'); ?>
            </div>
        </div>

        </div><!-- row -->

    </div><!-- signin -->

</section>


<script src="<?php echo public_path(); ?>assets_unified/js/jquery-1.10.2.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/bootstrap.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/modernizr.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/retina.min.js"></script>

<script src="<?php echo public_path(); ?>assets_unified/js/custom.js"></script>

</body>
</html>
