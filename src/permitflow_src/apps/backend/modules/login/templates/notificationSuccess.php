<?php
  use_helper('I18N');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="WebmastersAfrica">
  <link rel="shortcut icon" href="<?php echo public_path(); ?>assets_unified/images/favicon.png" type="image/png">

  <title><?php echo sfConfig::get('app_organisation_name'); ?> - <?php echo __('Sign In'); ?></title>

  <link href="<?php echo public_path(); ?>assets_unified/css/style.default.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>
<body class="signin">

<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>

    <div class="lockedpanel">
        <div class="locked">
            <i class="fa fa-lock"></i>
        </div>
        <div class="loginuser">
            An email has been sent to you. Use it to recover your account.
        </div>
    </div><!-- lockedpanel -->

</section>

<script src="<?php echo public_path(); ?>assets_unified/js/jquery-1.10.2.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/bootstrap.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/modernizr.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/retina.min.js"></script>

<script src="<?php echo public_path(); ?>assets_unified/js/custom.js"></script>

</body>
</html>
