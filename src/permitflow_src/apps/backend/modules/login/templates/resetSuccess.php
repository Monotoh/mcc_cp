<?php
  use_helper('I18N');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="WebmastersAfrica">
  <link rel="shortcut icon" href="<?php echo public_path(); ?>assets_unified/images/favicon.png" type="image/png">

  <title><?php echo sfConfig::get('app_organisation_name'); ?> - <?php echo __('Sign In'); ?></title>

  <link href="<?php echo public_path(); ?>assets_unified/css/style.default.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>
<body class="signin">

<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>

    <div class="lockedpanel">
        <div class="locked">
            <i class="fa fa-lock"></i>
        </div>
        <div class="loginuser">
        <?php
          if($token == "")
          {
            ?>
            Cannot recover your account
            <?php
          }
          else
          {
        ?>
            Enter and confirm your new password in the form below.
         <form method="post" action="/backend.php/login/reset/code/<?php echo $token; ?>" autocomplete="off">
            <?php if ($form->isCSRFProtected()) : ?>
                <?php echo $form['_csrf_token']->render(); ?>
            <?php endif; ?>
            <?php echo $form->renderGlobalErrors() ?>
            <div style="color: #F80000;"><?php echo $recoveryerror; ?></div>
            <input type="password" id="reset_password1" name="reset[password1]" required="required" placeholder="Enter New Password" class="form-control" />
            <input type="password" id="reset_password2" name="reset[password2]" required="required" placeholder="Confirm Password" class="form-control" />
            <button class="btn btn-success btn-block" type="submit">Change Password</button>
         </form>
         <?php
          }
         ?>
        </div>
    </div><!-- lockedpanel -->

</section>

<script src="<?php echo public_path(); ?>assets_unified/js/jquery-1.10.2.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/bootstrap.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/modernizr.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/retina.min.js"></script>

<script src="<?php echo public_path(); ?>assets_unified/js/custom.js"></script>

</body>
</html>
