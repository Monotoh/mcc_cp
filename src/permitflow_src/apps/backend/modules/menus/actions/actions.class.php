<?php
/**
 * Menus actions.
 *
 * Manages the stages of the workflow
 *
 * @package    backend
 * @subpackage menus
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
class menusActions extends sfActions
{
   /**
    * 
    * @param sfWebRequest $request
    * Function that can duplicate an existing workflow
    */
   public function executeDuplicateWorkflow(sfWebRequest $request)
   {
     $menu_id = $request->getParameter('id');
     error_log("Duplicate workflow id >>>> ".$menu_id) ;
     $workflow_manager = new WorkflowManager();
     //pass details to create the menu
     $workflow_manager->duplicateMenu($menu_id) ;
     $this->redirect('/backend.php/services/index');
   }
     /**
    * 
    * @param sfWebRequest $request
    * Function that can duplicate an existing workflow
    */
   public function executeDuplicateForms(sfWebRequest $request)
   {
     $menu_id = $request->getParameter('id');
     error_log("Duplicate workflow id >>>> ".$menu_id) ;
     $workflow_manager = new WorkflowManager();
     //pass details to create the menu
     $workflow_manager->duplicateForms($menu_id) ;
     $this->redirect('/backend.php/services/index');
   }
   /**
    * Duplicate permits
    */
   public function executeDuplicatePermits(sfWebRequest $request){
       $menu_id = $request->getParameter('id');
       error_log("Duplicate workflow id >>>> ".$menu_id) ;
       $workflow_manager = new WorkflowManager();
       //pass details to create the menu
     $workflow_manager->duplicatePermits($menu_id) ;
     $this->redirect('/backend.php/services/index');
   }
   
   /**
    * Duplicate permits
    */
   public function executeDuplicateInvoices(sfWebRequest $request){
       $menu_id = $request->getParameter('id');
       error_log("Duplicate workflow id >>>> ".$menu_id) ;
       $workflow_manager = new WorkflowManager();
       //pass details to create the menu
     $workflow_manager->duplicateInvoices($menu_id) ;
     $this->redirect('/backend.php/services/index');
   }
   
   /**
    * 
    * @param sfWebRequest $request
    * OTB patch
    */
   public function executeMachFormDuplicateForms(sfWebRequest $request){
        $audit = new Audit();
        $audit->saveAudit("", "Accessed form duplicate settings");

        $prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";
        require($prefix_folder.'includes/init.php');

	require($prefix_folder.'config.php');
	require($prefix_folder.'includes/db-core.php');
	require($prefix_folder.'includes/helper-functions.php');
	require($prefix_folder.'includes/check-session.php');
	require($prefix_folder.'includes/users-functions.php');
        ///
        //call our helper class
        $otbhelper = new OTBHelper();
        //select form ids to duplicate
        $menu_id = $request->getParameter('id') ;
        //
        $sql_forms = "select form_id from ap_forms where form_stage in (select id from sub_menus where menu_id = $menu_id )";
        $sql_forms_res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($sql_forms) ;
        //
        foreach($sql_forms_res as $r){

	$dbh = mf_connect_db();
	$mf_settings = mf_get_settings($dbh);

	$form_id = $r['form_id'];
	$duplicate_success = false;

	//check permission, is the user allowed to create new form?
	if(empty($_SESSION['mf_user_privileges']['priv_administer']) && empty($_SESSION['mf_user_privileges']['priv_new_forms'])){
		die("Access Denied. You don't have permission to create new form.");
	}

	//get the new form name
	$query 	= "select form_name from `".MF_TABLE_PREFIX."forms` where form_id=?";
	$params = array($form_id);

	$sth = mf_do_query($query,$params,$dbh);
	$row = mf_do_fetch_result($sth);

	$form_name 	 = trim($row['form_name']);
	$form_name .= " Copy";

	//get the new form_id
	$query = "select max(form_id)+1 new_form_id from `".MF_TABLE_PREFIX."forms`";
	$params = array();

	$sth = mf_do_query($query,$params,$dbh);
	$row = mf_do_fetch_result($sth);
	$new_form_id = (int) $row['new_form_id'];
	$new_form_id += rand(100,1000);

	//get the columns of ap_forms table
	$query = "show columns from ".MF_TABLE_PREFIX."forms";
	$params = array();

	$columns = array();
	$sth = mf_do_query($query,$params,$dbh);
	while($row = mf_do_fetch_result($sth)){
		if($row['Field'] == 'form_id' || $row['Field'] == 'form_name'){
			continue; //MySQL 4.1 doesn't support WHERE on show columns, hence we need this
		}
		$columns[] = $row['Field'];
	}

	$columns_joined = implode(",",$columns);

	//insert the new record into ap_forms table
	$query = "insert into
							`".MF_TABLE_PREFIX."forms`(form_id,form_name,{$columns_joined})
					   select
							? , ? ,{$columns_joined}
						from
							`".MF_TABLE_PREFIX."forms`
						where
							form_id = ?";
	$params = array($new_form_id,$form_name,$form_id);
	mf_do_query($query,$params,$dbh);

	//create the new table
	$query = "create table `".MF_TABLE_PREFIX."form_{$new_form_id}` like `".MF_TABLE_PREFIX."form_{$form_id}`";
	$params = array();
	mf_do_query($query,$params,$dbh);

	//copy ap_form_elements table

	//get the columns of ap_form_elements table
	$query = "show columns from ".MF_TABLE_PREFIX."form_elements";
	$params = array();

	$columns = array();
	$sth = mf_do_query($query,$params,$dbh);
	while($row = mf_do_fetch_result($sth)){
		if($row['Field'] == 'form_id'){
			continue; //MySQL 4.1 doesn't support WHERE on show columns, hence we need this
		}
		$columns[] = $row['Field'];
	}

	$columns_joined = implode(",",$columns);

	//insert the new record into ap_form_elements table
	$query = "insert into
							`".MF_TABLE_PREFIX."form_elements`(form_id, {$columns_joined})
					   select
							? , {$columns_joined}
						from
							`".MF_TABLE_PREFIX."form_elements`
						where
							form_id = ?";
	$params = array($new_form_id,$form_id);
	mf_do_query($query,$params,$dbh);

	//copy ap_element_options table

	//get the columns of ap_element_options table
	$query = "show columns from ".MF_TABLE_PREFIX."element_options";
	$params = array();

	$columns = array();
	$sth = mf_do_query($query,$params,$dbh);
	while($row = mf_do_fetch_result($sth)){
		if($row['Field'] == 'form_id' || $row['Field'] == 'aeo_id'){
			continue; //MySQL 4.1 doesn't support WHERE on show columns, hence we need this
		}
		$columns[] = $row['Field'];
	}

	$columns_joined = implode("`,`",$columns);

	//insert the new record into ap_element_options table
	$query = "insert into
							`".MF_TABLE_PREFIX."element_options`(form_id, `{$columns_joined}`)
					   select
							? , `{$columns_joined}`
						from
							`".MF_TABLE_PREFIX."element_options`
						where
							form_id = ?";
	$params = array($new_form_id,$form_id);
	mf_do_query($query,$params,$dbh);



	//copy ap_element_prices table

	//get the columns of ap_element_prices table
	$query = "show columns from ".MF_TABLE_PREFIX."element_prices";
	$params = array();

	$columns = array();
	$sth = mf_do_query($query,$params,$dbh);
	while($row = mf_do_fetch_result($sth)){
		if($row['Field'] == 'form_id' || $row['Field'] == 'aep_id'){
			continue; //MySQL 4.1 doesn't support WHERE on show columns, hence we need this
		}
		$columns[] = $row['Field'];
	}

	$columns_joined = implode("`,`",$columns);

	//insert the new record into ap_element_prices table
	$query = "insert into
							`".MF_TABLE_PREFIX."element_prices`(form_id, `{$columns_joined}`)
					   select
							? , `{$columns_joined}`
						from
							`".MF_TABLE_PREFIX."element_prices`
						where
							form_id = ?";
	$params = array($new_form_id,$form_id);
	mf_do_query($query,$params,$dbh);


	//copy ap_field_logic_elements table

	//get the columns of ap_logic_elements table
	$query = "show columns from ".MF_TABLE_PREFIX."field_logic_elements";
	$params = array();

	$columns = array();
	$sth = mf_do_query($query,$params,$dbh);
	while($row = mf_do_fetch_result($sth)){
		if($row['Field'] == 'form_id'){
			continue; //MySQL 4.1 doesn't support WHERE on show columns, hence we need this
		}
		$columns[] = $row['Field'];
	}

	$columns_joined = implode("`,`",$columns);

	//insert the new record into ap_field_logic_elements table
	$query = "insert into
							`".MF_TABLE_PREFIX."field_logic_elements`(form_id, `{$columns_joined}`)
					   select
							? , `{$columns_joined}`
						from
							`".MF_TABLE_PREFIX."field_logic_elements`
						where
							form_id = ?";
	$params = array($new_form_id,$form_id);
	mf_do_query($query,$params,$dbh);

	//copy ap_field_logic_conditions table

	//get the columns of ap_field_logic_conditions table
	$query = "show columns from ".MF_TABLE_PREFIX."field_logic_conditions";
	$params = array();

	$columns = array();
	$sth = mf_do_query($query,$params,$dbh);
	while($row = mf_do_fetch_result($sth)){
		if($row['Field'] == 'form_id' || $row['Field'] == 'alc_id'){
			continue; //MySQL 4.1 doesn't support WHERE on show columns, hence we need this
		}
		$columns[] = $row['Field'];
	}

	$columns_joined = implode("`,`",$columns);

	//insert the new record into ap_field_logic_conditions table
	$query = "insert into
							`".MF_TABLE_PREFIX."field_logic_conditions`(form_id, `{$columns_joined}`)
					   select
							? , `{$columns_joined}`
						from
							`".MF_TABLE_PREFIX."field_logic_conditions`
						where
							form_id = ?
				    order by
				    		alc_id asc";
	$params = array($new_form_id,$form_id);
	mf_do_query($query,$params,$dbh);

	//copy ap_page_logic table

	//get the columns of ap_page_logic table
	$query = "show columns from ".MF_TABLE_PREFIX."page_logic";
	$params = array();

	$columns = array();
	$sth = mf_do_query($query,$params,$dbh);
	while($row = mf_do_fetch_result($sth)){
		if($row['Field'] == 'form_id'){
			continue; //MySQL 4.1 doesn't support WHERE on show columns, hence we need this
		}
		$columns[] = $row['Field'];
	}

	$columns_joined = implode("`,`",$columns);

	//insert the new record into ap_page_logic table
	$query = "insert into
							`".MF_TABLE_PREFIX."page_logic`(form_id, `{$columns_joined}`)
					   select
							? , `{$columns_joined}`
						from
							`".MF_TABLE_PREFIX."page_logic`
						where
							form_id = ?";
	$params = array($new_form_id,$form_id);
	mf_do_query($query,$params,$dbh);

	//copy ap_page_logic_conditions table

	//get the columns of ap_page_logic_conditions table
	$query = "show columns from ".MF_TABLE_PREFIX."page_logic_conditions";
	$params = array();

	$columns = array();
	$sth = mf_do_query($query,$params,$dbh);
	while($row = mf_do_fetch_result($sth)){
		if($row['Field'] == 'form_id' || $row['Field'] == 'apc_id'){
			continue; //MySQL 4.1 doesn't support WHERE on show columns, hence we need this
		}
		$columns[] = $row['Field'];
	}

	$columns_joined = implode("`,`",$columns);

	//insert the new record into ap_page_logic_conditions table
	$query = "insert into
							`".MF_TABLE_PREFIX."page_logic_conditions`(form_id, `{$columns_joined}`)
					   select
							? , `{$columns_joined}`
						from
							`".MF_TABLE_PREFIX."page_logic_conditions`
						where
							form_id = ? order by apc_id asc";
	$params = array($new_form_id,$form_id);
	mf_do_query($query,$params,$dbh);




	
	//OTB Patch Start - Also duplicate dependency logic when duplicating form
	//get the columns of ap_dependency_logic table
	$query = "show columns from ".MF_TABLE_PREFIX."dependency_logic";
	$params = array();

	$columns = array();
	$logic_ids_old = array();
	$logic_ids_new = array();
	$sth = mf_do_query($query,$params,$dbh);
	while($row = mf_do_fetch_result($sth)){
		if($row['Field'] == 'form_id' || $row['Field'] == 'id'){
			continue; //MySQL 4.1 doesn't support WHERE on show columns, hence we need this
		}
		$columns[] = $row['Field'];
	}

	$columns_joined = implode("`,`",$columns);

	//get the old logic ids
	$query = "select id
						from
							`".MF_TABLE_PREFIX."dependency_logic`
						where
							form_id = ?";
	$params = array($form_id);
	$sth = mf_do_query($query,$params,$dbh);
	while($row = mf_do_fetch_result($sth)){
		$logic_ids_old[] = $row['id'];
	}

	//insert the new record into ap_dependency_logic table
	$query = "insert into
							`".MF_TABLE_PREFIX."dependency_logic`(form_id, `{$columns_joined}`)
					   select
							? , `{$columns_joined}`
						from
							`".MF_TABLE_PREFIX."dependency_logic`
						where
							form_id = ?";
	$params = array($new_form_id,$form_id);
	mf_do_query($query,$params,$dbh);

	//get the new/duplicated logic ids
	$query = "select id
						from
							`".MF_TABLE_PREFIX."dependency_logic`
						where
							form_id = ?";
	$params = array($new_form_id);
	$sth = mf_do_query($query,$params,$dbh);
	while($row = mf_do_fetch_result($sth)){
		$logic_ids_new[] = $row['id'];
	}

	//copy ap_dependency_logic_conditions table

	//get the columns of ap_dependency_logic_conditions table
	$query = "show columns from ".MF_TABLE_PREFIX."dependency_logic_conditions";
	$params = array();

	$columns = array();
	$sth = mf_do_query($query,$params,$dbh);
	while($row = mf_do_fetch_result($sth)){
		if($row['Field'] == 'form_id' || $row['Field'] == 'id'){
			continue; //MySQL 4.1 doesn't support WHERE on show columns, hence we need this
		}
		$columns[] = $row['Field'];
	}

	$columns_joined = implode("`,`",$columns);

	//insert the new record into ap_dependency_logic_conditions table
	$query = "insert into
							`".MF_TABLE_PREFIX."dependency_logic_conditions`(form_id, `{$columns_joined}`)
					   select
							? , `{$columns_joined}`
						from
							`".MF_TABLE_PREFIX."dependency_logic_conditions`
						where
							form_id = ? order by id asc";
	$params = array($new_form_id,$form_id);
	mf_do_query($query,$params,$dbh);

	//copy ap_dependency_logic_rule_permitted_values table

	//get the columns of ap_dependency_logic_rule_permitted_values table
	$query = "show columns from ".MF_TABLE_PREFIX."dependency_logic_rule_permitted_values";
	$params = array();

	$columns = array();
	$sth = mf_do_query($query,$params,$dbh);
	while($row = mf_do_fetch_result($sth)){
		if($row['Field'] == 'form_id' || $row['Field'] == 'id'){
			continue; //MySQL 4.1 doesn't support WHERE on show columns, hence we need this
		}
		$columns[] = $row['Field'];
	}

	$columns_joined = implode("`,`",$columns);

	//insert the new record into ap_dependency_logic_rule_permitted_values table
	$query = "insert into
							`".MF_TABLE_PREFIX."dependency_logic_rule_permitted_values`(form_id, `{$columns_joined}`)
					   select
							? , `{$columns_joined}`
						from
							`".MF_TABLE_PREFIX."dependency_logic_rule_permitted_values`
						where
							form_id = ? order by id asc";
	$params = array($new_form_id,$form_id);
	mf_do_query($query,$params,$dbh);


		//Update the new/duplicated conditions and permitted values to pick the new/duplicated logic ids 
	foreach($logic_ids_old as $old_logic_key=>$old_logic_value){
		$params = array($logic_ids_new[$old_logic_key], $new_form_id, $old_logic_value);
		$query = "update
								`".MF_TABLE_PREFIX."dependency_logic_conditions`
						   set logic_id = ?
							where
								form_id = ? and logic_id =?";
		mf_do_query($query,$params,$dbh);

		$query = "update
								`".MF_TABLE_PREFIX."dependency_logic_rule_permitted_values`
						   set logic_id = ?
							where
								form_id = ? and logic_id =?";
		mf_do_query($query,$params,$dbh);
	}
	//OTB Patch End - Also duplicate dependency logic when duplicating form




	//copy review table, if there is any
	$review_table_exist = true;
	try {
		  $dbh->query("select count(*) from `".MF_TABLE_PREFIX."form_{$form_id}_review`");
	} catch(PDOException $e) {
		  $review_table_exist  = false;
	}

	if($review_table_exist){
		$query = "CREATE TABLE `".MF_TABLE_PREFIX."form_{$new_form_id}_review` like `".MF_TABLE_PREFIX."form_{$form_id}_review`";
		mf_do_query($query,$params,$dbh);
	}


	//create data folder for this form
	if(is_writable($mf_settings['data_dir'])){

		$old_mask = umask(0);
		mkdir($mf_settings['data_dir']."/form_{$new_form_id}",0777);
		mkdir($mf_settings['data_dir']."/form_{$new_form_id}/css",0777);
		if($mf_settings['data_dir'] != $mf_settings['upload_dir']){
			@mkdir($mf_settings['upload_dir']."/form_{$new_form_id}",0777);
		}
		mkdir($mf_settings['upload_dir']."/form_{$new_form_id}/files",0777);

		//copy css file
		copy($mf_settings['data_dir']."/form_{$form_id}/css/view.css",$mf_settings['data_dir']."/form_{$new_form_id}/css/view.css");

		umask($old_mask);
	}

	//insert into permissions table
	$query = "insert into ".MF_TABLE_PREFIX."permissions(form_id,user_id,edit_form,edit_entries,view_entries) values(?,?,1,1,1)";
	$params = array($new_form_id,$_SESSION['mf_user_id']);
	mf_do_query($query,$params,$dbh);



	$duplicate_success = true;

	$response_data = new stdClass();

	if($duplicate_success){
		$response_data->status    	= "ok";
	}else{
		$response_data->status    	= "error";
	}

	$response_data->form_id 	= $new_form_id;
	$response_json = json_encode($response_data);

	$_SESSION['MF_SUCCESS'] = 'Forms Duplicated!!!.';
       }
        $this->redirect('/backend.php/forms/index');
   }
   
   
  public function executeBatch(sfWebRequest $request)
  {
    if($request->getPostParameter('delete'))
    {
      $item = Doctrine_Core::getTable('Menus')->find(array($request->getPostParameter('delete')));
      if($item)
      {
        //Check if any of the submenus has applications
          //If they don't, mark as deleted
         $sub_menus = Doctrine_Core::getTable('SubMenus')
          ->createQuery('a')
          ->where('a.menu_id = ?', $item)
          ->orderBy('a.order_no ASC')
          ->execute();

          $item->delete();
          foreach($sub_menus as $sub_menu)
          {
            $sub_menu->setDeleted("1");
            $sub_menu->save();
          }
      }
    }
    exit;
  }

   /**
   * Executes 'Index' action
   *
   * Lists all the available stages
   *
   * @param sfRequest $request A request object
   */
    public function executeIndex(sfWebRequest $request)
    {
		//OTB Patch Start - Only show services that user has access to
		$agency_manager = new AgencyManager();
		$allowed_service_ids = $agency_manager->getAllowedServices($_SESSION["SESSION_CUTEFLOW_USERID"]);
		//OTB Patch End - Only show services that user has access to
      $this->menuss = Doctrine_Core::getTable('Menus')
        ->createQuery('a')
		->whereIn('a.id', $allowed_service_ids)//OTB Patch - Only show services that user has access to
            ->orderBy('a.order_no ASC')
        ->execute();
          $this->setLayout("layout-settings");
    }
    /**
     * Executes 'Checkname' action
     *
     * Ajax used to check existence of name
     *
     * @param sfRequest $request A request object
     */
    public function executeCheckname(sfWebRequest $request)
    {
        // add new user
        $q = Doctrine_Query::create()
           ->from("Menus a")
           ->where('a.title = ?', $request->getPostParameter('name'));
        $existinggroup = $q->execute();
        if(sizeof($existinggroup) > 0)
        {
              echo '<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Name is already in use!</strong></div><script language="javascript">document.getElementById("submitbuttonname").disabled = true;</script>';
              exit;
        }
        else
        {
              echo '<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Name is available!</strong></div><script language="javascript">document.getElementById("submitbuttonname").disabled = false;</script>';
              exit;
        }
    }

  /**
   * Push order up.
   *
   * @return Response
   */
  public function executeOrderup(sfWebRequest $request)
  {
    $this->forward404Unless($menu = Doctrine_Core::getTable('Menus')->find(array($request->getParameter('id'))), sprintf('Object menus does not exist (%s).', $request->getParameter('id')));

    $q = Doctrine_Query::create()
         ->from("Menus a")
         ->where("a.order_no < ?", $menu->getOrderNo())
         ->orderBy('a.order_no DESC');
    $previous_menu = $q->fetchOne();

    if($previous_menu)
    {
      $current_order = $menu->getOrderNo();
      $previous_order = $previous_menu->getOrderNo();

      $previous_menu->setOrderNo(-1); //temporary set page order to prevent conflict
      $previous_menu->save();

      $menu->setOrderNo($previous_order);
      $menu->save();

      $previous_menu->setOrderNo($current_order);
      $previous_menu->save();
    }

    return $this->redirect("/backend.php/menus/index");
  }

  /**
   * Push order up.
   *
   * @return Response
   */
  public function executeOrderdown(sfWebRequest $request)
  {
    $menu = Doctrine_Core::getTable('Menus')->find(array($request->getParameter("id")));

    $q = Doctrine_Query::create()
         ->from("Menus a")
         ->where("a.order_no > ?", $menu->getOrderNo())
         ->orderBy('a.order_no ASC');
    $previous_menu = $q->fetchOne();

    if($previous_menu)
    {
      $current_order = $menu->getOrderNo();
      $previous_order = $previous_menu->getOrderNo();

      $previous_menu->setOrderNo(-1); //temporary set page order to prevent conflict
      $previous_menu->save();

      $menu->setOrderNo($previous_order);
      $menu->save();

      $previous_menu->setOrderNo($current_order);
      $previous_menu->save();
    }

    return $this->redirect("/backend.php/menus/index");
  }

       /**
       * Executes 'Duplicate' action
       *
       * Duplicates an entire workflow starting with the menu/stage
       *
       * @param sfRequest $request A request object
       */
        public function executeDuplicate(sfWebRequest $request)
        {
            $this->forward404Unless($menu = Doctrine_Core::getTable('Menus')->find(array($request->getParameter('id'))), sprintf('Object menus does not exist (%s).', $request->getParameter('id')));
            $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
            mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);


            //Mf_guard_group (create new)
            $newgroup = new MfGuardGroup();
            $newgroup->setName($menu->getTitle()." Copy ".date("Y-m-d h:m:s"));
            $newgroup->setDescription("Access to ".$menu->getTitle()." copy menu");
            $newgroup->save();
            //Menus
            $newmenu = new Menus();
            $newmenu->setTitle($menu->getTitle()." Copy ".date("Y-m-d h:m:s"));
            $newmenu->setOrderNo($menu->getOrderNo()+1);
            $newmenu->save();

            $q = Doctrine_Query::create()
               ->from("MfGuardPermission a")
               ->where("a.name LIKE ?","%accessmenu".$newmenu->getId()."%");
            $existing_permission = $q->execute();

            if(sizeof($existing_permission) <= 0)
            {
                //Mf_guard_permission (create menu permission and assign to new group)
                $credential = new MfGuardPermission();
                $credential->setName('accessmenu'.$newmenu->getId());
                $credential->setDescription("Access to ".$newmenu->getTitle()." menu");
                $credential->save();

                    if($credential)
                    {
                    //$newgroup->setMfGuardGroupPermission($credential, new mfGuardGroupPermission());
                    $sql = "INSERT INTO mf_guard_group_permission(group_id, permission_id) VALUES('".$newgroup->getId()."','".$credential->getId()."')";
                    mysql_query($sql,$dbconn);
                    }
            }

            //Sub_menus
            $q = Doctrine_Query::create()
               ->from("SubMenus a")
               ->where("a.menu_id = ?", $menu->getId());
            $submenus = $q->execute();
            foreach($submenus as $submenu)
            {
                //Menus
                $newsubmenu = new SubMenus();
                $newsubmenu->setTitle($submenu->getTitle());
                $newsubmenu->setOrderNo($submenu->getOrderNo()+1);
                $newsubmenu->setMenuId($newmenu->getId());
                $newsubmenu->save();

                 $q = Doctrine_Query::create()
                    ->from("MfGuardPermission a")
                    ->where("a.name LIKE ?","%accesssubmenu".$newsubmenu->getId()."%");
                 $existing_permission = $q->execute();

                 if(sizeof($existing_permission) <= 0)
                 {
                    //Mf_guard_permission (create submenu permission and assign to new group)
                    $credential = new MfGuardPermission();
                    $credential->setName('accesssubmenu'.$newsubmenu->getId());
                    $credential->setDescription("Access to ".$newsubmenu->getTitle()." submenu");
                    $credential->save();
                    //$newgroup->setMfGuardGroupPermission($credential);
                    $sql = "INSERT INTO mf_guard_group_permission(group_id, permission_id) VALUES('".$newgroup->getId()."','".$credential->getId()."')";
                    mysql_query($sql,$dbconn);
                 }
                //Sub_menus

                //Buttons
                $q = Doctrine_Query::create()
                   ->from("SubMenuButtons a")
                   ->where("a.sub_menu_id = ?", $submenu->getId());
                $submenubuttons = $q->execute();
                foreach($submenubuttons as $submenubutton)
                {
                    $q = Doctrine_Query::create()
                       ->from("Buttons a")
                       ->where("a.id = ?", $submenubutton->getButtonId());
                    $button = $q->fetchOne();

                    if(!empty($button)){
                        $newbutton = new Buttons();
                        $newbutton->setTitle($button->getTitle());
                        $link = $button->getLink();
                        $newbutton->setLink($link);
                        $newbutton->save();
                    }

                    if($newbutton)
                    {
                        $q = Doctrine_Query::create()
                            ->from("MfGuardPermission a")
                            ->where("a.name LIKE ?","%accessbutton".$newbutton->getId()."%");
                         $existing_permission = $q->execute();

                         if(sizeof($existing_permission) <= 0)
                         {
                            //Mf_guard_permission (create button permission and assign to new group)
                            $credential = new MfGuardPermission();
                            $credential->setName('accessbutton'.$newbutton->getId());
                            $credential->setDescription("Access to ".$newbutton->getTitle()." button");
                            $credential->save();
                            //$newgroup->setMfGuardGroupPermission($credential);
                            $sql = "INSERT INTO mf_guard_group_permission(group_id, permission_id) VALUES('".$newgroup->getId()."','".$credential->getId()."')";
                            mysql_query($sql,$dbconn);
                         }
                        //Sub_menus

                        //Sub_menu_buttons
                        $newsubmenubutton = new SubMenuButtons();
                        $newsubmenubutton->setSubMenuId($newsubmenu->getId());
                        $newsubmenubutton->setButtonId($newbutton->getId());
                        $newsubmenubutton->save();
                    }
                }
            }


            $this->redirect("/backend.php/menus/index");
        }

    public function executeNew(sfWebRequest $request)
    {
      $this->form = new MenusForm();
            $this->setLayout("layout-settings");
    }

    public function executeCreate(sfWebRequest $request)
    {
      $this->forward404Unless($request->isMethod(sfRequest::POST));

      $this->form = new MenusForm();

      $this->new = true;

      $this->processForm($request, $this->form);

      $this->setTemplate('new');
    }

    public function executeEdit(sfWebRequest $request)
    {
      $this->forward404Unless($menus = Doctrine_Core::getTable('Menus')->find(array($request->getParameter('id'))), sprintf('Object menus does not exist (%s).', $request->getParameter('id')));
      $this->form = new MenusForm($menus);
      $this->setTemplate('edit');
      $this->setLayout("layout-settings");
    }

    public function executeUpdate(sfWebRequest $request)
    {
      $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
      $this->forward404Unless($menus = Doctrine_Core::getTable('Menus')->find(array($request->getParameter('id'))), sprintf('Object menus does not exist (%s).', $request->getParameter('id')));
      $this->form = new MenusForm($menus);

      $this->processForm($request, $this->form);

      $this->setTemplate('edit');
    }

    public function executeDelete(sfWebRequest $request)
    {

      $this->forward404Unless($menus = Doctrine_Core::getTable('Menus')->find(array($request->getParameter('id'))), sprintf('Object menus does not exist (%s).', $request->getParameter('id')));

          $q = Doctrine_Query::create()
             ->from('SubMenus a')
             ->where('a.menu_id = ?', $menus->getId());
          $submenus = $q->execute();

          $delete_me = true;

          foreach($submenus as $sub_menu)
          {
            $existing_applications = Doctrine_Core::getTable('FormEntry')
              ->createQuery('a')
              ->where('a.approved = ?', $sub_menu->getId())
              ->execute();
            if(sizeof($existing_applications) > 0)
            {
              $delete_me = false;
              break;
            }
          }

          if($delete_me)
          {

            foreach($submenus as $submenu)
            {
              $submenu->setMenuId("0");
              $submenu->setDeleted("1");
              $submenu->save();
            }

            $menus->delete();
          }



      $this->redirect("/backend.php/menus/index");
    }

    protected function processForm(sfWebRequest $request, sfForm $form)
    {
      $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
      if ($form->isValid())
      {
        $menus = $form->save();

        if($this->new)
        {
          $menus->setOrderNo($menus->getId());
          $menus->save();
        }

        $translation = new translation();
        $translation->setTranslation("menus","title",$menus->getId(),$menus->getTitle());
  

            //check if credential exists for this menu
            $q = Doctrine_Query::create()
               ->from('MfGuardPermission a')
                   ->where('a.name = ?', 'accessmenu'.$menus->getId());
            $credential = $q->execute();
            if(sizeof($credential) == 0)
            {
                    $credential = new MfGuardPermission();
                    $credential->setName('accessmenu'.$menus->getId());
                    $credential->setDescription("Access to ".$menus->getTitle()." menu");
                    $credential->save();
            }


                $groups = $request->getPostParameter('allowed_groups');


                $q = Doctrine_Query::create()
                 ->from("MfGuardPermission a")
                 ->where("a.name = ?", "accessmenu".$menus->getId());
                $permission = $q->fetchOne();
                if($permission)
                {
                  //continue since permission Exists
                }
                else
                {
                  $permission = new MfGuardPermission();
                  $permission->setName('accessmenu'.$menus->getId());
                  $permission->setDescription("Access to ".$menus->getTitle()." menu");
                  $permission->save();
                }


                $q = Doctrine_Query::create()
                 ->from("MfGuardPermission a")
                 ->where("a.name = ?", "accessmenu".$menus->getId());
                $permission = $q->fetchOne();
                if($permission)
                {
                  $grouppermissions = $permission->getGuardPermissions();
                  foreach($grouppermissions as $grouppermission)
                  {
                    $grouppermission->delete();
                  }
                }

                $groups = $request->getPostParameter('allowed_groups');

                foreach($groups as $group)
                {
                     $q = Doctrine_Query::create()
                     ->from("MfGuardGroup a")
                     ->where("a.id = ?", $group)
                     ->orderBy("a.name ASC");
                  $group = $q->fetchOne();
                  if($group)
                  {
                    $found = false;
                    $grouppermissions = $group->getPermissions();
                    foreach($grouppermissions as $grouppermission)
                    {
                        if($permission->getId() == $grouppermission->getId())
                        {
                          //permission already exists
                        }
                        else
                        {
                          $found = true;
                        }
                    }

                    if($found)
                    {
                      //add permission to group
                      $permissiongroup = new MfGuardGroupPermission();
                      $permissiongroup->setGroupId($group->getId());
                      $permissiongroup->setPermissionId($permission->getId());
                      $permissiongroup->save();
                    }
                  }
                }

          // $q = Doctrine_Query::create()
          //    ->from('SubMenus a')
          //    ->where('a.menu_id = ?', $menus->getId());
          // $submenus = $q->execute();

          // foreach($submenus as $submenu)
          // {
          //         $submenu->setMenuId("0");
          //         $submenu->save();
          // }

          //   $stageids = $request->getPostParameter("stages");
          //   $count = 1;
          //   foreach($stageids as $stageid)
          //   {
          //       $stage = Doctrine_Core::getTable('SubMenus')->find(array($stageid));
          //       $stage->setMenuId($menus->getId());
          //       $stage->setOrderNo($count++);
          //       $stage->save();
          //   }


        $this->redirect("/backend.php/menus/index");
      }
    }
}
