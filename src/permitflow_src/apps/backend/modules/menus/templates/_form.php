<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php
use_helper("I18N");
?>
<script type="text/javascript" src="<?php echo public_path(); ?>assets_unified/js/jquery.bootstrap-duallistbox.js"></script>



<form id="stageform" class="form-bordered form-horizontal" action="<?php echo url_for('/backend.php/menus/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post">

<div class="panel panel-dark">
<div class="panel-heading">
  <h3 class="panel-title"><?php echo ($form->getObject()->isNew() ? __('New Workflow') : __('Edit Workflow')); ?></h3>
 </div>
<div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
  <strong><?php echo __('Well done'); ?>!</strong> <?php echo __('You successfully updated this stage group'); ?></a>.
</div>
<div class="panel-body panel-body-nopadding">

    <?php

    $translation = new translation();

    if(!$form->getObject()->isNew())
    {
       if($translation->getTranslation('menus','title',$form->getObject()->getId()))
       {
          $title = $translation->getTranslation('menus','title',$form->getObject()->getId());
         ?>
         <script language="javascript">
          jQuery(document).ready(function(){
            $("#menus_title").val("<?php echo $title; ?>");
          });
         </script>
         <?php
       }
    }

    ?>


	  <?php if (!$form->getObject()->isNew()): ?>
		<input type="hidden" name="sf_method" value="put" />
		<?php endif; ?>
      <?php echo $form->renderGlobalErrors() ?>

	   <?php if(isset($form['_csrf_token'])): ?>
            <?php echo $form['_csrf_token']->render(); ?>
            <?php endif; ?>


      <div class="form-group">
         <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Title'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['title']->renderError() ?>
          <?php echo $form['title']->render(array('required' => 'required')); ?>
        </div>
      </div>
                
     <!-- OTB - Start patch add app queuing -->
     <div class="form-group">
         <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Application Queuing'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['app_queuing']->renderError() ?>
          <?php echo $form['app_queuing']->render(array('required' => 'required')); ?>
        </div>
      </div>
     <!-- OTB - End patch add app queuing -->
     <!-- OTB - Start patch add service code -->
     <div class="form-group">
         <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Code e.g. district code,'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['service_code']->renderError() ?>
          <?php echo $form['service_code']->render(array('required' => 'required')); ?>
        </div>
      </div>
     <!-- OTB - End patch add service code -->
     <!-- OTB - Start patch add sms sender ID -->
     <div class="form-group">
         <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('SMS Notification Sender Name'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['sms_sender']->renderError() ?>
          <?php echo $form['sms_sender']->render(array('required' => 'required')); ?>
        </div>
      </div>
     <!-- OTB - End patch add sms sender ID -->

      <div id="nameresult" name="nameresult"></div>

        <script language="javascript">
          $('document').ready(function(){
            $('#menus_title').keyup(function(){
              $.ajax({
                        type: "POST",
                        url: "/backend.php/menus/checkname",
                        data: {
                            'name' : $('input:text[id=menus_title]').val()
                        },
                        dataType: "text",
                        success: function(msg){
                              //Receiving the result of search here
                              $("#nameresult").html(msg);
                        }
                    });
                });
          });
        </script>
      <div class="form-group">
      <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Groups'); ?></i></label>
      <div class="col-sm-8">
        <select name='allowed_groups[]' id='allowed_groups' multiple>
          <?php
            $selected = "";
            $q = Doctrine_Query::create()
               ->from("MfGuardGroup a")
               ->orderBy("a.name ASC");
            $groups = $q->execute();
            foreach($groups as $group)
            {
              $selected = "";
              $grouppermissions = $group->getPermissions();
              foreach($grouppermissions as $grouppermission)
              {
                if(!$form->getObject()->isNew())
                {
                  $q = Doctrine_Query::create()
                     ->from("MfGuardPermission a")
                     ->where("a.name = ?", "accessmenu".$form->getObject()->getId());
                  $permission = $q->fetchOne();
                  if($permission->getId() == $grouppermission->getId())
                  {
                    $selected = "selected";
                  }
                }
              }
              echo "<option value='".$group->getId()."' ".$selected.">".$group->getName()."</option>";
            }
          ?>
        </select>
      </div>
      </div>
      <script>
      jQuery(document).ready(function(){
        var demo2 = $('[id="allowed_groups"]').bootstrapDualListbox();
      });
      </script>
     </div><!--panel-body-->


        <div class="panel-footer">
		<button class="btn btn-danger mr10"><?php echo __('Reset'); ?></button><button type="submit" class="btn btn-primary" name="submitbuttonname" id="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button>
		</div>

  </fieldset>
</div>
</form>
<script>
jQuery(document).ready(function(){

  $( "#addgroup" ).click(function() {
      $("#groups").append("<div class='form-group' class='formgroup'><label class='col-sm-4'>Name</label><div class='col-sm-8'> <input type='text' name='name[]' class='form-control' /></div><label class='col-sm-4'>Description</label><div class='col-sm-8'><textarea name='description[]' class='form-control' /></textarea></div><a style='float: right; margin-top: 10px;' href='#' class='panel-close'>&times;</a></div>");
  });

  // Date Picker
  jQuery('#datepicker1').datepicker();
  jQuery('#datepicker2').datepicker();


});
</script>
