<?php
use_helper("I18N");

$audit = new Audit();
$audit->saveAudit("", "Accessed stage group settings");

if($sf_user->mfHasCredential("managestages"))
{
  $_SESSION['current_module'] = "stages";
  $_SESSION['current_action'] = "index";
  $_SESSION['current_id'] = "";
?>
<div class="contentpanel">
<div class="panel panel-dark">
<div class="panel-heading">
			<h3 class="panel-title"><?php echo __('Workflows'); ?></h3>
				<div class="pull-right">
            <a class="btn btn-primary-alt" style="margin-top:-42px;" id="newstage" href="<?php echo public_path(); ?>backend.php/menus/new"><?php echo __('New Workflow'); ?></a>
</div>
</div>


<div class="panel panel-body panel-body-nopadding">

<div class="table-responsive">
<table class="table dt-on-steroids mb0" id="table3">
  <thead>
    <tr>
      <th class="no-sort" width="40"><input type='checkbox' name='batchall' onclick="boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.name == 'batch') { if(this.checked == true){ box.checked = true; }else{ box.checked = false; } } } "></th>
      <th width="60">#</th>
      <th class="no-sort"><?php echo __('Title'); ?></th>
      <th width="110"><?php echo __('Order'); ?></th>
      <th class="no-sort" width="150"><?php echo __('Actions'); ?></th>
    </tr>
  </thead>
  <tbody>
    <?php
		$count = 1;
	?>
    <?php foreach ($menuss as $menus): ?>
    <tr id="row_<?php echo $menus->getId(); ?>">
      <td><input type='checkbox' name='batch' id='batch_<?php echo $menus->getId() ?>' value='<?php echo $menus->getId() ?>'></td>
      <td><?php echo $count++; ?></td>
      <td><?php

      $translation = new translation();

      if($translation->getTranslation('menus','title',$menus->getId()))
      {
         echo $translation->getTranslation('menus','title',$menus->getId());
      }
      else
      {
        echo $menus->getTitle();
      }
      ?></td>
      <td align="center">
      <a href="/backend.php/menus/orderup/id/<?php echo $menus->getId(); ?>"><span class="glyphicon glyphicon-circle-arrow-up"></span></a>
      <a href="/backend.php/menus/orderdown/id/<?php echo $menus->getId(); ?>"><span class="glyphicon glyphicon-circle-arrow-down"></span></a>
      </td>
      <td align="center">
		<a id="viewstage<?php echo $menus->getId(); ?>" title="<?php echo __('View'); ?>" href="<?php echo public_path(); ?>backend.php/submenus/index/filter/<?php echo $menus->getId(); ?>"><span class="badge badge-primary"><i class="fa fa-eye"></i></span></a>
		<a id="editstage<?php echo $menus->getId(); ?>" title="<?php echo __('Edit'); ?>" href="<?php echo public_path(); ?>backend.php/menus/edit/id/<?php echo $menus->getId(); ?>"><span class="badge badge-primary"><i class="fa fa-pencil"></i></span></a>
		<a id="duplicatestage<?php echo $menus->getId(); ?>" title="<?php echo __('Duplicate'); ?>" href="<?php echo public_path(); ?>backend.php/menus/duplicate/id/<?php echo $menus->getId(); ?>"><span class="badge badge-primary"><i class="fa fa-copy"></i></span></a>
    <a id="deletestage<?php echo $menus->getId(); ?>" onClick="if(confirm('Are you sure you want to delete this item?')){ return true; }else{ return false; }" title="<?php echo __('Delete'); ?>" href="<?php echo public_path(); ?>backend.php/menus/delete/id/<?php echo $menus->getId(); ?>"><span class="badge badge-primary"><i class="fa fa-trash-o"></i></span></a>

    </td>
    </tr>
    <?php endforeach; ?>
	</tbody>
	<tfoot>
   <tr><td colspan='7' style='text-align: left;'>
   <select id='batch_action' name='batch_action' onChange="if(this.value != ''){if(confirm('Are you sure?')){ batch('menus', this.options[this.selectedIndex].text, this.value); document.getElementById('default').selected='selected'; }}">
   <option id='default' value=''><?php echo __('Choose an action'); ?>..</option>
   <option value='delete'><?php echo __('Set As Deleted'); ?></option>
   </select>
   </td></tr>
   </tfoot>
</table>
</div>

</div><!--panel-body-->
</div><!--panel-dark-->
</div>
<script>
  jQuery('#table3').dataTable({
      "sPaginationType": "full_numbers",

      // Using aoColumnDefs
      "aoColumnDefs": [
      	{ "bSortable": false, "aTargets": [ 'no-sort' ] }
    	]
    });

</script>

<?php
}
else
{
  include_partial("accessdenied");
}
?>
