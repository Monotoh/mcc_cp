<?php
/**
 * Messages actions.
 *
 * Messaging Service
 *
 * @package    backend
 * @subpackage messaging
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
class messagesActions extends sfActions
{
    /**
     * Executes 'Index' action
     *
     * Displays list of all messages related to currently logged in reviewer
     *
     * @param sfRequest $request A request object
     */
      public function executeIndex(sfWebRequest $request)
      {
             $q = Doctrine_Query::create()
               ->from('ReviewerComments a')
               ->where('a.reviewer_id = ?', $_SESSION['SESSION_CUTEFLOW_USERID'])
               ->orderBy('a.id DESC');
			$this->messages = $q->execute();

            $this->setLayout('layout');
      }
      
    /**
     * Executes 'Sent' action
     *
     * Displays list of all messages related to currently logged in reviewer
     *
     * @param sfRequest $request A request object
     */
      public function executeSent(sfWebRequest $request)
      {
             $q = Doctrine_Query::create()
               ->from('ReviewerComments a')
               ->where('a.sender_id = ?', $_SESSION['SESSION_CUTEFLOW_USERID'])
               ->orderBy('a.id DESC');
			$this->messages = $q->execute();

            $this->setLayout('layout');
      }
    /**
     * Executes 'View' action
     *
     * Displays full message and a form for replying back to sender
     *
     * @param sfRequest $request A request object
     */
    public function executeView(sfWebRequest $request)
    {
      $q = Doctrine_Query::create()
      	->from("ReviewerComments a")
      	->where("a.id = ?", $request->getParameter('id'));
      $this->message = $q->fetchOne();
    }
    
    /**
     * Executes 'New' action
     *
     * Allows for reviewer to send a new message
     *
     * @param sfRequest $request A request object
     */
    public function executeNew(sfWebRequest $request)
    {
      $this->form = new BannerForm();
          $this->setLayout('layout');
    }
    
    /**
     * Executes 'Create' action
     *
     * Saves new message details to the database
     *
     * @param sfRequest $request A request object
     */
    public function executeCreate(sfWebRequest $request)
    {
      $this->forward404Unless($request->isMethod(sfRequest::POST));

          $message = new ReviewerComments();
          $message->setCommentcontent($request->getPostParameter("txtmessage"));
          $message->setDateCreated(date("Y-m-d"));
          $message->setReviewerId($request->getPostParameter("reviewer"));
          $message->setSenderId($_SESSION['SESSION_CUTEFLOW_USERID']);
          $message->setApplicationId($request->getPostParameter("application"));
          $message->setMessageread("0");
          $message->save();

          $this->redirect("/backend.php/messages/index");
    }

}
