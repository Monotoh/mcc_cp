<?php
/**
 * _form.php partial.
 *
 * Displays form for creating a new message
 *
 * @package    backend
 * @subpackage messages
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>
<form action="/backend.php/messages/create" method="post"  autocomplete="off" data-ajax="false" class="asholder">
<fieldset>
			<section>
				<label>Send To</label>
				<div>
				    <select name='reviewer' id='reviewer'>
					<option>Choose a reviewer...</option>
					<?php
							$q = Doctrine_Query::create()
							 ->from('Department a')
							 ->orderBy('a.department_name ASC');
							$departments = $q->execute();
							foreach($departments as $department)
							{
								echo "<optgroup label='".$department->getDepartmentName()."'>";
									$q = Doctrine_Query::create()
									   ->from('CfUser a')
									   ->where('a.strdepartment = ?', $department->getDepartmentName())
									   ->orderBy('a.strfirstname ASC');
									$reviewers = $q->execute();
									foreach($reviewers as $reviewer)
									{
										//What group does this user belong to
											$q = Doctrine_Query::create()
											   ->from('MfGuardUserGroup a')
											   ->where('a.user_id = ?', $reviewer->getNid());
											$group_relations = $q->execute();
									
											$color = "";
									
											$groupname = "";
									
											foreach($group_relations as $group_relation)
											{
														$q = Doctrine_Query::create()
														   ->from('MfGuardGroup a')
														   ->where('a.id = ?', $group_relation->getGroupId());
														$group = $q->fetchOne();
														
														if($group_relation->getGroupId() == "13")
														{
															$color = "style='background-color: #FFCC66;'";
															$groupname = $group->getName();
														}
														if($group_relation->getGroupId() == "6")
														{
															$color = "style='background-color: #66FF66;'";
															$groupname = $group->getName();
														}
														if($group_relation->getGroupId() == "4")
														{
															$color = "style='background-color: #66FFFF;'";
															$groupname = $group->getName();
														}
														if($group_relation->getGroupId() == "7")
														{
															$color = "style='background-color: #0099CC;'";
															$groupname = $group->getName();
														}
													}
													
													
													echo "<option ".$color." value='".$reviewer->getNid()."'>".$reviewer->getStrfirstname()." ".$reviewer->getStrlastname()."</option>";
									}
								echo "</optgroup>";
							}
					?>
					</select>
				</div>
			  </section>
			  <section>
				<label>Plan No</label>
				<div>
				  <input type="text" name='application' id='application'></textarea>
				</div>
			  </section>
			  <section>
				<label>Message</label>
				<div>
				  <textarea name='messagecontent' id='messagecontent'></textarea>
				</div>
			  </section>
			
	  
	  <section>
			<div><button class="reset">Reset</button><button class="submit" name="submitbuttonname" value="submitbuttonvalue">Submit</button></div>
	  </section>
</fieldset>
</form>

<script type="text/javascript">
	var options = {
		script:"/backend.php/messages/autosuggest?json=true&limit=6&",
		varname:"input",
		json:true,
		shownoresults:false,
		maxresults:6,
		callback: function (obj) { document.getElementById('application').value = obj.id; }
	};
	var as_json = new bsn.AutoSuggest('application', options);
	
</script>