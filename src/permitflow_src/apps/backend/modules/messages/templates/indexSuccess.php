<?php
/**
 * indexSuccess.php template.
 *
 * Displays list of all messages related to currently logged in reviewer
 *
 * @package    backend
 * @subpackage messages
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>
<?php use_helper('I18N', 'Date') ?>

    
    <div class="pageheader">
      <h2><i class="fa fa-envelope"></i> Messages <span>View Message</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo public_path(); ?>backend.php">Home</a></li>
          <li class="active">Messages</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel panel-email">

        <div class="row">
            <div class="col-sm-3 col-lg-2">
                <button class="btn btn-danger btn-block btn-compose-email" data-toggle="modal" data-target="#myModal">Compose Message</button>
                
                <ul class="nav nav-pills nav-stacked nav-email">
                    <li class="active">
                    <a href="<?php echo public_path(); ?>backend.php/messages/index">
                    	<?php
                    	   $q = Doctrine_Query::create()
			               ->from('ReviewerComments a')
			               ->where('a.reviewer_id = ?', $_SESSION['SESSION_CUTEFLOW_USERID'])
			               ->andWhere("a.messageread = 0")
			               ->orderBy('a.id DESC');
			            $unread = $q->execute();
                    	?>
                        <span class="badge pull-right"><?php echo sizeof($unread); ?></span>
                        <i class="glyphicon glyphicon-inbox"></i> Inbox
                    </a>
                    </li>
                    <li><a href="<?php echo public_path(); ?>backend.php/messages/sent"><i class="glyphicon glyphicon-send"></i> Sent Mail</a></li>
                </ul>
                
                <div class="mb30"></div>
            </div><!-- col-sm-3 -->
            
            <div class="col-sm-9 col-lg-10">
                
                <div class="panel panel-default">
                    <div class="panel-body panel-body-nopadding">
                                                <h5 class="subtitle mb5 mt10 ml10">Inbox</h5>
                        <p class="text-muted ml10">Showing <?php echo sizeof($messages); ?> messages</p>
                        
                        <div class="table-responsive">
                            <table class="table table-email">
                              <tbody>
                              <?php foreach ($messages as $message): ?>
                                <tr 
                                <?php
                                if($message->getMessageread() == "0")
                                {
                                ?>
                                class="unread"
                                <?php
                                }
                                ?>
                                >
                                  <td>
                                    <div class="ckbox ckbox-success">
                                        <input type="checkbox" id="checkbox1">
                                        <label for="checkbox1"></label>
                                    </div>
                                  </td>
                                  <td>
                                    <a href="" class="star"><i class="glyphicon glyphicon-star"></i></a>
                                  </td>
                                  <td>
                                    <div class="media">
                                        <div class="media-body">
                                            <span class="media-meta pull-right"><?php echo $message->getDateCreated(); ?></span>
                                            <?php
                                            $q = Doctrine_Query::create()
                                            	->from("CfUser a")
                                            	->where("a.nid = ?", $message->getSenderId());
                                            $sender = $q->fetchOne();
                                            ?>
                                            <a href="<?php echo public_path(); ?>backend.php/messages/view/id/<?php echo $message->getId(); ?>"><h4 class="text-primary"><?php echo $sender->getStrfirstname()." ".$sender->getStrlastname(); ?></h4></a>
                                            <small class="text-muted"></small>
                                            <p class="email-summary"> 
                                            <?php
                                            	 $words = explode(" ",$message->getCommentcontent());
												 echo implode(" ",array_splice($words,0,5))."....";
                                            ?> 
                                            </p>
                                        </div>
                                    </div>
                                  </td>
                                </tr>
                                <?php
                                endforeach;
                                ?>
								</tbody>
                            </table>
                        </div><!-- table-responsive -->
                        
                    </div><!-- panel-body -->
                </div><!-- panel -->
                
            </div><!-- col-sm-9 -->
            
        </div><!-- row -->
    
    </div>
    
  </div><!-- mainpanel -->
  
  
  <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <form method="post"  action="<?php echo public_path(); ?>backend.php/messages/create">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Compose Message</h4>
      </div>
      <div class="modal-body">
        <!-- BASIC WIZARD -->
      <div id="basicWizard" class="basic-wizard">
      
       <fieldset>
                
            <div class="mb20"></div>
              <div class="panel panel-default">
                 <div class="panel-body-nopadding">
                 <select id="reviewer" name="reviewer"  style="display: block !important;">
                 <?php 
	                 $q = Doctrine_Query::create()
	                 	->from("CfUser a")
	                 	->where("a.bdeleted = ?", 0)
	                 	->orderBy("a.Strfirstname DESC");
	                 $reviewers = $q->execute();
	                 
	                 foreach($reviewers as $reviewer)
	                 {
		                 ?>
						 <option value="<?php echo $reviewer->getNid(); ?>"><?php echo $reviewer->getStrfirstname(); ?> <?php echo $reviewer->getStrlastname(); ?></option>
		                 <?php
	                 }
                 ?>
                 </select>
                  <hr>
                  <textarea name='txtmessage' id="wysiwyg" placeholder="Enter text here..." class="form-control" rows="10" data-autogrow="true"></textarea>
             </div>
             <div class="panel-footer">
                  <button type='submit' class="btn btn-primary">Send</button>
             </div>
      
      </div><!-- #basicWizard -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div><!-- modal-content -->
  </div><!-- modal-dialog -->
</div><!-- modal -->