<?php
/**
 * newSuccess.php template.
 *
 * Allows for reviewer to send a new message
 *
 * @package    backend
 * @subpackage messages
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>
<div class="g12" style="padding-left: 4px; padding-top: 10px;">
 <form>
		<label style='height: 30px; margin-top: -2px;'>
			<div style='float: left; font-size: 20px; font-weight: 700;'>
			New Message
			</div>
			<div style='float: right; '>
			<button style="font-size: 12px; margin-top: -5px;" onClick="window.location='<?php echo public_path(); ?>backend.php/messages/index';">Back To Messages</button>
			</div>
		</label>
 </form>

<?php include_partial('form', array('form' => $form)) ?>

</div>
