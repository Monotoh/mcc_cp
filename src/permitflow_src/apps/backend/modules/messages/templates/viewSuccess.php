<?php
/**
 * viewSuccess.php template.
 *
 * Displays full message and a form for replying back to sender
 *
 * @package    backend
 * @subpackage messages
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>
<?php

$message->setMessageread("1");
$message->save();

 ?>

<div class="pageheader">
      <h2><i class="fa fa-envelope"></i> Messages <span>View Message</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo public_path(); ?>backend.php">Home</a></li>
          <li><a href="<?php echo public_path(); ?>backend.php/messages/index">Messages</a></li>
          <li class="active">View Message</li>
        </ol>
      </div>
    </div>

    <div class="contentpanel panel-email">
    <div class="row">

    <div class="panel panel-dark">
    <div class="panel-heading">
    <h3 class="panel-title">View Message</h3>
    </div>

	 <div class="panel-body">


<form action="/backend.php/messages/create" method="post"  autocomplete="off" data-ajax="false">
 <input name='reviewer' id='reviewer' type='hidden' value="<?php echo $message->getSenderId(); ?>">




				Sent By
				<?php
					$q = Doctrine_Query::create()
					   ->from('CfUser a')
					   ->where('a.nid = ?', $message->getSenderId() );
					$reviewer = $q->fetchOne();
					echo "<i>".$reviewer->getStrfirstname()." ".$reviewer->getStrlastname()."</i>";
				?>

            <br>
				  <?php echo html_entity_decode($message->getCommentcontent()); ?> <hr>
          <br>


				<i class="bold-label">Reply</i>

				<textarea name='messagecontent' id='wysiwyg' placeholder="Enter text here..." class="form-control" rows="10"></textarea>

             </div>

			<div class="panelfooter">
            <button class="btn btn-danger">Reset</button>
            <button class="btn btn-primary" name="submitbuttonname" value="submitbuttonvalue">Submit</button>
            </div><!--panel-footer-->
</form>


</div><!--panel-body-->
</div>
