<?php

/**
 * myinvoices actions.
 *
 * @package    permit
 * @subpackage invoices
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class myinvoicesActions extends sfActions
{
    /**
     * 
     * @param sfWebRequest $request
     * MY index action
     */
    public function executeIndex(sfWebRequest $request)
    {
		$invoice_manager = new InvoiceManager();
		$agency_manager = new AgencyManager();

        $payment_status = $request->getParameter("filter_status");
		$this->filter = $request->getParameter("filter");//OTB code refactoring
		$this->fromdate = $request->getPostParameter("fromdate") ? date('Y-m-d', strtotime($request->getPostParameter("fromdate"))) : false;//OTB code refactoring
		$this->todate = $request->getPostParameter("todate") ? date('Y-m-d', strtotime($request->getPostParameter("todate"))) : false;//OTB code refactoring
		$this->q = Doctrine_Query::create()
			->from("MfInvoice a")
			->leftJoin('a.FormEntry')
			->orderBy("a.created_at DESC")
			->whereIn('a.FormEntry.approved', $agency_manager->getAllowedStages($this->getUser()->getAttribute('userid')));
		$qt = Doctrine_Query::create()
			->select('SUM(a.total_amount) as total')
			->from("MfInvoice a")
			->leftJoin('a.FormEntry')
			->whereIn('a.FormEntry.approved', $agency_manager->getAllowedStages($this->getUser()->getAttribute('userid')));

        if($request->getPostParameter("search"))
        {
                $this->q->where("a.invoice_number LIKE ?", "%".$request->getPostParameter("search")."%");
                $qt->where("a.invoice_number = ?", $request->getPostParameter("search"));
        }
        else {
			if($payment_status){
				$this->q->where("a.paid = ?", $payment_status);//OTB code refactoring
				$qt->where("a.paid = ?", $payment_status);//OTB code refactoring
			}
            if ($this->fromdate) {
				$this->q->andWhere("a.created_at BETWEEN ? AND ?", array($this->fromdate . " 00:00:00", $this->todate . " 23:59:59"));
				$qt->andWhere("a.created_at BETWEEN ? AND ?", array($this->fromdate . " 00:00:00", $this->todate . " 23:59:59"));
            }
			if ($this->filter) {
				$this->q->andWhere("a.FormEntry.form_id = ?", $this->filter);
				$qt->andWhere("a.FormEntry.form_id = ?", $this->filter);
			}
        }
		$this->total = $qt->fetchOne();

        if($request->getParameter("export"))
        {
            $columns = "";
            $columns[] = "Date Of Issue";
            $columns[] = "Invoice No";
            $columns[] = "Service Code";
            $columns[] = "Application";
            $columns[] = "User";
            $columns[] = "Phone Number";
            $columns[] = "Fee";
            $columns[] = "Amount Paid";
            //$columns[] = "Reference Number";//OTB - Comment this out for invoices
            $columns[] = "Payment Mode";
            $columns[] = "Payment Status";

            $records = "";

            $invoices = $this->q->execute();

            foreach($invoices as $invoice)
            {
                $q = Doctrine_Query::create()
                    ->from("FormEntry a")
                    ->where("a.id = ?", array($invoice->getAppId()));
                $application = $q->fetchOne();
                if($application) {

                    $merchant_reference = $invoice_manager->get_merchant_reference($invoice->getId());

                    $q = Doctrine_Query::create()
                        ->from("ApFormPayments a")
                        ->where("a.payment_id = ?", $merchant_reference);
                    $payment = $q->fetchOne();

                    $record_columns = "";

                    $record_columns[] = $invoice->getCreatedAt();
                    $record_columns[] = $invoice->getInvoiceNumber();

                    $q = Doctrine_Query::create()
                        ->from("ApForms a")
                        ->where("a.form_id = ?", $application->getFormId());
                    $form = $q->fetchOne();
                    if ($form) {
                        $record_columns[] = $form->getFormCode();
                    } else {
                        $record_columns[] = "";
                    }
                    $record_columns[] = $application->getApplicationId();

                    $q = Doctrine_Query::create()
                        ->from("SfGuardUserProfile a")
                        ->where("a.user_id = ?", $application->getUserId());
                    $user = $q->fetchOne();
                    if ($user) {
                        $record_columns[] = ucwords(strtolower($user->getFullname()));
                        $record_columns[] = $user->getMobile();
                    } else {
                        $record_columns[] = "";
                    }

                    $totalfound = false;
                    foreach ($invoice->getMfInvoiceDetail() as $fee) {
                        if ($fee->getDescription() == "Total") {
                            $totalfound = true;
                            //$record_columns[] = sfConfig::get('app_currency') . ". " . $fee->getAmount();
                            $record_columns[] = $invoice->getCurrency() . ". " . $fee->getAmount();//OTB - Use currency set in invoice
                        }
                    }

                    if ($totalfound == false) {
                        $grand_total = 0;
                        foreach ($invoice->getMfInvoiceDetail() as $fee) {
                            $pos = strpos($fee->getDescription(), "Convenience fee");
                            if ($pos === false) {
                                //add amount to grand total
                            } else {
                                continue;
                            }
                            $grand_total += $fee->getAmount();
                        }
                        //$record_columns[] = sfConfig::get('app_currency') . ". " . $grand_total;
                        $record_columns[] = $invoice->getCurrency() . ". " . $grand_total;//OTB - Use currency set in invoice
                    }

                    $record_columns[] = $payment ? $payment->getPaymentAmount() : 0;
                    //$record_columns[] = $payment->getPaymentId();//OTB - Comment this out for invoices
                    $record_columns[] = ucfirst($payment ? $payment->getPaymentMerchantType() : False);

                    if ($invoice->getPaid() == "1") {
                        $record_columns[] = "Not Paid";
                    } elseif ($invoice->getPaid() == "15") {
                        $record_columns[] = "Pending Confirmation";
                    } elseif ($invoice->getPaid() == "2") {
                        $record_columns[] = "Paid";
                    }

                    $records[] = $record_columns;

                }

            }

            if($this->total && $this->filter)
            {
                $total_amount = $this->total->getTotal();

                $q = Doctrine_Query::create()
                    ->from("ApForms a")
                    ->where("a.form_id = ?", $this->filter);

                $filtered_form = $q->fetchOne();

                if($filtered_form)
                {
                    $main_currency = $filtered_form->getPaymentCurrency();
                }
                else
                {
                    $main_currency = sfConfig::get('app_currency');//OTB - Use Default Currency in config file
                }

                /*if($main_currency == "KES")
                {
                    $total_amount = $total_amount - (count($this->q->count()) * 50);
                }
                elseif($main_currency == "USD")
                {
                    $total_amount = $total_amount - (count($this->q->count()) * 1);
                }*///OTB - comment out this convenience fee stuff

                $record_columns = "";
                $record_columns[] = "";
                $record_columns[] = "";
                $record_columns[] = "";
                $record_columns[] = "";
                $record_columns[] = "";
                $record_columns[] = "Total";
                $record_columns[] = $total_amount;
                $record_columns[] = "";
                $record_columns[] = "";
                $record_columns[] = "";

                $records[] = $record_columns;
            }

            $this->ReportGenerator("Invoices Report ".date("Y-m-d"), $columns, $records);
            exit;
        }

        $this->pager = new sfDoctrinePager('MfInvoice', 10);
        $this->pager->setQuery($this->q);
        $this->pager->setPage($request->getParameter('page', 1));
        $this->pager->init();
    }

}
