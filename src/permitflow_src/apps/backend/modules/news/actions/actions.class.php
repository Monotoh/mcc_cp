<?php

/**
 * News actions.
 *
 * @package    permit
 * @subpackage news
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class newsActions extends sfActions
{
	
  public function executeBatch(sfWebRequest $request)
  {

       if($request->getPostParameter('delete'))
    {
      $item = Doctrine_Core::getTable('News')->find(array($request->getPostParameter('delete')));
      if($item)
      {
        $item->delete();
      }
    }
		
  }

  /**
   * Executes 'Checkname' action
   *
   * Ajax used to check existence of name
   *
   * @param sfRequest $request A request object
   */
  public function executeCheckname(sfWebRequest $request)
  {
      // add new user
      $q = Doctrine_Query::create()
         ->from("News a")
         ->where('a.title = ?', $request->getPostParameter('name'));
      $existinggroup = $q->execute();
      if(sizeof($existinggroup) > 0)
      {
            echo '<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Title is already in use!</strong></div>';
            exit;
      }
      else
      {
            echo '<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Title is available!</strong></div>';
            exit;
      }
  }

	
  public function executeIndex(sfWebRequest $request)
  {
	if($request->getParameter("ptoggle"))
	 {
		 $content = Doctrine_Core::getTable('News')->find(array($request->getParameter('ptoggle')));
		 if($content->getPublished() == "1")
		 {
			 $content->setPublished("0");
		 }
		 else
		 {
			 $content->setPublished("1");
		 }
		 $content->save();
	 }  
	
	  
    $q = Doctrine_Query::create()
       ->from('News a')
	   ->orderBy('a.id ASC');
     $this->articles = $q->execute();
	 
	 
	$this->setLayout("layout-settings");
  }

  public function executeShow(sfWebRequest $request)
  {
    $this->content = Doctrine_Core::getTable('News')->find(array($request->getParameter('id')));
    $this->forward404Unless($this->content);
	$this->setLayout('layout');
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new NewsForm();
	$this->setLayout("layout-settings");
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new NewsForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($content = Doctrine_Core::getTable('News')->find(array($request->getParameter('id'))), sprintf('Object content does not exist (%s).', $request->getParameter('id')));
    $this->form = new NewsForm($content);
	$this->setLayout("layout-settings");
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($content = Doctrine_Core::getTable('News')->find(array($request->getParameter('id'))), sprintf('Object content does not exist (%s).', $request->getParameter('id')));
    $this->form = new NewsForm($content);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {

    $this->forward404Unless($content = Doctrine_Core::getTable('News')->find(array($request->getParameter('id'))), sprintf('Object content does not exist (%s).', $request->getParameter('id')));

    $audit = new Audit();
    $audit->saveAudit("", "deleted news article of id ".$content->getId());

    $content->delete();

    $this->redirect('/backend.php/news/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
	 
     $content = $form->save();
	  
	  
	  if($content->getPublished() == "")
	  {
		  $content->setPublished("1");
		  $content->save();
	  }
	  
	  
	  $content->setCreatedOn(date("Y-m-d"));
	  $content->setCreatedBy($this->getUser()->getAttribute("user_id"));
	  $content->save();

     $audit = new Audit();
     $audit->saveAudit("", "<a href=\"/backend.php/news/edit?id=".$content->getId()."&language=en\">updated a news article</a>");

      $this->redirect('/backend.php/news/index');
    }
  }
}
