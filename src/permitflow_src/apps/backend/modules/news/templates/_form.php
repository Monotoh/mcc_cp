<?php
	use_helper("I18N");
	$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
	mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
	$locale = '';
?>
<form  id="newsform" name="newsform" action="/backend.php<?php echo url_for('news/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>   autocomplete="off" data-ajax="false">

<div class="panel panel-dark">
<div class="panel-heading">
 <h3 class="panel-title"><?php echo ($form->getObject()->isNew()?__('New News Article'):__('Edit News Article')); ?> <?php
 if($_SESSION['locale']){ 
    $sql = "SELECT * FROM ext_locales WHERE locale_identifier = '".$_SESSION['locale']."'";
    $rows = mysql_query($sql);
    $locale = mysql_fetch_assoc($rows);
    echo " </h3><p> (".$locale['local_title']." Translation)</p>";
 } 
?>

</div>

<div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
  <strong><?php echo __('Well done'); ?>!</strong> <?php echo __('You successfully updated this record'); ?></a>.
</div>
<div class="panel-body panel-body-nopadding form-bordered">


<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>

<?php if(isset($form['_csrf_token'])): ?>
<?php echo $form['_csrf_token']->render(); ?>
<?php endif; ?>


<div class="form-group">
        <label class="col-sm-4" for="text_field"><i class="bold-label"><?php echo __('Title'); ?></i></label>
	<div class="col-sm-12">
		<?php echo $form['title']->renderError() ?>
                <input type="text" name="news[title]" id="news_title" class="form-control" value="<?php 
					$sql = "SELECT * FROM ext_translations WHERE field_id = '".$form->getObject()->getId()."' AND field_name = 'news_title' AND table_class = 'news' AND locale = '".$_SESSION['locale']."'";
					$rows = mysql_query($sql);
					if($row = mysql_fetch_assoc($rows))
					{
						echo $row['trl_content'];
					}
					else
					{
						echo $form->getObject()->getTitle();
					}
				?>">
                
	</div>
</div>


	  <div id="nameresult" name="nameresult"></div>

      <script language="javascript">
        $('document').ready(function(){
          $('#news_title').keyup(function(){
            $.ajax({
                      type: "POST",
                      url: "/backend.php/news/checkname",
                      data: {
                          'name' : $('input:text[id=news_title]').val()
                      },
                      dataType: "text",
                      success: function(msg){
                            //Receiving the result of search here
                            $("#nameresult").html(msg);
                      }
                  });
              });
        });
      </script>
<div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Content'); ?></i></label>
	<div class="col-sm-12">
		<?php echo $form['article']->renderError() ?>
                <textarea name="news[article]" id="top_article_txt" class="form-control"><?php 
					$sql = "SELECT * FROM ext_translations WHERE field_id = '".$form->getObject()->getId()."' AND field_name = 'news_article' AND table_class = 'news' AND locale = '".$_SESSION['locale']."'";
					$rows = mysql_query($sql);
					if($row = mysql_fetch_assoc($rows))
					{
						echo $row['trl_content'];
					}
					else
					{
						echo $form->getObject()->getArticle();
					}
				?></textarea>
                
	        </div>
      </div>
	  
      </div>
	<div class="panel-footer">
		<button class="btn btn-danger mr10"><?php echo __('Reset'); ?></button><button type="submit" class="btn btn-primary" name="submitbuttonname" id="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button>
	</div>
</div>
</form>


<script src="<?php echo public_path(); ?>assets_unified/js/ckeditor/ckeditor.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/ckeditor/adapters/jquery.js"></script>


<script>
jQuery(document).ready(function(){
  
  // CKEditor
  jQuery('#top_article_txt').ckeditor();

});
</script>
