<?php
use_helper("I18N");

$audit = new Audit();
$audit->saveAudit("", "Accessed news settings");

if($sf_user->mfHasCredential("managenews"))
{
  $_SESSION['current_module'] = "news";
  $_SESSION['current_action'] = "index";
  $_SESSION['current_id'] = "";
?>
<div class="contentpanel panel-email">
<div class="panel panel-dark">

<div class="panel-heading">
<h3 class="panel-title"><?php echo __('News Article'); ?></h3>

    <div class="pull-right">
           <a class="btn btn-primary-alt settings-margin42" id="newnews" href="<?php echo public_path(); ?>backend.php/news/new"><?php echo __('New News Article'); ?></a>
    </div>
</div>

<div class="table-responsive">
<table class="table dt-on-steroids mb0" id="table3">
    <thead>
	<tr>
      <th class="no-sort"><input type='checkbox' name='batchall' onclick="boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.name == 'batch') { if(this.checked == true){ box.checked = true; }else{ box.checked = false; } } } "></th>
      <th width="60">#</th>
      <th><?php echo __('Title'); ?></th>
      <th width="60" class="no-sort"><?php echo __('Published'); ?></th>
	  <th class="no-sort" style="width:7%;"><?php echo __('Actions'); ?></th>
    </tr>
    </thead>
    <tbody>
	<?php
		$count = 1;
	?>
    <?php foreach($articles as $content): ?>
    <tr id="row_<?php echo $content->getId() ?>">
	  <td><input type='checkbox' name='batch[]' id='batch_<?php echo $content->getId() ?>' value='<?php echo $content->getId() ?>'></td>
	  <td><?php echo $count++; ?></td>
      <td><a id="editnews2<?php echo $content->getId(); ?>" href="#"><?php echo $content->getTitle() ?></a></td>
      <td align="center"><a id="publish<?php echo $content->getId(); ?>" href="<?php echo public_path(); ?>backend.php/news/index?ptoggle=<?php echo $content->getId(); ?>">
	  <?php
	  if($content->getPublished() == "1")
	  {
		  echo "<span class='badge-round badge-success'><span class='fa fa-check'></span></span>";
	  }
	  else
	  {
		  echo "<span class='badge-round badge-danger'><span class='fa fa-times'></span></span>";
	  }
	  ?>
      </a></td>
     <td>
		<a id="editnews<?php echo $content->getId(); ?>" href="<?php echo public_path(); ?>backend.php/news/edit/id/<?php echo $content->getId(); ?>" title="<?php echo __('Edit'); ?>"><span class="badge badge-primary"><i class="fa fa-pencil"></i></span></a>
		<a id="deletenews<?php echo $content->getId(); ?>" onClick="if(confirm('Are you sure you want to delete this item?')){ return true; }else{ return false; }" href="<?php echo public_path(); ?>backend.php/news/delete/id/<?php echo $content->getId(); ?>" title="<?php echo __('Delete'); ?>"><span class="badge badge-primary"><i class="fa fa-trash-o"></i></span></a>
    
  </td>
    </tr>
    <?php endforeach; ?>
    </tbody>
	<tfoot>
   <tr><td colspan='8' style='text-align: left;'>
   <select id='batch_action' name='batch_action' onChange="if(this.value != ''){if(confirm('Are you sure?')){ batch('news', this.options[this.selectedIndex].text, this.value); document.getElementById('default').selected='selected'; }}">
   <option id='default' value=''><?php echo __('Choose an action'); ?>..</option>
   <option value='delete'><?php echo __('Set As Deleted'); ?></option>
   </select>
   </td></tr>
   </tfoot>
</table>
</div>
</div>
</div>
<script>
jQuery(document).ready(function(){
  jQuery('#table3').dataTable({
      "sPaginationType": "full_numbers",

      // Using aoColumnDefs
      "aoColumnDefs": [
      	{ "bSortable": false, "aTargets": [ 'no-sort' ] }
    	]
    });
});
</script>
<?php
}
else
{
  include_partial("settings/accessdenied");
}
?>
