<?php

/**
 * notification actions.
 *
 * @package    permit
 * @subpackage notification
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class notificationActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeArchitectmailsent(sfWebRequest $request)
  {
	 $this->setLayout("layout");
  }
  public function executeArchitectmail(sfWebRequest $request)
  {
     $q = Doctrine_Query::create()
	     ->from('sfGuardUser a')
		 ->leftJoin('a.Profile p')
		 ->where('a.is_active = 1')
		 ->orderBy('p.Fullname ASC');
	 $this->architects = $q->execute();
	 $this->setLayout("layout");
  }
  public function executeNotificationedit(sfWebRequest $request)
  {
	  
  }
  public function executeNotificationsend(sfWebRequest $request)
  {
	  
  }
}
