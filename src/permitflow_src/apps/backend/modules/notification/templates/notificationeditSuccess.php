
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<?php
	
	$prefix_folder = dirname(__FILE__)."/../../../../..";
	include_once ($prefix_folder."/workflow/config/config.inc.php");
	include_once ($prefix_folder."/workflow/language_files/language.inc.php");
	
	$strParams				= 'language='.$_REQUEST["language"];
	$strEncyrptedParams		= $objURL->encryptURL($strParams);
?>

<head>
	<meta http-equiv="content-type" content="text/html; charset=<?php echo $DEFAULT_CHARSET ?>">
	<title></title>	
	<link rel="stylesheet" href="/asset_misc/pages/format.css" type="text/css">
    
    <link href="/assets_backend/fullcalendar.css" rel="stylesheet" type="text/css" />
<!-- Fancybox/Lightbox Effect -->
<link href="/assets_backend/jquery.fancybox-1.3.4.css" rel="stylesheet" type="text/css" />
<!-- WYSIWYG Editor -->
<link href="/assets_backend/wysiwyg.css" rel="stylesheet" type="text/css" />
<!-- Main Controlling Styles -->
<link href="/assets_backend/main.css" rel="stylesheet" type="text/css" />
<!-- Blue Theme Styles -->
<link href="/asset_themes/blue/styles.css" rel="stylesheet" type="text/css" />	
</head>
<body>
	
	<?php include_partial('dashboard/topbar', array('module' => 'notification','action'=>$action,'archivemode'=>$_REQUEST['archivemode'],'bOwnCirculations'=>$_REQUEST['bOwnCirculations'],'menu_own_circ'=>$MENU_OWN_CIRCULATIONS,'menu_circ'=>$MENU_CIRCULATION,'menu_archive'=>$MENU_ARCHIVE)) ?>
        
            <div id="content">
            <div class="container">
                	<div class="conthead">
                        <h2><?php  echo $MENU_NOTIFICATION; ?></h2>
                    </div>
                    <div class="contentbox">
	
	<form action="/backend.php/notification/notificationsend?key=<?php echo $strEncyrptedParams ?>" name="notification" method="post">

		<table style="border: 1px solid #c8c8c8; background: #efefef;" cellspacing="0" cellpadding="3" width="100%">
			<tr>
				<td class="table_header" colspan="2">
					<?php echo $NOTIFICATION_HEADER ?>:
				</td>
			</tr>
			<tr><td height="10"></td></tr>
			<tr>
				<td valign="top" align="left" width="110">
					<?php echo $NOTIFICATION_SUBJECT ?>:
				</td>
				<td valign="top" align="left">
					<input id="IN_strSubject" name="IN_strSubject" type="text" class="InputText" style="width:450px;" value="<?php echo $TITLE_1.': '.$NOTIFICATION_DEFAULT_SUBJECT;?>">
				</td>
			</tr>
			<tr><td height="10"></td></tr>
			<tr>
				<td valign="top" align="left">
					<?php echo $NOTIFICATION_CONTENT ?>:
				</td>
				<td valign="top" align="left">
					<textarea rows="10" cols="80" class="InputText" style="margin-right: 10px;" name="IN_strContent"></textarea>
				</td>
			</tr>
			<tr><td height="3"></td></tr>
			<tr>
				<td valign="top" align="left">
					<?php echo $NOTIFICATION_RECIEVER ?>:
				</td>
				<td valign="top" align="left">
					<table cellspacing="0" cellpadding="0">
						<tr>
							<td valign="top" align="left">
								<input type="radio" name="IN_nRecievers" value="1" checked>
							</td>
							<td valign="top" align="left">
								<?php echo $NOTIFICATION_RECIEVER_ALL ?>
							</td>
						</tr>
						<tr>
							<td valign="top" align="left">
								<input type="radio" name="IN_nRecievers" value="2">
							</td>
							<td valign="top" align="left">
								<?php echo $NOTIFICATION_RECIEVER_SENDER ?>
							</td>
						</tr>
						<tr>
							<td valign="top" align="left">
								<input type="radio" name="IN_nRecievers" value="3">
							</td>
							<td valign="top" align="left">
								<?php echo $NOTIFICATION_RECIEVER_ONLINE ?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td height="10"></td></tr>
		</table>
		
		<table width="100%">
		<tr>
			<td align="left">
				<input type="reset" class="btnalt" value="<?php echo $BTN_RESET;?>">
			</td>
			<td align="right">
            <div align="right">
				<input type="submit" value="<?php echo $BTN_SEND;?>" class="btn">
             </div>
			</td>
		</tr>
		</table>
		<?php
			$strQuery 		= "SELECT stremail from cf_user WHERE nid = '".$_REQUEST['userid']."' LIMIT 1;";
			$nResult 		= @mysql_query($strQuery);
			$arrResult 		= mysql_fetch_array($nResult, MYSQL_ASSOC);
			$strSenderEmail = $arrResult['stremail'];
		?>
		<input type="hidden" name="strSenderEmail" value="<?php echo $strSenderEmail ?>">
	</form>

</body>
</html>