<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<?php
	$prefix_folder = dirname(__FILE__)."/../../../../..";
	require_once $prefix_folder.'/lib/vendor/cp_workflow/config/config.inc.php';
	require_once $prefix_folder.'/lib/vendor/cp_workflow/language_files/language.inc.php';
	
	$strSubject = $_POST['IN_strSubject'];
	$strContent = $_POST['IN_strContent'];
	$nReciever 	= $_POST['IN_nRecievers'];
	
	$strSender = $_REQUEST['strSenderEmail'];
	
	// get the recievers
	$strQuery = "SELECT stremail FROM cf_user";
	
	switch($nReciever)
	{
		case 2:
			$strQuery .= " WHERE naccesslevel = '8' OR naccesslevel = '2' and bdeleted = 0";
			break;
		case 3:
			$strQuery .= " WHERE tslastaction > '".(time() - $USER_TIMEOUT)."' and bdeleted = 0";
			break;
	}
	
	$nResult = mysql_query($strQuery, $nConnection) or die ("<b>A fatal MySQL error occured</b>.\n<br />Query: " . $strQuery . "<br />\nError: (" . mysql_errno() . ") " . mysql_error());
	
	if ($nResult)
	{
		while ($arrRow = mysql_fetch_array($nResult, MYSQL_ASSOC))
		{
			$arrRecievers[] = $arrRow['stremail'];
		}
	}	
	
	// send the EMail	
	
	// Create the Transport
	if ($MAIL_SEND_TYPE == 'SMTP') {
		$transport = Swift_SmtpTransport::newInstance($SMTP_SERVER, $SMTP_PORT)
  					->setUsername($SMTP_USERID)
  					->setPassword($SMTP_PWD);
  					
		if ($SMPT_ENCRYPTION != 'NONE') {
  			$transport = $transport->setEncryption(strtolower($SMPT_ENCRYPTION));
  		}
	}
	else if ($MAIL_SEND_TYPE == 'PHP') {
		$transport = Swift_MailTransport::newInstance();
	}
	else if ($MAIL_SEND_TYPE == 'MTA') {
		$transport = Swift_SendmailTransport::newInstance($MTA_PATH);
	}
  					
  	// Create the Mailer using the created Transport
	$mailer = Swift_Mailer::newInstance($transport);
	$message = Swift_Message::newInstance()
						->setSubject($strSubject)
						->setFrom(array($strSender => 'CuteFlow'))
						->setBody($strContent)
						->setTo($arrRecievers);
	
	// Send the message
	$result = $mailer->batchSend($message, $failures);
?>

<head>
	<meta http-equiv="content-type" content="text/html; charset=<?php echo $DEFAULT_CHARSET ?>">
	<title></title>	
	<link rel="stylesheet" href="/asset_misc/pages/format.css" type="text/css">	
</head>
<body>
<br>
	<span style="font-size: 14pt; color: #ffa000; font-family: Verdana; font-weight: bold;">
		<?php echo $MENU_NOTIFICATION;?>
	</span>
	<br><br>
	<?php 
	if ($result)
	{
		echo $NOTIFICATION_SUCCESS;
	}
	else
	{
		echo $NOTIFICATION_ERROR;
	}
	?>


</body>
</html>