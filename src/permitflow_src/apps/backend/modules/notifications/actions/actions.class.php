<?php

/**
 * notifications actions.
 *
 * @package    permit
 * @subpackage notifications
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class notificationsActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $q = Doctrine_Query::create()
       ->from('NotificationHistory a')
	   ->orderBy('a.id DESC');
     $this->pager = new sfDoctrinePager('NotificationHistory', 20);
	 $this->pager->setQuery($q);
	 $this->pager->setPage($request->getParameter('page', 1));
	 $this->pager->init();
	 
	 
	$this->setLayout('layout');
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new NotificationHistoryForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new NotificationHistoryForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($notification_history = Doctrine_Core::getTable('NotificationHistory')->find(array($request->getParameter('id'))), sprintf('Object notification_history does not exist (%s).', $request->getParameter('id')));
    $this->form = new NotificationHistoryForm($notification_history);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($notification_history = Doctrine_Core::getTable('NotificationHistory')->find(array($request->getParameter('id'))), sprintf('Object notification_history does not exist (%s).', $request->getParameter('id')));
    $this->form = new NotificationHistoryForm($notification_history);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($notification_history = Doctrine_Core::getTable('NotificationHistory')->find(array($request->getParameter('id'))), sprintf('Object notification_history does not exist (%s).', $request->getParameter('id')));
    $notification_history->delete();

    $this->redirect('notifications/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notification_history = $form->save();

      $this->redirect('notifications/edit?id='.$notification_history->getId());
    }
  }
}
