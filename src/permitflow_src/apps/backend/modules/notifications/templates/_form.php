<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<form action="<?php echo url_for('notifications/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          <?php echo $form->renderHiddenFields(false) ?>
          &nbsp;<a href="<?php echo url_for('notifications/index') ?>">Back to list</a>
          <?php if (!$form->getObject()->isNew()): ?>
            &nbsp;<?php echo link_to('Delete', 'notifications/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
          <?php endif; ?>
          <input type="submit" value="Save" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><?php echo $form['user_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['user_id']->renderError() ?>
          <?php echo $form['user_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['notification']->renderLabel() ?></th>
        <td>
          <?php echo $form['notification']->renderError() ?>
          <?php echo $form['notification'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['notification_type']->renderLabel() ?></th>
        <td>
          <?php echo $form['notification_type']->renderError() ?>
          <?php echo $form['notification_type'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['sent_on']->renderLabel() ?></th>
        <td>
          <?php echo $form['sent_on']->renderError() ?>
          <?php echo $form['sent_on'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['confirmed_receipt']->renderLabel() ?></th>
        <td>
          <?php echo $form['confirmed_receipt']->renderError() ?>
          <?php echo $form['confirmed_receipt'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
