<?php use_helper('I18N', 'Date') ?>


<div class="block-controls">
					
					<ul class="controls-buttons">
						<?php if ($pager->haveToPaginate()): ?>
                        <li><?php echo link_to(image_tag('/assets_backend/images/icons/fugue/navigation-180.png', array('align' => 'absmiddle', 'alt' => __('First'), 'title' => __('First')))." First", $this->getModuleName().'/index?page='.$pager->getFirstPage()) ?></li>
                     <li><?php echo link_to(image_tag('/assets_backend/images/icons/fugue/navigation-180.png', array('align' => 'absmiddle', 'alt' => __('Prev'), 'title' => __('Prev')))." Prev", $this->getModuleName().'/index?page='.$pager->getPreviousPage()) ?></li>
                      <?php foreach ($pager->getLinks() as $page): ?>
                      <?php
					    $url = $_SERVER['REQUEST_URI'];
						if(substr($url,strlen($url)-1,strlen($url)) == $page)
						{
					  ?>
                       <li><?php echo link_to($page, $this->getModuleName().'?page='.$page, array('class' => 'current')) ?></li>
                       <?php
						}
						else
						{
							if($page == 1)
							{
								?>
                       <li><?php echo link_to($page, $this->getModuleName().'?page='.$page, array('class' => 'current')) ?></li>
                        <?php
							}
							else
							{
						?>
                       <li><?php echo link_to($page, $this->getModuleName().'?page='.$page) ?></li>
                        <?php
							}
						}
						?>
                      <?php endforeach; ?>
                        <li><?php echo link_to(image_tag('/assets_backend/images/icons/fugue/navigation.png', array('align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next')))." Next", $this->getModuleName().'?page='.$pager->getNextPage()) ?></li>
                        <li><?php echo link_to(image_tag('/assets_backend/images/icons/fugue/navigation.png', array('align' => 'absmiddle', 'alt' => __('Last'), 'title' => __('Last')))." Last", $this->getModuleName().'?page='.$pager->getLastPage()) ?></li>
                        
                       <?php endif; ?>
					</ul>
					
				</div>
<form action="/backend.php/notifications/batch" method='post'>
<div class="table-responsive">
<table width="100%" cellspacing="0" cellpadding="2">
  <thead>
    <tr>
      <th >#</th>
      <th>User</th>
      <th width="30%">Notification</th>
      <th>Notification Type</th>
      <th>Sent on</th>
      <th>Confirmed receipt</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($pager->getResults() as $notification_history): ?>
    <tr>
      <td><a href="<?php echo url_for('notifications/edit?id='.$notification_history->getId()) ?>"><?php echo $notification_history->getId() ?></a></td>
      <td><?php echo $notification_history->getUserId() ?></td>
      <td width="30%"><?php echo $notification_history->getNotification() ?></td>
      <td><?php echo $notification_history->getNotificationType() ?></td>
      <td><?php echo $notification_history->getSentOn() ?></td>
      <td><?php echo $notification_history->getConfirmedReceipt() ?></td>
      <td>
      
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
</div>
</div>
				
				<ul class="message no-margin">
					<li><?php echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults()) ?></li>
				</ul>
				
				<div class="extrabottom">
					<div class="bulkactions">
					<select name="sf_admin_batch_action" id="table-action" class="small">
						<option value="">Action for selected...</option>
						<option value="deleteSelected">Delete Selected</option>
					</select>
					<button type="submit" class="btn" onclick="if (confirm('Are you sure?')) {return true;}else{return false;}">Ok</button>
				</div>
            </div>
</form>

