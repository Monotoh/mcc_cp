<?php

/**
 * notificationsetup actions.
 *
 * @package    permit
 * @subpackage notificationsetup
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class notificationsetupActions extends sfActions
{
  public function executeBatch(sfWebRequest $request)
  {
		if($request->getPostParameter('delete'))
		{
			$item = Doctrine_Core::getTable('Notifications')->find(array($request->getPostParameter('delete')));
			if($item)
			{
				$item->delete();
			}
		}
    }
  public function executeIndex(sfWebRequest $request)
  {
    $this->notificationss = Doctrine_Core::getTable('notifications')
      ->createQuery('a')
      ->execute();
	$this->setLayout(false);
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new notificationsForm();
	$this->setLayout(false);
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new notificationsForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($notifications = Doctrine_Core::getTable('notifications')->find(array($request->getParameter('id'))), sprintf('Object notifications does not exist (%s).', $request->getParameter('id')));
    $this->form = new notificationsForm($notifications);
	$this->setLayout(false);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($notifications = Doctrine_Core::getTable('notifications')->find(array($request->getParameter('id'))), sprintf('Object notifications does not exist (%s).', $request->getParameter('id')));
    $this->form = new notificationsForm($notifications);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {

    $this->forward404Unless($notifications = Doctrine_Core::getTable('notifications')->find(array($request->getParameter('id'))), sprintf('Object notifications does not exist (%s).', $request->getParameter('id')));
    $notifications->delete();

    $this->redirect('/backend.php/settings/workflow?load=notifications');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notifications = $form->save();
	  
	  $notifications->setAutosend($request->getPostParameter("autosend"));
	  $notifications->setFormId($request->getPostParameter("application_type"));
	  $notifications->save();
	  
	  $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
		mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
	  
	  //Set Subject
		$sql = "SELECT * FROM ext_translations WHERE field_id = '".$notifications->getId()."' AND field_name = 'notification_title' AND table_class = 'notifications' AND locale = '".$_SESSION['locale']."'";
		$rows = mysql_query($sql);
		if($row = mysql_fetch_assoc($rows))
		{
			$sql = "UPDATE ext_translations SET trl_content = '".mysql_real_escape_string($request->getPostParameter('notifications[title]'))."' WHERE locale = '".$_SESSION['locale']."' AND table_class='notifications' AND field_name='notification_title' AND field_id = ".$notifications->getId();
			mysql_query($sql);
		}
		else
		{
			$sql = "INSERT INTO ext_translations(trl_content, locale, table_class, field_name, field_id) VALUES('".mysql_real_escape_string($request->getPostParameter('notifications[title]'))."', '".$_SESSION['locale']."', 'notifications', 'notification_title', '".$notifications->getId()."')";
			mysql_query($sql);
		}
		
		//Set Content
		$sql = "SELECT * FROM ext_translations WHERE field_id = '".$notifications->getId()."' AND field_name = 'notification_content' AND table_class = 'notifications' AND locale = '".$_SESSION['locale']."'";
		$rows = mysql_query($sql);
		if($row = mysql_fetch_assoc($rows))
		{
			$sql = "UPDATE ext_translations SET trl_content = '".mysql_real_escape_string($request->getPostParameter('notifications[content]'))."' WHERE locale = '".$_SESSION['locale']."' AND table_class='notifications' AND field_name='notification_content' AND field_id = ".$notifications->getId();
			mysql_query($sql);
		}
		else
		{
			$sql = "INSERT INTO ext_translations(trl_content, locale, table_class, field_name, field_id) VALUES('".mysql_real_escape_string($request->getPostParameter('notifications[content]'))."', '".$_SESSION['locale']."', 'notifications', 'notification_content', '".$notifications->getId()."')";
			mysql_query($sql);
		}
		
		//Set Sms
		$sql = "SELECT * FROM ext_translations WHERE field_id = '".$notifications->getId()."' AND field_name = 'notification_sms' AND table_class = 'notifications' AND locale = '".$_SESSION['locale']."'";
		$rows = mysql_query($sql);
		if($row = mysql_fetch_assoc($rows))
		{
			$sql = "UPDATE ext_translations SET trl_content = '".mysql_real_escape_string($request->getPostParameter('notifications[sms]'))."' WHERE locale = '".$_SESSION['locale']."' AND table_class='notifications' AND field_name='notification_sms' AND field_id = ".$notifications->getId();
			mysql_query($sql);
		}
		else
		{
			$sql = "INSERT INTO ext_translations(trl_content, locale, table_class, field_name, field_id) VALUES('".mysql_real_escape_string($request->getPostParameter('notifications[sms]'))."', '".$_SESSION['locale']."', 'notifications', 'notification_sms', '".$notifications->getId()."')";
			mysql_query($sql);
		}

      $this->redirect('/backend.php/settings/workflow?load=notifications');
    }
  }
}
