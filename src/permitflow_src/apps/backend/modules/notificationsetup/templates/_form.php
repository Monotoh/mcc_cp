<?php
	use_helper("I18N");
	$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
	mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
	$locale = '';
?>


<div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
  <strong><?php echo __('Well done'); ?>!</strong> <?php echo __('You successfully updated this record'); ?></a>.
</div>


<form id="notificationform" class="form-bordered" action="<?php echo url_for('/backend.php/notificationsetup/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>

<div class="panel panel-dark">
<div class="panel-heading">
  <h3 class="panel-title"><?php echo ($form->getObject()->isNew()?__('New Notification'):__('Edit Notification')); ?><?php
 if($_SESSION['locale']){ 
    $sql = "SELECT * FROM ext_locales WHERE locale_identifier = '".$sf_user->getCulture()."'";
    $rows = mysql_query($sql);
    $locale = mysql_fetch_assoc($rows);
    echo " </h3><p>(".$locale['local_title']." Translation)</p> ";
 } 
?>
 </div>
<div class="panel-body panel-body-nopadding">


  <?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
      <?php echo $form->renderGlobalErrors() ?>
	    <?php if(isset($form['_csrf_token'])): ?>
            <?php echo $form['_csrf_token']->render(); ?>
            <?php endif; ?>
    <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Stage'); ?></i></label>
         <div class="col-sm-8">
          <?php echo $form['submenu_id']->renderError() ?>
          <select class="form-control" name="notifications[submenu_id]" id="notifications_submenu_id">
			<?php
				 
				 $q = Doctrine_Query::create()
							  ->from('Menus a')
							  ->orderBy('a.order_no ASC');
							$stagegroups = $q->execute();
							foreach($stagegroups as $stagegroup)
							{
								echo "<optgroup label='".$stagegroup->getTitle()."'>";
								$q = Doctrine_Query::create()
								  ->from('SubMenus a')
								  ->where('a.menu_id = ?', $stagegroup->getId())
								  ->orderBy('a.order_no ASC');
								$stages = $q->execute();
								
								foreach($stages as $stage)
								{
									$selected = "";
									
									if(!$form->getObject()->isNew()){
									  if($form->getObject()->getSubmenuId() == $stage->getId()){
										$selected = "selected";  
									  } 
									}
									
									echo "<option value='".$stage->getId()."' ".$selected.">&nbsp;&nbsp;&nbsp;&nbsp;".$stage->getTitle()."</option>";
								}
								echo "</optgroup>";
							}
							
							
							echo "<optgroup label='Others'>";
							
							$q = Doctrine_Query::create()
							  ->from('SubMenus a')
							  ->where('a.menu_id = ?', '0')
							  ->orderBy('a.order_no ASC');
							$stages = $q->execute();
							
							foreach($stages as $stage)
							{
								$selected = "";
								
								if(!$form->getObject()->isNew()){
								  if($form->getObject()->getSubmenuId() == $stage->getId()){
									$selected = "selected";  
								  } 
								}
								
								echo "<option value='".$stage->getId()."' ".$selected.">".$stage->getTitle()."</option>";
							}
							
							echo "</optgroup>";
				?>
		  </select>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Application Form'); ?></i></label>
         <div class="col-sm-8">
		   <?php
			$q = Doctrine_Query::create()
			   ->from('ApForms a')
			   ->where('a.form_id <> 6 AND a.form_id <> 7 AND a.form_id <> 15 AND a.form_id <> 16 AND a.form_id <> 17 AND a.form_id <> 18 AND a.form_id <> 19');
			$forms = $q->execute();
		   ?>
		   <select class="form-control" name='application_type' id='application_type' onChange="<?php 
			  foreach($forms as $formd)
				{
					echo "document.getElementById('form_".$formd->getFormId()."').style.display = 'none';";
				}
			?> if(this.value != '0'){ document.getElementById('form_' + this.value).style.display = 'block'; }">
		   <option value='0' disabled>Choose an application</option>
         <?php
				//Get list of all available applications categorized by groups
				$q = Doctrine_Query::create()
				  ->from('FormGroups a');
				$groups = $q->execute();
				
				if(sizeof($groups) > 0)
				{
					foreach($groups as $group)
					{
						echo "<optgroup label='".$group->getGroupName()."'>";
						
						$q = Doctrine_Query::create()
						  ->from('ApForms a')
						  ->leftJoin('a.ApFormGroups b')
						  ->where('a.form_id = b.form_id')
						  ->andWhere('b.group_id = ?', $group->getGroupId());
						$forms = $q->execute();
						
						$count = 0;
						
						foreach($forms as $apform)
						{
							$selected = "";
							
							if($application_form != "" && $application_form == $apform->getFormId())
							{
								$selected = "selected";
								$_GET['form'] = $application_form;
							}
							
							
							echo "<option value='".$apform->getFormId()."' ".$selected.">".$apform->getFormDescription()."</option>";
							
							$count++;
						}
					echo "</optgroup>";
					}
				}
				else
				{
						$q = Doctrine_Query::create()
						  ->from('ApForms a')
						  ->where('a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ?',array('6','7','15','16','17'))
						  ->andWhere('a.form_active = 1')
						  ->orderBy('a.form_id ASC');
						$forms = $q->execute();
						
						$count = 0;
						
						echo "<optgroup label='Application Forms'>";
						
						foreach($forms as $apform)
						{
							
							$selected = "";
							
							if($application_form != "" && $application_form == $apform->getFormId())
							{
								$selected = "selected";
								$_GET['form'] = $application_form;
							}
							
							if($selectedform != "" && $selectedform == $apform->getFormId())
							{
								$selected = "selected";
								$_GET['form'] = $selectedform;
							}
							
							
							
							echo "<option value='".$apform->getFormId()."' ".$selected.">".$apform->getFormName()."</option>";
							
							$count++;
						}
						
						echo "</optgroup>";
				}
				?>
		  </select>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Automatically Send?'); ?></i></label>
         <div class="col-sm-8">
          <select name='autosend' id='autosend' class="form-control">
			  <option value='1' <?php if(!$form->getObject()->isNew()){ if($form->getObject()->getAutosend() == "1"){ echo "selected";  } } ?>>Yes</option>
			  <option value='2' <?php if(!$form->getObject()->isNew()){ if($form->getObject()->getAutosend() == "2"){ echo "selected";  } } ?>>No</option>
		  </select>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Subject'); ?></i></label>
         <div class="col-sm-8">
          <?php echo $form['title']->renderError() ?>
          <?php
				if($locale)
				{
				?>
                <input type="text" name="notifications[title]" id="notifications_title" class="form-control" style="width:100%;" value="<?php 
					$sql = "SELECT * FROM ext_translations WHERE field_id = '".$form->getObject()->getId()."' AND field_name = 'title' AND table_class = 'notifications' AND locale = '".$sf_user->getCulture()."'";
					$rows = mysql_query($sql);
					if($row = mysql_fetch_assoc($rows))
					{
						echo $row['trl_content'];
					}
					else
					{
						echo $form->getObject()->getTitle();
					}
				?>">
                <ul style="font-size: 12px; padding-left: 20px; padding-top: 10px; list-style-type:disc;">
                <?php
					$sql = "SELECT * FROM ext_translations WHERE field_id = '".$form->getObject()->getId()."' AND field_name = 'title' AND table_class = 'notifications' AND locale <> '".$sf_user->getCulture()."'";
					$rows = mysql_query($sql);
					while($row = mysql_fetch_assoc($rows))
					{
						$sql = "SELECT * FROM ext_locales WHERE locale_identifier = '".$row['locale']."'";
						$rows = mysql_query($sql);
						$locale = mysql_fetch_assoc($rows);
						echo "<li><b style='font-weight: 700;'><u>".$locale['local_title']." Translation</u></h3> ".$row['trl_content']."</b></li>"; 
					}
					
					$sql = "SELECT * FROM ext_locales WHERE is_default = '1'";
					$rows = mysql_query($sql);
					$locale = mysql_fetch_assoc($rows);
					
					echo "<li><b style='font-weight: 700;'><u>".$locale['local_title']." Translation</u></h3> ".$form->getObject()->getTitle()."</b></li>";
				 ?>
                 </ul>
                 <?php
				}
				else
				{
				?>
          <?php echo $form['title'] ?>
         		 <?php
				}
				?>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Mail'); ?></i></label>
         <div class="col-sm-8">
          <?php echo $form['content']->renderError() ?>
          <?php
				if($locale)
				{
				?>
                <textarea name="notifications[content]" id="top_article_txt" style="width:100%;" ><?php 
					$sql = "SELECT * FROM ext_translations WHERE field_id = '".$form->getObject()->getId()."' AND field_name = content' AND table_class = 'notifications' AND locale = '".$sf_user->getCulture()."'";
					$rows = mysql_query($sql);
					if($row = mysql_fetch_assoc($rows))
					{
						echo $row['trl_content'];
					}
					else
					{
						echo $form->getObject()->getContent();
					}
				?></textarea>
                <ul style="font-size: 12px; padding-left: 20px; padding-top: 10px; list-style-type:disc;">
                <?php
					$sql = "SELECT * FROM ext_translations WHERE field_id = '".$form->getObject()->getId()."' AND field_name = 'content' AND table_class = 'notifications' AND locale <> '".$sf_user->getCulture()."'";
					$rows = mysql_query($sql);
					while($row = mysql_fetch_assoc($rows))
					{
						$sql = "SELECT * FROM ext_locales WHERE locale_identifier = '".$row['locale']."'";
						$rows = mysql_query($sql);
						$locale = mysql_fetch_assoc($rows);
						echo "<li><b style='font-weight: 700;'><u>".$locale['local_title']." Translation</u></h3> ".$row['trl_content']."</b></li>"; 
					}
					
					$sql = "SELECT * FROM ext_locales WHERE is_default = '1'";
					$rows = mysql_query($sql);
					$locale = mysql_fetch_assoc($rows);
					
					echo "<li><b style='font-weight: 700;'><u>".$locale['local_title']." Translation</u></h3> ".$form->getObject()->getContent()."</b></li>";
				 ?>
                 </ul>
                 <?php
				}
				else
				{
				?>
          <?php echo $form['content'] ?>
          		<?php
				}
				?>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('SMS'); ?></i></label>
         <div class="col-sm-8">
          <?php echo $form['sms']->renderError() ?>
          <?php
				if($locale)
				{
				?>
                <textarea name="notifications[sms]" id="notifications_sms" class="form-control" ><?php 
					$sql = "SELECT * FROM ext_translations WHERE field_id = '".$form->getObject()->getId()."' AND field_name = 'sms' AND table_class = 'notifications' AND locale = '".$sf_user->getCulture()."'";
					$rows = mysql_query($sql);
					if($row = mysql_fetch_assoc($rows))
					{
						echo $row['trl_content'];
					}
					else
					{
						echo $form->getObject()->getSms();
					}
				?></textarea>
                <ul style="font-size: 12px; padding-left: 20px; padding-top: 10px; list-style-type:disc;">
                <?php
					$sql = "SELECT * FROM ext_translations WHERE field_id = '".$form->getObject()->getId()."' AND field_name = 'sms' AND table_class = 'notifications' AND locale <> '".$sf_user->getCulture()."'";
					$rows = mysql_query($sql);
					while($row = mysql_fetch_assoc($rows))
					{
						$sql = "SELECT * FROM ext_locales WHERE locale_identifier = '".$row['locale']."'";
						$rows = mysql_query($sql);
						$locale = mysql_fetch_assoc($rows);
						echo "<li><b style='font-weight: 700;'><u>".$locale['local_title']." Translation</u></h3> ".$row['trl_content']."</b></li>"; 
					}
					
					$sql = "SELECT * FROM ext_locales WHERE is_default = '1'";
					$rows = mysql_query($sql);
					$locale = mysql_fetch_assoc($rows);
					
					echo "<li><b style='font-weight: 700;'><u>".$locale['local_title']." Translation</u></h3> ".$form->getObject()->getSms()."</b></li>";
				 ?>
                 </ul>
                 <?php
				}
				else
				{
				?>
          <?php echo $form['sms'] ?>
          		<?php
				}
				?>
        </div>
      </div>
      
      
	  <div class="form-group">
      
  
<?php
//Get User Information (anything starting with sf_ )
		   //sf_email, sf_fullname, sf_username, ... other fields in the dynamic user profile form e.g sf_element_1
?>

<table class="table dt-on-steroids mb0">
<thead><tr><th width="50%"><?php echo __('User Details'); ?></th><th><?php echo __('Tag'); ?></th></tr></thead>
<tbody>
<tr>
<td><?php echo __('Username'); ?></td><td>(sf_username)</td>
</tr>
<tr>
<td><?php echo __('Email'); ?></td><td>(sf_email)</td>
</tr>
<tr>
<td><?php echo __('Full Name'); ?></td><td>(sf_fullname)</td>
</tr>
<?php
        $q = Doctrine_Query::create()
		   ->from('apFormElements a')
		   ->where('a.form_id = ?', '15');
		   
		$elements = $q->execute();
		
		foreach($elements as $element)
					{
						$childs = $element->getElementTotalChild();
						if($childs == 0)
						{
						   echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."}</td></tr>";
						}
						else
						{
							if($element->getElementType() == "select")
							{
								echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."}</td></tr>";
							}
							else
							{
								for($x = 0; $x < ($childs + 1); $x++)
								{
									echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."_".($x+1)."}</td></tr>";
								}
							}
						}
					}
			?>
</tbody>
</table>
<?php 
foreach($forms as $formd)
{
?>
<div id='form_<?php echo $formd->getFormId(); ?>' name='form_<?php echo $formd->getFormId(); ?>' style='display: none;'>
<table class="table dt-on-steroids mb0">
<thead><tr><th width="50%"><?php echo __('Application Details'); ?></th><th>Tag</th></tr></thead>
<tbody>
<tr>
<td><?php echo __('Plan Registration Number'); ?></td><td>{ap_application_id}</td>
</tr>
<tr>
<td><?php echo __('Created At'); ?></td> <td>{fm_created_at}</td>
</tr>
<tr>
<td><?php echo __('Approved At'); ?></td> <td>{fm_updated_at}</td>
</tr>
<?php

        $q = Doctrine_Query::create()
		   ->from('apFormElements a')
		   ->where('a.form_id = ?', $formd->getFormId());
		   
		$elements = $q->execute();
		
		foreach($elements as $element)
					{
						$childs = $element->getElementTotalChild();
						if($childs == 0)
						{
						   echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."}</td></tr>";
						}
						else
						{
							if($element->getElementType() == "select")
							{
								echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."}</td></tr>";
							}
							else
							{
								for($x = 0; $x < ($childs + 1); $x++)
								{
									echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."_".($x+1)."}</td></tr>";
								}
							}
						}
					}
			?>
</tbody>
</table>
</div>
<?php
}
?>


<table class="table dt-on-steroids mb0">
<thead><tr><th width="50%"><?php echo __('Conditions Of Approval'); ?></th><th>Tag</th></tr></thead>
<tbody>
<tr><td><?php echo __('Conditions Of Approval'); ?></td><td>{ca_conditions}</td></tr>
</tbody>
</table>


<table class="table dt-on-steroids mb0">
<thead><tr><th width="50%"><?php echo __('Agenda Cleaning'); ?></th><th><?php echo __('Tag'); ?></th></tr></thead>
<tbody>
<tr><td><?php echo __('Reasons for Rejection'); ?></td><td>{rj_reasons}</td></tr>
</tbody>
</table>

<table class="table dt-on-steroids mb0">
<thead><tr><th width="50%"><?php echo __('Invoice Details'); ?></th><th><?php echo __('Tag'); ?></th></tr></thead>
<tbody>
<tr><td><?php echo __('Total'); ?></td><td>{in_total}</td></tr>
</tbody>
</table>

<table class="table dt-on-steroids mb0">
<thead><tr><th width="50%"><?php echo __('Other Details'); ?></th><th><?php echo __('Tag'); ?></th></tr></thead>
<tbody>
<tr><td><?php echo __('Current Date'); ?></td><td>{current_date}</td></tr>
<tr><td><?php echo __('Comments'); ?></td><td>{ap_comments}</td></tr>
</tbody>
</table>
 </div>
	  
	   </div><!--panel-body-->
        
        <div class="panel-footer">
		<button class="btn btn-danger mr10"><?php echo __('Reset'); ?></button><button type="submit" class="btn btn-primary" name="submitbuttonname" id="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button>
		</div>



</div><!--panel-body-->
</div>
</form>

<script language="javascript">
 jQuery(document).ready(function(){
	$("#submitbuttonname").click(function() {
		 $.ajax({
			url: '<?php echo url_for('/backend.php/notificationsetup/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>',
			cache: false,
			type: 'POST',
			data : $('#notificationform').serialize(),
			success: function(json) {
				$('#alertdiv').attr("style", "display: block;");
				$("html, body").animate({ scrollTop: 0 }, "slow");
			}
		});
		return false;
	 });
	});
</script>


<script src="<?php echo public_path(); ?>assets_unified/js/ckeditor/ckeditor.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/ckeditor/adapters/jquery.js"></script>


<script>
jQuery(document).ready(function(){
  
  // CKEditor
  jQuery('#top_article_txt').ckeditor();

});
</script>
