<?php
use_helper("I18N");

$audit = new Audit();
$audit->saveAudit("", "Accessed notification settings");

if($sf_user->mfHasCredential("managenotifications"))
{
  $_SESSION['current_module'] = "notifications";
  $_SESSION['current_action'] = "index";
  $_SESSION['current_id'] = "";
?>
<div class="panel panel-dark">
<div class="panel-heading">
			<h3 class="panel-title"><?php echo __('Notifications'); ?></h3>
				<div class="pull-right">
            <a class="btn btn-primary-alt settings-margin42" id="newnotification" href="#"><?php echo __('New Notification'); ?></a>
            <script language="javascript">
            jQuery(document).ready(function(){
              $( "#newnotification" ).click(function() {
                  $("#contentload").load("<?php echo public_path(); ?>backend.php/notificationsetup/new");
              });
            });
            </script>
</div>
</div>


<div class="panel panel-body panel-body-nopadding ">

<div class="table-responsive">
<table class="table dt-on-steroids mb0" id="table3">
    <thead>
   <tr>
      <th class="no-sort" style="width: 10px;"><input type='checkbox' name='batchall' onclick="boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.name == 'batch') { if(this.checked == true){ box.checked = false; }else{ box.checked = true; } } } "></th>
      <th width="60">#</th>
      <th><?php echo __('Stage'); ?></th>
      <th class="no-sort"><?php echo __('Subject'); ?></th>
      <th class="no-sort" width="7%"><?php echo __('Actions'); ?></th>
    </tr>
  </thead>
  <tbody id="tblBdy">
    <?php foreach ($notificationss as $notifications): ?>
    <tr id="row_<?php echo $notifications->getId() ?>">
	  <td><input type='checkbox' name='batch' id='batch_<?php echo $notifications->getId() ?>' value='<?php echo $notifications->getId() ?>'></td>
      <td><?php echo $notifications->getId() ?></td>
      <td><?php
	  $submenu = Doctrine_Core::getTable("SubMenus")->find(array($notifications->getSubmenuId()));
	  if($submenu)
	  {
		echo $submenu->getTitle();
	  }
	  ?></td>
      <td><?php echo $notifications->getTitle() ?></td>
      <td>
		<a id="editnotification<?php echo $notifications->getId(); ?>" href="#" title="<?php echo __('Edit'); ?>"><span class="badge badge-primary"><i class="fa fa-pencil"></i></span></a>
		<a id="deletenotification<?php echo $notifications->getId(); ?>" href="#" title="<?php echo __('Delete'); ?>"><span class="badge badge-primary"><i class="fa fa-trash-o"></i></span></a>
	  <script language="javascript">
      jQuery(document).ready(function(){
        $( "#editnotification<?php echo $notifications->getId(); ?>" ).click(function() {
            $("#contentload").load("<?php echo public_path(); ?>backend.php/notificationsetup/edit/id/<?php echo $notifications->getId(); ?>");
        });
        $( "#deletenotification<?php echo $notifications->getId(); ?>" ).click(function() {
            if(confirm('Are you sure you want to delete this notification?')){
              $("#contentload").load("<?php echo public_path(); ?>backend.php/notificationsetup/delete/id/<?php echo $notifications->getId(); ?>");
            }
            else
            {
               return false;
            }
        });
      });
    </script>
  </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
	<tfoot>
   <tr><td colspan='7' style='text-align: left;'>
   <select id='batch_action' name='batch_action' onChange="if(this.value != ''){if(confirm('Are you sure?')){ batch('notificationsetup', this.options[this.selectedIndex].text, this.value); document.getElementById('default').selected='selected'; }}">
   <option id='default' value=''><?php echo __('Choose an action'); ?>..</option>
   <option value='delete'><?php echo __('Set As Deleted'); ?></option>
   </select>
   </td></tr>
   </tfoot>
</table>
</div>
</div><!--panel-body-->
</div><!--panel-dark-->

<script>
  jQuery('#table3').dataTable({
      "sPaginationType": "full_numbers",

      // Using aoColumnDefs
      "aoColumnDefs": [
      	{ "bSortable": false, "aTargets": [ 'no-sort' ] }
    	]
    });

</script>
<?php
}
else
{
  include_partial("accessdenied");
}
?>
