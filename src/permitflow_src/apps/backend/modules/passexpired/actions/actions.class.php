<?php

/**
 * about actions.
 *
 * @package    permit
 * @subpackage about
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class passexpiredActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $this->userid = $request->getParameter("userid");
    $this->setLayout('layoutexpired');
  }
  public function executeChanged(sfWebRequest $request){
      $this->getUser()->setFlash("pass_change","Success! Password updated successfuly.!") ;
      $this->setLayout('layoutexpired');
  }
  public function executeProcess(sfWebRequest $request){
      $otbhelper = new OTBHelper();
      //print "Done" ;
      $current_password = $request->getParameter("current_password") ;
      $new_password = $request->getParameter("new_password") ;
      $confirm_new_password = $request->getParameter("confirm_new_password") ;
	  $userid = $request->getParameter("userid") ? $request->getParameter("userid") : $this->getUser()->getAttribute('userid');//Allow admin to change user passwords but must be there to put in old password
      //just simple way of validating values sent - Not good but works
      if($current_password == ""){
          $this->getUser()->setFlash("current_pass_miss","Please Enter Current Password") ;
           $this->getUser()->setFlash("missing_value","Error") ;
          $this->redirect("/backend.php/passexpired/index/userid/".$userid);
      }else if($new_password == ""){
          $this->getUser()->setFlash("new_pass_miss","Please Enter New Password") ;
          $this->getUser()->setFlash("missing_value","Error") ;
          $this->redirect("/backend.php/passexpired/index/userid/".$userid);
      }elseif ($confirm_new_password == "") {
              $this->getUser()->setFlash("confirm_pass_miss","Please Confirm New Password") ;
              $this->getUser()->setFlash("missing_value","Error") ;
          $this->redirect("/backend.php/passexpired/index/userid/".$userid);
        }                           
      else{
          //check if new and confirm password match 
          if($new_password != $confirm_new_password){
               //password do not match
              $this->getUser()->setFlash("pass_match_error","Passwords do not match. Please make sure new and confirm values are same");
              $this->redirect("/backend.php/passexpired/index/userid/".$userid);
          }else {
              
              //check current password with values set for current logged user.
              $validate_current = $otbhelper->checkPassOldMethod($userid, $current_password) ;
              if($validate_current){
                  try{
                  error_log("Current password OK") ;
                  $q = Doctrine_Query::create()
                          ->update('CfUser c')
                          ->set('c.strpassword','?',password_hash($new_password, PASSWORD_BCRYPT))
                          ->set('c.pass_change','?', 1)
                          ->where('c.nid = ?',$userid);
                  $res = $q->execute();
                  
                  $this->redirect("/backend.php/passexpired/changed");
                  }catch(Exception $ex){
                      error_log("Error changing password >>".$ex->getMessage());
                      $this->getUser()->setFlash("server_error","Sorry cannot change password at the moment. Please try later..") ;
                      $this->redirect("/backend.php/passexpired/index/userid/".$userid);
                      }
                  
                  //set the new password
              }else {
                    $this->getUser()->setFlash("current_pass_bad","Current password is wrong! please type in your current correct password") ;
                    $this->redirect("/backend.php/passexpired/index/userid/".$userid);
              }
                      
            
          }
             print "Good boy" ;  exit();
         
      }
     
  }

}
