<?php use_helper('I18N', 'Date') ?>

<div class="panel panel-dark">

    <div class="panel-heading">
        <h2 class="panel-title"><?php echo __('Success: Password Updated!'); ?></h2>
    </div>
    <div class="panel panel-body">
        
        <div class="row">
            <div class="col-lg-12">
                    <?php if($sf_user->hasFlash('pass_change')) :?>
                            <div class="alert alert-warning">
                                <span style="color: red;">
                                   <?php echo $sf_user->getFlash('pass_change') ; ?>  
                                </span>  
                            </div>
                <a href="/backend.php/dashboard/index" class="btn btn-success"> <?php echo __('Go to Dashboard') ?> </a>
                    <?php endif; ?>
            </div>
        </div>

    </div>
</div>
