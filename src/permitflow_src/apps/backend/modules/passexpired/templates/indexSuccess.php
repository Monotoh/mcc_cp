<?php use_helper('I18N', 'Date') ?>
<?php
$audit = new Audit();
$audit->saveAudit("", "Accessed Password expired");
?>
<div class="panel panel-dark">

    <div class="panel-heading">
        <h2 class="panel-title"><?php echo __('Warning: Password Expired!'); ?></h2>
    </div>
    <div class="panel panel-body">
        
        <div class="row">
            <div class="col-lg-12">
                <form method="post" action="/backend.php/passexpired/process">
				    <input type="hidden" name="userid" id="userid" value="<?php echo $userid; ?>"/><br/>
                    <?php if($sf_user->hasFlash('missing_value')) :?>
                            <div class="alert alert-warning">
                                <span style="color: red;">
                                    <?php echo __("Please Correct below errors !") ?>
                                </span>  
                            </div>
                    <?php endif; ?>
                    <?php if($sf_user->hasFlash('pass_match_error')) :?>
                            <div class="alert alert-warning">
                                <span style="color: red;">
                                   <?php echo $sf_user->getFlash('pass_match_error') ; ?>  
                                </span>  
                            </div>
                    <?php endif; ?>
                    <?php if($sf_user->hasFlash('current_pass_bad')) :?>
                            <div class="alert alert-warning">
                                <span style="color: red;">
                                   <?php echo $sf_user->getFlash('current_pass_bad') ; ?>  
                                </span>  
                            </div>
                    <?php endif; ?>
                     <?php if($sf_user->hasFlash('server_error')) :?>
                            <div class="alert alert-warning">
                                <span style="color: red;">
                                   <?php echo $sf_user->getFlash('server_error') ; ?>  
                                </span>  
                            </div>
                    <?php endif; ?>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><font color="red">* </font> <i class="bold-label"><?php echo __('Current Password'); ?></i></label>
                        <div class="col-sm-8">
                            <input type="password" name="current_password" id="current_password" /> <br/>
                             <?php if($sf_user->hasFlash('current_pass_miss')) :?>
                               <span style="color: red;">
                                <?php echo $sf_user->getFlash('current_pass_miss') ; ?>  
                               </span>
                              <?php endif; ?>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><font color="red">* </font><i class="bold-label"><?php echo __('New Password'); ?></i></label>
                        <div class="col-sm-8">
                            <input type="password" name="new_password" id="new_password" /><br/>
                            <?php if($sf_user->hasFlash('new_pass_miss')) :?>
                             <span style="color: red;">
                                <?php echo $sf_user->getFlash('new_pass_miss') ; ?>  
                              </span>
                              <?php endif; ?>
                            <span>
                                <?php echo __("Minimum of 6 characters include numbers and symbols") ?>
                            </span>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label"><font color="red">* </font><i class="bold-label"><?php echo __('Confirm New Password'); ?></i></label>
                        <div class="col-sm-8">
                            <input type="password" name="confirm_new_password" id="confirm_new_password" />
                            <br/>
                            <?php if($sf_user->hasFlash('confirm_pass_miss')) :?>
                           <span style="color: red;">
                                 <?php echo $sf_user->getFlash('confirm_pass_miss') ; ?>  
                            </span>
                               
                             
                              <?php endif; ?>
                        </div>
                    </div>
                    
                    <input type="submit" value="Change Password" id="update_password" name="update_password" class="btn btn-success" />


                </form>
            </div>
        </div>

    </div>
</div>
