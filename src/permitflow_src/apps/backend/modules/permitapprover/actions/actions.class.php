<?php

/**
 * permitapprover actions.
 *
 * @package    symfony
 * @subpackage permitapprover
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class permitapproverActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $this->permit_approver_infos = Doctrine_Core::getTable('PermitApproverInfo')
      ->createQuery('a')
      ->execute();
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new PermitApproverInfoForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new PermitApproverInfoForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($permit_approver_info = Doctrine_Core::getTable('PermitApproverInfo')->find(array($request->getParameter('id'))), sprintf('Object permit_approver_info does not exist (%s).', $request->getParameter('id')));
    $this->form = new PermitApproverInfoForm($permit_approver_info);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($permit_approver_info = Doctrine_Core::getTable('PermitApproverInfo')->find(array($request->getParameter('id'))), sprintf('Object permit_approver_info does not exist (%s).', $request->getParameter('id')));
    $this->form = new PermitApproverInfoForm($permit_approver_info);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($permit_approver_info = Doctrine_Core::getTable('PermitApproverInfo')->find(array($request->getParameter('id'))), sprintf('Object permit_approver_info does not exist (%s).', $request->getParameter('id')));
    $permit_approver_info->delete();

    $this->redirect('backend.php/permitapprover/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $permit_approver_info = $form->save();

      $this->redirect('backend.php/permitapprover/index');
    }
  }
}
