<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php
use_helper("I18N");
?>
<div class="contentpanel">
<form class="form-bordered" action="<?php echo url_for('/backend.php/permitapprover/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId()."&filter=".$filter : '?filter='.$filter )) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>  autocomplete="off" data-ajax="false">

<div class="panel panel-dark">
<div class="panel-heading">
<h3 class="panel-title"><?php echo __('Permit Approval Details'); ?></h3>
</div>


<div class="panel-body panel-body-nopadding">

   <?php echo $form['_csrf_token']->render(); ?> 
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
      <?php echo $form->renderGlobalErrors() ?>
      <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Title'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['title']->renderError() ?>
          <?php echo $form['title']->render() ?>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Approval Signature Link'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['approval_signature']->renderError() ?>
          <?php echo $form['approval_signature']->render() ?>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Approval Text'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['approval_text']->renderError() ?>
          <?php echo $form['approval_text']->render(array('class' => 'ckeditor')) ?>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Permit'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['permit_list']->renderError() ?>
          <?php echo $form['permit_list'] ?>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Valid From'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['valid_from']->renderError() ?>
          <?php echo $form['valid_from']->render(array('value'=>date('m/d/y'))) ?>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Valid To'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['valid_to']->renderError() ?>
          <?php echo $form['valid_to']->render(array('value'=>date('m/d/y'))) ?>
        </div>
      </div>
	   <div class="panel-footer">
               <a href="<?php echo url_for('/backend.php/permitapprover/index') ?>" <button class="btn btn-danger mr10"><?php echo __('Back to List'); ?></button> </a>
                        <button type="submit" class="btn btn-primary" value="submitbuttonvalue"><?php echo __('Save'); ?></button>
	  </div>
</form>
</div>
    <script type="text/javascript"> 
    jQuery('#permit_approver_info_valid_to').datepicker();
    jQuery('#permit_approver_info_valid_from').datepicker();
    </script>
 