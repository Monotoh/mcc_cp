<?php
use_helper("I18N");
if($sf_user->mfHasCredential("managepermits"))
{
?>
<?php use_helper('I18N', 'Date') ?>

<div class="panel panel-dark">
<div class="panel-heading">
			<h3 class="panel-title"><?php echo __('Permits Approval Details'); ?></h3>
				<div class="pull-right">
            <a class="btn btn-primary-alt settings-margin42" id="newfee" href="<?php echo public_path(); ?>backend.php/permitapprover/new/"><?php echo __('New Approval Details'); ?></a>

           
</div>
</div>
		
 
<div class="panel panel-body panel-body-nopadding ">
    <?php if($sf_user->hasFlash('Success')): ?>
    <div class="alert alert-success"> 
        <button type="button" class="close" data-dismiss="alert"> * </button>
        <b> <?php echo $sf_user->getFlash('Success') ?> </b>
    </div>
    <?php endif; ?>
<div class="table-responsive">
<table id="table_permits_approver" class="table dt-on-steroids mb0" id="table3">
    <thead>
   <tr>
     <th class="no-sort" width="7%"><?php echo __('Title'); ?></th>
     <th class="no-sort" width="7%"><?php echo __('Approval Signature'); ?></th>
      <th class="no-sort" width="7%"><?php echo __('Approval Text'); ?></th>
    
      <th class="no-sort" width="7%"><?php echo __('Actions'); ?></th>
    </tr>
  </thead>
    <tbody>
    <?php foreach ($permit_approver_infos as $c): ?>
    <tr id="row_<?php echo $c->getId() ?>">
        <td> <?php echo $c->getTitle() ?> </td>
                <td><a href="<?php echo url_for('/backend.php/permitapprover/edit?id='.$c->getId()) ?>"><?php echo $c->getApprovalText() ?></a></td>
                <td><a href="<?php echo url_for('/backend.php/permitapprover/edit?id='.$c->getId()) ?>"><?php echo $c->getApprovalText() ?></a></td>
		
		
    <td>					
	  <a id="editfee<?php echo $c->getId() ?>" href="<?php echo url_for('/backend.php/permitapprover/edit?id='.$c->getId()) ?>" alt="Edit" title="<?php echo __('Edit'); ?>"><span class="badge badge-primary"><i class="fa fa-pencil"></i></span></a>
     
      <a id="deletefee<?php echo $c->getId() ?>" href="<?php echo url_for('/backend.php/permitapprover/delete?id='.$c->getId()) ?>" alt="Delete" title="<?php echo __('Delete'); ?>"><span class="badge badge-primary"><i class="fa fa-trash-o"></i></span></a>
	 

           
   </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
</div>
<a name="end"></a>

</div><!--panel-body-->
</div><!--panel-dark-->

<script>
  jQuery('#table_permits_approver').dataTable({
      "sPaginationType": "full_numbers",
     
      // Using aoColumnDefs
      "aoColumnDefs": [
      	{ "bSortable": false, "aTargets": [ 'no-sort' ] }
    	]
    });
    
</script>
<?php
}
else
{
  include_partial("settings/accessdenied");
}
?>
