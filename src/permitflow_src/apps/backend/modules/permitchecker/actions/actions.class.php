<?php

/**
 * permitchecker actions.
 *
 * @package    permit
 * @subpackage permitchecker
 */
class permitCheckerActions extends sfActions
{
  
  public function executeBatch(sfWebRequest $request)
  {
		if($request->getPostParameter('delete'))
		{
			$item = Doctrine_Core::getTable('PermitCheckerConfig')->find(array($request->getPostParameter('delete')));
			if($item)
			{
				$item->delete();
			}
		}
    }
  public function executeIndex(sfWebRequest $request)
  {
	  $q = Doctrine_Query::create()
       ->from('PermitCheckerConfig a')
       ->andWhere('a.permit_template_id = ?', $request->getParameter('filter'))
	   ->orderBy('a.sequence_no ASC');
     $this->permit_checker_config_details = $q->execute();

	 $this->filter = $request->getParameter("filter");
	$this->setLayout(false);
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new PermitCheckerConfigForm();
   $this->filter = $request->getParameter("filter");
	 $this->form->setDefault('permit_template_id', $this->filter);
	$this->setLayout(false);
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new PermitCheckerConfigForm();
	
	$this->new_record = true;

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($permit_checker_config_details = Doctrine_Core::getTable('PermitCheckerConfig')->find(array($request->getParameter('id'))), sprintf('Object permit_checker_config_details does not exist (%s).', $request->getParameter('id')));
    $this->form = new PermitCheckerConfigForm($permit_checker_config_details);
   $this->filter = $request->getParameter("filter");
	$this->setLayout(false);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($permit_checker_config_details = Doctrine_Core::getTable('PermitCheckerConfig')->find(array($request->getParameter('id'))), sprintf('Object permit_checker_config_details does not exist (%s).', $request->getParameter('id')));
	
	$this->current_object_sequence = $permit_checker_config_details->getSequenceNo();

    $this->form = new PermitCheckerConfigForm($permit_checker_config_details);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {

    $this->forward404Unless($permit_checker_config_details = Doctrine_Core::getTable('PermitCheckerConfig')->find(array($request->getParameter('id'))), sprintf('Object permit_checker_config_details does not exist (%s).', $request->getParameter('id')));

    $audit = new Audit();
    $audit->saveAudit("", "deleted permit checker of id ".$permit_checker_config_details->getId());
	
	$permit_template_id = $permit_checker_config_details->getPermitTemplateId();

    $permit_checker_config_details->delete();

      $this->redirect('/backend.php/permitchecker/index/filter/'.$permit_template_id);
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $permit_checker_config_details = $form->save();

		if($this->new_record)
		{
			$permit_checker_config_details->setSequenceNo($permit_checker_config_details->getId());
			$permit_checker_config_details->save();
		}else{
			$permit_checker_config_details->setSequenceNo($this->current_object_sequence);
			$permit_checker_config_details->save();
		}
      $audit = new Audit();
      $audit->saveAudit("", "<a href=\"/backend.php/permitchecker/edit?id=".$permit_checker_config_details->getId()."&language=en\">updated a permit checker detail</a>");

      $this->redirect('/backend.php/permitchecker/index/');
    }
  }

  public function executeMovedetail(sfWebRequest $request)
  {
    $this->forward404Unless($checker_config = Doctrine_Core::getTable('PermitCheckerConfig')->find(array($request->getParameter('id'))), sprintf('Object permit_checker_config_details does not exist (%s).', $request->getParameter('id')));

	$q = Doctrine_Query::create()
		 ->from("PermitCheckerConfig a")
		 ->where("a.permit_template_id < ?", $checker_config->getPermitTemplateId());

	if($request->getParameter('up')){
		$q->where("a.sequence_no < ?", $checker_config->getSequenceNo())
		  ->orderBy('a.sequence_no DESC');
	}else{
		$q->where("a.sequence_no > ?", $checker_config->getSequenceNo())
		  ->orderBy('a.sequence_no ASC');
	}
    $preceding_checker_config = $q->fetchOne();
    if($preceding_checker_config)
    {
      $current_order = $checker_config->getSequenceNo();
      $previous_order = $preceding_checker_config->getSequenceNo();

      $preceding_checker_config->setSequenceNo(-1); //temporary set page order to prevent conflict
      $preceding_checker_config->save();

      $checker_config->setSequenceNo($previous_order);
      $checker_config->save();

      $preceding_checker_config->setSequenceNo($current_order);
      $preceding_checker_config->save();
    }

	exit();

  }


}
