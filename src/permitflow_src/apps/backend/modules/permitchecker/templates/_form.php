<?php
use_helper("I18N");
?>
<div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
  <strong><?php echo __('Well done'); ?>!</strong> <?php echo __('You successfully updated this record'); ?></a>.
</div>

<form id="permitcheckerform" class="form-bordered" action="<?php echo url_for('/backend.php/permitchecker/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>  autocomplete="off" data-ajax="false">

<div class="panel panel-dark">
<div class="panel-heading">
<h3 class="panel-title"><?php echo ($form->getObject()->isNew()?__('New Detail'):__('Edit Detail')); ?></h3>
</div>


<div class="panel-body panel-body-nopadding">

<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
	<?php endif; ?>
	<?php if(isset($form['_csrf_token'])): ?>
	<?php echo $form['_csrf_token']->render(); ?>
	<?php endif; ?>
	<?php echo $form->renderGlobalErrors() ?>
      <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Permit'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['permit_template_id']->renderError() ?>
          <?php echo $form['permit_template_id'] ?>
        </div>
     </div>
      <!--<div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Reference Object'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['reference_object']->renderError() ?>
          <?php echo $form['reference_object'] ?>
        </div>
     </div>-->
      <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Label to show on permit checker'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['label_to_show']->renderError() ?>
          <?php echo $form['label_to_show'] ?>
        </div>
     </div>
      <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Field Value to show - Place the element reference in calibraces e.g. {fm_element_11}, {fm_application_id} etc.'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['value_to_show']->renderError() ?>
          <?php echo $form['value_to_show'] ?>
        </div>
     </div>
      <!--<div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Order'); ?></i></label>
        <div class="col-sm-8 rogue-input">
          <?php echo $form['sequence_no']->renderError() ?>
          <?php echo $form['sequence_no'] ?>
        </div>
     </div>-->

      <div class="form-group">
		<div class="col-sm-12 alignright">
		  <button type="button" class="btn btn-primary" data-target="#fieldsModal" data-toggle="modal">View available user/form fields</button>
		  <p>Note: use {ap_application_status} to show the application status</p>
		</div>
	  </div>


     </div>
<div class="panel-footer" align="right">
            <a id="backbuttonname" name="backbuttonname" class="btn btn-success"><?php echo __('Back'); ?></a> <button type="submit" class="btn btn-primary" name="submitbuttonname" id="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button>
       </div>
</div>
</form>
<script language="javascript">
 jQuery(document).ready(function(){
	$("#submitbuttonname").click(function() {
		 $.ajax({
			url: '<?php echo url_for('/backend.php/permitchecker/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId().'&filter='.$filter : '?filter='.$filter)) ?>',
			cache: false,
			type: 'POST',
			data : $('#permitcheckerform').serialize(),
			success: function(json) {
				$('#alertdiv').attr("style", "display: block;");
        $("#loadpchecker").load("<?php echo public_path(); ?>backend.php/permitchecker/index/filter/<?php echo $filter; ?>");
			}
		});
		return false;
	 });

	  $( "#backbuttonname" ).click(function() {
			$("#loadpchecker").load("<?php echo public_path(); ?>backend.php/permitchecker/index/filter/<?php echo $filter; ?>");
	  });

	});
</script>