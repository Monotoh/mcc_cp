<?php
use_helper("I18N");

$audit = new Audit();
$audit->saveAudit("", "Accessed Permit Checker settings");
?>
<?php
if($sf_user->mfHasCredential("managepermitchecker"))
{
?>
<div class="panel panel-dark">
<div class="panel-heading">
			<h3 class="panel-title"><?php echo __('Permit Checker Settings'); ?></h3>
				<div class="pull-right">
            <a class="btn btn-primary-alt settings-margin42" id="newpcdetail" href="#end"><?php echo __('New Detail'); ?></a>

            <script language="javascript">
            jQuery(document).ready(function(){
              $( "#newpcdetail" ).click(function() {
                  $("#loadpchecker").load("<?php echo public_path(); ?>backend.php/permitchecker/new/filter/<?php echo $filter; ?>");
              });
            });
            </script>
</div>
</div>


<div class="panel panel-body panel-body-nopadding ">

<div class="table-responsive">
<table class="table dt-on-steroids mb0" id="pcdetailtable">
    <thead>
   <tr>
      <th width="60">#</th>
      <th><?php echo __('Label'); ?></th>
      <th><?php echo __('Value'); ?></th>
      <!--<th><?php echo __('Object'); ?></th>-->
      <th><?php echo __('Order'); ?></th>
      <th width="7%"><?php echo __('Actions'); ?></th>
    </tr>
     </thead>
    <tbody>
 <?php
	$count = 1;
 ?>
 <?php foreach ($permit_checker_config_details as $pcdetail): ?>
    <tr id="row_<?php echo $pcdetail->getId() ?>">
	    <td><?php echo $count++; ?></td>
      <td><?php echo $pcdetail->getLabelToShow() ?></td>
      <td><?php echo $pcdetail->getValueToShow() ?></td>
      <!--<td><?php echo $pcdetail->getReferenceObject() ?></td>-->
      <td>
      <a href="#" id="moveup<?php echo $pcdetail->getId(); ?>"><span class="glyphicon glyphicon-circle-arrow-up"></span></a>
      <a href="#" id="movedown<?php echo $pcdetail->getId(); ?>"><span class="glyphicon glyphicon-circle-arrow-down"></span></a>
      </td>
      <!--<td><?php echo $pcdetail->getSequenceNo() ?></td>-->
      <td>
     	  <a id="editpcdetail<?php echo $pcdetail->getId() ?>" href="#end" title="<?php echo __('Edit'); ?>"><span class="badge badge-primary"><i class="fa fa-pencil"></i></span></a>
      	<a id="deletepcdetail<?php echo $pcdetail->getId() ?>" href="#end" title="<?php echo __('Delete'); ?>"><span class="badge badge-primary"><i class="fa fa-trash-o"></i></span></a>


        <script language="javascript">
        jQuery(document).ready(function(){
          $("#moveup<?php echo $pcdetail->getId() ?>" ).click(function() {
            $.ajax({
                type: 'GET',
                url: '<?php echo public_path() ?>backend.php/permitchecker/movedetail/id/<?php echo $pcdetail->getId(); ?>/filter/<?php echo $filter; ?>/up/true',
                success: function (results)
                {
					$("#loadpchecker").load("<?php echo public_path(); ?>backend.php/permitchecker/index/filter/<?php echo $filter; ?>");
                },
                error: function () {
                    alert("Error Saving Detail");
                }
            });

          });
          $("#movedown<?php echo $pcdetail->getId() ?>" ).click(function() {
            $.ajax({
                type: 'GET',
                url: '<?php echo public_path() ?>backend.php/permitchecker/movedetail/id/<?php echo $pcdetail->getId(); ?>/filter/<?php echo $filter; ?>',
                success: function (results)
                {
					$("#loadpchecker").load("<?php echo public_path(); ?>backend.php/permitchecker/index/filter/<?php echo $filter; ?>");
                },
                error: function () {
                    alert("Error Saving Detail");
                }
            });

          });
          $( "#editpcdetail<?php echo $pcdetail->getId() ?>" ).click(function() {
              $("#loadpchecker").load("<?php echo public_path(); ?>backend.php/permitchecker/edit/id/<?php echo $pcdetail->getId(); ?>/filter/<?php echo $filter; ?>");
          });
          $( "#deletepcdetail<?php echo $pcdetail->getId() ?>" ).click(function() {
              if(confirm('Are you sure you want to delete this group?')){
                $("#loadpchecker").load("<?php echo public_path(); ?>backend.php/permitchecker/delete/id/<?php echo $pcdetail->getId(); ?>/filter/<?php echo $filter; ?>");
              }
              else
              {
                return false;
              }
          });
        });
        </script>


  </td>
    </tr>
    <?php endforeach; ?>
    </tbody>
	<tfoot>
   <tr><td colspan='8' style='text-align: left;'>
   <select id='batch_action' name='batch_action' onChange="if(this.value != ''){if(confirm('Are you sure?')){ batch('permitchecker', this.options[this.selectedIndex].text, this.value); document.getElementById('default').selected='selected'; }}">
   <option id='default' value=''><?php echo __('Choose an action'); ?>..</option>
   <option value='delete'><?php echo __('Set As Deleted'); ?></option>
   </select>
   </td></tr>
   </tfoot>
</table>
</div>
</div>
<script>
  jQuery('#pcdetailtable').dataTable({
      "sPaginationType": "full_numbers",

      // Using aoColumnDefs
      "aoColumnDefs": [
      	{ "bSortable": false, "aTargets": [ 'no-sort' ] }
    	]
    });

</script>
<?php
}
else
{
  include_partial("accessdenied");
}
?>
