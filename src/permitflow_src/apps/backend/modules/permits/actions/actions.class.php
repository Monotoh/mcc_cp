<?php

/**
 * permits actions.
 *
 * @package    permit
 * @subpackage permits
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class permitsActions extends sfActions
{

  public function executeBarcode(sfWebRequest $request)
  {
      $q = Doctrine_Query::create()
         ->from("SavedPermit a")
         ->where("a.id = ?", $request->getParameter("id"));
      $saved_permit = $q->fetchOne();

      if($request->getParameter("size"))
      {
         $_GET["size"] = $request->getParameter("size");
      }

      // Get pararameters that are passed in through $_GET or set to the default value
      $text = $saved_permit->getFormEntry()->getApplicationId().": ".$saved_permit->getDateOfExpiry();
      $size = (isset($_GET["size"])?$_GET["size"]:"20");
      $orientation = (isset($_GET["orientation"])?$_GET["orientation"]:"horizontal");
      $code_type = (isset($_GET["codetype"])?$_GET["codetype"]:"code128");
      $code_string = "";
      // Translate the $text into barcode the correct $code_type
      if ( in_array(strtolower($code_type), array("code128", "code128b")) ) {
        $chksum = 104;
        // Must not change order of array elements as the checksum depends on the array's key to validate final code
        $code_array = array(" "=>"212222","!"=>"222122","\""=>"222221","#"=>"121223","$"=>"121322","%"=>"131222","&"=>"122213","'"=>"122312","("=>"132212",")"=>"221213","*"=>"221312","+"=>"231212",","=>"112232","-"=>"122132","."=>"122231","/"=>"113222","0"=>"123122","1"=>"123221","2"=>"223211","3"=>"221132","4"=>"221231","5"=>"213212","6"=>"223112","7"=>"312131","8"=>"311222","9"=>"321122",":"=>"321221",";"=>"312212","<"=>"322112","="=>"322211",">"=>"212123","?"=>"212321","@"=>"232121","A"=>"111323","B"=>"131123","C"=>"131321","D"=>"112313","E"=>"132113","F"=>"132311","G"=>"211313","H"=>"231113","I"=>"231311","J"=>"112133","K"=>"112331","L"=>"132131","M"=>"113123","N"=>"113321","O"=>"133121","P"=>"313121","Q"=>"211331","R"=>"231131","S"=>"213113","T"=>"213311","U"=>"213131","V"=>"311123","W"=>"311321","X"=>"331121","Y"=>"312113","Z"=>"312311","["=>"332111","\\"=>"314111","]"=>"221411","^"=>"431111","_"=>"111224","\`"=>"111422","a"=>"121124","b"=>"121421","c"=>"141122","d"=>"141221","e"=>"112214","f"=>"112412","g"=>"122114","h"=>"122411","i"=>"142112","j"=>"142211","k"=>"241211","l"=>"221114","m"=>"413111","n"=>"241112","o"=>"134111","p"=>"111242","q"=>"121142","r"=>"121241","s"=>"114212","t"=>"124112","u"=>"124211","v"=>"411212","w"=>"421112","x"=>"421211","y"=>"212141","z"=>"214121","{"=>"412121","|"=>"111143","}"=>"111341","~"=>"131141","DEL"=>"114113","FNC 3"=>"114311","FNC 2"=>"411113","SHIFT"=>"411311","CODE C"=>"113141","FNC 4"=>"114131","CODE A"=>"311141","FNC 1"=>"411131","Start A"=>"211412","Start B"=>"211214","Start C"=>"211232","Stop"=>"2331112");
        $code_keys = array_keys($code_array);
        $code_values = array_flip($code_keys);
        for ( $X = 1; $X <= strlen($text); $X++ ) {
          $activeKey = substr( $text, ($X-1), 1);
          $code_string .= $code_array[$activeKey];
          $chksum=($chksum + ($code_values[$activeKey] * $X));
        }
        $code_string .= $code_array[$code_keys[($chksum - (intval($chksum / 103) * 103))]];
        $code_string = "211214" . $code_string . "2331112";
      } elseif ( strtolower($code_type) == "code128a" ) {
        $chksum = 103;
        $text = strtoupper($text); // Code 128A doesn't support lower case
        // Must not change order of array elements as the checksum depends on the array's key to validate final code
        $code_array = array(" "=>"212222","!"=>"222122","\""=>"222221","#"=>"121223","$"=>"121322","%"=>"131222","&"=>"122213","'"=>"122312","("=>"132212",")"=>"221213","*"=>"221312","+"=>"231212",","=>"112232","-"=>"122132","."=>"122231","/"=>"113222","0"=>"123122","1"=>"123221","2"=>"223211","3"=>"221132","4"=>"221231","5"=>"213212","6"=>"223112","7"=>"312131","8"=>"311222","9"=>"321122",":"=>"321221",";"=>"312212","<"=>"322112","="=>"322211",">"=>"212123","?"=>"212321","@"=>"232121","A"=>"111323","B"=>"131123","C"=>"131321","D"=>"112313","E"=>"132113","F"=>"132311","G"=>"211313","H"=>"231113","I"=>"231311","J"=>"112133","K"=>"112331","L"=>"132131","M"=>"113123","N"=>"113321","O"=>"133121","P"=>"313121","Q"=>"211331","R"=>"231131","S"=>"213113","T"=>"213311","U"=>"213131","V"=>"311123","W"=>"311321","X"=>"331121","Y"=>"312113","Z"=>"312311","["=>"332111","\\"=>"314111","]"=>"221411","^"=>"431111","_"=>"111224","NUL"=>"111422","SOH"=>"121124","STX"=>"121421","ETX"=>"141122","EOT"=>"141221","ENQ"=>"112214","ACK"=>"112412","BEL"=>"122114","BS"=>"122411","HT"=>"142112","LF"=>"142211","VT"=>"241211","FF"=>"221114","CR"=>"413111","SO"=>"241112","SI"=>"134111","DLE"=>"111242","DC1"=>"121142","DC2"=>"121241","DC3"=>"114212","DC4"=>"124112","NAK"=>"124211","SYN"=>"411212","ETB"=>"421112","CAN"=>"421211","EM"=>"212141","SUB"=>"214121","ESC"=>"412121","FS"=>"111143","GS"=>"111341","RS"=>"131141","US"=>"114113","FNC 3"=>"114311","FNC 2"=>"411113","SHIFT"=>"411311","CODE C"=>"113141","CODE B"=>"114131","FNC 4"=>"311141","FNC 1"=>"411131","Start A"=>"211412","Start B"=>"211214","Start C"=>"211232","Stop"=>"2331112");
        $code_keys = array_keys($code_array);
        $code_values = array_flip($code_keys);
        for ( $X = 1; $X <= strlen($text); $X++ ) {
          $activeKey = substr( $text, ($X-1), 1);
          $code_string .= $code_array[$activeKey];
          $chksum=($chksum + ($code_values[$activeKey] * $X));
        }
        $code_string .= $code_array[$code_keys[($chksum - (intval($chksum / 103) * 103))]];
        $code_string = "211412" . $code_string . "2331112";
      } elseif ( strtolower($code_type) == "code39" ) {
        $code_array = array("0"=>"111221211","1"=>"211211112","2"=>"112211112","3"=>"212211111","4"=>"111221112","5"=>"211221111","6"=>"112221111","7"=>"111211212","8"=>"211211211","9"=>"112211211","A"=>"211112112","B"=>"112112112","C"=>"212112111","D"=>"111122112","E"=>"211122111","F"=>"112122111","G"=>"111112212","H"=>"211112211","I"=>"112112211","J"=>"111122211","K"=>"211111122","L"=>"112111122","M"=>"212111121","N"=>"111121122","O"=>"211121121","P"=>"112121121","Q"=>"111111222","R"=>"211111221","S"=>"112111221","T"=>"111121221","U"=>"221111112","V"=>"122111112","W"=>"222111111","X"=>"121121112","Y"=>"221121111","Z"=>"122121111","-"=>"121111212","."=>"221111211"," "=>"122111211","$"=>"121212111","/"=>"121211121","+"=>"121112121","%"=>"111212121","*"=>"121121211");
        // Convert to uppercase
        $upper_text = strtoupper($text);
        for ( $X = 1; $X<=strlen($upper_text); $X++ ) {
          $code_string .= $code_array[substr( $upper_text, ($X-1), 1)] . "1";
        }
        $code_string = "1211212111" . $code_string . "121121211";
      } elseif ( strtolower($code_type) == "code25" ) {
        $code_array1 = array("1","2","3","4","5","6","7","8","9","0");
        $code_array2 = array("3-1-1-1-3","1-3-1-1-3","3-3-1-1-1","1-1-3-1-3","3-1-3-1-1","1-3-3-1-1","1-1-1-3-3","3-1-1-3-1","1-3-1-3-1","1-1-3-3-1");
        for ( $X = 1; $X <= strlen($text); $X++ ) {
          for ( $Y = 0; $Y < count($code_array1); $Y++ ) {
            if ( substr($text, ($X-1), 1) == $code_array1[$Y] )
              $temp[$X] = $code_array2[$Y];
          }
        }
        for ( $X=1; $X<=strlen($text); $X+=2 ) {
          if ( isset($temp[$X]) && isset($temp[($X + 1)]) ) {
            $temp1 = explode( "-", $temp[$X] );
            $temp2 = explode( "-", $temp[($X + 1)] );
            for ( $Y = 0; $Y < count($temp1); $Y++ )
              $code_string .= $temp1[$Y] . $temp2[$Y];
          }
        }
        $code_string = "1111" . $code_string . "311";
      } elseif ( strtolower($code_type) == "codabar" ) {
        $code_array1 = array("1","2","3","4","5","6","7","8","9","0","-","$",":","/",".","+","A","B","C","D");
        $code_array2 = array("1111221","1112112","2211111","1121121","2111121","1211112","1211211","1221111","2112111","1111122","1112211","1122111","2111212","2121112","2121211","1121212","1122121","1212112","1112122","1112221");
        // Convert to uppercase
        $upper_text = strtoupper($text);
        for ( $X = 1; $X<=strlen($upper_text); $X++ ) {
          for ( $Y = 0; $Y<count($code_array1); $Y++ ) {
            if ( substr($upper_text, ($X-1), 1) == $code_array1[$Y] )
              $code_string .= $code_array2[$Y] . "1";
          }
        }
        $code_string = "11221211" . $code_string . "1122121";
      }
      // Pad the edges of the barcode
      $code_length = 20;
      for ( $i=1; $i <= strlen($code_string); $i++ )
        $code_length = $code_length + (integer)(substr($code_string,($i-1),1));
      if ( strtolower($orientation) == "horizontal" ) {
        $img_width = $code_length;
        $img_height = $size;
      } else {
        $img_width = $size;
        $img_height = $code_length;
      }
      $image = imagecreate($img_width, $img_height);
      $black = imagecolorallocate ($image, 0, 0, 0);
      $white = imagecolorallocate ($image, 255, 255, 255);
      imagefill( $image, 0, 0, $white );
      $location = 10;
      for ( $position = 1 ; $position <= strlen($code_string); $position++ ) {
        $cur_size = $location + ( substr($code_string, ($position-1), 1) );
        if ( strtolower($orientation) == "horizontal" )
          imagefilledrectangle( $image, $location, 0, $cur_size, $img_height, ($position % 2 == 0 ? $white : $black) );
        else
          imagefilledrectangle( $image, 0, $location, $img_width, $cur_size, ($position % 2 == 0 ? $white : $black) );
        $location = $cur_size;
      }
      // Draw barcode to the screen
      header ('Content-type: image/png');
      imagepng($image);
      imagedestroy($image);

      $this->setLayout(false);
      exit;
  }

    /**
     * Executes 'Sign' action
     *
     * Sign permit
     *
     * @param sfRequest $request A request object
     */
    public function executeSign(sfWebRequest $request)
    {
        $q = Doctrine_Query::create()
            ->from('SavedPermit a')
            ->where('a.id = ?', $request->getParameter('id'));
        $this->permit = $q->fetchOne();
    }

    /**
     * Executes 'Viewsigned' action
     *
     * Sign permit
     *
     * @param sfRequest $request A request object
     */
    public function executeViewsigned(sfWebRequest $request)
    {
        $q = Doctrine_Query::create()
            ->from('SavedPermit a')
            ->where('a.id = ?', $request->getParameter('id'));
        $this->permit = $q->fetchOne();
        $this->setLayout(false);
    }


    /**
     * Executes 'View' action
     *
     * Views permit
     *
     * @param sfRequest $request A request object
     */
    public function executeView(sfWebRequest $request)
    {
        $q = Doctrine_Query::create()
            ->from('SavedPermit a')
            ->where('a.id = ?', $request->getParameter('id'));
        $this->permit = $q->fetchOne();
        $this->setLayout('layout-metronic');
    }


    /**
     * Executes 'Viewarchive' action
     *
     * Views permit
     *
     * @param sfRequest $request A request object
     */
    public function executeViewarchive(sfWebRequest $request)
    {
        $q = Doctrine_Query::create()
            ->from('SavedPermitArchive a')
            ->where('a.id = ?', $request->getParameter('id'));
        $this->permit = $q->fetchOne();
        $this->setLayout('layout-metronic');
    }

    /**
     * Executes 'List' action
     *
     * Displays list of all of the currently logged in client's permits
     *
     * @param sfRequest $request A request object
     */
    public function executeList(sfWebRequest $request)
    {
        $this->setLayout('layout-metronic');
        $application_manager = new ApplicationManager();


        if($request->getParameter('filter'))
        {
            $filter = $request->getParameter('filter');
        }
        else
        {
            $filter = $application_manager->get_default_form();
        }

        //Filter invoices if logic permissions is set
        //if($application_manager->any_logic_permissions($filter)) {
        if(false){
            $logiced_forms = $application_manager->get_logiced_forms();

            $entries_b = null;

            if($filter)
            {
                $entries_b = $application_manager->get_logiced_entries($filter,$this->getUser(),'b');
            }
            else
            {
                $entries_b = $application_manager->get_logiced_entries($logiced_forms[0],$this->getUser(),'b');
            }

            if ($request->getPostParameter('search')) {
                $this->q = Doctrine_Query::create()
                    ->from('SavedPermit a')
                    ->leftJoin('a.FormEntry b')
                    ->where('b.application_id LIKE ?', "%" . $request->getPostParameter('search') . "%")
                    ->andWhere($entries_b)
                    ->andWhere('a.permit_status <> 3')
                    ->orderBy('a.id DESC');
            } else {
                if ($request->getParameter('filter')) {
                    $this->filter = $request->getParameter('filter');
                    if ($request->getPostParameter('fromdate') || $request->getParameter('fromdate')) {
                        if ($request->getParameter('fromdate')) {
                            $this->fromdate = $request->getParameter('fromdate');
                            $this->todate = $request->getParameter('todate');
                        } else {
                            $this->fromdate = $request->getPostParameter('fromdate');
                            $this->todate = $request->getPostParameter('todate');
                        }

                        if ($this->fromdate == $this->todate) {
                            $this->fromdate = date("Y-m-d", strtotime($this->fromdate));
                            $this->q = Doctrine_Query::create()
                                ->from('SavedPermit a')
                                ->leftJoin('a.FormEntry b')
                                ->where('b.form_id = ?', $this->filter)
                                ->andWhere('a.date_of_issue LIKE ?', "%" . $this->fromdate . "%")
                                ->andWhere($entries_b)
                                ->andWhere('a.permit_status <> 3')
                                ->orderBy('a.id DESC');
                        } else {
                            $this->q = Doctrine_Query::create()
                                ->from('SavedPermit a')
                                ->leftJoin('a.FormEntry b')
                                ->where('b.form_id = ?', $this->filter)
                                ->andWhere('a.date_of_issue BETWEEN ? AND ?', array(date('Y-m-d', strtotime(date("Y-m-d", strtotime($this->fromdate)) . "-1 day")), date('Y-m-d', strtotime(date("Y-m-d", strtotime($this->todate)) . "+1 day"))))
                                ->andWhere($entries_b)
                                ->andWhere('a.permit_status <> 3')
                                ->orderBy('a.id DESC');
                        }
                    } else {
                        $this->q = Doctrine_Query::create()
                            ->from('SavedPermit a')
                            ->leftJoin('a.FormEntry b')
                            ->where('b.form_id = ?', $request->getParameter('filter'))
                            ->andWhere($entries_b)
                            ->andWhere('a.permit_status <> 3')
                            ->orderBy('a.id DESC');
                        $this->filter = $request->getParameter('filter');
                    }
                } else {
                    $this->filter = 0;

                    if ($request->getPostParameter('fromdate') || $request->getParameter('fromdate')) {
                        if ($request->getParameter('fromdate')) {
                            $this->fromdate = $request->getParameter('fromdate');
                            $this->todate = $request->getParameter('todate');
                        } else {
                            $this->fromdate = $request->getPostParameter('fromdate');
                            $this->todate = $request->getPostParameter('todate');
                        }

                        if ($this->fromdate == $this->todate) {
                            $this->fromdate = date("Y-m-d", strtotime($this->fromdate));
                            $this->q = Doctrine_Query::create()
                                ->from('SavedPermit a')
                                ->leftJoin('a.FormEntry b')
                                ->where('a.date_of_issue LIKE ?', "%" . $this->fromdate . "%")
                                ->andWhere($entries_b)
                                ->andWhere('a.permit_status <> 3')
                                ->orderBy('a.id DESC');
                        } else {
                            $this->q = Doctrine_Query::create()
                                ->from('SavedPermit a')
                                ->leftJoin('a.FormEntry b')
                                ->where('a.date_of_issue BETWEEN ? AND ?', array(date('Y-m-d', strtotime(date("Y-m-d", strtotime($this->fromdate)) . "-1 day")), date('Y-m-d', strtotime(date("Y-m-d", strtotime($this->todate)) . "+1 day"))))
                                ->andWhere($entries_b)
                                ->andWhere('a.permit_status <> 3')
                                ->orderBy('a.id DESC');
                        }
                    } else {
                        $this->q = Doctrine_Query::create()
                            ->from('SavedPermit a')
                            ->leftJoin('a.FormEntry b')
                            ->andWhere($entries_b)
                            ->andWhere('a.permit_status <> 3')
                            ->orderBy('a.id DESC');
                    }
                }
            }
        }
        else
        {
            if ($request->getPostParameter('search')) {
                $this->q = Doctrine_Query::create()
                    ->from('SavedPermit a')
                    ->leftJoin('a.FormEntry b')
                    ->where('b.application_id LIKE ?', "%" . $request->getPostParameter('search') . "%")
                    ->andWhere('a.permit_status <> 3')
                    ->orderBy('a.id DESC');
            } else {
                if ($request->getParameter('filter')) {
                    $this->filter = $request->getParameter('filter');
                    if ($request->getPostParameter('fromdate') || $request->getParameter('fromdate')) {
                        if ($request->getParameter('fromdate')) {
                            $this->fromdate = $request->getParameter('fromdate');
                            $this->todate = $request->getParameter('todate');
                        } else {
                            $this->fromdate = $request->getPostParameter('fromdate');
                            $this->todate = $request->getPostParameter('todate');
                        }

                        if ($this->fromdate == $this->todate) {
                            $this->fromdate = date("Y-m-d", strtotime($this->fromdate));
                            $this->q = Doctrine_Query::create()
                                ->from('SavedPermit a')
                                ->leftJoin('a.FormEntry b')
                                ->where('b.form_id = ?', $this->filter)
                                ->andWhere('a.date_of_issue LIKE ?', "%" . $this->fromdate . "%")
                                ->andWhere('a.permit_status <> 3')
                                ->orderBy('a.id DESC');
                        } else {
                            $this->q = Doctrine_Query::create()
                                ->from('SavedPermit a')
                                ->leftJoin('a.FormEntry b')
                                ->where('b.form_id = ?', $this->filter)
                                ->andWhere('a.date_of_issue BETWEEN ? AND ?', array(date('Y-m-d', strtotime(date("Y-m-d", strtotime($this->fromdate)) . "-1 day")), date('Y-m-d', strtotime(date("Y-m-d", strtotime($this->todate)) . "+1 day"))))
                                ->andWhere('a.permit_status <> 3')
                                ->orderBy('a.id DESC');
                        }
                    } else {
                        $this->q = Doctrine_Query::create()
                            ->from('SavedPermit a')
                            ->leftJoin('a.FormEntry b')
                            ->where('b.form_id = ?', $request->getParameter('filter'))
                            ->andWhere('a.permit_status <> 3')
                            ->orderBy('a.id DESC');
                        $this->filter = $request->getParameter('filter');
                    }
                } else {
                    $this->filter = 0;

                    if ($request->getPostParameter('fromdate') || $request->getParameter('fromdate')) {
                        if ($request->getParameter('fromdate')) {
                            $this->fromdate = $request->getParameter('fromdate');
                            $this->todate = $request->getParameter('todate');
                        } else {
                            $this->fromdate = $request->getPostParameter('fromdate');
                            $this->todate = $request->getPostParameter('todate');
                        }

                        if ($this->fromdate == $this->todate) {
                            $this->fromdate = date("Y-m-d", strtotime($this->fromdate));
                            $this->q = Doctrine_Query::create()
                                ->from('SavedPermit a')
                                ->leftJoin('a.FormEntry b')
                                ->where('a.date_of_issue LIKE ?', "%" . $this->fromdate . "%")
                                ->andWhere('a.permit_status <> 3')
                                ->orderBy('a.id DESC');
                        } else {
                            $this->q = Doctrine_Query::create()
                                ->from('SavedPermit a')
                                ->leftJoin('a.FormEntry b')
                                ->where('a.date_of_issue BETWEEN ? AND ?', array(date('Y-m-d', strtotime(date("Y-m-d", strtotime($this->fromdate)) . "-1 day")), date('Y-m-d', strtotime(date("Y-m-d", strtotime($this->todate)) . "+1 day"))))
                                ->andWhere('a.permit_status <> 3')
                                ->orderBy('a.id DESC');
                        }
                    } else {
                        $this->q = Doctrine_Query::create()
                            ->from('SavedPermit a')
                            ->leftJoin('a.FormEntry b')
                            ->where('a.permit_status <> 3')
                            ->orderBy('a.id DESC');
                    }
                }
            }

        }

        if ($request->getParameter('fromdate')) {
            $this->fromdate = date("Y-m-d", strtotime($request->getParameter('fromdate')));
            $this->todate = date("Y-m-d", strtotime($request->getParameter('todate')));
        }

        if($request->getParameter("export"))
        {
            $columns = "";
            $columns[] = "Application No";
            $columns[] = "Service Code";
            $columns[] = "Application Form";
            $columns[] = "Application Stage";
            $columns[] = "Date Issued";
            $columns[] = "Issued To";

            $records = "";

            $permits = $this->q->execute();

            foreach($permits as $permit)
            {
                $application = $permit->getFormEntry();

                if(empty($application))
                {
                    continue;
                }

                $record_columns = "";

                $record_columns[] = $application->getApplicationId();

                $q = Doctrine_Query::create()
                    ->from("ApForms a")
                    ->where("a.form_id = ?", $application->getFormId());
                $form = $q->fetchOne();
                if($form)
                {
                    $record_columns[] = $form->getFormCode();
                    $record_columns[] = $form->getFormName();
                }
                else
                {
                    $record_columns[] = "";
                    $record_columns[] = "";
                }

                $q = Doctrine_Query::create()
                    ->from('SubMenus a')
                    ->where('a.id = ?', $application->getApproved());
                $submenu = $q->fetchOne();
                if($submenu)
                {
                    $record_columns[] = $submenu->getTitle();
                }
                else
                {
                    $record_columns[] = "";
                }

                $record_columns[] = $application->getDateOfResponse();

                $q = Doctrine_Query::create()
                    ->from("SfGuardUserProfile a")
                    ->where("a.user_id = ?", $application->getUserId());
                $user = $q->fetchOne();

                if($user)
                {
                    $record_columns[] = ucwords(strtolower($user->getFullname()));
                }
                else
                {
                    $record_columns[] = "";
                }

                $records[] = $record_columns;

            }

            $this->ReportGenerator("Permits Report ".date("Y-m-d"), $columns, $records);
            exit;
        }

        $this->pager = new sfDoctrinePager('SavedPermit', 10);
        $this->pager->setQuery($this->q);
        $this->pager->setPage($request->getParameter('page', 1));
        $this->pager->init();
        
    }
    /**
     * Executes 'Checkname' action
     *
     * Ajax used to check existence of name
     *
     * @param sfRequest $request A request object
     */
    public function executeCheckname(sfWebRequest $request)
    {
        // add new user
        $q = Doctrine_Query::create()
            ->from("permits a")
            ->where('a.title = ?', $request->getPostParameter('name'));
        $existinggroup = $q->execute();
        if(sizeof($existinggroup) > 0)
        {
            echo '<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Name is already in use!</strong></div><script language="javascript">document.getElementById("submitbuttonname").disabled = true;</script>';
            exit;
        }
        else
        {
            echo '<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Name is available!</strong></div><script language="javascript">document.getElementById("submitbuttonname").disabled = false;</script>';
            exit;
        }
    }


    public function executeBatch(sfWebRequest $request)
    {
        if($request->getPostParameter('delete'))
        {
            $item = Doctrine_Core::getTable('permits')->find(array($request->getPostParameter('delete')));
            if($item)
            {
                $item->delete();
            }
        }
        exit;
    }

    public function executeIndex(sfWebRequest $request)
    {
        if($request->getParameter("filter"))
        {
          //Save filter to session
          $this->getUser()->setAttribute('filter', $request->getParameter("filter"));
        }

        $q = Doctrine_Query::create()
           ->from("SubMenus a")
           ->where("a.menu_id = ?", $request->getParameter("filter", $this->getUser()->getAttribute("filter")));
        $stages = $q->execute();

        $stages_array = array();

        foreach($stages as $stage)
        {
            $stages_array[] = "a.applicationstage = ".$stage->getId();
        }

        $stages_query = implode(" OR ", $stages_array);

        $q = Doctrine_Query::create()
           ->from("Permits a")
           ->where($stages_query);
        $this->permitss = $q->execute();

        $this->setLayout("layout-settings");
    }


    public function executeGetsettings(sfWebRequest $request)
    {
        $this->form = $request->getParameter('id');
        $this->setLayout(false);
    }

    public function executeNew(sfWebRequest $request)
    {
        $this->form = new permitsForm();
        $this->setLayout("layout-settings");
    }

    public function executeAjaxcreate(sfWebRequest $request)
    {
        $this->forward404Unless($request->isMethod(sfRequest::POST));

        $this->form = new permitsForm();

        $this->ajaxprocessForm($request, $this->form);

        $this->setTemplate('new');
    }

    protected function ajaxprocessForm(sfWebRequest $request, sfForm $form)
    {
        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
        if ($form->isValid())
        {
            $permits = $form->save();

            $this->redirect('/backend.php/dashboard');
        }
    }

    public function executeCreate(sfWebRequest $request)
    {
        $this->forward404Unless($request->isMethod(sfRequest::POST));

        $this->form = new permitsForm();

        $this->processForm($request, $this->form);

        $this->setTemplate('new');
    }

    public function executeEdit(sfWebRequest $request)
    {
        $this->forward404Unless($permits = Doctrine_Core::getTable('permits')->find(array($request->getParameter('id'))), sprintf('Object permits does not exist (%s).', $request->getParameter('id')));
        $this->form = new permitsForm($permits);
        $this->setLayout("layout-settings");
    }

    public function executeUpdate(sfWebRequest $request)
    {
        $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
        $this->forward404Unless($permits = Doctrine_Core::getTable('permits')->find(array($request->getParameter('id'))), sprintf('Object permits does not exist (%s).', $request->getParameter('id')));
        $this->form = new permitsForm($permits);

        $this->processForm($request, $this->form);

        $this->setTemplate('edit');
    }

    public function executeDelete(sfWebRequest $request)
    {

        $this->forward404Unless($permits = Doctrine_Core::getTable('permits')->find(array($request->getParameter('id'))), sprintf('Object permits does not exist (%s).', $request->getParameter('id')));
        $permits->delete();

        $this->redirect('/backend.php/permits/index');
    }

    public function executeCancelpermit(sfWebRequest $request)
    {
        $this->forward404Unless($permits = Doctrine_Core::getTable('SavedPermit')->find(array($request->getParameter('id'))), sprintf('Object permits does not exist (%s).', $request->getParameter('id')));
        $permits->setPermitStatus(3);
        $permits->save();

        $this->redirect('/backend.php/applications/view/id/'.$permits->getFormEntry()->getId());
    }

    public function executeUpdatepermit(sfWebRequest $request)
    {
        $this->forward404Unless($permit = Doctrine_Core::getTable('SavedPermit')->find(array($request->getParameter('id'))), sprintf('Object permits does not exist (%s).', $request->getParameter('id')));

        //Run the save function first so that remote results are updated. Then generate pdf with new results and run save again
        $permit->save();

        $permit_manager = new PermitManager();

        $filename = $permit_manager->save_to_pdf_locally($permit->getId());
        $permit->setPdfPath($filename);
        $permit->save();

        $this->redirect('/backend.php/permits/view/id/'.$permit->getId());
    }

    public function executeUncancelpermit(sfWebRequest $request)
    {
        $this->forward404Unless($permits = Doctrine_Core::getTable('SavedPermit')->find(array($request->getParameter('id'))), sprintf('Object permits does not exist (%s).', $request->getParameter('id')));
        $permits->setPermitStatus(1);
        $permits->save();

        $this->redirect('/backend.php/permits/view/id/'.$permits->getId());
    }

    protected function processForm(sfWebRequest $request, sfForm $form)
    {
        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
        if ($form->isValid())
        {
            $permits = $form->save();

            $this->redirect('/backend.php/permits/index');
        }
    }

    /**
     * Executes 'ReportGenerator' action
     *
     * Reusable excel generator
     *
     * @param sfRequest $request A request object
     */
    public function ReportGenerator($reportname, $columns, $records)
    {
        date_default_timezone_set('Africa/Kigali');

        if (PHP_SAPI == 'cli')
            die('This example should only be run from a Web Browser');

        /** Include PHPExcel */
        require_once dirname(__FILE__).'/../../../../../lib/vendor/phpexcel/Classes/PHPExcel.php';

        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("One Stop Center, City Of Kigali")
            ->setTitle($reportname);

        // Add some data
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $alpha_count = "B";
        foreach ($columns as $key => $value)
        {
            $objPHPExcel->getActiveSheet()->getColumnDimension($alpha_count)->setAutoSize(true);
            $alpha_count++;
        }

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A4', 'No');
        $alpha_count = "B";
        foreach ($columns as $key => $value)
        {
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($alpha_count.'4', $value);
            $alpha_count++;
        }

        $alpha_count--;

        $objPHPExcel->getActiveSheet()->getStyle('A4')->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->getStyle('A4:'.($alpha_count).'4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A4:'.($alpha_count).'4')->getFill()->getStartColor()->setARGB('46449a');

        $objPHPExcel->getActiveSheet()->getStyle('A4')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);

        $alpha_count = "B";
        foreach ($columns as $key => $value)
        {
            $objPHPExcel->getActiveSheet()->getStyle($alpha_count.'4')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
            $alpha_count++;
        }

        $alpha_count--;

        $objPHPExcel->getActiveSheet()->getStyle('A1:'.($alpha_count).'1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A1:'.($alpha_count).'1')->getFill()->getStartColor()->setARGB('504dc5');
        $objPHPExcel->getActiveSheet()->getStyle('A2:'.($alpha_count).'2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A2:'.($alpha_count).'2')->getFill()->getStartColor()->setARGB('504dc5');
        $objPHPExcel->getActiveSheet()->getStyle('A3:'.($alpha_count).'3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $objPHPExcel->getActiveSheet()->getStyle('A3:'.($alpha_count).'3')->getFill()->getStartColor()->setARGB('504dc5');

        $objDrawing = new PHPExcel_Worksheet_Drawing();
        $objDrawing->setName('Logo');
        $objDrawing->setDescription('Logo');
        $objDrawing->setPath('./assets_unified/images/logo.png');
        $objDrawing->setHeight(60);
        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

        $alpha_count = "B";
        foreach ($columns as $key => $value)
        {
            $objPHPExcel->getActiveSheet()->getStyle($alpha_count.'4')->getFont()->setBold(true);
            $alpha_count++;
        }

        /**
         * Fetch all applications linked to the filtered 'type of application' and the 'start date'
         */
        $count = 5;

        // Miscellaneous glyphs, UTF-8
        $alpha_count = "B";

        foreach($records as $record_columns)
        {
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$count, $count-4);
            $alpha_count = "B";
            foreach ($record_columns as $key => $value)
            {
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($alpha_count.$count, $value);
                $alpha_count++;
            }
            $count++;
        }


        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle($reportname);


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$reportname.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    }

    public function executeUpdatesingleremote(sfWebRequest $request)
    {
        $q = Doctrine_Query::create()
            ->from('SavedPermit a')
            ->where('a.id = ?', $request->getParameter('id'));
        $permit = $q->fetchOne();

        if($permit)
        {
            try
            {
                $permit->save();

                $this->redirect("/backend.php/permits/view/id/".$permit->getId());

            }catch(Exception $ex)
            {
                error_log("Updates Managert: Remote update error: ".$ex);
                echo "Remote Update Error Debug-v: ".$error;
                exit;
            }
        }
    }

    public function executeUpdatesingleremotearchive(sfWebRequest $request)
    {
        $q = Doctrine_Query::create()
            ->from('SavedPermitArchive a')
            ->where('a.id = ?', $request->getParameter('id'));
        $permit = $q->fetchOne();

        if($permit)
        {
            try
            {
                //$permit->setRemoteResult($results);

                $permit->save();

                $this->redirect("/backend.php/permits/viewarchive/id/".$permit->getId());

            }catch(Exception $ex)
            {
                error_log("Debug-endorsement: Remote update error: ".$ex);
                echo "Remote Update Error Debug-v: ".$error;
                exit;
            }
        }
    }

    public function executeUpdatetest(sfWebRequest $request)
    {
        $q = Doctrine_Query::create()
            ->from('SavedPermit a')
            ->where('a.id = ?', $request->getParameter('id'));
        $permit = $q->fetchOne();

        if($permit)
        {
            //Remote Update
            $q = Doctrine_Query::create()
                ->from('Permits a')
                ->where('a.id = ?',$permit->getTypeId());
            $template = $q->fetchOne();

            //Update remote url
            if($template && $template->getRemoteUrl())
            {

                try{
                    error_log("Debug-endorsement: Found Permit Template");
                    $remote_url = $template->getRemoteUrl();
                    $remote_fields = $template->getRemoteField();
                    $remote_username = $template->getRemoteUsername();
                    $remote_password = $template->getRemotePassword();

                    $date_of_expiry = $permit->getDateOfExpiry();

                    //Send updates to remote database

                    $q = Doctrine_Query::create()
                        ->from('FormEntry a')
                        ->where('a.id = ?', $permit->getApplicationId());
                    $application = $q->fetchOne();

                    //parse the url through a template parser incase there are any fields from the form
                    $templateparser = new TemplateParser();

                    $remote_url = $templateparser->parseRemote($application->getId(), $application->getFormId(), $application->getEntryId(), $permit->getId(), $remote_url);

                    $remote_fields = $templateparser->parseRemote($application->getId(), $application->getFormId(), $application->getEntryId(), $permit->getId(), $remote_fields);

                    $fields = explode($remote_fields, '=');
                    $field_count = count($fields);

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $remote_url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                    if(!empty($remote_username) && !empty($remote_password))
                    {
                        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                        curl_setopt($ch, CURLOPT_USERPWD, "$remote_username:$remote_password");
                    }
                    curl_setopt($ch, CURLOPT_ENCODING ,"");
                    curl_setopt($ch,CURLOPT_POST, $field_count);
                    curl_setopt($ch,CURLOPT_POSTFIELDS, $remote_fields);

                    error_log("Debug-endorsement: Remote Data to be Updated Debug-v: ".$remote_fields);
                    echo "<pre>", "Debug-endorsement: Remote Data to be Updated Debug-v: ".$remote_fields, "</pre>";


                    $results = curl_exec($ch);

                    //$permit->setRemoteResult($results);

                    echo "Response: ".$results;

                    //$permit->save();

                    if($error = curl_error($ch))
                    {
                        $error = "Debug-endorsement: Remote Update Error Debug-v: ".$error;
                        echo "Remote Update Error Debug-v: ".$error;
                        exit;
                        error_log($error);
                    }
                    else
                    {
                        error_log("Debug-endorsement: Remote Update Success Debug-v");
                        echo "<pre>", "<br>Debug-endorsement: Remote Update Success Debug-v", "</pre>";
                        exit;
                    }

                    curl_close($ch);
                }catch(Exception $ex)
                {
                    error_log("Debug-endorsement: Remote update error: ".$ex);
                    echo "Remote Update Error Debug-v: ".$error;
                    exit;
                }
            }
        }
    }

    public function executeUpdateremote(sfWebRequest $request)
    {
        $from_date = $_GET['start_date'];
        $to_date = $_GET['end_date'];

        $from_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($from_date)) . "-1 day"));
        $to_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($to_date)) . "+1 day"));

        $records_count = 0;

        if(!empty($from_date) && !empty($to_date))
        {
            error_log('Debug-v: Found date filters for updating remote result');
            $permits = null;

            if($_GET['form_id'])
            {
                $q = Doctrine_Query::create()
                    ->from('SavedPermit a')
                    ->leftJoin('a.FormEntry b')
                    ->where('a.date_of_issue BETWEEN ? AND ?', array($from_date,$to_date))
                    ->andWhere('b.form_id = ?', $_GET['form_id']);
                $permits = $q->execute();
            }
            else
            {
                $q = Doctrine_Query::create()
                    ->from('SavedPermit a')
                    ->where('a.date_of_issue BETWEEN ? AND ?', array($from_date,$to_date));
                $permits = $q->execute();
            }
            foreach($permits as $permit)
            {
                //Remote Update
                $q = Doctrine_Query::create()
                    ->from('Permits a')
                    ->where('a.id = ?',$permit->getTypeId());
                $template = $q->fetchOne();

                //Update remote url
                if($template && $template->getRemoteUrl())
                {

                    try{
                        error_log("Debug-v: Found Permit Template");
                        $remote_url = $template->getRemoteUrl();
                        $remote_fields = $template->getRemoteField();
                        $remote_username = $template->getRemoteUsername();
                        $remote_password = $template->getRemotePassword();

                        $date_of_expiry = $permit->getDateOfExpiry();

                        //Send updates to remote database

                        $q = Doctrine_Query::create()
                            ->from('FormEntry a')
                            ->where('a.id = ?', $permit->getApplicationId());
                        $application = $q->fetchOne();

                        //parse the url through a template parser incase there are any fields from the form
                        $templateparser = new TemplateParser();

                        $remote_url = $templateparser->parseRemote($application->getId(), $application->getFormId(), $application->getEntryId(), $permit->getId(), $remote_url);

                        $remote_fields = $templateparser->parseRemote($application->getId(), $application->getFormId(), $application->getEntryId(), $permit->getId(), $remote_fields);

                        $fields = explode($remote_fields, '=');
                        $field_count = count($fields);

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $remote_url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                        if(!empty($remote_username) && !empty($remote_password))
                        {
                            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                            curl_setopt($ch, CURLOPT_USERPWD, "$remote_username:$remote_password");
                        }
                        curl_setopt($ch, CURLOPT_ENCODING ,"");
                        curl_setopt($ch,CURLOPT_POST, $field_count);
                        curl_setopt($ch,CURLOPT_POSTFIELDS, $remote_fields);


                        $results = curl_exec($ch);

                        $permit->setRemoteResult($results);

                        $permit->save();

                        if($error = curl_error($ch))
                        {
                            $error = "Remote Update Error Debug-v: ".$error;
                            error_log($error);
                        }
                        else
                        {
                            error_log("Remote Update Success Debug-v");
                            $records_count++;
                        }

                        curl_close($ch);
                    }catch(Exception $ex)
                    {
                        error_log("Debug-v: Remote update error: ".$ex);
                    }
                }
            }
        }
        else
        {
            error_log('Debug-v: No date filters found for updating remote results for permit');
        }

        $this->stats = "Successfully ran remote updates: ".$records_count." records updates";

    }

}
