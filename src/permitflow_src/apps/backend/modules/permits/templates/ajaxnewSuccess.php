<?php
use_helper("I18N");
?>

<div class="panel panel-dark">
<div class="panel-heading">
<h3 class="panel-title"><?php echo __('New Permit'); ?></h3>
</div>

    <?php include_partial('ajaxform', array('form' => $form)) ?>
</div>
