<?php
/**
 * indexSuccess.php template.
 *
 * Displays list of all of the currently logged in client's permits
 *
 * @package    frontend
 * @subpackage permits
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");
?>


 <div class="pageheader">
    <h2><i class="fa fa-certificate"></i><?php echo __('Permits'); ?></h2>
  <div class="breadcrumb-wrapper">

    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>index.php"><?php echo __('Home'); ?></a></li>
      <li><a href="<?php echo public_path(); ?>index.php/permits/index"><?php echo __('Permits'); ?></a></li>
    </ol>
  </div>
</div>

<div class="contentpanel">
    <div class="row">

 <div class="panel panel-dark widget-btns">
   <div class="panel-heading">
     <h3 class="panel-title"><?php echo __('Permits'); ?></h3>
      <p class="text-muted"><?php echo __('Issued Permits'); ?></p>
      <div class="pull-right">
      <form method="post">
      <input type="text" class="form-control pull-right" style="margin-top: -45px;" name="search" id="search" placeholder="Search..." value="<?php echo $search; ?>"/>
    </form>
    </div>
    </div>


  <div class="panel-body panel-body-nopadding">
 <div class="table-responsive">
   <table class="table table-bordered table-striped table-hover mb0 border-top-0 border-left-0 border-right-0 panel-table">
      <thead class="form-horizontal">
      <tr>
        <th class="form-group">
          <select size="1" name="table2_length" aria-controls="table2" class="form-control chosen-select" onChange="window.location='/backend.php/permits/list/filter/' + this.value;">
          <option value="0" selected="selected">Select Product/Service</option>
          <?php
          $q = Doctrine_Query::create()
              ->from('ApForms a')
              ->where('a.form_id <> 15 AND a.form_id <> 16 AND a.form_id <> 17 AND  a.form_id <> 6 AND a.form_id <> 7')
              ->andWhere('a.form_active = 1 AND a.form_type = 1')
              ->orderBy('a.form_name ASC');
          $applicationforms = $q->execute();
          foreach($applicationforms as $applicationform)
          {
            $selected = "";
            if($applicationform->getFormId() == $filter)
            {
              $selected = "selected='selected'";
            }
            echo "<option value='".$applicationform->getFormId()."' ".$selected.">".$applicationform->getFormCode()." - ".$applicationform->getFormName()."</option>";
          }
          ?>
          </select>
       </th>

       <form method="post" action="/backend.php/permits/list/filter/<?php echo $filter; ?>">
         <th class="form-group">
          <input name="fromdate" value="<?php echo $fromdate; ?>" style="padding: 5px;" type="text" class="form-control" placeholder="Starting From" id="datepicker1">

       </th>
        <th class="form-group">

          <input name="todate" value="<?php echo $todate; ?>" style="padding: 5px;" type="text" class="form-control" placeholder="Ending" id="datepicker2">

        </th>
           <th class="form-group">
          <button type="submit" class="btn btn-xs btn-default">GO</button>
         </th>
       </form>
        <th>
        <div><form style="margin:0" action="/backend.php/permits/list<?php if($filter){ echo "/filter/".$filter; } ?><?php if($fromdate){ echo "/fromdate/".$fromdate."/todate/".$todate; } ?>/export/1" method="post"><input type='hidden' name='export' id='export' value='1'><button class="btn btn-default btn-xs btn-excell" type="submit">Export</button></form></div>
       </th>
     </tr>
      </thead>
    </table>
</div>
          <div class="table-responsive">

           <?php if ($pager->getResults()): ?>
           <table class="table mb0">
           <thead>
             <th>#</th>
             <th><?php echo __('Permit No'); ?></th>
             <th><?php echo __('Application No'); ?></th>
             <th><?php echo __('Application Form'); ?></th>
             <th><?php echo __('Date Issued'); ?></th>
             <th><?php echo __('Issued To'); ?></th>
             <th class="no-sort" width="80"><?php echo __('Action'); ?></th>
           </thead>
                <tbody>
                <?php
                  $count = 0;
                  foreach($pager->getResults() as $permit)
                  {

                  $q = Doctrine_Query::create()
                      ->from('FormEntry a')
                      ->where('a.id = ?', $permit->getApplicationId());
                  $application = $q->fetchOne();

                      if(empty($application))
                      {
                          continue;
                      }

                    $count++;
                    ?>
                      <tr>
                         <td><?php echo $permit->getId(); ?></td>
                         <td>
                            <?php echo $permit->getPermitId(); ?>
                         </td>
                         <td>
                            <?php echo $application->getApplicationId(); ?>
                         </td>
                         <td>
                         <?php
                           $q = Doctrine_Query::create()
                             ->from('ApForms a')
                             ->where('a.form_id = ?', $application->getFormId());
                             $form = $q->fetchOne();
                             if($form)
                             {
                                echo $form->getFormName();
                             }
                         ?>
                         </td>
                         <td>

                          <?php echo $permit->getDateOfIssue(); ?>

                         </td>

                         <td>
                         <?php
                          $q = Doctrine_Query::create()
                             ->from("SfGuardUserProfile a")
                             ->where("a.user_id = ?", $application->getUserId());
                          $user = $q->fetchOne();
                          if($user)
                          {
                            echo ucwords(strtolower($user->getFullname()));
                          }
                         ?>
                         </td>

                          <td>
                          <a target="_blank" title='<?php echo __('View'); ?>' href='<?php echo public_path(); ?>backend.php/permits/view/id/<?php echo $permit->getId(); ?>'>
            <span class="badge badge-primary"><i class="fa fa-eye"></i></span></a>

                          <!-- <a title='<?php echo __('Print'); ?>' href='<?php echo public_path(); ?>backend.php/applications/viewpermit/id/<?php echo $permit->getId(); ?>'>
            <span class="badge badge-primary"><i class="fa fa-print"></i></span></a> -->
                          </td>
                   </tr>
                   <?php

                  }
                   ?>

                </tbody>
                <tfoot>
                <tr>
                <th colspan="12">
     <p class="table-showing pull-left"><strong><?php echo count($pager) ?></strong> Permits

                                            <?php if ($pager->haveToPaginate()): ?>
                                              - page <strong><?php echo $pager->getPage() ?>/<?php echo $pager->getLastPage() ?></strong>
                                            <?php endif; ?></p>




        <?php if ($pager->haveToPaginate()): ?>
                <ul class="pagination pagination-sm mb0 mt0 pull-right">
                  <li><a href="/backend.php/permits/list/page/1<?php if($filter){ echo "/filter/".$filter; } ?><?php if($fromdate){ echo "/fromdate/".$fromdate."/todate/".$todate; } ?>">
                      <i class="fa fa-angle-left"></i>
                  </a></li>

                  <li><a href="/backend.php/permits/list/page/<?php echo $pager->getPreviousPage() ?><?php if($filter){ echo "/filter/".$filter; } ?><?php if($fromdate){ echo "/fromdate/".$fromdate."/todate/".$todate; } ?>">
                      <i class="fa fa-angle-left"></i>
                  </a></li>

                  <?php foreach ($pager->getLinks() as $page): ?>
                    <?php if ($page == $pager->getPage()): ?>
                      <li class="active"><a href=""><?php echo $page ?></li></a>
                    <?php else: ?>
                      <li><a href="/backend.php/permits/list/page/<?php echo $page ?><?php if($filter){ echo "/filter/".$filter; } ?><?php if($fromdate){ echo "/fromdate/".$fromdate."/todate/".$todate; } ?>"><?php echo $page ?></a></li>
                    <?php endif; ?>
                  <?php endforeach; ?>

                  <li><a href="/backend.php/permits/list/page/<?php echo $pager->getNextPage() ?><?php if($filter){ echo "/filter/".$filter; } ?><?php if($fromdate){ echo "/fromdate/".$fromdate."/todate/".$todate; } ?>">
                      <i class="fa fa-angle-right"></i>
                  </a></li>

                  <li><a href="/backend.php/permits/list/page/<?php echo $pager->getLastPage() ?><?php if($filter){ echo "/filter/".$filter; } ?><?php if($fromdate){ echo "/fromdate/".$fromdate."/todate/".$todate; } ?>">
                      <i class="fa fa-angle-right"></i>
                  </a>
                 </li>
                </ul>
              <?php endif; ?>
                </th>
                </tr>
                <?php
                if($fromdate)
                {
                ?>
                <tr>
                <th>
                <a class="btn btn-primary" href="/backend.php/permits/updateremote?start_date=<?php echo $fromdate; ?>&end_date=<?php echo $todate; ?><?php if($filter){ echo "&form_id=".$filter; } ?>">Update remote database (If any is configured)</a>
                </th>
                </tr>
                <?php
                }
                ?>
                </tfoot>
              </table>
                        </div><!-- table-responsive -->
                    <?php else: ?>
                      <div class="table-responsive">
                          <table class="table dt-on-steroids mb0">
                            <tbody>
                              <tr><td>
                            No Records Found
                            </td></tr>
                          </tbody>
                        </table>
                      </div>
                    <?php endif; ?>



      </div><!-- panel-body-nopadding pt10 -->
  </div><!-- panel -->

 	</div><!--panel-row-->
</div><!--contentpanel-->

<script>
jQuery(document).ready(function(){
  // Date Picker
  jQuery('#datepicker1').datepicker();
  jQuery('#datepicker2').datepicker();
});
</script>
