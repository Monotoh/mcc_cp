<?php
/**
 * viewSuccess.php template.
 *
 * Displays a full invoice
 *
 * @package    frontend
 * @subpackage invoices
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
*/
  $q = Doctrine_Query::create()
   ->from('FormEntry a')
   ->where('a.id = ?', $permit->getApplicationId());
  $application = $q->fetchOne();
 ?>
 <div class="pageheader">
  <h2><i class="fa fa-envelope"></i> Permit <span>View permit details</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>index.php">Home</a></li>
      <li><a href="<?php echo public_path(); ?>index.php/permits/index">Permits</a></li>
      <li class="active"><?php echo $application->getApplicationId(); ?></li>
    </ol>
  </div>
</div>

<div class="contentpanel">
    <div class="row">

        <div class="col-sm-12">

        <ul id="myTab" class="nav nav-tabs" style="margin-top:20px; margin-right:20px;">
            <li class="active"><a href="#tabs-1" data-toggle="tab"><?php if($permit->getPermitId()){ echo $permit->getPermitId(); }else{ echo $permit->getFormEntry()->getApplicationId(); } ?> Details</a></li>
        	<li class="pull-right" style="padding-top: 5px; padding-right: 10px;"><button class="btn btn-white" id="printinvoice" type="button" onClick="window.location='/backend.php/applications/viewpermit/id/<?php echo $permit->getId(); ?>';"><i class="fa fa-print mr5"></i> Print</button></li>
		</ul>
        <div id="myTabContent" class="tab-content" style=" margin-right:20px;">
                    <div class="tab-pane fade in active" id="tabs-1">
                        <?php
                        $document_key = $permit->getDocumentKey();
                            $permit_manager = new PermitManager();
                            $html = $permit_manager->generate_permit_template($permit->getId(), false);

                            echo $html;

                            /**
                            $signed_document_key = "";
                            $api_key = sfConfig::get('app_echo_sign_key');


                            if($api_key)
                            {

                              $ESLoader = new SplClassLoader('EchoSign', dirname(__FILE__).'/../../../../../lib/vendor');
                              $ESLoader->register();

                              $client = new SoapClient(EchoSign\API::getWSDL());
                              $api = new EchoSign\API($client, $api_key);

                              $document_key = $permit->getDocumentKey();
                              $signed_document_key = "";

                              if($document_key)
                              {
                                try{
                                    $result = $api->getFormData($document_key);
                                }catch(Exception $e){
                                    print '<h3>An exception occurred:</h3>';
                                    var_dump($e);
                                }

                                $child_key = explode(",",$result->getFormDataResult->formDataCsv);
                                $signed_document_key = str_replace('"','',$child_key[14]);
                              }

                            }
                            **/

                        ?>

                        <div class="text-right btn-invoice" style="padding-right: 10px;">
                            <?php
                            /**
                            if(empty($signed_document_key) && $sf_user->mfHasCredential("sign_permits"))
                            {
                            ?>
                            <button class="btn btn-white" id="printinvoice" type="button" onClick="window.location='/backend.php/permits/sign/id/<?php echo $permit->getId(); ?>';"><i class="fa fa-print mr5"></i> Sign Permit</button>
                            <?php
                            }
                            ?>
                            <?php
                            if($signed_document_key)
                            {
                              ?>
                              <button class="btn btn-white" id="printinvoice" type="button" onClick="window.location='/backend.php/permits/viewsigned/id/<?php echo $permit->getId(); ?>';"><i class="fa fa-print mr5"></i> View Signed Permit</button>
                              <?php
                            }
                            else
                            {
                              if($document_key)
                              {
                                ?>
                                <button class="btn btn-white" id="printinvoice" type="button" disabled="disabled"><i class="fa fa-print mr5"></i>Signature Is Awaiting Confirmation</button>
                                <?php
                              }
                            }
                            ?>
                            <?php
                            **/
                            if(!empty($document_key))
                            {
                            ?>
                            <button class="btn btn-white" id="printinvoice" type="button" onClick="window.location='/<?php echo $document_key; ?>';"><i class="fa fa-print mr5"></i> Download Service</button>
                            <?php
                            }
                            else
                            {
                              ?>
                              <button class="btn btn-white" id="printinvoice" type="button" onClick="window.location='/backend.php/applications/viewpermit/id/<?php echo $permit->getId(); ?>';"><i class="fa fa-print mr5"></i> Print Service</button>
                              <?php
                            }

                            if($sf_user->mfHasCredential('cancel_permits') && $permit->getPermitStatus() != "3")
                            {
                            ?>
                            <button class="btn btn-white" id="cancelpermit" type="button" onClick="window.location='/backend.php/permits/cancelpermit/id/<?php echo $permit->getId(); ?>';"><i class="fa fa-print mr5"></i> Cancel Permit</button>
                            <?php
                            }

                            if($sf_user->mfHasCredential('cancel_permits') && $permit->getPermitStatus() == "3")
                            {
                            ?>
                            <button class="btn btn-white" id="cancelpermit" type="button" onClick="window.location='/backend.php/permits/uncancelpermit/id/<?php echo $permit->getId(); ?>';"><i class="fa fa-print mr5"></i> UnCancel Permit</button>
                            <?php
                            }

                            //If permit template has a remote url, add a button for update remote database
                            $q = Doctrine_Query::create()
                                ->from("Permits a")
                                ->where("a.id = ?", $permit->getTypeId())
                                ->andWhere("a.remote_url <> ?", "");
                            $permit_template = $q->fetchOne();

                            if($permit_template)
                            {
                                ?>
                                <button class="btn btn-white" id="cancelpermit" type="button" onClick="window.location='/backend.php/permits/updatesingleremote/id/<?php echo $permit->getId(); ?>';"><i class="fa fa-print mr5"></i> Update Remote Database</button>
                                <?php
                            }
                            ?>
                        </div>
                    </div>



</div>

</div><!-- /.row -->



</div><!-- /.marketing -->
