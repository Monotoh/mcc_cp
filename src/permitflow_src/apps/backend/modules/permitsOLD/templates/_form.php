<div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
  <strong><?php echo __('Well done'); ?>!</strong> <?php echo __('You successfully updated this record'); ?></a>.
</div>

<form id="permitform" class="form-bordered" action="/backend.php/<?php echo 'permits/'.($form->getObject()->isNew() ? 'create' : 'update'.(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post"  autocomplete="off">
<div class="panel-body panel-body-nopadding">
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
<?php echo $form->renderHiddenFields(false) ?>
<?php echo $form->renderGlobalErrors() ?>

      	<div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Title'); ?></i></label><br>
        <div class="col-sm-12 rogue-input">
          <?php echo $form['title']->renderError() ?>
          <?php echo $form['title'] ?>
        </div>
      </div>

	  <div id="nameresult" name="nameresult"></div>

      <script language="javascript">
        $('document').ready(function(){
          $('#permits_title').keyup(function(){
            $.ajax({
                      type: "POST",
                      url: "/backend.php/permits/checkname",
                      data: {
                          'name' : $('input:text[id=permits_title]').val()
                      },
                      dataType: "text",
                      success: function(msg){
                            //Receiving the result of search here
                            $("#nameresult").html(msg);
                      }
                  });
              });
        });
      </script>
      <div class="form-group">
        <label class="col-sm-12"><i class="bold-label"><?php echo __('Permit No Identification'); ?></i></label><br>
        <div class="col-sm-12">
          <?php echo $form['footer']->renderError() ?>
          <input type="text" class="form-control" name="permits[footer]" id="permits_footer" value="<?php if(!$form->getObject()->isNew()){ echo $form->getObject()->getFooter(); }else{ echo "0"; } ?>">
        </div>
      </div>
        <div class="form-group">
            <label class="col-sm-4"><i class="bold-label"><?php echo __('Permit Type'); ?></i></label><br>
            <div class="col-sm-12">
                <?php echo $form['parttype']->renderError() ?>
                <select class="form-control" name="permits[parttype]" id="permits_parttype">
                    <option value="1" <?php if(!$form->getObject()->isNew()){ if($form->getObject()->getParttype() == 1){ echo "selected='selected'"; } } ?>>Service for Clients and Reviewers</option>
                    <option value="3" <?php if(!$form->getObject()->isNew()){ if($form->getObject()->getParttype() == 3){ echo "selected='selected'"; } } ?>>Service for Reviewers Only</option>
                    <option value="2" <?php if(!$form->getObject()->isNew()){ if($form->getObject()->getParttype() == 2){ echo "selected='selected'"; } } ?>>PDF for Client to Download and Attach</option>
                </select>
            </div>
        </div>

    <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Page Type'); ?></i></label><br>
        <div class="col-sm-12">
            <?php echo $form['page_type']->renderError() ?>
            <select class="form-control" name="permits[page_type]" id="permits_page_type">
                <option value="A4" <?php if(!$form->getObject()->isNew()){ if($form->getObject()->getPageType() == "A4"){ echo "selected='selected'"; } } ?>>A4</option>
                <option value="A5" <?php if(!$form->getObject()->isNew()){ if($form->getObject()->getPageType() == "A5"){ echo "selected='selected'"; } } ?>>A5</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Page Orientation'); ?></i></label><br>
        <div class="col-sm-12">
            <?php echo $form['page_orientation']->renderError() ?>
            <select class="form-control" name="permits[page_orientation]" id="permits_page_orientation">
                <option value="potrait" <?php if(!$form->getObject()->isNew()){ if($form->getObject()->getPageOrientation() == "potrait"){ echo "selected='selected'"; } } ?>>Potrait</option>
                <option value="landscape" <?php if(!$form->getObject()->isNew()){ if($form->getObject()->getPageOrientation() == "landscape"){ echo "selected='selected'"; } } ?>>Landscape</option>
            </select>
        </div>
    </div>

      <div class="form-group">
      	<label class="col-sm-4"><i class="bold-label"><?php echo __('Application Form'); ?></i></label><br>
        <div class="col-sm-12">
          <?php echo $form['applicationform']->renderError() ?>
          <select class="form-control" name="permits[applicationform]" id="permits_applicationform"  onChange="ajaxFetchPermitsettings(this.value);">
      			<option value=""></option>
      			<?php
            $q = Doctrine_Query::create()
               ->from("SubMenus a") ;
               //OTB patch - Check if we should show all submenus to allow user configure duplicated permits
            if($sf_user->getAttribute('filter') != 'all'){
                $q->where("a.menu_id = ?", $sf_user->getAttribute('filter')) ;
            }
            $stages = $q->execute();

            $stages_array = array();

            foreach($stages as $stage)
            {
                $stages_array[] = "a.form_stage = ".$stage->getId();
            }

            $stages_query = implode(" OR ", $stages_array);

      			$q = Doctrine_Query::create()
                  ->from('ApForms a')
				  	      ->where('a.form_id <> 15 AND a.form_id <> 16 AND a.form_id <> 17 AND  a.form_id <> 6 AND a.form_id <> 7')
                	->andWhere('a.form_active = 1 AND a.form_type = 1')
                  ->andWhere($stages_query)
            		  ->orderBy('a.form_name ASC');
              $applicationforms = $q->execute();
              foreach($applicationforms as $appform)
              {/**
                                        * OTB patch
                                        * Add description message to differentiate forms with similar name i.e. the OSC forms
                                        */
      				  if($form->getObject()->isNew())
      				  {
      					echo "<option value='".$appform->getFormId()."'>".$appform->getFormName()." (".$appform->getFormDescription().") </option>";
      				  }
      				  else
      				  {
                                      
      				    if($form->getObject()->getApplicationform() == $appform->getFormId())
      					{
                                                
      						echo "<option value='".$appform->getFormId()."' selected>".$appform->getFormName()." (".$appform->getFormDescription().") </option>"; 
      					}
      					else
      					{
      						echo "<option value='".$appform->getFormId()."'>".$appform->getFormName()." (".$appform->getFormDescription().") </option>";
      					}
      				  }
      			  }
      			?>
      		</select>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Stage at which this permit is generated'); ?></i></label><br>
        <div class="col-sm-12">
          <?php echo $form['applicationstage']->renderError() ?>
          <select class="form-control" name="permits[applicationstage]" id="permits_applicationstage">
            <option value=""></option>
            <?php
                 $q = Doctrine_Query::create()
                        ->from('Menus a')
                        
                        ->orderBy('a.order_no ASC');
                 //OTB patch - show all stages
                    if($sf_user->getAttribute('filter') != 'all'){
                        $q->where("a.id = ?", $sf_user->getAttribute('filter')) ;
                    }
                      $stagegroups = $q->execute();
                      foreach($stagegroups as $stagegroup)
                      {
                        echo "<optgroup label='".$stagegroup->getTitle()."'>";
                        $q = Doctrine_Query::create()
                          ->from('SubMenus a')
                          ->where('a.menu_id = ?', $stagegroup->getId())
                          ->andWhere('a.deleted = 0')
                          ->orderBy('a.order_no ASC');
                        $stages = $q->execute();

                        foreach($stages as $stage)
                        {
                          $selected = "";

                          if(!$form->getObject()->isNew()){
                            if($form->getObject()->getApplicationstage() == $stage->getId()){
                            	$selected = "selected='selected'";
                            }
                          }

                          echo "<option value='".$stage->getId()."' ".$selected.">".$stage->getTitle()."</option>";
                        }
                        echo "</optgroup>";
                      }
            ?>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-12"><i class="bold-label"><?php echo __('Maximum number of days before expiration'); ?></i></label><br>
        <div class="col-sm-12">
          <?php echo $form['max_duration']->renderError() ?>
          <input type="text" class="form-control" name="permits[max_duration]" id="permits_max_duration" value="<?php if(!$form->getObject()->isNew()){ echo $form->getObject()->getMaxDuration(); }else{ echo "0"; } ?>">
        </div>
      </div>
		<!--OTB Start - Option for authorized user to set max duration for each permit instance-->
        <div class="form-group">
            <label class="col-sm-12"><i class="bold-label"><?php echo __('Allow authorized users to override days before expiration, by setting duration period of permit?'); ?></i></label><br>
            <div class="col-sm-12">
                <?php echo $form['user_set_duration']->renderError() ?>
                <select class="form-control" name="permits[user_set_duration]" id="permits_use_application_number">
                    <?php
                    $selected = "";
                    if(!$form->getObject()->isNew())
                    {
                        if($form->getObject()->getUserSetDuration() == 0)
                        {
                            $selected = "selected='selected'";
                        }
                    }
                    ?>
                    <option value="0" <?php echo $selected; ?>>No</option>
                    <?php
                    $selected = "";
                    if(!$form->getObject()->isNew())
                    {
                        if($form->getObject()->getUserSetDuration() == 1)
                        {
                            $selected = "selected='selected'";
                        }
                    }
                    ?>
                    <option value="1" <?php echo $selected; ?>>Yes</option>
                </select>
            </div>
        </div>
      <div class="form-group">
        <label class="col-sm-4"><i class="bold-label"><?php echo __('Stage at which validity duration/period of this permit is set'); ?></i></label><br>
        <div class="col-sm-12">
          <?php echo $form['stage_set_duration']->renderError() ?>
          <select class="form-control" name="permits[stage_set_duration]" id="permits_stage_set_duration">
            <option value=""></option>
            <?php
                 $q = Doctrine_Query::create()
                        ->from('Menus a')
                        ->where("a.id = ?", $sf_user->getAttribute('filter'))
                        ->orderBy('a.order_no ASC');
                      $stagegroups = $q->execute();
                      foreach($stagegroups as $stagegroup)
                      {
                        echo "<optgroup label='".$stagegroup->getTitle()."'>";
                        $q = Doctrine_Query::create()
                          ->from('SubMenus a')
                          ->where('a.menu_id = ?', $stagegroup->getId())
                          ->andWhere('a.deleted = 0')
                          ->orderBy('a.order_no ASC');
                        $stages = $q->execute();

                        foreach($stages as $stage)
                        {
                          $selected = "";

                          if(!$form->getObject()->isNew()){
                            if($form->getObject()->getStageSetDuration() == $stage->getId()){
                            	$selected = "selected='selected'";
                            }
                          }

                          echo "<option value='".$stage->getId()."' ".$selected.">".$stage->getTitle()."</option>";
                        }
                        echo "</optgroup>";
                      }
            ?>
          </select>
        </div>
      </div>          
		<!--OTB End - Option for authorized user to set max duration for each permit instance-->
      	<div class="form-group">
      	<label class="col-sm-4"><i class="bold-label"><?php echo __('Content'); ?></i></label><br>
        <div class="col-sm-12">
          <?php echo $form['content']->renderError() ?>
		  <?php
		  if($form->getObject()->isNew())
		  {
		  ?>
          <textarea class="form-control" rows="30" name="permits[content]" id="permits_content" ></textarea>
		  <?php
		  }
		  else
		  {
		  ?>
          <textarea class="form-control" rows="30" name="permits[content]" id="permits_content" ><?php echo $form->getObject()->getContent(); ?></textarea>
		  <?php
		  }
		  ?>
      <?php
      if(sfConfig::get('app_disable_ckeditor') != "true")
      {
      ?>
        <script src="<?php echo public_path(); ?>assets_unified/js/ckeditor/ckeditor.js"></script>
        <script src="<?php echo public_path(); ?>assets_unified/js/ckeditor/adapters/jquery.js"></script>

        <script>
        jQuery(document).ready(function(){

          // CKEditor
          jQuery('#permits_content').ckeditor();

        });
        </script>
        <?php
        }
        ?>
        </div>
      </div>
      <div class="form-group">
        <h4 style="padding-left: 10px;">Remote Updates</h4>
      </div>
      <div class="form-group">
        <label class="col-sm-12"><i class="bold-label"><?php echo __('Remote URL (Update a remote database with the data from this permit)'); ?></i></label><br>
        <div class="col-sm-12">
          <?php echo $form['remote_url']->renderError() ?>
          <input type="text" class="form-control" name="permits[remote_url]" id="permits_remote_url" value='<?php if(!$form->getObject()->isNew()){ echo $form->getObject()->getRemoteUrl(); }else{ echo ""; } ?>'>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-12"><i class="bold-label"><?php echo __('Remote Post Data (Actual data to be posted remotely)'); ?></i></label><br>
          <div class="col-sm-12">
          <?php echo $form['remote_field']->renderError() ?>
          <textarea class="form-control" name="permits[remote_field]" id="permits_remote_field"><?php if(!$form->getObject()->isNew()){ echo $form->getObject()->getRemoteField(); }else{ echo ""; } ?></textarea>
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-12"><i class="bold-label"><?php echo __('Remote Username (Username if the remote url requires authentication)'); ?></i></label><br>
        <div class="col-sm-12">
          <?php echo $form['remote_username']->renderError() ?>
          <input type="text" class="form-control" name="permits[remote_username]" id="permits_remote_username" value="<?php if(!$form->getObject()->isNew()){ echo $form->getObject()->getRemoteUsername(); }else{ echo ""; } ?>">
        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-12"><i class="bold-label"><?php echo __('Remote Password (Password if the remote url requires authentication)'); ?></i></label><br>
        <div class="col-sm-12">
          <?php echo $form['remote_password']->renderError() ?>
          <input type="password" class="form-control" name="permits[remote_password]" id="permits_remote_password" value="<?php if(!$form->getObject()->isNew()){ echo $form->getObject()->getRemotePassword(); }else{ echo ""; } ?>">
        </div>
      </div>

        <div class="form-group">
            <label class="col-sm-12"><i class="bold-label"><?php echo __('Remote Request Type'); ?></i></label><br>
            <div class="col-sm-12">
                <?php echo $form['remote_request_type']->renderError() ?>
                <select class="form-control" name="permits[remote_request_type]" id="permits_remote_request_type">
                    <option value='post' <?php if(!$form->getObject()->isNew()){ if($form->getObject()->getRemoteRequestType() == 'post'){ echo "selected='selected'"; } } ?>>POST (JSON)</option>
                    <option value='get' <?php if(!$form->getObject()->isNew()){ if($form->getObject()->getRemoteRequestType() == 'get'){ echo "selected='selected'"; } } ?>>GET (JSON)</option>
                    <option value='xmlpost' <?php if(!$form->getObject()->isNew()){ if($form->getObject()->getRemoteRequestType() == 'xmlpost'){ echo "selected='selected'"; } } ?>>POST (XML)</option>
                    <option value='xmlget' <?php if(!$form->getObject()->isNew()){ if($form->getObject()->getRemoteRequestType() == 'xmlget'){ echo "selected='selected'"; } } ?>>GET (XML)</option>
                </select>
            </div>
        </div>

      <div class="form-group">
		<div class="col-sm-12 alignright">
		  <button type="button" class="btn btn-primary" data-target="#fieldsModal" data-toggle="modal">View available user/form fields</button>
		</div>
	  </div>
		  <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="fieldsModal" class="modal fade" style="display: none;">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
			        <h4 id="myModalLabel" class="modal-title">View available user/form fields</h4>
			      </div>
			      <div class="modal-body">
			        <div class="form-group">
		<?php
			//Get User Information (anything starting with sf_ )
					   //sf_email, sf_fullname, sf_username, ... other fields in the dynamic user profile form e.g sf_element_1
			?>
			<table class="table dt-on-steroids mb0">
			<thead><tr><th width="50%"><?php echo __('User Details'); ?></th><th><?php echo __('Tag'); ?></th></tr></thead>
			<tbody>
			<tr><td><?php echo __('User ID'); ?></td><td>{sf_username}</td></tr>
            <tr><td><?php echo __('Mobile Number'); ?></td><td>{sf_mobile}</td></tr>
			<tr><td><?php echo __('Email'); ?></td><td>{sf_email}</td></tr>
			<tr><td><?php echo __('Full Name'); ?></td><td>{sf_fullname}</td></tr>
			<tr><td><?php echo __('User Category'); ?></td><td>{sf_user_category}</td></tr>
			<?php
					$q = Doctrine_Query::create()
					   ->from('apFormElements a')
					   ->where('a.form_id = ?', 15)
             ->andWhere('a.element_status = ?', 1)
             ->orderBy('a.element_position ASC');

					$elements = $q->execute();

					foreach($elements as $element)
					{
						$childs = $element->getElementTotalChild();
						if($childs == 0)
						{
						   echo "<tr><td>".$element->getElementTitle()."</td><td>{sf_element_".$element->getElementId()."}</td></tr>";
						}
						else
						{
							if($element->getElementType() == "select")
							{
								echo "<tr><td>".$element->getElementTitle()."</td><td>{sf_element_".$element->getElementId()."}</td></tr>";
							}
							else
							{
								for($x = 0; $x < ($childs + 1); $x++)
								{
									echo "<tr><td>".$element->getElementTitle()."</td><td>{sf_element_".$element->getElementId()."_".($x+1)."}</td></tr>";
								}
							}
						}
					}
			?>
			</tbody>
			</table>


			<table class="table dt-on-steroids mb0">
			<thead><tr><th width="50%"><?php echo __('Application Details'); ?></th><th><?php echo __('Tag'); ?></th></tr></thead>
			<tbody>
			     <tr><td><?php echo __('User Profile Picture'); ?></td><td>{profile_pic}</td></tr>
            <tr><td><?php echo __('Bar Code'); ?></td><td>{bar_code}</td></tr>
            <tr><td><?php echo __('Bar Code (Small)'); ?></td><td>{bar_code_small}</td></tr>
			<tr><td><?php echo __('Application Number'); ?></td><td>{ap_application_id}</td></tr>
			<tr><td><?php echo __('Created At'); ?></td><td>{fm_created_at}</td></tr>
			<tr><td><?php echo __('Approved At'); ?></td><td>{fm_updated_at}</td></tr>
      <tr><td><?php echo __('Permit ID'); ?></td><td>{ap_permit_id}</td></tr>
			<tr><td><?php echo __('Permit Issued At'); ?></td><td>{ap_issue_date}</td></tr>
      		<tr><td><?php echo __('Permit Expires At'); ?></td><td>{ap_expire_date}</td></tr>
          <tr><td><?php echo __('Permit UUID'); ?></td><td>{uuid}</td></tr>
			<?php
			//Get Application Information (anything starting with ap_ )
					   //ap_application_id

			//Get Form Details (anything starting with fm_ )
					   //fm_created_at, fm_updated_at.....fm_element_1
			?>
		 <?php
		  if(!$form->getObject()->isNew())
		  {
		    $appform = $form->getObject()->getApplicationform();
		  ?>
			<?php

					$q = Doctrine_Query::create()
					   ->from('apFormElements a')
					   ->where('a.form_id = ?', $appform)
             ->andWhere('a.element_status = ?', 1)
             ->orderBy('a.element_position ASC');

					$elements = $q->execute();

					foreach($elements as $element)
					{
						$childs = $element->getElementTotalChild();
						if($childs == 0)
						{
               if($element->getElementType() == "select")
               {
                 if($element->getElementExistingForm() && $element->getElementExistingStage())
                 {
                   $q = Doctrine_Query::create()
                    ->from("ApForms a")
                    ->where("a.form_id = ?", $element->getElementExistingForm());
                   $child_form = $q->fetchOne();

                   echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."} ";
                     echo '<table class="table dt-on-steroids mb0">
                     <thead><tr><th width="50%">'.__($child_form->getFormName().' Details').'</th><th>'.__('Tag').'</th></tr></thead>
                     <tbody>';

                      ?>
                      <tr><td><?php echo __('Application Number'); ?></td><td>{ap_child_application_id}</td></tr>
                      <tr><td><?php echo __('Created At'); ?></td><td>{fm_child_created_at}</td></tr>
                      <tr><td><?php echo __('Approved At'); ?></td><td>{fm_child_updated_at}</td></tr>
                      <?php
                        $q = Doctrine_Query::create()
                           ->from("Permits a")
                           ->where("a.applicationform = ?", $element->getElementExistingForm());
                        $permits = $q->execute();

                        foreach($permits as $permit)
                        {
                            echo "<tr><td>".$permit->getTitle()." ID</td><td>{ap_permit_id_".$permit->getId()."_element_child}</td></tr>";
                        }

                      $q = Doctrine_Query::create()
                         ->from('apFormElements a')
                         ->where('a.form_id = ?', $element->getElementExistingForm())
                         ->andWhere('a.element_status = ?', 1)
                         ->orderBy('a.element_position ASC');

                      $child_elements = $q->execute();

                      foreach($child_elements as $child_element)
                      {

                          //START CHILD ELEMENTS
                          $childs = $child_element->getElementTotalChild();
                          if($childs == 0)
                          {
                             if($child_element->getElementType() == "select")
                             {
                               if($child_element->getElementExistingForm() && $child_element->getElementExistingStage())
                               {

                                 $q = Doctrine_Query::create()
                                  ->from("ApForms a")
                                  ->where("a.form_id = ?", $child_element->getElementExistingForm());
                                 $grand_form = $q->fetchOne();

                                   echo "<tr><td>".$child_element->getElementTitle()."</td><td>{fm_child_element_".$child_element->getElementId()."} ";
                                   echo '<table class="table dt-on-steroids mb0">
                                   <thead><tr><th width="50%">'.__($grand_form->getFormName().' Details').'</th><th>'.__('Tag').'</th></tr></thead>
                                   <tbody>';

                                    $q = Doctrine_Query::create()
                                       ->from('apFormElements a')
                                       ->where('a.form_id = ?', $child_element->getElementExistingForm())
                                       ->andWhere('a.element_status = ?', 1)
                                       ->orderBy('a.element_position ASC');

                                    $grand_child_elements = $q->execute();

                                    foreach($grand_child_elements as $grand_child_element)
                                    {
                                      ?>
                                      <tr><td><?php echo __('Application Number'); ?></td><td>{ap_grand_child_application_id}</td></tr>
                                      <tr><td><?php echo __('Created At'); ?></td><td>{fm_grand_child_created_at}</td></tr>
                                      <tr><td><?php echo __('Approved At'); ?></td><td>{fm_grand_child_updated_at}</td></tr>
                                      <?php
                                       $q = Doctrine_Query::create()
                                          ->from("Permits a")
                                          ->where("a.applicationform = ?", $child_element->getElementExistingForm());
                                       $permits = $q->execute();

                                       foreach($permits as $permit)
                                       {
                                           echo "<tr><td>".$permit->getTitle()." ID</td><td>{ap_permit_id_".$permit->getId()."_element_grand_child}</td></tr>";
                                       }

                                        //START GRAND CHILD ELEMENTS
                                        $childs = $grand_child_element->getElementTotalChild();
                                        if($childs == 0)
                                        {
                                           if($grand_child_element->getElementType() == "select")
                                           {
                                               echo "<tr><td>".$grand_child_element->getElementTitle()."</td><td>{fm_grand_child_element_".$grand_child_element->getElementId()."}</td></tr>";
                                           }
                                           else
                                           {
                                              echo "<tr><td>".$grand_child_element->getElementTitle()."</td><td>{fm_grand_child_element_".$grand_child_element->getElementId()."}</td></tr>";
                                           }
                                        }
                                        else
                                        {
                                            for($x = 0; $x < ($childs + 1); $x++)
                                            {
                                              echo "<tr><td>".$grand_child_element->getElementTitle()."</td><td>{fm_grand_child_element_".$grand_child_element->getElementId()."_".($x+1)."}</td></tr>";
                                            }
                                        }
                                        //END GRAND CHILD ELEMENTS
                                    }

                                   echo '</tbody></table>';
                                 echo "</td></tr>";
                               }
                               else
                               {
                                 echo "<tr><td>".$child_element->getElementTitle()."</td><td>{fm_child_element_".$child_element->getElementId()."}</td></tr>";
                               }
                             }
                             else
                             {
                                echo "<tr><td>".$child_element->getElementTitle()."</td><td>{fm_child_element_".$child_element->getElementId()."}</td></tr>";
                             }
                          }
                          else
                          {
                              for($x = 0; $x < ($childs + 1); $x++)
                              {
                                echo "<tr><td>".$child_element->getElementTitle()."</td><td>{fm_child_element_".$child_element->getElementId()."_".($x+1)."}</td></tr>";
                              }
                          }
                          //END CHILD ELEMENTS
                      }

                     echo '</tbody></table>';
                   echo "</td></tr>";
                 }
                 else
                 {
                   echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."}</td></tr>";
                 }
               }
               else
               {
                  echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."}</td></tr>";
               }
            }
						else
						{
								for($x = 0; $x < ($childs + 1); $x++)
								{
									echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."_".($x+1)."}</td></tr>";
								}
						}
					}

          ?>
          <tr><td><strong>Comment Sheets</strong></td><td><strong>Tag</strong></td></tr>
          <?php

          //Comment Sheets
          $q = Doctrine_Query::create()
             ->from('SubMenus a')
             ->where('a.id = ?', $form->getObject()->getApplicationstage());
          $stage = $q->fetchOne();

          if($stage)
          {

		  //OTB Patch Start - Show comment sheet tags for the whole workflow
          $q = Doctrine_Query::create()
             ->from('Menus a')
             ->where('a.category_id = ?', $stage->getMenus()->getCategoryId());
          $workflows = $q->execute();
			$workflow_ids = array();
			foreach($workflows as $workflow){
				array_push($workflow_ids, $workflow->getId());
			}
		  //OTB Patch End - Show comment sheet tags for the whole workflow
		  
          $q = Doctrine_Query::create()
            ->from('SubMenus a')
            //->where('a.menu_id = ?', $stage->getMenuId())
            ->whereIn('a.menu_id', $workflow_ids)//OTB Patch - Show comment sheet tags for the whole workflow
            ->orderBy('a.order_no ASC');
          $stages = $q->execute();

          $filstages = "";

          $filtags = "";

          $count = 0;

          foreach($stages as $stage)
          {
              $filstages[] = $stage->getId();
              if($count == 0)
              {
                $filtags = $filtags."a.form_department_stage = ? ";
              }
              else
              {
                $filtags = $filtags."OR a.form_department_stage = ? ";
              }
            $count++;
          }

          $q = Doctrine_Query::create()
             ->from("ApForms a")
             ->where($filtags, $filstages);
          $forms = $q->execute();

          foreach($forms as $apform)
          {
            echo "<tr><td><strong>".$apform->getFormName()." details</strong></td><td><strong>Tag</strong></td></tr>";
             $q = Doctrine_Query::create()
                ->from('apFormElements a')
                ->where('a.form_id = ?', $apform->getFormId())
                ->andWhere('a.element_status = ?', 1)
                ->orderBy('a.element_position ASC');

             $elements = $q->execute();

             foreach($elements as $element)
             {
               $childs = $element->getElementTotalChild();
               if($childs == 0)
               {
                  if($element->getElementType() == "select")
                  {
                      echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_c".$apform->getFormId()."_element_".$element->getElementId()."}</td></tr>";
                  }
                  else
                  {
                     echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_c".$apform->getFormId()."_element_".$element->getElementId()."}</td></tr>";
                  }
               }
               else
               {
                   for($x = 0; $x < ($childs + 1); $x++)
                   {
                     echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_c".$apform->getFormId()."_element_".$element->getElementId()."_".($x+1)."}</td></tr>";
                   }
               }
             }
          }
        }

			?>
		  <?php
		  }
		  ?>
			</tbody>
			</table>
            <table class="table dt-on-steroids mb0">
                <thead><tr><th width="50%"><?php echo __('Invoice Details'); ?></th><th><?php echo __('Tag'); ?></th></tr></thead>
                <tbody>
                <tr><td><?php echo __('Invoice No'); ?></td><td>{inv_no}</td></tr>
                <tr><td><?php echo __('Invoice Date'); ?></td><td>{inv_date_created}</td></tr>
                <tr><td><?php echo __('Invoice Expiry Date'); ?></td><td>{inv_expires_at}</td></tr>
                <tr><td><?php echo __('List of Fees'); ?></td><td>{inv_fee_table}</td></tr>
                <tr><td><?php echo __('Total'); ?></td><td>{inv_total}</td></tr>
                <tr><td><?php echo __('Payment Reference Number'); ?></td><td>{inv_payment_id}</td></tr>
                <tr><td><?php echo __('Payment Transaction Id'); ?></td><td>{inv_transaction_id}</td></tr>
                </tbody>
            </table>
			<table class="table dt-on-steroids mb0">
			<thead><tr><th width="50%"><?php echo __('Conditions Of Approval'); ?></th><th><?php echo __('Tag'); ?></th></tr></thead>
			<tbody>
			<tr><td><?php echo __('Conditions Of Approval'); ?></td><td>{ca_conditions}</td></tr>
			</tbody>
			</table>
		</div>
	      <div class="modal-footer">
	        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
	      </div>
	    </div><!-- modal-content -->
	  </div><!-- modal-dialog -->
	</div>
	</div>
	<div class="form-group">
	        <div class="col-sm-12" id="loadinner" name="loadinner">

	        </div>
	        <div class="col-sm-12" id="loadpchecker" name="loadpchecker">

	        </div>

	        <?php
	          $permitid = 0;
			  if(!$form->getObject()->isNew())
			  {
			  		$permitid = $form->getObject()->getId();
			  }
	        ?>
			<script language="javascript">
			 jQuery(document).ready(function(){
		        $("#loadinner").load("<?php echo public_path(); ?>backend.php/conditionsmng/index/filter/<?php echo $permitid; ?>");
		        $("#loadpchecker").load("<?php echo public_path(); ?>backend.php/permitchecker/index/filter/<?php echo $permitid; ?>");
		     });
			 </script>
	      </div>
		</div>
		<a name="end"></a>


<div class="panel-footer">
            <button class='btn btn-danger mr10'><?php echo __('Reset'); ?></button><button type="submit" class='btn btn-primary' name="submitbuttonname" id="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button></div>
</div>
</div>
</form>
