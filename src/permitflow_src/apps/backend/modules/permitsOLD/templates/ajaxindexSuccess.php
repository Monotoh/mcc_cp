<?php
use_helper("I18N");

$audit = new Audit();
$audit->saveAudit("", "Accessed permit template settings");
?>
<div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
  <strong><?php echo __('Well done'); ?>!</strong> <?php echo __('You successfully updated this record'); ?></a>.
</div>

<?php
if($sf_user->mfHasCredential("managepermits"))
{
  $_SESSION['current_module'] = "permits";
  $_SESSION['current_action'] = "index";
  $_SESSION['current_id'] = "";
?>
<div class="panel panel-dark">
<div class="panel-heading">
			<h3 class="panel-title"><?php echo __('Permits'); ?></h3>
				<div class="pull-right">
            <a class="btn btn-primary-alt settings-margin42" id="newpermit" href="#"><?php echo __('New Permit'); ?></a>

            <script language="javascript">
            jQuery(document).ready(function(){
              $( "#newpermit" ).click(function() {
                  $("#contentload").load("<?php echo public_path(); ?>backend.php/permits/ajaxnew");
              });
            });
            </script>
</div>
</div>


<div class="panel panel-body panel-body-nopadding ">

<div class="table-responsive">
<table class="table dt-on-steroids mb0" id="table3">
  <thead>
    <tr>
      <th class="no-sort"><input type='checkbox' name='batchall' onclick="boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.name == 'batch') { if(this.checked == true){ box.checked = true; }else{ box.checked = false; } } } "></th>
      <th width="60">#</th>
      <th class="no-sort"><?php echo __('Title'); ?></th>
      <th class="no-sort"><?php echo __('Application Form'); ?></th>
      <th class="no-sort"><?php echo __('Application Stage'); ?></th>
      <th class="no-sort" width="7%"><?php echo __('Actions'); ?></th>
    </tr>
  </thead>
  <tbody>
    <?php
		$count = 1;
	?>
    <?php foreach ($permitss as $permits): ?>
    <tr id="row_<?php echo $permits->getId() ?>">
      <td><input type='checkbox' name='batch' id='batch_<?php echo $permits->getId() ?>' value='<?php echo $permits->getId() ?>'></td>

      <td><?php echo $count++; ?></td>
      <td><?php echo $permits->getTitle() ?></td>
      <td><?php
	    $q = Doctrine_Query::create()
		  ->from('ApForms a')
		  ->where('a.form_id = ?', $permits->getApplicationform());
		$form = $q->fetchOne();
    if($form)
    {
  		echo $form->getFormName();
    }
	  ?></td>

    <td>
      <?php
        $q = Doctrine_Query::create()
          ->from('SubMenus a')
          ->where('a.id = ?', $permits->getApplicationstage());
        $stage = $q->fetchOne();
        if($stage)
        {
          echo $stage->getTitle();
        }
      ?>
    </td>

        <td>
            <a id="editpermit<?php echo $permits->getId(); ?>" href="#" title="<?php echo __('Edit'); ?>"><span class="badge badge-primary"><i class="fa fa-pencil"></i></span></a>

            <a id="deletepermit<?php echo $permits->getId(); ?>" href="#" title="<?php echo __('Delete'); ?>"><span class="badge badge-primary"><i class="fa fa-trash-o"></i></span></a>


            <script language="javascript">
            jQuery(document).ready(function(){
              $( "#editpermit<?php echo $permits->getId(); ?>" ).click(function() {
                  $("#contentload").load("<?php echo public_path(); ?>backend.php/permits/ajaxedit/id/<?php echo $permits->getId(); ?>");
              });
              $( "#deletepermit<?php echo $permits->getId(); ?>" ).click(function() {
                  if(confirm('Are you sure you want to delete this permit?')){
                    $("#contentload").load("<?php echo public_path(); ?>backend.php/permits/ajaxdelete/id/<?php echo $permits->getId(); ?>");
                  }
                  else
                  {
                    return false;
                  }
              });
            });
            </script>
        </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
        <tfoot>
        <tr><td colspan='7' style='text-align: left;'>
            <select id='batch_action' name='batch_action' onChange="if(this.value != ''){if(confirm('Are you sure?')){ batch('permits', this.options[this.selectedIndex].text, this.value); document.getElementById('default').selected='selected'; }}">
                <option id='default' value=''><?php echo __('Choose an action'); ?>..</option>
                <option value='delete'><?php echo __('Set As Deleted'); ?></option>
            </select>
        </td></tr>
        </tfoot>
</table>
</div>
 <script language="javascript">
  jQuery(document).ready(function(){
    jQuery('#table3').dataTable({
        "sPaginationType": "full_numbers",

        // Using aoColumnDefs
        "aoColumnDefs": [
        	{ "bSortable": false, "aTargets": [ 'no-sort' ] }
      	]
      });
  });
</script>
<?php
}
else
{
  include_partial("accessdenied");
}
?>
