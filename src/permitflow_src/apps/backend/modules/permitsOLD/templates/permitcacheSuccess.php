<?php
use_helper("I18N");
?>
<div class="pageheader">
  <h2><i class="fa fa-envelope"></i> <?php echo __('Approval Details'); ?></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="/backend.php">Home</a></li>
      <li><a href="/backend.php/applications/list/get/your">Applications</a></li>
    </ol>
  </div>
</div>
<br/>
<div class="col-sm-10">
	<div class="panel panel-dark">
		<div class="panel-heading">
			<h4 class="panel-title"><?php echo __('Approval Details'); ?></h4>
		</div>
	   <div class="panel-body panel-body-nopadding">
	   <br/>
		<form class="form" action="" method="post" autocomplete="off">
		<input type="hidden" name="application_id" value="<?php echo $application_id; ?>" class="form-control" id="application_id">
		<input type="hidden" name="template_id" value="<?php echo $template_id; ?>"  class="form-control" id="template_id">
		<input type="hidden" name="moveto" value="<?php echo $moveto; ?>"  class="form-control" id="moveto">
			<div class="form-group">
				<label class="col-sm-2"><?php echo __('Approved Permit will be valid for'); ?></label>
				<div class="col-sm-4">
					<input type="text" name="validity_period"  class="form-control" id="validity_period">
				</div>
				<div class="col-sm-4">
					<select name="period_uom" id="period_uom" class="form-control">
					<option value="years"><?php echo __('Years'); ?></option>
					<option value="months"><?php echo __('Months'); ?></option>
					<option value="days"><?php echo __('Days'); ?></option>
					</select>
				</div>
			</div>
			<br/>
			<div class="form-group">
                            <label class="col-sm-2"><?php echo __('Special Conditions for Approval?'); ?></label>
                            <textarea maxlength="100" rows="5" cols="50" name="sp_condition" id="sp_condition">
                            
                            </textarea><span>Max (100 Characters) </span>
			</div>
			
		  <div class="modal-footer">
			<button type="submit" class="btn btn-primary"><?php echo __('Save changes'); ?></button>
		  </div>
		</form>
	  </div>
	</div>
</div>
