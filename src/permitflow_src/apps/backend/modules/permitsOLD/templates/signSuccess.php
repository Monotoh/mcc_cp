<?php
  /**
   * viewSuccess.php template.
   *
   * Displays a full invoice
   *
   * @package    frontend
   * @subpackage invoices
   * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
  */

  $q = Doctrine_Query::create()
   ->from('FormEntry a')
   ->where('a.id = ?', $permit->getApplicationId());
  $application = $q->fetchOne();
 ?>
 <div class="pageheader">
  <h2><i class="fa fa-envelope"></i> Permit <span>View permit details</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>index.php">Home</a></li>
      <li><a href="<?php echo public_path(); ?>index.php/permits/index">Permits</a></li>
      <li class="active"><?php echo $application->getApplicationId(); ?></li>
    </ol>
  </div>
</div>

<div class="contentpanel">
    <div class="row">

        <div class="col-sm-12">

<ul id="myTab" class="nav nav-tabs" style="margin-top:20px; margin-right:20px;">
                        <li class="active"><a href="#tabs-1" data-toggle="tab"><?php if($permit->getPermitId()){ echo $permit->getPermitId(); }else{ echo $permit->getFormEntry()->getApplicationId(); } ?> Details</a></li>
</ul>
        <div id="myTabContent" class="tab-content" style=" margin-right:20px;">
                    <div class="tab-pane fade in active" id="tabs-1">
                       <?php

                       $userid = $permit->getFormEntry()->getUserId();

                       $q = Doctrine_Query::create()
                          ->from("SfGuardUser a")
                          ->where("a.id = ?", $userid);
                       $user = $q->fetchOne();

                       $q = Doctrine_Query::create()
                          ->from("SfGuardUserProfile a")
                          ->where("a.user_id = ?", $userid);
                       $userprofile = $q->fetchOne();

                       $q = Doctrine_Query::create()
                         ->from('CfUser a')
                         ->where('a.nid = ?', $sf_user->getAttribute('userid'));
                       $logged_reviewer = $q->fetchOne();

                        /*
                         *  Setup variables
                         *  Change these before testing
                         */
                        $api_key = sfConfig::get('app_echo_sign_key');
                        
                        $recipient_email = $logged_reviewer->getStremail();
                        
                        $merge_fields = array(
                                                'first_name' => $logged_reviewer->getStrfirstname(), 
                                                'last_name' => $logged_reviewer->getStrlastname(), 
                                                'website' => "http://".$_SERVER['HTTP_HOST']
                                             );
                        
                        $application = $permit->getApplication();

                        require_once(dirname(__FILE__)."/../../../../../lib/vendor/dompdf/dompdf_config.inc.php");

                        $html = "<html>
                        <body>
                        ";

                        $templateparser = new TemplateParser();

                        $html .= $templateparser->parsePermit($application->getId(), $application->getFormId(), $application->getEntryId(), $permit->getId(), $permit->getPermit());

                        $html .= "
                        </body>
                        </html>";

                        $dompdf = new DOMPDF();
                        $dompdf->load_html(html_entity_decode($html));
                        $dompdf->render();
                        $output = $dompdf->output();

                        $filepath = dirname(__FILE__).'/../../../../../../html/asset_temp/'.$permit->getPermitId().'.pdf';
                        file_put_contents($filepath, $output);
                        
                        //end setup variables
                        
                        $ESLoader = new SplClassLoader('EchoSign', dirname(__FILE__).'/../../../../../lib/vendor');
                        $ESLoader->register();
                        
                        $client = new SoapClient(EchoSign\API::getWSDL());
                        $api = new EchoSign\API($client, $api_key);
                        

                        $file = EchoSign\Info\FileInfo::createFromFile($filepath);
                        
                        $widget = new EchoSign\Info\WidgetCreationInfo($permit->getPermitId(), $file);
                        $widget->setMergeFields(new EchoSign\Info\MergeFieldInfo($merge_fields));
                        
                        try{
                            $result = $api->createEmbeddedWidget($widget);
                        }catch(Exception $e){
                            print '<h3>An exception occurred:</h3>';
                            var_dump($e);
                        }

                        $permit->setDocumentKey($result->embeddedWidgetCreationResult->documentKey);
                        $permit->save();

                        echo $result->embeddedWidgetCreationResult->javascript;
                       ?>
                    </div>

</div>

</div><!-- /.row -->



</div><!-- /.marketing -->
