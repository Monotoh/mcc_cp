<?php
use_helper("I18N");

?>
<div class="pageheader">
  <h2><i class="fa fa-envelope"></i> <?php echo __('Approval Details'); ?></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="/backend.php">Home</a></li>
      <li><a href="/backend.php/applications/list/get/your">Applications</a></li>
    </ol>
  </div>
</div>
<br/>
<div class="col-sm-10">
	<div class="responses">
	</div>
	<div class="panel panel-dark">
		<div class="panel-heading">
			<h4 class="panel-title"><?php echo __('Extend/Reduce Duration of The Permit from: <i style="color:red;">'.$date_of_views ."<i>"); ?></h4>

		</div>
	   <div class="panel-body panel-body-nopadding">
	   <div class="results"></div>
	   <br/>
		<form class="form" action="" method="post" autocomplete="off" id="updatexpirydate">
		
		<input type="hidden" name="id" value="<?php echo $id; ?>"  class="form-control" id="id_permit">
		
			<div class="form-group">
				<label class="col-sm-4"><?php echo __('Approved Permit will be valid for: '); ?></label>
				<div class="col-sm-7">
					<input type="text" name="validity_period"  class="form-control" id="validity_period" required="required">
				</div>
				<!-- <div class="col-sm-4">
					<select required name="period_uom" id="period_uom" class="form-control">
					<option value="">Please select</option>
					<option value="years"><?php //echo __('Years'); ?></option>
					<option value="months"><?php //echo __('Months'); ?></option>
					<option value="days"><?php //echo __('Days'); ?></option>
					</select>
				</div> -->
			</div>
			<br/>
			
			
		  <div class="modal-footer">
			<button type="submit" class="btn btn-primary" id="submitButton"><?php echo __('Save changes'); ?></button>
		  </div>
		</form>
	  </div>
	</div>
	<script>
		$("#submitButton").click(function () {
			var validity_period = $("#validity_period").val();
			
			if(validity_period.length != 0){
				var days_today = new Date().toISOString().slice(0, 10);
				var validity_periods = validity_period
				if(days_today < validity_periods){
					alert("Date of Permit to change to " + validity_period + " Press Ok to continue");
				}else{
					alert("Invalid Date " + validity_period + " Press Ok to continue");
				}
			}
						
		});

		$(function() {
     		$( "#validity_period" ).datepicker({ dateFormat: 'yy-mm-dd'}); 
		});

	</script>

	
</div>

