<?php
/**
 * indexSuccess.php template.
 *
 * Displays list of all of the currently logged in client's permits
 *
 * @package    frontend
 * @subpackage permits
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");
?>


 <div class="pageheader">
    <h2><i class="fa fa-certificate"></i><?php echo __('Permits'); ?></h2>
  <div class="breadcrumb-wrapper">

    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>index.php"><?php echo __('Home'); ?></a></li>
      <li><a href="<?php echo public_path(); ?>index.php/permits/index"><?php echo __('Permits'); ?></a></li>
    </ol>
  </div>
</div>

<div class="contentpanel">
    <div class="row">

 <div class="panel panel-dark widget-btns">
   <div class="panel-heading">
     <h3 class="panel-title"><?php echo __('Permits'); ?></h3>
      <p class="text-muted"><?php echo __('Issued Permits'); ?></p>
      <div class="pull-right">
      <form method="post">
      <input type="text" class="form-control pull-right" style="margin-top: -45px;" name="search" id="search" placeholder="Search..." value="<?php echo $search; ?>"/>
    </form>
    </div>
    </div>


  <div class="panel-body panel-body-nopadding">
  <?php echo $stats; ?>
  </div>
  </div>
  </div>
  </div>
