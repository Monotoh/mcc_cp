<?php
/**
 * viewSuccess.php template.
 *
 * Displays a full invoice
 *
 * @package    frontend
 * @subpackage invoices
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
*/
  $q = Doctrine_Query::create()
   ->from('FormEntryArchive a')
   ->where('a.id = ?', $permit->getApplicationId());
  $application = $q->fetchOne();
 ?>
 <div class="pageheader">
  <h2><i class="fa fa-envelope"></i> Permit <span>View permit details</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>index.php">Home</a></li>
      <li><a href="<?php echo public_path(); ?>index.php/permits/index">Permits</a></li>
      <li class="active"><?php echo $application->getApplicationId(); ?></li>
    </ol>
  </div>
</div>

<div class="contentpanel">
    <div class="row">

        <div class="col-sm-12">

        <ul id="myTab" class="nav nav-tabs" style="margin-top:20px; margin-right:20px;">
            <li class="active"><a href="#tabs-1" data-toggle="tab"><?php if($permit->getPermitId()){ echo $permit->getPermitId(); }else{ echo $permit->getFormEntry()->getApplicationId(); } ?> Details</a></li>
        	<li class="pull-right" style="padding-top: 5px; padding-right: 10px;"><button class="btn btn-white" id="printinvoice" type="button" onClick="window.location='/backend.php/applications/viewpermit/id/<?php echo $permit->getId(); ?>';"><i class="fa fa-print mr5"></i> Print</button></li>
		</ul>
        <div id="myTabContent" class="tab-content" style=" margin-right:20px;">
                    <div class="tab-pane fade in active" id="tabs-1">
                        <?php
                            $permit_manager = new PermitManager();
                            $html = $permit_manager->generate_archive_permit_template($permit->getId(), false);

                            echo $html;
                        ?>

                        <div class="text-right btn-invoice" style="padding-right: 10px;">
                            
                           <button class="btn btn-white" id="printinvoice" type="button" onClick="window.location='/backend.php/applications/viewpermitarchive/id/<?php echo $permit->getId(); ?>';"><i class="fa fa-print mr5"></i> Print Service</button>
                            

                            <?php
                            //If permit template has a remote url, add a button for update remote database
                            $q = Doctrine_Query::create()
                                ->from("Permits a")
                                ->where("a.id = ?", $permit->getTypeId())
                                ->andWhere("a.remote_url <> ?", "");
                            $permit_template = $q->fetchOne();

                            if($permit_template)
                            {
                                ?>
                                <button class="btn btn-white" id="cancelpermit" type="button" onClick="window.location='/backend.php/permits/updatesingleremotearchive/id/<?php echo $permit->getId(); ?>';"><i class="fa fa-print mr5"></i> Update Remote Database</button>
                                <?php
                            }
                            ?>
                        </div>
                    </div>



</div>

</div><!-- /.row -->



</div><!-- /.marketing -->
