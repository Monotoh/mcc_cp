<?php

/**
 * plot actions.
 *
 * @package    permit
 * @subpackage plot
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class plotActions extends sfActions
{
  public function executeMap(sfWebRequest $request)
  {
  	$this->setLayout("layout");
  }
  public function executeGeneralmap(sfWebRequest $request)
  {
  	$this->setLayout("layout");
  }

  public function executeIndex(sfWebRequest $request)
  {
    $q = Doctrine_Query::create()
       ->from('Plot a')
	   ->orderBy('a.id DESC');
     $this->plots = $q->execute();
	 
	$this->setLayout(false);
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new plotForm();
	$this->setLayout(false);
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new plotForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($plot = Doctrine_Core::getTable('plot')->find(array($request->getParameter('id'))), sprintf('Object plot does not exist (%s).', $request->getParameter('id')));
    $this->form = new plotForm($plot);
	$this->setLayout(false);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($plot = Doctrine_Core::getTable('plot')->find(array($request->getParameter('id'))), sprintf('Object plot does not exist (%s).', $request->getParameter('id')));
    $this->form = new plotForm($plot);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {

    $this->forward404Unless($plot = Doctrine_Core::getTable('plot')->find(array($request->getParameter('id'))), sprintf('Object plot does not exist (%s).', $request->getParameter('id')));

    $audit = new Audit();
    $audit->saveAudit("", "deleted plot of id ".$plot->getId());

    $plot->delete();

    $this->redirect('/backend.php/settings/forms?load=plots');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $plot = $form->save();

      $audit = new Audit();
      $audit->saveAudit("", "<a href=\"/backend.php/plot/edit?id=".$plot->getId()."&language=en\">updated a plot</a>");

      $this->redirect('/backend.php/settings/forms?load=plots');
    }
  }
}
