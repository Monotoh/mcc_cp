<?php
use_helper("I18N");
?>
<?php
if($sf_user->mfHasCredential("manageplots"))
{
  $_SESSION['current_module'] = "plots";
  $_SESSION['current_action'] = "index";
  $_SESSION['current_id'] = "";
?>
<div class="panel panel-dark">
<div class="panel-heading">
			<h3 class="panel-title"><?php echo __('Plots'); ?></h3>
				<div class="pull-right">
            <a class="btn btn-primary-alt settings-margin42" id="newplot" href="#"><?php echo __('New Plot'); ?></a>

            <script language="javascript">
            jQuery(document).ready(function(){
              $( "#newplot" ).click(function() {
                  $("#contentload").load("<?php echo public_path(); ?>backend.php/plot/new");
              });
            });
            </script>
</div>
</div>
		
 
<div class="panel panel-body panel-body-nopadding ">

<div class="table-responsive">
<table class="table dt-on-steroids mb0" id="table3">
    <thead>
   <tr>
      <th class="no-sort" style="width: 10px;"><input type='checkbox' name='batchall' onclick="boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.name == 'batch') { if(this.checked == true){ box.checked = false; }else{ box.checked = true; } } } "></th>
      
      <th width="60">#</th>
      <th class="no-sort"><?php echo __('Plot No'); ?></th>
      <th><?php echo __('Plot Status'); ?></th>
      <th class="no-sort" width="17%"><?php echo __('Actions'); ?></th>
    </tr>
  </thead>
  <tbody>
    <?php
		$count = 1;
	?>
    <?php foreach ($plots as $plot): ?>
    <tr id="row_<?php echo $plot->getId() ?>">
	  <td><input type='checkbox' name='batch' id='batch_<?php echo $plot->getId() ?>' value='<?php echo $plot->getId() ?>'></td>
      <td><?php echo $count++; ?></td>
      <td><?php echo $plot->getPlotNo() ?></td>
      <td><?php if($plot->getPlotStatus() == ""){ echo "free"; }else{ echo $plot->getPlotStatus(); } ?></td>
      <td>
						<a id="activityplot<?php echo $plot->getId(); ?>" href="#" alt="<?php echo __('Activity'); ?>"><span class="badge badge-primary"><i class="fa fa-file"></i></span></a>						
						<a id="editplot<?php echo $plot->getId(); ?>" href="#" alt="<?php echo __('Edit'); ?>"><span class="badge badge-primary"><i class="fa fa-pencil"></i></span></a>
						<a id="deleteplot<?php echo $plot->getId(); ?>" href="#" alt="<?php echo __('Delete'); ?>"><span class="badge badge-primary"><i class="fa fa-trash-o"></i></span></a>
						
            <script language="javascript">
            jQuery(document).ready(function(){
              $( "#editplot<?php echo $plot->getId(); ?>" ).click(function() {
                  $("#contentload").load("<?php echo public_path(); ?>backend.php/plot/edit/id/<?php echo $plot->getId(); ?>");
              });
              $( "#activityplot<?php echo $plot->getId(); ?>" ).click(function() {
                  $("#contentload").load("<?php echo public_path(); ?>backend.php/plotactivity/index/id/<?php echo $plot->getId(); ?>");
              });
              $( "#deleteplot<?php echo $plot->getId(); ?>" ).click(function() {
                  if(confirm('Are you sure you want to delete this web page?')){
                    $("#contentload").load("<?php echo public_path(); ?>backend.php/plot/delete/id/<?php echo $plot->getId(); ?>");
                  }
                  else
                  {
                    return false;
                  }
              });
            });
            </script>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
	<tfoot>
   <tr><td colspan='8' style='text-align: left;'>
   <select id='batch_action' name='batch_action' onChange="if(this.value != ''){if(confirm('Are you sure?')){ batch('plot', this.options[this.selectedIndex].text, this.value); document.getElementById('default').selected='selected'; }}">
   <option id='default' value=''><?php echo __('Choose an action'); ?>..</option>
   <option value='delete'><?php echo __('Set As Deleted'); ?></option>
   </select>
   </td></tr>
   </tfoot>
</table>
</div>
</div>
<script>
  jQuery('#table3').dataTable({
      "sPaginationType": "full_numbers",
     
      // Using aoColumnDefs
      "aoColumnDefs": [
      	{ "bSortable": false, "aTargets": [ 'no-sort' ] }
    	]
    });
    
</script><?php
}
else
{
  include_partial("accessdenied");
}
?>