<?php

/**
 * plotactivity actions.
 *
 * @package    permit
 * @subpackage plotactivity
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class plotactivityActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $q = Doctrine_Query::create()
       ->from('PlotActivity a')
	   ->where('a.plot_id = ?', $request->getGetParameter("id"))
	   ->orderBy('a.id DESC');
     $this->pager = new sfDoctrinePager('PlotActivity', 20);
	 $this->pager->setQuery($q);
	 $this->pager->setPage($request->getParameter('page', 1));
	 $this->pager->init();
	  
	  
	$this->setLayout(false);
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new PlotActivityForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new PlotActivityForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($plot_activity = Doctrine_Core::getTable('PlotActivity')->find(array($request->getParameter('id'))), sprintf('Object plot_activity does not exist (%s).', $request->getParameter('id')));
    $this->form = new PlotActivityForm($plot_activity);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($plot_activity = Doctrine_Core::getTable('PlotActivity')->find(array($request->getParameter('id'))), sprintf('Object plot_activity does not exist (%s).', $request->getParameter('id')));
    $this->form = new PlotActivityForm($plot_activity);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($plot_activity = Doctrine_Core::getTable('PlotActivity')->find(array($request->getParameter('id'))), sprintf('Object plot_activity does not exist (%s).', $request->getParameter('id')));
    $plot_activity->delete();

    $this->redirect('plotactivity/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $plot_activity = $form->save();

      $this->redirect('plotactivity/edit?id='.$plot_activity->getId());
    }
  }
}
