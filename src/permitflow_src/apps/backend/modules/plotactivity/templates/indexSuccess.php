<?php use_helper('I18N', 'Date') ?>

<div class="g12" style="padding-left: 3px;">
			<form style="margin-bottom: 0px;">
			<label style='height: 30px; margin-top: 0px;'>
			<div style='float: left; font-size: 20px; font-weight: 700;'>Plot Activity</div>
<div style="float: right; margin-top: -12px;">
							<button style="height: 34px; font-size: 12px;" onClick="window.location='<?php echo public_path(); ?>backend.php/plot/index';">Back to Plots</button>
</div>
</label>
			</form>

<table class="datatable">
    <thead>
   <tr>
      <th width="60">ID</th>
      <th>Plot No</th>
      <th>Application</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($pager->getResults() as $plot_activity): ?>
    <tr>
      <td><a href="<?php echo url_for('plotactivity/edit?id='.$plot_activity->getId()) ?>"><?php echo $plot_activity->getId() ?></a></td>
      <td><?php echo $plot_activity->getPlot()->getPlotNo() ?></td>
      <td><a href='/backend.php/forms/viewentry?form_id=<?php echo $plot_activity->getApplication()->getFormId() ?>&id=<?php echo $plot_activity->getApplication()->getEntryId() ?>'><?php echo $plot_activity->getApplication()->getApplicationId() ?></a></td>
      <td>
      <a href="/backend.php/applications/view/id/<?php echo $plot_activity->getApplication()->getId() ?>" alt="Edit"><img src="/assets_backend/images/icons/dark/magnifying_glass.png" border="0" ></a>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
</div>
