<?php
use_helper('I18N');

$audit = new Audit();
$audit->saveAudit("", "Accessed report settings");

if($sf_user->mfHasCredential("managereports"))
{
  $_SESSION['current_module'] = "reports";
  $_SESSION['current_action'] = "index";
  $_SESSION['current_id'] = "";
/**
 * index template.
 *
 * Displays list of custom reports
 *
 * @package    backend
 * @subpackage reports
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>
<div class="contentpanel">
<div class="panel panel-dark">
<div class="panel-heading">
			<h3 class="panel-title"><?php echo __('Custom Reports'); ?></h3>
</div>


<div class="panel panel-body panel-body-nopadding ">

<div class="table-responsive">
<table class="table dt-on-steroids mb0" id="table3">
    <thead>
   <tr>
      <th  class="no-sort" width="1%">#</th>
      <th class="no-sort"><?php echo __('Title'); ?></th>
      <th class="no-sort" width="7%"><?php echo __('Actions'); ?></th>
    </tr>
    </thead>
    <tbody>
	<?php
		$count = 1;
	?>
    <?php foreach ($reports as $report): ?>
    <tr id="row_<?php echo $report->getFormId() ?>">
	   <td ><?php echo $count++; ?></td>
      <td ><?php echo $report->getFormName() ?></td>
      <td>
		    <a id="editreport<?php echo $report->getFormId(); ?>" href="<?php echo public_path(); ?>backend.php/forms/managereport/id/<?php echo $report->getFormId(); ?>" title="<?php echo __('Edit'); ?>"><span class="badge badge-primary"><i class="fa fa-pencil"></i></span></a>
      </td>
    </tr>
    <?php endforeach; ?>
     </tbody>
</table>
</div>
</div>
</div>
<script>
jQuery(document).ready(function(){
  jQuery('#table3').dataTable({
      "sPaginationType": "full_numbers",

      // Using aoColumnDefs
      "aoColumnDefs": [
      	{ "bSortable": false, "aTargets": [ 'no-sort' ] }
    	]
    });
});
</script>
<?php
}
else
{
  include_partial("accessdenied");
}
?>
