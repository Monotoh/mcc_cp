<!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css" /> -->
<link href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo public_path(); ?>assets_unified/css/auto_complete_custome.css">
<?php
use_helper("I18N");
if($sf_user->mfHasCredential("access_reports"))
{
?>
<?php
	$formoptions = "";

	$formoptions.="<optgroup label='Application Forms'>";

	$formoptions.="<option value=''>Choose form...</option>";

		$q = Doctrine_Query::create()
		  ->from('ApForms a')
		  ->where('a.form_type = ?', 1)
			->andWhere('a.form_active = ?', 1)
		  ->orderBy('a.form_name ASC');
		$forms = $q->execute();

		foreach($forms as $form)
		{

			$formoptions.="<option value='".$form->getFormId()."'>".$form->getFormCode()." - ".$form->getFormName()."-".$form->getFormDescription()."</option>";

		}

	$formoptions.="</optgroup>";

	$menuoptions = "";
	$q = Doctrine_Query::create()
	  ->from('Menus a')
	  ->orderBy('a.order_no ASC');
	$stagegroups = $q->execute();
	foreach($stagegroups as $stagegroup)
	{
		$menuoptions.="<div class=\"col-xs-12 col-sm-6 col-md-3\">
		               <div class=\"blog-item pt20 pl20 pr20 pb20\" style=\"background:white; border: 1px solid #dddddd\">
                       <h4 class=\"panel-title mb10 mt10\">".$stagegroup->getTitle()."</h4>";
		$q = Doctrine_Query::create()
		  ->from('SubMenus a')
		  ->where('a.menu_id = ?', $stagegroup->getId())
			->andWhere('a.deleted = 0')
		  ->orderBy('a.order_no ASC');
		$stages = $q->execute();

		foreach($stages as $stage)
		{
			$selected = "";

			if($_GET['filter'] != "" && $_GET['filter'] == $stage->getId())
			{
				$selected = "selected";
			}

			$menuoptions.="<div class=\"checkbox block\">
							<label>
								<input type='checkbox' name='pending_stage[".$stage->getId()."]' id='pending_stage_".$stage->getId()."' value='".$stage->getId()."'>
								".$stage->getTitle()."
							</label>
						  </div>";

		}
		$menuoptions.= '</div></div>';
	}

	$menuoptions2 = "";
	$q = Doctrine_Query::create()
	  ->from('Menus a')
	  ->orderBy('a.order_no ASC');
	$stagegroups = $q->execute();

	foreach($stagegroups as $stagegroup)
	{
		$menuoptions2.="<optgroup label='".$stagegroup->getTitle()."'>";
		$q = Doctrine_Query::create()
		  ->from('SubMenus a')
		  ->where('a.menu_id = ?', $stagegroup->getId())
			->andWhere('a.deleted = 0')
		  ->orderBy('a.order_no ASC');
		$stages = $q->execute();

		foreach($stages as $stage)
		{
			$selected = "";

			if($_GET['filter'] != "" && $_GET['filter'] == $stage->getId())
			{
				$selected = "selected";
			}

			$menuoptions2.="<option value='".$stage->getId()."' ".$selected.">".$stage->getTitle()."</option>";

		}
		$menuoptions2.="</optgroup>";
	}


	$formoptions2 = "";
	$formoptions2.="<optgroup label='Application Forms'>";

	foreach($groups as $group)
	{
		$formoptions2.="<optgroup label='".$group->getGroupName()."'>";

		$q = Doctrine_Query::create()
		  ->from('ApForms a')
		  ->leftJoin('a.ApFormGroups b')
		  ->where('a.form_id = b.form_id')
		  ->andWhere('b.group_id = ?', $group->getGroupId())
		  ->andWhere('a.form_id = 60 OR a.form_id = 47 OR a.form_id = 48 OR a.form_id = 49');
		$forms = $q->execute();

		foreach($forms as $form)
		{
			$selected = "";

			if($application_form != "" && $application_form == $form->getFormId())
			{
				$selected = "selected";
				$_GET['form'] = $application_form;
			}

			$formoptions2.="<option value='".$form->getFormId()."' ".$selected.">".$form->getFormDescription()."</option>";
		}

		$formoptions2.="</optgroup>";
	}


	$formoptions2.="</optgroup>";

	$mdaoptions = "";
	$mdaoptions .= "<option id='1'>".sfConfig::get('app_mda_branch')."</option>";

	$pspoptions = "";
	$pspoptions .= "<option id='pesapal'>PesaPal</option>";

?>

<div class="pageheader">
  <h2><i class="fa fa-file-text"></i><?php echo __('Reports'); ?></h2>
  <div class="breadcrumb-wrapper">
    <span class="label"><?php echo __('You are here'); ?>:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>backend.php/dashboard"><?php echo __('Home'); ?></a></li>
      <li><?php echo __('Reports'); ?></li>
    </ol>
  </div>
</div>

<div class="contentpanel">

  	<div class="panel-group" id="accordion">

			<!-- Built-In report 1: Report of all applications that have been submitted within a specified time period and their status -->
				<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#collapse3t">
							<?php echo __('Time Table'); ?>
						</a>
									</h4>
								</div>
								<div id="collapse3t" class="panel-collapse collapse">
								<div class="panel-body panel-body-nopadding" id="report1_div">

				<form id="report1_form" name="report1_form" class="form-horizontal fh-special form-bordered" method="post" action="<?php echo public_path(); ?>backend.php/reports/timetablereport?tr=1" autocomplete="off" data-ajax="false">
							<div class="panel-heading-inline panel-heading-inline-gray">
							<h3 class="panel-title"><?php echo __('Report that uses the date and time fields in an application form to create a schedule for submitted applications'); ?></h3>
							</div>
				<div class="form-group">
								<label class="col-sm-3 control-label"><?php echo __('Type Of Application'); ?></label>
						<div class="col-sm-9">
							<select id='application_form_timetable' name='application_form' required>
								<?php
									echo $formoptions;
								?>
							</select>
						</div>
					</div>
					<div class="form-group">
									<label class="col-sm-3 control-label"><?php echo __('Select Date Field'); ?></label>
							<div class="col-sm-9">
								<div id='ajaxdatefields' name='ajaxdatefields' >
								</div>
							</div>
					</div>
					<div class="form-group">
									<label class="col-sm-3 control-label"><?php echo __('Select Time Field'); ?></label>
							<div class="col-sm-9">
								<div id='ajaxtimefields' name='ajaxtimefields' >
								</div>
							</div>
					</div>
					<div class="form-group">
								<label class="col-sm-3 control-label"><?php echo __('Status'); ?></label>
						<div class="col-sm-6">
							<select id='application_status' name='application_status'>
							<option value="0"><?php echo __('Filter By Stage'); ?></option>
							<?php
								echo $menuoptions2;
							?>
						</select>
						</div>
					</div>
					</div>
					<div class="panel-footer">
						<button type="reset" class="btn btn-danger mr5"><?php echo __('Reset'); ?></button>
						<button type="submit" class="btn btn-primary" name="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button>
					</div>
				</form>
				</div>
			</div>

		<script language="javascript">
			jQuery(document).ready(function(){
					jQuery("#application_form_timetable" ).change(function() {
							var selecteditem = this.value;
							$.ajax({url:"/backend.php/reports/getdatefields?formid=" + selecteditem,success:function(result){
						    $("#ajaxdatefields").html(result);
						  }});
							$.ajax({url:"/backend.php/reports/gettimefields?formid=" + selecteditem,success:function(result){
								$("#ajaxtimefields").html(result);
							}});
					});
			});
		</script>

<!-- Built-In report 2: Report of all applications that have been approved within a specific time period and their status. -->
	<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapseFilter">
				<?php echo __('Filter Applications By Dropdown'); ?>
			</a>
						</h4>
					</div>
					<div id="collapseFilter" class="panel-collapse collapse">
					<div class="panel-body panel-body-nopadding">
<form class="form-horizontal fh-special form-bordered" method="post" action="<?php echo public_path(); ?>backend.php/reports/printreportfilter" autocomplete="off" data-ajax="false">
		<div class="panel-heading-inline panel-heading-inline-gray">
				<h3 class="panel-title"><?php echo __('Report of all applications within a time period filtered by a dropdown'); ?></h3>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label"><?php echo __('Type Of Application'); ?></label>
								<div class="col-sm-6">
										<select id='application_form_filter' name='application_form_filter' required>
												<?php
														echo $formoptions;
												?>
										</select>
								</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label"><?php echo __('From Date'); ?></label>
							<div class="col-sm-6">
													<div class="input-group">
								<input type='text' name='from_date_filter' id='from_date1' class="form-control" required>
														<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
													</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label"><?php echo __('To Date'); ?></label>
							<div class="col-sm-6">
												<div class="input-group">
								<input type='text' name='to_date_filter' id='to_date1' class="form-control" required>
														<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
													</div>
							</div>
						</div>

							<div class="form-group">
											<label class="col-sm-3 control-label"><?php echo __('Select Dropdown Field'); ?></label>
									<div class="col-sm-9">
										<div id='ajaxdropdownfields' name='ajaxdropdownfields' >
										</div>
									</div>
							</div>

							<div class="form-group">
											<label class="col-sm-3 control-label"><?php echo __('Filter By Dropdown Option'); ?></label>
									<div class="col-sm-9">
										<div id='ajaxdropdownvaluefields' name='ajaxdropdownvaluefields' >
										</div>
									</div>
							</div>
					</div>

			<div class="panel-footer">
			<button type="reset" class="btn btn-danger mr5"><?php echo __('Reset'); ?></button>
			<button type="submit" class="btn btn-primary" name="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button>
		</div>
	</form>
		</div>
	</div>

	<script language="javascript">
		jQuery(document).ready(function(){
				jQuery("#application_form_filter" ).change(function() {
						var selecteditem = this.value;
						$.ajax({url:"/backend.php/reports/getdropdownfields?formid=" + selecteditem,success:function(result){
							$("#ajaxdropdownfields").html(result);
						}});
				});
				jQuery("#form_dropdown_fields" ).change(function() {
						var selecteditem = this.value;
						$.ajax({url:"/backend.php/reports/getdropdownvaluefields?elementid=" + selecteditem,success:function(result){
							$("#ajaxdropdownvaluefields").html(result);
						}});
				});
		});
	</script>


		<!-- Built-In report 1: Report of all applications that have been submitted within a specified time period and their status -->
	    <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
					  <?php echo __('Submissions Report'); ?>
				  </a>

                </h4>
              </div>
              <div id="collapse3" class="panel-collapse collapse">
              <div class="panel-body panel-body-nopadding" id="report1_div">

			<form id="report1_form" name="report1_form" class="form-horizontal fh-special form-bordered" method="post" action="<?php echo public_path(); ?>backend.php/reports/printreport1" autocomplete="off" data-ajax="false">
            <div class="panel-heading-inline panel-heading-inline-gray">
            <h3 class="panel-title"><?php echo __('Report of all applications that have been submitted within a specified time period and their status'); ?></h3>
            </div>
			 <div class="form-group">
              <label class="col-sm-3 control-label"><?php echo __('Type Of Application'); ?></label>
					<div class="col-sm-9">
						<select id='application_form' name='application_form' onChange="ajaxSearchform(this.value);" required>
							<?php
								echo $formoptions;
							?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label"><?php echo __('From Date'); ?></label>
					<div class="col-sm-9">
                        <div class="input-group">
						<input type='text' name='from_dateblt1' id='from_dateblt1' class="form-control" required>
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                       </div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label"><?php echo __('To Date'); ?></label>
					<div class="col-sm-9">
                         <div class="input-group">
						<input type='text' name='to_date' id='to_date' class="form-control" required>
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                       </div>

					</div>
				 <div id='ajaxsearchform' name='ajaxsearchform' >
				 </div>
				</div>
                </div>
				<div class="panel-footer">
					<button type="reset" class="btn btn-danger mr5"><?php echo __('Reset'); ?></button>
					<button type="submit" class="btn btn-primary" name="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button>
				</div>
			</form>
         </div>
        </div>

		<!-- Built-In report 2: Report of all applications that have been approved within a specific time period and their status. -->
	    <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
					  <?php echo __('Approvals Report'); ?>
				  </a>
                </h4>
              </div>
              <div id="collapse4" class="panel-collapse collapse">
              <div class="panel-body panel-body-nopadding">
		<form class="form-horizontal fh-special form-bordered" method="post" action="<?php echo public_path(); ?>backend.php/reports/printreport2" autocomplete="off" data-ajax="false">
        <div class="panel-heading-inline panel-heading-inline-gray">
            <h3 class="panel-title"><?php echo __('Report of all applications that have been approved within a specific time period and their status'); ?></h3>
            </div>
			 <div class="form-group">
              <label class="col-sm-3 control-label"><?php echo __('Type Of Application'); ?></label>
                    <div class="col-sm-6">
                        <select id='application_form' name='application_form' onChange="ajaxSearchform(this.value,1);" required>
                            <?php
                                echo $formoptions;
                            ?>
                        </select>
                    </div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label"><?php echo __('From Date'); ?></label>
					<div class="col-sm-6">
                       <div class="input-group">
						<input type='text' name='from_date1' id='from_date1' class="form-control" required>
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                       </div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label"><?php echo __('To Date'); ?></label>
					<div class="col-sm-6">
                    <div class="input-group">
						<input type='text' name='to_date1' id='to_date1' class="form-control" required>
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                       </div>
					</div>
				 <div id='ajaxsearchform1' name='ajaxsearchform1'></div>
				</div>
               </div>

					<div class="panel-footer">
					<button type="reset" class="btn btn-danger mr5"><?php echo __('Reset'); ?></button>
					<button type="submit" class="btn btn-primary" name="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button>
				</div>
			</form>
         </div>
        </div>

        <!-- Built-In report 3: Report of all applications that are pending at a particular stage of the workflow. -->
	    <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
					  <?php echo __('Pending Stages Report'); ?>
				  </a>
                </h4>
              </div>
              <div id="collapse5" class="panel-collapse collapse">
              <div class="panel-body panel-body-nopadding">
		<form class="form-horizontal fh-special form-bordered" method="post" action="<?php echo public_path(); ?>backend.php/reports/printreport3" autocomplete="off" data-ajax="false">
         <div class="panel-heading-inline panel-heading-inline-gray">
            <h3 class="panel-title"><?php echo __('Report of all applications that are pending at a particular stage of the workflow'); ?></h3>
            </div>
			 <div class="form-group">
						<?php
							echo $menuoptions;
						?>
				</div>
                  </div>
				<div class="panel-footer">
					<button type="reset" class="btn btn-danger mr5"><?php echo __('Reset'); ?></button>
					<button onclick="if(atLeastOneCheckbox()){ return true; }else{ alert('Please choose one or more options first.'); window.location = '/backend.php/reports/list'; return false; }" type="submit "class="btn btn-primary" name="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button>
				</div>
			</form>
         </div>
        </div>

        <!-- Built-In report 4: Report of all applications that have exceeded their designated time limit within the various workflow stages. -->
	    <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
					  <?php echo __('Overdue Applications Reports'); ?>
				  </a>
                </h4>
              </div>
              <div id="collapse6" class="panel-collapse collapse">
              <div class="panel-body panel-body-nopadding">
		<form class="form-horizontal fh-special form-bordered" method="post" action="<?php echo public_path(); ?>backend.php/reports/printreport4" autocomplete="off" data-ajax="false">
         <div class="panel-heading-inline panel-heading-inline-gray">
            <h3 class="panel-title"><?php echo __('Report of all applications that have exceeded their designated time limit within the various workflow stages'); ?></h3>
            </div>
			 <div class="form-group">

						<?php
							echo $menuoptions;
						?>
				</div>
            </div>
			 <div class="panel-footer">
            	<button type="reset" class="btn btn-danger mr5"><?php echo __('Reset'); ?></button>
                <button onclick="if(atLeastOneCheckbox()){ return true; }else{ alert('Please choose one or more options first.'); return false; }" class="btn btn-default" type="submit" name="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button>
            </div>
			</form>
         </div>
        </div>

        <!-- Built-In report 5: Report of all applications pending action from the requestor (developer/architect). -->
	    <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">
					  <?php echo __('Pending Action Report'); ?>
				  </a>
                </h4>
              </div>
              <div id="collapse7" class="panel-collapse collapse">
              <div class="panel-body panel-body-nopadding">
		<form class="form-horizontal fh-special form-bordered" method="post" action="<?php echo public_path(); ?>backend.php/reports/printreport5" autocomplete="off" data-ajax="false">
        <div class="panel-heading-inline panel-heading-inline-gray">
            <h3 class="panel-title"><?php echo __('Report of all applications pending action from the client e.g. declined applications that need to be resubmitted or invoices that need payment'); ?></h3>
            </div>
			 <div class="form-group">
						<?php
							echo $menuoptions;
						?>
				</div>
             </div>

			 <div class="panel-footer">
				<button type="reset" class="btn btn-danger mr5"><?php echo __('Reset'); ?></button>
				<button type="submit" class="btn btn-default" name="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button>
			</div>
			</form>
         </div>
        </div>

        <!-- Built-In report 6: Report of all applications that are pending at a particular stage of the workflow. -->
	    <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">
					 <?php echo __('Approvals/Rejections Report'); ?>
				  </a>
                </h4>
              </div>
              <div id="collapse8" class="panel-collapse collapse">
              <div class="panel-body panel-body-nopadding">
		<form class="form-horizontal fh-special form-bordered" method="post" action="<?php echo public_path(); ?>backend.php/reports/printreport6" autocomplete="off" data-ajax="false">
        <div class="panel-heading-inline panel-heading-inline-gray">
            <h3 class="panel-title"><?php echo __('Report of all applications sent to a particular stage and the name of the person that sent it there'); ?></h3>
            </div>
			 <div class="form-group">
						<?php
							echo $menuoptions;
						?>
				</div>
             </div>
			 <div class="panel-footer">
				<button type="reset" class="btn btn-danger mr5"><?php echo __('Reset'); ?></button>
				<button type="submit" class="btn btn-primary" name="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button>
			</div>
			</form>
         </div>
        </div>

        <!-- Built-In report 7: Report of all notifications for an application that have been sent. -->
	    <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse9">
					  <?php echo __('Mail/SMS Notifications Report'); ?>
				  </a>
                </h4>
              </div>
              <div id="collapse9" class="panel-collapse collapse">
              <div class="panel-body panel-body-nopadding">
		<form class="form-horizontal fh-special form-bordered" method="post" action="<?php echo public_path(); ?>backend.php/reports/printreport7" autocomplete="off" data-ajax="false">
        <div class="panel-heading-inline panel-heading-inline-gray">
            <h3 class="panel-title"><?php echo __('Report of all notifications for an application that have been sent'); ?></h3>
            </div>
			 <div class="form-group">
						<?php
							echo $menuoptions;
						?>
				</div>
             </div>

			 <div class="panel-footer">
				<button type="reset" class="btn btn-default mr5"><?php echo __('Reset'); ?></button>
				<button type="submit" class="btn btn-default" name="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button>
			</div>
			</form>
         </div>
        </div>

      <!-- Built-In report 8: Report of all inspection carried out by a specific reviewer.
	    <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse10">
					  <?php echo __('Inspections Report'); ?>
				  </a>
                </h4>
              </div>
              <div id="collapse10" class="panel-collapse collapse">
              <div class="panel-body panel-body-nopadding">
		<form class="form-horizontal fh-special form-bordered" method="post" action="<?php echo public_path(); ?>backend.php/reports/printreport8" autocomplete="off" data-ajax="false">
          <div class="panel-heading-inline panel-heading-inline-gray">
            <h3 class="panel-title"><?php echo __('Report of all inspection carried out by a specific reviewer'); ?></h3>
            </div>
			 <div class="form-group">
              <label class="col-sm-3 control-label"><?php echo __('Reviewers'); ?></label>
					<div class="col-sm-6">
					<select id='reviewer' name='reviewer'>
						<?php
							$q = Doctrine_Query::create()
							  ->from('CfUser a')
							  ->orderBy('a.strfirstname ASC');
							$reviewers = $q->execute();

							foreach($reviewers as $reviewer)
							{
								echo "<option value='".$reviewer->getNid()."'>".$reviewer->getStrfirstname()." ".$reviewer->getStrlastname()."</option>";
							}
						?>
					</select>
					</div>
				</div>
			 <div class="form-group">
              <label class="col-sm-3 control-label"><?php echo __('From Date'); ?></label>
					<div class="col-sm-6">
                      <div class="input-group">
						<input type='text' name='from_date8' id='from_date8' class="form-control">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                       </div>
					</div>
				</div>

			 <div class="form-group">
              <label class="col-sm-3 control-label"><?php echo __('To Date'); ?></label>
					<div class="col-sm-6">
                     <div class="input-group">
						<input type='text' name='to_date8' id='to_date8' class="form-control">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                       </div>
					</div>
				</div>
                </div>
			 <div class="panel-footer">
				<button type="reset" class="btn btn-danger ml5"><?php echo __('Reset'); ?></button>
				<button type="submit" class="btn btn-primary" name="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button>
			 </div>
			</form>

         </div>
        </div>

         <!-- Built-In report 9: Inspection History of an Application.
	    <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse11">
					  Inspection History Report
				  </a>
                </h4>
              </div>
              <div id="collapse11" class="panel-collapse collapse">
              <div class="panel-body panel-body-nopadding">
		<form class="form-horizontal form-bordered" method="post" action="<?php echo public_path(); ?>backend.php/reports/report9" autocomplete="off" data-ajax="false">
        <div class="panel-heading-inline panel-heading-inline-gray">
            <h3 class="panel-title">Inspection History of an Application.</h3>
            </div>
			 <div class="form-group">
						<?php
							echo $menuoptions;
						?>
				</div>
                 </div>

			 <div class="panel-footer">
				<button type="reset" class="btn btn-danger">Reset</button>
				<button type="submit" class="btn btn-primary" name="submitbuttonname" value="submitbuttonvalue">Submit</button>
			 </div>
			</form>
         </div>
        </div>

        -->

        <!-- Built-In report 10: Report of all activities with a certain period of time. -->
	    <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse12">
					  <?php echo __('Audit Trail Report'); ?>
				  </a>
                </h4>
              </div>
              <div id="collapse12" class="panel-collapse collapse">
              <div class="panel-body panel-body-nopadding">
			<form class="form-horizontal fh-special form-bordered" method="post" action="<?php echo public_path(); ?>backend.php/reports/printreport10" autocomplete="off" data-ajax="false">
            <div class="panel-heading-inline panel-heading-inline-gray">

            <h3 class="panel-title"><?php echo __('Report of all activities with a certain period of time'); ?></h3>
            <?php echo $error; ?>
            </div>
			 <div class="form-group">
              <label class="col-sm-3 control-label"><?php echo __('From Date'); ?></label>
						<div class="col-sm-6">
                         <div class="input-group">
						<input type='text' name='from_date10' id='from_date10' class="form-control" required="required">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                       </div>
						</div>
					</div>
			 <div class="form-group">
	          <label class="col-sm-3 control-label"><?php echo __('To Date'); ?></label>
						<div class="col-sm-6">
	                    <div class="input-group">
						<input type='text' name='to_date10' id='to_date10' class="form-control" required="required">
	                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
	                   </div>
						</div>
					</div>
	          

	          <div class="form-group">
	          		<label class="col-sm-3 control-label"><?php echo __('Reviewer'); ?></label>
						<div class="col-sm-6">
			                    <select class="form-control" name="reviewer_id" id="combobox" required="required">
							      <?php
							      if(count($array_user) > 0){
							      	foreach ($array_user as $key => $value) {
											echo "<option value='".$value['user_id'] ."'>" .$value['name']. "</option>";
										}
							      }
							    	
								?>
								</select>		
								<!-- <input type='text' name='reviewer_id' id='reviewer_ids' class="form-control" /> -->
						</div>
					</div>
					<!-- <div class="form-group">
	          		<label class="col-sm-3 control-label"><?php //echo __('Reviewer'); ?></label>
						<div class="col-sm-6">	
								<input type='text' name='reviewer_ids' id='reviewer_ids' class="form-control" />
						</div>
					</div> -->
	          </div>

			 <div class="panel-footer">
				<button type="reset" class="btn btn-danger"><?php echo __('Reset'); ?></button>
				<button type="submit" class="btn btn-primary" name="submitbuttonname" value="submitbuttonvalue" id="submitbuttonname10"><?php echo __('Submit'); ?></button>
			</div>
			</form>

         </div>
        </div>

        <!-- Built-In report 11: Report of the data integrity of all archived Construction Permit Requests.
	    <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse13">
					  Checksums
				  </a>
                </h4>
              </div>
              <div id="collapse13" class="panel-collapse collapse">
              <div class="panel-body panel-body-nopadding">
		<form class="form-horizontal form-bordered" method="post" action="<?php echo public_path(); ?>backend.php/reports/printreport11" autocomplete="off" data-ajax="false">
        <div class="panel-heading-inline panel-heading-inline-gray">
            <h3 class="panel-title">Report of the data integrity of all archived Construction Permit Requests.</h3>
            </div>
			 <div class="form-group">
						<?php
							echo $menuoptions;
						?>
				</div>
                </div>
			 <div class="panel-footer">
				<button type="reset" class="btn btn-danger">Reset</button>
				<button type="submit" class="btn btn-primary" name="submitbuttonname" value="submitbuttonvalue">Submit</button>
			</div>
			</form>
         </div>
        </div>

        -->

        <!-- Built-In report 12: Report of the income from confirmed payments. -->
	    <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse14">
					  <?php echo __('Financial Report'); ?>
				  </a>
                </h4>
              </div>
              <div id="collapse14" class="panel-collapse collapse">
              <div class="panel-body panel-body-nopadding">
			<form class="form-horizontal form-bordered fh-special" method="post" action="<?php echo public_path(); ?>backend.php/invoices/report" autocomplete="off" data-ajax="false">
            <div class="panel-heading-inline panel-heading-inline-gray">
            <h3 class="panel-title"><?php echo __('Report of the income from confirmed payments'); ?></h3>
            </div>
			 <div class="form-group">
              <label class="col-sm-3 control-label"><?php echo __('Type Of Application'); ?></label>
			  	<div class="col-sm-6">
				<select id='application_form' name='application_form' >
			    <?php
					echo $formoptions;
				?>
			   </select>
			</div>
			</div>
			 <div class="form-group">
              <label class="col-sm-3 control-label"><?php echo __('From Date'); ?></label>
					<div class="col-sm-6">
                       <div class="input-group">
						<input type='text' name='fromdate' id='from_date12' class="form-control">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                       </div>
					</div>
				</div>
			 <div class="form-group">
              <label class="col-sm-3 control-label"><?php echo __('To Date'); ?></label>
					<div class="col-sm-6">
                     <div class="input-group">
						<input type='text' name='todate' id='to_date12' class="form-control">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                         </div>
					</div>
				</div>
                </div>
			 <div class="panel-footer">
				<button type="reset" class="btn btn-danger"><?php echo __('Reset'); ?></button>
				<button type="submit" class="btn btn-primary" name="export" value="export"><?php echo __('Submit'); ?></button>
			</div>
			</form>
         </div>
        </div>

        <!-- Built-In report 19: Report of the income from confirmed payments. -->
	    <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse19">
					  <?php echo __('Convenience Fee Report'); ?>
				  </a>
                </h4>
              </div>
              <div id="collapse19" class="panel-collapse collapse">
              <div class="panel-body panel-body-nopadding">
			<form class="form-horizontal form-bordered fh-special" method="post" action="<?php echo public_path(); ?>backend.php/invoices/convenience" autocomplete="off" data-ajax="false">
            <div class="panel-heading-inline panel-heading-inline-gray">
            <h3 class="panel-title"><?php echo __('Report of the income from confirmed payments'); ?></h3>
            </div>
			 <div class="form-group">
              <label class="col-sm-3 control-label"><?php echo __('Type Of Application'); ?></label>
			  	<div class="col-sm-6">
				<select id='application_form' name='application_form' >
			    <?php
					echo $formoptions;
				?>
			   </select>
			</div>
			</div>
			 <div class="form-group">
              <label class="col-sm-3 control-label"><?php echo __('From Date'); ?></label>
					<div class="col-sm-6">
                       <div class="input-group">
						<input type='text' name='fromdate' id='from_date19' class="form-control">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                       </div>
					</div>
				</div>
			 <div class="form-group">
              <label class="col-sm-3 control-label"><?php echo __('To Date'); ?></label>
					<div class="col-sm-6">
                     <div class="input-group">
						<input type='text' name='todate' id='to_date19' class="form-control">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                         </div>
					</div>
				</div>
                </div>
			 <div class="panel-footer">
				<button type="reset" class="btn btn-danger"><?php echo __('Reset'); ?></button>
				<button type="submit" class="btn btn-primary" name="export" value="export"><?php echo __('Submit'); ?></button>
			</div>
			</form>
         </div>
        </div>

        <!-- Built-In report 17: Detail Transaction Report. -->
	    <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse17">
					  <?php echo __('Detailed Transactions Report'); ?>
				  </a>
                </h4>
              </div>
              <div id="collapse17" class="panel-collapse collapse">
              <div class="panel-body panel-body-nopadding">
			<form class="form-horizontal form-bordered fh-special" method="post" action="<?php echo public_path(); ?>backend.php/reports/report17" autocomplete="off" data-ajax="false">
            <div class="panel-heading-inline panel-heading-inline-gray">
            <h3 class="panel-title"><?php echo __('This report gives details per transaction processed by PSPs. The users should be able to query the report based on the following criteria'); ?></h3>
            </div>
			 <div class="form-group">
              <label class="col-sm-3 control-label"><?php echo __('MDA'); ?></label>
			  	<div class="col-sm-6">
				<select id='payment_mda' name='payment_mda' >
			    <?php
					echo $mdaoptions;
				?>
			   </select>
			</div>
			</div>
			 <div class="form-group">
              <label class="col-sm-3 control-label"><?php echo __('Type of PSP'); ?></label>
			  	<div class="col-sm-6">
				<select id='payment_psp' name='payment_psp' >
			    <?php
					echo $pspoptions;
				?>
			   </select>
			</div>
			</div>
			 <div class="form-group">
              <label class="col-sm-3 control-label"><?php echo __('Service Code'); ?></label>
			  	<div class="col-sm-6">
				<select id='application_form' name='application_form' >
			    <?php
					echo $formoptions;
				?>
			   </select>
			</div>
			</div>
			 <div class="form-group">
              <label class="col-sm-3 control-label"><?php echo __('Period - From Date'); ?></label>
					<div class="col-sm-6">
                       <div class="input-group">
						<input type='text' name='fromdate' id='from_date17' class="form-control">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                       </div>
					</div>
				</div>
			 <div class="form-group">
              <label class="col-sm-3 control-label"><?php echo __('Period - To Date'); ?></label>
					<div class="col-sm-6">
                     <div class="input-group">
						<input type='text' name='todate' id='to_date17' class="form-control">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                         </div>
					</div>
				</div>
                </div>
			 <div class="panel-footer">
				<button type="reset" class="btn btn-danger"><?php echo __('Reset'); ?></button>
				<button type="submit" class="btn btn-primary" name="export" value="export"><?php echo __('Submit'); ?></button>
			</div>
			</form>
         </div>
        </div>

        <!-- Built-In report 18: Summary Collection and Remittance Report. -->
	    <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse18">
					  <?php echo __('Summary Collection and Remittance Report'); ?>
				  </a>
                </h4>
              </div>
              <div id="collapse18" class="panel-collapse collapse">
              <div class="panel-body panel-body-nopadding">
			<form class="form-horizontal form-bordered fh-special" method="post" action="<?php echo public_path(); ?>backend.php/reports/report18" autocomplete="off" data-ajax="false">
            <div class="panel-heading-inline panel-heading-inline-gray">
            <h3 class="panel-title"><?php echo __('The report will be used for remittances to MDA banks. It should aggregate all transactions per service code. The users should be able to query the report based on the following criteria'); ?></h3>
            </div>
			 <div class="form-group">
              <label class="col-sm-3 control-label"><?php echo __('MDA'); ?></label>
			  	<div class="col-sm-6">
				<select id='payment_mda' name='payment_mda' >
			    <?php
					echo $mdaoptions;
				?>
			   </select>
			</div>
			</div>
			 <div class="form-group">
              <label class="col-sm-3 control-label"><?php echo __('Type of PSP'); ?></label>
			  	<div class="col-sm-6">
				<select id='payment_psp' name='payment_psp' >
			    <?php
					echo $pspoptions;
				?>
			   </select>
			</div>
			</div>
			 <div class="form-group">
              <label class="col-sm-3 control-label"><?php echo __('Service Code'); ?></label>
			  	<div class="col-sm-6">
				<select id='application_form' name='application_form' >
			    <?php
					echo $formoptions;
				?>
			   </select>
			</div>
			</div>
			 <div class="form-group">
              <label class="col-sm-3 control-label"><?php echo __('Period - From Date'); ?></label>
					<div class="col-sm-6">
                       <div class="input-group">
						<input type='text' name='fromdate' id='from_date18' class="form-control">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                       </div>
					</div>
				</div>
			 <div class="form-group">
              <label class="col-sm-3 control-label"><?php echo __('Period - To Date'); ?></label>
					<div class="col-sm-6">
                     <div class="input-group">
						<input type='text' name='todate' id='to_date18' class="form-control">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                         </div>
					</div>
				</div>
                </div>
			 <div class="panel-footer">
				<button type="reset" class="btn btn-danger"><?php echo __('Reset'); ?></button>
				<button type="submit" class="btn btn-primary" name="export" value="export"><?php echo __('Submit'); ?></button>
			</div>
			</form>
         </div>
        </div>


		<?php
		$count = 12;

		$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
		mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

		$sql = "SELECT * FROM ap_report_elements WHERE chart_status = 1 ORDER BY chart_title ASC";
		$result = mysql_query($sql, $dbconn);

		$count = 0;

		while($report = mysql_fetch_assoc($result))
		{
			$count++;
			?>
            <!-- Built-In report <?php $count; ?>: Agenda Report. -->
	    <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapsec<?php echo $report['chart_id']; ?>">
					  <?php echo $report['chart_title']; ?>
				  </a>
                </h4>
              </div>
              <div id="collapsec<?php echo $report['chart_id']; ?>" class="panel-collapse collapse">
              <div class="panel-body panel-body-nopadding">
				<form class="form-horizontal form-bordered fh-special" method="post" action="" autocomplete="off" data-ajax="false">
					 <div class="panel-footer">
									<a class="btn btn-primary" target="_blank" href="<?php echo public_path(); ?>backend.php/forms/widget?key=<?php echo $report['access_key']; ?>"><?php echo __('View Report'); ?></a>
								</div>
				</form>

	         </div>
	        </div>

		</div>
			<?php
		}
		?>

		<?php
        $q = Doctrine_Query::create()
			  ->from('Reports a')
			  ->orderBy('a.title ASC');
	    $reports = $q->execute();

		$count = 0;
		foreach($reports as $report)
		{
			$count++;
			?>
            <?php $count; ?>
            <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapsecj<?php echo $report->getId(); ?>">
                          <?php echo $report->getTitle(); ?>
                      </a>
                    </h4>
                  </div>
                  <div id="collapsecj<?php echo $report->getId(); ?>" class="panel-collapse collapse">
                  <div class="panel-body panel-body-nopadding">
                    <form class="form-horizontal form-bordered fh-special" method="post" action="<?php echo public_path(); ?>backend.php/jsonreports/view?id=<?php echo $report->getId(); ?>&page=1" autocomplete="off" data-ajax="false">

						<div class="form-group">
							<label class="col-sm-3 control-label"><?php echo __('From Date'); ?></label>
							<div class="col-sm-6">
													<div class="input-group">
								<input type='text' name='from_date_filter' id='from_date_json<?php echo $report->getId(); ?>' class="form-control" required>
														<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
													</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label"><?php echo __('To Date'); ?></label>
							<div class="col-sm-6">
												<div class="input-group">
								<input type='text' name='to_date_filter' id='to_date_json<?php echo $report->getId(); ?>' class="form-control" required>
														<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
													</div>
							</div>
						</div>

                         <div class="panel-footer">
                            <button class="btn btn-primary" type="submit"><?php echo __('View Report'); ?></button>
                        </div>
                    </form>

                 </div>
                </div>

            </div>
            <script>
                jQuery(document).ready(function(){
                  jQuery('#from_date_json<?php echo $report->getId(); ?>').datepicker();
                  jQuery('#to_date_json<?php echo $report->getId(); ?>').datepicker();
                });
            </script>
			<?php
		}
		?>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>


<!-- The following functions ensure that for forms containing checkboxes or radiobuttons, atleast one option
    must be selected before the form can be submitted.
 -->


<script language="javascript">
	function atLeastOneRadio() {
	        return ($('input[type=radio]:checked').size() > 0);
	}
	
</script>
<script language="javascript">
function atLeastOneCheckbox() {
        return ($('input[type=checkbox]:checked').size() > 0);
}
</script>
<?php
}
else
{
	include_partial("settings/accessdenied");
}
?>
