<?php
/**
 * report1 template.
 *
 * Report of all applications that have been submitted within a specified time period and their status
 *
 * @package    backend
 * @subpackage reports
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
 
/**
*
* Function to get all the dates between a period of time
*
* @param String $sStartDate Start date to begin fetching dates from
* @param String $sEndDate End date where to stop fetching dates from
*
* @return String[]
*/
function GetDays($sStartDate, $sEndDate){  
    $aDays[] = $start_date;
	$start_date  = $sStartDate;
	$end_date = $sEndDate;
	$current_date = $start_date;
	while(strtotime($current_date) <= strtotime($end_date))
	{
		$aDays[] = gmdate("Y-m-d", strtotime("+1 day", strtotime($current_date)));
		$current_date = gmdate("Y-m-d", strtotime("+2 day", strtotime($current_date)));
	}

  
  return $aDays;  
} 


$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

?> 

<div class="pageheader">
  <h2><i class="fa fa-file-text"></i> Reports <span>Submissions Report</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>backend.php/dashboard">Home</a></li>
      <li>Reports</li>
      <li>Submissions Report</li>
    </ol>
  </div>
</div>

<div class="contentpanel">
  


    <form style="margin-bottom: 0px;">
        <fieldset>
            <label style='height: 30px; margin-top: 0px;'>
             <div style='float: left; font-size: 20px; font-weight: 700;'>Submissions Report</div>
             <div style="float: right;">
                <a href="/backend.php/reports/printreport1/form/<?php echo $_POST['application_form']; ?>/startdate/<?php echo $fromdate; ?>/enddate/<?php echo $todate; ?>"><u>Export to Excel</u></a>
             </div>
            </label>
                <div class="g12">
					<?php 
                        $days = GetDays($fromdate, $todate); 
                        
                        if($_POST['application_form'] != "0")
                        {
                        
                            $q = Doctrine_Query::create()
                              ->from('ApForms a')
                              ->where('a.form_id = ?',$_POST['application_form'])
                              ->orderBy('a.form_name ASC');
                            $appforms = $q->execute();
                            
                            $resultsize = 0;
                            
                            foreach($appforms as $form)
                            {
                                $query = "SELECT * FROM ap_form_".$form->getFormId()." a LEFT JOIN form_entry b ON a.id = b.entry_id WHERE a.date_created BETWEEN '".$fromdate."' AND '".$todate."' AND b.form_id = ".$form->getFormId()." AND b.approved <> 0 AND b.parent_submission = 0";
                                
                                $results = mysql_query($query,$dbconn);
                                $resultsize = $resultsize + mysql_num_rows($results);
                                
                            }
                        
                            echo "&nbsp; &nbsp;".$resultsize." entries found. <br> <br>";
                        
                        
                            //Use Days in Table Headers
                            
                            $tableRows = "";
                            $tableHeaders = "";
                            
                            $form_count =0;
                            
                            foreach($appforms as $form)
                            {
                                $form_count++;
                            
                                
                                $q = Doctrine_Query::create()
                                     ->from('ApForms a')
                                     ->where('a.form_id = ?', $form->getFormId());
                                $form = $q->fetchOne();
                                if($form)
                                {
                                    $tableRows[$form->getFormId()][] = "<th>".$form->getFormDescription()."</th>";
                                }
                                //If < 15 (days)
                                if(sizeof($days) <= 15)
                                {
                                    foreach($days as $day)
                                    {
                                       if(strlen($day) > 0)
                                       {
                                            if($form_count == 1)
                                            {
                                            $tableHeaders[] = "<th>".$day."</th>";
                                            }
                                                $dayquery = "SELECT * FROM ap_form_".$form->getFormId()." a LEFT JOIN form_entry b ON a.id = b.entry_id WHERE a.date_created LIKE '%".$day."%' AND b.form_id = ".$form->getFormId()." AND b.approved <> 0 AND b.parent_submission = 0";
                                                
                                                $q = Doctrine_Query::create()
                                                  ->from('ApFormElements a')
                                                  ->where('a.form_id = ?', $form->getFormId())
                                                  ->andWhere('a.element_type <> ? AND a.element_type <> ?', array('section','file'))
                                                  ->orderBy('a.element_position ASC');
                                                $fields = $q->execute();
                                                
                                                foreach($fields as $field)
                                                {
                                                    if($_POST['element_'.$field->getElementId()] != "")
                                                    {
                                                        $dayquery = $dayquery." AND ";
                                                        $dayquery = $dayquery."a.element_".$field->getElementId()." = ".$_POST['element_'.$field->getElementId()];
                                                        
                                                    }
                                                }
                                                $dayapps = mysql_query($dayquery,$dbconn);
                                                
                                                
                                            $tableRows[$form->getFormId()][] = "<td>".mysql_num_rows($dayapps)."</td>";
                                        }
                                    }
                                }
                                else if(sizeof($days) > 15 && sizeof($days) <= 40)//If < 40 (weeks)
                                {
                                    $dayofweek = 1;
                                    $total = 0;
                                    $header = "";
                                    foreach($days as $day)
                                    {
                                      if(strlen($day) > 0)
                                      {
                                        if($dayofweek == 1)
                                        {
                                            $header = $day;
                                        }
                                        if($dayofweek == 7 || (sizeof($days) == $total))
                                        {
                                            $dayquery = "SELECT * FROM ap_form_".$form->getFormId()." a LEFT JOIN form_entry b ON a.id = b.entry_id WHERE a.date_created BETWEEN '".$header."' AND '".$day."' AND b.form_id = ".$form->getFormId()." AND b.approved <> 0 AND b.parent_submission = 0";
                                            $q = Doctrine_Query::create()
                                              ->from('ApFormElements a')
                                              ->where('a.form_id = ?', $form->getFormId())
                                              ->andWhere('a.element_type <> ? AND a.element_type <> ?', array('section','file'))
                                              ->orderBy('a.element_position ASC');
                                            $fields = $q->execute();
                                            
                                            foreach($fields as $field)
                                            {
                                                if($_POST['element_'.$field->getElementId()] != "")
                                                {
                                                    $dayquery = $dayquery." AND ";
                                                    $dayquery = $dayquery."a.element_".$field->getElementId()." = ".$_POST['element_'.$field->getElementId()];
                                                    
                                                }
                                            }
                                            $dayapps = mysql_query($dayquery,$dbconn);
                                            $header = $header." to ".$day;
                                            if($form_count == 1)
                                            {
                                            $tableHeaders[] = "<th>".$header."</th>";
                                            }
                                            $tableRows[$form->getFormId()][] = "<td>".mysql_num_rows($dayapps)."</td>";
                                            $dayofweek = 0;
                                        }
                                        $total++;
                                        $dayofweek++;
                                       }
                                    }
                                }
                                else if(sizeof($days) > 40 && sizeof($days) <= 365)//If < 12 (months)
                                {
                                    $dayofmonth = 1;
                                    $total = 0;
                                    $header = "";
                                    foreach($days as $day)
                                    {
                                      if(strlen($day) > 0)
                                      {
                                        if($dayofmonth == 1)
                                        {
                                            $header = $day;
                                        }
                                        if($dayofmonth == 30 || (sizeof($days) == $total))
                                        {
                                            $dayquery = "SELECT * FROM ap_form_".$form->getFormId()."  a LEFT JOIN form_entry b ON a.id = b.entry_id WHERE a.date_created BETWEEN '".$header."' AND '".$day."' AND b.form_id = ".$form->getFormId()." AND b.approved <> 0 and b.parent_submission = 0";
                                            $q = Doctrine_Query::create()
                                              ->from('ApFormElements a')
                                              ->where('a.form_id = ?', $form->getFormId())
                                              ->andWhere('a.element_type <> ? AND a.element_type <> ?', array('section','file'))
                                              ->orderBy('a.element_position ASC');
                                            $fields = $q->execute();
                                            
                                            foreach($fields as $field)
                                            {
                                                if($_POST['element_'.$field->getElementId()] != "")
                                                {
                                                    $dayquery = $dayquery." AND ";
                                                    $dayquery = $dayquery."a.element_".$field->getElementId()." = ".$_POST['element_'.$field->getElementId()];
                                                    
                                                }
                                            }
                                            $dayapps = mysql_query($dayquery,$dbconn);
                                            $header = $header." to ".$day;
                                            if($form_count == 1)
                                            {
                                            $tableHeaders[] = "<th>".$header."</th>";
                                            }
                                            $tableRows[$form->getFormId()][] = "<td>".mysql_num_rows($dayapps)."</td>";
                                            $dayofmonth = 0;
                                        }
                                        $total++;
                                        $dayofmonth++;
                                       }
                                    }
                                }
                                else if(sizeof($days) > 365)//If > 12 (years)
                                {
                                    $dayofyear = 1;
                                    $total = 0;
                                    $header = "";
                                    foreach($days as $day)
                                    {
                                      if(strlen($day) > 0)
                                      {
                                        if($dayofyear == 1)
                                        {
                                            $header = $day;
                                        }
                                        if($dayofyear == 365 || (sizeof($days) == $total))
                                        {
                                            $dayquery = "SELECT * FROM ap_form_".$form->getFormId()."  a LEFT JOIN form_entry b ON a.id = b.entry_id WHERE a.date_created BETWEEN '".$header."' AND '".$day."' AND b.form_id = ".$form->getFormId()." AND b.approved <> 0 AND b.parent_submission = 0";
                                            $q = Doctrine_Query::create()
                                              ->from('ApFormElements a')
                                              ->where('a.form_id = ?', $form->getFormId())
                                              ->andWhere('a.element_type <> ? AND a.element_type <> ?', array('section','file'))
                                              ->orderBy('a.element_position ASC');
                                            $fields = $q->execute();
                                            
                                            foreach($fields as $field)
                                            {
                                                if($_POST['element_'.$field->getElementId()] != "")
                                                {
                                                    $dayquery = $dayquery." AND ";
                                                    $dayquery = $dayquery."a.element_".$field->getElementId()." = ".$_POST['element_'.$field->getElementId()];
                                                    
                                                }
                                            }
                                            $dayapps = mysql_query($dayquery,$dbconn);
                                            $header = $header." to ".$day;
                                            if($form_count == 1)
                                            {
                                            $tableHeaders[] = "<th>".$header."</th>";
                                            }
                                            $tableRows[$form->getFormId()][] = "<td>".mysql_num_rows($dayapps)."</td>";
                                            $dayofyear = 0;
                                        }
                                        $total++;
                                        $dayofyear++;
                                       }
                                    }
                                }
                            }
             ?>
			<table class="chart">
			<thead>
				<tr>
				    <th></th>
					<?php
					foreach($tableHeaders as $tableheader)
					{
						echo $tableheader;
					}
					?>
				</tr>
			</thead>
			<tbody>
			    <?php 
				foreach($appforms as $form)
				{
				?>
				<tr>
					<?php
					foreach($tableRows[$form->getFormId()] as $row)
					{
						echo $row;
					}
					?>
				</tr>
				<?php
				}
				?>
			</tbody>
		</table>
			
			<table>
				<thead>
					<tr><th>Type</th><th>No</th><th>Submitted On</th><th>Submitted By</th><th>Status</th><th style="background: none;">Actions</th>
					</tr>
				</thead>
				<tbody>
				
		    <?php
			$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
			mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
			
				foreach($appforms as $form)
				{
						$query = "SELECT a.id as id, a.date_created as date_created FROM ap_form_".$form->getFormId()."  a LEFT JOIN form_entry b ON a.id = b.entry_id WHERE a.date_created BETWEEN '".$fromdate."' AND '".$todate."' AND b.form_id = ".$form->getFormId()." AND b.approved <> 0 AND b.parent_submission = 0";
							
						$q = Doctrine_Query::create()
						  ->from('ApFormElements a')
						  ->where('a.form_id = ?', $form->getFormId())
						  ->andWhere('a.element_type <> ? AND a.element_type <> ?', array('section','file'))
						  ->orderBy('a.element_position ASC');
						$fields = $q->execute();
						
						foreach($fields as $field)
						{
							if($_POST['element_'.$field->getElementId()] != "")
							{
								$query = $query." AND ";
								$query = $query."a.element_".$field->getElementId()." = ".$_POST['element_'.$field->getElementId()];
								
							}
						}
						
						
						$results = mysql_query($query,$dbconn);
						
						?>
						<br>
						<?php
						while($row = mysql_fetch_assoc($results))
						{
							$q = Doctrine_Query::create()
							  ->from('FormEntry a')
							  ->where('a.form_id = ?', $form->getFormId())
							  ->andWhere('a.entry_id = ?', $row['id'])
							  ->andWhere('a.approved <> ?', '0')
							  ->andWhere('a.parent_submission = ?','0');
							$application = $q->fetchOne();
							if($application)
							{
							?>
							<tr id="row_<?php echo $application->getId() ?>">
									  
									  
									<td><?php 
									$q = Doctrine_Query::create()
										 ->from('ApForms a')
										 ->where('a.form_id = ?', $application->getFormId());
									$form = $q->fetchOne();
									if($form)
									{
										echo $form->getFormDescription();
									}
									else
									{
										echo "-";
									}
									?></td>
									<td><?php echo $application->getApplicationId(); ?></td>
									<td><?php
										echo $row['date_created'];
									?></td>
									<td class="c">
									<?php
										$q = Doctrine_Query::create()
										 ->from('sfGuardUserProfile a')
										 ->where('a.user_id = ?', $application->getUserId());
									$userprofile = $q->fetchOne();
									if($userprofile)
									{
										echo $userprofile->getFullname();
									}
									else
									{
										echo "-";
									}
									?>
									</td>
									<td class="c">
									<?php
										 $q = Doctrine_Query::create()
											->from('SubMenus a')
											->where('a.id = ?', $application->getApproved());
										$submenu = $q->fetchOne();
										echo $submenu->getTitle();
									?>
									</td>
									<td class="c">
									<a title='View Application' href='<?php echo public_path(); ?>backend.php/applications/view/id/<?php echo $application->getId(); ?>'><img src='<?php echo public_path(); ?>assets_backend/images/icons/dark/create_write.png'></a>
									</td>
								</tr>
							<?php
							}
						}
				}
			?>
			</tbody>
			</table>
			
			</p>

	</div>
	
 
<?php
}
else
{

$q = Doctrine_Query::create()
  ->from('ApForms a')
  ->where('a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ?',array('6','7','15','16','17'))
  ->orderBy('a.form_name ASC');
$appforms = $q->execute();

$resultsize = 0;

foreach($appforms as $form)
{
	$query = "SELECT * FROM ap_form_".$form->getFormId()." a LEFT JOIN form_entry b ON a.id = b.entry_id WHERE a.date_created BETWEEN '".$fromdate."' AND '".$todate."' AND b.form_id = ".$form->getFormId()." AND b.approved <> 0 AND b.parent_submission = 0";
	
	$results = mysql_query($query,$dbconn);
	$resultsize = $resultsize + mysql_num_rows($results);
	
}




echo "&nbsp; &nbsp;".$resultsize." entries found. <br>";


	//Use Days in Table Headers
	
	$tableRows = "";
	$tableHeaders = "";
	
	$form_count =0;
	
	foreach($appforms as $form)
	{
		$form_count++;
	
		
		$q = Doctrine_Query::create()
			 ->from('ApForms a')
			 ->where('a.form_id = ?', $form->getFormId());
		$form = $q->fetchOne();
		if($form)
		{
			$tableRows[$form->getFormId()][] = "<th>".$form->getFormDescription()."</th>";
		}
		//If < 15 (days)
		if(sizeof($days) <= 15)
		{
			foreach($days as $day)
			{
			   if(strlen($day) > 0)
			   {
					if($form_count == 1)
					{
					$tableHeaders[] = "<th>".$day."</th>";
					}
					    $dayquery = "SELECT * FROM ap_form_".$form->getFormId()." a LEFT JOIN form_entry b ON a.id = b.entry_id WHERE a.date_created LIKE '%".$day."%' AND b.form_id = ".$form->getFormId()." AND b.approved <> 0 AND b.parent_submission = 0";
						
						$q = Doctrine_Query::create()
						  ->from('ApFormElements a')
						  ->where('a.form_id = ?', $form->getFormId())
						  ->andWhere('a.element_type <> ? AND a.element_type <> ?', array('section','file'))
						  ->orderBy('a.element_position ASC');
						$fields = $q->execute();
						
						foreach($fields as $field)
						{
							if($_POST['element_'.$field->getElementId()] != "")
							{
								$dayquery = $dayquery." AND ";
								$dayquery = $dayquery."element_".$field->getElementId()." = ".$_POST['element_'.$field->getElementId()];
								
							}
						}
						$dayapps = mysql_query($dayquery,$dbconn);
						
						
					$tableRows[$form->getFormId()][] = "<td>".mysql_num_rows($dayapps)."</td>";
				}
			}
		}
		else if(sizeof($days) > 15 && sizeof($days) <= 40)//If < 40 (weeks)
		{
			$dayofweek = 1;
			$total = 0;
			$header = "";
			foreach($days as $day)
			{
			  if(strlen($day) >= 0)
			  {
				if($dayofweek == 1)
				{
					$header = $day;
				}
				if($dayofweek == 7 || (sizeof($days) == $total))
				{
				    $dayquery = "SELECT * FROM ap_form_".$form->getFormId()." a LEFT JOIN form_entry b ON a.id = b.entry_id WHERE a.date_created BETWEEN '".$header."' AND '".$day."' AND b.form_id = ".$form->getFormId()." AND b.approved <> 0 AND b.parent_submission = 0";
					$q = Doctrine_Query::create()
					  ->from('ApFormElements a')
					  ->where('a.form_id = ?', $form->getFormId())
					  ->andWhere('a.element_type <> ? AND a.element_type <> ?', array('section','file'))
					  ->orderBy('a.element_position ASC');
					$fields = $q->execute();
					
					foreach($fields as $field)
					{
						if($_POST['element_'.$field->getElementId()] != "")
						{
							$dayquery = $dayquery." AND ";
							$dayquery = $dayquery."element_".$field->getElementId()." = ".$_POST['element_'.$field->getElementId()];
							
						}
					}
					$dayapps = mysql_query($dayquery,$dbconn);
					$header = $header." to ".$day;
					if($form_count == 1)
					{
					$tableHeaders[] = "<th>".$header."</th>";
					}
					$tableRows[$form->getFormId()][] = "<td>".mysql_num_rows($dayapps)."</td>";
					$dayofweek = 0;
				}
				$total++;
				$dayofweek++;
			   }
			}
		}
		else if(sizeof($days) > 40 && sizeof($days) <= 365)//If < 12 (months)
		{
			$dayofmonth = 1;
			$total = 0;
			$header = "";
			foreach($days as $day)
			{
			  if(strlen($day) > 0)
			  {
				if($dayofmonth == 1)
				{
					$header = $day;
				}
				if($dayofmonth == 30 || (sizeof($days) == $total))
				{
					$dayquery = "SELECT * FROM ap_form_".$form->getFormId()."  a LEFT JOIN form_entry b ON a.id = b.entry_id WHERE a.date_created BETWEEN '".$header."' AND '".$day."' AND b.form_id = ".$form->getFormId()." AND b.approved <> 0 AND b.parent_submission = 0";
					$q = Doctrine_Query::create()
					  ->from('ApFormElements a')
					  ->where('a.form_id = ?', $form->getFormId())
					  ->andWhere('a.element_type <> ? AND a.element_type <> ?', array('section','file'))
					  ->orderBy('a.element_position ASC');
					$fields = $q->execute();
					
					foreach($fields as $field)
					{
						if($_POST['element_'.$field->getElementId()] != "")
						{
							$dayquery = $dayquery." AND ";
							$dayquery = $dayquery."element_".$field->getElementId()." = ".$_POST['element_'.$field->getElementId()];
							
						}
					}
					$dayapps = mysql_query($dayquery,$dbconn);
					$header = $header." to ".$day;
					if($form_count == 1)
					{
					$tableHeaders[] = "<th>".$header."</th>";
					}
					$tableRows[$form->getFormId()][] = "<td>".mysql_num_rows($dayapps)."</td>";
					$dayofmonth = 0;
				}
				$total++;
				$dayofmonth++;
			   }
			}
		}
		else if(sizeof($days) > 365)//If > 12 (years)
		{
			$dayofyear = 1;
			$total = 0;
			$header = "";
			foreach($days as $day)
			{
			  if(strlen($day) > 0)
			  {
				if($dayofyear == 1)
				{
					$header = $day;
				}
				if($dayofyear == 365 || (sizeof($days) == $total))
				{
					$dayquery = "SELECT * FROM ap_form_".$form->getFormId()."  a LEFT JOIN form_entry b ON a.id = b.entry_id WHERE a.date_created BETWEEN '".$header."' AND '".$day."' AND b.form_id = ".$form->getFormId()." AND b.approved <> 0 AND b.parent_submission = 0";
					$q = Doctrine_Query::create()
					  ->from('ApFormElements a')
					  ->where('a.form_id = ?', $form->getFormId())
					  ->andWhere('a.element_type <> ? AND a.element_type <> ?', array('section','file'))
					  ->orderBy('a.element_position ASC');
					$fields = $q->execute();
					
					foreach($fields as $field)
					{
						if($_POST['element_'.$field->getElementId()] != "")
						{
							$dayquery = $dayquery." AND ";
							$dayquery = $dayquery."element_".$field->getElementId()." = ".$_POST['element_'.$field->getElementId()];
							
						}
					}
					$dayapps = mysql_query($dayquery,$dbconn);
					$header = $header." to ".$day;
					if($form_count == 1)
					{
					$tableHeaders[] = "<th>".$header."</th>";
					}
					$tableRows[$form->getFormId()][] = "<td>".mysql_num_rows($dayapps)."</td>";
					$dayofyear = 0;
				}
				$total++;
				$dayofyear++;
			   }
			}
		}
	
	}


?>
			
			<table class="chart">
			<thead>
				<tr>
				    <th></th>
					<?php
					foreach($tableHeaders as $tableheader)
					{
						echo $tableheader;
					}
					?>
				</tr>
			</thead>
			<tbody>
			    <?php 
				foreach($appforms as $form)
				{
				?>
				<tr>
					<?php
					foreach($tableRows[$form->getFormId()] as $row)
					{
						echo $row;
					}
					?>
				</tr>
				<?php
				}
				?>
			</tbody>
		</table>
			
			<table>
				<thead>
					<tr><th>Type</th><th>No</th><th>Submitted On</th><th>Submitted By</th><th>Status</th><th style="background: none;">Actions</th>
					</tr>
				</thead>
				<tbody>
				
		    <?php
			$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
			mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
			
	foreach($appforms as $form)
	{
			$query = "SELECT a.id as id FROM ap_form_".$form->getFormId()."  a LEFT JOIN form_entry b ON a.id = b.entry_id WHERE a.date_created BETWEEN '".$fromdate."' AND '".$todate."' AND b.form_id = ".$form->getFormId()." AND b.approved <> 0 AND b.parent_submission = 0";
				
			$q = Doctrine_Query::create()
			  ->from('ApFormElements a')
			  ->where('a.form_id = ?', $form->getFormId())
			  ->andWhere('a.element_type <> ? AND a.element_type <> ?', array('section','file'))
			  ->orderBy('a.element_position ASC');
			$fields = $q->execute();
			
			foreach($fields as $field)
			{
				if($_POST['element_'.$field->getElementId()] != "")
				{
					$query = $query." AND ";
				    $query = $query."element_".$field->getElementId()." = ".$_POST['element_'.$field->getElementId()];
					
				}
			}
			
			
			$results = mysql_query($query,$dbconn);
			
			?>
			<br>
			<?php
			while($row = mysql_fetch_assoc($results))
			{
				$q = Doctrine_Query::create()
				  ->from('FormEntry a')
				  ->where('a.form_id = ?', $form->getFormId())
				  ->andWhere('a.entry_id = ?', $row['id'])
				  ->andWhere('a.approved <> ?', '0')
				  ->andWhere("a.approved <> ? AND a.approved <> ? AND a.parent_submission = ?", array("0","","0"));
				$application = $q->fetchOne();
				if($application)
				{
				?>
				<tr id="row_<?php echo $application->getId() ?>">
						  
						  
						<td><?php 
						$q = Doctrine_Query::create()
						     ->from('ApForms a')
							 ->where('a.form_id = ?', $application->getFormId());
					    $form = $q->fetchOne();
						if($form)
						{
							echo $form->getFormDescription();
						}
						else
						{
							echo "-";
						}
						?></td>
						<td><?php echo $application->getApplicationId(); ?></td>
						<td><?php
							echo $row['date_created'];
						?></td>
						<td class="c">
						<?php
							$q = Doctrine_Query::create()
						     ->from('sfGuardUserProfile a')
							 ->where('a.user_id = ?', $application->getUserId());
					    $userprofile = $q->fetchOne();
						if($userprofile)
						{
							echo $userprofile->getFullname();
						}
						else
						{
							echo "-";
						}
						?>
						</td>
						<td class="c">
						<?php
							 $q = Doctrine_Query::create()
								->from('SubMenus a')
								->where('a.id = ?', $application->getApproved());
							$submenu = $q->fetchOne();
							echo $submenu->getTitle();
						?>
						</td>
						<td class="c">
						<a title='View Application' href='<?php echo public_path(); ?>backend.php/applications/view/id/<?php echo $application->getId(); ?>'><img src='<?php echo public_path(); ?>assets_backend/images/icons/dark/create_write.png'></a>
						</td>
					</tr>
				<?php
				}
			}
	}
			?>
			</tbody>
			</table>
			
			</p>

	</div>
	
 
<?php
}
?>
     </fieldset>
	</form>

</div>
