<?php
/**
 * edit template.
 *
 * Displays form for editing an existing custom report
 *
 * @package    backend
 * @subpackage reports
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>
<div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
  <strong>Well done!</strong> You successfully updated this report</a>.
</div>

<form id="reportform" class="form-bordered" action="/backend.php/reports/update/id/<?php echo $report->getId(); ?>" method="post" enctype="multipart/form-data" autocomplete="off" data-ajax="false">

<div class="panel panel-dark">
<div class="panel-heading">
			<h3 class="panel-title">Custom Reports</h3>
            <p>Edit <?php if($title){ echo $title; }else{ echo "Report"; } ?></p>
			<div class="pull-right">
            <a class="btn btn-primary-alt settings-margin42" id="newreport" href="#">New Reports</a>

            <script language="javascript">
            jQuery(document).ready(function(){
              $( "#newreport" ).click(function() {
                  $("#contentload").load("<?php echo public_path(); ?>backend.php/reports/new");
              });
            });
            </script>
</div>
</div>
		
 
<div class="panel panel-body panel-body-nopadding">


		      <div class="form-group">
				<label class="col-sm-4"><i class="bold-label">Title</i></label>
				<div class="col-sm-8">
				  <input class="form-control" type="text" name="title" id="title" value="<?php if($title){ echo $title; } ?>">
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-4"><i class="bold-label">Type Of Application</i></label>
				<div class="col-sm-8">
					<select id='application_form' name='application_form' onChange="window.location='/backend.php/reports/edit/id/<?php echo $report->getId(); ?>/formid/' + (this.value) + '/type/' + document.getElementById('rpttype').value + '/title/' + document.getElementById('title').value ;">
					<option value="0">Choose an application form...</option>
					<?php
					
						$q = Doctrine_Query::create()
						  ->from('ApForms a')
						  ->where('a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ?',array('6','7','15','16','17'))
						  ->orderBy('a.form_name ASC');
						$forms = $q->execute();
						
						foreach($forms as $form)
						{
						    $selected = "";
							if($formid == $form->getFormId())
							{
								$selected = "selected";
							}
							echo "<option value='".$form->getFormId()."' ".$selected.">".$form->getFormName()."</option>";
						}
					?>
				</select>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-4"><i class="bold-label">Report Type</i></label>
				<div class="col-sm-8">
				 <select name="rpttype" id="rpttype" onChange="if(this.value == '2'){ document.getElementById('ajaxContent').style.display = 'block'; document.getElementById('ajaxSelectelements').style.display = 'none'; }else{ document.getElementById('ajaxContent').style.display = 'none'; document.getElementById('ajaxSelectelements').style.display = 'block'; }">
					<option value="2" <?php if($type != "" && $type == 2){ echo "selected"; }?>>Single Entry Summary</option>
					<option value="1" <?php if($type != "" && $type == 1){ echo "selected"; }?>>Multiple Entry List</option>
				 </select>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-4"><i class="bold-label">Content</i></label>
                <div class="col-sm-8">
			     	 <div id='ajaxContent' name='ajaxContent'  <?php if($type != "" && $type == 2){ ?>style="display: block;"<?php }else{ if($type == "" ){ ?>style="display: block;"<?php } if($type == "1" ){ ?>style="display: none;"<?php } } ?>>
				 <textarea name="rptcontent" id="rptcontent" ><?php echo $report->getContent(); ?></textarea>
				   <div id="ajaxTags" name="ajaxTags" style='height: 250px; overflow-y: auto;' align="right">
				   <?php
					if($formid != "")
					{
				   ?>
						<table>
							<thead><tr><th>User Details</th><th>Tag</th></tr></thead>
							<tbody>
							<tr><td>Username</td><td>{sf_username}</td></tr>
							<tr><td>Email</td><td>{sf_email}</td></tr>
							<tr><td>Full Name</td><td>{sf_fullname}</td></tr>
							<?php
									$q = Doctrine_Query::create()
									   ->from('apFormElements a')
									   ->where('a.form_id = ?', 15);
									   
									$elements = $q->execute();
									
									foreach($elements as $element)
									{
										$childs = $element->getElementTotalChild();
										if($childs == 0)
										{
										   echo "<tr><td>".$element->getElementTitle()."</td><td>{sf_element_".$element->getElementId()."}</td></tr>";
										}
										else
										{
											if($element->getElementType() == "select")
											{
												echo "<tr><td>".$element->getElementTitle()."</td><td>{sf_element_".$element->getElementId()."}</td></tr>";
											}
											else
											{
												for($x = 0; $x < ($childs + 1); $x++)
												{
													echo "<tr><td>".$element->getElementTitle()."</td><td>{sf_element_".$element->getElementId()."_".($x+1)."}</td></tr>";
												}
											}
										}
									}
							?>
							</tbody>
							</table>
							<?php 
							//Get Application Information (anything starting with ap_ )
									   //ap_application_id
									
							//Get Form Details (anything starting with fm_ )
									   //fm_created_at, fm_updated_at.....fm_element_1
							?>
							<table>
							<thead><tr><th>Application Details</th><th>Tag</th></tr></thead>
							<tbody>
							<tr><td>Plan Registration Number</td><td>{ap_application_id}</td></tr>
							<tr><td>Created At</td><td>{fm_created_at}</td></tr>
							<tr><td>Approved At</td><td>{fm_updated_at}</td></tr>
							<?php

									$q = Doctrine_Query::create()
									   ->from('apFormElements a')
									   ->where('a.form_id = ?', $formid);
									   
									$elements = $q->execute();
									
									foreach($elements as $element)
									{
										$childs = $element->getElementTotalChild();
										if($childs == 0)
										{
										   echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."}</td></tr>";
										}
										else
										{
											if($element->getElementType() == "select")
											{
												echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."}</td></tr>";
											}
											else
											{
												for($x = 0; $x < ($childs + 1); $x++)
												{
													echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."_".($x+1)."}</td></tr>";
												}
											}
										}
									}
							?>
							</tbody>
							</table>

							<table>
							<thead><tr><th>Conditions Details</th><th>Tag</th></tr></thead>
							<tbody>
							<tr><td>Conditions Of Approval</td><td>{ca_conditions}</td></tr>
							<tr><td>Other Conditions</td><td>{other_conditions}</td></tr>
							</tbody>
							</table>

							<table>
							<thead><tr><th>Invoice Details</th><th>Tag</th></tr></thead>
							<tbody>
							<tr><td>Total</td><td>{in_total}</td></tr>
							</tbody>
							</table>
					<?php
					}
					?>
				   </div>
				 </div>
				      <div id='ajaxSelectelements' name='ajaxSelectelements' <?php if($type != "" && $type == 1){ ?>style="display: block;"<?php }else{ ?>style="display: none;"<?php } ?>>
					<?php
					if($formid != "")
					{
					?>
				 
				 <select name="fields" id="fields" multiple>
                         <?php
                         $selected = "";
                         $q = Doctrine_Query::create()
                             ->from('ReportFields a')
                             ->where('a.report_id = ?', $report->getId())
                             ->andWhere('a.element = ?', "{ap_application_id}");
                         $existing = $q->execute();
                         if(sizeof($existing) > 0)
                         {
                             $selected = "selected";
                         }
                         ?>
                         <option value='{ap_application_id}' <?php echo $selected; ?>>Application Number</option>
                         <?php
                         $selected = "";
                         $q = Doctrine_Query::create()
                             ->from('ReportFields a')
                             ->where('a.report_id = ?', $report->getId())
                             ->andWhere('a.element = ?', "{ap_application_status}");
                         $existing = $q->execute();
                         if(sizeof($existing) > 0)
                         {
                             $selected = "selected";
                         }
                         ?>
                         <option value='{ap_application_status}' <?php echo $selected; ?>>Application Status</option>
                         <?php
                         $selected = "";
                         $q = Doctrine_Query::create()
                             ->from('ReportFields a')
                             ->where('a.report_id = ?', $report->getId())
                             ->andWhere('a.element = ?', "{ap_date_of_submission}");
                         $existing = $q->execute();
                         if(sizeof($existing) > 0)
                         {
                             $selected = "selected";
                         }
                         ?>
                         <option value='{ap_date_of_submission}' <?php echo $selected; ?>>Date Of Submission</option>
                         <?php
                         $selected = "";
                         $q = Doctrine_Query::create()
                             ->from('ReportFields a')
                             ->where('a.report_id = ?', $report->getId())
                             ->andWhere('a.element = ?', "{ap_date_of_approval}");
                         $existing = $q->execute();
                         if(sizeof($existing) > 0)
                         {
                             $selected = "selected";
                         }
                         ?>
                         <option value='{ap_date_of_approval}' <?php echo $selected; ?>>Date Of Approval</option>
				            <?php
							$selected = "";
							$q = Doctrine_Query::create()
								 ->from('ReportFields a')
								 ->where('a.report_id = ?', $report->getId())
								 ->andWhere('a.element = ?', "{sf_email}");
							$existing = $q->execute();
							if(sizeof($existing) > 0)
							{
								$selected = "selected";
							}
							?>
							<option value='{sf_email}' <?php echo $selected; ?>>Applicant's Email</option>
							<?php
							$selected = "";
							$q = Doctrine_Query::create()
								 ->from('ReportFields a')
								 ->where('a.report_id = ?', $report->getId())
								 ->andWhere('a.element = ?', "{sf_fullname}");
							$existing = $q->execute();
							if(sizeof($existing) > 0)
							{
								$selected = "selected";
							}
							?>
							<option value='{sf_fullname}' <?php echo $selected; ?>>Applicant's Fullname</option>
							<?php
							$selected = "";
							$q = Doctrine_Query::create()
								 ->from('ReportFields a')
								 ->where('a.report_id = ?', $report->getId())
								 ->andWhere('a.element = ?', "{sf_username}");
							$existing = $q->execute();
							if(sizeof($existing) > 0)
							{
								$selected = "selected";
							}
							?>
							<option value='{sf_username}' <?php echo $selected; ?>>Applicant's Username</option>
							<?php
									$q = Doctrine_Query::create()
									   ->from('apFormElements a')
									   ->where('a.form_id = ?', 15);
									   
									$elements = $q->execute();
									
									foreach($elements as $element)
									{
										$childs = $element->getElementTotalChild();
										if($childs == 0)
										{
										   $selected = "";
										   $q = Doctrine_Query::create()
												 ->from('ReportFields a')
												 ->where('a.report_id = ?', $report->getId())
												 ->andWhere('a.element = ?', "{sf_element_".$element->getElementId()."}");
										    $existing = $q->execute();
											if(sizeof($existing) > 0)
											{
												$selected = "selected";
											}
										   echo "<option value='{sf_element_".$element->getElementId()."}' ".$selected.">".$element->getElementTitle()."</option>";
										}
										else
										{
											if($element->getElementType() == "select")
											{
												 $selected = "";
												   $q = Doctrine_Query::create()
														 ->from('ReportFields a')
														 ->where('a.report_id = ?', $report->getId())
														 ->andWhere('a.element = ?', "{sf_element_".$element->getElementId()."}");
													$existing = $q->execute();
													if(sizeof($existing) > 0)
													{
														$selected = "selected";
													}
												echo "<option value='{sf_element_".$element->getElementId()."}' ".$selected.">".$element->getElementTitle()."</option>";
											}
											else
											{
												for($x = 0; $x < ($childs + 1); $x++)
												{
													$selected = "";
													   $q = Doctrine_Query::create()
															 ->from('ReportFields a')
															 ->where('a.report_id = ?', $report->getId())
															 ->andWhere('a.element = ?', "{sf_element_".$element->getElementId()."_".($x+1)."}");
														$existing = $q->execute();
														if(sizeof($existing) > 0)
														{
															$selected = "selected";
														}
													echo "<option value='{sf_element_".$element->getElementId()."_".($x+1)."}' ".$selected.">".$element->getElementTitle()."</option>";
												}
											}
										}
									}
							?>
							<?php 
							//Get Application Information (anything starting with ap_ )
									   //ap_application_id
									
							//Get Form Details (anything starting with fm_ )
									   //fm_created_at, fm_updated_at.....fm_element_1
									   
									   $selected = "";
									   $q = Doctrine_Query::create()
											 ->from('ReportFields a')
											 ->where('a.report_id = ?', $report->getId())
											 ->andWhere('a.element = ?', "{ap_application_id}");
										$existing = $q->execute();
										if(sizeof($existing) > 0)
										{
											$selected = "selected";
										}
							?>
							<option value='{ap_application_id}' <?php echo $selected; ?>>Application Number</option>
							<?php
							$selected = "";
							$q = Doctrine_Query::create()
								 ->from('ReportFields a')
								 ->where('a.report_id = ?', $report->getId())
								 ->andWhere('a.element = ?', "{fm_created_at}");
							$existing = $q->execute();
							if(sizeof($existing) > 0)
							{
								$selected = "selected";
							}
							?>
							<option value='{fm_created_at}' <?php echo $selected; ?>>Submitted On</option>
							<?php

									$q = Doctrine_Query::create()
									   ->from('apFormElements a')
									   ->where('a.form_id = ?', $formid);
									   
									$elements = $q->execute();
									
									foreach($elements as $element)
									{
										$childs = $element->getElementTotalChild();
										if($childs == 0)
										{
											$selected = "";
											$q = Doctrine_Query::create()
												 ->from('ReportFields a')
												 ->where('a.report_id = ?', $report->getId())
												 ->andWhere('a.element = ?', "{fm_element_".$element->getElementId()."}");
											$existing = $q->execute();
											if(sizeof($existing) > 0)
											{
												$selected = "selected";
											}
										   echo "<option value='{fm_element_".$element->getElementId()."}' ".$selected.">".$element->getElementTitle()."</option>";
										}
										else
										{
											if($element->getElementType() == "select")
											{
												$selected = "";
												$q = Doctrine_Query::create()
													 ->from('ReportFields a')
													 ->where('a.report_id = ?', $report->getId())
													 ->andWhere('a.element = ?', "{fm_element_".$element->getElementId()."}");
												$existing = $q->execute();
												if(sizeof($existing) > 0)
												{
													$selected = "selected";
												}
												echo "<option value='{fm_element_".$element->getElementId()."}' ".$selected.">".$element->getElementTitle()."</option>";
											}
											else
											{
												for($x = 0; $x < ($childs + 1); $x++)
												{
													$selected = "";
													$q = Doctrine_Query::create()
														 ->from('ReportFields a')
														 ->where('a.report_id = ?', $report->getId())
														 ->andWhere('a.element = ?', "{fm_element_".$element->getElementId()."_".($x+1)."}");
													$existing = $q->execute();
													if(sizeof($existing) > 0)
													{
														$selected = "selected";
													}
													echo "<option value='{fm_element_".$element->getElementId()."_".($x+1)."}' ".$selected.">".$element->getElementTitle()."</option>";
												}
											}
										}
									}
									
							$selected = "";
							$q = Doctrine_Query::create()
								 ->from('ReportFields a')
								 ->where('a.report_id = ?', $report->getId())
								 ->andWhere('a.element = ?', "{ca_conditions}");
							$existing = $q->execute();
							if(sizeof($existing) > 0)
							{
								$selected = "selected";
							}		
							?>
							<option value="{ca_conditions}" <?php echo $selected; ?>>Conditions of Approval</option>
							<?php
							$selected = "";
							$q = Doctrine_Query::create()
								 ->from('ReportFields a')
								 ->where('a.report_id = ?', $report->getId())
								 ->andWhere('a.element = ?', "{mini_ca_conditions}");
							$existing = $q->execute();
							if(sizeof($existing) > 0)
							{
								$selected = "selected";
							}	
							?>
							<option value="{mini_ca_conditions}" <?php echo $selected; ?>>Minimized Conditions Approval</option>
							<?php
							$selected = "";
							$q = Doctrine_Query::create()
								 ->from('ReportFields a')
								 ->where('a.report_id = ?', $report->getId())
								 ->andWhere('a.element = ?', "{other_conditions}");
							$existing = $q->execute();
							if(sizeof($existing) > 0)
							{
								$selected = "selected";
							}	
							?>
							<option value="{other_conditions}" <?php echo $selected; ?>>Other Conditions</option>
							<?php
							$selected = "";
							$q = Doctrine_Query::create()
								 ->from('ReportFields a')
								 ->where('a.report_id = ?', $report->getId())
								 ->andWhere('a.element = ?', "{in_total}");
							$existing = $q->execute();
							if(sizeof($existing) > 0)
							{
								$selected = "selected";
							}	
							?>
							<option value="{in_total}" <?php echo $selected; ?>>Invoice Total Amount</option>
					</select>
						<?php
					}
					?>
				 </div>
			  <div class="form-group">
				<label class="col-sm-4"><i class="bold-label">Custom Headers</i></label>
				<div class="col-sm-8">
				<table>
				<thead>
				<tr>
				<th>Field</th><th>Custom Name</th>
				</tr>
				</thead>
				<tbody>
				<?php
				$parser = new Templateparser();
				
				$q = Doctrine_Query::create()
					 ->from('ReportFields a')
					 ->where('a.report_id = ?', $report->getId());
				$fields = $q->execute();
				foreach($fields as $field)
				{
				?>
				<tr><td><?php echo $parser->parseHeaders($report->getFormId(),$field->getElement()); ?></td><td><input type='text' name='headers[]' value='<?php echo $field->getCustomheader(); ?>'></td></tr>
				<?php
				}
				?>
				</tbody>
				</table>
				</div>
			  </div>	
			  <div class="form-group">
				<label class="col-sm-4"><i class="bold-label">Filter Status</i></label>
				<div class="col-sm-8">
					<select id='application_filter' name='application_filter'>
					<?php
					$q = Doctrine_Query::create()
					 ->from('ReportFilters a')
					 ->where('a.report_id = ?', $report->getId());
				   $filter = $q->fetchOne();
					
						$q = Doctrine_Query::create()
						  ->from('SubMenus a')
						  ->where('a.id <> 0 AND a.id <> 650 AND a.id <> 750  AND a.id <> 750  AND a.id <> 850  AND a.id <> 950')
						  ->orderBy('a.order_no ASC');
						$stages = $q->execute();
						
						foreach($stages as $stage)
						{
						    $selected = "";
							
							
							if($filter){
							if($filter->getValue() == $stage->getId())
							{
								$selected = "selected";
							}
							}
							
							echo "<option value='".$stage->getId()."' ".$selected.">".$stage->getTitle()."</option>";
						}
					?>
				    </select>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-4"><i class="bold-label">Show Actions Column</i></label>
				<div class="col-sm-8">
					<select id='application_actions' name='application_actions'>
					<option value="0" <?php if($filter != ""){ if($filter->getElementId() == 0){ echo "selected"; } } ?>>No</option>
					<option value="1" <?php if($filter != ""){ if($filter->getElementId() == 1){ echo "selected"; } } ?>>Yes</option>
				</select>
				</div>
			  </div>
              
              </div><!--panel-body-->
<div class="panel-footer">
<button class="btn btn-danger mr10">Reset</button> <button type="submit" class="btn btn-primary" name="submitbuttonname" id="submitbuttonname" value="submitbuttonvalue">Submit</button>
</div>
</fieldset>
</div>

</form>

<script language="javascript">
 jQuery(document).ready(function(){
	$("#submitbuttonname").click(function() {
		 $.ajax({
			url: '/backend.php/reports/update/id/<?php echo $report->getId(); ?>',
			cache: false,
			type: 'POST',
			data : $('#reportform').serialize(),
			success: function(json) {
				$('#alertdiv').attr("style", "display: block;");
				$("html, body").animate({ scrollTop: 0 }, "slow");
			}
		});
		return false;
	 });
	});
</script>