<?php
/**
 * new template.
 *
 * Displays form for creating a new custom report 
 *
 * @package    backend
 * @subpackage reports
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>

<div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
  <strong>Well done!</strong> You successfully updated this report</a>.
</div>

<form id="reportform" class="form-bordered" action="/backend.php/reports/create" method="post" enctype="multipart/form-data" autocomplete="off" data-ajax="false">

<div class="panel panel-dark">
<div class="panel-heading">
<h3 class="panel-title">New Report</h3>
</div>

<div class="panel-body panel-body-nopadding">



			 <div class="form-group">
				<label class="col-sm-4"><i class="bold-label">Title</i></label>
				<div class="col-sm-8">
				  <input class="form-control" type="text" name="title" id="title" value="<?php if($title){ echo $title; } ?>">
				</div>
			  </div>
			 <div class="form-group">
				<label class="col-sm-4"><i class="bold-label">Type Of Application</i></label>
				<div class="col-sm-8">
					<select id='application_form' name='application_form' onChange="window.location='/backend.php/reports/new/formid/' + (this.value) + '/type/' + document.getElementById('rpttype').value + '/title/' + document.getElementById('title').value ;">
					<option value="0">Choose an application form...</option>
					<?php

                    $q = Doctrine_Query::create()
                        ->from('ApForms a')
                        ->where('a.form_id <> 6 AND a.form_id <> 7 AND a.form_id <> 15 AND a.form_id <> 16 AND a.form_id <> 17 AND a.form_id <> 18 AND a.form_id <> 19');
                    $forms = $q->execute();
                    ?>
                            <?php
                            //Get list of all available applications categorized by groups
                            $q = Doctrine_Query::create()
                                ->from('FormGroups a');
                            $groups = $q->execute();

                            if(sizeof($groups) > 0)
                            {
                                foreach($groups as $group)
                                {
                                    echo "<optgroup label='".$group->getGroupName()."'>";

                                    $q = Doctrine_Query::create()
                                        ->from('ApForms a')
                                        ->leftJoin('a.ApFormGroups b')
                                        ->where('a.form_id = b.form_id')
                                        ->andWhere('b.group_id = ?', $group->getGroupId());
                                    $forms = $q->execute();

                                    $count = 0;

                                    foreach($forms as $apform)
                                    {
                                        $selected = "";

                                        if($application_form != "" && $application_form == $apform->getFormId())
                                        {
                                            $selected = "selected";
                                            $_GET['form'] = $application_form;
                                        }


                                        echo "<option value='".$apform->getFormId()."' ".$selected.">".$apform->getFormDescription()."</option>";

                                        $count++;
                                    }
                                    echo "</optgroup>";
                                }
                            }
                            else
                            {
                                $q = Doctrine_Query::create()
                                    ->from('ApForms a')
                                    ->where('a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ?',array('6','7','15','16','17'))
                                    ->orderBy('a.form_id ASC');
                                $forms = $q->execute();

                                $count = 0;

                                echo "<optgroup label='Application Forms'>";

                                foreach($forms as $apform)
                                {

                                    $selected = "";

                                    if($application_form != "" && $application_form == $apform->getFormId())
                                    {
                                        $selected = "selected";
                                        $_GET['form'] = $application_form;
                                    }

                                    if($selectedform != "" && $selectedform == $apform->getFormId())
                                    {
                                        $selected = "selected";
                                        $_GET['form'] = $selectedform;
                                    }



                                    echo "<option value='".$apform->getFormId()."' ".$selected.">".$apform->getFormName()."</option>";

                                    $count++;
                                }

                                echo "</optgroup>";
                            }
					?>>
				</select>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-4"><i class="bold-label">Report Type</i></label>
				<div class="col-sm-8">
				 <select name="rpttype" id="rpttype" onChange="if(this.value == '2'){ document.getElementById('ajaxContent').style.display = 'block'; document.getElementById('ajaxSelectelements').style.display = 'none'; }else{ document.getElementById('ajaxContent').style.display = 'none'; document.getElementById('ajaxSelectelements').style.display = 'block'; }">
					<option value="2" <?php if($type != "" && $type == 2){ echo "selected"; }?>>Single Entry Summary</option>
					<option value="1" <?php if($type != "" && $type == 1){ echo "selected"; }?>>Multiple Entry List</option>
				 </select>
				</div>
			  </div>
			  <div class="form-group">
				<label class="col-sm-4"><i class="bold-label">Content</i></label>
                <div class="col-sm-8">
				 <div id='ajaxContent' name='ajaxContent'  <?php if($type != "" && $type == 2){ ?>style="display: block;"<?php }else{ if($type == ""){ ?>style="display: block;"<?php } } ?>>
				 <textarea name="rptcontent" id="rptcontent" class="html"></textarea>
				   <div id="ajaxTags" name="ajaxTags" style='height: 250px; overflow-y: auto;' align="right">
				   <?php
					if($formid != "")
					{
				   ?>
						<table class="table">
							<thead><tr><th>User Details</th><th>Tag</th></tr></thead>
							<tbody>
							<tr><td>Username</td><td>{sf_username}</td></tr>
							<tr><td>Email</td><td>{sf_email}</td></tr>
							<tr><td>Full Name</td><td>{sf_fullname}</td></tr>
							<?php
									$q = Doctrine_Query::create()
									   ->from('apFormElements a')
									   ->where('a.form_id = ?', 15);
									   
									$elements = $q->execute();
									
									foreach($elements as $element)
									{
										$childs = $element->getElementTotalChild();
										if($childs == 0)
										{
										   echo "<tr><td>".$element->getElementTitle()."</td><td>{sf_element_".$element->getElementId()."}</td></tr>";
										}
										else
										{
											if($element->getElementType() == "select")
											{
												echo "<tr><td>".$element->getElementTitle()."</td><td>{sf_element_".$element->getElementId()."}</td></tr>";
											}
											else
											{
												for($x = 0; $x < ($childs + 1); $x++)
												{
													echo "<tr><td>".$element->getElementTitle()."</td><td>{sf_element_".$element->getElementId()."_".($x+1)."}</td></tr>";
												}
											}
										}
									}
							?>
							</tbody>
							</table>
							<?php 
							//Get Application Information (anything starting with ap_ )
									   //ap_application_id
									
							//Get Form Details (anything starting with fm_ )
									   //fm_created_at, fm_updated_at.....fm_element_1
							?>
							<table>
							<thead><tr><th>Application Details</th><th>Tag</th></tr></thead>
							<tbody>
							<tr><td>Plan Registration Number</td><td>{ap_application_id}</td></tr>
							<tr><td>Created At</td><td>{fm_created_at}</td></tr>
							<tr><td>Approved At</td><td>{fm_updated_at}</td></tr>
							<?php

									$q = Doctrine_Query::create()
									   ->from('apFormElements a')
									   ->where('a.form_id = ?', $formid);
									   
									$elements = $q->execute();
									
									foreach($elements as $element)
									{
										$childs = $element->getElementTotalChild();
										if($childs == 0)
										{
										   echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."}</td></tr>";
										}
										else
										{
											if($element->getElementType() == "select")
											{
												echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."}</td></tr>";
											}
											else
											{
												for($x = 0; $x < ($childs + 1); $x++)
												{
													echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."_".($x+1)."}</td></tr>";
												}
											}
										}
									}
							?>
							</tbody>
							</table>

							<table>
							<thead><tr><th>Conditions Details</th><th>Tag</th></tr></thead>
							<tbody>
							<tr><td>Conditions Of Approval</td><td>{ca_conditions}</td></tr>
							<tr><td>Conditions Of Approval Minimized</td><td>{mini_ca_conditions}</td></tr>
							<tr><td>Other Conditions</td><td>{other_conditions}</td></tr>
							</tbody>
							</table>

							<table>
							<thead><tr><th>Invoice Details</th><th>Tag</th></tr></thead>
							<tbody>
							<tr><td>Total</td><td>{in_total}</td></tr>
							</tbody>
							</table>
					<?php
					}
					?>
				   </div>
				 </div>
				 <div id='ajaxSelectelements' name='ajaxSelectelements' <?php if($type != "" && $type == 1){ ?>style="display: block;"<?php }else{ ?>style="display: none;"<?php } ?>>
					<?php
					if($formid != "")
					{
					?>
				 <select name="fields" id="fields" multiple>
                             <?php
                             //Get Application Information (anything starting with ap_ )
                             //ap_application_id

                             //Get Form Details (anything starting with fm_ )
                             //fm_created_at, fm_updated_at.....fm_element_1
                             ?>
                            <option value='{ap_application_id}'>Application Number</option>
                            <option value='{ap_application_status}'>Application Status</option>
                            <option value='{ap_date_of_submission}'>Date Of Submission</option>
                            <option value='{ap_date_of_approval}'>Date Of Approval</option>
							<option value='{sf_email}'>Applicant's Email</option>
							<option value='{sf_fullname}'>Applicant's Fullname</option>
							<option value='{sf_username}'>Applicant's Username</option>
							<?php
									$q = Doctrine_Query::create()
									   ->from('apFormElements a')
									   ->where('a.form_id = ?', 15);
									   
									$elements = $q->execute();
									
									foreach($elements as $element)
									{
										$childs = $element->getElementTotalChild();
										if($childs == 0)
										{
										   echo "<option value='{sf_element_".$element->getElementId()."}'>".$element->getElementTitle()."</option>";
										}
										else
										{
											if($element->getElementType() == "select")
											{
												echo "<option value='{sf_element_".$element->getElementId()."}'>".$element->getElementTitle()."</option>";
											}
											else
											{
												for($x = 0; $x < ($childs + 1); $x++)
												{
													echo "<option value='{sf_element_".$element->getElementId()."_".($x+1)."}'>".$element->getElementTitle()."</option>";
												}
											}
										}
									}
							?>
							<?php

									$q = Doctrine_Query::create()
									   ->from('apFormElements a')
									   ->where('a.form_id = ?', $formid)
                                       ->andWhere('a.element_position <> ?', '0')
                                       ->orderBy('a.element_position ASC');
									   
									$elements = $q->execute();
									
									foreach($elements as $element)
									{
										$childs = $element->getElementTotalChild();
										if($childs == 0)
										{
										   echo "<option value='{fm_element_".$element->getElementId()."}'>".$element->getElementTitle()."</option>";
										}
										else
										{
											if($element->getElementType() == "select")
											{
												echo "<option value='{fm_element_".$element->getElementId()."}'>".$element->getElementTitle()."</option>";
											}
											else
											{
												for($x = 0; $x < ($childs + 1); $x++)
												{
													echo "<option value='{fm_element_".$element->getElementId()."_".($x+1)."}'>".$element->getElementTitle()."</option>";
												}
											}
										}
									}
							?>
							<option value="{ca_conditions}"></option>
							<option value="{mini_ca_conditions}">Minimized Conditions</option>
							<option value="{other_conditions}">Other Conditions</option>
							<option value="{in_total}">Invoice Total Amount</option>
					</select>
						<?php
					}
					?>
				 </div>
                </div>
			  </div>
		       <div class="form-group">
				<label class="col-sm-4"><i class="bold-label">Show Actions Column</i></label>
				<div class="col-sm-8">
					<select id='application_actions' name='application_actions'>
					<option value="0">No</option>
					<option value="1">Yes</option>
				</select>
				</div>
			  </div>
              
                </div><!--panel-body-->
	  <div class="panel-footer">
			<button 'btn btn-danger ml10'>Reset</button><button type="submit" class="btn btn-primary" name="submitbuttonname" id="submitbuttonname" value="submitbuttonvalue">Submit</button>
	  </div>
</fieldset>
</div>

</form>

<script language="javascript">
 jQuery(document).ready(function(){
	$("#submitbuttonname").click(function() {
		 $.ajax({
			url: '/backend.php/reports/create',
			cache: false,
			type: 'POST',
			data : $('#reportform').serialize(),
			success: function(json) {
				$('#alertdiv').attr("style", "display: block;");
				$("html, body").animate({ scrollTop: 0 }, "slow");
			}
		});
		return false;
	 });
	});
</script>