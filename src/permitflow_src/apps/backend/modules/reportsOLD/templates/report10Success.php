<?php use_helper('I18N', 'Date') ?>
<?php
/**
 * report10 template.
 *
 * Report of all activities with a certain period of time.
 *
 * @package    backend
 * @subpackage reports
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
 
/**
*
* Function to get all the dates between a period of time
*
* @param String $sStartDate Start date to begin fetching dates from
* @param String $sEndDate End date where to stop fetching dates from
*
* @return String[]
*/
?>
<div class="g12" style="padding-left: 3px;">
			<form style="margin-bottom: 0px;">
			<label style='height: 30px; margin-top: 0px;'>
			<div style='float: left; font-size: 20px; font-weight: 700;'>Audit Trail</div>
             <div style="float: right;">
				<a href="#" onClick="window.print();"><img src='/assets_backend/images/pdf.gif'></a>
			</div>
            </label>
			<fieldset style="margin: 0px;">


<div class="g12">

<table>
    <thead>
   <tr>
        <th width="60">ID</th>
        <th>Reviewer</th>
        <th>Date/Time</th>
        <th>Action</th>
    </tr>
</thead>
<tbody>
<?php
$count = 0;
foreach($audits as $audit)
{
	?>
    <?php 
	$q = Doctrine_Query::create()
		 ->from('cfUser a')
		 ->where('a.nid = ?', $audit->getUserId());
	$thisuser = $q->fetchOne();
	if($thisuser)
	{
		$count++;
	?>
    <tr>
    <td><?php echo $count; ?></td>
    <td>
    <a href='/backend.php/users/showuser?id=<?php echo $thisuser->getNid(); ?>'>
    <?php
	echo $thisuser->getStrlastname()." ".$thisuser->getStrfirstname();
	?></a>
	</td>
    <td><?php echo $audit->getActionTimestamp(); ?></td>
    <td><?php echo html_entity_decode($audit->getAction()); ?></td>
    </tr>
	<?php
	}
	?>
    <?php
}
?>
</tbody>
</table>

			</fieldset>
			</form>
</div>