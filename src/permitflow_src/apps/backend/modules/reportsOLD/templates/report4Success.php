<?php
/**
 * report4 template.
 *
 * Report of all applications that have exceeded their designated time limit within the various workflow stages.
 *
 * @package    backend
 * @subpackage reports
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
 
/**
*
* Function to get all the dates between a period of time
*
* @param String $sStartDate Start date to begin fetching dates from
* @param String $sEndDate End date where to stop fetching dates from
*
* @return String[]
*/
function GetDays($sStartDate, $sEndDate){  
    $aDays[] = $start_date;
	$start_date  = $sStartDate;
	$end_date = $sEndDate;
	$current_date = $start_date;
	while(strtotime($current_date) <= strtotime($end_date))
	{
		$aDays[] = gmdate("Y-m-d", strtotime("+1 day", strtotime($current_date)));
		$current_date = gmdate("Y-m-d", strtotime("+2 day", strtotime($current_date)));
	}

  
  return $aDays;  
} 

$q = Doctrine_Query::create()
  ->from('SubMenus a')
  ->where('a.id <> 0 AND a.id <> 650 AND a.id <> 750 AND a.id <> 850')
  ->orderBy('a.order_no ASC');
$stages = $q->execute();

?>

<div class="g12" style="padding-left: 3px;">
			<form style="margin-bottom: 0px;" method="post" action="/backend.php/reports/printreport4" autocomplete="off" data-ajax="false">
			<label style='height: 30px; margin-top: 0px;'>
			<div style='float: left; font-size: 20px; font-weight: 700;'>Overdue Requests Report</div>
             <div style="float: right;">
             	<?php
            	foreach($stages as $stage)
				{
					if($_POST['pending_stage'][$stage->getId()] == 'true')
					{
						echo "<input type='hidden' name='pending_stage[".$stage->getId()."]' value='true'>";
					}
				}
				?>
                <button class="submit" style="margin-top: -5px;">Export to Excel</button>
            </div>
            </label>
			<fieldset style="margin: 0px;">

		
 <div class="g12">
			<?php
			$q = Doctrine_Query::create()
			  ->from('SubMenus a')
			  ->where('a.id <> 0 AND a.id <> 650 AND a.id <> 750 AND a.id <> 850')
			  ->orderBy('a.order_no ASC');
			$stages = $q->execute();
			
			?>
			<table class="chart" data-type="pie" data-tilt="0.5">

							<thead>

								<tr>

									<th></th>
			<?php
			
			$columns = 0;
			
			foreach($stages as $stage)
			{
				if($_POST['pending_stage'][$stage->getId()] == 'true')
				{
					echo "<input type='hidden' name='pending_stage[".$stage->getId()."]' value='true'>";
					echo "<th>".$stage->getTitle()."</th>";
					echo "<th>".$stage->getTitle()." - Overdue</th>";
					$columns++;
					$columns++;
				}
				
			}
			
			
			echo "</thead><tbody>";
			
			$offset = 0;
			
			foreach($stages as $stage)
			{
				if($_POST['pending_stage'][$stage->getId()] == 'true')
				{
						$q = Doctrine_Query::create()
						  ->from('FormEntry a')
						  ->where('a.approved = ?', $stage->getId())
						  ->andWhere("a.approved <> ? AND a.approved <> ? AND a.parent_submission = ?", array("0","","0"));
						$applications = $q->execute();
					
				    $intime = 0;
					$overdue = 0;
					
					foreach($applications as $application)
					{
						$q = Doctrine_Query::create()
							->from('ApplicationReference a')
							->where('a.stage_id = ?', $application->getApproved())
							->andWhere('a.application_id = ?', $application->getId());
						$application_reference = $q->fetchOne();
						
						
						$q = Doctrine_Query::create()
							->from('SubMenus a')
							->where('a.id = ?', $application->getApproved());
						$stage = $q->fetchOne();
						
						if($application_reference)
						{
							$days = sizeOf(GetDays($application_reference->getStartDate(),date('Y-m-d')));
							if($days > $stage->getMaxDuration())
							{
								$overdue++;
							}
							else
							{	
								$intime++;
							}
						}
					}
								echo "<tr>";
								
								echo "<th>".$stage->getTitle()." - Intime</th>";
								
								echo "<td>".$intime."</td>";
								
								echo "</tr>";
					
					
								echo "<tr>";
								
								echo "<th>".$stage->getTitle()." - Overdue</th>";
								
								echo "<td>".$overdue."</td>";
								
								echo "</tr>";
								
				}
			}
			
			
			?>
							</tbody>


						</table>
						
			<table>
				<thead>
					<tr>
			         <th>Type</th><th>No</th><th>Submitted On</th><th>Duration</th><th>Status</th><th style="background: none;">Actions</th>
					</tr>
				</thead>
				<tbody>
				<?php
				$filstages = "";
				
				$filtags = "";
				
				$count = 0;
				foreach($stages as $stage)
				{
					if($_POST['pending_stage'][$stage->getId()] == 'true')
					{
						$filstages[] = $stage->getId();
						if($count == 0)
						{
							$filtags = $filtags."a.approved = ? ";
						}
						else
						{
							$filtags = $filtags."OR a.approved = ? ";
						}
					$count++;
					}
				}
				
				     if($filstages == "" OR $filtags == "")
					{
						$q = Doctrine_Query::create()
						->from('FormEntry a')
						->where("a.approved <> ? AND a.approved <> ? AND a.parent_submission = ?", array("0","","0"));
						$applications = $q->execute();
					 }
					 else
					{
						$q = Doctrine_Query::create()
						->from('FormEntry a')
						->where($filtags, $filstages)
						->andWhere("a.approved <> ? AND a.approved <> ? AND a.parent_submission = ?", array("0","","0"));
						$applications = $q->execute();
					 }
				
						foreach($applications as $application)
						{
							if(!$sf_user->mfHasCredential("accesssubmenu".$application->getApproved()))
							{
								continue;
							}
						
							$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
							mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
							$query = "SELECT * FROM ap_form_".$application->getFormId()." WHERE id = '".$application->getEntryId()."'";
							$result = mysql_query($query,$dbconn);

							$application_form = mysql_fetch_assoc($result);
				?>
					
						<tr id="row_<?php echo $application->getId() ?>">
						<td><?php 
						$q = Doctrine_Query::create()
						     ->from('ApForms a')
							 ->where('a.form_id = ?', $application->getFormId());
					    $form = $q->fetchOne();
						if($form)
						{
							echo $form->getFormDescription();
						}
						else
						{
							echo "-";
						}
						?></td>
						<td><?php echo $application->getApplicationId(); ?></td>
						<td class="c">
						<?php
							$q = Doctrine_Query::create()
						     ->from('sfGuardUserProfile a')
							 ->where('a.user_id = ?', $application->getUserId());
					    $userprofile = $q->fetchOne();
						if($userprofile)
						{
							echo $userprofile->getFullname();
						}
						else
						{
							echo "-";
						}
						?>
						</td>
						
						<td><?php
						
							$q = Doctrine_Query::create()
								->from('ApplicationReference a')
								->where('a.stage_id = ?', $application->getApproved())
								->andWhere('a.application_id = ?', $application->getId());
							$application_reference = $q->fetchOne();
							
							
							$q = Doctrine_Query::create()
								->from('SubMenus a')
								->where('a.id = ?', $application->getApproved());
							$stage = $q->fetchOne();
							
							if($application_reference)
							{
								$days = sizeOf(GetDays($application_reference->getStartDate(),date('Y-m-d')));
								if($days > $stage->getMaxDuration())
								{
									echo "<font color='red'>".$days." days taken</font>";
								}
								else
								{
									echo $days;
								}
							}
							else
							{
								echo "-";
							}
						?></td>
						<td class="c">
						<?php
							 $q = Doctrine_Query::create()
								->from('SubMenus a')
								->where('a.id = ?', $application->getApproved());
							$submenu = $q->fetchOne();
							if($submenu)
							{
								echo $submenu->getTitle();
							}
							else
							{
								echo " - ";
							}
						?>
						</td>
						<td class="c">
						<a title='View Application' href='<?php echo public_path(); ?>backend.php/applications/view/id/<?php echo $application->getId(); ?>'><img src='<?php echo public_path(); ?>assets_backend/images/icons/dark/create_write.png'></a>
						</td>
					</tr>
				<?php
						}
				?>
				</tbody>
			</table>

			
			</fieldset>
			</form>
	</div>