<?php
/**
 * Services actions.
 *
 * Allows user to manage services
 *
 * @package    backend
 * @subpackage services
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
class servicesActions extends sfActions
{
    /**
   * Executes 'Index' action 
   * 
   * Displays list of services
   *
   * @param sfRequest $request A request object
   */
    public function executeIndex(sfWebRequest $request)
    {
        if(!$this->getUser()->mfHasCredential('access_workflow'))
        {	
            //    $this->redirect('/backend.php/errors/notallowed');
        }
        $this->setLayout("layout-settings");
    }

    /**
     * Executes 'Delete' action
     *
     * Delete a service
     *
     * @param sfRequest $request A request object
     */
    public function executeDelete(sfWebRequest $request)
    {
        if(!$this->getUser()->mfHasCredential('managestages'))
        {
            $this->redirect('/backend.php/errors/notallowed');
        }

        $q = Doctrine_Query::create()
            ->from("Menus a")
            ->where("a.id = ?", $request->getParameter("id"));
        $service = $q->fetchOne();

        if($service)
        {
            $q = Doctrine_Query::create()
                ->from("SubMenus a")
                ->where("a.menu_id = ?", $request->getParameter("id"));
            $stages = $q->execute();

            foreach($stages as $stage)
            {
                $stage->delete();
            }

            $service->delete();
        }

        $this->redirect('/backend.php/services/index');
    }
}
