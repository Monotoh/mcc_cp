<?php
use_helper("I18N");

$audit = new Audit();
$audit->saveAudit("", "Accessed Services Settings");
if($sf_user->mfHasCredential("managestages"))
{
?>

<div class="contentpanel panel-email">
    <div class="panel panel-dark">

        <div class="panel-heading">
                <h3 class="panel-title"><?php echo __('Services'); ?></h3>

                <div class="pull-right">
                        <a class="btn btn-primary-alt settings-margin42" id="newpage" href="<?php echo public_path(); ?>backend.php/wizard/workflow" ><?php echo __('New Service'); ?></a>
            </div>
        </div>

        <div class="panel-group panel-group" id="accordion2">
            <?php
			/*OTB Start - Only show services for agency logged in user belongs to*/
			//$agency_manager = new AgencyManager();
			//$allowed_menu_ids = $agency_manager->getAllowedServices($_SESSION["SESSION_CUTEFLOW_USERID"], "id");
			/*OTB End - Only show services for agency logged in user belongs to*/

            $q = Doctrine_Query::create()
               ->from("Menus a")
			   //->whereIn('a.id',$allowed_menu_ids)//OTB - Only show services for agency logged in user belongs to
               ->orderBy("a.order_no ASC");
            $services = $q->execute();

            $count = 0;
            foreach($services as $service)
            {
                $count++;
                ?>
                <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion2" href="#collapseOne<?php echo $count; ?>" class="collapsed">
                        <?php echo $count; ?>:  <?php echo $service->getTitle(); ?>
                    </a>
                    </h4>
                </div>
                <div id="collapseOne<?php echo $count; ?>" class="panel-collapse collapse">
                    <div class="panel-body" style="padding: 20px;">
                        <a target="_blank" class="btn btn-primary btn-lg btn-form" style="margin-right: 10px;" href="<?php echo public_path(); ?>backend.php/forms/index/filter/<?php echo $service->getId(); ?>"><span class="fa fa-edit"></span> <?php echo __('Forms') ?></a>
                        <a target="_blank" class="btn btn-primary btn-lg btn-workflow" style="margin-right: 10px;" href="<?php echo public_path(); ?>backend.php/submenus/index/filter/<?php echo $service->getId(); ?>"><span class="fa fa-random"></span> <?php echo __('Workflow') ?></a>
                        <a target="_blank" class="btn btn-primary btn-lg btn-outputs" style="margin-right: 10px;" href="<?php echo public_path(); ?>backend.php/invoicetemplates/index/filter/<?php echo $service->getId(); ?>"><span class="fa fa-print"></span> <?php echo __('Invoices') ?></a>
                        <a target="_blank" class="btn btn-primary btn-lg btn-outputs" style="margin-right: 10px;" href="<?php echo public_path(); ?>backend.php/permits/index/filter/<?php echo $service->getId(); ?>"><span class="fa fa-print"></span> <?php echo __('Permits') ?></a>
                        <a target="_blank" class="btn btn-primary btn-lg btn-outputs" style="margin-right: 10px;" href="<?php echo public_path(); ?>backend.php/reports/index/filter/<?php echo $service->getId(); ?>"><span class="fa fa-print"></span> <?php echo __('Custom Reports') ?></a>
                          <a target="_blank" class="btn btn-primary btn-lg btn-outputs" style="margin-right: 10px;" href="<?php echo public_path(); ?>backend.php/permitapprover/index/filter/<?php echo $service->getId(); ?>"><span class="fa fa-print"></span> <?php echo __('Approval Config Details') ?></a> <!-- OTB patch -->
<!--OTB Start - Multiagency fucntionality-->
                        <a target="_blank" class="btn btn-primary btn-lg btn-outputs" style="margin-right: 10px;" href="<?php echo public_path(); ?>backend.php/agency/listforservice/filter/<?php echo $service->getId(); ?>"><span class="fa fa-print"></span> <?php echo __('Agencies') ?></a>
                      
<!--OTB End - Multiagency fucntionality-->
<?php if($sf_user->mfHasCredential("duplicateworkflows")): ?>
                         <a target="_blank" class="btn btn-success btn-lg btn-outputs" style="margin-right: 10px;" href="<?php echo public_path(); ?>backend.php/menus/edit/id/<?php echo $service->getId(); ?>"><span class="fa fa-edit"></span> <?php echo __('Edit') ?></a>
                         
                         <a target="_blank" class="btn btn-black btn-lg btn-outputs" style="margin-right: 10px;" href="<?php echo public_path(); ?>backend.php/menus/duplicateWorkflow/id/<?php echo $service->getId(); ?>"><span class="fa fa-dashcube"></span> <?php echo __('Duplicate Workflow') ?></a>
                         <a target="_blank" class="btn btn-black btn-lg btn-outputs" style="margin-right: 10px;" href="<?php echo public_path(); ?>backend.php/menus/machFormDuplicateForms/id/<?php echo $service->getId(); ?>"><span class="fa fa-dashcube"></span> <?php echo __('Duplicate Forms') ?></a>
                         <a target="_blank" class="btn btn-black btn-lg btn-outputs" style="margin-right: 10px;" href="<?php echo public_path(); ?>backend.php/menus/duplicatePermits/id/<?php echo $service->getId(); ?>"><span class="fa fa-dashcube"></span> <?php echo __('Duplicate Permits') ?></a>
                         <a target="_blank" class="btn btn-black btn-lg btn-outputs" style="margin-right: 10px;" href="<?php echo public_path(); ?>backend.php/menus/duplicateInvoices/id/<?php echo $service->getId(); ?>"><span class="fa fa-dashcube"></span> <?php echo __('Duplicate Invoices') ?></a>
                         
                         <a onClick="if(confirm('Are you sure? This action will delete this workflow')){ return true; }else{ return false; }" class="btn btn-danger btn-lg btn-outputs pull-right" style="margin-right: 10px;" href="<?php echo public_path(); ?>backend.php/services/delete/id/<?php echo $service->getId(); ?>"><span class="fa fa-trash-o"></span> <?php echo __('Delete') ?></a>
                    <?php endif; ?>
                    </div>
                </div>
                </div>
                <?php
            }
            ?>
          </div>
    </div>
</div>
<?php } else {
     include_partial("settings/accessdenied"); 
}
?>
