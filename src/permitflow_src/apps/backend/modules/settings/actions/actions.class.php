<?php
/**
 * Settings actions.
 *
 * System settings dashboard with links to other settings
 *
 * @package    backend
 * @subpackage settings
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
class settingsActions extends sfActions
{
    /**
   * Executes 'Index' action 
   * 
   * Displays system settings dashboard with links to various settings
   *
   * @param sfRequest $request A request object
   */
    public function executeIndex(sfWebRequest $request)
    {
              if(!$this->getUser()->mfHasCredential('access_settings'))
              {	
                      $this->redirect('/backend.php/errors/notallowed');
              }
        $this->setLayout("layout-settings");
    }

    /**
   * Executes 'Content' action 
   * 
   * Displays system settings dashboard with links to various settings
   *
   * @param sfRequest $request A request object
   */
    public function executeContent(sfWebRequest $request)
    {
              if(!$this->getUser()->mfHasCredential('access_settings'))
              { 
                      $this->redirect('/backend.php/errors/notallowed');
              }
    }

    /**
   * Executes 'Forms' action 
   * 
   * Displays system settings dashboard with links to various settings
   *
   * @param sfRequest $request A request object
   */
    public function executeForms(sfWebRequest $request)
    {
              if(!$this->getUser()->mfHasCredential('access_settings'))
              { 
                      $this->redirect('/backend.php/errors/notallowed');
              }
    }

    /**
   * Executes 'Workflow' action 
   * 
   * Displays system settings dashboard with links to various settings
   *
   * @param sfRequest $request A request object
   */
    public function executeWorkflow(sfWebRequest $request)
    {
              if(!$this->getUser()->mfHasCredential('access_settings'))
              { 
                      $this->redirect('/backend.php/errors/notallowed');
              }
    }

    /**
   * Executes 'Security' action 
   * 
   * Displays system settings dashboard with links to various settings
   *
   * @param sfRequest $request A request object
   */
    public function executeSecurity(sfWebRequest $request)
    {
              if(!$this->getUser()->mfHasCredential('access_settings'))
              { 
                      $this->redirect('/backend.php/errors/notallowed');
              }
        $this->setLayout("layout-settings");
    }

    /**
   * Executes 'Themes' action 
   * 
   * Displays system settings dashboard with links to various settings
   *
   * @param sfRequest $request A request object
   */
    public function executeThemes(sfWebRequest $request)
    {
              if(!$this->getUser()->mfHasCredential('access_settings'))
              { 
                      $this->redirect('/backend.php/errors/notallowed');
              }
    }
}
