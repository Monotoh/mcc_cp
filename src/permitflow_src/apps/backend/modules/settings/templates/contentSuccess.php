<?php
  use_helper("I18N");
  
  $audit = new Audit();
  $audit->saveAudit("", "Accessed content settings page");
?>
<div class="pageheader">
  <h2><i class="fa fa-envelope"></i> <?php echo __('Content'); ?></h2>
  <div class="breadcrumb-wrapper">
    <span class="label"><?php echo __('You are here'); ?>:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>backend.php"><?php echo __('Home'); ?></a></li>
      <li><a href="<?php echo public_path(); ?>backend.php/settings"><?php echo __('Settings'); ?></a></li>
    </ol>
  </div>
</div>

<div class="contentpanel panel-email">
    <div class="row">
    <?php
    if($sf_user->mfHasCredential("access_content"))
    {
    ?>
    	<div class="col-lg-2">
		    <ul class="nav nav-pills nav-stacked nav-email">
            <?php
            if($sf_user->mfHasCredential("managewebpages"))
            {
            ?>
		        <li id="liwebpages" <?php if($sf_context->getActionName() == "webpages"): ?>class="active"<?php endif; ?>>
		        <a id="webpages" href="#">
		            <i class="glyphicon  glyphicon-file"></i> <?php echo __('Web Pages'); ?>
		        </a>
            </li>
            <li id="libanners" <?php if($sf_context->getActionName() == "banners"): ?>class="active"<?php endif; ?>>
            <a id="banners" href="#">
                <i class="glyphicon  glyphicon-file"></i> <?php echo __('Banners'); ?>
            </a>
            </li>
            <?php
            }
            if($sf_user->mfHasCredential("managefaqs"))
            {
            ?>
		        <li id="lifaqs" <?php if($sf_context->getActionName() == "faqs"): ?>class="active"<?php endif; ?>>
		        <a id="faqs" href="#">
		        	<i class="glyphicon  glyphicon-question-sign"></i> <?php echo __('Frequently Asked Questions'); ?>
		        </a>
            </li>
            <?php
            }
            if($sf_user->mfHasCredential("managenews"))
            {
            ?>
		        <li id="linews" <?php if($sf_context->getActionName() == "news"): ?>class="active"<?php endif; ?>>
		        	<a id="news" href="#">
		        		<i class="glyphicon glyphicon-info-sign"></i> <?php echo __('News'); ?>
		        </a>
            </li>
            <?php
            }
            if($sf_user->mfHasCredential("managelanguages"))
            {
            ?>
		        <li id="lilanguages" <?php if($sf_context->getActionName() == "languages"): ?>class="active"<?php endif; ?>>
		        	<a id="languages" href="#">
		            <i class="glyphicon glyphicon-flag"></i> <?php echo __('Languages'); ?>
		        </a>
            </li>
            <?php
            }
            if($sf_user->mfHasCredential("manageannouncements"))
            {
            ?>
		        <li id="liannouncements" <?php if($sf_context->getActionName() == "announcements"): ?>class="active"<?php endif; ?>>
		        	<a id="announcements" href="#">
		            <i class="glyphicon glyphicon-volume-up"></i> <?php echo __('Announcements'); ?>
		        </a>
            </li>
            <?php
            }
            if($sf_user->mfHasCredential("managewebpages"))
            {
            ?>
            <li id="lisiteconfig" <?php if($sf_context->getActionName() == "siteconfig"): ?>class="active"<?php endif; ?>>
              <a id="siteconfig" href="#">
                <i class="glyphicon glyphicon-wrench"></i> <?php echo __('Site Config'); ?>
              </a>
            </li>
            <?php
            }
            ?>
		        <script language="javascript">
  					jQuery(document).ready(function(){
  						$( "#webpages" ).click(function() {
  							$("#contentload").load("<?php echo public_path(); ?>backend.php/content/index");
                $("#liwebpages").addClass("active");
                $("#libanners").removeClass("active");
                $("#lifaqs").removeClass("active");
                $("#linews").removeClass("active");
                $("#liannouncements").removeClass("active");
                $("#lilanguages").removeClass("active");
                $("#lisiteconfig").removeClass("active");
  						});
              $( "#banners" ).click(function() {
                $("#contentload").load("<?php echo public_path(); ?>backend.php/banner/index");
                $("#liwebpages").removeClass("active");
                $("#libanners").addClass("active");
                $("#lifaqs").removeClass("active");
                $("#linews").removeClass("active");
                $("#liannouncements").removeClass("active");
                $("#lilanguages").removeClass("active");
                $("#lisiteconfig").removeClass("active");
              });
  						$( "#faqs" ).click(function() {
  							$("#contentload").load("<?php echo public_path(); ?>backend.php/faq/index");
                $("#liwebpages").removeClass("active");
                $("#libanners").removeClass("active");
                $("#lifaqs").addClass("active");
                $("#linews").removeClass("active");
                $("#liannouncements").removeClass("active");
                $("#lilanguages").removeClass("active");
                $("#lisiteconfig").removeClass("active");
  						});
  						$( "#news" ).click(function() {
  							$("#contentload").load("<?php echo public_path(); ?>backend.php/news/index");
                $("#liwebpages").removeClass("active");
                $("#libanners").removeClass("active");
                $("#lifaqs").removeClass("active");
                $("#linews").addClass("active");
                $("#liannouncements").removeClass("active");
                $("#lilanguages").removeClass("active");
                $("#lisiteconfig").removeClass("active");
  						});
  						$( "#languages" ).click(function() {
  							$("#contentload").load("<?php echo public_path(); ?>backend.php/languages/index");
                $("#liwebpages").removeClass("active");
                $("#libanners").removeClass("active");
                $("#lifaqs").removeClass("active");
                $("#linews").removeClass("active");
                $("#liannouncements").removeClass("active");
                $("#lilanguages").addClass("active");
                $("#lisiteconfig").removeClass("active");
  						});
  						$( "#announcements" ).click(function() {
  							$("#contentload").load("<?php echo public_path(); ?>backend.php/announcements/index");
                $("#liwebpages").removeClass("active");
                $("#libanners").removeClass("active");
                $("#lifaqs").removeClass("active");
                $("#linews").removeClass("active");
                $("#lilanguages").removeClass("active");
                $("#liannouncements").addClass("active");
                $("#lisiteconfig").removeClass("active");
  						});
              $( "#siteconfig" ).click(function() {
                $("#contentload").load("<?php echo public_path(); ?>backend.php/siteconfig/index");
                $("#liwebpages").removeClass("active");
                $("#libanners").removeClass("active");
                $("#lifaqs").removeClass("active");
                $("#linews").removeClass("active");
                $("#lilanguages").removeClass("active");
                $("#liannouncements").removeClass("active");
                $("#lisiteconfig").addClass("active");
              });
  						<?php
                switch($_SESSION['current_module'])
                {
                  case "content":
                    $_GET['load'] = "webpages";
                    break;
                  case "content":
                    $_GET['load'] = "banner";
                    break;
                  case "faq":
                    $_GET['load'] = "faqs";
                    break;
                  case "news":
                    $_GET['load'] = "news";
                    break;
                  case "languages":
                    $_GET['load'] = "languages";
                    break;
                  case "announcements":
                    $_GET['load'] = "announcements";
                    break;
                  case "siteconfig":
                    $_GET['load'] = "siteconfig";
                    break;
                  default:
                    $_GET['load'] = "webpages";
                    break;
                }

  							switch($_GET['load'])
  							{
  								case "webpages":
  									echo "$('#contentload').load('".public_path()."backend.php/content/index');";
                    echo '$("#liwebpages").addClass("active");';
                    echo '$("#libanners").removeClass("active");';
                    echo '$("#lifaqs").removeClass("active");';
                    echo '$("#linews").removeClass("active");';
                    echo '$("#lilanguages").removeClass("active");';
                    echo '$("#liannoucements").removeClass("active");';
                    echo '$("#lisiteconfig").removeClass("active");';
  									break;
                  case "banner":
                    echo "$('#contentload').load('".public_path()."backend.php/banner/index');";
                    echo '$("#liwebpages").removeClass("active");';
                    echo '$("#libanners").addClass("active");';
                    echo '$("#lifaqs").removeClass("active");';
                    echo '$("#linews").removeClass("active");';
                    echo '$("#lilanguages").removeClass("active");';
                    echo '$("#liannoucements").removeClass("active");';
                    echo '$("#lisiteconfig").removeClass("active");';
                    break;
  							 	case "faqs":
  							 		echo "$('#contentload').load('".public_path()."backend.php/faq/index');";
                    echo '$("#liwebpages").removeClass("active");';
                    echo '$("#libanners").removeClass("active");';
                    echo '$("#lifaqs").addClass("active");';
                    echo '$("#linews").removeClass("active");';
                    echo '$("#lilanguages").removeClass("active");';
                    echo '$("#liannoucements").removeClass("active");';
                    echo '$("#lisiteconfig").removeClass("active");';
  							 		break;
  							 	case "news":
  							 		echo "$('#contentload').load('".public_path()."backend.php/news/index');";
                    echo '$("#liwebpages").removeClass("active");';
                    echo '$("#libanners").removeClass("active");';
                    echo '$("#lifaqs").removeClass("active");';
                    echo '$("#linews").addClass("active");';
                    echo '$("#lilanguages").removeClass("active");';
                    echo '$("#liannoucements").removeClass("active");';
                    echo '$("#lisiteconfig").removeClass("active");';
  							 		break;
  							 	case "languages":
  							 		echo "$('#contentload').load('".public_path()."backend.php/languages/index');";
                    echo '$("#liwebpages").removeClass("active");';
                    echo '$("#libanners").removeClass("active");';
                    echo '$("#lifaqs").removeClass("active");';
                    echo '$("#linews").removeClass("active");';
                    echo '$("#lilanguages").addClass("active");';
                    echo '$("#liannoucements").removeClass("active");';
                    echo '$("#lisiteconfig").removeClass("active");';
  							 		break;
  							 	case "announcements":
  							 		echo "$('#contentload').load('".public_path()."backend.php/announcements/index');";
                    echo '$("#liwebpages").removeClass("active");';
                    echo '$("#libanners").removeClass("active");';
                    echo '$("#lifaqs").removeClass("active");';
                    echo '$("#linews").removeClass("active");';
                    echo '$("#lilanguages").removeClass("active");';
                    echo '$("#liannouncements").addClass("active");';
                    echo '$("#lisiteconfig").removeClass("active");';
  							 		break;
                  case "siteconfig":
                     echo "$('#contentload').load('".public_path()."backend.php/siteconfig/index');";
                    echo '$("#liwebpages").removeClass("active");';
                    echo '$("#libanners").removeClass("active");';
                    echo '$("#lifaqs").removeClass("active");';
                    echo '$("#linews").removeClass("active");';
                    echo '$("#lilanguages").removeClass("active");';
                    echo '$("#liannouncements").removeClass("active");';
                    echo '$("#lisiteconfig").addClass("active");';
                     break;
  							 	default:
  							 		echo "$('#contentload').load('".public_path()."backend.php/content/index');";
                    echo '$("#liwebpages").addClass("active");';
                    echo '$("#libanners").removeClass("active");';
                    echo '$("#lifaqs").removeClass("active");';
                    echo '$("#linews").removeClass("active");';
                    echo '$("#lilanguages").removeClass("active");';
                    echo '$("#liannoucements").removeClass("active");';
                    echo '$("#lisiteconfig").removeClass("active");';
  							 		break;
  							}
  						?>
  					});
		        </script>
		    </ul>
		</div>

    	<div class="col-lg-10">
        <div id="contentload">

        </div>
      </div>
    <?php
    }
    else
    {
      include_partial("accessdenied");
    }
    ?>
    </div>
</div>
