<?php
use_helper("I18N");

$audit = new Audit();
$audit->saveAudit("", "Accessed forms settings page");
?>
<div class="pageheader">
  <h2><i class="fa fa-envelope"></i> <?php echo __('Forms'); ?></h2>
  <div class="breadcrumb-wrapper">
    <span class="label"><?php echo __('You are here'); ?>:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>backend.php"><?php echo __('Home'); ?></a></li>
      <li><a href="<?php echo public_path(); ?>backend.php/settings"><?php echo __('Settings'); ?></a></li>
    </ol>
  </div>
</div>

<div class="contentpanel panel-email">

    <div class="row">

    <?php
    if($sf_user->mfHasCredential("access_forms"))
    {
    ?>

    	<div class="col-sm-3 col-lg-2">
		    <ul class="nav nav-pills nav-stacked nav-email">

            <?php
            if($sf_user->mfHasCredential("manageformgroups"))
            {
            ?>
            <li id="liformgroups" <?php if($sf_context->getActionName() == "liststarred"): ?>class="active"<?php endif; ?>>
            <a id="formgroups" href="#">
              <i class="glyphicon  glyphicon-folder-open"></i> <?php echo __('Form Groups'); ?>
            </a>
            </li>
            <?php
            }
            if($sf_user->mfHasCredential("manageforms"))
            {
            ?>
		        <li id="liforms" <?php if($sf_context->getActionName() == "list"): ?>class="active"<?php endif; ?>>
		        <a id="forms" href="#">
		            <i class="glyphicon  glyphicon-file"></i> <?php echo __('Forms'); ?>
		        </a>
		        </li>
            <?php
            }
            if($sf_user->mfHasCredential("managefees"))
            {
            ?>
		        <li id="lifixedcharges" <?php if($sf_context->getActionName() == "listtrash"): ?>class="active"<?php endif; ?>>
		        	<a id="fixedcharges" href="#">
		            <i class="glyphicon glyphicon-usd"></i> <?php echo __('Fixed Charges'); ?>
		        </a>
            </li>
            <?php
            }
            if($sf_user->mfHasCredential("manageinvoices"))
            {
            ?>
		        <li id="liinvoicetemplate" <?php if($sf_context->getActionName() == "listtrash"): ?>class="active"<?php endif; ?>>
		        	<a id="invoicetemplate" href="#">
		            <i class="glyphicon glyphicon-file"></i> <?php echo __('Invoice Templates'); ?>
		        </a>
            </li>
            <?php
            }
            if($sf_user->mfHasCredential("managepermits"))
            {
            ?>
		        <li id="lipermits" <?php if($sf_context->getActionName() == "listtrash"): ?>class="active"<?php endif; ?>>
		        	<a id="permits" href="#">
		            <i class="glyphicon  glyphicon-ok-circle"></i> <?php echo __('Permit Templates'); ?>
		        </a>
            </li>
            <?php
            }
            if($sf_user->mfHasCredential("manageconditions"))
            {
            /**
            ?>
		        <li id="liconditions" <?php if($sf_context->getActionName() == "listtrash"): ?>class="active"<?php endif; ?>>
		        	<a id="conditions" href="#">
		            <i class="glyphicon glyphicon-ok-sign"></i> <?php echo __('Conditions'); ?>
		        </a>
            </li>
            <?php
            **/
            }
            if($sf_user->mfHasCredential("manageplots"))
            {
              /**
            ?>
		        <li id="liplots" <?php if($sf_context->getActionName() == "listtrash"): ?>class="active"<?php endif; ?>>
		        	<a id="plots" href="#">
		            <i class="glyphicon  glyphicon-map-marker"></i> <?php echo __('Plots'); ?>
		        </a>
            </li>
            <?php
              **/
            }
            if($sf_user->mfHasCredential("managereports"))
            {
            ?>
		        <li id="licustomreports" <?php if($sf_context->getActionName() == "listtrash"): ?>class="active"<?php endif; ?>>
		        	<a id="customreports" href="#">
		            <i class="glyphicon  glyphicon-stats"></i> <?php echo __('Custom Reports'); ?>
		        </a>
            </li>
            <?php
            }
            ?>
		        <script language="javascript">
  					jQuery(document).ready(function(){
  						$( "#forms" ).click(function() {
  							$("#contentload").load("<?php echo public_path(); ?>backend.php/forms/index");
                $("#liforms").addClass("active");
                $("#liformgroups").removeClass("active");
                $("#lisubmissionfees").removeClass("active");
                $("#lifixedcharges").removeClass("active");
                $("#liinvoicetemplate").removeClass("active");
                $("#lipermits").removeClass("active");
                $("#liconditions").removeClass("active");
                $("#liplots").removeClass("active");
                $("#licustomreports").removeClass("active");
  						});
  						$( "#formgroups" ).click(function() {
  							$("#contentload").load("<?php echo public_path(); ?>backend.php/formgroups/index");
                $("#liforms").removeClass("active");
                $("#liformgroups").addClass("active");
                $("#lisubmissionfees").removeClass("active");
                $("#lifixedcharges").removeClass("active");
                $("#liinvoicetemplate").removeClass("active");
                $("#lipermits").removeClass("active");
                $("#liconditions").removeClass("active");
                $("#liplots").removeClass("active");
                $("#licustomreports").removeClass("active");
  						});
  						$( "#fixedcharges" ).click(function() {
  							$("#contentload").load("<?php echo public_path(); ?>backend.php/fees/settingsindex/filter/0");
                $("#liforms").removeClass("active");
                $("#liformgroups").removeClass("active");
                $("#lisubmissionfees").removeClass("active");
                $("#lifixedcharges").addClass("active");
                $("#liinvoicetemplate").removeClass("active");
                $("#lipermits").removeClass("active");
                $("#liconditions").removeClass("active");
                $("#liplots").removeClass("active");
                $("#licustomreports").removeClass("active");
  						});
  						$( "#invoicetemplate" ).click(function() {
  							$("#contentload").load("<?php echo public_path(); ?>backend.php/invoicetemplates/index");
                $("#liforms").removeClass("active");
                $("#liformgroups").removeClass("active");
                $("#lisubmissionfees").removeClass("active");
                $("#lifixedcharges").removeClass("active");
                $("#liinvoicetemplate").addClass("active");
                $("#lipermits").removeClass("active");
                $("#liconditions").removeClass("active");
                $("#liplots").removeClass("active");
                $("#licustomreports").removeClass("active");
  						});
  						$( "#permits" ).click(function() {
  							$("#contentload").load("<?php echo public_path(); ?>backend.php/permits/index");
                $("#liforms").removeClass("active");
                $("#liformgroups").removeClass("active");
                $("#lisubmissionfees").removeClass("active");
                $("#lifixedcharges").removeClass("active");
                $("#liinvoicetemplate").removeClass("active");
                $("#lipermits").addClass("active");
                $("#liconditions").removeClass("active");
                $("#liplots").removeClass("active");
                $("#licustomreports").removeClass("active");
  						});
  						$( "#conditions" ).click(function() {
  							$("#contentload").load("<?php echo public_path(); ?>backend.php/conditionsmng/index");
                $("#liforms").removeClass("active");
                $("#liformgroups").removeClass("active");
                $("#lisubmissionfees").removeClass("active");
                $("#lifixedcharges").removeClass("active");
                $("#liinvoicetemplate").removeClass("active");
                $("#lipermits").removeClass("active");
                $("#liconditions").addClass("active");
                $("#liplots").removeClass("active");
                $("#licustomreports").removeClass("active");
  						});
  						$( "#plots" ).click(function() {
  							$("#contentload").load("<?php echo public_path(); ?>backend.php/plot/index");
                $("#liforms").removeClass("active");
                $("#liformgroups").removeClass("active");
                $("#lisubmissionfees").removeClass("active");
                $("#lifixedcharges").removeClass("active");
                $("#liinvoicetemplate").removeClass("active");
                $("#lipermits").removeClass("active");
                $("#liconditions").removeClass("active");
                $("#liplots").addClass("active");
                $("#licustomreports").removeClass("active");
  						});
  						$( "#customreports" ).click(function() {
  							$("#contentload").load("<?php echo public_path(); ?>backend.php/reports/index");
                $("#liforms").removeClass("active");
                $("#liformgroups").removeClass("active");
                $("#lisubmissionfees").removeClass("active");
                $("#lifixedcharges").removeClass("active");
                $("#liinvoicetemplate").removeClass("active");
                $("#lipermits").removeClass("active");
                $("#liconditions").removeClass("active");
                $("#liplots").removeClass("active");
                $("#licustomreports").addClass("active");
  						});
  						<?php
                switch($_SESSION['current_module'])
                {
                  case "forms":
                    $_GET['load'] = "forms";
                    break;
                  case "formgroups":
                    $_GET['load'] = "formgroups";
                    break;
                  case "fees":
                    $_GET['load'] = "fixedcharges";
                    break;
                  case "invoicetemplates":
                    $_GET['load'] = "invoicetemplate";
                    break;
                  case "permits":
                    $_GET['load'] = "permits";
                    break;
                  case "conditions":
                    $_GET['load'] = "conditions";
                    break;
                  case "plots":
                    $_GET['load'] = "plots";
                    break;
                  case "reports":
                    $_GET['load'] = "customreports";
                    break;
                }

  							switch($_GET['load'])
  							{
  								case "forms":
  									echo "$('#contentload').load('".public_path()."backend.php/forms/index');";
                    echo '$("#liforms").addClass("active");';
  									break;
  							 	case "formgroups":
  							 		echo "$('#contentload').load('".public_path()."backend.php/formgroups/index');";
                    echo '$("#liformgroups").addClass("active");';
  							 		break;
  							 	case "submissionfees":
  							 		echo "$('#contentload').load('".public_path()."backend.php/submissionfee/index');";
                    echo '$("#lisubmissionfees").addClass("active");';
  							 		break;
  							 	case "fixedcharges":
  							 		echo "$('#contentload').load('".public_path()."backend.php/fees/settingsindex/filter/0');";
                    echo '$("#lifixedcharges").addClass("active");';
  							 		break;
  							 	case "invoicetemplates":
  							 		echo "$('#contentload').load('".public_path()."backend.php/invoicetemplates/index');";
                    echo '$("#liinvoicetemplate").addClass("active");';
  							 		break;
  							 	case "permits":
  							 		echo "$('#contentload').load('".public_path()."backend.php/permits/index');";
                    echo '$("#lipermits").addClass("active");';
  							 		break;
  							 	case "conditions":
  							 		echo "$('#contentload').load('".public_path()."backend.php/conditionsmng/index');";
                    echo '$("#liconditions").addClass("active");';
  							 		break;
  							 	case "plots":
  							 		echo "$('#contentload').load('".public_path()."backend.php/plot/index');";
                    echo '$("#liplots").addClass("active");';
  							 		break;
  							 	case "customreports":
  							 		echo "$('#contentload').load('".public_path()."backend.php/reports/index');";
                    echo '$("#licustomreports").addClass("active");';
  							 		break;
  							 	default:
  							 		echo "$('#contentload').load('".public_path()."backend.php/formgroups/index');";
                    echo '$("#liformgroups").addClass("active");';
  							 		break;
  							}
  						?>
  					});
		        </script>
		    </ul>
		</div>

    	<div class="col-sm-9 col-lg-10">

                <div id="contentload">

                </div>
        </div>
    <?php
    }
    else
    {
      include_partial("accessdenied");
    }
    ?>
    </div>
</div>
