<?php
  use_helper("I18N");

  $audit = new Audit();
  $audit->saveAudit("", "Accessed security settings page");
?>
<div class="pageheader">
  <h2><i class="fa fa-unlock-alt"></i> <?php echo __('Security'); ?></h2>
  <div class="breadcrumb-wrapper">
    <span class="label"><?php echo __('You are here'); ?>:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>backend.php"><?php echo __('Home'); ?></a></li>
      <li><a href="<?php echo public_path(); ?>backend.php/settings"><?php echo __('Settings'); ?></a></li>
    </ol>
  </div>
</div>

<div class="contentpanel panel-email">

    <div class="row">

    <?php
    if($sf_user->mfHasCredential("access_security"))
    {
    ?>

    	<div class="col-sm-3 col-lg-2">
		    <ul class="nav nav-pills nav-stacked nav-email">
            <?php
            if($sf_user->mfHasCredential("managecategories"))
            {
            ?>
		        <li id="limembercategories">
		            <a id="membercategories" href="#">
		        	  <i class="fa  fa-users"></i> <?php echo __('User Categories'); ?>
		        </a>
            </li>
            <?php
            }
            if($sf_user->mfHasCredential("managegroups"))
            {
            ?>
		        <li id="ligroups">
		        	  <a id="groups" href="#">
		        		<i class="fa fa-group"></i> <?php echo __('Groups'); ?>
		        </a>
            </li>
            <?php
            }
            if($sf_user->mfHasCredential("manageroles"))
            {
            ?>
		        <li id="liroles">
		        	  <a id="roles" href="#">
		            <i class="fa fa-unlock-alt"></i> <?php echo __('Roles'); ?>
		        </a>
            </li>
            <?php
            }
            if($sf_user->mfHasCredential("access_security"))
            {
            ?>
            <li id="lisecurity">
              <a id="security" href="<?php echo public_path(); ?>backend.php/wizard/security">
                <i class="fa fa-envelope"></i> <?php echo __('Security Wizard'); ?>
            </a>
            </li>
            <?php
            }
            ?>
		        <script language="javascript">
  					jQuery(document).ready(function(){
  						$( "#membercategories" ).click(function() {
  							$("#contentload").load("<?php echo public_path(); ?>backend.php/usercategories/index");
                $("#liregisteredmembers").removeClass("active");
                $("#limembercategories").addClass("active");
                $("#ligroups").removeClass("active");
                $("#liroles").removeClass("active");
  						});
  						$( "#groups" ).click(function() {
  							$("#contentload").load("<?php echo public_path(); ?>backend.php/groups/index");
                $("#liregisteredmembers").removeClass("active");
                $("#limembercategories").removeClass("active");
                $("#ligroups").addClass("active");
                $("#liroles").removeClass("active");
  						});
  						$( "#roles" ).click(function() {
  							$("#contentload").load("<?php echo public_path(); ?>backend.php/credentials/index");
                $("#liregisteredmembers").removeClass("active");
                $("#limembercategories").removeClass("active");
                $("#ligroups").removeClass("active");
                $("#liroles").addClass("active");
  						});
  						<?php
                switch($_SESSION['current_module'])
                {
                  case "categories":
                    $_GET['load'] = "membercategories";
                    break;
                  case "groups":
                    $_GET['load'] = "groups";
                    break;
                  case "roles":
                    $_GET['load'] = "roles";
                    break;
                }

  							switch($_GET['load'])
  							{
  							 	case "membercategories":
  							 		echo "$('#contentload').load('".public_path()."backend.php/usercategories/index');";
                    echo '$("#liregisteredmembers").removeClass("active");';
                    echo '$("#limembercategories").addClass("active");';
                    echo '$("#ligroups").removeClass("active");';
                    echo '$("#liroles").removeClass("active");';
  							 		break;
  							 	case "groups":
  							 		echo "$('#contentload').load('".public_path()."backend.php/groups/index');";
                    echo '$("#liregisteredmembers").removeClass("active");';
                    echo '$("#limembercategories").removeClass("active");';
                    echo '$("#ligroups").addClass("active");';
                    echo '$("#liroles").removeClass("active");';
  							 		break;
  							 	case "roles":
  							 		echo "$('#contentload').load('".public_path()."backend.php/credentials/index');";
                    echo '$("#liregisteredmembers").removeClass("active");';
                    echo '$("#limembercategories").removeClass("active");';
                    echo '$("#ligroups").removeClass("active");';
                    echo '$("#liroles").addClass("active");';
  							 		break;
  							 	default:
                    echo "$('#contentload').load('".public_path()."backend.php/usercategories/index');";
                    echo '$("#liregisteredmembers").addClass("active");';
                    echo '$("#limembercategories").removeClass("active");';
                    echo '$("#ligroups").removeClass("active");';
                    echo '$("#liroles").removeClass("active");';
  							 		break;
  							}
  						?>
  					});
		        </script>
		    </ul>
		</div>

    	<div class="col-sm-9 col-lg-10">

            <div id="contentload">

            </div>
        </div>
    <?php
    }
    else
    {
      include_partial("accessdenied");
    }
    ?>
    </div>
</div>
