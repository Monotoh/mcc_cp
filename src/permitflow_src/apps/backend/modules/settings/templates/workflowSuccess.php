<?php
  use_helper("I18N");

  $audit = new Audit();
  $audit->saveAudit("", "Accessed workflow settings page");
?>
<div class="pageheader">
  <h2><i class="fa fa-envelope"></i> <?php echo __('Workflow'); ?></h2>
  <div class="breadcrumb-wrapper">
    <span class="label"><?php echo __('You are here'); ?>:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>backend.php"><?php echo __('Home'); ?></a></li>
      <li><a href="<?php echo public_path(); ?>backend.php/settings"><?php echo __('Settings'); ?></a></li>
    </ol>
  </div>
</div>

    <div class="row">

    <?php
    if($sf_user->mfHasCredential("access_workflow"))
    {
    ?>

    	<div class="col-sm-3 col-lg-2">
		    <ul class="nav nav-pills nav-stacked nav-email">
            <?php
            if($sf_user->mfHasCredential("managedepartments"))
            {
            ?>
		        <li id="lidepartments" <?php if($sf_context->getActionName() == "list"): ?>class="active"<?php endif; ?>>
		        <a id="departments" href="#">
		            <i class="fa fa-sitemap"></i> <?php echo __('Departments'); ?>
		        </a>
		        </li>
            <?php
            }
            if($sf_user->mfHasCredential("managecommentsheets"))
            {
              /** Deprecated. Comments sheets should now be assigned directly from form builder
            ?>
		        <li id="licommentsheets" <?php if($sf_context->getActionName() == "listsentback"): ?>class="active"<?php endif; ?>>
		        	<a id="commentsheets" href="#">
		        		<i class="fa fa-book"></i> <?php echo __('Comment Sheets'); ?>
		        </a>
            </li>
            <?php
              **/
            }
            if($sf_user->mfHasCredential("managestages"))
            {
            ?>
		        <li id="listages" <?php if($sf_context->getActionName() == "listtrash"): ?>class="active"<?php endif; ?>>
		        	<a id="stages" href="#">
		            <i class="fa fa-tasks"></i> <?php echo __('Stages'); ?>
		        </a>
            </li>
            <?php
            }
            if($sf_user->mfHasCredential("manageactions"))
            {
              /** Deprecated. Its now done in the stages menu above
            ?>
		        <li id="liactions" <?php if($sf_context->getActionName() == "listtrash"): ?>class="active"<?php endif; ?>>
		        	<a id="actions" href="#">
		            <i class="fa fa-check-square"></i> <?php echo __('Actions'); ?>
		        </a>
            </li>
            <?php
              **/
            }
            if($sf_user->mfHasCredential("managenotifications"))
            {
              /** Deprecated. Its now done in the stages menu above
            ?>
		        <li id="linotifications" <?php if($sf_context->getActionName() == "listtrash"): ?>class="active"<?php endif; ?>>
		        	<a id="notifications" href="#">
		            <i class="fa fa-envelope"></i> <?php echo __('Notifications'); ?>
		        </a>
            </li>
            <?php
              **/
            }
            if($sf_user->mfHasCredential("access_workflow"))
            {
            ?>
            <li id="liwizard">
              <a id="wizard" href="<?php echo public_path(); ?>backend.php/wizard/workflow">
                <i class="fa fa-envelope"></i> <?php echo __('Workflow Wizard'); ?>
            </a>
            </li>
            <?php
            }
            ?>

		        <script language="javascript">
  					jQuery(document).ready(function(){
  						$( "#departments" ).click(function() {
  							$("#contentload").load("<?php echo public_path(); ?>backend.php/department/index");
                $("#lidepartments").addClass("active");
                $("#lireviewers").removeClass("active");
                $("#licommentsheets").removeClass("active");
                $("#listages").removeClass("active");
                $("#liactions").removeClass("active");
                $("#linotifications").removeClass("active");
  						});
  						$( "#reviewers" ).click(function() {
  							$("#contentload").load("<?php echo public_path(); ?>backend.php/users/settingsindex");
                $("#lidepartments").removeClass("active");
                $("#lireviewers").addClass("active");
                $("#licommentsheets").removeClass("active");
                $("#listages").removeClass("active");
                $("#liactions").removeClass("active");
                $("#linotifications").removeClass("active");
  						});
  						$( "#commentsheets" ).click(function() {
  							$("#contentload").load("<?php echo public_path(); ?>backend.php/templates/edittemplatestep2/templateid/7");
                $("#lidepartments").removeClass("active");
                $("#lireviewers").removeClass("active");
                $("#licommentsheets").addClass("active");
                $("#listages").removeClass("active");
                $("#liactions").removeClass("active");
                $("#linotifications").removeClass("active");
  						});
  						$( "#stages" ).click(function() {
  							$("#contentload").load("<?php echo public_path(); ?>backend.php/menus/index");
                $("#lidepartments").removeClass("active");
                $("#lireviewers").removeClass("active");
                $("#licommentsheets").removeClass("active");
                $("#listages").addClass("active");
                $("#liactions").removeClass("active");
                $("#linotifications").removeClass("active");
  						});
  						$( "#actions" ).click(function() {
  							$("#contentload").load("<?php echo public_path(); ?>backend.php/buttons/index");
                $("#lidepartments").removeClass("active");
                $("#lireviewers").removeClass("active");
                $("#licommentsheets").removeClass("active");
                $("#listages").removeClass("active");
                $("#liactions").addClass("active");
                $("#linotifications").removeClass("active");
  						});
  						$( "#notifications" ).click(function() {
  							$("#contentload").load("<?php echo public_path(); ?>backend.php/notificationsetup/index");
                $("#lidepartments").removeClass("active");
                $("#lireviewers").removeClass("active");
                $("#licommentsheets").removeClass("active");
                $("#listages").removeClass("active");
                $("#liactions").removeClass("active");
                $("#linotifications").addClass("active");
  						});
  						<?php
                switch($_SESSION['current_module'])
                {
                  case "departments":
                    $_GET['load'] = "departments";
                    break;
                  case "reviewers":
                    $_GET['load'] = "reviewers";
                    break;
                  case "commentsheets":
                    $_GET['load'] = "commentsheets";
                    break;
                  case "stages":
                    $_GET['load'] = "stages";
                    break;
                  case "actions":
                    $_GET['load'] = "actions";
                    break;
                  case "notifications":
                    $_GET['load'] = "notifications";
                    break;
                }

  							switch($_GET['load'])
  							{
  								case "departments":
  									echo "$('#contentload').load('".public_path()."backend.php/department/index');";
                    echo '$("#lidepartments").addClass("active");';
  									break;
  							 	case "reviewers":
  							 		echo "$('#contentload').load('".public_path()."backend.php/users/settingsindex');";
                    echo '$("#lireviewers").addClass("active");';
  							 		break;
  							 	case "commentsheets":
  							 		echo "$('#contentload').load('".public_path()."backend.php/templates/edittemplatestep2/templateid/7');";
                    echo '$("#licommentsheets").addClass("active");';
  							 		break;
  							 	case "stages":
  							 		echo "$('#contentload').load('".public_path()."backend.php/menus/index');";
                    echo '$("#listages").addClass("active");';
  							 		break;
  							 	case "actions":
  							 		echo "$('#contentload').load('".public_path()."backend.php/buttons/index');";
                    echo '$("#liactions").addClass("active");';
  							 		break;
  							 	case "notifications":
  							 		echo "$('#contentload').load('".public_path()."backend.php/notificationsetup/index');";
                    echo '$("#linotifications").addClass("active");';
  							 		break;
  							 	default:
  							 		echo "$('#contentload').load('".public_path()."backend.php/department/index');";
                    echo '$("#lidepartments").addClass("active");';
  							 		break;
  							}

                if($_GET['load'] == 'stages' && !empty($_GET['next']))
                {
                  echo "$('#contentload').load('".public_path()."backend.php/submenus/index/filter/".$_GET['next']."');";
                }
  						?>
  					});
		        </script>
		    </ul>
		</div>

    	<div class="col-sm-9 col-lg-10">

                <div id="contentload">

                </div>
        </div>
    <?php
    }
    else
    {
      include_partial("accessdenied");
    }
    ?>
    </div>

