<?php
	use_helper("I18N");
?>
<script type="text/javascript" src="<?php echo public_path(); ?>assets_unified/js/jquery.bootstrap-duallistbox.js"></script>


<form id="stageform" class="form-bordered" action="<?php echo url_for('/backend.php/submenus/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '/id/'.$form->getObject()->getId() : '')) ?>/filter/<?php echo $filter; ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> autocomplete="off" data-ajax="false">

<div class="panel-heading">
<h3 class="panel-title"><?php echo ($form->getObject()->isNew() ? __('New Stage') : __($form->getObject()->getMenus()->getTitle().' -&gt; Edit Stage')); ?></h3>
</div>
<div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
  <strong><?php echo __('Well done'); ?>!</strong> <?php echo __('You successfully updated this stage'); ?></a>.
</div>
<div class="panel-body panel-body-nopadding">

	 <?php if (!$form->getObject()->isNew()): ?>
		<input type="hidden" name="sf_method" value="put" />
		<?php endif; ?>
      <?php echo $form->renderGlobalErrors() ?>

	   <?php if(isset($form['_csrf_token'])): ?>
            <?php echo $form['_csrf_token']->render(); ?>
            <?php endif; ?>

						<div class="form-group">
							<label class="col-sm-4"><i class="bold-label"><?php echo __('Title'); ?></i></label>
							<div class="col-sm-8">
							  <?php echo $form['title']->renderError() ?>
							  <?php
       						  $translation = new translation();
							  $title = "";
							  if(!$form->getObject()->isNew()){
							  	$title = $form->getObject()->getTitle();
							  	if($translation->getTranslation('submenus','title',$form->getObject()->getId()))
						        {
						          	$title = $translation->getTranslation('submenus','title',$form->getObject()->getId());
						        }
							  }
							  ?>
							  <input class="form-control" type='text' name='sub_menus[title]' id='sub_menus_title' required="required" value="<?php if(!$form->getObject()->isNew()){ echo $title; } ?>">
							</div>
						  </div>

						  <div id="nameresult" name="nameresult"></div>

					      <script language="javascript">
					        $('document').ready(function(){
					          $('#sub_menus_title').keyup(function(){
					            $.ajax({
					                      type: "POST",
					                      url: "/backend.php/submenus/checkname/filter/<?php echo $filter; ?>",
					                      data: {
					                          'name' : $('input:text[id=sub_menus_title]').val()
					                      },
					                      dataType: "text",
					                      success: function(msg){
					                            //Receiving the result of search here
					                            $("#nameresult").html(msg);
					                      }
					                  });
					              });
					        });
					      </script>
						  <div class="form-group">
							<label class="col-sm-4"><i class="bold-label"><?php echo __('Maximum duration of time an application is allowed in this stage (Days)'); ?></i></label>
							<div class="col-sm-8">
							  <input type="text" id="max_duration" name="max_duration" class='form-control' value="<?php echo (!$form->getObject()->isNew()) ? $form->getObject()->getMaxDuration() : '0';  ?>">
							</div>
						  </div>
                                              <!-- OTB patch - Stage Application queuing that can override default menu settings 
                                                   By default the set queuing will be workflow settings -->
                                                  <div class="form-group">
							<label class="col-sm-4"><i class="bold-label"><?php echo __('Application Queuing'); ?></i>
                                                        <?php echo __('(Default behavior is to use workflow settings)') ?>
                                                        </label>
							<div class="col-sm-8">							 
                                                          <?php echo $form['app_queuing']->render(array('class' => 'form-control')); ?>
							</div>
						  </div>
						  <?php
						  $notification = null;
                          $stage_type = null;
                          $stage_property = null;
                          $stage_type_movement = null;
                          $stage_type_notification = null;
                          $stage_expired_movement = null;
                          $change_application_number = null;
                          $application_id = null;
                          $application_id_start = null;

						  if(!$form->getObject()->isNew())
						  {
							  $q = Doctrine_Query::create()
							     ->from("Notifications a")
							     ->where("a.submenu_id = ?", $form->getObject()->getId());
							  $notification = $q->fetchOne();

                              $stage_type = $form->getObject()->getStageType();
                              $stage_property = $form->getObject()->getStageProperty();
                              $stage_type_movement = $form->getObject()->getStageTypeMovement();
                              $stage_type_movement_pass = $form->getObject()->getStageTypeMovement();
                              $stage_type_movement_fail = $form->getObject()->getStageTypeMovementFail();
							  $stage_payment_confirmation = $form->getObject()->getStagePaymentConfirmation();//OTB - Combine cash and electronic payments
                              $stage_type_notification = $form->getObject()->getStageTypeNotification();
                              $stage_expired_movement = $form->getObject()->getStageExpiredMovement();
                              $change_application_number = $form->getObject()->getChangeIdentifier();
                              $application_id = $form->getObject()->getNewIdentifier();
                              $application_id_start = $form->getObject()->getNewIdentifierStart();
						  }

                          //If applications expire, what to do

                          //Stage type properties
						  ?>
                        <div class="form-group">
                            <label class="col-sm-4"><i class="bold-label"><?php echo __('Send expired application to another stage'); ?></i></label>
                            <div class="col-sm-8">
                                <select id="stage_expired_movement" name="stage_expired_movement" class="form-control">
                                    <?php
                                    echo "<option value='0'>No</option>";
                                    $q = Doctrine_Query::create()
                                        ->from('Menus a')
                                        ->orderBy('a.order_no ASC');
                                    $stagegroups = $q->execute();
                                    foreach($stagegroups as $stagegroup)
                                    {
                                        $q = Doctrine_Query::create()
                                            ->from('SubMenus a')
                                            ->where('a.menu_id = ?', $stagegroup->getId())
                                            ->andWhere('a.deleted = ?', '0')
                                            ->orderBy('a.order_no ASC');
                                        $stages = $q->execute();

                                        echo "<optgroup label='".$stagegroup->getTitle()."'>";

                                        foreach($stages as $stage)
                                        {
                                            $selected = "";

                                            if($stage_expired_movement == $stage->getId())
                                            {
                                                $selected = "selected";
                                            }

                                            echo "<option value='".$stage->getId()."' ".$selected.">".$stage->getTitle()."</option>";
                                        }

                                        echo "</optgroup>";
                                    }

                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4"><i class="bold-label">Change Application Number</i></label>
                            <div class="col-sm-8">
                                <select id="change_application_number" name="change_application_number" class="form-control" onChange="if(this.value == '1'){ document.getElementById('application_number_area').style.display = 'block'; }else{ document.getElementById('application_number_area').style.display = 'none'; }">
                                    <option value='0' <?php echo ($change_application_number == 0) ? 'selected="selected"' : '' ?>>No</option>
                                    <option value='1' <?php echo ($change_application_number == 1) ? 'selected="selected"' : '' ?>>Yes</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group" id="application_number_area" name="application_number_area" style="display: <?php echo ($change_application_number == 1) ? 'block' : 'none' ?>;">
                            <label class="col-sm-4"><i class="bold-label">Application Number Properties</i></label>
                            <div class="col-sm-8">

                                <div class="form-group">
                                    <label class="col-sm-4"><i class="bold-label"><?php echo __('New Application Number Identifier e.g. CPF-'); ?></i></label>
                                    <div class="col-sm-8">
                                        <input type="text" name="new_identifier" id="new_identifier" class="form-control" value="<?php echo $application_id; ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4"><i class="bold-label"><?php echo __('Application Number Starting Point e.g. AAA0001'); ?></i></label>
                                    <div class="col-sm-8">
                                        <input type="text" name="new_identifier_start" id="new_identifier_start" class="form-control" value="<?php echo $application_id_start; ?>">
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4"><i class="bold-label">Type of Stage</i></label>
                            <div class="col-sm-8">
                                <select id="stage_type" name="stage_type" class="form-control" onChange="if(this.value == '2'){ document.getElementById('assessment_area').style.display = 'block'; document.getElementById('invoicing_area').style.display = 'none'; document.getElementById('corrections_area').style.display = 'none'; document.getElementById('payment_confirmation_area').style.display = 'none'; document.getElementById('dispatch_area').style.display = 'none'; document.getElementById('expired_area').style.display = 'none';}
								else if(this.value == '3'){ document.getElementById('invoicing_area').style.display = 'block'; document.getElementById('assessment_area').style.display = 'none'; document.getElementById('payment_confirmation_area').style.display = 'none'; document.getElementById('corrections_area').style.display = 'none'; document.getElementById('dispatch_area').style.display = 'none';document.getElementById('expired_area').style.display = 'none'; }
								else if(this.value == '5'){ document.getElementById('corrections_area').style.display = 'block'; document.getElementById('payment_confirmation_area').style.display = 'none'; document.getElementById('assessment_area').style.display = 'none'; document.getElementById('invoicing_area').style.display = 'none'; document.getElementById('dispatch_area').style.display = 'none';document.getElementById('expired_area').style.display = 'none'; }
								else if(this.value == '8'){ document.getElementById('corrections_area').style.display = 'none'; document.getElementById('payment_confirmation_area').style.display = 'none'; document.getElementById('assessment_area').style.display = 'none'; document.getElementById('invoicing_area').style.display = 'none'; document.getElementById('dispatch_area').style.display = 'block';document.getElementById('expired_area').style.display = 'none'; }else if(this.value == '9'){ document.getElementById('payment_confirmation_area').style.display = 'block'; document.getElementById('corrections_area').style.display = 'none'; document.getElementById('assessment_area').style.display = 'none'; document.getElementById('invoicing_area').style.display = 'none'; document.getElementById('dispatch_area').style.display = 'none';document.getElementById('expired_area').style.display = 'none'; }
								else if(this.value == '10'){ document.getElementById('expired_area').style.display = 'block';document.getElementById('payment_confirmation_area').style.display = 'none'; document.getElementById('corrections_area').style.display = 'none'; document.getElementById('assessment_area').style.display = 'none'; document.getElementById('invoicing_area').style.display = 'none'; document.getElementById('dispatch_area').style.display = 'none'; }
								else{ document.getElementById('assessment_area').style.display = 'none'; document.getElementById('invoicing_area').style.display = 'none'; document.getElementById('corrections_area').style.display = 'none'; document.getElementById('dispatch_area').style.display = 'none'; document.getElementById('payment_confirmation_area').style.display = 'none';document.getElementById('expired_area').style.display = 'none'; }">
                                    <option value='1' <?php echo ($stage_type == 1) ? 'selected="selected"' : '' ?>>Default</option>
                                    <option value='8' <?php echo ($stage_type == 8) ? 'selected="selected"' : '' ?>>Dispatch</option>
                                    <option value='2' <?php echo ($stage_type == 2) ? 'selected="selected"' : '' ?>>Assessment</option>
                                    <option value='3' <?php echo ($stage_type == 3) ? 'selected="selected"' : '' ?>>Invoicing</option>
                                    <option value='4' <?php echo ($stage_type == 4) ? 'selected="selected"' : '' ?>>Approved</option>
                                    <option value='5' <?php echo ($stage_type == 5) ? 'selected="selected"' : '' ?>>Corrections</option>
                                    <option value='6' <?php echo ($stage_type == 6) ? 'selected="selected"' : '' ?>>Rejected</option>
                                    <option value='7' <?php echo ($stage_type == 7) ? 'selected="selected"' : '' ?>>Archived</option>
                                    <option value='20' <?php echo ($stage_type == 20) ? 'selected="selected"' : '' ?>>Linked Submission Stage</option><!--OTB Linked submission stage-->
                                    <option value='9' <?php echo ($stage_type == 9) ? 'selected="selected"' : '' ?>>Cash Payments Confirmation</option><!--OTB: Add payments confirmation stage type-->
                                </select>
                            </div>
                        </div>
                           <!-- OTB patch - Extra type -->
                           <div class="form-group">
                            <label class="col-sm-4"><i class="bold-label">Extra Type of Stage</i></label>
                            <div class="col-sm-8">
                              <?php echo $form['extra_type']->render(array('class' => 'form-control')); ?>
                                
                            </div>
                            </div>          
						<!--OTB Start: add properties for payment confirmation stage type-->
                        <div class="form-group" id="payment_confirmation_area" name="payment_confirmation_area" style="display: <?php echo ($stage_type == 9) ? 'block' : 'none' ?>;">
                            <label class="col-sm-4"><i class="bold-label">Payment Confirmation Properties</i></label>
                            <div class="col-sm-8">

                                <div class="form-group">
                                    <label class="col-sm-4"><i class="bold-label"><?php echo __('What to do when cash payments are confirmed?'); ?></i></label>
                                    <div class="col-sm-8">
                                        <select id="payment_confirmation_properties" name="payment_confirmation_properties" class="form-control"  onChange="if(this.value == '2'){ document.getElementById('payment_confirmation_stage').style.display = 'block'; document.getElementById('payment_confirmation_notification').style.display = 'none'; }else if(this.value == '3'){ document.getElementById('payment_confirmation_notification').style.display = 'block'; document.getElementById('payment_confirmation_stage').style.display = 'none'; }else{ document.getElementById('payment_confirmation_stage').style.display = 'none'; document.getElementById('payment_confirmation_notification').style.display = 'none';  }">
                                            <option value='1' <?php echo ($stage_property == 1) ? 'selected="selected"' : '' ?>>Do Nothing</option>
                                            <option value='2' <?php echo ($stage_property == 2) ? 'selected="selected"' : '' ?>>Move to Another Stage</option>
                                            <option value='3' <?php echo ($stage_property == 3) ? 'selected="selected"' : '' ?>>Send a Notification</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" id="payment_confirmation_stage" name="payment_confirmation_stage" style="display: <?php echo ($stage_property == 2) ? 'block' : 'none' ?>;">
                                    <div class="form-group">
                                            <label class="col-sm-4"><i class="bold-label"><?php echo __('Select the stage for successful payments'); ?></i></label>
                                            <div class="col-sm-8">
                                                <select id="payment_confirmation_next_stage_pass" name="payment_confirmation_next_stage_pass" class="form-control">
                                                    <?php
                                                    $q = Doctrine_Query::create()
                                                        ->from('Menus a')
                                                        ->orderBy('a.order_no ASC');
                                                    $stagegroups = $q->execute();
                                                    foreach($stagegroups as $stagegroup)
                                                    {
                                                        $q = Doctrine_Query::create()
                                                            ->from('SubMenus a')
                                                            ->where('a.menu_id = ?', $stagegroup->getId())
                                                            ->andWhere('a.deleted = ?', '0')
                                                            ->orderBy('a.order_no ASC');
                                                        $stages = $q->execute();

                                                        echo "<optgroup label='".$stagegroup->getTitle()."'>";

                                                        foreach($stages as $stage)
                                                        {
                                                            $selected = "";

                                                            if($stage_type_movement_pass == $stage->getId())
                                                            {
                                                                $selected = "selected";
                                                            }

                                                            echo "<option value='".$stage->getId()."' ".$selected.">".$stage->getTitle()."</option>";
                                                        }

                                                        echo "</optgroup>";
                                                    }

                                                    ?>
                                                </select>
                                        </div>

                                    </div>

                                    <div class="form-group">  
                                        <label class="col-sm-4"><i class="bold-label"><?php echo __('Select the stage for failed payments'); ?></i></label>
                                        <div class="col-sm-8">
                                            <select id="payment_confirmation_next_stage_fail" name="payment_confirmation_next_stage_fail" class="form-control">
                                                <?php
                                                $q = Doctrine_Query::create()
                                                    ->from('Menus a')
                                                    ->orderBy('a.order_no ASC');
                                                $stagegroups = $q->execute();
                                                foreach($stagegroups as $stagegroup)
                                                {
                                                    $q = Doctrine_Query::create()
                                                        ->from('SubMenus a')
                                                        ->where('a.menu_id = ?', $stagegroup->getId())
                                                        ->andWhere('a.deleted = ?', '0')
                                                        ->orderBy('a.order_no ASC');
                                                    $stages = $q->execute();

                                                    echo "<optgroup label='".$stagegroup->getTitle()."'>";

                                                    foreach($stages as $stage)
                                                    {
                                                        $selected = "";

                                                        if($stage_type_movement_fail == $stage->getId())
                                                        {
                                                            $selected = "selected";
                                                        }

                                                        echo "<option value='".$stage->getId()."' ".$selected.">".$stage->getTitle()."</option>";
                                                    }

                                                    echo "</optgroup>";
                                                }

                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="payment_confirmation_properties_notification" name="payment_confirmation_properties_notification" style="display: <?php echo ($stage_property == 3) ? 'block' : 'none' ?>;">
                                    <label class="col-sm-4"><i class="bold-label"><?php echo __('Notification to be sent to reviewers'); ?></i></label>
                                    <div class="col-sm-8">
                                        <textarea id="payment_confirmation_notification" name="payment_confirmation_notification" class='form-control'><?php  echo $stage_type_notification; ?></textarea>
                                    </div>
                                </div>

                            </div>
                        </div><!--OTB End: add properties for payment confirmation stage type-->
                        <div class="form-group" id="dispatch_area" name="dispatch_area" style="display: <?php echo ($stage_type == 8) ? 'block' : 'none' ?>;">
                            <label class="col-sm-4"><i class="bold-label">Dispatch Properties</i></label>
                            <div class="col-sm-8">

                                <div class="form-group">
                                    <label class="col-sm-4"><i class="bold-label"><?php echo __('What to do when all tasks have been dispatched to reviewers?'); ?></i></label>
                                    <div class="col-sm-8">
                                        <select id="dispatch_properties" name="dispatch_properties" class="form-control"  onChange="if(this.value == '2'){ document.getElementById('dispatch_properties_stage').style.display = 'block'; document.getElementById('dispatch_properties_notification').style.display = 'none'; }else if(this.value == '3'){ document.getElementById('dispatch_properties_notification').style.display = 'block'; document.getElementById('dispatch_properties_stage').style.display = 'none'; }else{ document.getElementById('dispatch_properties_stage').style.display = 'none'; document.getElementById('dispatch_properties_notification').style.display = 'none';  }">
                                            <option value='1' <?php echo ($stage_property == 1) ? 'selected="selected"' : '' ?>>Do Nothing</option>
                                            <option value='2' <?php echo ($stage_property == 2) ? 'selected="selected"' : '' ?>>Move to Another Stage</option>
                                            <option value='3' <?php echo ($stage_property == 3) ? 'selected="selected"' : '' ?>>Send a Notification</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" id="dispatch_properties_stage" name="dispatch_properties_stage" style="display: <?php echo ($stage_property == 2) ? 'block' : 'none' ?>;">
                                    <label class="col-sm-4"><i class="bold-label"><?php echo __('Select a stage'); ?></i></label>
                                    <div class="col-sm-8">
                                        <select id="dispatch_next_stage" name="dispatch_next_stage" class="form-control">
                                            <?php
                                            $q = Doctrine_Query::create()
                                                ->from('Menus a')
                                                ->orderBy('a.order_no ASC');
                                            $stagegroups = $q->execute();
                                            foreach($stagegroups as $stagegroup)
                                            {
                                                $q = Doctrine_Query::create()
                                                    ->from('SubMenus a')
                                                    ->where('a.menu_id = ?', $stagegroup->getId())
                                                    ->andWhere('a.deleted = ?', '0')
                                                    ->orderBy('a.order_no ASC');
                                                $stages = $q->execute();

                                                echo "<optgroup label='".$stagegroup->getTitle()."'>";

                                                foreach($stages as $stage)
                                                {
                                                    $selected = "";

                                                    if($stage_type_movement == $stage->getId())
                                                    {
                                                        $selected = "selected";
                                                    }

                                                    echo "<option value='".$stage->getId()."' ".$selected.">".$stage->getTitle()."</option>";
                                                }

                                                echo "</optgroup>";
                                            }

                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" id="dispatch_properties_notification" name="dispatch_properties_notification" style="display: <?php echo ($stage_property == 3) ? 'block' : 'none' ?>;">
                                    <label class="col-sm-4"><i class="bold-label"><?php echo __('Notification to be sent to reviewers'); ?></i></label>
                                    <div class="col-sm-8">
                                        <textarea id="dispatch_notification" name="dispatch_notification" class='form-control'><?php  echo $stage_type_notification; ?></textarea>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="form-group" id="assessment_area" name="assessment_area" style="display: <?php echo ($stage_type == 2) ? 'block' : 'none' ?>;">
                            <label class="col-sm-4"><i class="bold-label">Assessment Properties</i></label>
                            <div class="col-sm-8">

                                <div class="form-group">
                                    <label class="col-sm-4"><i class="bold-label"><?php echo __('What to do when all tasks are complete?'); ?></i></label>
                                    <div class="col-sm-8">
                                        <select id="assessment_properties" name="assessment_properties" class="form-control"  onChange="if(this.value == '2'){ document.getElementById('assessment_properties_stage').style.display = 'block'; document.getElementById('assessment_properties_notification').style.display = 'none'; }else if(this.value == '3'){ document.getElementById('assessment_properties_notification').style.display = 'block'; document.getElementById('assessment_properties_stage').style.display = 'none'; }else{ document.getElementById('assessment_properties_stage').style.display = 'none'; document.getElementById('assessment_properties_notification').style.display = 'none';  }">
                                            <option value='1' <?php echo ($stage_property == 1) ? 'selected="selected"' : '' ?>>Do Nothing</option>
                                            <option value='2' <?php echo ($stage_property == 2) ? 'selected="selected"' : '' ?>>Move to Another Stage</option>
                                            <option value='3' <?php echo ($stage_property == 3) ? 'selected="selected"' : '' ?>>Send a Notification</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" id="assessment_properties_stage" name="assessment_properties_stage" style="display: <?php echo ($stage_property == 2) ? 'block' : 'none' ?>;">
                                    <label class="col-sm-4"><i class="bold-label"><?php echo __('Select a stage'); ?></i></label>
                                    <div class="col-sm-8">
                                        <select id="assessment_next_stage" name="assessment_next_stage" class="form-control">
                                            <?php
                                            $q = Doctrine_Query::create()
                                                ->from('Menus a')
                                                ->orderBy('a.order_no ASC');
                                            $stagegroups = $q->execute();
                                            foreach($stagegroups as $stagegroup)
                                            {
                                                $q = Doctrine_Query::create()
                                                    ->from('SubMenus a')
                                                    ->where('a.menu_id = ?', $stagegroup->getId())
                                                    ->andWhere('a.deleted = ?', '0')
                                                    ->orderBy('a.order_no ASC');
                                                $stages = $q->execute();

                                                echo "<optgroup label='".$stagegroup->getTitle()."'>";

                                                foreach($stages as $stage)
                                                {
                                                    $selected = "";

                                                    if($stage_type_movement == $stage->getId())
                                                    {
                                                        $selected = "selected";
                                                    }

                                                    echo "<option value='".$stage->getId()."' ".$selected.">".$stage->getTitle()."</option>";
                                                }

                                                echo "</optgroup>";
                                            }

                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" id="assessment_properties_notification" name="assessment_properties_notification" style="display: <?php echo ($stage_property == 3) ? 'block' : 'none' ?>;">
                                    <label class="col-sm-4"><i class="bold-label"><?php echo __('Notification to be sent to reviewers'); ?></i></label>
                                    <div class="col-sm-8">
                                        <textarea id="assessment_notification" name="assessment_notification" class='form-control'><?php  echo $stage_type_notification; ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-12"><i class="bold-label"><?php echo __('Select any reviewers you want automatically assigned to an application'); ?></i></label>
                                </div>
                                <div class="form-group" id="assessment_reviewers" name="assessment_reviewers">
                                    <div class="col-sm-12">
                                        <select name='allowed_reviewers[]' id='allowed_reviewers' multiple>
                                            <?php
                                            $selected = "";
                                            $q = Doctrine_Query::create()
                                                ->from("CfUser a")
                                                ->where("a.bdeleted = 0")
                                                ->orderBy("a.strfirstname ASC");
                                            $reviewers = $q->execute();
                                            foreach($reviewers as $reviewer)
                                            {
                                                $selected = "";

                                                if(!$form->getObject()->isNew()) {
                                                    $q = Doctrine_Query::create()
                                                        ->from("WorkflowReviewers a")
                                                        ->where("a.workflow_id = ?", $form->getObject()->getId())
                                                        ->andWhere("a.reviewer_id = ?", $reviewer->getNid());
                                                    $workflow_reviewer = $q->fetchOne();

                                                    if ($workflow_reviewer) {
                                                        $selected = "selected='selected'";
                                                    }
                                                }

                                                echo "<option value='".$reviewer->getNid()."' ".$selected.">".ucfirst($reviewer->getStrfirstname())." ".ucfirst($reviewer->getStrlastname())." (".$reviewer->getStrdepartment().")</option>";
                                            }
                                            ?>
                                        </select>

                                        <script>
                                            jQuery(document).ready(function(){
                                                var demo2 = $('[id="allowed_reviewers"]').bootstrapDualListbox();
                                            });
                                        </script>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="form-group" id="invoicing_area" name="invoicing_area" style="display: <?php echo ($stage_type == 3) ? 'block' : 'none' ?>;">
                            <label class="col-sm-4"><i class="bold-label">Invoicing Properties</i></label>
                            <div class="col-sm-8">

                                <div class="form-group">
                                    <label class="col-sm-4"><i class="bold-label"><?php echo __('What to do when invoices are paid?'); ?></i></label>
                                    <div class="col-sm-8">
                                        <select id="invoicing_properties" name="invoicing_properties" class="form-control"  onChange="if(this.value == '2'){ document.getElementById('invoicing_properties_stage').style.display = 'block'; document.getElementById('invoicing_properties_notification').style.display = 'none'; }else if(this.value == '3'){ document.getElementById('invoicing_properties_notification').style.display = 'block'; document.getElementById('invoicing_properties_stage').style.display = 'none'; }else{ document.getElementById('invoicing_properties_stage').style.display = 'none'; document.getElementById('invoicing_properties_notification').style.display = 'none';  }">
                                            <option value='1' <?php echo ($stage_property == 1) ? 'selected="selected"' : '' ?>>Do Nothing</option>
                                            <option value='2' <?php echo ($stage_property == 2) ? 'selected="selected"' : '' ?>>Move to Another Stage</option>
                                            <option value='3' <?php echo ($stage_property == 3) ? 'selected="selected"' : '' ?>>Send a Notification</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" id="invoicing_properties_stage" name="invoicing_properties_stage" style="display: <?php echo ($stage_property == 2) ? 'block' : 'none' ?>;">
                                    <div class="form-group">
                                            <label class="col-sm-4"><i class="bold-label"><?php echo __('Select the stage for successful payments'); ?></i></label>
                                            <div class="col-sm-8">
                                                <select id="invoicing_next_stage_pass" name="invoicing_next_stage_pass" class="form-control">
                                                    <?php
                                                    $q = Doctrine_Query::create()
                                                        ->from('Menus a')
                                                        ->orderBy('a.order_no ASC');
                                                    $stagegroups = $q->execute();
                                                    foreach($stagegroups as $stagegroup)
                                                    {
                                                        $q = Doctrine_Query::create()
                                                            ->from('SubMenus a')
                                                            ->where('a.menu_id = ?', $stagegroup->getId())
                                                            ->andWhere('a.deleted = ?', '0')
                                                            ->orderBy('a.order_no ASC');
                                                        $stages = $q->execute();

                                                        echo "<optgroup label='".$stagegroup->getTitle()."'>";

                                                        foreach($stages as $stage)
                                                        {
                                                            $selected = "";

                                                            if($stage_type_movement_pass == $stage->getId())
                                                            {
                                                                $selected = "selected";
                                                            }

                                                            echo "<option value='".$stage->getId()."' ".$selected.">".$stage->getTitle()."</option>";
                                                        }

                                                        echo "</optgroup>";
                                                    }

                                                    ?>
                                                </select>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4"><i class="bold-label"><?php echo __('Select the stage for failed payments'); ?></i></label>
                                        <div class="col-sm-8">
                                            <select id="invoicing_next_stage_fail" name="invoicing_next_stage_fail" class="form-control">
                                                <?php
                                                $q = Doctrine_Query::create()
                                                    ->from('Menus a')
                                                    ->orderBy('a.order_no ASC');
                                                $stagegroups = $q->execute();
                                                foreach($stagegroups as $stagegroup)
                                                {
                                                    $q = Doctrine_Query::create()
                                                        ->from('SubMenus a')
                                                        ->where('a.menu_id = ?', $stagegroup->getId())
                                                        ->andWhere('a.deleted = ?', '0')
                                                        ->orderBy('a.order_no ASC');
                                                    $stages = $q->execute();

                                                    echo "<optgroup label='".$stagegroup->getTitle()."'>";

                                                    foreach($stages as $stage)
                                                    {
                                                        $selected = "";

                                                        if($stage_type_movement_fail == $stage->getId())
                                                        {
                                                            $selected = "selected";
                                                        }

                                                        echo "<option value='".$stage->getId()."' ".$selected.">".$stage->getTitle()."</option>";
                                                    }

                                                    echo "</optgroup>";
                                                }

                                                ?>
                                            </select>
                                        </div>
                                    </div>

									<!--OTB Start - Combine cash and electronic payments -->
                                    <div class="form-group">
                                        <label class="col-sm-4"><i class="bold-label"><?php echo __('Select the stage for manual payment confirmation (if any)'); ?></i></label>
                                        <div class="col-sm-8">
                                            <select id="stage_payment_confirmation" name="stage_payment_confirmation" class="form-control">
                                                <?php
                                                $q = Doctrine_Query::create()
                                                    ->from('Menus a')
                                                    ->orderBy('a.order_no ASC');
                                                $stagegroups = $q->execute();
                                                foreach($stagegroups as $stagegroup)
                                                {
                                                    $q = Doctrine_Query::create()
                                                        ->from('SubMenus a')
                                                        ->where('a.menu_id = ?', $stagegroup->getId())
                                                        ->andWhere('a.deleted = ?', '0')
                                                        ->orderBy('a.order_no ASC');
                                                    $stages = $q->execute();

                                                    echo "<optgroup label='".$stagegroup->getTitle()."'>";

                                                    foreach($stages as $stage)
                                                    {
                                                        $selected = "";

                                                        if($stage_payment_confirmation == $stage->getId())
                                                        {
                                                            $selected = "selected";
                                                        }

                                                        echo "<option value='".$stage->getId()."' ".$selected.">".$stage->getTitle()."</option>";
                                                    }

                                                    echo "</optgroup>";
                                                }

                                                ?>
                                            </select>
                                        </div>
                                    </div>
									<!--OTB End - Combine cash and electronic payments -->
									
                                </div>

                                <div class="form-group" id="invoicing_properties_notification" name="invoicing_properties_notification" style="display: <?php echo ($stage_property == 3) ? 'block' : 'none' ?>;">
                                    <label class="col-sm-4"><i class="bold-label"><?php echo __('Notification to be sent to reviewers'); ?></i></label>
                                    <div class="col-sm-8">
                                        <textarea id="invoicing_notification" name="invoicing_notification" class='form-control'><?php  echo $stage_type_notification; ?></textarea>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="form-group" id="corrections_area" name="corrections_area" style="display: <?php echo ($stage_type == 5) ? 'block' : 'none' ?>;">
                            <label class="col-sm-4"><i class="bold-label">Corrections Properties</i></label>
                            <div class="col-sm-8">

                                <div class="form-group">
                                    <label class="col-sm-4"><i class="bold-label"><?php echo __('What to do when user makes corrections?'); ?></i></label>
                                    <div class="col-sm-8">
                                        <select id="correction_properties" name="correction_properties" class="form-control"  onChange="if(this.value == '2'){ document.getElementById('correction_properties_stage').style.display = 'block'; document.getElementById('correction_properties_notification').style.display = 'none'; }else if(this.value == '3'){ document.getElementById('correction_properties_notification').style.display = 'block'; document.getElementById('correction_properties_stage').style.display = 'none'; }else{ document.getElementById('correction_properties_stage').style.display = 'none'; document.getElementById('correction_properties_notification').style.display = 'none';  }">
                                            <option value='1' <?php echo ($stage_property == 1) ? 'selected="selected"' : '' ?>>Do Nothing</option>
                                            <option value='2' <?php echo ($stage_property == 2) ? 'selected="selected"' : '' ?>>Move to Another Stage</option>
                                            <option value='3' <?php echo ($stage_property == 3) ? 'selected="selected"' : '' ?>>Send a Notification</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" id="correction_properties_stage" name="correction_properties_stage" style="display: <?php echo ($stage_property == 2) ? 'block' : 'none' ?>;">
                                    <label class="col-sm-4"><i class="bold-label"><?php echo __('Select a stage'); ?></i></label>
                                    <div class="col-sm-8">
                                        <select id="correction_next_stage" name="correction_next_stage" class="form-control">
                                            <?php
                                            $q = Doctrine_Query::create()
                                                ->from('Menus a')
                                                ->orderBy('a.order_no ASC');
                                            $stagegroups = $q->execute();
                                            foreach($stagegroups as $stagegroup)
                                            {
                                                $q = Doctrine_Query::create()
                                                    ->from('SubMenus a')
                                                    ->where('a.menu_id = ?', $stagegroup->getId())
                                                    ->andWhere('a.deleted = ?', '0')
                                                    ->orderBy('a.order_no ASC');
                                                $stages = $q->execute();

                                                echo "<optgroup label='".$stagegroup->getTitle()."'>";

                                                foreach($stages as $stage)
                                                {
                                                    $selected = "";

                                                    if($stage_type_movement == $stage->getId())
                                                    {
                                                        $selected = "selected";
                                                    }

                                                    echo "<option value='".$stage->getId()."' ".$selected.">".$stage->getTitle()."</option>";
                                                }

                                                echo "</optgroup>";
                                            }

                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" id="correction_properties_notification" name="correction_properties_notification" style="display: <?php echo ($stage_property == 3) ? 'block' : 'none' ?>;">
                                    <label class="col-sm-4"><i class="bold-label"><?php echo __('Notification to be sent to reviewers'); ?></i></label>
                                    <div class="col-sm-8">
                                        <textarea id="correction_notification" name="correction_notification" class='form-control'><?php  echo $stage_type_notification; ?></textarea>
                                    </div>
                                </div>

                            </div>
                        </div>
						  <div class="form-group">
							<label class="col-sm-4"><i class="bold-label">Send notification to user when application enters this stage?</i></label>
							<div class="col-sm-8">
							  <select id="send_notification" name="send_notification" class="form-control" onChange="if(this.value == '1'){ document.getElementById('notification_area').style.display = 'block'; }else{ document.getElementById('notification_area').style.display = 'none'; }">
							  	<option value='0'>No</option>
							  	<option value='1' <?php echo ($notification) ? 'selected="selected"' : '' ?>>Yes</option>
							  </select>
							</div>
						  </div>
						  <div class="form-group" id="notification_area" name="notification_area" style="display: <?php echo ($notification) ? 'block' : 'none' ?>;">
							<label class="col-sm-4"><i class="bold-label">Notification</i></label>
							<div class="col-sm-8">

								<div class="form-group">
								<label class="col-sm-4"><i class="bold-label"><?php echo __('Mail Subject'); ?></i></label>
								<div class="col-sm-8">
								  <input type="text" id="mail_subject" name="mail_subject" class='form-control' value="<?php echo ($notification) ? $notification->getTitle() : '' ?>">
								</div>
							  </div>

								<div class="form-group">
								<label class="col-sm-4"><i class="bold-label"><?php echo __('Mail Content'); ?></i></label>
								<div class="col-sm-8">
								  <textarea id="mail_content" name="mail_content" class='form-control'><?php  echo ($notification) ? $notification->getContent() : '' ?></textarea>
								</div>
							  </div>

								<div class="form-group">
								<label class="col-sm-4"><i class="bold-label"><?php echo __('SMS Content'); ?></i></label>
								<div class="col-sm-8">
								  <textarea id="sms_content" name="sms_content" class='form-control'><?php  echo ($notification) ? $notification->getSms() : '' ?></textarea>
								</div>
							  </div>

							  <div class="form-group">
								<label class="col-sm-4"><i class="bold-label"><?php echo __('Send Options'); ?></i></label>
								<div class="col-sm-8">
								  <select id="send_options" name="send_options" class="form-control">
								  <option value="1">Send notification automatically</option>
								  <option value="0" <?php  echo ($notification && !$notification->getAutosend()) ? 'selected="selected"' : '' ?>>Allow reviewer to edit notification before sending</option>
								  </select>
								</div>
							  </div>


							   <div class="form-group">
									<div class="col-sm-12 alignright">
		  								 <button type="button" class="btn btn-primary" data-target="#fieldsModal" data-toggle="modal">View available user/form fields</button>
									</div>
	  							</div>


							  <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="fieldsModal" class="modal fade" style="display: none;">
								  <div class="modal-dialog">
								    <div class="modal-content">
								      <div class="modal-header">
								        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
								        <h4 id="myModalLabel" class="modal-title">View available user/form fields</h4>
								      </div>
								      <div class="modal-body">
								        <div class="form-group">											<?php
											//Get User Information (anything starting with sf_ )
													   //sf_email, sf_fullname, sf_username, ... other fields in the dynamic user profile form e.g sf_element_1
											?>

											<table class="table dt-on-steroids mb0">
											<thead><tr><th width="50%"><?php echo __('User Details'); ?></th><th><?php echo __('Tag'); ?></th></tr></thead>
											<tbody>
											<tr>
											<td><?php echo __('Username'); ?></td><td{sf_username}</td>
											</tr>
											<tr>
											<td><?php echo __('Email'); ?></td><td>{sf_email}</td>
											</tr>
											<tr>
											<td><?php echo __('Full Name'); ?></td><td>{sf_fullname}</td>
											</tr>
											<?php
											        $q = Doctrine_Query::create()
													   ->from('apFormElements a')
													   ->where('a.form_id = ?', '15');

													$elements = $q->execute();

													foreach($elements as $element)
																{
																	$childs = $element->getElementTotalChild();
																	if($childs == 0)
																	{
																	   echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."}</td></tr>";
																	}
																	else
																	{
																		if($element->getElementType() == "select")
																		{
																			echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."}</td></tr>";
																		}
																		else
																		{
																			for($x = 0; $x < ($childs + 1); $x++)
																			{
																				echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."_".($x+1)."}</td></tr>";
																			}
																		}
																	}
																}
														?>
											</tbody>
											</table>
											<?php
											foreach($forms as $formd)
											{
											?>
											<div id='form_<?php echo $formd->getFormId(); ?>' name='form_<?php echo $formd->getFormId(); ?>' style='display: none;'>
											<table class="table dt-on-steroids mb0">
											<thead><tr><th width="50%"><?php echo __('Application Details'); ?></th><th>Tag</th></tr></thead>
											<tbody>
											<tr>
											<td><?php echo __('Plan Registration Number'); ?></td><td>{ap_application_id}</td>
											</tr>
											<tr>
											<td><?php echo __('Created At'); ?></td> <td>{fm_created_at}</td>
											</tr>
											<tr>
											<td><?php echo __('Approved At'); ?></td> <td>{fm_updated_at}</td>
											</tr>
											<?php

											        $q = Doctrine_Query::create()
													   ->from('apFormElements a')
													   ->where('a.form_id = ?', $formd->getFormId());

													$elements = $q->execute();

													foreach($elements as $element)
																{
																	$childs = $element->getElementTotalChild();
																	if($childs == 0)
																	{
																	   echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."}</td></tr>";
																	}
																	else
																	{
																		if($element->getElementType() == "select")
																		{
																			echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."}</td></tr>";
																		}
																		else
																		{
																			for($x = 0; $x < ($childs + 1); $x++)
																			{
																				echo "<tr><td>".$element->getElementTitle()."</td><td>{fm_element_".$element->getElementId()."_".($x+1)."}</td></tr>";
																			}
																		}
																	}
																}
														?>
											</tbody>
											</table>
											</div>
											<?php
											}
											?>


											<table class="table dt-on-steroids mb0">
											<thead><tr><th width="50%"><?php echo __('Conditions Of Approval'); ?></th><th>Tag</th></tr></thead>
											<tbody>
											<tr><td><?php echo __('Conditions Of Approval'); ?></td><td>{ca_conditions}</td></tr>
											</tbody>
											</table>

											<table class="table dt-on-steroids mb0">
											<thead><tr><th width="50%"><?php echo __('Invoice Details'); ?></th><th><?php echo __('Tag'); ?></th></tr></thead>
											<tbody>
											<tr><td><?php echo __('Total'); ?></td><td>{in_total}</td></tr>
											</tbody>
											</table>

											<table class="table dt-on-steroids mb0">
											<thead><tr><th width="50%"><?php echo __('Other Details'); ?></th><th><?php echo __('Tag'); ?></th></tr></thead>
											<tbody>
											<tr><td><?php echo __('Current Date'); ?></td><td>{current_date}</td></tr>
											<tr><td><?php echo __('Comments and Reasons for Decline'); ?></td><td>{ap_comments}</td></tr>
											</tbody>
											</table>
											 </div>
								      </div>
								      <div class="modal-footer">
								        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
								      </div>
								    </div><!-- modal-content -->
								  </div><!-- modal-dialog -->
								</div>
							  <hr>

							</div>
						  </div>
                         <!-- Custom notification to send to selected reviewers when an application enters a stage -->
							    <div class="form-group">
									<label class="col-sm-4"><i class="bold-label">You can also optionally send notification to reviewers when an application is in this stage(Note: Reviewers with access to this stage will receive notifications)</i></label>
									<label class="col-sm-4"><i class="bold-label">Send Notification</i></label>
										<div class="col-sm-8">
										  <select id="send_notification_to_reviewer" name="send_notification_to_reviewer" class="form-control" onChange="if(this.value == '1'){ 
										  document.getElementById('notification_area_reviwers').style.display = 'block'; 
										 
										  }else{ document.getElementById('notification_area_reviwers').style.display = 'none'; }">
											<option value='0'>No</option>
											<option value='1' <?php echo ($notification) ? 'selected="selected"' : '' ?>>Yes</option>
										  </select>
												</div>
										</div>
										 <div class="form-group" id="notification_area_reviwers" name="notification_area_reviwers" style="display: <?php echo ($notification) ? 'block' : 'none' ?>;">
										<div class="form-group">
										<label class="col-sm-4"><i class="bold-label"><?php echo __('Mail Subject for Reviewers'); ?></i></label>
										<div class="col-sm-8">
										  <input type="text" id="mail_subject_reviewer" name="mail_subject_reviewer" class='form-control' value="<?php echo ($notification) ? $notification->getTitleReviewer() : '' ?>">
										</div>
									  </div>
							  	
										<div class="form-group">
										<label class="col-sm-4"><i class="bold-label"><?php echo __('Mail Content for Reviewers'); ?></i></label>
										<div class="col-sm-8">
										  <textarea id="mail_content_reviewer" name="mail_content_reviewer" class='form-control'><?php  echo ($notification) ? $notification->getContentReviewer() : '' ?></textarea>
										</div>
									  </div>
							  	
										<div class="form-group">
										<label class="col-sm-4"><i class="bold-label"><?php echo __('SMS Content for Reviewers'); ?></i></label>
										<div class="col-sm-8">
										  <textarea id="sms_content_reviewer" name="sms_content_reviewer" class='form-control'><?php  echo ($notification) ? $notification->getContentSmsReviewer() : '' ?></textarea>
										</div>
									   </div>

                                <div class="form-group">
                                    <label class="col-sm-4"><i class="bold-label"><?php echo __('Select groups you want to receive the notification at this stage'); ?></i></label>
                                    <div class="col-sm-8">
                                        <select name='stage_sms_groups[]' id='stage_sms_groups' multiple>
                                            <?php
                                            $selected = "";
                                            $q = Doctrine_Query::create()
                                                ->from("MfGuardGroup a")
                                                ->orderBy("a.name ASC");
                                            $groups = $q->execute();
                                            foreach($groups as $group)
                                            {
                                                $selected = "";

                                                if(!$form->getObject()->isNew()) {
                                                    $q = Doctrine_Query::create()
                                                        ->from("ReviewerGroupNotifications a")
                                                        ->where("a.workflow_id = ?", $form->getObject()->getId())
                                                        ->andWhere("a.group_id = ?", $group->getId());
                                                    $workflow_reviewer = $q->fetchOne();

                                                    if ($workflow_reviewer) {
                                                        $selected = "selected='selected'";
                                                    }
                                                }

                                                echo "<option value='".$group->getId()."' ".$selected.">".ucfirst($group->getName())."</option>";
                                            }
                                            ?>
                                        </select>

                                        <script>
                                            jQuery(document).ready(function(){
                                                var demo2 = $('[id="stage_sms_groups"]').bootstrapDualListbox();
                                            });
                                        </script>
                                    </div>
                                </div>
									</div>
							  <!-- End Custom notification -->

                    </div>

                   <div class="panel-footer">
                        <button class="btn btn-danger mr10"><?php echo __('Reset'); ?></button><button type="submit" class="btn btn-primary" name="submitbuttonname" id="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button>
                  </div>

                  </div>

</div>

</form>

<script>
jQuery(document).ready(function(){

  $( "#addgroup" ).click(function() {
      $("#groups").append("<div class='form-group' class='formgroup'><label class='col-sm-4'>Name</label><div class='col-sm-8'> <input type='text' name='name[]' class='form-control' /></div><label class='col-sm-4'>Description</label><div class='col-sm-8'><textarea name='description[]' class='form-control' /></textarea></div><a style='float: right; margin-top: 10px;' href='#' class='panel-close'>&times;</a></div>");
  });

  // Date Picker
  jQuery('#datepicker1').datepicker();
  jQuery('#datepicker2').datepicker();


});
</script>
