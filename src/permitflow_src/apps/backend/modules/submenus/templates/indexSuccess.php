<?php
use_helper("I18N");

$audit = new Audit();
$audit->saveAudit("", "Accessed stage settings");
/**
 * OTB patch
 * Support old permision access_stages
 */
if($sf_user->mfHasCredential("managestages"))
{
?>
<div class="contentpanel">
<div class="panel panel-dark">
	<div class="panel-heading">
			<h3 class="panel-title"><?php if($service){ echo $service->getTitle(); ?> -&gt; <?php } ?><?php echo __('Stages'); ?></h3>
			<div class="pull-right">
	        <a class="btn btn-primary-alt settings-margin42" id="newstage" href="<?php echo public_path(); ?>backend.php/submenus/new/filter/<?php echo $filter; ?>"><?php echo __('Add New Stage'); ?></a>
		 </div>
	</div>

	<div class="panel panel-body panel-body-nopadding ">
 			 <div class="panel-group panel-group" id="accordion2">
    <?php

		$list_of_stages = array();

		foreach ($stages as $stage)
		{
			$list_of_stages[$stage->getOrderNo()] = $stage->getTitle();
		}

	  $count = 0;
		foreach ($stages as $stage)
		{
			$count++;
			?>
			<div class="panel panel-default">
			<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion2" href="#collapseOne<?php echo $count; ?>">
							<?php echo $stage->getTitle(); ?>
					</a>
					</h4>
			</div>
			<div id="collapseOne<?php echo $count; ?>" class="panel-collapse collapse <?php if($count == 1){ ?>in<?php } ?>">
					<div class="panel-body" style="padding: 20px;">
							<a class="btn btn-primary btn-lg btn-form" style="margin-right: 10px;" href="/backend.php/submenus/edit/id/<?php echo $stage->getId(); ?>"><span class="fa fa-edit"></span> Edit Stage</a>
							<a class="btn btn-primary btn-lg btn-form" style="margin-right: 10px;" href="/backend.php/submenus/actions/id/<?php echo $stage->getId(); ?>"><span class="fa fa-edit"></span> Actions</a>
							<a class="btn btn-primary btn-lg btn-form" style="margin-right: 10px;" href="/backend.php/submenus/groups/id/<?php echo $stage->getId(); ?>"><span class="fa fa-edit"></span> Group Access</a>
							<a class="btn btn-primary btn-lg btn-form" style="margin-right: 10px;" href="/backend.php/submenus/tasks/id/<?php echo $stage->getId(); ?>"><span class="fa fa-edit"></span> Allowed Tasks</a>
							<a class="btn btn-danger btn-lg btn-form" style="margin-right: 10px;" href="/backend.php/submenus/delete/id/<?php echo $stage->getId(); ?>" onClick="if(confirm('Are you sure?')){ return true; }else{ return false; }"><span class="fa fa-edit"></span> Delete</a>

							<select class='form-control pull-right' style="width: 200px;" onChange="window.location='/backend.php/submenus/index/move/<?php echo $stage->getId(); ?>/to/' + this.value;">
								<option>Change order...</option>
								<option value="1">- Top -</option>
								<?php
								foreach($list_of_stages as $key => $value)
								{
										echo "<optgroup label='".$value."'>";
											echo "<option value='".($key+1)."'>Here</option>";
										echo "</optgroup>";
								}
								?>
						</select>
					</div>
			</div>
			</div>
    <?php
		}
		?>
	</div>
</div>
</div><!--panel-body-->
</div><!--panel-dark-->
<?php
}
else
{
  include_partial("accessdenied");
}
?>
