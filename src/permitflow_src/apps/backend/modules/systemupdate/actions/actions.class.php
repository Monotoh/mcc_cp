<?php
/**
 * Systemupdate actions.
 *
 * Update Management Service
 *
 * @package    backend
 * @subpackage systemupdate
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
class systemupdateActions extends sfActions
{
   /**
   * Executes 'Check' action 
   * 
   * Checks for available updates
   *
   * @param sfRequest $request A request object
   */
    public function executeCheck(sfWebRequest $request)
    {

    }
        
   /**
   * Executes 'Update' action 
   * 
   * Applies available updates
   *
   * @param sfRequest $request A request object
   */
    public function executeUpdate(sfWebRequest $request)
    {

    }
    
   /**
   * Executes 'Backup' action 
   * 
   * Backups up current system code
   *
   * @param sfRequest $request A request object
   */
    public function executeBackup(sfWebRequest $request)
    {

    }
}
