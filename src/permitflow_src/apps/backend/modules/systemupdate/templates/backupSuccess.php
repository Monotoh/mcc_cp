<?php
/**
 * backupSuccess.php template.
 *
 * Backups up current system code
 *
 * @package    backend
 * @subpackage systemupdate
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>
<div class="g12" style="padding-left: 3px;">
    <form style="margin-bottom: 0px;">
        <label style='height: 30px; margin-top: 0px;'>
        <div style='float: left; font-size: 20px; font-weight: 700;'>Backup System Code</div>
        <div style="float: right; margin-top: -12px;"> </div>
        </label>
    </form>

	<div id='notifications' name='notifications'></div>
    <div>
    <?php
	
	 $prefix_folder = dirname(__FILE__)."/../../../../../../public_html";
	 require_once($prefix_folder."/masterupdate.php");

     $update_script_file_name = $prefix_folder."/update.zip";
	 $update_script_md5_checksum_filename = $prefix_folder."/update.MD5";
     $backup_script_file_name = $prefix_folder."/backup.zip";
	 $backup_script_md5_checksum_filename = $prefix_folder."/system.MD5";
	 $source_code_folder = $prefix_folder."/../admincp1.3/";
	 
	 $update_server = sfConfig::get('app_mysql_host');
	
	  backup_folder($source_code_folder, $backup_script_file_name);
	?>
    </div>
</div>


