<?php
/**
 * updateSuccess.php template.
 *
 * Applies available updates
 *
 * @package    backend
 * @subpackage systemupdate
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>
<div class="g12" style="padding-left: 3px;">
    <form style="margin-bottom: 0px;">
        <label style='height: 30px; margin-top: 0px;'>
        <div style='float: left; font-size: 20px; font-weight: 700;'>Update System</div>
        <div style="float: right; margin-top: -12px;"> </div>
        </label>
    </form>

	<div id='notifications' name='notifications'></div>
	<div>
    <?php
	 
	 $prefix_folder = dirname(__FILE__)."/../../../../../../public_html";
	 require_once($prefix_folder."/masterupdate.php");

     $update_script_file_name = $prefix_folder."/update.zip";
	 $update_script_md5_checksum_filename = $prefix_folder."/update.MD5";
     $backup_script_file_name = $prefix_folder."/backup.zip";
	 $backup_script_md5_checksum_filename = $prefix_folder."/system.MD5";
	 $source_code_folder = $prefix_folder."/../admincp1.3/";
	 
	 $update_server = sfConfig::get('app_update_server');
	 
	 //Begin update process
	 if(download_update_md5_checksum($update_script_md5_checksum_filename,$update_server))
	 {
		 if(validate_update($update_script_md5_checksum_filename, $backup_script_md5_checksum_filename))
		 {
			 if(download_update($update_script_file_name,$update_server))
			 {
				 //Compare Update MD5 with Downloaded Update
				 if(validate_update_checksums($update_script_md5_checksum_filename,$update_script_file_name))
				 {
					 //If update is valid then proceed
					 //Should probably perform a backup first and label them by version
					 echo "<br>...Starting Backup";
					 backup_folder($source_code_folder, $backup_script_file_name);
					 
					 echo "<br>...Starting Update.";
					 update($update_script_file_name,$source_code_folder);
				 }
				 else
				 {
					 echo "<br>...Invalid Checksums. Update package is not valid or has been tampered!";
				 }
			 }
			 else
			 {
				 echo "<br>...Failed to fetch update package. Invalid update link."; 
			 }
		 }
		 else
		 {
			echo "<br>...Failed to validate checksum. Inconsistency betweeen current system version and update version."; 
		 }
	 }
	 else
	 {
	  	echo "<br>...Failed to fetch update checksum. Invalid update link."; 
	 }
	?>
    </div>
</div>


