<?php

/**
 * Tasks actions.
 *
 * Tasks Management Service.
 *
 * @package    backend
 * @subpackage tasks
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
class tasksActions extends sfActions {
    
    /**
     * OTB patch - Update tasks status
     */
    public function executeSiteVisitUpdate(sfWebRequest $request){
        $status_id = $request->getParameter('site_visit_status') ;
        $task_id = $request->getParameter('task_id') ;
        $q = Doctrine_Query::create()
                ->update('Task t')
                ->set('t.site_visit_status',' ? ',$status_id) 
                ->where('t.id = ?',$task_id);
        $res = $q->execute();
        $this->redirect('/backend.php/tasks/view/id/'.$task_id) ;
        
    }

    /**
     * Executes 'Batch Pick' action
     *
     * Allow reviewers to batch pick tasksS
     *
     * @param sfRequest $request A request object
     */
    public function executeBatchpick(sfWebRequest $request) {
        $picks = $request->getPostParameter("batch_pick");
        $already_picked = array();
        $successfully_picked = array();

        $tasks_manager = new TasksManager();

        foreach ($picks as $pick) {
            if ($tasks_manager->pick_task(1, $_SESSION["SESSION_CUTEFLOW_USERID"], $pick)) {
                $successfully_picked[] = $pick;
            } else {
                $already_picked[] = $pick;
            }
        }

        if (sizeof($successfully_picked) > 0) {
            $this->redirect("/backend.php/dashboard?mpage=1");
        } else {
            $this->redirect("/backend.php/dashboard?mpage=1&late=1");
        }
    }

    /**
     * Executes 'Start Task' action
     *
     * Reviewers can start tasks assigned to themselves
     *
     * @param sfRequest $request A request object
     */
    public function executeStarttask(sfWebRequest $request) {
        $tasks_manager = new TasksManager();

       // $task_id = $tasks_manager->pick_task($request->getParameter('type'), $_SESSION["SESSION_CUTEFLOW_USERID"], $request->getParameter('application'));
        //OTB patch

        $task_id = $tasks_manager->pick_task_no_limitation($request->getParameter('type'), $_SESSION["SESSION_CUTEFLOW_USERID"], $request->getParameter('application'));
        if ($task_id) {
            
            error_log("Debug:::::::::::::::::::::: TASK_ID: ".$task_id);
            $this->redirect("/backend.php/tasks/view/id/" . $task_id);
        } else {
           // $task_id = $tasks_manager->pick_task_no_limitation($request->getParameter('type'), $_SESSION["SESSION_CUTEFLOW_USERID"], $request->getParameter('application'));
            $this->redirect("/backend.php/dashboard?mpage=1&late=1");
           
          // $this->redirect("/backend.php/tasks/view/id/" . $task_id);
        }
    }


    /**
     * Executes 'Pick' action
     *
     * Reviewers can assign tasks to themselves
     *
     * @param sfRequest $request A request object
     */
    public function executePick(sfWebRequest $request) {
        $tasks_manager = new TasksManager();

        if ($tasks_manager->pick_task($request->getParameter('type'), $_SESSION["SESSION_CUTEFLOW_USERID"], $request->getParameter('id'))) {
            $this->redirect("/backend.php/dashboard?mpage=1");
        } else {
            $this->redirect("/backend.php/dashboard?apage=1&late=1");
        }
    }

    public function executeConfirm(sfWebRequest $request) {
        $prefix_folder = dirname(__FILE__) . "/../../../../../lib/vendor/cp_machform/";
        require($prefix_folder . 'includes/init.php');

        require($prefix_folder . 'config.php');
        require($prefix_folder . 'includes/db-core.php');
        require($prefix_folder . 'includes/helper-functions.php');

        $prefix_folder = dirname(__FILE__) . "/../../../../../lib/vendor/cp_machform/";
        require($prefix_folder . 'includes/language.php');
        require($prefix_folder . 'includes/common-validator.php');
        require($prefix_folder . 'includes/view-functions.php');
        require($prefix_folder . 'includes/theme-functions.php');
        require($prefix_folder . 'includes/post-functions.php');
        require($prefix_folder . 'includes/entry-functions.php');
        require($prefix_folder . 'hooks/custom_hooks.php');

        //get data from database
        $dbh = mf_connect_db();
        $ssl_suffix = mf_get_ssl_suffix();

        $form_id = $request->getParameter("id");

        $record_id = $_SESSION['review_id'];

        try {
            $commit_result = mf_commit_form_review($dbh, $form_id, $record_id);

            $q = Doctrine_Query::create()
                    ->from("TaskForms a")
                    ->where("a.task_id = ?", $request->getParameter("taskid"));
            $taskform = $q->fetchOne();
            if ($taskform) {
                $taskform->setEntryId($commit_result['record_insert_id']);
                $taskform->save();
            } else {
                $taskform = new TaskForms();
                $taskform->setTaskId($request->getParameter("taskid"));
                $taskform->setFormId($form_id);
                $taskform->setEntryId($commit_result['record_insert_id']);
                $taskform->save();
            }
        } catch (Exception $ex) {
            echo "Review commit failed";
        }

        return $this->redirect("/backend.php/tasks/view/id/" . $request->getParameter("taskid"));
    }

    /**
     * Executes 'Batch' action
     *
     * Sets the status for the selected tasks
     *
     * @param sfRequest $request A request object
     */
    public function executeBatch(sfWebRequest $request) {
        if ($request->getPostParameter('complete')) {
            $item = Doctrine_Core::getTable('Task')->find(array($request->getPostParameter('complete')));
            if ($item) {
                $item->setStatus("25");
                $item->save();
            }
        }
        if ($request->getPostParameter('cancel')) {
            $item = Doctrine_Core::getTable('Task')->find(array($request->getPostParameter('cancel')));
            if ($item) {
                $item->setStatus("55");
                $item->save();
            }
        }
        exit;
    }

    /**
     * Executes 'List' action
     *
     * Displays list of tasks assigned to current user
     *
     * @param sfRequest $request A request object
     */
    public function executeList(sfWebRequest $request) {
        $this->q = Doctrine_Query::create()
                ->from("Task a")
                ->leftJoin("a.Owner b")
                ->leftJoin("a.Application c")
                ->leftJoin("a.Creator d")
                ->where("a.owner_user_id = ? and a.status = ? and c.id = a.application_id", array($_SESSION["SESSION_CUTEFLOW_USERID"], 1))
               // ->where("a.owner_user_id = ? and a.status = ? and c.id = a.application_id and a.archived = 0", array($_SESSION["SESSION_CUTEFLOW_USERID"], 1))//OTB - archiving CPMIS old tasks
                //->orWhere("a.creator_user_id = ? and a.status = ? and c.id = a.application_id", array($_SESSION["SESSION_CUTEFLOW_USERID"], 2))
               // ->orWhere("a.creator_user_id = ? and a.status = ? and c.id = a.application_id and a.archived = 0", array($_SESSION["SESSION_CUTEFLOW_USERID"], 2))//OTB - archiving CPMIS old tasks
                ->orderBy("a.id ASC");
         $this->pager = $this->q->execute() ;

        /*$this->pager = new sfDoctrinePager('Task', 10);
        $this->pager->setQuery($this->q);
        $this->pager->setPage($request->getParameter('page', 1));
        $this->pager->init();*/

        $this->drafts_q = Doctrine_Query::create()
                ->from("Task a")
                ->leftJoin("a.Owner b")
                ->leftJoin("a.Application c")
                ->leftJoin("a.Taskform t")
                ->where("a.owner_user_id = b.nid")
                ->andWhere("a.owner_user_id = ?", $_SESSION["SESSION_CUTEFLOW_USERID"])
                ->andWhere("a.status = ?", 1)
				->andWhere("a.archived = 0")//OTB - archiving CPMIS old tasks
                ->andWhere("c.id = a.application_id")
                ->andWhere("t.id > 0")
                ->orderBy("a.id ASC");

        $this->drafts_pager = new sfDoctrinePager('Task', 10);
        $this->drafts_pager->setQuery($this->drafts_q);
        $this->drafts_pager->setPage($request->getParameter('dpage', 1));
        $this->drafts_pager->init();

        $this->assigned_q = Doctrine_Query::create()
                ->from("Task a")
                ->leftJoin("a.Application c")
                ->where("a.creator_user_id = ?", $_SESSION["SESSION_CUTEFLOW_USERID"])
                ->andWhere("a.owner_user_id <> ?", $_SESSION["SESSION_CUTEFLOW_USERID"])
                ->andWhere("a.status = ? OR a.status = ?", array(1, 2))
				->andWhere("a.archived = 0")//OTB - archiving CPMIS old tasks
                ->andWhere("c.id = a.application_id")
                ->orderBy("a.id ASC");
          $this->assigned_pager = $this->assigned_q->execute();
        /*$this->assigned_pager = new sfDoctrinePager('Task', 10);
        $this->assigned_pager->setQuery($this->assigned_q);
        $this->assigned_pager->setPage($request->getParameter('apage', 1));
        $this->assigned_pager->init();*/
        
        

        $this->completed_q = Doctrine_Query::create()
                ->from("Task a")
                ->leftJoin("a.Owner b")
                ->leftJoin("a.Application c")
                ->where("a.owner_user_id = ? AND (a.status = ? OR a.status = ?)", array($_SESSION["SESSION_CUTEFLOW_USERID"], 2, 25))
				
                ->orWhere("a.creator_user_id = ? AND a.status = ?", array($_SESSION["SESSION_CUTEFLOW_USERID"], 25))
                ->andWhere("a.archived = 0")//OTB - archiving CPMIS old tasks
                ->orderBy("a.id ASC");
        error_log("CCCCCCCCCCCCCCCCCCc ".$this->completed_q);
        $this->completed_pager=$this->completed_q->execute();

       /*$this->completed_pager = new sfDoctrinePager('Task', 10);
        $this->completed_pager->setQuery($this->completed_q);
        $this->completed_pager->setPage($request->getParameter('cpage', 1));
        $this->completed_pager->init();*/
        
        

        //OTB patch - For Skipped tasks
        if ($this->getUser()->mfHasCredential('accessalltasks')) {
            $q = Doctrine_Query::create()
                    ->from("Task a")
                    ->leftJoin("a.Owner b")
                    ->leftJoin("a.Application c")
                    ->where("a.owner_user_id = b.nid")
                    //->andWhere("b.strdepartment = ?", $filterdepartment)
                    ->andWhere("a.status = ?", 75)
					->andWhere("a.archived = 0")//OTB - archiving CPMIS old tasks
                    ->andWhere("c.id = a.application_id")
                    ->orderBy("a.id ASC");
            $this->skippedtasks = $q->execute();
        } else {
            $q = Doctrine_Query::create()
                    ->from("Task a")
                    ->leftJoin("a.Owner b")
                    ->leftJoin("a.Application c")
                    ->where("a.owner_user_id = b.nid")
                    ->andWhere("a.owner_user_id = ?", $_SESSION["SESSION_CUTEFLOW_USERID"])
                    //->andWhere("b.strdepartment = ?", $filterdepartment)
					->andWhere("a.archived = 0")//OTB - archiving CPMIS old tasks
                    ->andWhere("a.status = ?", 75)
                    ->andWhere("c.id = a.application_id")
                    ->orderBy("a.id ASC");
            $this->skippedtasks = $q->execute();
        }
        //Archived applications
        $archived_q = Doctrine_Query::create()
                    ->from("Task a")
                    ->leftJoin("a.Owner b")
                    ->leftJoin("a.Application c")
                    ->where("a.owner_user_id = b.nid")
                    ->andWhere("a.owner_user_id = ?", $_SESSION["SESSION_CUTEFLOW_USERID"])
                    //->andWhere("b.strdepartment = ?", $filterdepartment)
                    ->andWhere("a.archived = ?", 1)
                    ->andWhere("c.id = a.application_id")
                    ->orderBy("a.id ASC");
           // $this->archivedtasks = $q->execute();
            
        $this->archivedtasks = new sfDoctrinePager('Task', 100);
        $this->archivedtasks->setQuery($archived_q);
        $this->archivedtasks->setPage($request->getParameter('page', 1));
        $this->archivedtasks->init();
        
         //Applications To approve 
        //get logged in user department and allow him or her to have access to all tasks that have been assigned in his department
        //in status completing
        $q_depart = Doctrine_Query::create()
                ->from('CfUser c')
                ->where('c.nid = ? ', $_SESSION["SESSION_CUTEFLOW_USERID"])
                ->limit(1);
        $q_depart_re = $q_depart->fetchOne();
        
        $to_approve_q = Doctrine_Query::create()
                    ->from("Task a")
                    ->leftJoin("a.Owner b")
                    ->leftJoin("a.Application c")
                    ->where("a.status = ?", 2) //awaiting confirm completion by the person who assigned
                    ->andWhere("a.archived = ?", 0)
                    ->andWhere("c.id = a.application_id")
                    ->andWhere("c.parent_submission = ? ",0)
                    ->andWhere("c.deleted_status = ? ",0)
                    ->orderBy("a.id ASC");
           // $this->archivedtasks = $q->execute();
        if($q_depart_re){
            $to_approve_q->andWhere("b.strdepartment = ?", $q_depart_re->getStrdepartment()); 
        }
        
        $this->toapprovetasks = $to_approve_q->execute();
            
        /*$this->toapprovetasks = new sfDoctrinePager('Task', 100);
        $this->toapprovetasks->setQuery($to_approve_q);
        $this->toapprovetasks->setPage($request->getParameter('page', 1));
        $this->toapprovetasks->init();*/

        $_SESSION['department'] = $this->filterdepartment;
       // error_log("Logged in user department >>> ".$this->filterdepartment);
       $this->setLayout('layout-metronic');
    }

    /**
     * Executes 'Department' action
     *
     * Displays list of tasks assigned to current user
     *
     * @param sfRequest $request A request object
     */
    public function executeDepartment(sfWebRequest $request) {
        $this->viewingapproved = false;
        $submenus = "";

        $q = Doctrine_Query::create()
                ->from('SubMenus a');
        $stages = $q->execute();

        $qcount = 0;
        foreach ($stages as $stage) {
            if ($this->getUser()->mfHasCredential('accesssubmenu' . $stage->getId())) {
                $qcount++;
                if ($qcount == 1) {
                    $submenus .= "a.approved = " . $stage->getId() . " ";
                } else {
                    $submenus .= " OR a.approved = " . $stage->getId() . " ";
                }
            }
        }

        $this->filterdepartment = $request->getParameter("filter");

        $_SESSION['department'] = $this->filterdepartment;

        $q = Doctrine_Query::create()
                ->from("FormEntry a")
                ->where("a.parent_submission = ?", 0)
                ->andWhere("a.deleted_status = ?", 0)
                ->andWhere($submenus)
                ->orderBy("a.date_of_submission DESC");
        $this->applications = $q->execute();

        if ($request->getParameter("status") == "approved") {
            $this->q = Doctrine_Query::create()
                    ->from("Task a")
                    ->leftJoin("a.Application c")
                    ->andWhere("a.creator_user_id = ?", $_SESSION["SESSION_CUTEFLOW_USERID"])
                    ->andWhere("a.status = ?", 2)
                    ->andWhere("c.id = a.application_id")
                    ->orderBy("a.id DESC");
            $this->viewingapproved = true;
        } else {
            if ($this->getUser()->mfHasCredential('accessalltasks')) {

                if ($request->getParameter("status")) {
                    $this->filterstatus = $request->getParameter("status");
                    $this->q = Doctrine_Query::create()
                            ->from("Task a")
                            ->leftJoin("a.Owner b")
                            ->leftJoin("a.Application c")
                            ->where("a.owner_user_id = b.nid")
                            ->andWhere("b.strdepartment = ?", $this->filterdepartment)
                            ->andWhere("a.status = ?", $request->getParameter("status"))
                            ->andWhere("c.id = a.application_id")
                            ->orderBy("a.id DESC");
                } else {
                    $this->q = Doctrine_Query::create()
                            ->from("Task a")
                            ->leftJoin("a.Owner b")
                            ->leftJoin("a.Application c")
                            ->where("a.owner_user_id = b.nid")
                            ->andWhere("b.strdepartment = ?", $this->filterdepartment)
                            ->andWhere("a.status = ?", 1)
                            ->andWhere("c.id = a.application_id")
                            ->orderBy("a.id DESC");
                }
            } else {

                if ($request->getParameter("status")) {
                    $this->filterstatus = $request->getParameter("status");
                    $this->q = Doctrine_Query::create()
                            ->from("Task a")
                            ->leftJoin("a.Owner b")
                            ->leftJoin("a.Application c")
                            ->where("a.owner_user_id = b.nid")
                            ->andWhere("a.owner_user_id = ?", $_SESSION["SESSION_CUTEFLOW_USERID"])
                            ->andWhere("b.strdepartment = ?", $this->filterdepartment)
                            ->andWhere("a.status = ?", $request->getParameter("status"))
                            ->andWhere("c.id = a.application_id")
                            ->orderBy("a.id DESC");
                } else {
                    $this->q = Doctrine_Query::create()
                            ->from("Task a")
                            ->leftJoin("a.Owner b")
                            ->leftJoin("a.Application c")
                            ->where("a.owner_user_id = b.nid")
                            ->andWhere("a.owner_user_id = ?", $_SESSION["SESSION_CUTEFLOW_USERID"])
                            ->andWhere("b.strdepartment = ?", $this->filterdepartment)
                            ->andWhere("a.status = ?", 1)
                            ->andWhere("c.id = a.application_id")
                            ->orderBy("a.id DESC");
                }
            }
        }

        $this->pager = new sfDoctrinePager('Task', 10);
        $this->pager->setQuery($this->q);
        $this->pager->setPage($request->getParameter('page', 1));
        $this->pager->init();

        $_SESSION['department'] = $this->filterdepartment;
    }

    /**
     * Executes 'Sent' action
     *
     * Displays list of tasks assigned to current user
     *
     * @param sfRequest $request A request object
     */
    public function executeSent(sfWebRequest $request) {
        $submenus = "";

        $q = Doctrine_Query::create()
                ->from('SubMenus a');
        $stages = $q->execute();

        $qcount = 0;
        foreach ($stages as $stage) {
            if ($this->getUser()->mfHasCredential('accesssubmenu' . $stage->getId())) {
                $qcount++;
                if ($qcount == 1) {
                    $submenus .= "a.approved = " . $stage->getId() . " ";
                } else {
                    $submenus .= " OR a.approved = " . $stage->getId() . " ";
                }
            }
        }

        $q = Doctrine_Query::create()
                ->from("FormEntry a")
                ->where("a.parent_submission = ?", 0)
                ->andWhere("a.deleted_status = ?", 0)
                ->andWhere($submenus)
                ->orderBy("a.date_of_submission DESC");
        $this->applications = $q->execute();

        $q = Doctrine_Query::create()
                ->from("Task a")
                ->where("a.owner_user_id = ?", $_SESSION["SESSION_CUTEFLOW_USERID"])
                ->andWhere("a.status = ?", 2)
                ->orderBy("a.id DESC");
        $this->tasks = $q->execute();
    }

    /**
     * Executes 'Approve' action
     *
     * Displays list of tasks assigned to current user
     *
     * @param sfRequest $request A request object
     */
    public function executeApprove(sfWebRequest $request) {
        $submenus = "";

        $q = Doctrine_Query::create()
                ->from('SubMenus a');
        $stages = $q->execute();

        $qcount = 0;
        foreach ($stages as $stage) {
            if ($this->getUser()->mfHasCredential('accesssubmenu' . $stage->getId())) {
                $qcount++;
                if ($qcount == 1) {
                    $submenus .= "a.approved = " . $stage->getId() . " ";
                } else {
                    $submenus .= " OR a.approved = " . $stage->getId() . " ";
                }
            }
        }

        $q = Doctrine_Query::create()
                ->from("FormEntry a")
                ->where("a.parent_submission = ?", 0)
                ->andWhere("a.deleted_status = ?", 0)
                ->andWhere($submenus)
                ->orderBy("a.date_of_submission DESC");
        $this->applications = $q->execute();

        $q = Doctrine_Query::create()
                ->from("Task a")
                ->where("a.creator_user_id = ?", $_SESSION["SESSION_CUTEFLOW_USERID"])
                ->andWhere("a.status = ?", 2)
                ->orderBy("a.id DESC");
        $this->tasks = $q->execute();
    }

    /**
     * Executes 'Trash' action
     *
     * Displays list of tasks assigned to current user
     *
     * @param sfRequest $request A request object
     */
    public function executeTrash(sfWebRequest $request) {
        $submenus = "";

        $q = Doctrine_Query::create()
                ->from('SubMenus a');
        $stages = $q->execute();

        $qcount = 0;
        foreach ($stages as $stage) {
            if ($this->getUser()->mfHasCredential('accesssubmenu' . $stage->getId())) {
                $qcount++;
                if ($qcount == 1) {
                    $submenus .= "a.approved = " . $stage->getId() . " ";
                } else {
                    $submenus .= " OR a.approved = " . $stage->getId() . " ";
                }
            }
        }

        $q = Doctrine_Query::create()
                ->from("FormEntry a")
                ->where("a.parent_submission = ?", 0)
                ->andWhere("a.deleted_status = ?", 0)
                ->andWhere($submenus)
                ->orderBy("a.date_of_submission DESC");
        $this->applications = $q->execute();

        $q = Doctrine_Query::create()
                ->from("Task a")
                ->where("a.owner_user_id = ?", $_SESSION["SESSION_CUTEFLOW_USERID"])
                ->andWhere("a.status = ?", 55)
                ->orderBy("a.id DESC");
        $this->tasks = $q->execute();
    }

    /**
     * Executes 'Pending' action
     *
     * Displays pending tasks assigned to current user
     *
     * @param sfRequest $request A request object
     */
    public function executePending(sfWebRequest $request) {
        
    }

    /**
     * Executes 'Alltasks' action
     *
     * Displays all tasks in the system
     *
     * @param sfRequest $request A request object
     */
    public function executeAlltasks(sfWebRequest $request) {
        if ($request->getPostParameter('task_department') != "") {
            $this->task_department = $request->getPostParameter('task_department');
            if ($request->getPostParameter('task_department') == "0") {
                //Get All Departments
                if ($request->getPostParameter('task_sender') == "0") {
                    //Get All Reviewers
                    $q = Doctrine_Query::create()
                            ->from('Task a');

                    if ($request->getPostParameter('task_status') != "0") {
                        $q->where('a.status = ?', $request->getPostParameter('task_status'));
                        $this->task_status = $request->getPostParameter('task_status');
                    }
                    if ($request->getPostParameter('task_priority') != "0") {
                        $q->andWhere('a.priority = ?', $request->getPostParameter('task_priority'));
                        $this->task_priority = $request->getPostParameter('task_priority');
                    }

                    $q->orderBy('a.id DESC');
                    $this->tasks = $q->execute();
                } else {
                    //Get Selected Reviewer
                    $q = Doctrine_Query::create()
                            ->from('Task a')
                            ->where('a.creator_user_id = ?', $request->getPostParameter('task_sender'));
                    $this->task_sender = $request->getPostParameter('task_sender');

                    if ($request->getPostParameter('task_status') != "0") {
                        $q->andWhere('a.status = ?', $request->getPostParameter('task_status'));
                        $this->task_status = $request->getPostParameter('task_status');
                    }
                    if ($request->getPostParameter('task_priority') != "0") {
                        $q->andWhere('a.priority = ?', $request->getPostParameter('task_priority'));
                        $this->task_priority = $request->getPostParameter('task_priority');
                    }

                    $q->orderBy('a.id DESC');
                    $this->tasks = $q->execute();
                }
            } else {
                //Get Selected Department
                $q = Doctrine_Query::create()
                        ->from('Department a')
                        ->where('a.id = ?', $request->getPostParameter('task_department'));
                $department = $q->fetchOne();
                $this->task_department = $request->getPostParameter('task_department');

                $q = Doctrine_Query::create()
                        ->from('CfUser a')
                        ->where('a.strdepartment = ?', $department->getDepartmentName())
                        ->orderBy('a.strfirstname DESC');
                $reviewers = $q->execute();

                $q = Doctrine_Query::create()
                        ->from('Task a');

                foreach ($reviewers as $reviewer) {
                    $q->orWhere('a.owner_user_id = ?', $reviewer->getNid());

                    //Check if any processes have missing tasks
                    $qry = Doctrine_Query::create()
                            ->from('CfCirculationprocess a')
                            ->where('a.nuserid = ?', $reviewer->getNid())
                            ->andWhere('a.nresendcount <> ?', 2);
                    $processes = $qry->execute();
                    foreach ($processes as $process) {
                        $circulation_id = $process->getNcirculationformid();

                        $qry = Doctrine_Query::create()
                                ->from('FormEntry a')
                                ->where('a.circulation_id = ?', $circulation_id);
                        $application = $qry->fetchOne();

                        if ($application) {
                            $qry = Doctrine_Query::create()
                                    ->from('Task a')
                                    ->where('a.owner_user_id = ?', $reviewer->getNid())
                                    ->andWhere('a.status = ? OR a.status = ? OR a.status = ? OR a.status = ? OR a.status = ?', array('1', '2', '3', '4', '5'))
                                    ->andWhere('a.application_id = ?', $application->getId())
                                    ->orderBy('a.id DESC');
                            $tasks = $qry->execute();

                            if (sizeof($tasks) <= 0) {
                                $task = new Task();
                                $task->setType(2);
                                $task->setCreatorUserId($reviewer->getNid());
                                $task->setOwnerUserId($reviewer->getNid());
                                $task->setDuration("0");
                                $task->setStartDate(date('Y-m-d'));
                                $task->setEndDate(date('Y-m-d'));
                                $task->setPriority('3');
                                $task->setIsLeader("0");
                                $task->setActive("1");
                                $task->setStatus("1");
                                $task->setLastUpdate(date('Y-m-d'));
                                $task->setDateCreated(date('Y-m-d'));
                                $task->setRemarks("");
                                $task->setApplicationId($application->getId());
                                $task->save();
                            }
                        }
                    }
                }

                if ($request->getPostParameter('task_status') != "0") {
                    $q->andWhere('a.status = ?', $request->getPostParameter('task_status'));
                    $this->task_status = $request->getPostParameter('task_status');
                }
                if ($request->getPostParameter('task_priority') != "0") {
                    $q->andWhere('a.priority = ?', $request->getPostParameter('task_priority'));
                    $this->task_priority = $request->getPostParameter('task_priority');
                }

                $q->orderBy('a.id DESC');
                $this->tasks = $q->execute();
            }
        } else {
            //No Filters
            $q = Doctrine_Query::create()
                    ->from('Task a')
                    ->orderBy('a.id DESC');
            $this->tasks = $q->execute();
        }
    }

    /**
     * Executes 'Assigned' action
     *
     * Displays tasks the current reviewer has assigned to others (Awaiting Approval)
     *
     * @param sfRequest $request A request object
     */
    public function executeAssigned(sfWebRequest $request) {
        $q = Doctrine_Query::create()
                ->from('Task a')
                ->where('a.creator_user_id = ?', $_SESSION["SESSION_CUTEFLOW_USERID"])
                ->andWhere('a.status = ? OR a.status = ? OR a.status = ? OR a.status = ? OR a.status = ?', array('1', '2', '3', '4', '5'))
                ->orderBy('a.id DESC');
        $this->tasks = $q->execute();
    }

    /**
     * Executes 'Assignedbyme' action
     *
     * Displays tasks the current reviewer has assigned to others
     *
     * @param sfRequest $request A request object
     */
    public function executeAssignedbyme(sfWebRequest $request) {
        $q = Doctrine_Query::create()
                ->from('Task a')
                ->where('a.creator_user_id = ?', $_SESSION["SESSION_CUTEFLOW_USERID"])
                ->andWhere('a.status = ? OR a.status = ? OR a.status = ? OR a.status = ? OR a.status = ?', array('1', '2', '3', '4', '5'))
                ->orderBy('a.id DESC');
        $this->tasks = $q->execute();
    }

    /**
     * Executes 'New' action
     *
     * Allows current reviewer to assign a new task to other reviewers or to themselves
     *
     * @param sfRequest $request A request object
     */
    public function executeNew(sfWebRequest $request) {
        if ($request->getParameter("application")) {
            $this->appid = $request->getParameter("application");
        }

        if ($request->getParameter("submenu")) {
            $submenu = $request->getParameter("submenu");

            $q = Doctrine_Query::create()
                    ->from("FormEntry a")
                    ->where("a.approved = ?", $submenu)
                    ->orderBy("a.application_id DESC");
            $this->applications = $q->execute();
        }
    }

    /**
     * Executes 'Save' action
     *
     * Saves new task details to the database
     *
     * @param sfRequest $request A request object
     */
    public function executeSave(sfWebRequest $request) {
        error_log("Debug:::::::::::::::::::::::::::: workflow ".$request->getPostParameter("workflow_type"));
        $this->success = false;
        $move_to_comments_review=FALSE;
        if ($request->getPostParameter("workflow_type") == 0) {
            error_log("Debug::::: workflow is okay");
            if ($request->getPostParameter("reviewers") || $request->getPostParameter("workflow")) {
                error_log("Debug::::: workflow is okay, reviewers are here");
                if ($request->getPostParameter("workflow") == "none") {
                    error_log("Debug::::: workflow is NONE");
                    $reviewers = $request->getPostParameter("reviewers");
                    $supportreviewers = $request->getPostParameter("supporters");
                    $otherreviewers = $request->getPostParameter("otherreviewers");
                } else {
                    error_log("Debug::::: workflow is AIN'T NO NONE THERE IS SOMETHING");
                    $q = Doctrine_Query::create()
                            ->from('WorkflowReviewers a')
                            ->where('a.workflow_id = ?', $request->getPostParameter("workflow"))
                            ->orderBy('a.id ASC');
                    $workflowreviewers = $q->execute();
                    foreach ($workflowreviewers as $workflowreviewer) {
                        $reviewers[] = $workflowreviewer->getReviewerId();
                    }
                }

                if ($request->getPostParameter("application")) {
                    
                    error_log("Debug::::: APPLICATION IS HERE");
                    $count = 0;
                    $previous_task_id = 0;
                    foreach ($reviewers as $reviewer) {
                        $application = $request->getPostParameter("application");
                        if ($count == 0) {
                            $task = new Task();
                            $task->setType($request->getPostParameter("task_type"));
                            $task->setCreatorUserId($_SESSION["SESSION_CUTEFLOW_USERID"]);
                            $task->setOwnerUserId($reviewer);
                            $task->setSheetId($request->getPostParameter("task_sheet"));
                            $task->setDuration("0");
                            $task->setStartDate($request->getPostParameter("start_date"));
                            $task->setEndDate($request->getPostParameter("end_date"));
                            $task->setPriority($request->getPostParameter("priority"));
                            $task->setIsLeader("1");
                            $task->setActive("1");
                            $task->setStatus("1");
                            $task->setLastUpdate(date('Y-m-d'));
                            $task->setDateCreated(date('Y-m-d'));
                            $task->setRemarks($request->getPostParameter("description"));
                            $task->setApplicationId($application);
                            $task->save();

                            $this->task = $task;
                            $previous_task_id = $task->getId();
                            $this->success = true;

                            //if application is in submissions, move to circulations
                            $q = Doctrine_Query::create()
                                    ->from('FormEntry a')
                                    ->where('a.id = ?', $application);
                            $fullApplication = $q->fetchOne();

                            $q = Doctrine_Query::create()
                                    ->from('CfUser a')
                                    ->where('a.nid = ?', $reviewer);
                            $treviewer = $q->fetchOne();

                            $audit = new Audit();
                            $audit->saveFullAudit("<a href='/backend.php/tasks/view/id/" . $task->getId() . "'>Assigned " . $task->getTypeName() . " task on " . $fullApplication->getApplicationId() . " to " . $treviewer->getStrfirstname() . " " . $treviewer->getStrlastname() . "</a>", $task->getId(), "task", "", "Pending", $application);
                        } else {
                            error_log("Debug::::: NO APPICATION HERE");
                            $task = new Task();
                            $task->setType($request->getPostParameter("task_type"));
                            $task->setCreatorUserId($_SESSION["SESSION_CUTEFLOW_USERID"]);
                            $task->setOwnerUserId($reviewer);
                            $task->setSheetId($request->getPostParameter("task_sheet"));
                            $task->setDuration("0");
                            $task->setStartDate($request->getPostParameter("start_date"));
                            $task->setEndDate($request->getPostParameter("end_date"));
                            $task->setPriority($request->getPostParameter("priority"));
                            $task->setIsLeader("1");
                            if ($request->getPostParameter("workflow_type") == "1") {
                                $task->setActive("0");
                            } else {
                                $task->setActive("1");
                            }
                            $task->setStatus("1");
                            $task->setLastUpdate(date('Y-m-d'));
                            $task->setDateCreated(date('Y-m-d'));
                            $task->setRemarks($request->getPostParameter("description"));
                            $task->setApplicationId($application);
                            $task->save();
                            $this->task = $task;

                            $taskqueue = new TaskQueue();
                            $taskqueue->setCurrentTaskId($previous_task_id);
                            $taskqueue->setNextTaskId($task->getId());
                            $taskqueue->save();

                            $previous_task_id = $task->getId();
                            $this->success = true;

                            //if application is in submissions, move to circulations
                            $q = Doctrine_Query::create()
                                    ->from('FormEntry a')
                                    ->where('a.id = ?', $application);
                            $fullApplication = $q->fetchOne();

                            $q = Doctrine_Query::create()
                                    ->from('CfUser a')
                                    ->where('a.nid = ?', $reviewer);
                            $treviewer = $q->fetchOne();

                            $audit = new Audit();
                            $audit->saveFullAudit("<a href='/backend.php/tasks/view/id/" . $task->getId() . "'>Assigned " . $task->getTypeName() . " task on " . $fullApplication->getApplicationId() . " to " . $treviewer->getStrfirstname() . " " . $treviewer->getStrlastname() . "</a>", $task->getId(), "task", "", "Pending", $application);
                        }


                        $count++;

                        $q = Doctrine_Query::create()
                                ->from('FormEntry a')
                                ->where('a.id = ?', $application);
                        $this->application = $q->fetchOne();

                        $q = Doctrine_Query::create()
                                ->from("CfUser a")
                                ->where("a.nid = ?", $reviewer);
                        $reviewer = $q->fetchOne();

                        $body = "
                                    Hi " . $reviewer->getStrfirstname() . " " . $reviewer->getStrlastname() . ",<br>
                                    <br>
                                    You have been assigned a new task on " . $this->application->getApplicationId() . ":<br>
                                    <br><br>
                                    &ldquo; " . $request->getPostParameter("description") . " &rdquo;
                                    <br>
                                    <br>
                                    Click here to view the task: <br>
                                    ------- <br>
                                    <a href='http://" . $_SERVER['HTTP_HOST'] . "/backend.php/tasks/view/id/" . $this->task->getId() . "'>Link to " . $this->application->getApplicationId() . " task</a><br>
                                    ------- <br>

                                    <br>
                                    ";

                        $mailnotifications = new mailnotifications();
                        $mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $reviewer->getStremail(), "New Task", $body);
                    }

                    
                    $otbhelper = new OTBHelper();
                    foreach ($otherreviewers as $reviewer) {
                        error_log("Debug::::: OTHER REVIEWERS ARE HERE");
                        //error_log("Debug: Other Reviewers 1 ::::".$reviewer);
                        //error_log($$reviewer);
                        $application = $request->getPostParameter("application");
                        //Call to our function that assigns tasks to all users in a department
                        $task = $otbhelper->AssignTaskToDepartmentReviewers($reviewer,$request->getPostParameter("task_type")
                        ,$_SESSION["SESSION_CUTEFLOW_USERID"],
                        $request->getPostParameter("task_sheet"),$request->getPostParameter("start_date"),
                        $request->getPostParameter("end_date"),$request->getPostParameter("priority"),
                        $request->getPostParameter("description"),$application);
                        
                        if($task){
                            $this->success = true;
                        }
                         
                        //OTB patch - Removed task queuing - Not needed
                        /*else {
                            error_log("Else :::::::::::::::::::::::::::") ;
                            $task = new Task();
                            $task->setType($request->getPostParameter("task_type"));
                            $task->setCreatorUserId($_SESSION["SESSION_CUTEFLOW_USERID"]);
                            $task->setOwnerUserId($reviewer);
                            $task->setSheetId($request->getPostParameter("task_sheet"));
                            $task->setDuration("0");
                            $task->setStartDate($request->getPostParameter("start_date"));
                            $task->setEndDate($request->getPostParameter("end_date"));
                            $task->setPriority($request->getPostParameter("priority"));
                            $task->setIsLeader("0");
                            if ($request->getPostParameter("workflow_type") == "1") {
                                $task->setActive("0");
                            } else {
                                $task->setActive("1");
                            }
                            $task->setStatus("1");
                            $task->setLastUpdate(date('Y-m-d'));
                            $task->setDateCreated(date('Y-m-d'));
                            $task->setRemarks($request->getPostParameter("description"));
                            $task->setApplicationId($application);
                            $task->save();
                            $this->task = $task;

                            $taskqueue = new TaskQueue();
                            $taskqueue->setCurrentTaskId($previous_task_id);
                            $taskqueue->setNextTaskId($task->getId());
                            $taskqueue->save();

                            $previous_task_id = $task->getId();
                            $this->success = true;


                            $q = Doctrine_Query::create()
                                    ->from('FormEntry a')
                                    ->where('a.id = ?', $application);
                            $fullApplication = $q->fetchOne();

                            $q = Doctrine_Query::create()
                                    ->from('CfUser a')
                                    ->where('a.nid = ?', $reviewer);
                            $treviewer = $q->fetchOne();

                            $audit = new Audit();
                            $audit->saveFullAudit("<a href='/backend.php/tasks/view/id/" . $task->getId() . "'>Assigned " . $task->getTypeName() . " task on " . $fullApplication->getApplicationId() . " to " . $treviewer->getStrfirstname() . " " . $treviewer->getStrlastname() . "</a>", $task->getId(), "task", "", "Pending", $application);
                        }*/

                        $q = Doctrine_Query::create()
                                ->from('FormEntry a')
                                ->where('a.id = ?', $application);
                        $this->application = $q->fetchOne();

                        $q = Doctrine_Query::create()
                                ->from("CfUser a")
                                ->where("a.nid = ?", $reviewer);
                        $reviewer = $q->fetchOne();

                        $body = "
                                    Hi " . $reviewer->getStrfirstname() . " " . $reviewer->getStrlastname() . ",<br>
                                    <br>
                                    You have been assigned a new task on " . $this->application->getApplicationId() . ":<br>
                                    <br><br>
                                    &ldquo; " . $request->getPostParameter("description") . " &rdquo;
                                    <br>
                                    <br>
                                    Click here to view the task: <br>
                                    ------- <br>
                                    <a href='http://" . $_SERVER['HTTP_HOST'] . "/backend.php/tasks/view/id/" . $task->getId() . "'>Link to " . $this->application->getApplicationId() . " task</a><br>
                                    ------- <br>

                                    <br>
                                    ";

                        $mailnotifications = new mailnotifications();
                        $mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $reviewer->getStremail(), "New Task", $body);
                        $count++;
                    }

                    foreach ($supportreviewers as $reviewer) {
                        $application = $request->getPostParameter("application");
                        if ($count == 0) {
                            error_log("Debug::::: SUPPORT REVIEWERS ARE HERE");
                            
                            $task = new Task();
                            $task->setType($request->getPostParameter("task_type"));
                            $task->setCreatorUserId($_SESSION["SESSION_CUTEFLOW_USERID"]);
                            $task->setOwnerUserId($reviewer);
                            $task->setSheetId($request->getPostParameter("task_sheet"));
                            $task->setDuration("0");
                            $task->setStartDate($request->getPostParameter("start_date"));
                            $task->setEndDate($request->getPostParameter("end_date"));
                            $task->setPriority($request->getPostParameter("priority"));
                            $task->setIsLeader("0");
                            $task->setActive("1");
                            $task->setStatus("1");
                            $task->setLastUpdate(date('Y-m-d'));
                            $task->setDateCreated(date('Y-m-d'));
                            $task->setRemarks($request->getPostParameter("description"));
                            $task->setApplicationId($application);
                            $task->save();
                            $this->task = $task;
                            $previous_task_id = $task->getId();
                            $this->success = true;


                            $q = Doctrine_Query::create()
                                    ->from('FormEntry a')
                                    ->where('a.id = ?', $application);
                            $fullApplication = $q->fetchOne();

                            $q = Doctrine_Query::create()
                                    ->from('CfUser a')
                                    ->where('a.nid = ?', $reviewer);
                            $treviewer = $q->fetchOne();

                            $audit = new Audit();
                            $audit->saveFullAudit("<a href='/backend.php/tasks/view/id/" . $task->getId() . "'>Assigned " . $task->getTypeName() . " task on " . $fullApplication->getApplicationId() . " to " . $treviewer->getStrfirstname() . " " . $treviewer->getStrlastname() . "</a>", $task->getId(), "task", "", "Pending", $application);
                        } else {
                            error_log("Debug::::: THERE ARE NO SUPPORT REVIEWERS ARE HERE");
                            $task = new Task();
                            $task->setType($request->getPostParameter("task_type"));
                            $task->setCreatorUserId($_SESSION["SESSION_CUTEFLOW_USERID"]);
                            $task->setOwnerUserId($reviewer);
                            $task->setSheetId($request->getPostParameter("task_sheet"));
                            $task->setDuration("0");
                            $task->setStartDate($request->getPostParameter("start_date"));
                            $task->setEndDate($request->getPostParameter("end_date"));
                            $task->setPriority($request->getPostParameter("priority"));
                            $task->setIsLeader("0");
                            if ($request->getPostParameter("workflow_type") == "1") {
                                $task->setActive("0");
                            } else {
                                $task->setActive("1");
                            }
                            $task->setStatus("1");
                            $task->setLastUpdate(date('Y-m-d'));
                            $task->setDateCreated(date('Y-m-d'));
                            $task->setRemarks($request->getPostParameter("description"));
                            $task->setApplicationId($application);
                            $task->save();
                            $this->task = $task;

                            $taskqueue = new TaskQueue();
                            $taskqueue->setCurrentTaskId($previous_task_id);
                            $taskqueue->setNextTaskId($task->getId());
                            $taskqueue->save();

                            $previous_task_id = $task->getId();
                            $this->success = true;


                            $q = Doctrine_Query::create()
                                    ->from('FormEntry a')
                                    ->where('a.id = ?', $application);
                            $fullApplication = $q->fetchOne();

                            $q = Doctrine_Query::create()
                                    ->from('CfUser a')
                                    ->where('a.nid = ?', $reviewer);
                            $treviewer = $q->fetchOne();

                            $audit = new Audit();
                            $audit->saveFullAudit("<a href='/backend.php/tasks/view/id/" . $task->getId() . "'>Assigned " . $task->getTypeName() . " task on " . $fullApplication->getApplicationId() . " to " . $treviewer->getStrfirstname() . " " . $treviewer->getStrlastname() . "</a>", $task->getId(), "task", "", "Pending", $application);
                        }
                        $q = Doctrine_Query::create()
                                ->from('FormEntry a')
                                ->where('a.id = ?', $application);
                        $this->application = $q->fetchOne();

                        $q = Doctrine_Query::create()
                                ->from("CfUser a")
                                ->where("a.nid = ?", $reviewer);
                        $reviewer = $q->fetchOne();

                        $body = "
                                        Hi " . $reviewer->getStrfirstname() . " " . $reviewer->getStrlastname() . ",<br>
                                        <br>
                                        You have been assigned a new task on " . $this->application->getApplicationId() . ":<br>
                                        <br><br>
                                        &ldquo; " . $request->getPostParameter("description") . " &rdquo;
                                        <br>
                                        <br>
                                        Click here to view the task: <br>
                                        ------- <br>
                                        <a href='http://" . $_SERVER['HTTP_HOST'] . "/backend.php/tasks/view/id/" . $this->task->getId() . "'>Link to " . $this->application->getApplicationId() . " task</a><br>
                                        ------- <br>

                                        <br>
                                        ";

                        $mailnotifications = new mailnotifications();
                        $mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $reviewer->getStremail(), "New Task", $body);
                        $count++;
                    	error_log("Debug::::::::::   >>>>>>>>>>>>>>>>>>> COUNT: ".$count);
                    }
                    if ($count==1){
                        foreach($supportreviewers as $supportreviewer){
							error_log("Debug:::::::: SUPPORT REVIEWER IS: ".$supportreviewer);
                            if($supportreviewer==42){
                                $move_to_comments_review=TRUE;
								error_log("Debug:::::::: YES MOVE TO REVIEW");

                            }
                        }
                    }

                    if ($request->getPostParameter("task_type") == 2) {
                        
                        $application = $request->getPostParameter("application");
                        $q = Doctrine_Query::create()
                                ->from('FormEntry a')
                                ->where('a.id = ?', $application);
                        $appentry = $q->fetchOne();
                        if ($appentry && $appentry->getApproved() == "852") {
                            $appentry->setApproved("853");
                            $appentry->save();
                        } else if ($appentry && $appentry->getApproved() == "861") {
                            $appentry->setApproved("862");
                            $appentry->save();
                        } else if ($appentry && $appentry->getApproved() == "902") {
                            $appentry->setApproved("903");
                            $appentry->save();
                        } else if ($appentry && $appentry->getApproved() == "912") {
                            $appentry->setApproved("913");
                            $appentry->save();
                        } else if ($appentry && $appentry->getApproved() == "922") {
                            $appentry->setApproved("923");
                            $appentry->save();
                        }
                    }


                    $application = $request->getPostParameter("application");
                    $q = Doctrine_Query::create()
                            ->from('FormEntry a')
                            ->where('a.id = ?', $application);
                    $appentry = $q->fetchOne();
                    if ($appentry && $appentry->getApproved() == "869") {
                        $appentry->setApproved("864");
                        $appentry->save();
                    } else if ($appentry && $appentry->getApproved() == "865") {
                        $appentry->setApproved("867");
                        $appentry->save();
                    }
                } else {
                    $applications = $request->getPostParameter('applications');
                    foreach ($applications as $application) {
                        $count = 0;
                        $previous_task_id = 0;
                        foreach ($reviewers as $reviewer) {
                            if ($count == 0) {
                                $task = new Task();
                                $task->setType($request->getPostParameter("task_type"));
                                $task->setCreatorUserId($_SESSION["SESSION_CUTEFLOW_USERID"]);
                                $task->setOwnerUserId($reviewer);
                                $task->setSheetId($request->getPostParameter("task_sheet"));
                                $task->setDuration("0");
                                $task->setStartDate($request->getPostParameter("start_date"));
                                $task->setEndDate($request->getPostParameter("end_date"));
                                $task->setPriority($request->getPostParameter("priority"));
                                $task->setIsLeader("1");
                                $task->setActive("1");
                                $task->setStatus("1");
                                $task->setLastUpdate(date('Y-m-d'));
                                $task->setDateCreated(date('Y-m-d'));
                                $task->setRemarks($request->getPostParameter("description"));
                                $task->setApplicationId($application);
                                $task->save();

                                $this->task = $task;
                                $previous_task_id = $task->getId();
                                $this->success = true;

                                //if application is in submissions, move to circulations
                                $q = Doctrine_Query::create()
                                        ->from('FormEntry a')
                                        ->where('a.id = ?', $application);
                                $fullApplication = $q->fetchOne();

                                $q = Doctrine_Query::create()
                                        ->from('CfUser a')
                                        ->where('a.nid = ?', $reviewer);
                                $treviewer = $q->fetchOne();

                                $audit = new Audit();
                                $audit->saveFullAudit("<a href='/backend.php/tasks/view/id/" . $task->getId() . "'>Assigned " . $task->getTypeName() . " task on " . $fullApplication->getApplicationId() . " to " . $treviewer->getStrfirstname() . " " . $treviewer->getStrlastname() . "</a>", $task->getId(), "task", "", "Pending", $application);
                            } else {
                                $task = new Task();
                                $task->setType($request->getPostParameter("task_type"));
                                $task->setCreatorUserId($_SESSION["SESSION_CUTEFLOW_USERID"]);
                                $task->setOwnerUserId($reviewer);
                                $task->setSheetId($request->getPostParameter("task_sheet"));
                                $task->setDuration("0");
                                $task->setStartDate($request->getPostParameter("start_date"));
                                $task->setEndDate($request->getPostParameter("end_date"));
                                $task->setPriority($request->getPostParameter("priority"));
                                $task->setIsLeader("1");
                                if ($request->getPostParameter("workflow_type") == "1") {
                                    $task->setActive("0");
                                } else {
                                    $task->setActive("1");
                                }
                                $task->setStatus("1");
                                $task->setLastUpdate(date('Y-m-d'));
                                $task->setDateCreated(date('Y-m-d'));
                                $task->setRemarks($request->getPostParameter("description"));
                                $task->setApplicationId($application);
                                $task->save();
                                $this->task = $task;

                                $taskqueue = new TaskQueue();
                                $taskqueue->setCurrentTaskId($previous_task_id);
                                $taskqueue->setNextTaskId($task->getId());
                                $taskqueue->save();

                                $previous_task_id = $task->getId();
                                $this->success = true;

                                //if application is in submissions, move to circulations
                                $q = Doctrine_Query::create()
                                        ->from('FormEntry a')
                                        ->where('a.id = ?', $application);
                                $fullApplication = $q->fetchOne();

                                $q = Doctrine_Query::create()
                                        ->from('CfUser a')
                                        ->where('a.nid = ?', $reviewer);
                                $treviewer = $q->fetchOne();

                                $audit = new Audit();
                                $audit->saveFullAudit("<a href='/backend.php/tasks/view/id/" . $task->getId() . "'>Assigned " . $task->getTypeName() . " task on " . $fullApplication->getApplicationId() . " to " . $treviewer->getStrfirstname() . " " . $treviewer->getStrlastname() . "</a>", $task->getId(), "task", "", "Pending", $application);
                            }


                            $count++;

                            $q = Doctrine_Query::create()
                                    ->from('FormEntry a')
                                    ->where('a.id = ?', $application);
                            $this->application = $q->fetchOne();

                            $q = Doctrine_Query::create()
                                    ->from("CfUser a")
                                    ->where("a.nid = ?", $reviewer);
                            $reviewer = $q->fetchOne();

                            $body = "
                                    Hi " . $reviewer->getStrfirstname() . " " . $reviewer->getStrlastname() . ",<br>
                                    <br>
                                    You have been assigned a new task on " . $this->application->getApplicationId() . ":<br>
                                    <br><br>
                                    &ldquo; " . $request->getPostParameter("description") . " &rdquo;
                                    <br>
                                    <br>
                                    Click here to view the task: <br>
                                    ------- <br>
                                    <a href='http://" . $_SERVER['HTTP_HOST'] . "/backend.php/tasks/view/id/" . $this->task->getId() . "'>Link to " . $this->application->getApplicationId() . " task</a><br>
                                    ------- <br>

                                    <br>
                                    ";

                            $mailnotifications = new mailnotifications();
                            $mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $reviewer->getStremail(), "New Task", $body);
                        }
                        foreach ($otherreviewers as $reviewer) {
                            error_log("Debug: Other Reviewers 2");
                            if ($count == 0) {
                                error_log("Debug::::: OTHER REVIEWERS");
                                $task = new Task();
                                $task->setType($request->getPostParameter("task_type"));
                                $task->setCreatorUserId($_SESSION["SESSION_CUTEFLOW_USERID"]);
                                $task->setOwnerUserId($reviewer);
                                $task->setSheetId($request->getPostParameter("task_sheet"));
                                $task->setDuration("0");
                                $task->setStartDate($request->getPostParameter("start_date"));
                                $task->setEndDate($request->getPostParameter("end_date"));
                                $task->setPriority($request->getPostParameter("priority"));
                                $task->setIsLeader("0");
                                $task->setActive("1");
                                $task->setStatus("1");
                                $task->setLastUpdate(date('Y-m-d'));
                                $task->setDateCreated(date('Y-m-d'));
                                $task->setRemarks($request->getPostParameter("description"));
                                $task->setApplicationId($application);
                                $task->save();
                                $this->task = $task;
                                $previous_task_id = $task->getId();
                                $this->success = true;

                                $q = Doctrine_Query::create()
                                        ->from('FormEntry a')
                                        ->where('a.id = ?', $application);
                                $fullApplication = $q->fetchOne();

                                $q = Doctrine_Query::create()
                                        ->from('CfUser a')
                                        ->where('a.nid = ?', $reviewer);
                                $treviewer = $q->fetchOne();

                                $audit = new Audit();
                                $audit->saveFullAudit("<a href='/backend.php/tasks/view/id/" . $task->getId() . "'>Assigned " . $task->getTypeName() . " task on " . $fullApplication->getApplicationId() . " to " . $treviewer->getStrfirstname() . " " . $treviewer->getStrlastname() . "</a>", $task->getId(), "task", "", "Pending", $application);
                            } else {
                                error_log("Debug::::: THERE ARE NO OTHER REVIEWERS ARE HERE");
                                $task = new Task();
                                $task->setType($request->getPostParameter("task_type"));
                                $task->setCreatorUserId($_SESSION["SESSION_CUTEFLOW_USERID"]);
                                $task->setOwnerUserId($reviewer);
                                $task->setSheetId($request->getPostParameter("task_sheet"));
                                $task->setDuration("0");
                                $task->setStartDate($request->getPostParameter("start_date"));
                                $task->setEndDate($request->getPostParameter("end_date"));
                                $task->setPriority($request->getPostParameter("priority"));
                                $task->setIsLeader("0");
                                if ($request->getPostParameter("workflow_type") == "1") {
                                    $task->setActive("0");
                                } else {
                                    $task->setActive("1");
                                }
                                $task->setStatus("1");
                                $task->setLastUpdate(date('Y-m-d'));
                                $task->setDateCreated(date('Y-m-d'));
                                $task->setRemarks($request->getPostParameter("description"));
                                $task->setApplicationId($application);
                                $task->save();
                                $this->task = $task;

                                $taskqueue = new TaskQueue();
                                $taskqueue->setCurrentTaskId($previous_task_id);
                                $taskqueue->setNextTaskId($task->getId());
                                $taskqueue->save();

                                $previous_task_id = $task->getId();
                                $this->success = true;


                                $q = Doctrine_Query::create()
                                        ->from('FormEntry a')
                                        ->where('a.id = ?', $application);
                                $fullApplication = $q->fetchOne();

                                $q = Doctrine_Query::create()
                                        ->from('CfUser a')
                                        ->where('a.nid = ?', $reviewer);
                                $treviewer = $q->fetchOne();

                                $audit = new Audit();
                                $audit->saveFullAudit("<a href='/backend.php/tasks/view/id/" . $task->getId() . "'>Assigned " . $task->getTypeName() . " task on " . $fullApplication->getApplicationId() . " to " . $treviewer->getStrfirstname() . " " . $treviewer->getStrlastname() . "</a>", $task->getId(), "task", "", "Pending", $application);
                            }

                            $q = Doctrine_Query::create()
                                    ->from('FormEntry a')
                                    ->where('a.id = ?', $application);
                            $this->application = $q->fetchOne();

                            $q = Doctrine_Query::create()
                                    ->from("CfUser a")
                                    ->where("a.nid = ?", $reviewer);
                            $reviewer = $q->fetchOne();

                            $body = "
                                    Hi " . $reviewer->getStrfirstname() . " " . $reviewer->getStrlastname() . ",<br>
                                    <br>
                                    You have been assigned a new task on " . $this->application->getApplicationId() . ":<br>
                                    <br><br>
                                    &ldquo; " . $request->getPostParameter("description") . " &rdquo;
                                    <br>
                                    <br>
                                    Click here to view the task: <br>
                                    ------- <br>
                                    <a href='http://" . $_SERVER['HTTP_HOST'] . "/backend.php/tasks/view/id/" . $this->task->getId() . "'>Link to " . $this->application->getApplicationId() . " task</a><br>
                                    ------- <br>

                                    <br>
                                    ";

                            $mailnotifications = new mailnotifications();
                            $mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $reviewer->getStremail(), "New Task", $body);
                            $count++;
                        }

                        foreach ($supportreviewers as $reviewer) {
                            error_log("Debug::::: SUPPORT REVIEWERS ARE HERE");
                            if ($count == 0) {
                                $task = new Task();
                                $task->setType($request->getPostParameter("task_type"));
                                $task->setCreatorUserId($_SESSION["SESSION_CUTEFLOW_USERID"]);
                                $task->setOwnerUserId($reviewer);
                                $task->setSheetId($request->getPostParameter("task_sheet"));
                                $task->setDuration("0");
                                $task->setStartDate($request->getPostParameter("start_date"));
                                $task->setEndDate($request->getPostParameter("end_date"));
                                $task->setPriority($request->getPostParameter("priority"));
                                $task->setIsLeader("0");
                                $task->setActive("1");
                                $task->setStatus("1");
                                $task->setLastUpdate(date('Y-m-d'));
                                $task->setDateCreated(date('Y-m-d'));
                                $task->setRemarks($request->getPostParameter("description"));
                                $task->setApplicationId($application);
                                $task->save();
                                $this->task = $task;
                                $previous_task_id = $task->getId();
                                $this->success = true;


                                $q = Doctrine_Query::create()
                                        ->from('FormEntry a')
                                        ->where('a.id = ?', $application);
                                $fullApplication = $q->fetchOne();

                                $q = Doctrine_Query::create()
                                        ->from('CfUser a')
                                        ->where('a.nid = ?', $reviewer);
                                $treviewer = $q->fetchOne();

                                $audit = new Audit();
                                $audit->saveFullAudit("<a href='/backend.php/tasks/view/id/" . $task->getId() . "'>Assigned " . $task->getTypeName() . " task on " . $fullApplication->getApplicationId() . " to " . $treviewer->getStrfirstname() . " " . $treviewer->getStrlastname() . "</a>", $task->getId(), "task", "", "Pending", $application);
                            } else {
                                $task = new Task();
                                $task->setType($request->getPostParameter("task_type"));
                                $task->setCreatorUserId($_SESSION["SESSION_CUTEFLOW_USERID"]);
                                $task->setOwnerUserId($reviewer);
                                $task->setSheetId($request->getPostParameter("task_sheet"));
                                $task->setDuration("0");
                                $task->setStartDate($request->getPostParameter("start_date"));
                                $task->setEndDate($request->getPostParameter("end_date"));
                                $task->setPriority($request->getPostParameter("priority"));
                                $task->setIsLeader("0");
                                if ($request->getPostParameter("workflow_type") == "1") {
                                    $task->setActive("0");
                                } else {
                                    $task->setActive("1");
                                }
                                $task->setStatus("1");
                                $task->setLastUpdate(date('Y-m-d'));
                                $task->setDateCreated(date('Y-m-d'));
                                $task->setRemarks($request->getPostParameter("description"));
                                $task->setApplicationId($application);
                                $task->save();
                                $this->task = $task;

                                $taskqueue = new TaskQueue();
                                $taskqueue->setCurrentTaskId($previous_task_id);
                                $taskqueue->setNextTaskId($task->getId());
                                $taskqueue->save();

                                $previous_task_id = $task->getId();
                                $this->success = true;


                                $q = Doctrine_Query::create()
                                        ->from('FormEntry a')
                                        ->where('a.id = ?', $application);
                                $fullApplication = $q->fetchOne();

                                $q = Doctrine_Query::create()
                                        ->from('CfUser a')
                                        ->where('a.nid = ?', $reviewer);
                                $treviewer = $q->fetchOne();

                                $audit = new Audit();
                                $audit->saveFullAudit("<a href='/backend.php/tasks/view/id/" . $task->getId() . "'>Assigned " . $task->getTypeName() . " task on " . $fullApplication->getApplicationId() . " to " . $treviewer->getStrfirstname() . " " . $treviewer->getStrlastname() . "</a>", $task->getId(), "task", "", "Pending", $application);
                            }
                            $q = Doctrine_Query::create()
                                    ->from('FormEntry a')
                                    ->where('a.id = ?', $application);
                            $this->application = $q->fetchOne();

                            $q = Doctrine_Query::create()
                                    ->from("CfUser a")
                                    ->where("a.nid = ?", $reviewer);
                            $reviewer = $q->fetchOne();

                            $body = "
                                        Hi " . $reviewer->getStrfirstname() . " " . $reviewer->getStrlastname() . ",<br>
                                        <br>
                                        You have been assigned a new task on " . $this->application->getApplicationId() . ":<br>
                                        <br><br>
                                        &ldquo; " . $request->getPostParameter("description") . " &rdquo;
                                        <br>
                                        <br>
                                        Click here to view the task: <br>
                                        ------- <br>
                                        <a href='http://" . $_SERVER['HTTP_HOST'] . "/backend.php/tasks/view/id/" . $this->task->getId() . "'>Link to " . $this->application->getApplicationId() . " task</a><br>
                                        ------- <br>

                                        <br>
                                        ";

                            $mailnotifications = new mailnotifications();
                            $mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $reviewer->getStremail(), "New Task", $body);
                            $count++;
                        }

                        if ($request->getPostParameter("task_type") == 2) {
                            $q = Doctrine_Query::create()
                                    ->from('FormEntry a')
                                    ->where('a.id = ?', $application);
                            $appentry = $q->fetchOne();
                            if ($appentry && $appentry->getApproved() == "852") {
                                $appentry->setApproved("853");
                                $appentry->save();
                            } else if ($appentry && $appentry->getApproved() == "861") {
                                $appentry->setApproved("862");
                                $appentry->save();
                            } else if ($appentry && $appentry->getApproved() == "902") {
                                $appentry->setApproved("903");
                                $appentry->save();
                            } else if ($appentry && $appentry->getApproved() == "912") {
                                $appentry->setApproved("913");
                                $appentry->save();
                            } else if ($appentry && $appentry->getApproved() == "922") {
                                $appentry->setApproved("923");
                                $appentry->save();
                            }
                        }

                        $q = Doctrine_Query::create()
                                ->from('FormEntry a')
                                ->where('a.id = ?', $application);
                        $appentry = $q->fetchOne();
                        if ($appentry && $appentry->getApproved() == "869") {
                            $appentry->setApproved("864");
                            $appentry->save();
                        } else if ($appentry && $appentry->getApproved() == "865") {
                            $appentry->setApproved("867");
                            $appentry->save();
                        }
                    }
                }
            }
        } else {
            $q = Doctrine_Query::create()
                    ->from('FormEntry a')
                    ->where('a.id = ?', $request->getPostParameter("application"));
            $application = $q->fetchOne();

            $q = Doctrine_Query::create()
                    ->from("SubMenus a")
                    ->where("a.id = ?", $application->getApproved());
            $child_stage = $q->fetchOne();

            $q = Doctrine_Query::create()
                    ->from("SubMenus a")
                    ->where("a.menu_id = ?", $child_stage->getMenuId())
                    ->orderBy("a.order_no ASC");
            $stages = $q->execute();

            foreach ($stages as $stage) {
                $reviewers = $request->getPostParameter("reviewers_" . $stage->getId());

                $count = 0;
                $previous_task_id = 0;
                foreach ($reviewers as $reviewer) {
                    $task = new Task();
                    $task->setType($request->getPostParameter("task_type"));
                    $task->setCreatorUserId($this->getUser()->getAttribute('userid'));
                    $task->setOwnerUserId($reviewer);
                    $task->setSheetId($request->getPostParameter("task_sheet"));
                    $task->setDuration("0");
                    $task->setStartDate($request->getPostParameter("start_date"));
                    $task->setEndDate($request->getPostParameter("end_date"));
                    $task->setPriority($request->getPostParameter("priority"));
                    $task->setIsLeader("0");
                    $task->setActive("1");
                    $task->setStatus("1");
                    $task->setLastUpdate(date('Y-m-d'));
                    $task->setDateCreated(date('Y-m-d'));
                    $task->setRemarks($request->getPostParameter("description"));
                    $task->setApplicationId($application->getId());
                    $task->setTaskStage($stage->getId());
                    $task->save();
                    $this->task = $task;
                    $this->success = true;
                }
            }
        }

        //Check the current stage for automatic triggers
        error_log("Debug:::::::::: Check the current stage for automatic triggers") ;
        // if this is a dispatch stage, the application can either move to the next stage or send notifications
        if ($request->getPostParameter("application")) {
            $q = Doctrine_Query::create()
                    ->from('FormEntry a')
                    ->where('a.id = ?', $request->getPostParameter("application"));
            $application = $q->fetchOne();

            if ($application) {
                $q = Doctrine_Query::create()
                        ->from('SubMenus a')
                        ->where('a.id = ?', $application->getApproved());
                $stage = $q->fetchOne();
               // error_log("Debug:::::::::: Move application to another stage") ;
                if ($stage && ($stage->getStageType() == 8 || $stage->getStageType() == 9)) {
                   
                        error_log("Debug::::: STAGE TYPE IS ".$stage->getStageType());

                    if ($stage->getStageProperty() == 2) {
                        $next_stage=13;
                        //Move application to another stage
                        error_log("Debug:::::::::: Move application to another stage") ;
                        if($move_to_comments_review==TRUE){
                            $next_stage=14;
                            error_log("Debug::::::::::::::::::::: NEXT STAGE IS: ".$next_stage);
                        }
                        else{
                        $next_stage = $stage->getStageTypeMovement();
                    }
                        $application->setApproved($next_stage);
                        $application->save();
                        //OTB Start save task stage
                        $q = Doctrine_Query::create()
                                ->from("Task a")
                                ->where("a.application_id = ?", $application->getId())
                                ->andWhere("a.sheet_id IS NOT NULL")
                                ->andWhere("a.status = 1");
                        $tasks = $q->execute();

                        foreach ($tasks as $task) {
                            if ($request->getPostParameter("workflow_type") == 0) {
                                $task->setTaskStage($application->getApproved());
                                $task->setStatus("1");
                                $task->save();
                            }
                        }
                        //OTB End save task stage
                    } elseif ($stage->getStageProperty() == 3) {
                        //Send notification to reviewers
                        $notification = $stage->getStageTypeNotification();

                        $q = Doctrine_Query::create()
                                ->from('CfUser a')
                                ->where('a.bdeleted = 0');
                        $reviewers = $q->execute();
                        foreach ($reviewers as $reviewer) {
                            $q = Doctrine_Query::create()
                                    ->from('mfGuardUserGroup a')
                                    ->leftJoin('a.Group b')
                                    ->leftJoin('b.mfGuardGroupPermission c') //Left Join group permissions
                                    ->leftJoin('c.Permission d') //Left Join permissions
                                    ->where('a.user_id = ?', $reviewer->getNid())
                                    ->andWhere('d.name = ?', "accesssubmenu" . $application->getApproved());
                            $usergroups = $q->execute();
                            if (sizeof($usergroups) > 0) {
                                $body = "
	                        Hi " . $reviewer->getStrfirstname() . " " . $reviewer->getStrlastname() . ",<br>
	                        <br>
	                        " . $notification . "

	                        <br>
	                        Click here to view the application: <br>
	                        ------- <br>
	                        <a href='http://" . $_SERVER['HTTP_HOST'] . "/backend.php/applications/view/id/" . $application->getId() . "'>Link to " . $application->getApplicationId() . "</a><br>
	                        ------- <br>

	                        <br>
	                        ";

                                $mailnotifications = new mailnotifications();
                                $mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $reviewer->getStremail(), "Paid Invoice", $body);
                            }
                        }
                    }
                }
            }


            if ($this->success) {
                $q = Doctrine_Query::create()
                        ->from('CfUser a')
                        ->where('a.nid = ?', $this->getUser()->getAttribute('userid'));
                $reviewer = $q->fetchOne();
                $this->redirect("/backend.php/applications/view/id/" . $request->getPostParameter("application"));
            }
        } else {
            if ($this->success) {
                $q = Doctrine_Query::create()
                        ->from('CfUser a')
                        ->where('a.nid = ?', $this->getUser()->getAttribute('userid'));
                $reviewer = $q->fetchOne();
                $this->redirect("/backend.php/tasks/department/filter/" . $reviewer->getStrdepartment());
            }
        }
    }

    /**
     * Executes 'Comment' action
     *
     * Displays comment sheet for selected task
     *
     * @param sfRequest $request A request object
     */
    public function executeComment(sfWebRequest $request) {
        if ($request->getParameter("id") == "0") {
            $task = new Task();
            $task->setType(2);
            $task->setCreatorUserId($_SESSION["SESSION_CUTEFLOW_USERID"]);
            $task->setOwnerUserId($_SESSION["SESSION_CUTEFLOW_USERID"]);
            $task->setDuration("0");
            $task->setStartDate(date('Y-m-d'));
            $task->setEndDate(date('Y-m-d'));
            $task->setPriority('3');
            $task->setIsLeader("0");
            $task->setActive("1");
            $task->setStatus("1");
            $task->setLastUpdate(date('Y-m-d'));
            $task->setDateCreated(date('Y-m-d'));
            $task->setRemarks("");
            $task->setApplicationId($request->getParameter("application"));
            $task->save();

            $this->task = $task;
            $q = Doctrine_Query::create()
                    ->from('FormEntry a')
                    ->where('a.id = ?', $this->task->getApplicationId());
            $this->application = $q->fetchOne();
        } else {
            $q = Doctrine_Query::create()
                    ->from('Task a')
                    ->where('a.id = ?', $request->getParameter("id"));
            $this->task = $q->fetchOne();
            $q = Doctrine_Query::create()
                    ->from('FormEntry a')
                    ->where('a.id = ?', $this->task->getApplicationId());
            $this->application = $q->fetchOne();
        }

        if ($this->application->getCirculationId() == "") {
            //If No Circulation has been assigned, then create new circulationforms and processes

            $maillist = new CfMailinglist();
            $maillist->setStrname("Default");
            $maillist->setNtemplateid("7");
            $maillist->setBisedited("0");
            $maillist->setBisdefault("0");
            $maillist->setBdeleted("0");
            $maillist->save();

            $circulationform = new CfCirculationForm();
            $circulationform->setNsenderId($this->task->setCreatorUserId());
            $circulationform->setStrname($this->application->getApplicationId());
            $circulationform->setNmailinglistid($maillist->getNid());
            $circulationform->setBisarchived("0");
            $circulationform->setNendaction("3");
            $circulationform->setBdeleted("0");
            $circulationform->setBanonymize("0");
            $circulationform->save();


            $circulationhistory = new CfCirculationHistory();
            $circulationhistory->setNrevisionnumber("1");
            $circulationhistory->setDatesending($this->task->getStartDate());
            $circulationhistory->setStradditionaltext("");
            $circulationhistory->setNcirculationformid($circulationform->getNid());

            $this->application->setCirculationId($circulationform->getNid());
            $this->application->save();

            $q = Doctrine_Query::create()
                    ->from('CfUser a')
                    ->where('a.nid = ?', $_SESSION["SESSION_CUTEFLOW_USERID"]);
            $reviewer = $q->fetchOne();



            if ($request->getParameter('slot')) {
                $this->ext_slot = $request->getParameter('slot');
                $q = Doctrine_Query::create()
                        ->from('CfFormslot a')
                        ->where('a.strname = ?', $reviewer->getStrdepartment() . " - " . $request->getParameter('slot'))
                        ->orderBy("a.nid DESC");
                $slot = $q->fetchOne();
            } else {
                $q = Doctrine_Query::create()
                        ->from('CfFormslot a')
                        ->where('a.strname LIKE ?', '%' . $reviewer->getStrdepartment() . '%')
                        ->andWhere('a.nsendtype = ?', $this->application->getFormId())
                        ->andWhere('a.strname <> ? AND a.strname <> ? AND a.strname <> ?', array($reviewer->getStrdepartment() . " - Inspection", $reviewer->getStrdepartment() . " - Scanning", $reviewer->getStrdepartment() . " - Collection"))
                        ->orderBy("a.nid DESC");
                $slot = $q->fetchOne();

                if (empty($slot)) {
                    $q = Doctrine_Query::create()
                            ->from('CfFormslot a')
                            ->where('a.strname LIKE ?', '%' . $reviewer->getStrdepartment() . '%')
                            ->orderBy("a.nid DESC");
                    $slot = $q->fetchOne();
                }
            }
            try {
                $process = new CfCirculationprocess();
                $process->setNcirculationformid($circulationform->getNid());
                $process->setNslotid($slot->getNid());
                $process->setNuserid($_SESSION["SESSION_CUTEFLOW_USERID"]);
                $process->setDateinprocesssince("0");
                $process->setNdecissionstate("0");
                $process->setDatedecission("0");
                $process->setNissubstitiuteof("0");
                $process->setNcirculationhistoryid($circulationhistory->getNid());
                $process->setNresendcount("0");
                $process->save();
            } catch (Exception $e) {
                try {
                    $q = Doctrine_Query::create()
                            ->from('CfFormslot a')
                            ->where('a.strname LIKE ?', '%' . $reviewer->getStrdepartment() . '%')
                            ->orderBy("a.nid DESC");
                    $slot = $q->fetchOne();
                    $process = new CfCirculationprocess();
                    $process->setNcirculationformid($circulationform->getNid());
                    $process->setNslotid($slot->getNid());
                    $process->setNuserid($_SESSION["SESSION_CUTEFLOW_USERID"]);
                    $process->setDateinprocesssince("0");
                    $process->setNdecissionstate("0");
                    $process->setDatedecission("0");
                    $process->setNissubstitiuteof("0");
                    $process->setNcirculationhistoryid($circulationhistory->getNid());
                    $process->setNresendcount("0");
                    $process->save();
                } catch (Exception $e) {
                    $this->redirect("/backend.php/tasks/comment/id/" . $this->task->getId());
                }
            }
        }

        if ($request->getParameter('slot')) {
            $this->ext_slot = $request->getParameter('slot');
        }
    }

    /**
     * Executes 'Commentbeta' action
     *
     * Displays comment sheet for selected task

     *
     * @param sfRequest $request A request object
     */
    public function executeCommentbeta(sfWebRequest $request) {
        if ($request->getParameter("id") == "0") {
            $task = new Task();
            $task->setType(2);
            $task->setCreatorUserId($_SESSION["SESSION_CUTEFLOW_USERID"]);
            $task->setOwnerUserId($_SESSION["SESSION_CUTEFLOW_USERID"]);
            $task->setDuration("0");
            $task->setStartDate(date('Y-m-d'));
            $task->setEndDate(date('Y-m-d'));
            $task->setPriority('3');
            $task->setIsLeader("0");
            $task->setActive("1");
            $task->setStatus("1");
            $task->setLastUpdate(date('Y-m-d'));
            $task->setDateCreated(date('Y-m-d'));
            $task->setRemarks("");
            $task->setApplicationId($request->getParameter("application"));
            $task->save();

            $this->task = $task;
        } else {
            $q = Doctrine_Query::create()
                    ->from("Task a")
                    ->where("a.id = ?", $request->getParameter("id"));
            $this->task = $q->fetchOne();
        }
        $this->setLayout("layout");
    }

    /**
     * Executes 'Commentbeta' action
     *
     * Displays comment sheet for selected task

     *
     * @param sfRequest $request A request object
     */
    public function executeCommenteditbeta(sfWebRequest $request) {
        $this->setLayout("layout-machform");
    }

    /**
     * Executes 'Savecommentsheet' action
     *
     * Save comment sheet to the database
     *
     * @param sfRequest $request A request object
     */
    public function executeSavecommentsheet(sfWebRequest $request) {
        $this->notifier = new notifications($this->getMailer());

        $slot_id = $request->getPostParameter("slotid");
        $circ_id = $request->getPostParameter("circid");

        $this->slotid = $slot_id;
        $this->circid = $circ_id;

        $entry_id = 0;

        $q = Doctrine_Query::create()
                ->from('FormEntry a')
                ->where('a.circulation_id = ?', $circ_id);

        $entries = $q->execute();

        foreach ($entries as $entry) {
            $entry_id = $entry->getId();
        }
    }

    /**
     * Executes 'Invoice' action
     *
     * Generate an invoice for the selected application
     *
     * @param sfRequest $request A request object
     */
    public function executeInvoice(sfWebRequest $request) {
        $this->setLayout('layout-metronic');
        if ($request->getParameter("id") == "0") {
            $task = new Task();
            $task->setType(3);
            $task->setCreatorUserId($_SESSION["SESSION_CUTEFLOW_USERID"]);
            $task->setOwnerUserId($_SESSION["SESSION_CUTEFLOW_USERID"]);
            $task->setDuration("0");
            $task->setStartDate(date('Y-m-d H:i:s'));
            $task->setPriority('3');
            $task->setIsLeader("0");
            $task->setActive("1");
            $task->setStatus("1");
            $task->setLastUpdate(date('Y-m-d H:i:s'));
            $task->setDateCreated(date('Y-m-d H:i:s'));
            $task->setRemarks("");
            $task->setApplicationId($request->getParameter("application"));
            $task->save();

            $this->task = $task;
            $q = Doctrine_Query::create()
                    ->from('FormEntry a')
                    ->where('a.id = ?', $this->task->getApplicationId());
            $this->application = $q->fetchOne();
        } else {
            $q = Doctrine_Query::create()
                    ->from('Task a')
                    ->where('a.id = ?', $request->getParameter("id"));
            $this->task = $q->fetchOne();
            $q = Doctrine_Query::create()
                    ->from('FormEntry a')
                    ->where('a.id = ?', $this->task->getApplicationId());
            $this->application = $q->fetchOne();
        }
    }

    /**
     * Executes 'Saveinvoice' action
     *
     * Saves invoice details to the database
     *
     * @param sfRequest $request A request object
     */
    public function executeSaveinvoice(sfWebRequest $request) {
        $filename = $request->getPostParameter("filevalue");
        $originalfilename = $filename['0'];

        $client_ip = $_SERVER["REMOTE_ADDR"];

        if ($client_ip == "::1" || $client_ip == "") {
            $client_ip = "127.0.0.1";
        }

        $filename = $client_ip . $filename[0];

        $q = Doctrine_Query::create()
                ->from('Task a')
                ->where('a.id = ?', $request->getParameter("id"));
        $this->task = $q->fetchOne();

        $q = Doctrine_Query::create()
                ->from('FormEntry a')
                ->where('a.id = ?', $this->task->getApplicationId());
        $this->application = $q->fetchOne();

        $descriptions = $request->getPostParameter("feetitle");
        $amounts = $request->getPostParameter("feevalue");

        $invoice_manager = new InvoiceManager();

        $invoice = "";

        if ($invoice_manager->has_unpaid_invoice($this->application->getId())) {
            $invoice = $invoice_manager->get_unpaid_invoice($this->application->getId());
            if ($invoice) {
                $invoice = $invoice_manager->update_invoice($this->application->getId(), $invoice->getId(), $descriptions, $amounts);
            }
        } else {
            $invoice = $invoice_manager->create_invoice_from_task($this->application->getId(), $descriptions, $amounts);
        }


        if (empty($invoice)) {
            $q = Doctrine_Query::create()
                    ->from("MfInvoice a")
                    ->where("a.app_id = ?", $this->application->getId())
                    ->orderBy("a.id DESC");
            $invoice = $q->fetchOne();
        }

        if ($filename) {
            $element_name = md5($filename . $invoice->getId() . $this->task->getApplicationId() . date('Ymdgis'));

            $prefix_folder = dirname(__FILE__) . "/../../../../../../html/";

            $target_filename = $prefix_folder . "asset_uplds/invoices/" . $filename;

            $destination_filename = $prefix_folder . "asset_uplds/invoices/{$element_name}{$filename}";

            if (file_exists($target_filename)) {
                rename($target_filename, $destination_filename); //build update query
                $invoicedetail = new MfInvoiceDetail();
                $invoicedetail->setDescription("Attached Invoice");
                $invoicedetail->setAmount("<a href=\"/asset_uplds/invoices/{$element_name}{$filename}\">{$originalfilename}</a>");
                $invoicedetail->setInvoiceId($invoice->getId());
                $invoicedetail->save();
            }
        }

        $this->task->setStatus('25');

        $this->task->setEndDate(date('Y-m-d'));
        $this->task->save();

        $application = $invoice_manager->get_application_by_id($this->task->getApplicationId());

        $q = Doctrine_Query::create()
                ->from('Task a')
                ->where('a.application_id = ?', $this->task->getApplicationId())
                ->andWhere('a.task_stage = ?', $application->getApproved())
                ->andWhere('a.status = 1 OR a.status = 2 OR a.status = 3 OR a.status = 4 OR a.status = 5');
        $tasks = $q->execute();

        //Commented to decide whether after invoicing the application should stick in the same stage or move. Commented to reduce possible client complaints due to mistakes
        if (sizeof($tasks) == 0) {
            //If there no more tasks, check stage of assessment settings or move application to the next stage by default
            $q = Doctrine_Query::create()
                    ->from("SubMenus a")
                    ->where("a.id = ?", $application->getApproved());
            $stage = $q->fetchOne();

            if ($stage && $stage->getStageType() == 2) {
                if ($stage->getStageProperty() == 2) {
                    //Move application to another stage
                    $next_stage = $stage->getStageTypeMovement();
                    $application->setApproved($next_stage);
                    $application->save();
                }
            }
        }

        if ($request->getPostParameter("submit")) {
            $this->redirect($request->getPostParameter("submit"));
        } else {
            $this->redirect('/backend.php/applications/view/id/' . $this->task->getApplicationId());
        }
    }

    /**
     * Executes 'View' action
     *
     * Displays full task details
     *
     * @param sfRequest $request A request object
     */
    public function executeView(sfWebRequest $request) {
        if ($request->getParameter("decline")) {
            $this->decline_warning = true;
        }
        $this->getUser()->setAttribute("back_to_tasks", true);

        $q = Doctrine_Query::create()
                ->from('Task a')
                ->where('a.id = ?', $request->getParameter("id"));
        $this->task = $q->fetchOne();

        $q = Doctrine_Query::create()
                ->from("SfGuardUserProfile a")
                ->where("a.user_id = ?", $this->task->getApplicationId());
        $user_profile = $q->fetchOne();
        if ($request->getPostParameter("comment")) {
            $comment = new TaskComments();
            $comment->setCommentcontent($request->getPostParameter("comment"));
            $comment->setReviewerId($_SESSION["SESSION_CUTEFLOW_USERID"]);
            $comment->setTaskId($this->task->getId());
            $comment->setDateCreated(date('Y-m-d'));
            $comment->save();

            $alert = new Alerts();
            $alert->setSubject("New task comment on " . $this->task->getApplication()->getApplicationId());
            $alert->setContent("New task comment from " . $user_profile->getFullname() . " on " . $this->task->getApplication()->getApplicationId());
            $alert->setIsread("0");
            $alert->setLink("http://" . $_SERVER['HTTP_HOST'] . "/backend.php/tasks/view/id/" . $this->task->getId() . "/current_tab/memo");
            $alert->setUserid($task->getOwnerUserId());
            $alert->setDatesent(date("Y-m-d H:m:s"));
            $alert->save();
        }

        $this->setLayout('layout-metronic');
    }

    /**
     * Executes 'Complete' action
     *
     * Changes a tasks status to 'Complete' or 'Completed' when a reviewer submits comments
     *
     * @param sfRequest $request A request object
     */
    public function executeComplete(sfWebRequest $request) {
        //If user is HOD or Supervisor, set Status to 25 and ignore completed and cancelled tasks
        $otbhelper = new OTBHelper();
        error_log("executeComplete:::::: executed!!!!!!!!");
        if ($request->getParameter("id")) {
            $q = Doctrine_Query::create()
                    ->from('Task a')
                    ->where('a.id = ?', $request->getParameter("id"));
            $task = $q->fetchOne();

            $q = Doctrine_Query::create()
                    ->from("FormEntry a")
                    ->where("a.id = ?", $task->getApplicationId());
            $application = $q->fetchOne();

           

            if (($task->getStatus() != "25" && $task->getStatus() != "55" && $task->getStatus() != "45") || $task->getType() == "4") {
                if ($this->getUser()->mfHasCredential('director_complete') || $this->getUser()->mfHasCredential('has_hod_access') || $task->getCreatorUserId() == $_SESSION["SESSION_CUTEFLOW_USERID"]) {//Check if HOD or Supervisor and complete task
                    $task->setStatus("25");
                    $task->setEndDate(date('Y-m-d H:i:s'));
                    $task->setLastUpdate(date("Y-m-d H:i:s"));
                    $task->save();
                    $this->getUser()->setFlash("notice", "Task has been set as Completed.");

                    $audit = new Audit();
                    $audit->saveFullAudit("<a href='/backend.php/tasks/view/id/" . $task->getId() . "'>Marked " . $task->getTypeName() . " task on " . $application->getApplicationId() . " as 'Completed'</a>", $task->getId(), "task", "Pending", "Completed", $task->getApplicationId());
                      //OTB patch - System updates other tasks
                    $otb_helper = new OTBHelper();
                    //get users
                    $user_ids = $otb_helper->reviewers_list($task->getOwnerUserId()) ;
                    //pass users to process
                    $process_tasks = $otb_helper->task_manager($task->getApplicationId(),$user_ids);

                    //check if there are anymore pending or awaiting approval tasks for this application
                    $q = Doctrine_Query::create()
                            ->from('Task a')
                            ->where('a.application_id = ?', $task->getApplicationId())
                            //->andWhere('a.task_stage = ?', $application->getApproved())
                            //->andWhere('a.status = 1 OR a.status = 2 OR a.status = 3 OR a.status = 4 OR a.status = 5');
                            ->andWhere('a.status = 1 OR a.status = 2 OR a.status = 3 OR a.status = 4 OR a.status = 5');
                    $tasks = $q->execute();


                    if (sizeof($tasks) == 0) {
                        error_log("Debug::::: If there no more tasks, check stage of assessment settings or move application to the next stage by default");
                        //If there no more tasks, check stage of assessment settings or move application to the next stage by default
                        $q = Doctrine_Query::create()
                                ->from("SubMenus a")
                                ->where("a.id = ?", $application->getApproved());
                        $stage = $q->fetchOne();

                        if ($stage && $stage->getStageType() == 2) {
                           // error_log("Get getStageType >>> ".$stage->getStageType());
                            if ($stage->getStageProperty() == 2) {
                               // error_log("Get getStageProperty >>> ".$stage->getStageProperty());
                                //Move application to another stage
                                error_log("Debuyg:::::::: Move application to another stage");
                                $next_stage = $stage->getStageTypeMovement();
                                //B4 moving check if we have pending tasks for this application - we only move if there are zero 
                                //pending tasks
                                //if($otbhelper->checkPendingTasks($application->getId())){
                                     //true
                                //    error_log("Pending tasks") ;
                                //}else{
                                    //false
                                      //error_log("No Pending tasks") ;
                                    $application->setApproved($next_stage);
                                //}
                               
                                $application->save();
                            } elseif ($stage->getStageProperty() == 3) {
                                //Send notification to reviewers
                                $notification = $stage->getStageTypeNotification();

                                $q = Doctrine_Query::create()
                                        ->from('CfUser a')
                                        ->where('a.bdeleted = 0');
                                $reviewers = $q->execute();
                                foreach ($reviewers as $reviewer) {
                                    $q = Doctrine_Query::create()
                                            ->from('mfGuardUserGroup a')
                                            ->leftJoin('a.Group b')
                                            ->leftJoin('b.mfGuardGroupPermission c') //Left Join group permissions
                                            ->leftJoin('c.Permission d') //Left Join permissions
                                            ->where('a.user_id = ?', $reviewer->getNid())
                                            ->andWhere('d.name = ?', "accesssubmenu" . $application->getApproved());
                                    $usergroups = $q->execute();
                                    if (sizeof($usergroups) > 0) {
                                        $body = "
                        Hi " . $reviewer->getStrfirstname() . " " . $reviewer->getStrlastname() . ",<br>
                        <br>
                        " . $notification . "

                        <br>
                        Click here to view the application: <br>
                        ------- <br>
                        <a href='http://" . $_SERVER['HTTP_HOST'] . "/backend.php/applications/view/id/" . $application->getId() . "'>Link to " . $application->getApplicationId() . "</a><br>
                        ------- <br>

                        <br>
                        ";

                                        $mailnotifications = new mailnotifications();
                                        $mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $reviewer->getStremail(), "Paid Invoice", $body);
                                    }
                                }
                            }
                        }
                    }

                    $this->redirect('/backend.php/tasks/view/id/' . $task->getId());
                } else {
                    $task->setStatus("2");
                    $task->setLastUpdate(date("Y-m-d"));
                    $task->save();
                    $this->getUser()->setFlash("notice", "Task has been set as Completing. Await for approval.");

                    $audit = new Audit();
                    $audit->saveFullAudit("<a href='/backend.php/tasks/view/id/" . $task->getId() . "'>Marked " . $task->getTypeName() . " task on " . $application->getApplicationId() . " as 'Awaiting Approval'</a>", $task->getId(), "task", "Pending", "Awaiting Approval", $task->getApplicationId());

                    $this->redirect('/backend.php/tasks/view/id/' . $task->getId());
                }
            } else {
                if ($task->getStatus() == "25") {
                    //$this->getUser()->setFlash("error","Task is already set as Completed. It cannot be modified.");
                } else if ($task->getStatus() == "55") {
                    //$this->getUser()->setFlash("error","Task is already set as Cancelled. It cannot be modified.");
                } else if ($task->getStatus() == "45") {
                    //$this->getUser()->setFlash("error","Task is already set as Transferred. It cannot be modified.");
                }
            }
        }

        $this->redirect('/backend.php/tasks/view/id/' . $task->getId());
    }

    /**
     * Executes 'Transfer' action
     *
     * Transfers a task from one reviewer to another
     *
     * @param sfRequest $request A request object
     */
    public function executeTransfer(sfWebRequest $request) {
        //If user is HOD or Supervisor, set Status to 45

        if ($request->getParameter("id")) {
            $q = Doctrine_Query::create()
                    ->from('Task a')
                    ->where('a.id = ?', $request->getParameter("id"));
            $this->task = $q->fetchOne();
        }

        if ($request->getPostParameter("reviewers")) {
            //if(status not completed, not completing or not transferred already)
            if (true) {
                $q = Doctrine_Query::create()
                        ->from('MfGuardUserGroup a')
                        ->where('a.user_id = ?', $_SESSION["SESSION_CUTEFLOW_USERID"])
                        ->andWhere('a.group_id = ? or a.group_id = ? or a.group_id = ?', array('4', '7', '1'));
                $permitted_groups = $q->fetchOne();
                //if($permitted_groups)
                if (true) {
                    $reviewers = $request->getPostParameter("reviewers");
                    foreach ($reviewers as $reviewer) {
                        $newtask = new Task();
                        $newtask->setType($request->getPostParameter("task_type"));
                        $newtask->setCreatorUserId($_SESSION["SESSION_CUTEFLOW_USERID"]);
                        $newtask->setOwnerUserId($reviewer);
                        $newtask->setSheetId($this->task->getSheetId());
                        $newtask->setDuration("0");
                        $newtask->setStartDate($request->getPostParameter("start_date"));
                        $newtask->setEndDate($request->getPostParameter("end_date"));
                        $newtask->setPriority($request->getPostParameter("priority"));
                        $newtask->setIsLeader("0");
                        $newtask->setActive("1");
                        $newtask->setStatus("1");
                        $newtask->setLastUpdate(date('Y-m-d'));
                        $newtask->setDateCreated(date('Y-m-d'));
                        $newtask->setRemarks($request->getPostParameter("textarea"));
                        $newtask->setApplicationId($this->task->getApplicationId());
                        $newtask->save();

                        $this->task->setStatus('45');
                        $this->task->setLastUpdate(date('Y-m-d'));
                        $this->task->save();

                        $this->getUser()->setFlash("notice", "Task has been successfully transferred.");
                    }
                }
            } else {
                if ($this->task->getStatus() == "25") {
                    $this->getUser()->setFlash("error", "Task is already set as Completed. It cannot be modified.");
                } else if ($this->task->getStatus() == "55") {
                    $this->getUser()->setFlash("error", "Task is already set as Cancelled. It cannot be modified.");
                } else if ($this->task->getStatus() == "45") {
                    $this->getUser()->setFlash("error", "Task is already set as Transferred. It cannot be modified.");
                }
            }
            $this->redirect('/backend.php/tasks/view/id/' . $this->task->getId());
        }
    }

    /**
     * Executes 'Postpone' action
     *
     * Postpones a tasks start-date or end-date to a different date
     *
     * @param sfRequest $request A request object
     */
    public function executePostpone(sfWebRequest $request) {
        //set Status to 3 and ignore completed or cancelled tasks

        if ($request->getParameter("id")) {
            $q = Doctrine_Query::create()
                    ->from('Task a')
                    ->where('a.id = ?', $request->getParameter("id"));
            $task = $q->fetchOne();
            $this->task = $task;
        }

        if ($request->getPostParameter("start_date")) {

            if ($task->getStatus() != "25" && $task->getStatus() != "55" && $task->getStatus() != "45") {
                $q = Doctrine_Query::create()
                        ->from('MfGuardUserGroup a')
                        ->where('a.user_id = ?', $_SESSION["SESSION_CUTEFLOW_USERID"])
                        ->andWhere('a.group_id = ? or a.group_id = ? or a.group_id = ?', array('4', '7', '1'));
                $permitted_groups = $q->fetchOne();
                if ($permitted_groups) {
                    $task->setStartDate($request->getPostParameter("start_date"));
                    $task->setEndDate($request->getPostParameter("end_date"));
                    $task->setStatus("3");
                    $task->save();
                    $this->getUser()->setFlash("notice", "Task has been Postponed.");
                }
            } else {
                if ($task->getStatus() == "25") {
                    $this->getUser()->setFlash("error", "Task is already set as Completed. It cannot be modified.");
                } else if ($task->getStatus() == "55") {
                    $this->getUser()->setFlash("error", "Task is already set as Cancelled. It cannot be modified.");
                } else if ($task->getStatus() == "45") {
                    $this->getUser()->setFlash("error", "Task is already set as Transferred. It cannot be modified.");
                }
            }

            $this->redirect('/backend.php/tasks/view/id/' . $task->getId());
        }
    }

    /**
     * Executes 'Cancel' action
     *
     * Changes a task's status to cancelled thus preventing the assigned reviewer from working it.
     *
     * @param sfRequest $request A request object
     */
    public function executeCancel(sfWebRequest $request) {
        //If user is HOD or Supervisor, set Status to 55 and ignore completed and cancelled tasks

        if ($request->getParameter("id")) {
            $q = Doctrine_Query::create()
                    ->from('Task a')
                    ->where('a.id = ?', $request->getParameter("id"));
            $task = $q->fetchOne();

            if ($task->getStatus() != "25" && $task->getStatus() != "55" && $task->getStatus() != "45") {
                if ($task->getStatus() == "5" || $this->getUser()->mfHasCredential("has_hod_access")) { //Check if HOD or Supervisor and complete task
                    $task->setStatus("55");
                    $task->save();
                    $this->getUser()->setFlash("notice", "Task has been cancelled.");

                    //check if there are anymore pending or awaiting approval tasks for this application
                    $q = Doctrine_Query::create()
                            ->from('Task a')
                            ->where('a.application_id = ?', $task->getApplicationId())
                            ->andWhere('a.status = 1 OR a.status = 2 OR a.status = 3 OR a.status = 4 OR a.status = 5');
                    $tasks = $q->execute();

                    if (sizeof($tasks) == 0) {
                        $q = Doctrine_Query::create()
                                ->from('FormEntry a')
                                ->where('a.id = ?', $task->getApplicationId());
                        $application = $q->fetchOne();
                        //if there are no more pending tasks then move the application to the next stage if this is the submissions or circulations stage
                        if ($application->getApproved() == 6 || $application->getApproved() == 5) {
                            $application->setApproved('20');
                            $application->save();
                        }
                    }
                } else {
                    $task->setStatus("5");
                    $task->save();

                    $this->getUser()->setFlash("notice", "Task has been set as Cancelling. Await for approval.");
                }
            } else {
                if ($task->getStatus() == "25") {
                    $this->getUser()->setFlash("error", "Task is already set as Completed. It cannot be modified.");
                } else if ($task->getStatus() == "55") {
                    $this->getUser()->setFlash("error", "Task is already set as Cancelled. It cannot be modified.");
                } else if ($task->getStatus() == "45") {
                    $this->getUser()->setFlash("error", "Task is already set as Transferred. It cannot be modified.");
                }
            }
        }

        $this->forward('tasks', 'view');
    }

    /**
     * Executes 'Return' action
     *
     * Changes a task's status to pending to allow the assigned reviewer to make changes and complete it.
     *
     * @param sfRequest $request A request object
     */
    public function executeReturn(sfWebRequest $request) {
        //set Status to 1 and ignore completed and cancelled tasks

        if ($request->getParameter("id")) {
            $q = Doctrine_Query::create()
                    ->from('Task a')
                    ->where('a.id = ?', $request->getParameter("id"));
            $task = $q->fetchOne();

            if ($task->getStatus() != "25" && $task->getStatus() != "55" && $task->getStatus() != "45") {
                $task->setStatus('1');
                $task->setLastUpdate(date('Y-m-d'));
                $task->save();

                $this->getUser()->setFlash("notice", "Task has been successfully set as Pending.");
            } else {
                if ($task->getStatus() == "25") {
                    $this->getUser()->setFlash("error", "Task is already set as Completed. It cannot be modified.");
                } else if ($task->getStatus() == "55") {
                    $this->getUser()->setFlash("error", "Task is already set as Cancelled. It cannot be modified.");
                } else if ($task->getStatus() == "45") {
                    $this->getUser()->setFlash("error", "Task is already set as Transferred. It cannot be modified.");
                }
            }
        }

        $this->forward('tasks', 'view');
    }

    /**
     * OTB patch - Reset tasks status
     */
    public function executeReset(sfWebRequest $request){
        //get task id supplied
        $task_id = $request->getParameter('taskid');
        $task_reset = Doctrine_Query::create()
                   ->update('Task t')
                   ->set('t.status','?',1)
                   ->where('t.id = ?',$task_id) ;
        $res = $task_reset->execute();
        if($res){
            $this->getUser()->setFlash('task_reset','Task reset was successful! You can now edit your previous comments and submit');
        }else {
             $this->getUser()->setFlash('task_reset_error','Task reset was failed! Please try again later');
        }
        $this->redirect('/backend.php/tasks/view/id/'.$task_id.'/current_tab/review') ;
    }
    /**
     * Reset all tasks to pending for reviewers to edit on an application.
     */
    public function executeResetAllTasks(sfWebRequest $request){
        $application_id = $request->getParameter('app_id');
        //reset tasks to pending
        $q = Doctrine_Query::create()
                ->update('Task t')
                ->set('t.status', '?', 1)
                ->where('t.application_id = ? ',$application_id) ;
        $res = $q->execute();
        //
         if($res){
            $this->getUser()->setFlash('task_reset_all','Tasks Reset was successful. Reviewers can now edit comments and resubmit');
        }else {
             $this->getUser()->setFlash('task_reset_error_all','Tasks reset was failed! Please try again later');
        }
        $this->redirect('/backend.php/applications/view/id/'.$application_id.'/current_tab/review') ;
    }
    
}
