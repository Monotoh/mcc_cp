<?php
$q = Doctrine_Query::create()
   ->from("FormEntry a")
   ->where("a.id = ?", $task->getApplicationId());
$application = $q->fetchOne();
if($application)
{
?>
<tr <?php if($task->getStatus() == "1"): ?><?php else: ?>class="unread"<?php endif; ?>>
  <td>
	<div class="ckbox ckbox-success">
		<input type="checkbox" id="batchapp<?php echo $task->getId(); ?>" name="batchapp[<?php echo $task->getId(); ?>]" value="<?php echo $task->getId(); ?>">
		<label for="batchapp<?php echo $task->getId(); ?>"></label>
	</div>
  </td>
  <td>
  	<?php
	$q = Doctrine_Query::create()
		->from("Favorites a")
		->where("a.application_id = ?", $application->getId())
		->andWhere("a.userid = ?", $_SESSION['SESSION_CUTEFLOW_USERID']);
	$favorite = $q->fetchOne();
	if($favorite)
	{
	?>
	<a href="#" class="star star-checked" id="star<?php echo $application->getId(); ?>"><i class="glyphicon glyphicon-star"></i></a>
    <?php
	}
	else
	{
	?>
    <a href="#" class="star" id="star<?php echo $application->getId(); ?>"><i class="glyphicon glyphicon-star"></i></a>
    <?php
	}
	?>
    <script language="javascript">
	$(document).ready(function(){
		$("#star<?php echo $application->getId(); ?>").click(function(){
		  $.ajax({url:"<?php echo public_path(); ?>backend.php/applications/setstar/id/<?php echo $application->getId(); ?>",success:function(result){
			$("#star<?php echo $application->getId(); ?>").toggleClass("star-checked");
		  }});
		}); 
	 });
	</script>
  </td>
  <td><a href="<?php echo public_path(); ?>backend.php/tasks/view/id/<?php echo $task->getId(); ?>"><?php echo $application->getApplicationId(); ?></a></td>

  <td><?php echo $task->getTypeName(); ?> Task</td>
  <td><span class="badge badge-success"><?php echo $task->getStatusName(); ?></span></td>
  <td><span class="badge badge-success"><?php 
	switch($task->getPriority())
	{
		case 1:
			echo __("Critical");
			break;
		case 2:
			echo __("Important");
			break;
		case 3:
			echo __("Normal");
			break;
	}
   ?></span></td>
  <td>
   <?php echo $task->getDateCreated(); ?>
  </td>
  <td>
         <a  title='<?php echo __('View Task'); ?>' href='<?php echo public_path(); ?>backend.php/tasks/view/id/<?php echo $task->getId(); ?>'> <span class="badge badge-primary"><i class="fa fa-eye"></i></span></a>
  </td>
</tr>
<?php
}
?>