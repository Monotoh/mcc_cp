<?php
/**
 * listfooter partial.
 *
 * Display the footer for the tasks list containing pagination and bulk actions
 *
 * @package    backend
 * @subpackage tasks
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
 
$totaltasks = $_SESSION['totaltasks'];

//If no tasks are found, display the following
if($totaltasks == 0)
{
?>
<table>
	<tfoot>
   <tr><td colspan='8' style='text-align: left; padding: 10px; font-size: 16px;'>
   <div align='center'>No Tasks Found.</div>
   </td></tr>
   </tfoot>
</table>
<?php
}
?>
<!-- Bulk actions for tasks -->
<table>
	<tfoot>
   <tr><td colspan='8' style='text-align: left;'>
   <select id='batch_action' name='batch_action' onChange="if(this.value != ''){if(confirm('Are you sure?')){ batch('tasks', this.options[this.selectedIndex].text, this.value); document.getElementById('default').selected='selected'; }}">
   <option id='default' value=''>Choose an action..</option>
   <option value='complete'>Mark As Completed</option>
   <option value='cancel'>Mark As Cancelled</option>
   </select>
   </td></tr>
   </tfoot>
</table>
<br>

<div align='center'>
<?php
	//Display pages based on total number of tasks. Each page may have 20 tasks.
	$pages = round($totaltasks/20);
	
	if($totaltasks%20 > 0)
	{
		$pages++;
	}
	
	if($pages > 0)
	{
		?>
		<button OnClick="window.location='<?php echo public_path(); ?>backend.php/tasks/pending?<?php if(!empty($filterstatus)){ echo "&filterstatus=".$filterstatus; } ?><?php if(!empty($filterpriority)){ echo "&filterpriority=".$filterpriority; } ?><?php if(!empty($filterdepartment)){ echo "&filterdepartment=".$filterdepartment; } ?><?php if(!empty($filterreviewer)){ echo "&filterreviewer=".$filterreviewer; } ?>';">First</button>
		<?php
		
		if($selectedpage > 1)
		{
			echo ".... ";
			?>
			<button OnClick="window.location='<?php echo public_path(); ?>backend.php/tasks/pending?page=<?php echo ($selectedpage-1); ?><?php if(!empty($filterstatus)){ echo "&filterstatus=".$filterstatus; } ?><?php if(!empty($filterpriority)){ echo "&filterpriority=".$filterpriority; } ?><?php if(!empty($filterdepartment)){ echo "&filterdepartment=".$filterdepartment; } ?><?php if(!empty($filterreviewer)){ echo "&filterreviewer=".$filterreviewer; } ?>';"><?php echo ($selectedpage-1); ?></button>
			<?php
		}
		
		$count = 0;
		
		for($page = $selectedpage; $page <= $pages; $page++)
		{	
			$count++;
			
			$colour = "";
			
			if($selectedpage == $page)
			{
				$colour = "class='blue'";
			}
			?>
			<button <?php echo $colour; ?> OnClick="window.location='<?php echo public_path(); ?>backend.php/tasks/pending?page=<?php echo $page; ?><?php if(!empty($filterstatus)){ echo "&filterstatus=".$filterstatus; } ?><?php if(!empty($filterpriority)){ echo "&filterpriority=".$filterpriority; } ?><?php if(!empty($filterdepartment)){ echo "&filterdepartment=".$filterdepartment; } ?><?php if(!empty($filterreviewer)){ echo "&filterreviewer=".$filterreviewer; } ?>';"><?php echo $page ?></button>
			<?php
			if($count == 10)
			{
				echo "....";
				break;
			}
		}	
		?>
		<button OnClick="window.location='<?php echo public_path(); ?>backend.php/tasks/pending?page=<?php echo $pages; ?><?php if(!empty($filterstatus)){ echo "&filterstatus=".$filterstatus; } ?><?php if(!empty($filterpriority)){ echo "&filterpriority=".$filterpriority; } ?><?php if(!empty($filterdepartment)){ echo "&filterdepartment=".$filterdepartment; } ?><?php if(!empty($filterreviewer)){ echo "&filterreviewer=".$filterreviewer; } ?>';">Last</button>
		<?php
	}
?>
</div>