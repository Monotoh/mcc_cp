<?php
/**
 * _listheader partial.
 *
 * Display task list header
 *
 * @package    backend
 * @subpackage tasks
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
 
$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
?>
<form>
    <label style='height: 30px; margin-top: -2px;'>
        <div style='float: left; padding-top: 5px; font-size: 20px; font-weight: 700;'>
            My Tasks
        </div>
        <div style='float: left; margin-left: 30px;'>
        <!-- Filter tasks by status -->
   		<select id='task_status' name='task_status'  onChange="window.location='<?php echo public_path(); ?>backend.php/tasks/pending?filterstatus=' + this.value + '<?php if(!empty($filterpriority)){ echo "&filterpriority=".$filterpriority; } ?><?php if(!empty($filterdepartment)){ echo "&filterdepartment=".$filterdepartment; } ?>';">
            <option value=''>Filter By Status</option>
            <option value="1" <?php if($filterstatus != "" && $filterstatus == "1"){ echo "selected";  } ?>>Pending</option>
            <option value="25" <?php if($filterstatus != "" && $filterstatus == "25"){ echo "selected";  } ?>>Completed</option>
            <option value="45" <?php if($filterstatus != "" && $filterstatus == "45"){ echo "selected";  } ?>>Transferred</option>
            <option value="2" <?php if($filterstatus != "" && $filterstatus == "2"){ echo "selected";  } ?>>Awaiting Approval</option>
            <option value="5" <?php if($filterstatus != "" && $filterstatus == "5"){ echo "selected";  } ?>>Awaiting Cancellation</option>
            <option value="4" <?php if($filterstatus != "" && $filterstatus == "4"){ echo "selected";  } ?>>Awaiting Transferral</option>
            <option value="55" <?php if($filterstatus != "" && $filterstatus == "55"){ echo "selected";  } ?>>Cancelled</option>
            <option value="3" <?php if($filterstatus != "" && $filterstatus == "3"){ echo "selected";  } ?>>PostPoned</option>
            <option value="755" <?php if($filterstatus != "" && $filterstatus == "755"){ echo "selected";  } ?>>Queued</option>
    	</select>

        <!-- Filter tasks by priority -->
    	<select id='task_priority' name='task_priority'  onChange="window.location='<?php echo public_path(); ?>backend.php/tasks/pending?filterpriority=' + this.value + '<?php if(!empty($filterstatus)){ echo "&filterstatus=".$filterstatus; } ?><?php if(!empty($filterdepartment)){ echo "&filterdepartment=".$filterdepartment; } ?>';">
            <option value="0" <?php if($filterpriority != "" && $filterpriority == "0"){ echo "selected";  } ?>>Filter By Priority</option>
            <option value="3" <?php if($filterpriority != "" && $filterpriority == "3"){ echo "selected";  } ?>>Normal</option>
            <option value="2" <?php if($filterpriority != "" && $filterpriority == "2"){ echo "selected";  } ?>>Important</option>
            <option value="1" <?php if($filterpriority != "" && $filterpriority == "1"){ echo "selected";  } ?>>Critical</option>
    	</select>
     
        <!-- Filter tasks by department -->
    	<select id='task_department' name='task_department'   onChange="window.location='<?php echo public_path(); ?>backend.php/tasks/alltasks?filterdepartment=' + this.value + '<?php if(!empty($filterpriority)){ echo "&filterpriority=".$filterpriority; } ?><?php if(!empty($filterstatus)){ echo "&filterstatus=".$filterstatus; } ?>';">
            <option value="0" selected disabled>Filter By Department</option>
            <?php
				  //Get departments and number of tasks in each department
                  if($sf_user->mfHasCredential('assigntask'))
                  {
					  	//Iterate through all departments
                        $q = Doctrine_Query::create()
                         ->from('Department a');
                        $departments = $q->execute();
                        foreach($departments as $department)
                        {
                            $selected = "";
                            if($filterdepartment != "" && $filterdepartment == $department->getDepartmentName()){
								 $selected = "selected";
							}
                            
							//Get number of tasks in this departments, apply filters if neccesary
							$jobs = 0;
							$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE c.strdepartment = '".$department->getDepartmentName()."' AND a.status = ".$filterstatus;
							$result = mysql_query($query, $dbconn);
							$jobs = mysql_num_rows($result);
                            echo "<option value='".$department->getDepartmentName()."'>".$department->getDepartmentName()." (".$jobs.")</option>";
                        }
        
                  }
                  else
                  {
					  	//Iterate through current user's tasks if current user doesn't have privileges to view other departments
                        $q = Doctrine_Query::create()
                            ->from('CfUser a')
                            ->where('a.nid = ?', $_SESSION["SESSION_CUTEFLOW_USERID"]);
                        $logged_in_reviewer = $q->fetchOne();
                        $q = Doctrine_Query::create()
                         ->from('Department a')
                         ->where('a.department_name = ?', $logged_in_reviewer->getStrdepartment());
                        $departments = $q->execute();
                        foreach($departments as $department)
                        {
                            $selected = "";
                            if($filterdepartment != "" && $filterdepartment == $department->getDepartmentName()){
								 $selected = "selected";  
							}
                            
							//Get number of tasks current user has, apply filters if neccesary
							$jobs = 0;
							$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.status = ".$filterstatus." AND c.strdepartment = '".$department->getDepartmentName()."'";
							$result = mysql_query($query, $dbconn);
							$jobs = mysql_num_rows($result);
                            echo "<option value='".$department->getDepartmentName()."'>".$department->getDepartmentName()." (".$jobs.")</option>";
                        }
                   }
            ?>
    	</select>
	</div>
	</label>
	<fieldset style="margin: 0px;">
	<section>
   		<div style='width: 100%;'> &nbsp; &nbsp; &nbsp; 
			<?php
			//Allow user to create a new task if they have the privilege
            if($sf_user->mfHasCredential('assigntask'))
            {
            ?>
            <button onClick="window.location='<?php echo public_path(); ?>backend.php/tasks/new';">New Task</button>
            <?php
            }
            ?>
            <div style='float: right;'>
               <!-- Filter tasks assigned to me -->
               <button style='background-color: #BBBBBB;'  onClick="window.location='<?php echo public_path(); ?>backend.php/tasks/pending' + this.value;">Tasks Assigned To Me</button>
               <?php
			   //If user has privileges to assign tasks, show filters for finding their tasks
               if($sf_user->mfHasCredential('assigntask') || $sf_user->mfHasCredential('has_hod_access'))
               {
               ?>
                    <!-- Filter tasks assigned to me -->
                    <button onClick="window.location='<?php echo public_path(); ?>backend.php/tasks/assignedbyme' + this.value;">Tasks Assigned By Me</button>
                    
                    <!-- Filter tasks awaiting current user's approval -->
                    <button  <?php if($filterstatus == 2){ ?>style='background-color: #BBBBBB;'<?php } ?> onClick="window.location='<?php echo public_path(); ?>backend.php/tasks/assigned?filterstatus=2' + this.value;">Tasks Awaiting My Approval</button>
                <?php
                }
                ?>
            </div>
   		</div>
  </section>
 </fieldset>
</form>
<?php
//Show any user notifications
if($sf_user->hasFlash('notice') &&  $sf_user->getFlash('notice'))
{
	?>
	<div class="alert success"><?php echo $sf_user->getFlash('notice'); ?></div>
	<?php
	$sf_user->setFlash('notice','');
}

if($sf_user->hasFlash('error') && $sf_user->getFlash('error'))
{
	?>
	<div class="alert warning"><?php echo $sf_user->getFlash('error'); ?></div>
	<?php
	$sf_user->setFlash('error','');
}
?>