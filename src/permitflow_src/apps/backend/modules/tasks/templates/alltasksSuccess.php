<?php
/**
 * AlltasksSuccess templates.
 *
 * Displays all tasks in the system
 *
 * @package    backend
 * @subpackage tasks
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>
<?php
	$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
	mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
	
	function paginate($currentpage, $count)
	{
	    if($currentpage){
			$pagelimit = 20*$currentpage;
			$pagestart = $pagelimit - 20;
			
			if($count >= $pagestart && $count <= $pagelimit)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return true;
		}
	}
	
	$selectedpage = $_GET['page'];
	if(empty($selectedpage))
	{
		$selectedpage = 1;
	}
	
	$filterstatus = $_GET['filterstatus'];
	if(!empty($filterstatus))
	{
		$filter = " AND a.status = ".$filterstatus;
	}
	else
	{
		$filterstatus = "1";
		$filter = " AND (a.status = 1 OR a.status = 2 OR a.status = 3 OR a.status = 4 OR a.status = 5)";
	}
	
	$filterpriority = $_GET['filterpriority'];
	if(!empty($filterpriority))
	{
		if(!empty($filter))
		{
			$filter = $filter." AND a.priority = ".$filterpriority;
		}
		else
		{
			$filter = " AND a.priority = ".$filterpriority;
		}
	}
	
	$filterdepartment = $_GET['filterdepartment'];
	if(!empty($filterdepartment))
	{
		if(!empty($filter))
		{
			$filter = $filter." AND c.strdepartment = '".$filterdepartment."'";
		}
		else
		{
			$filter = " AND c.strdepartment = '".$filterdepartment."'";
		}
	}
	
	$filterreviewer = $_GET['filterreviewer'];
	if(!empty($filterreviewer))
	{
		if(!empty($filter))
		{
			$filter = $filter." AND a.owner_user_id = '".$filterreviewer."'";
		}
		else
		{
			$filter = " AND a.owner_user_id = '".$filterreviewer."'";
		}
	}
	
?>
<div class="g12" style="padding-left: 3px;">
			<form>
			<label style='height: 30px; margin-top: 0px;'>
			<div style='float: left; font-size: 20px; font-weight: 700;'><?php
					echo $filterdepartment;
			?> Tasks
			</div>
			<div style='float: right;'>
			<select id='task_status' name='task_status'  onChange="window.location='<?php echo public_path(); ?>backend.php/tasks/alltasks?filterstatus=' + this.value + '<?php if(!empty($filterpriority)){ echo "&filterpriority=".$filterpriority; } ?><?php if(!empty($filterdepartment)){ echo "&filterdepartment=".$filterdepartment; } ?>';">
			<option value=''>Filter By Status</option>
				<option value="1" <?php if($filterstatus != "" && $filterstatus == "1"){ echo "selected";  } ?>>Pending</option>
				<option value="25" <?php if($filterstatus != "" && $filterstatus == "25"){ echo "selected";  } ?>>Completed</option>
				<option value="45" <?php if($filterstatus != "" && $filterstatus == "45"){ echo "selected";  } ?>>Transferred</option>
				<option value="2" <?php if($filterstatus != "" && $filterstatus == "2"){ echo "selected";  } ?>>Awaiting Approval</option>
				<option value="5" <?php if($filterstatus != "" && $filterstatus == "5"){ echo "selected";  } ?>>Awaiting Cancellation</option>
				<option value="4" <?php if($filterstatus != "" && $filterstatus == "4"){ echo "selected";  } ?>>Awaiting Transferral</option>
				<option value="55" <?php if($filterstatus != "" && $filterstatus == "55"){ echo "selected";  } ?>>Cancelled</option>
				<option value="3" <?php if($filterstatus != "" && $filterstatus == "3"){ echo "selected";  } ?>>PostPoned</option>
				<option value="755" <?php if($filterstatus != "" && $filterstatus == "755"){ echo "selected";  } ?>>Queued</option>
			
			</select>
			
			<select id='task_priority' name='task_priority'  onChange="window.location='<?php echo public_path(); ?>backend.php/tasks/alltasks?filterpriority=' + this.value + '<?php if(!empty($filterstatus)){ echo "&filterstatus=".$filterstatus; } ?><?php if(!empty($filterdepartment)){ echo "&filterdepartment=".$filterdepartment; } ?>';">
				<option value="0" <?php if($filterpriority != "" && $filterpriority == "0"){ echo "selected";  } ?>>Filter By Priority</option>
				<option value="3" <?php if($filterpriority != "" && $filterpriority == "3"){ echo "selected";  } ?>>Normal</option>
				<option value="2" <?php if($filterpriority != "" && $filterpriority == "2"){ echo "selected";  } ?>>Important</option>
				<option value="1" <?php if($filterpriority != "" && $filterpriority == "1"){ echo "selected";  } ?>>Critical</option>
			</select>
			 
			<select id='task_department' name='task_department'   onChange="window.location='<?php echo public_path(); ?>backend.php/tasks/alltasks?filterdepartment=' + this.value + '<?php if(!empty($filterpriority)){ echo "&filterpriority=".$filterpriority; } ?><?php if(!empty($filterstatus)){ echo "&filterstatus=".$filterstatus; } ?>';">
				<option value="0" <?php if($filterdepartment != "" && $filterdepartment == "0"){ echo "selected";  } ?> disabled>Filter By Department</option>
				<?php
					  if($sf_user->mfHasCredential('assigntask'))
					  {
							$q = Doctrine_Query::create()
							 ->from('Department a');
							$departments = $q->execute();
							foreach($departments as $department)
							{
								$selected = "";
								if($filterdepartment != "" && $filterdepartment == $department->getDepartmentName()){ $selected = "selected";  }
								
									//Get Related Tasks
									$jobs = 0;
									$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE c.strdepartment = '".$department->getDepartmentName()."' AND a.status = ".$filterstatus;
									$result = mysql_query($query, $dbconn);
									$jobs = mysql_num_rows($result);
								echo "<option value='".$department->getDepartmentName()."' ".$selected.">".$department->getDepartmentName()." (".$jobs.")</option>";
							}

					  }
					  else
					  {
							$q = Doctrine_Query::create()
								->from('CfUser a')
								->where('a.nid = ?', $_SESSION["SESSION_CUTEFLOW_USERID"]);
							$logged_in_reviewer = $q->fetchOne();
							$q = Doctrine_Query::create()
							 ->from('Department a')
							 ->where('a.department_name = ?', $logged_in_reviewer->getStrdepartment());
							$departments = $q->execute();
							foreach($departments as $department)
							{
								$selected = "";
								if($filterdepartment != "" && $filterdepartment == $department->getDepartmentName()){ $selected = "selected";  }
								
									//Get Related Tasks
									$jobs = 0;
									$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.status = ".$filterstatus." AND c.strdepartment = '".$department->getDepartmentName()."'";
									$result = mysql_query($query, $dbconn);
									$jobs = mysql_num_rows($result);
								echo "<option value='".$department->getDepartmentName()."' ".$selected.">".$department->getDepartmentName()." (".$jobs.")</option>";
							}
					   }
				?>
			</select>
			 
			<select id='task_reviewer' name='task_reviewer'   onChange="window.location='<?php echo public_path(); ?>backend.php/tasks/alltasks?filterreviewer=' + this.value + '<?php if(!empty($filterdepartment)){ echo "&filterdepartment=".$filterdepartment; } ?><?php if(!empty($filterpriority)){ echo "&filterpriority=".$filterpriority; } ?><?php if(!empty($filterstatus)){ echo "&filterstatus=".$filterstatus; } ?>';">
				<option value="0" <?php if($filterreviewer != "" && $filterreviewer == "0"){ echo "selected";  } ?>>Filter By Reviewer</option>
				<?php
					$q = Doctrine_Query::create()
					   ->from('CfUser a')
					   ->where('a.strdepartment = ?', $filterdepartment)
					   ->orderBy('a.strfirstname ASC');
					$reviewers = $q->execute();
					foreach($reviewers as $reviewer)
					{
						$selected = "";
						
						if($reviewer->getNid() == $filterreviewer)
						{
							$selected = "selected";
						}
						
						//What group does this user belong to
				        $q = Doctrine_Query::create()
				           ->from('MfGuardUserGroup a')
				           ->where('a.user_id = ?', $reviewer->getNid());
				        $group_relations = $q->execute();
				
				        $color = "";
				
				        $groupname = "";
				
				        foreach($group_relations as $group_relation)
				        {
				            $q = Doctrine_Query::create()
				               ->from('MfGuardGroup a')
				               ->where('a.id = ?', $group_relation->getGroupId());
				            $group = $q->fetchOne();
				            
				            if($group_relation->getGroupId() == "13")
				            {
				                $color = "style='background-color: #FFCC66;'";
				                $groupname = $group->getName();
				            }
				            if($group_relation->getGroupId() == "6")
				            {
				                $color = "style='background-color: #66FF66;'";
				                $groupname = $group->getName();
				            }
				            if($group_relation->getGroupId() == "4")
				            {
				                $color = "style='background-color: #66FFFF;'";
				                $groupname = $group->getName();
				            }
				            if($group_relation->getGroupId() == "7")
				            {
				                $color = "style='background-color: #0099CC;'";
				                $groupname = $group->getName();
				            }
				        }
						
						
						//Get Related Slots
						$jobs = 0;
						$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.status = ".$filterstatus." AND c.nid = '".$reviewer->getNid()."'";
						$result = mysql_query($query, $dbconn);
						$jobs = mysql_num_rows($result);
						
						echo "<option ".$color." value='".$reviewer->getNid()."' ".$selected.">".$reviewer->getStrfirstname()." ".$reviewer->getStrlastname()." (".$jobs.")</option>";
					}
				?>
			</select>
			
			</div>
			</label>
			<fieldset style="margin: 0px; width: 100%;">
			<section>
			   <div style='width: 100%;'> &nbsp; &nbsp; &nbsp; 
				
				 <?php
			   if($sf_user->mfHasCredential('assigntask'))
			   {
			   ?>
				<button onClick="window.location='<?php echo public_path(); ?>backend.php/tasks/new';">New Task</button>
			   <?php
			   }
			   ?>
				<div style='float: right;'>
			   <button class='i_arrow_left icon' onClick="window.location='<?php echo public_path(); ?>backend.php/tasks/pending?filterstatus=' + this.value;">Back To My Tasks</button>
			   
			   <?php
			   if($sf_user->mfHasCredential('assigntask') || $sf_user->mfHasCredential('has_hod_access'))
			   {
			   ?>
				<button onClick="window.location='<?php echo public_path(); ?>backend.php/tasks/assigned' + this.value;">Tasks Assigned By Me</button>
				<button  <?php if($filterstatus == 2){ ?>style='background-color: #BBBBBB;'<?php } ?> onClick="window.location='<?php echo public_path(); ?>backend.php/tasks/assigned?filterstatus=2' + this.value;">Tasks Awaiting My Approval</button>
				<?php
			   }
				?>
				</div>
			   </div>
			</section>
			</fieldset>
			</form>
			
			
			
			
			<div id='notifications' name='notifications'>
			</div>
			
			<?php
				if($sf_user->hasFlash('notice') &&  $sf_user->getFlash('notice'))
				{
					?>
					<div class="alert success"><?php echo $sf_user->getFlash('notice'); ?></div>
					<?php
					$sf_user->setFlash('notice','');
				}
			?>
			
			<?php
				if($sf_user->hasFlash('error') && $sf_user->getFlash('error'))
				{
					?>
					<div class="alert warning"><?php echo $sf_user->getFlash('error'); ?></div>
					<?php
					$sf_user->setFlash('error','');
				}
			?>
			
		
				
				<?php
				
			$taskcount = 0;
			$totaltasks = 0;
				
				
					
					$daytasks = null;
					$daytaskstotal = null;
						
					$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.date_created LIKE '%".date('Y-m-d')."%'".$filter;
			
					$results = mysql_query($query,$dbconn);
					
					
					
					while($row = mysql_fetch_assoc($results))
					{
							
							{
								if(paginate($selectedpage, $totaltasks))
								{
									$daytasks[] = $row;
									$appcount++;
								}
							}
								$daytaskstotal[] = $row;
								$totaltasks++;
					}
					
					if(sizeof($daytasks) > 0)
					{
				?>
			    <div class="accordion">
					<h4 class='ui-state-active'><a href="#">Today / <?php echo date('l'); ?> <?php echo date('d/m/Y'); ?> <div style='float: right;'>(<?php echo sizeof($daytaskstotal); ?>)</div></a> </h4>
					<div style="margin: 0 0 0 0; padding: 0 0 0 0;">
					    <?php include_partial('list', array('tasks' => $daytasks)) ?>
					</div>
				</div>
				 <?php
				 echo "---------->";
				 }
				    $daytasks = null;
					$daytaskstotal = null;
					
					$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.date_created LIKE '%".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*1)."%'".$filter;
					$results = mysql_query($query,$dbconn);
					
					
					while($row = mysql_fetch_assoc($results))
					{
							
							{
								if(paginate($selectedpage, $totaltasks))
								{
									$daytasks[] = $row;
									$appcount++;
								}
							}
								$daytaskstotal[] = $row;
								$totaltasks++;
					}
					
					
					if(sizeof($daytasks) > 0)
					{
				?>
			    <div class="accordion">
					<h4 class='ui-state-active'><a href="#">Yersterday / <?php echo date('l', strtotime(date('Y-m-d')) - 86400); ?> <?php echo date('d/m/Y', strtotime(date('Y-m-d')) - 86400); ?> <div style='float: right;'>(<?php echo sizeof($daytaskstotal); ?>)</div></a></h4>
					<div style="margin: 0 0 0 0; padding: 0 0 0 0;">
					    <?php include_partial('list', array('tasks' => $daytasks)) ?>
					</div>
				</div>
				
				 <?php
				 }
				    $daytasks = null;
					$daytaskstotal = null;
					
					$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.date_created LIKE '%".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*2)."%'".$filter;
					$results = mysql_query($query,$dbconn);
					
					
					while($row = mysql_fetch_assoc($results))
					{
							
							{
								if(paginate($selectedpage, $totaltasks))
								{
									$daytasks[] = $row;
									$appcount++;
								}
							}
								$daytaskstotal[] = $row;
								$totaltasks++;
					}
					
					if(sizeof($daytasks) > 0)
					{
				?>
			    <div class="accordion">
					<h4><a href="#">2 Days Ago / <?php echo date('l', strtotime(date('Y-m-d')) - 86400*2); ?> <?php echo date('d/m/Y', strtotime(date('Y-m-d')) - 86400*2); ?> <div style='float: right;'>(<?php echo sizeof($daytaskstotal); ?>)</div></a></h4>
					<div style="margin: 0 0 0 0; padding: 0 0 0 0;">
					    <?php include_partial('list', array('tasks' => $daytasks)) ?>
					</div>
				</div>
				
				 <?php
				 }
				    $daytasks = null;
					$daytaskstotal = null;
						
					$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.date_created LIKE '%".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*3)."%'".$filter;
					$results = mysql_query($query,$dbconn);
					
					
					while($row = mysql_fetch_assoc($results))
					{
							
							{
								if(paginate($selectedpage, $totaltasks))
								{
									$daytasks[] = $row;
									$appcount++;
								}
							}
								$daytaskstotal[] = $row;
								$totaltasks++;
					}
					
					if(sizeof($daytasks) > 0)
					{
				?>
			    <div class="accordion">
					<h4><a href="#">3 Days Ago / <?php echo date('l', strtotime(date('Y-m-d')) - 86400*3); ?> <?php echo date('d/m/Y', strtotime(date('Y-m-d')) - 86400*3); ?> <div style='float: right;'>(<?php echo sizeof($daytaskstotal); ?>)</div></a></h4>
					<div style="margin: 0 0 0 0; padding: 0 0 0 0;">
					    <?php include_partial('list', array('tasks' => $daytasks)) ?>
					</div>
				</div>
				
				 <?php
				 }
				    $daytasks = null;
					$daytaskstotal = null;
					
					$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.date_created LIKE '%".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*4)."%'".$filter;
					$results = mysql_query($query,$dbconn);
					
					
					while($row = mysql_fetch_assoc($results))
					{
						
						{
							if(paginate($selectedpage, $totaltasks))
							{
								$daytasks[] = $row;
								$appcount++;
							}
						}
							$daytaskstotal[] = $row;
							$totaltasks++;
					}
					
					if(sizeof($daytasks) > 0)
					{
				?>
			    <div class="accordion">
					<h4><a href="#">4 Days Ago / <?php echo date('l', strtotime(date('Y-m-d')) - 86400*4); ?> <?php echo date('d/m/Y', strtotime(date('Y-m-d')) - 86400*4); ?> <div style='float: right;'>(<?php echo sizeof($daytaskstotal); ?>)</div></a></h4>
					<div style="margin: 0 0 0 0; padding: 0 0 0 0;">
					    <?php include_partial('list', array('tasks' => $daytasks)) ?>
					</div>
				</div>
				
				 <?php
				 }
				    $daytasks = null;
					$daytaskstotal = null;
				
					$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.date_created LIKE '%".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*5)."%'".$filter;
					$results = mysql_query($query,$dbconn);
					
					
					while($row = mysql_fetch_assoc($results))
					{
						
						{
							if(paginate($selectedpage, $totaltasks))
							{
								$daytasks[] = $row;
								$appcount++;
							}
						}
							$daytaskstotal[] = $row;
							$totaltasks++;
					}
					
					if(sizeof($daytasks) > 0)
					{
				?>
			    <div class="accordion">
					<h4><a href="#">5 Days Ago / <?php echo date('l', strtotime(date('Y-m-d')) - 86400*5); ?> <?php echo date('d/m/Y', strtotime(date('Y-m-d')) - 86400*5); ?> <div style='float: right;'>(<?php echo sizeof($daytaskstotal); ?>)</div></a></h4>
					<div style="margin: 0 0 0 0; padding: 0 0 0 0;">
					    <?php include_partial('list', array('tasks' => $daytasks)) ?>
					</div>
				</div>
				
				 <?php
				 
				 }
				    $daytasks = null;
					$daytaskstotal = null;
					
					$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.date_created LIKE '%".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*6)."%'".$filter;
					$results = mysql_query($query,$dbconn);
					
					
					while($row = mysql_fetch_assoc($results))
					{
						
						{
							if(paginate($selectedpage, $totaltasks))
							{
								$daytasks[] = $row;
								$appcount++;
							}
						}
							$daytaskstotal[] = $row;
							$totaltasks++;
					}
					
					if(sizeof($daytasks) > 0)
					{
				?>
			    <div class="accordion">
					<h4><a href="#">6 Days Ago / <?php echo date('l', strtotime(date('Y-m-d')) - 86400*6); ?> <?php echo date('d/m/Y', strtotime(date('Y-m-d')) - 86400*6); ?> <div style='float: right;'>(<?php echo sizeof($daytaskstotal); ?>)</div></a></h4>
					<div style="margin: 0 0 0 0; padding: 0 0 0 0;">
					    <?php include_partial('list', array('tasks' => $daytasks)) ?>
					</div>
				</div>
				
				 <?php
				 }
				 
				    $daytasks = null;
					$daytaskstotal = null;
					
					$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.date_created LIKE '%".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*7)."%'".$filter;
					$results = mysql_query($query,$dbconn);
					
					
					while($row = mysql_fetch_assoc($results))
					{
						
						{
							if(paginate($selectedpage, $totaltasks))
							{
								$daytasks[] = $row;
								$appcount++;
							}
						}
							$daytaskstotal[] = $row;
							$totaltasks++;
					}
					
					if(sizeof($daytasks) > 0)
					{
				?>
			    <div class="accordion">
					<h4><a href="#">7 Days Ago / <?php echo date('l', strtotime(date('Y-m-d')) - 86400*7); ?> <?php echo date('d/m/Y', strtotime(date('Y-m-d')) - 86400*7); ?> <div style='float: right;'>(<?php echo sizeof($daytaskstotal); ?>)</div></a></h4>
					<div style="margin: 0 0 0 0; padding: 0 0 0 0;">
					    <?php include_partial('list', array('tasks' => $daytasks)) ?>
					</div>
				</div>
				<?php
				}
				
				    $daytasks = null;
					$daytaskstotal = null;
					
					$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.date_created BETWEEN '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*15)."' AND '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*8)."'".$filter;
					$results = mysql_query($query,$dbconn);
					
					
					while($row = mysql_fetch_assoc($results))
					{
						
						{
							if(paginate($selectedpage, $totaltasks))
							{
								$daytasks[] = $row;
								$appcount++;
							}
						}
							$daytaskstotal[] = $row;
							$totaltasks++;
					}
				
					if(sizeof($daytasks) > 0)
					{
				?>
			    <div class="accordion">
					<h4><a href="#">1 Week Ago <div style='float: right;'>(<?php echo sizeof($daytaskstotal); ?>)</div></a></h4>
					<div style="margin: 0 0 0 0; padding: 0 0 0 0;">
					    <?php include_partial('list', array('tasks' => $daytasks)) ?>
					</div>
				</div>
				<?php
				}
				  $daytasks = null;
				  $daytaskstotal = null;
				
				$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.date_created BETWEEN '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*23)."' AND '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*16)."'".$filter;
				$results = mysql_query($query,$dbconn);
					
				
				while($row = mysql_fetch_assoc($results))
				{
					
					{
						if(paginate($selectedpage, $totaltasks))
						{
							$daytasks[] = $row;
							$appcount++;
						}
					}
						$daytaskstotal[] = $row;
						$totaltasks++;
				}
					
					if(sizeof($daytasks) > 0)
					{
				?>
			    <div class="accordion">
					<h4><a href="#">2 Weeks Ago <div style='float: right;'>(<?php echo sizeof($daytaskstotal); ?>)</div></a></h4>
					<div style="margin: 0 0 0 0; padding: 0 0 0 0;">
					    <?php include_partial('list', array('tasks' => $daytasks)) ?>
					</div>
				</div>
				<?php
				}
				
					$daytasks = null;
					$daytaskstotal = null;
					
					$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.date_created BETWEEN '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*30)."' AND '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*23)."'".$filter;
					$results = mysql_query($query,$dbconn);
					
				    
					while($row = mysql_fetch_assoc($results))
					{
						
						{
							if(paginate($selectedpage, $totaltasks))
							{
								$daytasks[] = $row;
								$appcount++;
							}
						}
							$daytaskstotal[] = $row;
							$totaltasks++;
					}
				
					
					if(sizeof($daytasks) > 0)
					{
				?>
			    <div class="accordion">
					<h4><a href="#">3 Weeks Ago <div style='float: right;'>(<?php echo sizeof($daytaskstotal); ?>)</div></a></h4>
					<div style="margin: 0 0 0 0; padding: 0 0 0 0;">
					    <?php include_partial('list', array('tasks' => $daytasks)) ?>
					</div>
				</div>
				<?php
				}
				
					$daytasks = null;
					$daytaskstotal = null;
					
					$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.date_created BETWEEN '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*60)."' AND '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*30)."'".$filter;
					$results = mysql_query($query,$dbconn);
				
					while($row = mysql_fetch_assoc($results))
					{
						
						{
							if(paginate($selectedpage, $totaltasks))
							{
								$daytasks[] = $row;
								$appcount++;
							}
						}
							$daytaskstotal[] = $row;
							$totaltasks++;
					}
					
					if(sizeof($daytasks) > 0)
					{
				?>
			    <div class="accordion">
					<h4><a href="#">1 Month Ago <div style='float: right;'>(<?php echo sizeof($daytaskstotal); ?>)</div></a></h4>
					<div style="margin: 0 0 0 0; padding: 0 0 0 0;">
					    <?php include_partial('list', array('tasks' => $daytasks)) ?>
					</div>
				</div>
				<?php
				}
				
					$daytasks = null;
					$daytaskstotal = null;
					
					$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.date_created BETWEEN '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*90)."' AND '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*61)."'".$filter;
					$results = mysql_query($query,$dbconn);
				
					while($row = mysql_fetch_assoc($results))
					{
						
						{
							if(paginate($selectedpage, $totaltasks))
							{
								$daytasks[] = $row;
								$appcount++;
							}
						}
							$daytaskstotal[] = $row;
							$totaltasks++;
					}
					
					if(sizeof($daytasks) > 0)
					{
				?>
			    <div class="accordion">
					<h4><a href="#">2 Months Ago <div style='float: right;'>(<?php echo sizeof($daytaskstotal); ?>)</div></a></h4>
					<div style="margin: 0 0 0 0; padding: 0 0 0 0;">
					    <?php include_partial('list', array('tasks' => $daytasks)) ?>
					</div>
				</div>
				<?php
				}
				
					$daytasks = null;
					$daytaskstotal = null;
					
					$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.date_created BETWEEN '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*120)."' AND '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*91)."'".$filter;
					$results = mysql_query($query,$dbconn);
					while($row = mysql_fetch_assoc($results))
					{
						
						{
							if(paginate($selectedpage, $totaltasks))
							{
								$daytasks[] = $row;
								$appcount++;
							}
						}
							$daytaskstotal[] = $row;
							$totaltasks++;
					}
					
					if(sizeof($daytasks) > 0)
					{
				?>
			    <div class="accordion">
					<h4><a href="#">3 Months Ago <div style='float: right;'>(<?php echo sizeof($daytaskstotal); ?>)</div></a></h4>
					<div style="margin: 0 0 0 0; padding: 0 0 0 0;">
					    <?php include_partial('list', array('tasks' => $daytasks)) ?>
					</div>
				</div>
				<?php
				}
				
					$daytasks = null;
					$daytaskstotal = null;
					
					$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.date_created BETWEEN '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*150)."' AND '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*121)."'".$filter;
					$results = mysql_query($query,$dbconn);
					while($row = mysql_fetch_assoc($results))
					{
						
						{
							if(paginate($selectedpage, $totaltasks))
							{
								$daytasks[] = $row;
								$appcount++;
							}
						}
							$daytaskstotal[] = $row;
							$totaltasks++;
					}
					
					if(sizeof($daytasks) > 0)
					{
				?>
			    <div class="accordion">
					<h4><a href="#">4 Months Ago <div style='float: right;'>(<?php echo sizeof($daytaskstotal); ?>)</div></a></h4>
					<div style="margin: 0 0 0 0; padding: 0 0 0 0;">
					    <?php include_partial('list', array('tasks' => $daytasks)) ?>
					</div>
				</div>
				<?php
				}
				
					$daytasks = null;
					$daytaskstotal = null;
				
					$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.date_created BETWEEN '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*180)."' AND '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*151)."'".$filter;
					$results = mysql_query($query,$dbconn);
					while($row = mysql_fetch_assoc($results))
					{
						
						{
							if(paginate($selectedpage, $totaltasks))
							{
								$daytasks[] = $row;
								$appcount++;
							}
						}
							$daytaskstotal[] = $row;
							$totaltasks++;
					}
					
					if(sizeof($daytasks) > 0)
					{
				?>
			    <div class="accordion">
					<h4><a href="#">5 Months Ago <div style='float: right;'>(<?php echo sizeof($daytaskstotal); ?>)</div></a></h4>
					<div style="margin: 0 0 0 0; padding: 0 0 0 0;">
					    <?php include_partial('list', array('tasks' => $daytasks)) ?>
					</div>
				</div>
				<?php
				}
				
					$daytasks = null;
					$daytaskstotal = null;
					
					$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.date_created BETWEEN '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*210)."' AND '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*181)."'".$filter;
					$results = mysql_query($query,$dbconn);
					while($row = mysql_fetch_assoc($results))
					{
						
						{
							if(paginate($selectedpage, $totaltasks))
							{
								$daytasks[] = $row;
								$appcount++;
							}
						}
							$daytaskstotal[] = $row;
							$totaltasks++;
					}
					
					if(sizeof($daytasks) > 0)
					{
				?>
			    <div class="accordion">
					<h4><a href="#">6 Months Ago <div style='float: right;'>(<?php echo sizeof($daytaskstotal); ?>)</div></a></h4>
					<div style="margin: 0 0 0 0; padding: 0 0 0 0;">
					    <?php include_partial('list', array('tasks' => $daytasks)) ?>
					</div>
				</div>
				<?php
				}
				
					$daytasks = null;
					$daytaskstotal = null;
					
					$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.date_created BETWEEN '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*240)."' AND '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*211)."'".$filter;
					$results = mysql_query($query,$dbconn);
					while($row = mysql_fetch_assoc($results))
					{
						
						{
							if(paginate($selectedpage, $totaltasks))
							{
								$daytasks[] = $row;
								$appcount++;
							}
						}
							$daytaskstotal[] = $row;
							$totaltasks++;
					}
						
					if(sizeof($daytasks) > 0)
					{
				?>
			    <div class="accordion">
					<h4><a href="#">7 Months Ago <div style='float: right;'>(<?php echo sizeof($daytaskstotal); ?>)</div></a></h4>
					<div style="margin: 0 0 0 0; padding: 0 0 0 0;">
					    <?php include_partial('list', array('tasks' => $daytasks)) ?>
					</div>
				</div>
				<?php
				}
				
					$daytasks = null;
					$daytaskstotal = null;
					
					$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.date_created BETWEEN '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*270)."' AND '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*241)."'".$filter;
					$results = mysql_query($query,$dbconn);
					while($row = mysql_fetch_assoc($results))
					{
						
						{
							if(paginate($selectedpage, $totaltasks))
							{
								$daytasks[] = $row;
								$appcount++;
							}
						}
							$daytaskstotal[] = $row;
							$totaltasks++;
					}
					
					if(sizeof($daytasks) > 0)
					{
				?>
			    <div class="accordion">
					<h4><a href="#">8 Months Ago <div style='float: right;'>(<?php echo sizeof($daytaskstotal); ?>)</div></a></h4>
					<div style="margin: 0 0 0 0; padding: 0 0 0 0;">
					    <?php include_partial('list', array('tasks' => $daytasks)) ?>
					</div>
				</div>
				<?php
				}
				
					$daytasks = null;
					$daytaskstotal = null;
					
					$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.date_created BETWEEN '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*300)."' AND '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*271)."'".$filter;
					$results = mysql_query($query,$dbconn);
					while($row = mysql_fetch_assoc($results))
					{
						
						{
							if(paginate($selectedpage, $totaltasks))
							{
								$daytasks[] = $row;
								$appcount++;
							}
						}
							$daytaskstotal[] = $row;
							$totaltasks++;
					}
					
					if(sizeof($daytasks) > 0)
					{
				?>
			    <div class="accordion">
					<h4><a href="#">9 Months Ago <div style='float: right;'>(<?php echo sizeof($daytaskstotal); ?>)</div></a></h4>
					<div style="margin: 0 0 0 0; padding: 0 0 0 0;">
					    <?php include_partial('list', array('tasks' => $daytasks)) ?>
					</div>
				</div>
				<?php
				}
				
					$daytasks = null;
					$daytaskstotal = null;
					
					$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.date_created BETWEEN '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*330)."' AND '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*301)."'".$filter;
					$results = mysql_query($query,$dbconn);
					while($row = mysql_fetch_assoc($results))
					{
						
						{
							if(paginate($selectedpage, $totaltasks))
							{
								$daytasks[] = $row;
								$appcount++;
							}
						}
							$daytaskstotal[] = $row;
							$totaltasks++;
					}
					
					if(sizeof($daytasks) > 0)
					{
				?>
			    <div class="accordion">
					<h4><a href="#">10 Months Ago <div style='float: right;'>(<?php echo sizeof($daytaskstotal); ?>)</div></a></h4>
					<div style="margin: 0 0 0 0; padding: 0 0 0 0;">
					    <?php include_partial('list', array('tasks' => $daytasks)) ?>
					</div>
				</div>
				<?php
				}
				
					$daytasks = null;
					$daytaskstotal = null;
					
					$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.date_created BETWEEN '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*360)."' AND '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*331)."'".$filter;
					$results = mysql_query($query,$dbconn);
					while($row = mysql_fetch_assoc($results))
					{
						
						{
							if(paginate($selectedpage, $totaltasks))
							{
								$daytasks[] = $row;
								$appcount++;
							}
						}
							$daytaskstotal[] = $row;
							$totaltasks++;
					}
					
					if(sizeof($daytasks) > 0)
					{
				?>
			    <div class="accordion">
					<h4><a href="#">11 Months Ago <div style='float: right;'>(<?php echo sizeof($daytaskstotal); ?>)</div></a></h4>
					<div style="margin: 0 0 0 0; padding: 0 0 0 0;">
					    <?php include_partial('list', array('tasks' => $daytasks)) ?>
					</div>
				</div>
				<?php
				}
				
					$daytasks = null;
					$daytaskstotal = null;
					
					$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.date_created BETWEEN '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*390)."' AND '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*361)."'".$filter;
					$results = mysql_query($query,$dbconn);
					while($row = mysql_fetch_assoc($results))
					{
						
						{
							if(paginate($selectedpage, $totaltasks))
							{
								$daytasks[] = $row;
								$appcount++;
							}
						}
							$daytaskstotal[] = $row;
							$totaltasks++;
					}
					
					if(sizeof($daytasks) > 0)
					{
				?>
			    <div class="accordion">
					<h4><a href="#">12 Months Ago <div style='float: right;'>(<?php echo sizeof($daytaskstotal); ?>)</div></a></h4>
					<div style="margin: 0 0 0 0; padding: 0 0 0 0;">
					    <?php include_partial('list', array('tasks' => $daytasks)) ?>
					</div>
				</div>
				<?php
				}
				
					$daytasks = null;
					$daytaskstotal = null;
						
					$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.date_created BETWEEN '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*757)."' AND '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*391)."'".$filter;
					$results = mysql_query($query,$dbconn);
					while($row = mysql_fetch_assoc($results))
					{
						
						{
							if(paginate($selectedpage, $totaltasks))
							{
								$daytasks[] = $row;
								$appcount++;
							}
						}
							$daytaskstotal[] = $row;
							$totaltasks++;
					}
					
					if(sizeof($daytasks) > 0)
					{
				?>
			    <div class="accordion">
					<h4><a href="#">1 Year Ago <div style='float: right;'>(<?php echo sizeof($daytaskstotal); ?>)</div></a></h4>
					<div style="margin: 0 0 0 0; padding: 0 0 0 0;">
					    <?php include_partial('list', array('tasks' => $daytasks)) ?>
					</div>
				</div>
				<?php
				}
				
				$unit = 758;
				
				for($i = 2; $i < 103; $i++)
				{
				    
					$daytasks = null;
					$daytaskstotal = null;
					
					$query = "SELECT a.id AS id FROM task a LEFT OUTER JOIN form_entry b ON a.application_id = a.id LEFT OUTER JOIN cf_user c ON a.owner_user_id = c.nid WHERE a.date_created BETWEEN '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*($unit+366))."' AND '".date('Y-m-d', strtotime(date('Y-m-d')) - 86400*$unit)."'".$filter;
					$results = mysql_query($query,$dbconn);
					while($row = mysql_fetch_assoc($results))
					{
						
						{
							if(paginate($selectedpage, $totaltasks))
							{
								$daytasks[] = $row;
								$appcount++;
							}
						}
							$daytaskstotal[] = $row;
							$totaltasks++;
					}
					
					if(sizeof($daytasks) > 0)
					{
						?>
						<div class="accordion">
							<h4><a href="#"><?php echo $i; ?> Years Ago <div style='float: right;'>(<?php echo sizeof($daytaskstotal); ?>)</div></a></h4>
							<div style="margin: 0 0 0 0; padding: 0 0 0 0;">
								<?php include_partial('list', array('tasks' => $daytasks)) ?>
							</div>
						</div>
						<?php
					}
					?>
					
					<?php
					$unit = $unit + 367;
				}
				?>
				
				<?php
				if($totaltasks == 0)
				{
				?>
				<table>
					<tfoot>
				   <tr><td colspan='8' style='text-align: left; padding: 10px; font-size: 16px;'>
				   <div align='center'>No Tasks Found.</div>
				   </td></tr>
				   </tfoot>
				</table>
				<?php
				}
				?>
				<table>
					<tfoot>
				   <tr><td colspan='8' style='text-align: left;'>
				   <select id='batch_action' name='batch_action' onChange="if(this.value != ''){if(confirm('Are you sure?')){ batch('tasks', this.options[this.selectedIndex].text, this.value); document.getElementById('default').selected='selected'; }}">
				   <option id='default' value=''>Choose an action..</option>
				   <option value='complete'>Mark As Completed</option>
				   <option value='cancel'>Mark As Cancelled</option>
				   </select>
				   </td></tr>
				   </tfoot>
				</table>
				<br>
				
				<div align='center'>
				<?php
					$pages = round($totaltasks/20);
					
					if($totaltasks%20 > 0)
					{
					    $pages++;
					}
					
					if($pages > 0)
					{
						?>
					    <button OnClick="window.location='<?php echo public_path(); ?>backend.php/tasks/alltasks?<?php if(!empty($filterstatus)){ echo '&filterstatus='.$filterstatus; } ?><?php if(!empty($filterpriority)){ echo '&filterpriority='.$filterpriority; } ?><?php if(!empty($filterdepartment)){ echo '&filterdepartment='.$filterdepartment; } ?><?php if(!empty($filterreviewer)){ echo '&filterreviewer='.$filterreviewer; } ?>';">First</button>
						<?php
						
						if($selectedpage > 1)
						{
							echo ".... ";
							?>
							<button OnClick="window.location='<?php echo public_path(); ?>backend.php/tasks/alltasks?page=<?php echo ($selectedpage-1); ?><?php if(!empty($filterstatus)){ echo '&filterstatus='.$filterstatus; } ?><?php if(!empty($filterpriority)){ echo '&filterpriority='.$filterpriority; } ?><?php if(!empty($filterdepartment)){ echo '&filterdepartment='.$filterdepartment; } ?><?php if(!empty($filterreviewer)){ echo '&filterreviewer='.$filterreviewer; } ?>';"><?php echo ($selectedpage-1); ?></button> 
							<?php
						}
						
						$count = 0;
						
						for($page = $selectedpage; $page <= $pages; $page++)
						{	
							$count++;
							
							$colour = "";
							
							if($selectedpage == $page)
							{
								$colour = "class='blue'";
							}
							?>
						    <button <?php echo $colour; ?> OnClick="window.location='<?php echo public_path(); ?>backend.php/tasks/alltasks?page=<?php echo $page; ?><?php if(!empty($filterstatus)){ echo '&filterstatus='.$filterstatus; } ?><?php if(!empty($filterpriority)){ echo '&filterpriority='.$filterpriority; } ?><?php if(!empty($filterdepartment)){ echo '&filterdepartment='.$filterdepartment; } ?><?php if(!empty($filterreviewer)){ echo '&filterreviewer='.$filterreviewer; } ?>';"><?php echo $page ?></button>
							<?php
							if($count == 10)
							{
								echo "....";
								break;
							}
						}	
						?>
					    <button OnClick="window.location='<?php echo public_path(); ?>backend.php/tasks/alltasks?page=<?php echo $pages; ?><?php if(!empty($filterstatus)){ echo '&filterstatus='.$filterstatus; } ?><?php if(!empty($filterpriority)){ echo '&filterpriority='.$filterpriority; } ?><?php if(!empty($filterdepartment)){ echo '&filterdepartment='.$filterdepartment; } ?><?php if(!empty($filterreviewer)){ echo '&filterreviewer='.$filterreviewer; } ?>';">Last</button>
						<?php
					}
				?>
				</div>
			
		</div>
