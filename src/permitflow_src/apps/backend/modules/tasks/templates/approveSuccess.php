<?php
$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
?>
   <div class="pageheader">
      <h2><i class="fa fa-envelope"></i> Tasks <span>List of tasks assigned to reviewers</span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo public_path(); ?>backend.php/dashboard">Home</a></li>
          <li class="active">Tasks</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel panel-email">

        <div class="row">
            <div class="col-sm-3 col-lg-2">
                <button class="btn btn-danger btn-block btn-compose-email" data-toggle="modal" data-target="#myModal">New Task</button>
                
                <ul class="nav nav-pills nav-stacked nav-email">
                    <li <?php if($sf_context->getActionName() == "list"): ?>class="active"<?php endif; ?>>
                    <a href="<?php echo public_path(); ?>backend.php/tasks/list">
                    	<?php 
                    	$q = Doctrine_Query::create()
                    		->from("Task a")
                    		->where("a.owner_user_id = ?", $_SESSION["SESSION_CUTEFLOW_USERID"])
                    		->andWhere("a.status = ?", 1);
                    	$apps_pending = $q->execute();
                    	?>
                        <span class="badge pull-right"><?php echo sizeof($apps_pending); ?></span>
                        <i class="glyphicon glyphicon-inbox"></i> Inbox
                    </a>
                    </li>
                    <li <?php if($sf_context->getActionName() == "sent"): ?>class="active"<?php endif; ?>>
                    <a href="<?php echo public_path(); ?>backend.php/tasks/sent">
                    	<?php 
                    	$q = Doctrine_Query::create()
                    		->from("Task a")
                    		->where("a.owner_user_id = ?", $_SESSION["SESSION_CUTEFLOW_USERID"])
                    		->andWhere("a.status = ?", 2);
                    	$apps_waiting = $q->execute();
                    	?>
                        <span class="badge pull-right"><?php echo sizeof($apps_waiting); ?></span>
                    <i class="glyphicon glyphicon-send"></i> Outbox</a>
                    </li>
                    <li <?php if($sf_context->getActionName() == "approve"): ?>class="active"<?php endif; ?>>
                    <a href="<?php echo public_path(); ?>backend.php/tasks/approve">
                        <span class="badge pull-right"><?php echo sizeof($tasks); ?></span>
                        <i class="glyphicon glyphicon-pencil"></i> Awaiting Approval
                    </a>
                    </li>
                    <li <?php if($sf_context->getActionName() == "trash"): ?>class="active"<?php endif; ?>><a href="<?php echo public_path(); ?>backend.php/tasks/trash"><i class="glyphicon glyphicon-trash"></i> Trash</a></li>
                </ul>
                
                
                
                
            </div><!-- col-sm-3 -->
            
            <div class="col-sm-9 col-lg-10">
                
             <div class="panel panel-dark">
                       <div class="panel-heading">
                        <h3 class="panel-title">Tasks Awaiting My Approval</h3>
                   <p class="text-muted">Showing <?php echo sizeof($tasks); ?> tasks</p>
                    </div>
           <div class="panel-body panel-body-nopadding">
                        
                       <div class="table-responsive">
                                <table class="table dt-on-steroids mb0" id="table2">
                                <thead>
                                    <th class="no-sort"></th>
                                    <th class="no-sort"></th>
                                    <th>Application No.</th>
                                    <th>Task</th>
                                    <th>Status</th>
                                     <th>Priority</th>
                                    <th>Date Created</th>
                                    <th class="no-sort" width="5%">Actions</th>
                                </thead>
                              <tbody>
                                <?php foreach ($tasks as $task): ?>
								<?php include_partial('list', array('task' => $task)) ?>
								<?php endforeach; ?>                              
                          		</tbody>
                            </table>
                        </div><!-- table-responsive -->
                        
                    </div><!-- panel-body -->
                </div><!-- panel -->
                
            </div><!-- col-sm-9 -->
            
        </div><!-- row -->
    
    </div>
    
  </div><!-- mainpanel -->
