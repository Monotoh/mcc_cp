<?php
/**
 * commentSuccess templates.
 *
 * Displays comment sheet for selected task
 *
 * @package    backend
 * @subpackage tasks
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */


try
{

    $prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";

    require($prefix_folder.'includes/init.php');

    require($prefix_folder.'config.php');
    require($prefix_folder.'includes/db-core.php');
    require($prefix_folder.'includes/helper-functions.php');
    require($prefix_folder.'includes/check-session.php');

    require($prefix_folder.'includes/entry-functions.php');
    require($prefix_folder.'includes/post-functions.php');
    require($prefix_folder.'includes/users-functions.php');

    $prefix_folder = dirname(__FILE__)."/../../../../..";

    require_once $prefix_folder.'/lib/vendor/cp_workflow/config/config.inc.php';
	require_once $prefix_folder.'/lib/vendor/cp_workflow/language_files/language.inc.php';
	require_once $prefix_folder.'/lib/vendor/cp_workflow/pages/CCirculation-comment.inc.php';
	require_once $prefix_folder.'/lib/vendor/cp_workflow/pages/version.inc.php';

	
	
	$form_id  = $application->getFormId();
	$entry_id = $application->getEntryId();



    $nav = trim($_GET['nav']);

    if(empty($form_id) || empty($entry_id)){
        die("Invalid Request");
    }

    $dbh = mf_connect_db();
    $mf_settings = mf_get_settings($dbh);

    //check permission, is the user allowed to access this page?
    if(empty($_SESSION['mf_user_privileges']['priv_administer'])){
        $user_perms = mf_get_user_permissions($dbh,$form_id,$_SESSION['mf_user_id']);

        //this page need edit_entries or view_entries permission
        if(empty($user_perms['edit_entries']) && empty($user_perms['view_entries'])){
            $_SESSION['MF_DENIED'] = "You don't have permission to access this page.";

            $ssl_suffix = mf_get_ssl_suffix();
            header("Location: http{$ssl_suffix}://".$_SERVER['HTTP_HOST'].mf_get_dirname($_SERVER['PHP_SELF'])."/restricted.php");
            exit;
        }
    }

    //if there is "nav" parameter, we need to determine the correct entry id and override the existing entry_id
    if(!empty($nav)){
        $all_entry_id_array = mf_get_filtered_entries_ids($dbh,$form_id);
        $entry_key = array_keys($all_entry_id_array,$entry_id);
        $entry_key = $entry_key[0];

        if($nav == 'prev'){
            $entry_key--;
        }else{
            $entry_key++;
        }

        $entry_id = $all_entry_id_array[$entry_key];

        //if there is no entry_id, fetch the first/last member of the array
        if(empty($entry_id)){
            if($nav == 'prev'){
                $entry_id = array_pop($all_entry_id_array);
            }else{
                $entry_id = $all_entry_id_array[0];
            }
        }
    }

    //get form name
    $query 	= "select
					 form_name,
					 payment_enable_merchant,
					 payment_merchant_type,
					 payment_price_type,
					 payment_price_amount,
					 payment_currency,
					 payment_ask_billing,
					 payment_ask_shipping,
					 payment_enable_tax,
					 payment_tax_rate
			     from
			     	 ".MF_TABLE_PREFIX."forms
			    where
			    	 form_id = ?";
    $params = array($form_id);

    $sth = mf_do_query($query,$params,$dbh);
    $row = mf_do_fetch_result($sth);

    if(!empty($row)){
        $form_name = htmlspecialchars($row['form_name']);
        $payment_enable_merchant = (int) $row['payment_enable_merchant'];
        if($payment_enable_merchant < 1){
            $payment_enable_merchant = 0;
        }

        $payment_price_amount = (double) $row['payment_price_amount'];
        $payment_merchant_type = $row['payment_merchant_type'];
        $payment_price_type = $row['payment_price_type'];
        $form_payment_currency = strtoupper($row['payment_currency']);
        $payment_ask_billing = (int) $row['payment_ask_billing'];
        $payment_ask_shipping = (int) $row['payment_ask_shipping'];

        $payment_enable_tax = (int) $row['payment_enable_tax'];
        $payment_tax_rate 	= (float) $row['payment_tax_rate'];
    }

    //if payment enabled, get the details
    if(!empty($payment_enable_merchant)){
        $query = "SELECT
						`payment_id`,
						 date_format(payment_date,'%e %b %Y - %r') payment_date,
						`payment_status`,
						`payment_fullname`,
						`payment_amount`,
						`payment_currency`,
						`payment_test_mode`,
						`payment_merchant_type`,
						`status`,
						`billing_street`,
						`billing_city`,
						`billing_state`,
						`billing_zipcode`,
						`billing_country`,
						`same_shipping_address`,
						`shipping_street`,
						`shipping_city`,
						`shipping_state`,
						`shipping_zipcode`,
						`shipping_country`
					FROM
						".MF_TABLE_PREFIX."form_payments
				   WHERE
				   		form_id = ? and record_id = ? and `status` = 1
				ORDER BY
						payment_date DESC
				   LIMIT 1";
        $params = array($form_id,$entry_id);

        $sth = mf_do_query($query,$params,$dbh);
        $row = mf_do_fetch_result($sth);

        $payment_id 		= $row['payment_id'];
        $payment_date 		= $row['payment_date'];
        $payment_status 	= $row['payment_status'];
        $payment_fullname 	= $row['payment_fullname'];
        $payment_amount 	= (double) $row['payment_amount'];
        $payment_currency 	= strtoupper($row['payment_currency']);
        $payment_test_mode 	= (int) $row['payment_test_mode'];
        $payment_merchant_type = $row['payment_merchant_type'];
        $billing_street 	= htmlspecialchars(trim($row['billing_street']));
        $billing_city 		= htmlspecialchars(trim($row['billing_city']));
        $billing_state 		= htmlspecialchars(trim($row['billing_state']));
        $billing_zipcode 	= htmlspecialchars(trim($row['billing_zipcode']));
        $billing_country 	= htmlspecialchars(trim($row['billing_country']));

        $same_shipping_address = (int) $row['same_shipping_address'];

        if(!empty($same_shipping_address)){
            $shipping_street 	= $billing_street;
            $shipping_city		= $billing_city;
            $shipping_state		= $billing_state;
            $shipping_zipcode	= $billing_zipcode;
            $shipping_country	= $billing_country;
        }else{
            $shipping_street 	= htmlspecialchars(trim($row['shipping_street']));
            $shipping_city 		= htmlspecialchars(trim($row['shipping_city']));
            $shipping_state 	= htmlspecialchars(trim($row['shipping_state']));
            $shipping_zipcode 	= htmlspecialchars(trim($row['shipping_zipcode']));
            $shipping_country 	= htmlspecialchars(trim($row['shipping_country']));
        }

        if(!empty($billing_street) || !empty($billing_city) || !empty($billing_state) || !empty($billing_zipcode) || !empty($billing_country)){
            $billing_address  = "{$billing_street}<br />{$billing_city}, {$billing_state} {$billing_zipcode}<br />{$billing_country}";
        }

        if(!empty($shipping_street) || !empty($shipping_city) || !empty($shipping_state) || !empty($shipping_zipcode) || !empty($shipping_country)){
            $shipping_address = "{$shipping_street}<br />{$shipping_city}, {$shipping_state} {$shipping_zipcode}<br />{$shipping_country}";
        }

        if(!empty($row)){
            $payment_has_record = true;

            if(empty($payment_id)){
                //if the payment has record but has no payment id, then the record was being inserted manually (the payment status was being set manually by user)
                //in this case, we consider this record empty
                $payment_has_record = false;
            }
        }else{
            //if the entry doesn't have any record within ap_form_payments table
            //we need to calculate the total amount
            $payment_has_record = false;
            $payment_status = "unpaid";

            if($payment_price_type == 'variable'){
                $payment_amount = (double) mf_get_payment_total($dbh,$form_id,$entry_id,0,'live');
            }else if($payment_price_type == 'fixed'){
                $payment_amount = $payment_price_amount;
            }

            //calculate tax if enabled
            if(!empty($payment_enable_tax) && !empty($payment_tax_rate)){
                $payment_tax_amount = ($payment_tax_rate / 100) * $payment_amount;
                $payment_tax_amount = round($payment_tax_amount,2); //round to 2 digits decimal
                $payment_amount += $payment_tax_amount;
            }

            $payment_currency = $form_payment_currency;
        }

        switch ($payment_currency) {
            case 'USD' : $currency_symbol = '&#36;';break;
            case 'EUR' : $currency_symbol = '&#8364;';break;
            case 'GBP' : $currency_symbol = '&#163;';break;
            case 'AUD' : $currency_symbol = '&#36;';break;
            case 'CAD' : $currency_symbol = '&#36;';break;
            case 'JPY' : $currency_symbol = '&#165;';break;
            case 'THB' : $currency_symbol = '&#3647;';break;
            case 'HUF' : $currency_symbol = '&#70;&#116;';break;
            case 'CHF' : $currency_symbol = 'CHF';break;
            case 'CZK' : $currency_symbol = '&#75;&#269;';break;
            case 'SEK' : $currency_symbol = 'kr';break;
            case 'DKK' : $currency_symbol = 'kr';break;
            case 'PHP' : $currency_symbol = '&#36;';break;
            case 'MYR' : $currency_symbol = 'RM';break;
            case 'PLN' : $currency_symbol = '&#122;&#322;';break;
            case 'BRL' : $currency_symbol = 'R&#36;';break;
            case 'HKD' : $currency_symbol = '&#36;';break;
            case 'MXN' : $currency_symbol = 'Mex&#36;';break;
            case 'TWD' : $currency_symbol = 'NT&#36;';break;
            case 'TRY' : $currency_symbol = 'TL';break;
            case 'NZD' : $currency_symbol = '&#36;';break;
            case 'SGD' : $currency_symbol = '&#36;';break;
            default: $currency_symbol = ''; break;
        }
    }


    //get entry details for particular entry_id
    $param['checkbox_image'] = 'images/icons/59_blue_16.png';
    $entry_details = mf_get_entry_details($dbh,$form_id,$entry_id,$param);
    $document_details = mf_get_attachments_details($dbh,$form_id,$entry_id,$param);

    //get entry information (date created/updated/ip address)
    $query = "select
					date_format(date_created,'%e %b %Y - %r') date_created,
					date_format(date_updated,'%e %b %Y - %r') date_updated,
					ip_address
				from
					`".MF_TABLE_PREFIX."form_{$form_id}`
			where id=?";
    $params = array($entry_id);

    $sth = mf_do_query($query,$params,$dbh);
    $row = mf_do_fetch_result($sth);

    $date_created = $row['date_created'];
    if(!empty($row['date_updated'])){
        $date_updated = $row['date_updated'];
    }else{
        $date_updated = '&nbsp;';
    }
    $ip_address   = $row['ip_address'];

    //check for any 'signature' field, if there is any, we need to include the javascript library to display the signature
    $query = "select
					count(form_id) total_signature_field
				from
					".MF_TABLE_PREFIX."form_elements
			   where
			   		element_type = 'signature' and
			   		element_status=1 and
			   		form_id=?";
    $params = array($form_id);
	
	function replaceLinks($value) {
		$linktext = preg_replace('/(([a-zA-Z]+:\/\/)([a-zA-Z0-9?&%.;:\/=+_-]*))/i', "<a href=\"$1\" target=\"_blank\">$1</a>", $value);
		return $linktext;
	}
	
	
	$textareas = "";
											
?>
<script type="text/javascript" src="/assets_backend/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/assets_backend/js/ckfinder/ckfinder.js"></script>
<script src="/assets_backend/js/ckeditor/_samples/sample.js" type="text/javascript"></script>
<div class="g12" style="padding-left: 4px; padding-top: 4px; margin-bottom: 0px; padding-bottom: 0px;">
<form autocomplete="off" data-ajax="false" style=" padding-top: 4px;">
<label><?php echo $application->getApplicationId(); ?> (<?php
				$q = Doctrine_Query::create()
					 ->from('SubMenus a')
					 ->where('a.id = ?', $application->getApproved());
				$submenu = $q->fetchOne();
				if($submenu)
				{
					echo $submenu->getTitle();
				}
			?>)

<div style="float: right; margin-top: -12px;">
							<button style="height: 34px; font-size: 12px;" onClick="window.location='<?php echo public_path(); ?>backend.php/tasks/pending';">Back To Tasks</button>
</div>
</label>
</form>			
			<div class="tab">
					<ul>
						<li><a href="#tabs-1">Comment Sheet</a></li>
						<li><a href="#tabs-2">Application Details</a></li>
						<li><a href="#tabs-3">Attachments</a></li>
						<li><a href="#tabs-4">Submitted By</a></li>
						<li><a href="#tabs-5">Other Comments</a></li>
					</ul>
					<div id="tabs-1">
							<form method="post" action="<?php echo public_path(); ?>backend.php/tasks/savecommentsheet" id="MailContentForm" name="MailContentForm" onSubmit="return validate_editfield();" autocomplete="off" data-ajax="false">
								<fieldset>
								<label>
								<?php
								$q = Doctrine_Query::create()
									 ->from('CfUser a')
									 ->where('a.nid = ?', $_SESSION["SESSION_CUTEFLOW_USERID"]);
								$reviewer = $q->fetchOne();
								
								
								if($ext_slot)
								{
								    //echo "Here 1";
									$q = Doctrine_Query::create()
										 ->from('CfFormslot a')
										 ->where('a.strname LIKE ?', "%".$reviewer->getStrdepartment()." - ".$ext_slot."%");
									$slot = $q->fetchOne();
									

								}else
								{
									$q = Doctrine_Query::create()
										 ->from('CfFormslot a')
										 ->where('a.strname = ?', $reviewer->getStrdepartment())
										 ->andWhere('a.nsendtype = ?', $form_id)
										 ->andWhere('a.strname <> ? OR a.strname <> ? OR a.strname <> ? OR a.strname <> ?', array($reviewer->getStrdepartment()."",$reviewer->getStrdepartment()." - Inspection",$reviewer->getStrdepartment()." - Scanning",$reviewer->getStrdepartment()." - Collection"));
			
									$slot = $q->fetchOne();
									
								}
								
								if(empty($slot))
								{
									$q = Doctrine_Query::create()
										 ->from('CfFormslot a')
										 ->where('a.strname LIKE ?', '%'.$reviewer->getStrdepartment().'%')
										 ->andWhere('a.nsendtype = ?', $form_id);
									$slot = $q->fetchOne();
								}
								
								if(empty($slot))
								{
									$q = Doctrine_Query::create()
										 ->from('CfFormslot a')
										 ->where('a.strname LIKE ?', '%'.$reviewer->getStrdepartment().'%')
										 ->orderBy("a.nid DESC");
									$slot = $q->fetchOne();
								}
								echo $slot->getStrname();
								
								
								
								$q = Doctrine_Query::create()
									 ->from('CfCirculationprocess a')
									 ->where('a.nuserid = ?', $_SESSION["SESSION_CUTEFLOW_USERID"])
									 ->andWhere('a.ncirculationformid = ?', $application->getCirculationId())
									 ->andWhere('a.ndecissionstate = ?', '0');
								$process = $q->fetchOne();
								
								
								
								$q = Doctrine_Query::create()
									 ->from('CfCirculationform a')
									 ->where('a.nid = ?', $application->getCirculationId());
								$circulation = $q->fetchOne();
								
								
								if($process)
								{
								
								}
								else
								{
									$process = new CfCirculationprocess();
									$process->setNcirculationformid($application->getCirculationId());
									$process->setNslotid($slot->getNid());
									$process->setNuserid($_SESSION["SESSION_CUTEFLOW_USERID"]);
									$process->setDateinprocesssince("0");
									$process->setNdecissionstate("0");
									$process->setDatedecission("0");
									$process->setNissubstitiuteof("0");
									$process->setNcirculationhistoryid($application->getCirculationId());
									$process->setNresendcount("0");
									$process->save();
								}
								
								?>
								</label>
								
								<?php
									$objMyCirculation = new CCirculation();
									
									//--- open database
									$nConnection = mysql_connect($DATABASE_HOST, $DATABASE_UID, $DATABASE_PWD);
									if ($nConnection)
									{
										if (mysql_select_db($DATABASE_DB, $nConnection))
										{
											//-----------------------------------------------
											//--- get the user information from 
											//--- cf_circulationprocess
											//-----------------------------------------------
											$strQuery = "SELECT * FROM cf_circulationprocess WHERE nid=".$process->getNid();
											$nResult = mysql_query($strQuery, $nConnection);
											if ($nResult)
											{
												if (mysql_num_rows($nResult) > 0)
												{
													$arrCirculationProcess = mysql_fetch_array($nResult);				
												}
											}
											
											//-----------------------------------------------
											//--- get the single circulation form
											//-----------------------------------------------
											$query = "select * from cf_circulationform WHERE nid=".$arrCirculationProcess["ncirculationformid"];
											$nResult = mysql_query($query, $nConnection);
											if ($nResult)
											{
												if (mysql_num_rows($nResult) > 0)
												{
													$arrCirculationForm = mysql_fetch_array($nResult);				
												}
											}
											
											//-----------------------------------------------
											//--- get the single circulation history
											//-----------------------------------------------
											$query = "select * from cf_circulationhistory WHERE nid=".$arrCirculationProcess["ncirculationhistoryid"];
											$nResult = mysql_query($query, $nConnection);
											if ($nResult)
											{
												if (mysql_num_rows($nResult) > 0)
												{
													$arrCirculationHistory = mysql_fetch_array($nResult);
												}
											}
											
											//-----------------------------------------------
											//--- get all users
											//-----------------------------------------------
											$arrUsers = array();
											$strQuery = "SELECT * FROM cf_user WHERE bdeleted <> 1";
											$nResult = mysql_query($strQuery, $nConnection);
											if ($nResult)
											{
												if (mysql_num_rows($nResult) > 0)
												{
													while (	$arrRow = mysql_fetch_array($nResult))
													{
														$arrUsers[$arrRow["nid"]] = $arrRow;
													}
												}
											}
											
											//-----------------------------------------------
											//--- get the template id
											//-----------------------------------------------
											$strQuery = "SELECT ntemplateid FROM cf_formslot WHERE nid=".$arrCirculationProcess["nslotid"];
											$nResult = mysql_query($strQuery, $nConnection);
											if ($nResult)
											{
												if (mysql_num_rows($nResult) > 0)
												{
													$arrRow = mysql_fetch_array($nResult);
													$templateid = $arrRow["ntemplateid"];
												}
											}
											
											//-----------------------------------------------
											//--- get the form slots
											//-----------------------------------------------	            
											$arrSlots = array();
											$strQuery = "SELECT * FROM cf_formslot WHERE strname = '".$slot->getStrname()."' ORDER BY nslotnumber ASC";
											$nResult = mysql_query($strQuery, $nConnection);
											if(mysql_num_rows($nResult) == 0)
											{
												$strQuery = "SELECT * FROM cf_formslot WHERE nsendtype = ".$form_id." AND strname = '".$reviewer->getStrdepartment()."' ORDER BY nslotnumber ASC";
												
												$nResult = mysql_query($strQuery, $nConnection);
											}
											if ($nResult)
											{
												if (mysql_num_rows($nResult) > 0)
												{
													while (	$arrRow = mysql_fetch_array($nResult))
													{
													
														$q = Doctrine_Query::create()
																   ->from('cfUser a')
																   ->where('a.nid = ?', $_SESSION['SESSION_CUTEFLOW_USERID']);
														 $cf_user = $q->fetchOne();
														 $department_name = $cf_user->getStrdepartment();
														 
														 $pos = strpos($arrRow['strname'], $department_name);
															 
															if($pos === false)
															{
																
															}
															else
															{     
																$arrSlots[$arrRow["nid"]] = $arrRow;
															}
													}
												}
											}
											
											//-----------------------------------------------
											//--- get the field values
											//-----------------------------------------------	            
											$arrValues = array();
											$strQuery = "SELECT * FROM cf_fieldvalue WHERE nformid=".$application->getCirculationId();
											$nResult = mysql_query($strQuery, $nConnection);
											if ($nResult)
											{
												if (mysql_num_rows($nResult) > 0)
												{
													while (	$arrRow = mysql_fetch_array($nResult))
													{
														$arrValues[$arrRow["ninputfieldid"]."_".$arrRow["nslotid"]."_".$arrRow["nformid"]] = $arrRow;
													}
												}
											}
										}
									}
									
								
									$strSearchBrowserInfo = $_SERVER['HTTP_USER_AGENT'];
									$bThunderbird 	= substr_count($strSearchBrowserInfo, 'Thunderbird');
									?>
									
									<input type="hidden" name="Answer" id="Answer" value="true">
														<?php
														if (sizeof($arrSlots) != 0)
														{
													?>
																<table border="0" width="100%" cellpadding="0" cellspacing="0" class="BorderSilver" style="background-color:White; margin-top: 15px; text-align: left;">
																	<?php
																		$nConnection = mysql_connect($DATABASE_HOST, $DATABASE_UID, $DATABASE_PWD);
																		if ($nConnection)
																		{
																			if (mysql_select_db($DATABASE_DB, $nConnection))
																			{
																				$arrSlotsToShow = array();
																				
																				$index = 1;
																				foreach ($arrSlots as $arrSlot) {
																					if ($SLOT_VISIBILITY == 'SINGLE') {
																						if ($arrSlot["nid"] == $arrCirculationProcess["nslotid"]) {
																							$arrSlotsToShow[] = $arrSlot;
																						}
																					}
																					else if ($SLOT_VISIBILITY == 'TOP') {
																						if ($arrSlot["nid"] != $arrCirculationProcess["nslotid"]) {
																							$arrSlotsToShow[$index] = $arrSlot;
																							$index++;
																						}
																						else {
																							$arrSlotsToShow[0] = $arrSlot;
																						}
																					}
																					else { // ALL
																						$arrSlotsToShow[] = $arrSlot;
																					}
																				}
																				
																				
																				for ($i = 0; $i < count($arrSlotsToShow); $i++)
																				{
																					$arrSlot = $arrSlotsToShow[$i];
																					?>
																						<tr>        
																							<td>
																								<?php 
																									$background = ""; 
																									if ($arrSlot["nid"] == $arrCirculationProcess["nslotid"]) {
																										$background = "style='background-color: #ffe88e'";
																									}
																								?>
																								<table width="100%" <?php echo $background;?>>
																								<tr>
																								<?php
																									$strQuery = "SELECT * FROM cf_inputfield INNER JOIN cf_slottofield ON cf_inputfield.nid = cf_slottofield.nfieldid WHERE cf_slottofield.nslotid = ".$arrSlot["nid"]."  ORDER BY cf_slottofield.nposition ASC";
																									$nResult = mysql_query($strQuery, $nConnection);
																									if ($nResult)
																									{
																										if (mysql_num_rows($nResult) > 0)
																										{
																											$nRunningCounter = 1;
																											while (	$arrRow = mysql_fetch_array($nResult))
																											{
																												if($arrRow["ntype"] == "10")
																												{
																													echo "<td width=\"100%\" valign=\"top\" align=\"left\"><h2 style=' text-align: left; margin: 0px; padding: 5px; font-size: 17px; font-family:\"Palatino Linotype\", \"Book Antiqua\", Palatino, serif;'>".htmlentities($arrRow["strname"])."</h2>";
																												}
																												else if($arrRow["ntype"] == "11")
																												{
																													echo "<td width=\"100%\" valign=\"top\" align=\"left\"><h3 style=' text-align: left;margin: 0px; padding: 5px; font-size: 16px; font-family:\"Palatino Linotype\", \"Book Antiqua\", Palatino, serif;'>".htmlentities($arrRow["strname"])."</h3>";
																												}
																												else
																												{
																													echo "<td width=\"100%\" valign=\"top\" align=\"left\" style=' text-align: left;background-color: #CCCCCC; border: 1px solid silver; padding: 5px;'><div style='margin-bottom: -2px; font-size: 14px; font-family:\"Palatino Linotype\", \"Book Antiqua\", Palatino, serif;'><b>".htmlentities($arrRow["strname"])."</b></div><br>";
																												}
																												$bReadOnly = "";
																												
																												$keyId = $arrRow["nfieldid"]."_".$arrSlot["nid"]."_".$arrCirculationProcess["ncirculationformid"];
																												
																												
																												if ($arrRow["ntype"] == 1)
																												{
																													if ( ($arrSlot["nid"] == $arrCirculationProcess["nslotid"]) &&
																														 ($arrCirculationProcess["ndecissionstate"] == 0) && (!$bReadOnly) )
																													{
																														//--- Slot is allowed to edit
																														if ($arrValues[$keyId]["strfieldvalue"]!='')
																														{																							
																															$arrValue = split('rrrrr',$arrValues[$keyId]["strfieldvalue"]);
																
																															$strFieldValue 	= $arrValue[0];																					
																															$REG_Text		= $arrValue[1];
																															
																															$bgStyle = $arrRow['strbgcolor'] != "" ? 'background-color: #'.$arrRow['strbgcolor'] : '';
																															echo "<input style=\"width:220px; $bgStyle\" class=\"InputText\" type=\"text\" name=\"".$keyId.'_1'."\" id=\"".$keyId.'_1'."\" value=\"".$strFieldValue."\"  onfocus=\"this.value = remove_underscore(this.value);\">";
																															?>
																															<script language="javascript">
																															addID('<?php echo $keyId."zz1zz".$REG_Text; ?>');
																															</script>
																															<?php
																														}
																														else
																														{
																															$arrValue = split('rrrrr',$arrRow['3']);
																
																															$strFieldValue 	= $arrValue[0];																					
																															$REG_Text		= $arrValue[1];
																															
																															$bgStyle = $arrRow['strbgcolor'] != "" ? 'background-color: #'.$arrRow['strbgcolor'] : '';
																															echo "<input style=\"width:220px; $bgStyle\" class=\"InputText\" type=\"text\" name=\"".$keyId.'_1'."\" id=\"".$keyId.'_1'."\" value=\"".$strFieldValue."\"  onfocus=\"this.value = remove_underscore(this.value); \">";
																															?>
																															<script language="javascript">
																															addID('<?php echo $keyId."zz1zz".$REG_Text; ?>');
																															</script>
																															<?php
																														}
																													}
																													else
																													{
																														if ($arrValues[$keyId]["strfieldvalue"]!='')
																														{
																															$arrValue = split('rrrrr',$arrValues[$keyId]["strfieldvalue"]);
																															$strFieldValue 	= $arrValue[0];		
																															$REG_Text		= $arrValue[1];
																															
																															$bgStyle = $arrRow['strbgcolor'] != "" ? 'background-color: #'.$arrRow['strbgcolor'] : '';
																															echo "<input style=\"width:220px; $bgStyle\" class=\"InputText\" type=\"text\" name=\"".$keyId.'_1'."\" id=\"".$keyId.'_1'."\" value=\"".$strFieldValue."\" >";
																														}
																														else
																														{
																															$arrValue = split('rrrrr',$arrRow['3']);
																															$strFieldValue 	= $arrValue[0];
																															$REG_Text		= $arrValue[1];		
																															
																															$bgStyle = $arrRow['strBgColor'] != "" ? 'background-color: #'.$arrRow['strbgcolor'] : '';
																															echo "<input style=\"width:220px; $bgStyle\" class=\"InputText\" type=\"text\" name=\"".$keyId.'_1'."\" id=\"".$keyId.'_1'."\" value=\"".$strFieldValue."\" >";;
																														}	
																													}
																													if ($REG_Text != '')
																													{
																														?>
																														<input type="hidden" name="<?php echo $keyId.'_REG'; ?>" value="<?php echo $REG_Text; ?>">
																														<?php
																													}
																												}
																												else if ($arrRow["ntype"] == 2)
																												{
																													$MyChecked = 0;
																													if ($arrValues[$keyId]["strfieldvalue"]!='')
																													{
																														if ($arrValues[$keyId]["strfieldvalue"] == 'on')
																														{
																															$MyChecked = 1;
																														}
																													}
																													else
																													{
																														if ($arrRow['3'] == 'on')
																														{
																															$MyChecked = 1;
																														}	
																													}
																													if ( ($arrSlot["nid"] == $arrCirculationProcess["nslotid"])  && ($arrCirculationProcess["ndecissionstate"] == 0) && (!$bReadOnly) )
																													{	//--- Slot is allowed to edit
																													
																														if ($MyChecked)
																														{
																															echo "<input type=\"checkbox\" name=\"".$keyId.'_2'."\" value=\"on\" checked>";
																														}
																														else
																														{
																															echo "<input type=\"checkbox\" name=\"".$keyId.'_2'."\" value=\"on\">";
																														}
																														
																													}
																													else
																													{																						
																														if ($MyChecked)
																														{
																															echo "<input type=\"checkbox\" name=\"".$keyId.'_2'."\" value=\"on\"  checked>";
																															echo "<input type=\"hidden\" name=\"".$keyId.'_2_hidden'."\" value=\"on\">";
																														}
																														else
																														{
																															echo "<input type=\"checkbox\" name=\"".$keyId.'_2'."\" value=\"on\" >";
																														}																						
																													}
																													echo "<input type=\"hidden\" name=\"".$keyId.'_2xx'."\" value=\"\">";
																												}
																												else if ($arrRow["ntype"] == 3)
																												{																					
																													$REGEDIT = 0;
																													if ( ($arrSlot["nid"] == $arrCirculationProcess["nslotid"]) &&
																														 ($arrCirculationProcess["ndecissionstate"] == 0) && (!$bReadOnly)  )
																													{
																														//--- Slot is allowed to edit
																														
																														if ($arrValues[$keyId]["strfieldvalue"]!='')
																														{
																															$arrMyValue = split('xx',$arrValues[$keyId]["strfieldvalue"]);
																															$strMyValue = $arrMyValue['2'];
																								
																															$arrValue3 = split('rrrrr',$strMyValue);
																															$strFieldValue 	= $arrValue3[0];
																															$REG_Number		= $arrValue3[1];
																															
																															$bgStyle = $arrRow['strbgcolor'] != "" ? 'background-color: #'.$arrRow['strbgcolor'] : '';
																															echo "<input class=\"InputText\" style='$bgStyle' type=\"text\" name=\"".$keyId.'_3_'.$arrMyValue['1']."\" id=\"".$keyId.'_3_'.$arrMyValue['1']."\" value=\"".$strFieldValue."\"><br>";
																														}
																														else
																														{
																															$arrMyValue = split('xx',$arrRow['3']);
																															$strMyValue = $arrMyValue['2'];
																								
																															$arrValue3 = split('rrrrr',$strMyValue);
																															$strFieldValue 	= $arrValue3[0];
																															$REG_Number		= $arrValue3[1];
																															
																															$bgStyle = $arrRow['strbgcolor'] != "" ? 'background-color: #'.$arrRow['strbgcolor'] : '';
																															echo "<input class=\"InputText\" style='$bgStyle' type=\"text\" name=\"".$keyId.'_3_'.$arrMyValue['1']."\" id=\"".$keyId.'_3_'.$arrMyValue['1']."\" value=\"".$strFieldValue."\"><br>";
																														}																					
																													}
																													else
																													{
																														if ($arrValues[$keyId]["strfieldvalue"]!='')
																														{
																															$arrMyValue = split('xx',$arrValues[$keyId]["strfieldvalue"]);
																															$strMyValue = $arrMyValue['2'];
																								
																															$arrValue3 = split('rrrrr',$strMyValue);
																															$strFieldValue 	= $arrValue3[0];
																															$REG_Number		= $arrValue3[1];
																															
																															$bgStyle = $arrRow['strbgcolor'] != "" ? 'background-color: #'.$arrRow['strbgcolor'] : '';
																															echo "<input class=\"InputText\" style='$bgStyle' type=\"text\" name=\"".$keyId.'_3_'.$arrMyValue['1']."\" id=\"".$keyId.'_3_'.$arrMyValue['1']."\" value=\"".$strFieldValue."\" ><br>";
																														}
																														else
																														{
																															$arrMyValue = split('xx',$arrRow['3']);
																															$strMyValue = $arrMyValue['2'];
																								
																															$arrValue3 = split('rrrrr',$strMyValue);
																															$strFieldValue 	= $arrValue3[0];
																															$REG_Number		= $arrValue3[1];

																															$bgStyle = $arrRow['strbgcolor'] != "" ? 'background-color: #'.$arrRow['strbgcolor'] : '';
																															echo "<input class=\"InputText\" style='$bgStyle' type=\"text\" name=\"".$keyId.'_3_'.$arrMyValue['1']."\" id=\"".$keyId.'_3_'.$arrMyValue['1']."\" value=\"".$strFieldValue."\" ><br>";
																														}	
																													}
																													switch ($arrMyValue['1'])
																													{
																														case '0':
																															echo "($FIELD_NUMTYPE_NOREGEX)";
																															?>
																															<script language="javascript">
																															addID('<?php echo $keyId."zz3_0"; ?>');
																															</script>
																															<?php
																															break;
																														case '1':
																															echo "($FIELD_NUMTYPE_POSITIVE)";
																															?>
																															<script language="javascript">
																															addID('<?php echo $keyId."zz3_1"; ?>');
																															</script>
																															<?php
																															break;
																														case '2':
																															echo "($FIELD_NUMTYPE_NEGATIVE)";
																															?>
																															<script language="javascript">
																															addID('<?php echo $keyId."zz3_2"; ?>');
																															</script>
																															<?php
																															break;
																														case '3':
																															?>
																															<script language="javascript">
																															addID('<?php echo $keyId."zz3_3zz".$REG_Number; ?>');
																															</script>
																															<?php
																															break;
																													}
																													if ($REG_Number != '')
																													{
																														?>
																														<input type="hidden" name="<?php echo $keyId.'_REG'; ?>" value="<?php echo $REG_Number; ?>">
																														<?php
																													}
																												}
																												else if ($arrRow["ntype"] == 4)
																												{																					
																													if ( ($arrSlot["nid"] == $arrCirculationProcess["nslotid"]) &&
																														 ($arrCirculationProcess["ndecissionstate"] == 0) && (!$bReadOnly)  )
																													{
																														//--- Slot is allowed to edit
																														if ($arrValues[$keyId]["strfieldvalue"]!='')
																														{
																															$arrMyValue = split('xx',$arrValues[$keyId]["strfieldvalue"]);
																															$strMyValue = $arrMyValue['2'];																					
																															$arrValue3 = split('rrrrr',$strMyValue);																						
																															$strFieldValue 	= $arrValue3[0];
																															$REG_Date		= $arrValue3[1];
																															
																															$bgStyle = $arrRow['strbgcolor'] != "" ? 'background-color: #'.$arrRow['strbgcolor'] : '';
																															echo "<input class=\"InputText\" style='$bgStyle' type=\"text\" name=\"".$keyId.'_4_'.$arrMyValue['1']."\" id=\"".$keyId.'_4_'.$arrMyValue['1']."\" value=\"".$strFieldValue."\"><br>";
																															
																														}
																														else
																														{
																															$arrMyValue = split('xx',$arrRow['3']);
																															$strMyValue = $arrMyValue['2'];																					
																															$arrValue3 = split('rrrrr',$strMyValue);																						
																															$strFieldValue 	= $arrValue3[0];
																															$REG_Date		= $arrValue3[1];
																															
																															$bgStyle = $arrRow['strbgcolor'] != "" ? 'background-color: #'.$arrRow['strbgcolor'] : '';
																															echo "<input class=\"InputText\" style='$bgStyle' type=\"text\" name=\"".$keyId.'_4_'.$arrMyValue['1']."\" id=\"".$keyId.'_4_'.$arrMyValue['1']."\" value=\"".$strFieldValue."\"><br>";
																															
																														}
																													}
																													else
																													{
																														if ($arrValues[$keyId]["strfieldvalue"]!='')
																														{
																															$arrMyValue = split('xx',$arrValues[$keyId]["strfieldvalue"]);
																															$strMyValue = $arrMyValue['2'];																					
																															$arrValue3 = split('rrrrr',$strMyValue);																						
																															$strFieldValue 	= $arrValue3[0];
																															$REG_Date		= $arrValue3[1];
																															
																															$bgStyle = $arrRow['strbgcolor'] != "" ? 'background-color: #'.$arrRow['strbgcolor'] : '';
																															echo "<input class=\"InputText\" style='$bgStyle' type=\"text\" name=\"".$keyId.'_4_'.$arrMyValue['1']."\" id=\"".$keyId.'_4_'.$arrMyValue['1']."\" value=\"".$strFieldValue."\" ><br>";
																														}
																														else
																														{
																															$arrMyValue = split('xx',$arrRow['3']);
																															$strMyValue = $arrMyValue['2'];																					
																															$arrValue3 = split('rrrrr',$strMyValue);																						
																															$strFieldValue 	= $arrValue3[0];
																															$REG_Date		= $arrValue3[1];
																															
																															$bgStyle = $arrRow['strbgcolor'] != "" ? 'background-color: #'.$arrRow['strbgcolor'] : '';
																															echo "<input class=\"InputText\" style='$bgColor' type=\"text\" name=\"".$keyId.'_4_'.$arrMyValue['1']."\" id=\"".$keyId.'_4_'.$arrMyValue['1']."\" value=\"".$strFieldValue."\" ><br>";
																														}																							
																													}
																													switch ($arrMyValue['1'])
																													{
																														case '0':
																															?>
																															<script language="javascript">
																															addID('<?php echo $keyId."zz4_0zz".$REG_Date; ?>');
																															</script>
																															<?php
																															break;
																														case '1':
																															echo "(dd-mm-yyyy)";
																															?>
																															<script language="javascript">
																															addID('<?php echo $keyId."zz4_1"; ?>');
																															</script>
																															<?php
																															break;
																														case '2':
																															echo "(mm-dd-yyyy)";
																															?>
																															<script language="javascript">
																															addID('<?php echo $keyId."zz4_2"; ?>');
																															</script>
																															<?php
																															break;
																														case '3':
																															echo "(yyyy-mm-dd)";
																															?>
																															<script language="javascript">
																															addID('<?php echo $keyId."zz4_3"; ?>');
																															</script>
																															<?php
																															break;
																													}
																													if ($REG_Date != '')
																													{
																														?>
																														<input type="hidden" name="<?php echo $keyId.'_REG'; ?>" value="<?php echo $REG_Date; ?>">
																														<?php
																													}
																												}
																												else if ($arrRow["ntype"] == 5)
																												{
																													if ( ($arrSlot["nid"] == $arrCirculationProcess["nslotid"]) &&
																														 ($arrCirculationProcess["ndecissionstate"] == 0) && (!$bReadOnly)  )
																													{
																														//--- Slot is allowed to edit
																														if ($arrValues[$keyId]["strfieldvalue"]!='')
																														{
																															?>
																															<script language="javascript">
																																addID('<?php echo $keyId."zz5"; ?>');
																															</script>
																															
																															<?php $bgStyle = $arrRow['strbgcolor'] != "" ? 'background-color: #'.$arrRow['strbgcolor'] : ''; ?>
																															<textarea Name="<?php echo $keyId.'_5'; ?>" id="<?php echo $keyId.'_5'; ?>" class="InputText" style="<?php echo $bgStyle?>; width:250px; height: 100px;"><?php echo $arrValues[$keyId]["strfieldvalue"];?></textarea>
																															<?php
																															$textareas[] = $keyId.'_5';
																														}
																														else
																														{
																															?>
																															<script language="javascript">
																															addID('<?php echo $keyId."zz5"; ?>');
																															</script>
																															
																															<?php $bgStyle = $arrRow['strbgcolor'] != "" ? 'background-color: #'.$arrRow['strbgcolor'] : ''; ?>
																															<textarea Name="<?php echo $keyId.'_5'; ?>" id="<?php echo $keyId.'_5'; ?>" class="InputText" style="<?php echo $bgStyle?>;width:250px; height: 100px;"><?php echo $arrRow['3'];?></textarea>
																															<?php
																															$textareas[] = $keyId.'_5';
																														}
																													}
																													else
																													{
																														if ($arrValues[$keyId]["strfieldvalue"]!='')
																														{
																															?>
																															<?php $bgStyle = $arrRow['strbgcolor'] != "" ? 'background-color: #'.$arrRow['strbgcolor'] : ''; ?>
																															<textarea  Name="<?php echo $keyId.'_5'; ?>" id="<?php echo $keyId.'_5'; ?>" class="InputText" style="<?php echo $bgStyle?>;width:250px; height: 100px;"><?php echo $arrValues[$keyId]["strfieldvalue"];?></textarea>
																															<?php
																															$textareas[] = $keyId.'_5';
																														}
																														else
																														{
																															?>
																															<?php $bgStyle = $arrRow['strbgcolor'] != "" ? 'background-color: #'.$arrRow['strbgcolor'] : ''; ?>
																															<textarea  Name="<?php echo $keyId.'_5'; ?>" id="<?php echo $keyId.'_5'; ?>" class="InputText" style="<?php echo $bgStyle?>;width:250px; height: 100px;"><?php echo $arrRow['3'];?></textarea>
																															<?php
																															$textareas[] = $keyId.'_5';
																														}	
																													}
																												}
																												else if ($arrRow["ntype"] == 6)
																												{
																													if ($arrValues[$keyId]["strfieldvalue"]!='')
																													{	// standard values
																														$strValue = $arrValues[$keyId]["strfieldvalue"];																						
																														$arrMySplit = split('---', $strValue);
																						
																														if ($arrMySplit[1] > 1)
																														{	// edited field values
																															
																															$strValue = '';
																															$nMax = (sizeof($arrMySplit));
																															for ($nIndex = 3; $nIndex < $nMax; $nIndex = $nIndex + 2)
																															{
																																$strValue .= $arrMySplit[$nIndex].'---';
																															}
																														}
																													}
																													
																													$nInputfieldID 	= $arrRow["nfieldid"];
																													
																													$bIsEnabled 	= 1;
																													if ( !(($arrSlot["nid"] == $arrCirculationProcess["nslotid"]) && ($arrCirculationProcess["ndecissionstate"] == 0) && (!$bReadOnly) ) )
																													{//--- Slot is not allowed to edit
																														$bIsEnabled 	= 0;
																													}
																													
																													
																													$strEcho = $objMyCirculation->getRadioGroup($nInputfieldID, $strValue, $bIsEnabled, $keyId, $nRunningCounter);
																													
																													echo $strEcho;
																												}
																												else if ($arrRow["ntype"] == 7)
																												{
																													if ($arrValues[$keyId]["strfieldvalue"]!='')
																													{	// standard values
																														$strValue = $arrValues[$keyId]["strfieldvalue"];																						
																														$arrMySplit = split('---', $strValue);
																						
																														if ($arrMySplit[1] > 1)
																														{	// edited field values
																															
																															$strValue = '';
																															$nMax = (sizeof($arrMySplit));
																															for ($nIndex = 3; $nIndex < $nMax; $nIndex = $nIndex + 2)
																															{
																																$strValue .= $arrMySplit[$nIndex].'---';
																															}
																														}
																													}
																													
																													$nInputfieldID 	= $arrRow["nfieldid"];
																													
																													$bIsEnabled 	= 1;
																													if ( !(($arrSlot["nid"] == $arrCirculationProcess["nslotid"]) && ($arrCirculationProcess["ndecissionstate"] == 0) && (!$bReadOnly) ) )
																													{//--- Slot is not allowed to edit
																														$bIsEnabled 	= 0;
																													}																					
																													
																													$strEcho = $objMyCirculation->getCheckBoxGroup($nInputfieldID, $strValue, $bIsEnabled, $keyId, $nRunningCounter);
																													
																													echo $strEcho;
																												}
																												elseif($arrRow["ntype"] == 8)
																												{
																													if ($arrValues[$keyId]["strfieldvalue"]!='')
																													{	// standard values
																														$strValue = $arrValues[$keyId]["strfieldvalue"];																						
																														$strRelations = $arrValues[$keyId]["strfieldvalue"];																						
																														$arrMySplit = split('---', $strValue);
																						
																														if ($arrMySplit[1] > 1)
																														{	// edited field values
																															
																															$strValue = '';
																															$nMax = (sizeof($arrMySplit));
																															for ($nIndex = 3; $nIndex < $nMax; $nIndex = $nIndex + 2)
																															{
																																$strValue .= $arrMySplit[$nIndex].'---';
																															}
																														}
																													}
																													
																													$nInputfieldID 	= $arrRow["nfieldid"];
																													
																													$bIsEnabled 	= 1;
																													if ( !(($arrSlot["nid"] == $arrCirculationProcess["nslotid"]) && ($arrCirculationProcess["ndecissionstate"] == 0) && (!$bReadOnly) ) )
																													{//--- Slot is not allowed to edit
																														$bIsEnabled 	= 0;
																													}																					
																													
																													$strEcho = $objMyCirculation->getComboBoxGroup($nInputfieldID, $strValue, $bIsEnabled, $keyId, $nRunningCounter);
																													
																													echo $strEcho;
																												}
																												elseif($arrRow["ntype"] == 9)
																												{
																													if ($arrValues[$keyId]["strfieldvalue"]!='')
																													{
																														$arrValue = split('rrrrr',$arrValues[$keyId]["strfieldvalue"]);																				
																														$REG_File		= $arrValue[1];
																														$arrSplit = split('---',$arrValue[0]);								
																													}
																													else
																													{
																														$arrValue = split('rrrrr',$arrRow['3']);																				
																														$REG_File		= $arrValue[1];
																														$arrSplit = split('---',$arrValue[0]);																						
																													}
																													
																													$nNumberOfUploads 	= $arrSplit[1];
																													$strDirectory		= $arrSplit[2].'_'.$nNumberOfUploads;
																													$strFilename		= $arrSplit[3];
																													$strUploadPath 		= '/uploads/';
																													$strLink			= $strUploadPath.$strDirectory.'/'.$strFilename;
																													
																													echo "<a href=\"$strLink\" target=\"_blank\">$strFilename</a>";
																													//if ($arrSlot["nid"] == $arrCirculationProcess["nslotid"] &&  !$bReadOnly)
																													{//--- Slot is allowed to edit
																														if ($bIsEmail)
																														{
																															echo "<br>".$INFO_IFRAME_FILES;
																														}
																														else
																														{
																															echo "<br><br>$FIELD_EDIT_REPLACEFILE<br>";
																															?>
																															<input style="margin-top: 3px;" type="file" Name="<?php echo $keyId.'_9'; ?>" id="<?php echo $keyId.'_9'; ?>">
																															<?php
																														}
																														?>
																														<input type="hidden" name="FILEName_<?php echo $keyId; ?>_<?php echo $nNumberOfUploads; ?>" value="<?php echo $arrValues[$keyId]["strfieldvalue"]; ?>">
																														<?php
																														if ($REG_File != '')
																														{
																															?>
																															<input type="hidden" name="<?php echo $keyId.'_REG'; ?>" id="<?php echo $keyId.'_REG'; ?>" value="<?php echo $REG_File; ?>">
																															<?php
																														}
																														?>
																														<script language="javascript">
																														addID('<?php echo $keyId."zz9zz".$REG_File; ?>');
																														</script>
																														<?php	
																													}
																												}
																												
																												echo "</td></tr>\n<tr>";
																												
																												
																												$nRunningCounter++;
																												echo "</div>";
																											}echo "</tr>\n<tr><td height=\"10\"></td></tr><tr>\n";
																										}
																									}
																								?>
																								</table>
																							</td>
																						</tr>
																					<?php
																				}
																			}
																		}
																		?>
																		<input type="hidden" name="slotid" id="slotid" value="<?php echo $arrSlot["nid"]; ?>">
																		<input type="hidden" name="circid" id="circid" value="<?php echo $arrCirculationProcess["ncirculationformid"]; ?>">
																		<?php
																			$q = Doctrine_Query::create()
																				  ->from('ConditionsOfApproval a')
																				  ->where('a.slot_id = ?', $arrSlot["nid"]);
																				  
																			 $conditions = $q->execute();
																			 if(sizeof($conditions) > 0)
																			 {
																					$background = ""; 
																					if ($arrSlot["nid"] == $arrCirculationProcess["nslotid"]) {
																						$background = "style='background-color: #ffe88e'";
																					}
																			  ?>
																			  <tr <?php echo $background; ?>>
																			  <td>
																			  <?php
																			  echo "<h2 style='margin: 0px; padding: 5px; font-size: 17px; font-family:\"Palatino Linotype\", \"Book Antiqua\", Palatino, serif;'>Conditions Of Approval</h2>";
																			  ?>
																			  </td>
																			  </tr>
																			  <tr>
																			  <td style="background-color: #CCCCCC; border: 1px solid silver; padding: 5px; margin: 5px; text-align: left;">
																			  <div>
																					<table>
																											<thead>
																											<tr><th>#</th><th style="min-width: 300px;">Description</th><th style="width: 150px;">Selected?</th></tr>
																												</thead>
																												<tbody>
																												<?php
																												$q = Doctrine_Query::create()
																												   ->from('ConditionsOfApproval a')
																												   ->orderBy('a.short_name ASC');
																												$conditions = $q->execute();
																												foreach($conditions as $condition)
																												{
																															$q = Doctrine_Query::create()
																															   ->from('ApprovalCondition a')
																															   ->where('a.entry_id = ?', $application->getId())
																															   ->andWhere('a.condition_id = ?', $condition->getId());
																															$cnd = $q->fetchOne();
																															
																															$resolved = "";
																															if(empty($cnd))
																															{
																																// if($sf_user->mfHasCredential("resolvecomment"))
																																 {
																																	$resolved =  "<a title='Click to Mark as Selected' href='' onClick=\"ajaxselect('/backend.php/applications/togglecondition/id/".$condition->getId()."/appid/".$application->getId()."','cn_".$condition->getId()."'); return false;\">";
																																 }
																																 $resolved = $resolved."<img src='/assets_backend/images/icons/dark/access_denied.png'>"; 
																																 //if($sf_user->mfHasCredential("resolvecomment"))
																																 {
																																	$resolved =  $resolved."</a>";
																																 }
																															}
																															else
																															{
																																// if($sf_user->mfHasCredential("resolvecomment"))
																																 {
																																	$resolved =  "<a title='Click to Mark as Not Selected.' href='' onClick=\"ajaxunselect('/backend.php/applications/togglecondition/id/".$condition->getId()."/appid/".$application->getId()."','cn_".$condition->getId()."');\">";
																																 }
																																 $resolved = $resolved."<img src='/assets_backend/images/icons/dark/tick.png'>"; 
																																// if($sf_user->mfHasCredential("resolvecomment"))
																																 {
																																	$resolved =  $resolved."</a>";
																																 }
																															}
																															
																															echo "<tr><td>".$condition->getShortName()."</td><td>".$condition->getDescription()."</td><td><div id='cn_".$condition->getId()."'>".$resolved."</div></td></tr>";
																												}	
																												?>
																												</tbody>
																					</table>
																					
																					
																					</ul>
																				</div>
																			  </td>
																			  </tr>
																			  <?php
																			}
																		?>
																</table>	
														<?PHP
															}
														?>
														</td>
													</tr>
													</table><br>
													<table cellspacing="0" cellpadding="0" width="90%" align="center">
													<tr>
														<td align="left">
															<?php
																$strKey = 'circid='.$application->getCirculationId().'&language=en&sortby=&start=1';
																$strKey	= $objURL->encryptURL($strKey);
															?>
															<a href="/backend.php/circulations/print?key=<?php echo $strKey ?>" target="_blank"><img src="/asset_misc/assets_backend/images/printer_small.png" border="0" align="absmiddle"> <?php echo $MAIL_CONTENT_PRINTVIEW;?></a>
														</td>
														<td align="right">
																	<button name="submitcomment" class='submit'  type="submit" value="true">Submit Comments</button>
																	<button name="submitdraft" class='submit' type="submit" value="true">Save As Draft</button>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<input type="hidden" name="language" value="en">
									<input type="hidden" name="chid" value="<?php echo $process->getNcirculationhistoryid(); ?>">
									<input type="hidden" name="cfid" value="<?php echo $process->getNcirculationformid(); ?>">
									<input type="hidden" name="cpid" value="<?php echo $process->getNid(); ?>">
									<input type="hidden" name="taskid" value="<?php echo $task->getId(); ?>">
								
								
								</fieldset>
							</form>
					</div>
					<div id="tabs-2">
							<form id="form" action="#" method="post" autocomplete="off" data-ajax="false">
								<fieldset>
						    <?php
							$q = Doctrine_Query::create()
								 ->from('ApForms a')
								 ->where('a.form_id = ?', $application->getFormId());
							$form = $q->fetchOne();
							$formtype = $form->getFormName();
							?>
									<label><?php echo $formtype ?> Details</label>
							<?php 
									$toggle = false;
									
									foreach ($entry_details as $data){ 
							?>  
								<section>
									<label for="text_field" style="font-weight: 900;"><?php echo $data['label']; ?></label>
									<div><?php if($data['value']){ echo nl2br($data['value']); }else{ echo "-"; } ?></div>
								</section>
							<?php } ?>  
								</fieldset>
							</form>
					</div>
					<div id="tabs-3">
							<form id="form" action="#" method="post" autocomplete="off" data-ajax="false">
								<fieldset>
									<label>Attached Documents</label>
							<?php 
									$toggle = false;
									
									foreach ($document_details as $data){ 
							?>  
								<section>
									<label for="text_field" style="font-weight: 900;"><?php echo $data['label']; ?></label>
									<div><?php if($data['value']){ echo nl2br($data['value']); }else{ echo "-"; } ?></div>
								</section>
							<?php } ?>  
								</fieldset>
							</form>
					</div>
					<div id="tabs-4">
						<form id="form" action="#" method="post" autocomplete="off" data-ajax="false">
								<fieldset>
									<label>Basic Details</label>
									<?php 
										$q = Doctrine_Query::create()
											 ->from('SfGuardUserProfile a')
											 ->where('a.user_id = ?', $application->getUserId());
										$user_profile = $q->fetchOne(); 
									?>  
								<section>
									<label for="text_field" style="font-weight: 900;">Name</label>
									<div><?php echo $user_profile->getFullname(); ?></div>
								</section>
								<section>
									<label for="text_field" style="font-weight: 900;">Email</label>
									<div><?php echo $user_profile->getEmail(); ?></div>
								</section>
								</fieldset>
							</form>
							<form id="form" action="#" method="post" autocomplete="off" data-ajax="false">
								<fieldset>
									<label>Additional Details</label>
							<?php 
									$q = Doctrine_Query::create()
										  ->from('mfUserProfile a')
										  ->where('a.user_id = ?', $application->getUserId());
									$profile = $q->fetchOne();
									if($profile)
                                                                        {
                                                                            $form_id = $profile->getFormId();
                                                                            $entry_id = $profile->getEntryId();



                                                                            $nav = trim($_GET['nav']);

                                                                            if(empty($form_id) || empty($entry_id)){
                                                                                die("Invalid Request");
                                                                            }

                                                                            $dbh = mf_connect_db();
                                                                            $mf_settings = mf_get_settings($dbh);

                                                                            //check permission, is the user allowed to access this page?
                                                                            if(empty($_SESSION['mf_user_privileges']['priv_administer'])){
                                                                                $user_perms = mf_get_user_permissions($dbh,$form_id,$_SESSION['mf_user_id']);

                                                                                //this page need edit_entries or view_entries permission
                                                                                if(empty($user_perms['edit_entries']) && empty($user_perms['view_entries'])){
                                                                                    $_SESSION['MF_DENIED'] = "You don't have permission to access this page.";

                                                                                    $ssl_suffix = mf_get_ssl_suffix();
                                                                                    header("Location: http{$ssl_suffix}://".$_SERVER['HTTP_HOST'].mf_get_dirname($_SERVER['PHP_SELF'])."/restricted.php");
                                                                                    exit;
                                                                                }
                                                                            }

                                                                            //if there is "nav" parameter, we need to determine the correct entry id and override the existing entry_id
                                                                            if(!empty($nav)){
                                                                                $all_entry_id_array = mf_get_filtered_entries_ids($dbh,$form_id);
                                                                                $entry_key = array_keys($all_entry_id_array,$entry_id);
                                                                                $entry_key = $entry_key[0];

                                                                                if($nav == 'prev'){
                                                                                    $entry_key--;
                                                                                }else{
                                                                                    $entry_key++;
                                                                                }

                                                                                $entry_id = $all_entry_id_array[$entry_key];

                                                                                //if there is no entry_id, fetch the first/last member of the array
                                                                                if(empty($entry_id)){
                                                                                    if($nav == 'prev'){
                                                                                        $entry_id = array_pop($all_entry_id_array);
                                                                                    }else{
                                                                                        $entry_id = $all_entry_id_array[0];
                                                                                    }
                                                                                }
                                                                            }

                                                                            //get form name
                                                                            $query 	= "select
					 form_name,
					 payment_enable_merchant,
					 payment_merchant_type,
					 payment_price_type,
					 payment_price_amount,
					 payment_currency,
					 payment_ask_billing,
					 payment_ask_shipping,
					 payment_enable_tax,
					 payment_tax_rate
			     from
			     	 ".MF_TABLE_PREFIX."forms
			    where
			    	 form_id = ?";
                                                                            $params = array($form_id);

                                                                            $sth = mf_do_query($query,$params,$dbh);
                                                                            $row = mf_do_fetch_result($sth);

                                                                            if(!empty($row)){
                                                                                $form_name = htmlspecialchars($row['form_name']);
                                                                                $payment_enable_merchant = (int) $row['payment_enable_merchant'];
                                                                                if($payment_enable_merchant < 1){
                                                                                    $payment_enable_merchant = 0;
                                                                                }

                                                                                $payment_price_amount = (double) $row['payment_price_amount'];
                                                                                $payment_merchant_type = $row['payment_merchant_type'];
                                                                                $payment_price_type = $row['payment_price_type'];
                                                                                $form_payment_currency = strtoupper($row['payment_currency']);
                                                                                $payment_ask_billing = (int) $row['payment_ask_billing'];
                                                                                $payment_ask_shipping = (int) $row['payment_ask_shipping'];

                                                                                $payment_enable_tax = (int) $row['payment_enable_tax'];
                                                                                $payment_tax_rate 	= (float) $row['payment_tax_rate'];
                                                                            }

                                                                            //if payment enabled, get the details
                                                                            if(!empty($payment_enable_merchant)){
                                                                                $query = "SELECT
						`payment_id`,
						 date_format(payment_date,'%e %b %Y - %r') payment_date,
						`payment_status`,
						`payment_fullname`,
						`payment_amount`,
						`payment_currency`,
						`payment_test_mode`,
						`payment_merchant_type`,
						`status`,
						`billing_street`,
						`billing_city`,
						`billing_state`,
						`billing_zipcode`,
						`billing_country`,
						`same_shipping_address`,
						`shipping_street`,
						`shipping_city`,
						`shipping_state`,
						`shipping_zipcode`,
						`shipping_country`
					FROM
						".MF_TABLE_PREFIX."form_payments
				   WHERE
				   		form_id = ? and record_id = ? and `status` = 1
				ORDER BY
						payment_date DESC
				   LIMIT 1";
                                                                                $params = array($form_id,$entry_id);

                                                                                $sth = mf_do_query($query,$params,$dbh);
                                                                                $row = mf_do_fetch_result($sth);

                                                                                $payment_id 		= $row['payment_id'];
                                                                                $payment_date 		= $row['payment_date'];
                                                                                $payment_status 	= $row['payment_status'];
                                                                                $payment_fullname 	= $row['payment_fullname'];
                                                                                $payment_amount 	= (double) $row['payment_amount'];
                                                                                $payment_currency 	= strtoupper($row['payment_currency']);
                                                                                $payment_test_mode 	= (int) $row['payment_test_mode'];
                                                                                $payment_merchant_type = $row['payment_merchant_type'];
                                                                                $billing_street 	= htmlspecialchars(trim($row['billing_street']));
                                                                                $billing_city 		= htmlspecialchars(trim($row['billing_city']));
                                                                                $billing_state 		= htmlspecialchars(trim($row['billing_state']));
                                                                                $billing_zipcode 	= htmlspecialchars(trim($row['billing_zipcode']));
                                                                                $billing_country 	= htmlspecialchars(trim($row['billing_country']));

                                                                                $same_shipping_address = (int) $row['same_shipping_address'];

                                                                                if(!empty($same_shipping_address)){
                                                                                    $shipping_street 	= $billing_street;
                                                                                    $shipping_city		= $billing_city;
                                                                                    $shipping_state		= $billing_state;
                                                                                    $shipping_zipcode	= $billing_zipcode;
                                                                                    $shipping_country	= $billing_country;
                                                                                }else{
                                                                                    $shipping_street 	= htmlspecialchars(trim($row['shipping_street']));
                                                                                    $shipping_city 		= htmlspecialchars(trim($row['shipping_city']));
                                                                                    $shipping_state 	= htmlspecialchars(trim($row['shipping_state']));
                                                                                    $shipping_zipcode 	= htmlspecialchars(trim($row['shipping_zipcode']));
                                                                                    $shipping_country 	= htmlspecialchars(trim($row['shipping_country']));
                                                                                }

                                                                                if(!empty($billing_street) || !empty($billing_city) || !empty($billing_state) || !empty($billing_zipcode) || !empty($billing_country)){
                                                                                    $billing_address  = "{$billing_street}<br />{$billing_city}, {$billing_state} {$billing_zipcode}<br />{$billing_country}";
                                                                                }

                                                                                if(!empty($shipping_street) || !empty($shipping_city) || !empty($shipping_state) || !empty($shipping_zipcode) || !empty($shipping_country)){
                                                                                    $shipping_address = "{$shipping_street}<br />{$shipping_city}, {$shipping_state} {$shipping_zipcode}<br />{$shipping_country}";
                                                                                }

                                                                                if(!empty($row)){
                                                                                    $payment_has_record = true;

                                                                                    if(empty($payment_id)){
                                                                                        //if the payment has record but has no payment id, then the record was being inserted manually (the payment status was being set manually by user)
                                                                                        //in this case, we consider this record empty
                                                                                        $payment_has_record = false;
                                                                                    }
                                                                                }else{
                                                                                    //if the entry doesn't have any record within ap_form_payments table
                                                                                    //we need to calculate the total amount
                                                                                    $payment_has_record = false;
                                                                                    $payment_status = "unpaid";

                                                                                    if($payment_price_type == 'variable'){
                                                                                        $payment_amount = (double) mf_get_payment_total($dbh,$form_id,$entry_id,0,'live');
                                                                                    }else if($payment_price_type == 'fixed'){
                                                                                        $payment_amount = $payment_price_amount;
                                                                                    }

                                                                                    //calculate tax if enabled
                                                                                    if(!empty($payment_enable_tax) && !empty($payment_tax_rate)){
                                                                                        $payment_tax_amount = ($payment_tax_rate / 100) * $payment_amount;
                                                                                        $payment_tax_amount = round($payment_tax_amount,2); //round to 2 digits decimal
                                                                                        $payment_amount += $payment_tax_amount;
                                                                                    }

                                                                                    $payment_currency = $form_payment_currency;
                                                                                }

                                                                                switch ($payment_currency) {
                                                                                    case 'USD' : $currency_symbol = '&#36;';break;
                                                                                    case 'EUR' : $currency_symbol = '&#8364;';break;
                                                                                    case 'GBP' : $currency_symbol = '&#163;';break;
                                                                                    case 'AUD' : $currency_symbol = '&#36;';break;
                                                                                    case 'CAD' : $currency_symbol = '&#36;';break;
                                                                                    case 'JPY' : $currency_symbol = '&#165;';break;
                                                                                    case 'THB' : $currency_symbol = '&#3647;';break;
                                                                                    case 'HUF' : $currency_symbol = '&#70;&#116;';break;
                                                                                    case 'CHF' : $currency_symbol = 'CHF';break;
                                                                                    case 'CZK' : $currency_symbol = '&#75;&#269;';break;
                                                                                    case 'SEK' : $currency_symbol = 'kr';break;
                                                                                    case 'DKK' : $currency_symbol = 'kr';break;
                                                                                    case 'PHP' : $currency_symbol = '&#36;';break;
                                                                                    case 'MYR' : $currency_symbol = 'RM';break;
                                                                                    case 'PLN' : $currency_symbol = '&#122;&#322;';break;
                                                                                    case 'BRL' : $currency_symbol = 'R&#36;';break;
                                                                                    case 'HKD' : $currency_symbol = '&#36;';break;
                                                                                    case 'MXN' : $currency_symbol = 'Mex&#36;';break;
                                                                                    case 'TWD' : $currency_symbol = 'NT&#36;';break;
                                                                                    case 'TRY' : $currency_symbol = 'TL';break;
                                                                                    case 'NZD' : $currency_symbol = '&#36;';break;
                                                                                    case 'SGD' : $currency_symbol = '&#36;';break;
                                                                                    default: $currency_symbol = ''; break;
                                                                                }
                                                                            }


                                                                            //get entry details for particular entry_id
                                                                            $param['checkbox_image'] = 'images/icons/59_blue_16.png';

                                                                            $architect_details = mf_get_entry_details($dbh,$form_id,$entry_id,$param);

                                                                            //get entry information (date created/updated/ip address)
                                                                            $query = "select
					date_format(date_created,'%e %b %Y - %r') date_created,
					date_format(date_updated,'%e %b %Y - %r') date_updated,
					ip_address
				from
					`".MF_TABLE_PREFIX."form_{$form_id}`
			where id=?";
                                                                            $params = array($entry_id);

                                                                            $sth = mf_do_query($query,$params,$dbh);
                                                                            $row = mf_do_fetch_result($sth);

                                                                            $date_created = $row['date_created'];
                                                                            if(!empty($row['date_updated'])){
                                                                                $date_updated = $row['date_updated'];
                                                                            }else{
                                                                                $date_updated = '&nbsp;';
                                                                            }
                                                                            $ip_address   = $row['ip_address'];

                                                                            //check for any 'signature' field, if there is any, we need to include the javascript library to display the signature
                                                                            $query = "select
					count(form_id) total_signature_field
				from
					".MF_TABLE_PREFIX."form_elements
			   where
			   		element_type = 'signature' and
			   		element_status=1 and
			   		form_id=?";
                                                                            $params = array($form_id);

                                                                            foreach ($architect_details as $data){ 
                                                                            ?>  
                                                                            <section>
                                                                                    <label for="text_field" style="font-weight: 900;"><?php echo $data['label']; ?></label>
                                                                                    <div><?php if($data['value']){ echo nl2br($data['value']); }else{ echo "-"; } ?></div>
                                                                            </section>
                                                                            <?php } 
                                                                        }
                                                                         ?>  
								</fieldset>
							</form>
					</div>
					<div id="tabs-5">
						<div class="accordion">
						<h4><a href="#">Comments Summary</a></h4>
						<div>
						    <ul>
							<?php
							    $comment_count = 0;
								$q = Doctrine_Query::create()
								   ->from('CfFormslot a');
								$slots = $q->execute();
								foreach($slots as $slot)
								{
												$q = Doctrine_Query::create()
												   ->from('Comments a')
												   ->where('a.circulation_id = ?', $application->getCirculationId())
												   ->andWhere('a.slot_id = ?', $slot->getNid());
												$comments = $q->execute();
												if(sizeof($comments) > 0)
												{
													$comment_count++;
												?>
													<li><?php echo $slot->getStrname(); ?>
														<ul>
															<?php
															foreach($comments as $comment)
															{
																	$q = Doctrine_Query::create()
																	   ->from('CfInputfield a')
																	   ->where('a.nid = ?', $comment->getFieldId());
																	$field = $q->fetchOne();
																	echo "<li><b style='color:#000; font-weight: 700;'>";
																	echo $field->getStrname()."</b><br>&nbsp;&nbsp;&nbsp;&nbsp;";
																	echo $comment->getComment();
																	echo "</li>";
															}
															?>
														</ul>
													</li>
												<?php
												}
								}
							?>
							
							<?php
								if($comment_count <= 0)
								{
									echo "<li>No Comments</li>";
								}
							?>
							</ul>
						</div>
						
						<?php
						if($application->getCirculationId())
						{
							$objMyCirculation = new CCirculation();
							
							 //--- open database
							$nConnection = mysql_connect($DATABASE_HOST, $DATABASE_UID, $DATABASE_PWD);
							if ($nConnection)
							{
								if (mysql_select_db($DATABASE_DB, $nConnection))
								{
									//--- get the single circulation form
									$query = "select * from cf_circulationform WHERE nid=".$application->getCirculationId();
									$nResult = mysql_query($query, $nConnection);

									if ($nResult)
									{
										if (mysql_num_rows($nResult) > 0)
										{
											$arrCirculationForm = mysql_fetch_array($nResult);
										}
									}
									$nSenderUserId = $arrCirculationForm['nsenderid'];
									//-----------------------------------------------
									//--- get history (all revisions)
									//-----------------------------------------------
									$arrHistoryData = array();
									$nMaxRevisionId = 0;
									$strQuery = "SELECT * FROM cf_circulationhistory WHERE ncirculationformid=".$application->getCirculationId()." ORDER BY nrevisionnumber DESC";
									$nResult = mysql_query($strQuery, $nConnection);
									if ($nResult)
									{
										if (mysql_num_rows($nResult) > 0)
										{
											while (	$arrRow = mysql_fetch_array($nResult))
											{
												if ($nMaxRevisionId == 0)
												{
													$nMaxRevisionId = $arrRow["nid"];	
												}
												$arrHistoryData[$arrRow["nid"]] = $arrRow;
											}
										}
									}
									
									if ($_REQUEST['nRevisionId'] == '')
									{
										$_REQUEST['nRevisionId'] = $nMaxRevisionId;
									}
									
									//-----------------------------------------------
									//--- get all users
									//-----------------------------------------------
									$arrUsers = array();
									$strQuery = "SELECT * FROM cf_user  WHERE bdeleted <> 1";
									$nResult = mysql_query($strQuery, $nConnection);
									if ($nResult)
									{
										if (mysql_num_rows($nResult) > 0)
										{
											while (	$arrRow = mysql_fetch_array($nResult))
											{
												$arrUsers[$arrRow["nid"]] = $arrRow;
											}
										}
									}
									
									//-----------------------------------------------
									//--- get the mailing list
									//-----------------------------------------------
									$query = "select * from cf_mailinglist WHERE nid=".$arrCirculationForm["nmailinglistid"];
									$nResult = mysql_query($query, $nConnection);
									if ($nResult)
									{
										if (mysql_num_rows($nResult) > 0)
										{
											$arrMailingList = mysql_fetch_array($nResult);
										}
									}

									$nMailingListID = $arrMailingList['nid'];
									
									//-----------------------------------------------
									//--- get the template
									//-----------------------------------------------	            
									$strQuery = "SELECT * FROM cf_formtemplate WHERE nid=".$arrMailingList["ntemplateid"];
									$nResult = mysql_query($strQuery, $nConnection);
									if ($nResult)
									{
										if (mysql_num_rows($nResult) > 0)
										{
											$arrTemplate = mysql_fetch_array($nResult);
											$strTemplateName = $arrTemplate["strname"];
										}
									}
									
									//-----------------------------------------------
									//--- get the form slots
									//-----------------------------------------------	            
									$arrSlots = array();
									$strQuery = "SELECT * FROM cf_formslot WHERE ntemplateid=".$arrMailingList["ntemplateid"]."  ORDER BY nslotnumber ASC";
									$nResult = mysql_query($strQuery, $nConnection);
									if ($nResult)
									{
										if (mysql_num_rows($nResult) > 0)
										{
											while (	$arrRow = mysql_fetch_array($nResult))
											{
												$arrSlots[] = $arrRow;
											}
										}
									}
									
									//-----------------------------------------------
									//--- get the field values
									//-----------------------------------------------	
												
									$arrValues = array();
									$strQuery = "SELECT * FROM cf_fieldvalue WHERE nformid=".$application->getCirculationId()." AND ncirculationhistoryid=".$_REQUEST["nRevisionId"];
									$nResult = mysql_query($strQuery, $nConnection);
									if ($nResult)
									{
										if (mysql_num_rows($nResult) > 0)
										{
											while (	$arrRow = mysql_fetch_array($nResult))
											{
												$arrValues[$arrRow["ninputfieldid"]."_".$arrRow["nslotid"]] = $arrRow;
											}
										}
									}
									
									//-----------------------------------------------
									//--- get the form process detail
									//-----------------------------------------------	            
									$arrProcessInformation = array();
									$arrProcessInformationSubstitute = array();
									
									$strQuery = "SELECT * FROM cf_circulationprocess WHERE ncirculationformid=".$application->getCirculationId()." AND ncirculationhistoryid=".$_REQUEST["nRevisionId"]." ORDER BY dateinprocesssince";
									$nResult = mysql_query($strQuery, $nConnection) or die ($strQuery."<br>".mysql_error());
									
									
									if ($nResult)
									{
										if (mysql_num_rows($nResult) > 0)
										{
											$nPosInSlot = -1;
											$nLastSlotId = -1;
											while (	$arrRow = mysql_fetch_array($nResult))
											{
												if ($arrRow["nissubstitiuteof"] != 0)
												{
													if ($arrRow["nslotid"] != $nLastSlotId)
													{
														$nLastSlotId = $arrRow["nslotid"];	
														$nPosInSlot = -1;
													}
													//$nPosInSlot++;
													$arrProcessInformationSubstitute[$arrRow["nissubstitiuteof"]] = $arrRow;
												}
												else
												{
													if ($arrRow["nslotid"] != $nLastSlotId)
													{
														$nLastSlotId = $arrRow["nslotid"];	
														$nPosInSlot = -1;
													}
													$nPosInSlot++;
													$arrProcessInformation[$arrRow["nuserid"]."_".$arrRow["nslotid"]."_".$nPosInSlot] = $arrRow;
												}
											}    				
										}
									}
								}
							}
							
							$nConnection = mysql_connect($DATABASE_HOST, $DATABASE_UID, $DATABASE_PWD);
							if ($nConnection)
							{
								if (mysql_select_db($DATABASE_DB, $nConnection))
								{
									foreach ($arrSlots as $arrSlot)
									{
										$q = Doctrine_Query::create()
											 ->from('CfFieldvalue a')
											 ->where('a.nslotid = ?', $arrSlot['nid'])
											 ->andWhere('a.nformid = ?', $application->getCirculationId());
										$fielddata = $q->execute();
										if(sizeof($fielddata) > 0)
										{
										?>
										<h4><a href="#"><?php echo $arrSlot['strname']; ?></a></h4>
										<div>
											<table width="100%">
											<tr><td style="font-weight: bold;background: #666666; color: #fff;  height: 40px;" colspan="5"><h1 style="margin: 0px; padding: 0px; font-size: 18px; font-family:'Palatino Linotype', 'Book Antiqua', Palatino, serif;"><?php echo $arrSlot['strname']; ?></h1></td></tr>
											<tr>
											<?php
												$strQuery = "SELECT * FROM cf_inputfield INNER JOIN cf_slottofield ON cf_inputfield.nid = cf_slottofield.nfieldid WHERE cf_slottofield.nslotid = ".$arrSlot["nid"]."  ORDER BY cf_slottofield.nposition ASC";
												$nResult = mysql_query($strQuery, $nConnection) or die ($strQuery."<br>".mysql_error());
												if ($nResult)
												{
													if (mysql_num_rows($nResult) > 0)
													{
														$nRunningCounter = 1;
														while (	$arrRow = mysql_fetch_array($nResult))
														{
															if($arrRow["ntype"] == "10")
																							{
																								echo "<td width=\"100%\" valign=\"top\" align=\"left\"><h2 style='margin: 0px; padding: 5px; font-size: 17px; font-family:\"Palatino Linotype\", \"Book Antiqua\", Palatino, serif;'>".htmlentities($arrRow["strname"])."</h2>";
																							}
																							else if($arrRow["ntype"] == "11")
																							{
																								echo "<td width=\"100%\" valign=\"top\" align=\"left\"><h3 style='margin: 0px; padding: 5px; font-size: 16px; font-family:\"Palatino Linotype\", \"Book Antiqua\", Palatino, serif;'>".htmlentities($arrRow["strname"])."</h3>";
																							}
																							else
																							{
																								echo "<td width=\"100%\" valign=\"top\" align=\"left\" style='background-color: #CCCCCC; border: 1px solid silver; padding: 5px;'><div style='margin-bottom: -2px; font-size: 14px; font-family:\"Palatino Linotype\", \"Book Antiqua\", Palatino, serif;'><b>".htmlentities($arrRow["strname"])."</b></div><br>";
																							}
																							
																							
															if ($arrRow["ntype"] == 1)
															{
																if ($arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"]!='')
																{
																	$arrValue = split('rrrrr',$arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"]);
																	
																	
																	$output = replaceLinks($arrValue[0]); 
																	if ($arrRow['strbgcolor'] != "") {
																		$output = '<span style="background-color: #'.$arrRow['strbgcolor'].'">'.$output.'<span>';
																	}																
																	echo $output; 
																}
																else
																{
																	$arrValue = split('rrrrr',$arrRow['strstandardvalue']);
																	
																	$output = replaceLinks($arrValue[0]); 
																	if ($arrRow['strbgcolor'] != "") {
																		$output = '<span style="background-color: #'.$arrRow['strbgcolor'].'">'.$output.'<span>';
																	}																
																	echo $output;
																}
															}
															else if ($arrRow["ntype"] == 2)
															{
																if ($arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"] != "on")
																{
																	$state = "inactive";
																}
																else
																{
																	$state = "active";
																}
																
																echo "<img src=\"/asset_misc/assets_backend/images/$state.gif\" height=\"16\" width=\"16\">";
															}
															else if ($arrRow["ntype"] == 3)
															{
																if ($arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"]!='')
																{
																	$arrValue = split('xx',$arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"]);								
																	$nNumGroup 	= $arrValue[1];														
																	$arrValue1 = split('rrrrr',$arrValue[2]);														
																	$strMyValue	= $arrValue1[0];
																}
																else
																{
																	$arrValue = split('xx',$arrRow['strstandardvalue']);								
																	$nNumGroup 	= $arrValue[1];														
																	$arrValue1 = split('rrrrr',$arrValue[2]);														
																	$strMyValue	= $arrValue1[0];
																}
																$output = replaceLinks($strMyValue); 
																if ($arrRow['strbgcolor'] != "") {
																	$output = '<span style="background-color: #'.$arrRow['strbgcolor'].'">'.$output.'<span>';
																}																
																echo $output;
															}
															else if ($arrRow["ntype"] == 4)
															{
																if ($arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"]!='')
																{
																	$arrValue = split('xx',$arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"]);
																	$nDateGroup 	= $arrValue[1];
																	$arrValue2 = split('rrrrr',$arrValue[2]);
																	$strMyValue 	= $arrValue2[0];
																}
																else
																{
																	$arrValue 		= split('xx',$arrRow['strstandardvalue']);
																	$nDateGroup 	= $arrValue[1];
																	$arrValue2 		= split('rrrrr',$arrValue[2]);
																	$strMyValue 	= $arrValue2[0];
																}
																$output = replaceLinks($strMyValue); 
																if ($arrRow['strbgcolor'] != "") {
																	$output = '<span style="background-color: #'.$arrRow['strbgcolor'].'">'.$output.'<span>';
																}																
																echo $output;
															}
															else if ($arrRow["ntype"] == 5)
															{
																if ($arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"]!='')
																{
																	echo replaceLinks($arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"]);
																}
																else
																{
																	echo replaceLinks($arrRow['strstandardvalue']);
																}
															}
															else if ($arrRow["ntype"] == 6)
															{
																if ($arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"]!='')
																{
																	$strValue = $arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"];
																	$arrMySplit = split('---', $strValue);
																	
																	if ($arrMySplit[1] > 1)
																	{	// edited field values
																		
																		$strValue = '';
																		$nMax = (sizeof($arrMySplit));
																		for ($nIndex = 3; $nIndex < $nMax; $nIndex = $nIndex + 2)
																		{
																			$strValue .= $arrMySplit[$nIndex].'---';
																		}
																		$keyId = rand(1, 150);
																	}
																	else
																	{	// we have to use the standard value
																		$strValue = $arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"];
																		$keyId = rand(1, 150);
																	}
																}
																else
																{
																	$strValue = $arrRow['strstandardvalue'];
																}
																
																$nInputfieldID 	= $arrRow["nfieldid"];
																$bIsEnabled 	= 0;
																
																$strEcho = $objMyCirculation->getRadioGroup2($nInputfieldID,$application->getCirculationId(),$arrSlot['nid'], $strValue, $bIsEnabled, $keyId, $nRunningCounter);
																
																echo $strEcho;
															}
															else if ($arrRow["ntype"] == 7)
															{
																if ($arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"]!='')
																{
																$strValue = $arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"];
																	$arrMySplit = split('---', $strValue);
																	
																	if ($arrMySplit[1] > 1)
																	{	// edited field values
																		
																		$strValue = '';
																		$nMax = (sizeof($arrMySplit));
																		for ($nIndex = 3; $nIndex < $nMax; $nIndex = $nIndex + 2)
																		{
																			$strValue .= $arrMySplit[$nIndex].'---';
																		}
																		$keyId = rand(1, 150);
																	}
																	else
																	{	// we have to use the standard value
																		$strValue = $arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"];
																		$keyId = rand(1, 150);
																	}
																}
																else
																{
																	$strValue = $arrRow['strstandardvalue'];
																}
																
																$nInputfieldID 	= $arrRow["nfieldid"];
																$bIsEnabled 	= 0;
																
																
																$strEcho = $objMyCirculation->getCheckboxGroup2($nInputfieldID,$application->getCirculationId(),$arrSlot['nid'], $strValue, $bIsEnabled, $keyId, $nRunningCounter);
																
																echo $strEcho;										
															}
															elseif($arrRow["ntype"] == 8)
															{
																if ($arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"]!='')
																{
																	$strValue = $arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"];
																	$arrMySplit = split('---', $strValue);
																	
																	if ($arrMySplit[1] > 1)
																	{	// edited field values
																		
																		$strValue = '';
																		$nMax = (sizeof($arrMySplit));
																		for ($nIndex = 3; $nIndex < $nMax; $nIndex = $nIndex + 2)
																		{
																			$strValue .= $arrMySplit[$nIndex].'---';
																		}
																		$keyId = rand(1, 150);
																	}
																	else
																	{	// we have to use the standard value
																		$strValue = $arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"];
																		$keyId = rand(1, 150);
																	}
																}
																else
																{
																	$strValue = $arrRow['strstandardvalue'];
																}
																
																$nInputfieldID 	= $arrRow["nfieldid"];
																$bIsEnabled 	= 0;
																
																
																$strEcho = $objMyCirculation->getComboBoxGroup2($nInputfieldID,$application->getCirculationId(),$arrSlot['nid'], $strValue, $bIsEnabled, $keyId, $nRunningCounter);
																
																echo $strEcho;
															}
															elseif($arrRow["ntype"] == 9)
															{
																if ($arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"]!='')
																{
																	$arrSplit = split('---',$arrValues[$arrRow["nfieldid"]."_".$arrSlot["nid"]]["strfieldvalue"]);
																}
																else
																{
																	$arrSplit = split('---',$arrRow['strstandardvalue']);
																}
																
																$nNumberOfUploads 	= $arrSplit[1];
																$strDirectory		= $arrSplit[2].'_'.$nNumberOfUploads;
																
																$arrValue22 = split('rrrrr',$arrSplit[3]);
																
																$strFilename		= $arrValue22[0];
																
																$strUploadPath 		= '/uploads/';
																$strLink			= $strUploadPath.$strDirectory.'/'.$strFilename;
																
																echo "<a href=\"$strLink\" target=\"_blank\">$strFilename</a>";
															}
															
															echo "</td></tr>\n<tr>";
																								
															
															
															$nRunningCounter++;
														}
														echo "<td></td>";
													}
												}
												
												?>
											</tr>
											</table>
										</div>
										<?php
										}
									}
								}
							}
						}
						?>
					</div>
					</div>
				</div>
</div>
<?php
}
catch(Exception $e)
{
?>
	<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=<?php echo $DEFAULT_CHARSET ?>">
		<title></title>
	</head>
	<body>
	<script language="javascript">
	window.location = '/backend.php/tasks/comment/id/<?php echo $task->getId(); ?>';
	</script>
	</body>
	</html>
<?php
}
?>

<script type="text/javascript">
	//<![CDATA[
	<?php
	$count = 1;
	foreach($textareas as $textarea)
	{
		?>
		var editor<?php echo $count; ?> = CKEDITOR.replace('<?php echo $textarea; ?>');
		CKFinder.setupCKEditor( editor<?php echo $count; ?>, '/assets_backend/js/ckfinder/');
		<?php
	   $count++;
	}
	?>
	//]]>
</script>
