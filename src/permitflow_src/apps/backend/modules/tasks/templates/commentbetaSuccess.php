<?php
/**
 * commentbetaSuccess templates.
 *
 * Displays new format comment sheet
 *
 * @package    backend
 * @subpackage tasks
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

$_SESSION["taskid"] = $task->getId();

$prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";
require($prefix_folder.'includes/init.php');

header("p3p: CP=\"IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT\"");

require($prefix_folder.'config.php');
require($prefix_folder.'includes/language.php');
require($prefix_folder.'includes/db-core.php');
require($prefix_folder.'includes/common-validator.php');
require($prefix_folder.'includes/view-functions.php');
require($prefix_folder.'includes/post-functions.php');
require($prefix_folder.'includes/entry-functions.php');
require($prefix_folder.'includes/helper-functions.php');
require($prefix_folder.'includes/filter-functions.php');
require($prefix_folder.'includes/theme-functions.php');
require($prefix_folder.'lib/recaptchalib.php');
require($prefix_folder.'lib/php-captcha/php-captcha.inc.php');
require($prefix_folder.'lib/text-captcha.php');
require($prefix_folder.'hooks/custom_hooks.php');

$q = Doctrine_Query::create()
 ->from('FormEntry a')
 ->where('a.id = ?', $task->getApplicationId());
$application = $q->fetchOne();
?>
<div class="pageheader">
  <h2><i class="fa fa-envelope"></i> Tasks <span>View task details</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>backend.php">Home</a></li>
      <li><a href="<?php echo public_path(); ?>backend.php/tasks/list">Tasks</a></li>
      <li><a href="<?php echo public_path(); ?>backend.php/tasks/list"><?php echo $task->getTypeName(); ?> Task</a></li>
      <li class="active"><?php echo $application->getApplicationId(); ?></li>
    </ol>
  </div>
</div>

<div class="contentpanel">
	<div class="row">

	 <div class="panel panel-default">
            <div class="panel-heading">
              <div class="panel-btns" align="right">
              	<?php
              		$q = Doctrine_Query::create()
              			->from("SubMenus a")
              			->where("a.id = ?", $application->getApproved());
              		$stage = $q->fetchOne();
              		if($stage)
              		{
              			$q = Doctrine_Query::create()
	              			->from("Menus a")
	              			->where("a.id = ?", $stage->getMenuId());
	              		$parentstage = $q->fetchOne();
              	?>
                <a><?php echo $parentstage->getTitle(); ?> / <?php echo $stage->getTitle(); ?><br><?php echo $application->getDateOfSubmission(); ?></a>
                <?php
                	}
                ?>
              </div>
              <h4 class="panel-title"><?php echo $application->getApplicationId(); ?></h4>
              <p>
	              <?php
	              $q = Doctrine_Query::create()
	              	->from("ApForms a")
	              	->where("a.form_id = ?", $application->getFormId());
	              $form = $q->fetchOne();
	              if($form)
	              {
		              echo $form->getFormName();
	              }
	              ?>
              </p>
            </div>
	       <div class="panel-body panel-body-nopadding">
		  <!-- BASIC WIZARD -->
	      <div id="basicWizard" class="basic-wizard">

	        <ul class="nav nav-pills nav-justified">
	            <li class="active"><a href="#ptab1" data-toggle="tab">Comment Sheet</a></li>
	            <li><a href="#ptab2" data-toggle="tab">Application Details</a></li>
	            <li><a href="#ptab4" data-toggle="tab">Applicant Details</a></li>
	        </ul>

	       <div class="tab-content tab-content-nopadding">
				<div class="tab-pane active" id="ptab1">
	         <?php
	            $q = Doctrine_Query::create()
	                 ->from('CfUser a')
	                 ->where('a.nid = ?', $_SESSION["SESSION_CUTEFLOW_USERID"]);
	            $reviewer = $q->fetchOne();
				
				$q = Doctrine_Query::create()
					->from("TaskForms a")
					->where("a.task_id = ?", $task->getId());
				$taskform = $q->fetchOne();

               if($taskform && $taskform->getFormId())
               {
                   $form_id = $taskform->getFormId();
                   $entry_id = $taskform->getEntryId();
                   
                   include_partial('commentform', array('form_id'=>$form_id, 'id'=>$entry_id, 'task' => $task, 'application' => $application));
               }
	             else
               {
                 $q = Doctrine_Query::create()
                      ->from('Department a')
                      ->where('a.department_name = ?', $reviewer->getStrdepartment());
                 $department = $q->fetchOne();

                 $q = Doctrine_Query::create()
                    ->from("ApForms a")
                    ->where("a.form_department = ?", $department->getId())
                    ->andWhere("a.form_department_stage = ?", $application->getApproved());
                $form = $q->fetchOne();

                if(empty($form))
                {
                  $q = Doctrine_Query::create()
                     ->from("ApForms a")
                     ->where("a.form_department_stage = ?", $application->getApproved());
                  $form = $q->fetchOne();
                }
				
				if(empty($form))
                {
                  $q = Doctrine_Query::create()
                    ->from("ApForms a")
                    ->where("a.form_department = ?", $department->getId());
                  $form = $q->fetchOne();
                }


  	             if($form)
  	             {
                   $form_id = $form->getFormId();

                   include_partial('commentform', array('form_id'=>$form_id, 'task' => $task, 'application' => $application));
  	             }
  	             else
  	             {
  	                    //Default to a comment sheet
  	             }
             }

	         ?>

	         <?php

     	$q = Doctrine_Query::create()
          ->from('Permits a')
          ->where('a.applicationform = ?', $form_id);
       $permit = $q->fetchOne();

				if($permit)
				{

				$q = Doctrine_Query::create()
					  ->from('ConditionsOfApproval a')
					  ->where('a.permit_id = ?', $permit->getId());

				 $conditions = $q->execute();
				 if(sizeof($conditions) > 0)
				 {
						$background = "";
				  ?>
				  <h1 style="margin-left: 10px;">Conditions</h1>
						<div class="table-responsive">
							<table class="table dt-on-steroids mb0" id="table3">
							<thead>
							<tr><th>#</th><th style="min-width: 300px;">Description</th><th style="width: 150px;">Selected?</th></tr>
								</thead>
								<tbody>
								<?php
								$q = Doctrine_Query::create()
								   ->from('ConditionsOfApproval a')
								   ->where('a.slot_id = ?', $department->getNid())
								   ->orderBy('a.short_name ASC');
								$conditions = $q->execute();
								foreach($conditions as $condition)
								{
											$q = Doctrine_Query::create()
											   ->from('ApprovalCondition a')
											   ->where('a.entry_id = ?', $application->getId())
											   ->andWhere('a.condition_id = ?', $condition->getId());
											$cnd = $q->fetchOne();

											$resolved = "";
											if(empty($cnd))
											{
												// if($sf_user->mfHasCredential("resolvecomment"))
												 {
													$resolved =  "<a title='Click to Mark as Selected' href='' onClick=\"ajaxselect('/backend.php/applications/togglecondition/id/".$condition->getId()."/appid/".$application->getId()."','cn_".$condition->getId()."'); return false;\">";
												 }
												 $resolved = $resolved."<span class='glyphicon glyphicon-remove'></span>";
												 //if($sf_user->mfHasCredential("resolvecomment"))
												 {
													$resolved =  $resolved."</a>";
												 }
											}
											else
											{
												// if($sf_user->mfHasCredential("resolvecomment"))
												 {
													$resolved =  "<a title='Click to Mark as Not Selected.' href='' onClick=\"ajaxunselect('/backend.php/applications/togglecondition/id/".$condition->getId()."/appid/".$application->getId()."','cn_".$condition->getId()."');\">";
												 }
												 $resolved = $resolved."<span class='glyphicon glyphicon-ok'></span>";
												// if($sf_user->mfHasCredential("resolvecomment"))
												 {
													$resolved =  $resolved."</a>";
												 }
											}

											echo "<tr><td>".$condition->getShortName()."</td><td>".$condition->getDescription()."</td><td><div id='cn_".$condition->getId()."'>".$resolved."</div></td></tr>";
								}
								?>
								</tbody>
					</table>
					</div>
				<?PHP
					}
				}
				?>
                </div>
                <div class="tab-pane" id="ptab2">
                   <form class="form-bordered">
            <?php
            include_partial('applications/viewdetails', array('application' => $application));
            ?>
        </form>
                </div>
                <div class="tab-pane" id="ptab4">
                 <form class="form-bordered">
            <?php
            include_partial('applications/viewclient', array('application' => $application));
            ?>
        </form>
                </div>
           </div>
          </div>
	      </div>
	</div>

    </div>
</div>
