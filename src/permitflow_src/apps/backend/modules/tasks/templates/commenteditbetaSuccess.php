<?php
    $taskid = null;
    if($_GET["taskid"])
    {
        $taskid = $_GET['taskid'];
        $_SESSION["taskid"] = $taskid;
    }
    else
    {
        $taskid = $_SESSION["taskid"];
    }

        $q = Doctrine_Query::create()
         ->from('Task a')
         ->where('a.id = ?', $taskid);
        $task = $q->fetchOne();

        $q = Doctrine_Query::create()
         ->from('FormEntry a')
         ->where('a.id = ?', $task->getApplicationId());
        $application = $q->fetchOne();
?>
<div class="pageheader">
  <h2><i class="fa fa-envelope"></i> Tasks <span>View task details</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>backend.php">Home</a></li>
      <li><a href="<?php echo public_path(); ?>backend.php/tasks/list">Tasks</a></li>
      <li><a href="<?php echo public_path(); ?>backend.php/tasks/list"><?php echo $task->getTypeName(); ?> Task</a></li>
      <li class="active"><?php echo $application->getApplicationId(); ?></li>
    </ol>
  </div>
</div>

<div class="contentpanel">
    <div class="row">
    
     <div class="panel panel-default">
            <div class="panel-heading">
              <div class="panel-btns" align="right">
                <?php
                    $q = Doctrine_Query::create()
                        ->from("SubMenus a")
                        ->where("a.id = ?", $application->getApproved());
                    $stage = $q->fetchOne();
                    if($stage)
                    {
                        $q = Doctrine_Query::create()
                            ->from("Menus a")
                            ->where("a.id = ?", $stage->getMenuId());
                        $parentstage = $q->fetchOne();
                ?>
                <a><?php echo $parentstage->getTitle(); ?> / <?php echo $stage->getTitle(); ?><br><?php echo $application->getDateOfSubmission(); ?></a>
                <?php
                    }
                ?>
              </div>
              <h4 class="panel-title"><?php echo $application->getApplicationId(); ?></h4>
              <p>
                  <?php
                  $q = Doctrine_Query::create()
                    ->from("ApForms a")
                    ->where("a.form_id = ?", $application->getFormId());
                  $form = $q->fetchOne();
                  if($form)
                  {
                      echo $form->getFormDescription();
                  }
                  ?>
              </p>
            </div>
           <div class="panel-body panel-body-nopadding">
          <!-- BASIC WIZARD -->
          <div id="basicWizard" class="basic-wizard">
            
                <ul class="nav nav-pills nav-justified">
              <li><a href="#ptab1" data-toggle="tab">Comment Sheet</a></li>
                <li><a href="#ptab2" data-toggle="tab">Application Details</a></li>
                <li><a href="#ptab4" data-toggle="tab">Applicant Details</a></li>
            </ul>
            
           <div class="tab-content tab-content-nopadding">
                <div class="tab-pane" id="ptab1">
        <script type="text/javascript" src="/assets_backend/js/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="/assets_backend/js/ckfinder/ckfinder.js"></script>
        <script src="/assets_backend/js/ckeditor/_samples/sample.js" type="text/javascript"></script>
        <?php
            $q = Doctrine_Query::create()
                 ->from('CfUser a')
                 ->where('a.nid = ?', $_SESSION["SESSION_CUTEFLOW_USERID"]);
            $reviewer = $q->fetchOne();

              $q = Doctrine_Query::create()
                  ->from("TaskForms a")
                  ->where("a.task_id = ?", $task->getId());
              $taskform = $q->fetchOne();

             if($taskform)
             {
                    $form_id = $taskform->getFormId();

                     $prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";
                   require($prefix_folder.'includes/init.php');

                   header("p3p: CP=\"IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT\"");

                   require($prefix_folder.'config.php');
                   require($prefix_folder.'includes/language.php');
                   require($prefix_folder.'includes/db-core.php');
                   require($prefix_folder.'includes/common-validator.php');
                   require($prefix_folder.'includes/view-functions.php');
                   require($prefix_folder.'includes/post-functions.php');
                   require($prefix_folder.'includes/entry-functions.php');
                   require($prefix_folder.'includes/helper-functions.php');
                   require($prefix_folder.'includes/filter-functions.php');
                   require($prefix_folder.'includes/theme-functions.php');
                   require($prefix_folder.'lib/recaptchalib.php');
                   require($prefix_folder.'lib/php-captcha/php-captcha.inc.php');
                   require($prefix_folder.'lib/text-captcha.php');
                   require($prefix_folder.'hooks/custom_hooks.php');
                    include_partial('commenteditform', array('form_id'=>$form_id, 'task' => $task, 'application' => $application));
             }
             else
             {
                    //Default to old commenting process
             }



            ?>
                </div>
                <div class="tab-pane" id="ptab2">
                   <form class="form-bordered">
            <?php
            include_partial('applications/viewdetails', array('application' => $application));
            ?>
        </form>    
                </div>
                <div class="tab-pane" id="ptab4">
                 <form class="form-bordered">
            <?php
            include_partial('applications/viewclient', array('application' => $application));
            ?>
        </form>    
                </div>
           </div>
          </div>
          </div>
    </div>
    
    </div>
</div>    
