<?php
use_helper("I18N");

if($sf_user->mfHasCredential("access_tasks"))
{
?>
<?php
$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
?>
<div class="pageheader">
  <h2><i class="fa fa-envelope"></i> <?php echo __("Tasks"); ?></h2>
  <div class="breadcrumb-wrapper">
    <span class="label"><?php echo __("You are here"); ?>:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>backend.php/dashboard"><?php echo __("Home"); ?></a></li>
      <li class="active"><?php echo __("Tasks"); ?></li>
    </ol>
  </div>
</div>

<div class="contentpanel panel-email">

    <div class="row">
        <div class="col-sm-3 col-lg-2">
            <ul class="nav nav-pills nav-stacked nav-email">
               <?php
			   if($sf_user->mfHasCredential('accessalltasks'))
			   {
                $q = Doctrine_Query::create()
                    ->from("Task a")
                    ->leftJoin("a.Owner b")
                    ->leftJoin("a.Application c")
                    ->where("a.owner_user_id = b.nid")
                    ->andWhere("b.strdepartment = ?", $filterdepartment)
                    ->andWhere("a.status = ?", 1)
                    ->andWhere("c.id = a.application_id")
                    ->orderBy("a.id DESC");
                $pendingtasks = $q->execute();
			   }
			   else
			   {
                $q = Doctrine_Query::create()
                    ->from("Task a")
                    ->leftJoin("a.Owner b")
                    ->leftJoin("a.Application c")
                    ->where("a.owner_user_id = b.nid")
				    ->andWhere("a.owner_user_id = ?", $_SESSION["SESSION_CUTEFLOW_USERID"])
                    ->andWhere("b.strdepartment = ?", $filterdepartment)
                    ->andWhere("a.status = ?", 1)
                    ->andWhere("c.id = a.application_id")
                    ->orderBy("a.id DESC");
                $pendingtasks = $q->execute();
			   }
                ?>
               <li <?php if($sf_context->getActionName() == "department" && ($filterstatus == "" || $filterstatus == 1) && $viewingapproved == false): ?>class="active"<?php endif; ?>>
                <a href="<?php echo public_path(); ?>backend.php/tasks/department/filter/<?php echo $filterdepartment; ?>/status/1">
                    <span class="badge pull-right"><?php echo sizeof($pendingtasks); ?></span>
                    <i class="glyphicon glyphicon-inbox"></i> <?php echo __("Pending"); ?>
                </a>
                </li>
               <?php
			   if($sf_user->mfHasCredential('accessalltasks'))
			   {
                $q = Doctrine_Query::create()
                    ->from("Task a")
                    ->leftJoin("a.Owner b")
                    ->leftJoin("a.Application c")
                    ->where("a.owner_user_id = b.nid")
                    ->andWhere("b.strdepartment = ?", $filterdepartment)
                    ->andWhere("a.status = ?", 25)
                    ->andWhere("c.id = a.application_id")
                    ->orderBy("a.id DESC");
                $completedtasks = $q->execute();
			   }
			   else
			   {
                $q = Doctrine_Query::create()
                    ->from("Task a")
                    ->leftJoin("a.Owner b")
                    ->leftJoin("a.Application c")
                    ->where("a.owner_user_id = b.nid")
				    ->andWhere("a.owner_user_id = ?", $_SESSION["SESSION_CUTEFLOW_USERID"])
                    ->andWhere("b.strdepartment = ?", $filterdepartment)
                    ->andWhere("a.status = ?", 25)
                    ->andWhere("c.id = a.application_id")
                    ->orderBy("a.id DESC");
                $completedtasks = $q->execute();
			   }
                ?>
               <li <?php if($sf_context->getActionName() == "department" && $filterstatus == 25): ?>class="active"<?php endif; ?>>
                <a href="<?php echo public_path(); ?>backend.php/tasks/department/filter/<?php echo $filterdepartment; ?>/status/25">
                    <span class="badge pull-right"><?php echo sizeof($completedtasks); ?></span>
                    <i class="glyphicon glyphicon-inbox"></i> <?php echo __("Completed"); ?>
                </a>
                </li>
               <?php
			   if($sf_user->mfHasCredential('accessalltasks'))
			   {
                $q = Doctrine_Query::create()
                    ->from("Task a")
                    ->leftJoin("a.Owner b")
                    ->leftJoin("a.Application c")
                    ->where("a.owner_user_id = b.nid")
                    ->andWhere("b.strdepartment = ?", $filterdepartment)
                    ->andWhere("a.status = ?", 55)
                    ->andWhere("c.id = a.application_id")
                    ->orderBy("a.id DESC");
                $cancelledtasks = $q->execute();
			   }
			   else
			   {
                $q = Doctrine_Query::create()
                    ->from("Task a")
                    ->leftJoin("a.Owner b")
                    ->leftJoin("a.Application c")
                    ->where("a.owner_user_id = b.nid")
				    ->andWhere("a.owner_user_id = ?", $_SESSION["SESSION_CUTEFLOW_USERID"])
                    ->andWhere("b.strdepartment = ?", $filterdepartment)
                    ->andWhere("a.status = ?", 55)
                    ->andWhere("c.id = a.application_id")
                    ->orderBy("a.id DESC");
                $cancelledtasks = $q->execute();
			   }
                ?>
               <li <?php if($sf_context->getActionName() == "department" && $filterstatus == 55): ?>class="active"<?php endif; ?>>
                <a href="<?php echo public_path(); ?>backend.php/tasks/department/filter/<?php echo $filterdepartment; ?>/status/55">
                    <span class="badge pull-right"><?php echo sizeof($cancelledtasks); ?></span>
                    <i class="glyphicon glyphicon-inbox"></i> <?php echo __("Cancelled"); ?>
                </a>
                </li>
               <?php
			   if($sf_user->mfHasCredential('accessalltasks'))
			   {
                $q = Doctrine_Query::create()
                    ->from("Task a")
                    ->leftJoin("a.Owner b")
                    ->leftJoin("a.Application c")
                    ->where("a.owner_user_id = b.nid")
                    ->andWhere("b.strdepartment = ?", $filterdepartment)
                    ->andWhere("a.status = ?", 3)
                    ->andWhere("c.id = a.application_id")
                    ->orderBy("a.id DESC");
                $postponedtasks = $q->execute();
			   }
			   else
			   {
                $q = Doctrine_Query::create()
                    ->from("Task a")
                    ->leftJoin("a.Owner b")
                    ->leftJoin("a.Application c")
                    ->where("a.owner_user_id = b.nid")
				    ->andWhere("a.owner_user_id = ?", $_SESSION["SESSION_CUTEFLOW_USERID"])
                    ->andWhere("b.strdepartment = ?", $filterdepartment)
                    ->andWhere("a.status = ?", 3)
                    ->andWhere("c.id = a.application_id")
                    ->orderBy("a.id DESC");
                $postponedtasks = $q->execute();
			   }
                ?>
               <li <?php if($sf_context->getActionName() == "department" && $filterstatus == 3): ?>class="active"<?php endif; ?>>
                <a href="<?php echo public_path(); ?>backend.php/tasks/department/filter/<?php echo $filterdepartment; ?>/status/3">
                    <span class="badge pull-right"><?php echo sizeof($postponedtasks); ?></span>
                    <i class="glyphicon glyphicon-inbox"></i> <?php echo __("Postponed"); ?>
                </a>
                </li>
               <?php
			   if($sf_user->mfHasCredential('accessalltasks'))
			   {
                $q = Doctrine_Query::create()
                    ->from("Task a")
                    ->leftJoin("a.Owner b")
                    ->leftJoin("a.Application c")
                    ->where("a.owner_user_id = b.nid")
                    ->andWhere("b.strdepartment = ?", $filterdepartment)
                    ->andWhere("a.status = ?", 45)
                    ->andWhere("c.id = a.application_id")
                    ->orderBy("a.id DESC");
                $tranferredtasks = $q->execute();
			   }
			   else
			   {
                $q = Doctrine_Query::create()
                    ->from("Task a")
                    ->leftJoin("a.Owner b")
                    ->leftJoin("a.Application c")
                    ->where("a.owner_user_id = b.nid")
				    ->andWhere("a.owner_user_id = ?", $_SESSION["SESSION_CUTEFLOW_USERID"])
                    ->andWhere("b.strdepartment = ?", $filterdepartment)
                    ->andWhere("a.status = ?", 45)
                    ->andWhere("c.id = a.application_id")
                    ->orderBy("a.id DESC");
                $tranferredtasks = $q->execute();
			   }
                ?>
               <li <?php if($sf_context->getActionName() == "department" && $filterstatus == 45): ?>class="active"<?php endif; ?>>
                <a href="<?php echo public_path(); ?>backend.php/tasks/department/filter/<?php echo $filterdepartment; ?>/status/45">
                    <span class="badge pull-right"><?php echo sizeof($tranferredtasks); ?></span>
                    <i class="glyphicon glyphicon-inbox"></i> <?php echo __("Transferred"); ?>
                </a>
                </li>
                <li <?php if($viewingapproved): ?>class="active"<?php endif; ?>>
                <a href="<?php echo public_path(); ?>backend.php/tasks/department/filter/<?php echo $filterdepartment; ?>/status/approved">
                    <?php 
                    $q = Doctrine_Query::create()
                        ->from("Task a")
                        ->leftJoin("a.Application c")
                        ->where("a.creator_user_id = ?", $_SESSION["SESSION_CUTEFLOW_USERID"])
                        ->andWhere("a.status = ?", 2)
                        ->andWhere("c.id = a.application_id");
                    $apps_approvals = $q->execute();
                    ?>
                    <span class="badge pull-right"><?php echo sizeof($apps_approvals); ?></span>
                    <i class="glyphicon glyphicon-pencil"></i> <?php echo __("Awaiting Approval"); ?>
                </a>
                </li>
            </ul>
            
            <div class="mb30"></div>
            
           
            
        </div><!-- col-sm-3 -->
        
        <div class="col-sm-9 col-lg-10">
            
               <div class="panel panel-dark">
                   <div class="panel-heading">
                    <h3 class="panel-title"><?php echo __("Tasks"); ?></h3>
                <p class="text-muted"><?php echo __("Showing"); ?> <?php echo count($pager); ?> <?php echo __("tasks"); ?></p>
                </div>
       <div class="panel-body panel-body-nopadding">
                    <?php if ($pager->getResults()): ?>
                    <div class="table-responsive">
                        <table class="table dt-on-steroids mb0">
                        <thead>
                            <th class="no-sort"></th>
                            <th class="no-sort"></th>
                            <th><?php echo __("Application No"); ?></th>
                            <th><?php echo __("Task"); ?></th>
                            <th><?php echo __("Status"); ?></th>
                            <th><?php echo __("Priority"); ?></th>
                            <th><?php echo __("Date Created"); ?></th>
                            <th class="no-sort" width="5%"><?php echo __("Actions"); ?></th>
                        </thead>
                          <tbody>
                                <?php foreach ($pager->getResults() as $task): ?>
                                <?php include_partial('list', array('task' => $task)) ?>
                                <?php endforeach; ?>                              
                                </tbody>
                                
                                
                         
                        <tfoot>
                		<tr>
                			<th colspan="12">
     <p class="table-showing pull-left"><strong><?php echo count($pager) ?></strong> Tasks
                                           
                                            <?php if ($pager->haveToPaginate()): ?>
                                              - page <strong><?php echo $pager->getPage() ?>/<?php echo $pager->getLastPage() ?></strong>
                                            <?php endif; ?></p>
                                            

     <?php if ($pager->haveToPaginate()): ?>
                <ul class="pagination pagination-sm mb0 mt0 pull-right">
							    <li> <a href="/backend.php/tasks/department/filter/<?php echo $filterdepartment; ?>/page/1">
                                <i class="fa fa-angle-left"></i>
							    </a></li>

							   <li> <a href="/backend.php/tasks/department/filter/<?php echo $filterdepartment; ?>/page/<?php echo $pager->getPreviousPage() ?>">
							    <i class="fa fa-angle-left"></i>
							    </a></li>

							    <?php foreach ($pager->getLinks() as $page): ?>
							      <?php if ($page == $pager->getPage()): ?>
							         <li class="active"><a href=""><?php echo $page ?></a>
							      <?php else: ?>
							        <li><a href="/backend.php/tasks/department/filter/<?php echo $filterdepartment; ?>/page/<?php echo $page ?>"><?php echo $page ?></a></li>
							      <?php endif; ?>
							    <?php endforeach; ?>

							   <li> <a href="/backend.php/tasks/department/filter/<?php echo $filterdepartment; ?>/page/<?php echo $pager->getNextPage() ?>">
							      <i class="fa fa-angle-right"></i>
							    </a></li>

							   <li> <a href="/backend.php/tasks/department/filter/<?php echo $filterdepartment; ?>/page/<?php echo $pager->getLastPage() ?>">
							      <i class="fa fa-angle-right"></i>
							    </a></li>
							  </ul>
							<?php endif; ?>
						
                </th>
                </tr>
                </tfoot>
            
             </table>
                       

                        
                    <?php else: ?>
                        <div class="table-responsive">
                            <table class="table dt-on-steroids mb0">
                                <tbody>
                                    <tr><td>
                                    No Records Found
                                    </td></tr>
                                </tbody>
                            </table>
                        </div>
                    <?php endif; ?>
                </div><!-- panel-body -->
            </div><!-- panel -->
            
        </div><!-- col-sm-9 -->
        
    </div><!-- row -->

</div>

</div><!-- mainpanel -->
<?php
}
else
{
    include_partial("settings/accessdenied");
}
?>
