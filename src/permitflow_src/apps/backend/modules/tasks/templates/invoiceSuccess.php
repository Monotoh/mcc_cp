<?php
/**
 * invoiceSuccess templates.
 *
 * Generate an invoice for the selected application
 *
 * @package    backend
 * @subpackage tasks
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

?>
<?php

    $prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";

    require($prefix_folder.'includes/init.php');

    require($prefix_folder.'config.php');
    require($prefix_folder.'includes/db-core.php');
    require($prefix_folder.'includes/helper-functions.php');
    require($prefix_folder.'includes/check-session.php');

    require($prefix_folder.'includes/entry-functions.php');
    require($prefix_folder.'includes/post-functions.php');
    require($prefix_folder.'includes/users-functions.php');

    $prefix_folder = dirname(__FILE__)."/../../../../..";

    require_once $prefix_folder.'/lib/vendor/cp_workflow/config/config.inc.php';
	require_once $prefix_folder.'/lib/vendor/cp_workflow/language_files/language.inc.php';
	require_once $prefix_folder.'/lib/vendor/cp_workflow/pages/CCirculation.inc.php';
	require_once $prefix_folder.'/lib/vendor/cp_workflow/pages/version.inc.php';


	$form_id  = $application->getFormId();
	$entry_id = $application->getEntryId();

    $nav = trim($_GET['nav']);

    if(empty($form_id) || empty($entry_id)){
        die("Invalid Request");
    }

    $dbh = mf_connect_db();
    $mf_settings = mf_get_settings($dbh);

    //check permission, is the user allowed to access this page?
    if(empty($_SESSION['mf_user_privileges']['priv_administer'])){
        $user_perms = mf_get_user_permissions($dbh,$form_id,$_SESSION['mf_user_id']);

        //this page need edit_entries or view_entries permission
        if(empty($user_perms['edit_entries']) && empty($user_perms['view_entries'])){
            $_SESSION['MF_DENIED'] = "You don't have permission to access this page.";

            $ssl_suffix = mf_get_ssl_suffix();
            header("Location: http{$ssl_suffix}://".$_SERVER['HTTP_HOST'].mf_get_dirname($_SERVER['PHP_SELF'])."/restricted.php");
            exit;
        }
    }

    //if there is "nav" parameter, we need to determine the correct entry id and override the existing entry_id
    if(!empty($nav)){
        $all_entry_id_array = mf_get_filtered_entries_ids($dbh,$form_id);
        $entry_key = array_keys($all_entry_id_array,$entry_id);
        $entry_key = $entry_key[0];

        if($nav == 'prev'){
            $entry_key--;
        }else{
            $entry_key++;
        }

        $entry_id = $all_entry_id_array[$entry_key];

        //if there is no entry_id, fetch the first/last member of the array
        if(empty($entry_id)){
            if($nav == 'prev'){
                $entry_id = array_pop($all_entry_id_array);
            }else{
                $entry_id = $all_entry_id_array[0];
            }
        }
    }

    //get form name
    $query 	= "select
                         form_name,
                         payment_enable_merchant,
                         payment_merchant_type,
                         payment_price_type,
                         payment_price_amount,
                         payment_currency,
                         payment_ask_billing,
                         payment_ask_shipping,
                         payment_enable_tax,
                         payment_tax_rate
                     from
                         ".MF_TABLE_PREFIX."forms
                    where
                         form_id = ?";
    $params = array($form_id);

    $sth = mf_do_query($query,$params,$dbh);
    $row = mf_do_fetch_result($sth);

    if(!empty($row)){
        $form_name = htmlspecialchars($row['form_name']);
        $payment_enable_merchant = (int) $row['payment_enable_merchant'];
        if($payment_enable_merchant < 1){
            $payment_enable_merchant = 0;
        }

        $payment_price_amount = (double) $row['payment_price_amount'];
        $payment_merchant_type = $row['payment_merchant_type'];
        $payment_price_type = $row['payment_price_type'];
        $form_payment_currency = strtoupper($row['payment_currency']);
        $payment_ask_billing = (int) $row['payment_ask_billing'];
        $payment_ask_shipping = (int) $row['payment_ask_shipping'];

        $payment_enable_tax = (int) $row['payment_enable_tax'];
        $payment_tax_rate 	= (float) $row['payment_tax_rate'];
    }

    //if payment enabled, get the details
    if(!empty($payment_enable_merchant)){
        $query = "SELECT
                            `payment_id`,
                             date_format(payment_date,'%e %b %Y - %r') payment_date,
                            `payment_status`,
                            `payment_fullname`,
                            `payment_amount`,
                            `payment_currency`,
                            `payment_test_mode`,
                            `payment_merchant_type`,
                            `status`,
                            `billing_street`,
                            `billing_city`,
                            `billing_state`,
                            `billing_zipcode`,
                            `billing_country`,
                            `same_shipping_address`,
                            `shipping_street`,
                            `shipping_city`,
                            `shipping_state`,
                            `shipping_zipcode`,
                            `shipping_country`
                        FROM
                            ".MF_TABLE_PREFIX."form_payments
                       WHERE
                            form_id = ? and record_id = ? and `status` = 1
                    ORDER BY
                            payment_date DESC
                       LIMIT 1";
        $params = array($form_id,$entry_id);

        $sth = mf_do_query($query,$params,$dbh);
        $row = mf_do_fetch_result($sth);

        $payment_id 		= $row['payment_id'];
        $payment_date 		= $row['payment_date'];
        $payment_status 	= $row['payment_status'];
        $payment_fullname 	= $row['payment_fullname'];
        $payment_amount 	= (double) $row['payment_amount'];
        $payment_currency 	= strtoupper($row['payment_currency']);
        $payment_test_mode 	= (int) $row['payment_test_mode'];
        $payment_merchant_type = $row['payment_merchant_type'];
        $billing_street 	= htmlspecialchars(trim($row['billing_street']));
        $billing_city 		= htmlspecialchars(trim($row['billing_city']));
        $billing_state 		= htmlspecialchars(trim($row['billing_state']));
        $billing_zipcode 	= htmlspecialchars(trim($row['billing_zipcode']));
        $billing_country 	= htmlspecialchars(trim($row['billing_country']));

        $same_shipping_address = (int) $row['same_shipping_address'];

        if(!empty($same_shipping_address)){
            $shipping_street 	= $billing_street;
            $shipping_city		= $billing_city;
            $shipping_state		= $billing_state;
            $shipping_zipcode	= $billing_zipcode;
            $shipping_country	= $billing_country;
        }else{
            $shipping_street 	= htmlspecialchars(trim($row['shipping_street']));
            $shipping_city 		= htmlspecialchars(trim($row['shipping_city']));
            $shipping_state 	= htmlspecialchars(trim($row['shipping_state']));
            $shipping_zipcode 	= htmlspecialchars(trim($row['shipping_zipcode']));
            $shipping_country 	= htmlspecialchars(trim($row['shipping_country']));
        }

        if(!empty($billing_street) || !empty($billing_city) || !empty($billing_state) || !empty($billing_zipcode) || !empty($billing_country)){
            $billing_address  = "{$billing_street}<br />{$billing_city}, {$billing_state} {$billing_zipcode}<br />{$billing_country}";
        }

        if(!empty($shipping_street) || !empty($shipping_city) || !empty($shipping_state) || !empty($shipping_zipcode) || !empty($shipping_country)){
            $shipping_address = "{$shipping_street}<br />{$shipping_city}, {$shipping_state} {$shipping_zipcode}<br />{$shipping_country}";
        }

        if(!empty($row)){
            $payment_has_record = true;

            if(empty($payment_id)){
                //if the payment has record but has no payment id, then the record was being inserted manually (the payment status was being set manually by user)
                //in this case, we consider this record empty
                $payment_has_record = false;
            }
        }else{
            //if the entry doesn't have any record within ap_form_payments table
            //we need to calculate the total amount
            $payment_has_record = false;
            $payment_status = "unpaid";

            if($payment_price_type == 'variable'){
                $payment_amount = (double) mf_get_payment_total($dbh,$form_id,$entry_id,0,'live');
            }else if($payment_price_type == 'fixed'){
                $payment_amount = $payment_price_amount;
            }

            //calculate tax if enabled
            if(!empty($payment_enable_tax) && !empty($payment_tax_rate)){
                $payment_tax_amount = ($payment_tax_rate / 100) * $payment_amount;
                $payment_tax_amount = round($payment_tax_amount,2); //round to 2 digits decimal
                $payment_amount += $payment_tax_amount;
            }

            $payment_currency = $form_payment_currency;
        }

        switch ($payment_currency) {
            case 'USD' : $currency_symbol = '&#36;';break;
            case 'EUR' : $currency_symbol = '&#8364;';break;
            case 'GBP' : $currency_symbol = '&#163;';break;
            case 'AUD' : $currency_symbol = '&#36;';break;
            case 'CAD' : $currency_symbol = '&#36;';break;
            case 'JPY' : $currency_symbol = '&#165;';break;
            case 'THB' : $currency_symbol = '&#3647;';break;
            case 'HUF' : $currency_symbol = '&#70;&#116;';break;
            case 'CHF' : $currency_symbol = 'CHF';break;
            case 'CZK' : $currency_symbol = '&#75;&#269;';break;
            case 'SEK' : $currency_symbol = 'kr';break;
            case 'DKK' : $currency_symbol = 'kr';break;
            case 'PHP' : $currency_symbol = '&#36;';break;
            case 'MYR' : $currency_symbol = 'RM';break;
            case 'PLN' : $currency_symbol = '&#122;&#322;';break;
            case 'BRL' : $currency_symbol = 'R&#36;';break;
            case 'HKD' : $currency_symbol = '&#36;';break;
            case 'MXN' : $currency_symbol = 'Mex&#36;';break;
            case 'TWD' : $currency_symbol = 'NT&#36;';break;
            case 'TRY' : $currency_symbol = 'TL';break;
            case 'NZD' : $currency_symbol = '&#36;';break;
            case 'SGD' : $currency_symbol = '&#36;';break;
            default: $currency_symbol = ''; break;
        }
    }


    //get entry details for particular entry_id
    $param['checkbox_image'] = 'images/icons/59_blue_16.png';
    $entry_details = mf_get_entry_details($dbh,$form_id,$entry_id,$param);
    //get entry information (date created/updated/ip address)
    $query = "select
                        date_format(date_created,'%e %b %Y - %r') date_created,
                        date_format(date_updated,'%e %b %Y - %r') date_updated,
                        ip_address
                    from
                        `".MF_TABLE_PREFIX."form_{$form_id}`
                where id=?";
    $params = array($entry_id);

    $sth = mf_do_query($query,$params,$dbh);
    $row = mf_do_fetch_result($sth);

    $date_created = $row['date_created'];
    if(!empty($row['date_updated'])){
        $date_updated = $row['date_updated'];
    }else{
        $date_updated = '&nbsp;';
    }
    $ip_address   = $row['ip_address'];

    //check for any 'signature' field, if there is any, we need to include the javascript library to display the signature
    $query = "select
                        count(form_id) total_signature_field
                    from
                        ".MF_TABLE_PREFIX."form_elements
                   where
                        element_type = 'signature' and
                        element_status=1 and
                        form_id=?";
    $params = array($form_id);


	function replaceLinks($value) {
		$linktext = preg_replace('/(([a-zA-Z]+:\/\/)([a-zA-Z0-9?&%.;:\/=+_-]*))/i', "<a href=\"$1\" target=\"_blank\">$1</a>", $value);
		return $linktext;
	}

	$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
	mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
	$query = "SELECT * FROM ap_form_".$application->getFormId()." WHERE id = '".$application->getEntryId()."'";
	$result = mysql_query($query,$dbconn);

	$application_form = mysql_fetch_assoc($result);

?>
<!-- OTB patch - Find out why jquery is not loaded from main layout -->
<script src="<?php echo public_path(); ?>assets_unified/js/jquery-1.11.1.min.js"></script>
<div class="pageheader">
  <h2><i class="fa fa-envelope"></i> Tasks <span>View task details</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>backend.php">Home</a></li>
      <li><a href="<?php echo public_path(); ?>backend.php/tasks/list">Tasks</a></li>
      <li><a href="<?php echo public_path(); ?>backend.php/tasks/list"><?php echo $task->getTypeName(); ?> Task</a></li>
      <li class="active"><?php echo $application->getApplicationId(); ?></li>
    </ol>
  </div>
</div>

<div class="contentpanel">

    <div class="row">
    <div class="col-sm-12 col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-comments"></i>Formulae
							</div>
							
						</div>
						<div class="portlet-body">
							<div class="table-scrollable">
								<table class="table table-bordered table-hover">
								<thead>
								<tr>
									<th>
										 #
									</th>
									<th>
										 Fees
									</th>
									<th>
										 VAT
									</th>
									
								</tr>
								</thead>
								<tbody>
								<tr class="active">
									<td>
										 1
									</td>
									<td>
                                    ( (Rate * Type_of_Development ) *0.1/100)
									</td>
									<td>
                                    ( (Rate * Type_of_Development ) *0.1/100) * 0.15
									</td>
									
								</tr>
								
								
								</tbody>
                                </table>
                               
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
    </div>
    </div>


    <div class="row">
    <div class="col-sm-12 col-md-12">
    <div class="portlet box black">
									<div class="portlet-title">
										<div class="caption">
                                            <i class="fa fa-money"></i> 
                                           <span class="caption-subject font-red-sunglo bold uppercase">
                                                Invoice Details - Fees Automatically calculated <span>
                                                
                                        </div>
                                        <div class="tools">
                                        <div>
                                            <a target="_blank" class="btn btn-success" href="/backend.php/applications/view/id/<?php echo $application->getId() ?>" id="view_app"><?php echo __("View Application Details"); ?></a>
                                            <a class="btn btn-warning" href="#" id="addfee"> <?php echo __('Add Fee') ?></a> 
                                            
                                        </div>
                                      </div>
										
									</div>
									<div class="portlet-body form">
                                        
										<!-- BEGIN FORM-->
										<form id="feeform" method="post" action="<?php echo public_path(); ?>backend.php/tasks/saveinvoice/id/<?php echo $task->getId(); ?>" id="MailContentForm" name="MailContentForm" onSubmit="return validate_editfield();" autocomplete="off" data-ajax="false" class="form-horizontal">
				
											<div class="form-body">
                                            <?php
									$grandtotal = 0;
                  $q = Doctrine_Query::create()
                     ->from('Invoicetemplates a')
                     ->where('a.applicationform = ?', $application->getFormId())
                     ->andWhere('a.applicationstage = ?', $application->getApproved());
                  $invoicetemplate = $q->fetchOne();
                  
                  error_log("Invoice Templates Application Form >>>>>>>>>>>>>> ".$application->getFormId());
                    error_log("Invoice Templates Application Approved >>>>>>>>>>>>>> ".$application->getApproved());

                  if($invoicetemplate)
                  {
                      
  									$q = Doctrine_Query::create()
  									   ->from('fee a')
                       				   ->where('a.invoiceid = ?', $invoicetemplate->getId());
  									$fixedfees = $q->execute();

									//OTB Start Patch - For Implementing Dynamic Fees
									$invoicemanager = new InvoiceManager();
									//OTB End Patch - For Implementing Dynamic Fees
  									foreach($fixedfees as $fee)
  									{
											//OTB Start Patch - For Implementing Dynamic Fees
											$fee_amount = $invoicemanager->getFeeAmount($fee, $application_form, $application->getFormId());
											//OTB End Patch - For Implementing Dynamic Fees
  										?>
  											<div class="form-group">
          										<label class="col-md-3 control-label">
          										<?php echo $fee->getFeeCode(); ?>: <?php echo $fee->getDescription(); ?>
                                                <input type='hidden' name='feetitle[]' value="<?php echo $fee->getFeeCode().": ".$fee->getDescription(); ?>">
                                                </label>
  												<div class="col-md-4">
  												<input class="form-control"  type='text' id='fee' name='feevalue[]' value="<?php echo round($fee_amount,0); ?>"><!--OTB Patch - Use fee amount from dynamic calculations-->
  												</div>
  											</div>
  											<?php
  											$grandtotal = $grandtotal + $fee_amount;//OTB Patch - Use fee amount from dynamic calculations
  									}

  									?>
  									<div class="form-group" id="otherfees">

                                      </div>
                                        
                                      <?php
                    $q = Doctrine_Query::create()
                       ->from("FeeCategory a")
                       ->orderBy("a.id ASC");
                    $categories = $q->execute();

                    foreach($categories as $category){

                      $feeselect .= "<optgroup label='".$category->getTitle()."'>";

                      $q = Doctrine_Query::create()
                         ->from("Fee a")
                         ->where("a.invoiceid = '' OR a.invoiceid = 0")
                         ->andWhere("a.fee_category = ?", $category->getId())
                         ->orderBy("a.fee_code ASC");
                      $fees = $q->execute();
                      $feeselect .= "<option value=''>Choose a fee</option>";
                      foreach($fees as $fee)
                      {
                        $feeselect .= "<option value='".$fee->getFeeCode().": ".$fee->getDescription()."'>".$fee->getFeeCode().": ".$fee->getDescription()."</option>";
                      }

                      $feeselect .= "</optgroup>";

                    }

                    $feeselect .= "<optgroup label='Others'>";

  									$q_fees = Doctrine_Query::create()
  									   ->from("Fee a")
  									   ->where("a.invoiceid = '' OR a.invoiceid = 0")
                       //->andWhere("a.fee_category = '' OR a.fee_category = 0")
  									   ->orderBy("a.fee_code ASC");
  									$fees = $q_fees->execute();
                                      $feeselect .= "<option value=''>Choose a fee</option>";
                                      
  									foreach($fees as $other_fee)
  									{
                                          error_log("Feesss<>>>>>>>>".$other_fee->getFeeCode());
  										$feeselect .= "<option value='".$other_fee->getFeeCode().": ".$other_fee->getDescription()."'>".$other_fee->getFeeCode().": ".$other_fee->getDescription()."</option>";
  									}

                    $feeselect .= "</optgroup>";
  									?>

  									<script language="javascript">
  									    function getFee(id, feecode)
  									    {
  									    	var xmlHttpReq1 = false;
											var self1 = this;
											// Mozilla/Safari

											if (window.XMLHttpRequest) {
													self.xmlHttpReq1 = new XMLHttpRequest();
											}
											// IE
											else if (window.ActiveXObject) {
													self.xmlHttpReq1 = new ActiveXObject("Microsoft.XMLHTTP");
											}
											self.xmlHttpReq1.open('POST', '/backend.php/fees/getfee', true);
											self.xmlHttpReq1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
											self.xmlHttpReq1.onreadystatechange = function() {
											  if (self.xmlHttpReq1.readyState == 4) {
											  	document.getElementById(id).value = self.xmlHttpReq1.responseText;
											  	updateTotal();
											  }
											}
											self.xmlHttpReq1.send('code' + '=' + feecode);
  									    }

  									    function updateTotal(amount)
  									    {
  									    	var total = 0;

											$("#feeform input[type=text]").each(function() {
												if($(this).attr('id') == 'servicefee' || $(this).attr('id') == 'totalfee')
												{

												}
												else
												{
													total = total + parseInt(this.value);
												}
											});

											var servicefee = 0;
                      if($("#servicefeepercentage").val())
                      {
											servicefee = (parseInt($("#servicefeepercentage").val())/100)*total;
											$("#servicefee").val(servicefee);
                      }
											total = total+ servicefee;
											$("#totalfee").val(total);
  									    }

										jQuery(document).ready(function(){
										  $("#addfee" ).click(function() {
										  	  $tmp = $.now();
											  $("#otherfees").append("<div class='form-group'><label class='col-md-3 control-label'><select name='feetitle[]' class='form-control' onChange='getFee(" + $tmp + ", this.value)'><?php echo $feeselect; ?></select></label><div class='col-md-4'> <input type='text' id='" + $tmp + "' onkeyup='updateTotal();' name='feevalue[]' class='form-control' /></div><a style='float: right; margin-top: 10px;' href='#' class='panel-close' onClick='$(this).closest(\"div\").remove(); updateTotal();'>&times;</a></div>");
										  });

										  $("#feeform input[id=fee]").bind("keyup input paste", function() {

										  		var total = 0;

												$("#feeform input[type=text]").each(function() {
												    if($(this).attr('id') == 'servicefee' || $(this).attr('id') == 'totalfee')
												    {

												    }
												    else
												    {
												    	total = total + parseInt(this.value);
												    }
												});


												var servicefee = 0;
                        if($("#servicefeepercentage").val())
                        {
  												servicefee = (parseInt($("#servicefeepercentage").val())/100)*total;
  												$("#servicefee").val(servicefee);
                        }
												total = total+ servicefee;
												$("#totalfee").val(total);
										   });

										});
									</script>

  									<?php
  									$servicefeepercentage = 0;
  									$servicefee = 0;

									//Get Service Fee (if set)
									if(sfConfig::get('app_service_fee_percentage'))
									{
  									   $servicefeepercentage = sfConfig::get('app_service_fee_percentage');
									   $servicefee = (sfConfig::get('app_service_fee_percentage')/100)*$grandtotal;
									   $grandtotal = $grandtotal + $servicefee;


									?>
									<div class="form-group">
										<label class="col-md-3 control-label">
										<>Service Fee
										<input type='hidden' name='feetitle[]' value="Service Fee">
										</label>
										<div class="col-md-4">
										<input class="form-control"  type='text' id='servicefee' name='feevalue[]' readonly='readonly' value="<?php echo $servicefee; ?>">
										</div>
									</div>

									<input type="hidden" id='servicefeepercentage' value='<?php echo $servicefeepercentage; ?>'>
									<?php
               }
									//Get Total
									?>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><i class="bold-label-2">Total Amount</i><input type='hidden' name='feetitle[]' value='Total' readonly></label>
										<div class="col-md-4">
										<input class="form-control" type='text' id='totalfee' name='feevalue[]' readonly='readonly' value="<?php echo round($grandtotal,0); ?>">
										</div>
									</div>
									<?php
								}
								?>
												
											</div>
											<div class="form-actions fluid">
												<div class="row">
													<div class="col-md-offset-3 col-md-9">
                                                    <?php

                                                        $q = Doctrine_Query::create()
                                                            ->from('SubMenuButtons a')
                                                            ->where('a.sub_menu_id = ?', $application->getApproved());
                                                        $submenubuttons = $q->execute();

                                                        foreach($submenubuttons as $submenubutton)
                                                        {
                                                            $q = Doctrine_Query::create()
                                                                ->from('Buttons a')
                                                                ->where('a.id = ?', $submenubutton->getButtonId());
                                                            $buttons = $q->execute();
                                                            foreach($buttons as $button)
                                                            {
                                                                if($sf_user->mfHasCredential("accessbutton".$button->getId()))
                                                                {

                                                                    if($button->getLink() == "/backend.php/circulations/generatepermit?language=en&permit=1&")
                                                                    {
                                                                            ?>

                                                                            <button class="btn btn-primary" type="submit" name="submit" value="<?php echo $button->getLink(); ?>&entryid=<?php echo $application->getId(); ?>&form_id=<?php echo $form_id; ?>&id=<?php echo $entry_id; ?>">Submit  &amp; <?php echo $button->getTitle(); ?></button>

                                                                            <?php

                                                                    }
                                                                    else
                                                                    {
                                                                ?>

                                                                <button class="btn btn-primary" type="submit" name="submit" value="<?php echo $button->getLink(); ?>&entryid=<?php echo $application->getId(); ?>&form_id=<?php echo $form_id; ?>&id=<?php echo $entry_id; ?>">Submit  &amp; <?php echo $button->getTitle(); ?></button>

                                                                <?php
                                                                    }
                                                                }
                                                            }
                                                        }
                                                       ?>

													</div>
												</div>
											</div>
										</form>
										<!-- END FORM-->
									</div>
								</div>

    </div>
</div>

