<?php
use_helper("I18N");

$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
?>
   <div class="pageheader">
      <h2><i class="fa fa-envelope"></i> <?php echo __("Tasks"); ?> <span><?php echo __("List of tasks assigned to reviewers"); ?></span></h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><?php echo __("You are here"); ?>:</span>
        <ol class="breadcrumb">
          <li><a href="<?php echo public_path(); ?>backend.php/dashboard"><?php echo __("Home"); ?></a></li>
          <li class="active"><?php echo __("Tasks"); ?></li>
        </ol>
      </div>
    </div>

    <div class="contentpanel panel-email">

        <ul class="nav nav-tabs nav-dark" style="background-color: #1d2939;">
            <li <?php //if(empty($_GET['mpage'])){ ?>class="active"<?php //} ?>><a data-toggle="tab" href="#available"><strong><?php echo __("Inbox"); ?> - <?php echo count($pager); ?> </strong></a></li>          <!--  <li <?php //if($_GET['mpage']){ ?>class="active"<?php //} ?>><a data-toggle="tab" href="#all"><strong><?php echo __("Inbox"); ?> - <?php //echo $pager->getNbResults(); ?></strong></a></li> -->
            <?php  if($sf_user->mfHasCredential("director_complete")) { ?> 
            <li><a data-toggle="tab" href="#approve"><strong><?php echo __("To Approve"); ?> - <?php echo count($toapprovetasks); ?></strong></a></li>
            <?php } ?>
             <li><a data-toggle="tab" href="#drafts"><strong><?php echo __("Drafts"); ?> - <?php echo $drafts_pager->getNbResults(); ?></strong></a></li>
             <?php  if($sf_user->mfHasCredential("assigntask")): ?> 
             <li><a data-toggle="tab" href="#assigned"><strong><?php echo __("Assigned To Others"); ?> - <?php echo count($assigned_pager) ?></strong></a></li>
            <?php endif; ?>
             <li><a data-toggle="tab" href="#resolved"><strong><?php echo __("Recently Completed"); ?> - <?php echo count($completed_pager); ?></strong></a></li>
            <!-- OTB patch - Show tasks user skipped -->
             <li><a data-toggle="tab" href="#skipped"><strong><?php echo __("Skipped"); ?> - <?php echo count($skippedtasks); ?></strong></a></li>
              <li><a data-toggle="tab" href="#archived"><strong><?php echo __("Archived"); ?> - <?php echo count($archivedtasks); ?></strong></a></li>
        </ul>
        <div class="tab-content">
        <!--<div id="available" class="tab-pane <?php// if(empty($_GET['mpage'])){ ?>active<?php //} ?>">
            <?php //include_partial('dashboard/tasks'); ?>
        </div> -->
            <?php 
            $otbhelper = new OTBHelper();
            ?>
       <div id="available" class="tab-pane <?php if(empty($_GET['mpage'])){ ?>active<?php } ?>">
            <p><strong><?php echo __('Note') ?>:</strong> <?php echo __('List of tasks available for you to work on') ?></p>
            <div style="float: right; font-size: 11px;">
                <img src='<?php echo public_path(); ?>asset_img/greenkey.png' /> <b> <?php echo __("Site Visit Done") ?> </b>              
                <img src='<?php echo public_path(); ?>asset_img/redkey.png' /> <b><?php echo __("Site Visit Pending")?></b>
                <img src='<?php echo public_path(); ?>asset_img/greykey.png' /> <b> <?php echo __("Site Visit Not Required") ?></b>
               </div>
            <div class="table-responsive">
            <table id="table1" class="table table-primary table-buglist">
            <thead>
            <tr>
                <th style="background-color: #428BCA;"><?php echo __("#"); ?></th>
                <th style="background-color: #428BCA;"><?php echo __("APPLICATION NO"); ?></th>
                <th style="background-color: #428BCA;"><?php echo __("UPI NO") ?></th>
                <th style="background-color: #428BCA;"><?php echo __("DESCRIPTION"); ?></th>
                <th style="background-color: #428BCA;"><?php echo __("ASSIGNED ON"); ?></th>
                <th style="background-color: #428BCA;"><?php echo __("SITE VISIT"); ?></th>
               <!-- <th style="background-color: #428BCA;"><?php //echo __("Priority"); ?></th> -->
                <th style="background-color: #428BCA;"><?php echo __("TASK STATUS"); ?></th>
                <th style="background-color: #428BCA;">ACTIONS</th>
                <th style="background-color: #428BCA;"><?php echo __("RESUBMITTED"); ?></th>
            </tr>
            </thead>
            <tbody>
                <?php $c = 1 ; ?>
            <?php foreach ($pager as $task): ?>
                <?php 
                  $color = "white" ;
                   if($task->getSiteVisitStatus() == 0)
                       { 
                       $color =  "red" ;
                       
                       }
                  else if ($task->getSiteVisitStatus() == 1)
                      { 
                      $color = "green"; 
                      
                      } 
                  else 
                      { 
                      $color = "grey" ;
                      
                      } 
                ?>
                <tr style="background-color:<?php echo $color ?> ; color: #FFFFFF !important;">
                <td>.</td>
                <td><a style="color: #FFFFFF !important;" href="/backend.php/tasks/view/id/<?php echo $task->getId(); ?>"><?php echo $task->getApplication()->getApplicationId(); ?></a></td>
                 <td><a style="color: #FFFFFF !important;" href="/backend.php/tasks/view/id/<?php echo $task->getId(); ?>"><?php echo $otbhelper->getApplicationUPI($task->getApplication()->getFormId(),$task->getApplication()->getEntryId()); ?></a></td>
                <td><a style="color: #FFFFFF !important;" href="/backend.php/tasks/view/id/<?php echo $task->getId(); ?>"><?php echo ($task->getRemarks()?$task->getRemarks():$task->getTypeName()." task") ?></a></td>
                <td><?php echo $task->getDateCreated();?></td>
                <td><?php 
                      //OTB patch - get the assignee details
                    /* $q = Doctrine_Query::create()
                             ->from('CfUser c')
                             ->where('c.nid = ? ',$task->getOwnerUserId()) 
                             ->limit(1);
                     $assignee_r = $q->execute();
                     //
                     foreach($assignee_r as $r){
                            echo $r->getStrfirstname()." ".$r->getStrlastname() ;
                           } */
                if($task->getSiteVisitStatus() == 0 ){
                                    echo "<b>".__('Pending')."</b>" ;
                                }else if($task->getSiteVisitStatus() == 1){
                                   
                                     echo "<b>".__('Done')."</b>" ;
                                }else{
                                 
                                     echo "<b>".__('Not Applicable')."</b>" ;
                                }
                     ?>
                </td>
               <!-- <td><?php //echo $task->getPriorityName(); ?></td> -->
				<td><?php echo $task->getStatusName(); ?></td>
                <td> 
                    <a  title='<?php echo __('View Task'); ?>' href='<?php echo public_path("backend.php/tasks/view/id/".$task->getId()); ?>'> <span class="badge badge-primary"><i class="fa fa-eye"></i></span></a>
                </td>
                <td>
                <?php

                //check whether the application was previously in resubmissions stage 
                $application_id=$task->getApplicationId();
                    $previous_stages= Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc("SELECT stage_id FROM application_reference where application_id =".$application_id." ORDER BY id DESC LIMIT 1, 1" );
                    $previous_stage=$previous_stages[0]['stage_id'];
                    error_log("DEBUG:::::::::::::::::::::::::: Previous Stage: ".$previous_stage);
                    //var_dump($previous_stage);
                    
                if ($previous_stage && (previous_stage==52 || $previous_stage==53 || $previous_stage==54)){
                    ?>
                    
                    <span class="badge badge-primary"><i class="fa fa-check " aria-hidden="true"></i></span>
                    <?php
                }
                
                ?>
                </td>
            </tr>
            <?php $c++; ?>
            <?php endforeach; ?>
            </tbody>
            </table>
            </div><!-- table-responsive -->

           
        </div><!-- tab-pane -->

        <div id="drafts" class="tab-pane">
            <p><strong><?php echo __('Note') ?> :</strong> <?php echo __('List of tasks you have worked on but no marked as completed') ?></p>
            <div class="table-responsive">
                <table id="table2" class="table table-primary table-buglist">
                    <thead>
                    <tr>
                        <th style="background-color: #428BCA;"><?php echo __('#') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Key') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Description') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __("Assigned On"); ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Assigned To') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Priority') ?></th>
                		<th style="background-color: #428BCA;"><?php echo __('Status') ?></th>
                        <th style="background-color: #428BCA;"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($drafts_pager->getResults() as $task): ?>
                        <tr>
                            <td><i class="fa fa-tasks tooltips" data-toggle="tooltip" title="Bug"></i></td>
                            <td><a href="/backend.php/tasks/view/id/<?php echo $task->getId(); ?>"><?php echo $task->getApplication()->getApplicationId(); ?></a></td>
                            <td><a href="/backend.php/tasks/view/id/<?php echo $task->getId(); ?>"><?php echo ($task->getRemarks()?$task->getRemarks():$task->getTypeName()." task") ?></a></td>
                             <td><?php echo $task->getDateCreated();?></td>
                            <td><?php 
                                //OTB patch - get the assignee details
                               $q = Doctrine_Query::create()
                                       ->from('CfUser c')
                                       ->where('c.nid = ? ',$task->getOwnerUserId()) 
                                       ->limit(1);
                               $assignee_r = $q->execute();
                               //
                               foreach($assignee_r as $r){
                                      echo $r->getStrfirstname()." ".$r->getStrlastname() ;
                                     }
                               ?>
                           </td>
                           
                            <td><?php echo $task->getPriorityName(); ?></td>
							<td><?php echo $task->getStatusName(); ?></td>
                            <td>
                                <a  title='<?php echo __('View Task'); ?>' href='<?php echo public_path("backend.php/tasks/view/id/".$task->getId()); ?>'> <span class="badge badge-primary"><i class="fa fa-eye"></i></span></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div><!-- table-responsive -->

            <?php if ($pager->haveToPaginate()): ?>
                <ul class="pagination pagination-sm mb0 mt0 pull-right">
                    <li> <a href="/backend.php/tasks/list<?php if($filterdepartment){ ?>/filter/<?php echo $filterdepartment; } ?>/dpage/1">
                            <i class="fa fa-angle-left"></i>
                        </a></li>

                    <li> <a href="/backend.php/tasks/list<?php if($filterdepartment){ ?>/filter/<?php echo $filterdepartment; } ?>/dpage/<?php echo $pager->getPreviousPage() ?>">
                            <i class="fa fa-angle-left"></i>
                        </a></li>

                    <?php foreach ($pager->getLinks() as $page): ?>
                        <?php if ($page == $pager->getPage()): ?>
                            <li class="active"><a href=""><?php echo $page ?></a>
                        <?php else: ?>
                            <li><a href="/backend.php/tasks/list<?php if($filterdepartment){ ?>/filter/<?php echo $filterdepartment; } ?>/dpage/<?php echo $page ?>"><?php echo $page ?></a></li>
                        <?php endif; ?>
                    <?php endforeach; ?>

                    <li> <a href="/backend.php/tasks/list<?php if($filterdepartment){ ?>/filter/<?php echo $filterdepartment; } ?>/dpage/<?php echo $pager->getNextPage() ?>">
                            <i class="fa fa-angle-right"></i>
                        </a></li>

                    <li> <a href="/backend.php/tasks/list<?php if($filterdepartment){ ?>/filter/<?php echo $filterdepartment; } ?>/dpage/<?php echo $pager->getLastPage() ?>">
                            <i class="fa fa-angle-right"></i>
                        </a></li>
                </ul>
            <?php endif; ?>
        </div><!-- tab-pane -->
        <?php  if($sf_user->mfHasCredential("assigntask")): ?> 
        <div id="assigned" class="tab-pane">
            <p><strong><?php echo __('Note') ?>:</strong> <?php echo __('List of tasks you have assigned to others to work on') ?></p>
            <div class="table-responsive">
                <table id="table3" class="table table-primary table-buglist">
                    <thead>
                    <tr>
                        <th style="background-color: #428BCA;"><?php echo __('#') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Application No') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Description') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __("Assigned On"); ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Assigned To') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Priority') ?></th>
                		<th style="background-color: #428BCA;"><?php echo __('Status') ?></th>
                        <th style="background-color: #428BCA;"></th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php $count2 = 1 ; ?>
                    <?php foreach ($assigned_pager as $task): ?>
                        <tr>
                             <td>.</td>
                            <td><a href="/backend.php/tasks/view/id/<?php echo $task->getId(); ?>"><?php echo $task->getApplication()->getApplicationId(); ?></a></td>
                            <td><a href="/backend.php/tasks/view/id/<?php echo $task->getId(); ?>"><?php echo ($task->getRemarks()?$task->getRemarks():$task->getTypeName()." task") ?></a></td>
                             <td><?php echo $task->getDateCreated();?></td>
                            <td><?php 
                                //OTB patch - get the assignee details
                               $q = Doctrine_Query::create()
                                       ->from('CfUser c')
                                       ->where('c.nid = ? ',$task->getOwnerUserId()) 
                                       ->limit(1);
                               $assignee_r = $q->execute();
                               //
                               foreach($assignee_r as $r){
                                      echo $r->getStrfirstname()." ".$r->getStrlastname() ;
                                     }
                               ?>
                           </td>
                            <td><?php echo $task->getPriorityName(); ?></td>
							<td><?php echo $task->getStatusName(); ?></td>
                            <td>
                                <a  title='<?php echo __('View Task'); ?>' href='<?php echo public_path("backend.php/tasks/view/id/".$task->getId()); ?>'> <span class="badge badge-primary"><i class="fa fa-eye"></i></span></a>
                            </td>
                        </tr>
                        <?php $count2++ ; ?>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div><!-- table-responsive -->

           
        </div><!-- tab-pane -->
        <?php endif; ?>

        <div id="resolved" class="tab-pane">
            <p><strong>Note:</strong> List of task you recently marked as completed</p>
            <div class="table-responsive">
                <table id="table4" class="table table-primary table-buglist">
                    <thead>
                    <tr>
                        <th style="background-color: #428BCA;"><?php echo __('Type') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Key') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Description') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __("Completed On"); ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Assigned To') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Priority') ?></th>
                		<th style="background-color: #428BCA;"><?php echo __('Status') ?></th>
                        <th style="background-color: #428BCA;"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php 
                    $count3=1;
                    foreach ($completed_pager as $task):

                            $q = Doctrine_Query::create()
                               ->from("FormEntry a")
                               ->where("a.id = ?", $task->getApplicationId());
                            $application = $q->fetchOne();

                            if($application)
                            {
                              //do nothing
                            }
                            else {
                              continue;
                            }
                    ?>
                        <tr>
                            <td><i class="fa fa-tasks tooltips" data-toggle="tooltip" title="Bug"></i></td>
                            <td><a href="/backend.php/tasks/view/id/<?php echo $task->getId(); ?>"><?php echo $application->getApplicationId(); ?></a></td>
                            <td><a href="/backend.php/tasks/view/id/<?php echo $task->getId(); ?>"><?php echo ($task->getRemarks()?$task->getRemarks():$task->getTypeName()." task") ?></a></td>
                            <td><?php echo $task->getLastUpdate();?></td>
                            <td><?php 
                                //OTB patch - get the assignee details
                               $q = Doctrine_Query::create()
                                       ->from('CfUser c')
                                       ->where('c.nid = ? ',$task->getOwnerUserId()) 
                                       ->limit(1);
                               $assignee_r = $q->execute();
                               //
                               foreach($assignee_r as $r){
                                      echo $r->getStrfirstname()." ".$r->getStrlastname() ;
                                     }
                               ?>
                           </td>
                            <td><?php echo $task->getPriorityName(); ?></td>
							<td><?php echo $task->getStatusName(); ?></td>
                            <td>
                                <a  title='<?php echo __('View Task'); ?>' href='<?php echo public_path("backend.php/tasks/view/id/".$task->getId()); ?>'> <span class="badge badge-primary"><i class="fa fa-eye"></i></span></a>
                            </td>
                        </tr>
                    <?php 
                    $count3++;
                    endforeach; ?>
                    </tbody>
                </table>
            </div><!-- table-responsive -->

            
        </div><!-- tab-pane -->
        
        <!-- OTB patch - Tasks that have been skipped by you / all users depending on user permission -->
        <?php 
           $otbhelp = new OTBHelper();                   
        ?>
        
        <div id="skipped" class="tab-pane">
            <p><strong>Note:</strong> List of tasks skipped</p>
            <div class="table-responsive">
                <table id="table6"class="table table-primary table-buglist">
                    <thead>
                    <tr>
                        <th style="background-color: #428BCA;"><?php echo __('Type') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Key') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Description') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Assigned On') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Assigned To') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Priority') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Status') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Skipped by') ?></th>
                        <th style="background-color: #428BCA;"></th>
                    </tr>
                    </thead>
                    <tbody>
                        
                    <?php foreach ($skippedtasks as $task){

                            $q = Doctrine_Query::create()
                               ->from("FormEntry a")
                               ->where("a.id = ?", $task->getApplicationId());
                            $application = $q->fetchOne();

                            if($application)
                            {
                              //do nothing
                            }
                            else {
                              continue;
                            }
                    ?>
                        <tr>
                            <td><i class="fa fa-tasks tooltips" data-toggle="tooltip" title="Bug"></i></td>
                            <td><a href="/backend.php/tasks/view/id/<?php echo $task->getId(); ?>"><?php echo $application->getApplicationId(); ?></a></td>
                            <td><a href="/backend.php/tasks/view/id/<?php echo $task->getId(); ?>"><?php echo ($task->getRemarks()?$task->getRemarks():$task->getTypeName()." task") ?></a></td>
                            <td><?php echo $task->getDateCreated();?></td>
                            <td><?php 
                                //OTB patch - get the assignee details
                               $q = Doctrine_Query::create()
                                       ->from('CfUser c')
                                       ->where('c.nid = ? ',$task->getOwnerUserId()) 
                                       ->limit(1);
                               $assignee_r = $q->execute();
                               //
                               foreach($assignee_r as $r){
                                      echo $r->getStrfirstname()." ".$r->getStrlastname() ;
                                     }
                               ?>
                           </td>
                            <td><?php echo $task->getPriorityName(); ?></td>
			    <td><?php
                            
                            if($task->getStatus() == 75){
                                echo "Skipped" ;
                            }else {
                                echo "N/A";
                            }
                            
                            ?></td>
                            <td>
                                <?php 
                                $user_detail = $otbhelp->getReviewerInfor($task->getSkippedBy()) ; 
                                if($user_detail){
                                     echo $user_detail->getStrfirstname()." ".$user_detail->getStrlastname() ;
                                }else {
                                    echo "N/A" ;
                                }
                               
                                ?>
                            </td>
                            <td>
                                <a  title='<?php echo __('View Task'); ?>' href='<?php echo public_path("backend.php/tasks/view/id/".$task->getId()); ?>'> <span class="badge badge-primary"><i class="fa fa-eye"></i></span></a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div><!-- table-responsive -->

            <?php if ($pager->haveToPaginate()): ?>
                <ul class="pagination pagination-sm mb0 mt0 pull-right">
                    <li> <a href="/backend.php/tasks/list<?php if($filterdepartment){ ?>/filter/<?php echo $filterdepartment; } ?>/cpage/1">
                            <i class="fa fa-angle-left"></i>
                        </a></li>

                    <li> <a href="/backend.php/tasks/list<?php if($filterdepartment){ ?>/filter/<?php echo $filterdepartment; } ?>/cpage/<?php echo $pager->getPreviousPage() ?>">
                            <i class="fa fa-angle-left"></i>
                        </a></li>

                    <?php foreach ($pager->getLinks() as $page): ?>
                        <?php if ($page == $pager->getPage()): ?>
                            <li class="active"><a href=""><?php echo $page ?></a>
                        <?php else: ?>
                            <li><a href="/backend.php/tasks/list<?php if($filterdepartment){ ?>/filter/<?php echo $filterdepartment; } ?>/cpage/<?php echo $page ?>"><?php echo $page ?></a></li>
                        <?php endif; ?>
                    <?php endforeach; ?>

                    <li> <a href="/backend.php/tasks/list<?php if($filterdepartment){ ?>/filter/<?php echo $filterdepartment; } ?>/cpage/<?php echo $pager->getNextPage() ?>">
                            <i class="fa fa-angle-right"></i>
                        </a></li>

                    <li> <a href="/backend.php/tasks/list<?php if($filterdepartment){ ?>/filter/<?php echo $filterdepartment; } ?>/cpage/<?php echo $pager->getLastPage() ?>">
                            <i class="fa fa-angle-right"></i>
                        </a></li>
                </ul>
            <?php endif; ?>
        </div><!-- tab-pane -->
        
        
         <div id="archived" class="tab-pane">
            <p><strong>Note:</strong> List of tasks archived</p>
            <div class="table-responsive">
                <table id="table5" class="table table-primary table-buglist">
                    <thead>
                    <tr>
                        <th style="background-color: #428BCA;"><?php echo __('Type') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Key') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Description') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Completed On') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Assigned To') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Priority') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Status') ?></th>              
                        <th style="background-color: #428BCA;"></th>
                    </tr>
                    </thead>
                    <tbody>
                        
                    <?php foreach ($archivedtasks as $task){

                            $q = Doctrine_Query::create()
                               ->from("FormEntry a")
                               ->where("a.id = ?", $task->getApplicationId());
                            $application = $q->fetchOne();

                            if($application)
                            {
                              //do nothing
                            }
                            else {
                              continue;
                            }
                    ?>
                        <tr>
                            <td><i class="fa fa-tasks tooltips" data-toggle="tooltip" title="Bug"></i></td>
                            <td><a href="/backend.php/tasks/view/id/<?php echo $task->getId(); ?>"><?php echo $application->getApplicationId(); ?></a></td>
                            <td><a href="/backend.php/tasks/view/id/<?php echo $task->getId(); ?>"><?php echo ($task->getRemarks()?$task->getRemarks():$task->getTypeName()." task") ?></a></td>
                            <td><?php echo $task->getLastUpdate();?></td>
                            <td><?php 
                                //OTB patch - get the assignee details
                               $q = Doctrine_Query::create()
                                       ->from('CfUser c')
                                       ->where('c.nid = ? ',$task->getOwnerUserId()) 
                                       ->limit(1);
                               $assignee_r = $q->execute();
                               //
                               foreach($assignee_r as $r){
                                      echo $r->getStrfirstname()." ".$r->getStrlastname() ;
                                     }
                               ?>
                           </td>
                            <td><?php echo $task->getPriorityName(); ?></td>
			    <td><?php
                            
                            if($task->getStatus() == 5){
                                echo "Archived" ;
                            }else {
                                echo "N/A";
                            }
                            
                            ?></td>
                            
                            <td>
                                <a  title='<?php echo __('View Task'); ?>' href='<?php echo public_path("backend.php/tasks/view/id/".$task->getId()); ?>'> <span class="badge badge-primary"><i class="fa fa-eye"></i></span></a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div><!-- table-responsive -->

            <?php if ($archivedtasks->haveToPaginate()): ?>
                <ul class="pagination pagination-sm mb0 mt0 pull-right">
                    <li> <a href="/backend.php/tasks/list<?php if($filterdepartment){ ?>/filter/<?php echo $filterdepartment; } ?>/cpage/1">
                            <i class="fa fa-angle-left"></i>
                        </a></li>

                    <li> <a href="/backend.php/tasks/list<?php if($filterdepartment){ ?>/filter/<?php echo $filterdepartment; } ?>/cpage/<?php echo $pager->getPreviousPage() ?>">
                            <i class="fa fa-angle-left"></i>
                        </a></li>

                    <?php foreach ($archivedtasks->getLinks() as $page): ?>
                        <?php if ($page == $archivedtasks->getPage()): ?>
                            <li class="active"><a href=""><?php echo $page ?></a>
                        <?php else: ?>
                            <li><a href="/backend.php/tasks/list<?php if($filterdepartment){ ?>/filter/<?php echo $filterdepartment; } ?>/cpage/<?php echo $page ?>"><?php echo $page ?></a></li>
                        <?php endif; ?>
                    <?php endforeach; ?>

                    <li> <a href="/backend.php/tasks/list<?php if($filterdepartment){ ?>/filter/<?php echo $filterdepartment; } ?>/cpage/<?php echo $archivedtasks->getNextPage() ?>">
                            <i class="fa fa-angle-right"></i>
                        </a></li>

                    <li> <a href="/backend.php/tasks/list<?php if($filterdepartment){ ?>/filter/<?php echo $filterdepartment; } ?>/cpage/<?php echo $archivedtasks->getLastPage() ?>">
                            <i class="fa fa-angle-right"></i>
                        </a></li>
                </ul>
            <?php endif; ?>
        </div><!-- tab-pane -->
        <?php  if($sf_user->mfHasCredential("director_complete")): ?> 
        <div id="approve" class="tab-pane">
            <p><strong>Note:</strong> List of Tasks waiting approval</p>
            <div class="table-responsive">
                <table id="table7" class="table table-primary table-buglist">
                    <thead>
                    <tr>
                        <th style="background-color: #428BCA;"><?php echo __('#') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Application No') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Description') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Completed On') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Assigned To') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Priority') ?></th>
                        <th style="background-color: #428BCA;"><?php echo __('Status') ?></th>              
                        <th style="background-color: #428BCA;"></th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php 
                         $count = 1 ;
                        ?>
                    <?php foreach ($toapprovetasks as $task){

                            $q = Doctrine_Query::create()
                               ->from("FormEntry a")
                               ->where("a.id = ?", $task->getApplicationId());
                            $application = $q->fetchOne();

                            if($application)
                            {
                              //do nothing
                            }
                            else {
                              continue;
                            }
                    ?>
                        <tr>
                             <td>.</td>
                            <td><a href="/backend.php/tasks/view/id/<?php echo $task->getId(); ?>"><?php echo $application->getApplicationId(); ?></a></td>
                            <td><a href="/backend.php/tasks/view/id/<?php echo $task->getId(); ?>"><?php echo ($task->getRemarks()?$task->getRemarks():$task->getTypeName()." task") ?></a></td>
                            <td><?php echo $task->getLastUpdate();?></td>
                            <td><?php 
                                //OTB patch - get the assignee details
                               $q = Doctrine_Query::create()
                                       ->from('CfUser c')
                                       ->where('c.nid = ? ',$task->getOwnerUserId()) 
                                       ->limit(1);
                               $assignee_r = $q->execute();
                               //
                               foreach($assignee_r as $r){
                                      echo $r->getStrfirstname()." ".$r->getStrlastname() ;
                                     }
                               ?>
                           </td>
                            <td><?php echo $task->getPriorityName(); ?></td>
			    <td><?php
                            
                            if($task->getStatus() == 2){
                                echo "Completing" ;
                            }else {
                                echo "N/A";
                            }
                            
                            ?></td>
                            
                            <td>
                                <a  title='<?php echo __('View Task'); ?>' href='<?php echo public_path("backend.php/tasks/view/id/".$task->getId()); ?>'> <span class="badge badge-primary"><i class="fa fa-eye"></i></span></a>
                            </td>
                        </tr>
                        <?php $count++ ; ?>
                    <?php } ?>
                    </tbody>
                </table>
            </div><!-- table-responsive -->

            <?php //if ($toapprovetasks->haveToPaginate()): ?>
                <!--<ul class="pagination pagination-sm mb0 mt0 pull-right">
                    <li> <a href="/backend.php/tasks/list<?php //if($filterdepartment){ ?>/filter/<?php //echo $filterdepartment; } ?>/cpage/1">
                            <i class="fa fa-angle-left"></i>
                        </a></li>

                    <li> <a href="/backend.php/tasks/list<?php //if($filterdepartment){ ?>/filter/<?php// echo $filterdepartment; } ?>/cpage/<?php //echo $toapprovetasks->getPreviousPage() ?>">
                            <i class="fa fa-angle-left"></i>
                        </a></li>

                    <?php //foreach ($toapprovetasks->getLinks() as $page): ?>
                        <?php //if ($page == $toapprovetasks->getPage()): ?>
                            <li class="active"><a href=""><?php //echo $page ?></a>
                        <?php //else: ?>
                            <li><a href="/backend.php/tasks/list<?php //if($filterdepartment){ ?>/filter/<?php //echo $filterdepartment; } ?>/cpage/<?php //echo $page ?>"><?php// echo $page ?></a></li>
                        <?php //endif; ?>
                    <?php //endforeach; ?>

                    <li> <a href="/backend.php/tasks/list<?php //if($filterdepartment){ ?>/filter/<?php //echo $filterdepartment; } ?>/cpage/<?php //echo $toapprovetasks->getNextPage() ?>">
                            <i class="fa fa-angle-right"></i>
                        </a></li>

                    <li> <a href="/backend.php/tasks/list<?php //if($filterdepartment){ ?>/filter/<?php //echo $filterdepartment; } ?>/cpage/<?php //echo $toapprovetasks->getLastPage() ?>">
                            <i class="fa fa-angle-right"></i>
                        </a></li>
                </ul> -->
            <?php //endif; ?>
        </div><!-- tab-pane -->
        <?php endif ; ?>

        </div><!-- tab-content -->

    </div>

  </div><!-- mainpanel -->
