<?php
/**
 * PendingSuccess templates.
 *
 * Display pending tasks assigned to current user
 *
 * @package    backend
 * @subpackage tasks
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

function paginate($currentpage, $count)
{
	if($currentpage){
		$pagelimit = 20*$currentpage;
		$pagestart = $pagelimit - 20;
		
		if($count >= $pagestart && $count <= $pagelimit)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return true;
	}
}

//Initialize variables and filters
$_SESSION['taskcount'] = 0;
$_SESSION['totaltasks'] = 0;

$selectedpage = $_GET['page'];
if(empty($selectedpage))
{
	$selectedpage = 1;
}

$filterstatus = $_GET['filterstatus'];
if(!empty($filterstatus))
{
	$filter = " AND a.status = ".$filterstatus;
}
else
{
	$filterstatus =  "1";
	$filter = " AND a.status = 1";
}

$filterpriority = $_GET['filterpriority'];
if(!empty($filterpriority))
{
	if(!empty($filter))
	{
		$filter = $filter." AND a.priority = ".$filterpriority;
	}
	else
	{
		$filter = " AND a.priority = ".$filterpriority;
	}
}

$filterdepartment = $_GET['filterdepartment'];
if(!empty($filterdepartment))
{
	if(!empty($filter))
	{
		$filter = $filter." AND c.strdepartment = '".$filterdepartment."'";
	}
	else
	{
		$filter = " AND c.strdepartment = '".$filterdepartment."'";
	}
}
?>
<div class="g12" style="padding-left: 3px;">
<?php 
//Display the list header with notifications
include_partial('listheader', array('filter' => $filter,'filterdepartment' => $filterdepartment,'filterpriority' => $filterpriority,'filterstatus' => $filterstatus));

//List the tasks according to date.
include_partial('listitems', array('filter' => $filter,'filterform' => $filterform, 'selectedpage' => $selectedpage));

//Display the list footer.
include_partial('listfooter', array('filter' => $filter,'filterform' => $filterform, 'selectedpage' => $selectedpage));
?>
</div>
