<?php
/**
 * postponeSuccess templates.
 *
 * Postpones a tasks start-date or end-date to a different date
 *
 * @package    backend
 * @subpackage tasks
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

?>




<div class="pageheader">
<h2><i class="fa fa-envelope"></i>Tasks<span>Create an new client from here</span></h2>
<div class="breadcrumb-wrapper">
<span class="label">You are here:</span>
<ol class="breadcrumb">
<li><a href="">Home</a></li>
<li><a href="">tasks</a></li>
<li class="active">Stages</li>
</ol>
</div>
</div>			


<div class="contentpanel">
<div class="row">
    
  <div class="panel panel-dark">  
      <div class="panel-heading">
      <h3 class="panel-title">Postpone Task</h3>
      </div>

    <div class="panel-body panel-body-nopadding">

				<form class="form-bordered" id="form" action="<?php echo public_path(); ?>backend.php/tasks/postpone/id/<?php echo $task->getId(); ?>" method="post" autocomplete="off" data-ajax="false">
						<div class="form-group">
                        <label class="col-sm-4" for="text_field">Assigned Application</label>
							<div class="col-sm-8">
							<?php
							 
								$q = Doctrine_Query::create()
									->from('FormEntry a')
									->where('a.id = ?', $task->getApplicationId());
								$application = $q->fetchOne();
								
								$q = Doctrine_Query::create()
									 ->from('sfGuardUserProfile a')
									 ->where('a.user_id = ?', $application->getUserId());
								$userprofile = $q->fetchOne();
								
							?>
							<button class="btn btn-primary" OnClick="window.location = '<?php echo public_path(); ?>backend.php/applications/view/id/<?php echo $task->getApplicationId(); ?>';"><?php echo $application->getApplicationId()." (Submitted by ".$userprofile->getFullname().")" ?></button>
							</div>
						</div>
						<div class="form-group">
                        <label class="col-sm-4" for="text_tooltip">Start Date</label>
							<div class="col-sm-8">
                            <input type="text" id="start_date" name="start_date" class="form-control" value="<?php echo $task->getStartDate(); ?>">
							</div>
						</div>
					<div class="form-group">
                        <label class="col-sm-4" for="text_tooltip">End Date</label>
							<div class="col-sm-8">
                            <input type="text" id="end_date" name="end_date" class="form-control" value="<?php echo $task->getEndDate(); ?>">
							</div>
						</div>
						<div class="form-group">
                        <label class="col-sm-4" for="text_tooltip">Priority</label>
							<div class="col-sm-8">
							<select name="priority" id="priority" style="width:30%;">
							<option value="3" <?php if($task->getPriority() == "3"){ echo "selected"; } ?>>Normal</option>
							<option value="2" <?php if($task->getPriority() == "2"){ echo "selected"; } ?>>Important</option>
							<option value="1" <?php if($task->getPriority() == "1"){ echo "selected"; } ?>>Critical</option>
							</select>
							</div>
						</div>
				
     </div><!--panel-body-->

			<div class="panel-footer">
					<button class="btn btn-danger mr10">Reset</button>  <button class="btn btn-primary mr20" name="submitbuttonname" value="submitbuttonvalue">Submit</button>
			</div><!--panel-footer-->
                    </form>	

</div><!--panel-->              


</div> <!--panel-->     
</div>    