<?php
/**
 * saveSuccess templates.
 *
 * Displays success message if tasks have been successfully saved to the database
 *
 * @package    backend
 * @subpackage tasks
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

?>

<div class="g12" style="margin-top: 0px;">
	<form>
		<label style='height: 30px; margin-top: 0px;'>
			<div style='float: left; font-size: 20px; font-weight: 700;'>
			Tasks Saved
            </div>
<div style="float: right;">
	<button style="font-size: 12px;" onClick="window.location='<?php echo public_path(); ?>backend.php/tasks/pending';">Back To Pending Tasks</button>
</div>
</label>
</form>
</div>

<div class="g12">

<?php
if($success == true)
{
?>
<div class="alert success">Task saved succesfully.</div>
<?php
}
else
{
?>
<div class="alert warning">Task Could Not Be Saved.</div>
<?php
}
?>
</div>