<?php
/**
 * savecommentsheetSuccess templates.
 *
 * Save comment sheet to the database
 *
 * @package    backend
 * @subpackage tasks
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
	
$prefix_folder = dirname(__FILE__)."/../../../../..";

require $prefix_folder.'/lib/vendor/cp_workflow/config/db_config.inc.php';
require_once $prefix_folder.'/lib/vendor/cp_workflow/config/config.inc.php';
require_once $prefix_folder.'/lib/vendor/cp_workflow/language_files/language.inc.php';
require_once $prefix_folder.'/lib/vendor/cp_workflow/lib/datetime.inc.php';
require_once $prefix_folder.'/lib/vendor/cp_workflow/pages/send_circulation.php';
require_once $prefix_folder.'/lib/vendor/cp_workflow/pages/CCirculation.inc.php';

			
require $prefix_folder.'/lib/vendor/cp_workflow/config/db_config.inc.php';

$nConnection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
$nConnection2 = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
if ($nConnection)
{
	if (mysql_select_db(sfConfig::get('app_mysql_db'),$nConnection))
	{
		mysql_select_db(sfConfig::get('app_mysql_db'),$nConnection2);
		
		//-----------------------------------------------
		//--- Write user inputs to database
		//-----------------------------------------------
		
			//--- get the current decission state
			$strQuery = "SELECT ndecissionstate FROM cf_circulationprocess WHERE nid=".$_REQUEST["cpid"];
			$nResult = mysql_query($strQuery, $nConnection);
			if ($nResult)
			{
				if (mysql_num_rows($nResult) > 0)
				{
					$arrProcessInfo = mysql_fetch_array($nResult);
					
					if ($arrProcessInfo["ndecissionstate"] != 0)
					{
						$bAlreadySend = true;
					}
					else
					{
						$bAlreadySend = false;
					}
				}
			}
			
			if ($bAlreadySend == false)
			{
				$strQuery = "SELECT * FROM cf_circulationprocess WHERE nid=".$_REQUEST["cpid"];
				$nResult = mysql_query($strQuery, $nConnection);
				if ($nResult)
				{
					if (mysql_num_rows($nResult) > 0)
					{
						$arrProcessInfo = mysql_fetch_array($nResult);
					}
				}
				
				$arrRBOverview = "";		
				function addRB($RBGroup, $strMyName, $nMyState, $nFieldId, $nSlotId, $nFormId)
				{
					global $arrRBOverview;
					
					$arrRBOverview[$RBGroup][] = array( 'strmyname' => $strMyName, 
														'nmystate' => $nMyState,
														'nfieldid' => $nFieldId,
														'nslotid' => $nSlotId,
														'nformid' => $nFormId
														 );
				}
				
				$arrCBOverview = "";					
				function addCB($CBGroup, $strMyName, $nMyState, $nFieldId, $nSlotId, $nFormId)
				{
					global $arrCBOverview;
					$arrCBOverview[$CBGroup][] = array( 'strmyname' => $strMyName, 
														'nmystate' => $nMyState,
														'nfieldid' => $nFieldId,
														'nslotid' => $nSlotId,
														'nformid' => $nFormId
														 );
				}
				
				$arrCOMBOOverview = "";					
				function addCOMBO($RBGroup, $strMyName, $nMyState, $nFieldId, $nSlotId, $nFormId)
				{
					global $arrCOMBOOverview;
					
					$arrCOMBOOverview[$RBGroup][] = array( 'strmyname' => $strMyName, 
															'nmystate' => $nMyState,
															'nfieldid' => $nFieldId,
															'nslotid' => $nSlotId,
															'nformid' => $nFormId
															 );
				}
				
				while(list($key, $value) = each($_REQUEST))
				{
					global $arrRBContent;
					global $arrCBContent;
					
											
					$arrValues = explode("_", $key);
					if (sizeof($arrValues) > 1)
					{
						if ($arrValues[0] == 'RBName')
						{
							$nFieldId	= $arrValues[1];
							$nSlotId 	= $arrValues[2];
							$nFormId	= $arrValues[3];
							
							$nRBGroupID	= $arrValues[5];
							$nPosition 	= $arrValues[6];
							
							$nMyGroupID = $nFieldId.'_'.$nSlotId.'_'.$nFormId;
							
							$strMyKey = 'RBName_'.$nFieldId.'_'.$nSlotId.'_'.$nFormId.'_nRadiogroup_'.$nRBGroupID.'_'.$nPosition;
								
							$strReq = $nFieldId.'_'.$nSlotId.'_'.$nFormId.'_nRadiogroup_'.$nRBGroupID;
							$strValue = $_REQUEST["$strMyKey"];
							
						
							
							$arrRBContent[] = array ( 'strMyKey' => $strMyKey, 'strMyValue' => $strValue );
							
							$strState = $_REQUEST[$strReq];
							
							//check if comment exists
							$strCommentKey = 'comment_'.$nFieldId.'_'.$nSlotId.'_'.$nFormId.'_nRadiogroup';
							
							if($_REQUEST["$strCommentKey"])
							{  
								$q = Doctrine_Query::create()
								   ->from('Comments a')
								   ->where('a.field_id = ? AND a.slot_id = ? AND a.circulation_id = ?', array($nFieldId, $nSlotId, $nFormId));
								$comment = $q->fetchOne();
								if($comment)
								{
									$comment->setComment($_REQUEST["$strCommentKey"]);
									$comment->save();
								}
								else
								{
									$comment = new Comments();
									$comment->setFieldId($nFieldId);
									$comment->setSlotId($nSlotId);
									$comment->setCirculationId($nFormId);
									$comment->setFormId($_SESSION["SESSION_CUTEFLOW_USERID"]);
									$comment->setComment($_REQUEST["$strCommentKey"]);
									$comment->save();
								}
							}
							else
							{
								$q = Doctrine_Query::create()
								   ->from('Comments a')
								   ->where('a.field_id = ? AND a.slot_id = ? AND a.circulation_id = ?', array($nFieldId, $nSlotId, $nFormId));
								$comment = $q->fetchOne();
								if($comment)
								{
									$comment->delete();
								}
							}
							
							
							//check if condition exists
							$strConditionKey = 'condition_'.$nFieldId.'_'.$nSlotId.'_'.$nFormId.'_nRadiogroup';
							
							if($_REQUEST["$strConditionKey"])
							{
								$q = Doctrine_Query::create()
								   ->from('Conditions a')
								   ->where('a.field_id = ? AND a.slot_id = ? AND a.circulation_id = ?', array($nFieldId, $nSlotId, $nFormId));
								$condition = $q->fetchOne();
								if($condition)
								{
									$condition->setConditionText($_REQUEST["$strConditionKey"]);
									$condition->save();
								}
								else
								{
									$condition = new Conditions();
									$condition->setFieldId($nFieldId);
									$condition->setSlotId($nSlotId);
									$condition->setCirculationId($nFormId);
									$condition->setConditionText($_REQUEST["$strConditionKey"]);
									$condition->save();
								}
							}
							else
							{
								$q = Doctrine_Query::create()
								   ->from('Conditions a')
								   ->where('a.field_id = ? AND a.slot_id = ? AND a.circulation_id = ?', array($nFieldId, $nSlotId, $nFormId));
								$condition = $q->fetchOne();
								if($condition)
								{
									$condition->delete();
								}
							}
							
							addRB($nMyGroupID, $strValue, $strState, $nFieldId, $nSlotId, $nFormId);
							
							
						}							
						elseif ($arrValues[0] == 'CBName')
						{
							$nFieldId	= $arrValues[1];
							$nSlotId 	= $arrValues[2];
							$nFormId	= $arrValues[3];
							
							$nCBGroupID	= $arrValues[5];
							$nPosition 	= $arrValues[6];
							
							$nMyGroupID = $nFieldId.'_'.$nSlotId.'_'.$nFormId;
							
							$strMyKey = 'CBName_'.$nFieldId.'_'.$nSlotId.'_'.$nFormId.'_nCheckboxGroup_'.$nCBGroupID.'_'.$nPosition;
							$strReq = $nFieldId.'_'.$nSlotId.'_'.$nFormId.'_nCheckboxGroup_'.$nCBGroupID.'_'.$nPosition;
							$strValue = $_REQUEST["$strMyKey"];
							$arrCBContent[] = array ( 'strMyKey' => $strMyKey, 'strMyValue' => $strValue );
							$strState = $_REQUEST[$strReq];
							
							
							//check if comment exists
							$strCommentKey = 'comment_'.$nFieldId.'_'.$nSlotId.'_'.$nFormId.'_nCheckboxGroup';
							
							if($_REQUEST["$strCommentKey"])
							{
								$q = Doctrine_Query::create()
								   ->from('Comments a')
								   ->where('a.field_id = ? AND a.slot_id = ? AND a.circulation_id = ?', array($nFieldId, $nSlotId, $nFormId));
								$comment = $q->fetchOne();
								if($comment)
								{
									$comment->setComment($_REQUEST["$strCommentKey"]);
									$comment->save();
								}
								else
								{
									$comment = new Comments();
									$comment->setFieldId($nFieldId);
									$comment->setSlotId($nSlotId);
									$comment->setCirculationId($nFormId);
									$comment->setFormId($_SESSION["SESSION_CUTEFLOW_USERID"]);
									$comment->setComment($_REQUEST["$strCommentKey"]);
									$comment->save();
								}
							}
							else
							{
								$q = Doctrine_Query::create()
								   ->from('Comments a')
								   ->where('a.field_id = ? AND a.slot_id = ? AND a.circulation_id = ?', array($nFieldId, $nSlotId, $nFormId));
								$comment = $q->fetchOne();
								if($comment)
								{
									$comment->delete();
								}
							}
							
							//check if condition exists
							$strConditionKey = 'condition_'.$nFieldId.'_'.$nSlotId.'_'.$nFormId.'_nCheckboxGroup';
							
							if($_REQUEST["$strConditionKey"])
							{
								$q = Doctrine_Query::create()
								   ->from('Conditions a')
								   ->where('a.field_id = ? AND a.slot_id = ? AND a.circulation_id = ?', array($nFieldId, $nSlotId, $nFormId));
								$condition = $q->fetchOne();
								if($condition)
								{
									$condition->setConditionText($_REQUEST["$strConditionKey"]);
									$condition->save();
								}
								else
								{
									$condition = new Conditions();
									$condition->setFieldId($nFieldId);
									$condition->setSlotId($nSlotId);
									$condition->setCirculationId($nFormId);
									$condition->setConditionText($_REQUEST["$strConditionKey"]);
									$condition->save();
								}
							}
							else
							{
								$q = Doctrine_Query::create()
								   ->from('Conditions a')
								   ->where('a.field_id = ? AND a.slot_id = ? AND a.circulation_id = ?', array($nFieldId, $nSlotId, $nFormId));
								$condition = $q->fetchOne();
								if($condition)
								{
									$condition->delete();
								}
							}
							
							
							
							addCB($nMyGroupID, $strValue, $strState, $nFieldId, $nSlotId, $nFormId);
						}
						elseif ($arrValues[0] == 'COMBOName')
						{
							$nFieldId	= $arrValues[1];
							$nSlotId 	= $arrValues[2];
							$nFormId	= $arrValues[3];
							
							$nRBGroupID	= $arrValues[5];
							$nPosition 	= $arrValues[6];
							
							$nMyGroupID = $nFieldId.'_'.$nSlotId.'_'.$nFormId;
							
							$strMyKey = 'COMBOName_'.$nFieldId.'_'.$nSlotId.'_'.$nFormId.'_nCombobox_'.$nRBGroupID.'_'.$nPosition;
							
							$strReq = $nFieldId.'_'.$nSlotId.'_'.$nFormId.'_nComboboxV_'.$nRBGroupID;
							$strValue = $_REQUEST["$strMyKey"];
							$arrRBContent[] = array ( 'strMyKey' => $strMyKey, 'strMyValue' => $strValue );
							
							$strState = $_REQUEST[$strReq];
							
							//check if comment exists
							$strCommentKey = 'comment_'.$nFieldId.'_'.$nSlotId.'_'.$nFormId.'_nCombobox';
							
							if($_REQUEST["$strCommentKey"])
							{
								$q = Doctrine_Query::create()
								   ->from('Comments a')
								   ->where('a.field_id = ? AND a.slot_id = ? AND a.circulation_id = ?', array($nFieldId, $nSlotId, $nFormId));
								$comment = $q->fetchOne();
								if($comment)
								{
									$comment->setComment($_REQUEST["$strCommentKey"]);
									$comment->save();
								}
								else
								{
									$comment = new Comments();
									$comment->setFieldId($nFieldId);
									$comment->setSlotId($nSlotId);
									$comment->setCirculationId($nFormId);
									$comment->setFormId($_SESSION["SESSION_CUTEFLOW_USERID"]);
									$comment->setComment($_REQUEST["$strCommentKey"]);
									$comment->save();
								}
							}
							else
							{
								$q = Doctrine_Query::create()
								   ->from('Comments a')
								   ->where('a.field_id = ? AND a.slot_id = ? AND a.circulation_id = ?', array($nFieldId, $nSlotId, $nFormId));
								$comment = $q->fetchOne();
								if($comment)
								{
									$comment->delete();
								}
							}
							
							//check if condition exists
							$strConditionKey = 'condition_'.$nFieldId.'_'.$nSlotId.'_'.$nFormId.'_nCombobox';
							
							if($_REQUEST["$strConditionKey"])
							{
								$q = Doctrine_Query::create()
								   ->from('Conditions a')
								   ->where('a.field_id = ? AND a.slot_id = ? AND a.circulation_id = ?', array($nFieldId, $nSlotId, $nFormId));
								$condition = $q->fetchOne();
								if($condition)
								{
									$condition->setConditionText($_REQUEST["$strConditionKey"]);
									$condition->save();
								}
								else
								{
									$condition = new Conditions();
									$condition->setFieldId($nFieldId);
									$condition->setSlotId($nSlotId);
									$condition->setCirculationId($nFormId);
									$condition->setConditionText($_REQUEST["$strConditionKey"]);
									$condition->save();
								}
							}
							else
							{
								$q = Doctrine_Query::create()
								   ->from('Conditions a')
								   ->where('a.field_id = ? AND a.slot_id = ? AND a.circulation_id = ?', array($nFieldId, $nSlotId, $nFormId));
								$condition = $q->fetchOne();
								if($condition)
								{
									$condition->delete();
								}
							}
							
							
							
							addCOMBO($nMyGroupID, $strValue, $strState, $nFieldId, $nSlotId, $nFormId);
						}
						elseif ($arrValues[0] == 'FILEName')
						{
							$nFieldId	= $arrValues[1];
							$nSlotId 	= $arrValues[2];
							$nFormId	= $arrValues[3];
							$strMyKey	= $nFieldId.'_'.$nSlotId.'_'.$nFormId.'_9';
							$strMyREGKey= $nFieldId.'_'.$nSlotId.'_'.$nFormId.'_REG';
							$myREGEX = $_REQUEST[$strMyREGKey];
							
						
							$strMyFile = $_POST[$strMyKey][0];
							
							
							if($strMyFile != '' && $strMyFile != 'n')
							{		
								$value			= '---1---'.$nSlotId.'_'.$nFormId.'_'.$arrProcessInfo["ncirculationhistoryid"].'---'.$strMyFile.'rrrrr'.$myREGEX;
								
								$prefix_folder = dirname(__FILE__)."/../../../../../../html";
								$uploaddir = $prefix_folder.'/asset_uplds/'.$nSlotId.'_'.$nFormId.'_'.$arrProcessInfo["ncirculationhistoryid"].'_1/';
								
								mkdir($uploaddir, 0777);
								
								$uploadfile = $uploaddir.$strMyFile;
								
								$client_ip = $_SERVER["REMOTE_ADDR"];
								if($client_ip == "::1" || $client_ip == "")
								{
									$client_ip = "127.0.0.1";
								}
                                                                
                                                                $uploadfile = str_replace("-","_", $uploadfile);
								
								copy($prefix_folder.'/asset_uplds/applications/'.$client_ip.$strMyFile, $uploadfile);
								
                                                                $strMyFile = str_replace("-","_", $strMyFile);
                                                                
                                                                $value	= '---1---'.$nSlotId.'_'.$nFormId.'_'.$arrProcessInfo["ncirculationhistoryid"].'---'.$strMyFile.'rrrrr'.$myREGEX;
                                                                
								$strQuery = "SELECT nid FROM cf_fieldvalue WHERE ninputfieldid=$nFieldId AND nslotid=$nSlotId AND nformid=$nFormId AND ncirculationhistoryid=".$arrProcessInfo["ncirculationhistoryid"]." AND nuserid='".$_SESSION["SESSION_CUTEFLOW_USERID"]."' AND nrevisionid='".$_POST['taskid']."'";
								$nResult = mysql_query($strQuery, $nConnection);
								
								if ($nResult)
								{
									if (mysql_num_rows($nResult) > 0)
									{
										$strQuery = "UPDATE cf_fieldvalue SET strfieldvalue='$value' WHERE ninputfieldid=".$arrValues[1]." AND nslotid=".$arrValues[2]." AND nformid=".$arrProcessInfo["ncirculationformid"]." AND ncirculationhistoryid=".$arrProcessInfo["ncirculationhistoryid"]." AND nuserid='".$_SESSION["SESSION_CUTEFLOW_USERID"]."' AND nrevisionid='".$_POST['taskid']."'";
									}
									else
									{
										
										$q = Doctrine_Query::create()
										   ->from('cfUser a')
										   ->where('a.nid = ?', $_SESSION["SESSION_CUTEFLOW_USERID"]);
										$user = $q->fetchOne();
										
										$strQuery = "INSERT INTO cf_fieldvalue values(null, ".$arrValues[1].", '$value', ".$arrValues[2].", ".$arrProcessInfo["ncirculationformid"].",".$arrProcessInfo["ncirculationhistoryid"].", ".$user->getNid().", ".$_POST['taskid'].", '".date("Y-m-d")."')";
									}
								}
								mysql_query($strQuery, $nConnection);
							}
						}
						else
						{								
							//--- Test if value already exists
							$nFieldId 	= $arrValues[0];
							$nSlotId 	= $arrValues[1];
							$nFormId 	= $arrValues[2];
							$nFieldType = $arrValues[3];
							$nFieldContentType = $arrValues[4];
							$nMyKey = $nFieldId.'_'.$nSlotId.'_'.$nFormId;
							
							switch ($nFieldType)
							{
								case '1':
									$curKey = $nMyKey.'_REG';
									$myREGEX = $_REQUEST[$curKey];
									if ($myREGEX!='')
									{
										$value	= $value.'rrrrr'.$myREGEX;
									}
									else
									{
										$value = $value;
									}
									break;
								case '2xx':
									$arrMyKey = split('xx',$key);
									
									$strMyKey = $arrMyKey[0];
									
									if ($_REQUEST[$strMyKey] == 'on')
									{
										$value	= 'on';
									}
									else
									{
										$value	= '';
										
										$anotherKey = $strMyKey.'_hidden';
										if ($_REQUEST[$anotherKey] == 'on')
										{
											$value	= 'on';
										}
									}
									break;
								case '3':
									$curKey = $nMyKey.'_REG';
									$myREGEX = $_REQUEST[$curKey];
									if ($myREGEX!='')
									{
										$value	= 'xx'.$nFieldContentType.'xx'.$value.'rrrrr'.$myREGEX;
									}
									else
									{
										$value	= 'xx'.$nFieldContentType.'xx'.$value;
									}
									break;
								case '4':
									$curKey = $nMyKey.'_REG';
									$myREGEX = $_REQUEST[$curKey];
									if ($myREGEX!='')
									{
										$value 	= 'xx'.$nFieldContentType.'xx'.$value.'rrrrr'.$myREGEX;
									}
									else
									{
										$value 	= 'xx'.$nFieldContentType.'xx'.$value;
									}
									break;
								case '5':
									$value = str_replace("\"", "\\\"", $value);
									$value = str_replace("'", "\\'", $value);
									break;
							}
							
							if (($nFieldType == 1) || ($nFieldType == '2xx') || ($nFieldType == 3) || ($nFieldType == 4) || ($nFieldType == 5))
							{
								$strQuery = "SELECT nid FROM cf_fieldvalue WHERE ninputfieldid=$nFieldId AND nslotid=$nSlotId AND nformid=$nFormId AND ncirculationhistoryid=".$arrProcessInfo["ncirculationhistoryid"]." AND nuserid='".$_SESSION["SESSION_CUTEFLOW_USERID"]."' AND nrevisionid='".$_POST['taskid']."'";
								$nResult = mysql_query($strQuery, $nConnection);
								
								if ($nResult)
								{
									if (mysql_num_rows($nResult) > 0)
									{
										$strQuery = "UPDATE cf_fieldvalue SET strfieldvalue='$value' WHERE ninputfieldid=".$arrValues[0]." AND nslotid=".$arrValues[1]." AND nformid=".$arrProcessInfo["ncirculationformid"]." AND ncirculationhistoryid=".$arrProcessInfo["ncirculationhistoryid"]." AND nuserid='".$_SESSION["SESSION_CUTEFLOW_USERID"]."' AND nrevisionid='".$_POST['taskid']."'";
									}
									else
									{
										
										$q = Doctrine_Query::create()
										   ->from('cfUser a')
										   ->where('a.nid = ?', $_SESSION["SESSION_CUTEFLOW_USERID"]);
										$user = $q->fetchOne();
										$strQuery = "INSERT INTO cf_fieldvalue values(null, ".$arrValues[0].", '$value', ".$arrValues[1].", ".$arrProcessInfo["ncirculationformid"].",".$arrProcessInfo["ncirculationhistoryid"].", ".$user->getNid().",  ".$_POST['taskid'].", '".date("Y-m-d")."')";
									}
								}
								mysql_query($strQuery, $nConnection);
							}
						}
						
					}
				}
				
				$strCrazyValue = '';
				global $arrRBOverview;
				if (sizeof($arrRBOverview) > 0)
				{						
					foreach($arrRBOverview as $arrCurRBOverview)
					{
						
						$nAmount = sizeof($arrCurRBOverview);
						//$strCrazyValue	= '---'.$nAmount;
						$strCrazyValue = '';
						$nCounter = 0;
						
						foreach($arrCurRBOverview as $arrCurRBEntries)
						{
							
							$strCurName = $arrCurRBEntries['strmyname'];
							$nCurState	= 0;
							if ($arrCurRBEntries['nmystate'] == $nCounter)
							{
								$nCurState = 1;
							}
							
							
							
							$nFieldId	= $arrCurRBEntries['nfieldid'];
							$nSlotId	= $arrCurRBEntries['nslotid'];
							$nFormId	= $arrCurRBEntries['nformid'];
							
							//$strCrazyValue = $strCrazyValue.'---'.$strCurName.'---'.$nCurState;
							$strCrazyValue = $strCrazyValue.$nCurState.'---';
							$nCounter++;
						}
						
						$strQuery = "SELECT nid FROM cf_fieldvalue WHERE ninputfieldid=$nFieldId AND nslotid=$nSlotId AND nformid=$nFormId AND ncirculationhistoryid=".$arrProcessInfo["ncirculationhistoryid"]." AND nuserid='".$_SESSION["SESSION_CUTEFLOW_USERID"]."' AND nrevisionid='".$_POST['taskid']."'";
						$nResult = mysql_query($strQuery, $nConnection);
						
						
						if ($nResult)
						{
							if (mysql_num_rows($nResult) > 0)
							{
								$strQuery = "UPDATE cf_fieldvalue SET strfieldvalue='$strCrazyValue' WHERE ninputfieldid= '$nFieldId' AND nslotid= '$nSlotId' AND nformid=".$arrProcessInfo["ncirculationformid"]." AND ncirculationhistoryid=".$arrProcessInfo["ncirculationhistoryid"]." AND nuserid='".$_SESSION["SESSION_CUTEFLOW_USERID"]."' AND nrevisionid='".$_POST['taskid']."'";
							}
							else
							{
								
										$q = Doctrine_Query::create()
										   ->from('CfUser a')
										   ->where('a.nid = ?', $_SESSION["SESSION_CUTEFLOW_USERID"]);
										$user = $q->fetchOne();
										
								$strQuery = "INSERT INTO cf_fieldvalue values(null, '$nFieldId', '$strCrazyValue', '$nSlotId', ".$arrProcessInfo["ncirculationformid"].",".$arrProcessInfo["ncirculationhistoryid"].", ".$user->getNid().",  ".$_POST['taskid'].", '".date('Y-m-d')."')";
							}
						}
						mysql_query($strQuery, $nConnection);
					}
				}
				
				$strCrazyValue = '';
				global $arrCBOverview;
				if (sizeof($arrCBOverview) > 0)
				{
					foreach($arrCBOverview as $arrCurRBOverview)
					{
						$nAmount = sizeof($arrCurRBOverview);
						//$strCrazyValue	= '---'.$nAmount;
						$strCrazyValue = '';
						
						foreach($arrCurRBOverview as $arrCurRBEntries)
						{
							$strCurName = $arrCurRBEntries['strmyname'];
							$nCurState	= 0;
							if ($arrCurRBEntries['nmystate'] == '1')
							{
								$nCurState = 1;
							}
							$nFieldId	= $arrCurRBEntries['nfieldid'];
							$nSlotId	= $arrCurRBEntries['nslotid'];;
							$nFormId	= $arrCurRBEntries['nformid'];;
							
							//$strCrazyValue = $strCrazyValue.'---'.$strCurName.'---'.$nCurState;
							$strCrazyValue = $strCrazyValue.$nCurState.'---';
						}
						
						$strQuery = "SELECT nid FROM cf_fieldvalue WHERE ninputfieldid=$nFieldId AND nslotid=$nSlotId AND nformid=$nFormId AND ncirculationhistoryid=".$arrProcessInfo["ncirculationhistoryid"]." AND nuserid='".$_SESSION["SESSION_CUTEFLOW_USERID"]."' AND nrevisionid='".$_POST['taskid']."'";
						$nResult = mysql_query($strQuery, $nConnection);
						
						if ($nResult)
						{
							if (mysql_num_rows($nResult) > 0)
							{
								$strQuery = "UPDATE cf_fieldvalue SET strfieldvalue='$strCrazyValue' WHERE ninputfieldid= '$nFieldId' AND nslotid= '$nSlotId' AND nformid=".$arrProcessInfo["ncirculationformid"]." AND ncirculationhistoryid=".$arrProcessInfo["ncirculationhistoryid"]." AND nuserid='".$_SESSION["SESSION_CUTEFLOW_USERID"]."' AND nrevisionid='".$_POST['taskid']."'";
							}
							else
							{
								
										$q = Doctrine_Query::create()
										   ->from('cfUser a')
										   ->where('a.nid = ?', $_SESSION["SESSION_CUTEFLOW_USERID"]);
										$user = $q->fetchOne();
								
								$strQuery = "INSERT INTO cf_fieldvalue values(null, '$nFieldId', '$strCrazyValue', '$nSlotId', ".$arrProcessInfo["ncirculationformid"].",".$arrProcessInfo["ncirculationhistoryid"].", ".$user->getNid().",  ".$_POST['taskid'].", '".date('Y-m-d')."')";
							}
						}
						mysql_query($strQuery, $nConnection);
					}
				}
				
				$strCrazyValue = '';
				global $arrCOMBOOverview;
				if (sizeof($arrCOMBOOverview) > 0)
				{
					foreach($arrCOMBOOverview as $arrCurCOMBOOverview)
					{
						$nAmount = sizeof($arrCurCOMBOOverview);
						//$strCrazyValue	= '---'.$nAmount;
						$strCrazyValue = '';
						$nCounter = 0;
						
						foreach($arrCurCOMBOOverview as $arrCurCOMBOEntries)
						{
							$strCurName = $arrCurCOMBOEntries['strmyname'];
							$nCurState	= 0;
							if ($arrCurCOMBOEntries['nmystate'] == $nCounter)
							{
								$nCurState = 1;
							}
							$nFieldId	= $arrCurCOMBOEntries['nfieldid'];
							$nSlotId	= $arrCurCOMBOEntries['nslotid'];;
							$nFormId	= $arrCurCOMBOEntries['nformid'];;
							
							//$strCrazyValue = $strCrazyValue.'---'.$strCurName.'---'.$nCurState;
							$strCrazyValue = $strCrazyValue.$nCurState.'---';
							$nCounter++;
						}
						
						$strQuery = "UPDATE cf_fieldvalue SET strfieldvalue='$strCrazyValue' WHERE ninputfieldid= '$nFieldId' AND nslotid= '$nSlotId' AND nformid=".$arrProcessInfo["ncirculationformid"]." AND ncirculationhistoryid=".$arrProcessInfo["ncirculationhistoryid"]." AND nuserid='".$_SESSION["SESSION_CUTEFLOW_USERID"]."' AND nrevisionid='".$_POST['taskid']."'";
						mysql_query($strQuery, $nConnection);
					}
				}
				
/////////////////////////////////////////////////////////////////////////////////////////////////////					
				
				//-----------------------------------------------
				//--- send mail to next user in list
				//-----------------------------------------------
				$strQuery = "SELECT * FROM cf_mailinglist INNER JOIN cf_circulationform ON cf_mailinglist.nid = cf_circulationform.nmailinglistid WHERE cf_circulationform.nid=".$arrProcessInfo["ncirculationformid"];
				$nResult = mysql_query($strQuery, $nConnection);
				if ($nResult)
				{
					if (mysql_num_rows($nResult) > 0)
					{
						$arrRow = mysql_fetch_array($nResult);
						
						$nListId = $arrRow[0];
					}
				}
				
				
				
				$arrCirculationProcess 	= $arrProcessInfo;
				$nCirculationProcessId 	= $arrCirculationProcess['nid'];
				$nCirculationFormId 	= $arrCirculationProcess['ncirculationformid'];
				$nSlotId			 	= $arrCirculationProcess['nslotid'];
				$nUserId 				= $arrCirculationProcess['nuserid'];
				$nIsSubtituteOf	 		= $arrCirculationProcess['nissubstitiuteof'];
				$nCirculationHistoryId 	= $arrCirculationProcess['ncirculationhistoryid'];
				$dateInProcessSince		= $arrCirculationProcess['dateinprocesssince'];
				
				// get the Position in current Slot
				$query 		= "SELECT nmailinglistid FROM cf_circulationform WHERE nid = '$nCirculationFormId' LIMIT 1;";
				$result 	= mysql_query($query, $nConnection);
				$arrResult 	= mysql_fetch_array($result, MYSQL_ASSOC);
				$nMailingListId = $arrResult['nmailinglistid'];
				
				$query 		= "SELECT * FROM cf_slottouser WHERE nslotid = '$nSlotId' AND nmailinglistid = '$nMailingListId' AND nuserid = '$nUserId' LIMIT 1;";
				$result 	= mysql_query($query, $nConnection);
				$arrResult 	= mysql_fetch_array($result, MYSQL_ASSOC);
				
				if ($nIsSubtituteOf == 0)
				{	// the current user is no substitute
					if ($arrResult['nid'] == '')
					{	// it's the sender of the circulation!!!
						$arrNextUser = getNextUserInList(-2, $nListId, $nSlotId);
					}
					else
					{
						$arrNextUser = getNextUserInList($nUserId, $nListId, $nSlotId);
					}
				}
				else
				{	// user is a substitute
					// let's see who this substitute belongs to
					// it's NOT saved in "nIsSubstituteOf" -.-
		
					$strQuery 	= "SELECT MAX(dateinprocesssince) as nmaxdateinprocesssince FROM cf_circulationprocess WHERE ncirculationformid = '$nCirculationFormId' AND nissubstitiuteof = '0' AND dateinprocesssince < '$dateInProcessSince' LIMIT 1;";
					$result 	= mysql_query($strQuery, $nConnection);
					$arrResult 	= mysql_fetch_array($result, MYSQL_ASSOC);
					
					$strQuery 	= "SELECT nuserid FROM cf_circulationprocess WHERE ncirculationformid = '$nCirculationFormId' AND dateinprocesssince = '".$arrResult['nmaxdateinprocesssince']."' LIMIT 1;";
					$result 	= mysql_query($strQuery, $nConnection);
					$arrResult 	= mysql_fetch_array($result, MYSQL_ASSOC);
					
					$nSubsUserId = $arrResult['nuserid'];
					
					$arrNextUser = getNextUserInList($nSubsUserId, $nListId, $nSlotId);
				}
				
				//Check if there is any remaining process
				$strQuery 	= "SELECT * FROM cf_circulationprocess WHERE ncirculationformid = '$nCirculationFormId' AND ndecissionstate = '0';";
				$procresult 	= mysql_query($strQuery, $nConnection);
				
				if (mysql_num_rows($procresult) > 0) //There are still users commenting on the CP
				{
					$q = Doctrine_Query::create()
								   ->from('cfCirculationprocess a')
								   ->where('a.nid = ?', $arrProcessInfo["nid"]);
								$process = $q->fetchOne();
								$process->setNresendcount(1);
								$process->save();
					if ($arrNextUser[0] == -2)
					{	// let's get the Sender User ID
						$objCirculation	= new CCirculation();
						$arrSender 		= $objCirculation->getSenderDetails($nCirculationFormId);
						$arrNextUser[0] = $arrSender['nid'];
					}
					
					//if circulation is simulateneous then do nothing otherwise active the code below
					$q = Doctrine_Query::create()
				   ->from('FormEntry a')
				   ->where('a.circulation_id = ?', $nCirculationFormId);
					$forms = $q->execute();
					$formid = 0;
					$theform = "";
					foreach($forms as $form)
					{
						$theform = $form;
					}
					
					
					if ($arrNextUser[2] !== false) {
						// Slot has changed
						// Send a notification if this is wished
										
						$strQuery = "SELECT * FROM cf_circulationform WHERE nid=".$arrProcessInfo["ncirculationformid"];
						$nResult = mysql_query($strQuery, $nConnection);
						if ($nResult)
						{
							if (mysql_num_rows($nResult) > 0)
							{
								$arrRow = mysql_fetch_array($nResult);
								
								$nSenderId		= $arrRow["nsenderid"];
								$strCircName	= $arrRow["strname"];
								$nEndAction		= $arrRow["nendaction"];
							}
						}
						
						$strQuery = "SELECT * FROM cf_formslot WHERE nid=".$arrNextUser[2];
						$nResult = mysql_query($strQuery, $nConnection);
						if ($nResult)
						{
							if (mysql_num_rows($nResult) > 0)
							{
								$arrRow = mysql_fetch_array($nResult);
								$slotname = $arrRow['strname'];
							}
						}
						
						if ( ($nEndAction & 8) == 8 ) {
							sendMessageToSender($nSenderId, $arrProcessInfo["nuserid"], "done", $strCircName, "ENDSLOT", $_REQUEST["cpid"], $slotname);
						}
					}
							   $q = Doctrine_Query::create()
									 ->from('FormEntry a')
									 ->where('a.circulation_id = ?', $arrProcessInfo["ncirculationformid"]);
								$entries = $q->execute();
								
								foreach($entries as $entry)
								{
									//Save Audit
									$audit = new Audit();
									$audit->saveAudit($entry->getId(), "approved content & commented on circulation");
								}  
								//Update the process status to 1
								
								$q = Doctrine_Query::create()
								   ->from('cfCirculationprocess a')
								   ->where('a.nid = ?', $arrProcessInfo["nid"]);
								$process = $q->fetchOne();
								$process->setNresendcount(1);
								$process->save();
				}
				else
				{
					$q = Doctrine_Query::create()
								   ->from('cfCirculationprocess a')
								   ->where('a.nid = ?', $arrProcessInfo["nid"]);
								$process = $q->fetchOne();
								$process->setNresendcount(1);
								$process->save();
					//--- send done email to sender if wanted
					$strQuery = "SELECT * FROM cf_circulationform WHERE nid=".$arrProcessInfo["ncirculationformid"];
					$nResult = mysql_query($strQuery, $nConnection);
					if ($nResult)
					{
						if (mysql_num_rows($nResult) > 0)
						{
							$arrRow = mysql_fetch_array($nResult);
							
							$nEndAction		= $arrRow["nendaction"];
							$nSenderId		= $arrRow["nsenderid"];
							$strCircName	= $arrRow["strname"];
							
							
							// check the hook CF_ENDACTION
								$circulation 	= new CCirculation();
								$endActions		= $circulation->getExtensionsByHookId('CF_ENDACTION');
								
								if ($endActions)
								{
									foreach ($endActions as $endAction)
									{
										$params		= $circulation->getEndActionParams($endAction);
										$hookValue	= (int) $params['hookValue'];
										
										if (($nEndAction & $hookValue) == $hookValue)
										{
											require_once $params['filename'];
										}
									}
								}
							
							$nShouldArchived 	= $nEndAction & 2;
							$nShouldMailed 		= $nEndAction & 1;
							$nShouldDeleted 	= 4;
							
							if ($nShouldMailed == 1)
							{
								sendMessageToSender($nSenderId, $arrProcessInfo["nuserid"], "done", $strCircName, "SUCCESS", $_REQUEST["cpid"]);
								$q = Doctrine_Query::create()
								   ->from('cfCirculationprocess a')
								   ->where('a.nid = ?', $arrProcessInfo["nid"]);
								$process = $q->fetchOne();
								$process->setNresendcount(1);
								$process->save();
							}
							
							if ($nShouldArchived == 2)
							{	// send circulation to endorsement
							
								$q = Doctrine_Query::create()
									 ->from('cfCirculationform a')
									 ->where('a.nid = ?', $arrProcessInfo["ncirculationformid"]);
								$circulations = $q->execute();
								$archivebit = 1;
								foreach($circulations as $circulation)
								{
									if($circulation->getBisarchived() == "3" || $circulation->getBisarchived() == "4" || $circulation->getBisarchived() == "25")
									{
										$archivebit = $circulation->getBisarchived();
									}
								}
							
								$strQuery = "UPDATE cf_circulationform SET bisarchived=".$archivebit." WHERE nid=".$arrProcessInfo["ncirculationformid"];
								mysql_query($strQuery, $nConnection);
								
								//change status of form_entry
								$q = Doctrine_Query::create()
									 ->from('FormEntry a')
									 ->where('a.circulation_id = ?', $arrProcessInfo["ncirculationformid"]);
								$entries = $q->execute();
								
								foreach($entries as $entry)
								{
									//Save Audit
									$audit = new Audit();
									$audit->saveAudit($entry->getId(), "approved content & commented on circulation");
									
									
								$q = Doctrine_Query::create()
								   ->from('cfCirculationprocess a')
								   ->where('a.nid = ?', $arrProcessInfo["nid"]);
								$process = $q->fetchOne();
								$process->setNresendcount(1);
								$process->save();
									
									if($entry->getApproved() <= 4){
										
									$approval = "4";
									
									 $q = Doctrine_Query::create()
										->from('skipstation a')
										->where('a.approval_stage = ? AND a.entry_id = ?', array(4,$entry->getId()));
									 $skip = $q->fetchOne();
									 if(sizeof($skip) > 0)
									 {
										 $approval = "5"; 
										 $circulation->setBisarchived("2");
										 $circulation->save();
									 }	
									 $q = Doctrine_Query::create()
										->from('skipstation a')
										->where('a.approval_stage = ? AND a.entry_id = ?', array(5,$entry->getId()));
									 $skip = $q->fetchOne();
									 if(sizeof($skip) > 0)
									 {
										 $approval = "55"; 
										 $circulation->setBisarchived("25");
										 $circulation->save();
									 }
									 
									 $q = Doctrine_Query::create()
										->from('skipstation a')
										->where('a.approval_stage = ? AND a.entry_id = ?', array(55,$entry->getId()));
									 $skip = $q->fetchOne();
									 if(sizeof($skip) > 0)
									 {
										 $approval = "6"; 
										 $circulation->setBisarchived("3");
										 $circulation->save();
									 }
									 
									 $q = Doctrine_Query::create()
										->from('skipstation a')
										->where('a.approval_stage = ? AND a.entry_id = ?', array(6,$entry->getId()));
									 $skip = $q->fetchOne();
									 if(sizeof($skip) > 0)
									 {
										 $approval = "7";
										 $circulation->setBisarchived("4");
										 $circulation->save(); 
									 }
									$entry->setApproved($approval);
									$entry->save();
									
									
									
																		   }
									

						$user = Doctrine_Core::getTable('sfGuardUser')->find(array($entry->getUserId()));
		
						$to = $user->getProfile()->getEmail();
						$from = sfConfig::get('app_fromemail');
						$subject = "Endorsement";
						$layout = Doctrine_Core::getTable("WebLayout")->find("9");
			
						$body = $layout->getContent();
						
						
			$templateparser = new Templateparser();
			
			$body = $templateparser->parse($entry->getId(),$entry->getFormId(), $entry->getEntryId(), $body);
						
						$notifier->sendemail($from,$to,$subject,$body);
						
									
								}
								
							   //Update the cf_slottouser status of the user to 1
								
								
								//Send email to the architect (Circulation is complete)
								$q = Doctrine_Query::create()
									  ->from('FormEntry a')
									  ->where('a.circulation_id = ?', $arrProcessInfo["ncirculationformid"]);
								$architects = $q->execute();
								$thisarchitect = "";
								foreach($architects as $architect)
								{
									$thisarchitect = $architect;
								}
								sendMessageToArchitect($thisarchitect, $strMessage);
								
							}
							elseif ($nShouldDeleted & $nEndAction)
							{	// delete circulation
								$query = "UPDATE cf_circulationform SET bdeleted = 1 WHERE nid = ".$arrProcessInfo['ncirculationformid'];
								mysql_query($query, $nConnection);
							}
						}
					}
				}
			}
		
	}
}	



if($_POST['submitdraft'])
{
	?>
	<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=<?php echo $DEFAULT_CHARSET ?>">
		<title></title>
	</head>
	<body>
	<script language="javascript">
	window.location = '/backend.php/tasks/view/id/<?php echo $_POST['taskid']; ?>';
	</script>
	</body>
	</html>
	<?php
}
else if($_POST['submitcomment'])
{
	$q = Doctrine_Query::create()
	   ->from('FormEntry a')
	   ->where('a.circulation_id = ?', $arrProcessInfo["ncirculationformid"]);
	$application = $q->fetchOne();
	?>
	<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=<?php echo $DEFAULT_CHARSET ?>">
		<title></title>
	</head>
	<body>
	<script language="javascript">
	window.location = '/backend.php/tasks/complete/id/<?php echo $_POST['taskid']; ?>';
	</script>
	</body>
	</html>
	<?php
}
else
{	
	
$q = Doctrine_Query::create()
   ->from('FormEntry a')
   ->where('a.circulation_id = ?', $arrProcessInfo["ncirculationformid"]);
$application = $q->fetchOne();
?>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=<?php echo $DEFAULT_CHARSET ?>">
	<title></title>
</head>
<body>
<script language="javascript">
window.location = '/backend.php/applications/view/id/<?php echo $application->getId(); ?>';
</script>
</body>
</html>

<?php
}
?>
