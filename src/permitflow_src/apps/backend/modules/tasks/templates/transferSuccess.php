<?php
/**
 * transferSuccess templates.
 *
 * Transfers a task from one reviewer to another
 *
 * @package    backend
 * @subpackage tasks
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

?>



<div class="pageheader">
<h2><i class="fa fa-envelope"></i>Tasks<span>Create an new client from here</span></h2>
<div class="breadcrumb-wrapper">
<span class="label">You are here:</span>
<ol class="breadcrumb">
<li><a href="">Home</a></li>
<li><a href="">tasks</a></li>
<li class="active">Stages</li>
</ol>
</div>
</div>


<div class="contentpanel">
<div class="row">

  <div class="panel panel-dark">
      <div class="panel-heading">
      <h3 class="panel-title">Transfer Task</h3>
      </div>

    <div class="panel-body panel-body-nopadding">
      <form id="form" action="<?php echo public_path(); ?>backend.php/tasks/transfer/id/<?php echo $task->getId(); ?>" method="post" autocomplete="off" data-ajax="false" class="form-bordered">
                    <div class="form-group">
                    <label class="col-sm-4" for="text_field">Assigned Application</label>
                            <div class="col-sm-8">
                            <?php

                                    $q = Doctrine_Query::create()
                                            ->from('FormEntry a')
                                            ->where('a.id = ?', $task->getApplicationId());
                                    $application = $q->fetchOne();

                                    $q = Doctrine_Query::create()
                                             ->from('sfGuardUserProfile a')
                                             ->where('a.user_id = ?', $application->getUserId());
                                    $userprofile = $q->fetchOne();

                            ?>
                            <button class="btn btn-primary" OnClick="window.location = '<?php echo public_path(); ?>backend.php/applications/view/id/<?php echo $task->getApplicationId(); ?>';"><?php echo $application->getApplicationId()." (Submitted by ".$userprofile->getFullname().")" ?></button>
                            </div>
                    </div>
                     <div class="form-group">
                    <label class="col-sm-4" for="text_tooltip">Assign Reviewers</label>
                            <div class="col-sm-8">
                            <select name="reviewers[]" id="reviewers" multiple>
                            <?php
                                    $q = Doctrine_Query::create()
                                            ->from('CfUser a')
                                            ->where('a.nid = ?', $_SESSION["SESSION_CUTEFLOW_USERID"]);
                                    $logged_in_reviewer = $q->fetchOne();

                                    //Add reviewers from same department to the list
                                    $q = Doctrine_Query::create()
                                            ->from('CfUser a');
                                    $reviewers = $q->execute();
                                    foreach($reviewers as $reviewer)
                                    {
                                            if($reviewer->getStrdepartment() == $logged_in_reviewer->getStrdepartment())
                                            {
                                                    if($reviewer->getNid() == $logged_in_reviewer->getNid())
                                                    {
                                                            echo "<option value='".$reviewer->getNid()."'>**".$reviewer->getStrfirstname()." ".$reviewer->getStrlastname()." (".$reviewer->getStrdepartment().")</option>";
                                                    }
                                                    else
                                                    {
                                                            echo "<option value='".$reviewer->getNid()."'>".$reviewer->getStrfirstname()." ".$reviewer->getStrlastname()." (".$reviewer->getStrdepartment().")</option>";
                                                    }
                                            }
                                    }

                                    //Get HODs from other departments and assign them to the list as well.
                                    $q = Doctrine_Query::create()
                                            ->from('Department a')
                                            ->where('a.department_name <> ?', $logged_in_reviewer->getStrdepartment());
                                    $departments = $q->execute();
                                    foreach($departments as $department)
                                    {
                                        $q = Doctrine_Query::create()
                                            ->from('CfUser a')
                                            ->where("a.nid = ?", $department->getDepartmentName());
                                        $reviewer = $q->fetchOne();
                                        if($reviewer)
                                        {
                                          echo "<option value='".$reviewer->getNid()."'>".$reviewer->getStrfirstname()." ".$reviewer->getStrlastname()." (HOD of ".$reviewer->getStrdepartment().")</option>";
                                        }
                                    }
                            ?>
                            </select>
                            </div>
                    </div>
                  <div class="form-group"><label class="col-sm-4" for="text_tooltip">Type of Task</label>
                            <div class="col-sm-8">
                            <select name="task_type" id="task_type" width="30%">
                            <option value="2" <?php if($task->getType() == "2"){ echo "selected"; } ?>>Assessment</option>
                            <option value="6" <?php if($task->getType() == "6"){ echo "selected"; } ?>>Inspection</option>
                            <option value="3" <?php if($task->getType() == "3"){ echo "selected"; } ?>>Invoicing</option>
                            <option value="4" <?php if($task->getType() == "4"){ echo "selected"; } ?>>Scanning</option>
                            </select>
                            </div>
                    </div>
                  <div class="form-group"><label class="col-sm-4" for="textarea">Description of Task</label>
                            <div class="col-sm-8"><textarea id="wysiwyg" name="textarea" class="form-control" rows="10"><?php echo $task->getRemarks(); ?></textarea></div>
                    </div>
                  <div class="form-group"><label class="col-sm-4" for="text_tooltip">Start Date</label>
                            <div class="col-sm-8">
                            <div class="input-group">
                           <input type="text" id="start_date" name="start_date" class="form-control" value="<?php echo $task->getStartDate(); ?>">
                           <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                           </div>

                            </div>
                    </div>
                  <div class="form-group"><label class="col-sm-4" for="text_tooltip">End Date</label>
                            <div class="col-sm-8">
                           <div class="input-group">
                           <input type="text" id="end_date" name="end_date" class="form-control" value="<?php echo $task->getEndDate(); ?>">
                           <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                           </div>
                           </div>
                    </div>
                  <div class="form-group"><label class="col-sm-4" for="text_tooltip">Priority</label>
                            <div class="col-sm-8">
                            <select name="priority" id="priority">
                            <option value="3" <?php if($task->getPriority() == "3"){ echo "selected"; } ?>>Normal</option>
                            <option value="2" <?php if($task->getPriority() == "2"){ echo "selected"; } ?>>Important</option>
                            <option value="1" <?php if($task->getPriority() == "1"){ echo "selected"; } ?>>Critical</option>
                            </select>
                            </div>
                    </div>

     </div><!--panel-body-->

			<div class="panel-footer">
					<button class="btn btn-danger mr10">Reset</button>  <button class="btn btn-primary mr20" name="submitbuttonname" value="submitbuttonvalue">Submit</button>
			</div><!--panel-footer-->
                    </form>

</div><!--panel-->




</div><!--panel-->


</div> <!--panel-->
</div>


<script>
jQuery(document).ready(function(){

  // Chosen Select
  jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});

  // Date Picker
  jQuery('#start_date').datepicker();
  jQuery('#end_date').datepicker();


});
</script>
