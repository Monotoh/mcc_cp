<?php
  use_helper("I18N");
  $application = $task->getApplication();
  include_partial('applications/libforms');
?>
<div class="pageheader">
  <h2><i class="fa fa-th-list"></i> <?php echo __("Tasks"); ?> <span><?php echo $application->getApplicationId(); ?></span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label"><?php echo __("You are here"); ?>:</span>
    <ol class="breadcrumb">
      <li><a href="/backend.php/tasks/list"><?php echo __("Tasks"); ?></a></li>
      <li class="active"><?php echo __("View Task"); ?></li>
    </ol>
  </div>
</div>

<div class="cd-panel from-left">
		<header class="cd-panel-header">
			<h1>
        <?php
          if($task->getType() == "3")
          {
            $q = Doctrine_Query::create()
               ->from('Invoicetemplates a')
               ->where('a.applicationform = ?', $application->getFormId())
               ->andWhere('a.applicationstage = ?', $application->getApproved());
            $invoicetemplate = $q->fetchOne();

            if($invoicetemplate)
            {
              echo $invoicetemplate->getTitle();
            }
            else {
              $q = Doctrine_Query::create()
                 ->from('Invoicetemplates a')
                 ->where('a.applicationform = ?', $application->getFormId());
              $invoicetemplate = $q->fetchOne();

              if($invoicetemplate)
              {
                echo $invoicetemplate->getTitle();
              }
              else {
                echo $application->getApplicationId()." Invoice";
              }
            }
          }
          else {
            echo "Comment Sheet";
          }
        ?>
      </h1>
			<a href="#0" class="cd-panel-close"><?php echo __("Close"); ?></a>
		</header>

		<div class="cd-panel-container-custom">
			<div class="cd-panel-content">

  		<div class="panel panel-default">
          <div class="panel-body panel-body-nopadding">

            <?php
              if($task->getType() == "3")
              {
                //Invoicing Task
                ?>
                <form class="form-bordered" id="feeform" method="post" action="<?php echo public_path(); ?>backend.php/tasks/saveinvoice/id/<?php echo $task->getId(); ?>" id="MailContentForm" name="MailContentForm" onSubmit="return validate_editfield();" autocomplete="off" data-ajax="false">
                  <?php
  									$grandtotal = 0;
                    $q = Doctrine_Query::create()
                       ->from('Invoicetemplates a')
                       ->where('a.applicationform = ?', $application->getFormId())
                       ->andWhere('a.applicationstage = ?', $application->getApproved());
                    $invoicetemplate = $q->fetchOne();

                    if($invoicetemplate)
                    {
    									$q = Doctrine_Query::create()
    									   ->from('fee a')
                         ->where('a.invoiceid = ?', $invoicetemplate->getId());
    									$fixedfees = $q->execute();

                      $count = 0;
	//OTB Start Patch - For Implementing Dynamic Fees
	$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
	mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
	$query = "SELECT * FROM ap_form_".$application->getFormId()." WHERE id = '".$application->getEntryId()."'";
	$result = mysql_query($query,$dbconn);

	$application_form = mysql_fetch_assoc($result);
	$invoicemanager = new InvoiceManager();
	//OTB End Patch - For Implementing Dynamic Fees
    									foreach($fixedfees as $fee)
    									{
											//OTB Start Patch - For Implementing Dynamic Fees
											$fee_amount = $invoicemanager->getFeeAmount($fee, $application_form, $application->getFormId());
											//OTB End Patch - For Implementing Dynamic Fees
                        $count++;
    										?>
    											<div class="form-group">
            										<label class="col-sm-4">
            										<?php echo $fee->getFeeCode(); ?>: <?php echo $fee->getDescription(); ?>
                                    <input type='hidden' name='feetitle[]' value="<?php echo $fee->getFeeCode().": ".$fee->getDescription(); ?>">
                                </label>
    												<div class="col-sm-8">
    												<input class="form-control"  type='text' id='fee<?php echo $count; ?>' name='feevalue[]' value="<?php echo $fee_amount; ?>" onkeyup='updatefee()'><!--OTB Patch - Use fee amount from dynamic calculations-->
    												</div>
    											</div>
    											<?php
												$grandtotal = $grandtotal + $fee_amount;//OTB Patch - Use fee amount from dynamic calculations
    									}
                    }

                    $feeselect = "<option>".__("Choose Fee")."</option>";

                    $q = Doctrine_Query::create()
                       ->from("FeeCategory a")
                       ->orderBy("a.id ASC");
                    $categories = $q->execute();

                    foreach($categories as $category){


                      $feeselect .= "<optgroup label='--------------------------------------------------------------------------------------'></optgroup>";

                      $feeselect .= "<optgroup label='".$category->getTitle()."'>";

                      $q = Doctrine_Query::create()
                         ->from("Fee a")
                         ->where("a.invoiceid = NULL OR a.invoiceid = ''")
                         ->andWhere("a.fee_category = ?", $category->getId())
                         ->orderBy("a.description ASC");
                      $fees = $q->execute();
                      foreach($fees as $fee)
                      {
                        $feeselect .= "<option value='".$fee->getId()."'>".$fee->getFeeCode().": ".$fee->getDescription()."</option>";
                      }

                      $feeselect .= "</optgroup>";


                    }

                    $feeselect .= "<optgroup label='--------------------------------------------------------------------------------------'></optgroup>";
                    $feeselect .= "<optgroup label='- ". __("OTHERS")." -'>";
                    $feeselect .= "<optgroup label='--------------------------------------------------------------------------------------'></optgroup>";

  									$q = Doctrine_Query::create()
  									   ->from("Fee a")
  									   ->orderBy("a.fee_code ASC");
  									$fees = $q->execute();
  									foreach($fees as $fee)
  									{
  										$feeselect .= "<option value='".$fee->getId()."'>".$fee->getFeeCode().": ".$fee->getDescription()."</option>";
  									}

                    $feeselect .= "</optgroup>";
  									?>

  									<script language="javascript">
								    function getFee(id, feecode)
								    {
  									  var xmlHttpReq1 = false;
											var self1 = this;
											// Mozilla/Safari

											if (window.XMLHttpRequest) {
													self.xmlHttpReq1 = new XMLHttpRequest();
											}
											// IE
											else if (window.ActiveXObject) {
													self.xmlHttpReq1 = new ActiveXObject("Microsoft.XMLHTTP");
											}
											self.xmlHttpReq1.open('POST', '/backend.php/fees/getfee', true);
											self.xmlHttpReq1.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
											self.xmlHttpReq1.onreadystatechange = function() {
											  if (self.xmlHttpReq1.readyState == 4) {
											  	document.getElementById(id).value = self.xmlHttpReq1.responseText;
											  	updateTotal();
											  }
											}
											self.xmlHttpReq1.send('code' + '=' + feecode);
  									}


								    function updatefee()
								    {
											  	updateTotal();
  									}

								    function updateTotal(amount)
								    {
								    	var total = 0;

											$("#feeform input[type=text]").each(function() {
												if($(this).attr('id') == 'servicefee' || $(this).attr('id') == 'totalfee')
												{

												}
												else
												{
													total = total + parseInt(this.value);
												}
											});

											$("#totalfee").val(total);
								    }
									</script>

                  <div class='form-group' class='formgroup'>
                    <label class='col-sm-4'>
                      <select name='feetitle[]' class='form-control' onChange='getFee("inv_1", this.value)'>
                        <?php echo $feeselect; ?>
                      </select>
                    </label>
                  <div class='col-sm-8'> <input type='text' id='inv_1' onkeyup='updateTotal();' name='feevalue[]' class='form-control' value="0"/></div></div>

                  <div class='form-group' class='formgroup'>
                    <label class='col-sm-4'>
                      <select name='feetitle[]' class='form-control' onChange='getFee("inv_2", this.value)'>
                        <?php echo $feeselect; ?>
                      </select>
                    </label>
                  <div class='col-sm-8'> <input type='text' id='inv_2' onkeyup='updateTotal();' name='feevalue[]' class='form-control' value="0"/></div></div>

                  <div class='form-group' class='formgroup'>
                    <label class='col-sm-4'>
                      <select name='feetitle[]' class='form-control' onChange='getFee("inv_3", this.value)'>
                        <?php echo $feeselect; ?>
                      </select>
                    </label>
                  <div class='col-sm-8'> <input type='text' id='inv_3' onkeyup='updateTotal();' name='feevalue[]' class='form-control' value="0"/></div></div>

                  <div class='form-group' class='formgroup'>
                    <label class='col-sm-4'>
                      <select name='feetitle[]' class='form-control' onChange='getFee("inv_4", this.value)'>
                        <?php echo $feeselect; ?>
                      </select>
                    </label>
                  <div class='col-sm-8'> <input type='text' id='inv_4' onkeyup='updateTotal();' name='feevalue[]' class='form-control' value="0"/></div></div>

                  <div class='form-group' class='formgroup'>
                    <label class='col-sm-4'>
                      <select name='feetitle[]' class='form-control' onChange='getFee("inv_5", this.value)'>
                        <?php echo $feeselect; ?>
                      </select>
                    </label>
                  <div class='col-sm-8'> <input type='text' id='inv_5' onkeyup='updateTotal();' name='feevalue[]' class='form-control' value="0"/></div></div>

                  <div class='form-group' class='formgroup'>
                    <label class='col-sm-4'>
                      <select name='feetitle[]' class='form-control' onChange='getFee("inv_6", this.value)'>
                        <?php echo $feeselect; ?>
                      </select>
                    </label>
                  <div class='col-sm-8'> <input type='text' id='inv_6' onkeyup='updateTotal();' name='feevalue[]' class='form-control' value="0"/></div></div>

                  <div class="form-group">
                    <label class="col-sm-4 control"><i class="bold-label-2">Total</i><input type='hidden' name='feetitle[]' value='Total' readonly></label>
										<div class="col-sm-8">
										<input class="form-control" type='text' id='totalfee' name='feevalue[]' readonly='readonly' value="<?php echo $grandtotal; ?>">
										</div>
									</div>
								<div class="form-group">
                  <div class="col-sm-12" style="padding: 10px;" align="right">
										<button class="btn btn-primary" type="submit" name="submitbuttonname" value="submitbuttonvalue"> <?php echo __("Submit"); ?> </button>
                  </div>
                </div>
              </form>
              <?php
              }
              else
              {
                 //Assessment Task
                 $_SESSION["taskid"] = $task->getId();

                 $prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";
                 require_once($prefix_folder.'includes/init.php');

                 header("p3p: CP=\"IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT\"");

                 require_once($prefix_folder.'config.php');
                 require_once($prefix_folder.'includes/language.php');
                 require_once($prefix_folder.'includes/db-core.php');
                 require_once($prefix_folder.'includes/common-validator.php');
                 require_once($prefix_folder.'includes/view-functions-task.php');
                 require_once($prefix_folder.'includes/post-functions.php');
                 require_once($prefix_folder.'includes/entry-functions.php');
                 require_once($prefix_folder.'includes/helper-functions.php');
                 require_once($prefix_folder.'includes/filter-functions.php');
                 require_once($prefix_folder.'includes/theme-functions.php');
                 require_once($prefix_folder.'lib/recaptchalib.php');
                 require_once($prefix_folder.'lib/php-captcha/php-captcha.inc.php');
                 require_once($prefix_folder.'lib/text-captcha.php');
                 require_once($prefix_folder.'hooks/custom_hooks.php');

                 $dbh = mf_connect_db();
                 $mf_settings = mf_get_settings($dbh);

                //If task is marked as completed, display read only comments
                if($task->getStatus() == "25")
                {

                  $q = Doctrine_Query::create()
                      ->from('CfUser a')
                      ->where('a.nid = ?', $task->getOwnerUserId());
                  $reviewer = $q->fetchOne();
                  ?>
                  <div class="panel panel-dark">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion_inner" href="#comments<?php echo $task->getId(); ?>" style="color: #FFF;">
                                <?php echo $reviewer->getStrfirstname()." ".$reviewer->getStrlastname(); ?> <i>(<?php echo __("Last updated on"); ?> <?php echo $task->getEndDate(); ?>)</i>
                            </a>
                        </h4>
                    </div>
                    <div id="comments<?php echo $task->getId(); ?>" class="panel-collapse collapse in">
                      <div class="panel-body">
                      <?php
                        $q = Doctrine_Query::create()
                            ->from("TaskForms a")
                            ->where("a.task_id = ?", $task->getId());
                        $taskforms = $q->execute();

                        foreach($taskforms as $taskform)
                        {
                          $form_id  = $taskform->getFormId();
                          $entry_id = $taskform->getEntryId();

                          //get entry details for particular entry_id
                          $param['checkbox_image'] = '/form_builder/images/icons/59_blue_16.png';
                          $entry_details = mf_get_entry_details($dbh,$form_id,$entry_id,$param);

                          //Print Out Application Details
                          foreach ($entry_details as $data){
                              if(strlen($data['element_type'] == "section"))
                              {
                                  ?>
                                  <section>
                                      <label for="text_field" style="font-weight: 900; width: 100%;"><?php echo $data['label']; ?></label>
                                  </section>
                              <?php
                              }
                              else
                              {
                                  if(($data['label'] == 'Cell' || $data['label'] == 'Village') && $data['value'] == "&nbsp;")
                                  {

                                  }
                                  else
                                  {
                                      ?>
                                      <section>
                                          <label for="text_field" style="font-weight: 900;"><?php echo $data['label']; ?></label>
                                          <div>&nbsp;&nbsp;&nbsp;&nbsp;<?php if($data['value']){ echo nl2br($data['value']); }else{ echo "-"; } ?></div>
                                          <hr>
                                      </section>
                                  <?php
                                  }
                              }
                          }
                        }
                        ?>
                    </div>
                  </div>
                </div>
                <?php
                }
                else
                {
                   //If task is marked as pending, display form for entering comments
                   $q = Doctrine_Query::create()
   	                 ->from('CfUser a')
   	                 ->where('a.nid = ?', $_SESSION["SESSION_CUTEFLOW_USERID"]);
     	             $reviewer = $q->fetchOne();

                   $q = Doctrine_Query::create()
          					->from("TaskForms a")
          					->where("a.task_id = ?", $task->getId());
          				 $taskform = $q->fetchOne();

                   if($taskform && $taskform->getFormId())
                   {
                       $form_id = $taskform->getFormId();
                       $entry_id = $taskform->getEntryId();

                       include_partial('commentform', array('form_id'=>$form_id, 'id'=>$entry_id, 'task' => $task, 'application' => $application));
                   }
    	             else
                   {
                     $q = Doctrine_Query::create()
                          ->from('Department a')
                          ->where('a.department_name = ?', $reviewer->getStrdepartment());
                     $department = $q->fetchOne();
                     error_log("Reviewer Department is >>>". $reviewer->getNid()) ;
                     $q = Doctrine_Query::create()
                        ->from("ApForms a")
                        ->where("a.form_department = ?", $department->getId())
                        ->andWhere("a.form_department_stage = ?", $application->getApproved())
                        ->andWhere("a.form_active <> 9 AND form_type = 2")
                        ->orderBy("a.form_id DESC");
                    $form = $q->fetchOne();

                    if(empty($form))
                    {
                      $q = Doctrine_Query::create()
                         ->from("ApForms a")
                         ->where("a.form_department_stage = ?", $application->getApproved())
                         ->andWhere("a.form_active <> 9 AND form_type = 2")
                         ->orderBy("a.form_id DESC");
                      $form = $q->fetchOne();
                    }

    				        if(empty($form))
                    {
                      $q = Doctrine_Query::create()
                        ->from("ApForms a")
                        ->where("a.form_department = ?", $department->getId())
                        ->andWhere("a.form_active <> 9 AND form_type = 2")
                        ->orderBy("a.form_id DESC");
                      $form = $q->fetchOne();
                    }

                    if(empty($form))
                    {
                      $q = Doctrine_Query::create()
                        ->from("ApForms a")
                        ->where("a.form_active <> 9 AND form_type = 2")
                        ->orderBy("a.form_id DESC");
                      $form = $q->fetchOne();
                    }

    	              if($form)
    	              {
                      $form_id = $form->getFormId();

                      include_partial('commentform', array('form_id'=>$form_id, 'task' => $task, 'application' => $application));
    	              }
    	              else
    	              {
    	                    //Default to a comment sheet
    	              }
                 }
               }
              }
            ?>

          </div><!-- panel-body -->
      </div>

			</div> <!-- cd-panel-content -->
		</div> <!-- cd-panel-container -->
</div> <!-- cd-panel -->

<div class="contentpanel">
<div class="row">
  <div class="col-sm-12">
      <!-- OTB patch -- Task Reset message -->
      <?php if($sf_user->hasFlash('task_reset')): ?>
      <div class="alert alert-success">         
            <?php echo $sf_user->getFlash('task_reset',ESC_RAW); ?>      
      </div>
      <?php endif; ?>    
       <?php if($sf_user->hasFlash('task_reset_error')): ?>
      <div class="alert alert-danger">
          <?php echo $sf_user->getFlash('task_reset',ESC_RAW); ?>
      </div>
      <?php endif; ?>
      
    <!-- Action Buttons -->
    <div class="btn-group mr5 pull-left">
      <button class="btn btn-primary" type="button"><?php echo __("Choose Action"); ?></button>
      <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">
        <span class="caret"></span>
        <span class="sr-only">><?php echo __("Toggle Dropdown"); ?></span>
      </button>
      <ul role="menu" class="dropdown-menu">
        <?php
        if(($task->getOwnerUserId() == $_SESSION["SESSION_CUTEFLOW_USERID"] || $task->getCreatorUserId() == $_SESSION["SESSION_CUTEFLOW_USERID"]) && $task->getStatus() == 1) {
        ?>
          <li><a onClick="if(confirm('Are you sure?')){ return true; }else{ return false; }"  href="<?php echo public_path("/backend.php/tasks/complete/id/" . $task->getId()); ?>"><i
                    class="fa fa-check mr5"></i><?php echo __("Complete Task"); ?></a></li>
        <?php
        }
        elseif($task->getCreatorUserId() == $_SESSION["SESSION_CUTEFLOW_USERID"] || $sf_user->mfHasCredential("director_complete") && $task->getStatus() == 2 ) {
        ?>
          <li><a onClick="if(confirm('Are you sure?')){ return true; }else{ return false; }" href="<?php echo public_path("/backend.php/tasks/complete/id/" . $task->getId()); ?>"><i
                    class="fa fa-check mr5"></i><?php echo __("Confirm Completion"); ?></a></li>
        <?php
        }

        if($sf_user->mfHasCredential("has_hod_access")){
          if($task->getCreatorUserId() == $_SESSION["SESSION_CUTEFLOW_USERID"] && $task->getStatus() != 25) {
          ?>
            <li><a onClick="if(confirm('Are you sure?')){ return true; }else{ return false; }" href="<?php echo public_path("/backend.php/tasks/transfer/id/".$task->getId()); ?>"><i
                      class="fa fa-arrow-right mr5"></i><?php echo __("Transfer Task"); ?></a></li>
          <?php
          }
          //OTB 8th Nov 2017. Hide Cancel Button if task already Cancelled
          if($task->getCreatorUserId() == $_SESSION["SESSION_CUTEFLOW_USERID"] && $task->getStatus() != 25 
            && $task->getStatus() != 55) {
          ?>
            <li><a onClick="if(confirm('Are you sure?')){ return true; }else{ return false; }" href="<?php echo public_path("/backend.php/tasks/cancel/id/".$task->getId()); ?>"><i
                      class="fa fa-trash-o mr5"></i><?php echo __("Cancel Task"); ?></a></li>
          <?php
          }
        }
        ?>
        <li class="divider"></li>
        <?php
        if($sf_user->mfHasCredential("sendtoany"))
        {
            $q = Doctrine_Query::create()
               ->from("SubMenus a")
               ->where("a.id = ?", $application->getApproved());
            $stage = $q->fetchOne();

            $q = Doctrine_Query::create()
               ->from("SubMenus a")
               ->where("a.menu_id = ?", $stage->getMenuId())
               ->andWhere("a.id <> ?", $stage->getId())
               ->andWhere("a.deleted = 0")
               ->orderBy("a.order_no ASC");
            $workflow_stages = $q->execute();

            foreach($workflow_stages as $workflow_stage)
            {
                if($workflow_stage->getStageType() == 5) //Declined
                {
                ?>
                <li><a onClick="if(confirm('Are you sure?')){ window.location='/backend.php/forms/decline?moveto=<?php echo $workflow_stage->getId(); ?>&entryid=<?php echo $application->getId(); ?>&form_id=<?php echo $form_id; ?>&id=<?php echo $entry_id; ?>'; }else{ return false; }">Send to <?php echo $workflow_stage->getTitle(); ?></a></li>
                <?php
                }
                elseif($workflow_stage->getStageType() == 6) //Rejected
                {
                ?>
                <li><a onClick="if(confirm('Are you sure?')){ window.location='/backend.php/forms/reject?moveto=<?php echo $workflow_stage->getId(); ?>&entryid=<?php echo $application->getId(); ?>&form_id=<?php echo $form_id; ?>&id=<?php echo $entry_id; ?>'; }else{ return false; }">Send to <?php echo $workflow_stage->getTitle(); ?></a></li>
                <?php
                }
                elseif($workflow_stage->getStageType() == 4) //Approved
                {
                ?>
                <li><a onClick="if(confirm('Are you sure?')){ window.location='/backend.php/forms/approve?moveto=<?php echo $workflow_stage->getId(); ?>&entryid=<?php echo $application->getId(); ?>&form_id=<?php echo $form_id; ?>&id=<?php echo $entry_id; ?>'; }else{ return false; }">Send to <?php echo $workflow_stage->getTitle(); ?></a></li>
                <?php
                }
                else
                {
                ?>
                <li><a onClick="if(confirm('Are you sure?')){ window.location='/backend.php/forms/viewentry?moveto=<?php echo $workflow_stage->getId(); ?>&entryid=<?php echo $application->getId(); ?>&form_id=<?php echo $form_id; ?>&id=<?php echo $entry_id; ?>'; }else{ return false; }">Send to <?php echo $workflow_stage->getTitle(); ?></a></li>
                <?php
                }
            }
        }
        else
        {
            $q = Doctrine_Query::create()
                 ->from('SubMenuButtons a')
                 ->where('a.sub_menu_id = ?', $application->getApproved());
            $submenubuttons = $q->execute();
            foreach($submenubuttons as $submenubutton)
            {
                $q = Doctrine_Query::create()
                     ->from('Buttons a')
                     ->where('a.id = ?', $submenubutton->getButtonId());
                $buttons = $q->execute();

                foreach($buttons as $button)
                {
                    if($sf_user->mfHasCredential("accessbutton".$button->getId()))
                    {
                        $pos = strpos($button->getLink(), "decline");
                        if($pos === false)
                        {
                                                                                            $pos = strpos($button->getTitle(), "delete");
                                                                                            if($pos === false)
                                                                                            {
                                                                                                    ?>
                                                                                                    <li><a onClick="if(confirm('Are you sure?')){window.location='<?php echo $button->getLink(); ?>&entryid=<?php echo $application->getId(); ?>&form_id=<?php echo $form_id; ?>&id=<?php echo $entry_id; ?>'; }else{ return false; }"><?php echo $button->getTitle(); ?></a></li>
                                                                                                    <?php
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                    ?>
                                                                                                    <li><a onClick="if(confirm('Are you sure?')){ window.location='<?php echo $button->getLink(); ?>&entryid=<?php echo $application->getId(); ?>&form_id=<?php echo $form_id; ?>&id=<?php echo $entry_id; ?>'; }else{ return false; }"><?php echo $button->getTitle(); ?></a></li>
                                                                                                    <?php
                                                                                            }
                        }
                        else
                        {
                            ?>
                                                                                                    <li><a onClick="if(confirm('Are you sure?')){ window.location='<?php echo $button->getLink(); ?>&entryid=<?php echo $application->getId(); ?>&form_id=<?php echo $form_id; ?>&id=<?php echo $entry_id; ?>'; }else{ return false; }"><?php echo $button->getTitle(); ?></a></li>
                            <?php
                        }
                    }
                }
            }
        }
        ?>
                                                                                                                                                                                            
      </ul>
    </div>

    <?php
    $q = Doctrine_Query::create()
      ->from("SavedPermit a")
      ->where("a.application_id = ?", $application->getId())
      ->andWhere("a.permit_status <> 3");
    $permits = $q->execute();

    if($q->count())
    {
    ?>
    <div class="btn-group mr5 pull-left">
      <button class="btn btn-success" type="button"><?php echo __("Download"); ?></button>
      <button data-toggle="dropdown" class="btn btn-success dropdown-toggle" type="button">
        <span class="caret"></span>
        <span class="sr-only"><?php echo __("Toggle Dropdown"); ?></span>
      </button>
      <ul role="menu" class="dropdown-menu">
        <?php

    		foreach($permits as $permit)
    		{
    			$q = Doctrine_Query::create()
    					->from('Permits a')
    					->where('a.id = ?', $permit->getTypeId());
    			$permittype = $q->fetchOne();

    			if($permittype->getPartType() == 2 && $permit->getDocumentKey() == "")
    			{
    					continue;
    			}
    			?>
    			<li><a target="_blank" onClick="window.location = '/backend.php/permits/view/id/<?php echo $permit->getId(); ?>';"><?php echo $permittype->getTitle()." (".$permit->getDateOfIssue().")"; ?></a></li>
    			<?php
    		}
    		?>
      </ul>
    </div>
    <?php
    }
    ?>
    <?php 
    //OTB patch - Allow a user to reset task status 
    $otbhelper = new OTBHelper();
    if($sf_user->mfHasCredential("resettask")):
    ?>
    <?php if($otbhelper->checkTaskInReviewAndCompleted($task->getId())): ?>
    <a class="btn btn-warning" href="/backend.php/tasks/reset?taskid=<?php echo $task->getId() ?> " >
        <?php echo __("Reset Task"); ?>
    </a>
    <?php endif; ?>
    <?php endif; ?>
    <div class="btn-group pull-right">
      <?php
      if($task->getOwnerUserId() == $_SESSION["SESSION_CUTEFLOW_USERID"] && $task->getStatus() == 1) {
          $q = Doctrine_Query::create()
              ->from("TaskForms a")
              ->where("a.task_id = ?", $task->getId());
          $taskforms = $q->execute();
          if(sizeof($taskforms) == 0)
          {
              if(!empty($form))
              {
              ?>
              <a class="btn btn-primary cd-btn"><i class="fa fa-comments mr5"></i> <?php echo $task->getTypeName(); ?></a>
          <?php
              }
              else {
                //if($task->getType() == 3)
                {
                  ?>
                  <a class="btn btn-primary cd-btn"><i class="fa fa-comments mr5"></i> <?php echo $task->getTypeName(); ?></a>
              <?php
                }
              }
          }
          else {
              ?>
              <a class="btn btn-primary cd-btn"><i class="fa fa-comments mr5"></i> <?php echo $task->getTypeName(); ?></a>
          <?php
          }
      }
      else {
        if($task->getStatus() == 55){
            //OTB 8th November 2017 Dont Display the View Comments Button
        }else{
          //OTB 8th November 2017 You can display the Comments Button
        ?>  
            <a class="btn btn-primary cd-btn"><i class="fa fa-comments mr5"></i> View Comments</a>
        <?php
        }
      }

      $q = Doctrine_Query::create()
  			->from('SubMenus a')
  			->where('a.id = ?', $application->getApproved());

  		$stage = $q->fetchOne();

  	 if($sf_user->mfHasCredential("assigntask") && ($stage->getStageType() == 2 || $stage->getStageType() == 8))
  	 {
  		?>
  		<a class="btn btn-primary" data-toggle="modal" data-target="#myModal"><?php echo __('Assign Application'); ?></a>
  		<?php
  	 }
     ?>

    </div>

  <hr class="mb10 mt0">
  </div><!-- col-sm-12 -->
  <div class="col-sm-3">
    <div class="blog-item pb5">
      <?php
      $q = Doctrine_Query::create()
             ->from('SfGuardUser a')
             ->where('a.id = ?', $application->getUserId());
        $user = $q->fetchOne();

        $q = Doctrine_Query::create()
            ->from('SfGuardUserProfile a')
            ->where('a.user_id = ?', $application->getUserId());
        $user_profile = $q->fetchOne();

        /*  if(sfConfig::get('app_enable_categories') == "no")
        {
          $account_type = "";

          if($user->getProfile()->getRegisteras() == 1)
          {
            $account_type = "citizen";
          }
          elseif($user->getProfile()->getRegisteras() == 3)
          {
            $account_type = "alien";
          }
          elseif($user->getProfile()->getRegisteras() == 4)
          {
            $account_type = "visitor";
          }

          ?>
          <div align="center" class="form-group">
            <img src="https://account.ecitizen.go.ke/profile-picture/<?php echo $user->getUsername(); ?>?t=<?php echo $account_type; ?>" class="thumbnail img-responsive mb20" alt="" />
          </div>
          <?php
        }
        else {
          echo "<br><br>";
        } */
        ?>

  <h5 class="subtitle mt20 ml20"><?php echo __("User Details"); ?></h5>

   <ul class="profile-social-list">
     <li><?php echo $user_profile->getFullname(); ?></li>
     <li><?php echo $user->getUsername(); ?></li>
     <li><?php echo $user_profile->getEmail(); ?></li>
     <li><?php echo $user_profile->getMobile(); ?></li>
    </ul>
  <div class="mb20"></div>


  <div class="mb20"></div>
      </div><!-- blog-item -->
  </div><!-- col-sm-3 -->

    <div class="col-sm-9">


      <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">
               <?php echo __("Task Details"); ?>
            </div>
        </div><!-- panel-heading-->
        <div class="panel-body panel-bordered" style="border-top:0;">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-xs-6"><strong><?php echo __("Type"); ?>:</strong></div>
                                <div class="col-xs-6"><?php echo $task->getTypeName(); ?></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6"><strong><?php echo __("Priority"); ?></strong></div>
                                <div class="col-xs-6"><?php echo $task->getPriorityName(); ?></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6"><strong><?php echo __("Task Sent On"); ?></strong></div>
                                <div class="col-xs-6"><?php echo date('d F Y', strtotime($task->getDateCreated())); ?></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6"><strong><?php echo __("Start Date"); ?></strong></div>
                                <div class="col-xs-6"><?php echo date('d F Y', strtotime($task->getStartDate())); ?></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6"><strong><?php echo __("End Date"); ?></strong></div>
                                <div class="col-xs-6"><?php echo date('d F Y', strtotime($task->getEndDate())); ?></div>
                            </div>
                        </div><!-- col-sm-6 -->
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-xs-6"><strong><?php echo __("Task Status"); ?></strong></div>
                                <div class="col-xs-6"><?php echo $task->getStatusName(); ?></div>
                            </div>
                             <div class="row">
                                <div class="col-xs-6"><strong><?php echo __("Site Visit Status"); ?></strong></div>
                                <div class="col-xs-6"><?php 
                                
                                if($task->getSiteVisitStatus() == 0 ){
                                    echo "<b style='color:red;' >".__('Pending')."</b>" ;
                                }else if($task->getSiteVisitStatus() == 1){
                                   
                                     echo "<b style='color:green;' >".__('Done')."</b>" ;
                                }else{
                                 
                                     echo "<b style='color:grey;' >".__('Not Applicable')."</b>" ;
                                }
                                
                                ?></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6"><strong><?php echo __("Assigned By"); ?></strong></div>
                                <div class="col-xs-6"><a href="<?php echo public_path('/backend.php/users/viewuser/userid/'.$task->getCreator()->getNid()); ?>"><?php echo $task->getCreator()->getStrfirstname()." ".$task->getCreator()->getStrlastname(); ?></a></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6"><strong><?php echo __("Assigned To"); ?></strong></div>
                                <div class="col-xs-6"><a href="<?php echo public_path('/backend.php/users/viewuser/userid/'.$task->getOwner()->getNid()); ?>"><?php echo $task->getOwner()->getStrfirstname()." ".$task->getOwner()->getStrlastname(); ?></a></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6"><strong><?php echo __("Application Status"); ?></strong></div>
                                <div class="col-xs-6">
                                  <?php echo $application->getStatusName(); ?>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-xs-6">
                                    <a target="blank" class="btn btn-success" href="/backend.php/applications/view/id/<?php echo $application->getId() ?>"
                                       <strong><?php echo __("View Application Details"); ?></strong> </a> </div>
                                
                            </div>
                            
                        </div><!-- col-sm-6 -->
                    </div><!-- row -->
                </div>
               
            </div><!-- row -->

        </div><!-- panel-body -->
    </div>
        <!-- OTB patch -->
        <?php if($task->getStatusName() == 'Pending'): ?>
         <div class="panel panel-default">
                        <div class="panel-title">
                            <?php echo __("Site Visit Details"); ?>
                        </div>
                        <div class="panel-body panel-bordered" style="border-top:0;">
                            <div class="alert alert-info"> 
                             You can now mark site visit status . Select from below options and click update to change 
                             project site visit status
                            </div>
                            <form class="form-bordered" action="/backend.php/tasks/siteVisitUpdate" method="post">
                                 <div class="form-group">
                                    <label class="col-sm-4"><i class="bold-label"><?php echo __('Select Site Visit Status'); ?></i></label>
                                    <div class="col-sm-8 rogue-input">
                                        <select id="site_visit_status" name="site_visit_status">
                                            <option value="0"> Site Visit Pending </option>
                                            <option value="1"> Done </option>
                                            <option value="2"> Not Applicable </option>
                                        </select>
                                        <input type="hidden" value="<?php echo $task->getId() ?>" name="task_id" id="task_id" />
                                    </div>
                                  </div>
                                <div class="form-group">
                                    <input class="btn btn-warning" type="submit" value="Update" />
                                </div>
                            </form>
                        </div>
         </div>
        <?php endif; ?>

  
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <form class="form" action="<?php echo public_path(); ?>backend.php/tasks/save/redirect/<?php echo $task->getId(); ?>" method="post" autocomplete="off">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo __('New Task'); ?></h4>
      </div>
      <div class="modal-body modal-body-nopadding" id="newtask">
        Content goes here...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close'); ?></button>
        <button type="submit" class="btn btn-primary"><?php echo __('Save changes'); ?></button>
      </div>
    </form>
    </div><!-- modal-content -->
  </div><!-- modal-dialog -->
</div><!-- modal -->


<script language="javascript">
	$("#newtask").load("<?php echo public_path(); ?>backend.php/tasks/new/application/<?php echo $application->getId(); ?>");
    <?php
    if($decline_warning == true)
    {
        ?> alert("You still have an unresolved reason for decline. Please mark it as resolved."); <?php
    }
	?>
</script>
