<?php

/**
 * templates actions.
 *
 * @package    permit
 * @subpackage templates
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class templatesActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeSaveinvoice(sfWebRequest $request)
  {
	 $q = Doctrine_Query::create()
	     ->from('WebLayout a')
		 ->where('a.id = ?', $request->getParameter("id"));
     $this->invoice = $q->fetchOne();
	 
	 $this->invoice->setContent($request->getPostParameter("txtcontent"));
	 $this->invoice->save();
	 
	 $this->redirect("/backend.php/templates/invoices");
		 
  }
  public function executeEditinvoice(sfWebRequest $request)
  {
	 $q = Doctrine_Query::create()
	     ->from('WebLayout a')
		 ->where('a.id = ?', $request->getParameter("id"));
     $this->invoice = $q->fetchOne();
		 
  }
  public function executeInvoices(sfWebRequest $request)
  {
	  
  }
  public function executeShowtemplates(sfWebRequest $request)
  {
	  
  }
  public function executeSelecttemplate(sfWebRequest $request)
  {
	  
  }
  public function executeDeletetemplate(sfWebRequest $request)
  {
	  
  }
  public function executeEdittemplatestep1(sfWebRequest $request)
  {
	  
  }
  public function executeEdittemplatestep2(sfWebRequest $request)
  {
	  $this->setLayout(false);
  }
  
  public function executeEdittemplatestep3(sfWebRequest $request)
  {
	  
  }
  
  public function executeEdittemplatewrite(sfWebRequest $request)
  {
	  
  }
  public function executeDeleteslot(sfWebRequest $request)
  {
	  $slotid = $request->getParameter('slotid');
    
    $q = Doctrine_Query::create()
        ->from("TaskFormsSettings a")
        ->where("a.id = ?", $slotid);
    $commentsheet = $q->fetchOne();

    $commentsheet->delete();

    $this->redirect('/backend.php/settings/workflow');
  }
  public function executeEditslot(sfWebRequest $request)
  {
      if($request->getParameter('slotid') != "")
	  {
		$this->slotid = $request->getParameter('slotid');
	  }
	  else
	  {
		$this->slotid = -1;
	  }
    $this->setLayout(false);
  }
  public function executeSlotup(sfWebRequest $request)
  {
	  $this->slotid = $request->getParameter('slotid');
  }
  public function executeSlotdown(sfWebRequest $request)
  {
	  $this->slotid = $request->getParameter('slotid');
  }

    public function executeNewslot(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    public function executeCreateslot(sfWebRequest $request)
    {
        $setting = new TaskFormsSettings();
        $setting->setTaskApplicationId($request->getPostParameter("applicationid"));
        $setting->setTaskCommentSheet($request->getPostParameter("commentsheetid"));
        $setting->setTaskDepartment($request->getPostParameter("task_department"));
        $setting->setTaskType($request->getPostParameter("task_type"));
        $setting->save();

        $this->redirect("/backend.php/settings/workflow?load=commentsheets");
    }

  public function executeWriteslot(sfWebRequest $request)
  {
	  $q = Doctrine_Query::create()
        ->from("TaskFormsSettings a")
        ->where("a.id = ?", $request->getPostParameter("slotid"));
      $setting = $q->fetchOne();

      $setting->setTaskApplicationId($request->getPostParameter("applicationid"));
      $setting->setTaskCommentSheet($request->getPostParameter("commentsheetid"));
      $setting->setTaskDepartment($request->getPostParameter("task_department"));
      $setting->setTaskType($request->getPostParameter("task_type"));
      $setting->save();

      $this->redirect("/backend.php/settings/workflow?load=commentsheets");
  }
}
