<?php
    $q = Doctrine_Query::create()
        ->from("TaskFormsSettings a")
        ->where("a.id = ?", $slotid);
    $commentsheet = $q->fetchOne();
?>
<?php
use_helper("I18N");
?>
<div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
  <strong><?php echo __('Well done'); ?>!</strong> <?php echo __('You successfully updated this commentsheet'); ?></a>.
</div>

<form id="commentsheetform" class="form-bordered form-horizontal" action="/backend.php/templates/writeslot" id="EditSlot" name="EditSlot" method="POST"  autocomplete="off" data-ajax="false">

<div class="panel panel-dark">
<div class="panel-heading">
<h3 class="panel-title"><?php echo __('Edit Comment Sheet'); ?></h3>
</div>

<div class="panel-body panel-body-nopadding">



			<div class="form-group">
			   <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Application Form'); ?></i> </label>
			   <div class="col-sm-8">
					<select name='applicationid' id='applicationid' class="form-control">
					<?php
				//Get list of all available applications categorized by groups
				$q = Doctrine_Query::create()
				  ->from('FormGroups a');
				$groups = $q->execute();

				if(sizeof($groups) > 0)
				{
					foreach($groups as $group)
					{
						echo "<optgroup label='".$group->getGroupName()."'>";

						$q = Doctrine_Query::create()
						  ->from('ApForms a')
						  ->leftJoin('a.ApFormGroups b')
						  ->where('a.form_id = b.form_id')
						  ->andWhere('b.group_id = ?', $group->getGroupId());
						$forms = $q->execute();

						$count = 0;

						foreach($forms as $apform)
						{
							$selected = "";

							if($apform->getFormId() == $commentsheet->getTaskApplicationId())
							{
								$selected = "selected";
							}


							echo "<option value='".$apform->getFormId()."' ".$selected.">".$apform->getFormDescription()."</option>";

							$count++;
						}
					echo "</optgroup>";
					}
				}
				else
				{
						$q = Doctrine_Query::create()
						  ->from('ApForms a')
						  ->where('a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ?',array('6','7','15','16','17'))
						  ->orderBy('a.form_id ASC');
						$forms = $q->execute();

						$count = 0;

						echo "<optgroup label='Application Forms'>";

						foreach($forms as $apform)
						{

							$selected = "";

							if($apform->getFormId() == $commentsheet->getTaskApplicationId())
							{
								$selected = "selected";
							}

							echo "<option value='".$apform->getFormId()."' ".$selected.">".$apform->getFormName()." (".$apform->getFormDescription().")</option>";

							$count++;
						}

						echo "</optgroup>";
				}
				?>
					</select>
			   </div>
			</div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Comment Sheet'); ?></i> </label>
                <div class="col-sm-8">
                    <select name='commentsheetid' id='commentsheetid' class="form-control">
                        <?php
                            $q = Doctrine_Query::create()
                                ->from('ApForms a')
                                ->where('a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ? AND a.form_id <> ?',array('6','7','15','16','17'))
                                ->orderBy('a.form_id ASC');
                            $forms = $q->execute();

                            $count = 0;

                            echo "<optgroup label='Application Forms'>";

                            foreach($forms as $apform)
                            {

                                $selected = "";

                                if($apform->getFormId() == $commentsheet->getTaskCommentSheet())
                                {
                                    $selected = "selected";
                                }

                                echo "<option value='".$apform->getFormId()."' ".$selected.">".$apform->getFormName()." (".$apform->getFormDescription().")</option>";

                                $count++;
                            }
                        ?>
                    </select>
                </div>
            </div>



            <div class="form-group">
                <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Department'); ?></i></label>
                <div class="col-sm-8">
                    <select name="task_department" id="task_department" class="form-control">
                        <option>Choose Department</option>
                        <?php
                        $q = Doctrine_Query::create()
                            ->from("Department a")
                            ->orderBy("a.department_name ASC");
                        $departments = $q->execute();

                        foreach($departments as $department)
                        {
                        	$selected = "";

                        	if($department->getDepartmentName() == $commentsheet->getTaskDepartment())
                        	{
                        		$selected = "selected";
                        	}
                        ?>
                        <option value="<?php echo $department->getDepartmentName(); ?>" <?php echo $selected; ?>><?php echo $department->getDepartmentName(); ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"><i class="bold-label"><?php echo __('Type of task'); ?></i> </label>
                <div class="col-sm-8">
                    <select name="task_type" id="task_type" class="form-control">
                        <option value="2" <?php if($commentsheet->getTaskType() == "2"){ echo "selected"; } ?>><?php echo __('Assessment'); ?></option>
                        <option value="3" <?php if($commentsheet->getTaskType() == "3"){ echo "selected"; } ?>><?php echo __('Invoicing'); ?></option>
                        <option value="6" <?php if($commentsheet->getTaskType() == "6"){ echo "selected"; } ?>><?php echo __('Inspection'); ?></option>
                        <option value="4" <?php if($commentsheet->getTaskType() == "4"){ echo "selected"; } ?>><?php echo __('Scanning'); ?></option>
                        <option value="5" <?php if($commentsheet->getTaskType() == "5"){ echo "selected"; } ?>><?php echo __('Collection'); ?></option>
                    </select>
                </div>
            </div>



           </div>  <!--panel-body-->

            <div class="panel-footer">
                <input type="hidden" name="slotid" id="slotid" value="<?php echo $commentsheet->getId(); ?>">
			    <button class="btn btn-danger mr10"><?php echo __('Reset'); ?></button> <button type="submit" class="btn btn-primary" name="submitbuttonname" id="submitbuttonname" value="submitbuttonvalue"><?php echo __('Submit'); ?></button>
	        </div>

    </div>

	    </form>

<script language="javascript">
 jQuery(document).ready(function(){
	$("#submitbuttonname").click(function() {
		 $.ajax({
			url: '/backend.php/templates/writeslot',
			cache: false,
			type: 'POST',
			data : $('#commentsheetform').serialize(),
			success: function(json) {
				$('#alertdiv').attr("style", "display: block;");
				$("html, body").animate({ scrollTop: 0 }, "slow");
			}
		});
		return false;
	 });
	});
</script>
