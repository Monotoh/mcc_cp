<?php

use_helper("I18N");

if($sf_user->mfHasCredential("managecommentsheets"))
{
  $_SESSION['current_module'] = "commentsheets";
  $_SESSION['current_action'] = "index";
  $_SESSION['current_id'] = "";
?>
<?php
    $q = Doctrine_Query::create()
       ->from("TaskFormsSettings a")
       ->orderBy("a.id DESC");
    $commentsheets = $q->execute();
?>


 
<div class="panel panel-dark">
<div class="panel-heading">
			<h3 class="panel-title"><?php echo __('Comment Sheets'); ?></h3>
				<div class="pull-right">
            <a class="btn btn-primary-alt" style="margin-top:-42px;" id="newsheet"><?php echo __('New Comment Sheet'); ?></a>

            <script language="javascript">
            jQuery(document).ready(function(){
              $( "#newsheet" ).click(function() {
                  $("#contentload").load("<?php echo public_path(); ?>backend.php/templates/newslot");
              });
            });
            </script>
</div>
</div>
		
 
<div class="panel panel-body panel-body-nopadding ">

<div class="table-responsive">
<table class="table dt-on-steroids mb0" id="table3">
	  <thead>
		<tr>
      <th class="no-sort" style="width: 10px;"><input type='checkbox' name='batchall' onclick="boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.name == 'batch') { if(this.checked == true){ box.checked = false; }else{ box.checked = true; } } } "></th>
       			<th width="60">#</th>
                <th class="no-sort"><?php echo __('Comment Sheet'); ?></th>
                <th><?php echo __('Department'); ?></th>
                <th><?php echo __('Type'); ?></th>
                <th class="no-sort" width="7%"><?php echo __('Actions'); ?></th>
          </tr>
		</thead>
        <tbody>
			<?php
                $count = 0;
					foreach ($commentsheets as $commentsheet)
					{
                        $count++;

                        $q = Doctrine_Query::create()
                           ->from("ApForms a")
                            ->where("a.form_id = ?", $commentsheet->getTaskApplicationId());
                        $form = $q->fetchOne();

                        $type = "";

                        if($commentsheet->getTaskType() == "1")
                        {
                            $type = __("Review");
                        }
                        else if($commentsheet->getTaskType() == "2")
                        {
                            $type = __("Assessment");
                        }
                        else if($commentsheet->getTaskType() == "6")
                        {
                            $type = __("Inspection");
                        }
                        else if($commentsheet->getTaskType() == "3")
                        {
                            $type = __("Invoicing");
                        }
                        else if($commentsheet->getTaskType() == "4")
                        {
                            $type = __("Scanning");
                        }
                        else if($commentsheet->getTaskType() == "5")
                        {
                            $type = __("Collection");
                        }

						echo "<tr>";
						
						echo "<td><input type='checkbox' name='batch' id='batch_".$commentsheet->getId()."' value='".$commentsheet->getId()."'></td>";
	                	
						echo "<td>".$count."</td>";
						
						echo "<td>".$form->getFormName().", ".$form->getFormDescription()."</td>";

            echo "<td>".$commentsheet->getTaskDepartment()."</td>";

            echo "<td>".$type."</td>";
											
						echo "<td>";
						echo "<a id='editsheet".$commentsheet->getId()."' href=\"#\" alt=\"Anzeigen\" title=\"".__('Edit')."\"><span class=\"badge badge-primary\"><i class=\"fa fa-pencil\"></i></span></a>";
						
            $q = Doctrine_Query::create()
               ->from("TaskForms a")
               ->where("a.form_id = ?", $commentsheet->getTaskCommentSheet());
            $taskforms = $q->execute();

            if(sizeof($taskforms) <= 0)
            {
              echo "<a id='deletesheet".$commentsheet->getId()."' href=\"#\" title=\"".__('Delete')."\"><span class=\"badge badge-primary\"><i class=\"fa fa-trash-o\"></i></span></a>";
            }

            ?>
             <script language="javascript">
            jQuery(document).ready(function(){
              $( "#editsheet<?php echo $commentsheet->getId(); ?>" ).click(function() {
                  $("#contentload").load("<?php echo public_path(); ?>backend.php/templates/editslot/slotid/<?php echo $commentsheet->getId(); ?>");
              });
              $( "#deletesheet<?php echo $commentsheet->getId(); ?>" ).click(function() {
                if(confirm('Are you sure you want to delete this button?')){
                  $("#contentload").load("<?php echo public_path(); ?>backend.php/templates/deleteslot/slotid/<?php echo $commentsheet->getId(); ?>");
               
                }
                else
                {
                  return false;
                }
              });
            });
            </script>
            <?php
						echo "</td></tr>";
					}
			?>   
    </tbody>
	<tfoot>
   <tr><td colspan='7' style='text-align: left;'>
   <select id='batch_action' name='batch_action' onChange="if(this.value != ''){if(confirm('Are you sure?')){ batch('templates', this.options[this.selectedIndex].text, this.value); document.getElementById('default').selected='selected'; }}">
   <option id='default' value=''><?php echo __('Choose an action'); ?>..</option>
   <option value='delete'><?php echo __('Set As Deleted'); ?></option>
   </select>
   </td></tr>
   </tfoot>
</table>
</div>
</div><!--panel-body-->
</div><!--panel-dark-->
<script>
  jQuery('#table3').dataTable({
      "sPaginationType": "full_numbers",
     
      // Using aoColumnDefs
      "aoColumnDefs": [
      	{ "bSortable": false, "aTargets": [ 'no-sort' ] }
    	]
    });
    
</script>

<?php
}
else
{
  include_partial("accessdenied");
}
?>
