<?php
	$prefix_folder = dirname(__FILE__)."/../../../../..";
	//--- write user to database
	include_once ($prefix_folder."/lib/vendor/cp_workflow/config/config.inc.php");
	include_once ($prefix_folder."/lib/vendor/cp_workflow/language_files/language.inc.php");
	
	//--- open database
	$nConnection = mysql_connect($DATABASE_HOST, $DATABASE_UID, $DATABASE_PWD);
	
	
	
	if ($nConnection)
	{
		if (mysql_select_db($DATABASE_DB, $nConnection))
		{
				
			if ($_REQUEST["slotid"] == "-1" || $_REQUEST["slotid"] <= 0)
			{
				//--- add new slot
				$strQuery = "SELECT MAX(nslotnumber) FROM cf_formslot where ntemplateid=".$_REQUEST["templateid"];
				$nResult = mysql_query($strQuery, $nConnection);
					
				if ($nResult)
	    		{
	    			if (mysql_num_rows($nResult) > 0)
    				{
    					$arrRow = mysql_fetch_array($nResult);
						$nMaxSlotNumber = $arrRow[0];
					}
				}
				
				$query = "";
				if($_REQUEST['strNameExt'] && $_REQUEST['strNameExt'] != "")
				{
				$query = "INSERT INTO cf_formslot values (null, \"".$_REQUEST["strName"]." - ".$_REQUEST["strNameExt"]."\", ".$_REQUEST["templateid"].", ".($nMaxSlotNumber+1).", ".$_REQUEST["applicationid"].")";
				}
				else
				{
				$query = "INSERT INTO cf_formslot values (null, \"".$_REQUEST["strName"]."\", ".$_REQUEST["templateid"].", ".($nMaxSlotNumber+1).", ".$_REQUEST["applicationid"].")";
				}
				
				$nResult = mysql_query($query, $nConnection);

				$entryid = mysql_insert_id($nConnection);
				$_REQUEST['slotid'] = $entryid;
				$audit = new Audit();
				$audit->saveAudit("", "<a href=\"/backend.php/templates/editslot?slotid=".$entryid."&language=en\">added a new slot</a>");
			}
			else
			{
				//--- update existing slot
				$query = "";
				if($_REQUEST['strNameExt'] && $_REQUEST['strNameExt'] != "")
				{
					$query = "UPDATE cf_formslot SET strname=\"".$_REQUEST["strName"]." - ".$_REQUEST["strNameExt"]."\", nsendtype=".$_REQUEST["applicationid"]." WHERE nid=".$_REQUEST["slotid"];
				}
				else
				{
					$query = "UPDATE cf_formslot SET strname=\"".$_REQUEST["strName"]."\", nsendtype=".$_REQUEST["applicationid"]." WHERE nid=".$_REQUEST["slotid"];
				}
				$nResult = mysql_query($query, $nConnection);
				$audit = new Audit();
				$audit->saveAudit("", "<a href=\"/backend.php/templates/editslot?slotid=".$_REQUEST["slotid"]."&language=en\">updated a slot</a>");
			}
			
			
		}
	}	
	
	//Delete CfSlottofield entries for this slot and repopulate with selected field
	$q = Doctrine_Query::create()
	   ->from('CfSlottofield a')
	   ->where('a.nslotid = ?', $_REQUEST['slotid'])
	   ->orderBy('a.nposition ASC');
	 $assigned_fields = $q->execute();
	 
	 foreach($assigned_fields as $field)
	 {
		$field->delete();
	 }
	
	$fields = $_REQUEST['commentfields'];
	$count = 0;
	foreach($fields as $field)
	{
		$count++;
		$slottofield = new CfSlottofield();
		$slottofield->setNslotid($_REQUEST['slotid']);
		$slottofield->setNfieldid($field);
		$slottofield->setNposition($count);
		$slottofield->save();
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<?php 
		echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=".$DEFAULT_CHARSET."\" />";
	?>
	
	<script language="JavaScript">
		function onLoad()
		{
			document.location.href="/backend.php/templates/edittemplatestep2?language=<?php echo $_REQUEST["language"];?>&start=<?php echo $_REQUEST["start"];?>&sortby=<?php echo $_REQUEST["sortby"];?>&templateid=<?php echo $_REQUEST["templateid"];?>&reload=1";
		}
	</script>
</head>
<body onLoad="onLoad()">

</body>
