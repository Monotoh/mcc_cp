<?php
/**
 * Themeupdate actions.
 *
 * Theme Management Service
 *
 * @package    backend
 * @subpackage themeupdate
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
class themeupdateActions extends sfActions
{
   /**
   * Executes 'Check' action 
   * 
   * Checks for available theme
   *
   * @param sfRequest $request A request object
   */
    public function executeCheck(sfWebRequest $request)
    {

    }
    
   /**
   * Executes 'Update' action 
   * 
   * Applies available theme
   *
   * @param sfRequest $request A request object
   */
    public function executeUpdate(sfWebRequest $request)
    {

    }
    
   /**
   * Executes 'Backup' action 
   * 
   * Backups up current theme
   *
   * @param sfRequest $request A request object
   */
    public function executeBackup(sfWebRequest $request)
    {

    }
}
