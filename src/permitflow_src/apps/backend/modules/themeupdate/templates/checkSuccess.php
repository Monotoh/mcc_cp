<?php
/**
 * checkSuccess.php template.
 *
 * Checks for available theme
 *
 * @package    backend
 * @subpackage themeupdate
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>
<div class="g12" style="padding-left: 3px;">
    <form style="margin-bottom: 0px;">
        <label style='height: 30px; margin-top: 0px;'>
        <div style='float: left; font-size: 20px; font-weight: 700;'>Check For Updates</div>
        <div style="float: right; margin-top: -12px;"> </div>
        </label>
    </form>

	<div id='notifications' name='notifications'></div>
    <div>
    <?php
	 
	 $prefix_folder = dirname(__FILE__)."/../../../../../../public_html";
	 require_once($prefix_folder."/masterupdate.php");

     $update_script_file_name = $prefix_folder."/update.zip";
	 $update_script_md5_checksum_filename = $prefix_folder."/update.MD5";
     $backup_script_file_name = $prefix_folder."/backup.zip";
	 $backup_script_md5_checksum_filename = $prefix_folder."/system.MD5";
	 $source_code_folder = $prefix_folder."/../admincp1.3/";
	 
	 $update_server = sfConfig::get('app_update_server');
	 
	 //Begin update process
	 if(download_update_md5_checksum($update_script_md5_checksum_filename,$update_server))
	 {
		 if(validate_update($update_script_md5_checksum_filename, $backup_script_md5_checksum_filename))
		 {
			 if(download_update($update_script_file_name,$update_server))
			 {
				 //Compare Update MD5 with Downloaded Update
				 if(validate_update_checksums($update_script_md5_checksum_filename,$update_script_file_name))
				 {
					 $updatemd5 = get_checksum_values($update_script_md5_checksum_filename);
					 //If update is valid then proceed
					 //Should probably perform a backup first and label them by version
					 echo "<br>...Update Version '".$updatemd5['version']."' is Available. ";
					 echo "<button onClick=\"window.location='/backend.php/systemupdate/update';\">Update Now</button>";
				 }
				 else
				 {
					 echo "<br>...Invalid Checksums. Update package is not valid or has been tampered!";
				 }
			 }
			 else
			 {
				 echo "<br>...Failed to fetch update package. Invalid update link."; 
			 }
		 }
		 else
		 {
			//echo "<br>...Failed to validate checksum. Inconsistency betweeen current system version and update version."; 
		 }
	 }
	 else
	 {
	  	echo "<br>...Failed to fetch update checksum. Invalid update link."; 
	 }
	?>
    </div>
</div>


