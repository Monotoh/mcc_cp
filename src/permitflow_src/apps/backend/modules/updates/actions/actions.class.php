<?php

/**
 * updates actions.
 *
 * @package    symfony
 * @subpackage updates
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class updatesActions extends sfActions
{
 /**
  * Executes add action
  *
  * @param sfRequest $request A request object
  */
  public function executeRestadd(sfWebRequest $request)
  {
    //Protect request with authentication
    if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW']))
    {
      $username = $_SERVER['PHP_AUTH_USER'];
      $password = $_SERVER['PHP_AUTH_PW'];

      $q = Doctrine_Query::create()
         ->from("CfUser a")
         ->where("a.struserid = ? OR a.stremail = ?", array($username,$username))
         ->andWhere('a.bdeleted = ?', 0);
      $available_user = $q->fetchOne();

      if($available_user)
      {
          $hash = $available_user->getStrpassword();
            if (password_verify($password, $hash)) {
                //rehash password if weak //just a bcrypt standard operation
                if (password_needs_rehash($hash, PASSWORD_BCRYPT, $options)) {
                    $hash = password_hash($password, PASSWORD_BCRYPT, $options);
                    /* Store new hash in db */
                    $available_user->setStrpassword($hash);
                    $available_user->save();
                }
                
                //The user has been authenticated so perform some action
                $rest_code = $request->getPostParameter("rest_code");
                $data = $request->getPostParameter("data");
                
                //get table name from rest_code
                $q = Doctrine_Query::create()
                   ->from("RestCrud a")
                   ->where("a.rest_code = ?", $rest_code);
                $rest_service = $q->fetchOne();

                if($rest_service)
                {
                    $rest_table = $rest_service->getRestTable();
                    $fields = json_decode($data, true);
                    
                    $sql_fields = "";
                    $sql_values = "";
                    
                    $count = 0;
                    foreach($fields as $key => $value)
                    {
                        $count++;
                        
                        if($count == 1)
                        {
                            $sql_fields .= "".$key."";
                            $sql_values .= "'".$value."'";
                        }
                        else
                        {
                            $sql_fields .= ",".$key."";
                            $sql_values .= ",'".$value."'";
                        }
                    }
                    
                    // Create connection
                    $conn = new mysqli(sfConfig::get('app_mysql_host'), sfConfig::get('app_mysql_user'), sfConfig::get('app_mysql_pass'), sfConfig::get('app_mysql_db'));
                    // Check connection
                    if ($conn->connect_error) {
                        die("Connection failed: " . $conn->connect_error);
                    } 
                    
                    $sql = "INSERT INTO ".$rest_table." (".$sql_fields.",live) VALUES (".$sql_values.",1)";
                    
                    if ($conn->query($sql) === TRUE) {
                        echo "{'result':'success'}";
                    } else {
                        echo "{'result':'error: " . $sql . ": " . $conn->error."'}";
                    }
                    
                    $conn->close();
                    
                    return sfView::NONE;
                }
                else
                {
                    echo "{'result':'invalid rest code'}";
                    return sfView::NONE;
                }
            }
            else
            {
                error_log("Debug-rest: Invalid logins");
                echo "{'result':'invalid logins'}";
                return sfView::NONE;
            }
      }
      else
      {
          error_log("Debug-rest: Unknown user");
          echo "{'result':'invalid logins'}";
          return sfView::NONE;
      }
    }
    else
    {
        $this->getResponse()->setHttpHeader('WWW-Authenticate',  'Basic realm="eCitizen Fetch API"');
        $this->getResponse()->setStatusCode('401');
        $this->sendHttpHeaders();
        return sfView::NONE;
    }
  }
  
 /**
  * Executes remove action
  *
  * @param sfRequest $request A request object
  */
  public function executeRestremove(sfWebRequest $request)
  {
    //Protect request with authentication
    if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW']))
    {
      $username = $_SERVER['PHP_AUTH_USER'];
      $password = $_SERVER['PHP_AUTH_PW'];

      $q = Doctrine_Query::create()
         ->from("CfUser a")
         ->where("a.struserid = ? OR a.stremail = ?", array($username,$username))
         ->andWhere('a.bdeleted = ?', 0);
      $available_user = $q->fetchOne();

      if($available_user)
      {
          $hash = $available_user->getStrpassword();
            if (password_verify($password, $hash)) {
                //rehash password if weak //just a bcrypt standard operation
                if (password_needs_rehash($hash, PASSWORD_BCRYPT, $options)) {
                    $hash = password_hash($password, PASSWORD_BCRYPT, $options);
                    /* Store new hash in db */
                    $available_user->setStrpassword($hash);
                    $available_user->save();
                }
                
                //The user has been authenticated so perform some action
                $rest_code = $request->getPostParameter("rest_code");
                $data = $request->getPostParameter("data");
                
                //get table name from rest_code
                $q = Doctrine_Query::create()
                   ->from("RestCrud a")
                   ->where("a.rest_code = ?", $rest_code);
                $rest_service = $q->fetchOne();

                if($rest_service)
                {
                    $rest_table = $rest_service->getRestTable();
                    $fields = json_decode($data);
                    
                    $sql_stmt = "";
                    
                    $count = 0;
                    foreach($fields as $key => $value)
                    {
                        $count++;
                        
                        if($count == 1)
                        {
                            $sql_stmt .= $key." = '".$value."'";
                        }
                        else
                        {
                            $sql_stmt .= " AND ".$key." = '".$value."'";
                        }
                    }
                    
                    // Create connection
                    $conn = new mysqli(sfConfig::get('app_mysql_host'), sfConfig::get('app_mysql_user'), sfConfig::get('app_mysql_pass'), sfConfig::get('app_mysql_db'));
                    // Check connection
                    if ($conn->connect_error) {
                        die("Connection failed: " . $conn->connect_error);
                    } 
                    
                    $sql = "UPDATE ".$rest_table." SET live = 0 WHERE ".$sql_stmt;
                    
                    if ($conn->query($sql) === TRUE) {
                        echo "{'result':'success'}";
                    } else {
                        echo "{'result':'error: " . $sql . ": " . $conn->error."'}";
                    }
                    
                    $conn->close();
                    
                    return sfView::NONE;
                }
            }
            else
            {
                error_log("Debug-rest: Invalid logins");
                echo "{'result':'invalid logins'}";
                return sfView::NONE;
            }
      }
      else
      {
          error_log("Debug-rest: Unknown user");
          echo "{'result':'invalid logins'}";
          return sfView::NONE;
      }
    }
    else
    {
        $this->getResponse()->setHttpHeader('WWW-Authenticate',  'Basic realm="eCitizen Fetch API"');
        $this->getResponse()->setStatusCode('401');
        $this->sendHttpHeaders();
        return sfView::NONE;
    }
  }
  
  /**
  * Executes update action
  *
  * @param sfRequest $request A request object
  */
  public function executeRestupdate(sfWebRequest $request)
  {
    //Protect request with authentication
    if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW']))
    {
      $username = $_SERVER['PHP_AUTH_USER'];
      $password = $_SERVER['PHP_AUTH_PW'];

      $q = Doctrine_Query::create()
         ->from("CfUser a")
         ->where("a.struserid = ? OR a.stremail = ?", array($username,$username))
         ->andWhere('a.bdeleted = ?', 0);
      $available_user = $q->fetchOne();

      if($available_user)
      {
          $hash = $available_user->getStrpassword();
            if (password_verify($password, $hash)) {
                //rehash password if weak //just a bcrypt standard operation
                if (password_needs_rehash($hash, PASSWORD_BCRYPT, $options)) {
                    $hash = password_hash($password, PASSWORD_BCRYPT, $options);
                    /* Store new hash in db */
                    $available_user->setStrpassword($hash);
                    $available_user->save();
                }
                
                //The user has been authenticated so perform some action
                $rest_code = $request->getPostParameter("rest_code");
                $data = $request->getPostParameter("data");
                $set = $request->getPostParameter("set");
                
                //get table name from rest_code
                $q = Doctrine_Query::create()
                   ->from("RestCrud a")
                   ->where("a.rest_code = ?", $rest_code);
                $rest_service = $q->fetchOne();

                if($rest_service)
                {
                    $rest_table = $rest_service->getRestTable();
                    $fields = json_decode($data);
                    
                    $sql_stmt = "";
                    
                    $count = 0;
                    foreach($fields as $key => $value)
                    {
                        $count++;
                        
                        if($count == 1)
                        {
                            $sql_stmt .= $key." = '".$value."'";
                        }
                        else
                        {
                            $sql_stmt .= ",".$key." = '".$value."'";
                        }
                    }
                    
                    $fields = json_decode($set);
                    
                    $set_stmt = "";
                    
                    $count = 0;
                    foreach($fields as $key => $value)
                    {
                        $count++;
                        
                        if($count == 1)
                        {
                            $set_stmt .= $key." = '".$value."'";
                        }
                        else
                        {
                            $set_stmt .= ",".$key." = '".$value."'";
                        }
                    }
                    
                    // Create connection
                    $conn = new mysqli(sfConfig::get('app_mysql_host'), sfConfig::get('app_mysql_user'), sfConfig::get('app_mysql_pass'), sfConfig::get('app_mysql_db'));
                    // Check connection
                    if ($conn->connect_error) {
                        die("Connection failed: " . $conn->connect_error);
                    } 
                    
                    $sql = "UPDATE ".$rest_table." SET ".$set_stmt.", live = 1 WHERE ".$sql_stmt."";
                    
                    if ($conn->query($sql) === TRUE) {
                        echo "{'result':'success'}";
                    } else {
                        echo "{'result':'error: " . $sql . ": " . $conn->error."'}";
                    }
                    
                    $conn->close();
                    
                    return sfView::NONE;
                }
            }
            else
            {
                error_log("Debug-rest: Invalid logins");
                echo "{'result':'invalid logins'}";
                return sfView::NONE;
            }
      }
      else
      {
          error_log("Debug-rest: Unknown user");
          echo "{'result':'invalid logins'}";
          return sfView::NONE;
      }
    }
    else
    {
        $this->getResponse()->setHttpHeader('WWW-Authenticate',  'Basic realm="eCitizen Fetch API"');
        $this->getResponse()->setStatusCode('401');
        $this->sendHttpHeaders();
        return sfView::NONE;
    }
  }
}
