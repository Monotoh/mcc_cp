<?php

/**
 * usercategories actions.
 *
 * @package    permit
 * @subpackage banner
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class usercategoriesActions extends sfActions
  {
    public function executeBatch(sfWebRequest $request)
    {
      if($request->getPostParameter('delete'))
      {
        $item = Doctrine_Core::getTable('sfGuardUserCategories')->find(array($request->getPostParameter('delete')));
        if($item)
        {
          $item->delete();
        }
      }
    }
    
    //OTB Start - User Membership  Database validation e.g. Boraqs, Engineers Association, Planner's association etc.
    public function executeChangefield(sfWebRequest $request) {
        if ($request->getParameter('form_id')) {
            $element_type = $request->getParameter('email') ? "email" : false;
            $elements = Doctrine_Core::getTable('ApFormElements')->getAllFields($request->getParameter('form_id'), $element_type);
            foreach ($elements as $key => $value) {
                $new_options .= "<option value='" . $key . "'>" . $value . "</option>";
            }
            echo $new_options;
        }
        exit();
    }

    //Update member fields
    //OTB patch

    public function executeUpdatememeberfields(sfWebRequest $request) {

        $form_id = $request->getParameter('form');

        if ($form_id) {

            //Get all field for member no

            $elements_all = Doctrine_Core::getTable('ApFormElements')->getAllFields($form_id);

            $elements_email = Doctrine_Core::getTable('ApFormElements')->getAllFields($form_id, 'email');

            echo json_encode(array('all' => $elements_all, 'email' => $elements_email));
        }

        exit();
    }

    public function executeElementvalues(sfWebRequest $request) {
        $form_id = $request->getParameter('form');
        $element_id = $request->getParameter('element');
        echo json_encode(array('elements' => Doctrine::getTable('ApElementOptions')->getElementOptions($form_id, $element_id)));
        exit;
    }

    protected function setSfFormChoiceWidgetOptionsFromApFormElements($apform_id, $widget_name, $element_type) {
        $widget = $this->form->getWidget($widget_name);
        $widget->setOptions(array('choices' => Doctrine_Core::getTable('ApFormElements')->getAllFields($apform_id, $element_type)));
    }
    public function executeUpdatememeberfieldsagenda(sfWebRequest $request)
	{
		$form_id=$request->getParameter('form');
		if ($form_id){
			//Get all field for member no
		  $elements_all = Doctrine_Core::getTable('ApFormElements')->getAllFormFielsIncludeEmail($form_id);
		  echo json_encode(array('all' => $elements_all));
	
		}
	  exit();
	}

//OTB End - User Membership  Database validation e.g. Boraqs, Engineers Association, Planner's association etc.

    /**
     * Executes 'Checkname' action
     *
     * Ajax used to check existence of name
     *
     * @param sfRequest $request A request object
     */
    public function executeCheckname(sfWebRequest $request)
    {
        // add new user
        $q = Doctrine_Query::create()
           ->from("sfGuardUserCategories a")
           ->where('a.name = ?', $request->getPostParameter('name'));
        $existingcategory = $q->execute();
        if(sizeof($existingcategory) > 0)
        {
              echo '<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Name is already in use!</strong></div><script language="javascript">document.getElementById("submitbuttonname").disabled = true;</script>';
              exit;
        }
        else
        {
              echo '<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Name is available!</strong></div><script language="javascript">document.getElementById("submitbuttonname").disabled = false;</script>';
              exit;
        }
    }


  public function executeIndex(sfWebRequest $request)
  {
    $q = Doctrine_Query::create()
       ->from('sfGuardUserCategories a');
     $this->pager = new sfDoctrinePager('sfGuardUserCategories', 20);
	 $this->pager->setQuery($q);
	 $this->pager->setPage($request->getParameter('page', 1));
	 $this->pager->init();


	$this->setLayout("layout-settings");
  }

  public function executeShow(sfWebRequest $request)
  {
    $this->category = Doctrine_Core::getTable('sfGuardUserCategories')->find(array($request->getParameter('id')));
    $this->forward404Unless($this->category);
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new sfGuardUserCategoriesForm();
	$this->setLayout("layout-settings");
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new sfGuardUserCategoriesForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($category = Doctrine_Core::getTable('sfGuardUserCategories')->find(array($request->getParameter('id'))), sprintf('Object sfGuardUserCategories does not exist (%s).', $request->getParameter('id')));
    $this->form = new sfGuardUserCategoriesForm($category);
	$this->setLayout("layout-settings");
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($category = Doctrine_Core::getTable('sfGuardUserCategories')->find(array($request->getParameter('id'))), sprintf('Object sfGuardUserCategories does not exist (%s).', $request->getParameter('id')));
    $this->form = new sfGuardUserCategoriesForm($category);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {

    $this->forward404Unless($category = Doctrine_Core::getTable('sfGuardUserCategories')->find(array($request->getParameter('id'))), sprintf('Object sfGuardUserCategories does not exist (%s).', $request->getParameter('id')));


      $audit = new Audit();
      $audit->saveAudit("", "deleted user category image of id ".$category->getId());

    $category->delete();

    $this->redirect('/backend.php/usercategories/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $category = $form->save();

	  $q = Doctrine_Query::create()
	     ->from("SfGuardUserCategoriesForms a")
		 ->where("a.categoryid = ?", $category->getId());
	  $links = $q->execute();
	  foreach($links as $link)
	  {
			$link->delete();
	  }

	  $forms = $request->getPostParameter("islinkedto");

	  foreach($forms as $apform)
	  {
		  $linkto = new SfGuardUserCategoriesForms();
		  $linkto->setCategoryid($category->getId());
		  $linkto->setFormid($apform);

		  if($request->getPostParameter("link_form_".$apform))
		  {
			  $linkto->setIslinkedto($request->getPostParameter("link_form_".$apform));
			  $linkto->setIslinkedtitle($request->getPostParameter("link_title_".$apform));
		  }

		  $linkto->save();

	  }



      $audit = new Audit();
      $audit->saveAudit("", "<a href=\"/backend.php/usercategories/edit?id=".$category->getId()."\">updated banner image</a>");

      $this->redirect('/backend.php/usercategories/index');
    }
  }
}
