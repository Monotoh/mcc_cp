<?php
	use_helper("I18N");
?>
<div class="contentpanel">
<div class="panel panel-dark">
<div class="panel-heading">
<h3 class="panel-title"><?php echo __('Edit User Category'); ?></h3>
</div>

<?php include_partial('form', array('form' => $form)) ?>

</div>
</div>
