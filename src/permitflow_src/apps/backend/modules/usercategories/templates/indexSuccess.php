<?php
$audit = new Audit();
$audit->saveAudit("", "Accessed user categories settings");

if($sf_user->mfHasCredential("managecategories"))
{
  $_SESSION['current_module'] = "categories";
  $_SESSION['current_action'] = "index";
  $_SESSION['current_id'] = "";
?>
<?php use_helper('I18N', 'Date') ?>

<div class="contentpanel panel-email">

<div class="panel panel-dark">
<div class="panel-heading">
  <h3 class="panel-title"><?php echo __('User Categories'); ?></h3>

    <div class="pull-right">
  <a id="newcategory" class="btn btn-primary-alt tooltips pull-right" type="button" data-toggle="tooltip" title="New Member Category" style="margin-top:-28px;" href="<?php echo public_path(); ?>backend.php/usercategories/new"><?php echo __('Add New User Category'); ?></a>

  </div>
 </div>
<div class="panel-body panel-body-nopadding">


<table id="table3" class="table mb0 dt-on-steroids">
    <thead>
   <tr>
      <th><input type='checkbox' name='batchall' onclick="boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.name == 'batch') { if(this.checked == true){ box.checked = true; }else{ box.checked = false; } } } "></th>
      <th width="60">#</th>
      <th><?php echo __('Name'); ?></th>
      <th><?php echo __('Description'); ?></th>
      <th><?php echo __('Order'); ?></th>
      <th><?php echo __('Actions'); ?></th>
    </tr>
  </thead>
    <tbody>
    <?php foreach ($pager->getResults() as $category): ?>
    <tr id="row_<?php echo $category->getId(); ?>">
	  <td><input type='checkbox' name='batch' id='batch_<?php echo $category->getId() ?>' value='<?php echo $category->getId() ?>'></td>
	  <td id="row_<?php echo $category->getId(); ?>"><?php echo $category->getId() ?></td>
      <td><?php echo $category->getName() ?></td>
      <td><?php echo $category->getDescription() ?></td>
      <td><?php echo $category->getOrderId() ?></td>
      <td>
		    <a id="editcategory<?php echo $category->getId(); ?>" href="<?php echo public_path(); ?>backend.php/usercategories/edit/id/<?php echo $category->getId(); ?>" title="<?php echo __('Edit'); ?>"><span class="badge badge-primary"><i class="fa fa-pencil"></i></span></a>
		    <?php
        $q = Doctrine_Query::create()
           ->from("SfGuardUserProfile a")
           ->where("a.registeras = ?", $category->getId());
        $related_profiles = $q->count();
        if($related_profiles <= 0)
        {
        ?>
      	<a id="deletecategory<?php echo $category->getId(); ?>" onClick="if(confirm('Are you sure you want to delete this item?')){ return true; }else{ return false; }" title="<?php echo __('Delete'); ?>" href="<?php echo public_path(); ?>backend.php/usercategories/delete/id/<?php echo $category->getId(); ?>"><span class="badge badge-primary"><i class="fa fa-trash-o"></i></span></a>
        <?php
        }
        ?>
	</td>
    </tr>
    <?php endforeach; ?>
    </tbody>
	<tfoot>
   <tr><td colspan='7' style='text-align: left;'>
   <select id='batch_action' name='batch_action' onChange="if(this.value != ''){if(confirm('Are you sure?')){ batch('usercategories', this.options[this.selectedIndex].text, this.value); document.getElementById('default').selected='selected'; }}">
   <option id='default' value=''><?php echo __('Choose an action'); ?>..</option>
   <option value='delete'><?php echo __('Set As Deleted'); ?></option>
   </select>
   </td></tr>
   </tfoot>
</table>

 </div>
 </div>
 
</div>

<script language='javascript'>
    jQuery(document).ready(function() {

    		jQuery('#table3').dataTable({
           "sPaginationType": "full_numbers"
         });

	});
</script>
<?php
}
else
{
  include_partial("settings/accessdenied");
}
?>
