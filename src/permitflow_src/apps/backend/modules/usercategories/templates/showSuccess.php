
<?php 
$prefix_folder = dirname(__FILE__)."/../../../../..";
require($prefix_folder.'/lib/vendor/cp_form/includes/check-session.php'); ?>
<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $banner->getId() ?></td>
    </tr>
    <tr>
      <th>Title:</th>
      <td><?php echo $banner->getTitle() ?></td>
    </tr>
    <tr>
      <th>Description:</th>
      <td><?php echo $banner->getDescription() ?></td>
    </tr>
    <tr>
      <th>Image:</th>
      <td><?php echo $banner->getImage() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('banner/edit?id='.$banner->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('banner/index') ?>">List</a>
