<?php
/**
 * Users actions.
 *
 * Reviewers Management Service.
 *
 * @package    backend
 * @subpackage users
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
class usersActions extends sfActions
{
    public function executeBatch(sfWebRequest $request)
    {
      if($request->getPostParameter('delete'))
      {
        $q = Doctrine_Query::create()
           ->from("CfUser a")
           ->where('a.nid = ?', $request->getPostParameter('delete'));
        $item = $q->fetchOne();
        if($item)
        {
          $item->delete();
        }
      }
    }

    /**
     * Executes 'Checkuser' action
     *
     * Ajax used to check existence of username
     *
     * @param sfRequest $request A request object
     */
    public function executeCheckuser(sfWebRequest $request)
    {
        // add new user
        $q = Doctrine_Query::create()
           ->from("CfUser a")
           ->where('a.Struserid = ?', $request->getPostParameter('username'));
        $existinguser = $q->execute();
        if(sizeof($existinguser) > 0)
        {
              echo '<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Username is already in use!</strong></div><script language="javascript">document.getElementById("submitbutton").disabled = true;</script>';
              exit;
        }
        else
        {
              echo '<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Username is available!</strong></div><script language="javascript">document.getElementById("submitbutton").disabled = false;</script>';
              exit;
        }
    }

    /**
     * Executes 'Reset' action
     *
     * Ajax used to check existence of username
     *
     * @param sfRequest $request A request object
     */
    public function executeReset(sfWebRequest $request)
    {
        // add new user
        $q = Doctrine_Query::create()
           ->from("CfUser a")
           ->where('a.stremail = ?', $request->getParameter('email'));
        $existinguser = $q->fetchOne();
        if($existinguser)
        {
            $random_pass = rand(10000, 1000000);
            $random_code = rand(10000, 1000000000);

            $temp_pass = password_hash($random_pass, PASSWORD_BCRYPT);
            $temp_code = md5($random_code);

            $existinguser->setStrtemppassword($temp_pass);
            $existinguser->setStrtoken($temp_code);

            $existinguser->save();

              //Send account recovery email
              $body = "
                Hi {$existinguser->getStrfirstname()} {$existinguser->getStrlastname()}, <br>
                <br>
                You have requested to reset your account password. Use the link below to reset it now: <br>
                <br>
                Temporary Password: {$random_pass}
                <br>
                ---- <br>
                http://".$_SERVER['HTTP_HOST']."/backend.php/login/recover/code/{$temp_code} <br>
                ---- <br>
                <br>
                Thanks,<br>
                ".sfConfig::get('app_organisation_name').".<br>
            ";

            $mailnotifications = new mailnotifications();
            $mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $existinguser->getStremail(),"Password Reset",$body);

            $this->success = true;
        }
        else
        {
            $this->success = false;
        }
    }

    /**
     * Executes 'Checkemail' action
     *
     * Ajax used to check existence of email
     *
     * @param sfRequest $request A request object
     */
    public function executeCheckemail(sfWebRequest $request)
    {
        // add new user
        $q = Doctrine_Query::create()
           ->from("CfUser a")
           ->where('a.Stremail = ?', $request->getPostParameter('email'));
        $existinguser = $q->execute();
        if(sizeof($existinguser) > 0)
        {
              echo '<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Email is already in use!</strong></div>';
              exit;
        }
        else
        {
              echo '<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Email is available!</strong></div>';
              exit;
        }
    }

    /**
     * Executes 'Checkuser' action
     *
     * Ajax used to check existence of username
     *
     * @param sfRequest $request A request object
     */
    public function executeCheckusermin(sfWebRequest $request)
    {
        // add new user
        $q = Doctrine_Query::create()
           ->from("CfUser a")
           ->where('a.Struserid = ?', $request->getPostParameter('username'));
        $existinguser = $q->execute();
        if(sizeof($existinguser) > 0)
        {
              echo 'fail';
              exit;
        }
        else
        {
              echo 'pass';
              exit;
        }
    }
    /**
     * Executes 'Checkemail' action
     *
     * Ajax used to check existence of email
     *
     * @param sfRequest $request A request object
     */
    public function executeCheckemailmin(sfWebRequest $request)
    {
        // add new user
        $q = Doctrine_Query::create()
           ->from("CfUser a")
           ->where('a.Stremail = ?', $request->getPostParameter('email'));
        $existinguser = $q->execute();
        if(sizeof($existinguser) > 0)
        {
              echo 'fail';
              exit;
        }
        else
        {
              echo 'pass';
              exit;
        }
    }
    /**
     * Executes 'Showuser' action
     *
     * Displays list of reviewers within the selected department
     *
     * @param sfRequest $request A request object
     */
    public function executeShowuser(sfWebRequest $request)
    {
        $this->department  = $request->getParameter("dept");

            if($request->getParameter("atoggle") != "")
            {
                  $user = Doctrine_Core::getTable("CfUser")->find($request->getParameter("atoggle"));
                  if($user)
                  {

                          if($user->getNaccesslevel() == "0")
                          {
                                  $user->setNaccesslevel("1");
                          }
                          else
                          {
                                  $user->setNaccesslevel("0");
                          }
                          $user->save();
                  }
            }

              $userids = $request->getPostParameter('user', array());
                  $departments = $request->getPostParameter('department', array());
                  try
                  {
                    $x = 0;
                    foreach($userids as $userid)
                    {
                                  $user = Doctrine_Core::getTable("cfUser")->find($userid);
                                  if($user)
                                  {
                                          $user->setStrdepartment($departments[$x]);
                                          $user->save();
                                  }
                                  $x++;
                    }
                  }catch(exception $ex)
                  {

                  }
    }

    /**
     * Executes 'Edituser' action
     *
     * Edit reviewer details
     *
     * @param sfRequest $request A request object
     */
    public function executeEdituser(sfWebRequest $request)
    {
        $this->setLayout('layout-metronic');
        if($_SESSION['SESSION_CUTEFLOW_USERID'] == $request->getParameter('userid'))
        {
                //$this->forward('users','myaccount');
        }

        if($request->getParameter('userid'))
        {
         $this->userid = $request->getParameter('userid');
        }
        else
        {
         $this->userid = "-1";
        }
    }

	/**
     * Executes 'Imitateuser' action
     *
     * Change current login session to selected user
     *
     * @param sfRequest $request A request object
     */
    public function executeImitateuser(sfWebRequest $request)
    {
		  $q = Doctrine_Query::create()
               ->from('CfUser a')
               ->where('a.nid = ?', $request->getParameter('userid'));
          $user = $q->fetchOne();
          if($user)
    		  {
              $this->getUser()->setAttribute('userid',$user->getNid());
              $this->getUser()->setAttribute('username',$user->getStruserid());

              //Backwards compatibility. As long old cuteflow modules still exist.
    				  $_SESSION['SESSION_CUTEFLOW_USERID'] = $user->getNid();
    				  $_SESSION['SESSION_CUTEFLOW_USERNAME'] = $user->getStruserid();
    		  }
		  $this->redirect("/backend.php/dashboard");
    }

    /**
     * Executes 'Myaccount' action
     *
     * Allows currently logged in reviewer to manage their account details
     *
     * @param sfRequest $request A request object
     */
    public function executeMyaccount(sfWebRequest $request)
    {
      if($request->getParameter('userid'))
          {
           $this->userid = $request->getParameter('userid');
          }
          else
          {
           $this->userid = "-1";
          }
    }

    /**
     * Executes 'Viewuser' action
     *
     * Displays full reviewer details
     *
     * @param sfRequest $request A request object
     */
    public function executeViewuser(sfWebRequest $request)
    {
        $this->setLayout('layout-metronic');
       $this->page = $request->getParameter('page', 0);
       $this->spage = $request->getParameter('spage', 0);
       $this->aspage = $request->getParameter('aspage', 0);
       $this->apppage = $request->getParameter('apppage', 0);
       $this->taskpage = $request->getParameter('taskpage', 0);
       $this->page_applications = $request->getParameter('pageapp', 1);
       $this->page_tasks = $request->getParameter('pagetask', 1);
       $this->userid = $request->getParameter('userid');

       if($request->getParameter('tab'))
       {
         $_GET['tab'] = $request->getParameter('tab');
       }

       if($request->getPostParameter("fromdate"))
       {
          $this->fromdate = $request->getPostParameter("fromdate");
          $this->fromtime = $request->getPostParameter("fromtime");
          $this->todate = $request->getPostParameter("todate");
          $this->totime = $request->getPostParameter("totime");
       }

		   $q = Doctrine_Query::create()
		      ->from("CfUser a")
			  ->where("a.nid = ?", $this->userid);
		   $reviewer = $q->fetchOne();

		   $this->forward404Unless($reviewer = Doctrine_Core::getTable('CfUser')->find(array($request->getParameter('userid'))), sprintf('The selected reviewer of id (%s) does not exist.', $request->getParameter('userid')));

		   $this->reviewer = $reviewer;
    }

    public function executeUpdateuser(sfWebRequest $request)
    {
       $userid = $request->getPostParameter('userid');
       $q = Doctrine_Query::create()
          ->from("CfUser a")
        ->where("a.nid = ?", $userid);
       $reviewer = $q->fetchOne();

       if($reviewer)
       {
          $reviewer->setStrfirstname($request->getPostParameter('first_name'));
          $reviewer->setStrlastname($request->getPostParameter('last_name'));
          $reviewer->setStrdepartment($request->getPostParameter('department'));
          $reviewer->setStrcity($request->getPostParameter('city'));
          $reviewer->setStrcountry($request->getPostParameter('country'));
          $reviewer->setUserdefined1Value($request->getPostParameter('userdefined1_value'));
          $reviewer->setUserdefined2Value($request->getPostParameter('userdefined2_value'));
          $reviewer->save();
          echo "STATUS: SUCCESS";
       }
       else
       {
          echo "STATUS: FAILED";
       }
       exit;
    }


    public function executeUpdateemail(sfWebRequest $request)
    {
       $userid = $request->getPostParameter('userid');
       $q = Doctrine_Query::create()
          ->from("CfUser a")
        ->where("a.nid = ?", $userid);
       $reviewer = $q->fetchOne();

       if($reviewer)
       {
          $reviewer->setStremail($request->getPostParameter('email_address'));
          $reviewer->save();
          echo "STATUS: SUCCESS";
       }
       else
       {
          echo "STATUS: FAILED";
       }
       exit;
    }


    public function executeUpdatephone(sfWebRequest $request)
    {
       $userid = $request->getPostParameter('userid');
       $q = Doctrine_Query::create()
          ->from("CfUser a")
        ->where("a.nid = ?", $userid);
       $reviewer = $q->fetchOne();

       if($reviewer)
       {
          $reviewer->setStrphoneMain1($request->getPostParameter('phone_number'));
          $reviewer->save();
          echo "STATUS: SUCCESS";
       }
       else
       {
          echo "STATUS: FAILED";
       }
       exit;
    }

    public function executeUpdategroup(sfWebRequest $request)
    {
       $userid = $request->getPostParameter('userid');
       $q = Doctrine_Query::create()
          ->from("CfUser a")
        ->where("a.nid = ?", $userid);
       $reviewer = $q->fetchOne();

       if($reviewer)
       {
           if($_POST['groups'])
           {
                $q = Doctrine_Query::Create()
                   ->from('mfGuardUserGroup a')
                   ->where('a.user_id = ?', $reviewer->getNid());
                $usergroups = $q->execute();
                if($usergroups)
                {
                   foreach($usergroups as $usergroup)
                   {
                      $usergroup->delete();
                   }
                }

               $groups = $_POST['groups'];
               foreach($groups as $group)
               {
                   $usergroup = new MfGuardUserGroup();
                   $usergroup->setUserId($reviewer->getNid());
                   $usergroup->setGroupId($group);
                   $usergroup->save();
               }
           }
          echo "STATUS: SUCCESS";
       }
       else
       {
          echo "STATUS: FAILED";
       }
       exit;
    }

    public function executeUpdatepicture(sfWebRequest $request)
    {
       $userid = $request->getPostParameter('userid');
       $q = Doctrine_Query::create()
          ->from("CfUser a")
        ->where("a.nid = ?", $userid);
       $reviewer = $q->fetchOne();

       $q = Doctrine_Query::create()
          ->from("ApSettings a")
          ->where("a.id = 1")
          ->orderBy("a.id DESC");
       $aplogo = $q->fetchOne();

       if($reviewer)
       {
          $prefix_folder = $aplogo->getUploadDir()."/";
          $allowedExts = array("gif", "jpeg", "jpg", "png");
          $temp = explode(".", $_FILES["file"]["name"]);
          $extension = end($temp);

          if ((($_FILES["file"]["type"] == "image/gif")
          || ($_FILES["file"]["type"] == "image/jpeg")
          || ($_FILES["file"]["type"] == "image/jpg")
          || ($_FILES["file"]["type"] == "image/pjpeg")
          || ($_FILES["file"]["type"] == "image/x-png")
          || ($_FILES["file"]["type"] == "image/png"))
          && ($_FILES["file"]["size"] < 200000)
          && in_array($extension, $allowedExts)) {
            if ($_FILES["file"]["error"] > 0) {
              echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
            } else {
              $new_filename = md5(date("Y-m-d g:I:s")).$_FILES["file"]["name"];
              echo "Upload: " . $_FILES["file"]["name"] . "<br>";
              echo "Type: " . $_FILES["file"]["type"] . "<br>";
              echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
              echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>";
              if (file_exists($prefix_folder. $new_filename)) {
                echo $_FILES["file"]["name"] . " already exists. ";
              } else {
                move_uploaded_file($_FILES["file"]["tmp_name"],
                $prefix_folder.$new_filename);
                echo "Stored in: " .$prefix_folder . $new_filename;
              }
            }
            $reviewer->setProfilePic($new_filename);
          } else {
            echo "Invalid file";
          }
          $reviewer->save();
          echo "STATUS: SUCCESS";
       }
       else
       {
          echo "STATUS: FAILED";
       }
       $this->redirect("/backend.php/users/viewuser/userid/".$reviewer->getNid());
    }


    /**
     * Executes 'Writeuser' action
     *
     * Saves reviewer details to the database
     *
     * @param sfRequest $request A request object
     */
    public function executeWriteuser(sfWebRequest $request)
    {
         $prefix_folder = dirname(__FILE__)."/../../../../..";

         require_once $prefix_folder.'/lib/vendor/cp_workflow/config/config.inc.php';
         $prefix_folder = dirname(__FILE__)."/../../../../..";
         require_once $prefix_folder.'/lib/vendor/cp_workflow/language_files/language.inc.php';

         $nAccessLevel = 1;

         if (isset($_REQUEST["UserAccessLevel"]))
         {
                 $nAccessLevel = $_REQUEST['UserAccessLevel'];
         }

         $arrSubstitutes;

         foreach ($_REQUEST as $key => $value)
         {
                 $arrSplit = split('_', $key);
                 if ($arrSplit[0] == 'SubstituteName')
                 {
                         if ($arrSplit[1] > 0 || $arrSplit[1] == -3)
                         {
                                 $arrSubstitutes[] = $arrSplit[1];
                         }
                 }
         }

         $_REQUEST['SubstitudeId'] = 0;
         if (sizeof($arrSubstitutes) > 0)
         {
                 $_REQUEST['SubstitudeId'] = -2;
         }

         switch ($_REQUEST["strIN_Email_Format"])
         {
                 case 'TEXT':
                         $mailFormat = "PLAIN";
                         break;
                 case 'HTML':
                         $mailFormat = "HTML";
                         break;
         }


         switch ($_REQUEST["strIN_Email_Value"])
         {
                 case 'NONE':
                         $mailValues = "NONE";
                         break;
                 case 'VALUES':
                         $mailValues = "VALUES";
                         break;
                 case 'IFRAME':
                         $mailValues = "IFRAME";
                         break;
         }

         if ($_REQUEST['IN_bIndividualSubsTime'])
         {
                 $_REQUEST['IN_bIndividualSubsTime'] = 0;
         }
         else
         {
                 $_REQUEST['IN_bIndividualSubsTime'] = 1;
         }

         if ($_REQUEST['IN_bIndividualEmail'])
         {
                 $_REQUEST['IN_bIndividualEmail'] = 0;
         }
         else
         {
                 $_REQUEST['IN_bIndividualEmail'] = 1;
         }

         //--- open database
         $nConnection = mysql_connect($DATABASE_HOST, $DATABASE_UID, $DATABASE_PWD);

         if ($nConnection)
         {
             error_log("Connection:::::::: user id ".$_REQUEST['strLastName']);
             error_log("Connection:::::::: user id ".$_REQUEST['strFirstName']);
             error_log("Connection:::::::: user id ".$_REQUEST['strEMail']);
             error_log("Connection:::::::: user id ".$_REQUEST['UserName']);
             error_log("Connection:::::::: user id ".$_REQUEST['IN_department']);
             error_log("Connection:::::::: user id ".$_REQUEST['strFirstName']);
                 //--- get maximum count of users
                 if (mysql_select_db($DATABASE_DB, $nConnection))
                 {
                         if ($_REQUEST['userid'] != -1)
                         {
                                 $strQuery 	= "DELETE FROM cf_substitute WHERE user_id = '".$_REQUEST['userid']."'";
                                 $nResult 	= mysql_query($strQuery, $nConnection) or die ($strQuery.mysql_error());
                         }

                         // build the index string
                         $strIndex = $_REQUEST['strLastName'].' '.
                                             $_REQUEST['strFirstName'].' '.
                                             $_REQUEST['strEMail'].' '.
                                             $_REQUEST['UserName'].' '.
                                             $_REQUEST['IN_street'].' '.
                                             $_REQUEST['IN_country'].' '.
                                             $_REQUEST['IN_zipcode'].' '.
                                             $_REQUEST['IN_city'].' '.
                                             $_REQUEST['IN_phone_main1'].' '.
                                             $_REQUEST['IN_phone_main2'].' '.
                                             $_REQUEST['IN_phone_mobile'].' '.
                                             $_REQUEST['IN_fax'].' '.
                                             $_REQUEST['IN_organisation'].' '.
                                             $_REQUEST['IN_department'].' '.
                                             $_REQUEST['IN_cost_center'].' '.
                                             $_REQUEST['IN_userdefined1_value'].' '.
                                             $_REQUEST['IN_userdefined2_value'];

                         if ($_REQUEST["userid"] == -1)
                         {
                            // add new user
                           $q = Doctrine_Query::create()
                              ->from("CfUser a")
                              ->where('a.Struserid = ?', $_REQUEST['UserName']);
                           $existinguser = $q->execute();
                           if(sizeof($existinguser) > 0)
                           {
                                 //ignore to avoid double entries
                           }
                           else
                           {
                                 $query = "INSERT INTO cf_user values (	null,
                                             '".$_REQUEST['strLastName']."',
                                             '".$_REQUEST['strFirstName']."',
                                             '".$_REQUEST['strEMail']."',
                                             '$nAccessLevel',
                                             '".$_REQUEST['UserName']."',
                                             '".password_hash($_REQUEST['Password1'], PASSWORD_BCRYPT)."',
                                             0,
                                             '$mailFormat',
                                             '$mailValues',
                                             0,
                                             0,
                                             0,
                                             '".$_REQUEST['IN_street']."',
                                             '".$_REQUEST['IN_country']."',
                                             '".$_REQUEST['IN_zipcode']."',
                                             '".$_REQUEST['IN_city']."',
                                             '".$_REQUEST['IN_phone_main1']."',
                                             '".$_REQUEST['IN_phone_main2']."',
                                             '".$_REQUEST['IN_phone_mobile']."',
                                             '".$_REQUEST['IN_fax']."',
                                             '".$_REQUEST['IN_organisation']."',
                                             '".$_REQUEST['IN_department']."',
                                             '".$_REQUEST['IN_cost_center']."',
                                             '".$_REQUEST['IN_userdefined1_value']."',
                                             '".$_REQUEST['IN_userdefined2_value']."',
                                             0,
                                             '".$_REQUEST['strIN_Subtitute_Person_Unit']."',
                                             0,
                                             0,
                                             0,
                                             0,
                                             '',
                                             '',
                                             '',
                                             '',
                                             '',
                                             '',
                                             '',
                                             '',
                                             '',
                                             '',
                                             ''
                                             )";


                                 $nResult = @mysql_query($query, $nConnection) or die(mysql_error());

                                 //Send account recovery email
                                 $message = $this->getMailer()->compose(
                                     array(sfConfig::get('app_organisation_email') => sfConfig::get('app_organisation_name')),
                                     $_REQUEST['strEMail'],
                                     'Recover your account', "
                         Hi {$_REQUEST['strFirstName']} {$_REQUEST['strLastName']},

                         Your account has been created.

                         Username: ".$_REQUEST['UserName']."
                         Password: ".$_REQUEST['Password1']."

                         Thanks,
                         ".sfConfig::get('app_organisation_name').".

                         ----
                         If you did not authorize this, please contact us and let us know.
                     ");

                                   $this->getMailer()->send($message);

                                 // get the User Id
                                 $strQuery = "SELECT MAX(nid) as max_id FROM cf_user";
                                 $nResult = mysql_query($strQuery, $nConnection);
                                 $arrResult = mysql_fetch_array($nResult, MYSQL_ASSOC);

                                 $nUserId = $arrResult['max_id'];

                                 $audit = new Audit();
                                 $audit->saveAudit("", "<a href=\"/backend.php/users/edituser?userid=".$nUserId."&language=en\">added a new user</a>");

                                 //update user groups
                                 if($_POST['groups'])
                                 {
                                         $groups = $_POST['groups'];
                                         foreach($groups as $group)
                                         {
                                                 $usergroup = new MfGuardUserGroup();
                                                 $usergroup->setUserId($nUserId);
                                                 $usergroup->setGroupId($group);
                                                 $usergroup->save();
                                         }
                                 }


                                 // write the index String
                                 $strQuery = "INSERT INTO cf_user_index values (	'$nUserId',
                                                                                                                                 '$strIndex')";
                                 $nResult = mysql_query($strQuery, $nConnection);
                             }
                         }
                         else
                         {	// update existing user
                                 $query = "UPDATE cf_user SET 	strlastname		= '".$_REQUEST['strLastName']."',
                                                                                                 strfirstname	= '".$_REQUEST['strFirstName']."',
                                                                                                 naccesslevel	= '".$nAccessLevel."',
                                                                                                 nsubstitudeid	= '".$_REQUEST['SubstitudeId']."',
                                                                                                 stremail_format	= '".$mailFormat."',
                                                                                                 stremail_values	= '".$mailValues."',
                                                                                                 strstreet		= '".$_REQUEST['IN_street']."',
                                                                                                 strcountry		= '".$_REQUEST['IN_country']."',
                                                                                                 strzipcode		= '".$_REQUEST['IN_zipcode']."',
                                                                                                 strcity			= '".$_REQUEST['IN_city']."',
                                                                                                 strphone_main1	= '".$_REQUEST['IN_phone_main1']."',
                                                                                                 strphone_main2	= '".$_REQUEST['IN_phone_main2']."',
                                                                                                 strphone_mobile	= '".$_REQUEST['IN_phone_mobile']."',
                                                                                                 strfax			= '".$_REQUEST['IN_fax']."',
                                                                                                 strorganisation	= '".$_REQUEST['IN_organisation']."',
                                                                                                 strdepartment	= '".$_REQUEST['IN_department']."',
                                                                                                 strcostcenter	= '".$_REQUEST['IN_cost_center']."',
                                                                                                 userdefined1_value	= '".$_REQUEST['IN_userdefined1_value']."',
                                                                                                 userdefined2_value	= '".$_REQUEST['IN_userdefined2_value']."',
                                                                                                 nsubstitutetimevalue	= '".$_REQUEST['strIN_Subtitute_Person_Value']."',
                                                                                                 strsubstitutetimeunit		= '".$_REQUEST['strIN_Subtitute_Person_Unit']."',
                                                                                                 busegeneralsubstituteconfig = '".$_REQUEST['IN_bIndividualSubsTime']."',
                                                                                                 busegeneralemailconfig 		= '".$_REQUEST['IN_bIndividualEmail']."'
                                                                                                 ";

                                 // add new user
                                 $q = Doctrine_Query::create()
                                     ->from("CfUser a")
                                     ->where('a.Struserid = ?', $_REQUEST['UserName']);
                                 $existinguser = $q->execute();
                                 if(sizeof($existinguser) > 0)
                                 {
                                     //ignore to avoid double entries
                                 }
                                 else
                                 {
                                     //$query .= ", struserid    = '".$_REQUEST['UserName']."'";
                                 }

                                 if ($_REQUEST["Password1"] != "unchanged" && $_REQUEST["Password1"] != "")
                                 {
                                     //$query .= ", strpassword	= '".password_hash($_REQUEST["Password1"], PASSWORD_BCRYPT)."'";
                                 }

                                 $query .= " WHERE nid = '".$_REQUEST["userid"]."' LIMIT 1;";
                                 $nResult = mysql_query($query, $nConnection);

                                 $audit = new Audit();
                                 $audit->saveAudit("", "<a href=\"/backend.php/users/edituser?userid=".$_REQUEST['userid']."&language=en\">updated a user</a>");

                                 // write the index String
                                 $strQuery = "UPDATE cf_user_index SET `indexe` = '$strIndex' WHERE user_id = '".$_REQUEST['userid']."' LIMIT 1";
                                 $nResult = mysql_query($strQuery, $nConnection) or die(mysql_error());
                         }

                         if ($_REQUEST['userid'] == -1)
                         {
                                 $strQuery 	= "SELECT MAX(nid) FROM cf_user LIMIT 1;";
                                 $nResult 	= mysql_query($strQuery);

                                 $arrRow = mysql_fetch_array($nResult);
                                 $_REQUEST['userid'] = $arrRow[0];
                         }

                         //update user groups
                         if($_POST['groups'])
                         {
                          $q = Doctrine_Query::Create()
                               ->from('mfGuardUserGroup a')
                                   ->where('a.user_id = ?', $_REQUEST['userid']);
                          $usergroups = $q->execute();
                          if($usergroups)
                          {
                                 foreach($usergroups as $usergroup)
                                 {
                                         $usergroup->delete();
                                 }

                          }

                                 $groups = $_POST['groups'];
                                 foreach($groups as $group)
                                 {
                                         $usergroup = new MfGuardUserGroup();
                                         $usergroup->setUserId($_REQUEST['userid']);
                                         $usergroup->setGroupId($group);
                                         $usergroup->save();
                                 }
                         }

                         $nMax = sizeof($arrSubstitutes);
                         for ($nIndex = 0; $nIndex < $nMax; $nIndex++)
                         {
                                 $strQuery 	= "INSERT INTO cf_substitute VALUES (NULL, '".$_REQUEST['userid']."', '".$arrSubstitutes[$nIndex]."', '$nIndex')";
                                 $nResult 	= mysql_query($strQuery, $nConnection) or die ($strQuery.mysql_error());
                         }
                 }
         }

          $this->setLayout('layout');
    }

    /**
     * Executes 'Changepassword' action
     *
     * Update the user password
     *
     * @param sfRequest $request A request object
     */
    public function executeChangepassword(sfWebRequest $request)
    {
        $user_id = $request->getPostParameter("user_id");
        $password = $request->getPostParameter("new_password");
        $confirm_password = $request->getPostParameter("confirm_password");

        $q = Doctrine_Query::create()
            ->from("CfUser a")
            ->where("a.nid = ?", $user_id);
        $reviewer = $q->fetchOne();

        if($reviewer && ($password == $confirm_password))
        {
            $reviewer->setStrpassword(password_hash($password, PASSWORD_BCRYPT));
            $reviewer->save();

            $this->getUser()->setFlash('notice', "Successfully changed this reviewer's the password");
        }
        else
        {
            $this->getUser()->setFlash('error', "Could not change your password");
        }
        $this->redirect("/backend.php/users/viewuser/userid/".$reviewer->getNid());
    }



    /**
     * Executes 'Index' action
     *
     * Displays list of reviewer departments
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {
         if($request->getParameter('filter'))
         {
             $this->filter = $request->getParameter('filter');
         }

         if($request->getPostParameter('search'))
         {
            $this->filter = $request->getPostParameter('search');
         }

         if($this->filter)
         {
             if($request->getParameter('filterstatus') != "")
             {
                 $this->filterstatus = $request->getParameter('filterstatus');

                 $q = Doctrine_Query::create()
                     ->from('CfUser a')
                     ->where('a.bdeleted = ?', $this->filterstatus)
                     ->andWhere('a.strfirstname LIKE ? OR a.stremail  = ? OR a.struserid = ?', array($this->filter . "%", $this->filter, $this->filter))
                     ->orderBy('a.strfirstname ASC');
             }
             else {
                 $q = Doctrine_Query::create()
                     ->from('CfUser a')
                     ->where('a.bdeleted = 0')
                     ->andWhere('a.strfirstname LIKE ? OR a.stremail  = ? OR a.struserid = ?', array($this->filter . "%", $this->filter, $this->filter))
                     ->orderBy('a.strfirstname ASC');
             }
         }
         else
         {
             if($request->getParameter('filterstatus') != "")
             {
                 $this->filterstatus = $request->getParameter('filterstatus');
                 $q = Doctrine_Query::create()
                     ->from('CfUser a')
                     ->where('a.bdeleted = ?', $this->filterstatus)
                     ->orderBy('a.strfirstname ASC');
             }
             else {
                 $q = Doctrine_Query::create()
                     ->from('CfUser a')
                     //->where('a.bdeleted = 0')
                     ->orderBy('a.strfirstname ASC');
             }
         }
         $this->pager = new sfDoctrinePager('CfUser', 5000);
         $this->pager->setQuery($q);
         $this->pager->setPage($request->getParameter('page', 1));
         $this->pager->init();
         $this->setLayout('layout-metronic');
    }




    /**
     * Executes 'Index' action
     *
     * Displays list of reviewer departments
     *
     * @param sfRequest $request A request object
     */
    public function executeSettingsindex(sfWebRequest $request)
    {
         $q = Doctrine_Query::create()
		   ->from('CfUser a')
		   ->where('a.bdeleted = 0')
		   ->orderBy('a.nid DESC');
		 $this->reviewers = $q->execute();
    }

    /**
     * Executes 'Restore' action
     *
     * Restores an existing reviewer
     *
     * @param sfRequest $request A request object
     */
    public function executeRestore(sfWebRequest $request)
    {
        if($this->getUser()->mfHasCredential("managereviewers"))
        {
            $q = Doctrine_Query::create()
                ->from('CfUser a')
                ->where('a.nid = ?', $request->getParameter('id'));
            $user = $q->fetchOne();
            if ($user) {
                $user->setBdeleted("0");
                $user->save();
            }

            $this->getUser()->setFlash('notice', 'Successfully restored the reviewer.');
            $audit = new Audit();
            $audit->saveAudit("","Restored a reviewer: ".$user->getStrfirstname()." ".$user->getStrlastname()." (".$user->getStremail().")");

            $this->redirect("/backend.php/users/index");
        }
        else
        {
            $this->getUser()->setFlash('notice', 'Could not restore the reviewer');
            $this->redirect("/backend.php/users/viewuser/userid/".$request->getParameter('id'));
        }
    }

    /**
     * Executes 'Delete' action
     *
     * Deletes an existing reviewer
     *
     * @param sfRequest $request A request object
     */
    public function executeDelete(sfWebRequest $request)
    {
        if($this->getUser()->mfHasCredential("managereviewers"))
        {
            $q = Doctrine_Query::create()
                ->from('CfUser a')
                ->where('a.nid = ?', $request->getParameter('id'));
            $user = $q->fetchOne();
            if ($user) {
                $user->setBdeleted("1");
                $user->save();
            }

            $this->getUser()->setFlash('notice', 'Successfully deleted the reviewer.');
            $audit = new Audit();
            $audit->saveAudit("","Delete a reviewer: ".$user->getStrfirstname()." ".$user->getStrlastname()." (".$user->getStremail().")");

            $this->redirect("/backend.php/users/index");
        }
        else
        {
            $this->getUser()->setFlash('notice', 'Could not delete the reviewer');
            $this->redirect("/backend.php/users/viewuser/userid/".$request->getParameter('id'));
        }
    }
}
