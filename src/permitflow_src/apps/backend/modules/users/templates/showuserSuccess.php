<?php
/**
 * showuserSuccess.php template.
 *
 * Displays list of reviewers within the selected department
 *
 * @package    backend
 * @subpackage users
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

$prefix_folder = dirname(__FILE__)."/../../../../..";
require_once $prefix_folder.'/lib/vendor/cp_workflow/config/config.inc.php';
require_once $prefix_folder.'/lib/vendor/cp_workflow/language_files/language.inc.php';
require_once $prefix_folder.'/lib/vendor/cp_workflow/pages/CCirculation.inc.php';

$objCirculation = new CCirculation();

$nShowRows = 50;

$_REQUEST['dept'] = $department;

if ($_REQUEST['sortby'] == '')
{
        $_REQUEST['sortby'] = 'strlastname';
}
if ($_REQUEST['sortdir'] == '')
{
        $_REQUEST['sortdir'] = 'ASC';
}

function getSortDirection($strColumn)
{
    global $_REQUEST;

    if ($strColumn == $_REQUEST['sortby'])
    {
            if ($_REQUEST['sortdir'] == 'ASC')
            {
                    return 'DESC';
            }
            else 
            {
                    return 'ASC';
            }
    }
    else 
    {
            return 'ASC';
    }
}

    function getColHighlight($nRow, $strCol)
{
    global $_REQUEST;

    if ($strCol == $_REQUEST['sortby'])
    {
            if (($nRow % 2) == 0)
            {
                    return ' style="background-color: #F4E8C2;" ';	
            }
            else 
            {
                    return ' style="background-color: #FFF7DE;" ';	
            }
    }
}
?>
<?php
    $prefix_folder = dirname(__FILE__)."/../../../../..";
	include_once ($prefix_folder."/workflow/lib/datetime.inc.php");
    include_once ($prefix_folder."/workflow/lib/viewutils.inc.php");
	
	//--- open database
	$nConnection = mysql_connect($DATABASE_HOST, $DATABASE_UID, $DATABASE_PWD);
	
	if ($nConnection)
	{
		//--- get maximum count of users
		if (mysql_select_db($DATABASE_DB, $nConnection))
		{
			$query = "select COUNT(*) from cf_user WHERE bdeleted = 0";
			$nResult = mysql_query($query, $nConnection);

			if ($nResult)
			{
				if (mysql_num_rows($nResult) > 0)
				{
					while (	$arrRow = mysql_fetch_array($nResult))
					{	
						$nUserCount = $arrRow[0];
					}				
				}
			}
		}
		
		if ($nUserCount > $_REQUEST['start'] + $nShowRows)
		{
			$end = $_REQUEST['start'] + ($nShowRows - 1);
		}
		else
		{
			if (($_REQUEST['start'] + $nShowRows) > $nUserCount)
			{
				$end = $nUserCount;
			}
			else
			{
				$end = $_REQUEST['start'] + $nShowRows;
			}
		}
		
		//--- get all users
		$arrUsers = array();
		
		$start = $_REQUEST['start']-1;
		
		if ($_REQUEST['sortby'] == 'tslastaction')
		{
			if ($sortdir == 'DESC') 
			{
				$sortdir = 'ASC';
			}
			else
			{
				$sortdir = 'DESC';
			}
		}
		else
		{
			$sortdir = $_REQUEST['sortdir'];
		}
		
		$strQuery = "SELECT * FROM cf_user WHERE bdeleted = '0' ORDER BY ".$_REQUEST['sortby']." $sortdir";
		$nResult = mysql_query($strQuery, $nConnection);
		
		if ($nResult)
		{
			if (mysql_num_rows($nResult) > 0)
			{
				$nRunningNumber = $_REQUEST['start'];
				while (	$arrRow = mysql_fetch_array($nResult))
				{
					$arrUsers[$arrRow["nid"]] = $arrRow;
				}
			}
		}	
	}
	
	$q = Doctrine_Query::create()
	   ->from('department a')
	   ->where('a.id = ?', $department);
	$your_department = $q->fetchOne();

?>


<div class="g12" style="padding-left: 3px;">
			<form style="margin-bottom: 5px;">
			<label style='height: 30px; margin-top: 0px;'>
			<div style='float: left; font-size: 20px; font-weight: 700;'><?php echo ($your_department?$your_department->getDepartmentName():'Unassigned'); ?> Reviewers</div></label>
			<fieldset style="margin: 0px; padding: 0px;">
			<section>
			<div style='width: 100%;'>
							
				<div style="float: right;">
				<button class="btnalt" onclick="window.location='<?php echo public_path(); ?>backend.php/users/edituser';">Add New Reviewer</button>
				</div>

			</div>
			</section>
			</fieldset>
			</form>


<table class="datatable">
  <thead>
    <tr>
      <th style="width: 10px;"><input type='checkbox' name='batchall' onclick="boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.name == 'batch') { if(this.checked == true){ box.checked = false; }else{ box.checked = true; } } } "></th>
      		<th width="60">ID</th>
			<th>Group</th>
			<th>Full Name</th>
			<th>Username</th>
			<th>Email</th>
			<th style="background: none;">Active?</th>
			<th style="background: none;">Online?</th>
			<th style="background: none; width: 80px;">Actions</th>
		</tr>
		</thead>
		 <tbody>
		<?php
			//--- output the user inbetween the range (start to end)
			$count = 1;
			foreach ($arrUsers as $arrRow)
			{	
			    //Fetch Slot name from $_REQUEST['department']
				if($_REQUEST['dept'])
				{
					if($_REQUEST['dept'] != 'none')
					{
						$dept_id = $_REQUEST['dept'];
						
						$q = Doctrine_Query::create()
						   ->from('department a')
						   ->where('a.id = ?', $dept_id);
						$department = $q->fetchOne();
						
						if($department)
						{
							//Fetch department name from slot name
							$department_name = $department->getDepartmentName();
							
							$pos = strpos($arrRow['strdepartment'], $department_name);
							
							if($pos === FALSE)
							{
								continue;
							}
						}
						else
						{
							continue;
						}
					}
					else
					{
							if($arrRow['strdepartment'] != "")
							{
								continue;
							}
					}
				
				}
					
                 			
				$style = "background-color: #efefef;";
				if ($nRunningNumber%2 == 1)
				{
					$style = "background-color: #fff;";
				}
				
				$strImage = '/asset_misc/images/inactive.gif';
				if (($arrRow["naccesslevel"] & 2) == 2)
				{
					$strImage = '/asset_misc/images/active.gif';
				}
				
				if ($arrRow["nsubstitudeid"] == -2)
				{
					$arrSubstitutes = $objCirculation->getSubstitutes($arrRow['nid']);
					if ($arrSubstitutes[0]['substitute_id'] == -3)
					{
						$strSubstitute = $SELF_DELEGATE_USER;
					}
					else
					{
						$strSubstitute 	= $objCirculation->getUsername($arrSubstitutes[0]['substitute_id']);
					}
				}
				elseif ($arrRow["nsubstitudeid"] != 0)
				{
					$strSubstitute 	= $objCirculation->getUsername($arrRow['nsubstitudeid']);
				}
				else
				{
					$strSubstitute = '-';
				}
				
				$strOnlineState = '';
				if (($arrRow['tslastaction'] + $USER_TIMEOUT) > time())
				{
					$strOnlineState = '<img src="/assets_backend/images/icons/dark/flag.png">';
				}
				else
				{
					//$strOnlineState = '<img src="/assets_backend/images/icons/dark/cross.png">';
				}
				
				//What group does this user belong to
				$q = Doctrine_Query::create()
				   ->from('MfGuardUserGroup a')
				   ->where('a.user_id = ?', $arrRow['nid']);
				$group_relations = $q->execute();
				
				$color = "";
				
				$groupname = "";
				
				foreach($group_relations as $group_relation)
				{
				    $q = Doctrine_Query::create()
				       ->from('MfGuardGroup a')
				       ->where('a.id = ?', $group_relation->getGroupId());
				    $group = $q->fetchOne();
				    
				    if($group_relation->getGroupId() == "13")
				    {
				        $color = "style='background-color: #FFCC66;'";
				        $groupname = $group->getName();
				    }
				    if($group_relation->getGroupId() == "6")
				    {
				        $color = "style='background-color: #66FF66;'";
				        $groupname = $group->getName();
				    }
				    if($group_relation->getGroupId() == "4")
				    {
				        $color = "style='background-color: #66FFFF;'";
				        $groupname = $group->getName();
				    }
				    if($group_relation->getGroupId() == "7")
				    {
				        $color = "style='background-color: #33FFFF;'";
				        $groupname = $group->getName();
				    }
				}
				?>
				<tr id="row_<?php echo $arrRow['nid'] ?>" <?php echo $color; ?>>
				  <td><input type='checkbox' name='batch' id='batch_<?php echo $arrRow['nid'] ?>' value='<?php echo $arrRow['nid'] ?>'></td>
					<td><?php echo $count++; ?><input type="hidden" name="user[]" id="user[]" value="<?php echo $arrRow['nid']; ?>">
                    </td>
					<td><?php echo $groupname; ?></td>
					<td><?php echo $arrRow['strfirstname'] ?> <?php echo $arrRow['strlastname'] ?></td>
					<td><?php echo $arrRow['struserid'] ?></td>
					<td><?php echo $arrRow['stremail'] ?></td>
					<td>
					<a href="<?php echo '/backend.php/users/showuser/dept/'.$department.'/atoggle/'.$arrRow['nid'] ?>">
					<?php 
					if($arrRow['naccesslevel'] == "1")
					{
					  echo "<img src='/assets_backend/images/icons/dark/tick.png'>";
					}
					else
					{
					  echo "<img src='/assets_backend/images/icons/dark/access_denied.png'>";
					}
					?></a>
					</td>
					<td><?php echo $strOnlineState ?></td>
					<td>	
					  <a href="/backend.php/users/viewuser/userid/<?php echo $arrRow[0] ?>" alt="View"><img src="/assets_backend/images/icons/dark/magnifying_glass.png" border="0" title="View" alt="View"></a>
						
					  <a href="/backend.php/users/edituser/userid/<?php echo $arrRow[0] ?>" alt="Edit"><img src="/assets_backend/images/icons/dark/pencil.png" border="0" title="Edit" alt="Edit"></a>
					  <?php if($sf_user->mfHasCredential("access_settings")){ ?>
					  <a href="/backend.php/users/imitateuser/userid/<?php echo $arrRow[0] ?>" alt="Imitate"><img src="/assets_backend/images/icons/dark/refresh.png" border="0" title="Imitate" alt="Imitate"></a>
					  <?php } ?>
						<a href="#" onclick="if(confirm('Are you sure you want to delete this user?')){ ajaxupdate('/backend.php/users/delete/id/<?php echo $arrRow['nid']; ?>', 'id', '<?php echo $arrRow['nid']; ?>'); }else{ return false; }"><img src="/assets_backend/images/icons/dark/cross.png" border="0" alt="Delete" title="Delete"></a>
					</td>
				</tr>
				
				<?php
				$nRunningNumber++;
			}
		?>
		</tbody>
	<tfoot>
   <tr><td colspan='8' style='text-align: left;'>
   <select id='batch_action' name='batch_action' onChange="if(this.value != ''){if(confirm('Are you sure?')){ batch('users', this.options[this.selectedIndex].text, this.value); document.getElementById('default').selected='selected'; }}">
   <option id='default' value=''>Choose an action..</option>
   <option value='delete'>Set As Deleted</option>
   </select>
   </td></tr>
   </tfoot>
	</table>
</div>
<?php
	//--- close database	
	mysql_close($nConnection);
?>
