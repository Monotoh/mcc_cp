<?php
/**
 * writeuserSuccess.php template.
 *
 * Saves reviewer details to the database
 *
 * @package    backend
 * @subpackage users
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

?>

<div id='search_adv' name='search_adv' class="g12" style='padding-top: 8px;'>
<form autocomplete="off" data-ajax="false" style="margin: 0px; padding-left: 4px;">
<label>Reviewer Account Details</label>
<fieldset>
<section>
<div class="alert success" style="width: 94%;">Account Details Saved Successfully</div>
</section>
</fieldset>
</form>
</div>
