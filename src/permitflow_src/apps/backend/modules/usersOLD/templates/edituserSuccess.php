<?php
/**
 * edituserSuccess.php template.
 *
 * Edit reviewer details
 *
 * @package    backend
 * @subpackage users
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");

$prefix_folder = dirname(__FILE__)."/../../../../..";
require_once $prefix_folder.'/lib/vendor/cp_workflow/config/config.inc.php';


require_once $prefix_folder.'/lib/vendor/cp_workflow/language_files/language.inc.php';
require_once $prefix_folder.'/lib/vendor/cp_workflow/pages/CCirculation.inc.php';

$objCirculation = new CCirculation();
?>
<?php

$strFirstName = "";

$strLastName = "";

$strEMail = "";

$nAccessLevel = 1;

$nSubstitudeId = 0;



if ($userid == -1)

{

    $strPassword = "";

}

else

{

    $strPassword = "unchanged";

}



$arrSubstitutes[0]['name'] = '-';

$arrSubstitutes[0]['id'] = 0;



$CUR_SUBTITUTE_PERSON_UNIT	= $SUBTITUTE_PERSON_UNIT;

$CUR_SUBTITUTE_PERSON_VALUE	= $SUBTITUTE_PERSON_VALUE;



if (-1 != $userid)

{

    //--- open database

    $nConnection = mysql_connect($DATABASE_HOST, $DATABASE_UID, $DATABASE_PWD);



    if ($nConnection)

    {

        //--- get maximum count of users

        if (mysql_select_db($DATABASE_DB, $nConnection))

        {

            //--- read the values of the user

            $strQuery = "SELECT * FROM cf_user WHERE nid = ".$userid;

            $nResult = mysql_query($strQuery, $nConnection);



            if ($nResult)

            {

                if (mysql_num_rows($nResult) > 0)

                {

                    while (	$arrRow = mysql_fetch_array($nResult))

                    {

                        $strFirstName 	= $arrRow["strfirstname"];

                        $strLastName 	= $arrRow["strlastname"];

                        $strEMail 		= $arrRow["stremail"];

                        $nAccessLevel 	= $arrRow["naccesslevel"];

                        $nSubstituteId 	= $arrRow["nsubstitudeid"];

                        $strUserId 		= $arrRow["struserid"];

                        $EMAIL_FORMAT 	= $arrRow["stremail_format"];

                        $EMAIL_VALUES 	= $arrRow["stremail_values"];

                        $street			= $arrRow['strstreet'];

                        $country		= $arrRow['strcountry'];

                        $zipcode		= $arrRow['strzipcode'];

                        $city			= $arrRow['strcity'];

                        $phone_main1	= $arrRow['strphone_main1'];

                        $phone_main2	= $arrRow['strphone_main2'];

                        $phone_mobile	= $arrRow['strphone_mobile'];

                        $fax			= $arrRow['strfax'];

                        $organisation	= $arrRow['strorganisation'];

                        $department		= $arrRow['strdepartment'];

                        $cost_center	= $arrRow['strcostcenter'];

                        $userdefined1_value	= $arrRow['userdefined1_value'];

                        $userdefined2_value	= $arrRow['userdefined2_value'];

                        $CUR_SUBTITUTE_PERSON_UNIT	= $arrRow['strsubstitutetimeunit'];

                        $CUR_SUBTITUTE_PERSON_VALUE	= $arrRow['nsubstitutetimevalue'];

                        $bIndividualSubsTime	= $arrRow['busegeneralsubstituteconfig'];

                        $bIndividualEmail		= $arrRow['busegeneralemailconfig'];

                    }

                }

            }



            if ($nSubstituteId == -2)

            {	// user has one or more substitutes - the DB table "cf_substitute" has to be checked

                $arrResult = $objCirculation->getSubstitutes($userid);



                $nMax = sizeof($arrResult);

                for ($nIndex = 0; $nIndex < $nMax; $nIndex++)

                {

                    $nCurSubstituteId = $arrResult[$nIndex]['substitute_id'];

                    if ($nCurSubstituteId == -3)

                    {

                        $arrSubstitutes[$nIndex]['name']	= $SELF_DELEGATE_USER;

                        $arrSubstitutes[$nIndex]['id']		= -3;

                    }

                    else

                    {

                        $arrUser = $objCirculation->getUserById($nCurSubstituteId);



                        $arrSubstitutes[$nIndex]['name'] 	= $arrUser['strlastname'].', '.$arrUser['strfirstname'];

                        $arrSubstitutes[$nIndex]['id'] 		= $nCurSubstituteId;

                    }

                }

            }

        }

    }

}

else

{

    $EMAIL_FORMAT = 'HTML';

    $EMAIL_VALUES = 'IFRAME';

}

?>


<div class="pageheader">
<h2><i class="fa fa-envelope"></i><?php echo __('Reviewers'); ?></h2>
<div class="breadcrumb-wrapper">
<span class="label"><?php echo __('You are here'); ?>:</span>
<ol class="breadcrumb">
<li>
<a href=""><?php echo __('Home'); ?></a>
</li>
<li>
<a href=""><?php echo __('Reviewers'); ?></a>
</li>
<li class="active"><?php echo __('new'); ?></li>
</ol>
</div>
</div>

<div class="contentpanel">
<div class="row">



<div class="panel panel-dark">
<div class="panel-heading">
<h3 class="panel-title">
            <?php

			if(empty($strFirstName) && empty($strLastName))
			{
				echo __("New Reviewer");
			}
			else
			{
				echo $strFirstName." ".$strLastName;
			}

			?> <?php echo __('Details'); ?>
</h3>
</div>      

<script type="text/javascript" src="<?php echo public_path(); ?>assets_unified/js/jquery.bootstrap-duallistbox.js"></script>

<div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
  <strong><?php echo __('Well done'); ?></strong> <?php echo __('You successfully updated reviewer'); ?></a>.
</div>      
			
<div class="panel-body panel-body-nopadding">


<div id="basicWizard" class="basic-wizard">


                <div class="tab-content tab-content-nopadding">


<div class="tab-pane active" id="tabs-1">
<form id="reviewerform" class="form-bordered form-horizontal">
    <div class="panel mb0">
         <div class="panel-body panel-body-nopadding">

				<div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('First Name'); ?></i></label>

						<div class="col-sm-8">


					<input id="strFirstName" Name="strFirstName" type="text" class="form-control"  value="<?php echo $strFirstName;?>" required="required">
					</div>

					</div>

				<div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Last Name'); ?></i></label>

						<div class="col-sm-8">

							<input id="strLastName" Name="strLastName" type="text" class="form-control"  value="<?php echo $strLastName;?>" required="required">

						</div>	

					</div>

				<div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Email Address'); ?></i></label>

						<div class="col-sm-8">

							<input id="strEMail" Name="strEMail" type="text" class="form-control" <?php if($strEMail){ echo "disabled='disabled'"; } ?>  value="<?php echo $strEMail;?>" required="required">

				
						</div>

					</div>	

				<div id="emailresult" name="emailresult"></div>

				<script language="javascript">
					$('document').ready(function(){
						$('#strEMail').keyup(function(){
							$.ajax({
				                type: "POST",
				                url: "/backend.php/users/checkemail",
				                data: {
				                    'email' : $('input:text[name=strEMail]').val()
				                },
				                dataType: "text",
				                success: function(msg){
				                      //Receiving the result of search here
				                      $("#emailresult").html(msg);
				                }
				            });
				        });
					});
				</script>

		    	<div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Username'); ?></i></label>

						<div class="col-sm-8">

							<input type="text" Name="UserName" id="UserName" class="form-control" <?php if($strUserId){ echo "disabled='disabled'"; } ?>  value="<?php echo $strUserId;?>" required="required">

				
						</div>

					</div>

				<div id="usernameresult" name="usernameresult"></div>

				<script language="javascript">
					$('document').ready(function(){
						$('#UserName').keyup(function(){
							$.ajax({
				                type: "POST",
				                url: "/backend.php/users/checkuser",
				                data: {
				                    'username' : $('input:text[name=UserName]').val()
				                },
				                dataType: "text",
				                success: function(msg){
				                      //Receiving the result of search here
				                      $("#usernameresult").html(msg);
				                }
				            });
				        });
					});
				</script>

				<?php if($strUserId){ ?>
				<div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Password'); ?></i></label>

					<div class="col-sm-8">
							<button class="btn btn-primary mr20" type="button" onClick="window.location='/backend.php/users/reset/email/<?php echo $strEMail; ?>'">Reset Password</button>
					</div>

				</div>
				<?php 
			    }
			    else
			    {
				?>

	            <div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Password'); ?></i></label>

						<div class="col-sm-8">

							<input type="password" Name="Password1" id="Password1" class="form-control"  value="" required="required">

				
						</div>

					</div>

	            <div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Confirm Password'); ?></i></label>

						<div class="col-sm-8">

							<input type="password" Name="Password2" id="Password2" class="form-control"  value="" required="required">

				
						</div>

					</div>

					<div id="passwordresult" name="passwordresult"></div>
				<?php
				}
				?>
					<script language="javascript">
						$('document').ready(function(){
							$('#Password1').keyup(function(){
								if($('#Password1').val() == $('#Password2').val() && $('#Password1').val() != "")
								{
									$('#passwordresult').html('<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Passwords match!</strong></div>');
								}
								else
								{
									$('#passwordresult').html('<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Passwords don\'t match!</strong> Try again.</div>');
								}
							});
							$('#Password2').keyup(function(){
								if($('#Password1').val() == $('#Password2').val() && $('#Password1').val() != "")
								{
									$('#passwordresult').html('<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Passwords match!</strong></div>');
								}
								else
								{
									$('#passwordresult').html('<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Passwords don\'t match!</strong> Try again.</div>');
								}
							});
						});
					</script>
					
       	         <div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Department'); ?></i></label>

						<div class="col-sm-8">
                    <select id="IN_department" name="IN_department" required="required">
                    <option value=''><?php echo __('Select Department'); ?>...</option>
                    <?php
					$q = Doctrine_Query::create()
					   ->from('Department a');
					$departments = $q->execute();
					foreach($departments as $this_department)
					{
					    $selected = "";
						if($department == $this_department->getDepartmentName())
						{
							$selected = "selected";
						}
						echo "<option value='".$this_department->getDepartmentName()."' ".$selected.">".$this_department->getDepartmentName()."</option>";
					}
					?>
                    </select>

				
						</div>

					</div>
					
		  		 <div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Groups'); ?></i></label>

						<div class="col-sm-8">

							<select id="groups" name="groups[]" multiple class="form-control" required="required">
							<?php
							$q = Doctrine_Query::create()
								 ->from('MfGuardGroup a')
								 ->orderBy('a.name ASC');
							$groups = $q->execute();
							foreach($groups as $group)
							{	
								$selected = "";
								$q = Doctrine_Query::create()
									 ->from('MfGuardUserGroup a')
									 ->where('a.user_id = ?',  $userid)
									 ->andWhere('a.group_id = ?', $group->getId());
								$usergroup = $q->execute();
								
								if(sizeof($usergroup) > 0)
								{
									$selected = "selected";
								}
								
								?>
								<option value='<?php echo $group->getId(); ?>' <?php echo $selected; ?>><?php echo $group->getName(); ?></option>
								<?php
							}
							?>
							</select>

				
						</div>
			</div>

			<script language="javascript">
			 jQuery(document).ready(function(){

			  var demo1 = $('[id="groups"]').bootstrapDualListbox();

			});
			 </script>


	       <div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Street'); ?></i></label>

						<div class="col-sm-8">

					<input type="text" id="IN_street" name="IN_street" class="form-control"  value="<?php echo $street ?>">

				
						</div>

					</div>

	       <div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Zip Code'); ?> </i></label>

						<div class="col-sm-2">

					<input type="text" id="IN_zipcode" name="IN_zipcode" class="form-control" value="<?php echo $zipcode ?>">
                    </div>
                    
                    <div class="col-sm-6">

					<input type="text" id="IN_city" name="IN_city" class="form-control"  value="<?php echo $city ?>">

				
						</div>

					</div>

	       <div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Country'); ?></i></label>

						<div class="col-sm-8">

					<input type="text" id="IN_country" name="IN_country" class="form-control"  value="<?php echo $country ?>">

				
						</div>

					</div>

	       <div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Phone 1'); ?></i></label>

						<div class="col-sm-8">
					<input type="text" id="IN_phone_main1" name="IN_phone_main1" class="form-control"  value="<?php echo $phone_main1 ?>">

				
						</div>

					</div>

	       <div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Phone 2'); ?></i></label>

						<div class="col-sm-8">

					<input type="text" id="IN_phone_main2" name="IN_phone_main2" class="form-control"  value="<?php echo $phone_main2 ?>">

				
						</div>

					</div>

	       <div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Mobile Number'); ?></i></label>

						<div class="col-sm-8">

					<input type="text" id="IN_phone_mobile" name="IN_phone_mobile" class="form-control"  value="<?php echo $phone_mobile ?>">

				
						</div>

					</div>

	       <div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Fax Number'); ?></i></label>

						<div class="col-sm-8">

					<input type="text" id="IN_fax" name="IN_fax" class="form-control"  value="<?php echo $fax ?>">

				
						</div>

					</div>

	       <div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Organization'); ?></i></label>

						<div class="col-sm-8">

					<input type="text" id="IN_organisation" name="IN_organisation" class="form-control"  value="<?php echo $organisation ?>">

				
						</div>

					</div>

	       <div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Cost Center'); ?></i></label>

						<div class="col-sm-8">

					<input type="text" id="IN_cost_center" name="IN_cost_center" class="form-control"  value="<?php echo $cost_center ?>">

				
						</div>

					</div>

	       <div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Designation'); ?></i></label>

						<div class="col-sm-8">

					<input type="text" id="IN_userdefined1_value" name="IN_userdefined1_value" class="form-control"  value="<?php echo $userdefined1_value ?>">

				
						</div>

					</div>

	       <div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Man Number'); ?></i></label>

						<div class="col-sm-8">

					<input type="text" id="IN_userdefined2_value" name="IN_userdefined2_value" class="form-control"  value="<?php echo $userdefined2_value ?>">

				</div>

			</div>
         </div>
           <div class="panel-footer">
            <button class="btn btn-danger mr20"><?php echo __('Reset'); ?></button><button type="submit" class="btn btn-primary" name="submitbuttonname" id="submitbutton"  value="submitbuttonvalue"><?php echo __('Submit'); ?></button>
            <input type="hidden" value="<?php echo $userid;?>" id="userid" name="userid">
          </div>
          </form>
        </div>    

</div><!--tabs-2-->

</div><!--basicWizard-->

</div><!--Panel-body-->

</div><!--Panel-dark-->





</div>
</div>
<script language="javascript">
 jQuery(document).ready(function(){
	$("#submitbutton").click(function() {
     var submit_form = true;
     $('form#reviewerform').find('input').each(function(){
        if($(this).prop('required') && $(this).val() == ""){
            alert('Required field missing');
            $(this).focus();
            submit_form = false;
        }
    });
     $('form#reviewerform').find('textarea').each(function(){
        if($(this).prop('required') && $(this).val() == ""){
            alert('Required field missing');
            $(this).focus();
            submit_form = false;
        }
    });
    if(submit_form)
    {
		 $.ajax({
			url: '<?php echo public_path(); ?>backend.php/users/writeuser',
			cache: false,
			type: 'POST',
			data : $('#reviewerform').serialize(),
			success: function(json) {
				$('#alertdiv').attr("style", "display: block;");
				$("html, body").animate({ scrollTop: 0 }, "slow");
			}
		});
	}
		return false;
	 });
	});
</script>
