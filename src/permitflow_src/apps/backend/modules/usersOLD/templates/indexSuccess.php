<?php
use_helper("I18N");

if($sf_user->mfHasCredential("managereviewers"))
{
$_SESSION['current_module'] = "reviewers";
$_SESSION['current_action'] = "index";
$_SESSION['current_id'] = "";
?>
<?php
/**
 * indexSuccess.php template.
 *
 * Displays list of reviewer departments
 *
 * @package    backend
 * @subpackage users
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

?>
<div class="pageheader">
    <h2><i class="fa fa-user"></i> <?php echo __('Reviewers'); ?> <span>List of backend reviewers</span></h2>
  <div class="breadcrumb-wrapper" style="margin-top: 10px;">
    <span class="label"><?php echo __('You are here'); ?>:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>backend.php"><?php echo __('Home'); ?></a></li>
      <li class="active"><?php echo __('Reviewers'); ?></li>
    </ol>
  </div>
</div>


<div class="contentpanel">

  <ul class="letter-list">
    <li><a <?php if(empty($filter)){ ?>class="activelist"<?php } ?> href="/backend.php/users/index<?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">All</a></li>
    <li><a <?php if($filter == "a"){ ?>class="activelist"<?php } ?> href="/backend.php/users/index/filter/a<?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">a</a></li>
    <li><a <?php if($filter == "b"){ ?>class="activelist"<?php } ?> href="/backend.php/users/index/filter/b<?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">b</a></li>
    <li><a <?php if($filter == "c"){ ?>class="activelist"<?php } ?> href="/backend.php/users/index/filter/c<?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">c</a></li>
    <li><a <?php if($filter == "d"){ ?>class="activelist"<?php } ?> href="/backend.php/users/index/filter/d<?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">d</a></li>
    <li><a <?php if($filter == "e"){ ?>class="activelist"<?php } ?> href="/backend.php/users/index/filter/e<?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">e</a></li>
    <li><a <?php if($filter == "f"){ ?>class="activelist"<?php } ?> href="/backend.php/users/index/filter/f<?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">f</a></li>
    <li><a <?php if($filter == "g"){ ?>class="activelist"<?php } ?> href="/backend.php/users/index/filter/g<?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">g</a></li>
    <li><a <?php if($filter == "h"){ ?>class="activelist"<?php } ?> href="/backend.php/users/index/filter/h<?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">h</a></li>
    <li><a <?php if($filter == "i"){ ?>class="activelist"<?php } ?> href="/backend.php/users/index/filter/i<?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">i</a></li>
    <li><a <?php if($filter == "j"){ ?>class="activelist"<?php } ?> href="/backend.php/users/index/filter/j<?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">j</a></li>
    <li><a <?php if($filter == "k"){ ?>class="activelist"<?php } ?> href="/backend.php/users/index/filter/k<?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">k</a></li>
    <li><a <?php if($filter == "l"){ ?>class="activelist"<?php } ?> href="/backend.php/users/index/filter/l<?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">l</a></li>
    <li><a <?php if($filter == "m"){ ?>class="activelist"<?php } ?> href="/backend.php/users/index/filter/m<?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">m</a></li>
    <li><a <?php if($filter == "n"){ ?>class="activelist"<?php } ?> href="/backend.php/users/index/filter/n<?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">n</a></li>
    <li><a <?php if($filter == "o"){ ?>class="activelist"<?php } ?> href="/backend.php/users/index/filter/o<?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">o</a></li>
    <li><a <?php if($filter == "p"){ ?>class="activelist"<?php } ?> href="/backend.php/users/index/filter/p<?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">p</a></li>
    <li><a <?php if($filter == "q"){ ?>class="activelist"<?php } ?> href="/backend.php/users/index/filter/q<?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">q</a></li>
    <li><a <?php if($filter == "r"){ ?>class="activelist"<?php } ?> href="/backend.php/users/index/filter/r<?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">r</a></li>
    <li><a <?php if($filter == "s"){ ?>class="activelist"<?php } ?> href="/backend.php/users/index/filter/s<?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">s</a></li>
    <li><a <?php if($filter == "t"){ ?>class="activelist"<?php } ?> href="/backend.php/users/index/filter/t<?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">t</a></li>
    <li><a <?php if($filter == "u"){ ?>class="activelist"<?php } ?> href="/backend.php/users/index/filter/u<?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">u</a></li>
    <li><a <?php if($filter == "v"){ ?>class="activelist"<?php } ?> href="/backend.php/users/index/filter/v<?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">v</a></li>
    <li><a <?php if($filter == "w"){ ?>class="activelist"<?php } ?> href="/backend.php/users/index/filter/w<?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">w</a></li>
    <li><a <?php if($filter == "x"){ ?>class="activelist"<?php } ?> href="/backend.php/users/index/filter/x<?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">x</a></li>
    <li><a <?php if($filter == "y"){ ?>class="activelist"<?php } ?> href="/backend.php/users/index/filter/y<?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">y</a></li>
    <li><a <?php if($filter == "z"){ ?>class="activelist"<?php } ?> href="/backend.php/users/index/filter/z<?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">z</a></li>
  </ul>
  
  <div class="mb30"></div>

  <div class="row">
      <?php
      if($sf_user->hasFlash('notice'))
      {
          ?>
          <div class="alert alert-success">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              <?php echo $sf_user->getFlash('notice',ESC_RAW); ?>.
          </div>
      <?php
      }
      ?>

    <div class="alert alert-success" id="notifications" name="notifications" style="display: none;">
      <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
      <strong><?php echo __('Well done'); ?>!</strong> <?php echo __('You successfully updated this user'); ?>.
    </div>

  </div>
 <div class="panel panel-bordered">
  <div class="panel-body panel-body-nopadding">
      <div class="table-responsive">
          <?php if ($pager->getResults()): ?>
          <table class="table mb0 border-left-0 border-right-0 border-bottom-0 panel-table radius-tl radius-tr">
              <thead class="form-horizontal">
              <tr>
              <th style="width:10%;" class="border-bottom-1 radius-tl">
                      <a href="<?php echo public_path(); ?>backend.php/users/edituser"
           class="btn btn-primary tooltips table-btn" data-original-title="New Reviewer"
           data-toggle="tooltip"><i class="fa fa-plus"></i> <span class="hidden-xs"><?php echo __('Add Reviewer'); ?></span></a>
              </th>
                  <form method="post" action="/backend.php/users/index/filter/<?php echo $filter; ?><?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?>">


                      <th class="border-bottom-1" style="width:50%;">
                              <input name="search" value="<?php echo $filter; ?>" type="text" class="form-control p10">
                      </th>
                      
                          <th class="border-bottom-1 radius-tr">
                                  <select size="1" name="filter_status" aria-controls="table2"
                                          class="select2"
                                          onChange="window.location='/backend.php/users/index/filterstatus/' + this.value;">
                                      <!--<option value="1">Select Status</option>-->
                                      <option value="0" <?php if ($filterstatus == "0") {
                                          echo "selected='selected'";
                                      } ?>>Active
                                      </option>
                                      <option value="1" <?php if ($filterstatus == "1") {
                                          echo "selected='selected'";
                                      } ?>>Inactive
                                      </option>
                                  </select>
                          </th>
                  </form>
              </tr>
              </thead>
          </table>

          <table  class="table table-striped table-hover mb0 radius-bl radius-br">
              <thead>
              <tr class="main-tr">
                  <th>#</th>
                  <th>Full Name</th>
                  <th>Email Address</th>
                  <th>Username</th>
				  <!--OTB Start - Show agencies in list of backend users-->
                  <th>Agencies</th>
				  <!--OTB End - Show agencies in list of backend users-->
                  <th>Status</th>
                  <th class="aligncenter">Action</th>
              </tr>
              </thead>
              <tbody>
              <?php
              $count = 0;

              if($pager->getPage() > 1)
              {
                  $count = 10 * ($pager->getPage() - 1);
              }

              foreach($pager->getResults() as $reviewer)
              {
                      $count++;
                      ?>
                      <tr>
                          <td><?php echo $count; ?></td>
                          <td><a title="View Reviewer" href="<?php echo public_path(); ?>backend.php/users/viewuser/userid/<?php echo $reviewer->getNid(); ?>"><?php echo strtoupper($reviewer->getStrfirstname()." ".$reviewer->getStrlastname()); ?></a></td>
                          <td><a title="View Reviewer" href="<?php echo public_path(); ?>backend.php/users/viewuser/userid/<?php echo $reviewer->getNid(); ?>"><?php echo $reviewer->getStremail(); ?></a></td>
                          <td><a title="View Reviewer" href="<?php echo public_path(); ?>backend.php/users/viewuser/userid/<?php echo $reviewer->getNid(); ?>"><?php echo $reviewer->getStruserid(); ?></a></td>
							<!--OTB Start - Show agencies in list of backend users-->
							<?php
								$agencies ="";
								   foreach($reviewer->getAgencyUser() as $agency){
									   $agencies .= $agency->getAgency()->getName()." ,";
									error_log("His agencies are ### ".print_R($agency->getAgency()->getName(), true));
								   }
							?>
                          <td><?php echo $agencies; ?></td>
							<!--OTB End - Show agencies in list of backend users-->
                          <?php
                          if($reviewer->getBdeleted() == 1) {
                          ?>
                              <td>
                                  <span class="label label-danger">Inactive</span>
                              </td>
                          <?php
                          }
                          else {
                              ?>
                              <td>
                                  <span class="label label-success">Active</span>
                              </td>
                          <?php
                          }
                          ?>
                          <td class="aligncenter">
                              <a title="View Reviewer" href="<?php echo public_path(); ?>backend.php/users/viewuser/userid/<?php echo $reviewer->getNid(); ?>"><span class="badge badge-primary"><i class="fa fa-eye"></i></span></a>
                              <?php
                                if($sf_user->mfHasCredential("managereviewers")) {
                                    if ($reviewer->getBdeleted() == 1) {
                                        ?>
                                        <a title="Activate Reviewer" onclick="if(confirm('Are you sure you want to restore this user?')){ return true; }else{ return false; }"
                                           href="<?php echo public_path(); ?>backend.php/users/restore/id/<?php echo $reviewer->getNid(); ?>"><span
                                                class="badge badge-primary"><i
                                                    class="fa fa-check-circle"></i></span></a>
                                    <?php
                                    } else {
                                        ?>
                                        <a title="Deactivate Reviewer" onclick="if(confirm('Are you sure you want to deactivate this user?')){ return true; }else{ return false; }"
                                           href="<?php echo public_path(); ?>backend.php/users/delete/id/<?php echo $reviewer->getNid(); ?>"><span
                                                class="badge badge-primary"><i class="fa fa-times-circle-o"></i></span></a>
                                    <?php
                                    }
                                }
                              ?>
                          </td>
                      </tr>
                  <?php
              }
              ?>
              </tbody>
              <tfoot>
              <tr>
                  <th colspan="12">
                      <p class="table-showing pull-left"><strong><?php echo count($pager) ?></strong> Reviewers

                          <?php if ($pager->haveToPaginate()): ?>
                              - page <strong><?php echo $pager->getPage() ?>/<?php echo $pager->getLastPage() ?></strong>
                          <?php endif; ?></p>


                      <?php if ($pager->haveToPaginate()): ?>
                          <ul class="pagination pagination-sm mb0 mt0 pull-right">
                              <li><a href="/backend.php/users/index/page/1<?php if($filter){ echo "/filter/".$filter; } ?><?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?><?php if($fromdate){ echo "/fromdate/".$fromdate."/todate/".$todate; } ?>">
                                      <i class="fa fa-angle-left"></i>
                                  </a></li>

                              <li><a href="/backend.php/users/index/page/<?php echo $pager->getPreviousPage() ?><?php if($filter){ echo "/filter/".$filter; } ?><?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?><?php if($fromdate){ echo "/fromdate/".$fromdate."/todate/".$todate; } ?>">
                                      <i class="fa fa-angle-left"></i>
                                  </a></li>

                              <?php foreach ($pager->getLinks() as $page): ?>
                                  <?php if ($page == $pager->getPage()): ?>
                                      <li class="active"><a href=""><?php echo $page ?></li></a>
                                  <?php else: ?>
                                      <li><a href="/backend.php/users/index/page/<?php echo $page ?><?php if($filter){ echo "/filter/".$filter; } ?><?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?><?php if($fromdate){ echo "/fromdate/".$fromdate."/todate/".$todate; } ?>"><?php echo $page ?></a></li>
                                  <?php endif; ?>
                              <?php endforeach; ?>

                              <li><a href="/backend.php/users/index/page/<?php echo $pager->getNextPage() ?><?php if($filter){ echo "/filter/".$filter; } ?><?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?><?php if($fromdate){ echo "/fromdate/".$fromdate."/todate/".$todate; } ?>">
                                      <i class="fa fa-angle-right"></i>
                                  </a></li>

                              <li><a href="/backend.php/users/index/page/<?php echo $pager->getLastPage() ?><?php if($filter){ echo "/filter/".$filter; } ?><?php if($filterstatus != ""){ echo "/filterstatus/".$filterstatus; } ?><?php if($fromdate){ echo "/fromdate/".$fromdate."/todate/".$todate; } ?>">
                                      <i class="fa fa-angle-right"></i>
                                  </a>
                              </li>
                          </ul>
                      <?php endif; ?>
                  </th>
              </tr>
              </tfoot>
          </table>
      </div><!-- table-responsive -->
      <?php else: ?>
          <div class="table-responsive">
              <table class="table dt-on-steroids mb0">
                  <tbody>
                  <tr><td>
                          No Records Found
                      </td></tr>
                  </tbody>
              </table>
          </div>
      <?php endif; ?>
  </div>

</div>
<?php
}
else
{
   include_partial("settings/accessdenied");
}
?>
