<?php
/**
 * myaccountSuccess.php template.
 *
 * Allows currently logged in reviewer to manage their account details
 *
 * @package    backend
 * @subpackage users
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

$_REQUEST['language'] = 'en';

$prefix_folder = dirname(__FILE__)."/../../../../..";
require_once $prefix_folder.'/lib/vendor/cp_workflow/config/config.inc.php';


require_once $prefix_folder.'/lib/vendor/cp_workflow/language_files/language.inc.php';
require_once $prefix_folder.'/lib/vendor/cp_workflow/pages/CCirculation.inc.php';

$objCirculation = new CCirculation();
?>
<?php

$strFirstName = "";

$strLastName = "";

$strEMail = "";

$nAccessLevel = 1;

$nSubstitudeId = 0;



if ($userid == -1)

{

    $strPassword = "";

}

else

{

    $strPassword = "unchanged";

}



$arrSubstitutes[0]['name'] = '-';

$arrSubstitutes[0]['id'] = 0;



$CUR_SUBTITUTE_PERSON_UNIT	= $SUBTITUTE_PERSON_UNIT;

$CUR_SUBTITUTE_PERSON_VALUE	= $SUBTITUTE_PERSON_VALUE;



if (-1 != $userid)

{

    //--- open database

    $nConnection = mysql_connect($DATABASE_HOST, $DATABASE_UID, $DATABASE_PWD);



    if ($nConnection)

    {

        //--- get maximum count of users

        if (mysql_select_db($DATABASE_DB, $nConnection))

        {

            //--- read the values of the user

            $strQuery = "SELECT * FROM cf_user WHERE nid = ".$userid;

            $nResult = mysql_query($strQuery, $nConnection);



            if ($nResult)

            {

                if (mysql_num_rows($nResult) > 0)

                {

                    while (	$arrRow = mysql_fetch_array($nResult))

                    {

                        $strFirstName 	= $arrRow["strfirstname"];

                        $strLastName 	= $arrRow["strlastname"];

                        $strEMail 		= $arrRow["stremail"];

                        $nAccessLevel 	= $arrRow["naccesslevel"];

                        $nSubstituteId 	= $arrRow["nsubstitudeid"];

                        $strUserId 		= $arrRow["struserid"];

                        $EMAIL_FORMAT 	= $arrRow["stremail_format"];

                        $EMAIL_VALUES 	= $arrRow["stremail_values"];

                        $street			= $arrRow['strstreet'];

                        $country		= $arrRow['strcountry'];

                        $zipcode		= $arrRow['strzipcode'];

                        $city			= $arrRow['strcity'];

                        $phone_main1	= $arrRow['strphone_main1'];

                        $phone_main2	= $arrRow['strphone_main2'];

                        $phone_mobile	= $arrRow['strphone_mobile'];

                        $fax			= $arrRow['strfax'];

                        $organisation	= $arrRow['strorganisation'];

                        $department		= $arrRow['strdepartment'];

                        $cost_center	= $arrRow['strcostcenter'];

                        $userdefined1_value	= $arrRow['userdefined1_value'];

                        $userdefined2_value	= $arrRow['userdefined2_value'];

                        $CUR_SUBTITUTE_PERSON_UNIT	= $arrRow['strsubstitutetimeunit'];

                        $CUR_SUBTITUTE_PERSON_VALUE	= $arrRow['nsubstitutetimevalue'];

                        $bIndividualSubsTime	= $arrRow['busegeneralsubstituteconfig'];

                        $bIndividualEmail		= $arrRow['busegeneralemailconfig'];

                    }

                }

            }



            if ($nSubstituteId == -2)

            {	// user has one or more substitutes - the DB table "cf_substitute" has to be checked

                $arrResult = $objCirculation->getSubstitutes($userid);



                $nMax = sizeof($arrResult);

                for ($nIndex = 0; $nIndex < $nMax; $nIndex++)

                {

                    $nCurSubstituteId = $arrResult[$nIndex]['substitute_id'];

                    if ($nCurSubstituteId == -3)

                    {

                        $arrSubstitutes[$nIndex]['name']	= $SELF_DELEGATE_USER;

                        $arrSubstitutes[$nIndex]['id']		= -3;

                    }

                    else

                    {

                        $arrUser = $objCirculation->getUserById($nCurSubstituteId);



                        $arrSubstitutes[$nIndex]['name'] 	= $arrUser['strlastname'].', '.$arrUser['strfirstname'];

                        $arrSubstitutes[$nIndex]['id'] 		= $nCurSubstituteId;

                    }

                }

            }

        }

    }

}

else

{

    $EMAIL_FORMAT = 'HTML';

    $EMAIL_VALUES = 'IFRAME';

}
	$q = Doctrine_Query::create()
		 ->from('CfUser a')
		 ->where('a.struserid = ?', $_REQUEST['userid']);
	$logged_reviewer = $q->fetchOne();
?>

<div class="pageheader">
  <h2><i class="fa fa-home"></i> Reviewers <span>List of reviewers</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>backend.php">Home</a></li>
      <li class="active">Reviewers</li>
    </ol>
  </div>
</div>


<div class="contentpanel">
<div class="row">



<div class="panel panel-dark">
<div class="panel-heading">
<h3 class="panel-title">My Account Details</h3>
</div>

<div class="panel-body panel-body-nopadding">



<ul class="nav nav-tabs nav-justified">
    <li class="active"><a href="#tabs-1"  data-toggle="tab">Basic Details</a></li>
    <li><a href="#tabs-2"  data-toggle="tab">Additional Details</a></li>
</ul>

<div class="tab-content tab-content-nopadding">
<div class="tab-pane active" id="tabs-1">
<form  class="form-bordered form-horizontal" action="<?php echo public_path(); ?>backend.php/users/writeuser" method="post"  autocomplete="off" data-ajax="false">

		    		<div class="form-group">
                <label class="col-sm-2 control-label" for="text_field">First Name</label>

						<div class="col-sm-8">

					<input id="strFirstName" Name="strFirstName" type="text" class="form-control" value="<?php echo $strFirstName;?>">
					</div>

					</div>

					<div class="form-group">
                    <label class="col-sm-2 control-label" for="text_field">Last Name</label>

						<div class="col-sm-8">

							<input id="strLastName" Name="strLastName" type="text" class="form-control" value="<?php echo $strLastName;?>">

						</div>

					</div>

			      		<div class="form-group"><label class="col-sm-2 control-label" for="text_field">Email Address</label>

						<div class="col-sm-8">

							<input id="strEMail" Name="strEMail" type="text" class="form-control" value="<?php echo $strEMail;?>">

				
						</div>

					</div>	

	         		<div class="form-group"><label class="col-sm-2 control-label" for="text_field">Username</label>

						<div class="col-sm-8">

							<input type="text" Name="UserName" id="UserName" class="form-control" value="<?php echo $strUserId;?>">

				
						</div>

					</div>
  
	                   <div class="form-group"><label class="col-sm-2 control-label" for="text_field">Password</label>

						<div class="col-sm-8">

							<input type="password" Name="Password1" id="Password1" class="form-control" value="<?php echo $strPassword;?>">

				
						</div>

					</div>
					
	                <div class="form-group"><label class="col-sm-2 control-label" for="text_field">Department</label>

						<div class="col-sm-8">
                    <select id="IN_department" name="IN_department">
                    <option value=''>Select Department...</option>
                    <?php
					$q = Doctrine_Query::create()
					   ->from('Department a');
					$departments = $q->execute();
					foreach($departments as $this_department)
					{
					    $selected = "";
						if($department == $this_department->getDepartmentName())
						{
							$selected = "selected";
						}
						echo "<option value='".$this_department->getDepartmentName()."' ".$selected.">".$this_department->getDepartmentName()."</option>";
					}
					?>
                    </select>

				
						</div>

					</div>

					<?php
					
					if($sf_user->mfHasCredential("access_groups"))
					{
					
					?>
					
					 <div class="form-group"><label class="col-sm-2 control-label" for="text_field">My Groups</label>

						<div class="col-sm-8">

							<select id="groups" name="groups" multiple>
							<?php
							$q = Doctrine_Query::create()
								 ->from('MfGuardGroup a')
								 ->orderBy('a.name ASC');
							$groups = $q->execute();
							foreach($groups as $group)
							{	
								$selected = "";
								$q = Doctrine_Query::create()
									 ->from('MfGuardUserGroup a')
									 ->where('a.user_id = ?',  $userid)
									 ->andWhere('a.group_id = ?', $group->getId());
								$usergroup = $q->execute();
								
								if(sizeof($usergroup) > 0)
								{
									$selected = "selected";
								}
								
								?>
								<option value='<?php echo $group->getId(); ?>' <?php echo $selected; ?>><?php echo $group->getName(); ?></option>
								<?php
							}
							?>
							</select>

				
						</div>

					</div>

					<?php
					
					}
					
					?>

					<input type="hidden" name="IN_bIndividualEmail" id="IN_bIndividualEmail" <?php if (!$bIndividualEmail) echo 'checked' ?> value="1" onClick="checkIndividual();">
			
                    <div class="panel-footer">
        <button class="btn btn-danger mr10">Reset</button> <button class="btn btn-primary" name="submitbuttonname" value="submitbuttonvalue">Submit</button>
    </div>
			
			</form>
</div><!--Tab1-->
   
<div  class="tab-pane" id="tabs-2">
    <form  class="form-bordered" action="<?php echo public_path(); ?>backend.php/users/writeuser" method="post"  autocomplete="off" data-ajax="false">



	       <div class="form-group"><label class="col-sm-2 control-label" for="text_field">Street Name</label>

						<div class="col-sm-8">

					<input type="text" id="IN_street" name="IN_street" class="form-control" value="<?php echo $street ?>">

				
						</div>

					</div>

	       <div class="form-group"><label class="col-sm-2 control-label" for="text_field">Zip Code / City</label>

					<div class="col-sm-2">
					<input type="text" id="IN_zipcode" name="IN_zipcode" class="form-control"  value="<?php echo $zipcode ?>">
                    </div>
                    <div class="col-sm-6">
					<input type="text" id="IN_city" name="IN_city" class="form-control" value="<?php echo $city ?>">
                    </div>
						

					</div>

	       <div class="form-group"><label class="col-sm-2 control-label" for="text_field">Country</label>

						<div class="col-sm-8">
					<input type="text" id="IN_country" name="IN_country" class="form-control" value="<?php echo $country ?>">

						</div>

					</div>

	       <div class="form-group"><label class="col-sm-2 control-label" for="text_field">Phone Number 1</label>

						<div class="col-sm-8">
					<input type="text" id="IN_phone_main1" name="IN_phone_main1" class="form-control" value="<?php echo $phone_main1 ?>">

				
						</div>

					</div>

	       <div class="form-group"><label class="col-sm-2 control-label" for="text_field">Phone Number 2</label>

						<div class="col-sm-8">

					<input type="text" id="IN_phone_main2" name="IN_phone_main2" class="form-control" value="<?php echo $phone_main2 ?>">

				
						</div>

					</div>

	       <div class="form-group"><label class="col-sm-2 control-label" for="text_field">Mobile Number</label>

						<div class="col-sm-8">

					<input type="text" id="IN_phone_mobile" name="IN_phone_mobile" class="form-control" value="<?php echo $phone_mobile ?>">

				
						</div>

					</div>

	       <div class="form-group"><label class="col-sm-2 control-label" for="text_field">Fax Number</label>

						<div class="col-sm-8">

					<input type="text" id="IN_fax" name="IN_fax" class="form-control" value="<?php echo $fax ?>">

				
						</div>

					</div>

	       <div class="form-group"><label class="col-sm-2 control-label" for="text_field">Organization</label>

						<div class="col-sm-8">

					<input type="text" id="IN_organisation" name="IN_organisation" class="form-control" value="<?php echo $organisation ?>">

				
						</div>

					</div>

	       <div class="form-group"><label class="col-sm-2 control-label" for="text_field">Cost Center</label>

						<div class="col-sm-8">

					<input type="text" id="IN_cost_center" name="IN_cost_center" class="form-control" value="<?php echo $cost_center ?>">

				
						</div>

					</div>

	       <div class="form-group"><label class="col-sm-2 control-label" for="text_field"><?php echo $USERDEFINED_TITLE1 ?></label>

						<div class="col-sm-8">

					<input type="text" id="IN_userdefined1_value" name="IN_userdefined1_value" class="form-control" value="<?php echo $userdefined1_value ?>">

				
						</div>

					</div>

	       <div class="form-group"><label class="col-sm-2 control-label" for="text_field"><?php echo $USERDEFINED_TITLE2 ?></label>

						<div class="col-sm-8">

					<input type="text" id="IN_userdefined2_value" name="IN_userdefined2_value" class="form-control" value="<?php echo $userdefined2_value ?>">

				</div>

			</div>

						<div class="panel-footer">
							<button class="btn btn-danger mr10">Reset</button><button class="btn btn-primary" name="submitbuttonname" value="submitbuttonvalue">Submit</button>
                            <input type="hidden" value="<?php echo $userid;?>" id="userid" name="userid">
						</div>
</form>



</div><!--tabcontent-->

</div><!--tab-content-->

</div><!--panel-dark-->

</div><!--row-->
</div><!--content-->

