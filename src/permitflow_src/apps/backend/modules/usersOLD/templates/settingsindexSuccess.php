<?php
$audit = new Audit();
$audit->saveAudit("", "Accessed reviewers settings");

if($sf_user->mfHasCredential("managereviewers"))
{
  $_SESSION['current_module'] = "reviewers";
  $_SESSION['current_action'] = "index";
  $_SESSION['current_id'] = "";
?>
<?php
/**
 * indexSuccess.php template.
 *
 * Displays list of reviewer departments
 *
 * @package    backend
 * @subpackage users
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

?>
<div class="contentpanel">


<div class="mb20"></div>
<div class="row">
<div class="panel panel-dark">
<div class="panel-heading">
  <h3 class="panel-title">Reviewers</h3>
  <p>List of all Reviewers</p>

  <div style="float: right; margin-top: -50px;">
<a id="newuser" class="btn btn-default-alt tooltips pull-right" type="button" data-toggle="tooltip" title="New Reviewer" href="#">Add New Reviewer</a>
        <script language="javascript">
          jQuery(document).ready(function(){
            $( "#newuser" ).click(function() {
                $("#contentload").load("<?php echo public_path(); ?>backend.php/users/edituser");
            });
          });
          </script>
</div>
 </div>
<div class="panel-body panel-body-nopadding">
<div class="table-responsive">
   <table class="table dt-on-steroids mb0" id="table3">
    <thead>
      <tr>
      <th width="60">ID</th>
      <th style="width:20%">Full Name</th>
      <th>Department </th>
      <th class="no-sort"width="7%">Actions </th>
    </tr>
    </thead>
    <tbody>
	<?php
		$count = 1;
	?>
    <?php foreach ($reviewers as $cfuser): ?>
    <tr id="row_<?php echo $cfuser->getNid() ?>">
	  <td><?php echo $cfuser->getNid(); ?></td>
      <td><a id="edituser2<?php echo $cfuser->getNid(); ?>" href="#" alt="Edit"><?php echo ucwords(strtolower($cfuser->getStrfirstname()." ".$cfuser->getStrlastname())); ?></a></td>
      <td>
      <?php echo $cfuser->getStrdepartment(); ?>
      </td>
	  <td>
	  <a id="edituser<?php echo $cfuser->getNid(); ?>" href="#" alt="Edit"><span class="badge badge-primary"><i class='fa fa-search-plus'></i></span></a>
	  <a id="deleteuser<?php echo $cfuser->getNid(); ?>" href="#"><span class="badge badge-primary"><i class='fa fa-trash-o'></i></span></a>

    <script language="javascript">
    jQuery(document).ready(function(){
      $( "#edituser<?php echo $cfuser->getNid(); ?>" ).click(function() {
          $("#contentload").load("<?php echo public_path(); ?>backend.php/users/edituser/userid/<?php echo $cfuser->getNid(); ?>");
      });
      $( "#edituser2<?php echo $cfuser->getNid(); ?>" ).click(function() {
          $("#contentload").load("<?php echo public_path(); ?>backend.php/users/edituser/userid/<?php echo $cfuser->getNid(); ?>");
      });
      $( "#deleteuser<?php echo $cfuser->getNid(); ?>" ).click(function() {
        if(confirm('Are you sure you want to delete this reviewer?')){
          $("#contentload").load("<?php echo public_path(); ?>backend.php/users/delete/id/<?php echo $cfuser->getNid(); ?>");
        }
        else
        {
          return false;
        }
      });
    });
    </script>
	  </td>
    </tr>
    <?php endforeach; ?>
   </tbody>
   </table>

     </div>
     </div>
</div>
</div>
</div>
<script language="javascript">
$(document).ready(function(){
  //batch activate architect accounts
  $('body').delegate('#batch_activate','click',function(){
	  	var selected_checkboxes = new Array();
		var count = 0;
		$("input[type='checkbox']").each(function(){
			var chkval = 0
			if($(this).is(":checked")){
				selected_checkboxes[count]  = $(this).attr('value');
				count++;
			}
		});
	    $.ajax({
		   url: "<?php echo public_path(); ?>backend.php/frusers/batch",
		   type: "POST",

		   data:{'batchitems':selected_checkboxes,'activate':'true'},

		   success:function(returndata){
				$("input[type='checkbox']").each(function(){
					var chkval = 0
					if($(this).is(":checked")){
						$(this).prop('checked', false);
						//convert crosses to ticks
						count++;
					}
				});
		   },error:function(errordata){
				alert("Could not finish your request. Please try again.");
		   }
		 });

   });
  //batch deactivate architect accounts
  $('body').delegate('#batch_deactivate','click',function(){
	  	var selected_checkboxes = new Array();
		var count = 0;
		$("input[type='checkbox']").each(function(){
			var chkval = 0
			if($(this).is(":checked")){
				selected_checkboxes[count]  = $(this).attr('value');
				count++;
			}
		});
	    $.ajax({
		   url: "<?php echo public_path(); ?>backend.php/frusers/batch",
		   type: "POST",

		   data:{'batchitems':selected_checkboxes,'deactivate':'true'},

		   success:function(returndata){
				$("input[type='checkbox']").each(function(){
					var chkval = 0
					if($(this).is(":checked")){
						$(this).prop('checked', false);
						//convert ticks to crosses
						count++;
					}
				});
		   },error:function(errordata){
				alert("Could not finish your request. Please try again.");
		   }
		 });

   });

});
</script>
<script>
  jQuery('#table3').dataTable({
      "sPaginationType": "full_numbers",

      // Using aoColumnDefs
      "aoColumnDefs": [
      	{ "bSortable": false, "aTargets": [ 'no-sort' ] }
    	]
    });

</script>

<?php
}
else
{
  include_partial("accessdenied");
}
?>
