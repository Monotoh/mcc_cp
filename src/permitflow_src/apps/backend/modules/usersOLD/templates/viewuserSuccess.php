<?php
/**
 * viewuserSuccess.php template.
 *
 * Displays full reviewer details
 *
 * @package    backend
 * @subpackage users
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
 use_helper("I18N");
?>
<?php if($sf_user->mfHasCredential("managereviewers"))
{ ?>
<script type="text/javascript" src="<?php echo public_path(); ?>assets_unified/js/jquery.bootstrap-duallistbox.js"></script>
<div class="pageheader">
  <h2><i class="fa fa-user"></i> <?php echo __('Profile'); ?><span>View reviewer details</span></h2>
  <div class="breadcrumb-wrapper">
    <span class="label"><?php echo __('You are here'); ?>:</span>
    <ol class="breadcrumb">
      <li><a href="<?php echo public_path(); ?>backend.php"><?php echo __('Home'); ?></a></li>
      <li class="active"><?php echo __('My Account'); ?></li>
    </ol>
  </div>
</div>

<div class="contentpanel">
	<div class="row">

     <div class="col-sm-12">

     <?php
     if($sf_user->hasFlash("notice"))
     {
        ?>
         <div class="alert alert-success">
             <button type="button" class="close" aria-hidden="true">&times;</button>
             <strong><?php echo __('Success'); ?>! </strong><?php echo $sf_user->getFlash("notice"); ?>
         </div>
        <?php
     }

     if($sf_user->hasFlash("error"))
     {
         ?>
         <div class="alert alert-danger">
             <button type="button" class="close" aria-hidden="true">&times;</button>
             <strong><?php echo __('Error'); ?>! </strong><?php echo $sf_user->getFlash("error"); ?>
         </div>
        <?php
     }
     ?>

	     <div class="user-profile">
	     <div class="panel panel-default">
	     <div class="panel-heading">
	     <h3 class="panel-title">
	     <?php echo $reviewer->getStrfirstname(); ?> <?php echo $reviewer->getStrlastname(); ?>
	     </h3>
	     </div>
	     <div class="panel-body panel-body-nopadding">

	         <div class="col-sm-3 sidebar">

	          <div class="mb30"></div>

	          <h5 class="subtitle">General</h5>

	          <ul class="profile-social-list">
	            <li><a id="account_settings_btn" href="#" class="active">Account Settings</a></li>
	            <li><a id="email_settings_btn"  href="#">Email Addresses</a></li>
	            <li><a id="phone_settings_btn"  href="#">Phone Numbers</a></li>
	          </ul>

	          <div class="mb30"></div>

	          <h5 class="subtitle">Access Management</h5>

	          <ul class="profile-social-list">
	          	<?php
	          	if($sf_user->mfHasCredential("managegroups"))
	          	{
	          	?>
	            <li><a id="group_settings_btn"  href="#"><?php echo __('User Groups') ?></a></li>
	            <?php
 				}
	            ?>
	          </ul>

<!--OTB Start - Multiagency fucntionality-->
	          <ul class="profile-social-list">
	          	<?php
	          	if($sf_user->mfHasCredential("manageagencies"))
	          	{
	          	?>
	            <li><a id="agency_settings_btn"  href="#"><?php echo __('Agencies') ?></a></li>
	            <?php
 				}
	            ?>
	          </ul>
<!--OTB End - Multiagency fucntionality-->

	   		  <div class="mb30"></div>

	          <h5 class="subtitle">Security</h5>

	          <ul class="profile-social-list">
	            <li><a id="password_settings_btn"  href="#"><?php echo __('Change Password') ?></a></li>
	            <li><a id="activity_log_btn"  href="#"><?php echo __('Activity Log') ?></a></li>
	            <li><a id="sessions_btn"  href="#"><?php echo __('Sessions') ?></a></li>
	          </ul>

	          <div class="mb30"></div>

	          <script language="javascript">
 				jQuery(document).ready(function(){
 					$("#account_settings_btn").click(function(e) {
		                $("#account_settings").css('display', 'block');
		                $("#agency_settings").css('display', 'none');//OTB Patch - Multiagency fucntionality
		                $("#email_settings").css('display', 'none');
		                $("#phone_settings").css('display', 'none');
		                $("#group_settings").css('display', 'none');
		                $("#password_settings").css('display', 'none');
		                $("#activity_log").css('display', 'none');
		                $("#sessions").css('display', 'none');
 					});
 					$("#email_settings_btn").click(function(e) {
		                $("#account_settings").css('display', 'none');
		                $("#agency_settings").css('display', 'none');//OTB Patch - Multiagency fucntionality
		                $("#email_settings").css('display', 'block');
		                $("#phone_settings").css('display', 'none');
		                $("#group_settings").css('display', 'none');
		                $("#password_settings").css('display', 'none');
		                $("#activity_log").css('display', 'none');
		                $("#sessions").css('display', 'none');
 					});
 					$("#phone_settings_btn").click(function(e) {
		                $("#account_settings").css('display', 'none');
		                $("#agency_settings").css('display', 'none');//OTB Patch - Multiagency fucntionality
		                $("#email_settings").css('display', 'none');
		                $("#phone_settings").css('display', 'block');
		                $("#group_settings").css('display', 'none');
		                $("#password_settings").css('display', 'none');
		                $("#activity_log").css('display', 'none');
		                $("#sessions").css('display', 'none');
 					});
 					$("#group_settings_btn").click(function(e) {
		                $("#account_settings").css('display', 'none');
		                $("#agency_settings").css('display', 'none');//OTB Patch - Multiagency fucntionality
		                $("#email_settings").css('display', 'none');
		                $("#phone_settings").css('display', 'none');
		                $("#group_settings").css('display', 'block');
		                $("#password_settings").css('display', 'none');
		                $("#activity_log").css('display', 'none');
		                $("#sessions").css('display', 'none');
 					});
 					$("#agency_settings_btn").click(function(e) {
		                $("#account_settings").css('display', 'none');
		                $("#agency_settings").css('display', 'block');//OTB Patch - Multiagency fucntionality
		                $("#email_settings").css('display', 'none');
		                $("#phone_settings").css('display', 'none');
		                $("#group_settings").css('display', 'none');
		                $("#password_settings").css('display', 'none');
		                $("#activity_log").css('display', 'none');
		                $("#sessions").css('display', 'none');
 					});
 					$("#password_settings_btn").click(function(e) {
		                $("#account_settings").css('display', 'none');
		                $("#agency_settings").css('display', 'none');//OTB Patch - Multiagency fucntionality
		                $("#email_settings").css('display', 'none');
		                $("#phone_settings").css('display', 'none');
		                $("#group_settings").css('display', 'none');
		                $("#password_settings").css('display', 'block');
		                $("#activity_log").css('display', 'none');
		                $("#sessions").css('display', 'none');
 					});
 					$("#activity_log_btn").click(function(e) {
		                $("#account_settings").css('display', 'none');
		                $("#agency_settings").css('display', 'none');//OTB Patch - Multiagency fucntionality
		                $("#email_settings").css('display', 'none');
		                $("#phone_settings").css('display', 'none');
		                $("#group_settings").css('display', 'none');
		                $("#password_settings").css('display', 'none');
		                $("#activity_log").css('display', 'block');
		                $("#sessions").css('display', 'none');
 					});
 					$("#sessions_btn").click(function(e) {
		                $("#account_settings").css('display', 'none');
		                $("#agency_settings").css('display', 'none');//OTB Patch - Multiagency fucntionality
		                $("#email_settings").css('display', 'none');
		                $("#phone_settings").css('display', 'none');
		                $("#group_settings").css('display', 'none');
		                $("#password_settings").css('display', 'none');
		                $("#activity_log").css('display', 'none');
		                $("#sessions").css('display', 'block');
 					});

 					<?php
 						if($page || $_POST['fromdate'])
 						{
 					?>
		                $("#account_settings").css('display', 'none');
		                $("#agency_settings").css('display', 'none');//OTB Patch - Multiagency fucntionality
		                $("#email_settings").css('display', 'none');
		                $("#phone_settings").css('display', 'none');
		                $("#group_settings").css('display', 'none');
		                $("#password_settings").css('display', 'none');
		                $("#activity_log").css('display', 'block');
		                $("#sessions").css('display', 'none');
 					<?php
 						}
 					?>

 					<?php
 						if($spage || $aspage)
 						{
 					?>
		                $("#account_settings").css('display', 'none');
		                $("#agency_settings").css('display', 'none');//OTB Patch - Multiagency fucntionality
		                $("#email_settings").css('display', 'none');
		                $("#phone_settings").css('display', 'none');
		                $("#group_settings").css('display', 'none');
		                $("#password_settings").css('display', 'none');
		                $("#activity_log").css('display', 'none');
		                $("#sessions").css('display', 'block');
 					<?php
            }
           if($apppage)
           {
         ?>
                  $("#account_settings").css('display', 'none');
                  $("#agency_settings").css('display', 'none');//OTB Patch - Multiagency fucntionality
                  $("#email_settings").css('display', 'none');
                  $("#phone_settings").css('display', 'none');
                  $("#group_settings").css('display', 'none');
                  $("#password_settings").css('display', 'none');
                  $("#activity_log").css('display', 'block');
                  $("#sessions").css('display', 'none');
         <?php
       }
          if($taskpage)
          {
        ?>
                 $("#account_settings").css('display', 'none');
                 $("#agency_settings").css('display', 'none');//OTB Patch - Multiagency fucntionality
                 $("#email_settings").css('display', 'none');
                 $("#phone_settings").css('display', 'none');
                 $("#group_settings").css('display', 'none');
                 $("#password_settings").css('display', 'none');
                 $("#activity_log").css('display', 'block');
                 $("#sessions").css('display', 'none');
        <?php
          }
 					?>
 				});
 			  </script>

	        </div><!-- col-sm-3 -->

	       <div class="col-sm-9 sidebar">
	         <div class="mb30"></div>
                 <!-- otb patch - show warning or error from controller -->
                 <?php if($sf_user->hasFlash('profile_size')): ?>
                <div class="alert alert-danger"> 
                    <button type="button" class="close" data-dismiss="alert"> <?php echo __('close') ?> </button>
                    <b> <?php echo $sf_user->getFlash('profile_size') ?> </b>
                </div>
                <?php endif; ?>
                  <?php if($sf_user->hasFlash('profile_error')): ?>
                <div class="alert alert-danger"> 
                    <button type="button" class="close" data-dismiss="alert"> <?php echo __('close') ?> </button>
                    <b> <?php echo $sf_user->getFlash('profile_error') ?> </b>
                </div>
                <?php endif; ?>
	        <div id="account_settings">
	         <div class="col-sm-9">

	         <h5 class="subtitle">Account Settings</h5>
	          <form id="reviewerform" class="form-horizontal form-bordered">

                <div class="alert alert-success" id="alertdiv" name="alertdiv" style="display: none;">
				  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
				  <strong><?php echo __('Well done'); ?></strong> <?php echo __('You successfully updated reviewer'); ?></a>.
				</div>

	            <input type="hidden" name="userid" value="<?php echo $reviewer->getNid(); ?>">

	            <div class="form-group">
	              <label class="col-sm-3">First Name</label>
	              <div class="col-sm-6">
	                <input type="text" required="required" class="form-control" id="readonlyinput" name="first_name" Placeholder="First Name" value="<?php echo $reviewer->getStrfirstname(); ?>">
	              </div>
	            </div>

	            <div class="form-group">
	              <label class="col-sm-3">Last Name</label>
	              <div class="col-sm-6">
	                <input type="text" required="required" class="form-control" id="readonlyinput" name="last_name" Placeholder="Last Name" value="<?php echo $reviewer->getStrlastname(); ?>">
	              </div>
	            </div>

       	         <div class="form-group">
       	         	<label class="col-sm-3"><i class="bold-label"><?php echo __('Department'); ?></i></label>
					<div class="col-sm-6">
	                    <select id="department" required="required" class="form-control" name="department" required="required">
	                    <option value=''><?php echo __('Select Department'); ?>...</option>
	                    <?php
						$q = Doctrine_Query::create()
						   ->from('Department a');
						$departments = $q->execute();
						foreach($departments as $this_department)
						{
						    $selected = "";
							if($reviewer->getStrdepartment() == $this_department->getDepartmentName())
							{
								$selected = "selected";
							}
							echo "<option value='".$this_department->getDepartmentName()."' ".$selected.">".$this_department->getDepartmentName()."</option>";
						}
						?>
	                    </select>
					</div>
				</div>

                <div class="form-group">
	       			<label class="col-sm-3" for="text_field"><i class="bold-label"><?php echo __('City'); ?> </i></label>
					<div class="col-sm-6">
						<input type="text" id="city" name="city" class="form-control"  value="<?php echo $reviewer->getStrcity(); ?>">
					</div>
				</div>

	       		<div class="form-group">
	       			<label class="col-sm-3" for="text_field"><i class="bold-label"><?php echo __('Country'); ?></i></label>
					<div class="col-sm-6">
						<input type="text" id="country" name="country" class="form-control"  value="<?php echo $reviewer->getStrcountry(); ?>">
					</div>
				</div>

	       		<div class="form-group">
	       			<label class="col-sm-3" for="text_field"><i class="bold-label"><?php echo __('Designation'); ?></i></label>
					<div class="col-sm-6">
						<input type="text" id="userdefined1_value" name="userdefined1_value" class="form-control"  value="<?php echo $reviewer->getUserdefined1Value(); ?>">
					</div>
				</div>

	       		<div class="form-group">
	       			<label class="col-sm-3" for="text_field"><i class="bold-label"><?php echo __('Man Number'); ?></i></label>
					<div class="col-sm-6">
						<input type="text" id="userdefined2_value" name="userdefined2_value" class="form-control"  value="<?php echo $reviewer->getUserdefined2Value(); ?>">
					</div>
				</div>

	            <br>
	             <div class="form-group">
	              <div class="col-sm-6">
                      <button class="btn btn-primary" id="submitbutton" name="submitbutton" type="submit">Save</button>
                      <?php
                      if($sf_user->mfHasCredential("managereviewers"))
                      {
                          if($reviewer->getBdeleted() == 1)
                          {
                              ?>
                              <button class="btn btn-success" id="submitbutton" name="submitbutton" type="button"
                                      onClick="if(confirm('Are you sure?')){ window.location='/backend.php/users/restore/id/' + <?php echo $reviewer->getNid(); ?>; }else{ return false; }">
                                  <?php echo __('Restore User') ?>
                              </button>
                          <?php
                          }
                          else {
                              ?>
                              <button class="btn btn-danger" id="submitbutton" name="submitbutton" type="button"
                                      onClick="if(confirm('Are you sure?')){ window.location='/backend.php/users/delete/id/' + <?php echo $reviewer->getNid(); ?>; }else{ return false; }">
                                  <?php echo __('Delete User') ?>
                              </button>
                          <?php
                          }
#OTB Africa Start Patch - Live Chat Link to users
						$livechat = new LiveChat();
						if($livechat->isOperator($reviewer->getNid()) && !$livechat->isInactiveOperator($reviewer->getNid())){
?>
                              <button class="btn btn-danger" id="submitbutton" name="submitbutton" type="button"
                                      onClick="if(confirm('Are you sure?')){ window.location='/backend.php/users/addoperator/do/deactivate/id/' + <?php echo $reviewer->getNid(); ?>; }else{ return false; }">
                                  <?php echo __('Deactivate Live Chat Operator'); ?>
                              </button>
<?php
						}else{
?>
                              <button class="btn btn-success" id="submitbutton" name="submitbutton" type="button"
                                      onClick="if(confirm('Are you sure?')){ window.location='/backend.php/users/addoperator/do/activate/id/' + <?php echo $reviewer->getNid(); ?>; }else{ return false; }">
                                  <?php echo __('Activate Live Chat Operator'); ?>
                              </button>
<?php
						}
#OTB Africa End Patch - Live Chat Link to users
                      }
                      ?>
                  </div>
	            </div>
	            <br>
	         </form>

	         <script language="javascript">
 				jQuery(document).ready(function(){
 				   $("#submitbutton").click(function(e) {
 				   	 e.preventDefault()
					 $.ajax({
						url: '<?php echo public_path(); ?>backend.php/users/updateuser',
						cache: false,
						type: 'POST',
						data : $('#reviewerform').serialize(),
						success: function(json) {
							$('#alertdiv').attr("style", "display: block;");
							$("html, body").animate({ scrollTop: 0 }, "slow");
						}
					});
				  });
			  });
	         </script>


	        	</div>
		        <div class="col-sm-3 aligncenter">
		         <div class="mb30"></div>
                         <!-- OTB patch -->
                          <div class="alert alert-warning">
                             <?php echo __('Max File Size 200kB') ?>
                         </div>
		         <?php
		         if($reviewer->getProfilePic())
		         {
               $q = Doctrine_Query::create()
                  ->from("ApSettings a")
                  ->where("a.id = 1")
                  ->orderBy("a.id DESC");
               $aplogo = $q->fetchOne();
               if($aplogo && $aplogo->getProfileDir())
               {
    		         ?>
                         
    		         <img src="<?php echo public_path() ?><?php echo $aplogo->getUploadDir() ?>/reviewer/profile/<?php echo $reviewer->getStruserid()  ?>/<?php echo $reviewer->getProfilePic(); ?>" width="200px;" class="thumbnail img-responsive mb0" alt="No image" />
    		        
                             <?php
               }
		         }
		         else
		         {
		         ?>
                        
		         <img src="/assets_unified/images/photos/profile-1.png" width="200px;" class="thumbnail img-responsive mb0" alt="" />
		         <?php
		     	 }
		         ?>
		         <div align="center" style="margin-top: 5px;">
		          <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">
	                <?php echo __('Change Picture') ?>
	              </button>
	             </div>
		        </div><!-- col-sm-3 -->

	        </div><!-- Account Settings -->

	        <div id="email_settings" style="display: none;">
	        	<h5 class="subtitle"><?php echo __('Email Addresses') ?></h5>
	        	<form id="emailform" class="form-horizontal form-bordered">

                <div class="alert alert-success" id="emailalertdiv" name="alertdiv" style="display: none;">
				  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
				  <strong><?php echo __('Well done'); ?></strong> <?php echo __('You successfully updated reviewer'); ?></a>.
				</div>

	            <input type="hidden" name="userid" value="<?php echo $reviewer->getNid(); ?>">

	            <div class="form-group">
	              <label class="col-sm-3"><?php echo __('Primary Email Address') ?></label>
	              <div class="col-sm-6">
				  <!--OTB - Email readonly if user cannot manager reviewers-->
						<?php if(!$sf_user->mfHasCredential("managereviewers")){
							$readonly = 'readonly="1"';
						  }?>
                          <input type="text" <?php echo $readonly ?> required="required" class="form-control" name="email_address" Placeholder="Email Address" value="<?php echo $reviewer->getStremail(); ?>">
	              </div>
	            </div>

	            <br>
						<?php if($sf_user->mfHasCredential("managereviewers")){ ?>
	            <div class="form-group">
	              <div class="col-sm-6"><button class="btn btn-primary" id="submitbutton2" name="submitbutton" type="submit"><?php echo __('Save')?></button></div>
	            </div>
						<?php } ?>
	            <br>

	         </form>

	         <script language="javascript">
 				jQuery(document).ready(function(){
 				   $("#submitbutton2").click(function(e) {
 				   	 e.preventDefault()
					 $.ajax({
						url: '<?php echo public_path(); ?>backend.php/users/updateemail',
						cache: false,
						type: 'POST',
						data : $('#emailform').serialize(),
						success: function(json) {
							$('#emailalertdiv').attr("style", "display: block;");
							$("html, body").animate({ scrollTop: 0 }, "slow");
						}
					});
				  });
			  });
	         </script>

	        </div><!-- Email Settings -->

	        <div id="phone_settings" style="display: none;">
	        	<h5 class="subtitle"><?php echo __('Phone Numbers') ?></h5>
	        	<form id="phoneform" class="form-horizontal form-bordered">

                <div class="alert alert-success" id="phonealertdiv" name="alertdiv" style="display: none;">
				  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
				  <strong><?php echo __('Well done'); ?></strong> <?php echo __('You successfully updated reviewer'); ?></a>.
				</div>

	            <input type="hidden" name="userid" value="<?php echo $reviewer->getNid(); ?>">

	            <div class="form-group">
	              <label class="col-sm-3"><?php echo __('Primary Phone Number') ?></label>
	              <div class="col-sm-6">
	                <input type="text" required="required" class="form-control" name="phone_number" Placeholder="Phone Number" value="<?php echo $reviewer->getStrphoneMain1(); ?>">
	              </div>
	            </div>

	            <br>
	             <div class="form-group">
	              <div class="col-sm-6"><button class="btn btn-primary" id="submitbutton3" name="submitbutton" type="submit"><?php echo __('Save') ?></button></div>
	            </div>
	            <br>

	         </form>

	         <script language="javascript">
 				jQuery(document).ready(function(){
 				   $("#submitbutton3").click(function(e) {
 				   	 e.preventDefault()
					 $.ajax({
						url: '<?php echo public_path(); ?>backend.php/users/updatephone',
						cache: false,
						type: 'POST',
						data : $('#phoneform').serialize(),
						success: function(json) {
							$('#phonealertdiv').attr("style", "display: block;");
							$("html, body").animate({ scrollTop: 0 }, "slow");
						}
					});
				  });
			  });
	         </script>

	        </div><!-- Phone Settings -->

	        <div id="group_settings" style="display: none;">
	        	<h5 class="subtitle">User Groups</h5>
	        	<form id="groupform" class="form-horizontal form-bordered">

                <div class="alert alert-success" id="groupalertdiv" name="alertdiv" style="display: none;">
				  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
				  <strong><?php echo __('Well done'); ?></strong> <?php echo __('You successfully updated reviewer'); ?></a>.
				</div>

	            <input type="hidden" name="userid" value="<?php echo $reviewer->getNid(); ?>">

	            <div class="form-group">
	              <label class="col-sm-3"><?php echo __('This reviewer belongs to the following groups') ?>:</label>
	              <div class="col-sm-6">
	                <select id="groups" name="groups[]" multiple class="form-control" required="required">
						<?php
						$q = Doctrine_Query::create()
							 ->from('MfGuardGroup a')
							 ->orderBy('a.name ASC');
						$groups = $q->execute();
						foreach($groups as $group)
						{
							$selected = "";
							$q = Doctrine_Query::create()
								 ->from('MfGuardUserGroup a')
								 ->where('a.user_id = ?',  $reviewer->getNid())
								 ->andWhere('a.group_id = ?', $group->getId());
							$usergroup = $q->execute();

							if(sizeof($usergroup) > 0)
							{
								$selected = "selected";
							}

							?>
							<option value='<?php echo $group->getId(); ?>' <?php echo $selected; ?>><?php echo $group->getName(); ?></option>
							<?php
						}
						?>
					</select>
					<script language="javascript">
					 jQuery(document).ready(function(){

					 	var demo1 = $('[id="groups"]').bootstrapDualListbox();

					 });
					 </script>
	              </div>
	            </div>

	            <br>
	             <div class="form-group">
	              <div class="col-sm-6"><button class="btn btn-primary" id="submitbutton4" name="submitbutton" type="submit">Save</button></div>
	            </div>
	            <br>

	         </form>

	         <script language="javascript">
 				jQuery(document).ready(function(){
 				   $("#submitbutton4").click(function(e) {
 				   	 e.preventDefault()
					 $.ajax({
						url: '<?php echo public_path(); ?>backend.php/users/updategroup',
						cache: false,
						type: 'POST',
						data : $('#groupform').serialize(),
						success: function(json) {
							$('#groupalertdiv').attr("style", "display: block;");
							$("html, body").animate({ scrollTop: 0 }, "slow");
						}
					});
				  });
			  });
	         </script>
	        </div><!-- Group Settings -->

<!--OTB Start - Multiagency fucntionality-->
	        <div id="agency_settings" style="display: none;">
	        	<h5 class="subtitle"><?php echo __('Agencies') ?></h5>
	        	<form id="agencyform" class="form-horizontal form-bordered">

                <div class="alert alert-success" id="agencyalertdiv" name="alertdiv" style="display: none;">
				  <button type="button" class="close" onClick="document.getElementById('alertdiv').style.display = 'none';" aria-hidden="true">&times;</button>
				  <strong><?php echo __('Well done'); ?></strong> <?php echo __('You successfully updated reviewer'); ?></a>.
				</div>

	            <input type="hidden" name="userid" value="<?php echo $reviewer->getNid(); ?>">

	            <div class="form-group">
	              <label class="col-sm-3"><?php echo __('This reviewer belongs to the following agencies') ?>:</label>
	              <div class="col-sm-6">
	                <select id="agencies" name="agencies[]" multiple class="form-control" required="required">
						<?php
						$q = Doctrine_Query::create()
							 ->from('Agency a')
							 ->orderBy('a.name ASC');
						$agencies = $q->execute();
						foreach($agencies as $agency)
						{
							$selected = "";
							$q = Doctrine_Query::create()
								 ->from('AgencyUser a')
								 ->where('a.user_id = ?',  $reviewer->getNid())
								 ->andWhere('a.agency_id = ?', $agency->getId());
							$useragencies = $q->execute();

							if(sizeof($useragencies) > 0)
							{
								$selected = "selected";
							}

							?>
							<option value='<?php echo $agency->getId(); ?>' <?php echo $selected; ?>><?php echo $agency->getName(); ?></option>
							<?php
						}
						?>
					</select>
					<script language="javascript">
					 jQuery(document).ready(function(){

					 	var demo1 = $('[id="agencies"]').bootstrapDualListbox();

					 });
					 </script>
	              </div>
	            </div>

	            <br>
	             <div class="form-group">
	              <div class="col-sm-6"><button class="btn btn-primary" id="agency_submitbutton" name="agency_submitbutton" type="submit">Save</button></div>
	            </div>
	            <br>

	         </form>

	         <script language="javascript">
 				jQuery(document).ready(function(){
 				   $("#agency_submitbutton").click(function(e) {
 				   	 e.preventDefault()
					 $.ajax({
						url: '<?php echo public_path(); ?>backend.php/users/updateagency',
						cache: false,
						type: 'POST',
						data : $('#agencyform').serialize(),
						success: function(json) {
							$('#agencyalertdiv').attr("style", "display: block;");
							$("html, body").animate({ scrollTop: 0 }, "slow");
						}
					});
				  });
			  });
	         </script>
	        </div><!-- Agency Settings -->
<!--OTB End - Multiagency fucntionality-->

	        <div id="password_settings" style="display: none;">
	        	<h5 class="subtitle"><?php echo __('Password Settings') ?></h5>
                <?php
                if($sf_user->mfHasCredential("manageusers"))
                {
                    ?>
					<!-- OTB Start - Allow administrator to change password from old system -->
					<li><a id="password_settings_btn"  href="/backend.php/passexpired/index/userid/<?php echo $userid; ?>"><?php echo __('Click here to reset password from old system version') ?></a></li><br/>
					<!-- OTB End - Allow administrator to change password from old system -->
                    <!--<form id="changepassword" class="form-bordered form-horizontal" method="post" action="/backend.php/users/changepassword">
                        <input type="hidden" name="user_id" value="<?php echo $reviewer->getNid(); ?>">
                        <div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('New Password'); ?></i></label>

                            <div class="col-sm-8">

                                <input type="password" Name="new_password" id="new_password" class="form-control"  value="" required="required">


                            </div>

                        </div>

                        <div class="form-group"><label class="col-sm-2 control-label" for="text_field"><i class="bold-label"><?php echo __('Confirm New Password'); ?></i></label>

                            <div class="col-sm-8">

                                <input type="password" Name="confirm_password" id="confirm_password" class="form-control"  value="" required="required">


                            </div>

                        </div>

                        <div id="passwordresult" name="passwordresult"></div>

                        <div class="panel-footer" align="center">
                            <button type="submit" class="btn btn-primary" name="submitbuttonname" id="submitbutton"  value="submitbuttonvalue"><?php echo __('Reset Password'); ?></button>
                        </div>

                        <script language="javascript">
                            $('document').ready(function(){
                                $('#new_password').keyup(function(){
                                    if($('#new_password').val() == $('#confirm_password').val() && $('#new_password').val() != "")
                                    {
                                        $('#passwordresult').html('<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Passwords match!</strong></div>');
                                    }
                                    else
                                    {
                                        $('#passwordresult').html('<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Passwords don\'t match!</strong> Try again.</div>');
                                    }
                                });
                                $('#confirm_password').keyup(function(){
                                    if($('#new_password').val() == $('#confirm_password').val() && $('#new_password').val() != "")
                                    {
                                        $('#passwordresult').html('<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Passwords match!</strong></div>');
                                    }
                                    else
                                    {
                                        $('#passwordresult').html('<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Passwords don\'t match!</strong> Try again.</div>');
                                    }
                                });
                            });
                        </script>
                    </form>-->
                    <hr>
                    <div align="center">
                    <?php echo __('OR') ?>
                    </div>
                    <hr>
                    <?php
                }
                ?>
                <div align="center"><button class="btn btn-primary mr20" type="button" onClick="window.location='/backend.php/users/reset/email/<?php echo $reviewer->getStremail(); ?>'">Send Password Reset Email</button></div>
	        </div><!-- Password Settings -->


	        <div id="activity_log" style="display: none;">
	        	<h5 class="subtitle"><?php echo __('Activity Log') ?></h5>

            <div class="panel-body panel-body-nopadding">
                <!-- BASIC WIZARD -->
                <div id="basicWizard" class="basic-wizard">

                   <ul class="nav nav-pills nav-justified">
                      <li><a href="#ptab1" data-toggle="tab"><?php echo __('Log') ?></a></li>
                         <li><a href="#ptab2" data-toggle="tab"><?php echo __('Applications Worked On') ?></a></li>
                            <li><a href="#ptab3" data-toggle="tab"><?php echo __('Tasks Worked On') ?></a></li>
                   </ul>

                   <div class="tab-content tab-content-nopadding">
                     <div class="tab-pane" id="ptab1">

                        <button class="btn btn-primary pull-right" style="margin-top: -15px;" onClick="window.location='/backend.php/users/viewuser/userid/<?php echo $reviewer->getNid(); ?>'">
                        <?php echo __('Reset') ?>
                        </button>
        	            <button class="btn btn-primary pull-right" style="margin-top: -15px; margin-right: 5px;" data-toggle="modal" data-target="#auditModal">
                        <?php echo __('Filter') ?>
                        </button>

        	        	<div class="mb30"></div>

        		         <p><?php echo __('This is a security log of important events involving your account') ?>.</p>
        		            <?php

        		            if($fromdate)
        		            {
        			            $q = Doctrine_Query::create()
              						   ->from("AuditTrail a")
              						   //->where("a.user_id = ?", $_SESSION['SESSION_CUTEFLOW_USERID'])
              						   ->where("a.user_id = ?", $userid)//OTB - get audit trail/activity log using user being viewed not logged in
              						   ->andWhere("a.action_timestamp BETWEEN ? AND ?", array($fromdate." ".$fromtime, $todate." ".$totime))
              						   ->orderBy("a.id DESC");
              		            }
              		            else
              		            {
              			            $q = Doctrine_Query::create()
              						   ->from("AuditTrail a")
              						   //->where("a.user_id = ?", $_SESSION['SESSION_CUTEFLOW_USERID'])
              						   ->where("a.user_id = ?", $userid)//OTB - get audit trail/activity log using user being viewed not logged in 
              						   ->orderBy("a.id DESC");
              					 }

              					 $pager = new sfDoctrinePager('AuditTrail', 10);
              					 $pager->setQuery($q);
              					 $pager->setPage($page);
              					 $pager->init();
        		            ?>
        		            <table class="table table-bordered">
        		            <thead>
        		            <tr><th><?php echo __('Client IP') ?></th>	<th><?php echo __('Activity') ?></th>	<th><?php echo __('Date and time') ?></th> <th><?php echo __('Device') ?></th></tr>
        		            </thead>
        		            <tbody>
        		            <?php foreach($pager->getResults() as $audit){ ?>
        						<tr>
        						<td><?php echo $audit->getIpaddress(); ?></td>
        						<td><?php echo ucfirst($audit->getAction()); ?></td>
        						<td><?php echo $audit->getActionTimestamp(); ?></td>
        		            	<td><?php echo $audit->getHttpAgent(); ?></td>
        						</tr>
        					<?php } ?>
        		            </tbody>
        		            </table>
        		            <?php if ($pager->haveToPaginate()): ?>
        		            <div align="center">
                        	<ul class="pagination pagination-sm mb0 mt0">
        					    <li><a href="/backend.php/users/viewuser/userid/<?php echo $userid; ?>/page/1<?php if($fromdate){ ?>/fromdate/<?php echo $fromdate ?>/todate/<?php echo $todate ?>/fromtime/<?php echo $fromtime ?>/totime/<?php echo $totime ?><?php } ?>">
                                <i class="fa fa-angle-left"></i>
        					    </a></li>

        					   <li> <a href="/backend.php/users/viewuser/userid/<?php echo $userid; ?>/page/<?php echo $pager->getPreviousPage() ?><?php if($fromdate){ ?>/fromdate/<?php echo $fromdate ?>/todate/<?php echo $todate ?>/fromtime/<?php echo $fromtime ?>/totime/<?php echo $totime ?><?php } ?>">
        					    <i class="fa fa-angle-left"></i>
        					    </a></li>

        					    <?php foreach ($pager->getLinks() as $page): ?>
        					      <?php if ($page == $pager->getPage()): ?>
        					         <li class="active"><a href=""><?php echo $page ?></a>
        					      <?php else: ?>
        					        <li><a href="/backend.php/users/viewuser/userid/<?php echo $userid; ?>/page/<?php echo $page ?><?php if($fromdate){ ?>/fromdate/<?php echo $fromdate ?>/todate/<?php echo $todate ?>/fromtime/<?php echo $fromtime ?>/totime/<?php echo $totime ?><?php } ?>"><?php echo $page ?></a></li>
        					      <?php endif; ?>
        					    <?php endforeach; ?>

        					   <li> <a href="/backend.php/users/viewuser/userid/<?php echo $userid; ?>/page/<?php echo $pager->getNextPage() ?><?php if($fromdate){ ?>/fromdate/<?php echo $fromdate ?>/todate/<?php echo $todate ?>/fromtime/<?php echo $fromtime ?>/totime/<?php echo $totime ?><?php } ?>">
        					      <i class="fa fa-angle-right"></i>
        					    </a></li>

        					   <li> <a href="/backend.php/users/viewuser/userid/<?php echo $userid; ?>/page/<?php echo $pager->getLastPage() ?><?php if($fromdate){ ?>/fromdate/<?php echo $fromdate ?>/todate/<?php echo $todate ?>/fromtime/<?php echo $fromtime ?>/totime/<?php echo $totime ?><?php } ?>">
        					      <i class="fa fa-angle-right"></i>
        					    </a></li>
        					  </ul>
        					  </div>
        					  <br>
        					  <br>
        					<?php endif; ?>
                </div>
                <div class="tab-pane" id="ptab2">

                   <button class="btn btn-primary pull-right" style="margin-top: -15px;" onClick="window.location='/backend.php/users/viewuser/userid/<?php echo $reviewer->getNid(); ?>'">
                   <?php echo __('Reset') ?>
                   </button>
                   <button class="btn btn-primary pull-right" style="margin-top: -15px; margin-right: 5px;" data-toggle="modal" data-target="#auditModal">
                   <?php echo __('Filter') ?>
                   </button>

               <div class="mb30"></div>

                <p><?php echo __('This is a security log of applications this reviewer has worked on') ?>.</p>
                   <?php

                   if($fromdate)
                   {
                     $q = Doctrine_Query::create()
                        ->from("AuditTrail a")
                        ->where("a.user_id = ?", $_SESSION['SESSION_CUTEFLOW_USERID'])
                        ->andWhere("a.form_entry_id > ?", 0)
                        ->andWhere("a.action_timestamp BETWEEN ? AND ?", array($fromdate." ".$fromtime, $todate." ".$totime))
                        ->orderBy("a.id DESC");
                   }
                   else
                   {
                     $q = Doctrine_Query::create()
                        ->from("AuditTrail a")
                        ->where("a.user_id = ?", $_SESSION['SESSION_CUTEFLOW_USERID'])
                        ->andWhere("a.form_entry_id > ?", 0)
                        ->orderBy("a.id DESC");
                    }

                    $apppager = new sfDoctrinePager('AuditTrail', 10);
                    $apppager->setQuery($q);
                    $apppager->setPage($apppage);
                    $apppager->init();
                   ?>
                   <table class="table table-bordered">
                   <thead>
                   <tr><th><?php echo __('Client IP') ?></th>	<th><?php echo __('Activity') ?></th>	<th><?php echo __('Date and time') ?></th> <th><?php echo __('Device') ?></th></tr>
                   </thead>
                   <tbody>
                   <?php foreach($apppager->getResults() as $audit){ ?>
               <tr>
               <td><?php echo $audit->getIpaddress(); ?></td>
               <td><?php echo ucfirst($audit->getAction()); ?></td>
               <td><?php echo $audit->getActionTimestamp(); ?></td>
                     <td><?php echo $audit->getHttpAgent(); ?></td>
               </tr>
             <?php } ?>
                   </tbody>
                   </table>
                   <?php if ($apppager->haveToPaginate()): ?>
                   <div align="center">
                     <ul class="pagination pagination-sm mb0 mt0">
                 <li><a href="/backend.php/users/viewuser/userid/<?php echo $userid; ?>/apppage/1<?php if($fromdate){ ?>/fromdate/<?php echo $fromdate ?>/todate/<?php echo $todate ?>/fromtime/<?php echo $fromtime ?>/totime/<?php echo $totime ?><?php } ?>">
                           <i class="fa fa-angle-left"></i>
                 </a></li>

                <li> <a href="/backend.php/users/viewuser/userid/<?php echo $userid; ?>/apppage/<?php echo $apppager->getPreviousPage() ?><?php if($fromdate){ ?>/fromdate/<?php echo $fromdate ?>/todate/<?php echo $todate ?>/fromtime/<?php echo $fromtime ?>/totime/<?php echo $totime ?><?php } ?>">
                 <i class="fa fa-angle-left"></i>
                 </a></li>

                 <?php foreach ($apppager->getLinks() as $page): ?>
                   <?php if ($page == $apppager->getPage()): ?>
                      <li class="active"><a href=""><?php echo $page ?></a>
                   <?php else: ?>
                     <li><a href="/backend.php/users/viewuser/userid/<?php echo $userid; ?>/apppage/<?php echo $page ?><?php if($fromdate){ ?>/fromdate/<?php echo $fromdate ?>/todate/<?php echo $todate ?>/fromtime/<?php echo $fromtime ?>/totime/<?php echo $totime ?><?php } ?>"><?php echo $page ?></a></li>
                   <?php endif; ?>
                 <?php endforeach; ?>

                <li> <a href="/backend.php/users/viewuser/userid/<?php echo $userid; ?>/apppage/<?php echo $apppager->getNextPage() ?><?php if($fromdate){ ?>/fromdate/<?php echo $fromdate ?>/todate/<?php echo $todate ?>/fromtime/<?php echo $fromtime ?>/totime/<?php echo $totime ?><?php } ?>">
                   <i class="fa fa-angle-right"></i>
                 </a></li>

                <li> <a href="/backend.php/users/viewuser/userid/<?php echo $userid; ?>/apppage/<?php echo $apppager->getLastPage() ?><?php if($fromdate){ ?>/fromdate/<?php echo $fromdate ?>/todate/<?php echo $todate ?>/fromtime/<?php echo $fromtime ?>/totime/<?php echo $totime ?><?php } ?>">
                   <i class="fa fa-angle-right"></i>
                 </a></li>
               </ul>
               </div>
               <br>
               <br>
             <?php endif; ?>
                </div>
                <div class="tab-pane" id="ptab3">

                   <button class="btn btn-primary pull-right" style="margin-top: -15px;" onClick="window.location='/backend.php/users/viewuser/userid/<?php echo $reviewer->getNid(); ?>'">
                   <?php echo __('Reset') ?>
                   </button>
                   <button class="btn btn-primary pull-right" style="margin-top: -15px; margin-right: 5px;" data-toggle="modal" data-target="#auditModal">
                   <?php echo __('Filter') ?>
                   </button>

               <div class="mb30"></div>

                <p><?php echo __('This is a security log of tasks this reviewer has worked on') ?>.</p>
                   <?php

                   if($fromdate)
                   {
                     $q = Doctrine_Query::create()
                        ->from("Task a")
                        ->where("a.owner_user_id = ?", $_SESSION['SESSION_CUTEFLOW_USERID'])
                        ->andWhere("a.last_update BETWEEN ? AND ?", array($fromdate." ".$fromtime, $todate." ".$totime))
                        ->orderBy("a.last_update DESC");
                   }
                   else
                   {
                     $q = Doctrine_Query::create()
                        ->from("Task a")
                        ->where("a.owner_user_id = ?", $_SESSION['SESSION_CUTEFLOW_USERID'])
                        ->orderBy("a.last_update DESC");
                    }

                    $taskpager = new sfDoctrinePager('Task', 10);
                    $taskpager->setQuery($q);
                    $taskpager->setPage($taskpage);
                    $taskpager->init();
                   ?>
                   <table class="table table-bordered">
                   <thead>
                   <tr><th><?php echo __('Application') ?></th>	<th><?php echo __('Task') ?></th>	<th><?php echo __('Date and time') ?></th> <th><?php echo __('Assigned By') ?></th></tr>
                   </thead>
                   <tbody>
                   <?php foreach($taskpager->getResults() as $task){ ?>
                   <tr>
                     <?php
                       $q = Doctrine_Query::create()
                          ->from("FormEntry a")
                          ->where("a.id = ?", $task->getApplicationId())
                          ->andWhere("a.approved <> 0");
                       $application = $q->fetchOne();

                       $q = Doctrine_Query::create()
                          ->from("CfUser a")
                          ->where("a.nid = ?", $task->getCreatorUserId());
                       $creator = $q->fetchOne();
                     ?>
                       <td><?php if($application){ ?><a href="/backend.php/applications/view/id/<?php echo $application->getId(); ?>" target="_blank"><?php echo $application->getApplicationId(); ?></a><?php } ?></td>
                       <td><a href="/backend.php/tasks/view/id/<?php echo $task->getId(); ?>" target="_blank"><?php echo $task->getTypeName(); ?></a></td>
                       <td><?php echo $task->getLastUpdate(); ?></td>
                       <td><?php echo $creator->getStrfirstname()." ".$creator->getStrlastname(); ?></td>
                   </tr>
             <?php } ?>
                   </tbody>
                   </table>
                   <?php if ($taskpager->haveToPaginate()): ?>
                   <div align="center">
                     <ul class="pagination pagination-sm mb0 mt0">
                 <li><a href="/backend.php/users/viewuser/userid/<?php echo $userid; ?>/taskpage/1<?php if($fromdate){ ?>/fromdate/<?php echo $fromdate ?>/todate/<?php echo $todate ?>/fromtime/<?php echo $fromtime ?>/totime/<?php echo $totime ?><?php } ?>">
                           <i class="fa fa-angle-left"></i>
                 </a></li>

                <li> <a href="/backend.php/users/viewuser/userid/<?php echo $userid; ?>/taskpage/<?php echo $taskpager->getPreviousPage() ?><?php if($fromdate){ ?>/fromdate/<?php echo $fromdate ?>/todate/<?php echo $todate ?>/fromtime/<?php echo $fromtime ?>/totime/<?php echo $totime ?><?php } ?>">
                 <i class="fa fa-angle-left"></i>
                 </a></li>

                 <?php foreach ($taskpager->getLinks() as $page): ?>
                   <?php if ($page == $taskpager->getPage()): ?>
                      <li class="active"><a href=""><?php echo $page ?></a>
                   <?php else: ?>
                     <li><a href="/backend.php/users/viewuser/userid/<?php echo $userid; ?>/taskpage/<?php echo $page ?><?php if($fromdate){ ?>/fromdate/<?php echo $fromdate ?>/todate/<?php echo $todate ?>/fromtime/<?php echo $fromtime ?>/totime/<?php echo $totime ?><?php } ?>"><?php echo $page ?></a></li>
                   <?php endif; ?>
                 <?php endforeach; ?>

                <li> <a href="/backend.php/users/viewuser/userid/<?php echo $userid; ?>/taskpage/<?php echo $taskpager->getNextPage() ?><?php if($fromdate){ ?>/fromdate/<?php echo $fromdate ?>/todate/<?php echo $todate ?>/fromtime/<?php echo $fromtime ?>/totime/<?php echo $totime ?><?php } ?>">
                   <i class="fa fa-angle-right"></i>
                 </a></li>

                <li> <a href="/backend.php/users/viewuser/userid/<?php echo $userid; ?>/taskpage/<?php echo $taskpager->getLastPage() ?><?php if($fromdate){ ?>/fromdate/<?php echo $fromdate ?>/todate/<?php echo $todate ?>/fromtime/<?php echo $fromtime ?>/totime/<?php echo $totime ?><?php } ?>">
                   <i class="fa fa-angle-right"></i>
                 </a></li>
               </ul>
               </div>
               <br>
               <br>
             <?php endif; ?>
                </div>
              </div>
            </div>
          </div>

		        <div class="mb30"></div>
	        </div><!-- Activity Log -->

	        <div id="sessions" style="display: none;">
	        	<h5 class="subtitle"><?php echo __('Sessions') ?></h5>
	        	<div class="mb30"></div>
		         <p><?php echo __('This is a list of devices that have logged into your account. Revoke any sessions that you do not recognize') ?>.</p>


		            <p><i class="fa fa-circle mr10" style="color:green;"></i> <strong><?php echo __('Active Session') ?></strong></p>

		            <?php
		             $q = Doctrine_Query::create()
      					   ->from("AuditTrail a")
      					   ->where("a.user_id = ?", $_SESSION['SESSION_CUTEFLOW_USERID'])
      					   ->andWhere("a.action_timestamp LIKE ?", "%".date("Y-m-d")."%")
      					   ->andWhere("a.action LIKE ?", "%Logged in%")
      					   ->orderBy("a.id DESC");

      					 $aspager = new sfDoctrinePager('AuditTrail', 10);
      					 $aspager->setQuery($q);
      					 $aspager->setPage($aspage);
      					 $aspager->init();
		            ?>
		            <table class="table table-bordered">
		            <thead>
		            <tr><th>Client IP</th>	<th>Logged in</th>	<th>Device</th> <th>Area</th></tr>
		            </thead>
		            <tbody>
		            <?php foreach($aspager->getResults() as $audit){ ?>
			            <tr>
			            	<td><?php echo $audit->getIpaddress(); ?></td>
			            	<td><?php echo $audit->getActionTimestamp(); ?></td>
			            	<td><?php echo $audit->getHttpAgent(); ?></td>
			            	<td><?php echo $audit->getUserLocation(); ?></td>
			            </tr>
		            <?php } ?>
		            </tbody>

		            </table>
		            <?php if ($aspager->haveToPaginate()): ?>
		            <div align="center">
                	<ul class="pagination pagination-sm mb0 mt0">
					    <li><a href="/backend.php/users/viewuser/userid/<?php echo $userid; ?>/page/1">
                        <i class="fa fa-angle-left"></i>
					    </a></li>

					   <li> <a href="/backend.php/users/viewuser/userid/<?php echo $userid; ?>/page/<?php echo $pager->getPreviousPage() ?>">
					    <i class="fa fa-angle-left"></i>
					    </a></li>

					    <?php foreach ($aspager->getLinks() as $page): ?>
					      <?php if ($page == $aspager->getPage()): ?>
					         <li class="active"><a href=""><?php echo $page ?></a>
					      <?php else: ?>
					        <li><a href="/backend.php/users/viewuser/userid/<?php echo $userid; ?>/page/<?php echo $page ?>"><?php echo $page ?></a></li>
					      <?php endif; ?>
					    <?php endforeach; ?>

					   <li> <a href="/backend.php/users/viewuser/userid/<?php echo $userid; ?>/page/<?php echo $pager->getNextPage() ?>">
					      <i class="fa fa-angle-right"></i>
					    </a></li>

					   <li> <a href="/backend.php/users/viewuser/userid/<?php echo $userid; ?>/page/<?php echo $pager->getLastPage() ?>">
					      <i class="fa fa-angle-right"></i>
					    </a></li>
					  </ul>
					  </div>
					  <br>
					  <br>
					<?php endif; ?>



		            <p><i class="fa fa-circle mr10"></i> <strong><?php echo __('Inactive Sessions') ?></strong></p>

		            <?php
		             $q = Doctrine_Query::create()
      					   ->from("AuditTrail a")
      					   ->where("a.user_id = ?", $_SESSION['SESSION_CUTEFLOW_USERID'])
      					   ->andWhere("a.action_timestamp < ?", date("Y-m-d"))
      					   ->andWhere("a.action LIKE ?", "%Logged in%")
      					   ->orderBy("a.id DESC");

      					 $spager = new sfDoctrinePager('AuditTrail', 10);
      					 $spager->setQuery($q);
      					 $spager->setPage($spage);
      					 $spager->init();
		            ?>
		            <table class="table table-bordered">
		            <thead>
		            <tr><th><?php echo __('Client IP') ?></th>	<th><?php echo __('Logged in') ?></th>	<th><?php echo __('Device') ?></th> <th><?php echo __('Area') ?></th></tr>
		            </thead>
		            <tbody>
		            <?php foreach($spager->getResults() as $audit){ ?>
			            <tr>
			            	<td><?php echo $audit->getIpaddress(); ?></td>
			            	<td><?php echo $audit->getActionTimestamp(); ?></td>
			            	<td><?php echo $audit->getHttpAgent(); ?></td>
			            	<td><?php echo $audit->getUserLocation(); ?></td>
			            </tr>
		            <?php } ?>
		            </tbody>

		            </table>
		            <?php if ($spager->haveToPaginate()): ?>
		            <div align="center">
                	<ul class="pagination pagination-sm mb0 mt0">
					    <li><a href="/backend.php/users/viewuser/userid/<?php echo $userid; ?>/page/1">
                        <i class="fa fa-angle-left"></i>
					    </a></li>

					   <li> <a href="/backend.php/users/viewuser/userid/<?php echo $userid; ?>/page/<?php echo $pager->getPreviousPage() ?>">
					    <i class="fa fa-angle-left"></i>
					    </a></li>

					    <?php foreach ($spager->getLinks() as $page): ?>
					      <?php if ($spage == $spager->getPage()): ?>
					         <li class="active"><a href=""><?php echo $page ?></a>
					      <?php else: ?>
					        <li><a href="/backend.php/users/viewuser/userid/<?php echo $userid; ?>/page/<?php echo $page ?>"><?php echo $page ?></a></li>
					      <?php endif; ?>
					    <?php endforeach; ?>

					   <li> <a href="/backend.php/users/viewuser/userid/<?php echo $userid; ?>/page/<?php echo $pager->getNextPage() ?>">
					      <i class="fa fa-angle-right"></i>
					    </a></li>

					   <li> <a href="/backend.php/users/viewuser/userid/<?php echo $userid; ?>/page/<?php echo $pager->getLastPage() ?>">
					      <i class="fa fa-angle-right"></i>
					    </a></li>
					  </ul>
					  </div>
					  <br>
					  <br>
					<?php endif; ?>



		        <div class="mb30"></div>

	        </div><!-- Sessions -->


	        </div><!-- col-sm-6 -->
	     </div><!-- end-panel-body -->
	     </div> <!-- end-panel -->
	       </div> <!-- end-user-profile -->
	     </div><!-- col-sm-12 -->
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="/backend.php/users/updatepicture" method="post" enctype="multipart/form-data">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo __('Update Picture') ?></h4>
      </div>
      <div class="modal-body">
            <input type="hidden" name="userid" value="<?php echo $reviewer->getNid(); ?>">
	        <div class="fallback">
	          <input name="file" type="file"/>
	        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close') ?></button>
        <button type="submit" class="btn btn-primary"><?php echo __('Save changes') ?></button>
      </div>
	  </form>
    </div><!-- modal-content -->
  </div><!-- modal-dialog -->
</div><!-- modal -->


<!-- Modal -->
<div class="modal fade" id="auditModal" tabindex="-1" role="dialog" aria-labelledby="auditModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="#" method="post" enctype="multipart/form-data">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo __('Filter Activity Log') ?></h4>
      </div>
      <div class="modal-body">
          <label><?php echo __('From') ?></label>
          <div class="input-group">
            <input type="text" id="from-multiple" name="fromdate" placeholder="mm/dd/yyyy" class="form-control">
            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
          </div>
          <div class="input-group mb15">
            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
            <div class="bootstrap-timepicker"><input id="timepicker1" name="fromtime" type="text" class="form-control"/></div>
          </div>
          <label><?php echo __('To') ?></label>
          <div class="input-group">
            <input type="text" id="to-multiple" name="todate" placeholder="mm/dd/yyyy" class="form-control">
            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
          </div>
          <div class="input-group mb15">
            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
            <div class="bootstrap-timepicker"><input id="timepicker2" name="totime" type="text" class="form-control"/></div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close') ?></button>
        <button type="submit" class="btn btn-primary"><?php echo __('Save changes') ?></button>
      </div>
	  </form>
    </div><!-- modal-content -->
  </div><!-- modal-dialog -->
</div><!-- modal -->

<script>
jQuery(document).ready(function(){

  jQuery('#from-multiple').datepicker({
    numberOfMonths: 3,
    showButtonPanel: true
  });

  jQuery('#to-multiple').datepicker({
    numberOfMonths: 3,
    showButtonPanel: true
  });

  // Time Picker
  jQuery('#timepicker1').timepicker({showMeridian: false});
  jQuery('#timepicker2').timepicker({showMeridian: false});


});
</script>
<?php
}
else
{
  include_partial("settings/accessdenied");
}
?>
