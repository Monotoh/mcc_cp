<?php

/**
 * workflow actions.
 *
 * @package    permit
 * @subpackage workflow
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class workflowActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $this->workflows = Doctrine_Core::getTable('workflow')
      ->createQuery('a')
      ->execute();
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new workflowForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $workflow = new Workflow();
	$workflow->setWorkflowTitle($request->getPostParameter('workflow[workflow_title]'));
	$workflow->setWorkflowType($request->getPostParameter('workflow[workflow_type]'));
	$workflow->save();
	
	if($request->getPostParameter('reviewers'))
	{
		foreach($request->getPostParameter('reviewers') as $reviewer)
		{
			$workflowreviewer = new WorkflowReviewers();
			$workflowreviewer->setWorkflowId($workflow->getId());
			$workflowreviewer->setReviewerId($reviewer);
			$workflowreviewer->save();
		}
	}
	
	$this->redirect('/backend.php/workflow/edittask/id/'.$workflow->getId());
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($workflow = Doctrine_Core::getTable('workflow')->find(array($request->getParameter('id'))), sprintf('Object workflow does not exist (%s).', $request->getParameter('id')));
    $this->form = new workflowForm($workflow);
	$this->workflow = $workflow;
  }
  
  public function executeEdittask(sfWebRequest $request)
  {
    $this->forward404Unless($workflow = Doctrine_Core::getTable('workflow')->find(array($request->getParameter('id'))), sprintf('Object workflow does not exist (%s).', $request->getParameter('id')));
    $this->workflow = $workflow;
  }
  
  public function executeUpdatetask(sfWebRequest $request)
  {
    $this->forward404Unless($workflow = Doctrine_Core::getTable('workflow')->find(array($request->getParameter('id'))), sprintf('Object workflow does not exist (%s).', $request->getParameter('id')));
   
	$q = Doctrine_Query::create()
		->from('WorkflowReviewers a')
		->where('a.workflow_id = ? ', $workflow->getId());
	$choosen_reviewers = $q->execute();
	
	foreach($choosen_reviewers as $rev)
	{
		$rev->setTaskType($request->getPostParameter("reviewer_".$rev->getId()));
		$rev->save();
	}
	
	$this->redirect('/backend.php/workflow/index');
	
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($workflow = Doctrine_Core::getTable('workflow')->find(array($request->getParameter('id'))), sprintf('Object workflow does not exist (%s).', $request->getParameter('id')));
    $workflow->setWorkflowTitle($request->getPostParameter('workflow_title'));
	$workflow->setWorkflowType($request->getPostParameter('workflow_type'));
	$workflow->save();
	
	$q = Doctrine_Query::create()
		->from('WorkflowReviewers a')
		->where('a.workflow_id = ? ', $workflow->getId());
	$choosen_reviewers = $q->execute();
	
	foreach($choosen_reviewers as $rev)
	{
		$rev->delete();
	}
	
	if($request->getPostParameter('reviewers'))
	{
		foreach($request->getPostParameter('reviewers') as $reviewer)
		{
			$workflowreviewer = new WorkflowReviewers();
			$workflowreviewer->setWorkflowId($workflow->getId());
			$workflowreviewer->setReviewerId($reviewer);
			$workflowreviewer->save();
		}
	}
	
	$this->redirect('/backend.php/workflow/edittask/id/'.$workflow->getId());
  }

  public function executeDelete(sfWebRequest $request)
  {

    $this->forward404Unless($workflow = Doctrine_Core::getTable('workflow')->find(array($request->getParameter('id'))), sprintf('Object workflow does not exist (%s).', $request->getParameter('id')));
    $workflow->delete();

    $this->redirect('/backend.php/workflow/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $workflow = $form->save();

      $this->redirect('workflow/edit?id='.$workflow->getId());
    }
  }
}
