<form action="/backend.php<?php echo url_for('workflow/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>  autocomplete="off" data-ajax="false">
<fieldset>
<label><?php echo ($form->getObject()->isNew()?'New Workflow':'Edit Workflow'); ?></label>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
<?php echo $form->renderHiddenFields(false) ?>
<?php echo $form->renderGlobalErrors() ?>
      <section>
        <label>Workflow Title</label>
        <div>
          <?php echo $form['workflow_title']->renderError() ?>
          <?php echo $form['workflow_title'] ?>
        </div>
      </section>
      <section>
        <label>Workflow Type</label>
        <div>
          <?php echo $form['workflow_type']->renderError() ?>
          <?php echo $form['workflow_type'] ?>
        </div>
      </section>
      <section>
        <label>Reviewers</label>
        <div>
          <?php echo $form['workflow_type']->renderError() ?>
          <select name="reviewers" id="reviewers" multiple>
							<?php
								$q = Doctrine_Query::create()
									->from('CfUser a')
									->where('a.nid = ?', $_SESSION["SESSION_CUTEFLOW_USERID"]);
								$logged_in_reviewer = $q->fetchOne();
							
								//Add reviewers from same department to the list
								$q = Doctrine_Query::create()
									->from('CfUser a');
								$reviewers = $q->execute();
								foreach($reviewers as $reviewer)
								{
								    $selected = "";
									
									if(!$form->getObject()->isNew())
									{
										$q = Doctrine_Query::create()
											->from('WorkflowReviewers a')
											->where('a.workflow_id = ? AND a.reviewer_id = ?', array($form->getObject()->getId(),$reviewer->getNid()));
										$choosen_reviewers = $q->execute();
										foreach($choosen_reviewers as $choosen_reviewer)
										{
											$selected = 'selected';
										}
									}
									
									if($reviewer->getStrdepartment() == $logged_in_reviewer->getStrdepartment())
									{
										if($reviewer->getNid() == $logged_in_reviewer->getNid())
										{
											echo "<option value='".$reviewer->getNid()."' ".$selected.">**".$reviewer->getStrfirstname()." ".$reviewer->getStrlastname()." (".$reviewer->getStrdepartment().")</option>";
										}
										else
										{
											echo "<option value='".$reviewer->getNid()."' ".$selected.">".$reviewer->getStrfirstname()." ".$reviewer->getStrlastname()." (".$reviewer->getStrdepartment().")</option>";
										}
									}
								}
								
								//Get HODs from other departments and assign them to the list as well.
								$q = Doctrine_Query::create()
									->from('MfGuardGroup a')
									->where('a.id = ?', 4);
								$departmenthead_group = $q->fetchOne();
								$heads_ofdepts = $departmenthead_group->getUsers();
								foreach($heads_ofdepts as $reviewer)
								{
									if($reviewer->getStrdepartment() != $logged_in_reviewer->getStrdepartment())
									{
										echo "<option value='".$reviewer->getNid()."'>".$reviewer->getStrfirstname()." ".$reviewer->getStrlastname()." (HOD of ".$reviewer->getStrdepartment().")</option>";
									}
								}
							?>
							</select>
        </div>
      </section>
	  
	  <section>
			<div><button class="reset">Reset</button><button class="submit" name="submitbuttonname" value="submitbuttonvalue">Next</button></div>
	  </section>
	  
    </fieldset>
</form>
