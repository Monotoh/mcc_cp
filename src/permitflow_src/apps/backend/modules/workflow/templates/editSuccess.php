<div style="float: left;">
<ul class="breadcrumb">
	<li><a href="#" onClick="window.location='<?php echo public_path(); ?>backend.php/workflow/index';">Manage Workflow</a></li>
	<li><a href="#" onClick="window.location='<?php echo public_path(); ?>backend.php/workflow/index';">PreConfigured Workflows</a></li>
	<li><a href="#">Edit <?php echo $workflow->getWorkflowTitle(); ?></a></li>
</ul>
</div>
<div style="float: right;">
<button onClick="window.location='<?php echo public_path(); ?>backend.php/workflow/index';">Back To Workflow</button>
</div>


<div class="g12">

<?php include_partial('form', array('form' => $form)) ?>
</div>