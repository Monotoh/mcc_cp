<div style="float: left;">
<ul class="breadcrumb">
	<li><a href="#" onClick="window.location='<?php echo public_path(); ?>backend.php/workflow/index';">Manage Workflow</a></li>
	<li><a href="#" onClick="window.location='<?php echo public_path(); ?>backend.php/workflow/index';">PreConfigured Workflows</a></li>
	<li><a href="#">Edit <?php echo $workflow->getWorkflowTitle(); ?></a></li>
</ul>
</div>
<div style="float: right;">
<button onClick="window.location='<?php echo public_path(); ?>backend.php/workflow/index';">Back To Workflow</button>
</div>

<div class="g12">
<form action="/backend.php/workflow/updatetask/id/<?php echo $workflow->getId(); ?>" method="post" enctype="multipart/form-data"  autocomplete="off" data-ajax="false">
<fieldset>
<label>Edit Reviewers Tasks</label>
<?php
	$q = Doctrine_Query::create()
		->from('WorkflowReviewers a')
		->where('a.workflow_id = ? ', $workflow->getId());
	$choosen_reviewers = $q->execute();
	foreach($choosen_reviewers as $reviewer)
	{
?>
      <section>
        <label><?php 
			$q = Doctrine_Query::create()
				->from('CfUser a')
				->where('a.nid = ? ', $reviewer->getReviewerId());
			$cfuser = $q->fetchOne();
			if($cfuser)
			{
				echo $cfuser->getStrfirstname()." ".$cfuser->getStrlastname();
			}
		?></label>
        <div>
          <select name='reviewer_<?php echo $reviewer->getId(); ?>' id="reviewer_<?php echo $reviewer->getId(); ?>">
			<option value="2">Commenting</option>
			<option value="6">Inspection</option>
			<option value="3">Invoicing</option>
			<option value="4">Scanning</option>
			<option value="5">Collection</option>
			<option value="1">Review</option>
		  </select>
        </div>
      </section>
<?php
	}
?>
	  <section>
			<div><button class="reset">Reset</button><button class="submit" name="submitbuttonname" value="submitbuttonvalue">Save</button></div>
	  </section>
	  
    </fieldset>
</form>

</div>
