<div style="float: left;">
<ul class="breadcrumb">
	<li><a href="#" onClick="window.location='<?php echo public_path(); ?>backend.php/settings/index';">Settings</a></li>
	<li><a href="#" onClick="window.location='<?php echo public_path(); ?>backend.php/workflow/index';">Manage Workflow</a></li>
	<li><a href="#" onClick="window.location='<?php echo public_path(); ?>backend.php/workflow/index';">PreConfigured Workflows</a></li>
</ul>
</div>
<div style="float: right;">
<button onClick="window.location='<?php echo public_path(); ?>backend.php/workflow/new';">New Workflow</button>
</div>

<div class="g12">
			<h1>PreConfigured Workflows</h1>
			<br>
			
			<div id='notifications' name='notifications'>
			</div>

<table class="datatable">
    <thead>
   <tr>
      <th style="width: 10px;"><input type='checkbox' name='batchall' onclick="boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.name == 'batch') { if(this.checked == true){ box.checked = false; }else{ box.checked = true; } } } "></th>
      <th width="60">ID</th>
      <th>Workflow Title</th>
      <th>Workflow Type</th>
      <th style="background: none;">Actions</th>
    </tr>
  </thead>
  <tbody>
    <?php
		$count = 1;
	?>
    <?php foreach ($workflows as $workflow): ?>
    <tr id="row_<?php echo $workflow->getId() ?>">
	  <td><input type='checkbox' name='batch' id='batch_<?php echo $workflow->getId() ?>' value='<?php echo $workflow->getId() ?>'></td>
     <td><?php echo $count++; ?></td>
      <td><?php echo $workflow->getWorkflowTitle() ?></td>
      <td><?php
	  if($workflow->getWorkflowType() == "0")
	  {
	      echo "Concurrent";
	  }
	  else
	  {
	      echo "Sequential";
	  }
	  ?></td>
      <td>
		<a href="/backend.php/workflow/edit/id/<?php echo $workflow->getId(); ?>" title="Edit"><img src="/assets_backend/images/icons/dark/pencil.png" border="0" alt="Edit" title="Edit"></a>
		
      	<a href="#" onclick="if(confirm('Are you sure you want to delete this group?')){ ajaxupdate('/backend.php/workflow/delete/id/<?php echo $workflow->getId(); ?>', 'id', '<?php echo $workflow->getId(); ?>'); }else{ return false; }"><img src="/assets_backend/images/icons/dark/cross.png" border="0" alt="Delete" title="Delete"></a>
					
      </td>
    </tr>
    <?php endforeach; ?>
     </tbody>
	<tfoot>
   <tr><td colspan='7' style='text-align: left;'>
   <select id='batch_action' name='batch_action' onChange="if(this.value != ''){if(confirm('Are you sure?')){ batch('groups', this.options[this.selectedIndex].text, this.value); document.getElementById('default').selected='selected'; }}">
   <option id='default' value=''>Choose an action..</option>
   <option value='delete'>Set As Deleted</option>
   </select>
   </td></tr>
   </tfoot>
</table>
</div>
