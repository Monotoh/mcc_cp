<?php
/**
 * Layout-edit template.
 *
 * Edit Template (Without Ajax File Attacher)
 *
 *
 * @package    Backend
 * @theme      WhiteLabel
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
include_component('dashboard', 'checksession');
?>

<!doctype html>
<html lang="en-us">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<meta name="description" content="eBPMIS - Rwanda Online Permit Application System">
	<meta name="author" content="eBPMIS">
	<title><?php echo sfConfig::get('app_organisation_name'); ?> - Rwanda Online Permit Application System</title>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

	<!-- Apple iOS and Android stuff -->
	<meta name="apple-mobile-web-app-capable" content="no">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">

	<!-- Apple iOS and Android stuff - don't remove! -->
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">

	<?php

        //Displays all required stylesheets
        include_component('dashboard', 'stylesheets');

        //Displays all required javascripts
        include_component('dashboard', 'javascripts');
        ?>
</head>
<body style="margin-left: 0px; margin-right: 0px;">
    <section id="content" style="background-image: none; width: 100%;margin-left: 0px; margin-right: 0px; padding: 0px;">
            <?php echo $sf_content ?>
    </section>
</body>
</html>
