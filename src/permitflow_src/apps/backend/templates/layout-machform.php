<?php
/**
 * Layout template.
 *
 * Main Backend Template
 *
 *
 * @package    Backend
 * @theme      Bracket Response Bootstrap 3 Admin Template
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
include_component('dashboard', 'checksession');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="eBPMIS - Rwanda Online Permit Application System">
  <meta name="author" content="eBPMIS">
  <title><?php echo sfConfig::get('app_organisation_name'); ?> - Rwanda Online Permit Application System </title>

  <link rel="shortcut icon" href="<?php echo public_path(); ?>assets_unified/images/favicon.png" type="image/png">
  <?php
	//Displays all required stylesheets
	include_component('dashboard', 'stylesheets');

	//Displays all required javascripts
	include_component('dashboard', 'javascripts');
  ?>
</head>
<body id="body">

    <!-- Preloader -->
    <div id="preloader">
        <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
    </div>

    <section>
			<?php
            //Displays the header
            include_component('dashboard', 'header');
            ?>
            <?php echo $sf_content ?>
        </div><!-- mainpanel -->

    <?php
	include_component('dashboard','rightbar');
	?>

	</section>
</body>
</html>
