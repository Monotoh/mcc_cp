<?php
/**
 * Layout template.
 *
 * Main Backend Template
 *
 *
 * @package    Backend
 * @theme      Bracket Response Bootstrap 3 Admin Template
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
include_component('dashboard', 'checksession');
$wizard_manager = new WizardManager();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="Maseru City - Construction Permit">
    <meta name="author" content="MCC">
    <title><?php echo sfConfig::get('app_organisation_name'); ?></title>

    <link rel="shortcut icon" href="<?php echo public_path(); ?>assets_unified/images/favicon.png" type="image/png">

    <?php
    //Displays all required stylesheets
    include_component('dashboard', 'stylesheets');

    //Displays all required javascripts
    include_component('dashboard', 'javascripts');

    if($wizard_manager->resume_step() == 6 || $sf_user->getAttribute('post_resume') == 4){ ?>
        <link rel="stylesheet" href="/assets_unified/jsPlumb/css/jsplumb.css">
        <link rel="stylesheet" href="/assets_unified/jsPlumb/demo/statemachine/demo.css">
    <?php } ?>
</head>
<body id="body">

<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>
    <?php
    //Displays the header
    include_component('dashboard', 'settingsheader');
    ?>

            <?php echo $sf_content ?>
    <?php
    include_component('dashboard','rightbar');
    ?>

</section>


<?php
$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

$sql = "SELECT * FROM ext_locales ORDER BY local_title ASC";
$rows = mysql_query($sql, $dbconn);
if(mysql_num_rows($rows) > 1)
{
    ?>
    <div class="btn-group pull-right mt20 mr10">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
            <?php echo __('Languages'); ?>
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
            <?php
            while($row = mysql_fetch_assoc($rows))
            {
                $selected = "";
                if($row['locale_identifier'] == $sf_user->getCulture())
                {
                    $selected = "active";
                }
                ?>
                <li class="<?php echo $selected; ?>"><a href="<?php echo public_path(); ?>backend.php/languages/setlocale/code/<?php echo $row['locale_identifier']; ?>"><i class="glyphicon glyphicon-cog"></i> <?php echo $row['local_title']; ?></a></li>
            <?php
            }
            ?>
        </ul>
    </div>
<?php
}

if($wizard_manager->resume_step() == 6 || $sf_user->getAttribute('post_resume') == 4){ ?>

    <!-- JS -->
    <!-- support lib for bezier stuff -->
    <script src="/assets_unified/jsPlumb/lib/jsBezier-0.6.js"></script>
    <!-- event adapter -->
    <script src="/assets_unified/jsPlumb/lib/mottle-0.4.js"></script>
    <!-- geometry functions -->
    <script src="/assets_unified/jsPlumb/lib/biltong-0.2.js"></script>
    <!-- drag -->
    <script src="/assets_unified/jsPlumb/lib/katavorio-0.4.js"></script>
    <!-- jsplumb util -->
    <script src="/assets_unified/jsPlumb/src/util.js"></script>
    <script src="/assets_unified/jsPlumb/src/browser-util.js"></script>
    <!-- base DOM adapter -->
    <script src="/assets_unified/jsPlumb/src/dom-adapter.js"></script>
    <!-- main jsplumb engine -->
    <script src="/assets_unified/jsPlumb/src/jsPlumb.js"></script>
    <!-- endpoint -->
    <script src="/assets_unified/jsPlumb/src/endpoint.js"></script>
    <!-- connection -->
    <script src="/assets_unified/jsPlumb/src/connection.js"></script>
    <!-- anchors -->
    <script src="/assets_unified/jsPlumb/src/anchors.js"></script>
    <!-- connectors, endpoint and overlays  -->
    <script src="/assets_unified/jsPlumb/src/defaults.js"></script>
    <!-- bezier connectors -->
    <script src="/assets_unified/jsPlumb/src/connectors-bezier.js"></script>
    <!-- state machine connectors -->
    <script src="/assets_unified/jsPlumb/src/connectors-statemachine.js"></script>
    <!-- flowchart connectors -->
    <script src="/assets_unified/jsPlumb/src/connectors-flowchart.js"></script>
    <script src="/assets_unified/jsPlumb/src/connector-editors.js"></script>
    <!-- SVG renderer -->
    <script src="/assets_unified/jsPlumb/src/renderers-svg.js"></script>


    <!-- vml renderer -->
    <script src="/assets_unified/jsPlumb/src/renderers-vml.js"></script>

    <!-- no library jsPlumb adapter -->
    <script src="/assets_unified/jsPlumb/src/base-library-adapter.js"></script>
    <script src="/assets_unified/jsPlumb/src/dom.jsPlumb.js"></script>
    <!-- /JS -->

    <!--  demo code -->
    <script src="/assets_unified/jsPlumb/demo/statemachine/demo.js"></script>

    <!-- demo list -->
    <script src="/assets_unified/jsPlumb/demo/demo-list.js"></script>

    <!--Google Analytics-->
    <script type="text/javascript" src="<?php echo public_path(); ?>asset_unified/js/analyticstracking.js"></script>


<?php }
//if(sfConfig::get('app_google_analytics_id')) {
    ?>
    <!--<script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', '<?php //echo sfConfig::get('app_google_analytics_id'); ?>', 'auto');
        ga('send', 'pageview');
    </script>-->
<?php
//}
?>
</body>
</html>
