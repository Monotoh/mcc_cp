<?php
/**
 * Layout template.
 *
 * Main Backend Template
 *
 *
 * @package    Backend
 * @theme      Bracket Response Bootstrap 3 Admin Template
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
include_component('dashboard', 'checksession');

//If this is the first run after installation then collapse left panel
$wizard_manager = new WizardManager();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="Maseru City - Construction Permit">
  <meta name="author" content="MCC">
  <title><?php echo sfConfig::get('app_organisation_name'); ?>(CP)</title>

  <link rel="shortcut icon" href="<?php echo public_path(); ?>assets_unified/images/favicon.png" type="image/png">
  <!-- google analytics -->
  <!--<script type="text/javascript" src="<?php //echo public_path(); ?>asset_unified/js/analyticstracking.js"></script>-->

  <?php
	//Displays all required stylesheets
	include_component('dashboard', 'stylesheets');
        

	//Displays all required javascripts
	include_component('dashboard', 'javascripts');

  if($wizard_manager->resume_step() == 6){ ?>
      <link rel="stylesheet" href="/assets_unified/jsPlumb/css/jsplumb.css">
      <link rel="stylesheet" href="/assets_unified/jsPlumb/demo/statemachine/demo.css">
  <?php } ?>
   <!-- Include extra styles Improved css -->
   <!-- Style for Inbox -->
    <link href="<?php echo public_path(); ?>otb_assets/assets/admin/pages/css/inbox.css" rel="stylesheet">
  <?php
  if(sfConfig::get('app_zopim_key'))
  {
  ?>
      
  <!--Start of Zopim Live Chat Script-->
  <script type="text/javascript">
  window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
  d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
  _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
  $.src="//v2.zopim.com/?<?php echo sfConfig::get('app_zopim_key'); ?>";z.t=+new Date;$.
  type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
  </script>
  <!--End of Zopim Live Chat Script-->
  <?php
  }
  ?>
</head>
<body id="body" <?php if($wizard_manager->is_first_run()){ ?>class="leftpanel-collapsed"<?php } ?>>

    <!-- Preloader
    <div id="preloader">
        <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
    </div>
    -->

    <section>
			<?php
            //Displays the header
            include_component('dashboard', 'header');
            ?>
            <?php echo $sf_content ?>
        </div><!-- mainpanel -->

    <?php
	include_component('dashboard','rightbar');
	?>

	</section>


                  <?php
                    $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
                    mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

                    $sql = "SELECT * FROM ext_locales ORDER BY local_title ASC";
                    $rows = mysql_query($sql, $dbconn);
                    if(mysql_num_rows($rows) > 1)
                    {
                     ?>
                    <div class="btn-group pull-right mt20 mr10">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <?php echo __('Languages'); ?>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                      <?php
                        while($row = mysql_fetch_assoc($rows))
                        {
                          $selected = "";
                          if($row['locale_identifier'] == $sf_user->getCulture())
                          {
                            $selected = "active";
                          }
                          ?>
                          <li class="<?php echo $selected; ?>"><a href="<?php echo public_path(); ?>backend.php/languages/setlocale/code/<?php echo $row['locale_identifier']; ?>"><i class="glyphicon glyphicon-cog"></i> <?php echo $row['local_title']; ?></a></li>
                          <?php
                        }
                      ?>
                    </ul>
                   </div>
                   <?php
                    }
                    ?>
<?php
// if(sfConfig::get('app_google_analytics_id')) {
    ?>
    <!--<script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', '<?php //echo sfConfig::get('app_google_analytics_id'); ?>', 'auto');
        ga('send', 'pageview');
    </script>-->
<?php
// }

if($wizard_manager->resume_step() == 6){ ?>

    <!-- JS -->
    <!-- support lib for bezier stuff -->
    <script src="/assets_unified/jsPlumb/lib/jsBezier-0.6.js"></script>
    <!-- event adapter -->
    <script src="/assets_unified/jsPlumb/lib/mottle-0.4.js"></script>
    <!-- geometry functions -->
    <script src="/assets_unified/jsPlumb/lib/biltong-0.2.js"></script>
    <!-- drag -->
    <script src="/assets_unified/jsPlumb/lib/katavorio-0.4.js"></script>
    <!-- jsplumb util -->
    <script src="/assets_unified/jsPlumb/src/util.js"></script>
    <script src="/assets_unified/jsPlumb/src/browser-util.js"></script>
    <!-- base DOM adapter -->
    <script src="/assets_unified/jsPlumb/src/dom-adapter.js"></script>
    <!-- main jsplumb engine -->
    <script src="/assets_unified/jsPlumb/src/jsPlumb.js"></script>
    <!-- endpoint -->
    <script src="/assets_unified/jsPlumb/src/endpoint.js"></script>
    <!-- connection -->
    <script src="/assets_unified/jsPlumb/src/connection.js"></script>
    <!-- anchors -->
    <script src="/assets_unified/jsPlumb/src/anchors.js"></script>
    <!-- connectors, endpoint and overlays  -->
    <script src="/assets_unified/jsPlumb/src/defaults.js"></script>
    <!-- bezier connectors -->
    <script src="/assets_unified/jsPlumb/src/connectors-bezier.js"></script>
    <!-- state machine connectors -->
    <script src="/assets_unified/jsPlumb/src/connectors-statemachine.js"></script>
    <!-- flowchart connectors -->
    <script src="/assets_unified/jsPlumb/src/connectors-flowchart.js"></script>
    <script src="/assets_unified/jsPlumb/src/connector-editors.js"></script>
    <!-- SVG renderer -->
    <script src="/assets_unified/jsPlumb/src/renderers-svg.js"></script>


    <!-- vml renderer -->
    <script src="/assets_unified/jsPlumb/src/renderers-vml.js"></script>

    <!-- no library jsPlumb adapter -->
    <script src="/assets_unified/jsPlumb/src/base-library-adapter.js"></script>
    <script src="/assets_unified/jsPlumb/src/dom.jsPlumb.js"></script>
    <!-- /JS -->

    <!--  demo code -->
    <script src="/assets_unified/jsPlumb/demo/statemachine/demo.js"></script>

    <!-- demo list -->
    <script src="/assets_unified/jsPlumb/demo/demo-list.js"></script>


<?php }
    if($wizard_manager->is_first_run())
    {
?>


    <script type="text/javascript" src="<?php echo public_path(); ?>assets_unified/js/jquery.bootstrap-duallistbox.js"></script>
    <script>
        function checkgroup(group)
        {
            var xmlhttp;
            if (window.XMLHttpRequest)
            {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp=new XMLHttpRequest();
            }
            else
            {// code for IE6, IE5
                xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange=function()
            {
                if (xmlhttp.readyState==4 && xmlhttp.status==200)
                {
                    if(xmlhttp.responseText == "fail")
                    {
                        alert("The name you entered already exists");
                        document.getElementById("submitgroups").disabled = true;
                    }
                    else
                    {
                        document.getElementById("submitgroups").disabled = false;
                    }
                }
            }
            xmlhttp.open("POST","/backend.php/groups/checknamemin",true);
            xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
            xmlhttp.send("name=" + group);
        }

        function checkuser(username)
        {
            var xmlhttp;
            if (window.XMLHttpRequest)
            {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp=new XMLHttpRequest();
            }
            else
            {// code for IE6, IE5
                xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange=function()
            {
                if (xmlhttp.readyState==4 && xmlhttp.status==200)
                {
                    if(xmlhttp.responseText == "fail")
                    {
                        alert("The username you entered already exists");
                        document.getElementById("submitusers").disabled = true;
                    }
                    else
                    {
                        document.getElementById("submitusers").disabled = false;
                    }
                }
            }
            xmlhttp.open("POST","/backend.php/users/checkusermin",true);
            xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
            xmlhttp.send("username=" + username);
        }

        function checkemail(email)
        {
            var xmlhttp;
            if (window.XMLHttpRequest)
            {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp=new XMLHttpRequest();
            }
            else
            {// code for IE6, IE5
                xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange=function()
            {
                if (xmlhttp.readyState==4 && xmlhttp.status==200)
                {
                    if(xmlhttp.responseText == "fail")
                    {
                        alert("The email you entered already exists");
                        document.getElementById("submitusers").disabled = true;
                    }
                    else
                    {
                        document.getElementById("submitusers").disabled = false;
                    }
                }
            }
            xmlhttp.open("POST","/backend.php/users/checkemailmin",true);
            xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
            xmlhttp.send("email=" + email);
        }

        jQuery(document).ready(function(){

            // Basic Wizard
            jQuery('#basicWizard').bootstrapWizard();

            // Progress Wizard
            $('#progressWizard').bootstrapWizard({
                'nextSelector': '.next',
                'previousSelector': '.previous',
                onNext: function(tab, navigation, index) {
                    var $total = navigation.find('li').length;
                    var $current = index+1;
                    var $percent = ($current/$total) * 100;
                    jQuery('#progressWizard').find('.progress-bar').css('width', $percent+'%');
                },
                onPrevious: function(tab, navigation, index) {
                    var $total = navigation.find('li').length;
                    var $current = index+1;
                    var $percent = ($current/$total) * 100;
                    jQuery('#progressWizard').find('.progress-bar').css('width', $percent+'%');
                },
                onTabShow: function(tab, navigation, index) {
                    var $total = navigation.find('li').length;
                    var $current = index+1;
                    var $percent = ($current/$total) * 100;
                    jQuery('#progressWizard').find('.progress-bar').css('width', $percent+'%');
                }
            });

            // Disabled Tab Click Wizard
            jQuery('#disabledTabWizard').bootstrapWizard({
                tabClass: 'nav nav-pills nav-justified nav-disabled-click',
                onTabClick: function(tab, navigation, index) {
                    return false;
                }
            });

            // With Form Validation Wizard
            var $validator = jQuery("#firstForm").validate({
                highlight: function(element) {
                    jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                success: function(element) {
                    jQuery(element).closest('.form-group').removeClass('has-error');
                }
            });

            jQuery('#validationWizard').bootstrapWizard({
                tabClass: 'nav nav-pills nav-justified nav-disabled-click',
                onTabClick: function(tab, navigation, index) {
                    return false;
                },
                onNext: function(tab, navigation, index) {
                    var $valid = jQuery('#firstForm').valid();
                    if(!$valid) {

                        $validator.focusInvalid();
                        return false;
                    }
                }
            });


        });
    </script>

<?php } ?>
</body>
</html>
