<?php
/**
 * Layout template.
 *
 * Main Backend Template
 *
 *
 * @package    Backend
 * @theme      Bracket Response Bootstrap 3 Admin Template
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
include_component('dashboard', 'checksession');

//If this is the first run after installation then collapse left panel
$wizard_manager = new WizardManager();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="Maseru City - Construction Permit">
  <meta name="author" content="eBPMIS">
  <title>Maseru City - Construction Permit</title>

  <link rel="shortcut icon" href="<?php echo public_path(); ?>assets_unified/images/favicon.png" type="image/png">
  <?php
    //Displays all required stylesheets
    include_component('dashboard', 'stylesheets');

    //Displays all required javascripts
    include_component('dashboard', 'javascripts');
  
  if(sfConfig::get('app_zopim_key'))
  {
  ?>
  <!--Start of Zopim Live Chat Script-->
  <script type="text/javascript">
  window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
  d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
  _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
  $.src="//v2.zopim.com/?<?php echo sfConfig::get('app_zopim_key'); ?>";z.t=+new Date;$.
  type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
  </script>
  <!--End of Zopim Live Chat Script-->
  <?php
  }
  ?>
</head>
<body id="body">
  <?php echo $sf_content ?>
</body>
</html>
