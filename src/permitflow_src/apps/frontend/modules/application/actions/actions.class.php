<?php
/**
 * Application actions.
 *
 * Displays applications submitted by the client
 *
 * @package    frontend
 * @subpackage application
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

class applicationActions extends sfActions
{
       /**
	 * Executes 'Index' action
	 *
	 * Displays list of all of the currently logged in client's applications
	 *
	 * @param sfRequest $request A request object
	 */
        public function executeIndex(sfWebRequest $request)
        {
                $this->page = $request->getParameter('page', 1);
				$this->setLayout("layoutdash");

				if($request->getParameter("subgroup"))
				{
					$q = Doctrine_Query::create()
						->from('SubMenus a')
						->where("a.id = ?", $request->getParameter("subgroup"));
					$submenu = $q->fetchOne();
					$_SESSION['group'] = $submenu->getMenuId();
					$_SESSION['subgroup'] = $request->getParameter("subgroup");

                    $this->stage = $submenu->getId();

					if($request->getParameter("form"))
					{
						$q = Doctrine_Query::create()
						   ->from("FormEntry a")
						   ->where("a.user_id = ?", $this->getUser()->getGuardUser()->getId())
						   ->andWhere("a.form_id = ?", $request->getParameter("form"))
						   ->andWhere("a.approved = ?", $request->getParameter("subgroup"))
						   ->andWhere('a.parent_submission = 0')
						   ->orderBy("a.application_id DESC");
					}
					else
					{
						$q = Doctrine_Query::create()
						   ->from("FormEntry a")
						   ->where("a.user_id = ?", $this->getUser()->getGuardUser()->getId())
						   ->andWhere("a.approved = ?", $request->getParameter("subgroup"))
						   ->andWhere('a.parent_submission = 0')
						   ->orderBy("a.application_id DESC");
					}
				}
				else
				{
					if($request->getParameter("form"))
					{
						$q = Doctrine_Query::create()
						   ->from("FormEntry a")
						   ->where("a.user_id = ?", $this->getUser()->getGuardUser()->getId())
						   ->andWhere("a.form_id = ?", $request->getParameter("form"))
						   ->andWhere('a.parent_submission = 0')
						   ->orderBy("a.application_id DESC");

					}
					else
					{
						if($request->getParameter("drafts"))
						{
							$q = Doctrine_Query::create()
							   ->from("FormEntry a")
							   ->where("a.user_id = ?", $this->getUser()->getGuardUser()->getId())
							   ->andWhere('a.approved = 0')
							   ->andWhere('a.parent_submission = 0')
							   ->orderBy("a.application_id DESC");

							$_SESSION['group'] = 0;
							$_SESSION['subgroup'] =0;
						}
						else
						{
							$q = Doctrine_Query::create()
							   ->from("FormEntry a")
							   ->where("a.user_id = ?", $this->getUser()->getGuardUser()->getId())
							   ->andWhere('a.parent_submission = 0')
							   ->orderBy("a.application_id DESC");

							$_SESSION['group'] = 0;
							$_SESSION['subgroup'] =0;
						}
					}
				}

            $this->pager = new sfDoctrinePager('FormEntry', 10);
            $this->pager->setQuery($q);
            $this->pager->setPage($request->getParameter('page', 1));
            $this->pager->init();
        }

    /**
     * Executes 'Canceltransfer' action
     *
     * Cancel Change of Ownership of an Application
     *
     * @param sfRequest $request A request object
     */
    public function executeCanceltransfer(sfWebRequest $request)
    {
        $data = $request->getParameter('code');
        $data = json_decode(base64_decode($data), true);

        $application_id = $data['id'];

        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.id = ?', $application_id);
        $this->application = $q->fetchOne();

        $this->application->setCirculationId("");
        $this->application->save();

        $this->getUser()->setFlash('notice', 'The request for transfer of ownership has been cancelled');
        return $this->redirect("/index.php/dashboard");
    }















	public function executeUnhold(sfWebRequest $request){
		
		$this->page = $request->getParameter('page', 1);
				$this->setLayout("layoutdash");
				$conn = new mysqli("localhost", "admin", "password", "mcc_v2");
		
				if ($conn->connect_error) {
					die("Connection failed: " . $conn->connect_error);
					}

	//get the stage the was held at....................................................................................
	$sql= "select stage_id from application_reference a where a.application_id = '".$request->getParameter("application_id")."' order by a.id desc limit 1,1 ";

	/*$res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($sql);	*/
	
	$result = $conn->query($sql);
	$thestageid = null ;	
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				$thestageid= $row["stage_id"];
				}
			} 
		$conn->close();
		if($res){

			foreach($sql as $r){
				$thestageid= $r["stage_id"];
			}
		}
		$application_manager = new ApplicationManager();
		//echo "please:   ".$thestageid;
		$q = Doctrine_Query::create()
		->from("FormEntry a")
		->where("a.id =?", $request->getParameter("application_id"));
			$submission = $q->fetchOne();

		$submission = $application_manager->application_onhold($submission->getId(),$thestageid);

		return $this->redirect("/index.php/application/index");

			}

		//CBS................................................................................
		/*public function executeUnhold(sfWebRequest $request){
		
		
			$this->page = $request->getParameter('page', 1);
				//echo $request->getParameter("application_id");.....................................................
			$conn = new mysqli("localhost", "admin", "password", "mcc_v2");
		
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
			}
	
	//get the stage the was held at....................................................................................
	$sql= "select stage_id from application_reference a where a.application_id = '".$request->getParameter("application_id")."' order by a.id desc limit 1,1 ";
	/*$res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($sql);	*/
	
	/*$result = $conn->query($sql);
	$thestageid = null ;	
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				$thestageid= $row["stage_id"];
				}
			} 
		$conn->close();
		if($res){

			foreach($sql as $r){
				$thestageid= $r["stage_id"];
			}
		}
		
		/*$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
		  mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
		//CBS: get the previous stage before hold
		$q = Doctrine_Query::create()
						   ->from("application_reference a")
						   ->where("a.application_id= ?", $request->getParameter("application_id"))
						   //->andWhere('a.parent_submission = 0')
						   //->andWhere("a.approved <> 16")		//CBS edit
						   ->orderBy("a.id DESC")
						   ->limit("1,1");
		//$result = mysql_query("select * from form_entry", $dbconn);
			 
		$entry_data = mysql_fetch_assoc($result);*/
		
		//refresh the page after unhold action
		/*  $q = Doctrine_Query::create()
		->from("FormEntry a")
		->where("a.id =?", $_GET['application_id']);
			$submission = $q->fetchOne();


		$application_manager = new ApplicationManager();
		$invoice_manager = new InvoiceManager();
		$payments_manager = new PaymentsManager();

		/*$payments_manager->validate_all_invoices($submission->getId());

		{
			//If the application was declined then attempt to make a resubmission
			$submission = $application_manager->application_onhold($submission->getId(),$thestageid);
			/*header("Location: ".public_path()."index.php/application/index/id/".$submission->getId());
			echo "successfully resubmittted";
			exit;
		}*/

			
						/*$q = Doctrine_Query::create()
						   ->from("FormEntry a")
						   ->where("a.user_id = ?", $this->getUser()->getGuardUser()->getId())
						   ->andWhere('a.parent_submission = 0')
						   //->andWhere("a.approved <> 16")		//CBS edit
						   ->orderBy("a.application_id DESC");

						$_SESSION['group'] = 0;
						$_SESSION['subgroup'] =0;*/
					
		//change onhold
		//echo "hello".$thestageid;
		//$application_manager->application_onhold($submission->getId(),$thestageid);
		/*$this->setLayout("layoutdash");
		//echo $request->getParameter("application_id");
		$this->pager = new sfDoctrinePager('FormEntry', 10);
		$this->pager->setQuery($q);
		$this->pager->setPage($request->getParameter('page', 1));
		$this->pager->init();
				
}*/




public function executeOnhold(sfWebRequest $request){
		
	if($_SESSION['SESSION_CUTEFLOW_USERID'])
	{
		$_SESSION['SESSION_CUTEFLOW_USERID'] = false;
		$this->getUser()->getAttributeHolder()->clear();
		$this->getUser()->clearCredentials();
		$this->getUser()->setAuthenticated(false);
		$this->getUser()->signout();
		header("Location: /index.php");
		exit;
	}

		if(!$this->getUser()->isAuthenticated())
		{
			$this->redirect('/index.php');
		}

		if($request->getParameter("show"))
		{
			$this->show = $request->getParameter("show");
		}
		
		
		
		  $q = Doctrine_Query::create()
		->from("FormEntry a")
		->where("a.id =?", $_GET['application_id']);
			$submission = $q->fetchOne();





		$application_manager = new ApplicationManager();
		$invoice_manager = new InvoiceManager();
		$payments_manager = new PaymentsManager();

		$payments_manager->validate_all_invoices($submission->getId());

		{
			//If the application was declined then attempt to make a resubmission
			$submission = $application_manager->application_onhold($submission->getId(),"56");
			/*header("Location: ".public_path()."index.php/application/index/id/".$submission->getId());
			echo "successfully resubmittted";
			exit;*/
		}
		
		
		
		
/* CBS: old way to move application to onhold stage
		$this->setLayout("layoutdash");
$q = Doctrine_Query::create()
		->update('FormEntry')
		->set('approved','?', 53)
		->where('id= ?',$request->getParameter("application_id"));
		$q->execute();*/
		
		$this->setLayout("layoutdash");

	$q = Doctrine_Query::create()
	   ->from("FormEntry a")
	   ->where("a.user_id = ?", $this->getUser()->getGuardUser()->getId())
	   //->andWhere('a.approved <> ?',53)														//*CBS patch* dont display aarchived(rejected apps)
	   ->andWhere('a.approved <> ?', 55)
	   ->orderBy("a.date_of_submission DESC")
	   ->limit(10);
	$this->latest_applications = $q->execute();
	
	
	
	
	//$this->setLayout("layout-metronic");
	
}




    /**
     * Executes 'Accepttransfer' action
     *
     * Cancel Change of Ownership of an Application
     *
     * @param sfRequest $request A request object
     */
    public function executeAccepttransfer(sfWebRequest $request)
    {
        $data = $request->getParameter('code');
        $data = json_decode(base64_decode($data), true);

        $application_id = $data['id'];

        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.id = ?', $application_id);
        $this->application = $q->fetchOne();

        $previous_owner = $this->application->getCirculationId();

        $this->application->setUserId($this->application->getCirculationId());
        $this->application->setCirculationId("");
        $this->application->save();

        $q = Doctrine_Query::create()
            ->from("SfGuardUserProfile a")
            ->where("a.user_id = ?",$this->application->getUserId());

        $new_user = $q->fetchOne();

        $q = Doctrine_Query::create()
            ->from("SfGuardUserProfile a")
            ->where("a.user_id = ?", $previous_owner);

        $previous_user = $q->fetchOne();

        //Send email to new owner to alert them
        //Send account recovery email
        $body = "
                Hi {$new_user->getFullname()}, <br>
                <br>
                Application '".$this->application->getApplicationId()."' has been transferred to your account from '".$previous_user->getFullname()."'. <br>
                <br>
                Click here to view the application details:<br>
                <a href='http://".$_SERVER['HTTP_HOST']."/index.php/application/view/id/".$this->application->getId()."'>".$this->application->getApplicationId()."</a>
                <br>
                <br>
                Thanks,<br>
                ".sfConfig::get('app_organisation_name').".<br>
            ";

        $mailnotifications = new mailnotifications();
        $mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $new_user->getEmail(),"Application Transfer",$body);


        $this->getUser()->setFlash('notice', 'The request for transfer of ownership has been accepted');
        return $this->redirect("/index.php/dashboard");
    }

    /**
	 * Executes 'Delete' action
	 *
	 * Delete an application
	 *
	 * @param sfRequest $request A request object
	 */
        public function executeDelete(sfWebRequest $request)
        {
                $q = Doctrine_Query::create()
                   ->from('FormEntry a')
                   ->where('a.id = ?', $request->getParameter("entry"))
                   ->andWhere('a.user_id = ?', $this->getUser()->getGuardUser()->getId());
                $application = $q->fetchOne();

                if($application && $application->getApproved() == "0")
                {
                	$application->delete();
                }
                $this->redirect("/index.php/application/index/drafts/1");
        }

    /**
     * Executes 'Delete' action
     *
     * Delete a draft application from the dashboard
     *
     * @param sfRequest $request A request object
     */
    public function executeDeletedraft(sfWebRequest $request)
    {
        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.id = ?', $request->getParameter("id"))
            ->andWhere('a.user_id = ?', $this->getUser()->getGuardUser()->getId());
        $application = $q->fetchOne();

        if($application && $application->getApproved() == "0")
        {
            $application->delete();
        }

        if($request->getParameter("redirect"))
        {
            $this->redirect("/index.php/forms/view?id=".$request->getParameter("redirect"));
        }
        else {
            $this->redirect("/index.php/dashboard");
        }
	}
	




	public function executeEdit2(sfWebRequest $request){
		
		if($_SESSION['SESSION_CUTEFLOW_USERID'])
        {
            $_SESSION['SESSION_CUTEFLOW_USERID'] = false;
            $this->getUser()->getAttributeHolder()->clear();
            $this->getUser()->clearCredentials();
            $this->getUser()->setAuthenticated(false);
            $this->getUser()->signout();
            header("Location: /index.php");
            exit;
        }

    		if(!$this->getUser()->isAuthenticated())
    		{
    			$this->redirect('/index.php');
    		}

    		if($request->getParameter("show"))
    		{
    			$this->show = $request->getParameter("show");
    		}

    		$this->setLayout("layoutdash");
			
			
/*	$q = Doctrine_Query::create()
			->update('FormEntry')
			->set('approved','?', 54)
			->where('id= ?',);
			$q->execute();*/
		$application_manager= new ApplicationManager();	
		$submission = $application_manager->application_onhold($request->getParameter("application_id"),"56");
		
		

        $q = Doctrine_Query::create()
           ->from("FormEntry a")
           ->where("a.user_id = ?", $this->getUser()->getGuardUser()->getId())
		   ->andWhere('a.approved <> ?',55)//*CBS patch* dont display onhold on dashboard list
		   ->andWhere('a.approved <> ?',56)//CBS: dont display archived(rejected apps) on dashboard list
           ->orderBy("a.date_of_submission DESC")
           ->limit(10);
        $this->latest_applications = $q->execute();
	//$this->redirect("/index.php/dashboard");
		$this->setLayout("layoutdash");
		
		
		//$this->setLayout("layout-metronic");
		
	}

        /**
	 * Executes 'View' action
	 *
	 * Displays full application details
	 *
	 * @param sfRequest $request A request object
	 */
        public function executeView(sfWebRequest $request)
        {
			error_log('---------EXECUTING THE VIEW---------');

            $q = Doctrine_Query::create()
                ->from('FormEntry a')
                ->where('a.id = ?', $request->getParameter("id"))
                ->andWhere('a.user_id = ?', $this->getUser()->getGuardUser()->getId());
            $this->application = $q->fetchOne();

            //Trigger any automated triggers
            $q = Doctrine_Query::create()
                ->from("MfInvoice a")
                ->where("a.app_id = ?", $this->application->getId())
                ->andWhere("a.paid = 2")
                ->limit(1);
            $paid_invoices = $q->execute();
            foreach($paid_invoices as $paid_invoice)
            {
                //$paid_invoice->save();//Errornous and Commented out by CBS. It's purpose remains ununderstoood 
            }

            if($request->getParameter("formid"))
        		{
        			$this->choosen_form = $request->getParameter("formid");
        		}

                if(empty($this->application))
                {
                    echo "Permission Denied.";
                    exit;
                }

                if($request->getParameter("messages") == "read")
                {
                    $q = Doctrine_Query::create()
                   ->from("Communications a")
                   ->Where('a.messageread = ?', '0')
                   ->andWhere('a.application_id = ?', $this->application->getId());
                $messages = $q->execute();
                foreach($messages as $message)
                {
                    if($message->getArchitectId() == "")
                    {
                        $message->setMessageread("1");
                        $message->save();
                    }
                }
                }

                if($request->getPostParameter("txtmessage"))
                {
                        $message = new Communications();
                        $message->setArchitectId($this->getUser()->getGuardUser()->getId());
                        $message->setMessageread("0");
                        $message->setContent($request->getPostParameter("txtmessage"));
                        $message->setApplicationId($this->application->getId());
                        $message->setActionTimestamp(date('Y-m-d'));
                        $message->save();

						$q = Doctrine_Query::create()
						   ->from("SfGuardUserProfile a")
						   ->where("a.user_id = ?", $this->application->getUserId());
						$user_profile = $q->fetchOne();

						/**$q = Doctrine_Query::create()
						   ->from("Communications a")
						   ->where("a.reviewer_id <> ? OR a.reviewer_id <> ?", array(NULL, ""))
						   ->groupBy("a.reviewer_id");
						$existing_participants = $q->execute();

						if(sizeof($existing_participants) > 0)
						{
							foreach($existing_participants as $existing_participant)
							{
								$q = Doctrine_Query::create()
								   ->from("CfUser a")
								   ->where("a.nid = ?", $existing_participant->getReviewerId());
								$reviewer = $q->fetchOne();

								if($reviewer)
								{
									$body = "
							        Hi ".$reviewer->getStrfirstname()." ".$reviewer->getStrlastname().",<br>
							        <br>
							        You have received a new message on ".$this->application->getApplicationId()." from ".$user_profile->getFullname().":<br>
							        <br><br>
							        -------
							        <br>
							        ".$request->getPostParameter("txtmessage")."
							        <br>
							        <br>
							        Click here to view the application: <br>
							        ------- <br>
							        <a href='http://".$_SERVER['HTTP_HOST']."/backend.php/applications/view/id/".$this->application->getId()."'>Link to ".$this->application->getApplicationId()."</a><br>
							        ------- <br>

							        <br>
							        ";

									$mailnotifications = new mailnotifications();
							        $mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $reviewer->getStremail(),"New Message",$body);
								}
							}
						}
						else
						{
							//Get list of reviewers who can see this application
							$q = Doctrine_Query::create()
		                       ->from('CfUser a')
            				   ->where('a.bdeleted = 0');
		                    $reviewers = $q->execute();
		                    foreach($reviewers as $reviewer)
		                    {
		                    	$q = Doctrine_Query::create()
									->from('mfGuardUserGroup a')
									->leftJoin('a.Group b')
									->leftJoin('b.mfGuardGroupPermission c') //Left Join group permissions
									->leftJoin('c.Permission d') //Left Join permissions
									->where('a.user_id = ?', $reviewer->getNid())
									->andWhere('d.name = ?', "accesssubmenu".$this->application->getApproved());
								$usergroups = $q->execute();
								if(sizeof($usergroups) > 0)
								{
									$body = "
							        Hi ".$reviewer->getStrfirstname()." ".$reviewer->getStrlastname().",<br>
							        <br>
							        You have received a new message on ".$this->application->getApplicationId()." from ".$user_profile->getFullname().":<br>
							        <br><br>
							        -------
							        <br>
							        ".$request->getPostParameter("txtmessage")."
							        <br>
							        <br>
							        Click here to view the application: <br>
							        ------- <br>
							        <a href='http://".$_SERVER['HTTP_HOST']."/backend.php/applications/view/id/".$this->application->getId()."'>Link to ".$this->application->getApplicationId()."</a><br>
							        ------- <br>

							        <br>
							        ";

									$mailnotifications = new mailnotifications();
							        $mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $reviewer->getStremail(),"New Message",$body);
								}
		                    }
                        }**/

                        $activity = new Activity();
                        $activity->setUserId($this->getUser()->getGuardUser()->getId());
                        $activity->setFormEntryId($this->application->getId());
                        $activity->setAction("User sent a message");
                        $activity->setActionTimestamp(date('Y-m-d'));
                        $activity->save();
                }



			if($request->getParameter("open"))
			{
				$this->open = $request->getParameter("open");
			}

            if($request->getParameter("messages") == "read")
            {
                $this->open = "messages";
            }

            $this->done = $request->getParameter("done", 0);

			$this->setLayout("layoutdash");
        }

        /**
	 * Executes 'Draft' action
	 *
	 * Displays list of all of the currently logged in client's draft applications
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeDrafts(sfWebRequest $request)
	{
		$this->page = $request->getParameter('page', 1);
		$this->setLayout("layoutdash");
	}

        /**
	 * Executes 'Groups' action
	 *
	 * Displays list of all of the categories of applications
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeGroups(sfWebRequest $request)
	{
                //OTB patch - We set an Env variable for the county a user has selected. 
               // $county = $request->getParameter("county_name");
                //$this->getUser()->setAttribute('county_name', $county);
                 
		if($request->getParameter("others"))
		{
			$this->display_others = true;
		}
		else
		{
			$this->display_others = false;
		}

		if($request->getParameter("id"))
		{
			$this->group_id = $request->getParameter("id");
		}
		else
		{
			$this->group_id = false;
		}

		$this->setLayout("layoutdash");
	}

        /**
	 * Executes 'Edit' action
	 *
	 * Allows client and resubmit an application
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeEdit(sfWebRequest $request)
	{
		$this->setLayout("layoutdash");
	}

        /**
	 * Executes 'Share' action
	 *
	 * Allows the client to share selected application with another client
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeShare(sfWebRequest $request)
	{

			$q = Doctrine_Query::create()
			->from('FormEntry a')
			   ->where('a.id = ?', $request->getParameter("id"));
			$this->application = $q->fetchOne();

			if($request->getParameter("filter"))
			{
				$this->filter = $request->getParameter("filter");
			}

			if($request->getPostParameter("filter"))
			{
				$this->filter = $request->getPostParameter("filter");
			}

			if($request->getParameter("page"))
			{
				$this->page = $request->getParameter("page");
			}

			if($request->getParameter("architect") && $request->getParameter("architect") != "")
			{
				$share = new FormEntryShares();
				$share->setSenderid($this->getUser()->getGuardUser()->getId());
				$share->setReceiverid($request->getParameter("architect"));
				$share->setFormentryid($request->getParameter("id"));
				$share->save();
					$this->redirect("/index.php/application/shared");
			}
		$this->setLayout("layoutdash");
	}

         /**
	 * Executes 'Shared' action
	 *
	 * Shows success message if application is shared successfully
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeShared(sfWebRequest $request)
	{
		$this->setLayout("layoutdash");
	}


    /**
    * Executes 'Viewpermit' action
    *
    * Displays the generated permit that is attached to an application
    *
    * @param sfRequest $request A request object
    */
    public function executeViewpermit(sfWebRequest $request)
    {
      $q = Doctrine_Query::create()
       ->from('SavedPermit a')
       ->where('a.id = ?', $request->getParameter('id'));
      $savedpermit = $q->fetchOne();

       if($savedpermit)
       {
         $application = $savedpermit->getApplication();

        require_once(dirname(__FILE__)."/../../../../../lib/vendor/dompdf/dompdf_config.inc.php");


        $html = "<html>
        <body>
        ";

        $templateparser = new TemplateParser();

        $html .= $templateparser->parsePermit($application->getId(), $application->getFormId(), $application->getEntryId(), $savedpermit->getId(), $savedpermit->getPermit());

        $html .= "
        </body>
        </html>";

        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->render();
        $dompdf->stream($application->getApplicationId().".pdf");
       }
       else
       {
         echo "Invalid Permit Link";
       }

       exit;

    }

}
