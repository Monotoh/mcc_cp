<div class="notfoundpanel">
  <h2>Access Denied!</h2>
  <h4>Sorry, you are not allowed to view the page you are looking for!</h4>
  <h4>If you are experiencing any trouble, please contact your system administrator.</h4>
</div><!-- notfoundpanel -->