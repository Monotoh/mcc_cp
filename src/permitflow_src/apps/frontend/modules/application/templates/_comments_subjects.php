<?php
/**
 * _comments_subjects template.
 *
 * Shows summary of subjects of approval from all reviewers
 *
 * @package    backend
 * @subpackage applications
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>