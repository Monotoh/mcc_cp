<?php
/**
 * OTB patch - Allow a user to first specify a subcounty selected from Subcounties table
 */
use_helper('I18N');

include_component('index', 'checksession');

//Get site config properties
$q = Doctrine_Query::create()
    ->from("ApSettings a")
    ->where("a.id = 1")
    ->orderBy("a.id DESC");
$apsettings = $q->fetchOne();
?>
<div class="contentpanel">

    <div class="row">
        <!--Display organisation description from the site config-->
        <div class="col-sm-6 col-lg-8">
            <div class="card-box card-box-light-orange widget-user">
                <div>
                    <img src="/asset_unified/images/users/avatar-1.jpg" class="img-responsive img-circle" alt="user">
                    <div class="wid-u-info">
                      <?php
                      $q = Doctrine_Query::create()
                      ->from('sfGuardUserProfile a')
                      ->where('a.user_id = ?', $sf_user->getGuardUser()->getId());
                      $user = $q->fetchOne();
                      ?>
                        <h4 class="m-t-0 m-b-5"><?php echo __('Welcome'); ?>, <?php echo $user->getFullname(); ?></h4>
                        <?php
                        if($apsettings)
                        {
                        ?>
                           <p class="m-b-5 font-13"><?php echo $apsettings->getOrganisationDescription(); ?></p>
                        <?php
                        }
                        else
                        {
                        ?>
                           <p class="m-b-5 font-13"><?php echo sfConfig::get('app_organisation_description') ?></p>
                        <?php
                        }
                        ?>
                        <a class="m-t-5 btn btn-sm btn-default" href="<?php echo public_path(); ?>index.php/settings"><?php echo __('My Profile'); ?></a>
                        <a class="m-t-5 btn btn-sm btn-danger" href="<?php echo url_for('signon/logout') ?>"><?php echo __('Log Out'); ?></a>
                      </div>
                </div>
            </div>
        </div>

        <!--Display the organisation help info from the site config (Appears on the sidebar)-->
        <?php if($apsettings){ ?>
        <div class="col-sm-6 col-lg-4">
            <div class="card-box widget-user">
                <?php echo html_entity_decode($apsettings->getOrganisationHelp()); ?>
            </div>
        </div>
        <?php } ?>
      </div>

      <div class="row">
         <div class="col-sm-8 col-lg-8">
          <!--Show a list of the latest applications-->
          <div class="panel panel-dark">
            <div class="panel-heading">
            <h3 class="panel-title"><?php echo __('Select County') ?></h3>
            </div>

              <div class="panel-body panel-body-nopadding">
                    <form class="form-bordered" action="/index.php/application/groups">
                       <div class="form-group">
                        <label class="col-sm-4"><i class="bold-label"><?php echo __('Please Select County'); ?></i></label>
                       <div class="col-sm-8 rogue-input">
                           <select id="county_name" name="county_name">
                               <?php foreach($counties as $c ):  ?>
                                    <option value="<?php echo $c->getCountyCode() ?>"><?php echo $c->getCountyName() ?></option>         
                               <?php endforeach; ?>
                           </select>
                        </div>
                      </div>
                         <div class="panel-footer">
			<button type="submit" class="btn btn-primary"><?php echo __('Select'); ?></button>
	                  </div>
                    </form>
              </div> 
          </div>
         
       </div>
         
      </div>
      <!-- end row -->

</div>
