<?php
/**
 * draftsSuccess.php template.
 *
 * Displays list of all of the currently logged in client's draft applications
 *
 * @package    frontend
 * @subpackage application
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
 
function GetDays($sEndDate, $sStartDate){  
    $aDays[] = $start_date;
	$start_date  = $sStartDate;
	$end_date = $sEndDate;
	$current_date = $start_date;
	while(strtotime($current_date) <= strtotime($end_date))
	{
		    $aDays[] = gmdate("Y-m-d", strtotime("+1 day", strtotime($current_date)));
		    $current_date = gmdate("Y-m-d", strtotime("+1 day", strtotime($current_date)));
	}

  
  return $aDays;  
} 
?>
<?php
	$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
	mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
	
	
	
	$filter = $_GET['filter'];
	if(!empty($filter))
	{
		$filter = " AND b.approved = '".$filter."'";
	}
	
	
?>

       <div class="pageheader">
       <h2><i class="fa fa-eraser"></i>Drafts (Unsubmitted or Saved Applications)<span> This page list all the applications you have saved as drafts</span></h2>
      <div class="breadcrumb-wrapper">
        
        <ol class="breadcrumb">
          <li><a href="#">Drafts</a></li>
          <li class="active">Saved Drafts</li>
        </ol>
      </div>
    </div>
    
    
    
    
    
    
    
    
    <div class="contentpanel">

    <div class="row">
    
    
      <div class="row">
            <div class="col-sm-3 col-lg-2">
                <a class="btn btn-warning btn-block btn-compose-email" href="<?php echo public_path(); ?>index.php/application/groups">Submit Application</a>
                
                <ul class="nav nav-pills nav-stacked nav-email">
                    <li>
                    <a href="<?php echo public_path(); ?>index.php/application/index">
                        <span class="badge pull-right"><?php echo sizeof($sharedapplications); ?></span>
                        <i class="glyphicon glyphicon-inbox"></i> My Applications
                        
                    </a>
                    </li>
                    
                    <li>
                    <a href="<?php echo public_path(); ?>index.php/sharedapplication/index">
                    <span class="badge pull-right"><?php echo sizeof($sharedapplications); ?></span>
                    <i class="glyphicon glyphicon-send"></i>  <?php echo sfConfig::get('app_'.$_SESSION['locale'].'_shared_applications'); ?> </a>
                  </li>
                
                 <li class="active">
                  <a href="<?php echo public_path(); ?>index.php/application/drafts">
                  <span class="badge pull-right"><?php echo sizeof($draftapplications); ?></span>
                  <i class="glyphicon glyphicon-pencil"></i>
                   <?php echo sfConfig::get('app_'.$_SESSION['locale'].'_drafts'); ?> 
                   </a>
                 </li>
            
                 <?php /*?> <li><a href="#"><i class="glyphicon glyphicon-trash"></i> Trash</a></li><?php */?>
                 
                 
                </ul>
                
              </ul>
                
            </div><!-- col-sm-3 -->
      
      
      
      
           <div class="col-sm-9 col-lg-10">
           
      

                    
                    <?php

	$q = Doctrine_Query::create()
	   ->from("FormEntry a")
	   ->where("a.user_id = ?", $sf_user->getGuardUser()->getId())
	   ->andWhere("a.approved = ?", 0)
	   ->orderBy("a.id DESC");

 $pager = new sfDoctrinePager('FormEntry', 10);
 $pager->setQuery($q);
 $pager->setPage($page);
 $pager->init();

 $counter = 1;
 include_partial('application/list', array('applications' => $pager->getResults()));
 
?>
                  
    
                
        
        
        
       

<?php /*?><ul class="pagination mt10 pull-right special-pager">
<?php if ($pager->haveToPaginate()): ?>
	<?php
    $filter = "";
	if($_GET['filter'])
	{
		$filter = "&filter=".$_GET['filter'];
	}
  ?>
  <li><?php echo "<a title='First' href='".public_path()."index.php/application/drafts?page=1".$filter."'><i class=\"fa fa-angle-left\"></i></a>"; ?></li>
  <li><?php echo "<a title='Previous' href='".public_path()."index.php/application/drafts?page=".$pager->getPreviousPage()."".$filter."'><i class=\"fa fa-angle-left\"></i></a>"; ?></li>
  
  <?php foreach ($pager->getLinks() as $page): ?>
    <?php
	if($pager->getPage() == $page)
	{
		?>
		<li class="active"><a><?php echo $page; ?></a></li>
		<?php
	}
	else
	{
	?>
		<li><?php echo "<a title='Page ".$page."' href='".public_path()."index.php/application/drafts?page=".$page."".$filter."'>".$page."</a>"; ?></li>
	<?php
	}
	?>
  <?php endforeach; ?>

 <li> <?php echo "<a title='Next' href='".public_path()."index.php/application/drafts?page=".$pager->getNextPage()."".$filter."'><i class=\"fa fa-angle-right\"></i></a>"; ?></li>
 <li> <?php echo "<a title='Last' href='".public_path()."index.php/application/drafts?page=".$pager->getLastPage()."".$filter."'><i class=\"fa fa-angle-right\"></i></a>"; ?></li>
<?php endif; ?>
</ul><!-- /.pagination --><?php */?>


</div>
            
             </div><!--panel-row-->
      </div><!--contentpanel-->          
                                    




