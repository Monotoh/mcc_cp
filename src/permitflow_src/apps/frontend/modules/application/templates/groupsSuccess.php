<?php
/**
 * groupsSuccess.php template.
 *
 * Displays list of all of the categories of applications
 *
 * @package    frontend
 * @subpackage application
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
 use_helper('I18N');


include_component('index', 'checksession');

//Get site config properties
$q = Doctrine_Query::create()
    ->from("ApSettings a")
    ->where("a.id = 1")
    ->orderBy("a.id DESC");
$apsettings = $q->fetchOne();
?>
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Make Application</h4>
    </div>
</div>
<!-- Page-Title -->

<div class="row">
    <div class="col-lg-8">

        <div class="panel-group" id="accordion-test-2">
            <?php

            $count = 0;

            $q = Doctrine_Query::create()
                ->from('FormGroups a')
                ->orderBy('a.group_name ASC');
            $groups = $q->execute();
            $count = 0;
            foreach($groups as $group)
            {
                $count++;
                $form_count = 0;

                $q = Doctrine_Query::create()
                    ->from('ApForms a')
                    ->where('a.form_group = ?', $group->getGroupId())
                    ->andWhere('a.form_type = 1')
                    ->andWhere('a.form_active = 1')
                    ->orderBy('a.form_name ASC');
                $applications = $q->execute();
                foreach ($applications as $application) {
                    //Check if enable_categories is set, if it is then filter application forms
                    if (sfConfig::get('app_enable_categories') == "yes") {
                        $q = Doctrine_Query::create()
                            ->from('sfGuardUserCategoriesForms a')
                            ->where('a.categoryid = ?', $sf_user->getGuardUser()->getProfile()->getRegisteras())
                            ->andWhere('a.formid = ?', $application->getFormId());
                        $category = $q->count();

                        if ($category == 0) {
                            continue;
                        } else {
                            $form_count++;
                        }
                    } else {
                        //If form category permissions is disabled and then just display the category
                        $form_count++;
                    }
                }

                if ($form_count == 0) {
                    continue;
                }

                ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion-test-2" href="#collapseTwo-<?php echo $count; ?>"
                               class="collapsed" aria-expanded="false">
                                <?php echo $group->getGroupName(); ?>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo-<?php echo $count; ?>" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul class="list-group list-group-forms m-b-0">
                                <?php
                                $q = Doctrine_Query::create()
                                    ->from('ApForms a')
                                    ->andWhere('a.form_type = 1')
                                    ->andWhere('a.form_active = 1')
                                    ->andWhere('a.form_group = ?', $group->getGroupId())
                                    ->orderBy('a.form_name ASC');
                                $forms = $q->execute();
                                foreach($forms as $form)
                                {
                                    if (sfConfig::get('app_enable_categories') == "yes") {
                                        $q = Doctrine_Query::create()
                                            ->from('sfGuardUserCategoriesForms a')
                                            ->where('a.categoryid = ?', $sf_user->getGuardUser()->getProfile()->getRegisteras())
                                            ->andWhere('a.formid = ?', $form->getFormId());
                                        $category = $q->count();

                                        if ($category == 0) {
                                            continue;
                                        }
                                    }
                                    ?>
                                    <li class="list-group-item">
                                        <a href="<?php echo public_path(); ?>index.php/forms/info?id=<?php echo $form->getFormId(); ?>"><?php echo $form->getFormName() ?></a>
                                    </li>
                                    <?php
                                }
                                ?>
                                <ul>
                        </div>
                    </div>
                </div>

                <?php
            }
            ?>
        </div>
    </div>

    <!--Display a sidebar with information from the site config-->
    <?php if($apsettings){ ?>
        <div class="col-lg-4">
            <div class="card-box widget-user">
                <div>
                    <img src="/asset_unified/images/users/avatar-1.jpg" class="img-responsive img-circle" alt="user">
                    <div class="wid-u-info">
                        <h4 class="m-t-0 m-b-5"><?php echo $sf_user->getGuardUser()->getProfile()->getFullname(); ?></h4>
                        <p class="m-b-5 font-13"><?php echo $sf_user->getGuardUser()->getProfile()->getEmail(); ?><br>
                            ID: <?php echo $sf_user->getGuardUser()->getUsername(); ?>
                        </p>
                        <a class="btn btn-primary" href="/index.php/signon/logout">Logout</a>
                    </div>
                </div>
            </div>

            <?php echo html_entity_decode($apsettings->getOrganisationSidebar()); ?>
        </div>
    <?php } ?>

</div>