<?php
/**
 * sharedSuccess.php template.
 *
 * Shows success message if application is shared successfully
 *
 * @package    frontend
 * @subpackage application
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>

<div class="content">

<ul class="breadcrumb">
        <li><a href="#">Applications</a> <span class="divider">/</span></li>
        <li class="active">Share Successful</li>
        <li></li>
</ul>    <!-- Docs nav-->


<div class="row">

<?php include_partial('index/sidemenu', array('' => '')) ?>

<div class="span9 padded-20">


<div class="alert alert-success" style="text-align:center;">
You have successfully shared the application.
</div>


</div><!-- /.span9 -->
						
</div><!-- /.row -->
			



