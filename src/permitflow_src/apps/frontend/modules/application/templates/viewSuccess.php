<?php
/**
 * viewSuccess.php template.
 *
 * Displays full application details
 *
 * @package    frontend
 * @subpackage application
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");

//If invoice is pending, check payment aggregator for remote confirmation
$invoice_manager = new InvoiceManager();
$invoice_manager->update_invoices($application->getId());

//Get site config properties
$q = Doctrine_Query::create()
    ->from("ApSettings a")
    ->where("a.id = 1")
    ->orderBy("a.id DESC");
$apsettings = $q->fetchOne();

//Get number of days since
function GetDaysSince($sStartDate, $sEndDate){
    $start_ts = strtotime($sStartDate);
    $end_ts = strtotime($sEndDate);
    $diff = $end_ts - $start_ts;
    return round($diff / 86400);
}

//Only the owner of the application should have access to the application
if($sf_user->getGuardUser()->getId() == $application->getUserId())
{
?>
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-6">
        <!-- OTB patch - copied from Mombasa CP -->
      
      <?php
       //Change this to use built in doctrine connections
        $dbconn = mysqli_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysqli_select_db($dbconn,sfConfig::get('app_mysql_db'));
        //
        $sql = "SELECT * FROM ap_forms WHERE form_stage = ".$application->getApproved()."  AND form_active = 1 AND form_type = 1 ";
        $form_result = mysqli_query($dbconn,$sql);

        while($row = mysqli_fetch_assoc($form_result))
        {
           
          if($row['form_id'] != $application->getFormId())
          {
		    //OTB Fix Show linkto if a user is permitted to access a particular form 
		    $user_registered_as = Doctrine_Query::create()
		                         ->from('sfGuardUserProfile u')
		                         ->where('u.user_id = ?',$sf_user->getGuardUser()->getId()) ;
		     $user_registered_as_res =   $user_registered_as->fetchOne();
		     //if we have something
		     if($user_registered_as_res){
				// echo  $user_registered_as_res->getRegisterAs();
				 $sql_q = "SELECT formid from sf_guard_user_categories_forms where categoryid = ".$user_registered_as_res->getRegisterAs()." and formid=".$row['form_id']."" ;
                                 error_log("Query Test ".$sql_q);
                                 $sql_result = mysqli_query($dbconn,$sql_q);
				 //
				 while($row_r = mysqli_fetch_assoc($sql_result)) {
					 //
					  echo "<a class=\"btn btn-primary\" href='/index.php/forms/view?id=".$row_r['formid']."&linkto=".$application->getId()."'>".__('Apply for')." ".$row['form_name']."</a>";
				 }
			 }                    	  
           
            $action_count++;
          }
        } ?>
    </div>
    <div class="col-sm-6 right">
      
        <div class="btn-group">
            <button type="button" class="btn btn-primary dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-print"></i> Downloads <span class="caret"></span> </button>
            <?php
            $q = Doctrine_Query::create()
               ->from("SavedPermit a")
               ->where("a.application_id = ?", $application->getId())
               ->orderBy("a.id ASC");
            $saved_permits = $q->execute();
            ?>
            <ul class="dropdown-menu dropdown-menu-right">
                <?php
                foreach($saved_permits as $permit) {
                    ?>
                    <li><a href="/index.php/permits/view/id/<?php echo $permit->getId(); ?>"><?php echo $permit->getTemplate()->getTitle(); ?></a></li>
                    <?php
                }
                ?>
            </ul>
        </div>
        <a href="/index.php/application/edit?application_id=<?php echo $application->getId(); ?>" class="btn btn-primary dropdown-toggle waves-effect"><i class="fa fa-edit"></i> Edit </a>
    </div>
</div>
<!-- Page-Title -->


<div class="row">

    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?php
                $q = Doctrine_Query::create()
                   ->from("ApForms a")
                   ->where("a.form_id = ?", $application->getFormId());
                $form = $q->fetchOne();
                if($form) {
                    ?>
                    <h4 class="text-dark  header-title"><?php echo $form->getFormName(); ?></h4>
                    <?php
                }
                ?>
                <p class="text-primary">
                    Ref: <?php echo $application->getApplicationId(); ?>
                </p>
                <p class="text-muted font-15">Date of Submission: <?php echo $application->getDateOfSubmission(); ?> <br>
                    <?php
                    if($application->getDateOfResponse())
                    {
                        ?>
                        Duration: <?php echo GetDaysSince($application->getDateOfSubmission(), $application->getDateOfResponse()); ?>days
                        <br>
                        <?php
                    }
                    else {
                        ?>
                        Duration: <?php echo GetDaysSince($application->getDateOfSubmission(), date("Y-m-d H:m:s")); ?>days
                        <br>
                        <?php
                    }
                    ?>
                    Approval: <?php echo $application->getStatusName(); ?>
                </p>
            </div>
            <div class="panel-body">
                <?php
                //Display control buttons that manipulate the application
                include_partial('viewdetails', array('application' => $application, 'choosen_form' => $choosen_form));
                ?>
            </div>
        </div>
    </div><!--Panel-dark-->


    <!--Display a sidebar with information from the site config-->
    <?php if($apsettings){ ?>

      <div class="col-lg-4">
            <div class="card-box widget-user">
                <h4 class="m-t-0 m-b-20 header-title"><b>Messages</b></h4>
                <div class="chat-conversation">
                    <ul class="conversation-list nicescroll">
                        <?php
                        $q = Doctrine_Query::create()
                            ->from('Communications a')
                            ->where('a.application_id = ?', $application->getId())
                            ->orderBy('a.id ASC');
                        $communications = $q->execute();
                        foreach($communications as $communication)
                        {
                            $messages[] = $communication;
                        }

                        if(sizeof($messages) <= 0){
                        ?>
                        <table class="table mb0">
                            <tbody>
                            <tr>
                                <td>
                                    <i class="bold-label"><?php echo __('No Messages'); ?></i>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <?php
                        }
                        else {
                            foreach($messages as $message) {
                                if($message->getArchitectId() != "") {
                                    $q = Doctrine_Query::create()
                                        ->from('SfGuardUser a')
                                        ->where('a.id = ?', $message->getArchitectId());
                                    $client = $q->fetchOne();

                                    $fullname = $client->getProfile()->getFullname();
                                    ?>
                                    <li class="clearfix">
                                        <div class="chat-avatar">
                                            <img style="height: 40px;" src="/asset_unified/images/users/avatar-1.jpg">
                                            <i><?php echo $message->getActionTimestamp(); ?></i>
                                        </div>
                                        <div class="conversation-text">
                                            <div class="ctext-wrap">
                                                <i><?php echo $fullname; ?></i>

                                                <p>
                                                    <?php echo $message->getContent(); ?>
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                <?php
                                }
                                else if($message->getReviewerId() != "") {
                                    $message->setMessageRead("1");
                                    $message->save();
                                    $q = Doctrine_Query::create()
                                        ->from('CfUser a')
                                        ->where('a.nid = ?', $message->getReviewerId());
                                    $reviewer = $q->fetchOne();

                                    $fullname = $reviewer->getStrdepartment()." - ".$reviewer->getStrfirstname()." ".$reviewer->getStrlastname();
                                    ?>
                                    <li class="clearfix odd">
                                        <div class="chat-avatar">
                                            <img style="height: 40px;" src="/asset_unified/images/users/avatar-1.jpg">
                                            <i><?php echo $message->getActionTimestamp(); ?></i>
                                        </div>
                                        <div class="conversation-text">
                                            <div class="ctext-wrap">
                                                <i><?php echo $fullname; ?></i>

                                                <p>
                                                    <?php echo $message->getContent(); ?>
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                }
                            }
                        }
                        ?>

                    </ul>
                    <div class="row">
                        <form action="/index.php/application/view/id/<?php echo $application->getId(); ?>" method="post"  autocomplete="off" data-ajax="false">
                            <div class="col-sm-9 chat-inputbar">
                                <input type="text" name="txtmessage" class="form-control chat-input" placeholder="Enter your text">
                            </div>
                            <div class="col-sm-3 chat-send">
                                <button type="submit" class="btn btn-md btn-primary btn-block waves-effect waves-light">Send</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

                  <!-- chatjs  -->
                  <script src="<?php echo public_path(); ?>asset_unified/pages/jquery.chat.js"></script>
                  <script src="<?php echo public_path(); ?>asset_unified/js/jquery.core.js"></script>

            <?php
            $q = Doctrine_Query::create()
               ->from("MfInvoice a")
               ->where("a.app_id = ?", $application->getId())
               ->addWhere("a.paid != ?", 3); //OTB patch - do not display cancelled invoices to clients
            $invoices = $q->execute();

            foreach($invoices as $invoice) {
                ?>
                <div class="card-box widget-user">
                    <div>
                        <div>
                            <h4 class="m-t-0 m-b-5">Billing Summary</h4>
                            <table class="table  table-card-box">
                                <tbody>
                                <tr>
                                    <td style="width:30%;"><strong>Bill Ref:</strong></td>
                                    <td><?php echo $application->getFormId(); ?>
                                        /<?php echo $application->getEntryId(); ?>/<?php echo $invoice->getId(); ?></td>
                                </tr>
                                <tr>
                                    <td style="width:30%;"><strong>Status:</strong></td>
                                    <td><?php if ($invoice->getPaid() == 2) {
                                            echo __("Paid");
                                        } elseif ($invoice->getPaid() == 3) {
                                            echo __("Failed");
                                        } 
                                        //OTB patch pending confirmation
                                        elseif ($invoice->getPaid() == 15) {
                                         echo __("Pending Confirmation");
                                    }
                                        else {
                                            echo __("Pending");
                                        } ?></td>
                                </tr>
                                <tr>
                                    <td style="width:30%;"><strong>Service:</strong></td>
                                    <td><?php
                                        $q = Doctrine_Query::create()
                                            ->from("ApForms a")
                                            ->where("a.form_id = ?", $application->getFormId());
                                        $form = $q->fetchOne();
                                        if ($form) {
                                            echo $form->getFormName();
                                        }
                                        ?></td>
                                </tr>
                                <tr>
                                    <td style="width:30%;"><strong>Total:</strong></td>
                                    <td><strong><?php echo $invoice->getTotalAmount(); ?></strong></td>
                                </tr>
                                </tbody>
                            </table>

                            <a href="/index.php/invoices/printinvoice/id/<?php echo $invoice->getId(); ?>" class="btn btn-primary waves-effect w-md waves-light"><i class="fa fa-print"></i>
                                Print Invoice</a>

                              <?php
                                              $otbhelper = new OTBHelper();
                                              $form_gateway = $otbhelper->getFormMerchant($application->getFormId());
                                              if($invoice->getPaid() == 1){
                                                  $sf_user->setAttribute('form_id', $application->getFormId());
                                                  $sf_user->setAttribute('entry_id', $application->getEntryId());
                                                  $sf_user->setAttribute('invoice_id', $invoice->getId());
                                              ?>
                                              <!--<button class="btn btn-white" id="makepayment" type="button" onClick="window.location='/index.php/forms/payment';"><i class="fa fa-print mr5"></i> <?php //echo __('Make Payment'); ?></button> -->
                                               
                                                <?php if($otbhelper->invoicePermitsMultiplePayment($invoice->getId()) == 1){ ?>
                                                
                                                <button class="btn btn-warning" id="makepayment" type="button" onClick="window.location='/index.php/forms/payment/gateway/<?php echo 'cash' ?>';"><i class="fa fa-print mr5"></i> <?php echo __('Attach Payment Receipt'); ?></button>
                                                <?php }else { ?>
                                                 <button class="btn btn-success" id="makepayment" type="button" onClick="window.location='/index.php/forms/payment'"><i class="fa fa-print mr5"></i> <?php echo __('Pay Online'); ?></button> 
                                                <?php } ?>
                                               <?php }
                                              if($invoice->getPaid() == 15){
                                                  $sf_user->setAttribute('form_id', $application->getFormId());
                                                  $sf_user->setAttribute('entry_id', $application->getEntryId());
                                                  $sf_user->setAttribute('invoice_id', $invoice->getId());
                                              ?>
                                            <!--  <button class="btn btn-white" id="makepayment" type="button" onClick="window.location='/index.php/forms/payment';"><i class="fa fa-print mr5"></i> <?php //echo __('Add Payment'); ?></button> -->
                                              <?php } ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

            <div class="card-box widget-user">
                <?php echo html_entity_decode($apsettings->getOrganisationHelp()); ?>
            </div>
        </div>
    <?php } ?>

</div><!-- /Content panel-->
<?php
}
else
{
    echo "<div class='contentpanel'><h3>Sorry! You are trying to view a permit that doesn't belong to you</h3></div>";
}

    if($done == 1) {
        ?>
        <!-- Modal -->
        <div class="modal fade" id="submissionsModal" tabindex="-1" role="dialog"
             aria-labelledby="submissionsModalLabel"
             aria-hidden="true" style="margin-top: 15%;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Your application has been received.</h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            <?php
                            //Display success message if available on the form
                            $q = Doctrine_Query::create()
                                ->from("ApForms a")
                                ->where("a.form_id = ?", $application->getFormId());
                            $apform = $q->fetchOne();
                            if ($apform && $apform->getFormSuccessMessage())
                            {                  
                            ?>

                        <div class="alert alert-success">
                            <?php echo $apform->getFormSuccessMessage(); ?>
                        </div>
                        <?php
                        }
                        else {
                            echo "Your application has been submitted. ";
                        }
                        ?>
                        </p>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- modal-content -->
            </div>
            <!-- modal-dialog -->
        </div><!-- modal -->
    <?php
    }
?>
