<?php
/**
 * Dashboard actions.
 *
 * Displays a summary of all application related activity.
 *
 * @package    frontend
 * @subpackage dashboard
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

class dashboardActions extends sfActions
{

	 /**
      * OTB patch --- Addition of missing drafts view
      */
	  public function executeDrafts(sfWebRequest $request){
		if($_SESSION['SESSION_CUTEFLOW_USERID'])
	   {
		   $_SESSION['SESSION_CUTEFLOW_USERID'] = false;
		   $this->getUser()->getAttributeHolder()->clear();
		   $this->getUser()->clearCredentials();
		   $this->getUser()->setAuthenticated(false);
		   $this->getUser()->signout();
		   header("Location: /index.php");
		   exit;
	   }
	   if(!$this->getUser()->isAuthenticated())
	   {
			   $this->redirect('/index.php');
	   }

	   if($request->getParameter("show"))
	   {
			   $this->show = $request->getParameter("show");
	   }
		$q = Doctrine_Query::create()
		  ->from("FormEntry a")
		  ->where("a.user_id = ?", $this->getUser()->getGuardUser()->getId())
		  ->addWhere("a.approved = ?", 0) //drafts only
		  ->orderBy("a.date_of_submission DESC")
		  ->limit(10);
	   $this->latest_draft_applications = $q->execute();
	  
	   $this->setLayout("layoutdash");
   }
     
 	/**
         
         
	* Executes 'Index' action
	*
 	* Displays the client's dashboard
 	*
 	* @param sfRequest $request A request object
 	*/
  	public function executeIndex(sfWebRequest $request)
  	{
        if($_SESSION['SESSION_CUTEFLOW_USERID'])
        {
            $_SESSION['SESSION_CUTEFLOW_USERID'] = false;
            $this->getUser()->getAttributeHolder()->clear();
            $this->getUser()->clearCredentials();
            $this->getUser()->setAuthenticated(false);
            $this->getUser()->signout();
            header("Location: /index.php");
            exit;
        }

    		if(!$this->getUser()->isAuthenticated())
    		{
    			$this->redirect('/index.php');
    		}

    		if($request->getParameter("show"))
    		{
    			$this->show = $request->getParameter("show");
    		}

    		$this->setLayout("layoutdash");


        $q = Doctrine_Query::create()
           ->from("FormEntry a")
					 ->where("a.user_id = ?", $this->getUser()->getGuardUser()->getId())
					 ->andWhere('a.approved <> ?',55)														//*CBS patch* dont display aarchived(rejected apps)
		   		 ->andWhere('a.approved <> ?',56)
           ->orderBy("a.date_of_submission DESC")
           ->limit(10);
        $this->latest_applications = $q->execute();

	   }
}
