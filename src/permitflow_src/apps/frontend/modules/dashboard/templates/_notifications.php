<?php
/**
 * _notifications template.
 *
 * Displays a list of the latest notifications
 *
 * @package    frontend
 * @subpackage notifications
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

$q = Doctrine_Query::create()
    ->from("FormEntry a")
    ->where("a.user_id = ?", $sf_user->getGuardUser()->getId())
    ->andWhere("a.approved <> ?", 0)
    ->andWhere('a.parent_submission = ?', 0)
    ->andWhere("a.declined = 1")
    ->orderBy("a.id DESC")
    ->limit(2);
$corrections_applications = $q->execute();
foreach($corrections_applications as $application)
{
    ?>
    <div class="alert alert-danger">
        <strong><?php echo __("Corrections!");?></strong> <?php echo __("You have an application(s) that has been sent back to corrections. The following are the reasons for rejection:");?>
        <?php
        $q = Doctrine_Query::create()
           ->from("EntryDecline a")
           ->where("a.entry_id = ?", $application->getId())
            ->andWhere("a.resolved = 0");
        $comments = $q->execute();
        //OTB patch 
        $edit_fields = null ;
        //OTB patch - Edit Elements 
                 $otb_helper = new OTBHelper();
                $edit_elements = $otb_helper->getEditFields($application->getId()) ;
        ?>
        <ul>
            <?php foreach($comments as $comment){ ?>
            <li>
                <?php echo $comment->getDescription(); ?> - <?php if($comment->getResolved()){ echo "<span class='label label-success'>".__("Resolved")."</span>"; }else{ echo "<span class='label label-danger'>".__("Not Resolved")."</span>"; } ?> - <strong> <a href="/index.php/application/edit?application_id=<?php echo $application->getId(); ?>"><?php echo __("Click here");?></a> <b style="color:green;"><?php echo __("to edit and resubmit"); ?> <?php echo __("Application") ?> (<?php echo $application->getApplicationId() ?>) </b></strong> 
           
                <?php
                    //populate edit fields
                   $edit_fields = json_decode($comment->getEditFields(), TRUE) ;
                   // OTB patch - Show Fields checked in the backend while declining application -->
                    //Select fields that this user should change as sent by reviewer                  
                  foreach($edit_fields as $fields)
                  {
                    $q_corrections = Doctrine_Query::create()
                           ->from("ApFormElements a")
                           ->where("a.form_id = ?", $application->getFormId())
                     ->andWhereIn("a.element_id", $edit_elements) ; 
                    $q_corrections_r = $q_corrections->execute();
                    //
                    
                    ?>
                 <?php } ?>
                <ul style="list-style-type: square;">
                    <?php foreach($q_corrections_r as $r){ ?>
                    <li style="color:green;"> <?php echo $r->getElementTitle() ?> </li>
                    <?php } ?>
                   
                </ul>
              
                
             </li>
            
            
           <?php } ?>
        </ul>
       
             
            
       <!-- <strong> <a href="/index.php/application/view/id/<?php //echo $application->getId(); ?>"><?php //echo __("Click here");?></a> <b style="color:green;"><?php //echo __("to edit and resubmit."); ?> </b></strong> -->
          
    </div>
    <?php
}
?>


<?php
$q = Doctrine_Query::create()
    ->from("FormEntry a")
    ->where("a.circulation_id = ?", $sf_user->getGuardUser()->getId())
    ->limit(2);
$transferring_applications = $q->execute();

foreach($transferring_applications as $application)
{
    $origin_user = Doctrine_Core::getTable("SfGuardUser")->find($application->getUserId());
    $origin_user_profile = $origin_user->getProfile();

    $id = $application->getId();
    $data = json_encode(array('id' => $id));
    $encryptdata = base64_encode($data);
    ?>
    <div class="alert alert-success">
        <h4><?php echo __("Transfer!");?></h4> <?php echo __("You have an application");?> (<?php echo $application->getApplicationId(); ?> ) <?php echo __("that is being transferred to you from ")?><?php echo $origin_user_profile->getFullname()." (".$origin_user_profile->getEmail().")"; ?>. <br><br>
        <a href="/index.php/application/accepttransfer/code/<?php echo $encryptdata; ?>" class="btn btn-success"><?php echo __("Confirm Transfer");?></a>  <a class="btn btn-danger" href="/index.php/application/canceltransfer/code/<?php echo $encryptdata; ?>"><?php echo __("Cancel Transfer");?></a>
    </div>
    <?php
}
?>
