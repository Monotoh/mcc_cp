<?php
  use_helper('I18N');

  $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  error_log('Debug-x callback: '.$actual_link."-".$sf_user->getGuardUser()->getProfile()->getFullname());

  $payments_manager = new PaymentsManager();
  $invoice_manager = new InvoiceManager();

?>
<div class="contentpanel">
  <div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <div class="panel-btns">
              <a href="" class="panel-close">×</a>
              <a href="" class="minimize">−</a>
            </div><!-- panel-btns -->
            <h4 class="panel-title">Payment</h4>
            <p>Thank you for using our payment services</p>
          </div><!-- panel-heading -->
          <div class="panel-body">
            <?php
              $invoice = $invoice_manager->get_invoice_by_id($sf_user->getAttribute('invoice_id'));

              if($payments_manager->authorize_validation($sf_user->getAttribute('invoice_id')))
              {
                if($payments_manager->process_validation($sf_user->getAttribute('invoice_id'), $_REQUEST))
                {
                   //Check invoice status and display notifications/messages
                   if($invoice->getPaid() == 2)
                   {
                      $link = "/index.php/application/view/id/".$invoice->getFormEntry()->getId();
                      ?>
                      <div class="alert alert-success" align="center">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong>Successful Payment!</strong> You are being redirected to your service..... <br>
                        <a href="<?php echo $link; ?>" class="alert-link">Click here to view your service</a>
                      </div>
                      <script type="text/javascript">   
                      function Redirect() 
                      {  
                        window.top.location.href="<?php echo $link; ?>"; 
                      } 
                      setTimeout('Redirect()', 5000);   
                      </script>
                      <?php
                   }
                   elseif($invoice->getPaid() == 3)
                   {
                      $link = "/index.php/invoices/view/id/".$invoice->getId();
                      ?>
                      <div class="alert alert-danger" align="center">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong>Failed Payment!</strong> You are being redirected to your invoice..... <br>
                        <a href="<?php echo $link; ?>" class="alert-link">Click here to view your invoice</a>
                      </div>
                      <script type="text/javascript">   
                      function Redirect() 
                      {  
                        window.top.location.href="<?php echo $link; ?>"; 
                      } 
                      setTimeout('Redirect()', 5000);   
                      </script>
                      <?php
                   }
                   else
                   {
                      $link = "/index.php/application/view/id/".$invoice->getFormEntry()->getId();
                      ?>
                      <div class="alert alert-info" align="center">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong>Payment is awaiting confirmation!</strong> It may take a few minutes to confirm your payment. <br> You are being redirected to your service..... <br>
                        <a href="<?php echo $link; ?>" class="alert-link">Click here to view your service</a>
                      </div>
                      <script type="text/javascript">   
                      function Redirect() 
                      {  
                        window.top.location.href="<?php echo $link; ?>"; 
                      } 
                      setTimeout('Redirect()', 5000);   
                      </script>
                      <?php
                   }
               
               }
                else
                {
                    $link = "/index.php/application/view/id/".$invoice->getFormEntry()->getId();
                      ?>
                      <div class="alert alert-info" align="center">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong>Payment is awaiting confirmation!</strong> It may take a few minutes to confirm your payment. <br> You are being redirected to your service..... <br>
                        <a href="<?php echo $link; ?>" class="alert-link">Click here to view your service</a>
                      </div>
                      <script type="text/javascript">   
                      function Redirect() 
                      {  
                        window.top.location.href="<?php echo $link; ?>"; 
                      } 
                      setTimeout('Redirect()', 2000);   
                      </script>
                      <?php
                }
              }
              else
              {
                    $link = "/index.php/application/view/id/".$invoice->getFormEntry()->getId();
                      ?>
                      <div class="alert alert-info" align="center">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong>Payment is awaiting confirmation!</strong> It may take a few minutes to confirm your payment. <br> You are being redirected to your service..... <br>
                        <a href="<?php echo $link; ?>" class="alert-link">Click here to view your service</a>
                      </div>
                      <script type="text/javascript">   
                      function Redirect() 
                      {  
                        window.top.location.href="<?php echo $link; ?>"; 
                      } 
                      setTimeout('Redirect()', 2000);   
                      </script>
                      <?php
              }

              $sf_user->setAttribute('invoice_id', 0);
            ?>     
        </div>
      </div><!-- panel -->
    </div>
  </div>
</div>
