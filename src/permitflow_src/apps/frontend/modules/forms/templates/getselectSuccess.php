<?php
/**
 * getselectSuccess.php template.
 *
 * Ajax for application form that retrieves comboboxes
 *
 * @package    frontend
 * @subpackage forms
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
 ?>
<script type="text/javascript" charset="utf-8" src="/asset_js/jquery.uniform.min.js" ></script>
<script type="text/javascript">
	$(function(){
		$("input, textarea, select, button").uniform();
	});
</script>
<?php 

$searchid = $_POST['element']; 

$formid = $_POST['formid'];
$column1 = $_POST['field1'];
$column2 = $_POST['field2'];

$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
							mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
$query = "SELECT * FROM ap_form_".$formid."_".$column1."_".$column2." WHERE element_".$column1." = '".$searchid."'";
$results = mysql_query($query,$dbconn);


$query1 = "SELECT * FROM ap_element_options WHERE form_id = '".$formid."' AND element_id = '".$column2."' AND live='1'";
$results1 = mysql_query($query1,$dbconn);


$onclick = "";
        
        $query = "SELECT * FROM ap_field_relations WHERE formid='".$formid."' AND field1='".$column2."'";
        $results2 = mysql_query($query,$dbconn);
		
		$onclick .= "onclick=\"";
		
		$count = 1;
		while($row2 = mysql_fetch_assoc($results2))
		{
			$onclick .= "updateSelect".$count."('/index.php/forms/getselect','".$formid."','".$row2['field1']."','".$row2['field2']."');";
			$count++;
		}
		
		$onclick .= "\"";
		

?>
<select class="element select medium" id="element_<?php echo $column2;  ?>" name="element_<?php echo $column2;  ?>" <?php echo $onclick; ?>> 
<option value="">Choose option...</option>
<?php
while($row = mysql_fetch_assoc($results))
{
   while($row1 = mysql_fetch_assoc($results1))
   {
        if($row['element_'.$column2] == $row1['option_id'])
		{
			echo "<option value='".$row1['option_id']."'>".$row1['option']."</option>";
			break;
		}
   }
}
?>
</select>
