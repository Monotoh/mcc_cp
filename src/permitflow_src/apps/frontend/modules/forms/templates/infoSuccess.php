<?php

/**
 * viewSuccess.php template.
 *
 * Displays a dynamically generated application form
 *
 * @package    frontend
 * @subpackage forms
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

use_helper('I18N');

//Check if backend user is logged in, if so log them out
if($sf_user->isAuthenticated()){
    if($sf_user->getGuardUser()->getId() == '1')
    {
        $sf_user->signout();
        header("Location: /index.php");
    }
    else
    {
        $_SESSION['userid'] = $sf_user->getGuardUser()->getId();
    }
}

$q = Doctrine_Query::create()
    ->from('ApForms a')
    ->where('a.form_id = ?', $_GET['id']);
$formObj = $q->fetchOne();
$form_name = $formObj->getFormName();
$form_description = $formObj->getFormDescription();

?>
<div class="pageheader">
       <h2><i class="fa fa-edit"></i><?php echo __('Application Forms'); ?><span> <?php echo __('Apply Now'); ?></span></h2>
      <div class="breadcrumb-wrapper">

        <ol class="breadcrumb">
          <li><a href=""><?php echo __('Application Forms'); ?></a></li>
          <li class="active"><?php echo $form_name; ?></li>
        </ol>
      </div>
    </div>


<div class="contentpanel">
<div class="row blog-content">
      <div class="col-sm-8">

        <div class="panel panel-default panel-blog">
          <div class="panel-body">
            <h3 class="blogsingle-title"><?php echo $form_name; ?></h3>


            <div class="mb20"></div>

            <?php echo html_entity_decode($form_description); ?>

           </div><!-- panel-body -->
           <div class="panel-footer">
            <a class="btn btn-warning" href="<?php echo public_path(); ?>index.php/forms/view?id=<?php echo $formObj->getFormId(); ?>"><?php echo __('Apply Now'); ?></a>   
           </div>

        </div><!-- panel -->




      </div><!-- col-sm-8 -->

      <div class="col-sm-4">
        <div class="blog-sidebar">
          <?php
            $q = Doctrine_Query::create()
                ->from('FormGroups a')
                ->orderBy('a.group_name ASC');
            $groups = $q->execute();
            foreach($groups as $group)
            {
              //Check if the user has access to any form in this category, if none then don't display category
              $q = Doctrine_Query::create()
                  ->from('ApForms a')
                  ->leftJoin('a.SfGuardUserCategoriesForms b')
                  ->where('a.form_group = ?', $group->getGroupId())
                  ->andWhere('a.form_type = 1')
                  ->andWhere('a.form_active = 1')
                  ->andWhere('b.categoryid = ?', $sf_user->getGuardUser()->getProfile()->getRegisteras())
                  ->orderBy('a.form_name ASC');
              $group_forms = $q->count();

              if($group_forms == 0)
              {
                 continue;
              }
          ?>
              <h5 class="subtitle"><?php echo $group->getGroupName(); ?></h5>
              <ul class="sidebar-list">
              <?php
                $q = Doctrine_Query::create()
                    ->from('ApForms a')
                    ->where('a.form_group = ?', $group->getGroupId())
                    ->andWhere('a.form_type = 1')
                    ->andWhere('a.form_active = 1')
                    ->orderBy('a.form_name ASC');
                $applications = $q->execute();
                $count = 1;
                foreach($applications as $application)
                {
                  //Check if enable_categories is set, if it is then filter application forms
                  if(sfConfig::get('app_enable_categories') == "yes")
                  {
                      $q = Doctrine_Query::create()
                          ->from('sfGuardUserCategoriesForms a')
                          ->where('a.categoryid = ?', $sf_user->getGuardUser()->getProfile()->getRegisteras())
                          ->andWhere('a.formid = ?', $application->getFormId());
                      $category = $q->count();

                      if ($category == 0) {
                          continue;
                      }
                  }
                ?>
                <li><a href="<?php echo public_path(); ?>index.php/forms/info?id=<?php echo $application->getFormId(); ?>"><i class="fa fa-angle-right"></i> <?php echo $application->getFormName(); ?></a></li>
                <?php
                }
                ?>
              </ul>

              <div class="mb30"></div>
           <?php
            }
           ?>
        </div><!-- blog-sidebar -->

      </div><!-- col-sm-4 -->

      </div><!-- row -->
</div><!-- /.content -->



</div><!-- /.marketing -->
