<?php
  try
  {
    $prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";
    include_once($prefix_folder.'includes/OAuth.php');

    require($prefix_folder.'config.php');
    require($prefix_folder.'includes/language.php');
    require($prefix_folder.'includes/db-core.php');
    require($prefix_folder.'includes/common-validator.php');
    require($prefix_folder.'includes/view-functions.php');
    require($prefix_folder.'includes/post-functions.php');
    require($prefix_folder.'includes/filter-functions.php');
    require($prefix_folder.'includes/entry-functions.php');
    require($prefix_folder.'includes/helper-functions.php');
    require($prefix_folder.'includes/theme-functions.php');
    require($prefix_folder.'lib/recaptchalib.php');
    require($prefix_folder.'lib/php-captcha/php-captcha.inc.php');
    require($prefix_folder.'lib/text-captcha.php');
    require($prefix_folder.'hooks/custom_hooks.php');


    $dbh    = mf_connect_db();

    $mf_settings = mf_get_settings($dbh);

    $actual_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    error_log('Jambopay IPN Debug-y callback: '.$actual_link);

    $ipn_status = "CHECKING";

    if (isset($_POST['JP_PASSWORD'])) {
        $JP_TRANID = $_POST['JP_TRANID'];
        $JP_MERCHANT_ORDERID = $_POST['JP_MERCHANT_ORDERID'];
        $JP_ITEM_NAME = $_POST['JP_ITEM_NAME'];
        $JP_AMOUNT = $_POST['JP_AMOUNT'];
        $JP_CURRENCY = $_POST['JP_CURRENCY'];
        $JP_TIMESTAMP = $_POST['JP_TIMESTAMP'];
        $JP_PASSWORD = $_POST['JP_PASSWORD'];
        $JP_CHANNEL = $_POST['JP_CHANNEL'];

        $submission_details = explode("/", $JP_MERCHANT_ORDERID);
        $form_id = $submission_details[0];
        $entry_id = $submission_details[1];

        $query  = "select
              payment_jambopay_business,
              payment_jambopay_shared_key
             from
                ".MF_TABLE_PREFIX."forms
            where
               form_id = ?";
        $params = array($form_id);

        $sth = mf_do_query($query,$params,$dbh);
        $form_details = mf_do_fetch_result($sth);

        $sharedkey = $form_details['payment_jambopay_shared_key'];
        $str = $JP_MERCHANT_ORDERID . $JP_AMOUNT . $JP_CURRENCY . $sharedkey . $JP_TIMESTAMP;

        if (md5(utf8_encode($str)) == $JP_PASSWORD) {
            error_log("JamboPay IPN Debug-y: JP Password Approved");

            $q = Doctrine_Query::create()
                ->from('FormEntry a')
                ->where('a.form_id = ? AND a.entry_id = ?', array($form_id,$entry_id));
            $application = $q->fetchOne();
            
            if($application)
            {
                //Get user details
                $q = Doctrine_Query::create()
                    ->from('sfGuardUserProfile a')
                    ->where('a.user_id = ?', $application->getUserId());
                $user = $q->fetchOne();

                //approve invoice
                foreach($application->getMfInvoice() as $invoice)
                {
                    if($invoice->getPaid() == 2)
                    {
                        error_log("JamboPay IPN Debug-y: Paid invoice found for ".$JP_MERCHANT_ORDERID);
                        $ipn_status = "OK";
                    }
                    else
                    {
                        error_log("JamboPay IPN Debug-y: Unpaid invoice found for ".$JP_MERCHANT_ORDERID);

                        $invoice->setPaid(2);
                        $invoice->setUpdatedAt(date("Y-m-d"));
                        $invoice->save();

                        error_log("JamboPay Debug-y: JP Payment Status: Paid");

                        $query = "INSERT INTO `".MF_TABLE_PREFIX."form_payments`(
                       `form_id`,
                       `record_id`,
                       `billing_state`,
                       `payment_id`,
                       `date_created`,
                       `payment_date`,
                       `payment_status`,
                       `payment_fullname`,
                       `payment_amount`,
                       `payment_currency`,
                       `payment_test_mode`,
                       `payment_merchant_type`,
                       `status`
                       )
                   VALUES (
                       :form_id,
                       :record_id,
                       :billing_state,
                       :payment_id,
                       :date_created,
                       :payment_date,
                       :payment_status,
                       :payment_fullname,
                       :payment_amount,
                       :payment_currency,
                       :payment_test_mode,
                       :payment_merchant_type,
                       :status
                       )";


                        $params = array();
                        $params[':form_id']             = $form_id;
                        $params[':record_id']           = $entry_id;
                        $params[':billing_state']       = $JP_TRANID;
                        $params[':payment_id']          = $JP_MERCHANT_ORDERID;
                        $params[':date_created']        = date("Y-m-d H:i:s");
                        $params[':payment_date']        = date("Y-m-d H:i:s");

                        $params[':payment_status']      = 'paid';
                       
                        $params[':payment_fullname']    = trim($user->getFullname());
                        $params[':payment_amount']      = $invoice->getTotalAmount();
                        $params[':payment_currency']    = $invoice->getCurrency();

                        if($use_paypal_sandbox){
                            $params[':payment_test_mode']   = 1;
                        }else{
                            $params[':payment_test_mode']   = 0;
                        }

                        $params[':payment_merchant_type'] = 'JamboPay';
                        $params[':status']                = 1;

                        mf_do_query($query,$params,$dbh);

                        $ipn_status = "OK";
                    }
                }
            }
            else
            {
                error_log("JamboPay IPN Debug-y: Application Not Found for ".$JP_MERCHANT_ORDERID);
                $ipn_status = "Failed";
            }

        } else{
            error_log("JamboPay IPN Debug-y: WRONG JP Password ".$JP_PASSWORD." for ".$JP_MERCHANT_ORDERID);
            $ipn_status = "Failed";

            $submission_details = explode("/", $JP_MERCHANT_ORDERID);
            $form_id = $submission_details[0];
            $entry_id = $submission_details[1];

            //Mark transaction as failed
            $q = Doctrine_Query::create()
                ->from('FormEntry a')
                ->where('a.form_id = ? AND a.entry_id = ?', array($form_id,$entry_id));
            $application = $q->fetchOne();

            if($application)
            {
                //Get user details
                $qt = Doctrine_Query::create()
                    ->from('sfGuardUserProfile a')
                    ->where('a.user_id = ?', $application->getUserId());
                $user = $qt->fetchOne();

                //approve invoice
                foreach($application->getMfInvoice() as $invoice)
                {
                    if($invoice->getPaid() == 3)
                    {
                        error_log("JamboPay IPN Debug-y: Cancelled invoice found for ".$JP_MERCHANT_ORDERID);
                        $ipn_status = "Failed";
                    }
                    else
                    {
                        error_log("JamboPay IPN Debug-y: Unpaid invoice found for ".$JP_MERCHANT_ORDERID);

                        $invoice->setPaid(3);
                        $invoice->setUpdatedAt(date("Y-m-d"));
                        $invoice->save();

                        error_log("JamboPay Debug-y: JP Payment Status: Failed");

                        $query = "INSERT INTO `".MF_TABLE_PREFIX."form_payments`(
                       `form_id`,
                       `record_id`,
                       `billing_state`,
                       `payment_id`,
                       `date_created`,
                       `payment_date`,
                       `payment_status`,
                       `payment_fullname`,
                       `payment_amount`,
                       `payment_currency`,
                       `payment_test_mode`,
                       `payment_merchant_type`,
                       `status`
                       )
                   VALUES (
                       :form_id,
                       :record_id,
                       :billing_state,
                       :payment_id,
                       :date_created,
                       :payment_date,
                       :payment_status,
                       :payment_fullname,
                       :payment_amount,
                       :payment_currency,
                       :payment_test_mode,
                       :payment_merchant_type,
                       :status
                       )";


                        $params = array();
                        $params[':form_id']             = $form_id;
                        $params[':record_id']           = $entry_id;
                        $params[':billing_state']       = $JP_TRANID;
                        $params[':payment_id']          = $JP_MERCHANT_ORDERID;
                        $params[':date_created']        = date("Y-m-d H:i:s");
                        $params[':payment_date']        = date("Y-m-d H:i:s");

                        $params[':payment_status']      = 'failed';

                        $params[':payment_fullname']    = trim($user->getFullname());
                        $params[':payment_amount']      = $invoice->getTotalAmount();
                        $params[':payment_currency']    = $invoice->getCurrency();

                        if($use_paypal_sandbox){
                            $params[':payment_test_mode']   = 1;
                        }else{
                            $params[':payment_test_mode']   = 0;
                        }

                        $params[':payment_merchant_type'] = 'JamboPay';
                        $params[':status']                = 1;

                        mf_do_query($query,$params,$dbh);

                        $ipn_status = "Failed";
                    }

                }
            }
        }
    }
    else
    {
        error_log("JamboPay IPN Debug-y: JP Password not found");
    }

    echo $ipn_status;
  }
  catch(Exception $ex)
  {
     error_log("JamboPay IPN Debug-y Bug: ".$ex);
  }
?>
