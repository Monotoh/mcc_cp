<?php
  use_helper('I18N');
?>
<div class="row">
    <div class="col-lg-12">
        <?php

          $user = $sf_user->getGuardUser();

          $form_id 	  = $sf_user->getAttribute('form_id');
          $record_id   = $sf_user->getAttribute('entry_id');

          $application_manager = new ApplicationManager();
          $invoice_manager = new InvoiceManager();
          $payments_manager = new PaymentsManager();

          if(empty($record_id) && $_SESSION['mf_payment_record_id'][$form_id])
          {
              $record_id = $_SESSION['mf_payment_record_id'][$form_id];
          }

          if($_POST['partial_amount'])
          {
              $_SESSION['partial_amount'] = $_POST['partial_amount'];
          }
          else
          {
              $_SESSION['partial_amount'] = false;
          }

          //Check if an application already exists for the form submission to prevent double entry
          if ($application_manager->application_exists($form_id, $record_id)) {
              //If save as draft/resume later was clicked then do nothing
              $submission = $application_manager->get_application($form_id, $record_id);
          } else {
              //If save as draft/resume later was clicked then create draft application
              $submission = $application_manager->create_application($form_id, $record_id, $sf_user->getGuardUser()->getId(), true);
          }

          //Check if application form record is valid, if it is not then redirect client to edit application
          $current_form_id = $submission->getFormId();
          $current_record_id = $submission->getEntryId();

          if(empty($current_form_id) || empty($current_record_id))
          {
            $form = $application_manager->create_adhoc_edit_by_form($submission->getId(), $form_id);
          }

          $application_manager->update_invoices($submission->getId());

          if ($invoice_manager->can_make_partial_payment($submission->getId()) && $_SESSION['partial_amount'] == false) {
              //Display a form the user can use to select amount they want to pay
              $invoice = "";
              if ($sf_user->getAttribute('invoice_id')) {
                  $invoice = $invoice_manager->get_invoice_by_id($sf_user->getAttribute('invoice_id'));
              } else {
                  $invoice = $invoice_manager->get_invoice_by_reference($submission->getFormId() . "/" . $submission->getEntryId() . "/1");

                  if($invoice)
                  {
                      $sf_user->setAttribute('invoice_id', $invoice->getId());
                  }
              }
              $total_amount_remaining = $invoice_manager->get_invoice_total_owed($invoice->getId());
              $currency = $invoice->getCurrency();
              include_partial('chooseamount', array('total_amount_remaining' => $total_amount_remaining, 'currency' => $currency));
          } else {

              $invoice = $invoice_manager->get_invoice_by_id($sf_user->getAttribute('invoice_id'));

              if($invoice)
              {
                  $invoice = $invoice_manager->get_invoice_by_id($sf_user->getAttribute('invoice_id'));

                  if($invoice->getPaid() == 2)
                  {
                      header("Location: /index.php/invoices/view/id/".$invoice->getId());
                      exit;
                  }
                  else
                  {
                      if($invoice_manager->can_make_partial_payment($submission->getId())) {
                          $invoice = $invoice_manager->update_invoice_partial($invoice->getId(), $_SESSION['partial_amount']);
                          echo $payments_manager->display_partial_checkout($invoice->getId(), false);
                      }
                      else
                      {
                          echo $payments_manager->display_checkout($invoice->getId(), false,$gateway);
                      }
                  }
              }
              else
              {
                  $invoice = $invoice_manager->get_invoice_by_reference($form_id . "/" . $record_id . "/1");

                  if($invoice)
                  {
                      $sf_user->setAttribute('invoice_id', $invoice->getId());

                      if($invoice_manager->can_make_partial_payment($submission->getId())) {
                          $invoice = $invoice_manager->update_invoice_partial($invoice->getId(), $_SESSION['partial_amount']);
                          echo $payments_manager->display_partial_checkout($invoice->getId(), false);
                      }
                      else
                      {
                          echo $payments_manager->display_checkout($invoice->getId(), false, $gateway);
                      }
                  }
                  else
                  {
                      echo __("No Invoice");
                  }
              }
          }
        ?>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
    </div>
</div>
