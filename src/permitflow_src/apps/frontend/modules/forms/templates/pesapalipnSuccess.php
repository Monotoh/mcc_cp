<?php

$prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";
include_once($prefix_folder.'includes/OAuth.php');

require($prefix_folder.'config.php');
require($prefix_folder.'includes/language.php');
require($prefix_folder.'includes/db-core.php');
require($prefix_folder.'includes/common-validator.php');
require($prefix_folder.'includes/view-functions.php');
require($prefix_folder.'includes/post-functions.php');
require($prefix_folder.'includes/filter-functions.php');
require($prefix_folder.'includes/entry-functions.php');
require($prefix_folder.'includes/helper-functions.php');
require($prefix_folder.'includes/theme-functions.php');
require($prefix_folder.'lib/recaptchalib.php');
require($prefix_folder.'lib/php-captcha/php-captcha.inc.php');
require($prefix_folder.'lib/text-captcha.php');
require($prefix_folder.'hooks/custom_hooks.php');

$dbh 		= mf_connect_db();

$mf_settings = mf_get_settings($dbh);

// Parameters sent to you by PesaPal IPN
$pesapalNotification=$_GET['pesapal_notification_type'];
$pesapalTrackingId=$_GET['pesapal_transaction_tracking_id'];
$pesapal_merchant_reference=$_GET['pesapal_merchant_reference'];

$mailnotification = new mailnotifications();

if($pesapalNotification=="CHANGE" && $pesapalTrackingId!='')
{
  $exploded = explode('/', $pesapal_merchant_reference); //the "custom" variable from PayPal format: xx_yy_zzzzzzzz (xx: form_id, yy: entry_id, zzz: unix_timestamp of the date_created field)
  $form_id  		 = (int) $exploded[0];
  $entry_id 		 = $exploded[1];
  $entry_timestamp = date('Y/m/d');

  error_log('EC_LOG: Form ID: '.$form_id.', Entry ID: '.$entry_id);

   //get form properties data
   $query 	= "select
             form_name,
             form_has_css,
             form_redirect,
             form_language,
             form_review,
             form_review_primary_text,
             form_review_secondary_text,
             form_review_primary_img,
             form_review_secondary_img,
             form_review_use_image,
             form_review_title,
             form_review_description,
             form_resume_enable,
             form_page_total,
             form_lastpage_title,
             form_pagination_type,
             form_theme_id,
             payment_show_total,
             payment_total_location,
             payment_enable_merchant,
             payment_merchant_type,
             payment_currency,
             payment_price_type,
             payment_price_name,
             payment_price_amount,
             payment_ask_billing,
             payment_ask_shipping,
             payment_pesapal_live_secret_key,
             payment_pesapal_live_public_key,
             payment_pesapal_test_secret_key,
             payment_pesapal_test_public_key,
             payment_pesapal_enable_test_mode,
             payment_enable_recurring,
             payment_recurring_cycle,
             payment_recurring_unit,
             payment_enable_trial,
             payment_trial_period,
             payment_trial_unit,
             payment_trial_amount,
             payment_delay_notifications,
             payment_enable_tax,
             payment_tax_rate,
             payment_tax_amount
            from
               ".MF_TABLE_PREFIX."forms
           where
              form_id = ?";
   $params = array($form_id);

   $sth = mf_do_query($query,$params,$dbh);
   $row = mf_do_fetch_result($sth);

   $consumer_key = "";
   $consumer_secret = "";
   $statusrequestAPI = "";

   if($row['payment_pesapal_enable_test_mode'])
   {
   $consumer_key=$row['payment_pesapal_test_secret_key'];//Register a merchant account on
                      //demo.pesapal.com and use the merchant key for testing.
                      //When you are ready to go live make sure you change the key to the live account
                      //registered on www.pesapal.com!
   $consumer_secret=$row['payment_pesapal_test_public_key'];// Use the secret from your test
                      //account on demo.pesapal.com. When you are ready to go live make sure you
                      //change the secret to the live account registered on www.pesapal.com!
   $statusrequestAPI = 'http://demo.pesapal.com/api/querypaymentstatus';//change to
   }
   else
   {
   $consumer_key=$row['payment_pesapal_live_secret_key'];//Register a merchant account on
                      //demo.pesapal.com and use the merchant key for testing.
                      //When you are ready to go live make sure you change the key to the live account
                      //registered on www.pesapal.com!
   $consumer_secret=$row['payment_pesapal_live_public_key'];// Use the secret from your test
                      //account on demo.pesapal.com. When you are ready to go live make sure you
                      //change the secret to the live account registered on www.pesapal.com!
   $statusrequestAPI = 'https://www.pesapal.com/api/querypaymentstatus';//change to
   }

   $token = $params = NULL;
   $consumer = new OAuthConsumer($consumer_key, $consumer_secret);
   $signature_method = new OAuthSignatureMethod_HMAC_SHA1();

   //get transaction status
   $request_status = OAuthRequest::from_consumer_and_token($consumer, $token, "GET", $statusrequestAPI, $params);
   $request_status->set_parameter("pesapal_merchant_reference", $pesapal_merchant_reference);
   $request_status->set_parameter("pesapal_transaction_tracking_id",$pesapalTrackingId);
   $request_status->sign_request($signature_method, $consumer, $token);

   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $request_status);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt($ch, CURLOPT_HEADER, 1);
   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
   if(defined('CURL_PROXY_REQUIRED')) if (CURL_PROXY_REQUIRED == 'True')
   {
      $proxy_tunnel_flag = (defined('CURL_PROXY_TUNNEL_FLAG') && strtoupper(CURL_PROXY_TUNNEL_FLAG) == 'FALSE') ? false : true;
      curl_setopt ($ch, CURLOPT_HTTPPROXYTUNNEL, $proxy_tunnel_flag);
      curl_setopt ($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
      curl_setopt ($ch, CURLOPT_PROXY, CURL_PROXY_SERVER_DETAILS);
   }

   $response = curl_exec($ch);

   error_log('EC_LOG: IPN Response:'.$response);

   $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
   $raw_header  = substr($response, 0, $header_size - 4);
   $headerArray = explode("\r\n\r\n", $raw_header);
   $header      = $headerArray[count($headerArray) - 1];

   //transaction status
   $elements = preg_split("/=/",substr($response, $header_size));
   $status = $elements[1];

   curl_close ($ch);

   //UPDATE YOUR DB TABLE WITH NEW STATUS FOR TRANSACTION WITH pesapal_transaction_tracking_id $pesapalTrackingId
   error_log('EC_LOG: Valid IPN with status:'.$status);

   $error_message = '';

   //parse the "custom" variable and make sure it's a valid entry within the database
   $entry_timestamp = date('Y/m/d');


     error_log('EC_LOG: Verification completed. Update/insert into table');

     //otherwise update/insert into ap_form_payments table with the completed status
     $query = "select count(afp_id) record_exist from ".MF_TABLE_PREFIX."form_payments where form_id = ? and record_id = ? and `status` = 1";
     $params = array($form_id,$entry_id);
     $sth = mf_do_query($query,$params,$dbh);
     $row = mf_do_fetch_result($sth);

     if(!empty($row['record_exist']) && $status == "COMPLETED"){

       error_log('EC_LOG: Updating form_payments table');

       //do update to ap_form_payments table
       $query = "update ".MF_TABLE_PREFIX."form_payments set payment_status = ? where form_id = ? and record_id = ? and `status` = 1";
       $params = array("paid",$form_id,$entry_id);
       mf_do_query($query,$params,$dbh);

       error_log('EC_LOG: Done updating form_payments table');

       //Here you can find the latest invoice and update status to paid and generate permit
       $q = Doctrine_Query::create()
          ->from("FormEntry a")
          ->where("a.form_id = ? and a.entry_id = ?", array($form_id, $entry_id));
       $application = $q->fetchOne();
       if($application)
       {
         $q = Doctrine_Query::create()
            ->from("MfInvoice a")
            ->where("a.app_id = ?", $application->getId())
            ->andWhere("a.paid <> 2")
            ->orderBy("a.id DESC");
         $invoice = $q->fetchOne();
         if($invoice)
         {
             $invoice->setPaid(2);
             $invoice->save();
             error_log('EC_LOG: Invoice marked as paid with payment status: '.$status);

             $q = Doctrine_Query::create()
                ->from("Permits a")
                ->where("a.applicationstage = ?", $application->getApproved())
                ->andWhere("a.applicationform = ?", $application->getFormId());
             $permit = $q->fetchOne();

             if($permit)
             {
                 //Ignore and continue
             }
             else
             {
                 //Check for permits assigned to duplicate stages
                 $q = Doctrine_Query::create()
                     ->from("Permits a")
                     ->where("a.applicationform = ?", $application->getFormId());
                 $diff_permit = $q->fetchOne();

                 if($diff_permit)
                 {
                     $q = Doctrine_Query::create()
                         ->from('SubMenus a')
                         ->where('a.id = ?', $diff_permit->getApplicationstage());
                     $original_stage = $q->fetchOne();

                     $q = Doctrine_Query::create()
                         ->from('SubMenus a')
                         ->where('a.id = ?', $application->getApproved());
                     $current_stage = $q->fetchOne();

                     if($original_stage->getTitle() == $current_stage->getTitle())
                     {
                         $permit = $diff_permit;
                     }
                 }
             }

             if($permit)
             {
                   $date_of_response = date("Y-m-d H:i:s");
                   $date_of_issue = $date_of_response;

                   $application->setSavedPermit($permit->getContent());
                   $application->setDateOfResponse(date("Y-m-d H:i:s"));
                   $application->setDateOfIssue(date("Y-m-d H:i:s"));

                   $application->save();

                   //New code to handle more than one permit per application
                   $newpermit = new SavedPermit();
                   $newpermit->setTypeId($permit->getId());
                   $newpermit->setApplicationId($application->getId());
                   $newpermit->setPermit($permit->getContent());
                   $newpermit->setDateOfIssue($date_of_issue);
                   $q = Doctrine_Query::create()
                      ->from("Permits a")
                      ->where("a.applicationform = ?", $application->getFormId());
                   $template = $q->fetchOne();
                   if($template)
                   {
                      if($template->getMaxDuration() > 0)
                      {
                         $date = strtotime("+".$template->getMaxDuration()." day");
                         $newpermit->setDateOfExpiry(date('Y-m-d', $date));
                      }
                   }
                   $newpermit->setCreatedBy("");
                   $newpermit->setLastUpdated($date_of_issue);

                   //if permit template has its own unique identifier then assign it
                   if($permit->getFooter())
                   {
                      $q = Doctrine_Query::create()
                         ->from('SavedPermit a')
                         ->where('a.type_id = ?', $permit->getId())
                         ->orderBy("a.permit_id DESC");
                      $last_permit = $q->fetchOne();

                      $new_permit_id = ""; //submission identifier
                      $identifier_start = ""; //first stage

                      if($last_permit && $last_permit->getPermitId())
                      {
                          $new_permit_id = $last_permit->getPermitId();
                          $new_permit_id = ++$new_permit_id;
                      }
                      else
                      {
                          $new_permit_id = $permit->getFooter();
                      }
                      $newpermit->setPermitId($new_permit_id);
                   }
                   else
                   {
                     $newpermit->setPermitId($application->getApplicationId());
                   }

                   $newpermit->save();
                   //Send email about downloading your permit

                   $q = Doctrine_Query::create()
                      ->from('sfGuardUserProfile a')
                      ->where('a.user_id = ?', $application->getUserId());
                   $userprofile = $q->fetchOne();

                   $q = Doctrine_Query::create()
                      ->from('sfGuardUser a')
                      ->where('a.id = ?', $application->getUserId());
                   $user = $q->fetchOne();

                   $body = "
                     Hi ".$userprofile->getFullname().",
                     <br>
                     Please log into your account and click the link below to download your permit:
                     <a href='http://".$_SERVER['HTTP_HOST']."/index.php/permits/view/id/".$newpermit->getId()."'>".$newpermit->getPermitId()."</a>
                     <br>
                     <br>
                     --
                     <br>
                     Thanks,<br>
                     eCiziten, ".sfConfig::get('app_organisation_name').".

                     ";

                    $mailnotification->sendemail(sfConfig::get('app_organisation_email'),$userprofile->getEmail(),"Your permit is ready",$body);


                   error_log('EC_LOG: Sent mail to user about permit');

                   //Save to notification history
                   $ntf_arch = new NotificationHistory();
                   $ntf_arch->setUserId($user->getId());
                   $ntf_arch->setApplicationId($application->getId());
                   $ntf_arch->setNotification($body);
                   $ntf_arch->setNotificationType('email');
                   $ntf_arch->setSentOn(''.date('Y-m-d').'');
                   $ntf_arch->setConfirmedReceipt('0');
                   $ntf_arch->save();
             }

             //Send email about successful payment

             $q = Doctrine_Query::create()
                ->from('sfGuardUserProfile a')
                ->where('a.user_id = ?', $application->getUserId());
             $userprofile = $q->fetchOne();

             $q = Doctrine_Query::create()
                ->from('sfGuardUser a')
                ->where('a.id = ?', $application->getUserId());
             $user = $q->fetchOne();

             $body = "
               Hi ".$userprofile->getFullname().",<br>
              <br>
               Your payment for the following application has been confirmed: <br>
               <a href='http://".$_SERVER['HTTP_HOST']."/index.php/application/view/id/".$application->getId()."'>".$application->getApplicationId()."</a>
               <br>
               <br>
               --
               <br>
               Thanks,<br>
               eCiziten, ".sfConfig::get('app_organisation_name').".<br>

               ";

             $mailnotification->sendemail(sfConfig::get('app_organisation_email'),$userprofile->getEmail(),"Your payment has been confirmed",$body);

             error_log('EC_LOG: Sent mail to user about invoice');
         }
       }

        $resp="pesapal_notification_type=$pesapalNotification&pesapal_transaction_tracking_id=$pesapalTrackingId&pesapal_merchant_reference=$pesapal_merchant_reference";
        ob_start();
        echo $resp;
        ob_flush();
        exit;

     }else{

         error_log('EC_LOG: Status is not completed');

        if(!empty($row['record_exist']) && $status == 'FAILED'){

         error_log('EC_LOG: Status failed');

         $query = "select * ".MF_TABLE_PREFIX."form_payments where form_id = ? and record_id = ? and `status` = 1";
         $params = array($form_id,$entry_id);
         $sth = mf_do_query($query,$params,$dbh);
         $row = mf_do_fetch_result($sth);

          $q = Doctrine_Query::create()
              ->from("FormEntry a")
              ->where("a.form_id = ? and a.entry_id = ?", array($form_id, $entry_id));
           $application = $q->fetchOne();
           if($application && $row['payment_status'] != "failed")
           {

             $q = Doctrine_Query::create()
                ->from('sfGuardUserProfile a')
                ->where('a.user_id = ?', $application->getUserId());
             $userprofile = $q->fetchOne();

             $q = Doctrine_Query::create()
                ->from('sfGuardUser a')
                ->where('a.id = ?', $application->getUserId());
             $user = $q->fetchOne();

             $body = "
               Hi ".$userprofile->getFullname().",<br>
              <br>
               Your payment for the following application has been marked as ".$status.": <br>
               <a href='http://".$_SERVER['HTTP_HOST']."/index.php/application/view/id/".$application->getId()."'>".$application->getApplicationId()."</a>
               <br>
               <br>
                Please view your application's invoice, click on add payment and try again.<br>
                Confirm the amount that you are supposed to pay before you make a payment.<br>
               <br>
               <br>
               --
               <br>
               Thanks,<br>
               eCiziten, ".sfConfig::get('app_organisation_name').".<br>

               ";

             $mailnotification->sendemail(sfConfig::get('app_organisation_email'),$userprofile->getEmail(),"Your payment has been marked as ".$status,$body);

             //UPDATE payment details to failed
             //do update to ap_form_payments table
             $query = "update ".MF_TABLE_PREFIX."form_payments set payment_status = ? where form_id = ? and record_id = ? and `status` = 1";
             $params = array("failed",$form_id,$entry_id);
             mf_do_query($query,$params,$dbh);

            $resp="pesapal_notification_type=$pesapalNotification&pesapal_transaction_tracking_id=$pesapalTrackingId&pesapal_merchant_reference=$pesapal_merchant_reference";
            ob_start();
            echo $resp;
            ob_flush();
            exit;

           }
         }
         else
         {
            error_log('EC_LOG: Could not find payment record');
            $resp="pesapal_notification_type=$pesapalNotification&pesapal_transaction_tracking_id=$pesapalTrackingId&pesapal_merchant_reference=$pesapal_merchant_reference";
            ob_start();
            echo $resp;
            ob_flush();
            exit;
         }
     }

   } //end update/insert into ap_form_payments
?>
