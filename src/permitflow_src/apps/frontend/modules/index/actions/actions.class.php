<?php
/**
 * index actions.
 *
 * Displays Web Page Content from the Content Management System
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
class indexActions extends sfActions
{
    /**
     * Executes 'Index' action
     *
     * Displays a web page from the CMS based on id
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {
        //If default language has not been set then pick english as the default
        if(is_null($this->getUser()->getCulture()) || $this->getUser()->getCulture() == 'localhost')
        {
            $this->getUser()->setCulture('en_US');
        }
       if($request->getParameter('id')  == 14 ){

            return $this->redirect("https://mcc-cp.org.ls/mcc_forum");

        }
        //If user is logged in then redirect to dashboard
        if($this->getUser()->isAuthenticated())
        {
            return $this->redirect("/index.php/dashboard");
        }
       

        //Fetch page by id or fetch homepage
        $this->page = Doctrine_Core::getTable('Content')->find(array($request->getParameter('id', 1)));

        //If page does not exist then redirect to 404
        if(empty($this->page))
        {
            return $this->redirect("/index.php/errors/notfound");
        }
    }


    /**
       * Executes 'Checkuser' action
       *
       * Ajax used to check existence of username
       *
       * @param sfRequest $request A request object
       */
      public function executeCheckuser(sfWebRequest $request)
      {
          // add new user
            if (!empty($request->getPostParameter('name')) && strlen($request->getPostParameter('name')) > 2 ) {
                $q = Doctrine_Query::create()
                     ->from("sfGuardUser a")
                     ->where('a.username = ?', $request->getPostParameter('name'));
                  $existinguser = $q->execute();

               if(sizeof($existinguser) > 0)
               {
                    echo '<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Username is already in use!</strong></div>';
                    return sfView::NONE;
               }
               else
               {
                    echo '<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Username is available!</strong></div>';
                    return sfView::NONE;
               }
            }
      }
      /**
       * Executes 'Checkemail' action
       *
       * Ajax used to check existence of email
       *
       * @param sfRequest $request A request object
       */
      public function executeCheckemail(sfWebRequest $request)
      {

          //OTB Fix first check the email format  - We use Simple PHP method filter_var(email,filter)
          $var = $request->getPostParameter('email') ;
          $var2 = "Test" ;
          if (!empty($request->getPostParameter('email')) && strlen($request->getPostParameter('email')) > 5 ) {
              if (!filter_var($request->getPostParameter('email'), FILTER_VALIDATE_EMAIL)) {
                  echo 2;
                  exit;
               }
              else {
                  
                               // add new user
                      $q = Doctrine_Query::create()
                         ->from("sfGuardUserProfile a")
                         ->where('a.email = ?', $request->getPostParameter('email'));
                      $existinguser = $q->execute();
                      if(sizeof($existinguser) > 0)
                      {
                            echo 0;
                           
                            return sfView::NONE;
                      }
                      else
                      {
                            echo 1;
                            
                            return sfView::NONE;
                      }
              }
          }
        }
}

