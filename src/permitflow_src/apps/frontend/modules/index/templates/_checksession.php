<?php
/**
 * _checksession.php template.
 *
 * Checks whether the user is logged in correctly
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

//Check if backend user is logged in, if so log them out
if($sf_user->isAuthenticated()){
    if($_SESSION['SESSION_CUTEFLOW_USERID'])
    {
        $sf_user->signout();
		    header("Location: /index.php");
        exit;
    }
}
?>
