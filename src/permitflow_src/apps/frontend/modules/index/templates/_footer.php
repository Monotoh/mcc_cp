<?php
/**
 * _footer.php template.
 *
 * Displays Footer
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>

<!-- FOOTER -->
		<footer>
			<div id="footer">

				<div class="container">
					<div class="row">
						<div class="col-sm-3">

							<h6 class="widget-title">Maseru City Council</h6>


							<div class="widget widget-contact">

								<ul>
									<li>
										<span>Address: </span>
										LNDC Development House <br> Cnr Kingsway & Pioneer Rd
									</li>
									<li>
										<span>Phone: </span>
										Switchboard – 22317386<br>
										I.T. – (+266) 63830704<br>
										Building Control – 27007386
									</li>
									<li>
										<span>E-mail: </span>
										<a href="mailto:general@mcc.org.ls">general@mcc.org.ls</a>
										<a href="mailto:general@mcc.org.ls">nhlajoane@mcc.org.ls</a>
										<a href="mailto:buildingcontrol@mcc.org.ls">buildingcontrol@mcc.org.ls</a>
									</li>
								</ul>

							</div><!-- widget-contact -->



						</div><!-- col -->
						<div class="col-sm-3">

							<div class="widget widget-pages">

								<h6 class="widget-title">Getting Started</h6>

								<ul>
									<li><a href="/index.php?id=7">How to apply</a></li>
									<li><a href="#">Who can apply</a></li>
									<li><a href="/index.php?id=6">Requirements checklist</a></li>
									<li><a href="/index.php?id=5">Permit workflow</a></li>
									<li><a href="#">Billing information</a></li>
								</ul>

							</div><!-- widget-pages -->

						</div><!-- col -->
						<div class="col-sm-3">

							<div class="widget widget-pages">

								<h6 class="widget-title">Resource Documents</h6>

								<ul>
									<li><a href="/index.php?id=8">Rules & Regulations</a></li>
									<li><a href="#">Risk Matrix</a></li>
								</ul>

							</div><!-- widget-pages -->

						</div><!-- col -->
						<div class="col-sm-3">

							<div class="widget widget-pages">

								<h6 class="widget-title">Help</h6>

								<ul>
									<li><a href="#">Video Tutorial</a></li>
									<li><a href="/index.php?id=7">User Manual</a></li>
								</ul>

							</div><!-- widget-pages -->

						</div><!-- col -->
					</div><!-- row -->
				</div><!-- container -->

			</div><!-- FOOTER -->
			<div id="footer-bottom">

				<div class="container">
					<div class="row">
						<div class="col-sm-12">

							<div class="widget widget-text">

								<div>
									<p class="text-center">Copyright &copy; Maseru City Construction Permit 2016 <br>
										<span style="color:#5c5c5c;font-size:12px;">This initiative is supported by the World Bank, Private Sector Competitiveness Project.</span>
									</p>

								</div>

							</div><!-- widget-text -->

						</div><!-- col -->
					</div><!-- row -->
				</div><!-- container -->

			</div><!-- FOOTER BOTTOM -->
		</footer><!-- FOOTER -->
