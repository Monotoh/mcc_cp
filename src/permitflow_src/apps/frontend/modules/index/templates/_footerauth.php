<?php
/**
 * _footer.php template.
 *
 * Displays Footer
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>
<?php
	  if($_SERVER['REQUEST_URI'] == "/index.php" || $_SERVER['REQUEST_URI'] == "/" || $_SERVER['REQUEST_URI'] == "/index.php/"){
	    ?>
		<?php
	  }
	?>




<?php
	  if($_SERVER['REQUEST_URI'] == "/index.php" || $_SERVER['REQUEST_URI'] == "/" || $_SERVER['REQUEST_URI'] == "/index.php/"){
	    ?>
		<?php
	  }
	?>
    
    

 <!-- Footer
    ================================================== -->

     
    
    
         
    
    <?php
	    if($_SERVER['REQUEST_URI'] == "/index.php" || $_SERVER['REQUEST_URI'] == "/" || $_SERVER['REQUEST_URI'] == "/index.php/")
		{
		  $q = Doctrine_Query::create()
		     ->from("Content p")
			 ->where("p.id = ?", 34);
		  $footer = $q->fetchOne();
		  if($footer)
		  {
		  
		  ?>
          
          <h4 class="title-gray">Welcome to the Kigali Online Construction Permit System:</h4> 
                  
                <div class="topbar-inner">
                
                <p class="intro-text">Get started now and receive your Construction Permit in a maximum of <strong>30 days</strong> in 5 Steps:</p>
                <div class="span3 steps first-tab">
                <h4>1. <a href="/index.php/?id=24">Pre-Application </a></h4>
                <p>First, visit your District or Kigali One Stop centre to hold a pre-application consultation: to  meet with a Customer Care Team member.</p>
				</div>
                
				<div class="span3 steps">
                <h4>2. <a href="/index.php/?id=25">Registration</a></h4>
                <p>Create an account on www.kcps.gov.rw as a Property Owner, Architect, Engineer or Contractor </p>
				</div>
                
				<div class="span3 steps">
                <h4>3. <a href="/index.php/?id=41">Application</a></h4>
				<p>Apply for a Construction Permit, Occupancy Permit, Change of Use Permit, or Renovation Permit</p>
				</div>
                
                <div class="span3 steps">
                <h4>4. <a href="/index.php/?id=26">Payment</a></h4>
				<p>Receive your invoice and pay the permitting fees.</p>
				</div>
                
                <div class="span3 steps">
                <h4>5. <a href="/index.php/?id=27">Get Permit</a></h4>
				<p> Download and print your approved Permit.</p>	
				</div>
                
    
               </div>
          
          
          
          
          
    <div class="footer-bar">
      <div class="topbar-inner">

		  <?php
			  echo html_entity_decode($footer->getTopArticle());
			  ?>
			  
   
      </div>
    </div>
			  <?php
		  }
		}
		  ?>
    

   
    </div><!-- /.container -->

  </div><!-- /.row -->
  
  
     <div class="row">

<div class="container content">         

 <div class="base">

      <p style="margin-right:20px;" class="pull-right"><a href="#">Back to top</a>
      
      <select style="width: 150px; margin-bottom:0" name='locale' id='locale' onChange="window.location='/index.php/index/setlocale/id/' + this.value;">
                            <?php
							$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
							mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
							
							
							
							$sql = "SELECT * FROM ext_locales";
							$rows = mysql_query($sql);
							
							$setlocale = $_SESSION['locale'];
							
							
							while($row = mysql_fetch_assoc($rows))
							{
								$selected = "";
								if($setlocale == $row['locale_identifier'])
								{
								    $selected = "selected";
								}
								else if($setlocale == "" && $row['is_default'] == "1")
								{
									$selected = "selected";
								}
							?>
                        	<option value='<?php echo $row['id']; ?>' <?php echo $selected; ?>><?php echo $row['local_title']; ?></option>
                            <?php
							}
							?>
                        </select>
        
        </p>
        <p style="margin-left:20px; float: left;">&copy; 2013 City of Kigali. All Rights Reserved. &middot; <a href="<?php echo public_path(); ?>index.php/?id=19"> Privacy Policy </a> &middot; <a href="<?php echo public_path(); ?>index.php/?id=18">Terms and Conditions</a>
     
        </p>
  
   </div>
   
   
    </div><!-- /.container -->

  </div><!-- /.row -->
  
  
  
  
  
  
  
   



