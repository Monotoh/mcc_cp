<?php
/**
 * _header.php template.
 *
 * Displays Header
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

 use_helper('I18N');
?>
        <section class="top-bar" style="display:none;">
      <div class="container">
        <?php
            $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
            mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

            $sql = "SELECT * FROM ext_locales ORDER BY local_title ASC";
            $rows = mysql_query($sql, $dbconn);

            if(mysql_num_rows($rows) > 1)
            {
            ?>
            <select name="locale_select" id="locale_select" onChange="window.location='/index.php/index/setlocale/code/' + this.value;">
            <?php
            while($row = mysql_fetch_assoc($rows))
            {
              $selected = "";
              if($row['locale_identifier'] == $sf_user->getCulture())
              {
                $selected = "selected";
              }
              echo "<option value='".$row['locale_identifier']."' ".$selected.">".$row['local_title']."</option>";
            }
          ?>
            </select>
          <?php
            }
          ?>
       <?php
        if($sf_user->isAuthenticated())
        {
        ?>
        <div class="top-links"> <a href="<?php echo url_for('settings') ?>">My Account</a> |
                    <a href="<?php echo url_for('signon/logout') ?>">Logout</a></div>
        <?php
        }
           else
        {
        ?>
        <div class="top-links"> <a href="<?php echo url_for('signon/login') ?>"><?php echo __('Sign In'); ?></a> |
                    <a href="<?php echo url_for('@register'); ?>"><?php echo __('Sign Up'); ?></a></div>
         <?php
        }
        ?>
          </div>
    </section>


    <!-- HEADER -->
    <header id="header">

      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-4">

            <!-- LOGO -->
            <div id="logo">
              <a href="<?php echo public_path(); ?>index.php">
              <?php
                $q = Doctrine_Query::create()
                   ->from("ApSettings a")
                   ->where("a.id = 1")
                   ->orderBy("a.id DESC");
                $aplogo = $q->fetchOne();
                if($aplogo && $aplogo->getAdminImageUrl())
                {
                  $file = $aplogo->getUploadDirWeb().$aplogo->getAdminImageUrl();
                    ?>
                      <img src="<?php echo $file; ?>" id="img-logo" alt="logo">
                    <?php
                }
                ?>
              </a>
            </div><!-- logo -->

          </div><!-- col -->
          <div class="col-sm-8">

             <?php include_component('index', 'menu') ?>

          </div><!-- col -->
        </div><!-- row -->
      </div><!-- container-fluid -->

    </header><!-- HEADER -->
