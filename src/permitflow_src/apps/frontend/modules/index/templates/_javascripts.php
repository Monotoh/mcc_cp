<?php
/**
 * _javascripts.php template.
 *
 * Displays Javascripts
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>

    <!-- jQUERY -->
    <script src="<?php echo public_path(); ?>permitflow/assets/plugins/jquery/jquery-2.2.0.min.js"></script>
    
    <!-- BOOTSTRAP JS -->
    <script src="<?php echo public_path(); ?>permitflow/assets/bootstrap/js/bootstrap.min.js"></script>
    
    <!-- VIEWPORT -->
    <script src="<?php echo public_path(); ?>permitflow/assets/plugins/viewport/jquery.viewport.js"></script>
    
    <!-- MENU -->
    <script src="<?php echo public_path(); ?>permitflow/assets/plugins/menu/hoverIntent.js"></script>
    <script src="<?php echo public_path(); ?>permitflow/assets/plugins/menu/superfish.js"></script>
    
    <!-- FANCYBOX -->
    <script src="<?php echo public_path(); ?>permitflow/assets/plugins/fancybox/jquery.fancybox.pack.js"></script>
    
    <!-- PARALLAX -->
    <script src="<?php echo public_path(); ?>permitflow/assets/plugins/parallax/jquery.stellar.min.js"></script>
    
    <!-- ISOTOPE -->
    <script src="<?php echo public_path(); ?>permitflow/assets/plugins/isotope/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo public_path(); ?>permitflow/assets/plugins/isotope/isotope.pkgd.min.js"></script>
    
    <!-- PLACEHOLDER -->
    <script src="<?php echo public_path(); ?>permitflow/assets/plugins/placeholders/jquery.placeholder.min.js"></script>
    
    <!-- CONTACT FORM VALIDATE & SUBMIT -->
    <script src="<?php echo public_path(); ?>permitflow/assets/plugins/validate/jquery.validate.min.js"></script>
    <script src="<?php echo public_path(); ?>permitflow/assets/plugins/submit/jquery.form.min.js"></script>
    
    <!-- GOOGLE MAPS -->
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="<?php echo public_path(); ?>permitflow/assets/plugins/googlemaps/gmap3.min.js"></script>
 
    <!-- ANIMATIONS -->
    <script src="<?php echo public_path(); ?>permitflow/assets/plugins/animations/wow.min.js"></script>
     
    <!-- CUSTOM JS -->
    <script src="<?php echo public_path(); ?>permitflow/assets/js/custom.js"></script>

<?php
if(sfConfig::get('app_google_analytics_id')) {
    ?>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', '<?php echo sfConfig::get('app_google_analytics_id'); ?>', 'auto');
        ga('send', 'pageview');

    </script>
<?php
}
?>