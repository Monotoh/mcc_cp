<?php
/**
 * _menu.php template.
 *
 * Displays Menu
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
  use_helper('I18N');
?>

<nav>
    <ul class="menu clearfix" id="menu">
    <?php
        foreach ($pages as $page) {

          $q = Doctrine_Query::create()
             ->from("Content a")
             ->where("a.parent_id = ?", $page->getId());
          $children = $q->execute();

            ?>
            <li <?php if ($_SERVER['REQUEST_URI'] == url_for($page->getUrl()) || ($_SERVER['REQUEST_URI'] == "/" && $x == 1)) {
                echo "  class='current' ";
              }  if($q->count() > 0) {echo " class='dropdown' ";} ?>>

              <a href="/index.php?id=<?php echo $page->getId(); ?>"
                    title="<?php echo $page->getMenuTitle(); ?>"  <?php  if($page->getId() == 14)  echo "target='blank'"; ?>   ><?php echo $page->getMenuTitle() ?></a>

            <ul>

            <?php
            foreach($children as $child)
            {
              ?>
              <li <?php if ($_SERVER['REQUEST_URI'] == url_for($child->getUrl()) || ($_SERVER['REQUEST_URI'] == "/" && $x == 1)) {
                  echo "  class='current' "; } ?>>

                  <a href="/index.php?id=<?php echo $child->getId(); ?>"
                      title="<?php echo $child->getMenuTitle(); ?>" ><?php echo $child->getMenuTitle() ?></a>
                      
                      </li>
              <?php
            }
            ?>
          </ul>

            <?php
        }
        ?>

        </li>
        
        <li  <?php if($sf_context->getModuleName() == "news"){ echo "class='active'"; } ?>><a href="<?php echo public_path(); ?>index.php/news" title="<?php echo __('News'); ?>"><?php echo __('News'); ?></a></li>
        <li  <?php if($_SERVER['REQUEST_URI'] == "/index.php/help/faq" || $_SERVER['REQUEST_URI'] == "/index.php/help/faq"){echo " class='active' ";} ?>><a href="<?php echo public_path(); ?>index.php/help/faq" title="<?php echo __('FAQ'); ?>"><?php echo __('FAQ'); ?> </a></li>
        <li  <?php if($_SERVER['REQUEST_URI'] == "/index.php/help/contact" || $_SERVER['REQUEST_URI'] == "/index.php/help/contact"){echo " class='active' ";} ?>><a href="<?php echo public_path(); ?>index.php/help/contact" title="<?php echo __('Contact Us'); ?>"><?php echo __('Contact Us'); ?></a></li>

        <?php
 				if(!$sf_user->isAuthenticated())
 				{
 				?>
         <!-- <li <?php if($_SERVER['REQUEST_URI'] == "/index.php/register"){echo " class='active' ";} ?>><a href="<?php echo url_for('signon/register'); ?>" title="<?php echo __('Sign Up'); ?>"><?php echo __('Sign Up'); ?> </a></li>
         <li <?php if($_SERVER['REQUEST_URI'] == "/index.php/login"){echo " class='active' ";} ?>><a href="<?php echo url_for('signon/login') ?>" title="<?php echo __('Sign In'); ?>"><?php echo __('Sign In'); ?></a></li> -->
 				 <?php
 				}
 				?>

    </ul>
</nav>
