<?php
/**
 * indexSuccess.php template.
 *
 * Displays a web page
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

use_helper('I18N');

//Display breadcrumbs if id parameter has been set
if($page->getId() != 1)
{
?>

<!-- CONTENT -->
        <div id="page-content">
            
            <div id="page-header">  
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">

                            <p><?php echo __('Maseru City Construction Permit'); ?></p>
                            
                            <div class="text-parallax" data-stellar-background-ratio="0.5">
                                <h1 class="text-parallax-content"><?php echo $page->getMenuTitle(); ?></h1>
                            </div><!-- text-parallax -->
                            
                        </div><!-- col -->
                    </div><!-- row -->
                </div><!-- container -->    
            </div><!-- page-header -->


                        <?php
                        }

                        //Display banner if homepage
                        if($page->getId() == 1)
                        {
                            include_component('index', 'banner');
                        }

                        //Add section and sixteeen column divs if not homepage
                        if($page->getId() != 1)
                        {
                        ?>
                        <div class="container">
                            <div class="row">
                                 <div class="col-md-12 col-sm-12">
                                    <?php
                                    }

                                    $article = html_entity_decode($page->getTopArticle());
                                    echo $article;

                                    if($page->getId() != 1)
                                    {
                                    ?>
                                </div><!-- col -->
                            </div><!-- row -->
                        </div><!-- container -->
                        <?php
                        }
                        ?>
            
        </div><!-- PAGE CONTENT -->






