<?php
/**
 * index actions.
 *
 * Displays Content
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
class indexActions extends sfActions
{
  /**
   * Executes 'Checkuser' action
   *
   * Ajax used to check existence of username
   *
   * @param sfRequest $request A request object
   */
  public function executeCheckuser(sfWebRequest $request)
  {
      // add new user
      $q = Doctrine_Query::create()
         ->from("sfGuardUser a")
         ->where('a.username = ?', $request->getPostParameter('name'));
      $existinguser = $q->execute();
      if(sizeof($existinguser) > 0)
      {
            echo '<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Username is already in use!</strong></div>';
            exit;
      }
      else
      {
            echo '<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Username is available!</strong></div>';
            exit;
      }
  }
  /**
   * Executes 'Checkemail' action
   *
   * Ajax used to check existence of email
   *
   * @param sfRequest $request A request object
   */
  public function executeCheckemail(sfWebRequest $request)
  {
       //OTB Fix first check the email format  - We use Simple PHP method filter_var(email,filter)
	  $var = $request->getPostParameter('email') ;
	  $var2 = "Test" ;
		  if (!filter_var($request->getPostParameter('email'), FILTER_VALIDATE_EMAIL)) {
			  // echo '<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Enter a valid email..</strong></div>';
	          // $emailErr = "Invalid email format";
	         //  
	         echo "invalid_email" ;
	         exit ;
	       }
	      else {
			  
						   // add new user
				  $q = Doctrine_Query::create()
					 ->from("sfGuardUserProfile a")
					 ->where('a.email = ?', $request->getPostParameter('email'));
				  $existinguser = $q->execute();
				  if(sizeof($existinguser) > 0)
				  {
						//echo '<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Email is already in use!</strong></div>';
						echo "email_in_use" ;
						exit;
				  }
				  else
				  {
						//echo '<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Email is available!..continue..</strong></div>';
						echo "email_valid" ;
						exit;
				  }
		  }
  }
        /**
	 * Executes 'Setlocale' action
	 *
	 * Changes the language for current user (logged in and not logged in)
	 *
	 * @param sfRequest $request A request object
	 */
    public function executeSetlocale(sfWebRequest $request)
    {
      $this->getUser()->setCulture($request->getParameter("code"));
      $this->redirect($request->getReferer());
    }

        /**
	 * Executes 'Captcha' action
	 *
	 * Displays captcha on certain forms
	 *
	 * @param sfRequest $request A request object
	 */
    public function executeCaptcha(sfWebRequest $request)
    {

    }

    /**
     * Executes 'isactive' action
     *
     * Displays inactive account warning to users
     *
     * @param sfRequest $request A request object
     */
    public function executeInactive(sfWebRequest $request)
    {

    }


         /**
	 * Executes 'Index' action
	 *
	 * Displays a web page
	 *
	 * @param sfRequest $request A request object
	 */
    public function executeIndex(sfWebRequest $request)
    {
          if(empty($this->getUser()->getCulture()) || $this->getUser()->getCulture() == 'localhost')
          {
            $this->getUser()->setCulture('en_US');
          }

          $module = $request->getParameter('module');
          $action = $request->getParameter('action');

          $class = sfConfig::get('app_sf_guard_plugin_signin_form', 'sfGuardFormSignin');
          $this->form = new $class();

	        $pageurl = $module."/".$action."/id/".$request->getParameter("id");

	        $q = Doctrine_Query::create()
				          ->from('Content a')
				          ->where('a.url = ?', $pageurl)
				          ->orderBy('a.menu_index DESC');
	        $pages = $q->execute();

//	        if(!$this->getUser()->isAuthenticated() && sizeof($pages) > 0)
            /*if (!$this->getUser()->isAuthenticated())
	        {
		        $this->redirect('@sf_guard_signin');
	        } */

	            $id = 1;

		        if($request->getGetParameter("id"))
		        {
			        $id = $request->getGetParameter("id");
		        }

                $q = Doctrine_Query::create()
			          ->from('Content a')
			          ->where('a.id = ?', $id);
	            $this->contents = $q->execute();
		        $this->currentpage = $q->fetchOne();

		        if($id == 1)
		        {
		          if($this->getUser()->isAuthenticated())
			        {
                if($_SESSION['SESSION_CUTEFLOW_USERID'])
                {
                    $_SESSION['SESSION_CUTEFLOW_USERID'] = false;
                    $this->getUser()->getAttributeHolder()->clear();
                    $this->getUser()->clearCredentials();
                    $this->getUser()->setAuthenticated(false);
                    $this->getUser()->signout();
                    header("Location: /index.php");
                    exit;
                }
                else
                {
		              $this->redirect('@dashboard');
                }
			        }
			        else
			        {
				        $this->setLayout('layout');
			        }
		        }
          }
}
