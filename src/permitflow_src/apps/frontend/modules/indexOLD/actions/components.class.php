<?php
/**
 * index components.
 *
 * Contains code snippets that can be inserted into the layout
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
class indexComponents extends sfComponents
{
        /**
	 * Executes 'Checksession' component
	 *
	 * Checks whether the user is logged in correctly
	 *
	 */
        public function executeChecksession()
        {

        }

        /**
	 * Executes 'Stylesheets' component
	 *
	 * Displays stylesheets on the layout
	 *
	 */
        public function executeStylesheets()
	{

	}

	/**
	 * Executes 'Javascripts' component
	 *
	 * Displays javascripts on the layout
	 *
	 */
        public function executeJavascripts()
	{

	}

        /**
	 * Executes 'Stylesheets' component
	 *
	 * Displays stylesheets on the layout
	 *
	 */
        public function executeStylesheetsdash()
	{

	}

	/**
	 * Executes 'Javascripts' component
	 *
	 * Displays javascripts on the layout
	 *
	 */
        public function executeJavascriptsdash()
	{

	}

	/**
	 * Executes 'Header' component
	 *
	 * Displays header
	 *
	 */
        public function executeHeader()
	{
           //OTB patch to dynamically load logo
           $q_settings = Doctrine_Query::create()
                ->from('ApSettings a') 
                ->limit(1) ;
           $this->appsettings = $q_settings->execute(); 
	}

	/**
	 * Executes 'Headerdash' component
	 *
	 * Displays header
	 *
	 */
        public function executeHeaderdash()
	{

	}


		/**
	 * Executes 'Sidemenu' component
	 *
	 * Displays sidemenu
	 *
	 */
        public function executeSidemenu()
	{

	}

	/**
	 * Executes 'Footer' component
	 *
	 * Displays footer
	 *
	 */
        public function executeFooter()
	{

	}

	/**
	 * Executes 'Footer' component
	 *
	 * Displays footer
	 *
	 */
     public function executeFooterdash()
	{

	}


	/**
	 * Executes 'Banner' component
	 *
	 * Displays banner
	 *
	 */
  public function executeBanner()
	{
		$q = Doctrine_Query::create()
			->from('Banner a')
			->orderBy('a.id ASC');
		$this->banners = $q->execute();
	}

	/**
	 * Executes 'Menu' component
	 *
	 * Displays menu
	 *
	 */
	public function executeMenu()
	{
		if($this->getUser()->isAuthenticated())
		{
			$q = Doctrine_Query::create()
				  ->from('Content a')
				  ->where('a.published = ?', 1)
				  ->andWhere('a.visibility = 1 AND a.visibility = 0')
				  ->orderBy('a.menu_index ASC');
			$this->pages = $q->execute();

		}
		else
		{
			$q = Doctrine_Query::create()
				  ->from('Content a')
				  ->where('a.published = ?', 1)
				  ->andWhere('a.visibility = 1')
				  ->orderBy('a.menu_index ASC');
			$this->pages = $q->execute();
		}
	}

	/**
	 * Executes 'Breadcrumb' component
	 *
	 * Displays breadcrumb
	 *
	 */
	public function executeBreadcrumb($request)
	{
                $module = $request->getParameter('module');
                $action = $request->getParameter('action');

                $pageurl = $module."/".$action."?id=".$request->getParameter("id");

                $q = Doctrine_Query::create()
                    ->from('Content a')
                    ->where('a.published = ?', 1)
                    ->andWhere('a.url = ?', $pageurl)
                    ->orderBy('a.menu_index DESC');
                $page = $q->fetchOne();

                if(sizeof($page) <= 0)
                {
                    $pageurl = $module."/".$action."/id/".$request->getParameter("id");

                    $q = Doctrine_Query::create()
                        ->from('Content a')
                        ->where('a.published = ?', 1)
                        ->andWhere('a.url = ?', $pageurl)
                        ->orderBy('a.menu_index DESC');
                    $page = $q->fetchOne();
                }
		if($module == 'index')
		{
		    if($request->getParameter('id'))
	            {
			    if($page)
                             {
					$menutitle = "";

					$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
					mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

					$sql = "SELECT * FROM ext_translations WHERE field_id = '".$page->getId()."' AND field_name = 'menu_title' AND table_class = 'content' AND locale = '".$_SESSION['locale']."'";
					$rows = mysql_query($sql);
					if($row = mysql_fetch_assoc($rows))
					{
						$menutitle = $row['trl_content'];
					}
					else
					{
						$menutitle = $page->getMenuTitle();
					}

                                    $this->url = "<a href=\"".$module."/".$this->action."/id/".$request->getParameter("id")."\"><strong>".$page->getMenuTitle()."</a></strong>";
                                    $this->url = "<strong>".$menutitle."</strong>";
                                }else
                                {
                                      $form = Doctrine_Core::getTable('ApForms')->find(array($request->getParameter('id')));

					                  if($form)
					                  {
						                $this->url = "<strong>".$form->getFormName()."</strong>";
					                  }
                                }
			}

		}
                elseif($module == 'Settings')
                {
                    $this->url = "<strong>Account Settings</strong>";
                }
                elseif($module == 'frusers')
                {
                    $this->url = "<strong>Edit Account Details</strong>";
                }
                elseif($module == 'permits')
                {
                    $this->url = "<strong>Permits</strong>";
                }
                elseif($module == 'invoices')
                {
                    $this->url = "<strong>Invoices</strong>";
                }
                elseif($module == 'application' && $action == "approvals")
                {
                    $this->url = "<strong>Approved Constructions</strong>";
                }
                elseif($module == 'application')
                {
                    $this->url = "<strong>Applications</strong>";
                }
                elseif($module == 'help')
                {
                    $this->url = "<strong>Help</strong>";
                }
                elseif($module == 'messages')
                {
                    $this->url = "<strong>Messages</strong>";
                }
                elseif($module == 'dashboard')
                {
                    $this->url = "<strong>My Dashboard</strong>";
                }
                elseif($module == 'sfGuardAuth')
                {
                    $this->url = "<strong>User Login</strong>";
                }
		elseif($module == 'faqs')
		{
			$this->url = "<strong>Frequently Asked Questions</strong>";
		}
		elseif($module == 'forms')
		{
			$form = Doctrine_Core::getTable('ApForms')->find(array($request->getParameter('id')));
			$this->url = "<strong>".$form->getFormName()."</strong>";
		}
		elseif($module == 'news' && $this->action == 'index')
		{
			$this->url = "<strong>News</strong>";
		}
		elseif($module == 'news'&& $this->action == 'article')
		{
			$this->url = "<a href = \"javascript:history.back(1)\">News</a> > <strong>News Article</strong>";
		}
	}

}
