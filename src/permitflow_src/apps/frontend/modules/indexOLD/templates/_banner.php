<?php
/**
 * _banner.php template.
 *
 * Displays banner
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>

<div class="sixteen columns">
		<div class="flexslider">
			<ul class="slides">
			<?php
                $q = Doctrine_Query::create()
                   ->from("ApSettings a")
                   ->where("a.id = 1")
                   ->orderBy("a.id DESC");
                $aplogo = $q->fetchOne();

				foreach($banners as $banner)
				{
	                if($aplogo && $aplogo->getAdminImageUrl())
	                {
	                    $file = $aplogo->getUploadDirWeb().$banner->getImage();
	                }
	                else
	                {
	                	$file = public_path()."asset_uplds/".$banner->getImage();
	                }
				?>
				        <li> <img src="<?php echo $file; ?>" alt="">
				          <article class="slide-caption">
				            <h3><?php echo $banner->getTitle(); ?></h3>
				            <p><?php echo $banner->getDescription(); ?></p>
				          </article>
				        </li>

				<?php
				}
				?>
</ul>
</div>
</div>
