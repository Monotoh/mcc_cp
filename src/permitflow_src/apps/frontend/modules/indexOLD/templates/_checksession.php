<?php
/**
 * _checksession.php template.
 *
 * Checks whether the user is logged in correctly
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>
<?php
//Check if backend user is logged in, if so log them out
if($sf_user->isAuthenticated()){
    if($_SESSION['SESSION_CUTEFLOW_USERID'])
    {
        $sf_user->signout();
		    header("Location: /index.php");
        exit;
    }
}


if(empty($_SESSION['locale']) || strlen($_SESSION['locale']) < 2)
{
	$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
	mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

	$sql = "SELECT * FROM ext_locales";
	$rows = mysql_query($sql);

	$setlocale = $_SESSION['locale'];


	while($row = mysql_fetch_assoc($rows))
	{
		$selected = "";
		if($setlocale == $row['locale_identifier'])
		{
			$_SESSION['locale'] = $row['locale_identifier'];
		}
		else if($row['is_default'] == "1")
		{
			$_SESSION['locale'] = $row['locale_identifier'];
		}
	}
}
?>
