<?php
/**
 * _footer.php template.
 *
 * Displays Footer
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>

<!-- Footer -->
<footer class="footer text-right">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                © Copyright Permitflow <?php echo date('Y') ?>. All rights Reserved.
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->

</div> <!-- end container -->
</div>
<!-- End wrapper -->
