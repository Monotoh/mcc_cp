<?php
/**
 * _header.php template.
 *
 * Displays Header
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

 use_helper('I18N');
?>
  	    <section class="top-bar">
			<div class="container">
        <?php
            $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
            mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

            $sql = "SELECT * FROM ext_locales ORDER BY local_title ASC";
            $rows = mysql_query($sql, $dbconn);

            if(mysql_num_rows($rows) > 1)
            {
            ?>
            <select name="locale_select" id="locale_select" onChange="window.location='/index.php/index/setlocale/code/' + this.value;">
            <?php
            while($row = mysql_fetch_assoc($rows))
            {
              $selected = "";
              if($row['locale_identifier'] == $sf_user->getCulture())
              {
                $selected = "selected";
              }
              echo "<option value='".$row['locale_identifier']."' ".$selected.">".$row['local_title']."</option>";
            }
          ?>
            </select>
          <?php
            }
          ?>
			 <?php
				if($sf_user->isAuthenticated())
				{
			  ?>
				<div class="top-links"> <a href="<?php echo url_for('settings') ?>"><?php echo __('My Account') ?></a> |
                    <a href="<?php echo url_for('signon/logout') ?>"><?php echo __('Logout') ?></a></div>
			  <?php
				}
			     else
				{
				?>
				<div class="top-links"> <a href="<?php echo url_for('@sf_guard_signin'); ?>"><?php echo __('Sign In'); ?></a> |
                    <a href="<?php echo url_for('@sf_guard_register') ?>"><?php echo __('Sign Up'); ?></a></div>
				 <?php
				}
				?>
			    </div>
		</section>
		<div id="sticker">
			<header id="header">
					<div class="container">
						<div class="four columns mt10" style="margin-left:0;">
							<div class="logo">
							<a href="<?php echo public_path(); ?>index.php">
							 <?php foreach($appsettings as $set): ?>
                                                            <img src="<?php echo public_path(); ?><?php echo $set->getUploadDir() ?>/<?php echo $set->getAdminImageUrl()?>" alt="" width="200px" height="200px" />
                                                            <?php endforeach;  ?>
							</a>
							</div>
						</div>
						<div class="eight mt10" style="text-align:right;">
							<div class="logo">
							<a href="<?php echo public_path(); ?>index.php">
              <?php
                $q = Doctrine_Query::create()
                   ->from("ApSettings a")
                   ->where("a.id = 1")
                   ->orderBy("a.id DESC");
                $aplogo = $q->fetchOne();
                if($aplogo && $aplogo->getAdminImageUrl())
                {
                  $file = $aplogo->getUploadDirWeb().$aplogo->getAdminImageUrl();
                    ?>
                      <img src="<?php echo $file; ?>" id="img-logo" alt="logo">
                    <?php
                }
                ?>
							</a>
							</div>
						</div>
						<div class="logo" style="display:none;">
						 <h6>Agency name</h6>
						</div>
					</div>
			    <!-- Navigation starts
 				 ================================================== -->
 				 <?php include_component('index', 'menu') ?>
      				<!-- /nav-wrap -->
					<!-- Navigation ends
  				================================================== -->
			</header>
		</div>
		<!-- end-header -->
