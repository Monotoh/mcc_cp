<?php
/**
 * _javascripts.php template.
 *
 * Displays Javascripts
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>





<!-- End Document
================================================== -->
<script src="<?php echo public_path(); ?>permitflow/js/jquery.jcarousel.min.js"></script>
<script type="text/javascript" src="<?php echo public_path(); ?>permitflow/js/doubletaptogo.js" ></script>
<script defer src="<?php echo public_path(); ?>permitflow/js/jquery.flexslider-min.js"></script>
<script src="<?php echo public_path(); ?>permitflow/js/bootstrap-alert.js"></script>
<script src="<?php echo public_path(); ?>permitflow/js/bootstrap-dropdown.js"></script>
<script src="<?php echo public_path(); ?>permitflow/js/bootstrap-tab.js"></script>
<script src="<?php echo public_path(); ?>permitflow/js/bootstrap-tooltip.js"></script>
<script type="text/javascript" src="<?php echo public_path(); ?>permitflow/js/permitflow-custom.js" ></script>
<script src="<?php echo public_path(); ?>permitflow/js/jquery.prettyPhoto.js" ></script>
<script type="text/javascript" src="<?php echo public_path(); ?>permitflow/js/jquery.sticky.js"></script>
<script type="text/javascript" src="<?php echo public_path(); ?>permitflow/js/jquery.easy-pie-chart.js"></script>

<?php
if(sfConfig::get('app_google_analytics_id')) {
    ?>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', '<?php echo sfConfig::get('app_google_analytics_id'); ?>', 'auto');
        ga('send', 'pageview');

    </script>
<?php
}
?>