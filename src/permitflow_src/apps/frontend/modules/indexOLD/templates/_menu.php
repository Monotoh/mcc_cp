<?php
/**
 * _menu.php template.
 *
 * Displays Menu
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
  use_helper('I18N');
?>

		<nav id="nav-wrap" class="nav-wrap2 mn4">
       	 	<ul id="nav">
            <?php $x = 0; foreach ($pages as $page) : $x++;?>
			<?php

			$menutitle = "";

			$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
			mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

			$sql = "SELECT * FROM ext_translations WHERE field_id = '".$page->getId()."' AND field_name = 'menu_title' AND table_class = 'content' AND locale = '".$sf_user->getCulture()."'";
			$rows = mysql_query($sql);
			if($row = mysql_fetch_assoc($rows))
			{
				$menutitle = $row['trl_content'];
			}
			else
			{
				$menutitle = $page->getMenuTitle();
			}


			?>
			<li <?php if($_SERVER['REQUEST_URI'] == url_for($page->getUrl()) || ($_SERVER['REQUEST_URI'] == "/" && $x == 1)){echo "  class='current' ";} ?>><a href="/index.php?id=<?php echo $page->getId(); ?>"  title="<?php echo $menutitle  ?>"><?php echo $menutitle  ?></a></li>
			<?php
			?>
			<?php endforeach ?>
     		 <li  <?php if($sf_context->getModuleName() == "news"){ echo "class='current'"; } ?>><a href="<?php echo public_path(); ?>index.php/news" title="<?php echo __('News'); ?>"><?php echo __('News'); ?></a></li>

      		<li  <?php if($_SERVER['REQUEST_URI'] == "/index.php/help/faq" || $_SERVER['REQUEST_URI'] == "/index.php/help/faq"){echo " class='current' ";} ?>><a href="<?php echo public_path(); ?>index.php/help/faq" title="<?php echo __('FAQ'); ?>"><?php echo __('FAQ'); ?> </a></li>

			<li  <?php if($_SERVER['REQUEST_URI'] == "/index.php/help/contact" || $_SERVER['REQUEST_URI'] == "/index.php/help/contact"){echo " class='current' ";} ?>><a href="<?php echo public_path(); ?>index.php/help/contact" title="<?php echo __('Contact Us'); ?>"><?php echo __('Contact Us'); ?></a></li>



		</ul>
</nav>
