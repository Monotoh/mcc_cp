<?php
/**
 * _stylesheets.php template.
 *
 * Displays Stylesheets
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

$translation = new Translation();
if($translation->IsLeftAligned())
{
?>


	<link rel="stylesheet" href="<?php echo public_path(); ?>permitflow/css/style.css" type="text/css"  media="all">




    <link rel="shortcut icon" href="<?php echo public_path(); ?>permitflow/images/favicon.png" />
    
    <!-- JS
    ================================================== -->
   <script type="text/javascript" src="<?php echo public_path(); ?>permitflow/js/jquery.min.js" ></script>
	<!--[if lt IE 9]>
	<script src="<?php echo public_path(); ?>permitflow/js/modernizr.custom.11889.js" type="text/javascript"></script>
	<![endif]-->
	<!-- HTML5 Shiv events (end)-->
 <?php
}
else
{
	?>


	<link rel="stylesheet" href="<?php echo public_path(); ?>permitflow/css/style.css" type="text/css"  media="all">

    <link href="<?php echo public_path(); ?>assets_unified/css/style.default-rtl.css" rel="stylesheet">


    <link rel="shortcut icon" href="<?php echo public_path(); ?>permitflow/images/favicon.png" />
    
    <!-- JS
    ================================================== -->
   <script type="text/javascript" src="<?php echo public_path(); ?>permitflow/js/jquery.min.js" ></script>
	<!--[if lt IE 9]>
	<script src="<?php echo public_path(); ?>permitflow/js/modernizr.custom.11889.js" type="text/javascript"></script>
	<![endif]-->
	<!-- HTML5 Shiv events (end)-->
	<?php
}