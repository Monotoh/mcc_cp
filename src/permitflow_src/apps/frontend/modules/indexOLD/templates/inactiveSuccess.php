<?php
use_helper('I18N');
?>
<div class="breadcrumb-box">
    <div class="container">
        <ul class="breadcrumb">
            <li>
                <a href="/index.php"><?php echo __('Home'); ?></a>
                <span class="divider">/</span>
            </li>

            <li>
                <a href="/index.php/register"><?php echo __('Your Account'); ?></a>
                <span class="divider">/</span>
            </li>

            <?php
            if($_GET['reg'] == 1)
            {
                ?>
                <li class="active"><?php echo __('Registration Successful'); ?></li>
            <?php
            }
            else {
                ?>
                <li class="active"><?php echo __('Inactive'); ?></li>
            <?php
            }
            ?>
        </ul>
    </div>
</div>

<section id="headline">
    <div class="container">
        <?php
        if($_GET['reg'] == 1)
        {
        ?>
        <h3><?php echo __('Registration Successful'); ?></h3>
        <?php
        }
        else {
            ?>
            <h3><?php echo __('Inactive Account'); ?></h3>
        <?php
        }
        ?>
    </div>
</section>
<div class="signinpanel">



    <div class="row">
        <div class="container">
            <div class="twelve columns offset-by-three">
                <?php
                if($_GET['reg'] == 1)
                {
                    ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><?php echo __('Registration Successful'); ?></h4>
                            <p><?php echo __('Your account has been created successfully'); ?></p>
                        </div><!-- panel-heading -->
                        <div class="panel-body">
                            <div class="alert alert-info">
                                <strong><?php echo __('Account Created'); ?>!</strong> <?php echo __('Your account has been created but needs to be activated by a reviewer.'); ?><br>
                            </div>
                        </div>
                    </div><!-- panel -->
                <?php
                }
                else
                {
                    ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><?php echo __('Inactive Account'); ?></h4>
                            <p><?php echo __('Your account is not active'); ?></p>
                        </div><!-- panel-heading -->
                        <div class="panel-body">
                            <div class="alert alert-info">
                                <strong><?php echo __('Inactive Account'); ?>!</strong> <?php echo __('Your account needs to be activated by a reviewer in the backend.'); ?><br>
                            </div>
                        </div>
                    </div><!-- panel -->
                <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>
