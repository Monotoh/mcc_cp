<?php
/**
 * indexSuccess.php template.
 *
 * Displays list of all of the currently logged in client's invoices
 *
 * @package    frontend
 * @subpackage invoices
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");
 ?>

     <div class="pageheader">
      <h2><i class="fa fa-credit-card"></i> <?php echo __('Billing'); ?></h2>
      <div class="breadcrumb-wrapper">

        <ol class="breadcrumb">
          <li><a href="/index.php"><?php echo __('Home'); ?></a> </li>
          <li class="active"><?php echo __('Billing'); ?></li>
        </ol>
      </div>
    </div>


<div class="contentpanel">


   <div class="panel panel-dark widget-btns">
         <!--          <div class="panel-heading">
                   <h3 class="panel-title"><?php echo __('Invoices'); ?></h3>
                    <p class="text-muted"><?php echo __('Invoice history'); ?></p>
                  </div> -->


                <div class="panel-body panel-body-nopadding">
                        <div class="table-responsive">
                            <?php if ($pager->getResults()): ?>
                            <table class="table mb0" id="table2">
                            <thead>
                                <th>
                                <?php echo __('Application Form'); ?>
                                </th>

                                <th>
                                <?php echo __('Application No'); ?>
                                </th>

                                <th>
                                <?php echo __('Invoices Status'); ?>
                                </th>

                                 <th>
                                 <?php echo __('Date Issued'); ?>
                                </th>

                                 <th class="no-sort" width="80">
                                 <?php echo __('Action'); ?>
                                </th>

                            </thead>
                              <tbody>
                              <?php foreach ($pager->getResults() as $invoice): ?>
                                <tr class="unread">
                                  <td>
                                  <?php
                                    echo $invoice->getFormEntry()->getForm()->getFormName();
                                  ?>
                                  </td>

                                  <td>
                                    <a title='<?php echo __('View'); ?>' href='<?php echo public_path(); ?>index.php/invoices/view/id/<?php echo $invoice->getId(); ?>'><?php echo $invoice->getFormEntry()->getApplicationId(); ?></a>
                                  </td>


                                  <td>
                                  <a title='<?php echo __('View'); ?>' href='<?php echo public_path(); ?>index.php/invoices/view/id/<?php echo $invoice->getId(); ?>'>
								  <?php
                                     $expired = false;

                                     $db_date_event = str_replace('/', '-', $invoice->getExpiresAt());

                                     $db_date_event = strtotime($db_date_event);

                                     if (time() > $db_date_event && !($invoice->getPaid() == "15" || $invoice->getPaid() == "2" || $invoice->getPaid() == "3"))
                                     {
                                          $expired = true;
                                     }

                                     if($expired)
                                     {
                                         echo "Expired";
                                     }
                                     else
                                     {
										     	if($invoice->getPaid() == 1)
										     	{
											    	echo "Not Paid";
										    	}
										        elseif($invoice->getPaid() == 15)
										    	{
										     		echo "Pending Confirmation";
										    	}
                                                elseif($invoice->getPaid() == 2)
                                                {
                                                  echo "Paid";
                                                }
                                                elseif($invoice->getPaid() == 3)
                                                {
                                                    echo "Cancelled";
                                                }
                        }
										     ?></a>

                                  </td>

                                  <td>
                                   <?php echo $invoice->getCreatedAt(); ?>
                                  </td>

                                  <td>
                                  <a title='<?php echo __('View'); ?>' href='<?php echo public_path(); ?>index.php/invoices/view/id/<?php echo $invoice->getId(); ?>'>
							     	<span class="badge badge-primary"><i class="fa fa-eye"></i></span></a>
                                  </td>
                                </tr>

            				   	<?php
								endforeach;
								?>
                              </tbody>
                                <tfoot>
                                <tr>
                                    <th colspan="12">
                                        <p class="table-showing pull-left"><strong><?php echo $pager->getNbResults(); ?></strong> invoices in this stage

                                            <?php if ($pager->haveToPaginate()): ?>
                                                - page <strong><?php echo $pager->getPage() ?>/<?php echo $pager->getLastPage() ?></strong>
                                            <?php endif; ?></p>

                                        <?php if ($pager->haveToPaginate()): ?>
                                            <ul class="pagination pagination-sm mb0 mt0 pull-right">
                                                <li><a href="/index.php/invoices/index/page/1">
                                                        <i class="fa fa-angle-left"></i>
                                                    </a></li>

                                                <li> <a href="/index.php/invoices/index/page/<?php echo $pager->getPreviousPage() ?>">
                                                        <i class="fa fa-angle-left"></i>
                                                    </a></li>

                                                <?php foreach ($pager->getLinks() as $page): ?>
                                                    <?php if ($page == $pager->getPage()): ?>
                                                        <li class="active"><a href=""><?php echo $page ?></a>
                                                    <?php else: ?>
                                                        <li><a href="/index.php/invoices/index/page/<?php echo $page ?>"><?php echo $page ?></a></li>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>

                                                <li> <a href="/index.php/invoices/index/page/<?php echo $pager->getNextPage() ?>">
                                                        <i class="fa fa-angle-right"></i>
                                                    </a></li>

                                                <li> <a href="/index.php/invoices/index/page/<?php echo $pager->getLastPage() ?>">
                                                        <i class="fa fa-angle-right"></i>
                                                    </a></li>
                                            </ul>
                                        <?php endif; ?>

                                    </th>
                                </tr>
                                </tfoot>
                            </table>
                            <?php else: ?>
                                <div class="table-responsive">
                                    <table class="table dt-on-steroids mb0">
                                        <tbody>
                                        <tr><td>
                                                No Records Found
                                            </td></tr>
                                        </tbody>
                                    </table>
                                </div>
                            <?php endif; ?>

                        </div><!-- table-responsive -->

                    </div><!-- panel-body-nopadding pt10 -->
                </div><!-- panel -->







</div><!--Responsive-table-->



</div><!--contentpanel-->
