<?php
/**
 * viewSuccess.php template.
 *
 * Displays the full message
 *
 * @package    frontend
 * @subpackage messages
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");
?>
<?php	
$q = Doctrine_Query::create()
    ->from('FormEntry a')
    ->where('a.id = ?', $communication->getApplicationId());
$application = $q->fetchOne();
?>


<div class="pageheader">
       <h2><i class="fa fa-envelope"></i><?php echo __('Messages'); ?></h2>
      <div class="breadcrumb-wrapper">
        
       <ol class="breadcrumb">
          <li><a href="<?php echo public_path(); ?>index.php"><?php echo __('Home'); ?></a></li>
          <li><a href="<?php echo public_path(); ?>index.php/messages/index"><?php echo __('Messages'); ?></a></li>
        </ol>
      </div>
    </div>
    

<div class="contentpanel">

<div class="col-sm-3 col-lg-2">
   <button class="btn btn-danger btn-block btn-compose-email" onClick="window.location='<?php echo public_path(); ?>index.php/messages/new';"><?php echo __('Compose Message'); ?></button>
       <ul class="nav nav-pills nav-stacked nav-email">
              <li>
                 <a href="<?php echo public_path(); ?>index.php/messages/index">
                    <span class="badge pull-right">2</span>
                    <i class="glyphicon glyphicon-inbox"></i> <?php echo __('Inbox'); ?>
                 </a>
              </li>
              
              <li class="active"><a href="<?php echo public_path(); ?>index.php/messages/sent"><i class="glyphicon glyphicon-send"></i> <?php echo __('Sent Mail'); ?></a></li>
                  
                </ul>
                
       
              </ul>
                
            </div><!-- col-sm-3 -->
            
            <div class="col-sm-9 col-lg-10">
                
                <div class="panel panel-dark mb0">
                  <div class="panel-heading">
                       <h3 class="panel-title"><?php echo __('Application No'); ?> <?php echo $application->getApplicationId(); ?></h3>
                           <div class="panel-btns">
                           <div class="pull-right">
                           <div class="top-btn">
                             <a class="btn btn-primary" href="<?php echo public_path(); ?>index.php/application/view/id/<?php echo $communication->getApplicationId(); ?>"><?php echo __('View Application'); ?></a>
                             </div>
                             </div>
                            </div>
                        </div><!-- panel-heading -->
                        
                    <div class="panel-body">
                      
                      
                         <?php

$q = Doctrine_Query::create()
    ->from('Communications a')
    ->where('a.application_id = ?', $communication->getApplicationId());
$messages = $q->execute();
?>
    <?php

    foreach($messages as $message){
	
    if($message->getArchitectId() != "")
    {
        $q = Doctrine_Query::create()
            ->from('SfGuardUser a')
            ->where('a.id = ?', $message->getArchitectId());
        $architect = $q->fetchOne();

        $fullname = $architect->getProfile()->getFullname();
    }
    else if($message->getReviewerId() != "")
    {
        $q = Doctrine_Query::create()
            ->from('CfUser a')
            ->where('a.nid = ?', $message->getReviewerId());
        $reviewer = $q->fetchOne();

        $fullname = $reviewer->getStrfirstname()." ".$reviewer->getStrlastname();
    }
    ?>
                        
                        <div class="read-panel">
                            
                            <div class="media">
                                <div class="media-body">
                                    <span class="media-meta pull-right"><?php echo $message->getActionTimestamp(); ?></span>
                                    <h4 class="text-primary"><?php echo $fullname; ?></h4>
                                    <small class="text-muted"><?php echo __('From'); ?>: hisemail@hisemail.com</small>
                                </div>
                            </div><!-- media -->
                            
                             <?php
                                 echo $message->getContent();
	                          ?>
                            <div class="mb20"></div>
                          
                            <?php
                              }
                               ?>
                                 <form id="reply_form" name="reply_form" method="post" action="<?php echo public_path(); ?>index.php/messages/view/id/<?php echo $communication->getId(); ?>"  autocomplete="off" data-ajax="false">
                           
                                 <hr>
                                      <textarea name="reply" id="wysiwyg" placeholder="<?php echo __('Enter text here'); ?>..." class="form-control" rows="10" data-autogrow="true"></textarea>
                                 </div>
                             </div>
                        </div><!-- read-panel -->
                        
                    </div><!-- panel-body -->
                    <div class="panel-footer">
                         <button type='submit' class="btn btn-primary"><?php echo __('Send'); ?> </button>
                    </div>
                    </form>
                </div><!-- panel -->
                
            </div><!-- col-sm-9 -->
            
</div><!-- row -->
        
        
</div><!--panel-row-->


</div><!--contentpanel--> 
