<?php
/**
 * articleSuccess.php template.
 *
 * Displays full news article
 *
 * @package    frontend
 * @subpackage news
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>
<?php use_helper('I18N', 'Date') ?>




<?php
foreach($newss as $content)
{
?>
<div class="breadcrumb-box">
<div class="container">
<ul class="breadcrumb">
<li>
<a href="/index.php"><?php echo __('Home'); ?></a>
<span class="divider">/</span>
</li>
<li>
<a href="/index.php/news"><?php echo __('News'); ?></a>
<span class="divider">/</span>
</li>
<li class="active"><?php echo $content->getTitle(); ?></li>
</ul>
</div>
</div>

<!-- end-header -->
  <section id="headline">
    <div class="container">
      <h3><?php echo __('News'); ?></h3>
    </div>
  </section>


 <section class="container page-content" >
    <hr class="vertical-space2">
    <section class="sixteen columns">
      <article class="blog-single-post">
        <div class="post">
          <div class="postmetadata">

            <h6 class="blog-author"><?php echo format_date($content->getCreatedOn(), "D"); ?> /  <?php
						$q = Doctrine_Query::create()
						->from('cfUser a')
						->where('a.nid = ?', $content->getCreatedBy());
						$users = $q->execute();
						foreach($users as $user)
						{
							echo " <strong>by</strong> ".$user->getStrfirstname()." ".$user->getStrlastname();
						}
					?> </h6>
          </div>
          <h1><?php echo $content->getTitle(); ?></h1>
         	<?php echo html_entity_decode($content->getArticle());?>
          <br class="clear">
		   <!-- ==================== End Single Post  ==================== -->


    		<!--  <div class="next-prev-posts">
     			 <a href="#" class="prev-post"><i class="icomoon-arrow-left"></i> Previous Post</a>
     			 <a href="#" class="next-post">Next Post <i class="icomoon-arrow-right-2"></i></a>
     			</div>End next-prev post -->

        </div>
      </article>

<?php
}
?>
    </section>


    <!-- end-main-content -->

    <div class="white-space"></div>
  </section>
  <!-- container -->
