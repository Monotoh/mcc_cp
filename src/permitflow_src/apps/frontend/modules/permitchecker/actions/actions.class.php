<?php

class permitCheckerActions extends sfActions
{
    /**
	 * Executes 'Index' action
	 *
	 * Displays list of all of the currently logged in client's permits
	 *
	 * @param sfRequest $request A request object
	 */
    public function executeIndex(sfWebRequest $request)
    {

    }

    /**
    *
    * Execute 'Openrequest' action
    * 
    * Allows external systems to request for a permit without having an account
    *
    **/
    public function executeOpenrequest(sfWebRequest $request)
    {
       $permit_manager = new PermitManager();
	   $template_parser = new templateparser();

       $reference = $request->getParameter("reference");

       if($request->getParameter("typeid"))
       {
          //Check if specific permit template is allowed to be accessed publicly
          if($permit_manager->has_public_permissions($request->getParameter("typeid")))
          {
            if($_GET['print'])
            {
              $permit_manager->generate_public_pdf($request->getParameter("typeid"), $reference);
            }
            else
            {
              $this->template = $permit_manager->generate_public_html($request->getParameter("typeid"), $reference);
            }
          }
          else
          {
             error_log("Debug-t: ".$request->getParameter('typeid')." not found in public_permits in settings.yml");
             echo "Unauthorized - No permissions for specified template";
          }
       }
       else
       {
          //If no template is explicitly referenced then assume there is a default template in the settings
          $template_id = $permit_manager->get_public_template();

          if($template_id)
          {
            if($_GET['print'])
            {
              $permit_manager->generate_public_pdf($template_id, $reference);
            }//Start - OTB Added for permit checker
            else if($request->getParameter("permitref"))
            {
			  $permitrefno = $request->getParameter("permitref");
			  $q = Doctrine_Query::create()
				 ->from('SavedPermit a')
				 ->leftJoin('a.FormEntry b')
				 ->where('a.permit_id = ? OR b.application_id = ? OR a.id = ?', array($permitrefno,$permitrefno,$permitrefno))
				 ->andWhere('a.permit_status = 1');
			  $permitref = $q->fetchOne();
	
                          if($permitref){
			  $q = Doctrine_Query::create()
				 ->from('FormEntry a')
				 ->where('a.id = ?', $permitref->getApplicationId());
			  $application = $q->fetchOne();

				  $q = Doctrine_Query::create()
					 ->from('PermitCheckerConfig a')
					 ->where('a.permit_template_id = ?', $permitref->getTypeId())
				   ->orderBy('a.sequence_no ASC');
				  $permit_checker_detail = $q->execute();
				  $this->template = '<div><h3><p style="font-weight:bold;">Permit Details</p></h3>';
				  
				  foreach ($permit_checker_detail as $pcdetail){
					$this->template = $this->template.'<p>'.$pcdetail->getLabelToShow().': '.$pcdetail->getValueToShow().'</p>';
				  }
				  $this->template = $this->template.'</div><br/>';
				  
				  $template_parser->application = $application;
				  $this->template = $template_parser->parseReport($template_parser->parse($permitref->getApplicationId(), $application->getFormId(), $application->getEntryId(), $this->template));
				  foreach($application->getMfInvoice() as $invoice){
					$this->template = $template_parser->parseInvoicePDF($permitref->getApplicationId(), $application->getFormId(), $application->getEntryId(), $invoice->getId(), $this->template);
				  }

			  }else{
					$this->template = '<div><h3><p style="color:red;font-weight:bold;">This is an invalid permit. Kindly report anyone using permit reference number '.$permitrefno.'</p></h3></div><br/>';
			  }
            }//End - OTB Added for permit checker
            else
            {
              $this->template = $permit_manager->generate_public_html($template_id, $reference);
            }
          }
          else
          {
            error_log("Debug-t: No config for public_permits in settings.yml");
            echo "<h3Unauthorized - Missing permissions for adhoc access";
          }
       }

       if($_GET['print'])
       {
        $this->setLayout(false);
        exit;
       }
    }

}
