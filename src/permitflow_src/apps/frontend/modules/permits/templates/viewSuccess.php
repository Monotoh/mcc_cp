<?php
/**
 * viewSuccess.php template.
 *
 * Displays a full invoice
 *
 * @package    frontend
 * @subpackage invoices
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");

//Get site config properties
$q = Doctrine_Query::create()
    ->from("ApSettings a")
    ->where("a.id = 1")
    ->orderBy("a.id DESC");
$apsettings = $q->fetchOne();
 ?>
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-6">
        <h4 class="page-title">Services</h4>
    </div>
</div>
<!-- Page-Title -->

<div class="row">
<?php
    if($permit->getFormEntry()->getUserId() == $sf_user->getGuardUser()->getId()) {
        ?>
        <div class="col-lg-12">
            <?php
            $document_key = $permit->getDocumentKey();
            ?>

            <div class="card-box">

                    <?php
                    $templateparser = new TemplateParser();

                    $q = Doctrine_Query::create()
                        ->from("Permits a")
                        ->where("a.id = ?", $permit->getTypeId());
                    $permit_template = $q->fetchOne();

                    $permit_manager = new PermitManager();
                    $html = $permit_manager->generate_permit_template($permit->getId(), false);

                    echo $html;
                    ?>

                    <div class="text-right btn-invoice" style="padding-right: 10px;">
                        <br>
                        <?php
                        if (empty($document_key)) {
                            ?>
                            <button class="btn btn-primary" id="printinvoice" type="button"
                                    onClick="window.location='/index.php/permits/print/id/<?php echo $permit->getId(); ?>';">
                                <i class="fa fa-print mr5"></i> Print Service
                            </button>
                        <?php
                        }
                        if (empty($document_key) && $permit_template->getPartType() == 2) {
                            ?>
                            <button class="btn btn-primary" id="printinvoice" type="button"
                                    onClick="window.location='/index.php/permits/attach/id/<?php echo $permit->getId(); ?>';">
                                <i class="fa fa-print mr5"></i> Attach Signed Copy
                            </button>
                        <?php
                        } elseif (!empty($document_key) && $permit_template->getPartType() == 2) {

                            //Client should only reattach in a corrections stage
                            if($application->getDeclined())
                            {
                            ?>
                            <button class="btn btn-primary" id="printinvoice" type="button"
                                    onClick="window.location='/index.php/permits/attach/id/<?php echo $permit->getId(); ?>';">
                                <i class="fa fa-print mr5"></i> Re-attach Signed Copy
                            </button>
                            <?php
                            }
                            ?>

                            <button class="btn btn-primary" id="printinvoice" type="button"
                                    onClick="window.location='/<?php echo $document_key; ?>';"><i
                                    class="fa fa-print mr5"></i> Print Signed Copy
                            </button>
                        <?php
                        }
                        ?>
                    </div>
                </div>
        </div>

    <?php /*    <!--Display a sidebar with information from the site config-->
        <?php if($apsettings){ ?>
            <div class="col-lg-4">
                <div class="card-box widget-user">
                    <div>
                        <img src="/asset_unified/images/users/avatar-1.jpg" class="img-responsive img-circle" alt="user">
                        <div class="wid-u-info">
                            <h4 class="m-t-0 m-b-5"><?php echo $sf_user->getGuardUser()->getProfile()->getFullname(); ?></h4>
                            <p class="m-b-5 font-13"><?php echo $sf_user->getGuardUser()->getProfile()->getEmail(); ?><br>
                                ID: <?php echo $sf_user->getGuardUser()->getUsername(); ?>
                            </p>
                            <a class="btn btn-primary" href="/index.php/signon/logout">Logout</a>
                        </div>
                    </div>
                </div>

                <?php echo html_entity_decode($apsettings->getOrganisationSidebar()); ?>
            </div>
        <?php } */?>
        <!-- /.marketing -->
    <?php
    }
    else
    {
        echo "<h3>Sorry! You are trying to view a permit that doesn't belong to you</h3>";
    }
    ?>
</div>
