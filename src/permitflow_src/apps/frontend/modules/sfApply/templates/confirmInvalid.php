<?php use_helper('I18N') ?>
<div class="sf_apply_notice">



  <div class="content">

<ul class="breadcrumb">
              <li><a href="#"><?php echo __("Home")?></a> <span class="divider">/</span></li>
              <li class="active"><?php echo __("Confirmation invalid")?></li>
            </ul>    <!-- Docs nav
    ================================================== -->
    <div class="row">
     
      <div class="span12" >



        <!-- Download
        ================================================== -->
        <section id="content-container">
        
          
		<div class="span6 offset3 well2 padded-20" >
			<legend><?php echo __("Lost Password Recovery");?></legend>
			
              <div class="alert alert-error">
                  <?php echo __("That confirmation code is invalid");?>
			   </div>
               
               
               
               <?php echo __(<<<EOM
<p>
This may be because you have already confirmed your account. If so,
just click on the "Log In" button to log in.
</p>
<h4>
Other possible explanations:
</h4>

<ol>
<li>
If you copied and pasted the URL from
your confirmation email, please make sure you did so correctly and
completely.
</li>
<li>
 If you received this confirmation email a long time ago
and never confirmed your account, it is possible that your account has
been purged from the system. In that case, you should simply apply
for a new account.
</li>
</ol>
EOM
) ?>
<?php include_partial('sfApply/continue') ?>
	  
	  
	  
	  
	  
	  
  </section>
	
</div>
    </div>









