<?php use_helper('I18N') ?>
<?php slot('sf_apply_login') ?>
<?php end_slot() ?>


<div class="breadcrumb-box">
<div class="container">
<ul class="breadcrumb">
<li>
<a href=""><?php echo __('Home'); ?></a>
<span class="divider">/</span>
</li>
<li class="active"><?php echo __('Forgot Your Password?'); ?></li>
</ul>
</div>
</div>

  <section id="headline">
    <div class="container">
      <h3><?php echo __('Forgot Your Password?'); ?></h3>
    </div>
  </section>



        <!-- BEGIN CONTAINER -->
        <div class="container margin-bottom-40">
           <div class="row signinpanel">

            <div class="eleven columns offset-by-five login-signup-page mb40">





            <form method="POST" action="<?php echo public_path(); ?>index.php/reset-request" name="sf_apply_reset_request" id="sf_apply_reset_request" autocomplete="off" data-ajax="false">
            <h4 class="nomargin"><?php echo __('Lost Password Recovery'); ?></h4>
            <p class="mt5 mb20"><?php echo __('If you have forgotten your username or password, you can request to have your username emailed to you and to reset your password. When you fill in your registered email address, you will be sent instructions on how to reset your password.'); ?></p>

            <?php if(isset($form['_csrf_token'])): ?>
      <?php echo $form['_csrf_token']->render(); ?>
      <?php endif; ?>
<div class="control-group">

<label class="control-label" for="focusedInput"><?php echo __('Email or Username'); ?></label>
<div class="controls">
    <div class="input-prepend input-append">
    <span class="add-on"><input maxlength="100" class="form-control" type="text" name="sfApplyResetRequest[username_or_email]" id="sfApplyResetRequest_username_or_email">
</div>
</div>
</div>
<button type="submit" name='submit' class="btn btn-primary btn-block" style="background-color: #428bca !important; color:#ffffff !important;"><?php echo __('Reset My Password'); ?></button>
</form>



                 <!-- col-sm-5 -->
                </form>
            </div>
        </div><!-- row -->
        </div>
        <!-- END CONTAINER -->
