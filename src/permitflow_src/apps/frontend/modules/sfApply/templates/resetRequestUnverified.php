<?php use_helper('I18N') ?>


  <div class="content">

<ul class="breadcrumb">
              <li><a href="#"><?php echo __("Home");?></a> <span class="divider">/</span></li>
              <li class="active"><?php echo __("Forgot your password?");?></li>
            </ul>    <!-- Docs nav
    ================================================== -->
    <div class="row">
     
      <div class="span12" >



        <!-- Download
        ================================================== -->
        <section id="content-container">
        
          
		<div class="span4 offset4 well2 padded-20" >
			<legend><?php echo __("Lost Password Recovery");?></legend>
			
              <div class="alert alert-info">
                      <?php echo(__(<<<EOM
That account was never verified. You must verify the account before you can log in or, if
necessary, reset the password. We have resent your verification email, which contains
instructions to verify your account. If you do not see that email, please be sure to check 
your "spam" or "bulk" folder.
EOM
)) ?>
			   </div>
	  
<?php include_partial('sfApply/continue') ?>
	  
	  
	  
	  
  </section>
	
</div>
</div>








      











