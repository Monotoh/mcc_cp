<script src="<?php echo public_path(); ?>assets_unified/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/jquery-migrate-1.2.1.min.js"></script>
<script language="javascript">
    $('document').ready(function () {
        $('#sfApplyApply_username').keyup(function () {
            $.ajax({
                type: "POST",
                url: "/index.php/index/checkuser",
                data: {
                    'name': $('input:text[id=sfApplyApply_username]').val()
                },
                dataType: "text",
                success: function (msg) {
                    //Receiving the result of search here
                    // alert('Test...');
                    $("#usernameresult").html(msg);
                    if (msg == '0') {
                        $("#usernameresult").html('<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Username is already in use!</strong></div>');
                        $('#submit_app').prop('disabled', true);
                    }
                    if (msg == '1') {
                        $("#usernameresult").html('<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Username is available!</strong></div>');
                        $('#submit_app').prop('disabled', false);
                    }
                }
            });
            // alert('Test...');
        });
    });
</script>

<?php
/**
 * indexSuccess.php template.
 *
 * Displays registration form for clients
 *
 * @package    sfDoctrineApplyPlugin
 * @subpackage sfGuardRegister
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
  use_helper('I18N');
?>

<div style="background:url(<?php echo public_path(); ?>permitflow/images/backgrounds/bg-6.jpg);">
<div class="row">
  <div class="container">

            <div class="col-md-offset-3 col-md-6 text-box wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;float:left;margin-top:30px; margin-bottom:67px;">

                      <div class="headline">

                        <h3><?php echo __('CREATE AN ACCOUNT'); ?></h3>
                      </div><!-- headline -->

                      <h4 class="text-default-color"><?php echo __('Already a member?'); ?> <a class="signup" href="<?php echo public_path(); ?>index.php/login"><?php echo __('Sign In'); ?></a></h4>


                <form method="post" id="registration_form" name="registration_form" action="<?php echo public_path(); ?>index.php/apply" method="post" enctype="multipart/form-data" autocomplete="off" data-ajax="false"  onsubmit="javascript:return validateall();">

                    <?php
            $class = sfConfig::get('app_sf_apply_apply_form', 'sfApplyApplyForm');
            $form = new $class();
           					?>
					<?php if(isset($form['_csrf_token'])): ?>
					        <?php echo $form['_csrf_token']->render(); ?>
					<?php endif;?>


                   <div class="mb10">
                    <label class="control-label"><?php echo __('Full Name'); ?></label>
                    <input class="validate[required,custom[onlyLetterSp],minSize[3]] form-control" type="text" name="sfApplyApply[fullname]" id="sfApplyApply_fullname" required>
                    </div>

                    <div class="mb10">
                    <label class="control-label"><?php echo __('Username'); ?></label>
                    <input class="validate[required,custom[onlyLetterNumber],minSize[3]] form-control" type="text" name="sfApplyApply[username]" id="sfApplyApply_username" required>
				    <div id='checkusername' name='checkusername'></div>
                    </div>
                    
                  <div id="usernameresult" name="usernameresult"></div>

                  <script language="javascript">
                    $('document').ready(function(){
                      $('#sfApplyApply_username').keyup(function(){
                        $.ajax({
                                  type: "POST",
                                  url: "/index.php/index/checkuser",
                                  data: {
                                      'name' : $('input:text[id=sfApplyApply_username]').val()
                                  },
                                  dataType: "text",
                                  success: function(msg){
                                        //Receiving the result of search here
                                        $("#usernameresult").html(msg);
                                  }
                              });
                          });
                    });
                  </script>
                    

                    <div class="mb10">
                        <label class="control-label"><?php echo __('Enter Email'); ?></label>
                       <input type="email"  class="validate[required,custom[email]] form-control" name="sfApplyApply[email]" id="sfApplyApply_email" required><span><?php // echo $form['email']->renderError() ?></span>
					             <div id='checkemail' name='checkemail'></div>
                    </div>

					          <div id="emailresult" name="emailresult"></div>

                    <div class="mb10">
                       <label class="control-label"><?php echo __('Confirm Email'); ?></label>
                       <input class="validate[required,equals[email-1]] form-control" type="email" name="sfApplyApply[email2]" id="sfApplyApply_email2" required>
					  <div id='confirmemail' name='confirmemail'></div>
                    </div>
                    
                    <div id="confirmemailresult" name="confirmemailresult"></div>

                    <script language="javascript">
                      $('document').ready(function(){
                        $('#sfApplyApply_email').keyup(function(){
                          $.ajax({
                                type: "POST",
                                url: "/index.php/index/checkemail",
                                data: {
                                  'email' : $('#sfApplyApply_email').val()
                                },
                                dataType: "text",
                                success: function(msg){
                                    //Receiving the result of search here
                                    $("#emailresult").html(msg);
                                }
                              });
                          if($('#sfApplyApply_email').val() == $('#sfApplyApply_email2').val() && $('#sfApplyApply_email').val() != "")
                          {
                            $('#confirmemailresult').html('<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Emails match!</strong></div>');
                            $('#submit_app').prop('disabled',false);
                          }
                          else
                          {
                            $('#confirmemailresult').html('<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Emails don\'t match!</strong> Try again.</div>');
                            $('#submit_app').prop('disabled',true);
                          }
                        });
                        $('#sfApplyApply_email2').keyup(function(){
                          if($('#sfApplyApply_email').val() == $('#sfApplyApply_email2').val() && $('#sfApplyApply_email').val() != "")
                          {
                            $('#confirmemailresult').html('<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Emails match!</strong></div>');
                            $('#submit_app').prop('disabled',false);
                          }
                          else
                          {
                            $('#confirmemailresult').html('<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Emails don\'t match!</strong> Try again.</div>');
                            $('#submit_app').prop('disabled',true);
                          }
                        });
                      });
                    </script>

                    <div class="mb10">
                        <label class="control-label"><?php echo __('New Password'); ?></label>
                        <input class="password validate[required] form-control" type="password" title='Minimum of Six Characters' data-min="6" name="sfApplyApply[password]" id="sfApplyApply_password" required>
				    	<div id='checkpassword' name='checkpassword'></div>
                    </div><span><?php // echo $form['password']->renderError() ?></span>

                    <div class="mb10">
                        <label class="control-label"><?php echo __('Confirm Password'); ?></label>
                        <input class="form-control" type="password" onKeyup='confirmpassword();'  title='Minimum of Six Characters' data-min="6"  name="sfApplyApply[password2]" id="sfApplyApply_password2" required>
					    <input type="hidden" name="sfApplyApply[id]" id="sfApplyApply_id"/>
						<div id='confirmpassword' name='confirmpassword'></div>
                    </div>
                    
                    <div id="passwordresult" name="passwordresult"></div>

                    <script language="javascript">
                      $('document').ready(function(){
                        $('#sfApplyApply_password').keyup(function(){
                          if ($('#sfApplyApply_password').val() == $('#sfApplyApply_password2').val() && $('#sfApplyApply_password').val() != "" && $('#sfApplyApply_password').val().length >= 6)
                            {
                                $('#passwordresult').html('<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Passwords match!</strong></div>');
                                $('#submit_app').prop('disabled', false);
                            }
                            else
                            {
                                $('#passwordresult').html('<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Passwords don\'t match!</strong> Try again.</div>');
                                if ($('#sfApplyApply_password').val().length < 6) {
                                    var html_result = $('#passwordresult').html();
                                    $('#passwordresult').html(html_result + '<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Password length is less than 6 characters</strong>Try again.</div>');
                                }
                                $('#submit_app').prop('disabled', true);
                            }
                        });
                        $('#sfApplyApply_password2').keyup(function(){
                          if($('#sfApplyApply_password').val() == $('#sfApplyApply_password2').val() && $('#sfApplyApply_password').val() != "" && $('#sfApplyApply_password').val().length >= 6)
                          {
                            $('#passwordresult').html('<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Passwords match!</strong></div>');
                            $('#submit_app').prop('disabled',false);
                          }
                          else
                          {
                            $('#passwordresult').html('<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Passwords don\'t match!</strong> Try again.</div>');
                                if ($('#sfApplyApply_password').val().length < 6) {
                                    var html_result = $('#passwordresult').html();
                                    $('#passwordresult').html(html_result + '<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Password length is less than 6 characters</strong>Try again.</div>');
                                }
                                $('#submit_app').prop('disabled', true);
                          }
                        });
                      });
                    </script>

                       <div class="mb10">
                        <label class="control-label"><?php echo __('Mobile Phone (e.g 26627007386 or 27007386)'); ?></label>
                        <input type="text" class="validate[required,custom[phone],minSize[10],maxSize[12]] form-control" name="sfApplyApply[mobile]" id="sfApplyApply_mobile" required>
                    </div>

                       <div class="mb10">
                        <label class="control-label"><?php echo __('Register As'); ?></label>
                        <select style="width:100%;" name="sfApplyApply[registeras]" id="sfApplyApply_registeras" required>
							<?php

                            $q = Doctrine_Query::create()

                               ->from("SfGuardUserCategories a")

                               ->orderBy("a.orderid ASC");

                            $cats = $q->execute();

                            foreach($cats as $cat)
                            {

                                echo "<option value='".$cat->getId()."'>".$cat->getName()."</option>";

                            }
                            ?>
					</select>
                    </div>


                       <div class="mb10">
                        <label class="control-label"><?php echo __('Terms and Conditions'); ?></label>
							<div>
								<?php /*?><?php

									$q = Doctrine_Query::create()

										->from("Content a")

										->where("a.id = ?", 18);

									$terms = $q->fetchOne();

									echo $terms->getTopArticle();

								?><?php */?>
                                <?php echo __('By clicking on "Save and Continue" below, you are agreeing to the'); ?> <a href="" target="_blank"><?php echo __('Terms of services'); ?></a> <?php echo __('and the'); ?> <a href="" target="_blank"><?php echo __('Privacy Policy'); ?></a>.
							</div>
                    </div>

                    <button class="btn btn-white btn-block" type="submit" name="submit_app" id="submit_app" value="submitbuttonvalue"><?php echo __('Save and Continue'); ?></button>
                </form>
            </div><!-- col-sm-6 -->
            </div>


                    </div>
         </div>
 </div>
       