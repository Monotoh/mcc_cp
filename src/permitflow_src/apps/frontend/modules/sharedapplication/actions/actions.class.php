<?php
/**
 * Sharedapplication actions.
 *
 * Displays shared applications submitted by the client
 *
 * @package    frontend
 * @subpackage sharedapplication
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
class sharedapplicationActions extends sfActions
{
        /**
	 * Executes 'Index' action
	 *
	 * Displays list of all of the currently logged in client's shared applications
	 *
	 * @param sfRequest $request A request object
	 */
    public function executeIndex(sfWebRequest $request)
    {
            $this->page = $request->getParameter('page', 1);
		$this->setLayout("layoutdash");
    }

        /**
	 * Executes 'Unshare' action
	 *
	 * Stop sharing selected application with another client
	 *
	 * @param sfRequest $request A request object
	 */
        public function executeUnshare(sfWebRequest $request)
        {
                  $q = Doctrine_Query::create()
                        ->from('FormEntryShares a')
                        ->where('a.Formentryid = ?', $request->getParameter("id"))
                        ->andWhere('a.senderid = ?', $this->getUser()->getGuardUser()->getId());
                  $shareapplication = $q->fetchOne();
                  $shareapplication->delete();
                  
                  $this->redirect("/index.php/sharedapplication/index");
        }

  /**
	 * Executes 'Unshare' action
	 *
	 * Stop sharing selected application with me
	 *
	 * @param sfRequest $request A request object
	 */
    public function executeUnshareme(sfWebRequest $request)
    {
              $q = Doctrine_Query::create()
                    ->from('FormEntryShares a')
                    ->where('a.Formentryid = ?', $request->getParameter("id"))
                    ->andWhere('a.receiverid = ?', $this->getUser()->getGuardUser()->getId());
              $shareapplication = $q->fetchOne();
              $shareapplication->delete();
              
              $this->redirect("/index.php/sharedapplication/index");
    }

  /**
   * Executes 'View' action
   *
   * View Shared Application
   *
   * @param sfRequest $request A request object
   */
    public function executeView(sfWebRequest $request)
    {
             $q = Doctrine_Query::create()
                    ->from('FormEntry a')
                    ->where('a.id = ?', $request->getParameter("id"));
             $this->application = $q->fetchOne();
		$this->setLayout("layoutdash");
    } 
  
}
