<?php
/**
 * indexSuccess.php template.
 *
 * Displays list of all of the currently logged in client's shared applications
 *
 * @package    frontend
 * @subpackage sharedapplication
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

function GetDays($sEndDate, $sStartDate){  
        $aDays[] = $start_date;
	$start_date  = $sStartDate;
	$end_date = $sEndDate;
	$current_date = $start_date;
	while(strtotime($current_date) <= strtotime($end_date))
	{
		    $aDays[] = gmdate("Y-m-d", strtotime("+1 day", strtotime($current_date)));
		    $current_date = gmdate("Y-m-d", strtotime("+1 day", strtotime($current_date)));
	}
        return $aDays;  
} 
?>
<?php
	$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
	mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
	
	
	
	$filter = $_GET['filter'];
	if(!empty($filter))
	{
		$filter = " AND b.approved = '".$filter."'";
	}
	
	
?>


     <div class="pageheader">
        <h2><i class="fa fa-share-square"></i>Shared Applications<span>This page list all the applications sent to you by other people</span></h2>
      <div class="breadcrumb-wrapper">
        
        <ol class="breadcrumb">
          <li><a href="#">Shared Applications</a></li>
          <li class="active">Submitted Plans</li>
        </ol>
      </div>
    </div>
    
    
    
    
    
    






         
          <div class="accordion" id="accordion2">
                <div class="accordion-group whitened">
                    <div class="accordion-heading" style="padding:10px 10px 0 10px; text-align:right;">
                    
                    <!--<label class="span1 table-lable">FIlter :</label>-->

                                  
                  
              <select  class="span5" id='application_status' name='application_status' onChange="window.location='<?php echo public_path(); ?>index.php/sharedapplication/index?filter=' + this.value;">
			    <option value="" >Filter By Plan Navigation</option>
				<?php
				$q = Doctrine_Query::create()
				  ->from('Menus a')
				  ->orderBy('a.order_no ASC');
				$stagegroups = $q->execute();
				foreach($stagegroups as $stagegroup)
				{
				    echo "<optgroup label='".$stagegroup->getTitle()."'>";
					$q = Doctrine_Query::create()
					  ->from('SubMenus a')
					  ->where('a.menu_id = ?', $stagegroup->getId())
					  ->orderBy('a.order_no ASC');
					$stages = $q->execute();
					
					foreach($stages as $stage)
					{
					    $selected = "";
						
						if($_GET['filter'] != "" && $_GET['filter'] == $stage->getId())
						{
							$selected = "selected";
						}
						
						$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
						mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
						$query = "SELECT a.*, b.* FROM form_entry a LEFT JOIN form_entry_shares b ON a.id = b.formentryid  WHERE a.approved = '".$stage->getId()."' AND b.formentryid <> ''";
						$result = mysql_query($query,$dbconn);

						
						echo "<option value='".$stage->getId()."' ".$selected.">".$stage->getTitle()." (".mysql_num_rows($result).")</option>";
					}
				    echo "</optgroup>";
				}
				
				echo "<optgroup label='Others'>";
				
				$q = Doctrine_Query::create()
				  ->from('SubMenus a')
				  ->where('a.menu_id = ?', '0')
				  ->orderBy('a.order_no ASC');
				$stages = $q->execute();
				
				foreach($stages as $stage)
				{
					$selected = "";
					
					if($application_status != "" && $application_status == $stage->getId())
					{
						$selected = "selected";
					}
					
					$q = Doctrine_Query::create()
						 ->from('FormEntry a')
						 ->where('a.user_id = ?', $sf_user->getGuardUser()->getId())
						 ->andWhere('a.approved = ?', $stage->getId());
					$menuapplications = $q->execute();
					
					echo "<option value='".$stage->getId()."' ".$selected.">".$stage->getTitle()." (".sizeof($menuapplications).")</option>";
				
				}
				
				echo "</optgroup>";
				
				?>
			</select>
                   
                            
                  </div><!-- accordion-heading -->
                  <div class="accordion-body in">
                    <div class="accordion-inner unspaced">
                    
                    <?php

if($_GET['filter'])
{
	$q = Doctrine_Query::create()
	   ->from("FormEntryShares a")
	   ->where("a.receiverid = ?", $sf_user->getGuardUser()->getId())
	   ->orWhere("a.senderid = ?", $sf_user->getGuardUser()->getId())
	   ->orderBy("a.id DESC");
	   
	   

	 $pager = new sfDoctrinePager('FormEntryShares', 10);
	 $pager->setQuery($q);
	 $pager->setPage($page);
	 $pager->init();
	
	 $counter = 1;
	 include_partial('sharedapplication/list', array('sharedapplications' => $pager->getResults(),'filter' => $_GET['filter']));
	 
}
else
{
	$q = Doctrine_Query::create()
	   ->from("FormEntryShares a")
	   ->where("a.receiverid = ?", $sf_user->getGuardUser()->getId())
	   ->orWhere("a.senderid = ?", $sf_user->getGuardUser()->getId())
	   ->orderBy("a.id DESC");
	   
	 $pager = new sfDoctrinePager('FormEntryShares', 10);
	 $pager->setQuery($q);
	 $pager->setPage($page);
	 $pager->init();
	
	 $counter = 1;
	 include_partial('sharedapplication/list', array('sharedapplications' => $pager->getResults()));
	 

}
?>
                  
                    </div><!-- accordion-inner -->
                  </div><!-- accordion-body -->
                </div><!-- accordion-group -->
          </div><!-- accordion2 -->

  
  
  
  
                
                
<div class="page-footer">

<ul class="pagination">
<?php if ($pager->haveToPaginate()): ?>
	<?php
    $filter = "";
	if($_GET['filter'])
	{
		$filter = "&filter=".$_GET['filter'];
	}
  ?>
  <li><?php echo "<a title='First' href='".public_path()."index.php/sharedapplication/index?page=1".$filter."'><i class=\"fa fa-angle-left\"></i></a>"; ?></li>
  <li><?php echo "<a title='Previous' href='".public_path()."index.php/sharedapplication/index?page=".$pager->getPreviousPage()."".$filter."'><i class=\"fa fa-angle-left\"></i></a>"; ?></li>
  
  <?php foreach ($pager->getLinks() as $page): ?>
    <?php
	if($pager->getPage() == $page)
	{
		?>
		<li class="active"><a><?php echo $page; ?></a></li>
		<?php
	}
	else
	{
	?>
		<li><?php echo "<a title='Page ".$page."' href='".public_path()."index.php/sharedapplication/index?page=".$page."".$filter."'>".$page."</a>"; ?></li>
	<?php
	}
	?>
  <?php endforeach; ?>

 <li> <?php echo "<a title='Next' href='".public_path()."index.php/sharedapplication/index?page=".$pager->getNextPage()."".$filter."'><i class=\"fa fa-angle-right\"></i></a>"; ?></li>
 <li> <?php echo "<a title='Last' href='".public_path()."index.php/sharedapplication/index?page=".$pager->getLastPage()."".$filter."'><i class=\"fa fa-angle-right\"></i></a>"; ?></li>
<?php endif; ?>
      
 </div><!-- /.page-footer -->
                
                             





