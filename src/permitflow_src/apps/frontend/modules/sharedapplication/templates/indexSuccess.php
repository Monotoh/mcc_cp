<?php
/**
 * indexSuccess.php template.
 *
 * Displays list of all of the currently logged in client's shared applications
 *
 * @package    frontend
 * @subpackage sharedapplication
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper('I18N');

function GetDays($sEndDate, $sStartDate){  
        $aDays[] = $start_date;
	$start_date  = $sStartDate;
	$end_date = $sEndDate;
	$current_date = $start_date;
	while(strtotime($current_date) <= strtotime($end_date))
	{
		    $aDays[] = gmdate("Y-m-d", strtotime("+1 day", strtotime($current_date)));
		    $current_date = gmdate("Y-m-d", strtotime("+1 day", strtotime($current_date)));
	}
        return $aDays;  
} 
?>
<?php
	$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
	mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
	
	
	
	$filter = $_GET['filter'];
	if(!empty($filter))
	{
		$filter = " AND b.approved = '".$filter."'";
	}
	
	
?>

  
    
<div class="contentpanel">

    <div class="row">
        <!--Display organisation description from the site config-->
        <div class="col-sm-6 col-lg-8">
            <div class="card-box card-box-light-orange widget-user">
                <div>
                    <img src="/asset_unified/images/users/avatar-1.jpg" class="img-responsive img-circle" alt="user">
                    <div class="wid-u-info">
                      <?php
                      $q = Doctrine_Query::create()
                      ->from('sfGuardUserProfile a')
                      ->where('a.user_id = ?', $sf_user->getGuardUser()->getId());
                      $user = $q->fetchOne();
                      ?>
                        <h4 class="m-t-0 m-b-5"><?php echo __('Welcome'); ?>, <?php echo $user->getFullname(); ?></h4>
                        <?php
                        if($apsettings)
                        {
                        ?>
                           <p class="m-b-5 font-13"><?php echo $apsettings->getOrganisationDescription(); ?></p>
                        <?php
                        }
                        else
                        {
                        ?>
                           <p class="m-b-5 font-13"><?php echo sfConfig::get('app_organisation_description') ?></p>
                        <?php
                        }
                        ?>
                        <a class="m-t-5 btn btn-sm btn-default" href="<?php echo public_path(); ?>index.php/settings"><?php echo __('My Profile'); ?></a>
                        <a class="m-t-5 btn btn-sm btn-danger" href="<?php echo url_for('signon/logout') ?>"><?php echo __('Log Out'); ?></a>
                      </div>
                </div>
            </div>
        </div>

        <!--Display the organisation help info from the site config (Appears on the sidebar)-->
        <?php if($apsettings){ ?>
        <div class="col-sm-6 col-lg-4">
            <div class="card-box widget-user">
                <?php echo html_entity_decode($apsettings->getOrganisationHelp()); ?>
            </div>
        </div>
        <?php } ?>
      </div>
     <div class="row">
       
           <div class="col-sm-12 col-lg-12">
                  
                    
                    <?php

if($_GET['filter'])
{
	$q = Doctrine_Query::create()
	   ->from("FormEntryShares a")
	   ->where("a.receiverid = ?", $sf_user->getGuardUser()->getId())
	   ->orWhere("a.senderid = ?", $sf_user->getGuardUser()->getId())
	   ->orderBy("a.id DESC");
	   
	   

	 $pager = new sfDoctrinePager('FormEntryShares', 10);
	 $pager->setQuery($q);
	 $pager->setPage($page);
	 $pager->init();
	
	 $counter = 1;
	 include_partial('sharedapplication/list', array('sharedapplications' => $pager->getResults(),'filter' => $_GET['filter']));
	 
}
else
{
	$q = Doctrine_Query::create()
	   ->from("FormEntryShares a")
	   ->where("a.receiverid = ?", $sf_user->getGuardUser()->getId())
	   ->orWhere("a.senderid = ?", $sf_user->getGuardUser()->getId())
	   ->orderBy("a.id DESC");
	   
	 $pager = new sfDoctrinePager('FormEntryShares', 10);
	 $pager->setQuery($q);
	 $pager->setPage($page);
	 $pager->init();
	
	 $counter = 1;
	 include_partial('sharedapplication/list', array('sharedapplications' => $pager->getResults()));
	 

}
?>
              
              
              <?php /*?><ul class="pagination">
<?php if ($pager->haveToPaginate()): ?>
	<?php
    $filter = "";
	if($_GET['filter'])
	{
		$filter = "&filter=".$_GET['filter'];
	}
  ?>
  <li><?php echo "<a title='First' href='".public_path()."index.php/sharedapplication/index?page=1".$filter."'><i class=\"fa fa-angle-left\"></i></a>"; ?></li>
  <li><?php echo "<a title='Previous' href='".public_path()."index.php/sharedapplication/index?page=".$pager->getPreviousPage()."".$filter."'><i class=\"fa fa-angle-left\"></i></a>"; ?></li>
  
  <?php foreach ($pager->getLinks() as $page): ?>
    <?php
	if($pager->getPage() == $page)
	{
		?>
		<li class="active"><a><?php echo $page; ?></a></li>
		<?php
	}
	else
	{
	?>
		<li><?php echo "<a title='Page ".$page."' href='".public_path()."index.php/sharedapplication/index?page=".$page."".$filter."'>".$page."</a>"; ?></li>
	<?php
	}
	?>
  <?php endforeach; ?>

 <li> <?php echo "<a title='Next' href='".public_path()."index.php/sharedapplication/index?page=".$pager->getNextPage()."".$filter."'><i class=\"fa fa-angle-right\"></i></a>"; ?></li>
 <li> <?php echo "<a title='Last' href='".public_path()."index.php/sharedapplication/index?page=".$pager->getLastPage()."".$filter."'><i class=\"fa fa-angle-right\"></i></a>"; ?></li>
<?php endif; ?>
</ul><!-- /.pagination --><?php */?>

    
       
            </div><!-- col-sm-9 -->
        </div><!-- row -->
  
  
                

</div><!-- row -->
         
 <div><!-- content panel -->       
                             



