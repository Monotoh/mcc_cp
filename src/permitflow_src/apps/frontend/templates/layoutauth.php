<?php
/**
 * Frontend Layout.
 *
 * Main layout for the frontend
 *
 * @package		Frontend
 * @theme		Kigali.CP
 * @author		Webmasters Africa (info@webmastersafrica.com)
 */

 //Logout backend users so they don't clash with frontend security & set language
 include_component('index', 'checksession')
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="Permitflow - Gateway to All Government Services">
  <meta name="author" content="Permitflow">
  <title><?php echo sfConfig::get('app_organisation_name'); ?> - Gateway to All Government Services</title>

 <!-- Keeping all css in one file. Keeping the layout tidy. -->
		<?php include_component('index', 'stylesheetsauth') ?>
</head>

<body class="signin">

<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

    <section>



    <div class="signuppaneled">
		<div class="row">
    		<div class="col-md-9">
            <!-- The following div must have id='content' or else file uploads will fail -->
            <span class="thecontent" id="content">
                <?php echo $sf_content ?>
            </span>
            </div>
        </div>
     </div>

    </section>
	<!-- Keeping all javascripts in one file. Keeping the layout tidy. -->
	<?php include_component('index', 'javascriptsauth') ?>
</body>
</html>
