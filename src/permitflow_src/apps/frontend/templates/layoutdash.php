<?php
/**
 * Frontend Layout.
 *
 * Main layout for the frontend
 *
 * @package		Frontend
 * @theme		Kigali.CP
 * @author		Webmasters Africa (info@webmastersafrica.com)
 */

 //Logout backend users so they don't clash with frontend security & set language
 include_component('index', 'checksession')
?>

<!DOCTYPE html>
<html lang="en">
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
      <meta name="description" content="Permitflow - Gateway to All Government Services">
      <meta name="author" content="Permitflow">
      <title><?php echo sfConfig::get('app_organisation_name'); ?></title>

        <!-- Keeping all css in one file. Keeping the layout tidy. -->
        <?php include_component('index', 'stylesheetsdash') ?>

        <!-- Keeping all javascripts in one file. Keeping the layout tidy. -->
        <?php include_component('index', 'javascriptsdash') ?>

    </head>

    <body>
          <?php
          if($sf_user->isAuthenticated())
          {
            include_component('index', 'headerdash');
          }
          ?>

          <?php
          //Load the banner if we are currently in the homepage
          if($_SERVER['REQUEST_URI'] == "/index.php" || $_SERVER['REQUEST_URI'] == "/" || $_SERVER['REQUEST_URI'] == "/index.php/")
          {
            include_component('index', 'banner');
          }
          ?>


          <div class="wrapper">
            <div class="container" id="content">

              <?php echo $sf_content ?>

                <!-- Footer -->
                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                               Copyright © 
                                <?php
                                $q = Doctrine_Query::create()
                                   ->from("ApSettings a")
                                   ->where("a.id = 1")
                                   ->orderBy("a.id DESC");
                                $apsettings = $q->fetchOne();
                                if($apsettings)
                                {
                                ?>
                                <?php echo $apsettings->getOrganisationName(); ?>
                                <?php
                                }
                                else
                                {
                                ?>
                                <?php echo sfConfig::get('app_organisation_name') ?>
                                <?php
                                }
                                ?>
                                <?php echo date("Y"); ?>. All rights Reserved.

                                <?php

                                $q = Doctrine_Query::create()
                                    ->from("ExtLocales a")
                                    ->orderBy("a.local_title ASC");

                                $locales = $q->execute();

                                if($q->count() > 0)
                                {
                                    ?>

                                    <div class="btn-group dropup pull-right">
                                        <button type="button" class="btn btn-xs btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false"><?php echo __('Lang'); ?> <span class="caret"></span></button>
                                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                            <?php
                                            foreach($locales as $locale)
                                            {
                                                $selected = "";
                                                if($locale->getLocaleIdentifier() == $sf_user->getCulture())
                                                {
                                                    $selected = "active";
                                                }
                                                ?>
                                                <li class="<?php echo $selected; ?>"><a href="<?php echo public_path(); ?>index.php/index/setlocale/code/<?php echo $locale->getLocaleIdentifier(); ?>"><?php echo $locale->getLocalTitle(); ?></a></li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                    <?php
                                }
                                ?>

                            </div>
                        </div>
                    </div>
                </footer>
                <!-- End Footer -->

            </div> <!-- end container -->
          </div>
        <!-- End wrapper -->
    </body>
</html>
