<?php
/**
 * Frontend Layout.
 *
 * Main layout for the frontend
 *
 * @package		Frontend
 * @theme		Kigali.CP
 * @author		Webmasters Africa (info@webmastersafrica.com)
 */

 //Logout backend users so they don't clash with frontend security & set language
 include_component('index', 'checksession')
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="eCitizen - Gateway to All Government Services">
  <meta name="author" content="eCitizen">
  <title>eCitizen - Gateway to All Government Services</title>

 <!-- Keeping all css in one file. Keeping the layout tidy. -->
		<?php include_component('index', 'stylesheetsdash') ?>
    
    <?php
    if(sfConfig::get('app_zopim_key'))
    {
    ?>
    <!--Start of Zopim Live Chat Script-->
    <script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
    d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
    $.src="//v2.zopim.com/?<?php echo sfConfig::get('app_zopim_key'); ?>";z.t=+new Date;$.
    type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
    </script>
    <!--End of Zopim Live Chat Script-->
    <?php
    }
    ?>
</head>

<body>

            <!-- The following div must have id='content' or else file uploads will fail -->
            <span class="thecontent" id="content">
                <?php echo $sf_content ?>
            </span>
       

	<!-- Keeping all javascripts in one file. Keeping the layout tidy. -->
	<?php include_component('index', 'javascriptsdash') ?>
</body>
</html>
