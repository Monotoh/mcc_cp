<!doctype html>
<html lang="en-us">
<head>
    <meta charset="utf-8">

    <title>MasterPMIS | 404 Page Not Found</title>

    <meta name="description" content="">
    <meta name="author" content="revaxarts.com">


    <!-- Apple iOS and Android stuff -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="apple-touch-icon-precomposed" href="img/icon.png">
    <link rel="apple-touch-startup-image" href="img/startup.png">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">

    <!-- Google Font and style definitions -->
    <link rel="stylesheet" href="assets_backend/style.css">

    <!-- include the skins (change to dark if you like) -->
    <link rel="stylesheet" href="assets_backend/light/theme.css" id="themestyle">
    <!-- <link rel="stylesheet" href="css/dark/theme.css" id="themestyle"> -->

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <link rel="stylesheet" href="css/ie.css">
    <![endif]-->

</head>
<body id="error">
<header>
    <div id="logo">
        <a href="login.html">MasterPMIS</a>
    </div>
</header>
<section id="content">
    <h1>404</h1>
    <h2>Page Not Found</h2>
    <hr>
    <p>An error occured while processing your request. Please try again.</p>
    <p>Please contact your system administrator if you're experiencing problems.</p>
    <a class="btn small" href="index.php">&lsaquo; Back to Dashboard</a>
</section>
<footer>Copyright &copy; MasterPMIS <?php echo date('Y'); ?></footer>

</body>
</html>