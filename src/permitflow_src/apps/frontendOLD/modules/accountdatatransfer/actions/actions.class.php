<?php
class accountdatatransferActions extends sfActions
{
    
       /**
	 * Executes 'Index' action
	 *
	 * Displays list of all of the currently logged in client's applications
	 *
	 * @param sfRequest $request A request object
	 */
        public function executeIndex(sfWebRequest $request)
        {
			$this->page = $request->getParameter('page', 1);
			$this->setLayout("layoutdash");


			$data = $request->getParameter('code');
			$data = json_decode(base64_decode($data), true);

			$origin_user_id = $data['origin_user_id'];

			$q = Doctrine_Query::create()
				->from('FormEntry a')
				->where('a.user_id = ?', $origin_user_id)
				->andWhere('a.deleted_status = 0')
				->andWhere('a.parent_submission = 0');
			$this->application = $q->fetchOne();

            $this->pager = new sfDoctrinePager('FormEntry', 10);
            $this->pager->setQuery($q);
            $this->pager->setPage($request->getParameter('page', 1));
            $this->pager->init();
        }

    /**
     * Executes 'Canceltransfer' action
     *
     * Cancel Change of Ownership of an Application
     *
     * @param sfRequest $request A request object
     */
    public function executeCanceltransfer(sfWebRequest $request)
    {
        $data = $request->getParameter('code');
        $data = json_decode(base64_decode($data), true);

        $application_id = $data['id'];

        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.id = ?', $application_id);
        $this->application = $q->fetchOne();

        $this->application->setCirculationId("");
        $this->application->save();

        $this->getUser()->setFlash('notice', 'The request for transfer of ownership has been cancelled');
        return $this->redirect("/index.php/dashboard");
    }


    public function executeTransfer(sfWebRequest $request)
    {
		$this->setLayout("layoutdash");
        $data = $request->getParameter('code');
        $data = json_decode(base64_decode($data), true);
        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.id = ?', $data['application_id']);
        $this->application = $q->fetchOne();

		
		$transfer_request = new AccountDataTransfer();
		$transfer_request->setApplicationId($this->application->getId());
		$transfer_request->setTransferredToId($this->getUser()->getGuardUser()->getId());
		$transfer_request->setTransferredFromId($this->application->getUserId());
		$transfer_request->setTransferComplete(false);

		$guid = "t" . self::createGuid();
		$transfer_request->setValidate($guid);
		$transfer_request->save();
		


        //Send email to new owner to alert them
        //Send account recovery email
		$encryptdata = base64_encode(json_encode(array('application_id' => $transfer_request->getApplication()->getId(), 'validate' =>$transfer_request->getValidate())));
        $body = "
                Hi {$transfer_request->getUserTransferredFrom()->getProfile()->getFullname()}, <br>
                <br>
                Application '".$this->application->getApplicationId()."' has been requested to be transferred from your 
				account to the account of '".$transfer_request->getUserTransferredTo()->getProfile()->getFullname()."' because these two share the same email address following a migration of Kigali CPMIS
				data to BPMIS. <br>
                <br>
                Click here to validate this request:<br>
                <a href='https://".$_SERVER['HTTP_HOST']."/index.php/accountdatatransfer/accepttransfer/code/".$encryptdata."'>".$this->application->getApplicationId()."</a>
                <br/><br/>
				If you cannot click the above link, copy the URL below to your web browser:<br/><br/>
                https://".$_SERVER['HTTP_HOST']."/index.php/accountdatatransfer/accepttransfer/code/".$encryptdata."				
                <br/>
                Thanks,<br/>
                ".sfConfig::get('app_organisation_name').".<br/>
            ";

        $mailnotifications = new mailnotifications();
        $mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $transfer_request->getUserTransferredFrom()->getProfile()->getEmail(),"Application Transfer: ".$transfer_request->getApplication()->getApplicationId(),$body);

		$transfer_request->setEmailRequestSent($body);
		$transfer_request->save();


        $this->getUser()->setFlash('notice', 'A request for transfer of ownership has been sent to the email , '.$transfer_request->getUserTransferredFrom()->getProfile()->getEmail().'. Kindly access the email to complete this tranfser');
    }

    /**
     * Executes 'Accepttransfer' action
     *
     * Cancel Change of Ownership of an Application
     *
     * @param sfRequest $request A request object
     */
    public function executeAccepttransfer(sfWebRequest $request)
    {
		$this->setLayout("layoutdash");
        $data = $request->getParameter('code');
        $data = json_decode(base64_decode($data), true);

        $application_id = $data['application_id'];
        $validate = $data['validate'];

        $q = Doctrine_Query::create()
            ->from('AccountDataTransfer a')
            ->where('a.application_id = ?', $application_id)
            ->andWhere('a.transfer_complete = ?', false)
            ->andWhere('a.validate = ?', $validate);
        $this->transfer_request = $q->fetchOne();

        $this->application = $this->transfer_request->getApplication();

        $this->application->setUserId($this->transfer_request->getUserTransferredTo()->getId());
        $this->application->save();

        $this->transfer_request->setTransferComplete(true);
        $this->transfer_request->save();

        //Send email to new owner to alert them
        //Send account recovery email
        $body = "
                Hi {$this->transfer_request->getUserTransferredTo()->getProfile()->getFullname()}, <br>
                <br>
                Application '".$this->application->getApplicationId()."' has been transferred to your account from '".$this->transfer_request->getUserTransferredFrom()->getProfile()->getFullname()."'. <br>
                <br>
                Click here to view the application details:<br>
                <a href='https://".$_SERVER['HTTP_HOST']."/index.php/application/view/id/".$this->application->getId()."'>".$this->application->getApplicationId()."</a>
                <br>
                <br>
                Thanks,<br>
                ".sfConfig::get('app_organisation_name').".<br>
            ";

        $mailnotifications = new mailnotifications();
        $mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $this->transfer_request->getUserTransferredTo()->getProfile()->getEmail(),"Application Transfer: ".$this->transfer_request->getApplication()->getApplicationId(),$body);


        $this->getUser()->setFlash('notice', 'The request for transfer of ownership has been accepted');
    }


    public function executeResendemail(sfWebRequest $request)
    {
		$this->setLayout("layoutdash");
        $transfer_id = $request->getParameter('transfer_id');
		
        $q = Doctrine_Query::create()
            ->from('AccountDataTransfer a')
            ->where('a.id = ?', $transfer_id)
            ->andWhere('a.transfer_complete = ?', false);
        $transfer_request = $q->fetchOne();

        $mailnotifications = new mailnotifications();
        $mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $transfer_request->getUserTransferredFrom()->getProfile()->getEmail(),"Application Transfer: ".$transfer_request->getApplication()->getApplicationId(),$transfer_request->getEmailRequestSent());

        $this->getUser()->setFlash('notice', 'A request for transfer of ownership has been sent to the email , '.$transfer_request->getUserTransferredFrom()->getProfile()->getEmail().'. Kindly access the email to complete this tranfser');
		return $this->redirect("/index.php/dashboard");
    }
	
	static private function createGuid(){
		$guid = "";
		// This was 16 before, which produced a string twice as
		// long as desired. I could change the schema instead
		// to accommodate a validation code twice as big, but
		// that is completely unnecessary and would break
		// the code of anyone upgrading from the 1.0 version.
		// Ridiculously unpasteable validation URLs are a
		// pet peeve of mine anyway.
		for ($i = 0; ($i < 8); $i++) {
		  $guid .= sprintf("%02x", mt_rand(0, 255));
		}
		return $guid;
	}

}
