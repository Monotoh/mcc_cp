<?php
use_helper("I18N");
?>
<tr <?php if($permit): ?><?php else: ?>class="unread"<?php endif; ?>>
  <?php
  $days =  GetDaysSince($application->getDateOfSubmission(), date("Y-m-d H:i:s"));
  ?>
      <td>
          <?php  echo html_entity_decode($application->getForm()->getFormName()); ?>
      </td>
      <td>
          <b><?php echo $application->getApplicationId(); ?></b>
      </td>
      <td>
          <span class="label label-primary"><?php
          echo $application->getStatusName();
          ?></span>
      </td>
      <td>
          <?php echo date('d F Y', strtotime($application->getDateOfSubmission())); ?>
      </td>
      <td>
		<?php
			$q = Doctrine_Query::create()
				->from('AccountDataTransfer a')
				->where('a.application_id = ?', $application->getId())
				->andWhere('a.transfer_complete = ?', false);
			if(!$q->count()){
				$encryptdata = base64_encode(json_encode(array('application_id' => $application->getId())));
		?>
			<a href="/index.php/accountdatatransfer/transfer/code/<?php echo $encryptdata; ?>" class="btn btn-success"><?php echo __('Transfer') ?></a>
		<?php
			}else{
				$transfer_request = $q->fetchOne();
		?>
			<a href="/index.php/accountdatatransfer/resendemail/transfer_id/<?php echo $transfer_request->getId(); ?>" class="btn btn-warning"><?php echo __('Resend Email Confirmation of Transfer') ?></a>		
		<?php
			}		
		?>
      </td>
      <!--<td>
<a href="/index.php/accountdatatransfer/accepttransfer/code/<?php echo $encryptdata; ?>" class="btn btn-success"><?php echo __('Confirm Transfer') ?></a>
      </td>-->
</tr>
