<?php
/**
 * indexSuccess.php template.
 *
 * Displays list of all of the currently logged in client's applications
 *
 * @package    frontend
 * @subpackage application
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

/**
 * Executes 'GetDays' function
 *
 * Gets the all the dates between two periods of time
 *
 * @param sfRequest $sEndDate A request object
 * @param sfRequest $sStartDate A request object
 * @return string
 */

use_helper("I18N");

function GetDaysSince($sStartDate, $sEndDate){
    $start_ts = strtotime($sStartDate);
    $end_ts = strtotime($sEndDate);
    $diff = $end_ts - $start_ts;
    return round($diff / 86400);
}
?>
<!-- Page-Title -->
<div class="row">
    <div class="col-xs-6 col-sm-6">
        <h4 class="page-title"><?php echo __('Transfer Data') ?></h4>
    </div>
    <div class="col-xs-6 col-sm-6 right">
      <a href="/index.php/application/groups" class="pull-right btn btn-success waves-effect w-md waves-light"><?php echo __('Make Applicaiton') ?></a>
    </div>
</div>
<!-- Page-Title -->

<div class="row">
    <div class="col-lg-12">
        <div class="card-box">
           <?php if ($pager->getResults()): ?>
               <div class="table-responsive">
               <table class="table table-striped" id="datatable-responsive">
                   <thead>
                     <th><?php echo __("Form"); ?></th>
                     <th><?php echo __("Ref No"); ?></th>
                     <th><?php echo __("Approval"); ?></th>
                     <th><?php echo __("Submitted On"); ?></th>
                     <th><?php echo __("Actions"); ?></th>
                   </thead>
                   <tbody>
                   <?php foreach ($pager->getResults() as $application): ?>
                       <?php
                       $days =  GetDaysSince($application->getDateOfSubmission(), date("Y-m-d H:i:s"));
                       ?>
                       <?php include_partial('list', array('application' => $application, 'days' => $days)) ?>
                   <?php endforeach; ?>
                   </tbody>
                   <tfoot>
                   <tr>
                       <th colspan="12">
                           <p class="table-showing pull-left"><strong><?php echo $pager->getNbResults(); ?></strong> <?php echo __('applications in this stage') ?>

                               <?php if ($pager->haveToPaginate()): ?>
                                   - page <strong><?php echo $pager->getPage() ?>/<?php echo $pager->getLastPage() ?></strong>
                               <?php endif; ?></p>

                           <?php if ($pager->haveToPaginate()): ?>
                               <ul class="pagination pagination-sm mb0 mt0 pull-right">
                                   <li><a href="/index.php/application/index<?php if($stage): ?>/subgroup/<?php echo $stage; ?><?php endif; ?>/page/1">
                                           <i class="fa fa-angle-left"></i>
                                       </a></li>

                                   <li> <a href="/index.php/application/index<?php if($stage): ?>/subgroup/<?php echo $stage; ?><?php endif; ?>/page/<?php echo $pager->getPreviousPage() ?>">
                                           <i class="fa fa-angle-left"></i>
                                       </a></li>

                                   <?php foreach ($pager->getLinks() as $page): ?>
                                       <?php if ($page == $pager->getPage()): ?>
                                           <li class="active"><a href=""><?php echo $page ?></a>
                                       <?php else: ?>
                                           <li><a href="/index.php/application/index<?php if($stage): ?>/subgroup/<?php echo $stage; ?><?php endif; ?>/page/<?php echo $page ?>"><?php echo $page ?></a></li>
                                       <?php endif; ?>
                                   <?php endforeach; ?>

                                   <li> <a href="/index.php/application/index<?php if($stage): ?>/subgroup/<?php echo $stage; ?><?php endif; ?>/page/<?php echo $pager->getNextPage() ?>">
                                           <i class="fa fa-angle-right"></i>
                                       </a></li>

                                   <li> <a href="/index.php/application/index<?php if($stage): ?>/subgroup/<?php echo $stage; ?><?php endif; ?>/page/<?php echo $pager->getLastPage() ?>">
                                           <i class="fa fa-angle-right"></i>
                                       </a></li>
                               </ul>
                           <?php endif; ?>

                       </th>
                   </tr>
                   </tfoot>
               </table>
               </div>
               <?php else: ?>
                   <div class="table-responsive">
                       <table class="table dt-on-steroids mb0">
                           <tbody>
                           <tr><td>
                                   <?php echo __('No Records Found') ?>
                               </td></tr>
                           </tbody>
                       </table>
                   </div>
               <?php endif; ?>

        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable( { keys: true } );
        $('#datatable-responsive').DataTable();
        $('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
        var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } );
    } );
    TableManageButtons.init();

</script>
