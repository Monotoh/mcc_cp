<?php
use_helper("I18N");
?>

<div class="content">

<ul class="breadcrumb">
        <li><a href="#"><?php echo __('Data Transfer') ?></a> <span class="divider">/</span></li>
        <li class="active"><?php echo __('Transfer Request Successful') ?></li>
        <li></li>
</ul>    <!-- Docs nav-->


<div class="row">

<div class="span9 padded-20">


<div class="alert alert-success" style="text-align:center;">
<?php echo $sf_user->getFlash('notice'); ?>.
</div>


</div><!-- /.span9 -->
						
</div><!-- /.row -->
			



