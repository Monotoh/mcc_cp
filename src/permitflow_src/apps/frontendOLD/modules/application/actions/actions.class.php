<?php
/**
 * Application actions.
 *
 * Displays applications submitted by the client
 *
 * @package    frontend
 * @subpackage application
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

class applicationActions extends sfActions
{
     /**
     * 
     * @param sfWebRequest $request
     * OTB patch - download attached file in client messages
     */
      public function executeDownloadFile(sfWebRequest $request){
    
    
    $target_file = sfConfig::get("sf_data_dir")."/client_messages/".$request->getParameter('filename') ;
    $filename_only = $request->getParameter('filename') ;
   /* error_log("Target File is ".$target_file);
    $filename_only = $request->getParameter('filename') ;
    //
    $header_file = (strstr($_SERVER['HTTP_USER_AGENT'], 'MSIE')) ? preg_replace('/\./', '%2e', $filename_only, substr_count($filename_only, '.') - 1) : $filename_only;
      //Prepare headers
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: public", false);
    header("Content-Description: File Transfer");
    header("Content-Type: " . $type);
    header("Accept-Ranges: bytes");
    header("Content-Disposition: attachment; filename=\"" . addslashes($header_file) . "\"");
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($target_file));
    exit();*/
//error_log("Target File >>>" .$target_file) ;
    if(file_exists($target_file)){
    //prompt user to download the file
    

    // Get extension of requested file
    $extension = strtolower(substr(strrchr($filename_only, "."), 1));

    // Determine correct MIME type
    switch($extension){
        case "asf":     $type = "video/x-ms-asf";                break;
        case "avi":     $type = "video/x-msvideo";               break;
        case "bin":     $type = "application/octet-stream";      break;
        case "bmp":     $type = "image/bmp";                     break;
        case "cgi":     $type = "magnus-internal/cgi";           break;
        case "css":     $type = "text/css";                      break;
        case "dcr":     $type = "application/x-director";        break;
        case "dxr":     $type = "application/x-director";        break;
        case "dll":     $type = "application/octet-stream";      break;
        case "doc":     $type = "application/msword";            break;
        case "exe":     $type = "application/octet-stream";      break;
        case "gif":     $type = "image/gif";                     break;
        case "gtar":    $type = "application/x-gtar";            break;
        case "gz":      $type = "application/gzip";              break;
        case "htm":     $type = "text/html";                     break;
        case "html":    $type = "text/html";                     break;
        case "iso":     $type = "application/octet-stream";      break;
        case "jar":     $type = "application/java-archive";      break;
        case "java":    $type = "text/x-java-source";            break;
        case "jnlp":    $type = "application/x-java-jnlp-file";  break;
        case "js":      $type = "application/x-javascript";      break;
        case "jpg":     $type = "image/jpeg";                    break;
        case "jpe":     $type = "image/jpeg";                    break;
        case "jpeg":    $type = "image/jpeg";                    break;
        case "lzh":     $type = "application/octet-stream";      break;
        case "mdb":     $type = "application/mdb";               break;
        case "mid":     $type = "audio/x-midi";                  break;
        case "midi":    $type = "audio/x-midi";                  break;
        case "mov":     $type = "video/quicktime";               break;
        case "mp2":     $type = "audio/x-mpeg";                  break;
        case "mp3":     $type = "audio/mpeg";                    break;
        case "mpg":     $type = "video/mpeg";                    break;
        case "mpe":     $type = "video/mpeg";                    break;
        case "mpeg":    $type = "video/mpeg";                    break;
        case "pdf":     $type = "application/pdf";               break;
        case "php":     $type = "application/x-httpd-php";       break;
        case "php3":    $type = "application/x-httpd-php3";      break;
        case "php4":    $type = "application/x-httpd-php";       break;
        case "png":     $type = "image/png";                     break;
        case "ppt":     $type = "application/mspowerpoint";      break;
        case "qt":      $type = "video/quicktime";               break;
        case "qti":     $type = "image/x-quicktime";             break;
        case "rar":     $type = "encoding/x-compress";           break;
        case "ra":      $type = "audio/x-pn-realaudio";          break;
        case "rm":      $type = "audio/x-pn-realaudio";          break;
        case "ram":     $type = "audio/x-pn-realaudio";          break;
        case "rtf":     $type = "application/rtf";               break;
        case "swa":     $type = "application/x-director";        break;
        case "swf":     $type = "application/x-shockwave-flash"; break;
        case "tar":     $type = "application/x-tar";             break;
        case "tgz":     $type = "application/gzip";              break;
        case "tif":     $type = "image/tiff";                    break;
        case "tiff":    $type = "image/tiff";                    break;
        case "torrent": $type = "application/x-bittorrent";      break;
        case "txt":     $type = "text/plain";                    break;
        case "wav":     $type = "audio/wav";                     break;
        case "wma":     $type = "audio/x-ms-wma";                break;
        case "wmv":     $type = "video/x-ms-wmv";                break;
        case "xls":     $type = "application/vnd.ms-excel";      break;
        case "xml":     $type = "application/xml";               break;
        case "7z":      $type = "application/x-compress";        break;
        case "zip":     $type = "application/x-zip-compressed";  break;
        default:        $type = "application/force-download";    break;
    }

    // Fix IE bug [0]
    $header_file = (strstr($_SERVER['HTTP_USER_AGENT'], 'MSIE')) ? preg_replace('/\./', '%2e', $filename_only, substr_count($filename_only, '.') - 1) : $filename_only;

    //Prepare headers
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: public", false);
    header("Content-Description: File Transfer");
    header("Content-Type: " . $type);
    header("Accept-Ranges: bytes");
    header("Content-Disposition: attachment; filename=\"" . addslashes($header_file) . "\"");
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($target_file));

    // Send file for download
    if ($stream = fopen($target_file, 'rb')){
        while(!feof($stream) && connection_status() == 0){
            //reset time limit for big files
            @set_time_limit(0);
            print(fread($stream,1024*8));
            flush();
        }
        fclose($stream);
    }
    }else{
        error_log('File not found!');
    }
    exit();
  }
    
       /***
        * Search for application 
        * OTB patch
        */
        public function executeSearch(sfWebRequest $request)
        {
          $user = $this->getUser();  
          //check if a user is logged in else redirect to login
          if($user->isAuthenticated()) {
          
                    $this->forwardUnless($query = $request->getParameter('query'), 'application', 'index');
                    //lets override this query and pass and id to search for
                    $query_get_entry_id = Doctrine_Query::create()
                            ->from('FormEntry f')
                            ->where('application_id LIKE ? ',"%".$request->getParameter('query') . "%") 
                            ->andWhere("f.parent_submission = ?", 0)
                            ->andWhere("f.deleted_status = ?", 0)
                            ->andWhere('user_id = ? ', $this->getUser()->getGuardUser()->getId()) ;
                            
                    $results = $query_get_entry_id->execute(); 
                    $id_to_search = null; 
                    $new_query = null ;
                    foreach($results as $res)
                   {
                         $id_to_search = $res->getId() ;
                    }
                    //search in our index
                    if($id_to_search){

                         $new_query = preg_replace('/\s+/', '', $id_to_search) ;
                    }
                    else {
                        $new_query = $request->getParameter('query') ;
                    }
                    $response = Doctrine_Core::getTable('FormEntry')->getForLuceneQuery($new_query);
                    //successful search
                    if($response)
                        {
                           $this->application = $response ;
                        }
                    else {
                         //Note: We cannot use symfony built in data load due to changes made by initial developer
                          //This therefore means we cannot search records previously created so we do dirty trick and execute a direct query
                         $like_val = preg_replace('/\s+/', '', $request->getParameter('query')) ; 
                        $query_get_entry_id = Doctrine_Query::create()
                            ->from('FormEntry f')
                            ->where('application_id LIKE ? ',"%".$like_val."%") 
                            ->andWhere("f.parent_submission = ?", 0)
                            ->andWhere("f.deleted_status = ?", 0)
                            ->andWhere('user_id = ? ', $this->getUser()->getGuardUser()->getId());
                          $dirty_query_res = $query_get_entry_id->execute(); 
                          //
                           $this->application = $dirty_query_res ; 

                     }
                       //if any of the above fails, then the item does not exist in our formentry table

                      $this->setLayout("layoutdash");
          }
          else {
                        //redirect to login.
                         $this->redirect('@sf_guard_signin');
          }
        }
    
       /**
	 * Executes 'Index' action
	 *
	 * Displays list of all of the currently logged in client's applications
	 *
	 * @param sfRequest $request A request object
	 */
        public function executeIndex(sfWebRequest $request)
        {
                $this->page = $request->getParameter('page', 1);
				$this->setLayout("layoutdash");

				if($request->getParameter("subgroup"))
				{
					$q = Doctrine_Query::create()
						->from('SubMenus a')
						->where("a.id = ?", $request->getParameter("subgroup"));
					$submenu = $q->fetchOne();
					$_SESSION['group'] = $submenu->getMenuId();
					$_SESSION['subgroup'] = $request->getParameter("subgroup");

                    $this->stage = $submenu->getId();

					if($request->getParameter("form"))
					{
						$q = Doctrine_Query::create()
						   ->from("FormEntry a")
						   ->where("a.user_id = ?", $this->getUser()->getGuardUser()->getId())
						   ->andWhere("a.form_id = ?", $request->getParameter("form"))
						   ->andWhere("a.approved = ?", $request->getParameter("subgroup"))
						   ->andWhere('a.parent_submission = 0')
						   ->orderBy("a.application_id DESC");
					}
					else
					{
						$q = Doctrine_Query::create()
						   ->from("FormEntry a")
						   ->where("a.user_id = ?", $this->getUser()->getGuardUser()->getId())
						   ->andWhere("a.approved = ?", $request->getParameter("subgroup"))
						   ->andWhere('a.parent_submission = 0')
						   ->orderBy("a.application_id DESC");
					}
				}
				else
				{
					if($request->getParameter("form"))
					{
						$q = Doctrine_Query::create()
						   ->from("FormEntry a")
						   ->where("a.user_id = ?", $this->getUser()->getGuardUser()->getId())
						   ->andWhere("a.form_id = ?", $request->getParameter("form"))
						   ->andWhere('a.parent_submission = 0')
						   ->orderBy("a.application_id DESC");

					}
					else
					{
						if($request->getParameter("drafts"))
						{
							$q = Doctrine_Query::create()
							   ->from("FormEntry a")
							   ->where("a.user_id = ?", $this->getUser()->getGuardUser()->getId())
							   ->andWhere('a.approved = 0')
							   ->andWhere('a.parent_submission = 0')
							   ->orderBy("a.application_id DESC");

							$_SESSION['group'] = 0;
							$_SESSION['subgroup'] =0;
						}
						else
						{
							$q = Doctrine_Query::create()
							   ->from("FormEntry a")
							   ->where("a.user_id = ?", $this->getUser()->getGuardUser()->getId())
							   ->andWhere('a.parent_submission = 0')
							   ->orderBy("a.application_id DESC");

							$_SESSION['group'] = 0;
							$_SESSION['subgroup'] =0;
						}
					}
				}

            $this->pager = new sfDoctrinePager('FormEntry', 10);
            $this->pager->setQuery($q);
            $this->pager->setPage($request->getParameter('page', 1));
            $this->pager->init();
        }

    /**
     * Executes 'Canceltransfer' action
     *
     * Cancel Change of Ownership of an Application
     *
     * @param sfRequest $request A request object
     */
    public function executeCanceltransfer(sfWebRequest $request)
    {
        $data = $request->getParameter('code');
        $data = json_decode(base64_decode($data), true);

        $application_id = $data['id'];

        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.id = ?', $application_id);
        $this->application = $q->fetchOne();

        $this->application->setCirculationId("");
        $this->application->save();

        $this->getUser()->setFlash('notice', 'The request for transfer of ownership has been cancelled');
        return $this->redirect("/index.php/dashboard");
    }


    /**
     * Executes 'Accepttransfer' action
     *
     * Cancel Change of Ownership of an Application
     *
     * @param sfRequest $request A request object
     */
    public function executeAccepttransfer(sfWebRequest $request)
    {
        $data = $request->getParameter('code');
        $data = json_decode(base64_decode($data), true);

        $application_id = $data['id'];

        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.id = ?', $application_id);
        $this->application = $q->fetchOne();

        $previous_owner = $this->application->getCirculationId();

        $this->application->setUserId($this->application->getCirculationId());
        $this->application->setCirculationId("");
        $this->application->save();

        $q = Doctrine_Query::create()
            ->from("SfGuardUserProfile a")
            ->where("a.user_id = ?",$this->application->getUserId());

        $new_user = $q->fetchOne();

        $q = Doctrine_Query::create()
            ->from("SfGuardUserProfile a")
            ->where("a.user_id = ?", $previous_owner);

        $previous_user = $q->fetchOne();

        //Send email to new owner to alert them
        //Send account recovery email
        $body = "
                Hi {$new_user->getFullname()}, <br>
                <br>
                Application '".$this->application->getApplicationId()."' has been transferred to your account from '".$previous_user->getFullname()."'. <br>
                <br>
                Click here to view the application details:<br>
                <a href='http://".$_SERVER['HTTP_HOST']."/index.php/application/view/id/".$this->application->getId()."'>".$this->application->getApplicationId()."</a>
                <br>
                <br>
                Thanks,<br>
                ".sfConfig::get('app_organisation_name').".<br>
            ";

        $mailnotifications = new mailnotifications();
        $mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $new_user->getEmail(),"Application Transfer",$body);


        $this->getUser()->setFlash('notice', 'The request for transfer of ownership has been accepted');
        return $this->redirect("/index.php/dashboard");
    }

    /**
	 * Executes 'Delete' action
	 *
	 * Delete an application
	 *
	 * @param sfRequest $request A request object
	 */
        public function executeDelete(sfWebRequest $request)
        {
                $q = Doctrine_Query::create()
                   ->from('FormEntry a')
                   ->where('a.id = ?', $request->getParameter("entry"))
                   ->andWhere('a.user_id = ?', $this->getUser()->getGuardUser()->getId());
                $application = $q->fetchOne();

                if($application && $application->getApproved() == "0")
                {
                	$application->delete();
                }
                $this->redirect("/index.php/application/index/drafts/1");
        }

    /**
     * Executes 'Delete' action
     *
     * Delete a draft application from the dashboard
     *
     * @param sfRequest $request A request object
     */
    public function executeDeletedraft(sfWebRequest $request)
    {
        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.id = ?', $request->getParameter("id"))
            ->andWhere('a.user_id = ?', $this->getUser()->getGuardUser()->getId());
        $application = $q->fetchOne();

        if($application && $application->getApproved() == "0")
        {
            $application->delete();
        }

        if($request->getParameter("redirect"))
        {
            $this->redirect("/index.php/forms/view?id=".$request->getParameter("redirect"));
        }
        else {
            $this->redirect("/index.php/dashboard");
        }
    }

        /**
	 * Executes 'View' action
	 *
	 * Displays full application details
	 *
	 * @param sfRequest $request A request object
	 */
        public function executeView(sfWebRequest $request)
        {
            $q = Doctrine_Query::create()
                ->from('FormEntry a')
                ->where('a.id = ?', $request->getParameter("id"))
                ->andWhere('a.user_id = ?', $this->getUser()->getGuardUser()->getId());
            $this->application = $q->fetchOne();
           // error_log("Test application id".$this->application->getId()) ;
           // error_log(print_r($this->application,True)) ;
            //we do not allow user view details on application that were submitted by other users
           
            if(count($this->application) > 1) {
            // error_log("Test applications ".$test) ;
            //Trigger any automated triggers
            $q = Doctrine_Query::create()
                ->from("MfInvoice a")
                ->where("a.app_id = ?", $this->application->getId())
                ->andWhere("a.paid = 2")
                ->limit(1);
            $paid_invoices = $q->execute();
            foreach($paid_invoices as $paid_invoice)
            {
                $paid_invoice->save();
            }

            if($request->getParameter("formid"))
        		{
        			$this->choosen_form = $request->getParameter("formid");
        		}

                if(empty($this->application))
                {
                    echo "Permission Denied.";
                    exit;
                }

                if($request->getParameter("messages") == "read")
                {
                    $q = Doctrine_Query::create()
                   ->from("Communications a")
                   ->Where('a.messageread = ?', '0')
                   ->andWhere('a.application_id = ?', $this->application->getId());
                $messages = $q->execute();
                foreach($messages as $message)
                {
                    if($message->getArchitectId() == "")
                    {
                        $message->setMessageread("1");
                        $message->save();
                    }
                }
                }

                if($request->getPostParameter("txtmessage"))
                {
                        $message = new Communications();
                        $message->setArchitectId($this->getUser()->getGuardUser()->getId());
                        $message->setMessageread("0");
                        $message->setContent($request->getPostParameter("txtmessage"));
                        $message->setApplicationId($this->application->getId());
                        $message->setActionTimestamp(date('Y-m-d'));
                        $message->save();

						$q = Doctrine_Query::create()
						   ->from("SfGuardUserProfile a")
						   ->where("a.user_id = ?", $this->application->getUserId());
						$user_profile = $q->fetchOne();

						/**$q = Doctrine_Query::create()
						   ->from("Communications a")
						   ->where("a.reviewer_id <> ? OR a.reviewer_id <> ?", array(NULL, ""))
						   ->groupBy("a.reviewer_id");
						$existing_participants = $q->execute();

						if(sizeof($existing_participants) > 0)
						{
							foreach($existing_participants as $existing_participant)
							{
								$q = Doctrine_Query::create()
								   ->from("CfUser a")
								   ->where("a.nid = ?", $existing_participant->getReviewerId());
								$reviewer = $q->fetchOne();

								if($reviewer)
								{
									$body = "
							        Hi ".$reviewer->getStrfirstname()." ".$reviewer->getStrlastname().",<br>
							        <br>
							        You have received a new message on ".$this->application->getApplicationId()." from ".$user_profile->getFullname().":<br>
							        <br><br>
							        -------
							        <br>
							        ".$request->getPostParameter("txtmessage")."
							        <br>
							        <br>
							        Click here to view the application: <br>
							        ------- <br>
							        <a href='http://".$_SERVER['HTTP_HOST']."/backend.php/applications/view/id/".$this->application->getId()."'>Link to ".$this->application->getApplicationId()."</a><br>
							        ------- <br>

							        <br>
							        ";

									$mailnotifications = new mailnotifications();
							        $mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $reviewer->getStremail(),"New Message",$body);
								}
							}
						}
						else
						{
							//Get list of reviewers who can see this application
							$q = Doctrine_Query::create()
		                       ->from('CfUser a')
            				   ->where('a.bdeleted = 0');
		                    $reviewers = $q->execute();
		                    foreach($reviewers as $reviewer)
		                    {
		                    	$q = Doctrine_Query::create()
									->from('mfGuardUserGroup a')
									->leftJoin('a.Group b')
									->leftJoin('b.mfGuardGroupPermission c') //Left Join group permissions
									->leftJoin('c.Permission d') //Left Join permissions
									->where('a.user_id = ?', $reviewer->getNid())
									->andWhere('d.name = ?', "accesssubmenu".$this->application->getApproved());
								$usergroups = $q->execute();
								if(sizeof($usergroups) > 0)
								{
									$body = "
							        Hi ".$reviewer->getStrfirstname()." ".$reviewer->getStrlastname().",<br>
							        <br>
							        You have received a new message on ".$this->application->getApplicationId()." from ".$user_profile->getFullname().":<br>
							        <br><br>
							        -------
							        <br>
							        ".$request->getPostParameter("txtmessage")."
							        <br>
							        <br>
							        Click here to view the application: <br>
							        ------- <br>
							        <a href='http://".$_SERVER['HTTP_HOST']."/backend.php/applications/view/id/".$this->application->getId()."'>Link to ".$this->application->getApplicationId()."</a><br>
							        ------- <br>

							        <br>
							        ";

									$mailnotifications = new mailnotifications();
							        $mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $reviewer->getStremail(),"New Message",$body);
								}
		                    }
                        }**/

                        $activity = new Activity();
                        $activity->setUserId($this->getUser()->getGuardUser()->getId());
                        $activity->setFormEntryId($this->application->getId());
                        $activity->setAction("User sent a message");
                        $activity->setActionTimestamp(date('Y-m-d'));
                        $activity->save();
                }



			if($request->getParameter("open"))
			{
				$this->open = $request->getParameter("open");
			}

            if($request->getParameter("messages") == "read")
            {
                $this->open = "messages";
            }

            $this->done = $request->getParameter("done", 0);
           }
           //this user is trying to view details of an application submitted by another user
           else {
               $this->redirect('application/denial') ;
              
           }
	   $this->setLayout("layoutdash");
           
           
        }
    
        /**
         * Denial Success
         */
        public function executeDenial(sfWebRequest $request)
                {
            $this->setLayout("layoutdash");
        }
        /**
	 * Executes 'Draft' action
	 *
	 * Displays list of all of the currently logged in client's draft applications
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeDrafts(sfWebRequest $request)
	{
		$this->page = $request->getParameter('page', 1);
		$this->setLayout("layoutdash");
	}

        /**
	 * Executes 'Groups' action
	 *
	 * Displays list of all of the categories of applications
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeGroups(sfWebRequest $request)
	{

		if($request->getParameter("others"))
		{
			$this->display_others = true;
		}
		else
		{
			$this->display_others = false;
		}

		if($request->getParameter("id"))
		{
			$this->group_id = $request->getParameter("id");
		}
		else
		{
			$this->group_id = false;
		}

		$this->setLayout("layoutdash");
	}

        /**
	 * Executes 'Edit' action
	 *
	 * Allows client and resubmit an application
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeEdit(sfWebRequest $request)
	{
		$this->setLayout("layoutdash");
	}

        /**
	 * Executes 'Share' action
	 *
	 * Allows the client to share selected application with another client
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeShare(sfWebRequest $request)
	{

			$q = Doctrine_Query::create()
			->from('FormEntry a')
			   ->where('a.id = ?', $request->getParameter("id"));
			$this->application = $q->fetchOne();

			if($request->getParameter("filter"))
			{
				$this->filter = $request->getParameter("filter");
			}

			if($request->getPostParameter("filter"))
			{
				$this->filter = $request->getPostParameter("filter");
			}

			if($request->getParameter("page"))
			{
				$this->page = $request->getParameter("page");
			}

			if($request->getParameter("architect") && $request->getParameter("architect") != "")
			{
				$share = new FormEntryShares();
				$share->setSenderid($this->getUser()->getGuardUser()->getId());
				$share->setReceiverid($request->getParameter("architect"));
				$share->setFormentryid($request->getParameter("id"));
				$share->save();
					$this->redirect("/index.php/application/shared");
			}
		$this->setLayout("layoutdash");
	}

         /**
	 * Executes 'Shared' action
	 *
	 * Shows success message if application is shared successfully
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeShared(sfWebRequest $request)
	{
		$this->setLayout("layoutdash");
	}


    /**
    * Executes 'Viewpermit' action
    *
    * Displays the generated permit that is attached to an application
    *
    * @param sfRequest $request A request object
    */
    public function executeViewpermit(sfWebRequest $request)
    {
      $q = Doctrine_Query::create()
       ->from('SavedPermit a')
       ->where('a.id = ?', $request->getParameter('id'));
      $savedpermit = $q->fetchOne();

       if($savedpermit)
       {
         $application = $savedpermit->getApplication();

        require_once(dirname(__FILE__)."/../../../../../lib/vendor/dompdf/dompdf_config.inc.php");


        $html = "<html>
        <body>
        ";

        $templateparser = new TemplateParser();

        $html .= $templateparser->parsePermit($application->getId(), $application->getFormId(), $application->getEntryId(), $savedpermit->getId(), $savedpermit->getPermit());

        $html .= "
        </body>
        </html>";

        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->render();
        $dompdf->stream($application->getApplicationId().".pdf");
       }
       else
       {
         echo "Invalid Permit Link";
       }

       exit;

    }
	//OTB Patch start - Get old system permits
	public function executePermitoldformat(sfWebRequest $request)
	{
		$this->setLayout("layoutdash");

	}
	//OTB Patch end - Get old system permits
}
