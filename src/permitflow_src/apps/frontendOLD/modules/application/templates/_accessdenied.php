<div class="notfoundpanel">
 
  <div class="alert alert-danger"> 
    <button type="button" class="close" data-dismiss="alert"> </button>
    <h2><?php echo __('Application Access Denied!') ?></h2>
   <h4><?php echo __('Sorry, you are not allowed to view details of this application !')?></h4>
   <h4><?php echo __('If you are experiencing any trouble, please') ?>  <a target="_blank" class="btn btn-primary" href="<?php echo public_path(); ?>index.php/help/contact"><i class="md md-call"></i>
                <span> <?php echo __('Contact us'); ?></span></a></h4>
   
</div>
</div><!-- notfoundpanel -->
