<?php
use_helper("I18N");
?>
<tr <?php if($permit): ?><?php else: ?>class="unread"<?php endif; ?>>
  <?php
  $days =  GetDaysSince($application->getDateOfSubmission(), date("Y-m-d H:i:s"));
  ?>
      <td>
          <?php  echo html_entity_decode($application->getForm()->getFormName()); ?>
      </td>
      <td>
          <a href="<?php echo public_path(); ?>index.php/application/view/id/<?php echo $application->getId(); ?>"><?php echo $application->getApplicationId(); ?></a>
      </td>
      <td>
        <?php
          $q = Doctrine_Query::create()
             ->from("MfInvoice a")
             ->where("a.app_id = ?", $application->getId())
             ->andWhere("a.paid = 1 OR a.paid = 15");
          $unpaid_invoices = $q->count();

          if($unpaid_invoices > 0)
          {
            ?>
            <span class="label label-danger"><?php echo __('Not Paid') ?></span>
            <?php
          }
          else
          {
              $q = Doctrine_Query::create()
                  ->from("MfInvoice a")
                  ->where("a.app_id = ?", $application->getId());
              $invoices = $q->count();
              if($invoices == 0)
              {
                  ?>
                  <span class="label label-default"><?php echo __("No Bill"); ?></span>
                  <?php
              }
              else
              {
                  ?>
                  <span class="label label-success"><?php echo __("Paid"); ?></span>
                  <?php
              }
          }
          ?>
      </td>
      <td>
          <span class="label label-primary"><?php
          echo $application->getStatusName();
          ?></span>
      </td>
      <td>
          <?php echo date('d F Y', strtotime($application->getDateOfSubmission())); ?>
      </td>
      <td>

      <div class="btn-group">
          <button type="button" class="btn btn-xs btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false"><?php echo __('Action') ?> <span class="caret"></span></button>
          <ul class="dropdown-menu dropdown-menu-right" role="menu">

          <li><a  title='<?php echo __('View Application'); ?>' href='<?php echo public_path(); ?>index.php/application/view/id/<?php echo $application->getId(); ?>'><?php echo __('View') ?></a></li>
          <?php
          if($application->getDeclined() == 1 || $application->getApproved() == 0) {
              ?>
              <li><a title='<?php echo __('Edit Application'); ?>'
                     href='<?php echo public_path(); ?>index.php/application/edit?application_id=<?php echo $application->getId(); ?>'><?php echo __("Edit"); ?> </a>
              </li>
              <?php
          }
          ?>
          <?php
          $q = Doctrine_Query::create()
             ->from("MfInvoice a")
             ->where("a.app_id = ?", $application->getId())
             ->limit(5);
          $invoices = $q->execute();

          foreach($invoices as $invoice)
          {
            ?>
            <li><a title="View Invoices" href="/index.php/invoices/view/id/<?php echo $invoice->getId(); ?>"><?php echo __('Print Invoice') ?></a></li>
            <?php
          }

          $q = Doctrine_Query::create()
             ->from("SavedPermit a")
             ->where("a.application_id = ?", $application->getId())
             ->limit(5);
          $permits = $q->execute();

          foreach($permits as $permit)
          {
            ?>
            <li><a title="<?php echo __('View Permit') ?>" href="/index.php/permits/view/id/<?php echo $permit->getId(); ?>"><?php echo __('Print Permit') ?> <?php echo $permit->getTemplate()->getTitle(); ?></a></li>
            <?php
          }
          ?>
        </ul>
    </div>
      </td>
</tr>
