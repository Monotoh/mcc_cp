<?php
/**
 * editSuccess.php template.
 *
 * Allows client and resubmit an application
 *
 * @package    frontend
 * @subpackage application
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
 use_helper("I18N");
$prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";
require($prefix_folder.'includes/init.php');

require($prefix_folder.'config.php');
require($prefix_folder.'includes/db-core.php');
require($prefix_folder.'includes/helper-functions.php');
require($prefix_folder.'includes/check-session.php');

require($prefix_folder.'includes/language.php');
require($prefix_folder.'includes/common-validator.php');
require($prefix_folder.'includes/post-functions.php');
require($prefix_folder.'includes/filter-functions.php');
require($prefix_folder.'includes/entry-functions.php');
require($prefix_folder.'includes/view-functions.php');
require($prefix_folder.'includes/users-functions.php');

$invoice_manager = new InvoiceManager();

//Get site config properties
$q = Doctrine_Query::create()
    ->from("ApSettings a")
    ->where("a.id = 1")
    ->orderBy("a.id DESC");
$apsettings = $q->fetchOne();


$q = Doctrine_Query::create()
    ->from("FormEntry a")
    ->where("a.id =?", $_GET['application_id']);
$submission = $q->fetchOne();

if($submission->getDeclined() != "1" && $submission->getApproved() != "0")
{
    header("Location: ".public_path()."index.php/application/view/id/".$submission->getId());
    exit;
}

$form_id = null;
$entry_id = null;

if($_GET['link'])
{
    $q = Doctrine_Query::create()
        ->from("FormEntryLinks a")
        ->where("a.id = ?", $_GET['link']);
    $link = $q->fetchOne();

    $form_id = (int)trim($link->getFormId());
    $entry_id = (int)trim($link->getEntryId());
}
else {
    $form_id = (int)trim($submission->getFormId());
    $entry_id = (int)trim($submission->getEntryId());
}
$nav = trim($_GET['nav']);


$dbh = mf_connect_db();
$mf_settings = mf_get_settings($dbh);

//get form name
$query 	= "select
					 form_name
			     from
			     	 ".MF_TABLE_PREFIX."forms
			    where
			    	 form_id = ?";
$params = array($form_id);

$sth = mf_do_query($query,$params,$dbh);
$row = mf_do_fetch_result($sth);

if(!empty($row)){
    $form_name = htmlspecialchars($row['form_name']);
}

//if there is "nav" parameter, we need to determine the correct entry id and override the existing entry_id
if(!empty($nav)){
    $all_entry_id_array = mf_get_filtered_entries_ids($dbh,$form_id);
    $entry_key = array_keys($all_entry_id_array,$entry_id);
    $entry_key = $entry_key[0];

    if($nav == 'prev'){
        $entry_key--;
    }else{
        $entry_key++;
    }

    $entry_id = $all_entry_id_array[$entry_key];

    //if there is no entry_id, fetch the first/last member of the array
    if(empty($entry_id)){
        if($nav == 'prev'){
            $entry_id = array_pop($all_entry_id_array);
        }else{
            $entry_id = $all_entry_id_array[0];
        }
    }
}

if(mf_is_form_submitted()){ //if form submitted

    if($_POST['save_as_draft'] || $_POST['save_as_draft2'])
    {
        $_SESSION['save_as_draft'] = true;
    }
    else
    {
        $_SESSION['save_as_draft'] = false;
    }

    $input_array   = mf_sanitize($_POST);
    $submit_result = mf_process_form($dbh,$input_array);

    if($submit_result['status'] === true){

        $application_manager = new ApplicationManager();
        $invoice_manager = new InvoiceManager();
        $payments_manager = new PaymentsManager();

        $payments_manager->validate_all_invoices($submission->getId());

        //If this is a draft submission being submitted then attempt to publish it to the workflow
        if($submission->getApproved() == "0")
        {
            //Return application back to initial stage else if not final then just redirect back to view application
            if($_SESSION["save_as_draft"] != 1)
            {
                if($invoice_manager->has_unpaid_invoice($submission->getId()))
                {
                    $invoice = $invoice_manager->get_unpaid_invoice($submission->getId());

                    //If invoice is pending, confirm payment status if IPN exists
                    if($invoice->getPaid() == 15)
                    {
                        $invoice = $invoice_manager->update_payment_status($invoice->getId());
                    }
                }

                //If there are still unpaid invoices then redirect to payment API
                if($invoice_manager->get_unpaid_invoice($submission->getId()) || !$invoice_manager->has_invoice($submission->getId()))
                {
                    $query  = "select
                    payment_enable_merchant,
                    payment_enable_invoice
                    from
                       `".MF_TABLE_PREFIX."forms`
                   where
                      form_id=?";

                    $params = array($submission->getFormId());

                    $sth = mf_do_query($query,$params,$dbh);
                    $row = mf_do_fetch_result($sth);

                    $payment_merchant_enable      = $row['payment_enable_merchant'];

                    //If online payment is enabled then redirect to payment API and publish draft later
                    if($payment_merchant_enable == 1 && $row['payment_enable_invoice'] == 0)
                    {
                        if(!$invoice_manager->has_invoice($submission->getId()))
                        {
                            $invoice = $invoice_manager->create_invoice_from_submission($submission->getId());

                            $sf_user->setAttribute('form_id', $submission->getFormId());
                            $sf_user->setAttribute('entry_id', $submission->getEntryId());
                            $sf_user->setAttribute('invoice_id', $invoice->getId());

                            header("Location: ".public_path()."index.php/forms/payment");
                            exit;

                        }
                        else
                        {
                            $invoice = $invoice_manager->get_unpaid_invoice($submission->getId());

                            $sf_user->setAttribute('form_id', $submission->getFormId());
                            $sf_user->setAttribute('entry_id', $submission->getEntryId());
                            $sf_user->setAttribute('invoice_id', $invoice->getId());

                            header("Location: ".public_path()."index.php/forms/payment");
                            exit;
                        }
                    }
                    else
                    {
                        //If online payment is not enabled then publish the draft to a live workflow
                        $submission = $application_manager->publish_draft($submission->getId());

                        header("Location: ".public_path()."index.php/application/view/id/".$submission->getId());
                        exit;
                    }

                }
                else
                {
                    //If invoices are already paid then publish the draft to a live workflow
                    $submission = $application_manager->publish_draft($submission->getId());

                    header("Location: ".public_path()."index.php/application/view/id/".$submission->getId());
                    exit;
                }
            }
            else
            {
                //If save as draft/resume later has been clicked then don't publish the draft
                header("Location: ".public_path()."index.php/application/view/id/".$submission->getId());
                exit;
            }
        }
        else
        {
            //If the application was declined then attempt to make a resubmission
            $submission = $application_manager->resubmit_application($submission->getId());

            header("Location: ".public_path()."index.php/application/view/id/".$submission->getId());
            exit;
        }

    }else if($submit_result['status'] === false){ //there are errors, display the form again with the errors
        $old_values 	= $submit_result['old_values'];
        $custom_error 	= @$submit_result['custom_error'];
        $error_elements = $submit_result['error_elements'];

        $form_params = array();
        $form_params['populated_values'] = $old_values;
        $form_params['error_elements']   = $error_elements;
        $form_params['custom_error'] 	 = $custom_error;
        $form_params['edit_id']			 = $input_array['edit_id'];
        $form_params['integration_method'] = 'php';
        $form_params['is_application'] = true;
        $form_params['page_number'] = 0; //display all pages (if any) as a single page
         //OTB patch, pass locale value 
        $form_markup = mf_display_form($dbh,$input_array['form_id'],$form_params,$sf_user->getCulture());
    }

}else{ //otherwise, display the form with the values
    //set session value to override password protected form
    $_SESSION['user_authenticated'] = $form_id;

    //set session value to bypass unique checking
    $_SESSION['edit_entry']['form_id']  = $form_id;
    $_SESSION['edit_entry']['entry_id'] = $entry_id;

    $form_values = mf_get_entry_values($dbh,$form_id,$entry_id);

    $form_params = array();
    $form_params['populated_values'] = $form_values;
    $form_params['edit_id']			 = $entry_id;
    $form_params['integration_method'] = 'php';
    $form_params['is_application'] = true;
    $form_params['page_number'] = 0; //display all pages (if any) as a single page
    //OTB patch, pass locale value
    $form_markup = mf_display_form($dbh,$form_id,$form_params,$sf_user->getCulture());
}
?>
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title"><?php echo __('Make Application') ?></h4>
    </div>
</div>
<!-- Page-Title -->

<?php
if($_SESSION['draft_edit'])
{
    ?>
    <div class="alert alert-success">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo __('You had an incomplete entry that was saved as draft. Please edit and resubmit') ?>.
    </div>
<?php
}
?>

<div class="row">
    <div class="col-lg-8">
        <div class="card-box p-b-0">
            <?php

            if($_GET['bill_error'])
            {
                ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong><?php echo __('Error') ?></strong> <?php echo __('Could not find records. Please confirm application details') ?>.
                </div>
                <?php
            }

            header("Content-Type: text/html; charset=UTF-8");
            echo $form_markup;
            ?>
        </div>
    </div>

    <!--Display a sidebar with information from the site config-->
    <?php if($apsettings){ ?>
        <div class="col-lg-4">
             <div class="card-box widget-user">
           
            <!-- BEGIN TAB PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <span style="color:red;"> <b> <?php echo __('REASONS FOR DECLINE'); ?> </b> </span>
                </div>
                <div class="portlet-body">
                   <?php include_partial('comments_declines', array('application'=>$submission)); ?> 
                </div>
            </div>
            <div class="portlet light">
                <div class="portlet-title">
                          
                      <?php echo __('Previous Submitted Details for the fields you should change on this Form') ?>
                 
                </div>
                <div class="portlet-body">
                    <div class=" portlet-tabs">
                        <ul class="nav nav-tabs">
                            
                            <li class="active">
                                <a href="#portlet_tab1" data-toggle="tab">
                                    <?php echo __('Details') ?> </a>
                            </li>
                             
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="portlet_tab1">
                                <?php 
                                $app_id = $_GET['application_id'] ;
                                $otbhelper = new OTBHelper();
                                $page_number_clause = '';
                                //
                                $edit_elements = $otbhelper->getEditFields($app_id) ;
                                $entry_details_custom = mf_get_entry_details_otb($dbh, $form_id, $entry_id, $param, $sf_user->getCulture(),$edit_elements);
                                ?>
                                <table class="table table-bordered">
       
        <?php
            foreach ($entry_details_custom as $data) {
                ?>
                <?php
                if ($data['element_type'] == "section") {
                    ?>
                    <tr>
                        <label class="col-sm-12"><?php echo $data['label']; ?></label>
                    </tr>
                <?php
                } elseif ($data['element_type'] == "page_break") {

                } 
                elseif ($data['element_type'] == "plinth_area"){
                    ?>
                    <tr>
                        <td><strong><?php echo $data['label']; ?></strong></td>
                        <td><?php if ($data['value']) { 
                                 $myvals = explode(" ",$data['value']) ;
                                // echo $myvals[0];
                               // echo nl2br($data['value']); ?>
                            <table>
                                <thead>
                                    <tr style="background-color: #002a80;">
                                        <th style="color: white;"> <?php echo __('Existing') ?> </th> <th> | </th><th style="color: yellow;"> <?php echo __('New') ?> </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td> <?php echo $myvals[0]; ?> </td>
                                        <td> | </td>
                                        <td> <?php echo $myvals[1]; ?> </td>
                                    </tr>
                                </tbody>
                            </table>
                           <?php }  else {
                                echo "-";
                        } ?></td>
                    </tr>
                <?php
                }
                    elseif ($data['element_type'] == "total_plinth_area"){
                    ?>
                    <tr>
                        <td><strong><?php echo $data['label']; ?></strong></td>
                        <td><?php if ($data['value']) { 
                                 $myvals = explode(" ",$data['value']) ;
                                // echo $myvals[0];
                               // echo nl2br($data['value']); ?>
                            <table>
                                <thead>
                                    <tr style="background-color: #002a80;">
                                        <th style="color: white;"> <?php echo __('Total Existing') ?> </th> <th> | </th><th style="color: yellow;"> <?php echo __('Total New') ?> </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td> <?php echo $myvals[0]; ?> </td>
                                        <td> | </td>
                                        <td> <?php echo $myvals[1]; ?> </td>
                                    </tr>
                                </tbody>
                            </table>
                           <?php }  else {
                                echo "-";
                        } ?></td>
                    </tr>
                <?php
                }
                else {
                    ?>
                    <tr>
                        <td><strong><?php echo $data['label']; ?></strong></td>
                        <td><?php if ($data['value']) {
                                echo nl2br($data['value']);
                            } else {
                                echo "-";
                        } ?></td>
                    </tr>
                <?php
                }
            }
        
        ?>
        </tbody>
    </table>
                                <?php 
                                  // count the number of resubmissions
                                /*  $q = Doctrine_Query::create()
                                          ->from('EntryDecline d')
                                          ->where('d.entry_id = ? ',$_GET['application_id'])
                                          ->andWhere('d.resolved = ? ', 1); 
                                  //
                                  $results_q = $q->execute();
                                  // get allowed resubmissions
                                  $q2 = Doctrine_Query::create()
                                          ->from('ApSettings a') ;
                                  //
                                  $results_q2 = $q2->execute();
                                  $alowed_resubmission = 0 ;
                                  foreach($results_q2 as $v){
                                      $alowed_resubmission = $v['allowed_max_resubmission'];
                                  } */
                                ?>
                               
                                <!--<div class="alert alert-info">
                                    <?php //echo __('Number of resubmission') ?> (<?php //echo count($results_q) ?>) <br/>
                                </div>
                                
                                <div class="alert alert-danger">
                                    <?php //echo __('Please Note you only have') ?> (<?php //echo $alowed_resubmission - count($results_q) ?>) <?php //echo __('remaining resubmissions.') ?> <?php //echo __('If you use all your remaining resubmissions system will block this application!!') ?>
                                </div> -->
                                
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>
            <!-- END TAB PORTLET-->

        </div>

            <?php //echo html_entity_decode($apsettings->getOrganisationSidebar()); ?>
        </div>
    <?php } ?>
</div>
 <!-- OTB patch - Check if current logged user is registered as property owner and if so hide other categories -->
        <?php
        $otbhelper = new OTBHelper() ;
        $user_id = $sf_user->getGuardUser()->getProfile()->getUserId() ;
        if($otbhelper->checkUserRegisteredAsPropertyOwner($user_id)) {
            //this script code only executes if the user is registered as property owner - else not parsed to the browser
        ?>
        <script> 
           //check for option names
           //  $('.a_buildingcategory').prop('disabled', true); Abit static change me in future >>>
           $(".a_buildingcategory > option").each(function() {
              if(this.text === 'Category 1.C' || this.text === 'Category 2' || this.text === 'Category 3' || this.text === 'Category 4'){
                   
                 //  $(this).remove(); //Diabled this becoz of previous registered users who submitted for the wrong category
                 
                   console.log('Removed >>> '+this.text + ' ' + this.value);
              }
              else {
                  //do nothing
              }
             }); 
        </script>
        <?php } ?>
<div id="t_hide_div">
        <script> 
            //OTB patch
              $('#a_builtup_error').hide();
              $('#a_no_of_floors_error').hide();
             var building_category = null;
             //parameters to check
             var no_of_floors = 0 ;
             var built_up_area_size = 0 ;
            //Get the value of Building category and store in browser local storage.
             //get the value of retrieved building category and store this value in browser local storage
             $(".a_buildingcategory").change(function(){
                   //alert($(".a_buildingcategory option:selected").text());
                   building_category = $(".a_buildingcategory option:selected").text() ;
                   localStorage.setItem('selected_category',building_category) ;
             }) ;
             //get value entered by user on keyup
           /*  $(".a_builtup").mouseout(function(){
                 alert("Value is >>> "+$(this).val()) ;
             }) ; */
             //function to validate built up area
                  //stored building category 
             function bc_check_builtup_area(built_up_area_size,building_category){
                     
                    if(building_category === 'Category 1.B'){
                        
                        if(parseInt(built_up_area_size) > 100){
                           $('#a_builtup_error').show();
                           console.log("The built up area is "+parseInt(built_up_area_size)+ "More than >> 100 ");
                           $('#submit_primary').attr("disabled","disabled") ;
                        }else {
                            //before allowing submission check common user tricks - check the number of floors
                            $('#a_builtup_error').hide();
                            if (parseInt($('#a_no_of_floors').val()) > 0 ){
                                $('#submit_primary').attr("disabled","disabled") ;
                            }else{
                              //  $('#a_no_of_floors_error').show();
                                console.log("The built up area is "+parseInt(built_up_area_size)+ "Less than >> 100 ");
                                $('#submit_primary').removeAttr("disabled") ; 
                             }
                        }
                        
                    }else if(building_category === 'Category 1.C'){
                         if(parseInt(built_up_area_size) < 100 || parseInt(built_up_area_size) > 200 ){
                           $('#a_builtup_error').show();
                           console.log("The built up area is wrong "+parseInt(built_up_area_size)+ " ");
                           $('#submit_primary').attr("disabled","disabled") ;
                        }else {
                            //before allowing submission check common user tricks - check the number of floors
                            $('#a_builtup_error').hide();
                              console.log("No of Floors category 1c built up area >>> "+parseInt($('#a_no_of_floors').val()));
                            if (parseInt($('#a_no_of_floors').val()) > 0 ){
                                $('#submit_primary').attr("disabled","disabled") ;
                            }else{
                              //  $('#a_no_of_floors_error').show();
                                console.log("The built up area is "+parseInt(built_up_area_size)+ " ");
                                $('#submit_primary').removeAttr("disabled") ; 
                             }
                        }
                    }
                    else if(building_category === 'Category 2' || building_category === 'Category 3' || building_category === 'Category 4'){
                         if(parseInt(built_up_area_size) < 200 ){
                           $('#a_builtup_error').show();
                           console.log("The built up area is wrong "+parseInt(built_up_area_size)+ " ");
                           $('#submit_primary').attr("disabled","disabled") ;
                        }else {
                            //before allowing submission check common user tricks - check the number of floors
                            $('#a_builtup_error').hide();
                              console.log("No of Floors category 2,3,4 built up area >>> "+parseInt($('#a_no_of_floors').val()));
                            if (parseInt($('#a_no_of_floors').val()) > 0 && parseInt($('#a_no_of_floors').val()) <= 2 ){
                                     $('#submit_primary').removeAttr("disabled") ; 
                            }else{                            
                                     $('#submit_primary').attr("disabled","disabled") ;
                             }
                        }
                    }
                    else {
                         
                        
                    }
         }
         function bc_check_no_of_floors(no_of_floors,building_category){            
              if(building_category === 'Category 1.B'){
                  
                        if(parseInt(no_of_floors) > 0 ){
                            $('#a_no_of_floors_error').show();
                            console.log("No of Floors Exceeds limit of 0 floors >> "+parseInt(no_of_floors));
                            $('#submit_primary').attr("disabled","disabled") ;
                        }else {
                            //avoid common user tricks -- check the built up area set
                            $('#a_no_of_floors_error').hide();
                             if(parseInt($('a_builtup').val()) > 100){
                                 
                                 $('#submit_primary').attr("disabled","disabled") ;
                             }else {
                                   
                                   console.log("No of Floors is okay >>"+parseInt(no_of_floors));
                                   $('#submit_primary').removeAttr("disabled") ;
                             }
                            
                            
                        }
                        
                    }else if(building_category === 'Category 1.C'){
                         if(parseInt($('a_builtup').val()) < 100 || parseInt($('a_builtup').val()) > 200 ){
                           $('#a_builtup_error').show();
                           //console.log("The built up area is wrong "+parseInt(built_up_area_size)+ " ");
                           $('#submit_primary').attr("disabled","disabled") ;
                        }else {
                            //before allowing submission check common user tricks - check the number of floors
                            $('#a_builtup_error').hide();
                            console.log("No of Floors category 1c >>> "+no_of_floors);
                            if (parseInt(no_of_floors) > 0 ){
                                $('#a_no_of_floors_error').show();
                                $('#submit_primary').attr("disabled","disabled") ;
                            }else{
                                $('#a_no_of_floors_error').hide();
                                //console.log("The built up area is "+parseInt(built_up_area_size)+ " ");
                                $('#submit_primary').removeAttr("disabled") ; 
                             }
                        }
                    }
                    else if(building_category === 'Category 2' || building_category === 'Category 3' || building_category === 'Category 4'){
                         if(parseInt($('a_builtup').val()) < 200 ){
                           $('#a_builtup_error').show();
                           //console.log("The built up area is wrong "+parseInt(built_up_area_size)+ " ");
                           $('#submit_primary').attr("disabled","disabled") ;
                        }else {
                            //before allowing submission check common user tricks - check the number of floors
                            $('#a_builtup_error').hide();
                            console.log("No of Floors category 2,3,4 >>> "+no_of_floors);
                            if (parseInt(no_of_floors) > 0 && parseInt(no_of_floors) <= 2 ){
                                $('#a_no_of_floors_error').show();
                                $('#submit_primary').attr("disabled","disabled") ;
                            }else{
                                $('#a_no_of_floors_error').hide();
                                //console.log("The built up area is "+parseInt(built_up_area_size)+ " ");
                                $('#submit_primary').removeAttr("disabled") ; 
                             }
                        }
                    }
                    else {
                        //nothing
                    }
         }
            
             //Binding focus in and out to detect when mouse enters and leaves an element
             //http://stackoverflow.com/questions/4585890/jquery-detecting-when-a-user-clicks-out-of-an-input-type-text-field
              //get built up area user entered 
             /* $(".a_builtup").focus(function() {
                  //  alert('in');
                }).blur(function() {
                   built_up_area_size = $(this).val();
                   bc_check_builtup_area(built_up_area_size);
                }); */
                 /*$(".a_builtup").mouseleave(function(){
                    var built_up_area_size = $(this).val();
                     var building_category = localStorage.getItem('selected_category') ;
                     bc_check_builtup_area(built_up_area_size,building_category);
                 }) ;*/
             //get no of floors user entered 
             /* $(".a_no_of_floors").focus(function() {
                  //  alert('in');
                }).blur(function() {
                   no_of_floors = $(this).val();
                }); */
                
               /* $(".a_no_of_floors").mouseleave(function(){
                     var floors = $(this).val();
                     var building_category = localStorage.getItem('selected_category') ;
                     bc_check_no_of_floors(floors,building_category);
                 }) ;*/
         
          
          
               /* var selectobject= $('.a_buildingcategory').attr("readonly","1") ;
            selectobject.attr("readonly","1") ;
                for (var i=0; i<selectobject.length; i++){
                if (selectobject.options[i].name == 'Category 1.C' )
                   selectobject.remove(i);
            }*/
             //function to check if a value is a valid integer
            function isInt(value) {
                var x = parseFloat(value);
                return !isNaN(value) && (x | 0) === x;
            }
           $('.a_buildingcoverage').attr("readonly","1") ;
          //for building coverage calculation 
          /*$(".a_builtup").keyup(function () {
              var plot_size = $(".a_plotsize").val();
              var built_up = $(".a_builtup").val(); 
              console.log("Plot sizsd "+plot_size);
              console.log("Plot built_up "+built_up);
             // var buildingcoverage = Math.round((built_up / plot_size ) * 100) ; 
              var buildingcoverage = built_up / plot_size ; 
              
               if (plot_size) {
                   $('.a_buildingcoverage').val(buildingcoverage);
               }
          });*/
          /* $(".a_plotsize").keyup(function () {
              var plot_size = $(".a_plotsize").val();
              var built_up = $(".a_builtup").val(); 
             // var buildingcoverage = Math.round((built_up / plot_size ) * 100) ; 
              var buildingcoverage = built_up / plot_size ; 
              
               if (built_up) {
                   $('.a_buildingcoverage').val(buildingcoverage);
               }
          });*/
          
          $("#btn_buildingcoverage").click(function (e) {
              var plot_size = $(".a_plotsize").val();
              var built_up = $(".a_builtup").val(); 
              var buildingcoverage = ((built_up / plot_size ) * 100).toFixed(2) ; 
              //var buildingcoverage = built_up / plot_size ; 
              
               if (built_up && plot_size) {
                   $('.a_buildingcoverage').val(buildingcoverage);
               }else {
                   alert("Please Add built up area and plot size information!");
               }
               e.preventDefault();
          });
          
           $("#btn_floorarearatio").click(function (e) {
              var plot_size = $(".a_plotsize").val();
              var grossfloorarea = $(".a_grossfloorarea").val(); 
              var floorarearatio = ((grossfloorarea / plot_size ) * 100).toFixed(2) ;  
              
               if (grossfloorarea && plot_size) {
                   $('.a_floorarearatio').val(floorarearatio);
               }else {
                   alert("Please Add gross floor area and plot size information!");
               }
               e.preventDefault();
          });
          
          //for floor area ratio calculation
          $('.a_floorarearatio').attr("readonly","1") ;
         /* $(".a_grossfloorarea").keyup(function () {
              var plot_size = $(".a_plotsize").val();
              var grossfloorarea = $(".a_grossfloorarea").val(); 
              var floorarearatio = Math.round((grossfloorarea / plot_size ) * 100) ; 
              
               if (isInt(plot_size) && isInt(grossfloorarea)) {
                   $('.a_floorarearatio').val(floorarearatio);
               }
          });*/
        </script>
       
              <script type="text/javascript">
          //Script to hide upload fields
         $(".hide_me").css("display","none");
         $("#success_upi").css("display","none");
         $("#failed_upi").css("display","none");
         $("#upi_connexition_error").css("display","none"); 
         $("#upi_exists").css("display","none");
            </script>
            
            <script type="text/javascript">
                function initChild(child) {
                $(child).parent().parent().parents().next().toggle(); return false;             
              }
              function initChild2(child) {
                $(child).parent().parents().next().toggle(); return false;             
              }
              
            </script>
            
            <!-- validate UPI -->
            <script type="text/javascript">
                $(".plot_number_upi").hover(function(){
                    
                    var element_id = $(this).attr("id");
                   // console.log("Key Up event "+$('.plot_number_upi').val());
                    $.ajax({
                        type : 'POST' ,
                        url: '<?php echo public_path() ?>index.php/forms/validateUPI' ,
                        data: { 
                               upi : $('.plot_number_upi').val(),
                               element: element_id ,
                               form_id : <?php echo $_GET['id'] ?> 
                        },        
                        success: function(response) 
                        {
                            //console.log("Before json request");

                             var parsed_data = JSON.parse(response);
                             var n = parsed_data ;
                             //
                             //console.log("Off test "+n);
                             if(n === 'invalid'){
                                 //disable submit
                                   $('#submit_primary').attr("disabled","disabled") ;
                                   $("#upi_exists").css("display","block");
                             }else {
                                 
                                 //enable submit...
                                 $('#submit_primary').removeAttr("disabled") ; 
                                 $("#upi_exists").css("display","none");
                               
                             }
                             
                        } ,
                        error: function(error) 
                        {
                            console.log("Error validate UPI ") ;
                        }
                     }) ;
                }) ;
                
            </script>
        </div>