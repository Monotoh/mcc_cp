<?php
/**
 * groupsSuccess.php template.
 *
 * Displays list of all of the categories of applications
 *
 * @package    frontend
 * @subpackage application
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
 use_helper('I18N');


include_component('index', 'checksession');

//Get site config properties
$q = Doctrine_Query::create()
    ->from("ApSettings a")
    ->where("a.id = 1")
    ->orderBy("a.id DESC");
$apsettings = $q->fetchOne();
?>
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title"><?php echo __('Submit New Applications for Approval') ?></h4>
    </div>
</div>
<!-- Page-Title -->
<?php 
 $otbhelper = new OTBHelper();
?>

<div class="row">
    <div class="col-lg-8">

        <div class="panel-group" id="accordion-test-2">
            
             <div class="panel panel-default">
                 <?php
                                $q = Doctrine_Query::create()
				->select('DISTINCT(a.province) as province')
                                     ->distinct()
                                    ->from('ApForms a')
				    ->where('a.province IS NOT NULL');
                                    $provinces = $q->execute();
		$mycount = 10 ;						
		?>
                 <?php foreach($provinces as $province){ ?>
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse"  data-parent="#accordion-test-2" href="#collapseTwo-<?php echo $mycount; ?>"
                               class="collapsed btn btn-default" style="color:#28a5d4;" aria-expanded="false">
                                <!-- OTB patch 
                                   Add Form descriptions 
                                -->
                               <b> <?php echo $province->getProvince(); ?> </b>
                               
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo-<?php echo $mycount; ?>" class="panel-collapse collapse">
                         <!-- FOR districts -->
                                <?php
                                $q = Doctrine_Query::create()
				->select('DISTINCT(a.district) as district')
                                     ->distinct()
                                    ->from('ApForms a')
				    ->where('a.district IS NOT NULL')
                                    ->andWhere('a.province = ?',$province->getProvince());
                                    $districts = $q->execute();
		               $mycount2 = 1 ;						
		                ?>
                           <?php foreach($districts as $district){ ?>
                             <div class="panel-default">
                                   <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion-test-3" href="#collapseTwo-<?php echo $mycount2; ?><?php echo str_replace(" ", "_", $district->getDistrict());?>"
                                           class="collapsed" aria-expanded="false" style="color:#ef9c07;">
                                            <!-- OTB patch 
                                               Add Form descriptions 
                                            -->
                                           (<?php echo $mycount2; ?>).<?php echo $district->getDistrict();?> - <?php echo __('one stop center') ?>
                                        </a>
                                    </h4>
                                   </div>
                                    <div id="collapseTwo-<?php echo $mycount2; ?><?php echo str_replace(" ", "_", $district->getDistrict());?>" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            
                                            <ul style="" class="list-group list-group-forms m-b-0">
                                <?php
                                $q = Doctrine_Query::create()
                                    ->from('ApForms a')
                                    ->andWhere('a.form_type = 1')
                                    ->andWhere('a.form_active = 1')
                                   // ->andWhere('a.form_group = ?', $group->getGroupId())
                                    ->andWhere('a.district = ?', $district->getDistrict())//OTB - Quick solution for arranging service access as per RHA requirements i.e. Construction Permit > Province > District > Apply
                                    ->orderBy('a.form_id ASC');
                                $forms = $q->execute();
                                $nums = 1 ;
                                foreach($forms as $form)
                                {
                                    //error_log("Form is >>>> ".$form->getFormName()) ;
                                    if (sfConfig::get('app_enable_categories') == "yes") {
                                        $q = Doctrine_Query::create()
                                            ->from('sfGuardUserCategoriesForms a')
                                            //OTB patch Disable this check. This check prevents users from viewing other forms   
                                            //->where('a.categoryid = ?', $sf_user->getGuardUser()->getProfile()->getRegisteras())
                                            ->andWhere('a.formid = ?', $form->getFormId());
                                        $category = $q->count();

                                        if ($category == 0) {
                                            continue;
                                        }
                                    }
                                    ?>
                                    <li class="list-group-item">
                                       
                                          <?php echo $otbhelper->integerToRoman($nums) ?>). <a href="<?php echo public_path(); ?>index.php/forms/info?id=<?php echo $form->getFormId(); ?>"><?php echo $form->getFormDescription(); ?></a><!--OTB - Removed form description when listing groups-->
                                    </li>
                                    <?php
                                    $nums++;
                                }
                                ?>
                                </ul>
</div>
                                    </div>
                              </div>
                           <?php 
                           $mycount2++;
                           } ?>
                         <!-- end for districts -->
                    </div>
                 <?php 
                  $mycount++;
                 } ?>
            </div>
            
            
            <?php

            $count = 0;

            $q = Doctrine_Query::create()
                ->from('FormGroups a')
                ->orderBy('a.group_name ASC');
            $groups = $q->execute();
            $count = 0;
            foreach($groups as $group)
            {
                $count++;
                $form_count = 0;

                $q = Doctrine_Query::create()
                    ->from('ApForms a')
                    ->where('a.form_group = ?', $group->getGroupId())
                    ->andWhere('a.form_type = 1')
                    ->andWhere('a.form_active = 1')
                    ->orderBy('a.form_name ASC');
                $applications = $q->execute();
                foreach ($applications as $application) {
                    //Check if enable_categories is set, if it is then filter application forms
                    if (sfConfig::get('app_enable_categories') == "yes") {
                        $q = Doctrine_Query::create()
                            ->from('sfGuardUserCategoriesForms a')
                            //OTB patch Disable this check. This check prevents users from viewing other forms    
                           // ->where('a.categoryid = ?', $sf_user->getGuardUser()->getProfile()->getRegisteras())
                            ->andWhere('a.formid = ?', $application->getFormId());
                        $category = $q->count();

                        if ($category == 0) {
                            continue;
                        } else {
                            $form_count++;
                        }
                    } else {
                        //If form category permissions is disabled and then just display the category
                        $form_count++;
                    }
                }

                if ($form_count == 0) {
                    continue;
                }

                ?>
             

                <?php
            }
            ?>
        </div>
    </div>

    <!--Display a sidebar with information from the site config-->
    <?php if($apsettings){ ?>
        <div class="col-lg-4">
            <div class="card-box widget-user">
                <div>
                    <img src="/asset_unified/images/users/avatar-1.jpg" class="img-responsive img-circle" alt="user">
                    <div class="wid-u-info">
                        <h4 class="m-t-0 m-b-5"><?php echo $sf_user->getGuardUser()->getProfile()->getFullname(); ?></h4>
                        <p class="m-b-5 font-13"><?php echo $sf_user->getGuardUser()->getProfile()->getEmail(); ?><br>
                           
                        </p>
                        <a class="btn btn-primary" href="/index.php/signon/logout"><?php echo __('Logout') ?></a>
                    </div>
                </div>
            </div>
            <div class="card-box widget-user">
                <?php echo html_entity_decode($apsettings->getOrganisationHelp()); ?>
            </div>
            <?php echo html_entity_decode($apsettings->getOrganisationSidebar()); ?>
        </div>
         <!--<div class="col-lg-4">
            <div class="card-box widget-user">
                <div>
                   
                        <h4 class="m-t-0 m-b-5"><?php echo __('Useful Resources') ?> </h4>
                        <ul>
                            <li>  </li>
                        </ul>
                    
                </div>
            </div>

        </div>-->
    <?php } ?>

</div>
