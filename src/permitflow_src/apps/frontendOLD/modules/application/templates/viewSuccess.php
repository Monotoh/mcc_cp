<?php
/**
 * viewSuccess.php template.
 *
 * Displays full application details
 *
 * @package    frontend
 * @subpackage application
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");

//If invoice is pending, check payment aggregator for remote confirmation
$invoice_manager = new InvoiceManager();
$invoice_manager->update_invoices($application->getId());

//Get site config properties
$q = Doctrine_Query::create()
    ->from("ApSettings a")
    ->where("a.id = 1")
    ->orderBy("a.id DESC");
$apsettings = $q->fetchOne();

//OTB patch 
$otbhelper = new OTBHelper();

//Get number of days since
function GetDaysSince($sStartDate, $sEndDate){
    //$start_ts = strtotime($sStartDate);
    $start_ts = strtotime(date("Y-m-d", strtotime($sStartDate)));
    $end_ts = strtotime($sEndDate);
    $diff = $end_ts - $start_ts;
    // date("Y-m-d H:i:s", strtotime($sEndDate));
    error_log("Start date >>> ".$sStartDate) ;
    error_log("End date >>> ".$sEndDate);
    return round($diff / 86400);
}

function GetLastDayWorkedOn($application){//OTB - Use last day of action on application. Work can still continue after a permit has been issued
							$q = Doctrine_Query::create()
								->from('ApplicationReference b')
								->where('b.application_id = ?', $application->getId())
								->andWhere('b.start_date IS NOT NULL')
								->andWhere('b.end_date IS NOT NULL')
								->andWhere('b.start_date <= b.end_date')
								->orderBy("b.id DESC");
							$application_reference_list = $q->execute();
							return $application_reference_list[0]->getEndDate();
						}

//Only the owner of the application should have access to the application
if($sf_user->getGuardUser()->getId() == $application->getUserId())
{
?>
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-6">
        <h4 class="page-title"><?php echo __('Application History') ?></h4>
    </div>
    <div class="col-sm-6 right">
        <div class="btn-group">
            <button type="button" class="btn btn-primary dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-print"></i> <?php echo __('Downloads') ?> <span class="caret"></span> </button>
            <ul class="dropdown-menu dropdown-menu-right">
			<?php
			//OTB Patch Start - For Old Permits in Kigali System, show link to download them
			$q = Doctrine_Query::create()
				 ->from('AttachedPermit a')
				 ->where('a.application_id = ?', $application->getId());
			$attachedpermits = $q->execute();

			foreach($attachedpermits as $attachedpermit)
			{
				?>
				<li><a target="_blank" onClick="window.location = '/index.php/application/permitoldformat?id=<?php echo $attachedpermit->getFormId(); ?>&entryid=<?php echo $attachedpermit->getEntryId(); ?>';"><?php echo $application->getApplicationId()." ".__('Permit'); ?></a></li>
				<?php
			}
			//OTB Patch End - For Old Permits in Kigali System, show link to download them
			?>
            <?php
			//OTB Patch Start - Do not show documents that are for reviewers only
			$q = Doctrine_Query::create()
				->select('a.id')
				->from("Permits a")
				->where("a.parttype = ?", 3);
			 $reviewer_permit_temp_ids = $q->execute(array(), Doctrine_Core::HYDRATE_SINGLE_SCALAR);//http://stackoverflow.com/questions/9135908/where-in-not-doctrine
			//OTB Patch End - Do not show documents that are for reviewers only

            $q = Doctrine_Query::create()
               ->from("SavedPermit a")
               ->where("a.application_id = ?", $application->getId())
                     ->andWhere("a.permit_status <> 0 ") //OTB patch - Do not show deleted permits
			   ->andWhereNotIn("a.type_id", $reviewer_permit_temp_ids)//OTB Patch - Do not show documents that are for reviewers only
               ->orderBy("a.id ASC");
            $saved_permits = $q->execute();
            ?>
                <?php
                foreach($saved_permits as $permit) {
                    ?>
                    <li><a href="/index.php/permits/view/id/<?php echo $permit->getId(); ?>"><?php echo $permit->getTemplate()->getTitle(); ?></a></li>
                    <?php
                }
                ?>
            </ul>
        </div>
        <a href="/index.php/application/edit?application_id=<?php echo $application->getId(); ?>" class="btn btn-primary dropdown-toggle waves-effect"><i class="fa fa-edit"></i><?php echo __('Edit') ?> </a>
<!--OTB linked-->
<?php
	$q = Doctrine_Query::create()
			->from('SubMenus a')
			->where('a.id = ?',$application->getApproved()) ;
	$stage = $q->fetchOne();
	if($stage and $stage->getStageType() == 20){//If this is a linked submission stage
		$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
		mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
        $sql = "SELECT * FROM ap_forms WHERE form_stage = ".$application->getApproved()."  AND form_active = 1 AND form_type = 1 ";
        $form_result = mysql_query($sql, $dbconn);

        while($row = mysql_fetch_assoc($form_result))
        {
          if($row['form_id'] != $application->getFormId())
          {
		    //OTB Fix Show linkto if a user is permitted to access a particular form 
		    $user_registered_as = Doctrine_Query::create()
		                         ->from('sfGuardUserProfile u')
		                         ->where('u.user_id = ?',$sf_user->getGuardUser()->getId()) ;
		     $user_registered_as_res =   $user_registered_as->fetchOne();
		     //if we have something
		     if($user_registered_as_res){
				// echo  $user_registered_as_res->getRegisterAs();
				 $sql_q = "SELECT formid from sf_guard_user_categories_forms where categoryid = ".$user_registered_as_res->getRegisterAs()." and formid=".$row['form_id']."" ;
				 $sql_result = mysql_query($sql_q, $dbconn);
				 //
				 while($row_r = mysql_fetch_assoc($sql_result)) {
					 //
					  //echo "<a class=\"btn btn-primary\" href='/index.php/forms/view?id=".$row['form_id']."&linkto=".$application->getId()."'>".'Apply for'." ".$row['form_description']." - ".$row['form_name']."</a>";
					  //Start OTB Africa - Remove static 'Apply for', commented out above
					//  echo "<a class=\"btn btn-primary\" href='/index.php/forms/view?id=".$row['form_id']."&linkto=".$application->getId()."'> ".$row['form_description']." - ".$row['form_name']."</a>";
					  //Start OTB Africa - Remove static 'Apply for'
				 }
			 }                    	  
           
            $action_count++;
          }
        }
	}
?>
<!--OTB linked-->
    </div>
</div>
<!-- Page-Title -->


<div class="row">

    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?php
                $q = Doctrine_Query::create()
                   ->from("ApForms a")
                   ->where("a.form_id = ?", $application->getFormId());
                $form = $q->fetchOne();
                if($form) {
                    ?>
                    <h4 class="text-dark  header-title"><?php echo $form->getFormName(); ?></h4>
                    <?php
                }
                ?>
                <p class="text-primary">
                    Ref: <?php echo $application->getApplicationId(); ?>
                </p>
                <p class="text-muted font-15"><?php echo __('Date of Submission') ?>: <?php echo $application->getDateOfSubmission(); ?> <br>
                    <?php
                    if($application->getDateOfResponse())
                    {
                        ?>
                        <?php echo __('Duration') ?>: <?php 
                                  $days = GetDaysSince($application->getDateOfSubmission(), GetLastDayWorkedOn($application));
                                  echo $days; ?>                           
                        <?php echo __('days') ?>
                        <br>
                        <?php
                    }
                    else {
                        ?>
                        <?php echo __('Duration') ?>: <?php 
                        echo GetDaysSince($application->getDateOfSubmission(), date("Y-m-d")); ?>
                            <?php echo __('days') ?>
                        <br>
                        <?php
                    }
                    ?>
                    <?php echo __('Approval') ?>: <?php echo $application->getStatusName(); ?>
                </p>
            </div>
            <div class="panel-body">
                <?php
                //Display control buttons that manipulate the application
                include_partial('viewdetails', array('application' => $application, 'choosen_form' => $choosen_form));
                ?>
            </div>
        </div>
    </div><!--Panel-dark-->


    <!--Display a sidebar with information from the site config-->
    <?php if($apsettings){ ?>
    <?php 
                        $q = Doctrine_Query::create() 
                             ->from('EntryDecline e')
                             ->where('e.entry_id = ? ',$application->getId()) 
                                ->andWhere('e.resolved = ?', 0);
                        $response =  $q->execute();
                     ?>
    <?php if(count($response) > 0): ?>
    <div class="col-lg-4">
        <div class="card-box widget-user">
                <h4 class="m-t-0 m-b-20 header-title"><b><?php echo __('Reasons for Decline') ?></b></h4>
                <div class="chat-conversation">
                    <ul>
                        
                        <?php foreach ($response as $r): ?>
                        <li> <font style="color:#FF0000;"> <?php echo $r['description'] ?> </font> </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
        </div>
    </div>
    <?php endif; ?>

      <div class="col-lg-4">
            <div class="card-box widget-user">
                <h4 class="m-t-0 m-b-20 header-title"><b><?php echo __('Messages') ?></b></h4>
                <div class="chat-conversation">
                    <ul class="conversation-list nicescroll">
                        <?php
                        $q = Doctrine_Query::create()
                            ->from('Communications a')
                            ->where('a.application_id = ?', $application->getId())
                            ->orderBy('a.id ASC');
                        $communications = $q->execute();
                        foreach($communications as $communication)
                        {
                            $messages[] = $communication;
                        }

                        if(sizeof($messages) <= 0){
                        ?>
                        <table class="table mb0">
                            <tbody>
                            <tr>
                                <td>
                                    <i class="bold-label"><?php echo __('No Messages'); ?></i>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <?php
                        }
                        else {
                            foreach($messages as $message) {
                                if($message->getArchitectId() != "") {
                                    $q = Doctrine_Query::create()
                                        ->from('SfGuardUser a')
                                        ->where('a.id = ?', $message->getArchitectId());
                                    $client = $q->fetchOne();

                                    $fullname = $client->getProfile()->getFullname();
                                    ?>
                                    <li class="clearfix">
                                        <div class="chat-avatar">
                                            <img style="height: 40px;" src="/asset_unified/images/users/avatar-1.jpg">
                                            <i><?php echo $message->getActionTimestamp(); ?></i>
                                        </div>
                                        <div class="conversation-text">
                                            <div class="ctext-wrap">
                                                <i><?php echo $fullname; ?></i>

                                                <p>
                                                    <?php echo $message->getContent(); ?>
                                                </p>
                                            </div>
                                            <?php if(!empty($message->getAttachment())): ?>
                                            <a class="btn btn-warning" href="/index.php/application/downloadFile/filename/<?php echo $message->getAttachment() ?>"> <?php echo __('Download File') ?>  </a>
                                            <?php endif; ?>
                                        </div>
                                        
                                    </li>
                                <?php
                                }
                                else if($message->getReviewerId() != "") {
                                    $message->setMessageRead("1");
                                    $message->save();
                                    $q = Doctrine_Query::create()
                                        ->from('CfUser a')
                                        ->where('a.nid = ?', $message->getReviewerId());
                                    $reviewer = $q->fetchOne();

                                    $fullname = $reviewer->getStrdepartment()." - ".$reviewer->getStrfirstname()." ".$reviewer->getStrlastname();
                                    ?>
                                    <li class="clearfix odd">
                                        <div class="chat-avatar">
                                            <img style="height: 40px;" src="/asset_unified/images/users/avatar-1.jpg">
                                            <i><?php echo $message->getActionTimestamp(); ?></i>
                                        </div>
                                        <div class="conversation-text">
                                            <div class="ctext-wrap">
                                                <i><?php echo $fullname; ?></i>

                                                <p>
                                                    <?php echo $message->getContent(); ?>
                                                     <br/>
                                            <?php if(!empty($message->getAttachment())): ?>
                                                     <a class="btn btn-warning" href="/index.php/application/downloadFile/filename/<?php echo $message->getAttachment() ?>"> <?php echo __('Download File') ?> </a>
                                            <?php endif; ?>
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                }
                            }
                        }
                        ?>

                    </ul>
                    <div class="row">
                        <form action="/index.php/application/view/id/<?php echo $application->getId(); ?>" method="post"  autocomplete="off" data-ajax="false">
                            <div class="col-sm-9 chat-inputbar">
                                <input type="text" name="txtmessage" class="form-control chat-input" placeholder="<?php echo __('Enter your text') ?>">
                            </div>
                            <div class="col-sm-3 chat-send">
                                <button type="submit" class="btn btn-md btn-primary btn-block waves-effect waves-light"><?php echo __('Send') ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

                  <!-- chatjs  -->
                  <script src="<?php echo public_path(); ?>asset_unified/pages/jquery.chat.js"></script>
                  <script src="<?php echo public_path(); ?>asset_unified/js/jquery.core.js"></script>

            <?php
            $q = Doctrine_Query::create()
               ->from("MfInvoice a")
               ->where("a.app_id = ?", $application->getId());
            $invoices = $q->execute();

            foreach($invoices as $invoice) {
                ?>
                  <?php  
                   $invoice_expired = false ;
                              //OTB patch - If an invoice is expired hide make payment options and inform the user
                            // $db_date_expiry = null ; //Check if invoice expired
                      $db_date_event = str_replace('/', '-', $invoice->getExpiresAt());
                             $db_date_expiry = strtotime($db_date_event);
                             if(time() > $db_date_expiry){
                                 $invoice_expired = true ;
                             }
                            
                  ?>
                <div class="card-box widget-user">
                    <div>
                        <div>
                            <h4 class="m-t-0 m-b-5"><?php echo __('Billing Summary') ?></h4>
                            <table class="table  table-card-box">
                                <tbody>
                                <tr>
                                    <td style="width:30%;"><strong><?php echo __('Bill Ref') ?>:</strong></td>
                                    <td><?php echo $application->getFormId(); ?>
                                        /<?php echo $application->getEntryId(); ?>/<?php echo $invoice->getId(); ?></td>
                                </tr>
                                <tr>
                                    <td style="width:30%;"><strong><?php echo __('Status') ?>:</strong></td>
                                    <td><?php if ($invoice->getPaid() == 2) {
                                            echo __("Paid");
                                        } elseif ($invoice->getPaid() == 3) {
                                            echo __("Failed");
                                        } else {
                                            if(!$invoice_expired) {
                                            echo __("Pending"); }else {
                                                 echo __("Expired!");
                                            }
                                        } ?></td>
                                </tr>
                                <tr>
                                    <td style="width:30%;"><strong><?php echo __("Service") ?>:</strong></td>
                                    <td><?php
                                        $q = Doctrine_Query::create()
                                            ->from("ApForms a")
                                            ->where("a.form_id = ?", $application->getFormId());
                                        $form = $q->fetchOne();
                                        if ($form) {
                                            echo $form->getFormName()."-".$form->getFormDescription();
                                        }
                                        ?></td>
                                </tr>
                                <tr>
                                    <td style="width:30%;"><strong><?php echo __('Total') ?>:</strong></td>
                                    <td><strong><?php echo $invoice->getTotalAmount(); ?></strong></td>
                                </tr>
                                </tbody>
                            </table>
                           
                              <?php if(!$invoice_expired) { ?>

                            <a href="/index.php/invoices/printinvoice/id/<?php echo $invoice->getId(); ?>" class="btn btn-primary waves-effect w-md waves-light"><i class="fa fa-print"></i>
                                <?php echo __('Print Invoice') ?></a>
                              <?php } ?>

                            <?php
                            //if($invoice->getPaid() == 1 || $invoice->getPaid() == 15)
                            
                           
                            if($invoice->getPaid())
                            {
                                $sf_user->setAttribute('form_id', $application->getFormId());
                                $sf_user->setAttribute('entry_id', $application->getEntryId());
                                $sf_user->setAttribute('invoice_id', $invoice->getId());
                                ?>
                             <?php if(!$invoice_expired) { ?>
                               <!-- <a href="/index.php/forms/payment" class="btn btn-success waves-effect w-md waves-light"><i class="fa fa-credit-card"></i>
                                    <?php //echo __('Make Payment') ?></a> -->
                                    
                                    <?php
                                              
                                                $form_gateway = $otbhelper->getFormMerchant($application->getFormId());
                                                if($invoice->getPaid() == 1){
                                                    $sf_user->setAttribute('form_id', $application->getFormId());
                                                    $sf_user->setAttribute('entry_id', $application->getEntryId());
                                                    $sf_user->setAttribute('invoice_id', $invoice->getId());
                                                ?>
                                                <!--<button class="btn btn-white" id="makepayment" type="button" onClick="window.location='/index.php/forms/payment';"><i class="fa fa-print mr5"></i> <?php //echo __('Make Payment'); ?></button> -->
                                                 
                                                  <?php if($otbhelper->invoicePermitsMultiplePayment($invoice->getId()) == 1){ ?>
                                                  
                                                  <button class="btn btn-warning" id="makepayment" type="button" onClick="window.location='/index.php/forms/payment/gateway/<?php echo 'cash' ?>';"><i class="fa fa-print mr5"></i> <?php echo __('Attach Payment Receipt'); ?></button>
                                                  <?php }else { ?>
                                                   <button class="btn btn-success" id="makepayment" type="button" onClick="window.location='/index.php/forms/payment';"><i class="fa fa-print mr5"></i> <?php echo __('Pay Online'); ?></button> 
                                                  <?php } ?>
                                                 <?php } ?>
                                    
                                    
                                    
                             <?php } else {?>
                            <?php if($invoice->getPaid() != 2): ?>
                            <a href="#" class="btn btn-danger waves-effect w-md waves-light">
                                <?php echo __('Invoice Expired on ') ?> <?php 
                                echo date ('F j, Y',strtotime($invoice->getExpiresAt()))  ;
                             //   $format = 
                                        ?>
                            </a>
                            <?php endif; ?>
                            
                             <?php }
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

            <div class="card-box widget-user">
                <?php echo html_entity_decode($apsettings->getOrganisationHelp()); ?>
            </div>
        </div>
    <?php } ?>

</div><!-- /Content panel-->
<?php
}
else
{ ?>
    "<div class='contentpanel'><h3> <?php echo __('Sorry! You are trying to view a permit that doesnt belong to you') ?></h3></div>";
<?php 

}

    if($done == 1) {
        ?>
        <!-- Modal -->
        <div class="modal fade" id="submissionsModal" tabindex="-1" role="dialog"
             aria-labelledby="submissionsModalLabel"
             aria-hidden="true" style="margin-top: 15%;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel"><?php echo __('Your application has been received') ?>.</h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            <?php
                            //Display success message if available on the form
                            $q = Doctrine_Query::create()
                                ->from("ApForms a")
                                ->where("a.form_id = ?", $application->getFormId());
                            $apform = $q->fetchOne();
                            if ($apform && $apform->getFormSuccessMessage())
                            {
                            ?>

                        <div class="alert alert-success">
                            <?php echo $apform->getFormSuccessMessage(); ?>
                        </div>
                        <?php
                        }
                        else {
                            echo __("Your application has been submitted");
                        }
                        ?>
                        </p>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close') ?></button>
                    </div>
                </div>
                <!-- modal-content -->
            </div>
            <!-- modal-dialog -->
        </div><!-- modal -->
    <?php
    }
?>
