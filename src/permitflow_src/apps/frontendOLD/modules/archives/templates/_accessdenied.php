<?php 
use_helper("I18N");
?>
<div class="notfoundpanel">
  <h2><?php echo __('Access Denied!') ?></h2>
  <h4><?php echo __('Sorry, you are not allowed to view the page you are looking for!') ?></h4>
  <h4><?php echo __('If you are experiencing any trouble, please contact your system administrator') ?>.</h4>
</div><!-- notfoundpanel -->
