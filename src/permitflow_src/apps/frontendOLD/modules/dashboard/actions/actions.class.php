<?php
/**
 * Dashboard actions.
 *
 * Displays a summary of all application related activity.
 *
 * @package    frontend
 * @subpackage dashboard
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

class dashboardActions extends sfActions
{
     
 	/**
         
         
	* Executes 'Index' action
	*
 	* Displays the client's dashboard
 	*
 	* @param sfRequest $request A request object
 	*/
  	public function executeIndex(sfWebRequest $request)
  	{
        if($_SESSION['SESSION_CUTEFLOW_USERID'])
        {
            $_SESSION['SESSION_CUTEFLOW_USERID'] = false;
            $this->getUser()->getAttributeHolder()->clear();
            $this->getUser()->clearCredentials();
            $this->getUser()->setAuthenticated(false);
            $this->getUser()->signout();
            header("Location: /index.php");
            exit;
        }

    		if(!$this->getUser()->isAuthenticated())
    		{
    			$this->redirect('/index.php');
    		}

    		if($request->getParameter("show"))
    		{
    			$this->show = $request->getParameter("show");
    		}

    		$this->setLayout("layoutdash");


        $q = Doctrine_Query::create()
           ->from("FormEntry a")
           ->where("a.user_id = ?", $this->getUser()->getGuardUser()->getId())
           ->orderBy("a.date_of_submission DESC")
           ->limit(10);
        $this->latest_applications = $q->execute();

	   }
}
