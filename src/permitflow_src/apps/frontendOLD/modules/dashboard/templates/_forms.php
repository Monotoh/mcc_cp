<?php
/**
 * Created by PhpStorm.
 * User: thomasjuma
 * Date: 8/17/14
 * Time: 1:41 PM
 */

//Creates a read more summary from text
function createPreview($text, $limit) {
    $text = preg_replace('/\[\/?(?:b|i|u|s|center|quote|url|ul|ol|list|li|\*|code|table|tr|th|td|youtube|gvideo|(?:(?:size|color|quote|name|url|img)[^\]]*))\]/', '', $text);

    if (strlen($text) > $limit) return substr($text, 0, $limit) . "...";
    return $text;
}

$application_manager = new ApplicationManager();

?>
        <div class="col-md-8">
        <?php
        $q = Doctrine_Query::create()
            ->from('ApForms a')
            ->where('a.form_group = ?', $group_id)
            ->andWhere('a.form_type = 1')
            ->andWhere('a.form_active = 1')
            ->orderBy('a.form_name ASC');
        $applications = $q->execute();
        foreach($applications as $application)
        {

            //Check if enable_categories is set, if it is then filter application forms
            if(sfConfig::get('app_enable_categories') == "yes")
            {
                $q = Doctrine_Query::create()
                    ->from('sfGuardUserCategoriesForms a')
                    ->where('a.categoryid = ?', $sf_user->getGuardUser()->getProfile()->getRegisteras())
                    ->andWhere('a.formid = ?', $application->getFormId());
                $category = $q->count();

                if ($category == 0) {
                    continue;
                }
            }
            ?>
            <div class="panel panel-default" style="border-radius:8px; border:3pxx solid;">
                <div class="panel-body">
                <div class="media" style="margin:0; padding:0;">
                <a href="<?php echo public_path(); ?>index.php/forms/info?id=<?php echo $application->getFormId(); ?>" class="btn btn-sm btn-success pull-right">Learn More</a>
                     <h1 class="fa fa-file-text-o pull-left" style="margin:0; padding:0; margin-right:20px; color:#555; font-size:40px;"></h1>
                         <div class="media-body">
                              <a href="<?php echo public_path(); ?>index.php/forms/info?id=<?php echo $application->getFormId(); ?>" class="text-primary" style="margin:0; padding:0; font-size:18px;"><?php echo $application->getFormName(); ?></a>
                              <p class="email-summary" style="margin-bottom:0; padding:0;"><strong><?php echo $application->getFormRedirect(); ?></strong> </p>
                         </div>
                   </div>
                 </div>
            </div>
        <?php
        }

        if(sizeof($applications) == 0)
        {
            ?>
            <div class="blog-details" style="border-top: 1px solid #d2d2d2;">
                <h4 class="blog-title"><a href="">Coming Soon</a></h4>
            </div>
            <?php
        }
        ?>
        </div>
        <div class="col-sm-4">
            <div class="blog-sidebar">
                <h5 class="subtitle">Categories</h5>
                <ul class="sidebar-list">
                    <?php
                        $q = Doctrine_Query::create()
                            ->from('FormGroups a')
                            ->orderBy('a.group_name ASC');
                        $groups = $q->execute();
                        $count = 0;
                        foreach($groups as $group)
                        {
                            $form_count = 0;

                            $q = Doctrine_Query::create()
                                ->from('ApForms a')
                                ->where('a.form_group = ?', $group->getGroupId())
                                ->andWhere('a.form_type = 1')
                                ->andWhere('a.form_active = 1')
                                ->orderBy('a.form_name ASC');
                            $applications = $q->execute();
                            foreach($applications as $application)
                            {
                                //Check if enable_categories is set, if it is then filter application forms
                                if(sfConfig::get('app_enable_categories') == "yes")
                                {
                                    $q = Doctrine_Query::create()
                                        ->from('sfGuardUserCategoriesForms a')
                                        ->where('a.categoryid = ?', $sf_user->getGuardUser()->getProfile()->getRegisteras())
                                        ->andWhere('a.formid = ?', $application->getFormId());
                                    $category = $q->count();

                                    if ($category == 0) {
                                        continue;
                                    }
                                    else
                                    {
                                        $form_count++;
                                    }
                                }
                                else
                                {
                                    //If form category permissions is disabled and then just display the category
                                    $form_count++;
                                }
                            }

                            if($form_count == 0)
                            {
                                continue;
                            } 
                            
                            ?>
                            <li><a href="/index.php/application/groups/id/<?php echo $group->getGroupId(); ?>"><i class="fa fa-angle-right"></i> <?php echo $group->getGroupName(); ?></a></li>
                            <?php
                        }
                    ?>
                </ul>
            </div>
        </div>
           
