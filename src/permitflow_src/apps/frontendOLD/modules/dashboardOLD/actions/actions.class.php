<?php
/**
 * Dashboard actions.
 *
 * Displays a summary of all application related activity.
 *
 * @package    frontend
 * @subpackage dashboard
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

class dashboardActions extends sfActions
{
    /**
     * Notice message
     */
    public function executeNotice(sfWebRequest $request){
        $record_id = $request->getParameter("rec_id") ;
        //
        $q = Doctrine_Query::create()
                ->from('EntryDecline e')
                ->where('e.id = ?', $record_id) ;
        //
        $this->res = $q->fetchOne();
        
        $this->setLayout("layoutdash");
    }


    
 	/**
	* Executes 'Index' action
	*
 	* Displays the client's dashboard
 	*
 	* @param sfRequest $request A request object
 	*/
  	public function executeIndex(sfWebRequest $request)
  	{
           $otbhelper = new OTBHelper() ;
        //
        $stage_ids = $otbhelper->getStageWithRequestExtraType(1) ; // inspections stages
        
        
        if($_SESSION['SESSION_CUTEFLOW_USERID'])
        {
            $_SESSION['SESSION_CUTEFLOW_USERID'] = false;
            $this->getUser()->getAttributeHolder()->clear();
            $this->getUser()->clearCredentials();
            $this->getUser()->setAuthenticated(false);
            $this->getUser()->signout();
            header("Location: /index.php");
            exit;
        }

    		if(!$this->getUser()->isAuthenticated())
    		{
    			$this->redirect('/index.php');
    		}

    		if($request->getParameter("show"))
    		{
    			$this->show = $request->getParameter("show");
    		}

    		$this->setLayout("layoutdash");


        $q = Doctrine_Query::create()
           ->from("FormEntry a")
           ->where("a.user_id = ?", $this->getUser()->getGuardUser()->getId())
           ->andWhere("a.deleted_status = ?", 0)
          ->andWhere('a.parent_submission = ?', 0)
           ->addWhere("a.approved != ?", 0) //show non-drafts only
           ->orderBy("a.date_of_submission DESC")
           ->limit(10);
        
        if($stage_ids){
            $q->addWhere("a.approved not in (". implode(',', $stage_ids).") ");
        }
        
        $this->latest_applications = $q->execute();

	   }
           
           /**
      * OTB patch --- Addition of missing drafts view
      */
    public function executeDrafts(sfWebRequest $request){
         if($_SESSION['SESSION_CUTEFLOW_USERID'])
        {
            $_SESSION['SESSION_CUTEFLOW_USERID'] = false;
            $this->getUser()->getAttributeHolder()->clear();
            $this->getUser()->clearCredentials();
            $this->getUser()->setAuthenticated(false);
            $this->getUser()->signout();
            header("Location: /index.php");
            exit;
        }
        if(!$this->getUser()->isAuthenticated())
        {
                $this->redirect('/index.php');
        }

        if($request->getParameter("show"))
        {
                $this->show = $request->getParameter("show");
        }
         $q = Doctrine_Query::create()
           ->from("FormEntry a")
           ->where("a.user_id = ?", $this->getUser()->getGuardUser()->getId())
           ->addWhere("a.approved = ?", 0) //drafts only
           ->orderBy("a.date_of_submission DESC")
           ->limit(10);
        $this->latest_draft_applications = $q->execute();
       
        $this->setLayout("layoutdash");
    }
    /**
     * 
     * @param sfWebRequest $request
     * Inspections apps listing
     */
     public function executeInspection(sfWebRequest $request){
         if($_SESSION['SESSION_CUTEFLOW_USERID'])
        {
            $_SESSION['SESSION_CUTEFLOW_USERID'] = false;
            $this->getUser()->getAttributeHolder()->clear();
            $this->getUser()->clearCredentials();
            $this->getUser()->setAuthenticated(false);
            $this->getUser()->signout();
            header("Location: /index.php");
            exit;
        }
        if(!$this->getUser()->isAuthenticated())
        {
                $this->redirect('/index.php');
        }

        if($request->getParameter("show"))
        {
                $this->show = $request->getParameter("show");
        }
        $otbhelper = new OTBHelper() ;
        //
        $stage_ids = $otbhelper->getStageWithRequestExtraType(1) ; // inspections stages
        //
         $q = Doctrine_Query::create()
           ->from("FormEntry a")
           ->where("a.user_id = ?", $this->getUser()->getGuardUser()->getId())
           ->orderBy("a.date_of_submission DESC")
           ->limit(10);
         
         if($stage_ids){
            $q->addWhere("a.approved not in (". implode(',', $stage_ids).") ");
        }
        $this->l_applications = $q->execute();
       
        $this->setLayout("layoutdash");
    }
     /**
     * 
     * @param sfWebRequest $request
     * Occupation apps listing
     */
     public function executeOccupation(sfWebRequest $request){
         if($_SESSION['SESSION_CUTEFLOW_USERID'])
        {
            $_SESSION['SESSION_CUTEFLOW_USERID'] = false;
            $this->getUser()->getAttributeHolder()->clear();
            $this->getUser()->clearCredentials();
            $this->getUser()->setAuthenticated(false);
            $this->getUser()->signout();
            header("Location: /index.php");
            exit;
        }
        if(!$this->getUser()->isAuthenticated())
        {
                $this->redirect('/index.php');
        }

        if($request->getParameter("show"))
        {
                $this->show = $request->getParameter("show");
        }
        $otbhelper = new OTBHelper() ;
        //
        $stage_ids = $otbhelper->getStageWithRequestExtraType(2) ; // occupation stages
        //
         $q = Doctrine_Query::create()
           ->from("FormEntry a")
           ->where("a.user_id = ?", $this->getUser()->getGuardUser()->getId())
          // ->addWhere("a.approved in (". implode(',', $stage_ids).") ") //only in inspection
           ->orderBy("a.date_of_submission DESC")
           ->limit(10);
         if($stage_ids){
            $q->addWhere("a.approved not in (". implode(',', $stage_ids).") ");
        }
        $this->l_applications = $q->execute();
       
        $this->setLayout("layoutdash");
    }
}
