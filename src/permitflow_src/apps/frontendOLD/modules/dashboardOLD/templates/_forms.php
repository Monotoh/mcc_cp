<?php
/**
 * Created by PhpStorm.
 * User: thomasjuma
 * Date: 8/17/14
 * Time: 1:41 PM
 */
use_helper("I18N");
//Creates a read more summary from text
function createPreview($text, $limit) {
    $text = preg_replace('/\[\/?(?:b|i|u|s|center|quote|url|ul|ol|list|li|\*|code|table|tr|th|td|youtube|gvideo|(?:(?:size|color|quote|name|url|img)[^\]]*))\]/', '', $text);

    if (strlen($text) > $limit) return substr($text, 0, $limit) . "...";
    return $text;
}

$application_manager = new ApplicationManager();

?>
        <div class="col-md-8">
        <?php
        $q = Doctrine_Query::create()
            ->from('ApForms a')
            ->where('a.form_group = ?', $group_id)
            ->andWhere('a.form_type = 1')
            ->andWhere('a.form_active = 1')
            ->orderBy('a.form_name ASC');
        $applications = $q->execute();
        foreach($applications as $application)
        {

            //Check if enable_categories is set, if it is then filter application forms
            if(sfConfig::get('app_enable_categories') == "yes")
            {
                $q = Doctrine_Query::create()
                    ->from('sfGuardUserCategoriesForms a')
                    ->where('a.categoryid = ?', $sf_user->getGuardUser()->getProfile()->getRegisteras())
                    ->andWhere('a.formid = ?', $application->getFormId());
                $category = $q->count();

                if ($category == 0) {
                    continue;
                }
            }
            ?>
            <div class="panel panel-default" style="border-radius:8px; border:3pxx solid;">
                <div class="panel-body">
                <div class="media" style="margin:0; padding:0;">
                <a href="<?php echo public_path(); ?>index.php/forms/info?id=<?php echo $application->getFormId(); ?>" class="btn btn-sm btn-success pull-right"><?php echo __('Learn More') ?></a>
                     <h1 class="fa fa-file-text-o pull-left" style="margin:0; padding:0; margin-right:20px; color:#555; font-size:40px;"></h1>
                         <div class="media-body">
                              <a href="<?php echo public_path(); ?>index.php/forms/info?id=<?php echo $application->getFormId(); ?>" class="text-primary" style="margin:0; padding:0; font-size:18px;"><?php echo $application->getFormName(); ?></a>
                              <p class="email-summary" style="margin-bottom:0; padding:0;"><strong><?php echo $application->getFormRedirect(); ?></strong> </p>
                         </div>
                   </div>
                 </div>
            </div>
        <?php
        }

        if(sizeof($applications) == 0)
        {
            ?>
            <div class="blog-details" style="border-top: 1px solid #d2d2d2;">
                <h4 class="blog-title"><a href=""><?php echo __('Coming Soon') ?></a></h4>
            </div>
            <?php
        }
        ?>
        </div>
        <div class="col-sm-4">
            <div class="blog-sidebar">
                <h5 class="subtitle"><?php echo __('Categories') ?></h5>
                <?php 
                  //OTB patch - Check if user validated
                        $membersManager = new MembersManager();
	                $membership = $membersManager->MembershipIsValidated($sf_user->getGuardUser()->getId());
                ?>
                 <?php if($membership and $membership['validated']) {?>
                <ul class="sidebar-list">
                    <?php
                        $q = Doctrine_Query::create()
                            ->from('FormGroups a')
                            ->orderBy('a.group_name ASC');
                        $groups = $q->execute();
                        $count = 0;
                        
                        
                        foreach($groups as $group)
                        {
                            ?>
                            <li><a href="/index.php/application/groups/id/<?php echo $group->getGroupId(); ?>"><i class="fa fa-angle-right"></i> <?php echo $group->getGroupName(); ?></a></li>
                            <?php
                        }
                    ?>
                 <?php } else {?>
                            <div class="blog-details" style="border-top: 1px solid #d2d2d2; background:#fff;">
					<div class="blog-summary">
					<h4 style="color:red"><b><i class="fa fa-exclamation-triangle fa-5x"></i><?php echo __('ATTENTION'); ?> <?php echo strtoupper($actual_category->getName());?> - <?php echo __('PLEASE PROVIDE US WITH YOUR ARCHITECTS/ENGINEERS REGISTRATION DETAILS'); ?></b></h4>
					<p><?php echo __('If you are viewing this message, you cannot make any submissions because');?>:</p>
					<ul>
					<li>1. <?php echo __('You have not provided us with additional details for your user category');?> <a class="btn btn-sm btn-primary" href="/index.php/mfRegister/registerDetails2?formid=<?php echo $user_form; ?>"><?php echo __('Click Here') ?></a> or ;</li>
					<li>2. <?php echo __('You need to update your professional membership details after annual renewal and you must validate this update by clicking the link sent to your membership email.');?></li>
					</ul>
					<p><?php echo __('Note: You must ensure you are duly registered with ');?><b><?php echo $actual_category->getMemberAssociationName();?></b><?php echo __(' as a ');?><b><?php echo $actual_category->getName();?>.</b></p>
                                        
					<?php if(strlen($membership['member_no'])): ?>
					<p><?php echo __('If you have still not received a verification email in your inbox, kindly click the button below.');?></p>
					<?php if(strlen($sf_user->getAttribute('boraqs_reset',''))): ?>
					<div class="alert alert-warning">
						<p><?php echo $sf_user->getAttribute('boraqs_reset') ?></p>
					</div>
					<?php endif; ?>
					<?php $sf_user->getAttributeHolder()->remove('boraqs_reset'); ?>
					<p>
                                            <a class="btn btn-sm btn-primary" href="/index.php/mfRegister/registerDetails2?formid=<?php echo $user_form; ?>"><?php echo __('Click Here') ?></a> </p>
                                            <a href="<?php echo '/index.php/membersdatabase/resendboraq' ?>" class="btn btn-warning"><?php echo __('Resend Verification Email');?></a></p>-->
					<?php endif; ?>
					</div>
				</div>
                 <?php } ?>
                </ul>
            </div>
        </div>
           
