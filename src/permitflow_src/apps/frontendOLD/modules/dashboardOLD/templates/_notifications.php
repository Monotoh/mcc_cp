<?php
/**
 * _notifications template.
 *
 * Displays a list of the latest notifications
 *
 * @package    frontend
 * @subpackage notifications
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");
?>
<!--OTB Start - Notification for users who need to transfer data to account -->
<?php

$statement = "select sfu.id,sfu.username,sfp.email, sfp.fullname, sfp.registeras,sfu.is_active,sfu.is_super_admin,sfu.created_at,sfu.last_login from sf_guard_user_profile sfp
				inner join sf_guard_user sfu on sfu.id=sfp.user_id
				where sfp.email='".$sf_user->getGuardUser()->getProfile()->getEmail()."' and sfu.id <> ".$sf_user->getGuardUser()->getId()." and sfu.id>=200000 order by email;";
$sql_res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($statement);
	error_log(count($sql_res)." ## count before We are transferring my guy!!!!");
foreach($sql_res as $transfer_from){
	error_log("We are transferring my guy!!!! ".$transfer_from['id']);

	$q = Doctrine_Query::create()
		->from("FormEntry a")
		->where("a.user_id = ?", $transfer_from['id'])
		->andWhere('a.deleted_status = 0')
		->andWhere('a.parent_submission = 0');
	if($q->count()>0){
	$transferring_applications = $q->execute();
	$encryptdata = base64_encode(json_encode(array('origin_user_id' => $transfer_from['id'])));
	?>
		<div class="alert alert-info">
		<h4><?php echo __('Dear User, Our system has detected that you had registered different accounts on BPMIS (bpmis.gov.rw) and Kigali CPMIS (kcps.gov.rw) using the same email address. This is before we merged the 2 systems to BPMIS.
		 Following this migration, kindly note that you can transfer any application that you do not currently see in your list of applications. Below are a list of the applications you can transfer to this account:') ?></h4>
		 <a href="/index.php/accountdatatransfer/index/code/<?php echo $encryptdata; ?>" class="btn btn-success"><?php echo __('Show List') ?></a> 
		</div>
	<?php
	}
}
?>
<!--OTB End - Notification for users who need to transfer data to account -->
<?php
$q = Doctrine_Query::create()
    ->from("FormEntry a")
    ->where("a.user_id = ?", $sf_user->getGuardUser()->getId())
    ->andWhere("a.approved <> ?", 0)
    ->andWhere('a.parent_submission = ?', 0)
    ->andWhere("a.declined = 1")
     ->andWhere("a.deleted_status = 0")
    ->orderBy("a.id DESC")
    ->limit(2);
$corrections_applications = $q->execute();
foreach($corrections_applications as $application)
{
    ?>
    <div class="alert alert-danger">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
        <strong><?php echo __('Corrections!') ?></strong><?php echo __(' Application with Reference No <b>
                <a href=/index.php/application/view/id/'.$application->getId().'>'.$application->getApplicationId().'</a>  has been sent back to you for Amendments. The following are the reasons:') ?>
        <?php
        $q = Doctrine_Query::create()
           ->from("EntryDecline a")
           ->where("a.entry_id = ?", $application->getId());
        $comments = $q->execute();
        ?>
        <ol style="numbers">
            <?php foreach($comments as $comment){ ?>
                    <li>
                        
                        <?php echo substr($comment->getDescription(), 0, 50.); ?> ... <a href="/index.php/dashboard/notice/rec_id/<?php echo $comment->getId(); ?>">
                Click Here to Read More
                </a> 
                       
                 <br/>
                <?php if($comment->getResolved()){ echo "<span class='label label-success'>Resolved</span>"; }else{ echo "<span class='label label-danger'>Not Resolved</span>"; } ?>
                <?php if(!empty($comment->getAttachment())): ?>
                <?php 
                   $filename = $comment->getAttachment() ;
                ?>
              <!--  <a class="btn btn-primary" href="/backend.php/forms/downloadFile/filename/<?php //echo $filename ?>" > Download File Shared </a>-->
                <?php endif; ?>
             </li>
            <?php } ?>
        </ol>
        <strong> <a href="/index.php/application/edit?application_id=<?php echo $application->getId(); ?>"><?php echo __('Click here') ?></a><?php echo __(' to edit application details and resubmit.') ?></strong>
    </div>
    <?php
}
?>


<?php
$q = Doctrine_Query::create()
    ->from("FormEntry a")
    ->where("a.circulation_id = ?", $sf_user->getGuardUser()->getId())
    ->limit(2);
$transferring_applications = $q->execute();

foreach($transferring_applications as $application)
{
    $origin_user = Doctrine_Core::getTable("SfGuardUser")->find($application->getUserId());
    $origin_user_profile = $origin_user->getProfile();

    $id = $application->getId();
    $data = json_encode(array('id' => $id));
    $encryptdata = base64_encode($data);
    ?>
    <div class="alert alert-success">
        <h4><?php echo __('Transfer!') ?></h4> <?php echo __('You have an application') ?> (<?php echo $application->getApplicationId(); ?> ) <?php echo __('that is being transferred to you from') ?> <?php echo $origin_user_profile->getFullname()." (".$origin_user_profile->getEmail().")"; ?>. <br><br>
        <a href="/index.php/application/accepttransfer/code/<?php echo $encryptdata; ?>" class="btn btn-success"><?php echo __('Confirm Transfer') ?></a>  <a class="btn btn-danger" href="/index.php/application/canceltransfer/code/<?php echo $encryptdata; ?>"><?php echo __('Cancel Transfer') ?></a>
    </div>
    <?php
}
?>

