<?php

/**
 * _latest_applications template.
 *
 * Displays a list of the latest applications
 *
 * @package    frontend
 * @subpackage lastest_applications
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");

function GetDaysSince($sStartDate, $sEndDate){
    $start_ts = strtotime($sStartDate);
    $end_ts = strtotime($sEndDate);
    $diff = $end_ts - $start_ts;
    return round($diff / 86400);
    
}
?>
<div class="col-lg-8">
     <!-- OTB patch - Message to Inform users of the upgrade -->
    <div class="card-box">
          <a href="/index.php/application/groups" class="pull-right btn btn-primary waves-effect w-md waves-light"><?php echo __('Submit New Application') ?></a>
        <h4 class="text-dark  header-title m-t-0"><?php echo __('Recent Occupancy Applications') ?></h4>
        <p class="text-muted m-b-25 font-13">
            <?php echo __('Below are the inspections applications you made recently') ?>
        </p>
        <div class="table-responsive">
        <table class="table">
            <thead>
            <th><?php echo __("District"); ?></th>
            <th><?php echo __("Ref No"); ?></th>
            <th><?php echo __("Submitted On"); ?></th>
            <th><?php echo __("Bill Status"); ?></th>
            <th><?php echo __("Stage"); ?></th>           
            <th></th>
            </thead>
            <tbody>
            <?php foreach ($latest_applications as $application): ?>
                <?php
                $days =  GetDaysSince($application->getDateOfSubmission(), date("Y-m-d H:i:s"));
                ?>
                <?php
                use_helper("I18N");

                $q = Doctrine_Query::create()
                    ->from("AttachedPermit a")
                    ->where("a.application_id = ?", $application->getId());
                $permit = $q->fetchOne();
                ?>
                <tr>
                    <td>
                        <?php
                        //Display form name (Truncate if more than 16 characters)
                        if(strlen($application->getForm()->getFormName()) > 16)
                        {
                            echo "<a title='".html_entity_decode($application->getForm()->getFormName())."'>".html_entity_decode(substr($application->getForm()->getFormName(), 0, 16))."...</a>";
                        }
                        else
                        {
                            echo html_entity_decode($application->getForm()->getFormName());
                        }
                        ?>
                    </td>
                    <td>
                        <a href="<?php echo public_path(); ?>index.php/application/view/id/<?php echo $application->getId(); ?>"><?php echo $application->getApplicationId(); ?></a>
                    </td>
                     <td>
                        <?php echo date('d F Y', strtotime($application->getDateOfSubmission())); ?>
                    </td>
                    <td>
                        <?php
                        //Check billing status of the application
                        $q = Doctrine_Query::create()
                            ->from("MfInvoice a")
                            ->where("a.app_id = ?", $application->getId())
                            ->andWhere("a.paid = 1 OR a.paid = 15");
                        $unpaid_invoices = $q->count();
                        if($unpaid_invoices > 0)
                        {
                            ?>
                        <?php 
                         //OTB patch - Check if Invoice Expired
                        $invoice = $q->fetchOne();
                        $db_date_event = str_replace('/', '-', $invoice->getExpiresAt());
                        $db_date_expiry = strtotime($db_date_event);
                        $expired_bill = false ;
                        ?>
                           <?php if(time() > $db_date_expiry) { ?>
                           <span class="label label-danger"><?php echo __("Expired Bill"); ?></span>
                           <?php 
                            $expired_bill = true ;
                           ?>
                           <?php } else { ?>                           
                             <span class="label label-warning"><?php echo __("Not Paid"); ?></span>
                           <?php } ?>
                            <?php
                        }
                        else
                        {
                            $q = Doctrine_Query::create()
                                ->from("MfInvoice a")
                                ->where("a.app_id = ?", $application->getId());
                            $invoices = $q->count();
                            if($invoices == 0)
                            {
                                ?>
                                <span class="label label-default"><?php echo __("No Bill"); ?></span>
                                <?php
                            }
                            else
                            {
                                ?>
                                <span class="label label-success"><?php echo __("Paid"); ?></span>
                                <?php
                            }
                        }
                        ?>
                    </td>
                    <td>
                        <span class="label label-primary">
                            <?php echo $application->getStatusName(); ?>
                        </span>
                    </td>
                   
                    <td>

                              <div class="btn-group">
                                <button type="button" class="btn btn-xs btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false"><?php echo __("Action"); ?> <span class="caret"></span></button>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a  title='<?php echo __('View Application'); ?>' href='<?php echo public_path(); ?>index.php/application/view/id/<?php echo $application->getId(); ?>'><?php echo __("View"); ?> </a></li>
                                <li><a  title='<?php echo __('Share Application'); ?>' href='<?php echo public_path(); ?>index.php/application/share/id/<?php echo $application->getId(); ?>'><?php echo __("Share"); ?> </a></li>
                                <?php
                                if($application->getDeclined() == 1 || $application->getApproved() == 0) {
                                    ?>
                                    <li><a title='<?php echo __('Edit Application'); ?>'
                                           href='<?php echo public_path(); ?>index.php/application/edit?application_id=<?php echo $application->getId(); ?>'><?php echo __("Edit"); ?> </a>
                                    </li>
                                    <?php
                                }

                                //Display links to the invoices
                                $q = Doctrine_Query::create()
                                    ->from("MfInvoice a")
                                    ->where("a.app_id = ?", $application->getId())
                                    ->limit(5);
                                $invoices = $q->execute();
                                foreach($invoices as $invoice)
                                {
                                    ?>
                                    <?php if(!$expired_bill): ?>
                                  <li><a title="<?php echo __('View Invoice') ?>" href="/index.php/invoices/view/id/<?php echo $invoice->getId(); ?>"><?php echo __("Print Invoice"); ?></a></li>
                                   <?php endif ; ?>
                                 <?php
                                }
                                ?>
                              </ul>
                          </div>

                        <?php
						//OTB Patch Start - Do not show documents that are for reviewers only
						$q = Doctrine_Query::create()
							->select('a.id')
							->from("Permits a")
							->where("a.parttype = ?", 3);
						 $reviewer_permit_temp_ids = $q->execute(array(), Doctrine_Core::HYDRATE_SINGLE_SCALAR);//http://stackoverflow.com/questions/9135908/where-in-not-doctrine
						//OTB Patch End - Do not show documents that are for reviewers only
                        //Display links to the permits
                        $q = Doctrine_Query::create()
                            ->from("SavedPermit a")
                            ->where("a.application_id = ?", $application->getId())
							->andWhereNotIn("a.type_id", $reviewer_permit_temp_ids)//OTB Patch - Do not show documents that are for reviewers only
                            ->limit(5);
                        if($q->count() > 0) {
                            ?>
                            <div class="btn-group">
                                <button type="button"
                                        class="btn btn-xs btn-default dropdown-toggle waves-effect waves-light"
                                        data-toggle="dropdown" aria-expanded="false"><?php echo __("Downloads"); ?>
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                    <?php
                                    $permits = $q->execute();
                                    foreach ($permits as $permit) {
                                        ?>
                                        <li><a title="View Permit"
                                               href="/index.php/permits/print/id/<?php echo $permit->getId(); ?>"><?php echo __("Print ") . $permit->getTemplate()->getTitle(); ?></a>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                            <?php
                        }
                        ?>
                    </td>
                </tr>

            <?php endforeach; ?>
            </tbody>

        </table>
       </div>
       <!--End responsive table-->

    </div>
</div>
