<?php
/**
 * indexSuccess.php template.
 *
 * Displays client dashboard
 *
 * @package    frontend
 * @subpackage dashboard
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper('I18N');

include_component('index', 'checksession');

//Get site config properties
$q = Doctrine_Query::create()
    ->from("ApSettings a")
    ->where("a.id = 1")
    ->orderBy("a.id DESC");
$apsettings = $q->fetchOne();
?>
<div class="contentpanel">

    <div class="row">
        <!--Display organisation description from the site config-->
        <div class="col-sm-6 col-lg-8">
            <div class="card-box card-box-light-orange widget-user">
                <div>
                    <img src="/asset_unified/images/users/avatar-1.jpg" class="img-responsive img-circle" alt="user">
                    <div class="wid-u-info">
                      <?php
                      $q = Doctrine_Query::create()
                      ->from('sfGuardUserProfile a')
                      ->where('a.user_id = ?', $sf_user->getGuardUser()->getId());
                      $user = $q->fetchOne();
                      ?>
                        <h4 class="m-t-0 m-b-5"><?php echo __('Welcome'); ?>, <?php  echo $user->getFullname(); ?></h4>
                        <?php
                        if($apsettings)
                        {
                        ?>
                           <p class="m-b-5 font-13"><?php echo $apsettings->getOrganisationDescription(); ?></p>
                        <?php
                        }
                        else
                        {
                        ?>
                           <p class="m-b-5 font-13"><?php echo sfConfig::get('app_organisation_description') ?></p>
                        <?php
                        }
                        ?>
                        <a class="m-t-5 btn btn-sm btn-default" href="<?php echo public_path(); ?>index.php/settings"><?php echo __('My Profile'); ?></a>
                        <a class="m-t-5 btn btn-sm btn-danger" href="<?php echo url_for('signon/logout') ?>"><?php echo __('Log Out'); ?></a>
                      </div>
                </div>
            </div>
        </div>

        <!--Display the organisation help info from the site config (Appears on the sidebar)-->
        <?php if($apsettings){ ?>
          <div class="col-lg-4">
              <?php echo html_entity_decode($apsettings->getOrganisationSidebar()); ?>
          </div>
        <?php } ?>
      </div>

      <div class="row">
          <!--Show a list of notifications for the client-->
          <div class="col-sm-6 col-lg-8">
            <?php  include_partial('notifications') ?>
          </div>

          <!--Show a list of the latest applications-->
          <?php include_partial('latest_applications', array('latest_applications' => $latest_applications)) ?>

          <!--Display a sidebar with information from the site config-->
          <?php if($apsettings){ ?>
			<div class="col-sm-6 col-lg-4">
				<div class="card-box widget-user">
					<?php echo html_entity_decode($apsettings->getOrganisationHelp()); ?>
				</div>
			</div>
          <?php } ?>
      </div>
      <!-- end row -->

</div>
