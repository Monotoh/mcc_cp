<?php
/**
 * indexSuccess.php template.
 *
 * Displays client dashboard
 *
 * @package    frontend
 * @subpackage dashboard
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper('I18N');
//OTB patch
$otbhelper = new OTBHelper();

include_component('index', 'checksession');

//Get site config properties
$q = Doctrine_Query::create()
        ->from("ApSettings a")
        ->where("a.id = 1")
        ->orderBy("a.id DESC");
$apsettings = $q->fetchOne();
?>
<div class="contentpanel">

   

    <div class="row">
        
        <!--Show a list of the latest applications-->

<?php include_partial('inspection_applications', array('latest_applications' => $l_applications)) ?>
        <!--Display a sidebar with information from the site config-->
        <?php if ($apsettings) { ?>
            <div class="col-lg-4">
            <?php echo html_entity_decode($apsettings->getOrganisationSidebar()); ?>
               
            </div>
            <?php } ?>
    </div>
    <!-- end row -->

</div>
