<?php 
use_helper('I18N');
$otb_helper = new OTBHelper();
$application_no = $otb_helper->get_application_by_id($res->getEntryId()) ;
?>
<div class="contentpanel">
    <div class="row">

        <div class="col-sm-12">
            <div class="panel panel-danger">
                
                <div class="panel panel-body">
                    <h4 style="color: red;" class="page-title">
                        <u><strong><?php echo __('Corrections!') ?></strong><?php echo __(' Application with Reference No <b>
                            <a href=/index.php/application/view/id/'.$res->getEntryId().'>'.$application_no->getApplicationId().'</a>  has been sent back to you for Amendments. The following are the reasons:') ?>
                        </u>
                    </h4> 
                    <span>
                     <?php echo html_entity_decode($res->getDescription()) ?>
                    </span>
                    <span>
                        <a href="/index.php/dashboard" class="btn btn-primary"> <?php echo __('Back to Dashboard') ?> </a>
                    </span>
                </div>
            </div>

        </div>
     </div>
 </div>