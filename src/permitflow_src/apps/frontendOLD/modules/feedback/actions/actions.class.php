<?php
/**
 * Feedback actions.
 *
 * Displays feedback content for clients
 *
 * @package    frontend
 * @subpackage none
 * @author     OTB Africa / Boniface Irungu (boniface@otbafica.com)
 */
class feedbackActions extends sfActions
{
          
         /**
	 * Executes 'Feedback' action
	 *
	 * Displays feedback form for clients
	 *
	 * @param sfRequest $request A request object
	 */
	  public function executeFeedback(sfWebRequest $request)
	  {
		  $this->notifier = new notifications($this->getMailer());
		  $this->formid = 7; //improve on this
		  $this->setLayout("layoutdash");
	  }
}
