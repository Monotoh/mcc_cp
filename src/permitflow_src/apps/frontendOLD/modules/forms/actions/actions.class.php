<?php
/**
 * Forms actions.
 *
 * Dynamic Form Generator Components for application forms
 *
 * @package    frontend
 * @subpackage forms
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
class formsActions extends sfActions
{
    /**
     * 
     * @param sfWebRequest $request
     * OTB patch
     */
    public function executeDownloadFile(sfWebRequest $request){
    //
    $target_file = sfConfig::get("sf_data_dir")."/sent_back_attachments/".$request->getParameter('filename') ;
    error_log("Target File is ".$target_file);
    $filename_only = $request->getParameter('filename') ;
    //
    $header_file = (strstr($_SERVER['HTTP_USER_AGENT'], 'MSIE')) ? preg_replace('/\./', '%2e', $filename_only, substr_count($filename_only, '.') - 1) : $filename_only;
      //Prepare headers
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: public", false);
    header("Content-Description: File Transfer");
    header("Content-Type: " . $type);
    header("Accept-Ranges: bytes");
    header("Content-Disposition: attachment; filename=\"" . addslashes($header_file) . "\"");
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($target_file));
    exit();
  }
  /**
   * Denied Access - OTB patch 
   * Users who cleverly enable submit button with invalid UPI will be blocked from submitting
   */
  public function executeDenied(sfWebRequest $request){
      $this->upi = $request->getParameter('upi') ;
      $this->setLayout('layoutdash');
  }
    /**
     * Check if an application exists with supplied UPI
     */
    public function executeValidateUPI(sfWebRequest $request){
        //get values
        $form_id = $request->getParameter('form_id');
        $element_id = $request->getParameter('element');
        $upi = $request->getParameter('upi');
        //
        $otbhelper = new OTBHelper();
        //
        $validatity = "invalid" ;
        //search for records like upi value entered by user and //validate the format of this UPI
        if(!empty($upi) && $otbhelper->validateUPIFormat($upi)){
            
            $validatity = $otbhelper->validateUPI($upi,$form_id,$element_id) ;
            
        }
        
        echo json_encode($validatity) ;
        //error_log("Validity >>>> ".$validatity);
        exit();
    }
    /**
     * Get all elements of a form
     */
    public function executeGetNonUPIFormElements(sfWebRequest $request){
        
         $form_id = $request->getParameter('form_id');
         $query = "select element_id from ap_form_elements where form_id = $form_id and element_is_upi = 0 and element_is_hidden = 0 ;" ;
         $query_r = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($query) ;
         $results = array() ;
         foreach($query_r as $res){
             array_push($results,$res['element_id']) ;
           //  error_log("Pushing values >>> ".$res['element_id']);
             //array_pu
         }
          echo json_encode($results) ;
          exit();
    }
    /**
     * Executes 'Payment' action
     *
     * Payment form
     *
     * @param sfRequest $request A request object
     */
    public function executePayment(sfWebRequest $request)
    {
         $this->gateway = $request->getParameter('gateway');
        $this->setLayout("layoutdash");
    }

	/*OTB Start - Cash Payment Option*/
    public function executeCashpayment(sfWebRequest $request)
    {
        $this->setLayout("layoutdash");
    }
	/*OTB End - Cash Payment Option*/

    /**
     * Executes 'Confirmpayment' action
     *
     * Payment form
     *
     * @param sfRequest $request A request object
     */
    public function executeConfirmpayment(sfWebRequest $request)
    {
		//OTB Start - Combine Cash and electronic payments
		$this->manualcashpayment = $request->getParameter("manualcashpayment");
		//OTB End - Combine Cash and electronic payments
        if(empty($this->getUser()->getAttribute("invoice_id")))
        {
           $this->getUser()->setAttribute("invoice_id", $request->getParameter("invoiceid"));
        }

        $this->setLayout("layoutdashfull");
    }

    /**
     * Executes 'invalidpayment' action
     *
     * Payment form
     *
     * @param sfRequest $request A request object
     */
    public function executeInvalidpayment(sfWebRequest $request)
    {
        $this->setLayout("layoutdash");
    }


   public function executePesapalipn(sfWebRequest $request)
   {
       $this->mailer = $this->getMailer();
       $this->setLayout(false);
   }


   public function executeJambopayipn(sfWebRequest $request)
   {
       $this->setLayout(false);
   }


    /**
     * Executes 'Download' action
     *
     * Downloads a file attached to a previously submitted form
     *
     * @param sfRequest $request A request object
     */
    public function executeDownload(sfWebRequest $request)
    {
        $this->setLayout(false);
    }

    /**
     * Executes 'Edit'  action
     *
     * A linked application
     *
     * @param sfRequest $request A request object
     */
    public function executeEdit(sfWebRequest $request)
    {
		$this->setLayout("layoutdash");
    }

     /**
	 * Executes 'View' action
	 *
	 * Displays a dynamically generated application form
	 *
	 * @param sfRequest $request A request object
	 */
      public function executeView(sfWebRequest $request)
      {
	        $module = $request->getParameter('module');
            $action = $request->getParameter('action');

              $pageurl = $module."/".$action."?id=".$request->getParameter("id");

              $q = Doctrine_Query::create()
                  ->from('Content a')
                  ->where('a.published = ?', 1)
                  ->andWhere('a.url = ?', $pageurl)
                  ->orderBy('a.menu_index DESC');
              $page = $q->fetchOne();

              if(sizeof($page) <= 0)
              {
                  $pageurl = $module."/".$action."/id/".$request->getParameter("id");

                  $q = Doctrine_Query::create()
                      ->from('Content a')
                      ->where('a.published = ?', 1)
                      ->andWhere('a.url = ?', $pageurl)
                      ->orderBy('a.menu_index DESC');
                  $page = $q->fetchOne();
              }

              if(!$this->getUser()->isAuthenticated() && sizeof($page) > 0)
              {
                  if($page->getContentType() == '3')
                  {
                      $this->redirect('@sf_guard_signin');
                  }
              }
	        $this->notifier = new notifications($this->getMailer());
            $this->formid = $request->getParameter("id");
	        if($request->getParameter("invoiceid"))
	        {
		        $this->invoiceid = $request->getParameter("invoiceid");
	        }
	        if($request->getParameter("applicationid"))
	        {
		        $this->applicationid = $request->getParameter("applicationid");
	        }
			$this->setLayout("layoutdash");

    }



     /**
   * Executes 'View' action
   *
   * Displays a dynamically generated application form
   *
   * @param sfRequest $request A request object
   */
      public function executeInfo(sfWebRequest $request)
      {
          $module = $request->getParameter('module');
            $action = $request->getParameter('action');

              $pageurl = $module."/".$action."?id=".$request->getParameter("id");

              $q = Doctrine_Query::create()
                  ->from('Content a')
                  ->where('a.published = ?', 1)
                  ->andWhere('a.url = ?', $pageurl)
                  ->orderBy('a.menu_index DESC');
              $page = $q->fetchOne();

              if(sizeof($page) <= 0)
              {
                  $pageurl = $module."/".$action."/id/".$request->getParameter("id");

                  $q = Doctrine_Query::create()
                      ->from('Content a')
                      ->where('a.published = ?', 1)
                      ->andWhere('a.url = ?', $pageurl)
                      ->orderBy('a.menu_index DESC');
                  $page = $q->fetchOne();
              }

              if(!$this->getUser()->isAuthenticated() && sizeof($page) > 0)
              {
                  if($page->getContentType() == '3')
                  {
                      $this->redirect('@sf_guard_signin');
                  }
              }
          $this->notifier = new notifications($this->getMailer());
            $this->formid = $request->getParameter("id");
          if($request->getParameter("invoiceid"))
          {
            $this->invoiceid = $request->getParameter("invoiceid");
          }
          if($request->getParameter("applicationid"))
          {
            $this->applicationid = $request->getParameter("applicationid");
          }
      $this->setLayout("layoutdash");

    }

         /**
	 * Executes 'ConfirmApplication' action
	 *
	 * Displays confirmation/review page for a submitted application
	 *
	 * @param sfRequest $request A request object
	 */
          public function executeConfirmApplication(sfWebRequest $request)
          {
            $this->formid = $request->getParameter("id");
		$this->setLayout("layoutdash");
          }

         /**
	 * Executes 'ConfirmDraft' action
	 *
	 * Displays confirmation/review page for a submitted draft
	 *
	 * @param sfRequest $request A request object
	 */
          public function executeConfirmDraft(sfWebRequest $request)
          {
            $this->formid = $request->getParameter("id");
		$this->setLayout("layoutdash");
          }

         /**
	 * Executes 'Confirm' action
	 *
	 * Displays confirmation/review page for non-application/non-draft forms
	 *
	 * @param sfRequest $request A request object
	 */
          public function executeConfirm(sfWebRequest $request)
          {
            $this->formid = $request->getParameter("id");
		$this->setLayout("layoutdash");
          }

          /**
	 * Executes 'Getselect' action
	 *
	 * Ajax for application form that retrieves comboboxes
	 *
	 * @param sfRequest $request A request object
	 */
          public function executeGetselect(sfWebRequest $request)
          {
		        $this->setLayout(false);
          }

          /**
	 * Executes 'Getrelation' action
	 *
	 * Ajax for application form that fetches related fields
	 *
	 * @param sfRequest $request A request object
	 */
          public function executeGetrelation(sfWebRequest $request)
          {
		        $this->setLayout(false);
          }
}
