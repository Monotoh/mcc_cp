<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 13/01/2015
 * Time: 13:48
 */
use_helper("I18N");
?>
<script language="JavaScript">
    function update_balance()
    {
        var outstanding = document.getElementById('outstanding_amount').value;
        var partial = document.getElementById('partial_amount').value;
        var balance = outstanding - partial;
        document.getElementById('remaining_amount').value = balance;
    }
</script>

<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title"><?php echo __('Enter an amount') ?>:</h4>
        <p><?php echo __('Enter the amount you wish to pay for now. You can make partial payments for this application but you will not receive the service until all payments are received.') ?></p>
    </div>
    <form class="form-horizontal form-bordered" method="POST" action="<?php echo $_SERVER["PHP_SELF"]."?id=".$sf_user->getAttribute('form_id')."&entryid=".$sf_user->getAttribute('entry_id').($sf_user->getAttribute('invoice_id')?"&invoiceid=".$sf_user->getAttribute('invoice_id'):""); ?>">

        <div class="panel-body panel-body-nopadding">

                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo __('Amount Outstanding') ?></label>
                    <div class="col-sm-6">
                        <input type="number" id="outstanding_amount" name="outstanding_amount" placeholder="0.00" readonly="readonly" class="form-control" value="<?php echo $total_amount_remaining; ?>" max="<?php echo $total_amount_remaining; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo __('Enter amount to pay now') ?> (<?php echo $currency; ?>)</label>
                    <div class="col-sm-6">
                        <input type="number" id="partial_amount" name="partial_amount" autofocus placeholder="0.00" class="form-control" value="<?php echo $total_amount_remaining; ?>" max="<?php echo $total_amount_remaining; ?>" onkeyup="update_balance();">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo __('Balance Due') ?></label>
                    <div class="col-sm-6">
                        <input type="number" id="remaining_amount" name="remaining_amount" placeholder="0.00" class="form-control" readonly="readonly" value="0.00" max="<?php echo $total_amount_remaining; ?>">
                    </div>
                </div>

        </div><!-- panel-body -->

        <div class="panel-footer">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <button type="submit" class="btn btn-primary"><?php echo __('Submit') ?></button>&nbsp;
                </div>
            </div>
        </div><!-- panel-footer -->

    </form>

</div><!-- panel -->

