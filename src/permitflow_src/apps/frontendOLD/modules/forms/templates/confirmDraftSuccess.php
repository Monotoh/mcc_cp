<?php
    /**
     * confirmApplicationSuccess.php template.
     *
     * Displays confirmation/review page for a submitted application
     *
     * @package    frontend
     * @subpackage forms
     * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
     */
    use_helper("I18N");
    $prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";

    require($prefix_folder.'config.php');
    require($prefix_folder.'includes/db-core.php');
    require($prefix_folder.'includes/helper-functions.php');

    require($prefix_folder.'includes/language.php');
    require($prefix_folder.'includes/common-validator.php');
    require($prefix_folder.'includes/view-functions.php');
    require($prefix_folder.'includes/theme-functions.php');
    require($prefix_folder.'includes/post-functions.php');
    require($prefix_folder.'includes/entry-functions.php');
    require($prefix_folder.'hooks/custom_hooks.php');

    //get data from database
    $dbh 		= mf_connect_db();
    $ssl_suffix = mf_get_ssl_suffix();


    $form_id    = (int) trim($_REQUEST['id']);

    if(!empty($_POST['review_submit']) || !empty($_POST['review_submit_x'])){ //if form submitted

        //commit data from review table to actual table
        //however, we need to check if this form has payment enabled or not

        //if the form doesn't have any payment enabled, continue with commit and redirect to success page
        $form_properties = mf_get_form_properties($dbh,$form_id,array('payment_enable_merchant','payment_delay_notifications','payment_merchant_type'));

        if($form_properties['payment_enable_merchant'] != 1){
            $record_id 	   = $_SESSION['review_id'];
            $commit_result = mf_commit_form_review($dbh,$form_id,$record_id);

            unset($_SESSION['review_id']);
            
            error_log("Attempting redirect for record id = ".$record_id);

            header("Location: http{$ssl_suffix}://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."?id={$form_id}&entryid={$commit_result['record_insert_id']}&done=1");
            echo "<script type=\"text/javascript\">top.location.replace('http{$ssl_suffix}://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."?id={$form_id}&entryid={$commit_result['record_insert_id']}&done=1')</script>";
            exit;
        }else{
            //if the form has payment enabled, continue commit and redirect to payment page
            $record_id 	    = $_SESSION['review_id'];
            $commit_options = array();

            //delay notifications only available on stripe
            if(!empty($form_properties['payment_delay_notifications']) && $form_properties['payment_merchant_type'] == 'stripe'){
                $commit_options['send_notification'] = false;
            }

            $commit_result = mf_commit_form_review($dbh,$form_id,$record_id,$commit_options);

            unset($_SESSION['review_id']);

            
            header("Location: http{$ssl_suffix}://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."?id={$form_id}&entryid={$commit_result['record_insert_id']}&done=1");

            echo "<script type=\"text/javascript\">top.location.replace('http{$ssl_suffix}://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."?id={$form_id}&entryid={$commit_result['record_insert_id']}&done=1')</script>";
            exit;

        }

    }elseif (!empty($_POST['review_back']) || !empty($_POST['review_back_x'])){
        //go back to form
        $origin_page_num = (int) $_POST['mf_page_from'];
        header("Location: http{$ssl_suffix}://".$_SERVER['HTTP_HOST'].mf_get_dirname($_SERVER['PHP_SELF'])."/view?id={$form_id}&mf_page={$origin_page_num}");
        exit;
    }else{

        if(empty($form_id)){
            die('ID required.');
        }

        if(!empty($_GET['done']) && !empty($_SESSION['mf_form_completed'][$form_id])){

            //We will use the application manager to create new applications or drafts from form submissions
            $application_manager = new ApplicationManager();

            //Check if an application already exists for the form submission to prevent double entry
            if($application_manager->application_exists($form_id, $_GET['entryid'])) {
                    //If save as draft/resume later was clicked then do nothing
                    $submission = $application_manager->get_application($form_id, $_GET['entryid']);
            }
            else {
                    //If save as draft/resume later was clicked then create draft application
                    $submission = $application_manager->create_application($form_id, $_GET['entryid'], $sf_user->getGuardUser()->getId(), true);
            }

            $markup = '
<div class="panel panel-dark">
    <div class="panel-body">
	<div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <p>Saved as Draft! Your application has been saved as a Draft. To complete your application process at a later time, go to My Drafts from your dashboard, view your application and click on Edit and Submit.</p>
        </div>
    </div>
</div>
';

        }else{
            if(empty($_SESSION['review_id'])){
                die("Your session has been expired. Please <a href='view?id={$form_id}'>click here</a> to start again.");
            }else{
                $record_id = $_SESSION['review_id'];
            }

            $from_page_num = (int) $_GET['mf_page_from'];
            if(empty($from_page_num)){
                $form_page_num = 1;
            }

            if($_GET['draft'])
            {
                $params['draft'] = true;
                $markup = mf_display_form_review($dbh,$form_id,$record_id,$from_page_num,$params);
            }
            else
            {
                $markup = mf_display_form_review($dbh,$form_id,$record_id,$from_page_num);
            }
        }
    }
?>
<div class="pageheader">
       <h2><i class="fa fa-edit"></i><?php echo __('Application Form') ?><span> <?php echo __('Submit an Application') ?></span></h2>
      <div class="breadcrumb-wrapper">
        
        <ol class="breadcrumb">
          <li><a href=""><?php echo __('Application Form') ?></a></li>
          <li class="active"><?php echo $form_name; ?></li>
        </ol>
      </div>
    </div>      


<div class="contentpanel">
<div class="row">

                                <?php
    header("Content-Type: text/html; charset=UTF-8");
    echo $markup;

    ?>
    
    
</div><!-- /.row -->
</div><!-- /.marketing -->


