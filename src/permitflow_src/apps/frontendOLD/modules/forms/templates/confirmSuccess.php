<?php
/**
 * confirmSuccess.php template.
 *
 * Displays confirmation/review page for non-application/non-draft forms
 *
 * @package    frontend
 * @subpackage forms
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");
 ?>
<?php
	$prefix_folder = dirname(__FILE__)."/../../../../..";
	require($prefix_folder.'/lib/vendor/cp_form/config.php');
	$prefix_folder = dirname(__FILE__)."/../../../../..";
	require($prefix_folder.'/lib/vendor/cp_form/includes/language.php');
	require($prefix_folder.'/lib/vendor/cp_form/includes/db-core.php');
	require($prefix_folder.'/lib/vendor/cp_form/includes/common-validator.php');
	require($prefix_folder.'/lib/vendor/cp_form/includes/view-functions.php');
	require($prefix_folder.'/lib/vendor/cp_form/includes/post-functions.php');
	$prefix_folder = dirname(__FILE__)."/../../../../..";
	require($prefix_folder.'/lib/vendor/cp_form/includes/helper-functions.php');
	require($prefix_folder.'/lib/vendor/cp_form/includes/entry-functions.php');
	require($prefix_folder.'/lib/vendor/cp_form/lib/class.phpmailer.php');
		
	//get data from database
	connect_db();
	
	$form_id   = (int) trim($formid);
	
	if(!empty($_POST['review_submit'])){ //if form submitted
		
		//commit data from review table to actual table
		$record_id 	   = $_SESSION['review_id'];
		$options['userid'] = $sf_user->getGuardUser()->getId();
		$commit_result = commit_form_review($form_id,$record_id,$options);
		
		unset($_SESSION['review_id']);
		
		if(empty($commit_result['form_redirect'])){
			$ssl_suffix = get_ssl_suffix();
			
			header("Location: http{$ssl_suffix}://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."?id={$form_id}&done=1");
			exit;
		}else{
			
			echo "<script type=\"text/javascript\">top.location.replace('{$commit_result['form_redirect']}')</script>";
			exit;
		}
		
	}elseif (!empty($_POST['review_back'])){ 
		//go back to form
		$ssl_suffix = get_ssl_suffix();
		header("Location: http{$ssl_suffix}://".$_SERVER['HTTP_HOST'].get_dirname($_SERVER['PHP_SELF'])."/view?id={$form_id}");
		exit;
	}else{
				
		if(empty($form_id)){
			die('ID required.');
		}
		
		if(!empty($_GET['done'])){
			$markup = display_success($form_id);
		}else{
			if(empty($_SESSION['review_id'])){
				die("Your session has been expired. Please <a href='/index.php/forms/view?id={$form_id}'>click here</a> to start again.");
			}else{
				$record_id = $_SESSION['review_id'];
			}
			$markup = display_form_review($form_id,$record_id);
		}
	}
	
	header("Content-Type: text/html; charset=UTF-8");
	
	$q = Doctrine_Query::create()
	  ->from('ApForms a')
	  ->where('a.form_id = ?', $form_id);
	$formObj = $q->fetchOne();
	$form_name = $formObj->getFormName();
	$form_description = $formObj->getFormDescription();
?>
<div class="pageheader">
       <h2><i class="fa fa-edit"></i><?php echo __('Application Form') ?><span> <?php echo __('Submit an Application') ?></span></h2>
      <div class="breadcrumb-wrapper">
        
        <ol class="breadcrumb">
          <li><a href=""><?php echo __('Application Form') ?></a></li>
          <li class="active"><?php echo $form_name; ?></li>
        </ol>
      </div>
    </div>      

<div class="contentpanel">
<div class="row">

                    <?php
	echo $markup;
    ?>
    
</div><!-- /.row -->
</div><!-- /.marketing -->               
