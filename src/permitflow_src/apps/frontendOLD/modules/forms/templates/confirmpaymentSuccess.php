<?php

$prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";
include_once($prefix_folder.'includes/OAuth.php');

require($prefix_folder.'config.php');
require($prefix_folder.'includes/language.php');
require($prefix_folder.'includes/db-core.php');
require($prefix_folder.'includes/common-validator.php');
require($prefix_folder.'includes/view-functions.php');
require($prefix_folder.'includes/post-functions.php');
require($prefix_folder.'includes/filter-functions.php');
require($prefix_folder.'includes/entry-functions.php');
require($prefix_folder.'includes/helper-functions.php');
require($prefix_folder.'includes/theme-functions.php');
require($prefix_folder.'lib/recaptchalib.php');
require($prefix_folder.'lib/php-captcha/php-captcha.inc.php');
require($prefix_folder.'lib/text-captcha.php');
require($prefix_folder.'hooks/custom_hooks.php');


$dbh 		= mf_connect_db();

$mf_settings = mf_get_settings($dbh);

$invoice_manager = new InvoiceManager();

//get form properties data
$query 	= "select
          form_name,
          form_has_css,
          form_redirect,
          form_language,
          form_review,
          form_review_primary_text,
          form_review_secondary_text,
          form_review_primary_img,
          form_review_secondary_img,
          form_review_use_image,
          form_review_title,
          form_review_description,
          form_resume_enable,
          form_page_total,
          form_lastpage_title,
          form_pagination_type,
          form_theme_id,
          payment_show_total,
          payment_total_location,
          payment_enable_merchant,
          payment_merchant_type,
          payment_currency,
          payment_price_type,
          payment_price_name,
          payment_price_amount,
          payment_ask_billing,
          payment_ask_shipping,
          payment_pesapal_live_secret_key,
          payment_pesapal_live_public_key,
          payment_pesapal_test_secret_key,
          payment_pesapal_test_public_key,
          payment_pesapal_enable_test_mode,
          payment_jambopay_business,
          payment_jambopay_shared_key,
          payment_enable_recurring,
          payment_recurring_cycle,
          payment_recurring_unit,
          payment_enable_trial,
          payment_trial_period,
          payment_trial_unit,
          payment_trial_amount,
          payment_delay_notifications,
          payment_enable_tax,
          payment_tax_rate,
          payment_tax_amount
         from
            ".MF_TABLE_PREFIX."forms
        where
           form_id = ?";
$params = array($_GET['id']);

$sth = mf_do_query($query,$params,$dbh);
$rowtop = mf_do_fetch_result($sth);


//OTB patch - Check if we have an override of the payment_merchant_type in the mf_invoice table for this invoice
$gateway_to_use = $rowtop['payment_merchant_type'] ;
$otbhelper = new OTBHelper() ;
if($otbhelper->invoicePermitsMultiplePayment($sf_user->getAttribute('invoice_id')) == 1){
    //allow for manual>>>
    error_log("allow for manual >> ".$otbhelper->invoicePermitsMultiplePayment($sf_user->getAttribute('invoice_id')));
    $gateway_to_use = 'cash' ; //improve me
}else {
    //do nothing. use available merchant...
    error_log("Do nothing >>>>> ".$otbhelper->invoicePermitsMultiplePayment($sf_user->getAttribute('invoice_id'))) ;
}
error_log("Gateway to use ".$gateway_to_use);
//if($rowtop['payment_merchant_type'] == "pesapal")
if($gateway_to_use == "pesapal") //OTB Engineers
{

    $consumer_key = "";
    $consumer_secret = "";

    $statusrequestAPI = "";

    if($rowtop['payment_pesapal_enable_test_mode'])
    {
    $consumer_key=$rowtop['payment_pesapal_test_secret_key'];//Register a merchant account on
                       //demo.pesapal.com and use the merchant key for testing.
                       //When you are ready to go live make sure you change the key to the live account
                       //registered on www.pesapal.com!
    $consumer_secret=$rowtop['payment_pesapal_test_public_key'];// Use the secret from your test
                       //account on demo.pesapal.com. When you are ready to go live make sure you
                       //change the secret to the live account registered on www.pesapal.com!
        $statusrequestAPI = 'https://demo.pesapal.com/api/querypaymentstatus';
    }
    else
    {
    $consumer_key=$rowtop['payment_pesapal_live_secret_key'];//Register a merchant account on
                       //demo.pesapal.com and use the merchant key for testing.
                       //When you are ready to go live make sure you change the key to the live account
                       //registered on www.pesapal.com!
    $consumer_secret=$rowtop['payment_pesapal_live_public_key'];// Use the secret from your test
                       //account on demo.pesapal.com. When you are ready to go live make sure you
                       //change the secret to the live account registered on www.pesapal.com!
        $statusrequestAPI = 'https://www.pesapal.com/api/querypaymentstatus';
    }

    // Parameters sent to you by PesaPal IPN
    $pesapalTrackingId=$_GET['pesapal_transaction_tracking_id'];
    $pesapal_merchant_reference=$_GET['pesapal_merchant_reference'];

    if($pesapalTrackingId!='')
    {
       error_log('EC_LOG: Pesapal trannsaction id: '.$pesapalTrackingId);

       $token = $params = NULL;
       $consumer = new OAuthConsumer($consumer_key, $consumer_secret);
       $signature_method = new OAuthSignatureMethod_HMAC_SHA1();

       //get transaction status
       $request_status = OAuthRequest::from_consumer_and_token($consumer, $token, "GET", $statusrequestAPI, $params);
       $request_status->set_parameter("pesapal_merchant_reference", $pesapal_merchant_reference);
       $request_status->set_parameter("pesapal_transaction_tracking_id",$pesapalTrackingId);
       $request_status->sign_request($signature_method, $consumer, $token);


       $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, $request_status);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
       curl_setopt($ch, CURLOPT_HEADER, 1);
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
       if(defined('CURL_PROXY_REQUIRED')) if (CURL_PROXY_REQUIRED == 'True')
       {
          $proxy_tunnel_flag = (defined('CURL_PROXY_TUNNEL_FLAG') && strtoupper(CURL_PROXY_TUNNEL_FLAG) == 'FALSE') ? false : true;
          curl_setopt ($ch, CURLOPT_HTTPPROXYTUNNEL, $proxy_tunnel_flag);
          curl_setopt ($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
          curl_setopt ($ch, CURLOPT_PROXY, CURL_PROXY_SERVER_DETAILS);
       }

       $response = curl_exec($ch);

       error_Log("EC_LOG: Confirm payment response: ".$response);

       $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
       $raw_header  = substr($response, 0, $header_size - 4);
       $headerArray = explode("\r\n\r\n", $raw_header);
       $header      = $headerArray[count($headerArray) - 1];

       //transaction status
       $elements = preg_split("/=/",substr($response, $header_size));
       $status = $elements[1];

       curl_close ($ch);

       //UPDATE YOUR DB TABLE WITH NEW STATUS FOR TRANSACTION WITH pesapal_transaction_tracking_id $pesapalTrackingId


       if($use_debug_mode){
         echo 'Valid IPN';
       }

       $error_message = '';


       //parse the "custom" variable and make sure it's a valid entry within the database
       $exploded = explode('/', $pesapal_merchant_reference); //the "custom" variable from Pesapal format: xx_yy_zzzzzzzz (xx: form_id, yy: entry_id, zzz: unix_timestamp of the date_created field)
       $form_id  		 = (int) $exploded[0];
       $entry_id 		 = $exploded[1];
       $entry_timestamp = date('Y/m/d');

       //$query = "select count(`id`) record_exist from ".MF_TABLE_PREFIX."form_{$form_id} where unix_timestamp(date_created) = ? and `id` = ? and `status` = 1";
       //$params = array($entry_timestamp,$entry_id);

       $query = "select count(`id`) record_exist from ".MF_TABLE_PREFIX."form_{$form_id} where `id` = ? and `status` = 1";
       $params = array($entry_id);

       $sth = mf_do_query($query,$params,$dbh);
       $row = mf_do_fetch_result($sth);

       if(empty($row['record_exist'])){
         $error_message .= "Invalid custom parameter: {$entry_id}";
       }

       $query 	= "select
                payment_currency,
                payment_price_type,
                payment_price_amount,
                payment_enable_tax,
                payment_tax_rate,
                payment_tax_amount
                from
                   `".MF_TABLE_PREFIX."forms`
               where
                  form_id=?";

       $params = array($form_id);

       $sth = mf_do_query($query,$params,$dbh);
       $row = mf_do_fetch_result($sth);

       $payment_currency 	  = $row['payment_currency'];
       $payment_price_type   = $row['payment_price_type'];
       $payment_price_amount = (float) $row['payment_price_amount'];

       $payment_enable_tax = (int) $row['payment_enable_tax'];
       $payment_tax_rate 	= (float) $row['payment_tax_rate'];
       $payment_tax_amount 	= (float) $row['payment_tax_amount'];

       //make sure the currency match
       //if(strtolower($payment_currency) != strtolower($_POST['mc_currency'])){
       //  $error_message .= "PesaPal currency does not match. Current: {$payment_currency}: - mc_currency: //{$_POST['mc_currency']}";
       //}

       $payment_amount = 0;

       //make sure the amount paid match or larger
       if($payment_price_type == 'variable'){
         $payment_amount = (double) mf_get_payment_total($dbh,$form_id,$entry_id,0,'live');
       }else if($payment_price_type == 'fixed'){
         $payment_amount = (double) $payment_price_amount;
       }

       //calculate tax if enabled
       if(!empty($payment_enable_tax)){
         if($payment_tax_amount)
         {
           $payment_amount += $payment_tax_amount;
         }
         else
         {
           $payment_tax_amount = ($payment_tax_rate / 100) * $payment_amount;
           $payment_tax_amount = round($payment_tax_amount,2); //round to 2 digits decimal
           $payment_amount += $payment_tax_amount;
         }
       }

       if($_SESSION['invoice'])
       {
           $q = Doctrine_Query::create()
              ->from('MfInvoice a')
              ->where('a.id = ?', $_SESSION['invoice']);
           $invoice = $q->fetchOne();

           if($invoice)
           {
            $payment_amount = $invoice->getTotalAmount();
           }
       }


       //$gross_payment = (double) $_POST['mc_gross'];
       //if($gross_payment < $payment_amount) {
       //  $error_message .= "Gross amount does not match. Amount: {$payment_amount} - mc_gross: {$gross_payment}";
       //}

       //if there is any error, log and exit
       if(!empty($error_message)){
         echo $error_message;
         echo $listener->getTextReport();
         exit;
       }else{

         if($use_debug_mode){
           error_log('EC_LOG: Verification completed. Update/insert into table');
         }

         //otherwise update/insert into ap_form_payments table with the completed status
         $query = "select count(afp_id) record_exist from ".MF_TABLE_PREFIX."form_payments where form_id = ? and record_id = ? and `status` = 1";
         $params = array($form_id,$entry_id);
         $sth = mf_do_query($query,$params,$dbh);
         $row = mf_do_fetch_result($sth);

         $payment_status = 'paid';

         if(!empty($row['record_exist'])){
           if($use_debug_mode){
             error_log('EC_LOG: Updating form_payments table');
           }

           //do update to ap_form_payments table
           $query = "update ".MF_TABLE_PREFIX."form_payments set payment_status = ? where form_id = ? and record_id = ? and `status` = 1";
           $params = array($payment_status,$form_id,$entry_id);
           mf_do_query($query,$params,$dbh);

           if($use_debug_mode){
             error_log('EC_LOG: Done updating form_payments table');
           }

         }else{

           if($use_debug_mode){
             error_log('EC_LOG: Inserting to form_payments table');
           }
           //do insert to ap_form_payments table
           //insert into ap_form_payments table
           $query = "INSERT INTO `".MF_TABLE_PREFIX."form_payments`(
                       `form_id`,
                       `record_id`,
                       `billing_state`,
                       `payment_id`,
                       `date_created`,
                       `payment_date`,
                       `payment_status`,
                       `payment_fullname`,
                       `payment_amount`,
                       `payment_currency`,
                       `payment_test_mode`,
                       `payment_merchant_type`,
                       `status`
                       )
                   VALUES (
                       :form_id,
                       :record_id,
                       :billing_state,
                       :payment_id,
                       :date_created,
                       :payment_date,
                       :payment_status,
                       :payment_fullname,
                       :payment_amount,
                       :payment_currency,
                       :payment_test_mode,
                       :payment_merchant_type,
                       :status
                       )";


           $params = array();
           $params[':form_id'] 		  	= $form_id;
           $params[':record_id'] 			= $entry_id;
           $params[':billing_state'] = $_GET['pesapal_transaction_tracking_id'];
           $params[':payment_id'] 			= $_GET['pesapal_merchant_reference'];
           $params[':date_created'] 		= date("Y-m-d H:i:s");
           $params[':payment_date'] 		= date("Y-m-d H:i:s");
           $params[':payment_status'] 		= 'pending confirmation';
           $params[':payment_fullname']  	= trim($_SESSION['first_name'].' '.$_SESSION['last_name']);
           $params[':payment_amount'] 	  	= $payment_amount;
           $params[':payment_currency']  	= $payment_currency;

           if($use_paypal_sandbox){
             $params[':payment_test_mode'] 	= 1;
           }else{
             $params[':payment_test_mode'] 	= 0;
           }

           $params[':payment_merchant_type'] = 'pesapal';
           $params[':status'] 			  	  = 1;

           mf_do_query($query,$params,$dbh);

           if($use_debug_mode){
             error_log('EC_LOG: Done inserting to form_payments table');
           }
         }

         if($_REQUEST['invoiceid'])
         {
            $q = Doctrine_Query::create()
               ->from('MfInvoice a')
               ->where('a.id = ?', $_REQUEST['invoiceid']);
            $invoice = $q->fetchOne();
            if($invoice)
            {
               $invoice->setPaid(15);
               $invoice->setUpdatedAt(date("Y-m-d"));
               $invoice->save();
            }

            $q = Doctrine_Query::create()
               ->from('FormEntry a')
               ->where('a.form_id = ? AND a.entry_id = ?', array($form_id,$entry_id));
            $application = $q->fetchOne();
            if($application)
            {
              $_SESSION['payment_updated'] = true;
              header("Location: /index.php/application/view/id/".$application->getId());
                ?>
                You are being redirected...

                <a href="/index.php/application/view/id=<?php echo $application->getId(); ?>">Click the link here to continue.</a>
                <script language="javascript">
                    window.location = "/index.php/application/view/id=<?php echo $application->getId(); ?>";
                </script>
                <?php
                exit;
            }
            else
            {
              $_SESSION['skip_external_approval'] = true;
              header("Location: /index.php/forms/view?id=".$form_id."&entryid=".$entry_id."&done=1");
                ?>
                You are being redirected...

                <a href="/index.php/forms/view?id=<?php echo $form_id; ?>&entryid=<?php echo $entry_id; ?>&done=1">Click the link here to continue.</a>
                <script language="javascript">
                    window.location = "/index.php/forms/view?id=<?php echo $form_id; ?>&entryid=<?php echo $entry_id; ?>&done=1";
                </script>
                <?php
                exit;
            }
         }
         else
         {
            $_SESSION['skip_external_approval'] = true;
            header("Location: /index.php/forms/view?id=".$form_id."&entryid=".$entry_id."&done=1");
             ?>
             You are being redirected...

             <a href="/index.php/forms/view?id=<?php echo $form_id; ?>&entryid=<?php echo $entry_id; ?>&done=1">Click the link here to continue.</a>
             <script language="javascript">
                 window.location = "/index.php/forms/view?id=<?php echo $form_id; ?>&entryid=<?php echo $entry_id; ?>&done=1";
             </script>
             <?php
             exit;
         }

       } //end update/insert into ap_form_payments

        error_log('EC_LOG: Confirm payment API Success');

        $resp="pesapal_notification_type=$pesapalNotification&pesapal_transaction_tracking_id=$pesapalTrackingId&pesapal_merchant_reference=$pesapal_merchant_reference";
        ob_start();
        echo $resp;
        ob_flush();
        exit;
    }
    else
    {
       error_log('EC_LOG: Missing pesapal trannsaction id');
    }
}

elseif($gateway_to_use == "cellulant") // OTB engineers
//elseif($rowtop['payment_merchant_type'] == "cellulant")
{
    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    error_log('Cellulant callback bonie: '.$actual_link);

    $form_id = $_GET['id'];
    $entry_id = $_GET['entryid'];

    if($_REQUEST['invoiceid'])
    {
        error_log("Test Invoice >>>>>>> ".$_REQUEST['invoiceid']);
        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.form_id = ? AND a.entry_id = ?', array($form_id,$entry_id));
        $application = $q->fetchOne();
        if($application)
        {
            $_SESSION['payment_updated'] = true;
            header("Location: /index.php/application/view/id/".$application->getId());
            ?>
            You are being redirected...

            <a href="/index.php/forms/view?id=<?php echo $form_id; ?>&entryid=<?php echo $entry_id; ?>&done=1">Click the link here to continue.</a>
            <script language="javascript">
                window.location = "/index.php/application/view/id=<?php echo $application->getId(); ?>";
            </script>
            <?php

            foreach($application->getMfInvoice() as $invoice)
            {
                $query = "INSERT INTO `".MF_TABLE_PREFIX."form_payments`(
                       `form_id`,
                       `record_id`,
                       `billing_state`,
                       `payment_id`,
                       `date_created`,
                       `payment_date`,
                       `payment_status`,
                       `payment_fullname`,
                       `payment_amount`,
                       `payment_currency`,
                       `payment_test_mode`,
                       `payment_merchant_type`,
                       `status`
                       )
                   VALUES (
                       :form_id,
                       :record_id,
                       :billing_state,
                       :payment_id,
                       :date_created,
                       :payment_date,
                       :payment_status,
                       :payment_fullname,
                       :payment_amount,
                       :payment_currency,
                       :payment_test_mode,
                       :payment_merchant_type,
                       :status
                       )";


                $params = array();
                $params[':form_id'] 		  	= $form_id;
                $params[':record_id'] 			= $entry_id;
                $params[':billing_state'] = $_GET['cellulantTransactionID'];
                $params[':billing_zipcode'] = $_GET['invoiceNumber'];
                $params[':payment_id'] 			= $form_id."/".$entry_id."/".$invoice->getId();
                $params[':date_created'] 		= date("Y-m-d H:i:s");
                $params[':payment_date'] 		= date("Y-m-d H:i:s");

                if($_GET['status'] == 253)
                {
                    $params[':payment_status'] 		= 'paid';
                }
                else if($_GET['status'] == 251)
                {
                    $params[':payment_status'] 		= 'failed';
                }
                else
                {
                    $params[':payment_status'] 		= 'pending confirmation';
                }

                $params[':payment_fullname']  	= trim($_SESSION['first_name'].' '.$_SESSION['last_name']);

                if($_SESSION['partial_amount']) {
                    $params[':payment_amount'] = $_SESSION['partial_amount'];
                }
                else
                {
                    $params[':payment_amount'] = $invoice->getTotalAmount();
                }

                $params[':payment_currency']  	= $invoice->getCurrency();

                if($use_paypal_sandbox){
                    $params[':payment_test_mode'] 	= 1;
                }else{
                    $params[':payment_test_mode'] 	= 0;
                }

                $params[':payment_merchant_type'] = 'cellulant';
                $params[':status'] 			  	  = 1;

                mf_do_query($query,$params,$dbh);


                if($_GET['status'] == 253 && $invoice_manager->get_invoice_total_owed($invoice->getId()) <= 0)
                {
                    $invoice->setPaid(2);
                    $invoice->setUpdatedAt(date("Y-m-d"));
                    $invoice->save();
                }
                else if($_GET['status'] == 251)
                {
                    $invoice->setPaid(1);
                    $invoice->setUpdatedAt(date("Y-m-d"));
                    $invoice->save();
                }
                else
                {
                    $invoice->setPaid(15);
                    $invoice->setUpdatedAt(date("Y-m-d"));
                    $invoice->save();
                }

            }

            exit;
        }
    }
    else
    {
        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.form_id = ? AND a.entry_id = ?', array($form_id,$entry_id));
        $application = $q->fetchOne();
        if($application)
        {
            //approve invoice
            if(sizeof($application->getMfInvoice()) == 0)
            {
              $q = Doctrine_Query::create()
                  ->from('Invoicetemplates a')
                  ->where("a.applicationform = ?", $application->getFormId());
              $invoicetemplate = $q->fetchOne();

              $invoice = new MfInvoice();
              $invoice->setAppId($application->getId());

              if($invoicetemplate->getInvoiceNumber())
              {
                  $q = Doctrine_Query::create()
                      ->from('MfInvoice a')
                      ->where("a.template_id = ?", $invoicetemplate->getId())
                      ->orderBy("a.id DESC");
                  $lastinvoice = $q->fetchOne();

                  if($lastinvoice)
                  {
                      $invoice_number = $lastinvoice->getInvoiceNumber();
                      $invoice_number++;
                      $invoice->setInvoiceNumber($invoice_number);
                  }
                  else
                  {
                      $invoice->setInvoiceNumber($invoicetemplate->getInvoiceNumber());
                  }

                  $invoice->setTemplateId($invoicetemplate->getId());
              }
              else
              {
                  $invoice->setInvoiceNumber("INV-".$application->getId());
              }

              $invoice->setPaid(15);
              $invoice->setCreatedAt(date("Y-m-d H:i:s"));
              $invoice->setUpdatedAt(date("Y-m-d H:i:s"));

              $invoice->save();

              //Get total paid and seperate service fee from amount
              $grand_total = 0;
              $total_amount = 0;
              $service_fee = 0;

              $query  = "select
                      form_code,
                      payment_currency,
                      payment_price_type,
                      payment_price_amount,
                      payment_enable_tax,
                      payment_tax_rate,
                      payment_tax_amount
                      from
                         `".MF_TABLE_PREFIX."forms`
                     where
                        form_id=?";

             $params = array($_GET['id']);

             $sth = mf_do_query($query,$params,$dbh);
             $row = mf_do_fetch_result($sth);

             $payment_currency      = $row['payment_currency'];
             $payment_price_type   = $row['payment_price_type'];
             $payment_price_amount = (float) $row['payment_price_amount'];

             $payment_enable_tax = (int) $row['payment_enable_tax'];
             $payment_tax_rate    = (float) $row['payment_tax_rate'];
             $payment_tax_amount  = (float) $row['payment_tax_amount'];

             $service_code = $row['form_code'];


             if(!empty($payment_enable_tax)){
               if($payment_tax_amount)
               {
                 $service_fee = $payment_tax_amount;
                 $total_amount = $total_amount - $service_fee;
               }
               else
               {
                 $before_total = ($total_amount * 100) / (100 + $payment_tax_rate);
                 $service_fee = round(($total_amount - $before_total),2); //round to 2 digits decimal
                 $total_amount = $before_total;
               }
             }

             if($service_fee > 0)
             {

              $invoicedetail = new MfInvoiceDetail();
              $invoicedetail->setInvoiceId($invoice->getId());
              $invoicedetail->setDescription("Convenience fee");
              $invoicedetail->setAmount($service_fee);
              $invoicedetail->setCreatedAt(date("Y-m-d H:i:s"));
              $invoicedetail->setUpdatedAt(date("Y-m-d H:i:s"));
              $invoicedetail->save();
              $grand_total += $service_fee;
             }

              $invoicedetail = new MfInvoiceDetail();
              $invoicedetail->setInvoiceId($invoice->getId());
              $invoicedetail->setDescription($form->getFormName()." submission fee");
              $invoicedetail->setAmount($total_amount);
              $invoicedetail->setCreatedAt(date("Y-m-d H:i:s"));
              $invoicedetail->setUpdatedAt(date("Y-m-d H:i:s"));
              $invoicedetail->save();
              $grand_total += $total_amount;

              $invoice->setMdaCode(sfConfig::get('app_mda_code'));
              $invoice->setBranch(sfConfig::get('app_branch'));

              $due_date = date("Y-m-d");
              $expires_at = date("Y-m-d");

              if($invoicetemplate->getMaxDuration())
              {
                  $date = strtotime("+".$invoicetemplate->getMaxDuration()." day", time());
                  $expires_at = date('Y-m-d', $date);
              }

              if($invoicetemplate->getDueDuration())
              {
                  $date = strtotime("+".$invoicetemplate->getDueDuration()." day", time());
                  $expires_at = date('Y-m-d', $date);
              }

              $invoice->setDueDate($due_date);
              $invoice->setExpiresAt($expires_at);
              $invoice->setPayerId($sf_user->getGuardUser()->getUsername());
              $invoice->setPayerName($sf_user->getGuardUser()->getProfile()->getFullname());
              $invoice->setDocRefNumber($application->getApplicationId());
              $invoice->setCurrency($payment_currency);
              $invoice->setServiceCode($service_code);
              $invoice->setTotalAmount($grand_total);
              $invoice->save();
            }

            foreach($application->getMfInvoice() as $invoice)
            {
                  $query = "INSERT INTO `".MF_TABLE_PREFIX."form_payments`(
                         `form_id`,
                         `record_id`,
                         `billing_state`,
                         `payment_id`,
                         `date_created`,
                         `payment_date`,
                         `payment_status`,
                         `payment_fullname`,
                         `payment_amount`,
                         `payment_currency`,
                         `payment_test_mode`,
                         `payment_merchant_type`,
                         `status`
                         )
                     VALUES (
                         :form_id,
                         :record_id,
                         :billing_state,
                         :payment_id,
                         :date_created,
                         :payment_date,
                         :payment_status,
                         :payment_fullname,
                         :payment_amount,
                         :payment_currency,
                         :payment_test_mode,
                         :payment_merchant_type,
                         :status
                         )";


                  $params = array();
                  $params[':form_id']         = $form_id;
                  $params[':record_id']       = $entry_id;
                  $params[':billing_state'] = $_GET['cellulantTransactionID'];
                  $params[':payment_id']      = $form_id."/".$entry_id."/1";
                  $params[':date_created']    = date("Y-m-d H:i:s");
                  $params[':payment_date']    = date("Y-m-d H:i:s");

                  if($_GET['status'] == 253)
                  {
                      $params[':payment_status']    = 'paid';
                  }
                  else if($_GET['status'] == 251)
                  {
                      $params[':payment_status']    = 'failed';
                  }
                  else
                  {
                      $params[':payment_status']    = 'pending confirmation';
                  }

                  $params[':payment_fullname']    = trim($_SESSION['first_name'].' '.$_SESSION['last_name']);

                if($_SESSION['partial_amount']) {
                    $params[':payment_amount'] = $_SESSION['partial_amount'];
                }
                else
                {
                    $params[':payment_amount'] = $invoice->getTotalAmount();
                }

                  $params[':payment_currency']    = $invoice->getCurrency();

                  if($use_paypal_sandbox){
                      $params[':payment_test_mode']   = 1;
                  }else{
                      $params[':payment_test_mode']   = 0;
                  }

                  $params[':payment_merchant_type'] = 'cellulant';
                  $params[':status']            = 1;

                  mf_do_query($query,$params,$dbh);

                if($_GET['status'] == 253 && $invoice_manager->get_invoice_total_owed($invoice->getId()) <= 0)
                {
                    $invoice->setPaid(2);
                    $invoice->setUpdatedAt(date("Y-m-d"));
                    $invoice->save();
                }
                else if($_GET['status'] == 251)
                {
                    $invoice->setPaid(1);
                    $invoice->setUpdatedAt(date("Y-m-d"));
                    $invoice->save();
                }
                else
                {
                    $invoice->setPaid(15);
                    $invoice->setUpdatedAt(date("Y-m-d"));
                    $invoice->save();
                }

            }
        }

        header("Location: /index.php/forms/view?id=".$form_id."&entryid=".$entry_id."&done=1");
        ?>
        You are being redirected...

        <a href="/index.php/forms/view?id=<?php echo $form_id; ?>&entryid=<?php echo $entry_id; ?>&done=1">Click the link here to continue.</a>
        <script language="javascript">
            window.location = "/index.php/forms/view?id=<?php echo $form_id; ?>&entryid=<?php echo $entry_id; ?>&done=1";
        </script>
        <?php
        exit;
    }
}
elseif($gateway_to_use == "ecitizen") //OTB engineers
//elseif($rowtop['payment_merchant_type'] == "ecitizen")
{
    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    error_log('ecitizen callback: '.$actual_link);

    $form_id = $_GET['id'];
    $entry_id = $_GET['entryid'];

    if($_REQUEST['invoiceid'])
    {
        error_log('ecitizen invoice id >>>>> '.$_REQUEST['invoiceid']) ;
        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.form_id = ? AND a.entry_id = ?', array($form_id,$entry_id));
        $application = $q->fetchOne();
        if($application)
        {

            foreach($application->getMfInvoice() as $invoice)
            {
                $query = "INSERT INTO `".MF_TABLE_PREFIX."form_payments`(
                       `form_id`,
                       `record_id`,
                       `billing_state`,
                       `payment_id`,
                       `date_created`,
                       `payment_date`,
                       `payment_status`,
                       `payment_fullname`,
                       `payment_amount`,
                       `payment_currency`,
                       `payment_test_mode`,
                       `payment_merchant_type`,
                       `status`
                       )
                   VALUES (
                       :form_id,
                       :record_id,
                       :billing_state,
                       :payment_id,
                       :date_created,
                       :payment_date,
                       :payment_status,
                       :payment_fullname,
                       :payment_amount,
                       :payment_currency,
                       :payment_test_mode,
                       :payment_merchant_type,
                       :status
                       )";


                $params = array();
                $params[':form_id'] 		  	= $form_id;
                $params[':record_id'] 			= $entry_id;
                $params[':billing_state'] = $_GET['cellulantTransactionID'];
                $params[':payment_id'] 			= $form_id."/".$entry_id."/".$invoice->getId();
                $params[':date_created'] 		= date("Y-m-d H:i:s");
                $params[':payment_date'] 		= date("Y-m-d H:i:s");

                if($_GET['status'] == 201)
                {
                    $params[':payment_status'] 		= 'paid';
                }
                else if($_GET['status'] == 203)
                {
                    $params[':payment_status'] 		= 'failed';
                }
                else
                {
                    $params[':payment_status'] 		= 'pending confirmation';
                }

                $params[':payment_fullname']  	= trim($_SESSION['first_name'].' '.$_SESSION['last_name']);

                if($_SESSION['partial_amount']) {
                    $params[':payment_amount'] = $_SESSION['partial_amount'];
                }
                else
                {
                    $params[':payment_amount'] = $invoice->getTotalAmount();
                }

                $params[':payment_currency']  	= $invoice->getCurrency();

                if($use_paypal_sandbox){
                    $params[':payment_test_mode'] 	= 1;
                }else{
                    $params[':payment_test_mode'] 	= 0;
                }

                $params[':payment_merchant_type'] = 'ecitizen';
                $params[':status'] 			  	  = 1;

                mf_do_query($query,$params,$dbh);

                if($_GET['status'] == 201 && $invoice_manager->get_invoice_total_owed($invoice->getId()) <= 0)
                {
                    $invoice->setPaid(2);
                    $invoice->setUpdatedAt(date("Y-m-d"));
                    $invoice->save();
                }
                else if($_GET['status'] == 203)
                {
                    $invoice->setPaid(3);
                    $invoice->setUpdatedAt(date("Y-m-d"));
                    $invoice->save();
                }
                else
                {
                    $invoice->setPaid(15);
                    $invoice->setUpdatedAt(date("Y-m-d"));
                    $invoice->save();
                }

            }

            $_SESSION['payment_updated'] = true;
            header("Location: /index.php/application/view/id/".$application->getId());
            ?>
            You are being redirected...

            <a href="/index.php/forms/view?id=<?php echo $form_id; ?>&entryid=<?php echo $entry_id; ?>&done=1">Click the link here to continue.</a>
            <script language="javascript">
                window.location = "/index.php/application/view/id=<?php echo $application->getId(); ?>";
            </script>
            <?php

            exit;
        }
    }
    else
    {
       
        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.form_id = ? AND a.entry_id = ?', array($form_id,$entry_id));
        $application = $q->fetchOne();
       //  echo("Ecitizen >>>>>>>> ".$_GET['app_id']);
        if($application)
        {
            //approve invoice
            if(sizeof($application->getMfInvoice()) == 0)
            {
                $q = Doctrine_Query::create()
                    ->from('Invoicetemplates a')
                    ->where("a.applicationform = ?", $application->getFormId());
                $invoicetemplate = $q->fetchOne();

                $invoice = new MfInvoice();
                $invoice->setAppId($application->getId());

                if($invoicetemplate->getInvoiceNumber())
                {
                    $q = Doctrine_Query::create()
                        ->from('MfInvoice a')
                        ->where("a.template_id = ?", $invoicetemplate->getId())
                        ->orderBy("a.id DESC");
                    $lastinvoice = $q->fetchOne();

                    if($lastinvoice)
                    {
                        $invoice_number = $lastinvoice->getInvoiceNumber();
                        $invoice_number++;
                        $invoice->setInvoiceNumber($invoice_number);
                    }
                    else
                    {
                        $invoice->setInvoiceNumber($invoicetemplate->getInvoiceNumber());
                    }

                    $invoice->setTemplateId($invoicetemplate->getId());
                }
                else
                {
                    $invoice->setInvoiceNumber("INV-".$application->getId());
                }

                $invoice->setPaid(15);
                $invoice->setCreatedAt(date("Y-m-d H:i:s"));
                $invoice->setUpdatedAt(date("Y-m-d H:i:s"));

                $invoice->save();

                //Get total paid and seperate service fee from amount
                $grand_total = 0;
                $total_amount = 0;
                $service_fee = 0;

                $query  = "select
                      form_code,
                      payment_currency,
                      payment_price_type,
                      payment_price_amount,
                      payment_enable_tax,
                      payment_tax_rate,
                      payment_tax_amount
                      from
                         `".MF_TABLE_PREFIX."forms`
                     where
                        form_id=?";

                $params = array($_GET['id']);

                $sth = mf_do_query($query,$params,$dbh);
                $row = mf_do_fetch_result($sth);

                $payment_currency      = $row['payment_currency'];
                $payment_price_type   = $row['payment_price_type'];
                $payment_price_amount = (float) $row['payment_price_amount'];

                $payment_enable_tax = (int) $row['payment_enable_tax'];
                $payment_tax_rate    = (float) $row['payment_tax_rate'];
                $payment_tax_amount  = (float) $row['payment_tax_amount'];

                $service_code = $row['form_code'];


                if(!empty($payment_enable_tax)){
                    if($payment_tax_amount)
                    {
                        $service_fee = $payment_tax_amount;
                        $total_amount = $total_amount - $service_fee;
                    }
                    else
                    {
                        $before_total = ($total_amount * 100) / (100 + $payment_tax_rate);
                        $service_fee = round(($total_amount - $before_total),2); //round to 2 digits decimal
                        $total_amount = $before_total;
                    }
                }

                if($service_fee > 0)
                {

                    $invoicedetail = new MfInvoiceDetail();
                    $invoicedetail->setInvoiceId($invoice->getId());
                    $invoicedetail->setDescription("Convenience fee");
                    $invoicedetail->setAmount($service_fee);
                    $invoicedetail->setCreatedAt(date("Y-m-d H:i:s"));
                    $invoicedetail->setUpdatedAt(date("Y-m-d H:i:s"));
                    $invoicedetail->save();
                    $grand_total += $service_fee;
                }

                $invoicedetail = new MfInvoiceDetail();
                $invoicedetail->setInvoiceId($invoice->getId());
                $invoicedetail->setDescription($form->getFormName()." submission fee");
                $invoicedetail->setAmount($total_amount);
                $invoicedetail->setCreatedAt(date("Y-m-d H:i:s"));
                $invoicedetail->setUpdatedAt(date("Y-m-d H:i:s"));
                $invoicedetail->save();
                $grand_total += $total_amount;

                $invoice->setMdaCode(sfConfig::get('app_mda_code'));
                $invoice->setBranch(sfConfig::get('app_branch'));

                $due_date = date("Y-m-d");
                $expires_at = date("Y-m-d");

                if($invoicetemplate->getMaxDuration())
                {
                    $date = strtotime("+".$invoicetemplate->getMaxDuration()." day", time());
                    $expires_at = date('Y-m-d', $date);
                }

                if($invoicetemplate->getDueDuration())
                {
                    $date = strtotime("+".$invoicetemplate->getDueDuration()." day", time());
                    $expires_at = date('Y-m-d', $date);
                }

                $invoice->setDueDate($due_date);
                $invoice->setExpiresAt($expires_at);
                $invoice->setPayerId($sf_user->getGuardUser()->getUsername());
                $invoice->setPayerName($sf_user->getGuardUser()->getProfile()->getFullname());
                $invoice->setDocRefNumber($application->getApplicationId());
                $invoice->setCurrency($payment_currency);
                $invoice->setServiceCode($service_code);
                $invoice->setTotalAmount($grand_total);
                $invoice->save();
            }

            foreach($application->getMfInvoice() as $invoice)
            {
                $query = "INSERT INTO `".MF_TABLE_PREFIX."form_payments`(
                         `form_id`,
                         `record_id`,
                         `billing_state`,
                         `payment_id`,
                         `date_created`,
                         `payment_date`,
                         `payment_status`,
                         `payment_fullname`,
                         `payment_amount`,
                         `payment_currency`,
                         `payment_test_mode`,
                         `payment_merchant_type`,
                         `status`
                         )
                     VALUES (
                         :form_id,
                         :record_id,
                         :billing_state,
                         :payment_id,
                         :date_created,
                         :payment_date,
                         :payment_status,
                         :payment_fullname,
                         :payment_amount,
                         :payment_currency,
                         :payment_test_mode,
                         :payment_merchant_type,
                         :status
                         )";


                $params = array();
                $params[':form_id']         = $form_id;
                $params[':record_id']       = $entry_id;
                $params[':billing_state'] = $_GET['cellulantTransactionID'];
                $params[':payment_id']      = $form_id."/".$entry_id."/1";
                $params[':date_created']    = date("Y-m-d H:i:s");
                $params[':payment_date']    = date("Y-m-d H:i:s");

                if($_GET['status'] == 201)
                {
                    $params[':payment_status']    = 'paid';
                }
                else if($_GET['status'] == 203)
                {
                    $params[':payment_status']    = 'failed';
                }
                else
                {
                    $params[':payment_status']    = 'pending confirmation';
                }

                $params[':payme$gateway_to_usent_fullname']    = trim($_SESSION['first_name'].' '.$_SESSION['last_name']);

                if($_SESSION['partial_amount']) {
                    $params[':payment_amount'] = $_SESSION['partial_amount'];
                }
                else
                {
                    $params[':payment_amount'] = $invoice->getTotalAmount();
                }

                $params[':payment_currency']    = $invoice->getCurrency();

                if($use_paypal_sandbox){
                    $params[':payment_test_mode']   = 1;
                }else{
                    $params[':payment_test_mode']   = 0;
                }

                $params[':payment_merchant_type'] = 'ecitizen';
                $params[':status']            = 1;

                mf_do_query($query,$params,$dbh);

                if($_GET['status'] == 201 && $invoice_manager->get_invoice_total_owed($invoice->getId()) <= 0)
                {
                    $invoice->setPaid(2);
                    $invoice->setUpdatedAt(date("Y-m-d"));
                    $invoice->save();
                }
                else if($_GET['status'] == 203)
                {
                    $invoice->setPaid(3);
                    $invoice->setUpdatedAt(date("Y-m-d"));
                    $invoice->save();
                }
                else
                {
                    $invoice->setPaid(15);
                    $invoice->setUpdatedAt(date("Y-m-d"));
                    $invoice->save();
                }

            }
        }
        error_log(" You are being redirected...") ;
        //OTB patch - force to set the stage of application to payment confirmation
        $otbpatch = new OTBHelper();
        $stage_id = 108 ;
        $app_id = $form_id ;
        $otbpatch->setApplicationPaymentConfirmationStage($stage_id, $_GET['app_id']) ;
     //   header("Location: /index.php/forms/view?id=".$form_id."&entryid=".$entry_id."&done=1");
        ?>
        You are being redirected...

        <a href="/index.php/forms/view?id=<?php echo $form_id; ?>&entryid=<?php echo $entry_id; ?>&done=1">Click the link here to continue.</a>
        <script language="javascript">
            window.location = "/index.php/forms/view?id=<?php echo $form_id; ?>&entryid=<?php echo $entry_id; ?>&done=1";
        </script>
        <?php
        exit;
    }
}

elseif($gateway_to_use == "jambopay")      //OTB patch  
//elseif($rowtop['payment_merchant_type'] == "jambopay")
{
    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    error_log('Jambopay Debug-x callback: '.$actual_link."-".$sf_user->getGuardUser()->getProfile()->getFullname());

    $form_id = $_GET['id'];
    $entry_id = $_GET['entryid'];

    //************************ CHECK IF VALUES HAVE BEEN SET *****************
    if (isset($_POST['JP_PASSWORD'])) {
      error_log("JamboPay Debug-x: JP password found".$sf_user->getGuardUser()->getProfile()->getFullname());

        $JP_TRANID = $_POST['JP_TRANID'];
        $JP_MERCHANT_ORDERID = $_POST['JP_MERCHANT_ORDERID'];
        $JP_ITEM_NAME = $_POST['JP_ITEM_NAME'];
        $JP_AMOUNT = $_POST['JP_AMOUNT'];
        $JP_CURRENCY = $_POST['JP_CURRENCY'];
        $JP_TIMESTAMP = $_POST['JP_TIMESTAMP'];
        $JP_PASSWORD = $_POST['JP_PASSWORD'];
        $JP_CHANNEL = $_POST['JP_CHANNEL'];

        //$sharedkey, IS ONLY SHARED BETWEEN THE MERCHANT AND JAMBOPAY. THE KEY SHOULD BE SECRET ********************

        //Make sure you get the key from JamboPay Support team
        $sharedkey = $rowtop['payment_jambopay_shared_key'];

        $str = $JP_MERCHANT_ORDERID . $JP_AMOUNT . $JP_CURRENCY . $sharedkey . $JP_TIMESTAMP;


        //**************** VERIFY *************************
        if (md5(utf8_encode($str)) == $JP_PASSWORD) {


            error_log("JamboPay Debug-x: JP password correct for ".$JP_MERCHANT_ORDERID."-".$sf_user->getGuardUser()->getProfile()->getFullname());

            //VALID
            //if valid, mark order as paid
            if($_REQUEST['invoiceid'])
            {
              error_log("JamboPay Debug-x: Found invoice id ".$JP_MERCHANT_ORDERID." - ".$sf_user->getGuardUser()->getProfile()->getFullname());
                $q = Doctrine_Query::create()
                    ->from('FormEntry a')
                    ->where('a.form_id = ? AND a.entry_id = ?', array($form_id,$entry_id));
                $application = $q->fetchOne();
                if($application)
                {
                    foreach($application->getMfInvoice() as $invoice)
                    {

                        error_log("JamboPay Debug-x: JP Payment Status: ".$_GET['status']." - ".$sf_user->getGuardUser()->getProfile()->getFullname());

                        $query = "INSERT INTO `".MF_TABLE_PREFIX."form_payments`(
                       `form_id`,
                       `record_id`,
                       `billing_state`,
                       `payment_id`,
                       `date_created`,
                       `payment_date`,
                       `payment_status`,
                       `payment_fullname`,
                       `payment_amount`,
                       `payment_currency`,
                       `payment_test_mode`,
                       `payment_merchant_type`,
                       `status`
                       )
                   VALUES (
                       :form_id,
                       :record_id,
                       :billing_state,
                       :payment_id,
                       :date_created,
                       :payment_date,
                       :payment_status,
                       :payment_fullname,
                       :payment_amount,
                       :payment_currency,
                       :payment_test_mode,
                       :payment_merchant_type,
                       :status
                       )";


                        $params = array();
                        $params[':form_id'] 		  	= $form_id;
                        $params[':record_id'] 			= $entry_id;
                        $params[':billing_state'] = $JP_TRANID;
                        $params[':payment_id'] 			= $JP_MERCHANT_ORDERID;
                        $params[':date_created'] 		= date("Y-m-d H:i:s");
                        $params[':payment_date'] 		= date("Y-m-d H:i:s");

                        if($_GET['status'] == 2)
                        {
                            $params[':payment_status'] 		= 'paid';
                        }
                        else if($_GET['status'] == 1)
                        {
                            $params[':payment_status'] 		= 'failed';
                        }
                        else
                        {
                            $params[':payment_status'] 		= 'pending confirmation';
                        }

                        $params[':payment_fullname']  	= trim($_SESSION['first_name'].' '.$_SESSION['last_name']);

                        if($_SESSION['partial_amount']) {
                            $params[':payment_amount'] = $_SESSION['partial_amount'];
                        }
                        else
                        {
                            $params[':payment_amount'] = $invoice->getTotalAmount();
                        }

                        $params[':payment_currency']  	= $invoice->getCurrency();

                        if($use_paypal_sandbox){
                            $params[':payment_test_mode'] 	= 1;
                        }else{
                            $params[':payment_test_mode'] 	= 0;
                        }

                        $params[':payment_merchant_type'] = 'JamboPay';
                        $params[':status'] 			  	  = 1;

                        mf_do_query($query,$params,$dbh);


                        if($_GET['status'] == 2 && $invoice_manager->get_invoice_total_owed($invoice->getId()) <= 0)
                        {
                            $invoice->setPaid(2);
                            $invoice->setUpdatedAt(date("Y-m-d"));
                            $invoice->save();
                        }
                        else if($_GET['status'] == 1)
                        {
                            $invoice->setPaid(3);
                            $invoice->setUpdatedAt(date("Y-m-d"));
                            $invoice->save();
                        }
                        else
                        {
                            $invoice->setPaid(15);
                            $invoice->setUpdatedAt(date("Y-m-d"));
                            $invoice->save();
                        }
                    }

                    error_log("JamboPay Debug-x: Attempting redirect to: /index.php/forms/view .... etc"." - ".$sf_user->getGuardUser()->getProfile()->getFullname());
                    ?>
                    You are being redirected...

                    <a href="/index.php/forms/view?id=<?php echo $form_id; ?>&entryid=<?php echo $entry_id; ?>&done=1">Click the link here to continue.</a>
                    <script language="javascript">
                        window.top.location.href = "/index.php/forms/view?id=<?php echo $form_id; ?>&entryid=<?php echo $entry_id; ?>&done=1";
                    </script>
                    <?php
                    exit;
                }
            }
            else
            {
              error_log("JamboPay Debug-x: No invoice id found ".$JP_MERCHANT_ORDERID." - ".$sf_user->getGuardUser()->getProfile()->getFullname());
                $q = Doctrine_Query::create()
                    ->from('FormEntry a')
                    ->where('a.form_id = ? AND a.entry_id = ?', array($form_id,$entry_id));
                $application = $q->fetchOne();
                
                if($application)
                {
                    //approve invoice
                    foreach($application->getMfInvoice() as $invoice)
                    {
                        error_log("JamboPay Debug-x: JP Payment Status: ".$_GET['status'].$sf_user->getGuardUser()->getProfile()->getFullname());

                        $query = "INSERT INTO `".MF_TABLE_PREFIX."form_payments`(
                       `form_id`,
                       `record_id`,
                       `billing_state`,
                       `payment_id`,
                       `date_created`,
                       `payment_date`,
                       `payment_status`,
                       `payment_fullname`,
                       `payment_amount`,
                       `payment_currency`,
                       `payment_test_mode`,
                       `payment_merchant_type`,
                       `status`
                       )
                   VALUES (
                       :form_id,
                       :record_id,
                       :billing_state,
                       :payment_id,
                       :date_created,
                       :payment_date,
                       :payment_status,
                       :payment_fullname,
                       :payment_amount,
                       :payment_currency,
                       :payment_test_mode,
                       :payment_merchant_type,
                       :status
                       )";


                        $params = array();
                        $params[':form_id'] 		  	= $form_id;
                        $params[':record_id'] 			= $entry_id;
                        $params[':billing_state'] = $JP_TRANID;
                        $params[':payment_id'] 			= $JP_MERCHANT_ORDERID;
                        $params[':date_created'] 		= date("Y-m-d H:i:s");
                        $params[':payment_date'] 		= date("Y-m-d H:i:s");

                        if($_GET['status'] == 2)
                        {
                            $params[':payment_status'] 		= 'paid';
                        }
                        else if($_GET['status'] == 1)
                        {
                            $params[':payment_status'] 		= 'failed';
                        }
                        else
                        {
                            $params[':payment_status'] 		= 'pending confirmation';
                        }

                        $params[':payment_fullname']  	= trim($_SESSION['first_name'].' '.$_SESSION['last_name']);

                        if($_SESSION['partial_amount']) {
                            $params[':payment_amount'] = $_SESSION['partial_amount'];
                        }
                        else
                        {
                            $params[':payment_amount'] = $invoice->getTotalAmount();
                        }

                        $params[':payment_currency']  	= $invoice->getCurrency();

                        if($use_paypal_sandbox){
                            $params[':payment_test_mode'] 	= 1;
                        }else{
                            $params[':payment_test_mode'] 	= 0;
                        }

                        $params[':payment_merchant_type'] = 'JamboPay';
                        $params[':status'] 			  	  = 1;

                        mf_do_query($query,$params,$dbh);


                        if($_GET['status'] == 2 && $invoice_manager->get_invoice_total_owed($invoice->getId()) <= 0)
                        {
                            $invoice->setPaid(2);
                            $invoice->setUpdatedAt(date("Y-m-d"));
                            $invoice->save();
                        }
                        else if($_GET['status'] == 1)
                        {
                            $invoice->setPaid(3);
                            $invoice->setUpdatedAt(date("Y-m-d"));
                            $invoice->save();
                        }
                        else
                        {
                            $invoice->setPaid(15);
                            $invoice->setUpdatedAt(date("Y-m-d"));
                            $invoice->save();
                        }

                    }
                }
                else
                {
                    error_log("JamboPay Debug-x: No application found for ".$form_id."/".$entry_id."- Possible bug. REF:".$JP_MERCHANT_ORDERID);
                }



                if($_GET['status'] == "0")
                {
                    $q = Doctrine_Query::create()
                        ->from("FormEntry a")
                        ->where("a.form_id = ? AND a.entry_id = ?", array($form_id,$entry_id));
                    $application = $q->fetchOne();
                    if($application)
                    {
                      error_log("JamboPay - Debug-x: 11".$sf_user->getGuardUser()->getProfile()->getFullname());
                      error_log("JamboPay Debug-x: Attempting redirect to: /index.php/application .... etc - ".$sf_user->getGuardUser()->getProfile()->getFullname());
                        //header("Location: /index.php/application/view/id/" . $application->getId());
                        ?>
                        You are being redirected...

                        <a href="/index.php/application/view/id/<?php echo $application->getId(); ?>">Click the link here to continue.</a>
                        <script language="javascript">
                            window.top.location.href = "/index.php/application/view/id/<?php echo $application->getId(); ?>";
                        </script>
                        <?php
                        exit;
                    }

                    error_log("JamboPay Debug-x: Attempting redirect to: /index.php/dashboard - ".$sf_user->getGuardUser()->getProfile()->getFullname());

                    //header("Location: /index.php/dashboard");
                    ?>
                    You are being redirected...

                    <a href="/index.php/dashboard">Click the link here to continue.</a>
                    <script language="javascript">
                        window.top.location.href = "/index.php/dashboard";
                    </script>
                    <?php
                    exit;
                }
                else {
                    //header("Location: /index.php/forms/view?id=" . $form_id . "&entryid=" . $entry_id . "&done=1");
                    ?>
                    You are being redirected...

                    <a href="/index.php/forms/view?id=<?php echo $form_id; ?>&entryid=<?php echo $entry_id; ?>&done=1">Click the link here to continue.</a>
                    <script language="javascript">
                        window.top.location.href = "/index.php/forms/view?id=<?php echo $form_id; ?>&entryid=<?php echo $entry_id; ?>&done=1";
                    </script>
                    <?php
                    exit;
                }
                error_log("JamboPay Debug-x: Attempting redirect to: /index.php/forms/view .... etc/.2 - ".$sf_user->getGuardUser()->getProfile()->getFullname());
                ?>
                You are being redirected...

                <a href="/index.php/forms/view?id=<?php echo $form_id; ?>&entryid=<?php echo $entry_id; ?>&done=1">Click the link here to continue.</a>
                <script language="javascript">
                    window.top.location.href = "/index.php/forms/view?id=<?php echo $form_id; ?>&entryid=<?php echo $entry_id; ?>&done=1";
                </script>
                <?php
                exit;
            }
        } else{
            //INVALID TRANSACTION
            //header("Location: /index.php/forms/invalidpayment");
            error_log("JamboPay Debug-x: INVALID JP PASSWORD ".$JP_PASSWORD." for (".$str.") - ".$sf_user->getGuardUser()->getProfile()->getFullname());
            ?>
            You are being redirected...

            <a href="/index.php/forms/invalidpayment">Click the link here to continue.</a>
            <script language="javascript">
                window.top.location.href = "/index.php/forms/invalidpayment";
            </script>
            <?php
            exit;
        }
        error_log("JamboPay - Debug-x: 12 - ".$sf_user->getGuardUser()->getProfile()->getFullname());
    }
    else
    {
        //INVALID TRANSACTION
        //header("Location: /index.php/forms/invalidpayment");
        error_log("JamboPay - Debug-x: MISSING JP PASSWORD - ".$sf_user->getGuardUser()->getProfile()->getFullname());
        ?>
        You are being redirected...

        <a href="/index.php/forms/invalidpayment">Click the link here to continue.</a>
        <script language="javascript">
            window.top.location.href = "/index.php/forms/invalidpayment";
        </script>
        <?php
        exit;
    }
    error_log("JamboPay - Debug-x: 13 - ".$sf_user->getGuardUser()->getProfile()->getFullname());
}

elseif($gateway_to_use == "cash") //OTB patch
//elseif($rowtop['payment_merchant_type'] == "cash")
{
   
    $form_id = $_GET['id'];
    $entry_id = $_GET['entryid'];

    $receipt_form_id = $_GET['receiptid'];
    $receipt_entry_id = $_GET['receiptentryid'];

    $q = Doctrine_Query::create()
        ->from('FormEntry a')
        ->where('a.form_id = ? AND a.entry_id = ?', array($form_id,$entry_id));
    $application = $q->fetchOne();
    if($application)
    {
        foreach($application->getMfInvoice() as $invoice)
        {
            // error_log("Cash Payment elseifexecuted >>>>") ;
            //$invoice->setPaid(2);
            $invoice->setPaid(15); //OTB - set status of invoice as "Pending Confirmation" for cash payments
            // OTB invoice should not change to paid when user attachs a receipt but rather after payments have been confirmed.
          //  $invoice->setPaid(1); //OTB - Setting this invoice remains pending but application only moves to next stage if invoice is paid
            $invoice->setUpdatedAt(date("Y-m-d"));
            $invoice->save();

            //Attach Receipt to Invoice
           /* $receipt = new UploadReceipt();
            $receipt->setInvoiceId($invoice->getId());
            $receipt->setFormId($receipt_form_id);
            $receipt->setEntryId($receipt_entry_id);
            $receipt->save();*/
            
            //Add form payment entry
            $query = "INSERT INTO `".MF_TABLE_PREFIX."form_payments`(
           `form_id`,
           `record_id`,
           `billing_state`,
           `payment_id`,
           `date_created`,
           `payment_date`,
           `payment_status`,
           `payment_fullname`,
           `payment_amount`,
           `payment_currency`,
           `payment_test_mode`,
           `payment_merchant_type`,
           `status`
           )
       VALUES (
           :form_id,
           :record_id,
           :billing_state,
           :payment_id,
           :date_created,
           :payment_date,
           :payment_status,
           :payment_fullname,
           :payment_amount,
           :payment_currency,
           :payment_test_mode,
           :payment_merchant_type,
           :status
           )";


            $params = array();
            $params[':form_id']         = $form_id;
            $params[':record_id']       = $entry_id;
            $params[':billing_state'] = $receipt_form_id."/".$receipt_entry_id;
            $params[':payment_id']      = $receipt_form_id."/".$receipt_entry_id;
            $params[':date_created']    = date("Y-m-d H:i:s");
            $params[':payment_date']    = date("Y-m-d H:i:s");

            if($_GET['status'] == 2)
            {
                $params[':payment_status']    = 'paid';
            }
            else if($_GET['status'] == 1)
            {
                $params[':payment_status']    = 'failed';
            }
            else
            {
                $params[':payment_status']    = 'pending confirmation';
            }

            $params[':payment_fullname']    = trim($_SESSION['first_name'].' '.$_SESSION['last_name']);

            if($_SESSION['partial_amount']) {
                $params[':payment_amount'] = $_SESSION['partial_amount'];
            }
            else
            {
                $params[':payment_amount'] = $invoice->getTotalAmount();
            }

            $params[':payment_currency']    = $invoice->getCurrency();

            if($use_paypal_sandbox){
                $params[':payment_test_mode']   = 1;
            }else{
                $params[':payment_test_mode']   = 0;
            }

            $params[':payment_merchant_type'] = 'Cash';
            $params[':status']            = 1;

            mf_do_query($query,$params,$dbh);
        }
      }

      ?>
       Receipt Upload Successful! .... You are being redirected...

     <!-- <a href="/index.php/forms/view?id=<?php //echo $form_id; ?>&entryid=<?php //echo $entry_id; ?>&done=1">Click the link here to continue.</a>
      <script language="javascript">
          window.top.location.href = "/index.php/forms/view?id=<?php //echo $form_id; ?>&entryid=<?php //echo $entry_id; ?>&done=1";
      </script> -->
        <a href="/index.php/application/view/id=<?php echo $application->getId(); ?>">Click here to continue.</a>
      <script language="javascript">
          window.top.location.href = "/index.php/application/view/id/<?php echo $application->getId(); ?>";
        
      </script>
      <?php
      exit;
}
?>
