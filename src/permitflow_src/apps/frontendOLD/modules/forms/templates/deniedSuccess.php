<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="/assets_unified/images/favicon.png" type="image/png">

  <title>403 Forbidden </title>

  <link href="/assets_unified/css/style.default.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="/assets_unified/js/html5shiv.js"></script>
  <script src="/assets_unified/js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="notfound">

<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>
  
  <div class="notfoundpanel">
    <h1>403!</h1>
    <h3>Submission Rejected!</h3>
    <h4 style="color: red ;"> Oops! Sorry system has rejected your submission because an application with UPI <?php echo $upi ?> exists in the system !</h4>
    <p style="color: blue ;"> How can i fix this ? Try submitting your application using a different UPI. </p>
    <ul>
        <li>Support: info@bpmis.gov.rw</li>
    </ul>
    <a class="btn btn-primary" href="/index.php/dashboard"> Back to Dashboard  </a>
  </div><!-- notfoundpanel -->
  
</section>


<script src="/assets_unified/js/jquery-1.10.2.min.js"></script>
<script src="/assets_unified/js/jquery-migrate-1.2.1.min.js"></script>
<script src="/assets_unified/js/bootstrap.min.js"></script>
<script src="/assets_unified/js/modernizr.min.js"></script>
<script src="/assets_unified/js/retina.min.js"></script>

<script src="/assets_unified/js/custom.js"></script>

</body>
</html>
