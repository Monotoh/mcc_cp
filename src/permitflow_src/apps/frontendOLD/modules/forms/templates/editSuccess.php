<?php
/**
 * editSuccess.php template.
 *
 * Allows client and resubmit an application
 *
 * @package    frontend
 * @subpackage application
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");
$prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";
require($prefix_folder.'includes/init.php');

require($prefix_folder.'config.php');
require($prefix_folder.'includes/db-core.php');
require($prefix_folder.'includes/helper-functions.php');
require($prefix_folder.'includes/check-session.php');

require($prefix_folder.'includes/language.php');
require($prefix_folder.'includes/common-validator.php');
require($prefix_folder.'includes/post-functions.php');
require($prefix_folder.'includes/filter-functions.php');
require($prefix_folder.'includes/entry-functions.php');
require($prefix_folder.'includes/view-functions.php');
require($prefix_folder.'includes/users-functions.php');

$q = Doctrine_Query::create()
   ->from("FormEntryLinks a")
   ->where("a.id = ?", $_GET['id']);
$linkedapp = $q->fetchOne();

$form_id  = (int) trim($linkedapp->getFormId());
$entry_id = (int) trim($linkedapp->getEntryId());

$_SESSION["main_application"] = $linkedapp->getFormentryid();
        
$nav = trim($_GET['nav']);

$dbh = mf_connect_db();
$mf_settings = mf_get_settings($dbh);


function find($needle, $haystack)
{
    $pos = strpos($haystack, $needle);
    if($pos === false)
    {
        return false;
    }
    else
    {
        return true;
    }
}


$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

function do_query($sql)
{
    return mysql_query($sql);
}

//check permission, is the user allowed to access this page?
if(empty($_SESSION['mf_user_privileges']['priv_administer'])){
    $user_perms = mf_get_user_permissions($dbh,$form_id,$_SESSION['mf_user_id']);

    //this page need edit_entries permission
    if(empty($user_perms['edit_entries'])){
        $_SESSION['MF_DENIED'] = __("You don't have permission to edit this entry.");

        $ssl_suffix = mf_get_ssl_suffix();
        header("Location: http{$ssl_suffix}://".$_SERVER['HTTP_HOST'].mf_get_dirname($_SERVER['PHP_SELF'])."/restricted.php");
        exit;
    }
}

//get form name
$query 	= "select
					 form_name
			     from
			     	 ".MF_TABLE_PREFIX."forms
			    where
			    	 form_id = ?";
$params = array($form_id);

$sth = mf_do_query($query,$params,$dbh);
$row = mf_do_fetch_result($sth);

if(!empty($row)){
    $form_name = htmlspecialchars($row['form_name']);
}

//if there is "nav" parameter, we need to determine the correct entry id and override the existing entry_id
if(!empty($nav)){
    $all_entry_id_array = mf_get_filtered_entries_ids($dbh,$form_id);
    $entry_key = array_keys($all_entry_id_array,$entry_id);
    $entry_key = $entry_key[0];

    if($nav == 'prev'){
        $entry_key--;
    }else{
        $entry_key++;
    }

    $entry_id = $all_entry_id_array[$entry_key];

    //if there is no entry_id, fetch the first/last member of the array
    if(empty($entry_id)){
        if($nav == 'prev'){
            $entry_id = array_pop($all_entry_id_array);
        }else{
            $entry_id = $all_entry_id_array[0];
        }
    }
}

if(mf_is_form_submitted()){ //if form submitted
    $input_array   = mf_sanitize($_POST);
    $submit_result = mf_process_form($dbh,$input_array);

    if($submit_result['status'] === true){
        header("Location: /index.php/application/view/id/".$_SESSION["main_application"]);
        exit;
    }else if($submit_result['status'] === false){ //there are errors, display the form again with the errors
        $old_values 	= $submit_result['old_values'];
        $custom_error 	= @$submit_result['custom_error'];
        $error_elements = $submit_result['error_elements'];

        $form_params = array();
        $form_params['populated_values'] = $old_values;
        $form_params['error_elements']   = $error_elements;
        $form_params['custom_error'] 	 = $custom_error;
        $form_params['edit_id']			 = $input_array['edit_id'];
        $form_params['integration_method'] = 'php';
        $form_params['is_application'] = true;
        $form_params['page_number'] = 0; //display all pages (if any) as a single page

        $form_markup = mf_display_form($dbh,$input_array['form_id'],$form_params);
    }

}else{ //otherwise, display the form with the values
    //set session value to override password protected form
    $_SESSION['user_authenticated'] = $form_id;

    //set session value to bypass unique checking
    $_SESSION['edit_entry']['form_id']  = $form_id;
    $_SESSION['edit_entry']['entry_id'] = $entry_id;

    $form_values = mf_get_entry_values($dbh,$form_id,$entry_id);

    $form_params = array();
    $form_params['populated_values'] = $form_values;
    $form_params['edit_id']			 = $entry_id;
    $form_params['integration_method'] = 'php';
    $form_params['is_application'] = true;
    $form_params['page_number'] = 0; //display all pages (if any) as a single page

    $form_markup = mf_display_form($dbh,$form_id,$form_params);
}
?>
<div class="pageheader">
       <h2><i class="fa fa-edit"></i><?php echo __('Application Form') ?><span> <?php echo __('Submit an Application') ?></span></h2>
      <div class="breadcrumb-wrapper">
        
        <ol class="breadcrumb">
          <li><a href=""><?php echo __('Application Form') ?></a></li>
          <li class="active"><?php echo $form_name; ?></li>
        </ol>
      </div>
    </div>
    

<div class="row clearfix">
<?php include_partial('index/sidemenu', array('' => '')) ?>

<div class="contentpanel">
<div class="row">



    <div class="accordion_custom" id="accordion2">
                <div class="accordion-group whitened">
                  <div class="accordion-body in">
                    <div class="accordion-inner unspaced">
                        <div id="content" class="full">
                            <div class="post edit_entry">
                                <div class="content_header">

                                </div>

                                <?php mf_show_message(); ?>

                                <div class="content_body">
                                    <div id="ve_details" data-formid="<?php echo $form_id; ?>" data-entryid="<?php echo $entry_id; ?>">
                                        <?php echo $form_markup; ?>
                                    </div>
                                </div> <!-- /end of content_body -->

                            </div><!-- /.post -->
                        </div><!-- /#content -->

    
    </div><!-- accordion-inner -->
                  </div><!-- accordion-body -->
                </div><!-- accordion-group -->
          </div><!-- accordion2 -->



</div><!-- /.row -->
</div><!-- /.marketing -->  
