<?php
/**
 * getrelationSuccess.php template.
 *
 * Ajax for application form that fetches related fields
 *
 * @package    frontend
 * @subpackage forms
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");
 ?>
<?php 

$searchid = $_POST['element']; 

$formid = $_POST['formid'];
$column1 = $_POST['field1'];

$q = Doctrine_Query::create()
				  ->from('ApTableRelation a')
				  ->where('a.form_id = ?', $formid)
				  ->andWhere('a.element_id = ?', $column1);
$relations = $q->execute();

foreach($relations as $relation)
{
	$tablename = $relation->getTblName();
	$tablevaluefld = $relation->getTblValueFld();
	$tablestatusfld = $relation->getTableStatusFld();
	$excludestatus = $relation->getExcludeStatus();
	
	$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
	mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
	$query = "SELECT * FROM ".$tablename." WHERE ".$tablevaluefld." = '".$_POST['element']."'";
	$result = mysql_query($query,$dbconn);
	if($result)
	{
	    if(mysql_num_rows($result) > 0)
		{
			while($row = mysql_fetch_assoc($result))
			{
				if($row["".$tablestatusfld.""] == "Black-Listed")
				{
					echo "<img src='/assets_backend/images/icons/warning.gif'> Plot has been Blacklisted";
				}
				else if($row["".$tablestatusfld.""] == "Contentious")
				{
					echo "<img src='/assets_backend/images/icons/warning.gif'> Plot has been marked as Contentious";
				}
			?>
            <hr>
			<div class="control-group">
			<label class="control-label"><?php echo __('Plot Size (In Ha)') ?>  &nbsp; &nbsp; &nbsp; (<span id=\"required_{$element->id}\" class=\"required\">* Required</span>)</label>
            <div class="controls">
                <input required id="element_plot_size" name="element_plot_size" type="text" value="<?php echo $row['plot_size']; ?>"/> 
            </div>
            </div>
			
			<hr>
			
			<div class="control-group">
			<label class="control-label"><?php echo __('Plot Location. Use this link to generate a location:') ?> <a target="_blank" href="http://itouchmap.com/latlong.html">http://itouchmap.com/latlong.html</a></label>
			</div>
			
			<hr>
			
            <div class="control-group">
			<label class="control-label"><?php echo __('Plot Latitude (e.g. -1.25)') ?></label>
            <div class="controls">
                <input id="element_plot_lat" name="element_plot_lat" type="text" value="<?php echo $row['plot_lat']; ?>"/> 
			</div>
			</div>
			
			<hr>
		
            <div class="control-group">
			<label class="control-label"><?php echo __('Plot Longitude (e.g. -1.25)') ?></label>
            <div class="controls">
                <input id="element_plot_long" name="element_plot_long" type="text" value="<?php echo $row['plot_long']; ?>"/> 
            </div>
            </div>
            <?php
			}
		}
		else
		{
			echo "<img src='/assets_backend/images/icons/icon_tick.png'> Available, Please Enter Additional Info Below";
			?>
           <hr>
								<div class="control-group">
								<label class="control-label"><?php echo __('Plot Size (In Ha)') ?> &nbsp; &nbsp; &nbsp; (<span id=\"required_{$element->id}\" class=\"required\">* Required</span>)</label>
								<div class="controls">
								<input id="element_plot_size" required name="element_plot_size" type="text" value="" /> 
								</div>
								</div>
								
								<hr>
			
								<div class="control-group">
								<label class="control-label"><?php echo __('Plot Location. Use this link to generate a location:') ?> <a target="_blank" href="http://itouchmap.com/latlong.html">http://itouchmap.com/latlong.html</a></label>
								</div>
								
								<hr>
								
								<div class="control-group">
								<label class="control-label"><?php echo __('Plot Latitude (e.g. -1.25)')?></label>
								<div class="controls">
									<input id="element_plot_lat" name="element_plot_lat" type="text" value="Lat..." onclick="this.value='';"/>
								</div>
								</div>
								
								<hr>
								
								<div class="control-group">
								<label class="control-label"><?php echo __('Plot Longitude (e.g. 1.25)') ?></label>
								<div class="controls">
									<input id="element_plot_long" name="element_plot_long" type="text" value="Long..." onclick="this.value='';"/> 
								</div>
								</div>
            <?php
		}
	}
}
?>
