<?php

/**
 * viewSuccess.php template.
 *
 * Displays a dynamically generated application form
 *
 * @package    frontend
 * @subpackage forms
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

include_component('index', 'checksession');

//Get site config properties
$q = Doctrine_Query::create()
    ->from("ApSettings a")
    ->where("a.id = 1")
    ->orderBy("a.id DESC");
$apsettings = $q->fetchOne();

$q = Doctrine_Query::create()
    ->from('ApForms a')
    ->where('a.form_id = ?', $_GET['id']);
$form = $q->fetchOne();
?>
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Make Application</h4>
    </div>
</div>
<!-- Page-Title -->

<div class="row">

    <div class="col-lg-8">

        <div class="panel panel-default panel-blog">
          <div class="panel-body">
            <h3 class="blogsingle-title"><?php echo $form->getFormName(); ?></h3>


            <div class="mb20"></div>

            <?php echo html_entity_decode($form->getFormDescription()); ?>

           </div><!-- panel-body -->
           <div class="panel-footer">
            <a class="btn btn-primary waves-effect w-md waves-light" href="<?php echo public_path(); ?>index.php/forms/view?id=<?php echo $form->getFormId(); ?>">Apply Now</a>
           </div>

        </div><!-- panel -->

    </div><!-- col-sm-8 -->

    <!--Display a sidebar with information from the site config-->
    <?php if($apsettings){ ?>
        <div class="col-lg-4">
            <div class="card-box widget-user">
                <div>
                    <img src="/asset_unified/images/users/avatar-1.jpg" class="img-responsive img-circle" alt="user">
                    <div class="wid-u-info">
                        <h4 class="m-t-0 m-b-5"><?php echo $sf_user->getGuardUser()->getProfile()->getFullname(); ?></h4>
                        <p class="m-b-5 font-13"><?php echo $sf_user->getGuardUser()->getProfile()->getEmail(); ?><br>
                            ID: <?php echo $sf_user->getGuardUser()->getUsername(); ?>
                        </p>
                        <a class="btn btn-primary" href="/index.php/signon/logout">Logout</a>
                    </div>
                </div>
            </div>

            <?php echo html_entity_decode($apsettings->getOrganisationSidebar()); ?>
        </div>
    <?php } ?>
</div><!-- row -->