<?php 
 use_helper("I18N");
?>
<div class="contentpanel">
    <div class="row">

        <div class="accordion_custom" id="accordion2">
            <div class="accordion-group whitened">
                <div class="accordion-body in">
                    <div class="accordion-inner unspaced">
                        <div class="contentpanel">
                            <div class="row">
                                <div class="panel panel-dark">
                                    <div class="panel-body">
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <p><?php echo __('Sorry! Your payment has been flagged as invalid.') ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>            </div><!-- accordion-inner -->
                </div><!-- accordion-body -->
            </div><!-- accordion-group -->
        </div><!-- accordion2 -->

    </div>
</div>
