<?php
//ini_set('display_errors', '1');     # don't show any errors...
//error_reporting(E_ALL | E_STRICT);
/**
 * viewSuccess.php template.
 *
 * Displays a dynamically generated application form
 *
 * @package    frontend
 * @subpackage forms
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper('I18N');
$otbhelper = new OTBHelper() ;
/**
 * OTB patch
 * PHP Spell 
 * 
 */

//$path = echo public_path("/") ;
$mySpell = new SpellAsYouType();
$mySpell->InstallationPath = public_path("/otb_assets/javascriptspellcheck/"); 	  
$mySpell->Fields = "ALL"; 
echo $mySpell->Activate(); 
//Get site config properties
$q = Doctrine_Query::create()
    ->from("ApSettings a")
    ->where("a.id = 1")
    ->orderBy("a.id DESC");
$apsettings = $q->fetchOne();

$sf_user->setAttribute('invoice_id', 0);

//$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
$dbconn = mysqli_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
//mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
mysqli_select_db($dbconn,sfConfig::get('app_mysql_db')) ;

$application_manager = new ApplicationManager();
$invoice_manager = new InvoiceManager();

//Check if enable_categories is set, if it is then filter application forms
if(sfConfig::get('app_enable_categories') == "yes")
{
    $q = Doctrine_Query::create()
        ->from('sfGuardUserCategoriesForms a')
        ->where('a.categoryid = ?', $sf_user->getGuardUser()->getProfile()->getRegisteras())
        ->andWhere('a.formid = ?', $_GET['id']);
    $category = $q->count();

    if ($category == 0) {
       // exit;
    }
}

//Check if the application form can be auto-submitted by fetching user details
if($application_manager->can_autosubmit_form($_GET['id']) && empty($_GET['done']))
{
  //Before autosubmit check if there is a form limit
  $q = Doctrine_Query::create()
       ->from("ApForms a")
       ->where("a.form_id = ?", $_GET['id']);
  $form = $q->fetchOne();

  $q = Doctrine_Query::create()
       ->from("SavedPermit a")
       ->leftJoin("a.FormEntry b")
       ->where("b.form_id = ?", $_GET['id'])
       ->andWhere("b.approved <> 0")
       ->andWhere("a.permit_status = 1")
       ->andWhere("b.user_id = ?", $sf_user->getGuardUser()->getId());
  $permit = $q->count();

  if($form->getFormUniqueIp() && $permit > 0)
  {
    //Check if permit is expired, if not then allow auto_submit
    $expired = false;

    $permit = $q->fetchOne();

    if(strtotime($permit->getDateOfExpiry()) < strtotime(date("Y-m-d")))
    {
      $expired = true;
    }

    if($expired == false)
    {
      //Display limit message
      ?>

      <div class="pageheader">
          <h2><i class="fa fa-edit"></i><?php echo __('Application Form'); ?>
              <span> <?php echo __('Submit an Application'); ?></span></h2>

          <div class="breadcrumb-wrapper">

              <ol class="breadcrumb">
                  <li><?php echo __('Application Form'); ?></li>
                  <li class="active"><?php echo $form_name; ?></li>
              </ol>
          </div>
      </div>


      <div class="contentpanel">
          <div class="row">

              <div class="accordion_custom" id="accordion2">
                  <div class="accordion-group whitened">
                      <div class="accordion-body in">
                          <div class="accordion-inner unspaced">
                              <div id="main_body" class="panel panel-dark mb0">
                                  <form id="form_{$form->id}" class="form-horizontal form-bordered" method="post" action="#main_body">
                                    <div class="panel-body panel-body-nopadding">
                                      <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h2><?php echo $form->getFormName() ?></h2>
                                        <h3><?phpp echo __('Sorry! You have reached the limit on the number of applications you can submit for'?>. <br><?php echo __('You must wait until your current service expires before applying for a new one.')?></h3>
                                      </div>
                                    </div>
                                  </form>
                                </div>
                          </div>
                          <!-- accordion-inner -->
                      </div>
                      <!-- accordion-body -->
                  </div>
                  <!-- accordion-group -->
              </div>
              <!-- accordion2 -->

          </div>
          <!-- /.row -->
      </div><!-- /.marketing -->



      </div><!-- /.marketing -->
      <?php
    }
    else
    {
      $application_manager->autosubmit_form($_GET['id'], $sf_user->getGuardUser()->getId());
    }
  }
  else
  {
    $application_manager->autosubmit_form($_GET['id'], $sf_user->getGuardUser()->getId());
  }
}
else {
//Check if backend user is logged in, if so log them out
    if ($sf_user->isAuthenticated()) {
        if ($sf_user->getGuardUser()->getId() == '1') {
            $sf_user->signout();
            header("Location: /index.php");
        } else {
            $_SESSION['userid'] = $sf_user->getGuardUser()->getId();
        }
    }

    $_SESSION['edit_entry']['entry_id'] = null;

    $_SESSION['redirect'] = false;
    $_SESSION['redirect_url'] = "";

    $_SESSION['draft_edit'] = false;

    $prefix_folder = dirname(__FILE__) . "/../../../../../lib/vendor/cp_machform/";

    header("p3p: CP=\"IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT\"");

    require($prefix_folder . 'config.php');
    require($prefix_folder . 'includes/language.php');
    require($prefix_folder . 'includes/db-core.php');
    require($prefix_folder . 'includes/common-validator.php');
    require($prefix_folder . 'includes/view-functions.php');
    require($prefix_folder . 'includes/post-functions.php');
    require($prefix_folder . 'includes/filter-functions.php');
    require($prefix_folder . 'includes/entry-functions.php');
    require($prefix_folder . 'includes/helper-functions.php');
    require($prefix_folder . 'includes/theme-functions.php');
    require($prefix_folder . 'lib/recaptchalib.php');
    require($prefix_folder . 'lib/php-captcha/php-captcha.inc.php');
    require($prefix_folder . 'lib/text-captcha.php');
    require($prefix_folder . 'hooks/custom_hooks.php');

    $dbh = mf_connect_db();
    $ssl_suffix = mf_get_ssl_suffix();
    $_SESSION["user_email"] = $sf_user->getGuardUser()->getProfile()->getEmail();

    $_SESSION['applicant_name'] = $sf_user->getGuardUser()->getProfile()->getFullname();
    $_SESSION['applicant_id'] = $sf_user->getGuardUser()->getUsername();


    if (mf_is_form_submitted()) { //if form submitted
        $form_id_cu = $_GET['id'];
        //OTB - Get form element with UPI
        $query = "select element_id from ap_form_elements where form_id = $form_id_cu and element_is_upi = 1 and element_is_hidden = 0 ;" ;
        $query_r = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($query) ;
        $upi_field_is = null ;
        foreach($query_r as $res){
             $upi_field_is = $res['element_id'] ;
         }
         //error_log("UPI field is >>> ".$upi_field_is);
         //error_log("element value to check >>> ".$_POST['element_'.$upi_field_is]);
         //Validate UPI field supplied by client
         $upi = $_POST['element_'.$upi_field_is] ;
         //$_SESSION['session_upi'] = 'None' ;
         if(!empty($upi)){
			    $_SESSION['session_upi'] = $upi;
			 }
         $element_id = "element_".$upi_field_is ;
         error_log("Session UPI >> ".$_SESSION['session_upi']);
         if(!empty($upi_field_is)){
             
			 if($otbhelper->validateUPI($_SESSION['session_upi'],$form_id_cu,$element_id) == "invalid"){
				 error_log("Oops Redirect to Error page. Invalid UPI") ;
				 header("Location: http{$ssl_suffix}://" . $_SERVER['HTTP_HOST']. "/index.php/forms/denied?upi=".$upi);
				 exit();
         }else{
             error_log("Great Continue ... Valid UPI") ;
         }
         }
         
       
        if ($_POST['save_as_draft'] || $_POST['save_as_draft2']) {
            $_SESSION['save_as_draft'] = true;
        } else {
            $_SESSION['save_as_draft'] = false;
        }

        $input_array = mf_sanitize($_POST);
        $submit_result = mf_process_form($dbh, $input_array);

        if (!isset($input_array['password'])) { //if normal form submitted

            if ($submit_result['status'] === true) {
                if (!empty($submit_result['form_resume_url'])) { //the user saving a form, display success page with the resume URL
                    $_SESSION['mf_form_resume_url'][$input_array['form_id']] = $submit_result['form_resume_url'];

                    if ($_SESSION['save_as_draft']) {
                        header("Location: http{$ssl_suffix}://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "?id={$input_array['form_id']}&entryid={$submit_result['entry_id']}&draft=1&done=1");
                    } else {
                        header("Location: http{$ssl_suffix}://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "?id={$input_array['form_id']}&entryid={$submit_result['entry_id']}&done=1");
                    }

                    exit;
                } else if ($submit_result['logic_page_enable'] === true) { //the page has skip logic enable and a custom destination page has been set
                    $target_page_id = $submit_result['target_page_id'];

                    if (is_numeric($target_page_id)) {
                        header("Location: http{$ssl_suffix}://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "?id={$input_array['form_id']}&mf_page={$target_page_id}");
                        exit;
                    } else if ($target_page_id == 'payment') {
                        //redirect to payment page, based on selected merchant
                        $form_properties = mf_get_form_properties($dbh, $input_array['form_id'], array('payment_merchant_type'));

                        if ($form_properties['payment_merchant_type'] == 'stripe') {
                            //allow access to payment page
                            $_SESSION['mf_form_payment_access'][$input_array['form_id']] = true;
                            $_SESSION['mf_payment_record_id'][$input_array['form_id']] = $submit_result['entry_id'];

                            $sf_user->setAttribute('form_id', $input_array['form_id']);
                            $sf_user->setAttribute('entry_id', $submit_result['entry_id']);

                            $form_id = $input_array['form_id'];
                            $new_record_id = $submit_result['entry_id'];

                            //We will use the application manager to create new applications or drafts from form submissions
                            $application_manager = new ApplicationManager();

                            //Check if an application already exists for the form submission to prevent double entry
                            if($application_manager->application_exists($form_id, $new_record_id)) {
                                //If save as draft/resume later was clicked then do nothing
                                $submission = $application_manager->get_application($form_id, $new_record_id);
                            }
                            else {
                                //If save as draft/resume later was clicked then create draft application
                                $submission = $application_manager->create_application($form_id, $new_record_id, $sf_user->getGuardUser()->getId(), true);
                            }

                            $application_manager->update_invoices($submission->getId());

                            header("Location: http{$ssl_suffix}://" . $_SERVER['HTTP_HOST'] . mf_get_dirname($_SERVER['PHP_SELF']) . "/payment");
                            exit;
                        } else if ($form_properties['payment_merchant_type'] == 'paypal_standard') {
                            echo "<script type=\"text/javascript\">top.location.replace('{$submit_result['form_redirect']}')</script>";
                            exit;
                        }
                    } else if ($target_page_id == 'review') {
                        if (!empty($submit_result['origin_page_number'])) {
                            $page_num_params = '&mf_page_from=' . $submit_result['origin_page_number'];
                        }

                        $_SESSION['review_id'] = $submit_result['review_id'];
                        header("Location: http{$ssl_suffix}://" . $_SERVER['HTTP_HOST'] . mf_get_dirname($_SERVER['PHP_SELF']) . "/confirm?id={$input_array['form_id']}{$page_num_params}");
                        exit;
                    } else if ($target_page_id == 'success') {
                        //redirect to success page
                        if (empty($submit_result['form_redirect'])) {

                            if ($_SESSION['save_as_draft']) {
                                header("Location: http{$ssl_suffix}://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "?id={$input_array['form_id']}&draft={$submit_result['draft']}&done=1");
                            } else {
                                header("Location: http{$ssl_suffix}://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "?id={$input_array['form_id']}&entryid={$submit_result['entry_id']}&done=1");
                            }
                            exit;
                        } else {
                            echo "<script type=\"text/javascript\">top.location.replace('{$submit_result['form_redirect']}')</script>";
                            exit;
                        }
                    }

                } else if (!empty($submit_result['review_id']) && empty($submit_result['entry_id'])) { //redirect to review page

                    if (!empty($submit_result['origin_page_number'])) {
                        $page_num_params = '&mf_page_from=' . $submit_result['origin_page_number'];
                    }

                    if (!empty($submit_result['next_page_number'])) { //redirect to the next page number
                        $_SESSION['mf_form_access'][$input_array['form_id']][$submit_result['next_page_number']] = true;

                        header("Location: http{$ssl_suffix}://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "?id={$input_array['form_id']}&mf_page={$submit_result['next_page_number']}");
                        exit;
                    }

                    $_SESSION['review_id'] = $submit_result['review_id'];
                    if ($_SESSION['save_as_draft']) {
                        //header("Location: http{$ssl_suffix}://" . $_SERVER['HTTP_HOST'] . mf_get_dirname($_SERVER['PHP_SELF']) . "/confirmDraft?id={$input_array['form_id']}&draft={$submit_result['draft']}{$page_num_params}");
                        header("Location: /index.php/forms/view?id={$input_array['form_id']}&entryid={$submit_result['entry_id']}&draft=1&done=1");
                    } else {
                        header("Location: /index.php/forms/confirmApplication?id={$input_array['form_id']}{$page_num_params}");
                    }
                    exit;
                } else {
                    if (!empty($submit_result['next_page_number'])) { //redirect to the next page number
                        $_SESSION['mf_form_access'][$input_array['form_id']][$submit_result['next_page_number']] = true;

                        header("Location: http{$ssl_suffix}://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "?id={$input_array['form_id']}&mf_page={$submit_result['next_page_number']}");
                        exit;
                    } else { //otherwise display success message or redirect to the custom redirect URL or payment page

                        //Disable payment on submission if enable_invoice is true
                        $form_properties = mf_get_form_properties($dbh, $input_array['form_id'], array('payment_enable_invoice'));

                        if (mf_is_payment_has_value($dbh, $input_array['form_id'], $submit_result['entry_id']) && $form_properties['payment_enable_invoice'] == 0) {
                            //redirect to credit card payment page, if the merchant is being enabled and the amount is not zero

                            //allow access to payment page
                            $_SESSION['mf_form_payment_access'][$input_array['form_id']] = true;
                            $_SESSION['mf_payment_record_id'][$input_array['form_id']] = $submit_result['entry_id'];

                            $sf_user->setAttribute('form_id', $input_array['form_id']);
                            $sf_user->setAttribute('entry_id', $submit_result['entry_id']);

                            $form_id = $input_array['form_id'];
                            $new_record_id = $submit_result['entry_id'];

                            //We will use the application manager to create new applications or drafts from form submissions
                            $application_manager = new ApplicationManager();

                            //Check if an application already exists for the form submission to prevent double entry
                            if($application_manager->application_exists($form_id, $new_record_id)) {
                                //If save as draft/resume later was clicked then do nothing
                                $submission = $application_manager->get_application($form_id, $new_record_id);
                            }
                            else {
                                //If save as draft/resume later was clicked then create draft application
                                $submission = $application_manager->create_application($form_id, $new_record_id, $sf_user->getGuardUser()->getId(), true);
                            }

                            $application_manager->update_invoices($submission->getId());


                            $q = Doctrine_Query::create()
                                ->from('Invoicetemplates a')
                                ->where("a.applicationform = ?", $input_array['form_id']);
                            $invoicetemplate = $q->fetchOne();
                            if ($invoicetemplate) {
                                header("Location: http{$ssl_suffix}://" . $_SERVER['HTTP_HOST'] . mf_get_dirname($_SERVER['PHP_SELF']) . "/payment");
                                exit;
                            } else {
                                echo "<script type=\"text/javascript\">alert(\"No invoice template exists for this application form. Cannot authorize payment. Please contact system administrator.\"); top.location.replace('/index.php/forms/view?id={$input_array['form_id']}')</script>";
                                exit;
                            }

                        } else {
                            //redirect to success page
                            if (empty($submit_result['form_redirect'])) {

                                $_SESSION['review_id'] = $submit_result['review_id'];

                                if ($_SESSION['save_as_draft']) {
                                    //header("Location: http{$ssl_suffix}://" . $_SERVER['HTTP_HOST'] . mf_get_dirname($_SERVER['PHP_SELF']) . "/confirmDraft?id={$input_array['form_id']}&draft={$submit_result['draft']}{$page_num_params}");
                                    header("Location: http{$ssl_suffix}://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "?id={$input_array['form_id']}&entryid={$submit_result['entry_id']}&draft=1&done=1");
                                } else {
                                    header("Location: http{$ssl_suffix}://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "?id={$input_array['form_id']}&entryid={$submit_result['entry_id']}&done=1");
                                }

                                exit;
                            } else {
                                echo "<script type=\"text/javascript\">top.location.replace('{$submit_result['form_redirect']}')</script>";
                                exit;
                            }
                        }
                    }
                }
            } else if ($submit_result['status'] === false) { //there are errors, display the form again with the errors
                $old_values = $submit_result['old_values'];
                $custom_error = @$submit_result['custom_error'];
                $error_elements = $submit_result['error_elements'];

                $form_params = array();
                $form_params['page_number'] = $input_array['page_number'];
                $form_params['populated_values'] = $old_values;
                $form_params['error_elements'] = $error_elements;
                $form_params['custom_error'] = $custom_error;

                $form_params['is_application'] = true;
                $markup = mf_display_form($dbh, $input_array['form_id'], $form_params, $sf_user->getCulture());
            }
        } else { //if password form submitted

            if ($submit_result['status'] === true) { //on success, display the form
                $form_params['is_application'] = true;
                $markup = mf_display_form($dbh, $input_array['form_id'], array(), $sf_user->getCulture());
            } else {
                $custom_error = $submit_result['custom_error']; //error, display the pasword form again

                $form_params = array();
                $form_params['custom_error'] = $custom_error;
                $form_params['is_application'] = true;
                $markup = mf_display_form($dbh, $input_array['form_id'], $form_params, $sf_user->getCulture());
            }
        }
    } else {
        $form_id = (int)trim($_GET['id']);
        $page_number = (int)trim($_GET['mf_page']);

        if ($_GET['linkto']) {
            $_SESSION["main_application"] = $_GET['linkto'];
        }

        $page_number = mf_verify_page_access($form_id, $page_number);

        $resume_key = trim($_GET['mf_resume']);
        if (!empty($resume_key)) {
            $_SESSION['mf_form_resume_key'][$form_id] = $resume_key;
        }

        if (!empty($_GET['done'])) {

            unset($_SESSION['review_id']);

            //We will use the application manager to create new applications or drafts from form submissions
            $application_manager = new ApplicationManager();

            //If the submission is to be attached to an existing application, then create a linked application, else create a normal application
            if ($_SESSION["main_application"]) {
                $submission = $application_manager->create_linked_application($_SESSION["main_application"], $_GET['id'], $_GET['entryid'], $sf_user->getGuardUser()->getId());

                $markup = mf_display_success($dbh, $form_id);

                $_SESSION["main_application"] = "";
            } else {
              if($_GET['draft'])
             {
                  //We will use the application manager to create new applications or drafts from form submissions
                  $application_manager = new ApplicationManager();

                  //Check if an application already exists for the form submission to prevent double entry
                  if($application_manager->application_exists($_GET['id'], $_GET['entryid'])) {
                          //If save as draft/resume later was clicked then do nothing
                          $submission = $application_manager->get_application($_GET['id'], $_GET['entryid']);
                          $markup = '
              <div class="panel panel-dark">
                  <div class="panel-body">
              	<div class="alert alert-success">
                               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <p>Saved as Draft! Your application has been saved as a Draft. To complete your application process at a later time, go to My Drafts from your dashboard, view your application and click on Edit and Submit.</p>
                      </div>
                  </div>
              </div>
              ';
                  }
                  else {
                          //If save as draft/resume later was clicked then create draft application
                          $submission = $application_manager->create_application($_GET['id'], $_GET['entryid'], $sf_user->getGuardUser()->getId(), true);
                          $markup = '
              <div class="panel panel-dark">
                  <div class="panel-body">
              	<div class="alert alert-success">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                              <p>Saved as Draft! Your application has been saved as a Draft. To complete your application process at a later time, go to My Drafts from your dashboard, view your application and click on Edit and Submit.</p>
                      </div>
                  </div>
              </div>
              ';
                  }
              }
              else {
                //Check if an application already exists for the form submission to prevent double entry
                if ($application_manager->application_exists($_GET['id'], $_GET['entryid'])) {
                    //If a draft existed, then publish the draft to a live workflow
                    if ($application_manager->is_draft($_GET['id'], $_GET['entryid'])) {
                        $submission = $application_manager->get_application($_GET['id'], $_GET['entryid']);
                        $submission = $application_manager->publish_draft($submission->getId());
                        $_SESSION['just_submitted'] = true;
                        $application_manager->update_services($submission->getId());
                        $markup = mf_display_success($dbh, $form_id);
                    } else {
                        //If the application was already submitted then display warning
                        $submission = $application_manager->get_application($_GET['id'], $_GET['entryid']);
                        $_SESSION['just_submitted'] = true;
                        $application_manager->update_services($submission->getId());
                        $markup = mf_display_already_submitted($dbh, $form_id);
                    }
                } else {
                    //Create new application and publish it to a live workflow
                    $submission = $application_manager->create_application($_GET['id'], $_GET['entryid'], $sf_user->getGuardUser()->getId(), false);
                    $_SESSION['just_submitted'] = true;
                    $application_manager->update_services($submission->getId());
                    $markup = mf_display_success($dbh, $form_id);
                }

              }

            }

        } else {
            $form_params = array();
            $form_params['page_number'] = $page_number;
            $form_params['is_application'] = true;

            //Check if there is a limit
            $q = Doctrine_Query::create()
                ->from('ApForms a')
                ->where('a.form_id = ?', $form_id);
            $form_settings = $q->fetchOne();
            if ($form_settings && $form_settings->getFormUniqueIp()) {
                $q = Doctrine_Query::create()
                    ->from("FormEntry a")
                    ->where("a.user_id = ?", $sf_user->getGuardUser()->getId())
                    ->andWhere("a.form_id = ?", $form_id)
                    ->andWhere('a.parent_submission = 0')
                    ->andWhere('a.approved <> 0')
                    ->orderBy("a.id DESC");
                $applications = $q->count();

                if ($applications > 0) {
                    //Check if the application has any expired licenses
                    if ($application_manager->form_entry_limit($form_id, $sf_user->getGuardUser()->getId())) {
                        $markup = mf_display_unique_entry_warning($dbh, $form_id, $form_params);
                    } else {
                        $markup = mf_display_form($dbh, $form_id, $form_params, $sf_user->getCulture());
                    }
                } else {
                    $markup = mf_display_form($dbh, $form_id, $form_params, $sf_user->getCulture());
                }
            } else {
                $markup = mf_display_form($dbh, $form_id, $form_params, $sf_user->getCulture());
            }
        }
    }

    $q = Doctrine_Query::create()
        ->from('ApForms a')
        ->where('a.form_id = ?', $_GET['id']);
    $formObj = $q->fetchOne();
    $form_name = $formObj->getFormName();
    $form_description = $formObj->getFormDescription();

    $sql = "SELECT * FROM ext_translations WHERE field_id = '" . $_GET['id'] . "' AND field_name = 'form_name' AND table_class = 'ap_forms' AND locale = '" . $sf_user->getCulture() . "'";

    $rows = mysql_query($sql, $dbconn);
    if ($row = mysql_fetch_assoc($rows)) {
        $form_name = $row['trl_content'];
    }
    ?>

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title"><?php echo __('Submit new application') ?></h4>
        </div>
    </div>
    <!-- Page-Title -->

    <div class="row">
        <div class="col-lg-10">

          <div class="panel panel-default p-b-0">
            <?php

            if($_GET['bill_error'])
            {
            ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong><?php echo __('Error') ?></strong> <?php echo __('Could not find records. Please confirm application details')?>
                 </div>
            <?php
            }

            header("Content-Type: text/html; charset=UTF-8");
            echo $markup;
            ?>
            </div>
        </div>

        <!--Display a sidebar with information from the site config-->
        <?php// if($apsettings){ ?>
           <!-- <div class="col-lg-4">
                <div class="card-box widget-user">
                    <div>
                        <img src="/asset_unified/images/users/avatar-1.jpg" class="img-responsive img-circle" alt="user">
                        <div class="wid-u-info">
                            <h4 class="m-t-0 m-b-5"><?php //echo $sf_user->getGuardUser()->getProfile()->getFullname(); ?></h4>
                            <p class="m-b-5 font-13"><?php //echo $sf_user->getGuardUser()->getProfile()->getEmail(); ?><br>
                                
                            </p>
                            <a class="btn btn-primary" href="/index.php/signon/logout">Logout</a>
                        </div>
                    </div>
                </div> -->

                <?php //echo html_entity_decode($apsettings->getOrganisationSidebar()); ?>
            </div>
        <?php //} ?>

    </div>



    <?php
    $q = Doctrine_Query::create()
        ->from("FormEntry a")
        ->where("a.user_id = ?", $sf_user->getGuardUser()->getId())
        ->andWhere("a.approved = ?", 0)
        ->andWhere("a.form_id = ?", $_GET['id'])
        ->andWhere('a.parent_submission = ?', 0)
        ->orderBy("a.id DESC");
    $applications = $q->execute();
    foreach ($applications as $application) {
        ?>

        

        <script language="javascript">
          
            jQuery(document).ready(function () {
               // jQuery('#draftModal').modal('show');
                 
            });
            
        </script>

        <?php
        break;
    }
}

?>
        <!-- OTB patch - Check if current logged user is registered as property owner and if so hide other categories -->
        <?php
       
        $user_id = $sf_user->getGuardUser()->getProfile()->getUserId() ;
        if($otbhelper->checkUserRegisteredAsPropertyOwner($user_id)) {
            //this script code only executes if the user is registered as property owner - else not parsed to the browser
        ?>
        <script> 
           //check for option names
           //  $('.a_buildingcategory').prop('disabled', true); Abit static change me in future >>>
           $(".a_buildingcategory > option").each(function() {
              if(this.text === 'Category 1.C' || this.text === 'Category 2' || this.text === 'Category 3' || this.text === 'Category 4'){
                   
                   $(this).remove();
                   //console.log('Removed >>> '+this.text + ' ' + this.value);
              }
              else {
                  //do nothing
              }
             }); 
        </script>
        <?php } ?>
        
         <div id="t_hide_div">
        <script> 
            //OTB patch
              $('#a_builtup_error').hide();
              $('#a_no_of_floors_error').hide();
             var building_category = null;
             //parameters to check
             var no_of_floors = 0 ;
             var built_up_area_size = 0 ;
            //Get the value of Building category and store in browser local storage.
             //get the value of retrieved building category and store this value in browser local storage
             $(".a_buildingcategory").change(function(){
                   //alert($(".a_buildingcategory option:selected").text());
                   building_category = $(".a_buildingcategory option:selected").text() ;
                   localStorage.setItem('selected_category',building_category) ;
             }) ;
             //get value entered by user on keyup
           /*  $(".a_builtup").mouseout(function(){
                 alert("Value is >>> "+$(this).val()) ;
             }) ; */
             //function to validate built up area
                  //stored building category 
             function bc_check_builtup_area(built_up_area_size,building_category){
                     
                    if(building_category === 'Category 1.B'){
                        
                        if(parseInt(built_up_area_size) > 100){
                           $('#a_builtup_error').show();
                           console.log("The built up area is "+parseInt(built_up_area_size)+ "More than >> 100 ");
                           $('#submit_primary').attr("disabled","disabled") ;
                        }else {
                            //before allowing submission check common user tricks - check the number of floors
                            $('#a_builtup_error').hide();
                            if (parseInt($('#a_no_of_floors').val()) > 0 ){
                                $('#submit_primary').attr("disabled","disabled") ;
                            }else{
                              //  $('#a_no_of_floors_error').show();
                                console.log("The built up area is "+parseInt(built_up_area_size)+ "Less than >> 100 ");
                                $('#submit_primary').removeAttr("disabled") ; 
                             }
                        }
                        
                    }else if(building_category === 'Category 1.C'){
                         if(parseInt(built_up_area_size) < 100 || parseInt(built_up_area_size) > 200 ){
                           $('#a_builtup_error').show();
                           console.log("The built up area is wrong "+parseInt(built_up_area_size)+ " ");
                           $('#submit_primary').attr("disabled","disabled") ;
                        }else {
                            //before allowing submission check common user tricks - check the number of floors
                            $('#a_builtup_error').hide();
                              console.log("No of Floors category 1c built up area >>> "+parseInt($('#a_no_of_floors').val()));
                            if (parseInt($('#a_no_of_floors').val()) > 0 ){
                                $('#submit_primary').attr("disabled","disabled") ;
                            }else{
                              //  $('#a_no_of_floors_error').show();
                                console.log("The built up area is "+parseInt(built_up_area_size)+ " ");
                                $('#submit_primary').removeAttr("disabled") ; 
                             }
                        }
                    }
                    else if(building_category === 'Category 2' || building_category === 'Category 3' || building_category === 'Category 4'){
                         if(parseInt(built_up_area_size) < 200 ){
                           $('#a_builtup_error').show();
                           console.log("The built up area is wrong "+parseInt(built_up_area_size)+ " ");
                           $('#submit_primary').attr("disabled","disabled") ;
                        }else {
                            //before allowing submission check common user tricks - check the number of floors
                            $('#a_builtup_error').hide();
                              console.log("No of Floors category 2,3,4 built up area >>> "+parseInt($('#a_no_of_floors').val()));
                            if (parseInt($('#a_no_of_floors').val()) > 0 && parseInt($('#a_no_of_floors').val()) <= 2 ){
                                     $('#submit_primary').removeAttr("disabled") ; 
                            }else{                            
                                     $('#submit_primary').attr("disabled","disabled") ;
                             }
                        }
                    }
                    else {
                         
                        
                    }
         }
         function bc_check_no_of_floors(no_of_floors,building_category){            
              if(building_category === 'Category 1.B'){
                  
                        if(parseInt(no_of_floors) > 0 ){
                            $('#a_no_of_floors_error').show();
                            console.log("No of Floors Exceeds limit of 0 floors >> "+parseInt(no_of_floors));
                            $('#submit_primary').attr("disabled","disabled") ;
                        }else {
                            //avoid common user tricks -- check the built up area set
                            $('#a_no_of_floors_error').hide();
                             if(parseInt($('a_builtup').val()) > 100){
                                 
                                 $('#submit_primary').attr("disabled","disabled") ;
                             }else {
                                   
                                   console.log("No of Floors is okay >>"+parseInt(no_of_floors));
                                   $('#submit_primary').removeAttr("disabled") ;
                             }
                            
                            
                        }
                        
                    }else if(building_category === 'Category 1.C'){
                         if(parseInt($('a_builtup').val()) < 100 || parseInt($('a_builtup').val()) > 200 ){
                           $('#a_builtup_error').show();
                           //console.log("The built up area is wrong "+parseInt(built_up_area_size)+ " ");
                           $('#submit_primary').attr("disabled","disabled") ;
                        }else {
                            //before allowing submission check common user tricks - check the number of floors
                            $('#a_builtup_error').hide();
                            console.log("No of Floors category 1c >>> "+no_of_floors);
                            if (parseInt(no_of_floors) > 0 ){
                                $('#a_no_of_floors_error').show();
                                $('#submit_primary').attr("disabled","disabled") ;
                            }else{
                                $('#a_no_of_floors_error').hide();
                                //console.log("The built up area is "+parseInt(built_up_area_size)+ " ");
                                $('#submit_primary').removeAttr("disabled") ; 
                             }
                        }
                    }
                    else if(building_category === 'Category 2' || building_category === 'Category 3' || building_category === 'Category 4'){
                         if(parseInt($('a_builtup').val()) < 200 ){
                           $('#a_builtup_error').show();
                           //console.log("The built up area is wrong "+parseInt(built_up_area_size)+ " ");
                           $('#submit_primary').attr("disabled","disabled") ;
                        }else {
                            //before allowing submission check common user tricks - check the number of floors
                            $('#a_builtup_error').hide();
                            console.log("No of Floors category 2,3,4 >>> "+no_of_floors);
                            if (parseInt(no_of_floors) > 0 && parseInt(no_of_floors) <= 2 ){
                                $('#a_no_of_floors_error').show();
                                $('#submit_primary').attr("disabled","disabled") ;
                            }else{
                                $('#a_no_of_floors_error').hide();
                                //console.log("The built up area is "+parseInt(built_up_area_size)+ " ");
                                $('#submit_primary').removeAttr("disabled") ; 
                             }
                        }
                    }
                    else {
                        //nothing
                    }
         }
            
             //Binding focus in and out to detect when mouse enters and leaves an element
             //http://stackoverflow.com/questions/4585890/jquery-detecting-when-a-user-clicks-out-of-an-input-type-text-field
              //get built up area user entered 
             /* $(".a_builtup").focus(function() {
                  //  alert('in');
                }).blur(function() {
                   built_up_area_size = $(this).val();
                   bc_check_builtup_area(built_up_area_size);
                }); */
                 /*$(".a_builtup").mouseleave(function(){
                    var built_up_area_size = $(this).val();
                     var building_category = localStorage.getItem('selected_category') ;
                     bc_check_builtup_area(built_up_area_size,building_category);
                 }) ;*/
             //get no of floors user entered 
             /* $(".a_no_of_floors").focus(function() {
                  //  alert('in');
                }).blur(function() {
                   no_of_floors = $(this).val();
                }); */
                
               /* $(".a_no_of_floors").mouseleave(function(){
                     var floors = $(this).val();
                     var building_category = localStorage.getItem('selected_category') ;
                     bc_check_no_of_floors(floors,building_category);
                 }) ;*/
         
          
          
               /* var selectobject= $('.a_buildingcategory').attr("readonly","1") ;
            selectobject.attr("readonly","1") ;
                for (var i=0; i<selectobject.length; i++){
                if (selectobject.options[i].name == 'Category 1.C' )
                   selectobject.remove(i);
            }*/
             //function to check if a value is a valid integer
            function isInt(value) {
                var x = parseFloat(value);
                return !isNaN(value) && (x | 0) === x;
            }
           $('.a_buildingcoverage').attr("readonly","1") ;
          //for building coverage calculation 
          /*$(".a_builtup").keyup(function () {
              var plot_size = $(".a_plotsize").val();
              var built_up = $(".a_builtup").val(); 
              console.log("Plot sizsd "+plot_size);
              console.log("Plot built_up "+built_up);
             // var buildingcoverage = Math.round((built_up / plot_size ) * 100) ; 
              var buildingcoverage = built_up / plot_size ; 
              
               if (plot_size) {
                   $('.a_buildingcoverage').val(buildingcoverage);
               }
          });*/
          /* $(".a_plotsize").keyup(function () {
              var plot_size = $(".a_plotsize").val();
              var built_up = $(".a_builtup").val(); 
             // var buildingcoverage = Math.round((built_up / plot_size ) * 100) ; 
              var buildingcoverage = built_up / plot_size ; 
              
               if (built_up) {
                   $('.a_buildingcoverage').val(buildingcoverage);
               }
          });*/
          
          $("#btn_buildingcoverage").click(function (e) {
              var plot_size = $(".a_plotsize").val();
              var built_up = $(".a_builtup").val(); 
              var buildingcoverage = ((built_up / plot_size ) * 100).toFixed(2) ; 
              //var buildingcoverage = built_up / plot_size ; 
              
               if (built_up && plot_size) {
                   $('.a_buildingcoverage').val(buildingcoverage);
               }else {
                   alert("Please Add built up area and plot size information!");
               }
               e.preventDefault();
          });
          
           $("#btn_floorarearatio").click(function (e) {
              var plot_size = $(".a_plotsize").val();
              var grossfloorarea = $(".a_grossfloorarea").val(); 
              var floorarearatio = ((grossfloorarea / plot_size ) * 100).toFixed(2) ;  
              
               if (grossfloorarea && plot_size) {
                   $('.a_floorarearatio').val(floorarearatio);
               }else {
                   alert("Please Add gross floor area and plot size information!");
               }
               e.preventDefault();
          });
          
          //for floor area ratio calculation
          $('.a_floorarearatio').attr("readonly","1") ;
         /* $(".a_grossfloorarea").keyup(function () {
              var plot_size = $(".a_plotsize").val();
              var grossfloorarea = $(".a_grossfloorarea").val(); 
              var floorarearatio = Math.round((grossfloorarea / plot_size ) * 100) ; 
              
               if (isInt(plot_size) && isInt(grossfloorarea)) {
                   $('.a_floorarearatio').val(floorarearatio);
               }
          });*/
        </script>
       
              <script type="text/javascript">
          //Script to hide upload fields
         $(".hide_me").css("display","none");
         $("#success_upi").css("display","none");
         $("#failed_upi").css("display","none");
         $("#upi_connexition_error").css("display","none"); 
         $("#upi_exists").css("display","none");
            </script>
            
            <script type="text/javascript">
                function initChild(child) {
                $(child).parent().parent().parents().next().toggle(); return false;             
              }
              function initChild2(child) {
                $(child).parent().parents().next().toggle(); return false;             
              }
              
            </script>
            
            <!-- validate UPI -->
            <script type="text/javascript">
                $(".plot_number_upi").hover(function(){
                    
                    var element_id = $(this).attr("id");
                   // console.log("Key Up event "+$('.plot_number_upi').val());
                    $.ajax({
                        type : 'POST' ,
                        url: '<?php echo public_path() ?>index.php/forms/validateUPI' ,
                        data: { 
                               upi : $('.plot_number_upi').val(),
                               element: element_id ,
                               form_id : <?php echo $_GET['id'] ?> 
                        },        
                        success: function(response) 
                        {
                            //console.log("Before json request");

                             var parsed_data = JSON.parse(response);
                             var n = parsed_data ;
                             //
                             //console.log("Off test "+n);
                             if(n === 'invalid'){
                                 //disable submit
                                   $('#submit_primary').attr("disabled","disabled") ;
                                    $('#resume_submit_button_text').attr("disabled","disabled") ;
                                   $("#upi_exists").css("display","block");
                                   //////////// TODO - Add code to disable other elements if users provides invalid UPI
                                   /* $.ajax({
                                        type : 'POST' ,
                                        url: '<?php //echo public_path() ?>index.php/forms/getNonUPIFormElements' ,
                                        data: {                                               
                                               form_id : <?php// echo $_GET['id'] ?> 
                                        },        
                                        success: function(response) 
                                        { 
                                           disable_fields(response) ;
                                        },error: function(error) 
                                        {
                                            console.log("Error getting fields to disable ") ;
                                        }});*/
                                   //////////////
                                   
                             }else {
                                 
                                 //enable submit...
                                 $('#submit_primary').removeAttr("disabled") ;
                                  $('#resume_submit_button_text').removeAttr("disabled") ; 
                                 
                                 $("#upi_exists").css("display","none");
                               
                             }
                             
                        } ,
                        error: function(error) 
                        {
                            console.log("Error validate UPI ") ;
                        }
                     }) ;
                     
                     
                     function disable_fields(response){
                      // $("#element_2").hide() ;
                      $("element_2").style.display = 'none';
                        for(item in response){
                            console.log("Disable element "+item);
                             
                        }
                     } ;
                }) ;
                
            </script>
        </div>
        
      
