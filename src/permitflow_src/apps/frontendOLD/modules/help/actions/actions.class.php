<?php
/**
 * Help actions.
 *
 * Displays help content for clients
 *
 * @package    frontend
 * @subpackage sharedapplication
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
class helpActions extends sfActions
{
         /**
	 * Executes 'Faq' action
	 *
	 * Displays Frequently Asked Questions
	 *
	 * @param sfRequest $request A request object
	 */
  public function executeFaq(sfWebRequest $request)
  {
	$this->setLayout("layout");
  }
          
         /**
	 * Executes 'Contact' action
	 *
	 * Displays contact form for clients
	 *
	 * @param sfRequest $request A request object
	 */
	  public function executeContact(sfWebRequest $request)
	  {
		  $this->notifier = new notifications($this->getMailer());
		  $this->formid = 6;
		$this->setLayout("layout");
	  }
}
