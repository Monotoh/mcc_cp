<?php
  use_helper('I18N');
?>
<div class="breadcrumb-box">
<div class="container">
<ul class="breadcrumb">
<li>
<a href="index.html"><?php echo __('Home'); ?></a>
<span class="divider">/</span>
</li>
<li class="active"><?php echo __('Frequently Asked Questions'); ?></li>
</ul>
</div>
</div>

  <section id="headline">
    <div class="container">
      <h3><?php echo __('FAQ'); ?></h3>
      <p class="subtitle"><?php echo __('Frequently Asked Questions'); ?></p>
    </div>
  </section>


  <section class="container page-content" >
    <hr class="vertical-space2">

    <section id="main-content">
<div class="sixteen columns">



<?php
 $q = Doctrine_Query::create()
 ->from("Faq a");
 $basicfaqs = $q->execute();
 foreach($basicfaqs as $faq)
 {
?>


<!-- Question #1 -->
<span class="acc-trigger active"><a href="#"> <?php echo $faq->getQuestion(); ?></a></span>
<div class="acc-container">
<div class="content">
	<?php echo html_entity_decode($faq->getAnswer()); ?>
</div>
</div>

 <?php
		  }
		  ?>
</div>
</section><!-- end-main-conten -->
    <hr class="vertical-space2">
  </section>
  <!-- container -->
