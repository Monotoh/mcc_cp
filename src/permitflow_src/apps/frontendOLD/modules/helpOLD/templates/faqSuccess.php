<?php
  use_helper('I18N');
?>
<?php
 $q = Doctrine_Query::create()
 ->from("Faq a");
 $basicfaqs = $q->execute();
 $count = 1 ;
 foreach($basicfaqs as $faq)
 {
?>
 <?php if($count == 1){ ?>
  <div class="panel panel-default">
    <a class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#collapse<?php echo $count ?>" style="color:#0081c6;font-size:22px;"><?php echo $faq->getQuestion(); ?> </a> 
    <div class="panel-collapse collapse in" id="collapse<?php echo $count ?>"> 
     <div class="panel-body"> 

       <p><?php echo html_entity_decode($faq->getAnswer()); ?></p> 

     </div>
    </div> 
  </div>
 <?php } else { ?>
<div class="panel panel-default">
    <a class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#collapse<?php echo $count ?>" style="color:#0081c6;font-size:22px;"><?php echo $faq->getQuestion(); ?> </a> 
    <div class="panel-collapse collapse" id="collapse<?php echo $count ?>"> 
     <div class="panel-body"> 

       <p><?php echo html_entity_decode($faq->getAnswer()); ?></p> 

     </div>
    </div> 
  </div>
 <?php } ?>
    
 <?php
 $count++;
 }
		  ?>
