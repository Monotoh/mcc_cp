<?php
/**
 * _banner.php template.
 *
 * Displays banner
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>
<link rel="stylesheet" href="<?php echo public_path(); ?>permitflow/css/nguvu.css" type="text/css"  media="all">

<div class="container-fluid">

<div class="row">

<div class="ecitizen-about">
        <div class="ecitizen-hero ecitizen-hero-carousel ecitizen-js-carousel">

          <div style="position:absolute;z-index:3;width:100%;">
            <div class="col-lg-offset-7 col-lg-5 col-md-offset-6 col-md-6 col-sm-offset-0 col-sm-12 col-xs-offset-0 col-xs-12">
            <div class="text-box wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;margin:23px 0;">

                      <div class="headline">

                        <h3>SIGN IN</h3>

                      </div><!-- headline -->

                      <h4 class="text-default-color">Not Registered? <a class="signup" href="/index.php/signon/register">Sign up Now!</a></h4>

                      <form class="form-horizontal" action="/index.php/login" method="post">
                        <?php if(isset($form['_csrf_token'])): ?>
                        <?php echo $form['_csrf_token']->render(); ?>
                        <?php endif; ?>                                              
                        <div class="form-group">
                          <div class="col-sm-12">
                            <input type="text" class="form-control" id="signin_username" name="signin[username]" placeholder="Email">
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-sm-12">
                            <input type="password" class="form-control" id="signin_password" name="signin[password]" placeholder="Password">
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-sm-12">
                            <div class="checkbox">
                              <label>
                                <input type="checkbox"> Remember me
                              </label>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-sm-12">
                            <button type="submit" class="btn btn-white">Sign in <i class="fa fa-angle-right"></i></button>
                          </div>
                        </div>
                      </form>

                    </div>
          </div>
          </div>

          <div class="nguvu-scroll-carousel">
            <ul class="nguvu-scroll-carousel-content">
        <?php
        $q = Doctrine_Query::create()
           ->from("ApSettings a")
           ->where("a.id = 1")
           ->orderBy("a.id DESC");
        	$aplogo = $q->fetchOne();

			foreach($banners as $banner)
				{
            if($aplogo && $aplogo->getAdminImageUrl())
            {
                $file = $aplogo->getUploadDirWeb().$banner->getImage();
            }
            else
            {
            	$file = public_path()."asset_uplds/".$banner->getImage();
            }
			?>
              <li class="nguvu-scroll-carousel-item ecitizen-about-carousel-item-4" style="background-image:url(<?php echo $file; ?>);">
                <div class="maia-aux" style="z-index:2;position:relative;" >
                  <div class="maia-cols">
                    <div class="maia-col-5">
                      <div class="ecitizen-vertical-center">
                        <div class="ecitizen-content">
                          <h1>
                             <?php echo $banner->getDescription(); ?>
                          </h1>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>

             <?php
				}
			?>
            </ul>
          </div>
          <div class="ecitizen-carousel-selector nguvu-selector" data-autoplay-interval="7000"
          data-enable-autoplay="true" data-enable-wrap="true">
            <div class="nguvu-selector-content">
           <?php
	        $q = Doctrine_Query::create()
	           ->from("ApSettings a")
	           ->where("a.id = 1")
	           ->orderBy("a.id DESC");
	        $aplogo = $q->fetchOne();

           foreach($banners as $banner)
				{
			?>
              <a class="nguvu-selector-control-item nguvu-selector-control-item-selected"></a>
             <?php
				}
			?>
            </div>
          </div>
      </div>
</div>
</div>

<script src="<?php echo public_path(); ?>permitflow/js/jquery.nguvu.js"></script>
