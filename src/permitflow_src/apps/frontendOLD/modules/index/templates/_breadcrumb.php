<?php
/**
 * _breadcrumb.php template.
 *
 * Displays breadcrumbs
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>
<?php
if($url != "" && !$sf_user->isAuthenticated())
{
?>
<ul class="breadcrumb">
	<li><a href='/index.php/index/index/id/1'>Home</a> <span class="divider">/</span></li><?php if(strlen($url) > 0){ ?><li><?php  echo html_entity_decode($url); ?></li><?php } ?>
</ul>
<?php
}
if($sf_context->getModuleName() == "sfGuardRegister")
{
?>
<ul class="breadcrumb">
        <li><a>Client Registration: </a> </li>
        <li class="active"><a><small>Step 1</small></a> <span class="divider">/</span></li>
        <li><a>Step 2</a> <span class="divider">/</span></li>
        <li><a >Step 3</a> </li>
 </ul>
<?php
}
if($sf_context->getModuleName() == "mfRegister" && $sf_context->getActionName() == "registerDetails")
{
?>
<ul class="breadcrumb">
        <li><a>Client Registration: </a> </li>
        <li><a><small>Step 1</small></a> <span class="divider">/</span></li>
        <li class="active"><a>Step 2</a> <span class="divider">/</span></li>
        <li><a >Step 3</a> </li>
 </ul>
<?php
}
if($sf_context->getModuleName() == "mfRegister" && $sf_context->getActionName() == "notification")
{
?>
<ul class="breadcrumb">
        <li><a>Client Registration: </a> </li>
        <li><a><small>Step 1</small></a> <span class="divider">/</span></li>
        <li><a>Step 2</a> <span class="divider">/</span></li>
        <li class="active"><a >Step 3</a> </li>
 </ul>
<?php
}
?>
