<?php
/**
 * _sidemenu.php template.
 *
 * Displays Side Menu
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
  use_helper('I18N');
  $translation = new Translation();
?>

<?php
if($sf_user->isAuthenticated() && $sf_user->getGuardUser()->getId() != '1')
{
?>

       <script src="<?php echo public_path(); ?>asset_unified/js/jquery.app.js"></script>

        <div id="navigation">
            <!-- Navigation Menu-->
            <ul class="navigation-menu">

       <li <?php if($sf_context->getModuleName() == "dashboard" && $sf_context->getActionName() == "index"){ ?>class="active"<?php } ?>>
       <a href="<?php echo public_path(); ?>index.php/dashboard"><i class="md md-dashboard <?php if($sf_context->getModuleName() == "dashboard" && $sf_context->getActionName() == "index"){ ?>icon-white<?php } ?>"></i>
	   <span><?php echo __('Dashboard'); ?></span></a>
       </li>

       <li <?php if(($sf_context->getActionName() == "groups" && $sf_context->getModuleName() == "application") || ($sf_context->getModuleName() == "forms")){ echo "class='active'"; } ?>"><a href="<?php echo public_path(); ?>index.php/application/groups"><i class="md md-mode-edit"></i><?php echo __('Make Application'); ?></a></li>

	   <li class="<?php if(($sf_context->getModuleName() == "application" && $sf_context->getActionName() == "index") || ($sf_context->getModuleName() == "forms")){ ?>active<?php } ?>">
       <a href="<?php echo public_path(); ?>index.php/application/index"><i class="md md-view-headline <?php if($sf_context->getModuleName() == "application" && $sf_context->getActionName() == "index"){ ?>icon-white<?php } ?>"></i>
       <span> <?php echo __('Applications History'); ?></span></a>
       </li>

        <!--<li>
            <a href="<?php echo public_path(); ?>index.php/permits/index"><i class="md md-print"></i>
                <span> <?php echo __('Downloads'); ?></span></a>
        </li>-->

      </ul>
      <!-- End navigation menu -->
  </div>




<?php
}
?>
