<?php
/**
 * _banner.php template.
 *
 * Displays banner
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>

<div class="edu2_main_bn_wrap">
	<div id="owl-demo-main" class="owl-carousel owl-theme">
		<div class="item">
			<figure>
				<img src="<?php echo public_path(); ?>rwanda_frontend/extra-images/kigali-by-night.jpg" alt=""/>
				<figcaption>
					<span><?php echo __('welcome to the'); ?></span>
					<h2><?php echo __('rwanda building permit system'); ?></h2>
					<p><?php echo __('Get approved building permits online.'); ?></p>
					<a href="/index.php/signon/login" class="btn-1"><?php echo __('Get Started'); ?></a>
				</figcaption>
			</figure>
		</div>
	</div>
</div>

<div class="kf_content_wrap">
	<!--COURSE OUTER WRAP START-->
	<div class="kf_course_outerwrap">
		<div class="container">

			<div class="row">

				<div class="col-md-12">
					<div class="row">
						<!--COURSE CATEGORIES WRAP START-->
						<div class="kf_cur_catg_wrap">
							<!--COURSE CATEGORIES WRAP HEADING START-->
							<div class="col-md-12">
								<div class="kf_edu2_heading1">
									<h3><?php echo __('Available Services'); ?></h3>
								</div>
							</div>
							<!--COURSE CATEGORIES WRAP HEADING END-->

							<!--COURSE CATEGORIES DES START-->
							<div class="col-md-4">
								<div class="kf_cur_catg_des color-1">
									<span><i class="fa fa-wrench"></i></span>
									<div class="kf_cur_catg_capstion">
										<h5><?php echo __('Building Permit'); ?></h5>
										<p><?php echo __('Submit building plans for online approval.'); ?></p>
									</div>
								</div>
							</div>
							<!--COURSE CATEGORIES DES END-->

							<!--COURSE CATEGORIES DES START-->
							<div class="col-md-4">
								<div class="kf_cur_catg_des color-2">
									<span><i class="fa fa-search"></i></span>
									<div class="kf_cur_catg_capstion">
										<h5><?php echo __('Inspections'); ?></h5>
										<p><?php echo __('Notify your district office for site inspections.'); ?></p>
									</div>
								</div>
							</div>
							<!--COURSE CATEGORIES DES END-->

							<!--COURSE CATEGORIES DES START-->
							<!--<div class="col-md-6">
								<div class="kf_cur_catg_des color-3">
									<span><i class="icon-chemistry29"></i></span>
									<div class="kf_cur_catg_capstion">
										<h5>OCCUPANCY PERMIT</h5>
										<p>Latest technologies online courses are available with new courses. </p>
									</div>
								</div>
							</div>-->
							<!--COURSE CATEGORIES DES END-->

							<!--COURSE CATEGORIES DES START-->
							<div class="col-md-4">
								<div class="kf_cur_catg_des color-4">
									<span><i class="fa fa-institution"></i></span>
									<div class="kf_cur_catg_capstion">
										<h5><?php echo __('OCCUPANCY PERMIT'); ?></h5>
										<p><?php echo __('Ready for occupation? Apply for occupancy permits online.'); ?></p>
									</div>
								</div>
							</div>
							<!--COURSE CATEGORIES DES END-->

							<!--
							<div class="col-md-6">
								<div class="kf_cur_catg_des color-5">
									<span><i class="icon-cocktail32"></i></span>
									<div class="kf_cur_catg_capstion">
										<h5>Inspections</h5>
										<p>Get the best eating education and practice by taking online courses.</p>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="kf_cur_catg_des color-6">
									<span><i class="fa fa-line-chart"></i></span>
									<div class="kf_cur_catg_capstion">
										<h5>Creative Arts & Media</h5>
										<p>Come and explore your creative arts and media by going further.</p>
									</div>
								</div>
							</div>-->

						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<!--COURSE OUTER WRAP END-->

</div>
<!--<div class="sixteen columns">
		<div class="flexslider">
			<ul class="slides">
			<?php
                $q = Doctrine_Query::create()
                   ->from("ApSettings a")
                   ->where("a.id = 1")
                   ->orderBy("a.id DESC");
                $aplogo = $q->fetchOne();

				foreach($banners as $banner)
				{
	                if($aplogo && $aplogo->getAdminImageUrl())
	                {
	                    $file = $aplogo->getUploadDirWeb().$banner->getImage();
	                }
	                else
	                {
	                	$file = public_path()."asset_uplds/".$banner->getImage();
	                }
				?>
				        <li> <img src="<?php echo $file; ?>" alt="">
				          <article class="slide-caption">
				            <h3><?php echo $banner->getTitle(); ?></h3>
				            <p><?php echo $banner->getDescription(); ?></p>
				          </article>
				        </li>

				<?php
				}
				?>
</ul>
</div>
</div>-->
