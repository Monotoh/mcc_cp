<?php 
 use_helper('I18N') ;
?> 
<div id="content" class="row">
            <div class="col-md-12">					
                <div class="columns-1" id="main-content" role="main">
                    <div class="portlet-layout row-fluid">
                        <div class="portlet-column portlet-column-only span12" id="column-1">
                            <div class="portlet-dropzone portlet-column-content portlet-column-content-only" id="layout-column_column-1">

                                <div class="portlet-boundary portlet-boundary_homepagepopular_WAR_rolserviceslistportlet_  portlet-static portlet-static-end portlet-borderless homepage-popular-portlet " id="p_p_id_homepagepopular_WAR_rolserviceslistportlet_" >
                                    <span id="p_homepagepopular_WAR_rolserviceslistportlet"></span>
                                    <div class="portlet-borderless-container" style="">

                                        <div class="portlet-body">

                                            <div class="home-content">
                                                <div class="container">
                                                    <div class="text-center">
                                                        <!--<div class="irambo-home-logo"><img src="<?php echo public_path(); ?>irembo/themes/images/ebpmis.png" width="355" height="75" alt=""/> </div> -->
                                                        <div class="irambo-home-logo">
                                                            <!--<h2 style="color:#00FF00;"> <u>Upgrade Notice !</u> </h2>
                                                            <h4>
                                                            <span style="color:#00FF00;">
                                                                Kindly note that following an upgrade of the BPMIS, the system will no longer be 
                                                            allowing users to 
                                                                </span>-->
                                                            </h4>
                                                        </div>
                                                        <div class="search-box"> 
                                                            <div class="input-group"> <input class="form-control" name="searchKey" id="searchKey__" type="text" onkeydown="" placeholder='<?php echo __('Service Search') ?>'>
                                                                <div class="input-group-btn">
                                                                    <button class="btn" type="button" id="btnSearch__" name="btnSearch"  data-toggle="dropdown">
                                                                       <?php echo __('Search') ?>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="popular-service-head"><?php echo __('Our Building Permit Services') ?></div>
                                                        <div class="home-page-small-content">
                                                            <!-- Test2-->
                                                            <div class="row">

                                                                <div class="col-sm-4">
                                                                    <a href="<?php echo url_for('@dashboard') ?>" class="home-page-links">
                                                                        <span class="text-small"><?php echo __('Construction Permit') ?></span>
                                                                       <span class="home-link">
                                                                           </span>
                                                                    </a>
                                                                </div>

                                                                <div class="col-sm-4"><a href="<?php echo url_for('@dashboard') ?>" class="home-page-links">
                                                                        <span class="text-small"><?php echo __('Extension Permit') ?></span>
                                                                      <span class="home-link">
                                                                           </span>
                                                                    </a>
                                                                </div>




                                                                <div class="col-sm-4"><a href="<?php echo url_for('@dashboard') ?>" class="home-page-links">
                                                                        <span class="text-small"><?php echo __('Refurbshiment of existing building with structural alteration') ?></span>
                                                                      <span class="home-link">
                                                                           </span>
                                                                    </a>
                                                                </div>

                                                            </div>
                                                            <div class="row">

                                                                <div class="col-sm-4"><a href="<?php echo url_for('@dashboard') ?>" class="home-page-links">
                                                                        <span class="text-small"><?php echo __('Refurbshiment of existing building without structural alteration') ?></span>
                                                                        <span class="home-link">
                                                                           </span></a>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <a href="<?php echo url_for('@dashboard') ?>" class="home-page-links">
                                                                        <span class="text-small"><?php echo __('Demolation - Full / Partial') ?></span>
                                                                       <span class="home-link">
                                                                           </span>
                                                                    </a>
                                                                </div>
                                                                <div class="col-sm-4"><a href="<?php echo url_for('@dashboard') ?>" class="home-page-links">
                                                                        <span class="text-small"><?php echo __('Occupancy Permit Application') ?></span><span class="home-link">
                                                                           </a>
                                                                </div>
                                                                <div class="col-sm-4"><a href="<?php echo url_for('@dashboard') ?>" class="home-page-links">
                                                                        <span class="text-small"><?php echo __('Change of Building Use') ?></span>
                                                                        <span class="home-link">
                                                                           </span>
                                                                    </a>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <a href="<?php echo url_for('@dashboard') ?>" class="home-page-links">
                                                                        <span class="text-small"><?php echo __('Inspection Notice') ?></span>
                                                                         <span class="home-link">
                                                                           </span>
                                                                    </a>
                                                                </div>
                                                            </div>

                                                            
                                                        </div>
                                                    </div>
                                                </div>                                             
                                            </div>
                                        </div>
                                    </div>
                                   
                                </div>
                               
                                <!-- special booklet -->
                                <div class="portlet-boundary portlet-boundary_56_  portlet-static portlet-static-end portlet-borderless portlet-journal-content " id="p_p_id_56_INSTANCE_lY9yu7rqU32N_" >
                                    <span id="p_56_INSTANCE_lY9yu7rqU32N"></span>
                                    <div class="portlet-borderless-container" style="">
                                        <div class="portlet-body">
                                            <div class="journal-content-article">
                                                 <!-- my container -->
                                                <div class="container">
                                                   <div id="content" class="row"> 
                    <div class="col-md-12"> 
                        <div class="columns-1" id="main-content" role="main"> 
                            <div class="portlet-layout row-fluid"> <div class="portlet-column portlet-column-only span12" id="column-1"> 
                                    <div class="portlet-dropzone portlet-column-content portlet-column-content-only" id="layout-column_column-1"> 
                                        <div class="portlet-boundary portlet-boundary_56_ portlet-static portlet-static-end portlet-borderless portlet-journal-content " id="p_p_id_56_INSTANCE_7mAyand1Rbel_" > <span id="p_56_INSTANCE_7mAyand1Rbel"></span> <div class="portlet-borderless-container" style=""> 
                                                <div class="portlet-body">
                                                    <h4 class="text-center"><font style="color:#0081c6;"> <u><?php echo __('KNOWLEDGE CENTER'); ?>  </u></font> </h4>
                                                    <div class="journal-content-article"> 
                                                        <div class="row faq"> 
                                                            <div class="col-md-12"> 
                                                               <div class="panel panel-default">
                                                                        <div class="panel-body"> 
                                                                          <?php
                                                    $q = Doctrine_Query::create()
                                                    ->from("Booklet a");
                                                    $booklet = $q->execute();
                                                    $count = 1 ;
                                                    foreach($booklet as $faq)
                                                    {
                                                   ?>
                                                    <?php if($count == 1){ ?>
                                                     <div class="panel panel-default">
                                                       <a class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#collapse<?php echo $count ?>" style="color:#0081c6;font-size:22px;"><?php echo $faq->getTitle(); ?> </a> 
                                                       <div class="panel-collapse collapse in" id="collapse<?php echo $count ?>"> 
                                                        <div class="panel-body"> 

                                                          <p><?php echo html_entity_decode($faq->getContent()); ?></p> 

                                                        </div>
                                                       </div> 
                                                     </div>
                                                    <?php } else { ?>
                                                   <div class="panel panel-default">
                                                       <a class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#collapse<?php echo $count ?>" style="color:#0081c6;font-size:22px;"><?php echo $faq->getTitle(); ?> </a> 
                                                       <div class="panel-collapse collapse" id="collapse<?php echo $count ?>"> 
                                                        <div class="panel-body"> 

                                                          <p><?php echo html_entity_decode($faq->getContent()); ?></p> 

                                                        </div>
                                                       </div> 
                                                     </div>
                                                    <?php } ?>

                                                    <?php
                                                    $count++;
                                                    }
                                                                     ?>

                                                                        </div>
                                                                    </div>
                                                            </div> 
                                                        </div> 
                                                    </div> 
                                                    <div class="entry-links"> </div> 
                                                </div> 
                                            </div> </div> 
                                    </div> 
                                </div> </div> </div> 
                        <form action="#" id="hrefFm" method="post" name="hrefFm"> <span>
                                    
                            </span> </form> 
                    </div>
                </div>
                                                </div>
                                                <!-- end container -->

                                            </div>
                                             
                                        </div> 
                                    </div> 
                                </div>
                                <!-- end booklet -->
                                
                                
                                <div class="">
                              
                                <div class="portlet-boundary portlet-boundary_56_  portlet-static portlet-static-end portlet-borderless portlet-journal-content " id="p_p_id_56_INSTANCE_lY9yu7rqU32N_" >
                                    <span id="p_56_INSTANCE_lY9yu7rqU32N"></span>
                                    
                                    <div class="portlet-borderless-container" style="">
                                        <div class="portlet-body">
                                            <div class="journal-content-article">
                                                 <div class="carousel slide kcc-tower" data-interval="4000" data-ride="carousel" id="myCarousel"><!-- Indicators --><!--<img src="/themes/images/wats-new-image.jpg" width="272" height="75" alt=""/>--> 
                                                   <div class="carousel-caption"> 
                                                       <h1><img alt="" height="" src="<?php echo public_path(); ?>irembo/themes/images/wats-new.jpg" width="" /><?php echo __('Our Services Info') ?></h1> 
                                                   </div> <!-- Wrapper for slides --> 
                                                       <div class="carousel-inner text-center"> 
                                                           <div class="item active">
                                                           <div class="carousel-caption"> 
                                                               <h3><?php echo __('BUILDING PERMIT') ?></h3> 
                                                               <p><?php echo __('Submit building plans for online approval') ?></p> 
                                                           </div> 
                                                           </div> 
                                                           <div class="item"> 
                                                               <div class="carousel-caption"> 
                                                                   <h3><?php echo __('INSPECTIONS') ?></h3>
                                                                  
                                                                   <p><?php echo __('Notify your district office for site inspections') ?></p> 
                                                                   
                                                               </div> 
                                                           </div> 
                                                            <div class="item"> 
                                                               <div class="carousel-caption"> 
                                                                   <h3><?php echo __('OCCUPANCY PERMIT') ?></h3>
                                                                   <p> <?php echo __('Ready for occupation? Apply for occupancy permits online') ?> </p> 
                                                               </div> 
                                                           </div> 
                                           
                                                    </div> 
                                                    <!-- Controls --><a class="left carousel-control" data-slide="prev" href="#myCarousel">
                                                        <span>
                                                            <img alt="" src="<?php echo public_path(); ?>irembo/themes/images/home-prev.png" /></span> </a> 
                                                    <a class="right carousel-control" data-slide="next" href="#myCarousel"> 
                                                        <span><img alt="" src="<?php echo public_path(); ?>irembo/themes/images/home-next.png" /></span> </a></div>
                                            </div> 
                                            <div class="entry-links"> </div> 
                                        </div> 
                                    </div> 
                                </div>
                               </div> 
                            </div> 
                        </div> 
                    </div> </div> 
                <form action="#" id="hrefFm" method="post" name="hrefFm"> <span></span> </form> 
            
            </div> 
        </div> 
