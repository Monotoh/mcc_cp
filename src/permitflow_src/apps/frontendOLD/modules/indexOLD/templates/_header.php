<?php
/**
 * _header.php template.
 *
 * Displays Header
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

 use_helper('I18N');
?>
    <!-- register Modal -->
    <div class="modal fade" id="reg-box" tabindex="-1" role="dialog">
        <div class="modal-dialog">
        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-content">
            	<!--SIGNIN AS USER START-->
                <div class="user-box">
                	<h2><?php echo __('Sign up as a User'); ?></h2>
                    <!--FORM FIELD START-->
                    <div class="form">
                        <div class="input-container">
                            <input type="text" placeholder="Name">
                            <i class="fa fa-user"></i>
                        </div>
                        <div class="input-container">
                            <input type="text" placeholder="E-mail">
                            <i class="fa fa-envelope-o"></i>
                        </div>
                        <div class="input-container">
                            <input type="password" placeholder="Password">
                            <i class="fa fa-unlock"></i>
                        </div>
                        <div class="input-container">
                            <label>
                                <span class="radio">
                                    <input type="checkbox" name="foo" value="1" checked>
                                    <span class="radio-value" aria-hidden="true"></span>
                                </span>
                                <span><?php echo __('Remember me'); ?></span>
                            </label>
                        </div>
                        <div class="input-container">
                            <button class="btn-style"><?php echo __('Sign Up'); ?></button>
                        </div>
                    </div>
                    <!--FORM FIELD END-->
                    <!--OPTION START-->
                    <div class="option">
                        <h5>Or Using</h5>
                    </div>
                    <!--OPTION END-->
                    <!--OPTION START-->
                    <div class="social-login">
                        <a href="#" class="google"><i class="fa fa-google-plus"></i><?php echo __('Google Account'); ?></a>
                        <a href="#" class="facebook"><i class="fa fa-facebook"></i><?php echo __('Facebook Account'); ?></a>
                    </div>
                    <!--OPTION END-->
                </div>
                <!--SIGNIN AS USER END-->
                <div class="user-box-footer">
                    <?php echo __('Already have an account?'); ?> <a href="#"><?php echo __('Sign In'); ?></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- register Modal end-->
    
    <!-- SIGNIN MODEL START -->
    <div class="modal fade" id="signin-box" tabindex="-1" role="dialog">
        <div class="modal-dialog">
        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-content">
                <div class="user-box">
                    <h2><?php echo __('Sign In'); ?></h2>
                    <!--FORM FIELD START-->
                    <div class="form">
                        <div class="input-container">
                            <input type="text" placeholder="E-mail">
                            <i class="fa fa-envelope-o"></i>
                        </div>
                        <div class="input-container">
                            <input type="password" placeholder="Password">
                            <i class="fa fa-unlock"></i>
                        </div>
                        <div class="input-container">
                            <label>
                                <span class="radio">
                                    <input type="checkbox" name="foo" value="1" checked>
                                    <span class="radio-value" aria-hidden="true"></span>
                                </span>
                                <span><?php echo __('Remember me'); ?></span>
                            </label>
                        </div>
                        <div class="input-container">
                            <button class="btn-style"><?php echo __('Sign In'); ?></button>
                        </div>
                    </div>
                    <!--FORM FIELD END-->
                    <!--OPTION START-->
                    <div class="option">
                        <h5>Or Using</h5>
                    </div>
                    <!--OPTION END-->
                    <!--OPTION START-->
                    <div class="social-login">
                        <a href="#" class="google"><i class="fa fa-google-plus"></i><?php echo __('Google Account'); ?></a>
                        <a href="#" class="facebook"><i class="fa fa-facebook"></i><?php echo __('Facebook Account'); ?></a>
                    </div>
                    <!--OPTION END-->
                
                </div>
                <div class="user-box-footer">
                    <p><?php echo __('Don\'t have an account?'); ?><br><a href="#"><?php echo __('Sign up as a User'); ?></a></p>
                </div>
                <div class="clearfix"></div>
            </div>
			
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- SIGNIN MODEL END -->
    
	<!--<div id="sidr">
		<div class="logo_wrap">
			<a href="#"><img src="<?php echo public_path(); ?>rwanda_frontend/extra-images/logo.png" alt=""></a>
		</div>
		<div class="clearfix clear"></div>
		<div class="kf-sidebar">
			<div class="widget widget-search">
				<h2>Search Course</h2>
				<form>
					<input type="search" placeholder="Keyword...">
				</form>
			</div>

			<div class="widget widget-archive ">
				<h2>Archives</h2>
				<ul class="sidebar_archive_des">
					<li>
						<a href=""><i class="fa fa-angle-right"></i>January 2016</a>
					</li>
					<li>
						<a href=""><i class="fa fa-angle-right"></i>February 2016</a>
					</li>
					<li>
						<a href=""><i class="fa fa-angle-right"></i>March 2016</a>
					</li>
					<li>
						<a href=""><i class="fa fa-angle-right"></i>April 2016</a>
					</li>
					<li>
						<a href=""><i class="fa fa-angle-right"></i>May 2016</a>
					</li>
					<li>
						<a href=""><i class="fa fa-angle-right"></i>June 2016</a>
					</li>
					<li>
						<a href=""><i class="fa fa-angle-right"></i>August 2016</a>
					</li>
				</ul>
			</div>

			<p class="copy-right-sidr">Design and Developed by KodeForest @ All Rights Reserved by KodeForest</p>
		</div>
	</div>-->
    	<!--HEADER START-->
    	<header id="header_2">
    		<!--kode top bar start-->
    		<div class="top_bar_2">
	    		<div class="container">
	    			<div class="row">
	    				<div class="col-md-5">
	    					<div class="pull-left">
	    						<!--<em class="contct_2"><i class="fa fa-phone"></i> Call Us  on 0800 123 46 4747</em>-->
	    						<em class="contct_2"><i class="fa fa-phone"></i> <?php echo __('Contact Us on');?> info@bpmis.gov.rw</em>
	    					</div>
	    				</div>
	    				<div class="col-md-7">
    						<div class="lng_wrap">
	    						<div class="dropdown">
								<?php
									$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
									mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

									$sql = "SELECT * FROM ext_locales ORDER BY local_title ASC";
									$rows = mysql_query($sql, $dbconn);

									if(mysql_num_rows($rows) > 1)
									{
									?>
										<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
										<i class="fa fa-globe"></i><?php echo __('Language'); ?>
											<span class="caret"></span>
										</button>
										<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
										<?php
										while($row = mysql_fetch_assoc($rows))
										{?>
											<li><a href="/index.php/index/setlocale/code/<?php echo $row['locale_identifier']; ?>">
										<?php
										  if($row['locale_identifier'] == "en_US"){
										  ?>
											<i><img src="<?php echo public_path(); ?>rwanda_frontend/images/english.jpg" alt=""></i>
										  <?php
										  }else if($row['locale_identifier'] == "fr_FR"){
										  ?>
											<i><img src="<?php echo public_path(); ?>rwanda_frontend/images/french.jpg" alt=""></i>
										  <?php
										  }else if($row['locale_identifier'] == "kin"){
										  ?>
											<i><img src="<?php echo public_path(); ?>rwanda_frontend/images/rwanda.jpg" alt=""></i>
										  <?php
										  }
										  ?>
											<?php echo $row['local_title'];?></a></li>
										  <?php
										}
										?>
									</ul>
									<?php
									}
								  ?>
								</div>
	    					</div>
    						<ul class="login_wrap">

							 <?php
								if($sf_user->isAuthenticated())
								{
							  ?>
									<li><a href="<?php echo url_for('signon/logout') ?>"><i class="fa fa-sign-in"></i><?php echo __('Logout') ?></a></li>
							  <?php
								}
								 else
								{
								?>
									<li><a href="<?php echo url_for('@sf_guard_register'); ?>"><i class="fa fa-user"></i><?php echo __('Register'); ?></a></li>
									<li><a href="<?php echo url_for('signon/login') ?>"><i class="fa fa-sign-in"></i><?php echo __('Sign In'); ?></a></li>
								 <?php
								}
								?>

    						</ul>	    					
	    					<ul class="top_nav">
	    						<li><a href="/index.php/news"><?php echo __('News'); ?></a></li>
	    						<li><a href="/index.php/help/faq"><?php echo __('FAQs'); ?></a></li>
	    						<li><a href="/index.php/help/contact"><?php echo __('Contact Us'); ?></a></li>
	    					</ul>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
    		<!--kode top bar end-->
        	
	    	<!--kode navigation start-->
    		<div class="kode_navigation">
    			<div id="mobile-header">
                	<a id="responsive-menu-button" href="#sidr-main"><i class="fa fa-bars"></i></a>
                </div>
    			<div class="container">
    				<div class="row">
    					<div class="col-md-4">
    						<div class="logo_wrap">
							  <?php
								$q = Doctrine_Query::create()
								   ->from("ApSettings a")
								   ->where("a.id = 1")
								   ->orderBy("a.id DESC");
								$aplogo = $q->fetchOne();
								if($aplogo && $aplogo->getAdminImageUrl())
								{
								  $file = $aplogo->getUploadDirWeb().$aplogo->getAdminImageUrl();
									?>
									  <!--<img src="<?php echo $file; ?>" id="img-logo" alt="logo">-->
									<a href="#"><img src="<?php echo public_path(); ?>rwanda_frontend/extra-images/logo_2.png" alt=""></a>
									<?php
								}
								?>
    						</div>
    					</div>
						<!-- Navigation starts
						 ================================================== -->
						 <?php include_component('index', 'menu') ?>
							<!-- /nav-wrap -->
							<!-- Navigation ends
						================================================== -->
    				</div>
    			</div>
    		</div>
    		<!--kode navigation end-->
		</header>
		<!--HEADER END-->

