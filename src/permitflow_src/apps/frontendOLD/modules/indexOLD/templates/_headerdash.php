<?php
/**
 * _header.php template.
 *
 * Displays Header
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
  use_helper('I18N');
?>

<!-- Navigation Bar-->
<header id="topnav">
<?php
if (!$agency_code or $agency_code = "COK"){
	$style_bg_color = "background-color: #0687C1";
}else{
}
?>
    <div class="topbar-main" style="<?php echo $style_bg_color; ?>">
        <div class="container">
            <!-- Logo container-->
            <div class="logo">
              <?php
              $q = Doctrine_Query::create()
                 ->from("ApSettings a")
                 ->where("a.id = 1")
                 ->orderBy("a.id DESC");
              $apsettings = $q->fetchOne();
              if($apsettings)
              {
              ?>
              <h1> <img src="<?php echo public_path(); ?>irembo/themes/images/ebpmis_home.png" height="65px" alt="" /></h1>
			  <!--<a href="/index.php" class="logo"><?php echo $apsettings->getOrganisationName(); ?></a>-->
              <?php
              }
              else
              {
              ?>
              <a href="/index.php" class="logo"><?php echo sfConfig::get('app_organisation_name') ?></a>
              <?php
              }
              ?>

            </div>
            <!-- End Logo container-->
            <?php
                      $q = Doctrine_Query::create()
                      ->from('sfGuardUserProfile a')
                      ->where('a.user_id = ?', $sf_user->getGuardUser()->getId());
                      $user = $q->fetchOne();
                      ?>

            <div class="menu-extras">
                <ul class="nav navbar-nav navbar-right pull-right">
                    <li class="dropdown">
                        <a href="" class="dropdown-toggle waves-effect waves-light profile" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-arrow-down"></i><?php  echo $user->getFullname(); ?></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo public_path(); ?>index.php/settings"><i class="ti-user m-r-5"></i> <?php echo __('My Profile'); ?></a></li>
                            <li><a href="<?php echo url_for('signon/logout') ?>"><i class="ti-power-off m-r-5"></i> <?php echo __('Log Out'); ?></a></li>
                        </ul>
                    </li>
                </ul>


                <div class="menu-item">
                    <!-- Mobile menu toggle-->
                    <a class="navbar-toggle">
                        <div class="lines">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </a>
                    <!-- End mobile menu toggle-->
                </div>
            </div>

        </div>
    </div>
    <!-- End topbar -->


    <!-- Navbar Start -->
    <div class="navbar-custom">
        <div class="container">
          <!-- Navigation Call-->
          <?php include_component('index', 'sidemenu') ?>
    </div>
    </div>
</header>
<!-- End Navigation Bar-->


















<?php /*
     <div class="logopanel hidden-lg hidden-md" style="background:#fff;  border-bottom: 1px solid #eee;">
         <h1 class="pull-left"> <a href="/index.php"><img alt="" src="/assets_unified/images/ecitizen-logo.png"></a></h1>
         <div class="btn-group pull-right" style="margin:5px;">
              <a href="<?php echo url_for('signon/logout') ?>" class="btn btn-default dropdown-toggle">
                Logout
              </a>
            </div>
        <div class="clearfix"></div>

        </div>
    <div class="headerbar full-width">
        <div class="logopanel hidden-sm hidden-xs">
         <h1> <a href="/index.php"><img alt="" src="/assets_unified/images/ecitizen-logo.png"></a></h1>
        </div>
        <ul class="headermenu pull-right">
            <li>
                <?php
                $q = Doctrine_Query::create()
                    ->from('Communications a')
                    ->leftJoin('a.FormEntry b')
                    ->where('a.reviewer_id <> ?', "")
                    ->andWhere('a.messageread = ?', 0)
                    ->andWhere('b.user_id = ?', $sf_user->getGuardUser()->getId());
                $newmessages = $q->count();
                ?>
                <div class="btn-group hidden-sm hidden-xs">
                    <button class="btn btn-default dropdown-toggle tp-icon" data-toggle="dropdown" onClick="window.location='<?php echo public_path("/index.php/messages/index"); ?>';">
                        <i class="glyphicon glyphicon-envelope"></i>
                        <?php if($newmessages){ ?>
                            <span class="badge"><?php echo $newmessages; ?></span>
                        <?php } ?>
                    </button>
                </div>
            </li>
            <li>
                <?php
                $q = Doctrine_Query::create()
                    ->from("NotificationHistory a")
                    ->where("a.user_id = ?", $sf_user->getGuardUser()->getId())
                    ->andWhere("a.application_id <> ?",'')
                    ->andWhere("a.confirmed_receipt = ?", 0)
                    ->orderBy("a.id DESC");
                $newalerts = $q->count();
                ?>
                <div class="btn-group hidden-sm hidden-xs">
                    <button class="btn btn-default dropdown-toggle tp-icon" data-toggle="dropdown" onClick="window.location='<?php echo public_path("/index.php/notifications/index"); ?>';">
                        <i class="glyphicon glyphicon-globe"></i>
                        <?php
                        if($newalerts)
                        {
                            ?>
                            <span class="badge"><?php echo $newalerts; ?></span>
                        <?php
                        }
                        ?>
                    </button>
                </div>
            </li>
          <li>
        </ul>
    </div><!-- headerbar -->
*/?>
