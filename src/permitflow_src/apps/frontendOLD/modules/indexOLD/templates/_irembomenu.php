<?php
use_helper('I18N');
?>

<ul class="nav navbar-nav"> 
    <?php $x = 0;
    foreach ($pages as $page) : $x++;
       
        ?>
        <?php
        $menutitle = "";

        $otbhelper = new OTBHelper();
        $dbconn  = $otbhelper->getDbConnection();

        $sql = "SELECT * FROM ext_translations WHERE field_id = '" . $page->getId() . "' AND field_name = 'menu_title' AND table_class = 'content' AND locale = '" . $sf_user->getCulture() . "'";
        $rows = mysqli_query($dbconn,$sql) or die($sql);
        if ($row = mysqli_fetch_assoc($rows)) {
            $menutitle = $row['trl_content'];
            
            
        } else {
            $menutitle = $page->getMenuTitle();
        }
       
        if ($page->getParentId() == 0 && $menutitle == 'Home') {
             // error_log("Called  irembo!!!!!".$menutitle) ;
            ?>
            <li class=""><a href="/"><?php echo __('Home'); ?></a>
                <?php
            } else if ($page->getParentId() == 0) {
                ?>
            <li class="" <?php
                if ($_SERVER['REQUEST_URI'] == url_for($page->getUrl()) || ($_SERVER['REQUEST_URI'] == "/" && $x == 1)) {
                    echo "  class='current' ";
                }
                ?>>
                <?php
                $q = "select * from content where parent_id =" . $page->getId();
                $r = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($q);
             //   $r =  mysqli_query($dbconn,$q) or die($q);
               // $c = mysqli_affected_rows($r);
               

                if (count($r) > 0) { ?>
                 <a class="dropdown-toggle" data-toggle="dropdown" href="#"  title="<?php echo $menutitle ?>"><?php echo $menutitle ?></a>
                  <?php  // createmenu($id, $c); 
                    createsubmenus($page->getId());
                    
                    ?>
               <?php  } else {?>
                <a  href="/index.php?id=<?php echo $page->getId(); ?>"  title="<?php echo $menutitle ?>"><?php echo $menutitle ?></a>
               <?php 
               
               } ?>
            <?php } ?>
        </li>
       
        
    <?php endforeach ?>
        
    <?php

    function createsubmenus($pid) {

        if ($pid != 0) {
            ?>
            <ul class="dropdown-menu">
                <?php
            }
            $q = Doctrine_Query::create()
                    ->from('Content a')
                    ->where('a.parent_id = ? ', $pid)
                    ->addWhere('a.deleted = ? ', 0)
                    ->addWhere('a.published = ? ', 1)//added to only show published pages
                    ->orderBy('a.menu_index ASC');
            $childpages = $q->execute();

            $count = count($childpages);
            if ($count > 0) {
                $x = 0;
                foreach ($childpages as $childpage) : $x++;
                    $id = $childpage->getId();
                    $menutitle = $childpage->getMenuTitle();
                    ?>
                    <li><a style="white-space: normal;" href="/index.php?id=<?php echo $childpage->getId(); ?>"><?php echo $menutitle; ?></a></li>
                    <?php
                endforeach;
            }

            if ($pid != 0)
                
                ?>
        </ul>
    <?php
}
?>
</ul>
