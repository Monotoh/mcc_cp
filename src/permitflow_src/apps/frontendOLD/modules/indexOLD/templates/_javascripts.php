<!--Bootstrap core JavaScript-->
<script src="<?php echo public_path(); ?>rwanda_frontend/js/jquery.js"></script>
<script src="<?php echo public_path(); ?>rwanda_frontend/js/bootstrap.min.js"></script>
<!--Bx-Slider JavaScript-->
<script src="<?php echo public_path(); ?>rwanda_frontend/js/jquery.bxslider.min.js"></script>
<!--Owl Carousel JavaScript-->
<script src="<?php echo public_path(); ?>rwanda_frontend/js/owl.carousel.min.js"></script>
<!--Pretty Photo JavaScript-->
<script src="<?php echo public_path(); ?>rwanda_frontend/js/jquery.prettyPhoto.js"></script>
<!--Full Calender JavaScript-->
<script src="<?php echo public_path(); ?>rwanda_frontend/js/moment.min.js"></script>
<script src="<?php echo public_path(); ?>rwanda_frontend/js/fullcalendar.min.js"></script>
<script src="<?php echo public_path(); ?>rwanda_frontend/js/jquery.downCount.js"></script>
<!--Image Filterable JavaScript-->
<script src="<?php echo public_path(); ?>rwanda_frontend/js/jquery-filterable.js"></script>
<!--Accordian JavaScript-->
<script src="<?php echo public_path(); ?>rwanda_frontend/js/jquery.accordion.js"></script>
<!--Number Count (Waypoints) JavaScript-->
<script src="<?php echo public_path(); ?>rwanda_frontend/js/waypoints-min.js"></script>
<!--v ticker-->
<script src="<?php echo public_path(); ?>rwanda_frontend/js/jquery.vticker.min.js"></script>
<!--select menu-->
<script src="<?php echo public_path(); ?>rwanda_frontend/js/jquery.selectric.min.js"></script>
<!--Side Menu-->
<script src="<?php echo public_path(); ?>rwanda_frontend/js/jquery.sidr.min.js"></script>
<!--Custom JavaScript-->
<script src="<?php echo public_path(); ?>rwanda_frontend/js/custom.js"></script>
<script type="text/javascript" src="<?php echo public_path(); ?>asset_unified/js/analyticstracking.js"></script>

<?php
// if(sfConfig::get('app_google_analytics_id')) {
    ?>
    <!--<script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', '<?php //echo sfConfig::get('app_google_analytics_id'); ?>', 'auto');
        ga('send', 'pageview');

    </script>-->
<?php
// }
?>
