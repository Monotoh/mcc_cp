<?php
/**
 * _javascripts.php template.
 *
 * Displays Javascripts
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


<!-- jQuery  -->
      <script src="<?php echo public_path(); ?>asset_unified/js/jquery.min.js"></script>
      <script src="<?php echo public_path(); ?>asset_unified/js/bootstrap.min.js"></script>
      <script src="<?php echo public_path(); ?>asset_unified/js/detect.js"></script>
      <script src="<?php echo public_path(); ?>asset_unified/js/fastclick.js"></script>
      <script src="<?php echo public_path(); ?>asset_unified/js/jquery.blockUI.js"></script>
      <script src="<?php echo public_path(); ?>asset_unified/js/waves.js"></script>
      <script src="<?php echo public_path(); ?>asset_unified/js/wow.min.js"></script>
      <script src="<?php echo public_path(); ?>asset_unified/js/jquery.nicescroll.js"></script>
      <script src="<?php echo public_path(); ?>asset_unified/js/jquery.scrollTo.min.js"></script>

      <!-- Datatables-->
      <script src="<?php echo public_path(); ?>asset_unified/plugins/datatables/jquery.dataTables.min.js"></script>
      <script src="<?php echo public_path(); ?>asset_unified/plugins/datatables/dataTables.bootstrap.js"></script>
      <script src="<?php echo public_path(); ?>asset_unified/plugins/datatables/dataTables.buttons.min.js"></script>
      <script src="<?php echo public_path(); ?>asset_unified/plugins/datatables/buttons.bootstrap.min.js"></script>
      <script src="<?php echo public_path(); ?>asset_unified/plugins/datatables/jszip.min.js"></script>
      <script src="<?php echo public_path(); ?>asset_unified/plugins/datatables/pdfmake.min.js"></script>
      <script src="<?php echo public_path(); ?>asset_unified/plugins/datatables/vfs_fonts.js"></script>
      <script src="<?php echo public_path(); ?>asset_unified/plugins/datatables/buttons.html5.min.js"></script>
      <script src="<?php echo public_path(); ?>asset_unified/plugins/datatables/buttons.print.min.js"></script>
      <script src="<?php echo public_path(); ?>asset_unified/plugins/datatables/dataTables.fixedHeader.min.js"></script>
      <script src="<?php echo public_path(); ?>asset_unified/plugins/datatables/dataTables.keyTable.min.js"></script>
      <script src="<?php echo public_path(); ?>asset_unified/plugins/datatables/dataTables.responsive.min.js"></script>
      <script src="<?php echo public_path(); ?>asset_unified/plugins/datatables/responsive.bootstrap.min.js"></script>
      <script src="<?php echo public_path(); ?>asset_unified/plugins/datatables/dataTables.scroller.min.js"></script>

      <!-- Datatable init js -->
      <script src="<?php echo public_path(); ?>asset_unified/pages/datatables.init.js"></script>

      <!-- Custom js -->
      <script src="<?php echo public_path(); ?>asset_unified/js/jquery.core.js"></script>
      <script src="<?php echo public_path(); ?>asset_unified/js/jquery.app.js"></script>

      <!-- chatjs  -->
      <script src="<?php echo public_path(); ?>asset_unified/pages/jquery.chat.js"></script>
      <!-- OTB patch include javascript library for spell checking -->
      <script type="text/javascript" src="<?php echo public_path(); ?>otb_assets/javascriptspellcheck/include.js"></script>
           

      <script type="text/javascript">
          $(document).ready(function() {
              $('#datatable').dataTable();
              $('#datatable-keytable').DataTable( { keys: true } );
              $('#datatable-responsive').DataTable();
              $('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
              var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } );
          } );
          TableManageButtons.init();

      </script>




<!--[if lt IE 9]><script src="/form_builder/js/signaturepad/flashcanvas.js"></script><![endif]-->
<script type="text/javascript" src="/form_builder/js/signaturepad/jquery.signaturepad.min.js"></script>
<script type="text/javascript" src="/form_builder/js/signaturepad/json2.min.js"></script>




<script src="<?php echo public_path(); ?>elm/elm.js"></script>
<script type="text/javascript" src="<?php echo public_path(); ?>asset_unified/js/analyticstracking.js"></script>

<?php
/*if(sfConfig::get('app_google_analytics_id')) {
?>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', '<?php echo sfConfig::get('app_google_analytics_id'); ?>', 'auto');
        ga('send', 'pageview');
    </script>
<?php
} */
?>
