<?php
/**
 * _menu.php template.
 *
 * Displays Menu
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
  use_helper('I18N');
?>
		<div class="col-md-8">
			<!--kode nav_2 start-->
			<div class="nav_2" id="navigation">
				<ul>

					<?php $x = 0; foreach ($pages as $page) : $x++;?>
						<?php

						$menutitle = "";

						$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
						mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

						$sql = "SELECT * FROM ext_translations WHERE field_id = '".$page->getId()."' AND field_name = 'menu_title' AND table_class = 'content' AND locale = '".$sf_user->getCulture()."'";
						$rows = mysql_query($sql);
						if($row = mysql_fetch_assoc($rows))
						{
							$menutitle = $row['trl_content'];
						}
						else
						{
							$menutitle = $page->getMenuTitle();
						}

						if($page->getParentId() == 0 && $menutitle == 'Home')
						{

						?>
							<li><a href="/"><?php echo __('Home'); ?></a>
						<?php
						}else if($page->getParentId() == 0){
						?>
							<li <?php if($_SERVER['REQUEST_URI'] == url_for($page->getUrl()) || ($_SERVER['REQUEST_URI'] == "/" && $x == 1)){echo "  class='current' ";} ?>><a href="/index.php?id=<?php echo $page->getId(); ?>"  title="<?php echo $menutitle  ?>"><?php echo $menutitle  ?></a>
						<?php
						}
						?>
						<?php
							$q = "select * from content where parent_id =".$page->getId() ;
							$r = mysql_query($q);          
							$c = mysql_affected_rows();

							if($c > 0) 
							{
							   // createmenu($id, $c); 
							   createsubmenus($page->getId()) ;
							}
						?>
						</li>
					<?php endforeach ?>
					<!--<li><a href="aboutus.html">Getting Started</a></li>-->
					<?php
						function createsubmenus($pid) {
						
						 if($pid != 0) 
						   {
							?>
							<ul>
							<?php
						   }
						 $q = Doctrine_Query::create()
											  ->from('Content a')
											  ->where('a.parent_id = ? ', $pid)
											  ->addWhere('a.deleted = ? ', 0)
											  ->addWhere('a.published = ? ', 1)//added to only show published pages
											  ->orderBy('a.menu_index ASC');
						 $childpages = $q->execute();
						
						   $count = count($childpages) ;
						   if($count > 0) 
						   {
							   $x = 0; foreach ($childpages as $childpage) : $x++;
									$id = $childpage->getId();
									$menutitle = $childpage->getMenuTitle();
								?>
								<li><a href="/index.php?id=<?php echo $childpage->getId(); ?>"><?php echo $menutitle; ?></a></li>
							<?php
							 endforeach;
						  }

					if($pid != 0)
							?>
							</ul>
							<?php
					}
					?>
				</ul>
			</div>
			<!--kode nav_2 end-->
		</div>
