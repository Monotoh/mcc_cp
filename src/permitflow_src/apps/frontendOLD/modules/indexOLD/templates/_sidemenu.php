<?php
/**
 * _sidemenu.php template.
 *
 * Displays Side Menu
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper('I18N');
$translation = new Translation();
?>

<?php
if ($sf_user->isAuthenticated() && $sf_user->getGuardUser()->getId() != '1') {
    ?>

    <script src="<?php echo public_path(); ?>asset_unified/js/jquery.app.js"></script>

    <div id="navigation">
        <!-- Navigation Menu-->
        <ul class="navigation-menu">

            <li <?php if ($sf_context->getModuleName() == "dashboard" && $sf_context->getActionName() == "index") { ?>class="active"<?php } ?>>
                <a href="<?php echo public_path(); ?>index.php/dashboard"><i class="md md-dashboard <?php if ($sf_context->getModuleName() == "dashboard" && $sf_context->getActionName() == "index") { ?>icon-white<?php } ?>"></i>
                    <span><?php echo __('Dashboard'); ?></span></a>
            </li>
             <li <?php if($sf_context->getModuleName() == "dashboard" && $sf_context->getActionName() == "index"){ ?>class="active"<?php } ?>>
       <a href="<?php echo public_path(); ?>index.php/dashboard/drafts"><i class="md md-drafts <?php if($sf_context->getModuleName() == "dashboard" && $sf_context->getActionName() == "drafts"){ ?>icon-white<?php } ?>"></i>
	   <span><?php echo __('My Drafts'); ?></span></a>
       </li>
       <li <?php if (($sf_context->getActionName() == "groups" && $sf_context->getModuleName() == "application") || ($sf_context->getModuleName() == "forms")) {
        echo "class='active'";
    } ?>><a href="<?php echo public_path(); ?>index.php/application/groups"><i class="md md-attach-file"></i><?php echo __('Submit New Application'); ?></a></li>
 
        <li class="<?php if (($sf_context->getModuleName() == "application" && $sf_context->getActionName() == "index") || ($sf_context->getModuleName() == "forms")) { ?>active<?php } ?>">
                <a href="<?php echo public_path(); ?>index.php/application/index"><i class="md md-view-headline <?php if ($sf_context->getModuleName() == "application" && $sf_context->getActionName() == "index") { ?>icon-white<?php } ?>"></i>
                    <span> <?php echo __('Applications History'); ?></span></a>
            </li>
       
      
            <li class="dropdown">
                <a href="" class="dropdown-toggle waves-effect waves-light profile" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-expand"></i><?php echo __('More') ?></a>
                <ul class="dropdown-menu">
                          
                          
                    <li>
                   <a href="<?php echo public_path(); ?>index.php/dashboard/inspection"><i class="md md-insert-emoticon <?php if($sf_context->getModuleName() == "dashboard" && $sf_context->getActionName() == "inspection"){ ?>icon-white<?php } ?>"></i>
                       <span><?php echo __('Inspections'); ?></span></a>
                   </li>

                   <li>
                   <a href="<?php echo public_path(); ?>index.php/dashboard/occupation"><i class="md md-home <?php if($sf_context->getModuleName() == "dashboard" && $sf_context->getActionName() == "occupation"){ ?>icon-white<?php } ?>"></i>
                       <span><?php echo __('Occupancy'); ?></span></a>
                   </li>

                    <!--OTB Start Patch - Shared applications -->
                    <li>
                        <a href="<?php echo public_path(); ?>index.php/sharedapplication"><i class="fa fa-share"></i>
                            <span> <?php echo __('Shared Applications'); ?></span></a>
                    </li>
                    <!--OTB End Patch - Shared applications -->
                    <li>
                        <a href="<?php echo public_path(); ?>index.php/feedback/feedback"><i class="md md-add-alarm"></i>
                            <span> <?php echo __('FeedBack'); ?></span></a>
                    </li>
                    <!--OTB Patch-->
                    <li>
                        <a href="#" onclick="javascript:void window.open('<?php echo public_path(); ?>chat/phplive/phplive.php', '1464084834566', 'width=400,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');
                                            return false;"><i class="md md-chat"></i>
                            <span> <?php echo __('Live Chat'); ?></span></a>
                    </li>
                    <!--OTB Start Patch - Fee Estimate Calculator -->
                    <li>
                        <a href="<?php echo public_path(); ?>index.php/calculator"><i class="md md-add-alarm"></i>
                            <span> <?php echo __('Cost Estimate'); ?></span></a>
                    </li>
                     
                </ul>
            </li>
            <!--OTB End Patch - Fee Estimate Calculator -->
            <li>


                <div class="frontend-search-box">

                    <form action="<?php echo url_for('application/search') ?>" method="get">
                        <div class="row">
                            <div class="col-md-3">
                                <input type="text" placeholder="<?php __('Enter Application Number..') ?>" name="query" value="<?php echo $sf_request->getParameter('query') ?>" id="search_keywords" />
                            </div>
                            <div class="col-md-5 pull-right">
                                <div style="position: absolute; left:90px;">
                                    <input class="btn btn-primary" type="submit" value="<?php echo __('search application') ?>" />
                                </div>
                            </div>
                        </div>



                    </form>
                </div>

            </li>
            <!--OTB Patch-->
        </ul>
        <!-- End navigation menu -->
    </div>




    <?php
}
?>
