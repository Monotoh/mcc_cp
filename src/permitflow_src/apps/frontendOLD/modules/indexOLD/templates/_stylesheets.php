<!-- OTB patch Add a call for our frontend custom styles -->
<link rel="stylesheet" href="<?php echo public_path(); ?>otb_assets/assets/frontend/styles.css" type="text/css" >
<?php
/**
 * _stylesheets.php template.
 *
 * Displays Stylesheets
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

$translation = new Translation();
if($translation->IsLeftAligned())
{
?>
      


<!-- Bootstrap core CSS -->
<link href="<?php echo public_path(); ?>rwanda_frontend/css/bootstrap.min.css" rel="stylesheet">
<!-- Full Calender CSS -->
<link href="<?php echo public_path(); ?>rwanda_frontend/css/fullcalendar.css" rel="stylesheet">
<!-- Owl Carousel CSS -->
<link href="<?php echo public_path(); ?>rwanda_frontend/css/owl.carousel.css" rel="stylesheet">
<!-- Pretty Photo CSS -->
<link href="<?php echo public_path(); ?>rwanda_frontend/css/prettyPhoto.css" rel="stylesheet">
<!-- Bx-Slider StyleSheet CSS -->
<link href="<?php echo public_path(); ?>rwanda_frontend/css/jquery.bxslider.css" rel="stylesheet"> 
<!-- Font Awesome StyleSheet CSS -->
<link href="<?php echo public_path(); ?>rwanda_frontend/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo public_path(); ?>rwanda_frontend/svg/style.css" rel="stylesheet">
<!-- Widget CSS -->
<link href="<?php echo public_path(); ?>rwanda_frontend/css/widget.css" rel="stylesheet">
<!-- Typography CSS -->
<link href="<?php echo public_path(); ?>rwanda_frontend/css/typography.css" rel="stylesheet">
<!-- Shortcodes CSS -->
<link href="<?php echo public_path(); ?>rwanda_frontend/css/shortcodes.css" rel="stylesheet">
<!-- Custom Main StyleSheet CSS -->
<link href="<?php echo public_path(); ?>rwanda_frontend/style.css" rel="stylesheet">
<!-- Color CSS -->
<link href="<?php echo public_path(); ?>rwanda_frontend/css/color.css" rel="stylesheet">
<!-- Responsive CSS -->
<link href="<?php echo public_path(); ?>rwanda_frontend/css/responsive.css" rel="stylesheet">
<!-- SELECT MENU -->
<link href="<?php echo public_path(); ?>rwanda_frontend/css/selectric.css" rel="stylesheet">
<!-- SIDE MENU -->
<link rel="stylesheet" href="<?php echo public_path(); ?>rwanda_frontend/css/jquery.sidr.dark.css">
 <?php
}
else
{
	?>


	<link rel="stylesheet" href="<?php echo public_path(); ?>permitflow/css/style.css" type="text/css"  media="all">

    <link href="<?php echo public_path(); ?>assets_unified/css/style.default-rtl.css" rel="stylesheet">


    <link rel="shortcut icon" href="<?php echo public_path(); ?>permitflow/images/favicon.png" />
    
    <!-- JS
    ================================================== -->
   <script type="text/javascript" src="<?php echo public_path(); ?>permitflow/js/jquery.min.js" ></script>
	<!--[if lt IE 9]>
	<script src="<?php echo public_path(); ?>permitflow/js/modernizr.custom.11889.js" type="text/javascript"></script>
	<![endif]-->
	<!-- HTML5 Shiv events (end)-->
	<?php
}
