<!-- OTB patch Add a call for our frontend custom styles -->
<link rel="stylesheet" href="<?php echo public_path(); ?>otb_assets/assets/frontend/styles.css" type="text/css" >
<?php
/**
 * _stylesheets.php template.
 *
 * Displays Stylesheets
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */

$translation = new Translation();
if($translation->IsLeftAligned())
{
	?>

	<!-- DataTables -->
	<link href="<?php echo public_path(); ?>asset_unified/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo public_path(); ?>asset_unified/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo public_path(); ?>asset_unified/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo public_path(); ?>asset_unified/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo public_path(); ?>asset_unified/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />


	<link href="<?php echo public_path(); ?>asset_unified/plugins/jquery-circliful/css/jquery.circliful.css" rel="stylesheet" type="text/css" />

	<link href="<?php echo public_path(); ?>asset_unified/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo public_path(); ?>asset_unified/css/core.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo public_path(); ?>asset_unified/css/components1.6.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo public_path(); ?>asset_unified/css/icons.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo public_path(); ?>asset_unified/css/pages.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo public_path(); ?>asset_unified/css/menu.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo public_path(); ?>asset_unified/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo public_path(); ?>asset_unified/css/elements.css" rel="stylesheet" type="text/css" />


	<?php
}
else
{
	?>
	<link href="<?php echo public_path(); ?>asset_unified/plugins/jquery-circliful/css/jquery.circliful.css" rel="stylesheet" type="text/css" />

	<link href="<?php echo public_path(); ?>asset_unified/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo public_path(); ?>asset_unified/css/core.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo public_path(); ?>asset_unified/css/components.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo public_path(); ?>asset_unified/css/icons.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo public_path(); ?>asset_unified/css/pages.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo public_path(); ?>asset_unified/css/menu.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo public_path(); ?>asset_unified/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo public_path(); ?>asset_unified/css/elements.css" rel="stylesheet" type="text/css" />

  	<link href="<?php echo public_path(); ?>assets_unified/css/style.default.rtl.css" rel="stylesheet">
	<?php
}
?>
