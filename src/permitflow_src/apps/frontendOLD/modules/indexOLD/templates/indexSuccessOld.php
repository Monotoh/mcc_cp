<?php
/**
 * indexSuccess.php template.
 *
 * Displays a web page
 *
 * @package    frontend
 * @subpackage index
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
  use_helper('I18N');

$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));

mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

foreach($contents as $content)
{
	$menutitle = "";


	$sql = "SELECT * FROM ext_translations WHERE field_id = '".$content->getId()."' AND field_name = 'menu_title' AND table_class = 'content' AND locale = '".$sf_user->getCulture()."'";

	$rows = mysql_query($sql);

	if($row = mysql_fetch_assoc($rows))
	{
		$menutitle = $row['trl_content'];
	}
	else
	{
		$menutitle = $content->getMenuTitle();
	}


   $toparticle = "";


	$sql = "SELECT * FROM ext_translations WHERE field_id = '".$content->getId()."' AND field_name = 'top_article' AND table_class = 'content' AND locale = '".$sf_user->getCulture()."'";

	$rows = mysql_query($sql);

	if($row = mysql_fetch_assoc($rows))
	{
		$toparticle = $row['trl_content'];
	}
  else
  {
  	$toparticle = $content->getTopArticle();
  }

if($_GET['id'])
{
?>
<!--<div class="breadcrumb-box">
	<div class="container">
		<ul class="breadcrumb">
			<li>
				<a href="/index.php"><?php echo __('Home'); ?></a>
				<span class="divider">/</span>
			</li>
			<li class="active"><?php echo $menutitle; ?></li>
		</ul>
	</div>
</div>-->

<?php
if($_GET['id'] != 1 && !empty($_GET['id']))
{
?>
  <section id="headline">
    <div class="container">
      <h3><?php echo $menutitle; ?></h3>
    </div>
  </section>
 <?php
}
 ?>


        <?php
}



  //check if this is the homepage, if it is then show banner
  $q = Doctrine_Query::create()
     ->from("Content a")
     ->where("a.menu_index < ?", $content->getMenuIndex());
  $is_not_first_page = $q->fetchOne();

  if($is_not_first_page)
  {
      //don't display banner
  }
  else
  {
    include_component('index', 'banner');
  }

        ?>
<?php
if($_GET['id'] != 1 && !empty($_GET['id']))
{
?>
<section class="container">
    <div class="sixteen columns">
      <hr class="vertical-space2">
   <?php
}
 ?>
<!-- BEGIN CONTAINER -->
<?php
  $article = html_entity_decode($toparticle);
  echo $article;
?>
<?php
    if($_GET['id'] != 1 && !empty($_GET['id']))
    {
    ?>
      </div>
    </section>
    <?php
    }
	}
?>
