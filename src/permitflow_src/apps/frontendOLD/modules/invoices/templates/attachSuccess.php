<?php

/**
 * viewSuccess.php template.
 *
 * Displays a dynamically generated application form
 *
 * @package    frontend
 * @subpackage forms
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");
$prefix_folder = dirname(__FILE__)."/../../../../../lib/vendor/cp_machform/";

require($prefix_folder.'includes/init.php');

header("p3p: CP=\"IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT\"");

$_SESSION['invoice']  = $invoice->getId();


$expired = false;

$db_date_event = str_replace('/', '-', $invoice->getExpiresAt());

$db_date_event = strtotime($db_date_event);

if (time() > $db_date_event)
{
     $expired = true;
}

if($expired)
{
    echo "Expired";
    exit;
}

require($prefix_folder.'config.php');
require($prefix_folder.'includes/language.php');
require($prefix_folder.'includes/db-core.php');
require($prefix_folder.'includes/common-validator.php');
require($prefix_folder.'includes/view-functions.php');
require($prefix_folder.'includes/post-functions.php');
require($prefix_folder.'includes/filter-functions.php');
require($prefix_folder.'includes/entry-functions.php');
require($prefix_folder.'includes/helper-functions.php');
require($prefix_folder.'includes/theme-functions.php');
require($prefix_folder.'lib/recaptchalib.php');
require($prefix_folder.'lib/php-captcha/php-captcha.inc.php');
require($prefix_folder.'lib/text-captcha.php');
require($prefix_folder.'hooks/custom_hooks.php');

$dbh 		= mf_connect_db();
$ssl_suffix = mf_get_ssl_suffix();


function find($needle, $haystack)
{
    $pos = strpos($haystack, $needle);
    if($pos === false)
    {
        return false;
    }
    else
    {
        return true;
    }
}


$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

function do_query($sql)
{
    return mysql_query($sql);
}


function parse($form_id, $entry_id, $content, $identifier_type){
    $sql = "SELECT * FROM ap_form_".$form_id." WHERE id = ".$entry_id;
    $app_results = do_query($sql);

    $apform = mysql_fetch_assoc($app_results);


    if(find('{fm_created_at}', $content))
    {
        if($identifier_type == "0" || $identifier_type == "2")
        {
            $content = str_replace('{fm_created_at}', substr($apform['date_created'], 0, 1), $content);
        }
        else
        {
            $content = str_replace('{fm_created_at}', substr($apform['date_created'], 0, 11), $content);
        }
    }

    if(find('{fm_updated_at}', $content))
    {
        if($identifier_type == "0" || $identifier_type == "2")
        {
            $content = str_replace('{fm_updated_at}', substr($apform['date_updated'], 0, 1), $content);
        }
        else
        {
            $content = str_replace('{fm_updated_at}', substr($apform['date_updated'], 0, 11), $content);
        }
    }

    $sql = "SELECT * FROM ap_form_elements WHERE form_id = ".$form_id;
    $app_form_elements = do_query($sql);

    while($element_row = mysql_fetch_assoc($app_form_elements))
    {
        $childs = $element_row['element_total_child'];
        if($childs == 0)
        {
            if(find('{fm_element_'.$element_row['element_id'].'}', $content))
            {
                if($identifier_type == "0" || $identifier_type == "2")
                {
                    $content = str_replace('{fm_element_'.$element_row['element_id'].'}',substr($apform['element_'.$element_row['element_id']], 0, 1), $content);
                }
                else
                {
                    $content = str_replace('{fm_element_'.$element_row['element_id'].'}',substr($apform['element_'.$element_row['element_id']], 0, 1), $content);
                }
            }
        }
        else
        {
            if($element_row['element_type'] == "select")
            {
                if(find('{fm_element_'.$element_row['element_id'].'}', $content))
                {

                    $sql = "SELECT * FROM ap_element_options WHERE form_id = ".$form_id." AND element_id = ".$element_row['element_id']." AND option_id = ".$apform['element_'.$element_row['element_id']];
                    $options = do_query($sql);

                    if(mysql_num_rows($options) > 0)
                    {
                        $option = mysql_fetch_assoc($options);
                        if($identifier_type == "0" || $identifier_type == "2")
                        {
                            $content = str_replace('{fm_element_'.$element_row['element_id'].'}',substr($option['option'],0,1), $content);
                        }
                        else
                        {
                            $content = str_replace('{fm_element_'.$element_row['element_id'].'}',$option['option'], $content);
                        }
                    }
                }
            }
            else
            {
                for($x = 0; $x < ($childs + 1); $x++)
                {
                    if(find('{fm_element_'.$element_row['element_id'].'}', $content))
                    {
                        if($identifier_type == "0" || $identifier_type == "2")
                        {
                            $content = str_replace('{fm_element_'.$element_row['element_id']."}",substr($apform['element_'.$element_row['element_id']."_".($x+1)],0,1), $content);
                        }
                        else
                        {
                            $content = str_replace('{fm_element_'.$element_row['element_id']."}",$apform['element_'.$element_row['element_id']."_".($x+1)], $content);
                        }
                    }
                }
            }
        }


    }
    return $content;
}

function parseFull($form_id, $entry_id, $content, $identifier_type){


    $sql = "SELECT * FROM ap_form_".$form_id." WHERE id = ".$entry_id;
    $app_results = do_query($sql);

    $apform = mysql_fetch_assoc($app_results);


    if(find('{fm_created_at}', $content))
    {
        if($identifier_type == "0" || $identifier_type == "2")
        {
            $content = str_replace('{fm_created_at}', $apform['date_created'], $content);
        }
        else
        {
            $content = str_replace('{fm_created_at}', $apform['date_created'], $content);
        }
    }

    if(find('{fm_updated_at}', $content))
    {
        if($identifier_type == "0" || $identifier_type == "2")
        {
            $content = str_replace('{fm_updated_at}', $apform['date_updated'], $content);
        }
        else
        {
            $content = str_replace('{fm_updated_at}', $apform['date_updated'], $content);
        }
    }

    $sql = "SELECT * FROM ap_form_elements WHERE form_id = ".$form_id;
    $app_form_elements = do_query($sql);

    while($element_row = mysql_fetch_assoc($app_form_elements))
    {
        $childs = $element_row['element_total_child'];
        if($childs == 0)
        {
            if(find('{fm_element_'.$element_row['element_id'].'}', $content))
            {
                if($identifier_type == "0" || $identifier_type == "2")
                {
                    $content = str_replace('{fm_element_'.$element_row['element_id'].'}',$apform['element_'.$element_row['element_id']], $content);
                }
                else
                {
                    $content = str_replace('{fm_element_'.$element_row['element_id'].'}',$apform['element_'.$element_row['element_id']], $content);
                }
            }
        }
        else
        {
            if($element_row['element_type'] == "select")
            {
                if(find('{fm_element_'.$element_row['element_id'].'}', $content))
                {

                    $sql = "SELECT * FROM ap_element_options WHERE form_id = ".$form_id." AND element_id = ".$element_row['element_id']." AND option_id = ".$apform['element_'.$element_row['element_id']];
                    $options = do_query($sql);

                    if(mysql_num_rows($options) > 0)
                    {
                        $option = mysql_fetch_assoc($options);
                        if($identifier_type == "0" || $identifier_type == "2")
                        {
                            $content = str_replace('{fm_element_'.$element_row['element_id'].'}',$option['option'], $content);
                        }
                        else
                        {
                            $content = str_replace('{fm_element_'.$element_row['element_id'].'}',$option['option'], $content);
                        }
                    }
                }
            }
            else
            {
                for($x = 0; $x < ($childs + 1); $x++)
                {
                    if(find('{fm_element_'.$element_row['element_id'].'}', $content))
                    {
                        if($identifier_type == "0" || $identifier_type == "2")
                        {
                            $content = str_replace('{fm_element_'.$element_row['element_id']."}",$apform['element_'.$element_row['element_id']."_".($x+1)], $content);
                        }
                        else
                        {
                            $content = str_replace('{fm_element_'.$element_row['element_id']."}",$apform['element_'.$element_row['element_id']."_".($x+1)], $content);
                        }
                    }
                }
            }
        }


    }
    return $content;
}

if(mf_is_form_submitted()){ //if form submitted
    $input_array   = mf_sanitize($_POST);
    $submit_result = mf_process_form($dbh,$input_array);

    if(!isset($input_array['password'])){ //if normal form submitted
        echo $submit_result['status'];
        if($submit_result['status'] === true){
            if(!empty($submit_result['form_resume_url'])){ //the user saving a form, display success page with the resume URL
                $_SESSION['mf_form_resume_url'][$input_array['form_id']] = $submit_result['form_resume_url'];

                header("Location: http{$ssl_suffix}://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."?invoiceid={$invoice->getId()}&id={$input_array['form_id']}&entryid={$submit_result['entry_id']}&done=1");
                exit;
            }else if($submit_result['logic_page_enable'] === true){ //the page has skip logic enable and a custom destination page has been set
                $target_page_id = $submit_result['target_page_id'];

                if(is_numeric($target_page_id)){
                    header("Location: http{$ssl_suffix}://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."?invoiceid={$invoice->getId()}&id={$input_array['form_id']}&mf_page={$target_page_id}");
                    exit;
                }else if($target_page_id == 'payment'){
                    //redirect to payment page, based on selected merchant
                    $form_properties = mf_get_form_properties($dbh,$input_array['form_id'],array('payment_merchant_type'));

                    if($form_properties['payment_merchant_type'] == 'stripe'){
                        //allow access to payment page
                        $_SESSION['mf_form_payment_access'][$input_array['form_id']] = true;
                        $_SESSION['mf_payment_record_id'][$input_array['form_id']] = $submit_result['entry_id'];

                        header("Location: ".public_path()."index.php/invoices/payment?id={$input_array['form_id']}");
                        exit;
                    }else if($form_properties['payment_merchant_type'] == 'paypal_standard'){
                        echo "<script type=\"text/javascript\">top.location.replace('{$submit_result['form_redirect']}')</script>";
                        exit;
                    }
                }else if($target_page_id == 'review'){
                    if(!empty($submit_result['origin_page_number'])){
                        $page_num_params = '&mf_page_from='.$submit_result['origin_page_number'];
                    }

                    $_SESSION['review_id'] = $submit_result['review_id'];
                    header("Location: http{$ssl_suffix}://".$_SERVER['HTTP_HOST'].mf_get_dirname($_SERVER['PHP_SELF'])."/confirm?invoiceid={$invoice->getId()}&entryid={$submit_result['entry_id']}&id={$input_array['form_id']}{$page_num_params}");
                    exit;
                }else if($target_page_id == 'success'){
                    //redirect to success page
                    if(empty($submit_result['form_redirect'])){
                        header("Location: http{$ssl_suffix}://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."?invoiceid={$invoice->getId()}&id={$input_array['form_id']}&entryid={$submit_result['entry_id']}&done=1");
                        exit;
                    }else{
                        echo "<script type=\"text/javascript\">top.location.replace('{$submit_result['form_redirect']}')</script>";
                        exit;
                    }
                }

            }else if(!empty($submit_result['review_id'])){ //redirect to review page

                if(!empty($submit_result['origin_page_number'])){
                    $page_num_params = '&mf_page_from='.$submit_result['origin_page_number'];
                }

                $_SESSION['review_id'] = $submit_result['review_id'];
                header("Location: http{$ssl_suffix}://".$_SERVER['HTTP_HOST'].mf_get_dirname($_SERVER['PHP_SELF'])."/confirmApplication?id={$input_array['form_id']}{$page_num_params}");
                exit;
            }else{
                if(!empty($submit_result['next_page_number'])){ //redirect to the next page number
                    $_SESSION['mf_form_access'][$input_array['form_id']][$submit_result['next_page_number']] = true;

                    header("Location: http{$ssl_suffix}://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."?invoiceid={$invoice->getId()}&id={$input_array['form_id']}&mf_page={$submit_result['next_page_number']}");
                    exit;
                }else{ //otherwise display success message or redirect to the custom redirect URL or payment page

                   // if(mf_is_payment_has_value($dbh,$input_array['form_id'],$submit_result['entry_id'])){
                        //redirect to credit card payment page, if the merchant is being enabled and the amount is not zero

                        //allow access to payment page
                        $_SESSION['mf_form_payment_access'][$input_array['form_id']] = true;
                        $_SESSION['mf_payment_record_id'][$input_array['form_id']] = $submit_result['entry_id'];

                        header("Location: /index.php/forms/payment?id={$input_array['form_id']}");
                        exit;
                   // }else{
                        //redirect to success page
                      //  if(empty($submit_result['form_redirect'])){
                      //      header("Location: http{$ssl_suffix}://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."?invoiceid={$invoice->getId()}&id={$input_array['form_id']}&entryid={$submit_result['entry_id']}&done=1");
                      //      exit;
                      //  }else{
                       //     echo "<script type=\"text/javascript\">top.location.replace('{$submit_result['form_redirect']}')</script>";
                       //     exit;
                        //}
                    //}
                }
            }
        }else if($submit_result['status'] === false){ //there are errors, display the form again with the errors
            $old_values 	= $submit_result['old_values'];
            $custom_error 	= @$submit_result['custom_error'];
            $error_elements = $submit_result['error_elements'];

            $form_params = array();
            $form_params['page_number'] = $input_array['page_number'];
            $form_params['populated_values'] = $old_values;
            $form_params['error_elements'] = $error_elements;
            $form_params['custom_error'] = $custom_error;

            $markup = mf_display_form($dbh,$input_array['form_id'],$form_params);
        }
    }else{ //if password form submitted

        if($submit_result['status'] === true){ //on success, display the form
            $markup = mf_display_form($dbh,$input_array['form_id']);
        }else{
            $custom_error = $submit_result['custom_error']; //error, display the pasword form again

            $form_params = array();
            $form_params['custom_error'] = $custom_error;
            $markup = mf_display_form($dbh,$input_array['form_id'],$form_params);
        }
    }
}else{
    $form_id 		= (int) trim(16);
    $page_number	= (int) trim($_GET['mf_page']);

    $page_number 	= mf_verify_page_access($form_id,$page_number);

    $resume_key		= trim($_GET['mf_resume']);
    if(!empty($resume_key)){
        $_SESSION['mf_form_resume_key'][$form_id] = $resume_key;
    }

    if(!empty($_GET['done'])){
        $markup = mf_display_success($dbh,$form_id);

		$q = Doctrine_Query::create()
		   ->from("FormEntry a")
		   ->where("a.id = ?", $_SESSION['invoice']);
		$application = $q->fetchOne();

        //If form details are submitted successfully, then create invoice receipt entry
        $receipt = new UploadReceipt();
        $receipt->setFormId($form_id);
        $receipt->setEntryId($_GET['entryid']);
        $receipt->setInvoiceId($invoice->getId());
        $receipt->save();

		$q = Doctrine_Query::create()
		   ->from("FormEntry a")
		   ->where("a.id = ?", $invoice->getAppId());
		$application = $q->fetchOne();

		$approved = $application->getApproved();
		$approved++;
		$application->setApproved($approved);
		$application->save();

    }else{
        $form_params = array();
        $form_params['page_number'] = $page_number;
        $markup = mf_display_form($dbh,$form_id,$form_params);
    }
}
$q = Doctrine_Query::create()
    ->from('ApForms a')
    ->where('a.form_id = ?', 16);
$formObj = $q->fetchOne();
$form_name = $formObj->getFormName();
$form_description = $formObj->getFormDescription();

$form_name = "";

$sql = "SELECT * FROM ext_translations WHERE field_id = '".$_GET['id']."' AND field_name = 'form_name' AND table_class = 'ap_forms' AND locale = '".$_SESSION['locale']."'";

$rows = mysql_query($sql, $dbconn);
if($row = mysql_fetch_assoc($rows))
{
    $form_name = $row['trl_content'];
}
else
{
    $form_name = $formObj->getFormName();
}


?>
<div class="pageheader">
       <h2><i class="fa fa-edit"></i><?php echo __('Application Form') ?><span> <?php echo __('Submit an Application') ?></span></h2>
      <div class="breadcrumb-wrapper">

        <ol class="breadcrumb">
          <li><a href=""><?php echo __('Application Form') ?></a></li>
          <li class="active"><?php echo $form_name; ?></li>
        </ol>
      </div>
    </div>


<div class="contentpanel">
<div class="row">


                <div class="accordion_custom" id="accordion2">
                    <div class="accordion-group whitened">
                        <div class="accordion-body in">
                            <div class="accordion-inner unspaced">
                                <?php

                                header("Content-Type: text/html; charset=UTF-8");
                                echo $markup;

                                ?>

                            </div><!-- accordion-inner -->
                        </div><!-- accordion-body -->
                    </div><!-- accordion-group -->
                </div><!-- accordion2 -->

</div><!-- /.row -->
</div><!-- /.marketing -->
