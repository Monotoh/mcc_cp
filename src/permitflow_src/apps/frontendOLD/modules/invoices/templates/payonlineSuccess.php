<?php
/**
 * payonlineSuccess.php template.
 *
 * Allows client to make mobile payments
 *
 * @package    frontend
 * @subpackage invoices
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");
$q = Doctrine_Query::create()
   ->from('mfInvoiceDetail a')
   ->where('a.invoice_id = ? AND a.description = ?', array($invoice->getId(),'Total'));
$details = $q->fetchOne();
$totalamount = 0;
if($details)
{
	$totalamount = $details->getAmount();
}
$currency = "RWF ";
?>
	
<section class="main eightcol last" style="width: 70%;">	
<?php
   $q = Doctrine_Query::create()
      ->from('FormEntry a')
	  ->where('a.id = ?', $invoice->getAppId());
   $application = $q->fetchOne();
?>

<form id="registration_form" class="appnitro" action="<?php echo public_path(); ?>index.php/invoices/paycomplete" method="post" enctype="multipart/form-data" autocomplete="off" data-ajax="false">
    <fieldset>
	<label><?php echo __('Payment Details') ?></label>
                <section>
                    <label class="description"><b> <?php echo __('Phone Number') ?></b></label>
                    <div id="element_div_1" name="element_div_1" style="display:block;" class="highlighted">
                        <span><input type="text" class="element text" name="phone" id="phone" required/></span>
                    </div>
                </section>
				
                <section>
                        <label class="description"><b><?php echo __('Permit') ?></b></label>
                    <div id="element_div_1" name="element_div_1" style="display:block;" class="highlighted">
                        <input type="text" class="element text" name="item" id="item" readonly="readonly"  value='<?php echo $application->getApplicationId(); ?>' required/>
                    </div>
                </section>

                <section>
                        <label class="description"><b><?php echo __('Amount') ?></b></label>
                    <div id="element_div_1" name="element_div_1" style="display:block;" class="highlighted">
                        <span><input type="text" class="element text" readonly="readonly"  name="amount_txt" id="amount_txt" value="<?php echo $currency ?> <?php echo $totalamount; ?>" required/>
                    </div>
                </section>
				
				<input type='hidden' name='merchant_id' id='amount' value='amount' value='<?php echo $totalamount; ?>'>
				<input type='hidden' name='merchant_id' id='merchant_id' value='kigali.ecp'>
				<input type='hidden' name='merchant_url' id='merchant_url' value='<?php public_path(); ?>index.php/invoices/paycomplete'>
				<input type='hidden' name='currency' id='currency' value='<?php echo $currency ?>'>
				

                <section>
				  <div style='width: 98%;'>
			<button class="submit" type="submit" name="submit_app" value="submitbuttonvalue"><?php echo __('Confirm and Continue') ?></button>
				  </div>
                </section>
	</fieldset>
 </form>
</section>
