<?php
/**
 * viewSuccess.php template.
 *
 * Displays a full invoice
 *
 * @package    frontend
 * @subpackage invoices
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");

//Get site config properties
$q = Doctrine_Query::create()
    ->from("ApSettings a")
    ->where("a.id = 1")
    ->orderBy("a.id DESC");
$apsettings = $q->fetchOne();

$invoice_manager = new InvoiceManager();

$invoice_manager->update_invoices($application->getId());
?>
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-6">
        <h4 class="page-title">Billing</h4>
    </div>
</div>
<!-- Page-Title -->

<div class="row">
<?php
if($invoice->getFormEntry()->getUserId() == $sf_user->getGuardUser()->getId()) {
    ?>
    <!-- col-sm-3 -->
    <div class="col-lg-8">

        <ul id="myTab" class="nav nav-tabs">
            <li class="active"><a href="#tabs-1" data-toggle="tab"><?php echo __('Invoice Details'); ?></a></li>
            <li><a href="#tabs-2" data-toggle="tab"><?php echo __('Payment Details'); ?></a></li>
        </ul>
        <div id="myTabContent" class="tab-content">
            <div class="tab-pane fade in active" id="tabs-1">
                <?php
                $templateparser = new TemplateParser();
                $invoice_manager = new InvoiceManager();

                try {
                    $html = $invoice_manager->generate_invoice_template($invoice->getId(), false);
                } catch (Exception $ex) {
                    error_log("Debug-t: Invoice Parse Error: " . $ex);

                    $html = $invoice_manager->generate_invoice_template_old_parser($invoice->getId(), false);
                }

                echo $html;
                ?>

                <div class="text-right btn-invoice" style="padding-right: 10px;">
                    <?php
                    if($invoice->getDocumentKey())
                    {
                        ?>
                        <button class="btn btn-primary" id="printinvoice" type="button"
                                onClick="window.location='<?php echo $invoice->getDocumentKey(); ?>';">
                            <i class="fa fa-print mr5"></i> <?php echo __('Download Invoice'); ?>
                        </button>
                    <?php
                    }
                    else {
                        ?>
                        <button class="btn btn-primary" id="printinvoice" type="button"
                                onClick="window.location='/index.php/invoices/printinvoice/id/<?php echo $invoice->getId(); ?>';">
                            <i class="fa fa-print mr5"></i> <?php echo __('Print Invoice'); ?></button>
                    <?php
                    }

                    $expired = false;

                    $db_date_event = str_replace('/', '-', $invoice->getExpiresAt());

                    $db_date_event = strtotime($db_date_event);

                    if (time() > $db_date_event && !($invoice->getPaid() == "15" || $invoice->getPaid() == "2")) {
                        $expired = true;
                    }

                    if ($expired) {
                        echo __("Expired. No Payments Possible");
                    } else {
                        if ($invoice->getPaid() == 1) {
                            ?>
                            <button class="btn btn-primary" id="makepayment" type="button"
                                    onClick="window.location='/index.php/invoices/pay/id/<?php echo $invoice->getId(); ?>';">
                                <i class="fa fa-print mr5"></i> <?php echo __('Make Payment'); ?></button>
                        <?php
                        }
                    }

                    if ($invoice->getPaid() != 1 && $invoice->getPaid() != 2) {
                        ?>
                        <button class="btn btn-primary" id="makepayment" type="button"
                                onClick="window.location='/index.php/invoices/pay/id/<?php echo $invoice->getId(); ?>';">
                            <i class="fa fa-print mr5"></i> <?php echo __('Add Payment'); ?></button>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <div class="tab-pane fade" id="tabs-2">
                <?php

                function get_ordinal($number)
                {
                    $ends = array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');
                    if (($number % 100) >= 11 && ($number % 100) <= 13)
                        $abbreviation = $number . 'th';
                    else
                        $abbreviation = $number . $ends[$number % 10];
                    return $abbreviation;
                }

                $paid = false;


                $receipts = $invoice->getUploadReceipt();

                if (sizeof($receipts) > 0) {
                    $count = 0;
                    foreach ($receipts as $receipt) {
                        $count++;
                        ?>
                        <h4><a href="#"><?php echo __('Receipt'); ?> <?php echo $count; ?></a></h4>
                        <div>
                            <p style="font-size: 12px; font-family:Times New Roman,'Georgia',Serif;">
                                <?php echo "<a target='_blank' href='/index.php/invoices/viewreceipt?form_id=" . $receipt->getFormId() . "&id=" . $receipt->getEntryId() . "'>(" . __("View Receipt") . ")</a>"; ?>
                            </p>
                        </div>
                    <?php

                    }
                }

                if (sizeof($receipts) == 0 && $paid == false) {
                    ?>
                    <h4><a href="#"><?php echo __('Receipts'); ?></a></h4>
                    <div>
                        <p style="font-size: 12px; font-family:Times New Roman,'Georgia',Serif;">

                        <div class="table-responsive">
                            <table class="table table-bordered mb0">
                                <tbody>
                                <tr>
                                    <td colspan="4" class="aligned">
                                        <h4><?php echo __('No Payments Made'); ?></h4>

                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--Responsive-table-->
                        </p>
                    </div>
                <?php
                }
                ?>
            </div>

        </div>

        </div>

        <!--Display a sidebar with information from the site config-->
        <?php if($apsettings){ ?>
            <div class="col-lg-4">
                <div class="card-box widget-user">
                    <div>
                        <img src="/asset_unified/images/users/avatar-1.jpg" class="img-responsive img-circle" alt="user">
                        <div class="wid-u-info">
                            <h4 class="m-t-0 m-b-5"><?php echo $sf_user->getGuardUser()->getProfile()->getFullname(); ?></h4>
                            <p class="m-b-5 font-13"><?php echo $sf_user->getGuardUser()->getProfile()->getEmail(); ?><br>
                                ID: <?php echo $sf_user->getGuardUser()->getUsername(); ?>
                            </p>
                            <a class="btn btn-primary" href="/index.php/signon/logout"><?php echo __('Logout') ?></a>
                        </div>
                    </div>
                </div>

                <?php echo html_entity_decode($apsettings->getOrganisationSidebar()); ?>
            </div>
        <?php } ?>

    </div>

<?php
}
else
{ ?>
    <h3><?php echo __('Sorry! You are trying to view an invoice that doesnt belong to you') ?></h3>;
<?php }
?>
</div>
