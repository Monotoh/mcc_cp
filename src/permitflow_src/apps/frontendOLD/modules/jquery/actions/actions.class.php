<?php

class jqueryActions extends sfActions
{
    public function executeCheckupi(sfWebRequest $request)
    {
		$form_manager = new FormManager();
		$upi = $request->getParameter("upi");
		echo json_encode($form_manager->getUPIDetails($upi));
		exit();
    }

    public function executeOptionidfromtext(sfWebRequest $request)
    {
		$form_manager = new FormManager();
		$form_id = $request->getParameter("form_id");
		$element_id = $request->getParameter("element_id");
		$option_text = $request->getParameter("option_text");
		echo $form_manager->getElementOptionIdForText($form_id, $element_id, $option_text);
		exit();
    }

}
