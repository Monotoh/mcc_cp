<?php
/**
 * newSuccess.php template.
 *
 * Displays form for submitting a new message
 *
 * @package    frontend
 * @subpackage messages
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>

    <div class="pageheader">
       <h2><i class="fa fa-envelope"></i>New Message<span>Send A Message To Reviewers</span></h2>
      <div class="breadcrumb-wrapper">
        
        <ol class="breadcrumb">
          <li><a href="<?php echo public_path(); ?>index.php">Home</a></li>
          <li><a href="<?php echo public_path(); ?>index.php/messages/index">Messages</a></li>
          <li class="active">Compose New Message</li>
        </ol>
      </div>
    </div>
    
    
     
        
        
<div class="contentpanel">
<div class="row">
  
  
 <div class="row">
           
            <div class="col-sm-3 col-lg-2">
                <button class="btn btn-danger btn-block btn-compose-email" onClick="window.location='<?php echo public_path(); ?>index.php/messages/new';">Compose Message</button>
                
                <ul class="nav nav-pills nav-stacked nav-email">
                
                    <li>
                    <a href="<?php echo public_path(); ?>index.php/messages/index">
                        <span class="badge pull-right">2</span>
                        <i class="glyphicon glyphicon-inbox"></i> Inbox
                    </a>
                    </li>
                    <li><a href="<?php echo public_path(); ?>index.php/messages/sent"><i class="glyphicon glyphicon-send"></i> Sent Mail</a></li>
                  
                </ul>
                
       
              </ul>
                
            </div><!-- col-sm-3 -->
            
            <div class="col-sm-9 col-lg-10">
                
                <div class="panel panel-default">
                    <div class="panel-body">
                    <form action="/index.php/messages/send" method="post" enctype="multipart/form-data"   autocomplete="off" data-ajax="false">
                    
                <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                     <label class="control-label">Select Application</label>
                              <select class="form-control" name='application' id='application'>
			            	<option disabled>Choose an application</option>
			              	<?php
				               $q = Doctrine_Query::create()
					          ->from('FormEntry a')
					          ->where('a.user_id = ?', $sf_user->getGuarduser()->getId()); 
				                $applications = $q->execute();
				                foreach($applications as $application)
				                  {
					               echo "<option value='".$application->getId()."'>".$application->getApplicationId()."</option>";
			                       	  }
			                    	?>
				              </select>
                  </div>
                                  <div class="mb15"></div>

                </div><!-- col-sm-4 -->
               
               <div class="col-sm-12">
                  <div class="form-group">
                     <textarea name='appmessage' id="wysiwyg" placeholder="Enter your message here..." class="form-control" rows="10" data-autogrow="true"></textarea>
                  </div>
                </div><!-- col-sm-12 -->
                          
                
                                 </div><!-- row -->
                                 
                                  </div><!-- panel-body-->
                                 <div class="panel-footer">
                                      <button type='submit' class="btn btn-primary">Send </button>
                                 </div>
                                      </form>
                              </div>
                        
                        
                    </div><!-- panel-body -->
                </div><!-- panel -->
                
            </div><!-- col-sm-9 -->
            
</div><!-- row -->
        
        
</div><!--panel-row-->
</div><!--contentpanel--> 


        





