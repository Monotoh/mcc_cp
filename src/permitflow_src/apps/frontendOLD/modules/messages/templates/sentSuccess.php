<?php
/**
 * sentSuccess.php template.
 *
 * Displays list of all sent messages from the currently logged in client
 *
 * @package    frontend
 * @subpackage messages
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>

    <div class="pageheader">
       <h2><i class="fa fa-envelope"></i>Messages<span>This page list Messages you have sent</span></h2>
      <div class="breadcrumb-wrapper">
        
        <ol class="breadcrumb">
          <li><a href="<?php echo public_path(); ?>index.php">Home</a></li>
          <li><a href="<?php echo public_path(); ?>index.php/messages/index">Messages</a></li>
          <li class="active">Outbox</li>
        </ol>
      </div>
    </div>
    










<div class="contentpanel">

    <div class="row">




<?php
foreach($messages as $message)
{
    if($message->getMessageread() == "0")
    {
        $unreadmessages[] = $message;
    }
    else
    {
        $readmessages[] = $message;
    }
}
?>


     <div class="row">
            <div class="col-sm-3 col-lg-2">
                <button class="btn btn-danger btn-block btn-compose-email" onClick="window.location='<?php echo public_path(); ?>index.php/messages/new';">Compose Message</button>
                
                <ul class="nav nav-pills nav-stacked nav-email">
                    <li>
                    <a href="<?php echo public_path(); ?>index.php/messages/index">
                        <span class="badge pull-right">2</span>
                        <i class="glyphicon glyphicon-inbox"></i> Inbox
                    </a>
                    </li>
                    <li class="active"><a href="<?php echo public_path(); ?>index.php/messages/sent"><i class="glyphicon glyphicon-send"></i> Sent Mail</a></li>
                  
                </ul>
                
       
              </ul>
                
            </div><!-- col-sm-3 -->
            
            <div class="col-sm-9 col-lg-10">
                
                 <div class="panel panel-dark widget-btns">
                 <div class="panel-heading">
                   <h3 class="panel-title"> Inbox</h3>
                    <p class="text-muted  ml10">Messages from Reviewers</p>
                  </div>  
                   
                
                <div class="panel-body panel-body-nopadding">        
                        <div class="table-responsive">
                        
                            <table class="table" id="table2">
                            <thead>
                                <th>
                                Application No.
                                </th>
                                
                                <th>
                                Message
                                </th>
                                
                                 <th>
                                 date sent
                                </th>
                                
                            </thead>
                              <tbody>
                        
                          
                                   <?php foreach($messages as $message){
                                          $q = Doctrine_Query::create()
                                             ->from('FormEntry a')
                                             ->where('a.id = ?', $message->getApplicationId());
                                              $application = $q->fetchOne();
                                   ?>
                                <tr class="unread">
                                
                                 <td>
                                 <a class="table-item-title" href="<?php echo public_path(); ?>index.php/messages/view/id/<?php echo $message->getId(); ?>">
			                                  <?php echo $application->getApplicationId(); ?>
                                            </a>
                                 </td>
                                  <td>
                                 <?php echo substr($message->getContent(),0,100)."..."; ?>  
                                  </td>
                                  
                                   <td>
                                 <?php echo $message->getActionTimestamp(); ?>
                                  </td>
                         
                                </tr>
                                
                                  <?php }
				if(sizeof($messages) == 0)
				{
				?>
                <tr>
				<td>			
                <h4>No Messages Available</h4>
                </td>
                </tr>
     
				<?php
				}

				?>
                              </tbody>
                            </table>
                        </div><!-- table-responsive -->
                        
                    </div><!-- panel-body -->
                </div><!-- panel -->
                
            </div><!-- col-sm-9 -->
            
        </div><!-- row -->
        



    </div><!--panel-row-->
</div><!--contentpanel-->         

