<?php
/**
 * indexSuccess.php template.
 *
 * Displays list of published news articles
 *
 * @package    frontend
 * @subpackage news
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>
<?php use_helper('I18N', 'Date') ?>


<div class="breadcrumb-box">
<div class="container">
<ul class="breadcrumb">
<li>
<a href="/index.php"><?php echo __('Home'); ?></a>
<span class="divider">/</span>
</li>
<li class="active"><?php echo __('News'); ?></li>
</ul>
</div>
</div>


<!-- end-header -->
  <section id="headline">
    <div class="container">
      <h3><?php echo __('News Articles'); ?></h3>
    </div>
  </section>

<section class="container">
  <div class="sixteen columns">
  <hr class="vertical-space2">


  <?php if (!$pager->getNbResults()): ?>
<?php echo __('No News Articles Posted'); ?>


<?php else: ?>



 <section class="container page-content" >
    <hr class="vertical-space2">



 <?php
    foreach($pager->getResults() as $content)
		{
	?>
      <article class="blog-post">
        <div class="one columns alpha">
          <div class="blog-date-sec"> <span><?php echo format_date($content->getCreatedOn(), "MMM"); ?></span>
            <h3><?php echo format_date($content->getCreatedOn(), "dd"); ?></h3>
            <span><?php echo format_date($content->getCreatedOn(), "yyyy"); ?></span> </div>
        </div>
        <div class="fifteen columns omega">
          <h3><a href="/index.php/news/article/id/<?php echo $content->getId(); ?>'"><?php echo html_entity_decode($content->getTitle()); ?></a></h3>
          <div class="postmetadata">
            <h6 class="blog-author"><strong>by</strong> <?php
								$q = Doctrine_Query::create()
									 ->from('cfUser a')
									 ->where('a.nid = ?', $content->getCreatedBy());
								$users = $q->execute();
								foreach($users as $user)
								{
									echo " Posted By ".$user->getStrfirstname()." ".$user->getStrlastname();
								}
							?></h6>
          </div>
          <p>

          <?php
						$newscontent = html_entity_decode($content->getArticle());

						if(strlen($newscontent) > 400)
						{
							echo substr($newscontent, 0, 400).".....";
							echo '</p><a class="readmore" href="/index.php/news/article/id/'.$content->getId().'">Read more</a>';
						}
						else
						{
							echo html_entity_decode($content->getArticle());
						}

						?>


          </div>
        <br class="clear">
      </article>



<br class="clear">
<?php if ($pager->haveToPaginate()): ?>
      <div class="pagination2 pagination2-centered">
        <ul>
          <li><?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir').'/assets_backend/images/first.png', array('align' => 'absmiddle', 'alt' => 'First', 'title' => 'First')), 'news/index?page=1') ?></li>
  		  <li><?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir').'/assets_backend/images/previous.png', array('align' => 'absmiddle', 'alt' => 'Previous', 'title' => 'Previous')), 'news/index?page='.$pager->getPreviousPage()) ?></li>
         	<li><?php foreach ($pager->getLinks() as $page): ?>
    	  		<?php echo link_to_unless($page == $pager->getPage(), $page, 'news/index?page='.$page) ?></li>
 		 	<?php endforeach; ?>
          <li><?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir').'/assets_backend/images/next.png', array('align' => 'absmiddle', 'alt' => 'Next', 'title' => 'Next')), 'news/index?page='.$pager->getNextPage()) ?></li>
  		  <li><?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir').'/assets_backend/images/last.png', array('align' => 'absmiddle', 'alt' => 'Last', 'title' => 'Last')), 'news/index?page='.$pager->getLastPage()) ?></li>

        </ul>
      </div>
<?php endif; ?>
      <div class="vertical-space2"></div>
    <!-- end-main-content -->
    <hr class="vertical-space2">
  </section>
  <!-- container -->

 <?php
			}
 ?>
<?php endif; ?>
	</div>
</section>
