<?php
/**
 * confirmSuccess.php template.
 *
 * Displays success message when a client successfully confirms receipt of alert/notification via email
 *
 * @package    frontend
 * @subpackage notifications
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
?>
Thank you for your confirmation.
