<?php
/**
 * Payment actions.
 *
 * Payment api for a remote payment gateway
 *
 * @package    frontend
 * @subpackage payment
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
class paymentActions extends sfActions
{
    /**
     * Executes 'Query' action
     *
     * Query for Invoice details
     *
     * @param sfRequest $request A request object
     */
    public function executeQueryinvoice(sfWebRequest $request)
    {
        $api_key = $request->getPostParameter("apikey");
        $api_secret = $request->getPostParameter("apisecret");
        $invoice_no = $request->getPostParameter("invoice");

        $query_details = null;

        $invoice_manager = new InvoiceManager();

        $query_details = $invoice_manager->api_query_invoice($api_key, $api_secret, $invoice_no);

        return $this->renderText(json_encode($query_details));
    }

    /**
     * Executes 'Update' action
     *
     * Query for Invoice details
     *
     * @param sfRequest $request A request object
     */
    public function executeUpdateinvoice(sfWebRequest $request)
    {
      try
      {
        $api_key = $request->getParameter("api_key");
        $api_secret = $request->getParameter("api_secret");

        $payments_manager = new PaymentsManager();

        $update_details = array();

        if($payments_manager->api_validate_request($api_key, $api_secret))
        {
          $update_details = $payments_manager->process_ipn("irembo", $_REQUEST);
          return $this->renderText(json_encode($update_details));
          return $this->renderText();
        }
        else
        {
          return $this->renderText(json_encode($update_details));
          return $this->renderText();
        }
      }
      catch(Exception $ex)
      {
        error_log("Debug-pesa: ".$ex);
      }
    }
/*OTB Start - Irembo offline payment option chosen on payment checkout form*/
    public function executeIrembooffline(sfWebRequest $request)
    {
        $this->invoice_id = $request->getParameter("invoice_id");
        $this->irembo_gateway = new IremboGateway();
        $payments_manager = new PaymentsManager();
		$payment_settings = $payments_manager->get_payment_settings($this->invoice_id);
		$this->payment_currency = $payment_settings['payment_currency'];
		$transaction = $this->irembo_gateway->sendInvoiceDetails($this->invoice_id, $payment_settings);//Send invoice details to irembo
		$this->payment_amount = $transaction->getPaymentAmount();
		$this->rol_bill_number = $transaction->getPaymentId();
		$this->setLayout("layoutdash");
    }
/*OTB End - Irembo offline payment option chosen on payment checkout form*/
}
