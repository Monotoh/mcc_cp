<?php
  use_helper('I18N');
?>
<?php echo html_entity_decode($irembo_gateway->getIremboOfflineCheckoutForm($invoice_id, $payment_currency, $payment_amount)); ?>
<!--
<div class="panel panel-default panel-blog panel-checkout">
                            <div class="panel-body">
                                <h3 class="blogsingle-title"><?php echo __("Irembo Offline Payment");?></h3>
                                <ul class="blog-meta mb5">
                                </ul>

                                <div class="panel-group mt10 mb0" id="accordion" style="border:1px solid #e7e7e7;">
<div id="offlinepayment" class="panel panel-default">
    <div id="collapseOne" class="panel-collapse collapse in">
        <div class="panel-body">
			<p><?php echo __("Payment Options");?>:</p>
            <div class="clearfix"></div>
            <img class="media-object thumbnail pull-left" src="/assets_unified/images/merchants/airtel-money.jpg" alt="" style="margin-right:10px; width:65px;"/>
            <img class="media-object thumbnail pull-left" src="/assets_unified/images/merchants/momo.jpg" alt="" style="margin-right:10px; width:65px;"/>
            <img class="media-object thumbnail pull-left" src="/assets_unified/images/merchants/tigocash.jpg" alt="" style="margin-right:10px; width:65px;"/>
            <img class="media-object thumbnail pull-left" src="/assets_unified/images/merchants/bk.jpg" alt="" style="margin-right:10px; width:65px;"/>
            <img class="media-object thumbnail pull-left" src="/assets_unified/images/merchants/bk_yacu.jpg" alt="" style="margin-right:10px; width:65px;"/>
            <div class="clearfix"></div>
            <br/>
            <div id="BK">
                <p class="alert alert-success m0" style="padding:20px; color:000; border-radius:5px; background:fff; border: 1px solidg 428bca"><?php echo __("Follow the Steps below to pay your bill via Bank of Kigali");?> (<?php echo $payment_currency; ?> <?php echo $payment_amount; ?>)</p>
				<ol class="mt10">
					<li><?php echo __("Go to any Bank of Kigali branch or YACU agent");?></li>
					<li><?php echo __("Provide your billing number");?>: <span style="font-size:12px; font-weight: bold;"><?php echo $rol_bill_number; ?></span> <?php echo __("as part of the payment information");?>.</li>
					<li><?php echo __("Once your payment is successful, your permit will be automatically issued");?>.</li>
				</ol>
            </div>
            <div id="Airtel">
                <p class="alert alert-success m0" style="padding:20px; color:000; border-radius:5px; background:fff; border: 1px solidg 428bca"><?php echo __("Follow the Steps below to pay your bill via Airtel Money");?> (<?php echo $payment_currency; ?> <?php echo $payment_amount; ?>)</p>
				<ol class="mt10">
					<li><?php echo __("Go to Airtel Money on your phone");?></li>
					<li><?php echo __("Use your billing number");?>: <span style="font-size:12px; font-weight: bold;"><?php echo $rol_bill_number; ?></span></li>
					<li><?php echo __("You will receive a confirmation SMS");?></li>
					<li><?php echo __("Once your payment is successful, your permit will be automatically issued");?>.</li>
				</ol>
            </div>
            <div id="Mtn">
                <p class="alert alert-success m0" style="padding:20px; color:000; border-radius:5px; background:fff; border: 1px solidg 428bca"><?php echo __("Follow the Steps below to pay your bill via MTN Mobile Money");?> (<?php echo $payment_currency; ?> <?php echo $payment_amount; ?>)</p>
				<ol class="mt10">
					<li><?php echo __("Go to MTN Mobile Money on your phone");?></li>
					<li><?php echo __("Use your billing number");?>: <span style="font-size:12px; font-weight: bold;"><?php echo $rol_bill_number; ?></span></li>
					<li><?php echo __("You will receive a confirmation SMS");?></li>
					<li><?php echo __("Once your payment is successful, your permit will be automatically issued");?>.</li>
				</ol>
            </div>
            <div id="Tigo">
                <p class="alert alert-success m0" style="padding:20px; color:000; border-radius:5px; background:fff; border: 1px solidg 428bca"><?php echo __("Follow the Steps below to pay your bill via Tigo Cash");?> (<?php echo $payment_currency; ?> <?php echo $payment_amount; ?>) </p>
				<ol class="mt10">
					<li><?php echo __("Go to Tigo cash on your phone");?></li>
					<li><?php echo __("Use your billing number");?>: <span style="font-size:12px; font-weight: bold;"><?php echo $rol_bill_number; ?></span></li>
					<li><?php echo __("You will receive a confirmation SMS");?></li>
					<li><?php echo __("Once your payment is successful, your permit will be automatically issued");?>.</li>
				</ol>
            </div>
<br>
        </div>
    </div>
</div>

                                </div>
                            </div>
</div>
	-->
