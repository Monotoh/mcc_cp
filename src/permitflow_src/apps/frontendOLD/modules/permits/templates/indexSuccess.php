<?php
/**
 * indexSuccess.php template.
 *
 * Displays list of all of the currently logged in client's permits
 *
 * @package    frontend
 * @subpackage permits
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper("I18N");
?>

<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title"><?php echo __('Downloads') ?></h4>
    </div>
</div>
<!-- Page-Title -->

<div class="row">
    <div class="col-lg-12">
        <div class="card-box">
        <?php if ($pager->getResults()): ?>
        <div class="table-responsive">
        <table class="table">
            <thead>
            <th><?php echo __('Service'); ?></th>
            <th><?php echo __('Ref No'); ?></th>
            <th><?php echo __('Date Issued'); ?></th>
            <th class="no-sort" width="80"><?php echo __('Action'); ?></th>
            </thead>
            <tbody>
            <?php foreach ($pager->getResults() as $permit): ?>
                <tr>
                    <td>
                        <?php
                        echo $permit->getTemplate()->getTitle();
                        ?>
                    </td>
                    <td> <a class="table-item-title" title='<?php echo __('View'); ?>' href='<?php echo public_path(); ?>index.php/application/view/id/<?php echo  $permit->getFormEntry()->getId(); ?>'>
                            <?php echo  $permit->getFormEntry()->getApplicationId(); ?>
                        </a>
                    </td>
                    <td>

                        <?php echo  $permit->getFormEntry()->getDateOfResponse(); ?>

                    </td>

                    <td>
                        <a title='<?php echo __('View'); ?>' href='<?php echo public_path(); ?>index.php/permits/view/id/<?php echo $permit->getId(); ?>'>
                            <i class="fa fa-eye"></i></a>
                        <?php if($permit->getPdfPath()){ ?>
                        <a title='<?php echo __('Print'); ?>' href='<?php echo $permit->getPdfPath(); ?>'>
                            <i class="fa fa-print"></i></a>
                        <?php } ?>
                    </td>
                </tr>
            <?php
            endforeach;
            ?>
            </tbody>
            <tfoot>
            <tr>
                <th colspan="12">
                    <p class="table-showing pull-left"><strong><?php echo $pager->getNbResults(); ?></strong> downloads in this stage

                        <?php if ($pager->haveToPaginate()): ?>
                            - page <strong><?php echo $pager->getPage() ?>/<?php echo $pager->getLastPage() ?></strong>
                        <?php endif; ?></p>

                    <?php if ($pager->haveToPaginate()): ?>
                        <ul class="pagination pagination-sm mb0 mt0 pull-right">
                            <li><a href="/index.php/permits/index/page/1">
                                    <i class="fa fa-angle-left"></i>
                                </a></li>

                            <li> <a href="/index.php/permits/index/page/<?php echo $pager->getPreviousPage() ?>">
                                    <i class="fa fa-angle-left"></i>
                                </a></li>

                            <?php foreach ($pager->getLinks() as $page): ?>
                                <?php if ($page == $pager->getPage()): ?>
                                    <li class="active"><a href=""><?php echo $page ?></a>
                                <?php else: ?>
                                    <li><a href="/index.php/permits/index/page/<?php echo $page ?>"><?php echo $page ?></a></li>
                                <?php endif; ?>
                            <?php endforeach; ?>

                            <li> <a href="/index.php/permits/index/page/<?php echo $pager->getNextPage() ?>">
                                    <i class="fa fa-angle-right"></i>
                                </a></li>

                            <li> <a href="/index.php/permits/index/page/<?php echo $pager->getLastPage() ?>">
                                    <i class="fa fa-angle-right"></i>
                                </a></li>
                        </ul>
                    <?php endif; ?>

                </th>
            </tr>
            </tfoot>
        </table>
    </div><!-- table-responsive -->
    <?php else: ?>
        <div class="table-responsive">
            <table class="table dt-on-steroids mb0">
                <tbody>
                <tr><td>
                        <?php echo __('No Records Found') ?>
                    </td></tr>
                </tbody>
            </table>
        </div>
    <?php endif; ?>
    </div>
</div>
</div>
