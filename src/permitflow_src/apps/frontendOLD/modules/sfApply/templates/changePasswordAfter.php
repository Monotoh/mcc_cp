<?php use_helper('I18N') ?>
<div class="contentpanel">

    <div class="row">
        <div class="col-sm-6 col-lg-8">
            <div class="card-box">
                <div class="alert alert-success">
                    <p> <?php echo __('Password Changed successfuly') ?> </p> 
                </div>
                <?php include_partial('sfApply/continue') ?>
            </div> </div> 
    </div> 
</div>
