  <div class="content">
<ul class="breadcrumb">
              <li><a href="#"><?php echo __("Home")?></a> <span class="divider">/</span></li>
              <li class="active"><?php echo __("Account Activation");?></li>
            </ul>    <!-- Docs nav
    ================================================== -->
    <div class="row">
     
      <div class="span12" >



        <!-- Download
        ================================================== -->
        <section id="content-container">
        
          
		<div class="span4 offset4 well2 padded-20" >
			<legend><?php echo __("Account Activated");?></legend>
              
              <div class="table-box">
            <div class="alert alert-info">
                 <h4 class="alert-heading"><?php echo __("Thank you for activating your account");?></h4>
                <?php echo __("You may now login and submit an application for review and approval.");?>
              </div>
			
			</div>
		</div>
			
			
			
			
			
			
        </section>
		  
      </div>
      </div>
