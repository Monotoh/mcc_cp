 <?php echo $form->renderGlobalErrors() ?>
<?php use_helper('I18N'); ?>
<form id="fm1" class="form-horizontal" action="<?php echo url_for('@sf_guard_signin') ?>" method="post">
    <!-- Félicitations, votre serveur est en ligne ! Vou pouvez maintenant essayer le service d'authentification par défaut, qui authentifie lorsque le mot de passe est égal au nom d'utilisateur. -->
    <!--h2>Veuillez entrer nom de l'utilisateur et le mot de passe</h2-->
     <?php if(isset($form['_csrf_token'])): ?>
     <?php echo $form['_csrf_token']->render(); ?>
    <?php endif; ?>
    <div class="col-md-12">
         <?php if ($sf_user->getFlash('notice')):?>
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert"></button> 
                    <?php echo __('Sorry the username or password entered is incorrect!') ?>
           </div>
          <?php endif; ?>
			<!--OTB Start: show change_username_success notice-->
         <?php if ($sf_user->getFlash('change_username_success')):?>
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert"></button> 
                    <?php echo $sf_user->getFlash('change_username_success') ?>
           </div>
			<!--OTB End: show change_username_success notice-->		   
          <?php endif; ?>
        <div class="form-group">
            <label for="" class="col-md-4 control-label"><?php echo __('Username') ?>:</label>
            <div class="col-md-8">
                <?php echo $form['username']->renderError() ?>
                <input id="signin_username" name="signin[username]"  class="required form-control" tabindex="1" type="text" value="" size="25" autocomplete="off"/>
               
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-4 control-label"><?php echo __("Password"); ?>:</label>
            <div class="col-md-8">
                <input name="signin[password]" id="signin_password" class="required form-control" tabindex="2" type="password" value="" size="25" autocomplete="off"/>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-4 control-label"></label>
            <div class="col-md-8">
                <span class="form-control-static">

                    <a href="<?php echo public_path(); ?>index.php/reset-request">
                       <?php echo __('Forgot password') ?></a>
                    &#160;
                    <a class="login-register" style="" href="<?php echo url_for('@sf_guard_register'); ?> ">
                        <i class="fa fa-pencil-square-o"></i>
                        <?php echo __('Sign Up') ?>
                    </a>

                </span>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-4 control-label"></label>
            <div class="col-md-8">

                <input class="btn btn-default" name="submit" accesskey="l" value="<?php echo __('Login') ?>" tabindex="4" type="submit" />
               
            </div>
        </div>


    </div>

</form>
