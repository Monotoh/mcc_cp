<?php
/**
 * indexSuccess.php template.
 *
 * Displays login form for clients
 *
 * @package    sfDoctrineGuardPlugin
 * @subpackage sfGuardAuth
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
  use_helper('I18N');
?>

<div class="breadcrumb-box">
<div class="container">
<ul class="breadcrumb">
<li>
<a href="index.html"><?php echo __("Home"); ?></a>
<span class="divider">/</span>
</li>
<li class="active"><?php echo __("Sign In"); ?></li>
</ul>
</div>
</div>

  <section id="headline">
    <div class="container">
      <h3><?php echo __("Sign In"); ?></h3>
    </div>
</section>

         <!-- BEGIN CONTAINER -->
        <div class="container margin-bottom-40">
           <div class="row signinpanel">

            <div class="eleven columns offset-by-five login-signup-page mb40">

                <form method="post" id="contact-form" action="<?php echo url_for('@sf_guard_signin') ?>">
                <?php if(isset($form['_csrf_token'])): ?>
				<?php echo $form['_csrf_token']->render(); ?>
                <?php endif; ?>
                    <h4 class="nomargin"><?php echo __("Please Sign In"); ?></h4>
                    <p class="mt5 mb20"><?php echo __("Login to access your account"); ?>.</p>

                    <input type="text" class="form-control uname" placeholder="<?php echo __("Username or Email"); ?>" name="signin[username]" id="signin_username" />
                    <input type="password" class="form-control pword" placeholder="<?php echo __("Password"); ?>" name="signin[password]" id="signin_password"/>
                    <button class="btn btn-success btn-block"><?php echo __("Sign In"); ?></button>
                    <a class="btn btn-danger btn-block" style="color:white;" href="<?php echo public_path(); ?>index.php/reset-request"><?php echo __("Can't access your account?"); ?></a>

                 <!-- col-sm-5 -->
                </form>
            </div>
        </div><!-- row -->
        </div>
        <!-- END CONTAINER -->
