<script src="<?php echo public_path(); ?>assets_unified/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo public_path(); ?>assets_unified/js/jquery-migrate-1.2.1.min.js"></script>
<script language="javascript">
    $('document').ready(function () {
        $('#sfApplyApply_username').keyup(function () {
            $.ajax({
                type: "POST",
                url: "/index.php/index/checkuser",
                data: {
                    'name': $('input:text[id=sfApplyApply_username]').val()
                },
                dataType: "text",
                success: function (msg) {
                    //Receiving the result of search here
                    // alert('Test...');
                    $("#usernameresult").html(msg);
                    if (msg == '0') {
                        $("#usernameresult").html('<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Username is already in use!</strong></div>');
                        $('#submit_app').prop('disabled', true);
                    }
                    if (msg == '1') {
                        $("#usernameresult").html('<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Username is available!</strong></div>');
                        $('#submit_app').prop('disabled', false);
                    }
                }
            });
            // alert('Test...');
        });
    });
</script>

<?php
/**
 * indexSuccess.php template.
 *
 * Displays registration form for clients
 *
 * @package    sfDoctrineApplyPlugin
 * @subpackage sfGuardRegister
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper('I18N');
?>

<div class="content">
    <section>
        <div class="container">
            <div class="row" style="margin-top:20px">
                <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h4 class="text-center"><?php echo __('Please Sign up for an account'); ?> </h4>
                             <hr class="colorgraph">
                        </div>
                    <div class="panel-body">
                        
                    <form role="form" method="post" id="registration_form" name="registration_form" action="<?php echo public_path(); ?>index.php/apply" method="post" enctype="multipart/form-data" autocomplete="off" data-ajax="false"  onsubmit="javascript:return validateall();">
                        
                         <fieldset>                       
                        <?php
                        $class = sfConfig::get('app_sf_apply_apply_form', 'sfApplyApplyForm');
                        $form = new $class();
                        ?>
                        <?php if (isset($form['_csrf_token'])): ?>
                            <?php echo $form['_csrf_token']->render(); ?>
                        <?php endif; ?>

                        <?php if ($sf_user->hasFlash('email_error')): ?>
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert"></button>
                                <strong> <?php echo $sf_user->getFlash('email_error') ?> </strong> 
                            </div>
                        <?php endif ?>
                        <?php if ($sf_user->hasFlash('username_error')): ?>
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert"></button>
                                <strong> <?php echo $sf_user->getFlash('username_error') ?> </strong> 
                            </div>
                        <?php endif ?>
                        <?php if ($sf_user->hasFlash('pass_error')): ?>
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert"></button>
                                <strong> <?php echo $sf_user->getFlash('pass_error') ?> </strong> 
                            </div>
                        <?php endif ?>
                        <?php if ($sf_user->hasFlash('notice')): ?>
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert"></button>
                                <strong> <?php echo $sf_user->getFlash('notice') ?> </strong> 
                            </div>
                        <?php endif ?>
                        <div class="form-group">                           
                            <input placeholder="<?php echo __('Full Name'); ?>" class="form-control" type="text" name="sfApplyApply[fullname]" id="sfApplyApply_fullname" required>
                        </div>

                        <div class="form-group">                          
                            <input placeholder="<?php echo __('Username'); ?>" class="form-control" type="text" name="sfApplyApply[username]" id="sfApplyApply_username" required>
                            <div id='checkusername' name='checkusername'></div>
                        </div>

                        <div id="usernameresult" name="usernameresult"></div> 
                        <div class="form-group">
                           
                            <input placeholder="<?php echo __('Enter Email'); ?>" type="email"  class="form-control" name="sfApplyApply[email]" id="sfApplyApply_email" required>
                            <div id='checkemail' name='checkemail'></div>
                        </div>

                        <div id="emailresult" name="emailresult"></div>

                        <script language="javascript">
                            $('document').ready(function () {
                                $('#sfApplyApply_email').keyup(function (e) {
                                    //submit only after enter key
                                    //if(e.keyCode == 13) {
                                    //OTB Fix
                                    console.log($('#sfApplyApply_email').val());
                                    $.ajax({
                                        type: "POST",
                                        url: "/index.php/index/checkemail",
                                        data: {
                                            'email': $('#sfApplyApply_email').val(),
                                        },
                                        dataType: "text",
                                        success: function (msg) {
                                            //Receiving the result of search here
                                            // $("#emailresult").html(msg);

                                            if (msg == "invalid_email") {
                                                //invalid email
                                                $("#emailresult").html('<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Enter a valid email..</strong></div>');
                                                $('#submit_app').prop('disabled', true);
                                            }
                                            else if (msg == "email_in_use") {
                                                //email in use
                                                $("#emailresult").html('<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Email is already in use!</strong></div>');
                                                $('#submit_app').prop('disabled', true);
                                            }
                                            else if (msg == "email_valid") {
                                                $("#emailresult").html('<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Email is available!..continue..</strong></div>');
                                                // $('#submit_app').prop('disabled',false);
                                            }
                                            else {
                                                $("#emailresult").html('<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Email checking failed.. please refresh your page..</strong></div>');

                                                $('#submit_app').prop('disabled', true);
                                            }

                                        }
                                    });

                                    //}



                                });
                            });
                        </script>

                        <div class="form-group">
                            
                            <input placeholder="<?php echo __('Confirm Email'); ?>" class="form-control" type="email" name="sfApplyApply[email2]" id="sfApplyApply_email2" required>
                            <div id='confirmemail' name='confirmemail'></div>
                        </div>

                        <div id="confirmemailresult" name="confirmemailresult"></div>

                        <script language="javascript">
                            $('document').ready(function () {
                                $('#sfApplyApply_email').keyup(function () {
                                    if ($('#sfApplyApply_email').val() == $('#sfApplyApply_email2').val() && $('#sfApplyApply_email').val() != "")
                                    {
                                        $('#confirmemailresult').html('<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Emails match!</strong></div>');
                                        $('#submit_app').prop('disabled', false);
                                    }
                                    else
                                    {
                                        $('#confirmemailresult').html('<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Emails don\'t match!</strong>.</div>');
                                        $('#submit_app').prop('disabled', true);
                                    }
                                });
                                $('#sfApplyApply_email2').keyup(function () {
                                    if ($('#sfApplyApply_email').val() == $('#sfApplyApply_email2').val() && $('#sfApplyApply_email').val() != "")
                                    {
                                        $('#confirmemailresult').html('<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Emails match!</strong></div>');
                                        $.ajax({
                                            type: "POST",
                                            url: "/index.php/index/checkemail",
                                            data: {
                                                'email': $('#sfApplyApply_email').val(),
                                            },
                                            dataType: "text",
                                            success: function (msg) {
                                                //Receiving the result of search here
                                                // $("#emailresult").html(msg);

                                                if (msg == "invalid_email") {
                                                    //invalid email
                                                    $("#emailresult").html('<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Enter a valid email..</strong></div>');
                                                    $('#submit_app').prop('disabled', true);
                                                }
                                                else if (msg == "email_in_use") {
                                                    //email in use
                                                    $("#emailresult").html('<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Email is already in use!</strong></div>');
                                                    $('#submit_app').prop('disabled', true);
                                                }
                                                else if (msg == "email_valid") {
                                                    $("#emailresult").html('<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Email is available!..continue..</strong></div>');
                                                    console.log("Email valid message", msg);
                                                    //	 $('#submit_app').prop('disabled',true);
                                                }
                                                else {
                                                    $("#emailresult").html('<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Email checking failed.. please refresh your page..</strong></div>');

                                                    $('#submit_app').prop('disabled', true);
                                                }

                                            }
                                        });
                                        //  $('#submit_app').prop('disabled',false);
                                    }
                                    else
                                    {
                                        $('#confirmemailresult').html('<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Emails don\'t match! test</strong> Try again.</div>');
                                        $('#submit_app').prop('disabled', true);
                                    }
                                });
                            });
                        </script>

                        <div class="form-group">
                            
                            <input placeholder="<?php echo __('New Password'); ?>" class="form-control" type="password" title='Minimum of Six Characters and must contain numbers and characters' data-min="6" name="sfApplyApply[password]" id="sfApplyApply_password" required>
                            <div id='checkpassword' name='checkpassword'></div>
                        </div>

                        <div class="form-group">
                            
                            <input placeholder="<?php echo __('Confirm Password'); ?>" class="form-control" type="password" onKeyup='confirmpassword();'  title='Minimum of Six Characters' data-min="6"  name="sfApplyApply[password2]" id="sfApplyApply_password2" required>
                            <input type="hidden" name="sfApplyApply[id]" id="sfApplyApply_id"/>
                            <div id='confirmpassword' name='confirmpassword'></div>
                        </div>

                        <div id="passwordresult" name="passwordresult"></div>

                        <script language="javascript">
                            $('document').ready(function () {
                                $('#sfApplyApply_password').keyup(function () {


                                    if ($('#sfApplyApply_password').val() == $('#sfApplyApply_password2').val() && $('#sfApplyApply_password').val() != "" && $('#sfApplyApply_password').val().length >= 6)
                                    {
                                        $('#passwordresult').html('<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Passwords match!</strong></div>');
                                        $('#submit_app').prop('disabled', false);
                                    }
                                    else
                                    {
                                        $('#passwordresult').html('<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Passwords don\'t match!</strong> Try again.</div>');
                                        if ($('#sfApplyApply_password').val().length < 6) {
                                            var html_result = $('#passwordresult').html();
                                            $('#passwordresult').html(html_result + '<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Password length is less than 6 characters</strong>Try again.</div>');
                                        }
                                        $('#submit_app').prop('disabled', true);
                                    }
                                });
                                $('#sfApplyApply_password2').keyup(function () {
                                    if ($('#sfApplyApply_password').val() == $('#sfApplyApply_password2').val() && $('#sfApplyApply_password').val() != "" && $('#sfApplyApply_password').val().length >= 6)
                                    {
                                        $('#passwordresult').html('<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Passwords match!</strong></div>');
                                        $('#submit_app').prop('disabled', false);
                                    }
                                    else
                                    {
                                        $('#passwordresult').html('<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Passwords don\'t match!</strong> Try again</div>');
                                        if ($('#sfApplyApply_password').val().length < 6) {
                                            var html_result = $('#passwordresult').html();
                                            $('#passwordresult').html(html_result + '<div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><strong>Password length is less than 6 characters</strong>Try again.</div>');
                                        }
                                        $('#submit_app').prop('disabled', true);
                                    }
                                });
                            });
                        </script>

                        <div class="form-group">
                            
                            <input placeholder="<?php echo __('Mobile Phone'); ?>" type="text" class="form-control" name="sfApplyApply[mobile]" id="sfApplyApply_mobile" required><?php echo __('(Example Format: 0700123456)'); ?> <span> <font style="color:red"> <?php echo __('You must provide a valid phone number for notifications') ?> </font> </span>
                        </div>

                        <div class="form-group">
                           <label for="select" class="control-label">Register as</label>
                            <select class="form-control" name="sfApplyApply[registeras]" id="sfApplyApply_registeras" required>
                                <?php
                                $q = Doctrine_Query::create()
                                        ->from("SfGuardUserCategories a")
                                        ->orderBy("a.orderid ASC");

                                $cats = $q->execute();

                                foreach ($cats as $cat) {

                                    echo "<option value='" . $cat->getId() . "'>" . $cat->getName() . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="row">
                             <div class="col-xs-6 col-sm-6 col-md-6">
                                  <button class="btn btn-success" type="submit" name="submit_app" id="submit_app" value="submitbuttonvalue"><?php echo __('Save and Continue'); ?></button>
                             </div>
                         </div>
                        <p class="text-center help-block">
                            
                                <?php echo __('By clicking on "Save and Continue" above, you are agreeing to the'); ?> <a href="<?php echo public_path(); ?>index.php/terms/index " target="_blank"><?php echo __('Terms of services'); ?></a>
                           
                        </p>
                        
                         <hr class="colorgraph">
                         
                     </fieldset>
                    </form>
                </div> </div>
            </div>

        </div>
    </section>
</div>  
