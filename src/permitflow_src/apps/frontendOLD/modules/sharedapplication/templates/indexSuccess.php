<?php
/**
 * indexSuccess.php template.
 *
 * Displays list of all of the currently logged in client's shared applications
 *
 * @package    frontend
 * @subpackage sharedapplication
 * @author     Webmasters Africa / Thomas Juma (thomas.juma@webmastersafrica.com)
 */
use_helper('I18N');

function GetDays($sEndDate, $sStartDate){  
        $aDays[] = $start_date;
	$start_date  = $sStartDate;
	$end_date = $sEndDate;
	$current_date = $start_date;
	while(strtotime($current_date) <= strtotime($end_date))
	{
		    $aDays[] = gmdate("Y-m-d", strtotime("+1 day", strtotime($current_date)));
		    $current_date = gmdate("Y-m-d", strtotime("+1 day", strtotime($current_date)));
	}
        return $aDays;  
} 
?>
<?php
	$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
	mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
	
	
	
	$filter = $_GET['filter'];
	if(!empty($filter))
	{
		$filter = " AND b.approved = '".$filter."'";
	}
	
	
?>
   <div class="pageheader">
      <!--<h2><i class="fa fa-share"></i>Shared Applications<span>This page list all the applications shared to you by others</span></h2>-->
      <div class="breadcrumb-wrapper">
        
        <ol class="breadcrumb">
          <li><a href="#"><?php echo __('Shared Applications'); ?></a></li>
          <li class="active"><?php echo __('Submitted Plans'); ?></li>
        </ol>
      </div>
    </div>
	<div class="panel panel-success mb0">
			  <div class="panel-heading">
				  <h3 class="panel-title" style="color: white;"><i class="fa fa-share"></i><?php echo __('Shared Applications'); ?><span></h3>
				  <p class="text-muted" style="color: white;"><?php echo __('This page list all the applications shared to you by others'); ?></span>...</p>
				</div>
	</div>

    
    
    
<div class="contentpanel">

    <div class="row">



       
    
 <?php /*?><select  class="span5" id='application_status' name='application_status' onChange="window.location='<?php echo public_path(); ?>index.php/sharedapplication/index?filter=' + this.value;">
			    <option value="" >Filter By Plan Navigation</option>
				<?php
				$q = Doctrine_Query::create()
				  ->from('Menus a')
				  ->orderBy('a.order_no ASC');
				$stagegroups = $q->execute();
				foreach($stagegroups as $stagegroup)
				{
				    echo "<optgroup label='".$stagegroup->getTitle()."'>";
					$q = Doctrine_Query::create()
					  ->from('SubMenus a')
					  ->where('a.menu_id = ?', $stagegroup->getId())
					  ->orderBy('a.order_no ASC');
					$stages = $q->execute();
					
					foreach($stages as $stage)
					{
					    $selected = "";
						
						if($_GET['filter'] != "" && $_GET['filter'] == $stage->getId())
						{
							$selected = "selected";
						}
						
						$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
						mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
						$query = "SELECT a.*, b.* FROM form_entry a LEFT JOIN form_entry_shares b ON a.id = b.formentryid  WHERE a.approved = '".$stage->getId()."' AND b.formentryid <> ''";
						$result = mysql_query($query,$dbconn);

						
						echo "<option value='".$stage->getId()."' ".$selected.">".$stage->getTitle()." (".mysql_num_rows($result).")</option>";
					}
				    echo "</optgroup>";
				}
				
				echo "<optgroup label='Others'>";
				
				$q = Doctrine_Query::create()
				  ->from('SubMenus a')
				  ->where('a.menu_id = ?', '0')
				  ->orderBy('a.order_no ASC');
				$stages = $q->execute();
				
				foreach($stages as $stage)
				{
					$selected = "";
					
					if($application_status != "" && $application_status == $stage->getId())
					{
						$selected = "selected";
					}
					
					$q = Doctrine_Query::create()
						 ->from('FormEntry a')
						 ->where('a.user_id = ?', $sf_user->getGuardUser()->getId())
						 ->andWhere('a.approved = ?', $stage->getId());
					$menuapplications = $q->execute();
					
					echo "<option value='".$stage->getId()."' ".$selected.">".$stage->getTitle()." (".sizeof($menuapplications).")</option>";
				
				}
				
				echo "</optgroup>";
				
				?>
			</select><?php */?>
                   
                            
                
    
    
    
    
     <div class="row">
            <!--<div class="col-sm-3 col-lg-2">
                <a class="btn btn-warning btn-block btn-compose-email" href="<?php echo public_path(); ?>index.php/application/groups">Submit Application</a>
                
                <ul class="nav nav-pills nav-stacked nav-email">
                    <li>
                    <a href="<?php echo public_path(); ?>index.php/application/index">
                        <span class="badge pull-right"><?php echo sizeof($sharedapplications); ?></span>
                        <i class="glyphicon glyphicon-inbox"></i> My Applications
                        
                    </a>
                    </li>
                    
                    <li class="active">
                    <a href="<?php echo public_path(); ?>index.php/sharedapplication/index">
                    <span class="badge pull-right"><?php echo sizeof($sharedapplications); ?></span>
                    <i class="glyphicon glyphicon-send"></i>  <?php echo sfConfig::get('app_'.$_SESSION['locale'].'_shared_applications'); ?> </a>
                  </li>
                
                 <li>
                  <a href="<?php echo public_path(); ?>index.php/application/drafts">
                  <span class="badge pull-right"><?php echo sizeof($draftapplications); ?></span>
                  <i class="glyphicon glyphicon-pencil"></i>
                   <?php echo sfConfig::get('app_'.$_SESSION['locale'].'_drafts'); ?> 
                   </a>
                 </li>
                </ul>
                
              </ul>
                
            </div> -->
      
      
      
      
           <div class="col-sm-12 col-lg-12">
                  
                    
                    <?php

if($_GET['filter'])
{
	$q = Doctrine_Query::create()
	   ->from("FormEntryShares a")
	   ->where("a.receiverid = ?", $sf_user->getGuardUser()->getId())
	   ->orWhere("a.senderid = ?", $sf_user->getGuardUser()->getId())
	   ->orderBy("a.id DESC");
	   
	   

	 $pager = new sfDoctrinePager('FormEntryShares', 10);
	 $pager->setQuery($q);
	 $pager->setPage($page);
	 $pager->init();
	
	 $counter = 1;
	 include_partial('sharedapplication/list', array('sharedapplications' => $pager->getResults(),'filter' => $_GET['filter']));
	 
}
else
{
	$q = Doctrine_Query::create()
	   ->from("FormEntryShares a")
	   ->where("a.receiverid = ?", $sf_user->getGuardUser()->getId())
	   ->orWhere("a.senderid = ?", $sf_user->getGuardUser()->getId())
	   ->orderBy("a.id DESC");
	   
	 $pager = new sfDoctrinePager('FormEntryShares', 10);
	 $pager->setQuery($q);
	 $pager->setPage($page);
	 $pager->init();
	
	 $counter = 1;
	 include_partial('sharedapplication/list', array('sharedapplications' => $pager->getResults()));
	 

}
?>
              
              
              <?php /*?><ul class="pagination">
<?php if ($pager->haveToPaginate()): ?>
	<?php
    $filter = "";
	if($_GET['filter'])
	{
		$filter = "&filter=".$_GET['filter'];
	}
  ?>
  <li><?php echo "<a title='First' href='".public_path()."index.php/sharedapplication/index?page=1".$filter."'><i class=\"fa fa-angle-left\"></i></a>"; ?></li>
  <li><?php echo "<a title='Previous' href='".public_path()."index.php/sharedapplication/index?page=".$pager->getPreviousPage()."".$filter."'><i class=\"fa fa-angle-left\"></i></a>"; ?></li>
  
  <?php foreach ($pager->getLinks() as $page): ?>
    <?php
	if($pager->getPage() == $page)
	{
		?>
		<li class="active"><a><?php echo $page; ?></a></li>
		<?php
	}
	else
	{
	?>
		<li><?php echo "<a title='Page ".$page."' href='".public_path()."index.php/sharedapplication/index?page=".$page."".$filter."'>".$page."</a>"; ?></li>
	<?php
	}
	?>
  <?php endforeach; ?>

 <li> <?php echo "<a title='Next' href='".public_path()."index.php/sharedapplication/index?page=".$pager->getNextPage()."".$filter."'><i class=\"fa fa-angle-right\"></i></a>"; ?></li>
 <li> <?php echo "<a title='Last' href='".public_path()."index.php/sharedapplication/index?page=".$pager->getLastPage()."".$filter."'><i class=\"fa fa-angle-right\"></i></a>"; ?></li>
<?php endif; ?>
</ul><!-- /.pagination --><?php */?>

    
       
            </div><!-- col-sm-9 -->
        </div><!-- row -->
  
  
                

</div><!-- row -->
         
 <div><!-- content panel -->       
                             



