<?php
/**
 * Frontend Layout.
 *
 * Main layout for the frontend
 *
 * @package   Frontend
 * @theme   Lesotho.CP
 * @author    Webmasters Africa (info@webmastersafrica.com)
 */

 //Logout backend users so they don't clash with frontend security & set language
 include_component('index', 'checksession')
?>

<!DOCTYPE html>
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <!-- Mobile Specific Metas
  ================================================== -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="description" content="Construction Permit System">
  <meta name="author" content="Maseru City">
  <title>Maseru City - Construction Permit</title>
  <meta name="google-site-verification" content="ONcGyvUUurvZATdyWT2N2WjbfWuiBA9pxTeT-EF-yDc" />
 <!-- Keeping all css in one file. Keeping the layout tidy. -->
  <?php include_component('index', 'stylesheets') ?>

  <?php
  if(sfConfig::get('app_zopim_key'))
  {
  ?>
  <!--Start of Zopim Live Chat Script-->
  <script type="text/javascript">
  window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
  d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
  _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
  $.src="//v2.zopim.com/?<?php echo sfConfig::get('app_zopim_key'); ?>";z.t=+new Date;$.
  type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
  </script>
  <!--End of Zopim Live Chat Script-->
  <?php
  }
  ?>
</head>

<body>

<div id="main-container">

    <?php include_component('index', 'header') ?>

    <?php
     if($sf_user->hasFlash("notice"))
     {
     ?>
     <div class="col-md-offset-3 col-md-6 alert alert-danger" style="margin-top:30px;">
          <?php echo __("The username or password you entered is incorrect."); ?>
     </div>
     <?php
     }
     ?>

     <!-- BEGIN CONTAINER -->
        <span id="content">
    <!-- The following div must have id='content' or else file uploads will fail -->
        <?php echo $sf_content ?>
    </span>
        <!-- END CONTAINER -->
    <?php
  //Closing a hanging div if we are not in the homepage
    if($_SERVER['REQUEST_URI'] != "/index.php" && $_SERVER['REQUEST_URI'] != "/" && $_SERVER['REQUEST_URI'] != "/index.php/")
  {
    ?>
    
  
  <?php
  }
  ?>

<!-- Keeping all javascripts in one file. Keeping the layout tidy. -->
  <?php include_component('index', 'footer') ?>

</div><!-- main-container -->

<!-- GO TOP -->
  <a id="scroll-up"><i class="fa fa-angle-up"></i></a>

<!-- Keeping all javascripts in one file. Keeping the layout tidy. -->
  <?php include_component('index', 'javascripts') ?>

</body>
</html>
