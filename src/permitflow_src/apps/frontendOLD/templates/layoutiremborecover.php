<!DOCTYPE html> <!--<html lang="en">--> 
<?php
 use_helper('I18N');
?>
<html class="aui ltr" dir="ltr" lang="en"> 
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <head> 
        <meta charset="utf-8"> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
        <meta name="viewport" content="width=device-width, initial-scale=1"> 
        <meta name="description" content="eBPMIS-Rwanda Building Permit Management System" />
      <!--  <meta name="google-site-verification" content="dznzuaAMd7MknqBcnYYXCNnzHcvPYqNMFJAO_S_3824" />--> 
        <title><?php echo __('User Account Recovery - Rwanda Building Permitting System') ?></title> 
        <meta content="initial-scale=1.0, width=device-width" name="viewport" />
        <meta content="text/html; charset=UTF-8" http-equiv="content-type" /> 
        <link href="<?php echo public_path(); ?>irembo/rol-theme/images/favicon.ico" rel="Shortcut Icon" /> 
        <link href="public-user-registration.html" rel="canonical" /> 
        <link href="en/public-user-registration.html" hreflang="en" rel="alternate" /> 
        <link href="public-user-registration.html" hreflang="x-default" rel="alternate" /> 
        <link href="public-user-registration.html" hreflang="rw" rel="alternate" />
        <link href="fr/public-user-registration.html" hreflang="fr" rel="alternate" />
        <link class="lfr-css-file" href="<?php echo public_path(); ?>irembo/rol-theme/css/auid0ff.css?browserId=other&amp;themeId=rol_WAR_roltheme&amp;minifierType=css&amp;languageId=rw&amp;b=6210&amp;t=1472157203000" rel="stylesheet" type="text/css" /> 
        <link href="html/css/maind0ff.css?browserId=other&amp;themeId=rol_WAR_roltheme&amp;minifierType=css&amp;languageId=rw&amp;b=6210&amp;t=1472157203000" rel="stylesheet" type="text/css" /> <link href="html/portlet/journal_content/css/main84ee.css?browserId=other&amp;themeId=rol_WAR_roltheme&amp;minifierType=css&amp;languageId=rw&amp;b=6210&amp;t=1472242170000" rel="stylesheet" type="text/css" />
        <script type="text/javascript">var Liferay = {Browser:{acceptsGzip:function(){return true}, getMajorVersion:function(){return 0}, getRevision:function(){return""}, getVersion:function(){return""}, isAir:function(){return false}, isChrome:function(){return false}, isFirefox:function(){return false}, isGecko:function(){return false}, isIe:function(){return false}, isIphone:function(){return false}, isLinux:function(){return false}, isMac:function(){return false}, isMobile:function(){return false}, isMozilla:function(){return false}, isOpera:function(){return false}, isRtf:function(){return false}, isSafari:function(){return false}, isSun:function(){return false}, isWap:function(){return false}, isWapXhtml:function(){return false}, isWebKit:function(){return false}, isWindows:function(){return true}, isWml:function(){return false}}, Data:{NAV_SELECTOR:"#navigation", isCustomizationView:function(){return false}, notices:[null]}, ThemeDisplay:{getLayoutId:function(){return"713"}, getLayoutURL:function(){return"https://bpmis.gov.rw"}, getParentLayoutId:function(){return"0"}, isPrivateLayout:function(){return"false"}, isVirtualLayout:function(){return false}, getBCP47LanguageId:function(){return"rw"}, getCDNBaseURL:function(){return"https://bpmis.gov.rw"}, getCDNDynamicResourcesHost:function(){return""}, getCDNHost:function(){return""}, getCompanyId:function(){return"20154"}, getCompanyGroupId:function(){return"20194"}, getDefaultLanguageId:function(){return"rw"}, getDoAsUserIdEncoded:function(){return""}, getLanguageId:function(){return"rw"}, getParentGroupId:function(){return"20181"}, getPathContext:function(){return"/rolportal"}, getPathImage:function(){return"/rolportal/image"}, getPathJavaScript:function(){return"/rolportal/html/js"}, getPathMain:function(){return"/rolportal/c"}, getPathThemeImages:function(){return"https://bpmis.gov.rw"}, getPathThemeRoot:function(){return"/rol-theme"}, getPlid:function(){return"33247"}, getPortalURL:function(){return"https://bpmis.gov.rw"}, getPortletSetupShowBordersDefault:function(){return false}, getScopeGroupId:function(){return"20181"}, getScopeGroupIdOrLiveGroupId:function(){return"20181"}, getSessionId:function(){return""}, getSiteGroupId:function(){return"20181"}, getURLControlPanel:function(){return"/rolportal/group/control_panel?refererPlid=33247"}, getURLHome:function(){return"https\x3a\x2f\x2fbpmis\x2egov\x2erw\x2frolportal\x2fweb\x2frol\x2fhome"}, getUserId:function(){return"20158"}, getUserName:function(){return""}, isAddSessionIdToURL:function(){return false}, isFreeformLayout:function(){return false}, isImpersonated:function(){return false}, isSignedIn:function(){return false}, isStateExclusive:function(){return false}, isStateMaximized:function(){return false}, isStatePopUp:function(){return false}}, PropsValues:{NTLM_AUTH_ENABLED:false}}; var themeDisplay = Liferay.ThemeDisplay; Liferay.AUI = {getAvailableLangPath:function(){return"available_languages.jsp?browserId=other&themeId=rol_WAR_roltheme&colorSchemeId=01&minifierType=js&languageId=rw&b=6210&t=1472242170000"}, getCombine:function(){return true}, getComboPath:function(){return"/rolportal/combo/?browserId=other&minifierType=&languageId=rw&b=6210&t=1472242170000&"}, getFilter:function(){return"min"}, getJavaScriptRootPath:function(){return"/rolportal/html/js"}, getLangPath:function(){return"aui_lang.jsp?browserId=other&themeId=rol_WAR_roltheme&colorSchemeId=01&minifierType=js&languageId=rw&b=6210&t=1472242170000"}}; Liferay.authToken = "2yA7iHeM"; Liferay.currentURL = "\x2frolportal\x2fpublic-user-registration"; Liferay.currentURLEncoded = "%2Frolportal%2Fpublic-user-registration";</script> <script src="html/js/barebone55cd.jsp?browserId=other&amp;themeId=rol_WAR_roltheme&amp;colorSchemeId=01&amp;minifierType=js&amp;minifierBundleId=javascript.barebone.files&amp;languageId=rw&amp;b=6210&amp;t=1472242170000" type="text/javascript"></script> <script type="text/javascript">Liferay.Portlet.list = ["56_INSTANCE_7mAyand1Rbel"];</script> <script type="text/javascript">var _gaq = _gaq || []; _gaq.push(["_setAccount", "UA-105880137-1"]); _gaq.push(["_trackPageview"]); (function(){var b = document.createElement("script"); b.async = true; b.src = ("https:" == document.location.protocol?"https://ssl":"http://www") + ".google-analytics.com/ga.js"; b.type = "text/javascript"; var a = document.getElementsByTagName("script")[0]; a.parentNode.insertBefore(b, a)})();</script> <link class="lfr-css-file" href="<?php echo public_path(); ?>irembo/rol-theme/css/maind0ff.css?browserId=other&amp;themeId=rol_WAR_roltheme&amp;minifierType=css&amp;languageId=rw&amp;b=6210&amp;t=1472157203000" rel="stylesheet" type="text/css" /> <style type="text/css"></style> <!-- Bootstrap --> <link href="<?php echo public_path(); ?>irembo/themes/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/> <link href="<?php echo public_path(); ?>irembo/themes/css/bootstrap.min.css" rel="stylesheet"> <link href="<?php echo public_path(); ?>irembo/themes/css/style.css" rel="stylesheet"> <link href="<?php echo public_path(); ?>irembo/themes/css/custom.css" rel="stylesheet"> <link href="<?php echo public_path(); ?>irembo/rol-theme/css/custom.css" rel="stylesheet"> <link href="<?php echo public_path(); ?>irembo/themes/css/home.css" rel="stylesheet"> <link href="<?php echo public_path(); ?>irembo/themes/css/print.css" rel="stylesheet"> <!--<link href='//fonts.googleapis.com/css?family=Open+Sans:400,800,300,600' rel='stylesheet' type='text/css'>--> </head> <script>function searchSubmit(){var a = escape(document.getElementById("searchKey").value); if (a != ""){window.location = "/rolportal/web/rol/search?_3_formDate=&p_p_id=3&p_p_lifecycle=0&p_p_state=maximized&p_p_mode=view&_3_struts_action=%2Fsearch%2Fsearch&_3_cur=1&_3_format=&_3_keywords=" + a + "&_3_groupId=0"}}function setLanguage(a){if (typeof (Storage) !== "undefined"){localStorage.setItem("language", a)}}try{if (top.location != self.location && top.origin != self.origin){top.location = self.location.href}} catch (e){top.location = self.location.href};</script> 
         <body class="home-page" onload="setLanguage('en')"> 
          <div id="popup-alert-blocker"></div> <!--<div id="lfr-spinner" class="loader-status loader-status-start"><img src='/themes/assets/css/loader.gif' width='32' height='32'></div>--> <div class="navbar navbar-default " role="navigation"> 
            <div class="home-page-top-nav "> <div class="container relative"> 
                    <?php include_component('index', 'logoheader') ?>
                    <div class="login-icon margin-top-10"> 

                        <ul> 
                             <!--<li class="hidden-xs"><a href="/index.php/news"><?php //echo __('News'); ?></a></li> -->
                            <li class="hidden-xs"><a href="/index.php/help/faq"><?php echo __('FAQs'); ?></a></li>
                            <li class="hidden-xs"><a href="/index.php/help/contact"><?php echo __('Contact Us'); ?></a></li>
                            <?php
                            if ($sf_user->isAuthenticated()) {
                                ?>
                                <li class="hidden-xs"><a href="<?php echo url_for('signon/logout') ?>"><?php echo __('Logout') ?></a></li>
                                <?php
                            } else {
                                ?>
                                       <!--  <li class="hidden-xs"><a href="<?php //echo url_for('@sf_guard_register'); ?>"><?php //echo __('Register'); ?></a></li>
                                         <li class="hidden-xs"><a href="<?php //echo url_for('signon/login') ?>"><?php //echo __('Sign In'); ?></a></li> -->
                                <?php
                            }
                            ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Language <i class="fa fa-angle-down"></i> </a> 

<?php
$dbconn = mysql_connect(sfConfig::get('app_mysql_host'), sfConfig::get('app_mysql_user'), sfConfig::get('app_mysql_pass'));
mysql_select_db(sfConfig::get('app_mysql_db'), $dbconn);

$sql = "SELECT * FROM ext_locales ORDER BY local_title ASC";
$rows = mysql_query($sql, $dbconn);

if (mysql_num_rows($rows) > 1) {
    ?>

                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <?php
                                    while ($row = mysql_fetch_assoc($rows)) {
                                        ?>
                                            <li><a href="/index.php/index/setlocale/code/<?php echo $row['locale_identifier']; ?>">
                                            <?php
                                            if ($row['locale_identifier'] == "en_US") {
                                                ?>
                                                        <i><img src="<?php echo public_path(); ?>rwanda_frontend/images/english.jpg" alt=""></i>
                                                        <?php
                                                    } else if ($row['locale_identifier'] == "fr_FR") {
                                                        ?>
                                                        <i><img src="<?php echo public_path(); ?>rwanda_frontend/images/french.jpg" alt=""></i>
                                                        <?php
                                                    } else if ($row['locale_identifier'] == "kin") {
                                                        ?>
                                                        <i><img src="<?php echo public_path(); ?>rwanda_frontend/images/rwanda.jpg" alt=""></i>
                                                        <?php
                                                    }
                                                    ?>
                                                    <?php echo $row['local_title']; ?></a></li>
                                                    <?php
                                                }
                                                ?>
                                    </ul>
                                            <?php
                                        }
                                        ?>

                            </li> </ul>  
                    </div> </div> </div> 
            <div id="stickey"> 
                <div id="stickey"> <div class="navbar-collapse"> 
                        <div class="container"> 
<?php include_component('index', 'irembomenu') ?>
                        </div> 
                    </div> 
                    <div class="container relative"> 
                        <div class="nav-right"> 
                            <ul class="nav navbar-nav"> 
                                <li class="register"> <a href="<?php echo url_for('@sf_guard_register'); ?>"><i class="fa fa-pencil-square-o"></i> <?php echo __('Register'); ?></a> </li> 
                                <li class="dropdown-login"><a href="<?php echo url_for('signon/login') ?>" class="last"><?php echo __('Sign In'); ?></a> 


                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="content-bg">
            <div class="container">
                <div class="text-center"> 
                    <div class="heading"><?php echo __('User Account Recovery') ?> </div> 
                </div>    
            </div>
        </div>

        <!--BANNER ENDS-->
        <!--
        <div class="fix-border border">
           <div class="container">
           <div class="row">
              <div class="col-md-7 no-space">
                    <ul class="nav nav-tabs no-margin border-right">                
                        <li class="active"><a href="/rolportal/web/offlineservice/byname" ><img src="/themes/images/by-name-icon.png" alt=""/>Ukoresheje izina </a></li> <li class=""><a href="/rolportal/web/offlineservice/bytopic"><img src="/themes/images/by-category-icon.png" alt=""/>Ukoresheje ikigamijwe </a></li> <li class=""><a href="/rolportal/web/offlineservice/byagency"><img src="/themes/images/by-ministry-icon.png" alt=""/>Ukoresheje Minisiteri/Ikigo </a></li> </ul> </div> <div class="col-md-5 no-space"> <div class="col-md-12 margin-top-20 margin-bottom-10"> <div class="input-group"> <input class="form-control height" type="text" name="searchKey" id="searchKey" placeholder='Gushakisha serivisi...' onkeydown="if (event.keyCode == 13){ document.getElementById('btnSearch').click()}">
                    <span class="input-group-btn">
                    <button class="btn blue height" type="button" name="btnSearch" id="btnSearch" onclick="searchSubmit()" ><img src="/themes/images/search-icon.png" alt=""/></button> </span> </div> </div> </div> <div class="clearfix"></div> </div> </div> </div> --> <div class="container"> <div class="container"> 
                <div id="content" class="row"> 
                    <div class="col-md-12"> 
                        <div class="columns-1" id="main-content" role="main"> 
                            <div class="portlet-layout row-fluid"> <div class="portlet-column portlet-column-only span12" id="column-1"> 
                                    <div class="portlet-dropzone portlet-column-content portlet-column-content-only" id="layout-column_column-1"> 
                                        <div class="portlet-boundary portlet-boundary_56_ portlet-static portlet-static-end portlet-borderless portlet-journal-content " id="p_p_id_56_INSTANCE_7mAyand1Rbel_" > <span id="p_56_INSTANCE_7mAyand1Rbel"></span> <div class="portlet-borderless-container" style=""> 
                                                <div class="portlet-body">
                                                     <h4 class="text-center"><?php echo __('Forgot your account password') ?> </h4>
                                                    <div class="journal-content-article"> 
                                                        <div class="row faq"> 
                                                            <div class="col-md-12"> 
                                                               <div class="panel panel-default">
                                                                        <div class="panel-body"> 
                                                                           <div class="login-form"> 
                                                                              <?php echo $sf_content ?>
                                                                           </div>  
                                                                        </div>
                                                                    </div>
                                                            </div> 
                                                        </div> 
                                                    </div> 
                                                    <div class="entry-links"> </div> 
                                                </div> 
                                            </div> </div> 
                                    </div> 
                                </div> </div> </div> 
                        <form action="#" id="hrefFm" method="post" name="hrefFm"> <span>
                                    
                            </span> </form> 
                    </div>
                </div> </div> 
            <div class="footer"> <div class="container"> 
                                        <div class="row"> <div class="col-xs-7"> <!--<h2>Gov Connect</h2>--> <ul class="social-footer">
                                                    <li><a href='#'>
                                                            <img src="<?php echo public_path(); ?>irembo/themes/images/facebook-icon.png" alt=""/></a></li> 
                                                            <li><a href='#'><img src="<?php echo public_path(); ?>irembo/themes/images/twiter-icon.png" alt=""/></a></li> 
                                                    <li><a href='#'>
                                                    <img src="<?php echo public_path(); ?>irembo/themes/images/youtube-icon.png" alt=""/></a>
                                                    </li>
                                                                                </ul> </div> 
                                                                            <div class="col-xs-5 text-right"> <a><img src="<?php echo public_path(); ?>irembo/themes/images/entrust.jpg" alt=""/> </div> 
                                                                        </div> </div> </div> 
            <div class="copyright"> <div class="container"> <div class="row"> <div class="col-md-8 col-sm-8"> 
                                                                                    <ul class="footer-links"> <!--<li><a href="/rolportal/web/rol/contact"> </a> </li>--> 
                                                                                        <li><a href='#'> <?php echo __('Privacy Statement') ?></a> </li> 
                                                                                        <li><a href='#'><?php echo __('Terms of Service') ?></a> 
                                                                                        </li> 
                                                                                        <li><a href='#'><?php echo __('Disclaimer') ?></a> </li> <!-- <a href="#">Uko ubona uru rubuga</a>--> </ul> 
                                                                                </div> 
                        <div class="col-md-4 col-sm-4"> 
                            <span class="pull-right"> <?php echo __('Rwanda Housing Authority') ?></span> 
                        </div> 
                    </div> </div> </div> </div> 

        <script type="text/javascript">Liferay.Util.addInputFocus();</script> <script type="text/javascript">Liferay.Portlet.onLoad({canEditTitle:false, columnPos:0, isStatic:"end", namespacedId:"p_p_id_56_INSTANCE_7mAyand1Rbel_", portletId:"56_INSTANCE_7mAyand1Rbel", refreshURL:"\x2frolportal\x2fc\x2fportal\x2frender_portlet\x3fp_l_id\x3d33247\x26p_p_id\x3d56_INSTANCE_7mAyand1Rbel\x26p_p_lifecycle\x3d0\x26p_t_lifecycle\x3d0\x26p_p_state\x3dnormal\x26p_p_mode\x3dview\x26p_p_col_id\x3dcolumn-1\x26p_p_col_pos\x3d0\x26p_p_col_count\x3d1\x26p_p_isolated\x3d1\x26currentURL\x3d\x252Frolportal\x252Fpublic-user-registration"}); AUI().use("aui-base", "liferay-menu", "liferay-notice", "liferay-poller", "liferay-session", function(a){(function(){Liferay.Util.addInputType(); Liferay.Portlet.ready(function(b, c){Liferay.Util.addInputType(c)}); if (a.UA.mobile){Liferay.Util.addInputCancel()}})(); (function(){new Liferay.Menu(); var b = Liferay.Data.notices; for (var c = 1; c < b.length; c++){new Liferay.Notice(b[c])}})(); (function(){Liferay.Session = new Liferay.SessionBase({autoExtend:true, sessionLength:20, redirectOnExpire:false, redirectUrl:"https\x3a\x2f\x2fbpmis\x2egov\x2erw\x2frolportal\x2fweb\x2frol\x2fhome", warningLength:1})})()});</script> <script src="<?php echo public_path(); ?>irembo/rol-theme/js/maineb6c.js?browserId=other&amp;minifierType=js&amp;languageId=rw&amp;b=6210&amp;t=1472157203000" type="text/javascript"></script> <script type="text/javascript"></script> <!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> <script src="<?php echo public_path(); ?>irembo/themes/js/jquery-1.10.2.min.js" type="text/javascript"></script> <script>$ = $.noConflict(true); if (!jQuery){jQuery = $};</script> <!-- Include all compiled plugins (below), or include individual files as needed --> <script src="<?php echo public_path(); ?>irembo/themes/js/bootstrap.min.js"></script> <script type="text/javascript" src="<?php echo public_path(); ?>irembo/themes/js/jquery.sticky.js"></script> <script src="<?php echo public_path(); ?>irembo/themes/js/custom.js"></script> <script>$(window).load(function(){jQuery("#stickey").sticky({topSpacing:0})}); var btnSubmit = $("input[name=submit]", $("#cas").contents()); if (btnSubmit && btnSubmit.length > 0){$("#signIn").removeAttr("disabled")} else{$("#cas").on("load", function(){$("#signIn").removeAttr("disabled")})}$("#signIn").on("click", function(){var a = $("#cas").contents(); $("#fm1", a).attr("target", "_top"); $("input[name=username]", a).val($("#username").val()); $("input[name=password]", a).val($("#password").val()); $("input[name=submit]", a).click()});</script> <script>function openwindow(a){window.open(a, "_target", "width=1200, height=900,directories=no, menubar=no,locationbar=no,resizable=no,scrollbars=yes,status=yes")}function onlineChat(){var c = (document.referrer)?encodeURIComponent(document.referrer.substr(document.referrer.indexOf("://") + 1)):""; var d = (document.location)?encodeURIComponent(window.location.href.substring(window.location.protocol.length)):""; window.open("http://crm.co.ke/rol/live/index.php?r='+" + c + "'&l='" + d + "'", "_target", "width=400, height=400,directories=no, menubar=no,locationbar=no,resizable=no,scrollbars=no,status=yes")};</script> </body> 
    <!-- Mirrored from irembo.gov.rw/rolportal/public-user-registration by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 27 Aug 2016 21:22:05 GMT -->
</html> 
