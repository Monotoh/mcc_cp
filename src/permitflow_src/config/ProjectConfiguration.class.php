<?php

require_once dirname(__FILE__).'/../lib/vendor/symfony/lib/autoload/sfCoreAutoload.class.php';
//OTB patch phpspell load lib
require_once dirname(__FILE__).'/../lib/vendor/phpspellcheck/include.php';

//
sfCoreAutoload::register();

class ProjectConfiguration extends sfProjectConfiguration
{
 
    
  public function setup()
  {
      
    $vendor_dir = sfConfig::get('sf_lib_dir').'/vendor'.PATH_SEPARATOR;
    $composer_autoload_dir = dirname(__FILE__).'/../../vendor' . PATH_SEPARATOR;
    set_include_path($vendor_dir . $composer_autoload_dir . get_include_path());

    #OTB - CoK Migration helpers: enable cokMigrationPlugin
    $this->enablePlugins('sfDoctrinePlugin','sfDoctrineGuardPlugin','mfUserProfilePlugin','mfInvoicePlugin','sfDoctrineApplyPlugin','cokMigrationPlugin');

  	//$this->setWebDir($this->getRootDir().'../../../../../../assets_sf/');
         //$this->setWebDir($this->getRootDir().'/../www');
  }
  /**
     * OTB Fix
     * We use Old Symfony 1.4 search implementations
     * http://symfony.com/legacy/doc/jobeet/1_4/en/17?orm=Doctrine
     * 
     */  
     static protected $zendLoaded = false;
 
    static public function registerZend()
    {
      if (self::$zendLoaded)
      {
        return;
      }

    //  set_include_path(sfConfig::get('sf_lib_dir').'/../../vendor'.PATH_SEPARATOR.get_include_path());
      require_once dirname(__FILE__).'/../lib/vendor/Zend/Loader/Autoloader.php';
     
      Zend_Loader_Autoloader::getInstance();
      self::$zendLoaded = true;
    } 
  public function configureDoctrine(Doctrine_Manager $manager)
  {
    $manager->setAttribute(Doctrine::ATTR_QUOTE_IDENTIFIER, true);
  }
}
