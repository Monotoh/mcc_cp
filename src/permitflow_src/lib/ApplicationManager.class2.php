<?php
/**
 *
 * Submissions class will handle all functions regarding the creation and modifications of applications
 *
 * Created by PhpStorm.
 * User: thomasjuma
 * Date: 11/19/14
 * Time: 12:26 AM
 */

class ApplicationManager {

    public $invoice_manager = null;
    public $permit_manager = null;

    //Constructor for submissions class
    public function ApplicationManager()
    {
        $this->invoice_manager = new InvoiceManager();
        $this->permit_manager = new PermitManager();
    }

    //Create a new application from a form submission
    public function create_application($form_id, $entry_id, $user_id, $save_as_draft)
    {  
        $is_not_draft=true;
        error_log("DEBUG>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>creating application");
        $submission = new FormEntry();
        $submission->setDeclined('0');
        $submission->setUserId($user_id);
        $submission->setFormId($form_id);
        $submission->setEntryId($entry_id);
        
        if($save_as_draft)
        {   $is_not_draft=FALSE;
            error_log("DEBUG>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Save as draft");
            $submission->setApplicationId("Draft-".date("Y-m-d H:i:s"));
            $submission->setApproved("0");
            $submission->setDateOfSubmission(date("Y-m-d H:i:s"));
        }
        else
        {   error_log("DEBUG>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>NOT A DRAFT");

            $submission->setApplicationId($this->generate_application_number($form_id));
            $submission->setApproved($this->get_submission_stage($form_id,$entry_id));
            $submission->setDateOfSubmission(date("Y-m-d H:i:s"));
        }
        if($is_not_draft==true){
            $appref_id = $submission->save();
            error_log("DEBUG>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>SUBMISSION APP ID: ".$submission->getId());
            error_log("DEBUG>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>SUBMISSION APP REF ID: ".$appref_id);
            return $values=array($submission,$appref_id);
        }
        else{
            $submission->save();
            error_log("DEBUG>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>SUBMISSION APP ID: ".$submission->getId());
            $this->update_services($submission->getId());
            return $submission;
        }
    }

    //output invoice to pdf but save as file locally and return link
    public function save_to_pdf_locally($application_id)
    {
        /**$q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.id = ?', $application_id)
            ->limit(1);
        $application = $q->fetchOne();

        //Get html template
        $prefix_folder = dirname(__FILE__)."/vendor/cp_machform/";

        require_once($prefix_folder.'config.php');
        require_once($prefix_folder.'includes/db-core.php');
        require_once($prefix_folder.'includes/helper-functions.php');
        require_once($prefix_folder.'includes/check-session.php');

        require_once($prefix_folder.'includes/entry-functions.php');

        $q = Doctrine_Query::create()
            ->from("ApSettings a");

        $settings = $q->fetchOne();

    //get entry details for particular entry_id
        $suffix = "";
        if(empty($_SERVER['HTTPS'])) {
            $suffix = "http://";
        }
        else
        {
            $suffix = "https://";
        }

        $dbh = mf_connect_db();

        $param['checkbox_image'] = $suffix.$_SERVER['HTTP_HOST'].'/assets_unified/images/59_blue_16.png';
        $entry_details = mf_get_all_entry_details($dbh, $application->getFormId(), $application->getEntryId(), $param, sfContext::getInstance()->getUser()->getCulture());

    //Prepare html template for pdf
        $html = "<link rel='stylesheet' href='".$suffix.$_SERVER['HTTP_HOST']."/assets_unified/pure-min.css'><table class='pure-table'><tbody>";

        $count = 0;

    //Print Out Application Details
        foreach ($entry_details as $data) {
            $count++;

            if ($data['element_type'] == "section") {
                $html .= "<tr>
                    <td colspan='1'>".$data['label']."</td>
                </tr>";
            } elseif ($data['element_type'] == "page_break") {

            } else {
                $opt_value = "";
                if ($data['value']) {
                    $opt_value = nl2br($data['value']);
                } else {
                    $opt_value = "-";
                }

                $css = "";

                if($count % 2 == 0)
                {
                    $css = "pure-table-odd";
                }

                $html .= "<tr style='border=bottom: 2px solid; padding-bottom: 5px;' class='".$css."'>
                    <td width='30%'><b>".$data['label']."</b></td>

                    <td>".$opt_value."</td>
                </tr>";
            }
        }

        $html .= "</tbody></table>";

        require_once(dirname(__FILE__)."/vendor/dompdf/dompdf_config.inc.php");

        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->render();

        $output = $dompdf->output();

        $filename = md5($application->getApplicationId()."-".date("Y-m-d g:i:s")).'.pdf';

        $q = Doctrine_Query::create()
            ->from("ApSettings a");

        $settings = $q->fetchOne();
        if($settings) {
            try {
                if (substr($settings->getUploadDir(), 1) == "/") {
                    $file_to_save = $settings . '/' . $filename;
                    error_log("Debug-p: ".$file_to_save);
                    file_put_contents($file_to_save, $output);
                } else {
                    $file_to_save = $settings->getUploadDir() . '/' . $filename;
                    error_log("Debug-p: ".$file_to_save);
                    file_put_contents($file_to_save, $output);
                }
            }catch(Exception $ex)
            {
                error_log("Debug-p: ".$ex);
            }
        }

        $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

        $sql = "UPDATE form_entry SET pdf_path = '".$filename."' WHERE id = ".$application->getId();
        mysql_query($sql, $db_connection);
         **/
        return $filename;
    }

    public function application_onhold($application_id,$nextstage)
    {
        $submission = $this->get_application_by_id($application_id);
        $submission->setApproved($nextstage);
        $submission->save();

       // $this->update_services($submission->getId());

        return $submission;
    }



    //output invoice to pdf but save as file locally and return link
    public function save_archive_to_pdf_locally($application_id)
    {
        $q = Doctrine_Query::create()
            ->from('FormEntryArchive a')
            ->where('a.id = ?', $application_id)
            ->limit(1);
        $application = $q->fetchOne();

        //Get html template
        $prefix_folder = dirname(__FILE__)."/vendor/cp_machform/";

        require_once($prefix_folder.'config.php');
        require_once($prefix_folder.'includes/db-core.php');
        require_once($prefix_folder.'includes/helper-functions.php');
        require_once($prefix_folder.'includes/check-session.php');

        require_once($prefix_folder.'includes/entry-functions.php');

        $q = Doctrine_Query::create()
            ->from("ApSettings a");

        $settings = $q->fetchOne();

    //get entry details for particular entry_id
        $suffix = "";
        if(empty($_SERVER['HTTPS'])) {
            $suffix = "http://";
        }
        else
        {
            $suffix = "https://";
        }

        $dbh = mf_connect_db();

        $param['checkbox_image'] = $suffix.$_SERVER['HTTP_HOST'].'/assets_unified/images/59_blue_16.png';
        $entry_details = mf_get_all_entry_details($dbh, $application->getFormId(), $application->getEntryId(), $param, sfContext::getInstance()->getUser()->getCulture());

    //Prepare html template for pdf
        $html = "<link rel='stylesheet' href='".$suffix.$_SERVER['HTTP_HOST']."/assets_unified/pure-min.css'><table class='pure-table'><tbody>";

        $count = 0;

    //Print Out Application Details
        foreach ($entry_details as $data) {
            $count++;

            if ($data['element_type'] == "section") {
                $html .= "<tr>
                    <td colspan='1'>".$data['label']."</td>
                </tr>";
            } elseif ($data['element_type'] == "page_break") {

            } else {
                $opt_value = "";
                if ($data['value']) {
                    $opt_value = nl2br($data['value']);
                } else {
                    $opt_value = "-";
                }

                $css = "";

                if($count % 2 == 0)
                {
                    $css = "pure-table-odd";
                }

                $html .= "<tr style='border=bottom: 2px solid; padding-bottom: 5px;' class='".$css."'>
                    <td width='30%'><b>".$data['label']."</b></td>

                    <td>".$opt_value."</td>
                </tr>";
            }
        }

        $html .= "</tbody></table>";

        require_once(dirname(__FILE__)."/vendor/dompdf/dompdf_config.inc.php");

        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->render();

        $output = $dompdf->output();

        $filename = md5($application->getApplicationId()."-".date("Y-m-d H:i:s")).'.pdf';

        $q = Doctrine_Query::create()
            ->from("ApSettings a");

        $settings = $q->fetchOne();
        if($settings) {
            try {
                if (substr($settings->getUploadDir(), 1) == "/") {
                    $file_to_save = $settings . '/' . $filename;
                    error_log("Debug-p: ".$file_to_save);
                    file_put_contents($file_to_save, $output);
                } else {
                    $file_to_save = $settings->getUploadDir() . '/' . $filename;
                    error_log("Debug-p: ".$file_to_save);
                    file_put_contents($file_to_save, $output);
                }
            }catch(Exception $ex)
            {
                error_log("Debug-p: ".$ex);
            }
        }

        $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

        $sql = "UPDATE form_entry SET pdf_path = '".$filename."' WHERE id = ".$application->getId();
        mysql_query($sql, $dbconn);

        return $filename;
    }

    //Push an application from drafts to a live workflow
    public function publish_draft($application_id)
    {
        $submission = $this->get_application_by_id($application_id);
        $submission->setApplicationId($this->generate_application_number($submission->getFormId()));
        $submission->setApproved($this->get_submission_stage($submission->getFormId(),$submission->getEntryId()));
        $submission->setDateOfSubmission(date("Y-m-d H:i:s"));
        $submission->save();

        return $submission;
    }

    //Push an application from drafts to a live workflow
    public function resubmit_application($application_id)
    {
        $submission = $this->get_application_by_id($application_id);
       // $submission->setApplicationId($this->generate_application_number($submission->getFormId()));
        $submission->setApproved($this->get_resubmission_stage($submission->getFormId(),$submission->getEntryId()));
        $submission->setDeclined("0");
        //OTB patch - This is a bug - Systemm replaces date of submission with every resubmission
        $submission->setDateOfSubmission(date("Y-m-d H:i:s"));
        $submission->save();

        if($submission->getDeclined() == "1" && $submission->getParentSubmission() == "0")
        {
            $new_id = $this->DuplicateMySQLRecord ("ap_form_".$submission->getFormId(), "id", $submission->getEntryId());

            $new_entry = new FormEntry();
            $new_entry->setFormId($submission->getFormId());
            $new_entry->setEntryId($submission->getEntryId());
            $new_entry->setApproved($submission->getApproved());
            $new_entry->setApplicationId($submission->getApplicationId());
            $new_entry->setUserId($submission->getUserId());
            $new_entry->setParentSubmission($submission->getId());
            $new_entry->setDeclined("1");
            $new_entry->setDateOfSubmission($submission->getDateOfSubmission());
            $new_entry->setDateOfResponse($submission->getDateOfResponse());
            $new_entry->setDateOfIssue($submission->getDateOfIssue());
            $new_entry->setObservation($submission->getObservation());
            $new_entry->save();

            $submission->setEntryId($new_id);

            $submission->setPreviousSubmission($new_entry->getId());
            $submission->save();
        }

        $this->update_services($submission->getId());

        return $submission;
    }

    //Link a new form submission to an existing application
    public function create_linked_application($main_application, $form_id, $entry_id, $user_id)
    {
        if($this->linked_application_exists($main_application,$form_id,$entry_id))
        {
            return $this->get_linked_application($main_application, $form_id, $entry_id);
        }
        else {
            $form_link = new FormEntryLinks();
            $form_link->setFormentryid($main_application);
            $form_link->setFormId($form_id);
            $form_link->setEntryId($entry_id);
            $form_link->setUserId($user_id);
            $form_link->setDateOfSubmission(date("Y-m-d H:i:s"));
            $form_link->save();
			$this->get_application_by_id($main_application)->setApproved($this->get_submission_stage($form_id,$entry_id))->save();//OTB Patch - Get The main application then set the main application stage as per worklflow condition logic
        }

        return $form_link;
    }

    //Create an edit application form from an application with a missing record
    public function create_adhoc_edit_by_stage($application_id, $stage_id)
    {
       $edit_markup = "";

       $default_form = $this->get_default_form_by_stage($stage_id);
       $submission = $this->get_application_by_id($application_id);

       $_SESSION['edit_entry']['entry_id'] = null;

       $_SESSION['redirect'] = false;
       $_SESSION['redirect_url'] = "";

       $_SESSION['draft_edit'] = false;

       $prefix_folder = dirname(__FILE__) . "/vendor/cp_machform/";



       header("p3p: CP=\"IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT\"");

       require_once($prefix_folder . 'config.php');
       require_once($prefix_folder . 'includes/language.php');
       require_once($prefix_folder . 'includes/db-core.php');
       require_once($prefix_folder . 'includes/common-validator.php');
       require_once($prefix_folder . 'includes/view-functions.php');
       require_once($prefix_folder . 'includes/post-functions.php');
       require_once($prefix_folder . 'includes/filter-functions.php');
       require_once($prefix_folder . 'includes/entry-functions.php');
       require_once($prefix_folder . 'includes/helper-functions.php');
       require_once($prefix_folder . 'includes/theme-functions.php');
       require_once($prefix_folder . 'lib/recaptchalib.php');
       require_once($prefix_folder . 'lib/php-captcha/php-captcha.inc.php');
       require_once($prefix_folder . 'lib/text-captcha.php');
       require_once($prefix_folder . 'hooks/custom_hooks.php');

       $dbh = mf_connect_db();
       $ssl_suffix = mf_get_ssl_suffix();

       $q = Doctrine_Query::create()
            ->from("SfGuardUserProfile a")
            ->where("a.user_id = ?", $submission->getUserId());
       $profile = $q->fetchOne();

       $q = Doctrine_Query::create()
            ->from("SfGuardUser a")
            ->where("a.id = ?", $submission->getUserId());
       $user = $q->fetchOne();

       $_SESSION["user_email"] = $profile->getEmail();

       $_SESSION['applicant_name'] = $profile->getFullname();
       $_SESSION['applicant_id'] = $user->getUsername();

       $edit_markup = mf_display_form($dbh, $default_form, $form_params, "en_US");

       return $edit_markup;
    }

    //Create an edit application form from an application with a missing record
    public function create_adhoc_edit_by_form($application_id, $form_id)
    {
       $edit_markup = "";

       $default_form = $form_id;
       $submission = $this->get_application_by_id($application_id);

       $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
       mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

       $sql = "INSERT INTO ap_form_".$form_id." (date_created,date_updated,ip_address) VALUES('".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."','".$_SERVER['REMOTE_ADDR']."')";
       $result = mysql_query($sql, $dbconn);
       $record_id = mysql_insert_id();

       $submission->setFormId($form_id);
       $submission->setEntryId($record_id);
       $submission->save();

       if($_SESSION["SESSION_CUTEFLOW_USERID"] )
       {
         header("Location: /backend.php/applications/edit?form_id=".$form_id."&id=".$record_id);
       }
       else
       {
         header("Location: /index.php/application/edit?application_id=".$submission->getId());
       }
       exit;
    }

    //Create an edit application form from an application with a missing record
    public function create_adhoc_chooser($application_id)
    {
       $edit_markup = '<div class="alert alert-error fade in nomargin">
             <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
             <h4>Submission Recovery Tool</h4>
             <p>Select an application form to use to recover this submission:</p>
             <p><ol>';

       $q = Doctrine_Query::create()
           ->from('ApForms a')
           ->where('a.form_type = 1')
           ->andWhere('a.form_active = 1')
           ->orderBy('a.form_name ASC');
       $forms = $q->execute();
       foreach($forms as $form)
       {
          if($_SESSION["SESSION_CUTEFLOW_USERID"] )
          {
            $edit_markup .= "<li><a href='/backend.php/applications/view/id/".$application_id."/formid/".$form->getFormId()."'>".$form->getFormName()."</a></li>";
          }
          else {
            $edit_markup .= "<li><a href='/index.php/application/view/id/".$application_id."/formid/".$form->getFormId()."'>".$form->getFormName()."</a></li>";
          }
       }

       $edit_markup .= "</ol></p></div>";

       return $edit_markup;
    }

    //Check if an application already exists for a given form entry
    public function application_exists($form_id,$entry_id)
    {
        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.entry_id = ?', $entry_id)
            ->andWhere('a.form_id = ?', $form_id)
            ->orderBy("a.application_id DESC")
            ->limit(1);
        $existing_app = $q->fetchOne();
        if($existing_app)//Already submitted then tell client its already submitted
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //Check if a review entry already exists for a given form entry
    public function review_entry_exists($form_id,$entry_id)
    {
        $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

        $sql = "SELECT * FROM ap_form_".$form_id."_review WHERE id = ".$entry_id;
        $result = mysql_query($sql, $db_connection);

        if(mysql_num_rows($result))//Already submitted then tell client its already submitted
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //Check if a new entry already exists for a given form entry
    public function new_entry_exists($form_id,$entry_id)
    {
        $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

        $sql = "SELECT * FROM ap_form_".$form_id." WHERE id = ".$entry_id;
        $result = mysql_query($sql, $db_connection);

        if(mysql_num_rows($result))//Already submitted then tell client its already submitted
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //Check if the form submission is already linked to an application
    public function linked_application_exists($main_application,$form_id,$entry_id)
    {
        $q = Doctrine_Query::create()
            ->from('FormEntryLinks a')
            ->where('a.entry_id = ?', $entry_id)
            ->andWhere('a.form_id = ?', $form_id)
            ->andWhere('a.formentryid = ?', $main_application)
            ->limit(1);
        $existing_link = $q->fetchOne();
        if($existing_link)//Already submitted then tell client its already submitted
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //Returns an existing application
    public function get_application($form_id,$entry_id)
    {
        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.entry_id = ?', $entry_id)
            ->andWhere('a.form_id = ?', $form_id)
            ->orderBy("a.application_id DESC")
            ->limit(1);
        $existing_app = $q->fetchOne();
        if($existing_app)//Already submitted then tell client its already submitted
        {
            return $existing_app;
        }
        else
        {
            return false;
        }
    }

    //Returns an existing linked application
    public function get_linked_application($main_application,$form_id,$entry_id)
    {
        $q = Doctrine_Query::create()
            ->from('FormEntryLinks a')
            ->where('a.entry_id = ?', $entry_id)
            ->andWhere('a.form_id = ?', $form_id)
            ->andWhere('a.formentryid = ?', $main_application)
            ->limit(1);
        $existing_app = $q->fetchOne();
        if($existing_app)//Already submitted then tell client its already submitted
        {
            return $existing_app;
        }
        else
        {
            return false;
        }
    }

    //Returns an existing application
    public function get_application_by_id($application_id)
    {
        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.id = ?', $application_id)
            ->orderBy("a.application_id DESC")
            ->limit(1);
        $existing_app = $q->fetchOne();
        if($existing_app)//Already submitted then tell client its already submitted
        {
            return $existing_app;
        }
        else
        {
            return false;
        }
    }

    //Check if an application is a draft
    public function is_draft($form_id,$entry_id)
    {
        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.entry_id = ?', $entry_id)
            ->andWhere('a.form_id = ?', $form_id)
            ->andWhere("a.approved = 0")
            ->orderBy("a.application_id DESC")
            ->limit(1);
        $existing_app = $q->fetchOne();
        if($existing_app)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //Check if an application is a resubmission
    public function is_resubmission($form_id,$entry_id)
    {
        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.entry_id = ?', $entry_id)
            ->andWhere('a.form_id = ?', $form_id)
            ->andWhere("a.approved = 0 AND a.parent_submission <> 0")
            ->orderBy("a.application_id DESC")
            ->limit(1);
        $existing_app = $q->fetchOne();
        if($existing_app)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //Check if the form already has an existing draft application
    public function has_draft($form_id,$user_id)
    {
        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.user_id = ?', $user_id)
            ->andWhere('a.form_id = ?', $form_id)
            ->andWhere("a.approved = 0")
            ->orderBy("a.application_id DESC")
            ->limit(1);
        $existing_app = $q->fetchOne();
        if($existing_app)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    /*
         * OTB patch
         * The database connections present in function generate_application_number is depreacated i.e. no longer supported !
         * we will comment out unsupported features and implement new ways of mysql connections
         */
    //Generate a new application number for a new application
    public function generate_application_number($form_id)
    {
      //  $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
      //  mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);
       
        $db_connection = mysqli_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysqli_select_db($db_connection, sfConfig::get('app_mysql_db'));
        //Check if application_numbering table exists (Preferred)
        // Select 1 from ap_number_generator will return false if the table does not exist.
        $val = mysqli_query($db_connection, 'select 1 from `ap_number_generator`');

        if($val !== FALSE)
        {
          //READ AND UPDATE ap_number_generator
          $sql = "SELECT * FROM ap_number_generator WHERE form_id = " . $form_id;
         // $form_result = mysql_query($sql, $db_connection);
          $form_result = mysqli_query($db_connection, $sql );

          if(mysqli_num_rows($form_result) > 0)
          {
            //$number_row = mysql_fetch_assoc($form_result);
            $number_row = mysqli_fetch_assoc($form_result);  
            $application_number = $number_row["application_number"];

            //LOCK ap_number_generator
            $sql = "LOCK TABLES ap_number_generator WRITE";
           // $lock_result = mysql_query($sql, $db_connection);
            $lock_result = mysqli_query($db_connection,$sql);

            //Update the value
            $application_number++;

			//OTB Patch Start - Update the application number in ap_number_generator for all forms that share the same numbering sequence stored in ap_column_preferences. This helps for Kigali City where the 3 districts (gasabo,nyarugenge and kicukiro) have been sharing the same numbering sequence for applications
			$q = Doctrine_Query::create()
				->from("ApColumnPreferences a")
				->where("a.form_id = ?", $form_id);
			$form_preference = $q->fetchOne();
			if($form_preference){
				$q = Doctrine_Query::create()
					->select('a.form_id')
					->from("ApColumnPreferences a")
					->where("a.element_name = ?", $form_preference->getElementName());
				$other_form_ids = $q->execute(array(), Doctrine_Core::HYDRATE_SINGLE_SCALAR);
				$in_query = " or form_id in (".implode(',',$other_form_ids).")";
			}
			//OTB Patch End - Update the application number in ap_number_generator for all forms that share the same numbering sequence stored in ap_column_preferences. This helps for Kigali City where the 3 districts (gasabo,nyarugenge and kicukiro) have been sharing the same numbering sequence for applications

            //$sql = "UPDATE ap_number_generator SET application_number = '".$application_number."' WHERE form_id = ".$form_id;
            $sql = "UPDATE ap_number_generator SET application_number = '".$application_number."' WHERE form_id = ".$form_id." ".$in_query;//OTB Patch - Update the application number in ap_number_generator for all forms that share the same numbering sequence stored in ap_column_preferences. This helps for Kigali City where the 3 districts (gasabo,nyarugenge and kicukiro) have been sharing the same numbering sequence for applications
           // mysql_query($sql, $db_connection) or die(mysql_error());
            mysqli_query($db_connection,$sql) or die(mysqli_error());

            //UNLOCK ap_number_generator
            $sql = "UNLOCK TABLES";
           // $lock_result = mysql_query($sql, $db_connection);
            $lock_result = mysqli_query($db_connection,$sql);
            return $application_number;
          }
          else
          {
            $application_number = 0;

            $sql = "SELECT * FROM ap_forms WHERE form_id = " . $form_id;
            //$form_result = mysql_query($sql, $db_connection);
            $form_result = mysqli_query($db_connection,$sql);

            //$form_settings = mysql_fetch_assoc($form_result);
            $form_settings = mysqli_fetch_assoc($form_result);
            //LOCK ap_number_generator
            $sql = "LOCK TABLES ap_number_generator WRITE";
          // $lock_result = mysql_query($sql, $db_connection);
            $lock_result = mysqli_query($db_connection,$sql);

            $q = Doctrine_Query::create()
                ->from('FormEntry a')
                ->where('a.approved <> 0 AND a.declined <> 1 AND a.parent_submission = 0')
                ->andWhere('a.form_id = ?', $form_id)
                ->orderBy("a.application_id DESC")
                ->limit(1);
            $last_app = $q->fetchOne();

            if ($last_app) {
                $new_app_id = $last_app->getApplicationId();
                $new_app_id = ++$new_app_id;
                $application_number = $new_app_id;
            } else 
                {
                $application_number = $form_settings['form_idn'];
            }


            //mysql_query("INSERT INTO ap_number_generator VALUES(".$form_id.",'".$application_number."')", $db_connection) or die(mysql_error());
            mysqli_query($db_connection,"INSERT INTO ap_number_generator VALUES(".$form_id.",'".$application_number."')") or die(mysqli_error());


            //UNLOCK ap_number_generator
            $sql = "UNLOCK TABLES";
           // $lock_result = mysql_query($sql, $db_connection);
            $lock_result = mysqli_query($db_connection,$sql);

            return $application_number;
          }
        }
        else//Use the settings from ap_forms (Fallback)
        {
            $sql = "SELECT * FROM ap_forms WHERE form_id = " . $form_id;
           // $form_result = mysql_query($sql, $db_connection);
            $form_result = mysqli_query($db_connection, $sql);
           // $form_settings = mysql_fetch_assoc($form_result);
            $form_settings = mysqli_fetch_assoc($form_result);

            $q = Doctrine_Query::create()
                ->from('FormEntry a')
                ->where('a.approved <> 0 AND a.declined <> 1 AND a.parent_submission = 0')
                ->andWhere('a.form_id = ?', $form_id)
                ->orderBy("a.application_id DESC")
                ->limit(1);
            $last_app = $q->fetchOne();

            if ($last_app) {
                $new_app_id = $last_app->getApplicationId();
                $new_app_id = ++$new_app_id;
                return $new_app_id;
            } else {
                return $form_settings['form_idn'];
            }
        }
    }

    //Get the stage of submission for a form entry (Depends on whether logic for workflow is set or not)
    public function get_submission_stage($form_id, $entry_id)
    {
        //Use the settings from ap_forms
        $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

        $sql = "SELECT * FROM ap_forms WHERE form_id = ".$form_id;
        $form_result = mysql_query($sql, $db_connection);

        $form_settings = mysql_fetch_assoc($form_result);

        //Check workflow override logic
        if($form_settings['logic_workflow_enable'])
        {
            //Logic field
            $sql = "SELECT * FROM ap_workflow_logic_elements WHERE form_id = ".$form_id;
            $element_result = mysql_query($sql, $db_connection);

            $element_values = mysql_fetch_assoc($element_result);

            $element_id = $element_values['element_id'];

            //Get element value
            $sql = "SELECT * FROM ap_form_".$form_id." WHERE id = ".$entry_id;
            $entry_result = mysql_query($sql, $db_connection);

            $entry_values = mysql_fetch_assoc($entry_result);

            $option_id = $entry_values['element_'.$element_id];

            $sql = "SELECT * FROM ap_element_options WHERE form_id = ".$form_id." AND element_id = ".$element_id." AND option_id = ".$option_id;
            $option_result = mysql_query($sql, $db_connection);

            $option_values = mysql_fetch_assoc($option_result);

            $option_value = $option_values['option'];

            $sql = "SELECT * FROM ap_workflow_logic_conditions WHERE form_id = ".$form_id." AND target_element_id = ".$element_id." AND rule_keyword = '".$option_value."'";
            $stage_result = mysql_query($sql, $db_connection);

            $stage_values = mysql_fetch_assoc($stage_result);

            if($stage_values)
            {
                $stage_value = $stage_values['element_name'];

                return $stage_value;
            }
            else
            {
                return $form_settings['form_stage'];
            }
        }
        else
        {
            return $form_settings['form_stage'];
        }
    }

    //Get the stage of submission for a form entry (Depends on whether logic for workflow is set or not)
    public function get_resubmission_stage($form_id, $entry_id)
    {
        $submission = $this->get_application($form_id,$entry_id);

        $q = Doctrine_Query::create()
            ->from('SubMenus a')
            ->where('a.id = ?', $submission->getApproved())
            ->limit(1);
        $current_stage = $q->fetchOne();

        if($current_stage->getStageType() == 5 && $current_stage->getStageProperty() == 2)
        {
            return $current_stage->getStageTypeMovement();
        }
        elseif($current_stage->getStageType() == 5 && $current_stage->getStageProperty() == 3)
        {
            //Send notification to reviewers
            $notification = $current_stage->getStageTypeNotification();

            $q = Doctrine_Query::create()
                ->from('CfUser a')
                ->where('a.bdeleted = 0');
            $reviewers = $q->execute();
            foreach($reviewers as $reviewer)
            {
                $q = Doctrine_Query::create()
                    ->from('mfGuardUserGroup a')
                    ->leftJoin('a.Group b')
                    ->leftJoin('b.mfGuardGroupPermission c') //Left Join group permissions
                    ->leftJoin('c.Permission d') //Left Join permissions
                    ->where('a.user_id = ?', $reviewer->getNid())
                    ->andWhere('d.name = ?', "accesssubmenu".$submission->getApproved());
                $usergroups = $q->execute();
                if(sizeof($usergroups) > 0)
                {
                    $body = "
                            Hi ".$reviewer->getStrfirstname()." ".$reviewer->getStrlastname().",<br>
                            <br>
                            ".$notification."

                            <br>
                            Click here to view the application: <br>
                            ------- <br>
                            <a href='http://".$_SERVER['HTTP_HOST']."/backend.php/applications/view/id/".$submission->getId()."'>Link to ".$submission->getApplicationId()."</a><br>
                            ------- <br>

                            <br>
                            ";

                    $mailnotifications = new mailnotifications();
                    $mailnotifications->sendemail(sfConfig::get('app_organisation_email'), $reviewer->getStremail(),"Corrected Application",$body);
                }
            }
        }
        else {
            //Use the settings from ap_forms
            $db_connection = mysql_connect(sfConfig::get('app_mysql_host'), sfConfig::get('app_mysql_user'), sfConfig::get('app_mysql_pass'));
            mysql_select_db(sfConfig::get('app_mysql_db'), $db_connection);

            $sql = "SELECT * FROM ap_forms WHERE form_id = " . $form_id;
            $form_result = mysql_query($sql, $db_connection);

            $form_settings = mysql_fetch_assoc($form_result);

            //Check workflow override logic
            if ($form_settings['logic_workflow_enable']) {
                //Logic field
                $sql = "SELECT * FROM ap_workflow_logic_elements WHERE form_id = " . $form_id;
                $element_result = mysql_query($sql, $db_connection);

                $element_values = mysql_fetch_assoc($element_result);

                $element_id = $element_values['element_id'];

                //Get element value
                $sql = "SELECT * FROM ap_form_" . $form_id . " WHERE id = " . $entry_id;
                $entry_result = mysql_query($sql, $db_connection);

                $entry_values = mysql_fetch_assoc($entry_result);

                $option_id = $entry_values['element_' . $element_id];

                $sql = "SELECT * FROM ap_element_options WHERE form_id = " . $form_id . " AND element_id = " . $element_id . " AND option_id = " . $option_id;
                $option_result = mysql_query($sql, $db_connection);

                $option_values = mysql_fetch_assoc($option_result);

                $option_value = $option_values['option'];

                $sql = "SELECT * FROM ap_workflow_logic_conditions WHERE form_id = " . $form_id . " AND target_element_id = " . $element_id . " AND rule_keyword = '" . $option_value . "'";
                $stage_result = mysql_query($sql, $db_connection);

                $stage_values = mysql_fetch_assoc($stage_result);

                if ($stage_values) {
                    $stage_value = $stage_values['element_name'];

                    return $stage_value;
                }
                else
                {
                    return $form_settings['form_stage'];
                }
            } else {
                return $form_settings['form_stage'];
            }
        }
    }

    //Check if an application requires the generation of permits
    public function update_services($application_id)
    {
        error_log("DEBUG::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::NEW APPLICATION'S ID: ".$application_id);
      //Only auto generate permit if there is permit template configured for the current stage and there is nothing owed
      if($this->permit_manager->needs_permit_for_current_stage($application_id))
      {
        error_log("DEBUG::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::NEEDS PERMIT: ".$application_id);
         $this->permit_manager->create_permit($application_id);
      }

      //If application form has just been paid for from the frontend then redirect to the generated permit
      // This improves userability as the client doesn't need to search for where the permit link is
      if($_SESSION['just_submitted'] && empty($_SESSION['SESSION_CUTEFLOW_USERID']))
      {error_log("DEBUG::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::PAID FOR");
        //Redirect to permits page if available (more user friendly that way)
        if ($this->permit_manager->has_permit($application_id)) {
            //redirect to permit page
            $q = Doctrine_Query::create()
                ->from("SavedPermit a")
                ->leftJoin("a.FormEntry b")
                ->where("b.id = ?", $application_id)
                ->andWhere("a.permit_status <> 3")
                ->limit(1);
            $permit = $q->fetchOne();

            if ($permit) {
                echo "<script language='javascript'>window.parent.location.href = '/index.php/permits/view/id/" . $permit->getId()."/done/1';</script>";
                exit;
            }
        }

        //Redirect to applications page if permit is not available
        echo "<script language='javascript'>window.parent.location.href = '/index.php/application/view/id/" . $application_id."/done/1';</script>";
        exit;
      }

    }

    //Check if application requires generation of invoices
    public function update_invoices($application_id)
    {
        //Check if there is already an invoice generated for this application
        if($this->invoice_manager->has_unpaid_invoice($application_id))
        {
            //Try changing the invoice status to pending since the user is trying to
            // add a payment for an application
            $invoice = $this->invoice_manager->get_unpaid_invoice($application_id);
            if($invoice) {
                $invoice->setPaid(1);
                $invoice->save();
            }
        }
        else
        {
            if(!$this->invoice_manager->has_paid_invoice($application_id)) {
                //If no invoice then generate an invoice for this application since this function is
                // only called when there is a payment on submission
                $invoice = $this->invoice_manager->create_invoice_from_submission($application_id);
            }
        }
    }

    public function DuplicateMySQLRecord ($table, $id_field, $id) {
        $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

        // load the original record into an array
        $result = mysql_query("SELECT * FROM {$table} WHERE {$id_field}={$id}");
        $original_record = mysql_fetch_assoc($result);

        // insert the new record and get the new auto_increment id
        mysql_query("INSERT INTO {$table} (`{$id_field}`) VALUES (NULL)",$db_connection);
        $new_id = mysql_insert_id();

        // generate the query to update the new record with the previous values
        $query = "UPDATE {$table} SET ";
        foreach ($original_record as $key => $value) {
            if ($key != $id_field) {
                $query .= '`'.$key.'` = "'.str_replace('"','\"',$value).'", ';
            }
        }
        $query = substr($query,0,strlen($query)-2); // lop off the extra trailing comma
        $query .= " WHERE {$id_field}={$new_id}";
        mysql_query($query,$db_connection) or die(mysql_error());

        // return the new id
        return $new_id;
    }

    //Check if logic permissions is set on a form
    public function has_logic_permission($form_id)
    {
        //Use the settings from ap_forms
        $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

        $sql = "SELECT * FROM ap_forms WHERE form_id = ".$form_id;
        $form_result = mysql_query($sql, $db_connection);

        $form_settings = mysql_fetch_assoc($form_result);

        //Check workflow override logic
        if($form_settings['logic_permission_enable'])
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //Check if logic permissions is set on any form
    public function any_logic_permissions($form_id)
    {
        //Use the settings from ap_forms
        $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

        $sql = "SELECT * FROM ap_forms WHERE form_active = 1 AND logic_permission_enable = 1 AND form_id = ".$form_id;
        $form_result = mysql_query($sql, $db_connection);

        //Check workflow override logic
        while($form_settings = mysql_fetch_assoc($form_result))
        {
            if($form_settings['logic_permission_enable'])
            {
                return true;
            }
        }

        return false;
    }

    //Check if logic permissions is set on any form in a workflow
    public function any_logic_permissions_for_stage($stage_id)
    {
        //Use the settings from ap_forms
        $db_connection = mysql_connect(sfConfig::get('app_mysql_host'), sfConfig::get('app_mysql_user'), sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'), $db_connection);

        $q = Doctrine_Query::create()
            ->from('SubMenus a')
            ->where("a.id = ?", $stage_id)
            ->limit(1);
        $submenu = $q->fetchOne();

        if($submenu) {
            $q = Doctrine_Query::create()
                ->from('SubMenus a')
                ->where("a.menu_id = ?", $submenu->getMenuId());
            $stages = $q->execute();
        }

        foreach($stages as $stage) {

            //Check permission override logic
            $sql = "SELECT * FROM ap_forms WHERE form_active = 1 AND logic_permission_enable = 1";
            $form_result = mysql_query($sql, $db_connection);

            while ($form_settings = mysql_fetch_assoc($form_result)) {

                $form_id = $form_settings['form_id'];

                //Check workflow override logic
                if ($form_settings['logic_workflow_enable']) {

                    $sql = "SELECT * FROM ap_workflow_logic_conditions WHERE form_id = " . $form_id . " AND element_name = " . $stage->getId();
                    $stage_result = mysql_query($sql, $db_connection);

                    $stage_values = mysql_fetch_assoc($stage_result);

                    if ($stage_values) {
                        return true;
                    }
                } elseif($form_settings['form_stage'] == $stage->getId()) {
                    return true;
                }

            }

        }

        return false;
    }

    //Check if logic permissions is set on any form
    public function get_logiced_forms()
    {
        $forms = array();

        //Use the settings from ap_forms
        $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

        $sql = "SELECT * FROM ap_forms WHERE form_active = 1 AND form_type = 1";
        $form_result = mysql_query($sql, $db_connection);

        //Check workflow override logic
        while($form_settings = mysql_fetch_assoc($form_result))
        {
            $forms[] = $form_settings['form_id'];
        }

        return $forms;
    }

    //Check if logic permissions is set on any form
    public function get_default_form()
    {
        //Use the settings from ap_forms
        $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

        $sql = "SELECT * FROM ap_forms WHERE form_active = 1 AND form_type = 1 ORDER BY form_name";
        $form_result = mysql_query($sql, $db_connection);

        //Check workflow override logic
        while($form_settings = mysql_fetch_assoc($form_result))
        {
            return $form_settings['form_id'];
        }
    }

    //Check if logic permissions is set on any form
    public function get_default_form_by_stage($stage_id)
    {
        //Use the settings from ap_forms
        $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

        $q = Doctrine_Query::create()
            ->from('SubMenus a')
            ->where("a.id = ?", $stage_id)
            ->limit(1);
        $submenu = $q->fetchOne();

        $q = Doctrine_Query::create()
            ->from('SubMenus a')
            ->where("a.menu_id = ?", $submenu->getMenuId());
        $stages = $q->execute();

        foreach($stages as $stage) {

            $sql = "SELECT * FROM ap_forms WHERE form_active = 1 AND form_type = 1 AND form_stage = ".$stage->getId()." ORDER BY form_name";
            $form_result = mysql_query($sql, $db_connection);

            //Check workflow override logic
            while ($form_settings = mysql_fetch_assoc($form_result)) {
                return $form_settings['form_id'];
            }
        }
    }

    //Get number of applications that only the logged in user group is allowed to see
    public function get_logiced_applications_count($form_id, $stage,$user)
    {
        $filters = array();

        $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

        $q = null;

        if($user->mfHasCredential('accesssubmenu'.$stage))
        {
            $submenus = "a.approved = ".$stage." ";
        }
        else
        {
            $submenus = "a.approved = -1 "; //Coz they don't have permissions to see applications in this stage
        }

        $filters['application_form'] = $form_id;

        //Get other filters from logic settings
        $sql = "SELECT * FROM ap_forms WHERE form_id = ".$form_id;
        $form_result = mysql_query($sql, $dbconn);

        $form_settings = mysql_fetch_assoc($form_result);

        //Check which fields have logic permissions enabled
          //This query lists all fields that have any logic permisions.
        $sql = "SELECT * FROM ap_permission_logic_elements WHERE form_id = " . $form_id;
        $element_result = mysql_query($sql, $dbconn);


        $entries = "";
        $total_apps = 0;

        //Dropdown's can fetch their data from either Default Choices or Table Queries

        while($element_values = mysql_fetch_assoc($element_result))
        {
          $element_id = $element_values['element_id'];

          $q = Doctrine_Query::create()
             ->from("ApFormElements a")
             ->where("a.element_id = ?", $element_id)
             ->andWhere("a.form_id = ?", $form_id);
          $element = $q->fetchOne();

          if($element & $element->getElementTableName() == "")
          {
            //1. Default Choices
            $groups = "";

            $q = Doctrine_Query::create()
               ->from("MfGuardUserGroup a")
               ->where("a.user_id = ?", $user->getAttribute('userid'));
            $user_groups = $q->execute();

            $count = 0;
            foreach($user_groups as $user_group)
            {
              if($count == 0)
              {
                $groups .= "d.element_name = {$user_group->group_id}";
              }
              else
              {
                $groups .= " OR d.element_name = {$user_group->group_id}";
              }

              $count++;
            }

            //For each field with logic conditions, retrieve all form_entries that satisfy the conditions
            $sql = "SELECT a.id as id FROM form_entry a
                        LEFT JOIN ap_form_{$form_id} b
                          ON a.entry_id = b.id
                        LEFT JOIN ap_element_options c
                          ON b.element_{$element_id} = c.option_id
                        LEFT JOIN ap_permission_logic_conditions d
                          ON c.element_id = d.target_element_id AND c.option = d.rule_keyword
                        WHERE
                          a.form_id = {$form_id}
                          AND c.form_id = {$form_id}
                          AND c.element_id = {$element_id}
                          AND a.approved = {$stage}
                          AND ({$groups})
                          GROUP BY a.id";
            $total_apps = $total_apps + mysql_num_rows(mysql_query($sql, $dbconn));
          }
          else{
            //2. Table Query
            $groups = "";

            $q = Doctrine_Query::create()
               ->from("MfGuardUserGroup a")
               ->where("a.user_id = ?", $user->getAttribute('userid'));
            $user_groups = $q->execute();

            $count = 0;
            foreach($user_groups as $user_group)
            {
              if($count == 0)
              {
                $groups .= "d.element_name = {$user_group->group_id}";
              }
              else
              {
                $groups .= " OR d.element_name = {$user_group->group_id}";
              }

              $count++;
            }

            //For each field with logic conditions, retrieve all form_entries that satisfy the conditions
            $sql = "SELECT a.id as id FROM form_entry a
                        LEFT JOIN ap_form_{$form_id} b
                          ON a.entry_id = b.id
                        LEFT JOIN {$element->getElementTableName()} c
                          ON b.element_{$element_id} = c.{$element->getElementFieldValue()}
                        LEFT JOIN ap_permission_logic_conditions d
                          ON c.{$element->getElementFieldName()} = d.rule_keyword
                        WHERE
                          a.form_id = {$form_id}
                          AND a.approved = {$stage}
                          AND ({$groups})
                          GROUP BY a.id";
            $total_apps = $total_apps + mysql_num_rows(mysql_query($sql, $dbconn));
          }

        }

        return $total_apps;
    }

    //Get list of applications that only the logged in user group is allowed to see
    public function get_logiced_applications($form_id, $page,$stage,$user,$filters = array())
    {
        $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

        $q = null;

        if(!$user->mfHasCredential('accesssubmenu'.$stage))
        {
          $stage = "-1"; //Coz they don't have permissions to see applications in this stage
        }

        $filters['application_form'] = $form_id;

        //Get other filters from logic settings
        $sql = "SELECT * FROM ap_forms WHERE form_id = ".$form_id;
        $form_result = mysql_query($sql, $dbconn);

        $form_settings = mysql_fetch_assoc($form_result);

        //Logic field values
        $sql = "SELECT * FROM ap_permission_logic_elements WHERE form_id = " . $form_id;
        $element_result = mysql_query($sql, $dbconn);

        $entries = "";
        $count = 0;

        while($element_values = mysql_fetch_assoc($element_result))
        {
              $element_id = $element_values['element_id'];

              $q = Doctrine_Query::create()
                 ->from("ApFormElements a")
                 ->where("a.element_id = ?", $element_id)
                 ->andWhere("a.form_id = ?", $form_id);
              $element = $q->fetchOne();

              if($element & $element->getElementTableName() == "")
              {
                $groups = "";

                $q = Doctrine_Query::create()
                   ->from("MfGuardUserGroup a")
                   ->where("a.user_id = ?", $user->getAttribute('userid'));
                $user_groups = $q->execute();

                $count = 0;
                foreach($user_groups as $user_group)
                {
                  if($count == 0)
                  {
                    $groups .= "d.element_name = {$user_group->group_id}";
                  }
                  else
                  {
                    $groups .= " OR d.element_name = {$user_group->group_id}";
                  }

                  $count++;
                }

                //For each field with logic conditions, retrieve all form_entries that satisfy the conditions
                $sql = "SELECT a.id as id FROM form_entry a
                            LEFT JOIN ap_form_{$form_id} b
                              ON a.entry_id = b.id
                            LEFT JOIN ap_element_options c
                              ON b.element_{$element_id} = c.option_id
                            LEFT JOIN ap_permission_logic_conditions d
                              ON c.element_id = d.target_element_id AND c.option = d.rule_keyword
                            WHERE
                              a.form_id = {$form_id}
                              AND c.form_id = {$form_id}
                              AND c.element_id = {$element_id}
                              AND a.approved = {$stage}
                              AND ({$groups})
                              GROUP BY a.id";
                $total_apps = mysql_query($sql, $dbconn);

                $count = 0;
                while($row = mysql_fetch_assoc($total_apps))
                {
                    $count++;
                    if($count == 1)
                    {
                        $entries = "a.id = ".$row['id'];
                    }
                    else
                    {
                        $entries .= " OR a.id = ".$row['id'];
                    }
                }
            }
            else {
                $groups = "";

                $q = Doctrine_Query::create()
                   ->from("MfGuardUserGroup a")
                   ->where("a.user_id = ?", $user->getAttribute('userid'));
                $user_groups = $q->execute();

                $count = 0;
                foreach($user_groups as $user_group)
                {
                  if($count == 0)
                  {
                    $groups .= "d.element_name = {$user_group->group_id}";
                  }
                  else
                  {
                    $groups .= " OR d.element_name = {$user_group->group_id}";
                  }

                  $count++;
                }

                //For each field with logic conditions, retrieve all form_entries that satisfy the conditions
                $sql = "SELECT a.id as id FROM form_entry a
                            LEFT JOIN ap_form_{$form_id} b
                              ON a.entry_id = b.id
                            LEFT JOIN {$element->getElementTableName()} c
                              ON b.element_{$element_id} = c.{$element->getElementFieldValue()}
                            LEFT JOIN ap_permission_logic_conditions d
                              ON c.{$element->getElementFieldName()} = d.rule_keyword
                            WHERE
                              a.form_id = {$form_id}
                              AND a.approved = {$stage}
                              AND ({$groups})
                              GROUP BY a.id";
                $total_apps = mysql_query($sql, $dbconn);

                $count = 0;
                while($row = mysql_fetch_assoc($total_apps))
                {
                    $count++;
                    if($count == 1)
                    {
                        $entries = "a.id = ".$row['id'];
                    }
                    else
                    {
                        $entries .= " OR a.id = ".$row['id'];
                    }
                }
            }
        }

        if($count == 0)
        {
            $q = Doctrine_Query::create()
                ->from("FormEntry a")
                ->where("a.id = 0")
                ->orderBy("a.application_id DESC");
                //->orderBy("a.date_of_submission DESC");
        }
        else
        {
            $q = Doctrine_Query::create()
                ->from("FormEntry a")
                ->where("a.parent_submission = ?", 0)
                ->andWhere("a.deleted_status = ?", 0)
                ->andWhere($entries)
                ->orderBy("a.application_id DESC");
                //->orderBy("a.date_of_submission DESC");
        }

        if($page == 0)
        {
            return $q->execute();
        }
        else
        {
            $pager = new sfDoctrinePager(
                'FormEntry',
                50000
            );
            $pager->setQuery($q);
            $pager->setPage($page);
            $pager->init();

            return $pager;
        }
    }

    //Has a limit to make it more effecient on view list of applications with pagination
    public function get_logiced_tabled_applications($form_id, $page,$stage,$user,$filters = array())
    {
        $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

        $q = null;

        if(!$user->mfHasCredential('accesssubmenu'.$stage))
        {
          $stage = "-1"; //Coz they don't have permissions to see applications in this stage
        }

        $filters['application_form'] = $form_id;

        //Get other filters from logic settings
        $sql = "SELECT * FROM ap_forms WHERE form_id = ".$form_id;
        $form_result = mysql_query($sql, $dbconn);

        $form_settings = mysql_fetch_assoc($form_result);

                //Logic field values
                $sql = "SELECT * FROM ap_permission_logic_elements WHERE form_id = " . $form_id;
                $element_result = mysql_query($sql, $dbconn);


        $entries = "";
        $count = 0;

        while($element_values = mysql_fetch_assoc($element_result))
        {
                $element_id = $element_values['element_id'];

                $q = Doctrine_Query::create()
                   ->from("ApFormElements a")
                   ->where("a.element_id = ?", $element_id)
                   ->andWhere("a.form_id = ?", $form_id);
                $element = $q->fetchOne();

                if($element & $element->getElementTableName() == "")
                {
                  $groups = "";

                  $q = Doctrine_Query::create()
                     ->from("MfGuardUserGroup a")
                     ->where("a.user_id = ?", $user->getAttribute('userid'));
                  $user_groups = $q->execute();

                  $user_count = 0;
                  foreach($user_groups as $user_group)
                  {
                    if($user_count == 0)
                    {
                      $groups .= "d.element_name = {$user_group->group_id}";
                    }
                    else
                    {
                      $groups .= " OR d.element_name = {$user_group->group_id}";
                    }

                    $user_count++;
                  }

                  $offset = ($page * 10) - 10;

                  //For each field with logic conditions, retrieve all form_entries that satisfy the conditions
                  $sql = "SELECT a.id as id FROM form_entry a
                              LEFT JOIN ap_form_{$form_id} b
                                ON a.entry_id = b.id
                              LEFT JOIN ap_element_options c
                                ON b.element_{$element_id} = c.option_id
                              LEFT JOIN ap_permission_logic_conditions d
                                ON c.element_id = d.target_element_id AND c.option = d.rule_keyword
                              WHERE
                                a.form_id = {$form_id}
                                AND c.form_id = {$form_id}
                                AND c.element_id = {$element_id}
                                AND a.approved = {$stage}
                                AND ({$groups})
                              GROUP BY a.id
                              ORDER BY a.application_id ASC
                              LIMIT ".$offset.",10";
                  $total_apps = mysql_query($sql, $dbconn);

                  while($row = mysql_fetch_assoc($total_apps))
                  {
                      $count++;
                      if($count == 1)
                      {
                          $entries = "a.id = ".$row['id'];
                      }
                      else
                      {
                          $entries .= " OR a.id = ".$row['id'];
                      }
                  }
                }
                else {
                    $groups = "";

                    $q = Doctrine_Query::create()
                       ->from("MfGuardUserGroup a")
                       ->where("a.user_id = ?", $user->getAttribute('userid'));
                    $user_groups = $q->execute();

                    $user_count = 0;
                    foreach($user_groups as $user_group)
                    {
                      if($user_count == 0)
                      {
                        $groups .= "d.element_name = {$user_group->group_id}";
                      }
                      else
                      {
                        $groups .= " OR d.element_name = {$user_group->group_id}";
                      }

                      $user_count++;
                    }

                    $offset = ($page * 10) - 10;

                    //For each field with logic conditions, retrieve all form_entries that satisfy the conditions
                    $sql = "SELECT a.id as id FROM form_entry a
                                LEFT JOIN ap_form_{$form_id} b
                                  ON a.entry_id = b.id
                                LEFT JOIN {$element->getElementTableName()} c
                                  ON b.element_{$element_id} = c.{$element->getElementFieldValue()}
                                LEFT JOIN ap_permission_logic_conditions d
                                  ON c.{$element->getElementFieldName()} = d.rule_keyword
                                WHERE
                                  a.form_id = {$form_id}
                                  AND a.approved = {$stage}
                                  AND ({$groups})
                                GROUP BY a.id
                                ORDER BY a.application_id ASC
                                LIMIT ".$offset.",10";
                    $total_apps = mysql_query($sql, $dbconn);

                    while($row = mysql_fetch_assoc($total_apps))
                    {
                        $count++;
                        if($count == 1)
                        {
                            $entries = "a.id = ".$row['id'];
                        }
                        else
                        {
                            $entries .= " OR a.id = ".$row['id'];
                        }
                    }
                }
        }

        if($count == 0)
        {
          if(sfConfig::get('app_applications_order') == "asc")
          {
            $q = Doctrine_Query::create()
                ->from("FormEntry a")
                ->where("a.id = 0")
                ->orderBy("a.application_id ASC")
                ->limit(10);
                //->orderBy("a.date_of_submission DESC");
          }
          else
          {
            $q = Doctrine_Query::create()
                ->from("FormEntry a")
                ->where("a.id = 0")
                ->orderBy("a.application_id DESC")
                ->limit(10);
                //->orderBy("a.date_of_submission DESC");
          }
        }
        else
        {
          if(sfConfig::get('app_applications_order') == "asc")
          {
            $q = Doctrine_Query::create()
                ->from("FormEntry a")
                ->where("a.parent_submission = ?", 0)
                ->andWhere("a.deleted_status = ?", 0)
                ->andWhere($entries)
                ->limit(10);
                //->orderBy("a.date_of_submission DESC");
          }
          else {
            $q = Doctrine_Query::create()
                ->from("FormEntry a")
                ->where("a.parent_submission = ?", 0)
                ->andWhere("a.deleted_status = ?", 0)
                ->andWhere($entries)
                ->limit(10);
                //->orderBy("a.date_of_submission DESC");
          }
        }

        if($page == 0)
        {
            return $q->execute();
        }
        else
        {
            $pager = new sfDoctrinePager(
                'FormEntry',
                50000
            );
            $pager->setQuery($q);
            $pager->setPage(1);
            $pager->init();

            return $pager;
        }
    }

    //Get list of applications that only the logged in user group is allowed to see
    public function get_logiced_entries($form_id, $user, $identifier)
    {
        $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

        $q = null;

        $filters['application_form'] = $form_id;

        //Get other filters from logic settings
        $sql = "SELECT * FROM ap_forms WHERE form_id = ".$form_id;
        $form_result = mysql_query($sql, $dbconn);

        $form_settings = mysql_fetch_assoc($form_result);

        //Logic field values
        $sql = "SELECT * FROM ap_permission_logic_elements WHERE form_id = " . $form_id;
        $element_result = mysql_query($sql, $dbconn);

        $entries = "";
        $count = 0;

        while($element_values = mysql_fetch_assoc($element_result))
        {

        $element_id = $element_values['element_id'];

        $sql = "SELECT * FROM ap_permission_logic_conditions WHERE form_id = " . $form_id . " AND target_element_id = " . $element_id;
        $conditions = mysql_query($sql, $dbconn);


        $filters['form_dropdown_fields'] = $element_id;

        //Since a user can belong to more than one group, concat the value based on user's current groups
        $allowed_values = "";
        $all_count = 0;
        while($condition = mysql_fetch_assoc($conditions))
        {
            if($user->hasMfGroup($condition['element_name'], $user->getAttribute('userid')))
            {
                $all_count++;

                $sql = "SELECT * FROM ap_element_options WHERE form_id = " . $form_id . " AND element_id = " . $element_id . " AND `option` = '" . $condition['rule_keyword']."'";
                $option_result = mysql_query($sql, $dbconn);

                $option_values = mysql_fetch_assoc($option_result);

                if($all_count == 1)
                {
                    $allowed_values = "b.element_".$filters['form_dropdown_fields']." = ".$option_values['option_id'];
                }
                else
                {
                    $allowed_values .= " OR b.element_".$filters['form_dropdown_fields']." = ".$option_values['option_id'];
                }
            }
        }

        $sql = "SELECT a.id as id FROM form_entry a LEFT JOIN ap_form_".$filters['application_form']." b ON a.entry_id = b.id WHERE a.form_id = ".$filters['application_form']." AND (".$allowed_values.")";
        $results = mysql_query($sql, $dbconn);

        while($row = mysql_fetch_assoc($results))
        {
            $count++;
            if($count == 1)
            {
                $entries = $identifier.".id = ".$row['id'];
            }
            else
            {
                $entries .= " OR ".$identifier.".id = ".$row['id'];
            }
        }
      }

        return $entries;
    }
   
    /**Get list of all applications -
     *  OTB patch - I added this becoz i didnt want to mess around with the existing function
        The initial developer may have called this function 
     * some place and may have different parameters */
    public function get_applications_listing_custom($page,$stage,$user,$filters,$app_queuing)
    {
        $q = null;

		 $agency_manager = new AgencyManager();//OTB Patch - Multiagency fucntionality, stage listing check
        //if($user->mfHasCredential('accesssubmenu'.$stage) && $agency_manager->checkAgencyStageAccess($user->getAttribute('userid'), $stage))//OTB Patch - Multiagency fucntionality, stage listing check
        if($user->mfHasCredential('accesssubmenu'.$stage))
        {
        error_log("User Has permissions >>>>> ") ;
          $submenus = "a.approved = ".$stage." ";
        }
        else
        {
            error_log("No permissions >>>>> ") ;
          $submenus = "a.approved = -1 "; //Coz they don't have permissions to see applications in this stage
        }

        if(sizeof($filters) > 0)
        {
            error_log("Debug: Filters for Apps >> ") ;
           // $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
          //  mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

            $sql = "SELECT a.id as id FROM form_entry a LEFT JOIN ap_form_".$filters['application_form']." b ON a.entry_id = b.id WHERE a.form_id = ".$filters['application_form']." AND b.element_".$filters['form_dropdown_fields']." = ".$filters['form_dropdown_value_fields']." AND (".$submenus.")";
           // $results = mysql_query($sql, $dbconn);
            //OTB patch - Lets remove the connection request above and use current connection instead
            $results = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($sql) ;
            
            $entries = "";

            $count = 0;
            while($row = $results)
            {
                $count++;
                if($count == 1)
                {
                    $entries = "a.id = ".$row['id'];
                }
                else
                {
                    $entries .= " OR a.id = ".$row['id'];
                }
            }

            if($count == 0)
            {
               $q = Doctrine_Query::create()
                        ->from("FormEntry a")
                        ->where("a.id = 0")
                       // ->orderBy("a.application_id ".$app_queuing);
                        ->orderBy("a.date_of_submission ".$app_queuing);
            }
            else
            {
               $q = Doctrine_Query::create()
                        ->from("FormEntry a")
                        ->where("a.parent_submission = ?", 0)
                        ->andWhere("a.deleted_status = ?", 0)
                        ->andWhere($entries)
                        ->orderBy("a.date_of_submission ".$app_queuing);
             }
        }
        else
        {
           
             $q = Doctrine_Query::create()
                   ->from("FormEntry a")
                   ->where("a.parent_submission = ?", 0)
                   ->andWhere("a.deleted_status = ?", 0)
                   ->andWhere($submenus) 
                   ->orderBy("a.id ".$app_queuing);

                   error_log("Debug: No Filters for Apps 1>> ".$submenus) ;
                   error_log("Debug: No Filters for Apps 2 >> ".$app_queuing) ;
         }
        $pager = new sfDoctrinePager(
            'FormEntry',
            50000
        );
        $pager->setQuery($q);
        $pager->setPage($page);
        $pager->init();

        return $pager;
    }
    
    

    public function get_applications_listing_custom_today($page,$stage,$user,$filters,$app_queuing)
    {
        $q = null;
		$date_today=date("Y-m-d");
		 $agency_manager = new AgencyManager();//OTB Patch - Multiagency fucntionality, stage listing check
        //if($user->mfHasCredential('accesssubmenu'.$stage) && $agency_manager->checkAgencyStageAccess($user->getAttribute('userid'), $stage))//OTB Patch - Multiagency fucntionality, stage listing check
        if($user->mfHasCredential('accesssubmenu'.$stage))
        {
        error_log("User Has permissions >>>>> ") ;
          $submenus = "a.approved = ".$stage." ";
        }
        else
        {
            error_log("No permissions >>>>> ") ;
          $submenus = "a.approved = -1 "; //Coz they don't have permissions to see applications in this stage
        }
		if($stage==9)
        {
             $q = Doctrine_Query::create()
                   ->from("FormEntry a")
                   ->where("a.parent_submission = ?", 0)
                   ->andWhere("a.deleted_status = ?", 0)
                   ->andWhere("a.date_of_submission like ?", '%'.$date_today.'%')
                   //->andWhere($submenus) //display only applications at resubmission stage...................................................
                   ->orderBy("a.id ".$app_queuing);

                   error_log("Debug: No Filters for Apps 1>> ".$submenus) ;
                   error_log("Debug: No Filters for Apps 2 >> ".$app_queuing) ;
         }	
			
		if($stage==50)
        {
             $q = Doctrine_Query::create()
                   ->from("FormEntry a")
                   ->where("a.parent_submission = ?", 0)
                   ->andWhere("a.deleted_status = ?", 0)
                   //->andWhere("a.date_of_submission like ?", '%'.date("Y-m-d").'%')
                   ->andWhere($submenus) //display only applications at resubmission stage...................................................
                   ->orderBy("a.id ".$app_queuing);

                   error_log("Debug: No Filters for Apps 1>> ".$submenus) ;
                   error_log("Debug: No Filters for Apps 2 >> ".$app_queuing) ;
         }
		 
		 if($stage==21)
        {
             $q = Doctrine_Query::create()
                   ->from("FormEntry a")
                   ->where("a.parent_submission = ?", 0)
                   ->andWhere("a.deleted_status = ?", 0)
				   ->andWhere ("a.date_of_issue like ?",'%'.$date_today.'%')
                   ->andWhere($submenus) //display only applications at resubmission stage...................................................
                   ->orderBy("a.id ".$app_queuing);

                   error_log("Debug: No Filters for Apps 1>> ".$submenus) ;
                   error_log("Debug: No Filters for Apps 2 >> ".$app_queuing) ;
         }
		 
		 if($stage==16){
			 
			 //select all stage types is 6 - rejected
        $agency = new AgencyManager();
        $q_stages_reject = Doctrine_Query::create()
                ->from('SubMenus s')

                ->where('s.stage_type = ? ', 6); //Rejected
        $q_stages_res = $q_stages_reject->execute();
        $stage_ids = array();
        //
        foreach($q_stages_res as $res){
            //Only push menus that the user has access for
            array_push($stage_ids, $res['id']) ;
           
        }

        //
        $q = Doctrine_Query::create()
                ->from('FormEntry f')
                ->where('f.approved in ('.implode(',',$stage_ids).')')
                ->andWhere('f.parent_submission = ? ', 0)
                ->andWhere('f.deleted_status = ? ', 0);
		 }
		 
		 
		 if($stage==10){
			 
			 	
		
        $agency = new AgencyManager();
		
		//select tuples with whose type is send back for corrections..........
        $q_selected_corrections = Doctrine_Query::create()
                ->from('SubMenus s')

                ->where('s.stage_type = ? ', 5); //Sent for Corrections	
         $q_selected_corrections_res = $q_selected_corrections->execute();
         $stage_c_ids = array() ;
		 
		 //collect id of stage type is send back to corrections..............
         foreach($q_selected_corrections_res as $res){
             //Only push menus that the user has access for
            /* if($sf_user->mfHasCredential('accesssubmenu'.$res['id'])) {
               array_push($stage_c_ids, $res['id']) ;
             }*/
              array_push($stage_c_ids, $res['id']) ;

        }
       // error_log("Stage ids >>>> ".implode(",",$stage_c_ids));
	   
	   //get application in stages of type send back to corrections
       $q= Doctrine_Query::create()
                ->from('FormEntry f')
                ->where('f.approved in ('.implode(",",$stage_c_ids).')')
                ->andWhere('f.parent_submission = ? ', 0)
                ->andWhere('f.deleted_status = ? ', 0); 
			 
			 
		 }
		 
		 
		 
		 
        $pager = new sfDoctrinePager(
            'FormEntry',
            50
        );
        $pager->setQuery($q);
        $pager->setPage($page);
        $pager->init();

        return $pager;
    }


    
    //Get list of all applications
    public function get_applications($page,$stage,$user,$filters)
    {
        $q = null;

        if($user->mfHasCredential('accesssubmenu'.$stage))
        {
          $submenus = "a.approved = ".$stage." ";
        }
        else
        {
          $submenus = "a.approved = -1 "; //Coz they don't have permissions to see applications in this stage
        }

        if(sizeof($filters) > 0)
        {
            $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
            mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

            $sql = "SELECT a.id as id FROM form_entry a LEFT JOIN ap_form_".$filters['application_form']." b ON a.entry_id = b.id WHERE a.form_id = ".$filters['application_form']." AND b.element_".$filters['form_dropdown_fields']." = ".$filters['form_dropdown_value_fields']." AND (".$submenus.")";
            $results = mysql_query($sql, $dbconn);
            $entries = "";

            $count = 0;
            while($row = mysql_fetch_assoc($results))
            {
                $count++;
                if($count == 1)
                {
                    $entries = "a.id = ".$row['id'];
                }
                else
                {
                    $entries .= " OR a.id = ".$row['id'];
                }
            }

            if($count == 0)
            {
                if(sfConfig::get('app_applications_order') == "asc")
                {
                    $q = Doctrine_Query::create()
                        ->from("FormEntry a")
                        ->where("a.id = 0")
                        ->orderBy("a.date_of_submission ASC");
                        //->orderBy("a.date_of_submission DESC");
                }
                else
                {
                    $q = Doctrine_Query::create()
                        ->from("FormEntry a")
                        ->where("a.id = 0")
                        ->orderBy("a.application_id DESC");
                        //->orderBy("a.date_of_submission DESC");
                }
            }
            else
            {
                if(sfConfig::get('app_applications_order') == "asc")
                {
                    $q = Doctrine_Query::create()
                        ->from("FormEntry a")
                        ->where("a.parent_submission = ?", 0)
                        ->andWhere("a.deleted_status = ?", 0)
                        ->andWhere($entries)
                        ->orderBy("a.date_of_submission ASC");
                        //->orderBy("a.date_of_submission DESC");
                }
                else
                {
                    $q = Doctrine_Query::create()
                        ->from("FormEntry a")
                        ->where("a.parent_submission = ?", 0)
                        ->andWhere("a.deleted_status = ?", 0)
                        ->andWhere($entries)
                        ->orderBy("a.application_id DESC");
                        //->orderBy("a.date_of_submission DESC");
                }
             }
        }
        else
        {
            if(sfConfig::get('app_applications_order') == "asc")
            {
                $q = Doctrine_Query::create()
                   ->from("FormEntry a")
                   ->where("a.parent_submission = ?", 0)
                   ->andWhere("a.deleted_status = ?", 0)
                   ->andWhere($submenus)
                   ->orderBy("a.date_of_submission ASC");
                   //->orderBy("a.date_of_submission DESC");
            }
            else
            {
                $q = Doctrine_Query::create()
                   ->from("FormEntry a")
                   ->where("a.parent_submission = ?", 0)
                   ->andWhere("a.deleted_status = ?", 0)
                   ->andWhere($submenus)
                   ->orderBy("a.application_id DESC");
                   //->orderBy("a.date_of_submission DESC");
            }
         }

        $pager = new sfDoctrinePager(
            'FormEntry',
            10
        );
        $pager->setQuery($q);
        $pager->setPage($page);
        $pager->init();

        return $pager;
    }

    //Determines whether the form can be autosubmitted
    public function can_autosubmit_form($form_id)
    {
        //The following conditions must be met
            //1. The form must have only 1 active element
            //2. The element must have a default value which is a tag that returns a value e.g. {sf_username}
            //3. The elemnt must have settings for fetching a remote value from a remote database

        // First condition, check number of elements
        $q = Doctrine_Query::create()
            ->from("ApFormElements a")
            ->where("a.form_id = ?", $form_id)
            ->andWhere("a.element_status = 1");
        $elements = $q->count();

        if($elements > 1)
        {
            return false;
        }

        // Second condition, check the default value
        $q = Doctrine_Query::create()
            ->from("ApFormElements a")
            ->where("a.form_id = ?", $form_id)
            ->andWhere("a.element_status = 1")
            ->limit(1);
        $element = $q->fetchOne();

        if($element->getElementDefaultValue() == "")
        {
            return false;
        }

        // Third condition, can fetch remote data
        $q = Doctrine_Query::create()
            ->from("ApFormElements a")
            ->where("a.form_id = ?", $form_id)
            ->andWhere("a.element_status = 1")
            ->limit(1);
        $element = $q->fetchOne();

        if($element->getElementOptionQuery() == "")
        {
            return false;
        }

        return true;
    }

    //Attempts to autosubmit a form
    public function autosubmit_form($form_id, $user_id)
    {
        $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

        $template_parser = new templateparser();

        //Fetch default value and parse it
        $q = Doctrine_Query::create()
            ->from("ApFormElements a")
            ->where("a.form_id = ?", $form_id)
            ->andWhere("a.element_status = 1")
            ->limit(1);
        $element = $q->fetchOne();

        $default_value = $element->getElementDefaultValue();

        $entry_data  = $template_parser->parseUser($user_id,$default_value);

        //First check if data is in remote database
        $remote_url  = $element->getElementOptionQuery(); //http://remote
        $criteria = $element->getElementFieldName(); //can be 'records', 'norecords' or 'value'
        $result_value = $element->getElementFieldValue(); //If criteria is 'value'
        $remote_username = $element->getElementRemoteUsername(); //Remote username
        $remote_password = $element->getElementRemotePassword(); //Remote password

        $ch = curl_init();

        $pos = strpos($remote_url, '$value');

        if($pos === false)
        {
            //dont' do anything
        }
        else
        {
            $remote_url = str_replace('$value', curl_escape($ch, $entry_data) , $remote_url);
        }

        $pos = strpos($remote_url, '{fm_element_'.$element['element_id'].'}');

        curl_setopt($ch, CURLOPT_URL, $remote_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        if(!empty($remote_username) && !empty($remote_password))
        {
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            curl_setopt($ch, CURLOPT_USERPWD, "$remote_username:$remote_password");
        }

        $results = curl_exec($ch);

        curl_close($ch);

        if(empty($error))
        {
            $values = json_decode($results);
            if($criteria == "records")
            {
                //If count is = 0, fail
                if($values->{'count'} == 0)
                {
                    $error = "<div class='contentpanel'><div class='panel panel-default'><div class='panel-heading'><h4 class='panel-title'>Your application cannot be submitted at this time:</h4></div> <div class='panel-body'><div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>No records found on server</div></div></div></div>";
                    echo $error;
                }
                else
                {
                    //Create review page entry details
                    //The form should have a review page otherwise it would just go straight to the payment settings or applications page
                    $q = Doctrine_Query::create()
                        ->from("ApForms a")
                        ->where("a.form_id = ?", $form_id)
                        ->limit(1);
                    $form = $q->fetchOne();

                    if($form->getFormReview())
                    {
                        //Enter data into review table and redirect
                        $sql = "INSERT INTO ap_form_".$form_id."_review (date_created, element_".$element->getElementId().") VALUES('".date('Y-m-d')."','".$entry_data."')";
                        mysql_query($sql,$dbconn) or die(mysql_error());

                        $_SESSION['review_id'] = mysql_insert_id($dbconn);

                        //Redirect to review page
                        header("Location: /index.php/forms/confirmApplication?id={$form_id}");
                        exit;
                    }
                    else
                    {
                        echo "<div class='contentpanel'><div class='panel panel-default'><div class='panel-heading'><h4 class='panel-title'>Your application cannot be submitted at this time:</h4></div> <div class='panel-body'><div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>You must enable review page for this form if you want to use automatic submission</div></div></div></div>";
                    }
                }
            }
            else if($criteria == "norecords")
            {
                //If count is greater than 0, then pass
                if($values->{'count'} > 0)
                {
                    $error = "<div class='contentpanel'><div class='panel panel-default'><div class='panel-heading'><h4 class='panel-title'>Your application cannot be submitted at this time:</h4></div> <div class='panel-body'><div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Existing records found on server</div></div></div></div>";
                    echo $error;
                }
            }
            else if($criteria == "value")
            {
                //If count is greater than 0, then pass
                if($result_value != $results)
                {
                    $error = "<div class='contentpanel'><div class='panel panel-default'><div class='panel-heading'><h4 class='panel-title'>Your application cannot be submitted at this time:</h4></div> <div class='panel-body'><div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>No matching records found on server</div></div></div></div>";
                    echo $error;
                }
                else
                {
                    //Create review page entry details
                    //The form should have a review page otherwise it would just go straight to the payment settings or applications page
                    $q = Doctrine_Query::create()
                        ->from("ApForms a")
                        ->where("a.form_id = ?", $form_id)
                        ->limit(1);
                    $form = $q->fetchOne();

                    if($form->getFormReview())
                    {
                        //Enter data into review table and redirect
                        $sql = "INSERT INTO ap_form_".$form_id."_review (date_created, element_".$element->getElementId().") VALUES('".date('Y-m-d')."','".$entry_data."')";
                        mysql_query($sql,$dbconn) or die(mysql_error());

                        $_SESSION['review_id'] = mysql_insert_id($dbconn);

                        //Redirect to review page
                        header("Location: /index.php/forms/confirmApplication?id={$form_id}");
                        exit;
                    }
                    else
                    {
                        echo "<div class='contentpanel'><div class='panel panel-default'><div class='panel-heading'><h4 class='panel-title'>Your application cannot be submitted at this time:</h4></div> <div class='panel-body'><div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>You must enable review page for this form if you want to use automatic submission</div></div></div></div>";
                    }
                }
            }
        }
        else
        {
            $error = "<div class='contentpanel'></div><div class='panel panel-default'><div class='panel-heading'><h4 class='panel-title'>Your application cannot be submitted at this time:</h4></div> <div class='panel-body'><div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Could not connect to remote database. Try again later.</div></div></div></div>";
            echo $error;
        }
    }

    //Attempts to autosubmit a form
    public function autosubmit_form_backend($form_id, $user_id)
    {
        $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

        $template_parser = new templateparser();

        //Fetch default value and parse it
        $q = Doctrine_Query::create()
            ->from("ApFormElements a")
            ->where("a.form_id = ?", $form_id)
            ->andWhere("a.element_status = 1")
            ->limit(1);
        $element = $q->fetchOne();

        $default_value = $element->getElementDefaultValue();

        $entry_data  = $template_parser->parseUser($user_id,$default_value);

        //First check if data is in remote database
        $remote_url  = $element->getElementOptionQuery(); //http://remote
        $criteria = $element->getElementFieldName(); //can be 'records', 'norecords' or 'value'
        $result_value = $element->getElementFieldValue(); //If criteria is 'value'
        $remote_username = $element->getElementRemoteUsername(); //Remote username
        $remote_password = $element->getElementRemotePassword(); //Remote password

        $ch = curl_init();

        $pos = strpos($remote_url, '$value');

        if($pos === false)
        {
            //dont' do anything
        }
        else
        {
            $remote_url = str_replace('$value', curl_escape($ch, $entry_data) , $remote_url);
        }

        $pos = strpos($remote_url, '{fm_element_'.$element['element_id'].'}');

        curl_setopt($ch, CURLOPT_URL, $remote_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        if(!empty($remote_username) && !empty($remote_password))
        {
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            curl_setopt($ch, CURLOPT_USERPWD, "$remote_username:$remote_password");
        }

        $results = curl_exec($ch);

        curl_close($ch);

        if(empty($error))
        {
            $values = json_decode($results);
            if($criteria == "records")
            {
                //If count is = 0, fail
                if($values->{'count'} == 0)
                {
                    $error = "<div class='contentpanel'><div class='panel panel-default'><div class='panel-heading'><h4 class='panel-title'>Your application cannot be submitted at this time:</h4></div> <div class='panel-body'><div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>No records found on server</div></div></div></div>";
                    echo $error;
                }
                else
                {
                    //Create review page entry details
                    //The form should have a review page otherwise it would just go straight to the payment settings or applications page
                    $q = Doctrine_Query::create()
                        ->from("ApForms a")
                        ->where("a.form_id = ?", $form_id)
                        ->limit(1);
                    $form = $q->fetchOne();

                    if($form->getFormReview())
                    {
                        //Enter data into review table and redirect
                        $sql = "INSERT INTO ap_form_".$form_id."_review (date_created, element_".$element->getElementId().") VALUES('".date('Y-m-d')."','".$entry_data."')";
                        mysql_query($sql,$dbconn) or die(mysql_error());

                        $_SESSION['review_id'] = mysql_insert_id($dbconn);

                        //Redirect to review page
                        header("Location: /backend.php/applications/confirmApplication?id={$form_id}");
                        exit;
                    }
                    else
                    {
                        echo "<div class='contentpanel'><div class='panel panel-default'><div class='panel-heading'><h4 class='panel-title'>Your application cannot be submitted at this time:</h4></div> <div class='panel-body'><div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>You must enable review page for this form if you want to use automatic submission</div></div></div></div>";
                    }
                }
            }
            else if($criteria == "norecords")
            {
                //If count is greater than 0, then pass
                if($values->{'count'} > 0)
                {
                    $error = "<div class='contentpanel'><div class='panel panel-default'><div class='panel-heading'><h4 class='panel-title'>Your application cannot be submitted at this time:</h4></div> <div class='panel-body'><div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Existing records found on server</div></div></div></div>";
                    echo $error;
                }
            }
            else if($criteria == "value")
            {
                //If count is greater than 0, then pass
                if($result_value != $results)
                {
                    $error = "<div class='contentpanel'><div class='panel panel-default'><div class='panel-heading'><h4 class='panel-title'>Your application cannot be submitted at this time:</h4></div> <div class='panel-body'><div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>No matching records found on server</div></div></div></div>";
                    echo $error;
                }
                else
                {
                    //Create review page entry details
                    //The form should have a review page otherwise it would just go straight to the payment settings or applications page
                    $q = Doctrine_Query::create()
                        ->from("ApForms a")
                        ->where("a.form_id = ?", $form_id)
                        ->limit(1);
                    $form = $q->fetchOne();

                    if($form->getFormReview())
                    {
                        //Enter data into review table and redirect
                        $sql = "INSERT INTO ap_form_".$form_id."_review (date_created, element_".$element->getElementId().") VALUES('".date('Y-m-d')."','".$entry_data."')";
                        mysql_query($sql,$dbconn) or die(mysql_error());

                        $_SESSION['review_id'] = mysql_insert_id($dbconn);

                        //Redirect to review page
                        header("Location: /backend.php/applications/confirmApplication?id={$form_id}");
                        exit;
                    }
                    else
                    {
                        echo "<div class='contentpanel'><div class='panel panel-default'><div class='panel-heading'><h4 class='panel-title'>Your application cannot be submitted at this time:</h4></div> <div class='panel-body'><div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>You must enable review page for this form if you want to use automatic submission</div></div></div></div>";
                    }
                }
            }
        }
        else
        {
            $error = "<div class='contentpanel'></div><div class='panel panel-default'><div class='panel-heading'><h4 class='panel-title'>Your application cannot be submitted at this time:</h4></div> <div class='panel-body'><div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Could not connect to remote database. Try again later.</div></div></div></div>";
            echo $error;
        }
    }

    //Function that checks if an application form has reached its limit for number of entries permitted
    public function form_entry_limit($form_id, $user_id)
    {
        $form = Doctrine_Core::getTable('ApForms')->find($form_id);

        if($form->getFormUniqueIp())
        {
            //Check for applications with pending invoices and permits that haven't expired
            $q = Doctrine_Query::create()
                ->from("FormEntry a")
                ->leftJoin("a.mfInvoice b")
                ->where("a.user_id = ?", $user_id)
                ->andWhere("a.form_id = ?", $form_id)
                ->andWhere('a.parent_submission = 0')
                ->andWhere('a.approved <> 0')
                ->andWhere('b.paid = 15 OR b.paid = 1')
                ->andWhere('a.declined <> 1')
                ->orderBy("a.id DESC");
            $applications_with_pending_invoices = $q->count();

            if ($applications_with_pending_invoices > 0) {
                return true;
            }
            else
            {
                //Check for permits that haven't expired
                $q = Doctrine_Query::create()
                    ->from("SavedPermit a")
                    ->leftJoin("a.FormEntry b")
                    ->where("b.form_id = ?", $form_id)
                    ->andWhere("b.user_id = ?", $user_id)
                    ->andWhere('b.declined <> 1')
                    ->andWhere("a.date_of_expiry > CURRENT_DATE() or a.date_of_expiry = ?", '');
                $applications_with_valid_permits = $q->count();


                if($applications_with_valid_permits > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        else
        {
            return false;
        }
    }



    function getWorkingDays($startDate,$endDate,$holidays) {
        // do strtotime calculations just once
        $endDate = strtotime($endDate);
        $startDate = strtotime($startDate);
    
    
        //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
        //We add one to inlude both dates in the interval.
        $days = ($endDate - $startDate) / 86400 + 1;
    
        $no_full_weeks = floor($days / 7);
        $no_remaining_days = fmod($days, 7);
    
        //It will return 1 if it's Monday,.. ,7 for Sunday
        $the_first_day_of_week = date("N", $startDate);
        $the_last_day_of_week = date("N", $endDate);
    
        //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
        //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
        if ($the_first_day_of_week <= $the_last_day_of_week) {
            if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
            if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
        }
        else {
            // (edit by Tokes to fix an edge case where the start day was a Sunday
            // and the end day was NOT a Saturday)
    
            // the day of the week for start is later than the day of the week for end
            if ($the_first_day_of_week == 7) {
                // if the start date is a Sunday, then we definitely subtract 1 day
                $no_remaining_days--;
    
                if ($the_last_day_of_week == 6) {
                    // if the end date is a Saturday, then we subtract another day
                    $no_remaining_days--;
                }
            }
            else {
                // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
                // so we skip an entire weekend and subtract 2 days
                $no_remaining_days -= 2;
            }
        }
    
        //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
    //---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
       $workingDays = $no_full_weeks * 5;
        if ($no_remaining_days > 0 )
        {
          $workingDays += $no_remaining_days;
        }
    
        //We subtract the holidays
        foreach($holidays as $holiday){
            $time_stamp=strtotime($holiday);
            //If the holiday doesn't fall in weekend
            if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
                $workingDays--;
        }
    
        return $workingDays;
    }

      //CBS latest version
      public function insert_audit($id, $application_id, $stage_id, $stage_name)
      {
        //Save an application specific log
        try
        {
             //This is not working, dont know why?
              /*$fetch_application = Doctrine_Query::create()
               ->from('FormEntry a')
               ->where('a.id = ?', $id);
           $current_application = $fetch_application->fetchOne();
           error_log(" CURRENT APPLICATION IS ".$current_application->getApproved()) ;*/
               error_log("DEBUG:::::::::::::::::::::::::::::: App id ".$id) ;
               //CBS
               //update the application movement tracking table
              $q = Doctrine_Query::create()
              ->from('ApplicationReference a')
              ->where('a.application_id = ?', $id)
              ->andWhere('a.end_date = ?', "")
              ->orderBy('a.id DESC')
              ->limit(1);
              $oldappref = $q->fetchOne();
               
               error_log("DEBUG:::::::::::::::::::::::::::::: App id ".$id) ;
               
               if($oldappref && ($oldappref->getStageId() != $stage_id))
               {
                  error_log("DEBUG:::::::::::::::::::::::::::::: Update end date App Reference stage >>> ".$oldappref->getStageId(). " New Stage id >> ".$stage_id);
                  $oldappref->setEndDate(date('Y-m-d H:i:s'));
                  $oldappref->save();
                
               } 
           
                error_log("DEBUG:::::::::::::::::::::::::::::: Do a New entry ". " New Stage id >> ".$stage_id);
                  $appref2 = new ApplicationReference();
                  $appref2->setStageId($stage_id);
                  $appref2->setApplicationId($id);
                if($_SESSION["SESSION_CUTEFLOW_USERID"])
                {
                  $appref2->setApprovedBy($_SESSION["SESSION_CUTEFLOW_USERID"]);
                }
                else
                {
					$appref2->setApprovedBy(0);

                }
                  $appref2->setStartDate(date('Y-m-d H:i:s'));
                  $appref2->setEndDate("");
                  $appref2->save();
                
                  error_log("DEBUGXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXappref2 id: ".$appref2->getId());
            
        
             }catch(Exception $ex)
             {
           		error_log("Application Reference Log Failed");
            }
  
        //Save a general log as well
        if($stage_name)
        {
            //Save Audit Log
            $audit = new Audit();
            $audit->saveFullAudit("Moved ".$application_id." to ".$stage_name, $id, "form_entry", "", $stage_name);
        }
        return $appref2->getId();
      }


    //Insert audit trail for current application
    public function insert_auditotb($id, $application_id, $stage_id, $stage_name)
    {
      //Save an application specific log
      try
      {
        if($_SESSION["SESSION_CUTEFLOW_USERID"])
        {
            $q = Doctrine_Query::create()
			 ->from('ApplicationReference a')
			 ->where('a.application_id = ?', $id)
			 ->andWhere('a.end_date = ?', "");
	     $oldappref = $q->fetchOne();
             //
             error_log(" App id ".$id) ;
             
             if($oldappref && ($oldappref->getStageId() != $stage_id))
             {
                 error_log("Update end date App Reference stage >>> ".$oldappref->getStageId(). " New Stage id >> ".$stage_id);
                 $oldappref->setEndDate(date('Y-m-d H:i:s'));
		 $oldappref->save();
             
              /*  $appref = new ApplicationReference();
                $appref->setStageId($stage_id);
                $appref->setApplicationId($id);
                $appref->setApprovedBy($_SESSION["SESSION_CUTEFLOW_USERID"]);
                $appref->setStartDate(date('Y-m-d'));
                $appref->setEndDate("");
                $appref->save(); */
             }else {
              error_log("Do a New entry ". " New Stage id >> ".$stage_id);
                $appref = new ApplicationReference();
                $appref->setStageId($stage_id);
                $appref->setApplicationId($id);
                $appref->setApprovedBy($_SESSION["SESSION_CUTEFLOW_USERID"]);
                $appref->setStartDate(date('Y-m-d H:i:s'));
                $appref->setEndDate("");
                $appref->save();
             }
               
        }
        else
        {
         error_log("Else part called > System Action ") ;
          $appref = new ApplicationReference();
          $appref->setStageId($stage_id);
          $appref->setApplicationId($id);
          $appref->setApprovedBy(0);
          $appref->setStartDate(date('Y-m-d'));
          $appref->setEndDate("");
          $appref->save(); 
        }
      }catch(Exception $ex)
      {
         error_log("Application Reference Log Failed");
      }

      //Save a general log as well
      if($stage_name)
      {
          //Save Audit Log
          $audit = new Audit();
          $audit->saveFullAudit("Moved ".$application_id." to ".$stage_name, $id, "form_entry", "", $stage_name);
      }
    }

    //Send notifications for current stage of the application
    public function queue_notifications($id, $form_id, $entry_id, $user_id, $stage_id, $subject=False, $message=False, $sender_id=False)
    {
	  $templateparser = new Templateparser();
      $q = Doctrine_Query::create()
           ->from('Notifications b')
           ->where('b.submenu_id = ?', $stage_id)
          ->limit(1);
      $notification = $q->fetchOne();

	  //OTB Start Notification to others - get form elements and data for contacts to be copied
      $q = Doctrine_Query::create()
           ->from('ApFormElements b')
           ->where('b.form_id = ?', $form_id)
		   ->andWhereIn('b.element_type',array('email', 'simple_phone'))
           ->andWhere('b.element_notify_contact = ?', 1);
      $elements = $q->execute();
	  
	  $email_query = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc("SELECT * FROM ap_form_".$form_id." WHERE id=".$entry_id);
	  $copy_emails = array();
	  $copy_phone_numbers = array();
	  foreach($email_query as $eq){
			foreach($elements as $element){
				if($element->getElementType() == "email"){
					array_push($copy_emails, $eq['element_'.$element->getElementId()]);
				}else if($element->getElementType() == "simple_phone"){
					array_push($copy_phone_numbers, $eq['element_'.$element->getElementId()]);
				}
			}
	  }
	  //OTB End Notification to others - get form elements and data for contacts to be copied

      if(($notification && $notification->getAutosend() == "1") or $message)
      {
		if($message){
			$body = $message;
			$sms = $message;
		}else{
			$body = $templateparser->parse($id,$form_id,$entry_id, $notification->getContent());
			$subject = $templateparser->parse($id,$form_id,$entry_id, $notification->getTitle());
			$sms = $templateparser->parse($id,$form_id,$entry_id, $notification->getSms());
		}

        $q = Doctrine_Query::create()
          ->from('sfGuardUserProfile b')
          ->where('b.user_id = ?', $user_id)
          ->limit(1);
        $userprofile = $q->fetchOne();

        $notifier = new mailnotifications();
        //$notifier->queueemail(sfConfig::get('app_organisation_email'), $userprofile->getEmail(), $subject, $body, $user_id, $id);
        $notifier->sendemail(sfConfig::get('app_organisation_email'),$userprofile->getEmail(),$subject,$body);//OTB enabled this for now. We should revert to queueing when live

		//OTB Start Notification to others - get form elements and data for contacts to be copied
		foreach($copy_emails as $email){
			//$notifier->queueemail(sfConfig::get('app_organisation_email'), $email, $subject, $body, $user_id, $id);
			$notifier->sendemail(sfConfig::get('app_organisation_email'),$email,$subject,$body);//OTB enabled this for now. We should revert to queueing when live
		}
		//OTB End Notification to others - get form elements and data for contacts to be copied

        if($sms)
        {
            //$notifier->queuesms($userprofile->getMobile(), $sms, $user_id, $id);
			if(!$sender_id and $notification){
				$sender_id = $notification->getSubmenu()->getMenus()->getSmsSender();
			}
			$notifier->sendsms($userprofile->getMobile(), $sms, $sender_id);//OTB enabled this for now. We should revert to queueing when live
			//OTB Start Notification to others - get form elements and data for contacts to be copied
			foreach($copy_phone_numbers as $phone){
				//$notifier->queuesms($phone, $sms, $user_id, $id);
				$notifier->sendsms($phone, $sms, $sender_id);//OTB enabled this for now. We should revert to queueing when live
			}
			//OTB End Notification to others - get form elements and data for contacts to be copied
        }
      }
    }

    //Execute triggers for current stage if any
    public function execute_triggers($current_application)
    {   
        error_log("Debug::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>:::FORM ENTRY ID ".$current_application->getId());

        $q0 = Doctrine_Query::create()
        ->from('ApplicationReference a')
        ->where('a.application_id = ?', $current_application->getId())
        ->andWhere('a.end_date <> ?', "");
        $thecount= $q0->count();

        error_log("Debug::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>:::The count ".$thecount);

        if($thecount>0){
        //we find the previous stage of the application //CBS
        //NB the below query is redundant and should be removed if we can perform count() and excecute() on one $q 
        $q = Doctrine_Query::create()
        ->from('ApplicationReference a')
        ->where('a.application_id = ?', $current_application->getId())
        ->andWhere('a.end_date <> ?', "")
        ->orderBy('a.id DESC')
        ->limit(1);
        $oldappref = $q->fetchOne();
        $previous_stage="";

        if($oldappref){
            $previous_stage=$oldappref->getStageId();
            error_log("Debug:::::::::::: PREVIOUS STAGE ID IS ".$previous_stage);

        }

      $q = Doctrine_Query::create()
           ->from('SubMenus a')
           ->where('a.id = ?', $current_application->getApproved())//OTB get stage from passed form_entry object
          ->limit(1);

      $current_stage = $q->fetchOne();
    
      
      if($current_stage)
      {
          // if the previous stage is resubmissions, do not assign task to any other default reviewers of the stage current stage //CBS
        error_log("Debug::::: CURRENT STAGE IS ".$current_stage->getId());
        if ($previous_stage==51 || $previous_stage==52 || $previous_stage==53 || $previous_stage==54){
            error_log("Debug::::: PREVIOUS STAGE IS RESUBMISSIONS");
        }
        else{
            error_log("Debug::::: OTHER STAGES, This is stage: ".$current_stage->getId());
        //1. If stage has default reviewers, assign them
        $q = Doctrine_Query::create()
            ->from("WorkflowReviewers a")
            ->where("a.workflow_id = ?", $current_stage->getId());//OTB Patch fixed get id for workflow stage
        $workflow_reviewers = $q->execute();
        $number_of_reviewers=$q->count();
        
//assigning tasks to reviewers in that satge		
if($workflow_reviewers && $number_of_reviewers>0){
	error_log("DEBUG::::::::::::::::::::::::::::::::::::::::::::::FOUND SOME DEFAULT REVIEWERS: ".$number_of_reviewers);
        foreach($workflow_reviewers as $reviewer)
        {
            $task = new Task();
            $task->setType(2);
            $task->setCreatorUserId($reviewer->getReviewerId());
            $task->setOwnerUserId($reviewer->getReviewerId());
            $task->setDuration("0");
            $task->setStartDate(date('Y-m-d H:i:s'));
            $task->setEndDate("");
            $task->setPriority('3');
            $task->setIsLeader("0");
            $task->setActive("1");
            $task->setStatus("1");
            $task->setLastUpdate(date('Y-m-d H:i:s'));
            $task->setDateCreated(date('Y-m-d H:i:s'));
            $task->setRemarks("");
            $task->setApplicationId($current_application->getId());//OTB Fixed to get passed application id
            $task->setTaskStage($current_application->getApproved());
            $task->save();
			
			error_log("DEBUG::::::::::::::::::::::::::::::::::::::::::::::NEW TASK ID: ".$task->getId()." For MR/MRS: ".$reviewer->getReviewerId());
		
			}
		}
		else{
			error_log("DEBUG::::::::::::::::::::::::::::::::::::::::::::::NO DEFAULT REVIEWERS");
		}
    }
		//OTB Start - Add trigger for changing application number
        //2. If stage has change identifier set as true, change identifier and start as required
		if($current_stage->getChangeIdentifier() == 1){

			$app_identifier = $current_stage->getNewIdentifier();
			$starting_number = $current_stage->getNewIdentifierStart();

			$new_app_id = str_replace("{Y}",date("Y"),$app_identifier);//OTB Replace, Y in identifier with current year

			$q = Doctrine_Query::create()
			   ->from("FormEntry a")
			   ->where("a.application_id LIKE ?", "%".$new_app_id."%")
			   ->orderBy("a.application_id DESC");
			$other_submissions = $q->count();

			if($other_submissions > 0 && $starting_number)
			{
				$last_entry = $q->fetchOne();
				$new_app_id = $last_entry->getApplicationId();
				$new_app_id++;
			}
			else if ($starting_number)
			{
				$new_app_id = $new_app_id.$starting_number;
			}
			else//if starting number is null, then only replace the identifier (prefix) of the app number. (separator must be included in prefix)
			{
				$arr = explode(substr($current_stage->getNewIdentifier(), -1), $current_application->getApplicationId(), 2);
				$new_app_id = $new_app_id.$arr[1];
			}

			if(strpos($current_application->getApplicationId(),$current_stage->getNewIdentifier()) !== false){//OTB Fix Start: Only update plan number if new identifier is different from old. Maintain plan number for resubmissions

                        }else{
				//Insert old number into history table before changing the number
				Doctrine_Manager::getInstance()->getCurrentConnection()->execute("Insert into application_number_history (form_entry_id,application_number) Values ('".$current_application->getId()."','".$current_application->getApplicationId()."')");
			    $current_application->setApplicationId($new_app_id);
			}
			//OTB Fix End
		}
		//OTB End - Add trigger for changing application number
      }
    }
    }
}
