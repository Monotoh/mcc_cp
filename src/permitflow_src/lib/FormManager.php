<?php
/**
 * Description of FormManager
 *
 * @author OTB Africa
 */
class FormManager{

    public function FormManager()
    {
    }

	public function getPermittedValueOptions($form_id, $element_id, $dbh=false, $values=false, $logic_id=false)
	{
		$element_data = $this->getElementData($form_id, $element_id, $dbh);
		$options_markup = "Provide Permitted Values:<br/><br/>";
		//$options_markup = "";
		if($element_data->type == 'select'){
			$options = $this->getElementOptions($form_id, $element_id);
			foreach ($options as $option){
				$selected = "";
				if($values and in_array($option->getOptionId(), $values)){
					$selected = "checked='1'";
				}
				$options_markup .= "<input id='dependencyrulepermittedvalues_".$element_id."' name='dependencyrulepermittedvalues_".$element_id."[]' id='select_value_".$option->getOptionId()."' type='checkbox' value='".$option->getOptionId()."' class='checkbox rule_permitted_values' ".$selected."/><label for='select_value_".$option->getOptionId()."'>".$option->getOption()."</label>";
			}
		}else{
				$options_markup .= "<input id='dependencyrulepermittedvalues_".$element_id."' name='dependencyrulepermittedvalues_".$element_id."[]' type='textbox' class='rule_permitted_values'/>";
				$options_markup .= "<br/><a class='add_option_text_field' href='#select_value_input_text'>Add additional permitted value(s)</a>";
				$options_markup .= "<script>jQuery(document).ready(function() {
									\$('a').click(function() {
									\$('<br/><p>OR</p><br/><input id=\'dependencyrulepermittedvalues_".$element_id."\' name=\'dependencyrulepermittedvalues_".$element_id."[]\' type=\'textbox\' class=\'rule_permitted_values\'/><br/>').insertAfter('.add_option_text_field');
									});
									})</script>";
		}

		return $options_markup;
	}

	public function getElementData($form_id, $element_id, $dbh=false){

		$form_html = "";

		//Get Options
		$params = array($form_id, $element_id);
		$query = "SELECT *
					FROM ap_form_elements
				   WHERE
						form_id = ? and element_id = ?
				LIMIT 1";

		$sth = mf_do_query($query,$params,$dbh);

		$element_data = new stdClass();

		while($row = mf_do_fetch_result($sth)){
			$element_data->title 		= nl2br($row['element_title']);
			$element_data->guidelines 	= $row['element_guidelines'];
			$element_data->size 			= $row['element_size'];
			$element_data->is_required 	= $row['element_is_required'];
			$element_data->is_unique 	= $row['element_is_unique'];
			$element_data->is_private 	= $row['element_is_private'];
			$element_data->type 			= $row['element_type'];
			$element_data->position 		= $row['element_position'];
			$element_data->id 			= $row['element_id'];
			$element_data->is_db_live 	= 1;
			$element_data->form_id 		= $form_id;
			$element_data->choice_has_other   = (int) $row['element_choice_has_other'];
			$element_data->choice_other_label = $row['element_choice_other_label'];
			$element_data->choice_columns   	 = (int) $row['element_choice_columns'];
			$element_data->time_showsecond    = (int) $row['element_time_showsecond'];
			$element_data->time_24hour    	 = (int) $row['element_time_24hour'];
			$element_data->address_hideline2	 = (int) $row['element_address_hideline2'];
			$element_data->address_us_only	 = (int) $row['element_address_us_only'];
			$element_data->date_enable_range	 = (int) $row['element_date_enable_range'];
			$element_data->date_range_min	 = $row['element_date_range_min'];
			$element_data->date_range_max	 = $row['element_date_range_max'];
			$element_data->date_enable_selection_limit	 = (int) $row['element_date_enable_selection_limit'];
			$element_data->date_selection_max	 		 = (int) $row['element_date_selection_max'];
			$element_data->date_disable_past_future	 	= (int) $row['element_date_disable_past_future'];
			$element_data->date_past_future	 			= $row['element_date_past_future'];
			$element_data->date_disable_weekend	 		= (int) $row['element_date_disable_weekend'];
			$element_data->date_disable_specific	 		= (int) $row['element_date_disable_specific'];
			$element_data->date_disabled_list	 		= $row['element_date_disabled_list'];
			$element_data->file_enable_type_limit	 	= (int) $row['element_file_enable_type_limit'];
			$element_data->file_block_or_allow	 		= $row['element_file_block_or_allow'];
			$element_data->file_type_list	 			= $row['element_file_type_list'];
			$element_data->file_as_attachment	 		= (int) $row['element_file_as_attachment'];
			$element_data->file_enable_advance	 		= (int) $row['element_file_enable_advance'];
			$element_data->table_name	 		= $row['element_table_name'];
			$element_data->field_value	 		= $row['element_field_value'];
			$element_data->field_error_message	 		= $row['element_field_error_message'];
			$element_data->field_name	 		= $row['element_field_name'];
			$element_data->option_query	 		= $row['element_option_query'];
			$element_data->existing_form	 		= $row['element_existing_form'];
			$element_data->existing_stage	 		= $row['element_existing_stage'];
			$element_data->file_auto_upload	 			= (int) $row['element_file_auto_upload'];
			$element_data->file_enable_multi_upload	 	= (int) $row['element_file_enable_multi_upload'];
			$element_data->file_max_selection	 		= (int) $row['element_file_max_selection'];
			$element_data->file_enable_size_limit	 	= (int) $row['element_file_enable_size_limit'];
			$element_data->file_size_max	 				= (int) $row['element_file_size_max'];
			$element_data->matrix_allow_multiselect	 	= (int) $row['element_matrix_allow_multiselect'];
			$element_data->matrix_parent_id	 			= (int) $row['element_matrix_parent_id'];
			$element_data->upload_dir	 				= $mf_settings['upload_dir'];
			$element_data->range_min	 					= $row['element_range_min'];
			$element_data->range_max	 					= $row['element_range_max'];
			$element_data->range_limit_by	 			= $row['element_range_limit_by'];
			$element_data->jsondef	 					= $row['element_jsondef'];
			$element_data->css_class	 					= $row['element_css_class'];
			$element_data->machform_path	 				= $machform_path;
			$element_data->machform_data_path	 		= $machform_data_path;
			$element_data->section_display_in_email	 	= (int) $row['element_section_display_in_email'];
			$element_data->section_enable_scroll	 		= (int) $row['element_section_enable_scroll'];
		}
		return  $element_data;
	}

	/* OTB - Start Form builder useful functions */
	public function getFieldLabel($form_id, $field_id){
	$q = Doctrine_Query::create()
			->from('ApFormElements a')
			->where('a.form_id = ? AND a.element_id = ?', array($form_id, $field_id))
			->limit(1);
	$element = $q->fetchOne();
	return $element->getElementTitle();
	}

	public function getFormElements($form_id){
		$q = Doctrine_Query::create()
				->from('ApFormElements a')
				->where('a.form_id = ?', array($form_id));
		return  $q->execute();
	}

	public function getElementOptions($form_id, $element_id){
		$q = Doctrine_Query::create()
				->from('ApElementOptions a')
				->where('a.form_id = ? and a.element_id = ? and a.live=1', array($form_id, $element_id));
		return  $q->execute();
	}

	public function getElementOptionIdForText($form_id, $element_id, $option_text){
		$q = Doctrine_Query::create()
				->from('ApElementOptions a')
				->where('a.form_id = ? and a.element_id = ? and a.live=1 and a.option=?', array($form_id, $element_id, $option_text));
		$result = $q->fetchOne();
		return  $result ? $result->getOptionId() : 0;
	}

	public function get_fields_html_markup($form_id, $element_ids){
		//$prefix_folder = dirname(__FILE__) . "/../../../../../lib/vendor/cp_machform/";
		$prefix_folder = "vendor/cp_machform/";
		require($prefix_folder . 'includes/view-functions.php');
		require($prefix_folder . 'includes/db-core.php');
		require($prefix_folder . 'config.php');
		$dbh = mf_connect_db();

		$form_html = "";

		//Get Options
		$params = array($form_id);
		$in  = str_repeat('?,', count($element_ids) - 1) . '?';
		$params = array_merge($params, $element_ids);
		$query = "SELECT
				aeo_id,
						element_id,
						option_id,
						`position`,
						`option`,
						option_is_default
					FROM
						".MF_TABLE_PREFIX."element_options
				   where
						form_id = ? and live=1 and element_id in ($in)
				order by
						element_id asc,`option` asc";

		$sth = mf_do_query($query,$params,$dbh);
		while($row = mf_do_fetch_result($sth)){
			$element_id = $row['element_id'];
			$option_id  = $row['option_id'];
			$options_lookup[$element_id][$option_id]['aeo_id'] 		  = $row['aeo_id'];
			$options_lookup[$element_id][$option_id]['position'] 		  = $row['position'];
			$options_lookup[$element_id][$option_id]['option'] 			  = $row['option'];



			$sql = "SELECT * FROM ext_translations WHERE field_id = ? AND option_id = ? AND field_name = 'option' AND table_class = 'ap_element_options' AND locale = ?";
			$params = array($row['aeo_id'], $option_id, $locale);
			$translation_sth = mf_do_query($sql,$params,$dbh);
			$translation_row = mf_do_fetch_result($translation_sth);
			if($translation_row)
			{
				$options_lookup[$element_id][$option_id]['option']  = $translation_row['trl_content'];
			}

			$options_lookup[$element_id][$option_id]['option_is_default'] = $row['option_is_default'];

			if(isset($element_prices_array[$element_id][$option_id])){
				$options_lookup[$element_id][$option_id]['price_definition'] = $element_prices_array[$element_id][$option_id];
			}
		}
		//End Get Options

		$params = array($form_id);
		$in  = str_repeat('?,', count($element_ids) - 1) . '?';
		$params = array_merge($params, $element_ids);
		$query = "SELECT *
					FROM ap_form_elements
				   WHERE
						form_id = ? and element_status='1' and element_type <> 'page_break' and element_id IN ($in)
				ORDER BY
						element_position asc";

		$sth = mf_do_query($query,$params,$dbh);
		while($row = mf_do_fetch_result($sth)){
			$element_data = new stdClass();
			//lookup element options first
			if(!empty($options_lookup[$element_id])){
				$element_options = array();
				$i=0;
				foreach ($options_lookup[$element_id] as $option_id=>$data){
					$element_options[$i] = new stdClass();
					$element_options[$i]->id 		 = $option_id;
					$element_options[$i]->option 	 = $data['option'];

					$sql = "SELECT * FROM ext_translations WHERE field_id = ? AND option_id = ? AND field_name = 'option' AND table_class = 'ap_element_options' AND locale = ?";
					$params = array($data['aeo_id'], $element_options[$i]->id, $locale);
					$translation_sth = mf_do_query($sql,$params,$dbh);
					$translation_row = mf_do_fetch_result($translation_sth);
					if($translation_row)
					{
						$element_options[$i]->option = $translation_row['trl_content'];
					}

					$element_options[$i]->is_default = $data['option_is_default'];
					$element_options[$i]->is_db_live = 1;

					if(isset($data['price_definition'])){
						$element_options[$i]->price_definition = $data['price_definition'];
					}

					$i++;
				}
			}
			if(!empty($element_options)){
				$element_data->options 	= $element_options;
			}else{
				$element_data->options 	= '';
			}
			$element_data->title 		= nl2br($row['element_title']);
			$element_data->guidelines 	= $row['element_guidelines'];
			$element_data->size 			= $row['element_size'];
			$element_data->is_required 	= $row['element_is_required'];
			$element_data->is_unique 	= $row['element_is_unique'];
			$element_data->is_private 	= $row['element_is_private'];
			$element_data->type 			= $row['element_type'];
			$element_data->position 		= $row['element_position'];
			$element_data->id 			= $row['element_id'];
			$element_data->is_db_live 	= 1;
			$element_data->form_id 		= $form_id;
			$element_data->choice_has_other   = (int) $row['element_choice_has_other'];
			$element_data->choice_other_label = $row['element_choice_other_label'];
			$element_data->choice_columns   	 = (int) $row['element_choice_columns'];
			$element_data->time_showsecond    = (int) $row['element_time_showsecond'];
			$element_data->time_24hour    	 = (int) $row['element_time_24hour'];
			$element_data->address_hideline2	 = (int) $row['element_address_hideline2'];
			$element_data->address_us_only	 = (int) $row['element_address_us_only'];
			$element_data->date_enable_range	 = (int) $row['element_date_enable_range'];
			$element_data->date_range_min	 = $row['element_date_range_min'];
			$element_data->date_range_max	 = $row['element_date_range_max'];
			$element_data->date_enable_selection_limit	 = (int) $row['element_date_enable_selection_limit'];
			$element_data->date_selection_max	 		 = (int) $row['element_date_selection_max'];
			$element_data->date_disable_past_future	 	= (int) $row['element_date_disable_past_future'];
			$element_data->date_past_future	 			= $row['element_date_past_future'];
			$element_data->date_disable_weekend	 		= (int) $row['element_date_disable_weekend'];
			$element_data->date_disable_specific	 		= (int) $row['element_date_disable_specific'];
			$element_data->date_disabled_list	 		= $row['element_date_disabled_list'];
			$element_data->file_enable_type_limit	 	= (int) $row['element_file_enable_type_limit'];
			$element_data->file_block_or_allow	 		= $row['element_file_block_or_allow'];
			$element_data->file_type_list	 			= $row['element_file_type_list'];
			$element_data->file_as_attachment	 		= (int) $row['element_file_as_attachment'];
			$element_data->file_enable_advance	 		= (int) $row['element_file_enable_advance'];
			$element_data->table_name	 		= $row['element_table_name'];
			$element_data->field_value	 		= $row['element_field_value'];
			$element_data->field_error_message	 		= $row['element_field_error_message'];
			$element_data->field_name	 		= $row['element_field_name'];
			$element_data->option_query	 		= $row['element_option_query'];
			$element_data->existing_form	 		= $row['element_existing_form'];
			$element_data->existing_stage	 		= $row['element_existing_stage'];
			$element_data->file_auto_upload	 			= (int) $row['element_file_auto_upload'];
			$element_data->file_enable_multi_upload	 	= (int) $row['element_file_enable_multi_upload'];
			$element_data->file_max_selection	 		= (int) $row['element_file_max_selection'];
			$element_data->file_enable_size_limit	 	= (int) $row['element_file_enable_size_limit'];
			$element_data->file_size_max	 				= (int) $row['element_file_size_max'];
			$element_data->matrix_allow_multiselect	 	= (int) $row['element_matrix_allow_multiselect'];
			$element_data->matrix_parent_id	 			= (int) $row['element_matrix_parent_id'];
			$element_data->upload_dir	 				= $mf_settings['upload_dir'];
			$element_data->range_min	 					= $row['element_range_min'];
			$element_data->range_max	 					= $row['element_range_max'];
			$element_data->range_limit_by	 			= $row['element_range_limit_by'];
			$element_data->jsondef	 					= $row['element_jsondef'];
			$element_data->css_class	 					= $row['element_css_class'];
			$element_data->machform_path	 				= $machform_path;
			$element_data->machform_data_path	 		= $machform_data_path;
			$element_data->section_display_in_email	 	= (int) $row['element_section_display_in_email'];
			$element_data->section_enable_scroll	 		= (int) $row['element_section_enable_scroll'];
			$form_html .= call_user_func('mf_display_'.$element_data->type,$element_data);
		}
		
		return $form_html;
	}
	/* OTB - End Form builder useful functions */

	//generate the javascript code for conditional logic
	public function mf_get_dependency_logic_javascript($dbh,$form_id,$page_number){

		$form_id = (int) $form_id;
		
		//Start get permitted values
		$query = "SELECT 
					A.id,
					A.logic_id,
					A.permitted_value,
					A.element_id,
					A.element_name,
					B.element_title,
					B.element_page_number 
				FROM 
					".MF_TABLE_PREFIX."dependency_logic_rule_permitted_values A LEFT JOIN ".MF_TABLE_PREFIX."form_elements B
				  ON 
				  	A.form_id = B.form_id and A.element_id=B.element_id and B.element_status = 1
			   WHERE
					A.form_id = ?";
		$query .= $page_number ? " and B.element_page_number = ?" : "";
		$query .= "ORDER BY 
					B.element_position asc";

		$params = $page_number ? array($form_id,$page_number) : array($form_id);
		$sth = mf_do_query($query,$params,$dbh);
		while($row = mf_do_fetch_result($sth)){
			$element_id = (int) $row['element_id'];
			$permitted_value_id = (int) $row['id'];

			$logic_permitted_values_array[$permitted_value_id]['element_id']  = $element_id;
			$logic_permitted_values_array[$permitted_value_id]['element_name']  = $row['element_name'];
			$logic_permitted_values_array[$permitted_value_id]['logic_id']  = $row['logic_id'];
			$logic_permitted_values_array[$permitted_value_id]['permitted_value']   = $row['permitted_value'];
		}
		//End get permitted values

		//get the target elements for the current page
		$query = "SELECT 
					A.id,
					A.element_id,
					A.rule_all_any,
					A.integration_logic,
					B.element_title,
					B.element_page_number,
					B.element_type
				FROM 
					".MF_TABLE_PREFIX."dependency_logic A LEFT JOIN ".MF_TABLE_PREFIX."form_elements B
				  ON 
				  	A.form_id = B.form_id and A.element_id=B.element_id and B.element_status = 1
			   WHERE
					A.form_id = ?";
		$query .= $page_number ? " and B.element_page_number = ?" : "";
		$query .= "ORDER BY 
					B.element_position asc";

		$params = $page_number ? array($form_id,$page_number) : array($form_id);
		$sth = mf_do_query($query,$params,$dbh);

		$logic_elements_array = array();
		
		while($row = mf_do_fetch_result($sth)){
			//$element_id = (int) $row['element_id'];
			$id = (int) $row['id'];

			$logic_elements_array[$id]['element_id']  = $row['element_id'];
			$logic_elements_array[$id]['integration_logic']  = $row['integration_logic'];
			$logic_elements_array[$id]['element_title']  = $row['element_title'];
			$logic_elements_array[$id]['rule_all_any']   = $row['rule_all_any'];
			$logic_elements_array[$id]['element_type']   = $row['element_type'];
		}

		//get the conditions array
		$query = "SELECT 
						A.target_element_id,
						A.element_name,
						A.rule_condition,
						A.rule_keyword,
						A.rule_keyword_option_id,
						A.logic_id,
						trim(leading 'element_' from substring_index(A.element_name,'_',2)) as condition_element_id,
						(select 
							   B.element_page_number 
						   from 
						   	   ".MF_TABLE_PREFIX."form_elements B 
						  where 
						  		form_id=A.form_id and 
						  		element_id=condition_element_id
						) condition_element_page_number,
						(select 
							   C.element_type 
						   from 
						   	   ".MF_TABLE_PREFIX."form_elements C 
						  where 
						  		form_id=A.form_id and 
						  		element_id=condition_element_id
						) condition_element_type
					FROM 
						".MF_TABLE_PREFIX."dependency_logic_conditions A 
				   WHERE
						A.form_id = ?
				   ORDER by A.id";
		$params = array($form_id);
		$sth = mf_do_query($query,$params,$dbh);
		
		$logic_conditions_array = array();
		$prev_element_id = 0;

		$i=0;
		while($row = mf_do_fetch_result($sth)){
			$target_element_id = (int) $row['target_element_id'];
			
			if($target_element_id != $prev_element_id){
				$i=0;
			}

			$logic_conditions_array[$target_element_id][$i]['element_name']  		= $row['element_name'];
			$logic_conditions_array[$target_element_id][$i]['element_type']  		= $row['condition_element_type'];
			$logic_conditions_array[$target_element_id][$i]['element_page_number'] 	= (int) $row['condition_element_page_number'];
			$logic_conditions_array[$target_element_id][$i]['rule_condition'] 		= $row['rule_condition'];
			$logic_conditions_array[$target_element_id][$i]['rule_keyword']   		= $row['rule_keyword'];
			$logic_conditions_array[$target_element_id][$i]['rule_keyword_option_id'] = $row['rule_keyword_option_id'];
			$logic_conditions_array[$target_element_id][$i]['logic_id']   		= $row['logic_id'];


			$prev_element_id = $target_element_id;
			$i++;
		}

		//build mf_handler_xx() function for each element
		$mf_handler_code = '';
		$mf_bind_code = '';
		$mf_initialize_code = '';
		/*Start - include icontains function in dynamically generated javascript, so that it can be used to compare option text against string e.g. for integration, comparing local values against remote e.g. sector, cell, village for rwanda lais integration.*/
		$mf_initialize_code .= "jQuery.expr[':'].icontains = function(a, i, m) {";
		$mf_initialize_code .= "return jQuery(a).text().toUpperCase()";
		$mf_initialize_code .= ".indexOf(m[3].toUpperCase()) >= 0;";
		$mf_initialize_code .= "};";
		/*End - include icontains function in dynamically generated javascript, so that it can be used to compare option text against string e.g. for integration, comparing local values against remote e.g. sector, cell, village for rwanda lais integration.*/

		foreach ($logic_elements_array as $logic_id => $value) {
			$rule_all_any   = $value['rule_all_any'];
			$element_id   = $value['element_id'];
			$integration_logic   = $value['integration_logic'];
			$target_element_type   = $value['element_type'];

			$current_handler_conditions_array = array();

			$mf_handler_code .= "\n"."function mf_handler_{$element_id}_{$logic_id}(initialization){"."\n"; //start mf_handler_xx
			/************************************************************************************/
			
			$target_element_conditions = $logic_conditions_array[$element_id];

			$unique_field_suffix_id = 0;

			//initialize the condition status for any other elements which is not within this page
			//store the status into a variable
			foreach ($target_element_conditions as $value) {
				if(($value['element_page_number'] == $page_number) or !$page_number){
					continue;
				}
				if($value['logic_id'] != $logic_id){
					continue;
				}

				$unique_field_suffix_id++;

				$current_handler_conditions_array[] = 'condition_'.$value['element_name'].'_'.$unique_field_suffix_id;

				$condition_params = array();
				$condition_params['form_id']		= $form_id;
				$condition_params['element_name'] 	= $value['element_name'];
				$condition_params['rule_condition'] = $value['rule_condition'];
				$condition_params['rule_keyword'] 	= $value['rule_keyword'];
				$condition_params['rule_keyword_option_id'] 	= $value['rule_keyword_option_id'];
				$condition_params['logic_id'] 	= $value['logic_id'];

				$condition_status = mf_get_condition_status_from_table($dbh,$condition_params);
				
				if($condition_status === true){
					$condition_status_value = 'true';
				}else{
					$condition_status_value = 'false';
				}

				$mf_handler_code .= "\t"."var condition_{$value['element_name']}_{$unique_field_suffix_id} = {$condition_status_value};"."\n";
			}

			$mf_handler_code .= "\n";

			//build the conditions for the current element
			foreach ($target_element_conditions as $value) {
				$unique_field_suffix_id++;

				//skip field which doesn't belong current page
				//if($value['element_page_number'] != $page_number){
				if($page_number and ($value['element_page_number'] != $page_number)){
					continue;
				}

				if($value['logic_id'] != $logic_id){
					continue;
				}
				//for checkbox with other, we need to replace the id
				if($value['element_type'] == 'checkbox'){
					$checkbox_has_other = false;

					if(substr($value['element_name'], -5) == 'other'){
						$value['element_name'] = str_replace('_other', '_0', $value['element_name']);
						$checkbox_has_other = true;
					}
				}

				

				$condition_params = array();
				$condition_params['form_id']		= $form_id;
				$condition_params['element_name'] 	= $value['element_name'];
				$condition_params['rule_condition'] = $value['rule_condition'];
				$condition_params['rule_keyword'] 	= $value['rule_keyword'];
				$condition_params['rule_keyword_option_id'] = $value['rule_keyword_option_id'];

				
				//we need to add unique suffix into the element name
				//so that we can use the same field multiple times to build a rule
				$condition_params['element_name'] = $value['element_name'].'_'.$unique_field_suffix_id;
				$current_handler_conditions_array[] = 'condition_'.$value['element_name'].'_'.$unique_field_suffix_id;
				

				$mf_handler_code .= $this->mf_get_condition_javascript_with_select_option_id($dbh,$condition_params);

				//build the bind code
				if($value['element_type'] == 'radio'){
					$mf_bind_code .= "\$('input[name={$value['element_name']}]').bind('change click', function() {\n";
				}else if($value['element_type'] == 'time'){
					$mf_bind_code .= "\$('#{$value['element_name']}_1,#{$value['element_name']}_2,#{$value['element_name']}_3,#{$value['element_name']}_4').bind('keyup mouseout change click', function() {\n";
				}else if($value['element_type'] == 'money'){
					$mf_bind_code .= "\$('#{$value['element_name']},#{$value['element_name']}_1,#{$value['element_name']}_2').bind('keyup mouseout change click', function() {\n";
				}else if($value['element_type'] == 'matrix'){
					
					$exploded = array();
					$exploded = explode('_',$value['element_name']);

					$matrix_element_id = (int) $exploded[1];
					//we only need to bind the event to the parent element_id of the matrix
					$query = "select element_matrix_parent_id from ".MF_TABLE_PREFIX."form_elements where element_id = ? and form_id = ? and element_status = 1";
					
					$params = array($matrix_element_id,$form_id);
					$sth 	= mf_do_query($query,$params,$dbh);
					$row 	= mf_do_fetch_result($sth);
					if(!empty($row['element_matrix_parent_id'])){
						$matrix_element_id = $row['element_matrix_parent_id'];
					}

					
					$mf_bind_code .= "\$('#li_{$matrix_element_id} :input').bind('change click', function() {\n";
				}else if($value['element_type'] == 'checkbox'){
					if($checkbox_has_other){
						$exploded = array();
						$exploded = explode('_', $value['element_name']);

						$mf_bind_code .= "\$('#{$value['element_name']},#element_{$exploded[1]}_other').bind('keyup mouseout change click', function() {\n";
					}else{
						$mf_bind_code .= "\$('#{$value['element_name']}').bind('keyup mouseout change click', function() {\n";
					}
				}else if($value['element_type'] == 'select'){
					$mf_bind_code .= "\$('#{$value['element_name']}').bind('keyup change', function() {\n";
				}else if($value['element_type'] == 'date' || $value['element_type'] == 'europe_date'){
					$mf_bind_code .= "\$('#{$value['element_name']}_1,#{$value['element_name']}_2,#{$value['element_name']}_3').bind('keyup mouseout change click', function() {\n";
				}else if($value['element_type'] == 'phone'){
					$mf_bind_code .= "\$('#{$value['element_name']}_1,#{$value['element_name']}_2,#{$value['element_name']}_3').bind('keyup mouseout change click', function() {\n";
				}else{
					$mf_bind_code .= "\$('#{$value['element_name']}').bind('keyup mouseout change click', function() {\n";
				}

				$mf_bind_code .= "\tmf_handler_{$element_id}_{$logic_id}();\n";
				$mf_bind_code .= "});\n";
			}
			
			//evaluate all conditions
			if($rule_all_any == 'all'){
				$logic_operator = ' && ';
			}else{
				$logic_operator = ' || ';
			}

			/*if($rule_show_hide == 'show'){
				$action_code_primary 	= "\$('#li_{$element_id}').show();";
				$action_code_secondary  = "\$('#li_{$element_id}').hide();";
			}else if($rule_show_hide == 'hide'){
				$action_code_primary 	= "\$('#li_{$element_id}').hide();";
				$action_code_secondary  = "\$('#li_{$element_id}').show();";
			}*/

			$mf_handler_code .= "";
			$action_code_primary = "";//Reset primary code for the next logic rule
			$action_code_secondary = "";//Reset primary code for the next logic rule
			//build javascript for permitted values
			if(count($logic_permitted_values_array > 0 and !$integration_logic)){
				if($target_element_type == 'select'){
					$action_code_primary .= "\$('#element_{$element_id} > option').each(function(){\n";
					$action_code_primary .= "\$(this).hide();\n";
					$action_code_primary .= "});\n";
				}
			}

			foreach($logic_permitted_values_array as $permitted){
				if($permitted['element_id'] == $element_id and $permitted['logic_id'] == $logic_id and !in_array($element_id, array_column($target_element_conditions, 'element_name')) and !$integration_logic){
					if($target_element_type == 'select'){
						$action_code_primary .= "if(initialization !== true){\n";//only reset if not initialization
						$action_code_primary .= "\$('#element_{$element_id}').val('');\n";//reset dependency field when value of field it depends on is changed so user can select permitted values
						$action_code_primary .= "}\n";
						$action_code_primary .= "\$('#element_{$element_id} option[value=\"".$permitted['permitted_value']."\"]').show();\n";
					}
				}
			}
			
			if($current_handler_conditions_array){//Only include conditions if they exist
				$current_handler_conditions_joined = implode($logic_operator, $current_handler_conditions_array);
				$mf_handler_code .= "\tif({$current_handler_conditions_joined}){\n";
				$mf_handler_code .= "\t\t{$action_code_primary}\n";
				$mf_handler_code .= "\t}else{\n";
				$mf_handler_code .= "\t\t{$action_code_secondary}\n";
				$mf_handler_code .= "\t}\n\n";
			}
			//postMessage to adjust the height of the iframe
			$mf_handler_code .= "\tif($(\"html\").hasClass(\"embed\")){\n";
			$mf_handler_code .= "\t\t$.postMessage({mf_iframe_height: $('body').outerHeight(true)}, '*', parent );\n";
			$mf_handler_code .=	"\t}\n";
			
			if($integration_logic){
				//Get fields to be populated
				$mf_handler_code .= $this->mf_get_integration_javascript($dbh, $form_id, $element_id, $logic_id, $page_number);//Include dependency code for integration
				$mf_bind_code .= "\$('#element_{$element_id}').focusout(function() {\n";
				$mf_bind_code .= "\tmf_handler_{$element_id}_{$logic_id}();\n";
				$mf_bind_code .= "});\n";
			}
			/************************************************************************************/
			$mf_handler_code .= "}"."\n"; //end mf_handler_xx

			$mf_initialize_code .= "mf_handler_{$element_id}_{$logic_id}(true);\n";
		}


		$javascript_code = <<<EOT
<script type="text/javascript">
$(function(){

{$mf_handler_code}
{$mf_bind_code}
{$mf_initialize_code}

});
</script>
EOT;

		return $javascript_code;
	}

	public function mf_get_condition_javascript_with_select_option_id($dbh,$condition_params){
		
		$form_id 		= (int) $condition_params['form_id'];
		$element_name 	= $condition_params['element_name']; //this could be 'element_x_y' or 'element_x_x_y', where 'y' is just unique field suffix
		$rule_condition = $condition_params['rule_condition'];
		
		if(function_exists('mb_strtolower')){
			$rule_keyword 	= addslashes(mb_strtolower($condition_params['rule_keyword'],'UTF-8')); //keyword is case insensitive
		}else{
			$rule_keyword 	= addslashes(strtolower($condition_params['rule_keyword'])); //keyword is case insensitive
		}
		$rule_keyword_option_id = $condition_params['rule_keyword_option_id'];//Include option id for select fields

		$exploded = explode('_', $element_name);
		$element_id = (int) $exploded[1];

		//get the element properties of the current element id
		$query 	= "select 
						 element_type,
						 element_time_showsecond,
						 element_time_24hour,
						 element_constraint,
						 element_matrix_parent_id,
						 element_matrix_allow_multiselect 
					 from 
					 	 ".MF_TABLE_PREFIX."form_elements 
					where 
						 form_id = ? and element_id = ?";
		$params = array($form_id,$element_id);
		$sth 	= mf_do_query($query,$params,$dbh);
		$row 	= mf_do_fetch_result($sth);

		$element_type 			  = $row['element_type'];
		$element_time_showsecond  = (int) $row['element_time_showsecond'];
		$element_time_24hour	  = (int) $row['element_time_24hour'];
		$element_constraint		  = $row['element_constraint'];
		$element_matrix_parent_id = (int) $row['element_matrix_parent_id'];
		$element_matrix_allow_multiselect = (int) $row['element_matrix_allow_multiselect'];

		//if this is matrix field, we need to determine wether this is matrix choice or matrix checkboxes
		if($element_type == 'matrix'){
			if(empty($element_matrix_parent_id)){
				if(!empty($element_matrix_allow_multiselect)){
					$element_type = 'matrix_checkbox';
				}else{
					$element_type = 'matrix_radio';
				}
			}else{
				//this is a child row of a matrix, get the parent id first and check the status of the multiselect option
				$query = "select element_matrix_allow_multiselect from ".MF_TABLE_PREFIX."form_elements where form_id = ? and element_id = ?";
				$params = array($form_id,$element_matrix_parent_id);
				$sth 	= mf_do_query($query,$params,$dbh);
				$row 	= mf_do_fetch_result($sth);

				if(!empty($row['element_matrix_allow_multiselect'])){
					$element_type = 'matrix_checkbox';
				}else{
					$element_type = 'matrix_radio';
				}
			}
		}


		$condition_javascript = '';
		
		$exploded = array();
		$exploded = explode('_', $element_name);

		if(count($exploded) == 3){
			$element_name_clean = 'element_'.$exploded[1]; //element name, without the unique suffix
		}else{
			$element_name_clean = 'element_'.$exploded[1].'_'.$exploded[2]; //element name, without the unique suffix
		}
		
		if(in_array($element_type, array('text','textarea','simple_name','name','simple_name_wmiddle','name_wmiddle','address','simple_phone','url','email'))){
			$condition_javascript .= "var {$element_name} = \$('#{$element_name_clean}').val().toLowerCase();"."\n";

			if($rule_condition == 'is'){
				$condition_javascript .= "\tif({$element_name} == '{$rule_keyword}'){"."\n";	
			}else if($rule_condition == 'is_not'){
				$condition_javascript .= "\tif({$element_name} != '{$rule_keyword}'){"."\n";
			}else if($rule_condition == 'begins_with'){
				$condition_javascript .= "\tif({$element_name}.indexOf('{$rule_keyword}') == 0){"."\n";
			}else if($rule_condition == 'ends_with'){
				$condition_javascript .= "\tvar keyword_{$element_name} = '{$rule_keyword}';\n";
				$condition_javascript .= "\tif({$element_name}.indexOf(keyword_{$element_name},{$element_name}.length - keyword_{$element_name}.length) != -1){"."\n";
			}else if($rule_condition == 'contains'){
				$condition_javascript .= "\tif({$element_name}.indexOf('{$rule_keyword}') >= 0){"."\n";
			}else if($rule_condition == 'not_contain'){
				$condition_javascript .= "\tif({$element_name}.indexOf('{$rule_keyword}') == -1){"."\n";
			}

			$condition_javascript .= "\t\tvar condition_{$element_name} = true;\n\t}else{\n\t\tvar condition_{$element_name} = false; \n\t}\n";

		}else if($element_type == 'radio'){
			$condition_javascript .= "var {$element_name} = \$('input[name={$element_name_clean}]:checked').next().text().toLowerCase();"."\n";

			if($rule_condition == 'is'){
				$condition_javascript .= "\tif({$element_name} == '{$rule_keyword}'){"."\n";	
			}else if($rule_condition == 'is_not'){
				$condition_javascript .= "\tif({$element_name} != '{$rule_keyword}'){"."\n";
			}else if($rule_condition == 'begins_with'){
				$condition_javascript .= "\tif({$element_name}.indexOf('{$rule_keyword}') == 0){"."\n";
			}else if($rule_condition == 'ends_with'){
				$condition_javascript .= "\tvar keyword_{$element_name} = '{$rule_keyword}';\n";
				$condition_javascript .= "\tif({$element_name}.indexOf(keyword_{$element_name},{$element_name}.length - keyword_{$element_name}.length) != -1){"."\n";
			}else if($rule_condition == 'contains'){
				$condition_javascript .= "\tif({$element_name}.indexOf('{$rule_keyword}') >= 0){"."\n";
			}else if($rule_condition == 'not_contain'){
				$condition_javascript .= "\tif({$element_name}.indexOf('{$rule_keyword}') == -1){"."\n";
			}

			$condition_javascript .= "\t\tvar condition_{$element_name} = true;\n\t}else{\n\t\tvar condition_{$element_name} = false; \n\t}\n";
		}else if($element_type == 'time'){
			
			//there are few variants of the the time field, get the specific type
			if(!empty($element_time_showsecond) && !empty($element_time_24hour)){
				$element_type = 'time_showsecond24hour';
			}else if(!empty($element_time_showsecond) && empty($element_time_24hour)){
				$element_type = 'time_showsecond';
			}else if(empty($element_time_showsecond) && !empty($element_time_24hour)){
				$element_type = 'time_24hour';
			}

			$exploded = array();
			$exploded = explode(':', $rule_keyword); //rule keyword format -> HH:MM:SS:AM

			if($element_type == 'time'){
				$time_keyword 		   = "Date.parse('01/01/2012 {$exploded[0]}:{$exploded[1]} {$exploded[3]}')";
				$condition_javascript .= "var {$element_name}_timestring = $('#{$element_name_clean}_1').val() + ':' + $('#{$element_name_clean}_2').val() + ' ' + $('#{$element_name_clean}_4').val();\n";
			}else if($element_type == 'time_showsecond'){
				$time_keyword 		   = "Date.parse('01/01/2012 {$exploded[0]}:{$exploded[1]}:{$exploded[2]} {$exploded[3]}')";
				$condition_javascript .= "var {$element_name}_timestring = $('#{$element_name_clean}_1').val() + ':' + $('#{$element_name_clean}_2').val() + ':' + $('#{$element_name_clean}_3').val() + ' ' + $('#{$element_name_clean}_4').val();\n";
			}else if($element_type == 'time_24hour'){
				$time_keyword 		   = "Date.parse('01/01/2012 {$exploded[0]}:{$exploded[1]}')";
				$condition_javascript .= "var {$element_name}_timestring = $('#{$element_name_clean}_1').val() + ':' + $('#{$element_name_clean}_2').val();\n";
			}else if($element_type == 'time_showsecond24hour'){
				$time_keyword 		   = "Date.parse('01/01/2012 {$exploded[0]}:{$exploded[1]}:{$exploded[2]}')";
				$condition_javascript .= "var {$element_name}_timestring = $('#{$element_name_clean}_1').val() + ':' + $('#{$element_name_clean}_2').val() + ':' + $('#{$element_name_clean}_3').val();\n";
			}

			$condition_javascript .= "\tvar {$element_name} = Date.parse('01/01/2012 ' + {$element_name}_timestring);\n\n"; 

			if($rule_condition == 'is'){
				$condition_javascript .= "\tif({$time_keyword} == {$element_name}){"."\n";	
			}else if($rule_condition == 'is_before'){
				$condition_javascript .= "\tif({$time_keyword} > {$element_name}){"."\n";	
			}else if($rule_condition == 'is_after'){
				$condition_javascript .= "\tif({$time_keyword} < {$element_name}){"."\n";	
			}

			$condition_javascript .= "\t\tvar condition_{$element_name} = true;\n\t}else{\n\t\tvar condition_{$element_name} = false; \n\t}\n";
		}else if($element_type == 'money'){

			if($element_constraint == 'yen'){ //yen only have one field
				$condition_javascript .= "var {$element_name} = \$('#{$element_name_clean}').val();"."\n";
			}else{
				$condition_javascript .= "var {$element_name} = \$('#{$element_name_clean}_1').val() + '.' + \$('#{$element_name_clean}_2').val();"."\n";
			}
			

			if($rule_condition == 'is'){
				$condition_javascript .= "\tif(parseFloat({$element_name}) == parseFloat('{$rule_keyword}')){"."\n";	
			}else if($rule_condition == 'less_than'){
				$condition_javascript .= "\tif(parseFloat({$element_name}) < parseFloat('{$rule_keyword}')){"."\n";
			}else if($rule_condition == 'greater_than'){
				$condition_javascript .= "\tif(parseFloat({$element_name}) > parseFloat('{$rule_keyword}')){"."\n";
			}

			$condition_javascript .= "\t\tvar condition_{$element_name} = true;\n\t}else{\n\t\tvar condition_{$element_name} = false; \n\t}\n";

		}else if($element_type == 'matrix_radio'){

			$condition_javascript .= "var selected_choice_{$element_id} = \$('#mr_{$element_id} input[name=element_{$element_id}]:checked').index('#mr_{$element_id} input[name=element_{$element_id}]') + 1;\n";
			$condition_javascript .= "\tvar {$element_name} = \$('#mr_{$element_id}').parentsUntil('li').filter('table').children('thead').children().children().eq(selected_choice_{$element_id}).text().toLowerCase();\n\n";

			if($rule_condition == 'is'){
				$condition_javascript .= "\tif({$element_name} == '{$rule_keyword}'){"."\n";	
			}else if($rule_condition == 'is_not'){
				$condition_javascript .= "\tif({$element_name} != '{$rule_keyword}'){"."\n";
			}else if($rule_condition == 'begins_with'){
				$condition_javascript .= "\tif({$element_name}.indexOf('{$rule_keyword}') == 0){"."\n";
			}else if($rule_condition == 'ends_with'){
				$condition_javascript .= "\tvar keyword_{$element_name} = '{$rule_keyword}';\n";
				$condition_javascript .= "\tif({$element_name}.indexOf(keyword_{$element_name},{$element_name}.length - keyword_{$element_name}.length) != -1){"."\n";
			}else if($rule_condition == 'contains'){
				$condition_javascript .= "\tif({$element_name}.indexOf('{$rule_keyword}') >= 0){"."\n";
			}else if($rule_condition == 'not_contain'){
				$condition_javascript .= "\tif({$element_name}.indexOf('{$rule_keyword}') == -1){"."\n";
			}

			$condition_javascript .= "\t\tvar condition_{$element_name} = true;\n\t}else{\n\t\tvar condition_{$element_name} = false; \n\t}\n";
		}else if($element_type == 'matrix_checkbox' || $element_type == 'checkbox'){

			$condition_javascript .= "var {$element_name} = \$('#{$element_name_clean}').prop('checked');"."\n";

			if($rule_condition == 'is_one'){
				$condition_javascript .= "\tif({$element_name} == true){"."\n";	
			}else if($rule_condition == 'is_zero'){
				$condition_javascript .= "\tif({$element_name} == false){"."\n";
			}

			$condition_javascript .= "\t\tvar condition_{$element_name} = true;\n\t}else{\n\t\tvar condition_{$element_name} = false; \n\t}\n";
		}else if($element_type == 'number'){

			$condition_javascript .= "var {$element_name} = \$('#{$element_name_clean}').val();"."\n";
			
			if($rule_condition == 'is'){
				$condition_javascript .= "\tif(parseFloat({$element_name}) == parseFloat('{$rule_keyword}')){"."\n";	
			}else if($rule_condition == 'less_than'){
				$condition_javascript .= "\tif(parseFloat({$element_name}) < parseFloat('{$rule_keyword}')){"."\n";
			}else if($rule_condition == 'greater_than'){
				$condition_javascript .= "\tif(parseFloat({$element_name}) > parseFloat('{$rule_keyword}')){"."\n";
			}

			$condition_javascript .= "\t\tvar condition_{$element_name} = true;\n\t}else{\n\t\tvar condition_{$element_name} = false; \n\t}\n";

		}else if($element_type == 'select'){
			$condition_javascript .= "var {$element_name} = \$('#{$element_name_clean} option:selected').text().toLowerCase();"."\n";
			if ($rule_keyword_option_id){
				$condition_javascript .= "var {$element_name}_option_id_set = true;\n";
			}else{
				$condition_javascript .= "var {$element_name}_option_id_set = false;\n";
			}
			$condition_javascript .= "var {$element_name}_option_id = \$('#{$element_name_clean} option:selected').val().toLowerCase();"."\n";

			if($rule_condition == 'is'){
				$condition_javascript .= "\tif({$element_name} == '{$rule_keyword}' && (!{$element_name}_option_id_set || {$element_name}_option_id == '{$rule_keyword_option_id}')){"."\n";	
			}else if($rule_condition == 'is_not'){
				$condition_javascript .= "\tif({$element_name} != '{$rule_keyword}' && (!{$element_name}_option_id_set || {$element_name}_option_id == '{$rule_keyword_option_id}')){"."\n";
			}else if($rule_condition == 'begins_with'){
				$condition_javascript .= "\tif({$element_name}.indexOf('{$rule_keyword}') == 0 && (!{$element_name}_option_id_set || {$element_name}_option_id == '{$rule_keyword_option_id}')){"."\n";
			}else if($rule_condition == 'ends_with'){
				$condition_javascript .= "\tvar keyword_{$element_name} = '{$rule_keyword}';\n";
				$condition_javascript .= "\tif({$element_name}.indexOf(keyword_{$element_name},{$element_name}.length - keyword_{$element_name}.length) != -1 && (!{$element_name}_option_id_set || {$element_name}_option_id == '{$rule_keyword_option_id}')){"."\n";
			}else if($rule_condition == 'contains'){
				$condition_javascript .= "\tif({$element_name}.indexOf('{$rule_keyword}') >= 0 && (!{$element_name}_option_id_set || {$element_name}_option_id == '{$rule_keyword_option_id}')){"."\n";
			}else if($rule_condition == 'not_contain'){
				$condition_javascript .= "\tif({$element_name}.indexOf('{$rule_keyword}') == -1 && (!{$element_name}_option_id_set || {$element_name}_option_id == '{$rule_keyword_option_id}')){"."\n";
			}

			$condition_javascript .= "\t\tvar condition_{$element_name} = true;\n\t}else{\n\t\tvar condition_{$element_name} = false; \n\t}\n";

		}else if($element_type == 'date' || $element_type == 'europe_date'){
			
			$date_keyword 		   = "Date.parse('{$rule_keyword}')";

			if($element_type == 'date'){
				$condition_javascript .= "var {$element_name}_datestring = $('#{$element_name_clean}_1').val() + '/' + $('#{$element_name_clean}_2').val() + '/' + $('#{$element_name_clean}_3').val();\n";
			}else if ($element_type == 'europe_date') {
				$condition_javascript .= "var {$element_name}_datestring = $('#{$element_name_clean}_2').val() + '/' + $('#{$element_name_clean}_1').val() + '/' + $('#{$element_name_clean}_3').val();\n";
			}
			$condition_javascript .= "\tvar {$element_name} = Date.parse({$element_name}_datestring);\n\n"; 

			if($rule_condition == 'is'){
				$condition_javascript .= "\tif({$date_keyword} == {$element_name}){"."\n";	
			}else if($rule_condition == 'is_before'){
				$condition_javascript .= "\tif({$date_keyword} > {$element_name}){"."\n";	
			}else if($rule_condition == 'is_after'){
				$condition_javascript .= "\tif({$date_keyword} < {$element_name}){"."\n";	
			}

			$condition_javascript .= "\t\tvar condition_{$element_name} = true;\n\t}else{\n\t\tvar condition_{$element_name} = false; \n\t}\n";
		}else if($element_type == 'phone'){
			$condition_javascript .= "var {$element_name} = \$('#{$element_name_clean}_1').val() + \$('#{$element_name_clean}_2').val() + \$('#{$element_name_clean}_3').val();"."\n";

			if($rule_condition == 'is'){
				$condition_javascript .= "\tif({$element_name} == '{$rule_keyword}'){"."\n";	
			}else if($rule_condition == 'is_not'){
				$condition_javascript .= "\tif({$element_name} != '{$rule_keyword}'){"."\n";
			}else if($rule_condition == 'begins_with'){
				$condition_javascript .= "\tif({$element_name}.indexOf('{$rule_keyword}') == 0){"."\n";
			}else if($rule_condition == 'ends_with'){
				$condition_javascript .= "\tvar keyword_{$element_name} = '{$rule_keyword}';\n";
				$condition_javascript .= "\tif({$element_name}.indexOf(keyword_{$element_name},{$element_name}.length - keyword_{$element_name}.length) != -1){"."\n";
			}else if($rule_condition == 'contains'){
				$condition_javascript .= "\tif({$element_name}.indexOf('{$rule_keyword}') >= 0){"."\n";
			}else if($rule_condition == 'not_contain'){
				$condition_javascript .= "\tif({$element_name}.indexOf('{$rule_keyword}') == -1){"."\n";
			}

			$condition_javascript .= "\t\tvar condition_{$element_name} = true;\n\t}else{\n\t\tvar condition_{$element_name} = false; \n\t}\n";

		}

		return "\t".$condition_javascript."\n";

	}

	public function mf_get_integration_javascript($dbh, $form_id, $main_element_id, $logic_id, $page_number){
		$jscript = "var upi = $('#element_".$main_element_id."').val().toLowerCase();\n";
        $jscript .= "$.post('/index.php/jquery/checkupi/', {upi:upi}, function(plot){\n";
        $jscript .= "if ( plot == 'false' ) {\n";
        $jscript .= "alert('Please Note! Plot data associated with the UPI number supplied could not be found.');\n";
        $jscript .= "}\n";
        $jscript .= "else if ( plot && plot.return ) {\n";
        $jscript .= "alert('Please Note! The plot data has been pre-filled for your UPI');$('#upi_connexition_error').css('display','none'); $('#success_upi').css('display','block'); $('#failed_upi').css('display','none'); \n";

		foreach($this->getPermittedValues($dbh, $form_id, $logic_id, False) as $remote_populate){//Iterate permitted values for all pages
			$jscript .= "var element = $('#".$remote_populate['element_name']."');\n";
			$jscript .= "var value = '';\n";
			$jscript .= $remote_populate['permitted_value'].";\n";

			$jscript .= "if (!element.length){\n";
			$jscript .= "$('#form_".$form_id."').prepend('<input type=\'hidden\' id=\'element_".$remote_populate['element_id']."\' name=\'element_".$remote_populate['element_id']."\' value=' + value + ' />');\n";
			$jscript .= "$('#form_".$form_id."').prepend('<input type=\'hidden\' id=\'success_prefill\' name=\'success_prefill\' value=\'1\' />');\n";
			$jscript .= "}else{\n";
			$jscript .= "if (value){\n";
			$jscript .= "element.val(value);\n";
			$jscript .= "}\n";
			$jscript .= "}\n";
		}
        $jscript .= "}\n";
        $jscript .= "else if (upi) {\n";
        $jscript .= "alert('Please Note! Plot data could not be fetched for the given UPI. Please enter the data manually.');$('#upi_connexition_error').css('display','none'); $('#success_upi').css('display','none'); $('#failed_upi').css('display','block');\n";
       
        $jscript .= "}\n";
        $jscript .= "}, 'json').fail(function() {  $('#success_upi').css('display','none'); $('#upi_connexition_error').css('display','block');  $('#failed_upi').css('display','none');});\n";
		return $jscript;
	}

	public function getUPIDetails($upi, $logic_obj=False){
		
             //Check if URL is up/down - 
            /* This is causing slow load of pages - especially when user is trying to view applications or 
             * submit form when LAIS is down
             */
            if($this->urlPing("http://10.10.69.141:4740/LAISWebService/RHA?wsdl"))
            {
		try{
                    
                       $client = new SoapClient("http://10.10.69.141:4740/LAISWebService/RHA?wsdl");
		       $upi_request = new StdClass();
		       $upi_request->upi = $upi;
			$upi_details = $client->getParcel($upi_request);
			return $upi_details;
		}catch (Exception $ex){
			error_log("Could not get UPI Details >> ".$ex->getMessage());
			return False;
		}
            }else{
                error_log("Information: Lais is down!!");
                return False;
            }
           
	}
        
        public function urlPing($url=NULL)  
        {  
            if($url == NULL) return false;  
            $ch = curl_init($url);  
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);  //5 seconds 
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);  //5 seconds 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
            $data = curl_exec($ch);  
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);  
            curl_close($ch);  
            if($httpcode>=200 && $httpcode<300){  
                return true;  
            } else {  
                return false;  
            }  
        } 
       

	public function getLogicFields($dbh, $form_id,$integration=False){
		if(!$dbh){
			$prefix_folder = "vendor/cp_machform/";
			require($prefix_folder . 'includes/db-core.php');
			require($prefix_folder . 'config.php');
			$dbh = mf_connect_db();			
		}
		//get the target elements for the current page
		$query = "SELECT 
					A.id,
					A.element_id,
					A.rule_all_any,
					A.integration_logic,
					B.element_title,
					B.element_page_number,
					B.element_type
				FROM 
					".MF_TABLE_PREFIX."dependency_logic A LEFT JOIN ".MF_TABLE_PREFIX."form_elements B
				  ON 
				  	A.form_id = B.form_id and A.element_id=B.element_id and B.element_status = 1
			   WHERE
					A.form_id = ?";
		$query .= $page_number ? " and B.element_page_number = ?" : "";
		$query .= "ORDER BY 
					B.element_position asc";

		$params = $page_number ? array($form_id,$page_number) : array($form_id);

		$sth = mf_do_query($query,$params,$dbh);

		$logic_elements_array = array();
		
		while($row = mf_do_fetch_result($sth)){
			//$element_id = (int) $row['element_id'];
			$id = (int) $row['id'];
			if($row['integration_logic'] or !$integration){
				$logic_elements_array[$id]['element_id']  = $row['element_id'];
				$logic_elements_array[$id]['integration_logic']  = $row['integration_logic'];
				$logic_elements_array[$id]['element_title']  = $row['element_title'];
				$logic_elements_array[$id]['rule_all_any']   = $row['rule_all_any'];
				$logic_elements_array[$id]['element_type']   = $row['element_type'];
			}
		}
		return $logic_elements_array;
	}

	public function getPermittedValues($dbh, $form_id, $logic_id, $page_number){
		//Start get permitted values
		$query = "SELECT 
					A.id,
					A.logic_id,
					A.permitted_value,
					A.element_id,
					A.element_name,
					B.element_title,
					B.element_page_number 
				FROM 
					".MF_TABLE_PREFIX."dependency_logic_rule_permitted_values A LEFT JOIN ".MF_TABLE_PREFIX."form_elements B
				  ON 
				  	A.form_id = B.form_id and A.element_id=B.element_id and B.element_status = 1
			   WHERE
					A.form_id = ? AND A.logic_id = ?";
		$query .= $page_number ? " and B.element_page_number = ?" : "";
		$query .= " ORDER BY 
					B.element_position asc";

		$params = $page_number ? array($form_id,$logic_id,$page_number) : array($form_id,$logic_id);
		$sth = mf_do_query($query,$params,$dbh);
		while($row = mf_do_fetch_result($sth)){
			$element_id = (int) $row['element_id'];
			$permitted_value_id = (int) $row['id'];

			$logic_permitted_values_array[$permitted_value_id]['element_id']  = $element_id;
			$logic_permitted_values_array[$permitted_value_id]['element_name']  = $row['element_name'];
			$logic_permitted_values_array[$permitted_value_id]['logic_id']  = $row['logic_id'];
			$logic_permitted_values_array[$permitted_value_id]['permitted_value']   = $row['permitted_value'];
		}
		//End get permitted values
		return $logic_permitted_values_array;
	}

	public function getReviewElementValue($dbh, $form_id, $element_name, $entry_id){
		$query = "SELECT 
					A.".$element_name.",
					B.form_review
				FROM 
					".MF_TABLE_PREFIX."form_".$form_id."_review A LEFT JOIN ".MF_TABLE_PREFIX."forms B
				  ON 
				  	B.form_id = ".$form_id."
			   WHERE
					B.form_id = ? AND A.id = ? and B.form_review=1 LIMIT 1";

		$params = array($form_id,$entry_id);
		$sth = mf_do_query($query,$params,$dbh);
		$row = mf_do_fetch_result($sth);
		if ($row[$element_name]){
			return $row[$element_name];
		}else{
			return False;
		}
	}

	public function getElementValue($form_id,$entry_id,$element_id=false,$locale="en_US",$options=array()){
		$prefix_folder = "vendor/cp_machform/";
		//try{
		//	$dbh = mf_connect_db();
		//}catch (Exception $ex){
			require($prefix_folder . 'includes/db-core.php');
			require($prefix_folder . 'config.php');
			$dbh = mf_connect_db();			
		//}
		//try{
		//	$entry_details = mf_get_entry_details($dbh,$form_id,$entry_id,$options, $locale);
		//}catch (Exception $ex){
			require($prefix_folder . 'includes/entry-functions.php');
                        require($prefix_folder . 'includes/helper-functions.php');

			$entry_details = mf_get_entry_details($dbh,$form_id,$entry_id,$options, $locale);
		//}
		$element_ids = array_column($entry_details, 'element_id');
		$key = array_search($element_id, $element_ids);
		error_log("Debug: The value for element ".$element_id." >>>> ".$entry_details[$key]['value']);
		return $entry_details[$key]['value'];
	}

}
