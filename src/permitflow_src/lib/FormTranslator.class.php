<?php
/**
 *
 * Class to handle all translations for the form builder
 *
 * Created by PhpStorm.
 * User: thomasjuma
 * Date: 11/19/14
 * Time: 12:26 AM
 */

class FormTranslator {

    public $locale = null;
    public $form_id = null;

    public $form_name = null;
    public $form_description_long = null;
    public $form_description_short = null;
    public $form_success_message = null;

    //Enable logs for debugging
    private $debug = true;

    //Translation class
    private $translation = null;

    //Constructor for form translator class
    public function FormTranslator($locale, $form_id)
    {
      $this->locale = $locale;
      $this->form_id = $form_id;

      //Initialize translation class
      $this->translation = new translation();

      if($this->debug)
      {
        error_log("form_translator: Locale: ".$this->locale);
      }
    }

    public function setFormName($form_name)
    {
      $this->form_name = $form_name;
      $this->set_default_translation("form_name", $this->form_name);

      if($this->debug)
      {
        error_log("form_translator: Form Name: ".$this->form_name);
      }
    }

    public function setFormDescriptionLong($form_description)
    {
      $this->form_description_long = $form_description;
      $this->set_default_translation("form_description", $this->form_description_long);

      if($this->debug)
      {
        error_log("form_translator: Form Description Long: ".$this->form_description_long);
      }
    }

    public function setFormDescriptionShort($form_description)
    {
      $this->form_description_short = $form_description;
      $this->set_default_translation("form_redirect", $this->form_description_short);

      if($this->debug)
      {
        error_log("form_translator: Form Description Short: ".$this->form_description_short);
      }
    }

    public function setFormSuccessMessage($success_message)
    {
      $this->form_success_message = $success_message;
      $this->set_default_translation("form_success_message", $success_message);

      if($this->debug)
      {
        error_log("form_translator: Form Success Message: ".$this->form_success_message);
      }
    }

    public function set_default_translation($field_name, $field_value)
    {
        $this->translation->setTranslation("ap_forms",$field_name, $this->form_id, $field_value);

        if($this->debug)
        {
            error_log("form_translator: Form Property Translation: ".$field_name.": ".$field_value);
        }
    }

    public function set_extended_translation($field_name, $option_id, $option_value, $table_name)
    {
        $this->translation->setOptionTranslation($table_name,$field_name,$this->form_id,$option_id,$option_value);

        if($this->debug)
        {
            error_log("form_translator: Form Field Translation: ".$field_name.": ".$option_id.":".$option_value);
        }
    }

    public function dump_vars()
    {
      echo "<pre>", var_dump($this),"</pre>";
    }
}
