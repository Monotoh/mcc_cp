<?php 

class Functions {

    //Format numbers for statistics nicely by rounding off
    public static function bd_nice_number($n) {
        // first strip any formatting;
        $n = (0+str_replace(",","",$n));

        // is this a number?
        if(!is_numeric($n)) return false;

        // now filter it;
        if($n>1000000000000) return round(($n/1000000000000),1).' T';
        else if($n>1000000000) return round(($n/1000000000),1).' B';
        else if($n>1000000) return round(($n/1000000),1).' M';

        return number_format($n);
    }

    //Get the currently logged in reviewer
    public static function current_user()
    {
        $q = Doctrine_Query::create()
            ->from('CfUser a')
            ->where('a.nid = ?', sfContext::getInstance()->getUser()->getAttribute('userid'));
        return $q->fetchOne();
    }

    //Get the site settings
    public static function site_settings()
    {
        $q = Doctrine_Query::create()
            ->from("ApSettings a");
        return $q->fetchOne();
    }

    //Get number of days since a given date
    public static function get_days_since($sStartDate, $sEndDate){
        $start_ts = new DateTime($sStartDate);
        $end_ts = new DateTime($sEndDate);
        $diff = $start_ts->diff($end_ts)->d;
        return $diff;
    }

    //Get number of months since a given date
    public static function get_months_since($sStartDate, $sEndDate){
        $start_ts = new DateTime($sStartDate);
        $end_ts = new DateTime($sEndDate);
        $diff = $start_ts->diff($end_ts)->m;
        return $diff;
    }

    //Get all services that current user is allowed to access
    public static function get_allowed_services()
    {
        $allowed_services = array();

        $q = Doctrine_Query::create()
            ->from('Menus a')
            ->orderBy('a.title ASC');
        $services = $q->execute();
        foreach($services as $service)
        {
            if(sfContext::getInstance()->getUser()->mfHasCredential('accessmenu'.$service->getId()))
            {
               $allowed_services[] = $service;
            }
        }

        return $allowed_services;
    }

    //Get all allowed stages the user is allowed to access
    public static function get_allowed_stage_models($workflow_id)
    {
        $allowed_stages = array();

        $q = Doctrine_Query::create()
            ->from('SubMenus a')
            ->where('a.menu_id = ?', $workflow_id)
            ->andWhere('a.deleted = 0')
            ->orderBy('a.order_no ASC');
        $stages = $q->execute();

        foreach($stages as $stage)
        {
            if(sfContext::getInstance()->getUser()->mfHasCredential('accesssubmenu'.$stage->getId()))
            {
               $allowed_stages[] = $stage;
            }
        }

        return $allowed_stages;
    }

    //Get the first stage of a service
    public static function get_allowed_service_first_stage($service_id)
    {
        $q = Doctrine_Query::create()
            ->from('SubMenus a')
            ->where('a.menu_id = ?', $service_id)
            ->andWhere('a.deleted = 0')
            ->orderBy('a.order_no ASC');
        $stages = $q->execute();
        foreach($stages as $stage)
        {
            if(sfContext::getInstance()->getUser()->mfHasCredential('accesssubmenu'.$stage->getId()))
            {
               return $stage;
            }
        }

        //If no stages are accessible then return false
        return false;
    }

    //Get all allowed stages the user is allowed to access
    public static function get_allowed_stages()
    {
        $allowed_stages = array();

        $q = Doctrine_Query::create()
            ->from('Menus a')
            ->orderBy('a.title ASC');
        $services = $q->execute();
        foreach($services as $service)
        {
            if(sfContext::getInstance()->getUser()->mfHasCredential('accessmenu'.$service->getId()))
            {
                 $q = Doctrine_Query::create()
                    ->from('SubMenus a')
                    ->where('a.menu_id = ?', $service->getId())
                    ->andWhere('a.deleted = 0')
                    ->orderBy('a.order_no ASC');
                $stages = $q->execute();
                foreach($stages as $stage)
                {
                    if(sfContext::getInstance()->getUser()->mfHasCredential('accesssubmenu'.$stage->getId()))
                    {
                        $allowed_stages[] = $stage->getId();
                    }
                }
            }
        }

        return $allowed_stages;
    }

    //Get list of available languages
    public static function get_languages()
    {
        $q = Doctrine_Query::create()
            ->from('ExtLocales a')
            ->orderBy("a.local_title ASC");
        return $q->execute();
    }

    //Check if logged in client can create business profiles
    public static function client_can_add_businesses()
    {
        $user = sfContext::getInstance()->getUser()->getGuardUser();

        $q = Doctrine_Query::create()
            ->from('SfGuardUserCategories a')
            ->where("a.id = ?", $user->getProfile()->getRegisteras());
        $category = $q->fetchOne();

        if($category && $category->getFormid() == 0)
        {
            return false;
        }
        else 
        {
            return true;
        }
    }

    //Return the category of the client
    public static function get_client_category()
    {
        $user = sfContext::getInstance()->getUser()->getGuardUser();

        $q = Doctrine_Query::create()
            ->from('SfGuardUserCategories a')
            ->where("a.id = ?", $user->getProfile()->getRegisteras());
        $category = $q->fetchOne();

        return $category;

    }

    //Check if the client has a profile
    public static function client_has_profile()
    {
        $user = sfContext::getInstance()->getUser()->getGuardUser();

        $q = Doctrine_Query::create()
            ->from('MfUserProfile a')
            ->where("a.user_id = ?", $user->getId());
        if($q->count() > 0)
        {
            return true;
        }
        else 
        {
            return false;
        }
    }

    //Get the profile
    public static function get_client_profile($id)
    {
        $q = Doctrine_Query::create()
            ->from('MfUserProfile a')
            ->where("a.id = ?", $id);
        if($q->count() > 0)
        {
            return $q->fetchOne();
        }
        else 
        {
            return false;
        }
    }

    //Get current profile
    public static function get_current_profile()
    {
        $q = Doctrine_Query::create()
            ->from('MfUserProfile a')
            ->where("a.id = ?", sfContext::getInstance()->getUser()->getAttribute("current_profile"));
        if($q->count() > 0)
        {
            return $q->fetchOne();
        }
        else 
        {
            return false;
        }
    }

    //Find a substring in a string
    public function find($needle, $haystack)
    {
        $pos = strpos($haystack, $needle);
        if($pos === false)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public function has_accessible_forms()
    {
        $count = 0;

        $q = Doctrine_Query::create()
            ->from('FormGroups a')
            ->orderBy('a.group_name ASC');
        $groups = $q->execute();

        foreach($groups as $group)
        {
            $q = Doctrine_Query::create()
                ->from('ApForms a')
                ->where('a.form_group = ?', $group->getGroupId())
                ->andWhere('a.form_type = 1')
                ->andWhere('a.form_active = 1')
                ->orderBy('a.form_name ASC');

            foreach ($q->execute() as $form) {
                //Check if enable_categories is set, if it is then filter application forms
                if (sfConfig::get('app_enable_categories') == "yes") {
                    $q = Doctrine_Query::create()
                        ->from('sfGuardUserCategoriesForms a')
                        ->where('a.categoryid = ?', sfContext::getInstance()->getUser()->getGuardUser()->getProfile()->getRegisteras())
                        ->andWhere('a.formid = ?', $form->getFormId());
                    $category = $q->count();

                    if ($category == 0) {
                        continue;
                    } else {
                        $count++;
                    }
                } else {
                    //If form category permissions is disabled and then just display the category
                    $count++;
                }
            }
        }

        if($count > 0)
        {
            return true;
        }
        else 
        {
            return false;
        }

    }

}

?>
