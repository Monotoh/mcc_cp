<?php

/* 
 * This class will contain functions that can be called to fix
 *  missing functionality or perform a particular task 
 * Developer : boniface@otbafrica.com
 */


class Helper {
    
    /**
     * 
     * @param type $variable
     * Function to remove non-ascii characters
     * return a string with non-ascii characters
     * Warning: This function does not exhaust all characters!!!
     */
    public function removeNonASCIICharacters($variable)
    {
        $variable_string = iconv(mb_detect_encoding($variable), "ISO-8859-1//IGNORE", $variable);
        $variable_string = trim(iconv(mb_detect_encoding($variable), "ISO-8859-1", $variable), " \t\n\r\0\x0B\xA0");
        $variable_string = str_replace("\t", "", iconv(mb_detect_encoding($variable), "ISO-8859-1", $variable));
        $variable_string = str_replace("\n", "", iconv(mb_detect_encoding($variable), "ISO-8859-1", $variable));
        $variable_string = str_replace("\r", "", iconv(mb_detect_encoding($variable), "ISO-8859-1", $variable));
        $variable_string = str_replace("\0", "", iconv(mb_detect_encoding($variable), "ISO-8859-1", $variable));
        $variable_string = str_replace("\x0B", "", iconv(mb_detect_encoding($variable), "ISO-8859-1", $variable));
        $variable_string = str_replace("\xA0", "", iconv(mb_detect_encoding($variable), "ISO-8859-1", $variable)); 
        $variable_string = str_replace("'", "", iconv(mb_detect_encoding($variable), "ISO-8859-1", $variable)); 
        
        return $variable_string ;
                 
    }
    
   
}
