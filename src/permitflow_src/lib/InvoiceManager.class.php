<?php
/**
 *
 * Invoicing class that will manage the creation and modification of invoices
 *
 * Created by PhpStorm.
 * User: thomasjuma
 * Date: 11/19/14
 * Time: 12:28 AM
 */

class InvoiceManager {

    //Public constructor for the invoice manager class
    public function InvoiceManager()
    {

    }

    //output invoice to html
    public function generate_invoice_template($invoice_id, $pdf=false)
    {
        $templateparser = new TemplateParser();

        $q = Doctrine_Query::create()
            ->from('MfInvoice a')
            ->where('a.id = ?', $invoice_id)
            ->limit(1);
        $invoice = $q->fetchOne();

        //get application, if its in payment confirmation then move to submissions
        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.id = ?', $invoice->getAppId())
            ->limit(1);
        $application = $q->fetchOne();

        $q = Doctrine_Query::create()
            ->from('Invoicetemplates a')
            ->where("a.applicationform = ?", $application->getFormId())
            ->limit(1);
        $invoicetemplate = $q->fetchOne();

        $html = "<html>
        <body>
        ";

        $expired = false;
        $cancelled = false;

        $db_date_event = str_replace('/', '-', $invoice->getExpiresAt());

        $db_date_event = strtotime($db_date_event);

        if (time() > $db_date_event && !($invoice->getPaid() == "15" || $invoice->getPaid() == "2" || $invoice->getPaid() == "3"))
        {
            $expired = true;
        }

        if($invoice->getPaid() == "3")
        {
            $cancelled = true;
        }

        $invoice_content = $templateparser->parseInvoice($application->getId(), $application->getFormId(), $application->getEntryId(), $invoice->getId(), $invoicetemplate->getContent());

        if($pdf == false)
        {
            $ssl_suffix = "s";

            if (empty($_SERVER['HTTPS'])) {
                $ssl_suffix = "";
            }

            //replace src=" for images with src="http://localhost
            $invoice_content = str_replace('<img src="', '<img src="http'.$ssl_suffix.'://'.$_SERVER['HTTP_HOST'].'/', $invoice_content);
        }

        $html .= $invoice_content;

        $html .= "
        </body>
        </html>";

        return html_entity_decode($html);
    }

    //output invoice to html using the old parser
    public function generate_invoice_template_old_parser($invoice_id, $pdf=false)
    {
        $templateparser = new TemplateParser();

        $q = Doctrine_Query::create()
            ->from('MfInvoice a')
            ->where('a.id = ?', $invoice_id)
            ->limit(1);
        $invoice = $q->fetchOne();

        //get application, if its in payment confirmation then move to submissions
        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.id = ?', $invoice->getAppId())
            ->limit(1);
        $application = $q->fetchOne();

        $q = Doctrine_Query::create()
            ->from('Invoicetemplates a')
            ->where("a.applicationform = ?", $application->getFormId())
            ->limit(1);
        $invoicetemplate = $q->fetchOne();

        $html = "<html><body>";

        $expired = false;
        $cancelled = false;

        $db_date_event = str_replace('/', '-', $invoice->getExpiresAt());

        $db_date_event = strtotime($db_date_event);

        if (time() > $db_date_event && !($invoice->getPaid() == "15" || $invoice->getPaid() == "2" || $invoice->getPaid() == "3"))
        {
            $expired = true;
        }

        if($invoice->getPaid() == "3")
        {
            $cancelled = true;
        }

        $invoice_content = $templateparser->parseInvoiceOld($application->getId(), $application->getFormId(), $application->getEntryId(), $invoice->getId(), $invoicetemplate->getContent());

        if($pdf == false)
        {
            $ssl_suffix = "s";

            if (empty($_SERVER['HTTPS'])) {
                $ssl_suffix = "";
            }

            //replace src=" for images with src="http://localhost
            $invoice_content = str_replace('<img src="', '<img src="http'.$ssl_suffix.'://'.$_SERVER['HTTP_HOST'].'/', $invoice_content);
        }

        $html .= $invoice_content;

        $html .= "
			</body></html>";

        return html_entity_decode($html);
    }

    //output invoice to pdf
    public function save_to_pdf($invoice_id)
    {
        $q = Doctrine_Query::create()
            ->from('MfInvoice a')
            ->where('a.id = ?', $invoice_id)
            ->limit(1);
        $invoice = $q->fetchOne();

        $html = $this->generate_invoice_template($invoice_id, true);

        require_once(dirname(__FILE__)."/vendor/dompdf/dompdf_config.inc.php");

        $dompdf = new DOMPDF();
        error_log("DEBUG::::::::".str_replace('align"left"','',$html)); //BUG FIX: Removing the align"left" attribute because it's wrong syntax
    try{
        $dompdf->load_html( str_replace('align"left"','', $html));
        $dompdf->render();
    } catch(Exception $e) {
        error_log(">>>>>>>>>>>>DEBUG>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>".$e);
        }
        $dompdf->stream($invoice->getInvoiceNumber().".pdf");
    }

    //output invoice to pdf and save locally on the server
    public function save_to_pdf_locally($invoice_id)
    {
        $q = Doctrine_Query::create()
            ->from('MfInvoice a')
            ->where('a.id = ?', $invoice_id)
            ->limit(1);
        $invoice = $q->fetchOne();

        $html = $this->generate_invoice_template($invoice_id, true);

        require_once(dirname(__FILE__)."/vendor/dompdf/dompdf_config.inc.php");

        $dompdf = new DOMPDF();
    try{
        $dompdf->load_html( str_replace('align"left"','', $html)); //BUG FIX: Removing the align"left" attribute because it's wrong syntax
        $dompdf->render();
    } catch(Exception $e) {
        error_log(">>>>>>>>>>>>DEBUG>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>".$e);
        }

        $output = $dompdf->output();

        $filename = md5($invoice->getFormEntry()->getApplicationId()."-".date("Y-m-d H:i:s")).'.pdf';

        $q = Doctrine_Query::create()
            ->from("ApSettings a");

        $settings = $q->fetchOne();
        if($settings) {
            try {
                if (substr($settings->getUploadDir(), 1) == "/") {
                    $file_to_save = $settings . '/' . $filename;
                    error_log("Debug-p: ".$file_to_save);
                    file_put_contents($file_to_save, $output);
                } else {
                    $file_to_save = dirname(__FILE__) . "/../../html/" . $settings->getUploadDir() . '/' . $filename;
                    error_log("Debug-p: ".$file_to_save);
                    file_put_contents($file_to_save, $output);
                }
            }catch(Exception $ex)
            {
                error_log("Debug-p: ".$ex);
            }
            $filename = $settings->getUploadDirWeb().$filename;
        }

        return $filename;
    }

    //Validate request from external API
    public function api_validate_request($api_key, $api_secret)
    {
        $q = Doctrine_Query::create()
            ->from("InvoiceApiAccount a")
            ->where("a.api_key = ?", $api_key);
        $mdas = $q->count();

        if($mdas > 0)
        {
            $mda = $q->fetchOne();
            error_log("Invoice API: Valid Request From ".$mda->getMdaName()." - ".$mda->getMdaBranch());
            return true;
        }
        else
        {
            error_log("Invoice API: Bad Request For ".$api_key." - ".$api_secret);
            return false;
        }
    }

    //Validate request from external API
    public function api_fetch_mda($api_key, $api_secret)
    {
        $q = Doctrine_Query::create()
            ->from("InvoiceApiAccount a")
            ->where("a.api_key = ?", $api_key)
            ->limit(1);
        $mda = $q->fetchOne();

        return $mda;
    }
     
    /**
     * OTB patch - Get Invoice by Invoice No
     */
    public function get_invoice_by_invoice_number($invoice_no){
        $q = Doctrine_Query::create()
            ->from("MfInvoice a")
            ->where("a.invoice_number = ?", $invoice_no)
            ->orderBy("a.id DESC")
            ->limit(1);
        $invoice = $q->fetchOne();

        if ($invoice) {
            return $invoice;
        } else {
            return $q->fetchOne();
        }
    }

    //Get an invoice from the transaction id/reference no
    public function get_invoice_by_reference($reference)
    {
        $exploded = explode('/', $reference);
        $form_id  		 = (int) $exploded[0];
        $entry_id 		 = (int) $exploded[1];
        $invoice_id 		 = (int) $exploded[2];

        if($invoice_id != "1")
        {
            $q = Doctrine_Query::create()
                ->from("MfInvoice a")
                ->where("a.id = ?", $invoice_id)
                ->orderBy("a.id DESC")
                ->limit(1);
            $invoice = $q->fetchOne();

            if ($invoice) {
                return $invoice;
            } else {
                $q = Doctrine_Query::create()
                    ->from("MfInvoice a")
                    ->leftJoin("a.FormEntry b")
                    ->where("b.form_id = ? AND b.entry_id = ?", array($form_id, $entry_id))
                    ->orderBy("a.id DESC")
                    ->limit(1);
                $invoice = $q->fetchOne();

                if ($invoice) {
                    return $invoice;
                } else {
                    return false;
                }
            }
        }
        else {
            $q = Doctrine_Query::create()
                ->from("MfInvoice a")
                ->leftJoin("a.FormEntry b")
                ->where("b.form_id = ? AND b.entry_id = ?", array($form_id, $entry_id))
                ->orderBy("a.id DESC")
                ->limit(1);
            $invoice = $q->fetchOne();

            if ($invoice) {
                return $invoice;
            } else {
                return false;
            }
        }
    }

    //Query for the total collected from a service for a specific day
    public function api_query_daily_total($api_key, $api_secret, $service_id, $date)
    {
        $query_details = array();

        //check if api_key matches the one set in the config files
        if($this->api_validate_request($api_key, $api_secret))
        {
            //Get Institution Name
            $mda = $this->api_fetch_mda($api_key, $api_secret);

            error_log("Invoice Query API: Daily Query request from ".$mda->getMdaName()." - ".$mda->getMdaBranch()." on ".$invoice_no);

            $qt = Doctrine_Query::create()
                ->select('SUM(b.amount) as total')
                ->from('MfInvoice a')
                ->leftJoin('a.FormEntry c')
                ->leftJoin('a.mfInvoiceDetail b')
                ->where('b.description LIKE ? OR b.description LIKE ?', array("%Total%", "%submission fee%"))
                ->andWhere('a.paid = ?', 2)
                ->andWhere('c.approved <> 0')
                ->andWhere('c.form_id = ?', $service_id)
                ->andWhere('a.updated_at LIKE ?', "%".$date."%")
                ->orderBy('a.id DESC');
            $total = $qt->fetchOne();

            $query_details['total'] = $total->getTotal();
        }

        return $query_details;
    }

    //Query for the total collected from a service for a specific day
    public function api_query_period_total($api_key, $api_secret, $service_id, $from_date, $to_date)
    {
        $query_details = array();

        //check if api_key matches the one set in the config files
        if($this->api_validate_request($api_key, $api_secret))
        {
            //Get Institution Name
            $mda = $this->api_fetch_mda($api_key, $api_secret);

            error_log("Invoice Query API: Period Query request from ".$mda->getMdaName()." - ".$mda->getMdaBranch()." on ".$invoice_no);

            $qt = Doctrine_Query::create()
                ->select('SUM(b.amount) as total')
                ->from('MfInvoice a')
                ->leftJoin('a.FormEntry c')
                ->leftJoin('a.mfInvoiceDetail b')
                ->where('b.description LIKE ? OR b.description LIKE ?', array("%Total%", "%submission fee%"))
                ->andWhere('a.paid = ?', 2)
                ->andWhere('c.approved <> 0')
                ->andWhere('c.form_id = ?', $service_id)
                ->andWhere('a.updated_at BETWEEN ? AND ?', array($from_date, $to_date))
                ->orderBy('a.id DESC');
            $total = $qt->fetchOne();

            $query_details['total'] = $total->getTotal();
        }

        return $query_details;
    }

    //Query invoice details from external API
    public function api_query_invoice($api_key, $api_secret, $invoice_no)
    {
        $query_details = array();

        //check if api_key matches the one set in the config files
        if($this->api_validate_request($api_key, $api_secret))
        {
            //Get Institution Name
            $mda = $this->api_fetch_mda($api_key, $api_secret);

            error_log("Invoice Query API: Query request from ".$mda->getMdaName()." - ".$mda->getMdaBranch()." on ".$invoice_no);

            //$invoice = $this->get_invoice_by_reference($invoice_no);
            //OTB patch - get invoice by invoice number
            $invoice = $this->get_invoice_by_invoice_number($invoice_no);

            if($invoice)
            {
                $invoice = $invoice;
                $application = $invoice->getFormEntry();

                $q = Doctrine_Query::create()
                    ->from("SfGuardUserProfile a")
                    ->where("a.user_id = ?", $application->getUserId())
                    ->limit(1);
                $user = $q->fetchOne();

                /**
                 * Send back the following details:
                 *  - invoice_number
                 *  - total_amount
                 *  - invoice_status
                 *  - date_of_invoice
                 *  - application_id
                 *  - user's email
                 *  - user's mobile
                 *  - user's name
                 * */

                $query_details['invoice_number'] = $invoice->getInvoiceNumber();

                foreach($invoice->getMfInvoiceDetail() as $detail)
                {
                    if($detail->getDescription() == "Total")
                    {
                        $query_details['total_amount'] = $detail->getAmount();
                        $query_details['currency'] = "KES";
                    }
                }

                if($invoice->getPaid() == 1)
                {
                    $query_details['invoice_status'] = "pending";
                }
                elseif($invoice->getPaid() == 15)
                {
                    $query_details['invoice_status'] = "pending confirmation";
                }
                elseif($invoice->getPaid() == 2)
                {
                    $query_details['invoice_status'] = "paid";
                }
                elseif($invoice->getPaid() == 3)
                {
                    $query_details['invoice_status'] = "cancelled";
                }

                $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
                mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

                $sql = "UPDATE mf_invoice SET remote_validate = 1 WHERE id = ".$invoice->getId();
                $result = mysql_query($sql, $db_connection);

                $query_details['date_of_invoice'] = $invoice->getCreatedAt();
                $query_details['application_id'] = $application->getApplicationId();
                $query_details['user_email'] = $user->getEmail();
                $query_details['user_mobile'] = $user->getMobile();
                $query_details['user_fullname'] = $user->getFullname();
                $query_details['query_status'] = "success";
            }
            else
            {
                $query_details['query_status'] = "invalid invoice";
            }
        }
        else
        {
            $query_details['query_status'] = "unauthorized";
        }

        return $query_details;
    }

    //Query invoice details from external API
    public function api_update_invoice($api_key, $api_secret, $invoice_no, $transaction_details)
    {
        $update_details = array();

        $transaction_id = $transaction_details['transaction_id'];
        $transaction_date = $transaction_details['transaction_date'];
        $transaction_status = $transaction_details['transaction_status'];
        $amount_paid = $transaction_details['amount_paid'];
        $paid_by = $transaction_details['paid_by'];

        if(empty($transaction_id))
        {
            $update_details['update_status'] = "invalid transaction id";
            return $update_details;
        }

        if(empty($amount_paid))
        {
            $update_details['update_status'] = "invalid transaction amount";
            return $update_details;
        }

        $invoice_manager = new InvoiceManager();

        //check if api_key matches the one set in the config files
        if($invoice_manager->api_validate_request($api_key, $api_secret))
        {
            //Get Institution Name
            $mda = $invoice_manager->api_fetch_mda($api_key, $api_secret);

            $invoice = $this->get_invoice_by_reference($transaction_id);

            if($invoice && $invoice->getPaid() != 2)
            {
                $application = $invoice->getFormEntry();

                $q = Doctrine_Query::create()
                    ->from("SfGuardUserProfile a")
                    ->where("a.user_id = ?", $application->getUserId())
                    ->limit(1);
                $user = $q->fetchOne();

                $update_details['invoice_number'] = $invoice->getInvoiceNumber();

                foreach($invoice->getMfInvoiceDetail() as $detail)
                {
                    if($detail->getDescription() == "Total")
                    {
                        $update_details['total_amount'] = $detail->getAmount();
                        $update_details['currency'] = "KES";
                    }
                }

                if($invoice->getPaid() == 1)
                {
                    $update_details['invoice_status'] = "pending";
                }
                elseif($invoice->getPaid() == 15)
                {
                    $update_details['invoice_status'] = "pending confirmation";
                }
                elseif($invoice->getPaid() == 2)
                {
                    $update_details['invoice_status'] = "paid";
                }
                elseif($invoice->getPaid() == 3)
                {
                    $update_details['invoice_status'] = "cancelled";
                }

                $update_details['date_of_invoice'] = $invoice->getCreatedAt();
                $update_details['application_id'] = $application->getApplicationId();
                $update_details['user_email'] = $user->getEmail();
                $update_details['user_mobile'] = $user->getMobile();
                $update_details['user_fullname'] = $user->getFullname();

                //insert transaction details
                $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
                mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

                $sql = "INSERT INTO `ap_form_payments` (`form_id`, `record_id`, `payment_id`, `date_created`, `payment_date`, `payment_status`, `payment_fullname`, `payment_amount`, `payment_currency`, `payment_test_mode`, `payment_merchant_type`, `status`, `billing_street`, `billing_city`, `billing_state`, `billing_zipcode`, `billing_country`, `same_shipping_address`, `shipping_street`, `shipping_city`, `shipping_state`, `shipping_zipcode`, `shipping_country`) VALUES ('".$application->getFormId()."', '".$application->getEntryId()."', '".$transaction_id."', '".date("Y-m-d H:i:s")."', '".$transaction_date."', '".$transaction_status."', '".$paid_by."', '".$amount_paid."', 'KES', '0', '".$mda->getMdaName()." - ".$mda->getMdaBranch()."', '".$transaction_status."', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL);";
                mysql_query($sql, $dbconn);

                //update invoice
                if(doubleval($amount_paid) >= doubleval($update_details['total_amount']) && ($transaction_status == "completed" || $transaction_status == "paid"))
                {
                    $invoice->setPaid(2);
                    $invoice->save();

                    $update_details['invoice_status'] = "paid";
                    $update_details['update_status'] = "success";
                }
                else
                {
                  if(!($transaction_status == "completed" || $transaction_status == "paid"))
                  {
                    if($transaction_status == "cancelled" || $transaction_status == "failed")
                    {
                      $invoice->setPaid(3);
                      $invoice->save();

                      //Cancel any generated permits
                      $permits = $application->getSavedPermits();
                      foreach($permits as $permit)
                      {
                        $permit->delete();
                      }
                    }
                  }
                  else
                  {
                    $update_details['update_status'] = "invalid amount";
                  }
                }

                //return invoice details

                /**
                 * Send back the following details:
                 *  - invoice_number
                 *  - total_amount
                 *  - invoice_status
                 *  - date_of_invoice
                 *  - application_id
                 *  - user's email
                 *  - user's mobile
                 *  - user's name
                 * */
            }
            else
            {
                if($invoice && $invoice->getPaid() == 2)
                {
                  $update_details['update_status'] = "already paid";
                }
                else
                {
                  $update_details['update_status'] = "invalid invoice";
                }
            }
        }
        else
        {
            $update_details['update_status'] = "unauthorized";
        }

        return $update_details;
    }

    //Confirm transaction details from external API
    public function api_confirm_invoice($api_key, $api_secret, $invoice_no, $transaction_details)
    {
        $update_details = array();

        $transaction_id = $transaction_details['transaction_id'];
        $transaction_date = $transaction_details['transaction_date'];
        $transaction_status = $transaction_details['transaction_status'];
        $amount_paid = $transaction_details['amount_paid'];
        $paid_by = $transaction_details['paid_by'];

        if(empty($transaction_id))
        {
            $update_details['update_status'] = "invalid transaction id";
            return $update_details;
        }

        if(empty($amount_paid))
        {
            $update_details['update_status'] = "invalid transaction amount";
            return $update_details;
        }

        $invoice_manager = new InvoiceManager();

        //check if api_key matches the one set in the config files
        if($invoice_manager->api_validate_request($api_key, $api_secret))
        {
            //Get Institution Name
            $mda = $invoice_manager->api_fetch_mda($api_key, $api_secret);

            $invoice = $this->get_invoice_by_reference($transaction_id);

            if($invoice)
            {
                $application = $invoice->getFormEntry();

                $q = Doctrine_Query::create()
                    ->from("SfGuardUserProfile a")
                    ->where("a.user_id = ?", $application->getUserId())
                    ->limit(1);
                $user = $q->fetchOne();

                $update_details['invoice_number'] = $invoice->getInvoiceNumber();

                foreach($invoice->getMfInvoiceDetail() as $detail)
                {
                    if($detail->getDescription() == "Total")
                    {
                        $update_details['total_amount'] = $detail->getAmount();
                        $update_details['currency'] = "KES";
                    }
                }

                if($invoice->getPaid() == 1)
                {
                    $update_details['invoice_status'] = "pending";
                }
                elseif($invoice->getPaid() == 15)
                {
                    $update_details['invoice_status'] = "pending confirmation";
                }
                elseif($invoice->getPaid() == 2)
                {
                    $update_details['invoice_status'] = "paid";
                }
                elseif($invoice->getPaid() == 3)
                {
                    $update_details['invoice_status'] = "cancelled";
                }

                $update_details['date_of_invoice'] = $invoice->getCreatedAt();
                $update_details['application_id'] = $application->getApplicationId();
                $update_details['user_email'] = $user->getEmail();
                $update_details['user_mobile'] = $user->getMobile();
                $update_details['user_fullname'] = $user->getFullname();

                //update invoice
                if(doubleval($amount_paid) >= doubleval($update_details['total_amount']) && ($transaction_status == "completed" || $transaction_status == "paid"))
                {
                    $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
                    mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

                    $sql = "UPDATE mf_invoice SET remote_validate = 1 WHERE id = ".$invoice->getId();
                    $result = mysql_query($sql, $db_connection);

                    $update_details['invoice_status'] = "paid";
                    $update_details['update_status'] = "success";
                }
                else
                {
                  if(!($transaction_status == "completed" || $transaction_status == "paid"))
                  {
                    if($transaction_status == "cancelled" || $transaction_status == "failed")
                    {
                      $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
                      mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

                      $sql = "UPDATE mf_invoice SET remote_validate = 1 WHERE id = ".$invoice->getId();
                      $result = mysql_query($sql, $db_connection);

                      //Cancel any generated permits
                      $permits = $application->getSavedPermits();
                      foreach($permits as $permit)
                      {
                        $permit->delete();
                      }
                    }
                  }
                  else
                  {
                    $update_details['update_status'] = "invalid amount";
                  }
                }

                //return invoice details

                /**
                 * Send back the following details:
                 *  - invoice_number
                 *  - total_amount
                 *  - invoice_status
                 *  - date_of_invoice
                 *  - application_id
                 *  - user's email
                 *  - user's mobile
                 *  - user's name
                 * */
            }
            else
            {
               $update_details['update_status'] = "invalid invoice";
            }
        }
        else
        {
            $update_details['update_status'] = "unauthorized";
        }

        return $update_details;
    }

    //Generate a new invoice on submission using form payment settings
    public function create_invoice_from_submission($application_id)
    {
        $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

        $prefix_folder = dirname(__FILE__)."/vendor/cp_machform/";

        require_once($prefix_folder.'includes/init.php');

        require_once($prefix_folder.'config.php');
        require_once($prefix_folder.'includes/db-core.php');
        require_once($prefix_folder.'includes/helper-functions.php');

        require_once($prefix_folder.'includes/language.php');
        require_once($prefix_folder.'includes/common-validator.php');
        require_once($prefix_folder.'includes/view-functions.php');
        require_once($prefix_folder.'includes/theme-functions.php');
        require_once($prefix_folder.'includes/post-functions.php');
        require_once($prefix_folder.'includes/entry-functions.php');
        require_once($prefix_folder.'hooks/custom_hooks.php');

        $dbh 		= mf_connect_db();

        $submission = $this->get_application_by_id($application_id);

        $unpaid_invoice = $this->has_unpaid_invoice($application_id);

        //Create invoice
        $q = Doctrine_Query::create()
            ->from('Invoicetemplates a')
            ->where("a.applicationform = ?", $submission->getFormId())
            ->limit(1);
        $invoicetemplate = $q->fetchOne();

        if($invoicetemplate && $unpaid_invoice == false) {

            $invoice = new MfInvoice();
            $invoice->setAppId($submission->getId());

            if ($invoicetemplate->getInvoiceNumber()) {
                $q = Doctrine_Query::create()
                    ->from('MfInvoice a')
                    ->where("a.template_id = ?", $invoicetemplate->getId())
                    ->orderBy("a.id DESC")
                    ->limit(1);
                $lastinvoice = $q->fetchOne();

                if ($lastinvoice) {
                    $invoice_number = $lastinvoice->getInvoiceNumber();
                    $invoice_number++;
                    $invoice->setInvoiceNumber($invoice_number);
                } else {
                    $invoice->setInvoiceNumber($invoicetemplate->getInvoiceNumber());
                }

                $invoice->setTemplateId($invoicetemplate->getId());
            } else {
                $invoice->setInvoiceNumber("INV-" . $submission->getId());
            }

            $invoice->setPaid(15);
            $invoice->setCreatedAt(date("Y-m-d H:i:s"));
            $invoice->setUpdatedAt(date("Y-m-d H:i:s"));

            $invoice->save();

            //Get total paid and seperate service fee from amount
            $grand_total = 0;
            $service_fee = 0;

            $query = "select
                        form_code,
                        payment_currency,
                        payment_price_type,
                        payment_price_amount,
                        payment_enable_tax,
                        payment_tax_rate,
                        payment_discount_code,
                        payment_tax_amount
                        from
                           `ap_forms`
                       where
                          form_id=?";

            $params = array($submission->getFormId());

            $sth = mf_do_query($query, $params, $dbh);
            $row = mf_do_fetch_result($sth);

            $payment_currency = $row['payment_currency'];
            $payment_price_type = $row['payment_price_type'];
            $payment_price_amount = (float)$row['payment_price_amount'];

            $payment_enable_tax = (int)$row['payment_enable_tax'];
            $payment_tax_rate = (float)$row['payment_tax_rate'];
            $payment_tax_amount = (float)$row['payment_tax_amount'];
            $payment_tax_code = (float)$row['payment_discount_code'];
            $payment_currency = $row['payment_currency']; //OTB patch

            $service_code = $row['form_code'];

            //make sure the amount paid match or larger
            $payment_amount = "";
            if ($payment_price_type == 'variable') {
                $payment_amount = (double)mf_get_payment_total($dbh, $submission->getFormId(), $submission->getEntryId(), 0, 'live');
            } else if ($payment_price_type == 'fixed') {
                $payment_amount = (double)$payment_price_amount;
            }

            $total_amount = $payment_amount;

            if (!empty($payment_enable_tax)) {
                if ($payment_tax_amount) {
                    $service_fee = $payment_tax_amount;
                    $total_amount = $total_amount + $service_fee;
                } else {
                    $before_total = ($total_amount * 100) / (100 + $payment_tax_rate);
                    $service_fee = round(($total_amount - $before_total), 2); //round to 2 digits decimal
                    $total_amount = $before_total;
                }
            }

            if ($service_fee > 0) {
                $invoicedetail = new MfInvoiceDetail();
                $invoicedetail->setInvoiceId($invoice->getId());
                if ($payment_tax_code) {
                    $invoicedetail->setDescription($payment_tax_code . " - Convenience fee");
                } else {
                    $invoicedetail->setDescription("Convenience fee");
                }
                $invoicedetail->setAmount($service_fee);
                $invoicedetail->setCreatedAt(date("Y-m-d H:i:s"));
                $invoicedetail->setUpdatedAt(date("Y-m-d H:i:s"));
                $invoicedetail->save();
                $grand_total += $service_fee;
            }

            $invoicedetail = new MfInvoiceDetail();
            $invoicedetail->setInvoiceId($invoice->getId());
            if ($service_code) {
                $invoicedetail->setDescription($service_code . " - Submission fee");
            } else {
                $q = Doctrine_Query::create()
                    ->from("ApForms a")
                    ->where("a.form_id = ?", $submission->getFormId())
                    ->limit(1);
                $form = $q->fetchOne();
                if ($form) {
                    $invoicedetail->setDescription($form->getFormName() . " - Submission fee");
                } else {
                    $invoicedetail->setDescription("Submission fee");
                }
            }
            $invoicedetail->setAmount($payment_amount);
            $invoicedetail->setCreatedAt(date("Y-m-d H:i:s"));
            $invoicedetail->setUpdatedAt(date("Y-m-d H:i:s"));
            $invoicedetail->save();
            $grand_total += $payment_amount;

            $invoice->setMdaCode(sfConfig::get('app_mda_code'));
            $invoice->setBranch(sfConfig::get('app_branch'));

            $due_date = date("Y-m-d");
            $expires_at = date("Y-m-d");

            if ($invoicetemplate->getMaxDuration()) {
                $date = strtotime("+" . $invoicetemplate->getMaxDuration() . " day", time());
                $expires_at = date('Y-m-d', $date);
            }

            if ($invoicetemplate->getDueDuration()) {
                $date = strtotime("+" . $invoicetemplate->getDueDuration() . " day", time());
                $expires_at = date('Y-m-d', $date);
            }

            $q = Doctrine_Query::create()
                ->from('SfGuardUser a')
                ->where('a.id = ?', $submission->getUserId())
                ->limit(1);
            $sf_user = $q->fetchOne();

            $invoice->setDueDate($due_date);
            $invoice->setExpiresAt($expires_at);
            $invoice->setPayerId($sf_user->getUsername());
            $invoice->setPayerName($sf_user->getProfile()->getFullname());
            $invoice->setDocRefNumber($submission->getApplicationId());
            $invoice->setCurrency($payment_currency);
            $invoice->setServiceCode($service_code);
            $invoice->setTotalAmount($grand_total);
            $invoice->save();

            return $invoice;
        }
    }

    //Generate a new invoice on submission using form payment settings
    public function create_invoice_from_different($application_id, $difference_total)
    {
        $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

        $prefix_folder = dirname(__FILE__)."/vendor/cp_machform/";

        require_once($prefix_folder.'includes/init.php');

        require_once($prefix_folder.'config.php');
        require_once($prefix_folder.'includes/db-core.php');
        require_once($prefix_folder.'includes/helper-functions.php');

        require_once($prefix_folder.'includes/language.php');
        require_once($prefix_folder.'includes/common-validator.php');
        require_once($prefix_folder.'includes/view-functions.php');
        require_once($prefix_folder.'includes/theme-functions.php');
        require_once($prefix_folder.'includes/post-functions.php');
        require_once($prefix_folder.'includes/entry-functions.php');
        require_once($prefix_folder.'hooks/custom_hooks.php');

        $dbh        = mf_connect_db();

        $submission = $this->get_application_by_id($application_id);

        $unpaid_invoice = $this->has_unpaid_invoice($application_id);

        //Create invoice
        $q = Doctrine_Query::create()
            ->from('Invoicetemplates a')
            ->where("a.applicationform = ?", $submission->getFormId())
            ->limit(1);
        $invoicetemplate = $q->fetchOne();

        if($invoicetemplate && $unpaid_invoice == false) {

            $invoice = new MfInvoice();
            $invoice->setAppId($submission->getId());

            if ($invoicetemplate->getInvoiceNumber()) {
                $q = Doctrine_Query::create()
                    ->from('MfInvoice a')
                    ->where("a.template_id = ?", $invoicetemplate->getId())
                    ->orderBy("a.id DESC")
                    ->limit(1);
                $lastinvoice = $q->fetchOne();

                if ($lastinvoice) {
                    $invoice_number = $lastinvoice->getInvoiceNumber();
                    $invoice_number++;
                    $invoice->setInvoiceNumber($invoice_number);
                } else {
                    $invoice->setInvoiceNumber($invoicetemplate->getInvoiceNumber());
                }

                $invoice->setTemplateId($invoicetemplate->getId());
            } else {
                $invoice->setInvoiceNumber("INV-" . $submission->getId());
            }

            $invoice->setPaid(1);
            $invoice->setCreatedAt(date("Y-m-d H:i:s"));
            $invoice->setUpdatedAt(date("Y-m-d H:i:s"));

            $invoice->save();

            //Get total paid and seperate service fee from amount
            $grand_total = 0;
            $service_fee = 0;

            $query = "select
                        form_code,
                        payment_currency,
                        payment_price_type,
                        payment_price_amount,
                        payment_enable_tax,
                        payment_tax_rate,
                        payment_discount_code,
                        payment_tax_amount
                        from
                           `ap_forms`
                       where
                          form_id=?";

            $params = array($submission->getFormId());

            $sth = mf_do_query($query, $params, $dbh);
            $row = mf_do_fetch_result($sth);

            $payment_currency = $row['payment_currency'];
            $payment_price_type = $row['payment_price_type'];

            $service_code = $row['form_code'];

            //make sure the amount paid match or larger
            $payment_amount = "";
            if ($payment_price_type == 'variable') {
                $payment_amount = (double)mf_get_payment_total($dbh, $submission->getFormId(), $submission->getEntryId(), 0, 'live');
            } else if ($payment_price_type == 'fixed') {
                $payment_amount = (double)$payment_price_amount;
            }

            $total_amount = $difference_total;

            $invoicedetail = new MfInvoiceDetail();
            $invoicedetail->setInvoiceId($invoice->getId());
            if ($service_code) {
                $invoicedetail->setDescription($service_code . " - Submission fee");
            } else {
                $q = Doctrine_Query::create()
                    ->from("ApForms a")
                    ->where("a.form_id = ?", $submission->getFormId())
                    ->limit(1);
                $form = $q->fetchOne();
                if ($form) {
                    $invoicedetail->setDescription($form->getFormName() . " - Submission fee");
                } else {
                    $invoicedetail->setDescription("Submission fee");
                }
            }
            $invoicedetail->setAmount($total_amount);
            $invoicedetail->setCreatedAt(date("Y-m-d H:i:s"));
            $invoicedetail->setUpdatedAt(date("Y-m-d H:i:s"));
            $invoicedetail->save();

            $invoice->setMdaCode(sfConfig::get('app_mda_code'));
            $invoice->setBranch(sfConfig::get('app_branch'));

            $due_date = date("Y-m-d");
            $expires_at = date("Y-m-d");

            if ($invoicetemplate->getMaxDuration()) {
                $date = strtotime("+" . $invoicetemplate->getMaxDuration() . " day", time());
                $expires_at = date('Y-m-d', $date);
            }

            if ($invoicetemplate->getDueDuration()) {
                $date = strtotime("+" . $invoicetemplate->getDueDuration() . " day", time());
                $expires_at = date('Y-m-d', $date);
            }

            $q = Doctrine_Query::create()
                ->from('SfGuardUser a')
                ->where('a.id = ?', $submission->getUserId())
                ->limit(1);
            $sf_user = $q->fetchOne();

            $invoice->setDueDate($due_date);
            $invoice->setExpiresAt($expires_at);
            $invoice->setPayerId($sf_user->getUsername());
            $invoice->setPayerName($sf_user->getProfile()->getFullname());
            $invoice->setDocRefNumber($submission->getApplicationId());
            $invoice->setCurrency($payment_currency);
            $invoice->setServiceCode($service_code);
            $invoice->setTotalAmount($total_amount);
            $invoice->save();

            return $invoice->getId();
        }
    }

    //Generate a new invoice from invoicing task
    public function create_invoice_from_task($application_id,$descriptions,$amounts)
    {
        $descriptions1 = $descriptions;
        $amounts1 = $amounts;

        $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

        $prefix_folder = dirname(__FILE__)."/vendor/cp_machform/";

        require_once($prefix_folder.'includes/init.php');

        require_once($prefix_folder.'config.php');
        require_once($prefix_folder.'includes/db-core.php');
        require_once($prefix_folder.'includes/helper-functions.php');

        require_once($prefix_folder.'includes/language.php');
        require_once($prefix_folder.'includes/common-validator.php');
        require_once($prefix_folder.'includes/view-functions.php');
        require_once($prefix_folder.'includes/theme-functions.php');
        require_once($prefix_folder.'includes/post-functions.php');
        require_once($prefix_folder.'includes/entry-functions.php');
        require_once($prefix_folder.'hooks/custom_hooks.php');

        $dbh 		= mf_connect_db();

        $submission = $this->get_application_by_id($application_id);

        $q = Doctrine_Query::create()
            ->from('Invoicetemplates a')
            ->where("a.applicationform = ?", $submission->getFormId())
            ->limit(1);
        $invoicetemplate = $q->fetchOne();

        $invoice = new MfInvoice();
        $invoice->setAppId($submission->getId());

        if($invoicetemplate->getInvoiceNumber() && !$invoicetemplate->getUseApplicationNumber())//OTB - option to set invoice number to application number - added check for option to use app number
        {
            $q = Doctrine_Query::create()
                ->from('MfInvoice a')
                ->where("a.template_id = ?", $invoicetemplate->getId())
                ->orderBy("a.id DESC")
                ->limit(1);
            $lastinvoice = $q->fetchOne();

            if($lastinvoice)
            {
                $invoice_number = $lastinvoice->getInvoiceNumber();
                $invoice_number++;
                $invoice->setInvoiceNumber($invoice_number);
            }
            else
            {
                $invoice->setInvoiceNumber($invoicetemplate->getInvoiceNumber());
            }

            $invoice->setTemplateId($invoicetemplate->getId());
        }
		//OTB Start - option to set invoice number to application number
        else if($invoicetemplate->getInvoiceNumber() && $invoicetemplate->getUseApplicationNumber()){
            $invoice->setInvoiceNumber($invoice->getFormEntry()->getApplicationId());
            $invoice->setTemplateId($invoicetemplate->getId());
		}
		//OTB End - option to set invoice number to application number
        else
        {
            $invoice->setInvoiceNumber("INV-".$submission->getId());
        }

        $invoice->setCreatedAt(date("Y-m-d"));
        $invoice->setUpdatedAt(date("Y-m-d"));

        $invoice->setPaid(1);
        $invoice->save();


        $grand_total = 0;

        $index = 0;



        $query  = "select
                form_code,
                payment_currency,
                payment_price_type,
                payment_price_amount,
                payment_enable_tax,
                payment_tax_rate,
                payment_tax_amount,
                payment_discount_code
                from
                   `".MF_TABLE_PREFIX."forms`
               where
                  form_id=?";

        $params = array($submission->getFormId());

        $sth = mf_do_query($query,$params,$dbh);
        $row = mf_do_fetch_result($sth);

        $payment_price_type 	     = $row['payment_price_type'];
        $payment_enable_tax 		 = (int) $row['payment_enable_tax'];
        $payment_tax_rate 			 = (float) $row['payment_tax_rate'];
        $payment_tax_amount 		 = (float) $row['payment_tax_amount'];
        $payment_tax_code	     = $row['payment_discount_code'];
         $payment_currency = $row['payment_currency']; //OTB patch
        $total_taxable = 0;

        while (list ($key,$val) = @each ($descriptions)) {
            if ($val != "Total") {
                $total_taxable += $amounts[$index];
            }
            $index++;
        }

        //Add convenience fee
        if($payment_price_type == 'variable'){
            //calculate tax if enabled
            if(!empty($payment_enable_tax)){
                if($payment_tax_amount)
                {
                    $total_taxable += $payment_tax_amount;

                    $invoicedetail = new MfInvoiceDetail();
                    $invoicedetail->setDescription($payment_tax_code." - Convenience Fee");
                    $invoicedetail->setAmount($payment_tax_amount);
                    $invoicedetail->setInvoiceId($invoice->getId());
                    $invoicedetail->save();
                }
                else
                {
                    $payment_tax_amount = ($payment_tax_rate / 100) * $total_taxable;
                    $payment_tax_amount = round($payment_tax_amount,2); //round to 2 digits decimal

                    $invoicedetail = new MfInvoiceDetail();
                    $invoicedetail->setDescription($payment_tax_code." - Convenience Fee");
                    $invoicedetail->setAmount($payment_tax_amount);
                    $invoicedetail->setInvoiceId($invoice->getId());
                    $invoicedetail->save();

                    $total_taxable += $payment_tax_amount;
                }
            }
        }else if($payment_price_type == 'fixed'){
            //calculate tax if enabled
            $tax_label = '';
            if(!empty($payment_enable_tax)){
                if($payment_tax_amount)
                {
                    $total_taxable += $payment_tax_amount;

                    $invoicedetail = new MfInvoiceDetail();
                    $invoicedetail->setDescription($payment_tax_code." - Convenience Fee");
                    $invoicedetail->setAmount($payment_tax_amount);
                    $invoicedetail->setInvoiceId($invoice->getId());
                    $invoicedetail->save();
                }
                else
                {
                    $payment_tax_amount = ($payment_tax_rate / 100) * $total_taxable;
                    $payment_tax_amount = round($payment_tax_amount,2); //round to 2 digits decimal

                    $invoicedetail = new MfInvoiceDetail();
                    $invoicedetail->setDescription($payment_tax_code." - Convenience Fee");
                    $invoicedetail->setAmount($payment_tax_amount);
                    $invoicedetail->setInvoiceId($invoice->getId());
                    $invoicedetail->save();

                    $total_taxable += $payment_tax_amount;
                }
            }
        }

        $index = 0;

        while (list ($key,$val) = @each ($descriptions1)) {
            if($val != "Total") {
              if($amounts1[$index] != 0)
              {
                $invoicedetail = new MfInvoiceDetail();
                $invoicedetail->setDescription($val);
                $invoicedetail->setAmount($amounts1[$index]);
                $invoicedetail->setInvoiceId($invoice->getId());
                $invoicedetail->save();
              }
            }
            else
            {
                if($total_taxable == 0)
                {
                    continue;
                }

                $invoicedetail = new MfInvoiceDetail();
                $invoicedetail->setDescription($val);
                $invoicedetail->setAmount($total_taxable);
                $invoicedetail->setInvoiceId($invoice->getId());
                $invoicedetail->save();
            }

            $index++;
        }

        $due_date = date("Y-m-d");
        $expires_at = date("Y-m-d");

        if($invoicetemplate->getMaxDuration())
        {
            $date = strtotime("+".$invoicetemplate->getMaxDuration()." day", time());
            $expires_at = date('Y-m-d', $date);
        }

        if($invoicetemplate->getDueDuration())
        {
            $date = strtotime("+".$invoicetemplate->getDueDuration()." day", time());
            $expires_at = date('Y-m-d', $date);
        }

        $invoice->setMdaCode(sfConfig::get('app_mda_code'));
        $invoice->setBranch(sfConfig::get('app_branch'));
        $invoice->setDueDate($due_date);
        $invoice->setExpiresAt($expires_at);

        $q = Doctrine_Query::create()
            ->from('sfGuardUser a')
            ->where('a.id = ?', $submission->getUserId())
            ->limit(1);
        $user = $q->fetchOne();
        if($user)
        {
            $invoice->setPayerId($user->getUsername());
        }

        $q = Doctrine_Query::create()
            ->from('sfGuardUserProfile a')
            ->where('a.user_id = ?', $submission->getUserId())
            ->limit(1);
        $userprofile = $q->fetchOne();
        if($userprofile)
        {
            $invoice->setPayerName($userprofile->getFullname());
        }

        $invoice->setDocRefNumber($submission->getApplicationId());
        //$invoice->setCurrency(sfConfig::get('app_currency'));
        $invoice->setCurrency($payment_currency); //OTB patch
        $invoice->setServiceCode($row['form_code']);
        $invoice->setTotalAmount($total_taxable);
        $invoice->save();
    }

    //Update an existing unpaid invoice (Prevents an application from having many unused invoices)
    public function update_invoice($application_id,$invoice_id,$descriptions,$amounts)
    {
        $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

        $prefix_folder = dirname(__FILE__)."/vendor/cp_machform/";

        require_once($prefix_folder.'includes/init.php');

        require_once($prefix_folder.'config.php');
        require_once($prefix_folder.'includes/db-core.php');
        require_once($prefix_folder.'includes/helper-functions.php');

        require_once($prefix_folder.'includes/language.php');
        require_once($prefix_folder.'includes/common-validator.php');
        require_once($prefix_folder.'includes/view-functions.php');
        require_once($prefix_folder.'includes/theme-functions.php');
        require_once($prefix_folder.'includes/post-functions.php');
        require_once($prefix_folder.'includes/entry-functions.php');
        require_once($prefix_folder.'hooks/custom_hooks.php');

        $dbh 		= mf_connect_db();

        $submission = $this->get_application_by_id($application_id);
        $invoice = $this->get_invoice_by_id($invoice_id);

        $invoice_details = $invoice->getMfInvoiceDetail();
        foreach($invoice_details as $detail)
        {
            $detail->delete();
        }

        $invoice->setCreatedAt(date("Y-m-d"));
        $invoice->setUpdatedAt(date("Y-m-d"));

        $invoice->setPaid(1);
        $invoice->save();

        $q = Doctrine_Query::create()
            ->from('Invoicetemplates a')
            ->where("a.applicationform = ?", $submission->getFormId())
            ->limit(1);
        $invoicetemplate = $q->fetchOne();


        $grand_total = 0;

        $index = 0;
        while (list ($key,$val) = @each ($descriptions)) {
            $invoicedetail = new MfInvoiceDetail();
            $invoicedetail->setDescription($val);
            $invoicedetail->setAmount($amounts[$index]);
            $invoicedetail->setInvoiceId($invoice->getId());
            $invoicedetail->save();
            if($val != "Total")
            {
                $grand_total += $amounts[$index];
            }
            $index++;

            $query  = "select
                form_code,
                payment_currency,
                payment_price_type,
                payment_price_amount,
                payment_enable_tax,
                payment_tax_rate,
                payment_tax_amount
                from
                   `".MF_TABLE_PREFIX."forms`
               where
                  form_id=?";

            $params = array($submission->getFormId());

            $sth = mf_do_query($query,$params,$dbh);
            $row = mf_do_fetch_result($sth);

            $payment_currency      = $row['payment_currency'];
            $service_code = $row['form_code'];


            $due_date = date("Y-m-d");
            $expires_at = date("Y-m-d");

            if($invoicetemplate->getMaxDuration())
            {
                $date = strtotime("+".$invoicetemplate->getMaxDuration()." day", time());
                $expires_at = date('Y-m-d', $date);
            }

            if($invoicetemplate->getDueDuration())
            {
                $date = strtotime("+".$invoicetemplate->getDueDuration()." day", time());
                $expires_at = date('Y-m-d', $date);
            }

            $invoice->setMdaCode(sfConfig::get('app_mda_code'));
            $invoice->setBranch(sfConfig::get('app_branch'));
            $invoice->setDueDate($due_date);
            $invoice->setExpiresAt($expires_at);

            $q = Doctrine_Query::create()
                ->from('sfGuardUser a')
                ->where('a.id = ?', $submission->getUserId())
                ->limit(1);
            $user = $q->fetchOne();
            if($user)
            {
                $invoice->setPayerId($user->getUsername());
            }

            $q = Doctrine_Query::create()
                ->from('sfGuardUserProfile a')
                ->where('a.user_id = ?', $submission->getUserId())
                ->limit(1);
            $userprofile = $q->fetchOne();
            if($userprofile)
            {
                $invoice->setPayerName($userprofile->getFullname());
            }

            $invoice->setDocRefNumber($submission->getApplicationId());
            $invoice->setCurrency($payment_currency);
            $invoice->setServiceCode($service_code);
            $invoice->setTotalAmount($grand_total);
            $invoice->save();
        }
    }

    //Check if an application already has an invoice
    public function has_invoice($application_id)
    {
        $q = Doctrine_Query::create()
            ->from('MfInvoice a')
            ->where('a.app_id = ?', $application_id)
            ->limit(1);
        $existing_invoice = $q->fetchOne();
        if($existing_invoice)//Already submitted then tell client its already submitted
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //Check if an application has an unpaid invoice
    public function has_unpaid_invoice($application_id)
    {
        $q = Doctrine_Query::create()
            ->from('MfInvoice a')
            ->where('a.app_id = ?', $application_id)
            ->andWhere('a.paid <> 2 AND a.paid <> 3')
            ->limit(1);
        $existing_invoice = $q->fetchOne();
        if($existing_invoice)//Already submitted then tell client its already submitted
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //Check if an application has a paid invoice
    public function has_paid_invoice($application_id)
    {
        $q = Doctrine_Query::create()
            ->from('MfInvoice a')
            ->where('a.app_id = ?', $application_id)
            ->andWhere('a.paid = 2')
            ->limit(1);
        $existing_invoice = $q->fetchOne();
        if($existing_invoice)//Already submitted then tell client its already submitted
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //Return an unpaid invoice
    public function get_unpaid_invoice($application_id)
    {
        $q = Doctrine_Query::create()
            ->from('MfInvoice a')
            ->where('a.app_id = ?', $application_id)
            ->andWhere('a.paid <> 2')
            ->andWhere('a.paid <> 3')
            ->limit(1);
        $existing_invoice = $q->fetchOne();
        if($existing_invoice)//Already submitted then tell client its already submitted
        {
            return $existing_invoice;
        }
        else
        {
            $q = Doctrine_Query::create()
                ->from('MfInvoice a')
                ->where('a.app_id = ?', $application_id)
                ->andWhere('a.paid <> 2')
                ->limit(1);
            $existing_invoice = $q->fetchOne();

            if($existing_invoice)
            {
              return $existing_invoice;
            }
            else
            {
              return false;
            }
        }
    }

    //Return paid invoice
    public function get_paid_invoice($application_id)
    {
        $q = Doctrine_Query::create()
            ->from('MfInvoice a')
            ->where('a.app_id = ?', $application_id)
            ->andWhere('a.paid = 2')
            ->limit(1);
        $existing_invoice = $q->fetchOne();
        if($existing_invoice)//Already submitted then tell client its already submitted
        {
            return $existing_invoice;
        }
        else
        {
            return false;
        }
    }

    //Returns an existing invoice
    public function get_invoice_by_id($invoice_id)
    {
        $q = Doctrine_Query::create()
            ->from('MfInvoice a')
            ->where('a.id = ?', $invoice_id)
            ->limit(1);
        $existing_invoice = $q->fetchOne();
        if($existing_invoice)//Already submitted then tell client its already submitted
        {
            return $existing_invoice;
        }
        else
        {
            return false;
        }
    }

    //Returns an existing invoice
    public function get_merchant_reference($invoice_id)
    {
        $q = Doctrine_Query::create()
            ->from('MfInvoice a')
            ->where('a.id = ?', $invoice_id)
            ->limit(1);
        $existing_invoice = $q->fetchOne();
        if($existing_invoice)//Already submitted then tell client its already submitted
        {
            return $existing_invoice->getFormEntry()->getFormId()."/".$existing_invoice->getFormEntry()->getEntryId()."/".$existing_invoice->getId();
        }
        else
        {
            return false;
        }
    }

    //Returns an existing application
    public function get_application_by_id($application_id)
    {
        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.id = ?', $application_id)
            ->orderBy("a.application_id DESC")
            ->limit(1);
        $existing_app = $q->fetchOne();
        if($existing_app)//Already submitted then tell client its already submitted
        {
            return $existing_app;
        }
        else
        {
            return false;
        }
    }

    //Query the payment status of an invoice if a transaction id exists
    public function update_payment_status($invoice_id)
    {
        $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

        $prefix_folder = dirname(__FILE__)."/vendor/cp_machform/";

        require_once($prefix_folder.'includes/init.php');

        require_once($prefix_folder.'config.php');
        require_once($prefix_folder.'includes/db-core.php');
        require_once($prefix_folder.'includes/helper-functions.php');

        require_once($prefix_folder.'includes/language.php');
        require_once($prefix_folder.'includes/common-validator.php');
        require_once($prefix_folder.'includes/view-functions.php');
        require_once($prefix_folder.'includes/theme-functions.php');
        require_once($prefix_folder.'includes/post-functions.php');
        require_once($prefix_folder.'includes/entry-functions.php');
        require_once($prefix_folder.'hooks/custom_hooks.php');

        require_once($prefix_folder.'includes/OAuth.php');

        $dbh 		= mf_connect_db();

        $invoice = $this->get_invoice_by_id($invoice_id);
        $submission = $this->get_application_by_id($invoice->getAppId());

        $query = "select * from ".MF_TABLE_PREFIX."form_payments where form_id = ? and record_id = ?";
        $params = array($submission->getFormId(),$submission->getEntryId());
        $sth = mf_do_query($query,$params,$dbh);
        $count = 0;
        while($row = mf_do_fetch_result($sth))
        {
            $count++;
            if(!empty($row)){
                $paid = true;

                if($row['billing_state'] != "" && $invoice->getPaid() != 2 && $invoice->getPaid() != 3)
                {
                    //Query pesapal status
                    //get form properties data
                    $query  = "select
                                              form_name,
                                              form_has_css,
                                              form_redirect,
                                              form_language,
                                              form_review,
                                              form_review_primary_text,
                                              form_review_secondary_text,
                                              form_review_primary_img,
                                              form_review_secondary_img,
                                              form_review_use_image,
                                              form_review_title,
                                              form_review_description,
                                              form_resume_enable,
                                              form_page_total,
                                              form_lastpage_title,
                                              form_pagination_type,
                                              form_theme_id,
                                              payment_show_total,
                                              payment_total_location,
                                              payment_enable_merchant,
                                              payment_merchant_type,
                                              payment_currency,
                                              payment_price_type,
                                              payment_price_name,
                                              payment_price_amount,
                                              payment_ask_billing,
                                              payment_ask_shipping,
                                              payment_pesapal_live_secret_key,
                                              payment_pesapal_live_public_key,
                                              payment_pesapal_test_secret_key,
                                              payment_pesapal_test_public_key,
                                              payment_pesapal_enable_test_mode,
                                              payment_enable_recurring,
                                              payment_recurring_cycle,
                                              payment_recurring_unit,
                                              payment_enable_trial,
                                              payment_trial_period,
                                              payment_trial_unit,
                                              payment_trial_amount,
                                              payment_delay_notifications,
                                              payment_enable_tax,
                                              payment_tax_rate,
                                              payment_tax_amount
                                             from
                                                ".MF_TABLE_PREFIX."forms
                                            where
                                               form_id = ?";
                    $params = array($submission->getFormId());

                    $sth = mf_do_query($query,$params,$dbh);
                    $rowtop = mf_do_fetch_result($sth);

                    if($rowtop['payment_merchant_type'] == "pesapal") {

                        $consumer_key = null;
                        $consumer_secret = null;
                        $statusrequestAPI = null;

                        if ($rowtop['payment_pesapal_enable_test_mode']) {
                            $consumer_key = trim($rowtop['payment_pesapal_test_secret_key']);//Register a merchant account on
                            //demo.pesapal.com and use the merchant key for testing.
                            //When you are ready to go live make sure you change the key to the live account
                            //registered on www.pesapal.com!
                            $consumer_secret = trim($rowtop['payment_pesapal_test_public_key']);// Use the secret from your test
                            //account on demo.pesapal.com. When you are ready to go live make sure you
                            //change the secret to the live account registered on www.pesapal.com!
                            $statusrequestAPI = trim('http://demo.pesapal.com/api/querypaymentstatus');//change to
                        } else {
                            $consumer_key = trim($rowtop['payment_pesapal_live_secret_key']);//Register a merchant account on
                            //demo.pesapal.com and use the merchant key for testing.
                            //When you are ready to go live make sure you change the key to the live account
                            //registered on www.pesapal.com!
                            $consumer_secret = trim($rowtop['payment_pesapal_live_public_key']);// Use the secret from your test
                            //account on demo.pesapal.com. When you are ready to go live make sure you
                            //change the secret to the live account registered on www.pesapal.com!
                            $statusrequestAPI = trim('https://www.pesapal.com/api/querypaymentstatus');//change to
                        }
                        //https://demo.pesapal.com/api/querypaymentstatus' when you are ready to go live!
                        // Parameters sent to you by PesaPal IPN
                        $pesapalTrackingId = trim($row['billing_state']);
                        $pesapal_merchant_reference = trim($row['payment_id']);

                        if ($pesapalTrackingId != '') {
                            $token = $params = NULL;
                            $consumer = new OAuthConsumer($consumer_key, $consumer_secret);
                            $signature_method = new OAuthSignatureMethod_HMAC_SHA1();

                            //get transaction status
                            $request_status = OAuthRequest::from_consumer_and_token($consumer, $token, "GET", $statusrequestAPI, $params);
                            $request_status->set_parameter("pesapal_merchant_reference", $pesapal_merchant_reference);
                            $request_status->set_parameter("pesapal_transaction_tracking_id", $pesapalTrackingId);
                            $request_status->sign_request($signature_method, $consumer, $token);


                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $request_status);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_HEADER, 1);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                            if (defined('CURL_PROXY_REQUIRED')) if (CURL_PROXY_REQUIRED == 'True') {
                                $proxy_tunnel_flag = (defined('CURL_PROXY_TUNNEL_FLAG') && strtoupper(CURL_PROXY_TUNNEL_FLAG) == 'FALSE') ? false : true;
                                curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, $proxy_tunnel_flag);
                                curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
                                curl_setopt($ch, CURLOPT_PROXY, CURL_PROXY_SERVER_DETAILS);
                            }

                            $response = curl_exec($ch);

                            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
                            $raw_header = substr($response, 0, $header_size - 4);
                            $headerArray = explode("\r\n\r\n", $raw_header);
                            $header = $headerArray[count($headerArray) - 1];

                            //transaction status
                            $elements = preg_split("/=/", substr($response, $header_size));
                            $status = $elements[1];

                            curl_close($ch);

                            //UPDATE YOUR DB TABLE WITH NEW STATUS FOR TRANSACTION WITH pesapal_transaction_tracking_id $pesapalTrackingId
                            if ($status == "COMPLETED") {
                                $invoice->setPaid(2);
                                $invoice->save();
                            } elseif ($status == "FAILED") {
                                $invoice->setPaid(3);
                                $invoice->save();
                            }
                        }

                    }
                    elseif($rowtop['payment_merchant_type'] == "cellulant")
                    {
                        $invoiceNumber = $row['billing_zipcode'];
                        $beepTransactionID = $row['billing_state'];

                        $url = "http://197.159.100.249:9000/hub/services/paymentGateway/XML/index.php";
                        $client = new IXR_Client($url);
                        $client->debug = false;


                        $credentials = array(
                            "username" => $rowtop['payment_cellulant_merchant_username'],
                            "password" => $rowtop['payment_cellulant_merchant_password'],
                        );
                        $dataPacket = array(
                            "invoiceNumber" => $invoiceNumber,
                            "beepTransactionID" => $beepTransactionID,
                        );

                        $request[] = $dataPacket;
                        $payload = array("credentials" => $credentials, "packet" => $request);

                        $client->query('BEEP.queryInvoicePayStatus',
                            $payload);
                        $response = $client->getResponse();

                        if (!$response) {
                            $error_message = $client->getErrorMessage();
                            error_log("Cellulant Error: ".$error_message);
                        }

                        if($response['results']['statusCode'] == "253")
                        {
                            $invoice->setPaid(2);
                            $invoice->save();
                        }
                        elseif($response['results']['statusCode'] == "195" || $response['results']['statusCode'] == "251")
                        {
                            $invoice->setPaid(3);
                            $invoice->save();
                        }
                    }
                }
            }
        }

        return $invoice;
    }

    public function can_make_partial_payment($application_id)
    {
        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.id = ?', $application_id)
            ->limit(1);
        $application = $q->fetchOne();

        $q = Doctrine_Query::create()
            ->from('Invoicetemplates a')
            ->where("a.applicationform = ?", $application->getFormId())
            ->limit(1);
        $invoicetemplate = $q->fetchOne();

        if($invoicetemplate && $invoicetemplate->getPaymentType() == "2")
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function get_invoice_total_owed($invoice_id)
    {
        $q = Doctrine_Query::create()
            ->from('MfInvoice a')
            ->where('a.id = ?', $invoice_id)
            ->limit(1);
        $existing_invoice = $q->fetchOne();
        if($existing_invoice)//Already submitted then tell client its already submitted
        {
            $total_amount = $existing_invoice->getTotalAmount();

                //Get all ap_form_payments that are paid and subtract from total amount
                $prefix_folder = dirname(__FILE__)."/vendor/cp_machform/";

                require_once($prefix_folder.'includes/init.php');

                require_once($prefix_folder.'config.php');
                require_once($prefix_folder.'includes/db-core.php');
                require_once($prefix_folder.'includes/helper-functions.php');

                $dbh 		= mf_connect_db();

                $submission = $this->get_application_by_id($existing_invoice->getAppId());

                $query = "select payment_amount from ".MF_TABLE_PREFIX."form_payments where form_id = ? and record_id = ? and (payment_status = ? or payment_status = ?)";
                $params = array($submission->getFormId(),$submission->getEntryId(),'paid','completed');
                $sth = mf_do_query($query,$params,$dbh);
                $count = 0;
                while($row = mf_do_fetch_result($sth)) {
                    $total_amount = $total_amount - $row['payment_amount'];
                }

            return $total_amount;
        }
        else
        {
            return false;
        }
    }

    //Checks for any invoice that should have been marked as paid becaused all partial payments were submitted
    public function update_invoices($application_id)
    {
        $q = Doctrine_Query::create()
            ->from("MfInvoice a")
            ->where("a.app_id = ?", $application_id);
        $invoices = $q->execute();

        foreach($invoices as $invoice)
        {
            if($invoice->getPaid() != 2)
            {
                $result = $this->remote_reconcile($invoice->getFormEntry()->getFormId()."/".$invoice->getFormEntry()->getEntryId()."/".$invoice->getId());

                //If response is paid, then mark invoice as paid
                if($result == "paid")
                {
                    $invoice->setPaid(2);
                    $invoice->save();

                    error_log("Pesaflow Remote Validated: ".$invoice->getFormEntry()->getApplicationId());
                }
                else
                {
                    error_log("Pesaflow Remote Pending: ".$invoice->getFormEntry()->getApplicationId());
                }
            }
            else
            {
                //if invoice is paid but the application is still in the draft stage. Try to execute triggers by saving the invoice again.
                if($invoice->getFormEntry()->getApproved() == 0)
                {
                    $invoice->save();
                }
            }
        }
    }

    //Automatic triggers
    public function update_invoices_all($application_id)
    {
        $application = $this->get_application_by_id($application_id);

        if($application)
        {
            foreach($application->getMfInvoice() as $invoice)
            {
                 $invoice->save();
            }
        }
    }

    //Checks if the specified invoice is expired
    public function is_invoice_expired($invoice_id)
    {
        $q = Doctrine_Query::create()
            ->from('MfInvoice a')
            ->where('a.id = ?', $invoice_id)
            ->limit(1);
        $existing_invoice = $q->fetchOne();

        if($existing_invoice->getPaid() == 2)
        {
            return false;
        }
        else {

            $q = Doctrine_Query::create()
                ->from('Invoicetemplates a')
                ->where('a.applicationform = ?', $existing_invoice->getFormEntry()->getFormId())
                ->limit(1);
            $invoice_template = $q->fetchOne();

            if ($invoice_template->getExpirationType() == 3) {
                //Check if the invoice expires based on yearly expiry dates
                $first_day_of_current_year = date("Y-01-01");
                if (strtotime($existing_invoice->getCreatedAt()) > strtotime($first_day_of_current_year)) {
                    return false;
                } else {
                    return true;
                }
            } elseif ($invoice_template->getExpirationType() == 2) {
                //Check if the invoice expires based on monthly expiry dates
                $first_day_of_current_month = date("Y-m-01");
                if (strtotime($existing_invoice->getCreatedAt()) > strtotime($first_day_of_current_month)) {
                    return false;
                } else {
                    return true;
                }
            } else {
                $db_date_event = str_replace('/', '-', $existing_invoice->getExpiresAt());

                $db_date_event = strtotime($db_date_event);

                if (time() > $db_date_event) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    //Returns an existing invoice
    public function update_invoice_partial($invoice_id, $new_total)
    {
        $q = Doctrine_Query::create()
            ->from('MfInvoice a')
            ->where('a.id = ?', $invoice_id)
            ->limit(1);
        $existing_invoice = $q->fetchOne();

        $q = Doctrine_Query::create()
            ->from("MfInvoiceDetail a")
            ->where("a.invoice_id = ?", $invoice_id)
            ->orderBy("a.id DESC");
        $detail = $q->fetchOne();

        $detail->setAmount($new_total);
        $detail->save();

        $total_amount = 0;
        foreach($q->execute() as $detail)
        {
            $total_amount = $total_amount + $detail->getAmount();
        }

        $existing_invoice->setTotalAmount($total_amount);
        $existing_invoice->save();

        return $existing_invoice;
    }

    //Confirm payment on remote payment gateway if available
    public function remote_reconcile($billing_reference)
    {
        /*$request_url = "https://pesaflow.ecitizen.go.ke/PaymentAPI/getStatus.php?billRefNumber=".$billing_reference;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $request_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        if (defined('CURL_PROXY_REQUIRED')) if (CURL_PROXY_REQUIRED == 'True') {
            $proxy_tunnel_flag = (defined('CURL_PROXY_TUNNEL_FLAG') && strtoupper(CURL_PROXY_TUNNEL_FLAG) == 'FALSE') ? false : true;
            curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, $proxy_tunnel_flag);
            curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
            curl_setopt($ch, CURLOPT_PROXY, CURL_PROXY_SERVER_DETAILS);
        }

        $response = curl_exec($ch);

        $start_pos = strpos($response, "{");
        $end_pos = strpos($response, "}");

        $short_response = substr($response, $start_pos, (($end_pos - $start_pos) + 1));

        $array_response = json_decode($short_response, true);

        error_log("Response: ".$short_response);

        if($array_response['status'] == 4 || $array_response['status'] == 1)
        {
            $invoice = $this->get_invoice_by_reference($billing_reference);
            
            try
            {
              $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
              mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

              $sql = "UPDATE mf_invoice SET remote_validate = 1 WHERE id = ".$invoice->getId();
              mysql_query($sql, $dbconn);

              //Update transaction table
              $q = Doctrine_Query::create()
                 ->from("ApFormPayments a")
                 ->where("a.payment_id = ?", $billing_reference)
                 ->andWhere("a.status <> ? or a.payment_status <> ?", array(2, 'paid'));
              $transaction = $q->fetchOne();

              if($transaction && $transaction->getPaymentStatus() != "paid")
              {
                $transaction->setStatus(2);
                $transaction->setPaymentStatus("paid");
                $transaction->setPaymentDate(date("Y-m-d H:i:s"));

                if($array_response['transaction_id'])
                {
                    $transaction->setBillingState($array_response['transaction_id']);
                }

                $transaction->save();
              }
            }catch(Exception $ex)
            {
              error_log("Pesaflow: Could not update ".$billing_reference." transaction to paid ".$ex);
            }

            return "paid";
        }
        else
        {
            $invoice = $this->get_invoice_by_reference($billing_reference);

            try
            {
              //Update transaction table
              $q = Doctrine_Query::create()
                 ->from("ApFormPayments a")
                 ->where("a.payment_id = ?", $billing_reference);
              $transaction = $q->fetchOne();

              if($transaction)
              {
                $transaction->setStatus(1);
                $transaction->setPaymentStatus("pending");
                $transaction->setPaymentDate(date("Y-m-d H:i:s"));
                $transaction->save();
              }
            }catch(Exception $ex)
            {
              error_log("Pesaflow: Could not update ".$billing_reference." transaction to pending ".$ex);
            }

            return "pending";
        }
        */
        return "pending";
    }

	//OTB Start Patch - For Implementing Dynamic Fees
    public function getFeeAmount($fee, $application_form, $application_form_id, $estimate_requested = false)
    {
		$fee_amount = 0;
		$base_field_value = str_replace(',','',$this->getFieldValue($fee->getBaseField(), $application_form, $application_form_id));

		if ($fee->getFeeType()=='percentage'){
			$fee_amount = ($fee->getAmount()/100) * $base_field_value;
		}else if($fee->getFeeType()=='range' or $fee->getFeeType()=='range_percentage'){
			$q = Doctrine_Query::create()
			->from('FeeRange a')
			 ->where('a.fee_id = ?', $fee->getId())
			 ->orderBy('a.id ASC');
			 $fee_ranges = $q->execute();
             
			 foreach($fee_ranges as $r ){
				$condition_met = false;
				//Get condition matching result
				
				$q = Doctrine_Query::create()
				->from('FeeRangeCondition a')
				 ->where('a.fee_range_id = ?', $r->getId())
				 ->orderBy('a.id ASC');
				 $fee_range_conditions = $q->execute();

			$condition_met_array = array();
			 foreach($fee_range_conditions as $f_con ){
					$coniditon_field_value = $f_con->getConditionField() ? $this->getFieldValue($f_con->getConditionField(), $application_form, $application_form_id) : false;

					if($coniditon_field_value and $f_con->getConditionOperator() == 1){
						$condition_met = strtolower(str_replace(" ", "", $f_con->getConditionValue())) == strtolower(str_replace(" ", "", $coniditon_field_value)) ? true : false;
					}else if($coniditon_field_value and $f_con->getConditionOperator() == 2){
						$condition_met = $coniditon_field_value < $f_con->getConditionValue() ? true : false;
					}else if($coniditon_field_value and $f_con->getConditionOperator() == 3){
						$condition_met = $coniditon_field_value > $f_con->getConditionValue() ? true : false;
					}else if($coniditon_field_value and $f_con->getConditionOperator() == 4){
						$condition_met = $coniditon_field_value <= $f_con->getConditionValue() ? true : false;
					}else if($coniditon_field_value and $f_con->getConditionOperator() == 5){
						$condition_met = $coniditon_field_value >= $f_con->getConditionValue() ? true : false;
					}else if($coniditon_field_value and $f_con->getConditionOperator() == 6){
						$condition_met = strtolower(str_replace(" ", "", $f_con->getConditionValue())) != strtolower(str_replace(" ", "", $coniditon_field_value)) ? true : false;
					}else if($coniditon_field_value and $f_con->getConditionOperator() == 7){
						$needle = strtolower(str_replace(" ", "", $f_con->getConditionValue()));
						$haystack = strtolower(str_replace(" ", "", $coniditon_field_value));
						$condition_met =  strpos($haystack, $needle) !== false ? true : false;
					}else if(!$f_con->getConditionField()){
						$condition_met = true;
					}
					array_push($condition_met_array, $condition_met);
				}

				if(count($condition_met_array) > 0){
					if($r->getConditionSetOperator() == "and"){
						$condition_met = !in_array(false, $condition_met_array);//all the conditions must be met i.e. true, if the configuration is "All of the conditions are met"
					}else{
						$condition_met = in_array(true, $condition_met_array) or in_array(false, $condition_met_array);//all the conditions do not have to be met i.e. true, if the configuration is "Any of the conditions are met"
					}
				}else{
					$condition_met = true;
				}

				if($r->getValueType() == "formula"){
					require_once("vendor/otbafrica/eos-1.0.0/eos.class.php");
					$eq = new eqEOS();
					$equation = str_replace('{base_field}',$base_field_value, $r->getResultValue());
					$equation = str_replace('{conditon_value}',$coniditon_field_value, $equation);
					$equation = $this->parseFormula($application_form, $application_form_id, $equation, $estimate_requested);
					$range_result = $eq->solveIF($equation);
				}else{
					$range_result = $r->getResultValue();
				}
				$range_result = round($range_result, 3);//round off to 3 decimal places

				if($base_field_value && $condition_met){
						if($fee->getFeeType()=='range_percentage'){
							$fee_amount += ($range_result / 100 ) * ($base_field_value);
						}else{
							//$fee_amount += $range_result;  //OTB
                            $fee_amount = $range_result; //CBS
						}
					}else if(!$base_field_value && $condition_met){
						$fee_amount += $range_result;
					}
			}
			if($fee_amount < $fee->getMinimumFee()){
				$fee_amount=$fee->getMinimumFee();
			}
		}else if ($fee->getFeeType()=='formula'){
			require_once("vendor/otbafrica/eos-1.0.0/eos.class.php");
			$eq = new eqEOS();
            $equation = str_replace('{base_field}',$base_field_value, $fee->getAmount());
			$equation = $this->parseFormula($application_form, $application_form_id, $equation, $estimate_requested);
			$fee_amount = $eq->solveIF($equation);
			$fee_amount = round($fee_amount, 3);//round off to 3 decimal places
		}
		else{
				$fee_amount=$fee->getAmount();
		}
		
		return $fee_amount;
    }
    public function parseFormula($application_form, $application_form_id, $formula, $estimate_requested = false){
		if($estimate_requested){
			foreach($application_form as $key => $value){
				if(strpos($formula, '{fm_'.$key.'}') !== false){
					$field_id = explode("element_", $key);
					$field_value = $this->getFieldValue(explode('element_', $key)[1], $application_form, $application_form_id);
					$formula = str_replace('{fm_'.$key.'}',$field_value, $formula);
				}
			}
			return $formula;
		}else{
			$templateparser = new TemplateParser();
			$q = Doctrine_Query::create()
			->from('FormEntry a')
			 ->where('a.form_id = ?', $application_form_id)
			 ->where('a.entry_id = ?', $application_form['id'])
			 ->orderBy('a.id ASC');
			$app = $q->fetchOne();

			return $templateparser->parse($app->getId(), $application_form_id, $application_form['id'], $formula);	
		}
	}
	
    public function getFieldValue($element_id, $application_form, $application_form_id){
		if ($element_id){
			$q = Doctrine_Query::create()
			->from('ApFormElements a')
			 ->where('a.element_id = ?', $element_id)
			 ->andWhere('a.form_id = ?', $application_form_id)
			 ->andWhereIn('a.element_type', array('select', 'checkbox', 'radio', 'simple_name'));
			 $option_elements = $q->fetchOne();
			 if($option_elements){
				if($option_elements->getElementType() == 'select' || $option_elements->getElementType() == 'radio'){
					$q = Doctrine_Query::create()
					   ->from('ApElementOptions a')
					   ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ? and live=1', array($application_form_id,$element_id,$application_form['element_'.$element_id]));
					$option = $q->fetchOne();
					return $option ? $option->getOption() : False;
				}else if($option_elements->getElementType() == 'simple_name'){
					$name = $application_form['element_'.$element_id.'_1'].$application_form['element_'.$element_id.'_2'];
					return $name;
				}else if($option_elements->getElementType() == 'checkbox'){//for checkbox, return values as strings separated by commas
					$q = Doctrine_Query::create()
					   ->from('ApElementOptions a')
					   ->where('a.form_id = ? AND a.element_id = ? and live=1', array($application_form_id,$element_id));
					$options = $q->execute();
					$optionsString = false;
					foreach ($options as $option){
						if ($application_form['element_'.$element_id.'_'.$option->getOptionId()] == 1){
							$optionsString .= $option->getOption().",";
						}
					}
					$optionsString = rtrim($optionsString, ",");
					return $optionsString ? $optionsString : False;
				}
			}else{
				return $application_form['element_'.$element_id];
			}
		}else{
			return False;
		}
	}
	//OTB End Patch - For Implementing Dynamic Fees

	//OTB Start Patch - Only get invoices in an 'invoicing' type stage, where payments can be made
    public function invoice_can_be_paid($invoice){
		$q = Doctrine_Query::create()
				->from("SubMenus a")
				->where("a.stage_type = 3");
		$payment_stage_ids = array_column($q->fetchArray(), 'id');
		
		if(in_array($invoice->getFormEntry()->getApproved(), $payment_stage_ids)){
			return true;
		}else{
			return false;
		}
	}
	//OTB End Patch - Only get invoices in an 'invoicing' type stage, where payments can be made
    //OTB Start - Use Expired stage settings to reset expired invoice accordingly - originated from Kisumu requirements
    public function update_expired_invoices($application_id, $stage_expired_invoice_action)
    {
        $application = $this->get_application_by_id($application_id);

        if($application)
        {
            foreach($application->getMfInvoice() as $invoice)
            {
				if($stage_expired_invoice_action == 2){
					$invoice->setPaid(1);
				}else if($stage_expired_invoice_action == 3){
					$invoice->setPaid(15);
				}else if($stage_expired_invoice_action == 4){
					$invoice->setPaid(2);				
				}else if($stage_expired_invoice_action == 5){
					$q = Doctrine_Query::create()
						->from('Invoicetemplates a')
						->where("a.applicationform = ?", $application->getFormId())
						->limit(1);
					$invoicetemplate = $q->fetchOne();

					$expires_at = date("Y-m-d");

					if ($invoicetemplate->getMaxDuration()) {
						$date = strtotime("+" . $invoicetemplate->getMaxDuration() . " day", time());
						$expires_at = date('Y-m-d', $date);
					}

					if ($invoicetemplate->getDueDuration()) {
						$date = strtotime("+" . $invoicetemplate->getDueDuration() . " day", time());
						$expires_at = date('Y-m-d', $date);
					}

					$invoice->setPaid(1);
					$invoice->setDueDate(date("Y-m-d H:i:s"));
					$invoice->setExpiresAt($expires_at);
				}
				$invoice->save();
            }
        }
    }
    //OTB End - Use Expired stage settings to reset expired invoice accordingly - originated from Kisumu requirements

	/* OTB - Start Calculator */
    public function getEstimationInputFields($form_id){
		$otb_helper = new OTBHelper();
		$q = Doctrine_Query::create()
		->from('Invoicetemplates a')
		 ->where('a.applicationform = ?', $form_id)
		 ->orderBy('a.id ASC');
		 $inv_templates = $q->execute();
		$element_ids = array('fees_exist' => 'no');

		foreach($inv_templates as $inv_template){

		$q = Doctrine_Query::create()
		->from('Fee a')
		 ->where('a.invoiceid = ?', $inv_template->getId())
		 ->orderBy('a.id ASC');
		 $fees = $q->execute();
		 
		 if(count($fees) > 0){
			$element_ids['fees_exist'] = 'yes';
		 }
			foreach($fees as $fee){
				if($fee->getBaseField() and !in_array($fee->getBaseField(), $element_ids)){
					array_push($element_ids, $fee->getBaseField());
				}

				$element_ids = $this->getElementsInFormula($form_id, $fee->getAmount(), $element_ids);//Get fields specified if fee amount is a formula
				if($fee->getFeeType() == "range" or $fee->getFeeType() == "range_percentage"){
					$q = Doctrine_Query::create()
					->from('FeeRange a')
					 ->where('a.fee_id = ?', $fee->getId())
					 ->orderBy('a.id ASC');
					 $fee_ranges = $q->execute();
					 foreach($fee_ranges as $r ){
						$q = Doctrine_Query::create()
						->from('FeeRangeCondition a')
						 ->where('a.fee_range_id = ?', $r->getId())
						 ->orderBy('a.id ASC');
						 $fee_range_conditions = $q->execute();
							foreach($fee_range_conditions as $f_con ){
								if($f_con->getConditionField() and !in_array($f_con->getConditionField(), $element_ids)){
									array_push($element_ids, $f_con->getConditionField());
								}
							}
							$element_ids = $this->getElementsInFormula($form_id, $r->getResultValue(), $element_ids);//Get fields specified if value of range condition is a formula
						}
					}
			}
		}
		return $element_ids;
	}

    public function getElementsInFormula($form_id, $formula, $element_ids){
		$otb_helper = new OTBHelper();
		foreach($otb_helper->getFormElements($form_id) as $element){		
			if(strpos($formula, '{fm_element_'.$element->getElementId().'}') !== false and !in_array($element->getElementId(), $element_ids)){
				array_push($element_ids, $element->getElementId());
			}
		}

		return $element_ids;
	}

    public function calculateEstimate($input_data, $form_id){
		$q = Doctrine_Query::create()
		->from('Invoicetemplates a')
		 ->where('a.applicationform = ?', $form_id)
		 ->orderBy('a.id ASC');
		 $inv_templates = $q->execute();
		 $fee_arr = Array();
		foreach($inv_templates as $inv_template){
			$fee_arr[$inv_template->getId()]['total_amount'] = 0;
			$fee_arr[$inv_template->getId()]['title'] = $inv_template->getTitle();
			$q = Doctrine_Query::create()
			->from('Fee a')
			 ->where('a.invoiceid = ?', $inv_template->getId())
			 ->orderBy('a.id ASC');
			 $fees = $q->execute();
				foreach($fees as $fee){
					$fee_arr[$inv_template->getId()]['total_amount'] += $this->getFeeAmount($fee, $input_data, $form_id, $input_data);
				}
		}
		return $fee_arr;
	}
	/* OTB - End Calculator */
}