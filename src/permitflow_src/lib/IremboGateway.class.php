<?php
/**
 *
 * Manages Irembo Payments Gateway
 *
 */
class ApplicationRequest {
    function ApplicationRequest($serviceCode, $transactionRefNo, $agencyIdOrCode, $districtCode, $paymentDetails, $notificationDetails, $passCode, $nationalIdOrPassportno, $surname, $name)
    {
        $this->serviceCode = $serviceCode;
        $this->transactionRefNo = $transactionRefNo;
        $this->agencyIdOrCode = $agencyIdOrCode;
        $this->districtCode = $districtCode;
        $this->paymentDetails = $paymentDetails;
        $this->notificationDetails = $notificationDetails;
        $this->passCode = $passCode;
        $this->nationalIdOrPassportno = $nationalIdOrPassportno;
        $this->surname = $surname;
        $this->name = $name;
    }
}

class notificationDetails {
    function notificationDetails($mobileNo, $emailAddress) 
    {
        $this->mobileNo = $mobileNo;
        $this->emailAddress = $emailAddress;
    }
}

class paymentDetails {
    function paymentDetails($currency, $amount) 
    {
        $this->currency = $currency;
        $this->amount = $amount;
    }
}

class IremboGateway {

    private $suffix = "s";
    public $invoice_manager = null;

    //Constructor for IremboGateway class
    public function IremboGateway()
    {
        $this->invoice_manager = new InvoiceManager();

        if (empty($_SERVER['HTTPS'])) {
            $this->suffix = "";
        }
    }

    public function BillToIrembo($application)
    {
		foreach($application->getMfInvoice() as $invoice){
			if($invoice->getPaid() == 1){
				$invoice_id = $invoice->getId();
				$payments_manager = new PaymentsManager();
				$payment_settings = $payments_manager->get_payment_settings($invoice_id);
				$queue_data = array('invoice_id'=>$invoice_id, 'payment_settings'=>$payment_settings);
				$queuemanager = new QueueManager();
				$queuemanager->queue_data($queue_data);

				/*$transaction = $this->sendInvoiceDetails($invoice_id, $payment_settings);//Send invoice details to irembo
				$rol_bill_number = $transaction->getPaymentId();*/
			}
		}
	}

    //Add transaction details
    public function addTransactionDetails($invoice, $merchant_reference, $fullname, $amountExpected, $payment_currency, $status){
		$transaction = new ApFormPayments();
		$transaction->setFormId($invoice->getFormEntry()->getFormId());
		$transaction->setRecordId($invoice->getFormEntry()->getEntryId());
		$transaction->setPaymentId($merchant_reference);
		$transaction->setDateCreated(date("Y-m-d H:i:s"));
		$transaction->setPaymentFullname($fullname);
		$transaction->setPaymentAmount($amountExpected);
		$transaction->setPaymentCurrency($payment_currency);
		$transaction->setPaymentMerchantType('Irembo');
		$transaction->setPaymentTestMode("0");

		$transaction->setPaymentStatus("pending");
		$transaction->setStatus($status);
		$transaction->setInvoiceId($invoice->getId());

		$transaction->save();
		
		return $transaction;
	}
    public function getIremboOfflineCheckoutForm($invoice_id, $payment_currency, $payment_amount){
		$irembo_offline_url = 'http://' . $_SERVER['HTTP_HOST'] . '/index.php/payment/irembooffline/invoice_id/' . $invoice_id;
        $invoice = $this->invoice_manager->get_invoice_by_id($invoice_id);

        $q = Doctrine_Query::create()
           ->from("ApFormPayments a")
           ->where("a.form_id = ? AND a.record_id = ?", array($invoice->getFormEntry()->getFormId(), $invoice->getFormEntry()->getEntryId()))
		   ->andWhere("a.invoice_id = ?", $invoice_id)
           ->andWhere("a.status <> ? or a.payment_status <> ?", array(2, 'paid'))
           ->andWhere("a.status <> ? or a.payment_status <> ?", array(3, 'cancelled'));
        $transaction = $q->fetchOne();
		if($transaction and ($transaction->getStatus() == 2 or $transaction->getPaymentStatus() == 'paid')){
				$offline_form = <<<EOT
<img class="media-object thumbnail pull-left" src="/assets_unified/images/merchants/irembo.jpg" alt="" style="margin-right:10px; width:65px;"/>
<div class="clearfix"></div>
<div id="offline">
	<p class="alert alert-success m0" style="padding:20px; color:000; border-radius:5px; background:fff; border: 1px solidg 428bca">Your payment for bill ({$payment_currency} {$payment_amount}) through Irembo offline payment has already been completed please go to your application {$invoice->getFormEntry()->getApplicationId()} and download your permit.</p>
</div>
EOT;
		}else if(!$transaction or ($transaction and ($transaction->getStatus() == 15 or $transaction->getPaymentStatus() == 'pending'))){
				$rol_bill_number = $transaction ? $transaction->getPaymentId() : "";
				$offline_form = <<<EOT
<div class="panel panel-default panel-blog panel-checkout">
                            <div class="panel-body">
                                <h3 class="blogsingle-title">Irembo Offline Payment</h3>
                                <ul class="blog-meta mb5">
                                </ul>

                                <div class="panel-group mt10 mb0" id="accordion" style="border:1px solid #e7e7e7;">
<div id="offlinepayment" class="panel panel-default">
    <div id="collapseOne" class="panel-collapse collapse in">
        <div class="panel-body">
			<p>Payment Options:</p>
            <div class="clearfix"></div>
            <img class="media-object thumbnail pull-left" src="/assets_unified/images/merchants/airtel-money.jpg" alt="" style="margin-right:10px; width:65px;"/>
            <img class="media-object thumbnail pull-left" src="/assets_unified/images/merchants/momo.jpg" alt="" style="margin-right:10px; width:65px;"/>
            <img class="media-object thumbnail pull-left" src="/assets_unified/images/merchants/tigocash.jpg" alt="" style="margin-right:10px; width:65px;"/>
            <img class="media-object thumbnail pull-left" src="/assets_unified/images/merchants/bk.jpg" alt="" style="margin-right:10px; width:65px;"/>
            <img class="media-object thumbnail pull-left" src="/assets_unified/images/merchants/bk_yacu.jpg" alt="" style="margin-right:10px; width:65px;"/>
            <div class="clearfix"></div>
            <br/>
            <div id="BK">
                <p class="alert alert-success m0" style="padding:20px; color:000; border-radius:5px; background:fff; border: 1px solidg 428bca">Follow the Steps below to pay your bill via Bank of Kigali ({$payment_currency} {$payment_amount})</p>
				<ol class="mt10">
					<li>Go to any Bank of Kigali branch or YACU agent</li>
					<li>Provide your billing number: <span style="font-size:12px; font-weight: bold;">{$rol_bill_number}</span> as part of the payment information.</li>
					<li>Once your payment is successful, your permit will be automatically issued.</li>
				</ol>
            </div>
            <div id="Airtel">
                <p class="alert alert-success m0" style="padding:20px; color:000; border-radius:5px; background:fff; border: 1px solidg 428bca">Follow the Steps below to pay your bill via Airtel Money ({$payment_currency} {$payment_amount})</p>
				<ol class="mt10">
					<li>Go to Airtel Money on your phone</li>
					<li>Use your billing number: <span style="font-size:12px; font-weight: bold;">{$rol_bill_number}</span></li>
					<li>You will receive a confirmation SMS</li>
					<li>Once your payment is successful, your permit will be automatically issued.</li>
				</ol>
            </div>
            <div id="Mtn">
                <p class="alert alert-success m0" style="padding:20px; color:000; border-radius:5px; background:fff; border: 1px solidg 428bca"><?php echo __("Follow the Steps below to pay your bill via MTN Mobile Money");?> ({$payment_currency} {$payment_amount})</p>
				<ol class="mt10">
					<li>Go to MTN Mobile Money on your phone</li>
					<li>Use your billing number: <span style="font-size:12px; font-weight: bold;">{$rol_bill_number}</span></li>
					<li>You will receive a confirmation SMS</li>
					<li>Once your payment is successful, your permit will be automatically issued.</li>
				</ol>
            </div>
            <div id="Tigo">
                <p class="alert alert-success m0" style="padding:20px; color:000; border-radius:5px; background:fff; border: 1px solidg 428bca">Follow the Steps below to pay your bill via Tigo Cash ({$payment_currency} {$payment_amount}) </p>
				<ol class="mt10">
					<li>Go to Tigo cash on your phone</li>
					<li>Use your billing number: <span style="font-size:12px; font-weight: bold;">{$rol_bill_number}</span></li>
					<li>You will receive a confirmation SMS</li>
					<li>Once your payment is successful, your permit will be automatically issued.</li>
				</ol>
            </div>
<br>
        </div>
    </div>
</div>

                                </div>
                            </div>
</div>
EOT;
		}else{
				$offline_form = <<<EOT
<img class="media-object thumbnail pull-left" src="/assets_unified/images/merchants/irembo.jpg" alt="" style="margin-right:10px; width:65px;"/>
<div class="clearfix"></div>
<p>Payment Options:</p>
<div class="clearfix"></div>
<img class="media-object thumbnail pull-left" src="/assets_unified/images/merchants/airtel-money.jpg" alt="" style="margin-right:10px; width:65px;"/>
<img class="media-object thumbnail pull-left" src="/assets_unified/images/merchants/momo.jpg" alt="" style="margin-right:10px; width:65px;"/>
<img class="media-object thumbnail pull-left" src="/assets_unified/images/merchants/tigocash.jpg" alt="" style="margin-right:10px; width:65px;"/>
<img class="media-object thumbnail pull-left" src="/assets_unified/images/merchants/bk.jpg" alt="" style="margin-right:10px; width:65px;"/>
<img class="media-object thumbnail pull-left" src="/assets_unified/images/merchants/bk_yacu.jpg" alt="" style="margin-right:10px; width:65px;"/>
<div class="clearfix"></div>
<div id="offline">
	<p class="alert alert-success m0" style="padding:20px; color:000; border-radius:5px; background:fff; border: 1px solidg 428bca">To Pay your bill ({$payment_currency} {$payment_amount}) through Irembo offline payment, click the payment button below. This option will allow you to pay with Bank of Kigali, MTN Mobile Money, Airtel Money or Tigo Cash</p>
</div>
<br/>
<a href="{$irembo_offline_url}"><button class="btn btn-success">Make Payment</button></a>
EOT;
}
		return $offline_form;
	}

    public function getIremboOnlineCheckoutForm($invoice_id, $payment_currency, $backend = false){
        $invoice = $this->invoice_manager->get_invoice_by_id($invoice_id);
        $application = $invoice->getFormEntry();

        $user = Doctrine_Core::getTable('SfGuardUser')->find(array($application->getUserId()));

        $fullname = $user->getProfile()->getFullname();
        $username = $user->getUsername();
        $idnumber = $user->getUsername();
        $email = $user->getEmailAddress();

        //One of email or phonenumber is required
        $phonenumber = $user->getProfile()->getMobile();

        //Get Payment Details
        $payment_description = $invoice->getFormEntry()->getForm()->getFormName();
        $payment_amount = $invoice->getTotalAmount();
        $merchant_reference = $this->invoice_manager->get_merchant_reference($invoice->getId());

        $amountExpected = $payment_amount;
        $callback_url = "";

        if($backend)
        {
            $callback_url = 'http://' . $_SERVER['HTTP_HOST'] . '/backend.php/applications/confirmpayment?id=' . $application->getFormId() . '&entryid=' . $application->getEntryId() . "&done=1&invoiceid=" . $invoice->getId();
        }
        else
        {
            $callback_url = 'http://' . $_SERVER['HTTP_HOST'] . '/index.php/forms/confirmpayment?id=' . $application->getFormId() . '&entryid=' . $application->getEntryId() . "&done=1&invoiceid=" . $invoice->getId();
        }
			return <<<EOT
<form method="post" action="https://rol.rw/rolCommon/processBillingIdDetails.jsp" id="billingDetails" name="billingDetails" autocomplete="off" target="_parent"> 	
<input type="hidden" name="rol_svc_code" value="EPYMNT" />
<input type="hidden" name="rol_transactionref" value="{$invoice->getInvoiceNumber()}" /><!-- Should be auto generated 32 characters length-->	   			   
<input type="hidden"  name="rol_agency_code" value="RHA"/> <!-- Agency Code -->	
<input type="hidden"  name="rol_password" value="98B7FD87D9330A09393A89494GD63GKJ"/>	<!-- Merchant password (For production will share actual)-->			   
<input type="hidden"  name="rol_customer_id" value="TESTROL000001" /> <!-- Merchant ID  (For production will share actual)-->
<input type="hidden" name="rol_customer_surname" value="{$fullname}" /><!-- Citizen surname-->
<input type="hidden"  name="rol_customer_othername" value="{$fullname}" /><!-- Citizen othername-->			  
<input type="hidden"  name="rol_amount" value="{$payment_amount}"  />		<!-- Amount for service-->	  	
<input type="hidden"  name="rol_currency_type" value="RWF" /><!-- currency type for amount-->	 
<input type="hidden"  name="rol_email" value="{$email}"/>	<!-- Citizen email address if any-->
<input type="hidden"  name="rol_mobilenumber" value="{$phonenumber}" />   	<!-- Citizen mobile number if any-->   				 
<input type="hidden" name="rol_returnURL" value="{$callback_url}"/>		<!--RDB return URL-->  
<input type="submit" class="btn btn-success" name="Submitbtn" value="Make Payment" />    
</form>
EOT;
	}
    //Display a checkout for the current gateway
    public function checkout($invoice_id, $payment_settings, $backend = false)
    {
        $invoice = $this->invoice_manager->get_invoice_by_id($invoice_id);
        $application = $invoice->getFormEntry();

        $user = Doctrine_Core::getTable('SfGuardUser')->find(array($application->getUserId()));

        $fullname = $user->getProfile()->getFullname();
        $idnumber = $user->getUsername();
        $email = $user->getEmailAddress();

        //One of email or phonenumber is required
        $phonenumber = $user->getProfile()->getMobile();

        //Get Payment Details
        $payment_description = $invoice->getFormEntry()->getForm()->getFormName();
        $payment_amount = $invoice->getTotalAmount();
        $merchant_reference = $this->invoice_manager->get_merchant_reference($invoice->getId());

        //Params
        //$jambopay_business = $payment_settings['payment_jambopay_business'];
        $payment_currency = $payment_settings['payment_currency'];

        $amountExpected = $payment_amount;

        $q = Doctrine_Query::create()
           ->from("ApFormPayments a")
           ->where("a.form_id = ? AND a.record_id = ?", array($invoice->getFormEntry()->getFormId(), $invoice->getFormEntry()->getEntryId()))
           ->andWhere("a.status <> ? or a.payment_status <> ?", array(2, 'paid'));
        $transaction = $q->fetchOne();

		//Completly ignore payment from BPMIS. By default show instructions for irembo payment, whether irembo has received app request or not
		return $this->getIremboOfflineCheckoutForm($invoice_id, $payment_currency, $payment_amount);	
        /*if($transaction)
        {
			if(($transaction->getInvoiceId() == $invoice->getId())and ($transaction->getStatus() == 15 or $transaction->getPaymentStatus() == 'pending')){
				return $this->getIremboOfflineCheckoutForm($invoice_id, $payment_currency, $payment_amount);	
			}
        }
        else
        {//For Irembo, let us not create a transaction on checkout until user actually initiates process. Will allow us to maintain irembo billing numbers as transaction payment_ids
			//$this->addTransactionDetails($invoice, $merchant_reference, $fullname, $amountExpected, $payment_currency, 15);
        }*/

            $checkout = <<<EOT
<div class="panel panel-default panel-blog panel-checkout">
                            <div class="panel-body">
                                <h3 class="blogsingle-title">Payment Method</h3>
                                <ul class="blog-meta mb5">
                                </ul>

                                <div class="panel-group mt10 mb0" id="accordion" style="border:1px solid #e7e7e7;">
<div id="onlinepayment" class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapseOne">
                <div class="rdio rdio-default">
                    <input value="1" id="onlinepayment" name="radio" type="radio">
                    <label for="radioDefault" style="margin-bottom:0 !important;">CLICK HERE FOR IREMBO ONLINE PAYMENT (DEBIT/CREDIT CARD)</label>
                </div>
            </a>
        </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse">
        <div class="panel-body">
            <img class="media-object thumbnail pull-left" src="/assets_unified/images/merchants/irembo.jpg" alt="" style="margin-right:10px; width:65px;"/>
            <div class="clearfix"></div>
			<p>Payment Options:</p>
            <div class="clearfix"></div>
            <img class="media-object thumbnail pull-left" src="/assets_unified/images/merchants/visa.jpg" alt="" style="margin-right:10px; width:65px;"/>
            <img class="media-object thumbnail pull-left" src="/assets_unified/images/merchants/mastercard.jpg" alt="" style="margin-right:10px; width:65px;"/>
            <img class="media-object thumbnail pull-left" src="/assets_unified/images/merchants/momo.jpg" alt="" style="margin-right:10px; width:65px;"/>
            <img class="media-object thumbnail pull-left" src="/assets_unified/images/merchants/tigocash.jpg" alt="" style="margin-right:10px; width:65px;"/>
            <img class="media-object thumbnail pull-left" src="/assets_unified/images/merchants/bk.jpg" alt="" style="margin-right:10px; width:65px;"/>
            <img class="media-object thumbnail pull-left" src="/assets_unified/images/merchants/bk_yacu.jpg" alt="" style="margin-right:10px; width:65px;"/>
            <div class="clearfix"></div>
            <div id="OnlinePaymentForm">
                <p class="alert alert-success m0" style="padding:20px; color:000; border-radius:5px; background:fff; border: 1px solidg 428bca">To Pay your bill ({$payment_currency} {$payment_amount}) through Irembo online payment, click the payment button below. This option will allow you to pay with a MasterCard, VISA or any other debit/credit card online.</p>
				<br/>
				{$this->getIremboOnlineCheckoutForm($invoice_id, $payment_currency, $backend)}
            </div>
<br>
        </div>
    </div>
</div>

<div id="offlinepayment" class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapseTwo">
                <div class="rdio rdio-default">
                    <input value="1" id="offlinepayment" name="radio" type="radio">
                    <label for="radioDefault" style="margin-bottom:0 !important;">CLICK HERE FOR IREMBO OFFLINE PAYMENT (BANK/MOBILE MONEY/WALLET)</label>
                </div>
            </a>
        </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
        <div class="panel-body">
				{$this->getIremboOfflineCheckoutForm($invoice_id, $payment_currency, $payment_amount)}
<br>
        </div>
    </div>
</div>
                                </div>
                            </div><!-- panel-body -->
</div>
EOT;

        return $checkout;
    }

    //Validate payment details received after redirect from checkout
    public function validate($invoice_id, $request_details, $payment_settings)
    {
        $invoice = $this->invoice_manager->get_invoice_by_id($invoice_id);

        /*if (isset($request_details->passcode))Secure Irembo post*/ {
			//Get returned data from offline or online payment
			$billRefNumber = $request_details['billRefNumber'] ? $request_details['billRefNumber'] : $request_details['rol_billing_id'];
			$transactionId = $request_details['transactionId'] ? $request_details['transactionId'] : $request_details['rol_transactionref'];
			$statusCode = $request_details['statusCode'] ? $request_details['statusCode'] : $request_details['rol_paystatus'];
			$agencyCode = $request_details['agencyCode'] ? $request_details['agencyCode'] : $request_details['rol_agency_code'];
			$statusMessage = $request_details['statusMessage'];
			$paymentTxNo = $request_details['paymentTxNo'];
			$paymentTxTimestamp = $request_details['paymentTxTimestamp'] ? $request_details['paymentTxTimestamp'] : time();
			$paymentMethod = $request_details['paymentMethod'] ? $request_details['paymentMethod'] : $request_details['rol_payment_mode'];
			$paymentChannel = $request_details['paymentChannel'] ? $request_details['paymentChannel'] : $request_details['rol_payment_mode'];
			//$passcode = $request_details->passcode;

            //$irembo_key = $payment_settings['payment_shared_key'];//shared key in ap_forms for irembo


            //**************** VERIFY *************************
            /*if (md5(utf8_encode($irembo_key)) == $passcode)*/ {
				if($request_details['billRefNumber']){
					$q = Doctrine_Query::create()
					   ->from("ApFormPayments a")
					   ->where("a.payment_id = ?", $billRefNumber);
				}else{
					$q = Doctrine_Query::create()
					   ->from("ApFormPayments a")
					   ->where("a.form_id = ? AND a.record_id = ?", array($invoice->getFormEntry()->getFormId(), $invoice->getFormEntry()->getEntryId()))
					   ->andWhere("a.status <> ?", 2)
					   ->andWhere("a.payment_amount = ?", $invoice->getTotalAmount())
					   ->andWhere("a.invoice_id = ?", $invoice->getId());
				}
				$transaction = $q->fetchOne();

                if($transaction)
                {
                    //Update transaction details
                    $transaction->setBillingState($statusMessage);
                    $transaction->setPaymentDate(date("Y-m-d H:i:s", $paymentTxTimestamp));
					if($billRefNumber and ($billRefNumber != $transaction->getPaymentId())){//Update bill id
						$transaction->setPaymentId($billRefNumber);
					}
					if($paymentChannel){
						$transaction->setPaymentMerchantType($paymentChannel);					
					}

                    if($statusCode == 0)//Confirm status codes to check from ROL, for now just pay
                    {
                        $transaction->setStatus(2);
                        $transaction->setPaymentStatus("paid");
                    }
                    elseif($statusCode == 1)
                    {
                        $transaction->setStatus(1);
                        $transaction->setPaymentStatus("failed");
                    }
                    else
                    {
                        $transaction->setStatus(15);
                        $transaction->setPaymentStatus("pending");
                    }

                    $transaction->save();
                }
                else
                {
                    $application = $invoice->getFormEntry();

                    $user = Doctrine_Core::getTable('SfGuardUser')->find(array($application->getUserId()));

                    $fullname = $user->getProfile()->getFullname();
                    //Add a new transaction if one doesn't exist
                    $transaction = new ApFormPayments();
                    $transaction->setFormId($invoice->getFormEntry()->getFormId());
                    $transaction->setRecordId($invoice->getFormEntry()->getEntryId());
                    $transaction->setPaymentId($billRefNumber);
                    $transaction->setDateCreated(date("Y-m-d H:i:s"));
                    $transaction->setPaymentFullname($fullname);
                    $transaction->setPaymentAmount($invoice->getTotalAmount());
                    $transaction->setPaymentCurrency($invoice->getCurrency());
                    $transaction->setPaymentMerchantType('Irembo');
                    $transaction->setPaymentTestMode("0");
                    $transaction->setInvoiceId($invoice->getId());
                    $transaction->setBillingState($statusMessage);
                    $transaction->setPaymentDate(date("Y-m-d H:i:s", $paymentTxTimestamp));

					if($billRefNumber and ($billRefNumber != $transaction->getPaymentId())){//Update bill id
						$transaction->setPaymentId($request_details['rol_billing_id']);
					}
					if($paymentChannel){
						$transaction->setPaymentMerchantType($paymentChannel);					
					}
                    if($statusCode == 0)//Confirm status codes to check from ROL, for now just pay
                    {
                        $transaction->setStatus(2);
                        $transaction->setPaymentStatus("paid");
                    }
					elseif($statusCode == 1)
                    {
                        $transaction->setStatus(1);
                        $transaction->setPaymentStatus("failed");
                    }
                    else
                    {
                        $transaction->setStatus(15);
                        $transaction->setPaymentStatus("pending");
                    }

                    $transaction->save();
                }

                error_log("Irembo: ".$statusCode."/".$this->invoice_manager->get_invoice_total_owed($invoice->getId()));

                if(($statusCode == 0) && $this->invoice_manager->get_invoice_total_owed($invoice->getId()) <= 0)//Confirm status codes to check from ROL, for now just pay
                {
                    //Payment is successful
                    $invoice->setPaid(2);
                    $invoice->setUpdatedAt(date("Y-m-d"));
                }
                elseif($statusCode == 1)
                {
                    //Payment has failed
                    $invoice->setPaid(3);
                    $invoice->setUpdatedAt(date("Y-m-d"));
                }
                else
                {
                    //Payment is incomplete
                    $invoice->setPaid(15);
                    $invoice->setUpdatedAt(date("Y-m-d"));
                }

                //Update invoice and allow any triggers to take place
                $invoice->save();

                error_log("Irembo - Debug-x: Successful Validation - ".$invoice->getFormEntry()->getApplicationId());

                return true;
            }
            /*else
            {
                error_log("Irembo - Debug-x: MISMATCHED PASSWORD - ".$invoice->getFormEntry()->getApplicationId());
                return false;
            }*/
        }
        /*else
        {
            error_log("Irembo - Debug-x: MISSING PASSWORD - ".$invoice->getFormEntry()->getApplicationId());
            return false;
        }*/
    }

    //Process payment notifications received from external payment server
    public function ipn($request_details)
    {
      try
      {
        $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

        $update_details = array();

        $transaction_id = $request_details["invoice"];
        $amount_paid = $request_details["amount"];

        if($request_details["transactiondate"])
        {
          $transaction_date = $request_details["transactiondate"];
        }
        elseif($request_details["transaction_date"])
        {
          $transaction_date = $request_details["transaction_date"];
        }

        if($request_details["transactionstatus"])
        {
          $transaction_status = $request_details["transactionstatus"];
        }
        elseif($request_details["transaction_status"])
        {
          $transaction_status = $request_details["transaction_status"];
        }

        if(empty($transaction_id))
        {
            $update_details['update_status'] = "invalid transaction id";
            return $update_details;
        }

        if(empty($amount_paid))
        {
            $update_details['update_status'] = "invalid transaction amount";
            return $update_details;
        }

        $amount_paid = $request_details["amount"];
        $paid_by = $request_details["paidby"];

        $invoice = $this->invoice_manager->get_invoice_by_invoice_number($transaction_id);

        $application = $invoice->getFormEntry();
        $user = Doctrine_Core::getTable('SfGuardUser')->find(array($application->getUserId()));
        $fullname = $user->getProfile()->getFullname();

        if($invoice->getPaid() <> 2 && $this->invoice_manager->invoice_can_be_paid($invoice))
        {
          $q = Doctrine_Query::create()
             ->from("ApFormPayments a")
             ->where("a.form_id = ? AND a.record_id = ?", array($invoice->getFormEntry()->getFormId(), $invoice->getFormEntry()->getEntryId()))
             ->andWhere("a.status <> ?", 2)
             ->andWhere("a.payment_amount = ?", $invoice->getTotalAmount());
          $transaction = $q->fetchOne();

          if($transaction)
          {
              //Update transaction details
              $transaction->setBillingState($request_details['transaction_id']);
              $transaction->setPaymentDate(date("Y-m-d H:i:s"));

              if($transaction_status == 'completed')
              {
                  $transaction->setStatus(2);
                  $transaction->setPaymentStatus("paid");
              }
              elseif($transaction_status == 'failed')
              {
                  $transaction->setStatus(1);
                  $transaction->setPaymentStatus("failed");
              }
              else
              {
                  $transaction->setStatus(15);
                  $transaction->setPaymentStatus("pending");
              }

              $transaction->save();
          }
          else
          {
              $application = $invoice->getFormEntry();

              $user = Doctrine_Core::getTable('SfGuardUser')->find(array($application->getUserId()));

              $fullname = $user->getProfile()->getFullname();

              //Add a new transaction if one doesn't exist
              $transaction = new ApFormPayments();
              $transaction->setFormId($invoice->getFormEntry()->getFormId());
              $transaction->setRecordId($invoice->getFormEntry()->getEntryId());
              $transaction->setPaymentId($invoice->getFormEntry()->getFormId()."/".$invoice->getFormEntry()->getEntryId()."/".$invoice->getId());
              $transaction->setDateCreated(date("Y-m-d H:i:s"));
              $transaction->setPaymentFullname($fullname);
              $transaction->setPaymentAmount($invoice->getTotalAmount());
              $transaction->setPaymentCurrency($invoice->getCurrency());
              $transaction->setPaymentMerchantType('Irembo');
              $transaction->setPaymentTestMode("0");

              $transaction->setBillingState($transaction_id);
              $transaction->setPaymentDate(date("Y-m-d H:i:s"));

              if($transaction_status == 'completed')
              {
                  $transaction->setStatus(2);
                  $transaction->setPaymentStatus("paid");
              }
              elseif($transaction_status == 'failed')
              {
                  $transaction->setStatus(1);
                  $transaction->setPaymentStatus("failed");
              }
              else
              {
                  $transaction->setStatus(15);
                  $transaction->setPaymentStatus("pending");
              }

              $transaction->save();
          }

          if($transaction_status == 'completed' && $this->invoice_manager->get_invoice_total_owed($invoice->getId()) <= 0)
          {
              //Payment is successful
              $invoice->setPaid(2);
              $invoice->setUpdatedAt(date("Y-m-d H:i:s"));
          }
          elseif($transaction_status == 'failed')
          {
              //Payment has failed
              $invoice->setPaid(3);
              $invoice->setUpdatedAt(date("Y-m-d H:i:s"));
          }
          else
          {
              //Payment is incomplete
              $invoice->setPaid(15);
              $invoice->setUpdatedAt(date("Y-m-d H:i:s"));
          }

          //Update invoice and allow any triggers to take place
          $invoice->save();
        }
        else if($invoice->getPaid() <> 2 && !$this->invoice_manager->invoice_can_be_paid($invoice)){
          $update_details['update_status'] = "not ready for payment";
		}
        else
        {
          $update_details['update_status'] = "already paid";
        }

        error_log("Irembo IPN: ".$request_details['status']."/".$this->invoice_manager->get_invoice_total_owed($invoice->getId()));


        // The default invoice status details
        $update_details['invoice_status'] = "pending";

        // Set the invoice status details
        if($invoice->getPaid() == 15)
        {
            $update_details['invoice_status'] = "pending confirmation";
        }
        elseif($invoice->getPaid() == 2)
        {
            $update_details['invoice_status'] = "paid";
        }
        elseif($invoice->getPaid() == 3)
        {
            $update_details['invoice_status'] = "cancelled";
        }

        $update_details['total_amount'] = $invoice->getTotalAmount();
        $update_details['currency'] = $invoice->getCurrency();
        $update_details['date_of_invoice'] = $invoice->getCreatedAt();
        $update_details['application_id'] = $application->getApplicationId();
        $update_details['user_email'] = $user->getEmailAddress();
        $update_details['user_mobile'] = $user->getProfile()->getMobile();
        $update_details['user_fullname'] = $user->getProfile()->getFullname();
      }
      catch(Exception $ex)
      {
        error_log("Debug-pesa: ".$ex);
      }

        return $update_details;
    }
}
