<?php
/**
 *
 * Manages Iveri Payments Gateway
 *
 * User: thomasjuma
 * Date: 11/19/14
 * Time: 12:26 AM
 */

class IveriGateway {

    private $suffix = "s";
    public $invoice_manager = null;

    //Constructor for PesaflowGateway class
    public function __construct()
    {
        $this->invoice_manager = new InvoiceManager();

        if (empty($_SERVER['HTTPS'])) {
            $this->suffix = "";
        }
    }

    //Display a checkout for the current gateway
    public function checkout($invoice_id, $payment_settings, $backend = false)
    {
        $invoice = $this->invoice_manager->get_invoice_by_id($invoice_id);
        $application = $invoice->getFormEntry();

        $user = Doctrine_Core::getTable('SfGuardUser')->find(array($application->getUserId()));

        $fullname = $user->getProfile()->getFullname();
        $idnumber = $user->getUsername();
        $email = $user->getEmailAddress();

        //One of email or phonenumber is required
        $phonenumber = $user->getProfile()->getMobile();

        $callBackURLOnSuccess = 'http'.$this->suffix.'://'.$_SERVER['HTTP_HOST'].'/index.php/forms/confirmpayment?id='.$invoice->getFormEntry()->getFormId().'&entryid='.$invoice->getFormEntry()->getEntryId().'&done=1&invoiceid='.$invoice->getId().'&status=201'; //redirect url, the page that will handle the response from pesaflow.
        $callBackURLOnFail = 'http'.$this->suffix.'://'.$_SERVER['HTTP_HOST'].'/index.php/forms/invalidpayment';

        $checkout = '
        <div class="message warning">
							<div class="inset">
								<div class="login-head">
									<h3>Are you sure you want to pay for this service now?</h3>
									
								</div>
								<form id="myform" name="myform" METHOD="POST" ACTION="https://backoffice.nedsecure.co.za/Lite/Transactions/New/EasyAuthorise.aspx">
									<!-- Mandatory form variables  -->
									<input id="Lite_Merchant_Applicationid" name="Lite_Merchant_Applicationid" type="hidden" value="{a79afc32-a8af-41e3-b745-afd5e0e104f9}" readonly="true">
									<input id="Lite_Order_Amount" name="Lite_Order_Amount" type="hidden" value="'.$invoice->getTotalAmount().'00" readonly="true">
									<input type="hidden" name="Lite_Website_Successful_url" value="'.$callBackURLOnSuccess.'" />
									<input type="hidden" name="Lite_Website_Fail_url" value="'.$callBackURLOnFail.'" />
									<input type="hidden" name="Lite_Website_TryLater_url" value="'.$callBackURLOnFail.'" />
									<input type="hidden" name="Lite_Website_Error_url" value="'.$callBackURLOnFail.'" /> 
									<input id="Lite_Order_LineItems_Product_1" name="Lite_Order_LineItems_Product_1" type="hidden" value="'.$application->getApplicationId().'" readonly="true">
									<input id="Lite_Order_LineItems_Quantity_1" name="Lite_Order_LineItems_Quantity_1" type="hidden" value="1" readonly="true">
									<input id="Lite_Order_LineItems_Amount_1" name="Lite_Order_LineItems_Amount_1" type="hidden" value="'.$invoice->getTotalAmount().'00" readonly="true">
									<input id="Lite_ConsumerOrderID_PreFix" name="Lite_ConsumerOrderID_PreFix" type="hidden" value="DML" readonly="true">
									<input id="Ecom_BillTo_Online_Email" name="Ecom_BillTo_Online_Email" type="hidden" value="'.$user->getProfile()->getEmail().'">
									<input id="Ecom_Payment_Card_Protocols" name="Ecom_Payment_Card_Protocols" type="hidden" value="iVeri" readonly="true">
									<input id="Ecom_ConsumerOrderID" name="Ecom_ConsumerOrderID" type="hidden" value="AUTOGENERATE">
									<input id="Ecom_TransactionComplete" name="Ecom_TransactionComplete" type="hidden" value="" readonly="true">
									<!-- End-->
                  <font color="red"><p class="warningtext" id="warning" name="warning" >&nbsp;</p></font>
                  
                  <br><br>

									<div class="submit">
										<input type="submit" id="btnsubmit" name="btnsubmit"  value="Proceed">
										<button type="button" id="btnsubmit" name="btnsubmit" onClick="window.location=\'/index.php/application/view/id/'.$application.id.'\'"> Go Back </button>
										<div class="clear">  </div>	
									</div>

								</form>
							</div>					
						</div>
					</div>
					<div class="clear"> </div>
					<!--- footer --->
        ';

        return $checkout;
    }

    //Validate payment details received after redirect from checkout
    public function validate($invoice_id, $request_details, $payment_settings)
    {
      if ($request_details['status'] == 201 || $request_details['status'] == "201") {
        $invoice = $this->invoice_manager->get_invoice_by_id($invoice_id);

        $q = Doctrine_Query::create()
           ->from("ApFormPayments a")
           ->where("a.form_id = ? AND a.record_id = ?", array($invoice->getFormEntry()->getFormId(), $invoice->getFormEntry()->getEntryId()))
           ->andWhere("a.status <> ?", 2);
        $transaction = $q->fetchOne();

        if($transaction)
        {
            //Update transaction details
            $transaction->setBillingState("cash");
            $transaction->setPaymentDate(date("Y-m-d H:i:s"));

            $transaction->setStatus(2);
            $transaction->setPaymentStatus("paid");

            $transaction->save();
        }

        //Payment is successful
        $invoice->setPaid(2);
        $invoice->setUpdatedAt(date("Y-m-d H:i:s"));

        //Update invoice and allow any triggers to take place
        $invoice->save();

        error_log("Iveri - Debug-x: Successful Validation - ".$invoice->getFormEntry()->getApplicationId());

        return true;
      } else {
        return false;
      }
    }

    //Process payment notifications received from external payment server
    public function ipn($request_details)
    {
        error_log("Iveri - No IPN");
        return false;
    }
}
