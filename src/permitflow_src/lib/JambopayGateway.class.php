<?php
/**
 *
 * Manages Pesaflow Payments Gateway
 *
 * User: thomasjuma
 * Date: 11/19/14
 * Time: 12:26 AM
 */

class JambopayGateway {

    private $suffix = "s";
    public $invoice_manager = null;

    //Constructor for PesaflowGateway class
    public function JambopayGateway()
    {
        $this->invoice_manager = new InvoiceManager();

        if (empty($_SERVER['HTTPS'])) {
            $this->suffix = "";
        }
    }

    //Display a checkout for the current gateway
    public function checkout($invoice_id, $payment_settings, $backend = false)
    {
        $invoice = $this->invoice_manager->get_invoice_by_id($invoice_id);
        $application = $invoice->getFormEntry();

        $user = Doctrine_Core::getTable('SfGuardUser')->find(array($application->getUserId()));

        $fullname = $user->getProfile()->getFullname();
        $idnumber = $user->getUsername();
        $email = $user->getEmailAddress();

        //One of email or phonenumber is required
        $phonenumber = $user->getProfile()->getMobile();

        //Get Payment Details
        $payment_description = $invoice->getFormEntry()->getForm()->getFormName();
        $payment_amount = $invoice->getTotalAmount();
        $merchant_reference = $this->invoice_manager->get_merchant_reference($invoice->getId());

        //Params
        $jambopay_business = $payment_settings['payment_jambopay_business'];
        $payment_currency = $payment_settings['payment_currency'];

        $amountExpected = $payment_amount;

        $q = Doctrine_Query::create()
           ->from("ApFormPayments a")
           ->where("a.form_id = ? AND a.record_id = ?", array($invoice->getFormEntry()->getFormId(), $invoice->getFormEntry()->getEntryId()))
           ->andWhere("a.status <> ? or a.payment_status <> ?", array(2, 'paid'));
        $transaction = $q->fetchOne();

        if($transaction)
        {
          //Should we update any existing transactions at this point?
        }
        else
        {
          //Add transaction details
          $transaction = new ApFormPayments();
          $transaction->setFormId($invoice->getFormEntry()->getFormId());
          $transaction->setRecordId($invoice->getFormEntry()->getEntryId());
          $transaction->setPaymentId($merchant_reference);
          $transaction->setDateCreated(date("Y-m-d H:i:s"));
          $transaction->setPaymentFullname($fullname);
          $transaction->setPaymentAmount($amountExpected);
          $transaction->setPaymentCurrency($payment_currency);
          $transaction->setPaymentMerchantType('Jambopay');
          $transaction->setPaymentTestMode("0");

          $transaction->setPaymentStatus("pending");
          $transaction->setStatus(15);

          $transaction->save();
        }

        $callback_url = "";

        if($backend)
        {
            $callback_url = 'http://' . $_SERVER['HTTP_HOST'] . '/backend.php/applications/confirmpayment?id=' . $application->getFormId() . '&entryid=' . $application->getEntryId() . "&done=1&invoiceid=" . $invoice->getId();
        }
        else
        {
            $callback_url = 'http://' . $_SERVER['HTTP_HOST'] . '/index.php/forms/confirmpayment?id=' . $application->getFormId() . '&entryid=' . $application->getEntryId() . "&done=1&invoiceid=" . $invoice->getId();
        }

        $checkout = "";

        if($payment_currency == "USD")
        {
            $usd_currency = $amountExpected;
            $ksh_currency = $amountExpected * 90;

            $checkout = <<<EOT
<iframe name="iframe" width="100%" height="700px" scrolling="no" frameBorder="0">
<p>Browser unable to load iFrame</p> </iframe>
<form id="details" method="post" action="https://www.jambopay.com/JPExpress.aspx" target="iframe">
<input type="hidden" name="jp_item_type" value="cart"/>
<input type="hidden" name="jp_item_name" value="{$payment_description}"/>
<input type="hidden" name="order_id" value="{$merchant_reference}"/>
<input type="hidden" name="jp_business" value="{$jambopay_business}"/>
<input type="hidden" name="jp_amount_1" value="{$ksh_currency}"/>
<input type="hidden" name="jp_amount_2" value="0"/>
<input type="hidden" name="jp_amount_5" value="{$usd_currency}"/>
<input type="hidden" name="jp_payee" value="{$email}"/>
<input type="hidden" name="jp_shipping" value="eCitizen"/>
<input type="hidden" name="jp_rurl" value="{$callback_url}&status=2"/>
<input type="hidden" name="jp_furl" value="{$callback_url}&status=1"/>
<input type="hidden" name="jp_curl" value="{$callback_url}&status=0"/>
</form>
<script> document.getElementById("details").submit(); </script>
EOT;
        }
        else
        {
            $checkout = <<<EOT
<iframe name="iframe" width="100%" height="700px" scrolling="no" frameBorder="0">
<p>Browser unable to load iFrame</p> </iframe>
<form id="details" method="post" action="https://www.jambopay.com/JPExpress.aspx" target="iframe">
<input type="hidden" name="jp_item_type" value="cart"/>
<input type="hidden" name="jp_item_name" value="{$payment_description}"/>
<input type="hidden" name="order_id" value="{$merchant_reference}"/>
<input type="hidden" name="jp_business" value="{$jambopay_business}"/>
<input type="hidden" name="jp_amount_1" value="{$amountExpected}"/>
<input type="hidden" name="jp_amount_2" value="0"/>
<input type="hidden" name="jp_amount_5" value="0"/>
<input type="hidden" name="jp_payee" value="{$email}"/>
<input type="hidden" name="jp_shipping" value="eCitizen"/>
<input type="hidden" name="jp_rurl" value="{$callback_url}&status=2"/>
<input type="hidden" name="jp_furl" value="{$callback_url}&status=1"/>
<input type="hidden" name="jp_curl" value="{$callback_url}&status=0"/>
</form>
<script> document.getElementById("details").submit(); </script>
EOT;
        }

        return $checkout;
    }

    //Validate payment details received after redirect from checkout
    public function validate($invoice_id, $request_details, $payment_settings)
    {
        $invoice = $this->invoice_manager->get_invoice_by_id($invoice_id);

        if (isset($request_details['JP_PASSWORD'])) {

            $JP_TRANID = $request_details['JP_TRANID'];
            $JP_MERCHANT_ORDERID = $request_details['JP_MERCHANT_ORDERID'];
            $JP_ITEM_NAME = $request_details['JP_ITEM_NAME'];
            $JP_AMOUNT = $request_details['JP_AMOUNT'];
            $JP_CURRENCY = $request_details['JP_CURRENCY'];
            $JP_TIMESTAMP = $request_details['JP_TIMESTAMP'];
            $JP_PASSWORD = $request_details['JP_PASSWORD'];
            $JP_CHANNEL = $request_details['JP_CHANNEL'];

            //$sharedkey, IS ONLY SHARED BETWEEN THE MERCHANT AND JAMBOPAY. THE KEY SHOULD BE SECRET ********************

            //Make sure you get the key from JamboPay Support team
            $sharedkey = $payment_settings['payment_jambopay_shared_key'];

            $str = $JP_MERCHANT_ORDERID . $JP_AMOUNT . $JP_CURRENCY . $sharedkey . $JP_TIMESTAMP;

            error_log(md5(utf8_encode($str)));


            //**************** VERIFY *************************
            if (md5(utf8_encode($str)) == $JP_PASSWORD) {

                $q = Doctrine_Query::create()
                   ->from("ApFormPayments a")
                   ->where("a.form_id = ? AND a.record_id = ?", array($invoice->getFormEntry()->getFormId(), $invoice->getFormEntry()->getEntryId()))
                   ->andWhere("a.status <> ?", 2)
                   ->andWhere("a.payment_amount = ?", $invoice->getTotalAmount());
                $transaction = $q->fetchOne();

                if($transaction)
                {
                    //Update transaction details
                    $transaction->setBillingState($JP_TRANID);
                    $transaction->setPaymentDate(date("Y-m-d H:i:s"));

                    if($request_details['status'] == 2)
                    {
                        $transaction->setStatus(2);
                        $transaction->setPaymentStatus("paid");
                    }
                    elseif($request_details['status'] == 1)
                    {
                        $transaction->setStatus(1);
                        $transaction->setPaymentStatus("failed");
                    }
                    else
                    {
                        $transaction->setStatus(15);
                        $transaction->setPaymentStatus("pending");
                    }

                    $transaction->save();
                }
                else
                {
                    $application = $invoice->getFormEntry();

                    $user = Doctrine_Core::getTable('SfGuardUser')->find(array($application->getUserId()));

                    $fullname = $user->getProfile()->getFullname();

                    //Add a new transaction if one doesn't exist
                    $transaction = new ApFormPayments();
                    $transaction->setFormId($invoice->getFormEntry()->getFormId());
                    $transaction->setRecordId($invoice->getFormEntry()->getEntryId());
                    $transaction->setPaymentId($invoice->getFormEntry()->getFormId()."/".$invoice->getFormEntry()->getEntryId()."/".$invoice->getId());
                    $transaction->setDateCreated(date("Y-m-d H:i:s"));
                    $transaction->setPaymentFullname($fullname);
                    $transaction->setPaymentAmount($invoice->getTotalAmount());
                    $transaction->setPaymentCurrency($invoice->getCurrency());
                    $transaction->setPaymentMerchantType('Jambopay');
                    $transaction->setPaymentTestMode("0");

                    $transaction->setBillingState($JP_TRANID);
                    $transaction->setPaymentDate(date("Y-m-d H:i:s"));

                    if($request_details['status'] == 2)
                    {
                        $transaction->setStatus(2);
                        $transaction->setPaymentStatus("paid");
                    }
                    elseif($request_details['status'] == 1)
                    {
                        $transaction->setStatus(1);
                        $transaction->setPaymentStatus("failed");
                    }
                    else
                    {
                        $transaction->setStatus(15);
                        $transaction->setPaymentStatus("pending");
                    }

                    $transaction->save();
                }

                error_log("JamboPay: ".$request_details['status']."/".$this->invoice_manager->get_invoice_total_owed($invoice->getId()));

                if($request_details['status'] == 2 && $this->invoice_manager->get_invoice_total_owed($invoice->getId()) <= 0)
                {
                    //Payment is successful
                    $invoice->setPaid(2);
                    $invoice->setUpdatedAt(date("Y-m-d"));
                }
                elseif($request_details['status'] == 1)
                {
                    //Payment has failed
                    $invoice->setPaid(3);
                    $invoice->setUpdatedAt(date("Y-m-d"));
                }
                else
                {
                    //Payment is incomplete
                    $invoice->setPaid(15);
                    $invoice->setUpdatedAt(date("Y-m-d"));
                }

                //Update invoice and allow any triggers to take place
                $invoice->save();

                error_log("JamboPay - Debug-x: Successful Validation - ".$invoice->getFormEntry()->getApplicationId());

                return true;
            }
            else
            {
                error_log("JamboPay - Debug-x: MISMATCHED JP PASSWORD - ".$invoice->getFormEntry()->getApplicationId());
                return false;
            }
        }
        else
        {
            error_log("JamboPay - Debug-x: MISSING JP PASSWORD - ".$invoice->getFormEntry()->getApplicationId());
            return false;
        }
    }

    //Process payment notifications received from external payment server
    public function ipn($request_details)
    {
        $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

        if (isset($request_details['JP_PASSWORD'])) {

            $JP_TRANID = $request_details['JP_TRANID'];
            $JP_MERCHANT_ORDERID = $request_details['JP_MERCHANT_ORDERID'];
            $JP_ITEM_NAME = $request_details['JP_ITEM_NAME'];
            $JP_AMOUNT = $request_details['JP_AMOUNT'];
            $JP_CURRENCY = $request_details['JP_CURRENCY'];
            $JP_TIMESTAMP = $request_details['JP_TIMESTAMP'];
            $JP_PASSWORD = $request_details['JP_PASSWORD'];
            $JP_CHANNEL = $request_details['JP_CHANNEL'];

            $invoice = $this->invoice_manager->get_invoice_by_reference($JP_MERCHANT_ORDERID);

            $exploded = explode('/', $JP_MERCHANT_ORDERID);
            $form_id  		 = (int) $exploded[0];

            $sql = "SELECT * FROM ap_forms WHERE form_id = ".$form_id;
            $results = mysql_query($sql, $db_connection);

            $payment_settings = mysql_fetch_assoc($results);

            //$sharedkey, IS ONLY SHARED BETWEEN THE MERCHANT AND JAMBOPAY. THE KEY SHOULD BE SECRET ********************

            //Make sure you get the key from JamboPay Support team
            $sharedkey = $payment_settings['payment_jambopay_shared_key'];

            $str = $JP_MERCHANT_ORDERID . $JP_AMOUNT . $JP_CURRENCY . $sharedkey . $JP_TIMESTAMP;


            //**************** VERIFY *************************
            if (md5(utf8_encode($str)) == $JP_PASSWORD) {

                $q = Doctrine_Query::create()
                   ->from("ApFormPayments a")
                   ->where("a.form_id = ? AND a.record_id = ?", array($invoice->getFormEntry()->getFormId(), $invoice->getFormEntry()->getEntryId()))
                   ->andWhere("a.status <> ?", 2)
                   ->andWhere("a.payment_amount = ?", $invoice->getTotalAmount());
                $transaction = $q->fetchOne();

                if($transaction)
                {
                    //Update transaction details
                    $transaction->setBillingState($JP_TRANID);
                    $transaction->setPaymentDate(date("Y-m-d H:i:s"));

                    if($request_details['status'] == 2)
                    {
                        $transaction->setStatus(2);
                        $transaction->setPaymentStatus("paid");
                    }
                    elseif($request_details['status'] == 1)
                    {
                        $transaction->setStatus(1);
                        $transaction->setPaymentStatus("failed");
                    }
                    else
                    {
                        $transaction->setStatus(15);
                        $transaction->setPaymentStatus("pending");
                    }

                    $transaction->save();
                }
                else
                {
                    $application = $invoice->getFormEntry();

                    $user = Doctrine_Core::getTable('SfGuardUser')->find(array($application->getUserId()));

                    $fullname = $user->getProfile()->getFullname();

                    //Add a new transaction if one doesn't exist
                    $transaction = new ApFormPayments();
                    $transaction->setFormId($invoice->getFormEntry()->getFormId());
                    $transaction->setRecordId($invoice->getFormEntry()->getEntryId());
                    $transaction->setPaymentId($invoice->getFormEntry()->getFormId()."/".$invoice->getFormEntry()->getEntryId()."/".$invoice->getId());
                    $transaction->setDateCreated(date("Y-m-d H:i:s"));
                    $transaction->setPaymentFullname($fullname);
                    $transaction->setPaymentAmount($invoice->getTotalAmount());
                    $transaction->setPaymentCurrency($invoice->getCurrency());
                    $transaction->setPaymentMerchantType('Jambopay');
                    $transaction->setPaymentTestMode("0");

                    $transaction->setBillingState($JP_TRANID);
                    $transaction->setPaymentDate(date("Y-m-d H:i:s"));

                    if($request_details['status'] == 2)
                    {
                        $transaction->setStatus(2);
                        $transaction->setPaymentStatus("paid");
                    }
                    elseif($request_details['status'] == 1)
                    {
                        $transaction->setStatus(1);
                        $transaction->setPaymentStatus("failed");
                    }
                    else
                    {
                        $transaction->setStatus(15);
                        $transaction->setPaymentStatus("pending");
                    }

                    $transaction->save();
                }

                error_log("JamboPay: ".$request_details['status']."/".$this->invoice_manager->get_invoice_total_owed($invoice->getId()));

                if($request_details['status'] == 2 && $this->invoice_manager->get_invoice_total_owed($invoice->getId()) <= 0)
                {
                    //Payment is successful
                    $invoice->setPaid(2);
                    $invoice->setUpdatedAt(date("Y-m-d"));
                }
                elseif($request_details['status'] == 1)
                {
                    //Payment has failed
                    $invoice->setPaid(3);
                    $invoice->setUpdatedAt(date("Y-m-d"));
                }
                else
                {
                    //Payment is incomplete
                    $invoice->setPaid(15);
                    $invoice->setUpdatedAt(date("Y-m-d"));
                }

                //Update invoice and allow any triggers to take place
                $invoice->save();

                error_log("JamboPay - Debug-x: Successful Validation - ".$invoice->getFormEntry()->getApplicationId());

                return true;
            }
            else
            {
                error_log("JamboPay - Debug-x: MISMATCHED JP PASSWORD - ".$invoice->getFormEntry()->getApplicationId());
                return false;
            }
        }
        else
        {
            error_log("JamboPay - Debug-x: MISSING JP PASSWORD - ".$invoice->getFormEntry()->getApplicationId());
            return false;
        }
    }
}
