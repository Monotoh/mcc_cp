<?php
/**
 *
 * Class for managing PHPLive Chat
 *
 */

class LiveChat {

    public function LiveChat()
    {
    }

    public function isOperator($user_id){
		$q = Doctrine_Query::create()
			->from('CfUser a')
			->where('a.nid = ?', $user_id);
		$user = $q->fetchOne();
		$q = Doctrine_Manager::getInstance()->getCurrentConnection();
		$existing_operator = $q->fetchAssoc("SELECT * FROM p_operators WHERE login = '".$user->getNid()."' and password <> '-1' LIMIT 1");

		if ($existing_operator) {
			return true;
		}else{
			return false;
		}
	}

    public function isInactiveOperator($user_id){
		$q = Doctrine_Query::create()
			->from('CfUser a')
			->where('a.nid = ?', $user_id);
		$user = $q->fetchOne();
		$q = Doctrine_Manager::getInstance()->getCurrentConnection();
		$existing_operator = $q->fetchAssoc("SELECT * FROM p_operators WHERE login = '".$user->getNid()."' and password = '-1' LIMIT 1");

		if ($existing_operator) {
			return true;
		}else{
			return false;
		}
	}

    public function addOperator($user_id){
		$q = Doctrine_Query::create()
			->from('CfUser a')
			->where('a.nid = ?', $user_id);
		$user = $q->fetchOne();
		$q = Doctrine_Manager::getInstance()->getCurrentConnection();
		$existing_operator = $q->fetchAssoc("SELECT * FROM p_operators WHERE login = '".$user->getNid()."' LIMIT 1");

		if ($user and !$existing_operator) {
			$result = $q->execute(" INSERT INTO p_operators (lastactive, lastrequest, status, mapper, mapp, signall, rate, op2op, traffic, viewip, nchats, maxc, canID, ses, ces, rating, sms, smsnum, login, password, name, email, pic, theme) VALUES ('', '', 0, 0, 0, 0, 1, 1, 1, 0, 0, -1, 0, '', '', 0, 0, '', '".$user->getNid()."', '".md5($user->getStrpassword())."', '".$user->getStrfirstname()." ".$user->getStrlastname()."', '".$user->getStremail()."', '', '')");

			if($user->getStrdepartment()){
				$existing_department = $q->fetchAssoc("SELECT * FROM p_departments WHERE name = '".$user->getStrdepartment()."' LIMIT 1");
				if(!$existing_department){//Only save if department does not exist
					$result = $q->execute("INSERT INTO p_departments (visible, queue, tshare, texpire, rquestion, remail, temail, temaild, rtype, rtime, rloop, savem, custom, smtp, lang, name, email, emailt, emailt_bcc, msg_greet, msg_offline, msg_busy, msg_email) VALUES (1, 0, 1, 0, 1, 1, 1, 0, 2, 45, 1, 3, '', '', 'english', '".$user->getStrdepartment()."', '".sfConfig::get('app_organisation_email')."', '', 0, 'An agent will be with you shortly. Thank you for your patience.', 'Agents are not available at this time. Please leave a message.', 'Agents are not available at this time. Please leave a message.', 'Hi %%visitor%%,\r\n\r\nThank you for taking the time to chat with us.  Here is the complete chat transcript for your reference:\r\n\r\n%%transcript%%\r\n\r\nThank you,\r\n%%operator%%\r\n%%op_email%%\r\n')");
				}
				$operator = $q->fetchAssoc("SELECT * FROM p_operators WHERE login = '".$user->getNid()."' LIMIT 1");
				$chat_department = $q->fetchAssoc("SELECT * FROM p_departments WHERE name = '".$user->getStrdepartment()."' LIMIT 1");

				$result = $q->execute("INSERT INTO p_dept_ops (deptID, opID, display, visible, status) VALUES (".$chat_department[0]['deptID'].", ".$operator[0]['opID'].", 1, 1, 0)");
			}
			$audit = new Audit();
			$audit->saveAudit("","Live Chat reviewer: ".$user->getStrfirstname()." ".$user->getStrlastname()." (".$user->getStremail().")");
		}else if ($user) {
			$result = $q->execute(" Update p_operators set password ='".md5($user->getStrpassword())."', name='".$user->getStrfirstname()." ".$user->getStrlastname()."', email='".$user->getStremail()."' where login ='".$user->getNid()."'");
		}
	}

    public function updateOperator($user_id){
		$q = Doctrine_Query::create()
			->from('CfUser a')
			->where('a.nid = ?', $user_id);
		$user = $q->fetchOne();
		$q = Doctrine_Manager::getInstance()->getCurrentConnection();
		$existing_operator = $q->fetchAssoc("SELECT * FROM p_operators WHERE login = '".$user->getNid()."' LIMIT 1");

		if ($user and $existing_operator) {
			$result = $q->execute(" Update p_operators set password ='".md5($user->getStrpassword())."', name='".$user->getStrfirstname()." ".$user->getStrlastname()."', email='".$user->getStremail()."' where login ='".$user->getNid()."' and password <> '-1'");

			$audit = new Audit();
			$audit->saveAudit("","Updated Live Chat reviewer: ".$user->getStrfirstname()." ".$user->getStrlastname()." (".$user->getStremail().")");
		}
	}

    public function deactivateOperator($user_id){
		$q = Doctrine_Query::create()
			->from('CfUser a')
			->where('a.nid = ?', $user_id);
		$user = $q->fetchOne();
		$q = Doctrine_Manager::getInstance()->getCurrentConnection();
		$existing_operator = $q->fetchAssoc("SELECT * FROM p_operators WHERE login = '".$user->getNid()."' LIMIT 1");

		if ($user and $existing_operator) {
			$result = $q->execute("Update p_operators set password ='-1' where login ='".$user->getNid()."'");

			$audit = new Audit();
			$audit->saveAudit("","Updated Live Chat reviewer: ".$user->getStrfirstname()." ".$user->getStrlastname()." (".$user->getStremail().")");
		}
	}

	//PHPLive Login and return landing URL
    public function PHPLiveLogin(){
		//Login to PHP Live Chat

		$q = Doctrine_Query::create()
			->from('CfUser a')
			->where('a.nid = ?', $_SESSION['SESSION_CUTEFLOW_USERID']);
        $user = $q->fetchOne();
		
		//Check if request is https or http
		$suffix = "s";
		 if (empty($_SERVER['HTTPS'])) {
            $suffix = "";
        }

		
		if ($user){
			$username = $user->getNid();
			$hashpassword = $user->getStrpassword();
		}

		$now = time();
		$ip = array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];//Get IP as HTTP_X_FORWARDED_FOR if from a proxy server, else get REMOTE_ADDR
		$ses = md5( $now.$ip );
		$graburl = "http".$suffix."://".$_SERVER['HTTP_HOST']."/chat/phplive/index.php?action=submit&auto=0&wp=0&mapp=0&menu=operator&wpress=0&phplive_password=".md5(md5($hashpassword))."&phplive_login=".$username."&phplive_password_temp=".$hashpassword;

		$q = Doctrine_Manager::getInstance()->getCurrentConnection();
		$operator = $q->fetchAssoc("SELECT * FROM p_operators WHERE login = '".$username."' LIMIT 1");		
		if ($operator){
			setcookie( "phplive_opID", $operator[0]['opID'], $now+(60*60*24*90), "/" ) ;//Set phplive_opID cookie value for windows
			$opID = $operator[0]['opID'];//For ubuntu set phplive_opID cookie value in curl (CURLOPT_COOKIE)
		}

		$output = $this->get_php_live_page($graburl,$opID,$ip);//Login

		return "http".$suffix."://".$_SERVER['HTTP_HOST']."/chat/phplive/ops/?auto=0&wp=0&1464266163&ses=".$ses;
	}

    protected function get_php_live_page( $url, $opID, $ip)
    {
        $user_agent=$_SERVER['HTTP_USER_AGENT'];
        $header = "X-Forwarded-For: {$ip}, {$_SERVER['SERVER_ADDR']}";

        $options = array(
			CURLOPT_COOKIE         =>"phplive_cookie_check=1;phplive_opID=".$opID,
            CURLOPT_CUSTOMREQUEST  =>"GET",        //set request type post or get
            CURLOPT_POST           =>false,        //set to GET
            CURLOPT_USERAGENT      => $user_agent, //set user agent
            CURLOPT_COOKIEFILE     =>"cookie.txt", //set cookie file
            CURLOPT_COOKIEJAR      =>"cookie.txt", //set cookie jar
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
			CURLOPT_HTTPHEADER => array($header),//Make sure curl request passes IP from client Machine
        );

        $ch      = curl_init( $url );
        curl_setopt_array( $ch, $options );
        $content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );

        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['content'] = $content;
        return $header;
    }
}
