<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OTBHelper
 *
 * @author boniboy
 */
class OTBHelper implements Swift_CharacterReader
{

    /** get Invoice details
     * Assumption: Application has one invoice pending at a time
     */
    public function getInvoiceInfo($app_id){
       
        $q = Doctrine_Query::create()
                    ->from("MfInvoice m")
                    ->where("m.app_id = ? ", $app_id)
                    ->addwhere("m.paid = ? ", 1);
            $invoice_details = $q->fetchOne();
            //
         return $invoice_details ;


    }

    
    /**
     * Assign Task to all users in a department
     */
    public function AssignTaskToDepartmentReviewers($department_head_id,$task_type,$creater_id,
    $task_sheet,$start_date,$end_date,$priority,$remarks,$application)
    {
      error_log("Debug:: Search for All users in department head ".$department_head_id);
      $q = Doctrine_Query::create()
      ->from('CfUser a')
      ->where('a.nid = ?', $department_head_id);
       $treviewer = $q->fetchOne();
      //
      error_log("<<<<<<< Assign task to >>>>>".$treviewer->getStrdepartment());
       $q2 = Doctrine_Query::create()
       ->from('CfUser a')
                 ->where('a.bdeleted = 0')
                 ->andWhere('a.strdepartment LIKE ?', '%'.$treviewer->getStrdepartment().'%') ; 
   $reviewers = $q2->execute();
   //
   $this->task = null;
   foreach($reviewers as $reviewer)
   {
        error_log("<<<<<<< Assign task to >>>>>".$reviewer->getNid());
        $task = new Task();
        $task->setType($task_type);
        $task->setCreatorUserId($creater_id);
        $task->setOwnerUserId($reviewer->getNid());
        $task->setSheetId($task_sheet);
        $task->setDuration("0");
        $task->setStartDate($start_date);
        $task->setEndDate($end_date);
        $task->setPriority($priority);
        $task->setIsLeader("0");
        $task->setActive("1");
        $task->setStatus("1");
        $task->setLastUpdate(date('Y-m-d'));
        $task->setDateCreated(date('Y-m-d'));
        $task->setRemarks($remarks);
        $task->setApplicationId($application);
        $task->save();
        $this->task = $task;
        $previous_task_id = $task->getId();
       // $this->success = true;

        

        /*$q = Doctrine_Query::create()
                ->from('FormEntry a')
                ->where('a.id = ?', $application);
        $fullApplication = $q->fetchOne();*/

       /* $q = Doctrine_Query::create()
                ->from('CfUser a')
                ->where('a.nid = ?', $reviewer);
        $treviewer = $q->fetchOne();*/

        //$audit = new Audit();
       // $audit->saveFullAudit("<a href='/backend.php/tasks/view/id/" . $task->getId() . "'>Assigned " . $task->getTypeName() . " task on " . $fullApplication->getApplicationId() . " to " . $treviewer->getStrfirstname() . " " . $treviewer->getStrlastname() . "</a>", $task->getId(), "task", "", "Pending", $application);

          }
          return $this->task ;

        
    
    }

     /**
     *  Function to automatically complete other tasks in users within
     *  same department
     */
    public function task_manager($application_id,$user_ids)
    {
       //error_log("task_manager >>>>>>>>> ".implode($user_ids)) ;
        $update_tasks = Doctrine_Query::create()
                ->UPDATE('Task t')
                ->SET('t.status', '?', 25)
                ->WHERE('t.owner_user_id in  ('.implode(',', $user_ids).')')
                ->andWhere("t.application_id = ? ", $application_id);

        $update_tasks_res = $update_tasks->execute();
    }
    /**
     * Return list of reviwers in same department
     */
    public function reviewers_list($user_id){
        //related users ids
        $user_ids = array();
        //
        $q = Doctrine_Query::create()
        ->from('CfUser a')
                  ->where('a.bdeleted = 0')
                  //->andWhere('a.nid','?',$user_id);
                  ->andWhere("a.nid = ? ", $user_id);
        $reviewer = $q->fetchOne();
        if($reviewer){
                //
                $reviewer_dept = $reviewer->getStrdepartment() ;
                //get list of all users in this department
                $q2 = Doctrine_Query::create()
                                        ->from('CfUser a')
                                                ->where('a.bdeleted = 0')
                                                ->andWhere('a.strdepartment LIKE ?', '%'.$reviewer_dept.'%')
                                                ->orderBy('a.Strfirstname ASC');
                $all_reviewers = $q2->execute();
                
                foreach($all_reviewers as $r){
                    array_push($user_ids,$r->getNid()) ;
                }
                return $user_ids ;
            }else {
                return false;
            }

    }

    //mail
    /*const MAP_TYPE_INVALID = 0x01;
  const MAP_TYPE_FIXED_LEN = 0x02;
  const MAP_TYPE_POSITIONS = 0x03;*/ 
  const MAP_TYPE_POSITIONS_CUSTOM = 0x03 ;


    /** Pre-computed for optimization */
  private static $length_map=array(
    //N=0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,
        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, //0x0N
        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, //0x1N
        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, //0x2N
        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, //0x3N
        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, //0x4N
        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, //0x5N
        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, //0x6N
        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, //0x7N
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, //0x8N
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, //0x9N
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, //0xAN
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, //0xBN
        2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2, //0xCN
        2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2, //0xDN
        3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3, //0xEN
        4,4,4,4,4,4,4,4,5,5,5,5,6,6,0,0  //0xFN
     );
      private static $s_length_map=array(
      "\x00"=>1, "\x01"=>1, "\x02"=>1, "\x03"=>1, "\x04"=>1, "\x05"=>1, "\x06"=>1, "\x07"=>1,
      "\x08"=>1, "\x09"=>1, "\x0a"=>1, "\x0b"=>1, "\x0c"=>1, "\x0d"=>1, "\x0e"=>1, "\x0f"=>1,
      "\x10"=>1, "\x11"=>1, "\x12"=>1, "\x13"=>1, "\x14"=>1, "\x15"=>1, "\x16"=>1, "\x17"=>1,
      "\x18"=>1, "\x19"=>1, "\x1a"=>1, "\x1b"=>1, "\x1c"=>1, "\x1d"=>1, "\x1e"=>1, "\x1f"=>1,
      "\x20"=>1, "\x21"=>1, "\x22"=>1, "\x23"=>1, "\x24"=>1, "\x25"=>1, "\x26"=>1, "\x27"=>1,
      "\x28"=>1, "\x29"=>1, "\x2a"=>1, "\x2b"=>1, "\x2c"=>1, "\x2d"=>1, "\x2e"=>1, "\x2f"=>1,
      "\x30"=>1, "\x31"=>1, "\x32"=>1, "\x33"=>1, "\x34"=>1, "\x35"=>1, "\x36"=>1, "\x37"=>1,
      "\x38"=>1, "\x39"=>1, "\x3a"=>1, "\x3b"=>1, "\x3c"=>1, "\x3d"=>1, "\x3e"=>1, "\x3f"=>1,
      "\x40"=>1, "\x41"=>1, "\x42"=>1, "\x43"=>1, "\x44"=>1, "\x45"=>1, "\x46"=>1, "\x47"=>1,
      "\x48"=>1, "\x49"=>1, "\x4a"=>1, "\x4b"=>1, "\x4c"=>1, "\x4d"=>1, "\x4e"=>1, "\x4f"=>1,
      "\x50"=>1, "\x51"=>1, "\x52"=>1, "\x53"=>1, "\x54"=>1, "\x55"=>1, "\x56"=>1, "\x57"=>1,
      "\x58"=>1, "\x59"=>1, "\x5a"=>1, "\x5b"=>1, "\x5c"=>1, "\x5d"=>1, "\x5e"=>1, "\x5f"=>1,
      "\x60"=>1, "\x61"=>1, "\x62"=>1, "\x63"=>1, "\x64"=>1, "\x65"=>1, "\x66"=>1, "\x67"=>1,
      "\x68"=>1, "\x69"=>1, "\x6a"=>1, "\x6b"=>1, "\x6c"=>1, "\x6d"=>1, "\x6e"=>1, "\x6f"=>1,
      "\x70"=>1, "\x71"=>1, "\x72"=>1, "\x73"=>1, "\x74"=>1, "\x75"=>1, "\x76"=>1, "\x77"=>1,
      "\x78"=>1, "\x79"=>1, "\x7a"=>1, "\x7b"=>1, "\x7c"=>1, "\x7d"=>1, "\x7e"=>1, "\x7f"=>1,
      "\x80"=>0, "\x81"=>0, "\x82"=>0, "\x83"=>0, "\x84"=>0, "\x85"=>0, "\x86"=>0, "\x87"=>0,
      "\x88"=>0, "\x89"=>0, "\x8a"=>0, "\x8b"=>0, "\x8c"=>0, "\x8d"=>0, "\x8e"=>0, "\x8f"=>0,
      "\x90"=>0, "\x91"=>0, "\x92"=>0, "\x93"=>0, "\x94"=>0, "\x95"=>0, "\x96"=>0, "\x97"=>0,
      "\x98"=>0, "\x99"=>0, "\x9a"=>0, "\x9b"=>0, "\x9c"=>0, "\x9d"=>0, "\x9e"=>0, "\x9f"=>0,
      "\xa0"=>0, "\xa1"=>0, "\xa2"=>0, "\xa3"=>0, "\xa4"=>0, "\xa5"=>0, "\xa6"=>0, "\xa7"=>0,
      "\xa8"=>0, "\xa9"=>0, "\xaa"=>0, "\xab"=>0, "\xac"=>0, "\xad"=>0, "\xae"=>0, "\xaf"=>0,
      "\xb0"=>0, "\xb1"=>0, "\xb2"=>0, "\xb3"=>0, "\xb4"=>0, "\xb5"=>0, "\xb6"=>0, "\xb7"=>0,
      "\xb8"=>0, "\xb9"=>0, "\xba"=>0, "\xbb"=>0, "\xbc"=>0, "\xbd"=>0, "\xbe"=>0, "\xbf"=>0,
      "\xc0"=>2, "\xc1"=>2, "\xc2"=>2, "\xc3"=>2, "\xc4"=>2, "\xc5"=>2, "\xc6"=>2, "\xc7"=>2,
      "\xc8"=>2, "\xc9"=>2, "\xca"=>2, "\xcb"=>2, "\xcc"=>2, "\xcd"=>2, "\xce"=>2, "\xcf"=>2,
      "\xd0"=>2, "\xd1"=>2, "\xd2"=>2, "\xd3"=>2, "\xd4"=>2, "\xd5"=>2, "\xd6"=>2, "\xd7"=>2,
      "\xd8"=>2, "\xd9"=>2, "\xda"=>2, "\xdb"=>2, "\xdc"=>2, "\xdd"=>2, "\xde"=>2, "\xdf"=>2,
      "\xe0"=>3, "\xe1"=>3, "\xe2"=>3, "\xe3"=>3, "\xe4"=>3, "\xe5"=>3, "\xe6"=>3, "\xe7"=>3,
      "\xe8"=>3, "\xe9"=>3, "\xea"=>3, "\xeb"=>3, "\xec"=>3, "\xed"=>3, "\xee"=>3, "\xef"=>3,
      "\xf0"=>4, "\xf1"=>4, "\xf2"=>4, "\xf3"=>4, "\xf4"=>4, "\xf5"=>4, "\xf6"=>4, "\xf7"=>4,
      "\xf8"=>5, "\xf9"=>5, "\xfa"=>5, "\xfb"=>5, "\xfc"=>6, "\xfd"=>6, "\xfe"=>0, "\xff"=>0,
     );
    
      /**
       * Returns the complete charactermap
       *
       * @param string $string
       * @param int $startOffset
       * @param array $currentMap
       * @param mixed $ignoredChars
       */
      public function getCharPositions($string, $startOffset, &$currentMap, &$ignoredChars)
      {
        if (!isset($currentMap['i']) || !isset($currentMap['p']))
        {
          $currentMap['p'] = $currentMap['i'] = array();
         }
        $strlen=strlen($string);
        $charPos=count($currentMap['p']);
        $foundChars=0;
        $invalid=false;
        for ($i=0; $i<$strlen; ++$i)
        {
          $char=$string[$i];
          $size=self::$s_length_map[$char];
          if ($size==0)
          {
            /* char is invalid, we must wait for a resync */
            $invalid=true;
            continue;
           }
           else
           {
             if ($invalid==true)
             {
               /* We mark the chars as invalid and start a new char */
               $currentMap['p'][$charPos+$foundChars]=$startOffset+$i;
               $currentMap['i'][$charPos+$foundChars]=true;
               ++$foundChars;
               $invalid=false;
             }
             if (($i+$size) > $strlen){
               $ignoredChars=substr($string, $i);
               break;
             }
             for ($j=1; $j<$size; ++$j)
             {
              $char=$string[$i+$j];
              if ($char>"\x7F" && $char<"\xC0")
              {
                // Valid - continue parsing
              }
              else
              {
                /* char is invalid, we must wait for a resync */
                $invalid=true;
                continue 2;
              }
             }
             /* Ok we got a complete char here */
             $currentMap['p'][$charPos+$foundChars]=$startOffset+$i+$size;
             $i+=$j-1;
             ++$foundChars;
           }
        }
        return $foundChars;
      }
      
      /**
       * Returns mapType
       * @return int mapType
       */
      public function getMapType()
      {
        return self::MAP_TYPE_POSITIONS_CUSTOM;
      }
     
      /**
       * Returns an integer which specifies how many more bytes to read.
       * A positive integer indicates the number of more bytes to fetch before invoking
       * this method again.
       * A value of zero means this is already a valid character.
       * A value of -1 means this cannot possibly be a valid character.
       * @param string $bytes
       * @return int
       */
      public function validateByteSequence($bytes, $size)
      {
        if ($size<1){
          return -1;
        }
        $needed = self::$length_map[$bytes[0]] - $size;
        return ($needed > -1)
          ? $needed
          : -1
          ;
      }
    
      /**
       * Returns the number of bytes which should be read to start each character.
       * @return int
       */
      public function getInitialByteSize()
      {
        return 1;
      }

      public function otbtestcall(){
          error_log("Otb ok !!!!!!!!!!!") ;
      }
    // end mail
    
    /**
     * Get stage type id
     */
    public function getStageType($stage_id){
        $q = Doctrine_Query::create()
                ->from('SubMenus s')
                ->where('s.id = ? ', $stage_id) ;
        $res = $q->fetchOne() ;
        if($res){
            return $res->getStageType();
        }else {
            return false ;
        }
    }
    
    /**
     * Return Form type value
     * 0 = General
     * 1 = Application form
     * 2 = Comment Form
     * 3 = Member Form
     */
    public function getFormTypeValue($form_id){
        $q = Doctrine_Query::create()
                ->from('ApForms f')
                ->where('f.form_id = ? ', $form_id) ;
        //
        $res = $q->fetchOne();
        //
        if($res){
            
            $form_type_id = $res->getFormType();
            return $form_type_id ;
        }else{
            return false;
        }
    }
    
    
    
    /**
     * 
     * @param type $dbh
     * @param type $form_id
     * @param type $integration
     * @return type
     * Only include files once
     */
    public function getCustomLogicFields($dbh, $form_id,$integration=False){
		if(!$dbh){
			$prefix_folder = "vendor/cp_machform/";
			require_once($prefix_folder . 'includes/db-core.php');
			require_once($prefix_folder . 'config.php');
			$dbh = mf_connect_db();			
		}
		//get the target elements for the current page
		$query = "SELECT 
					A.id,
					A.element_id,
					A.rule_all_any,
					A.integration_logic,
					B.element_title,
					B.element_page_number,
					B.element_type
				FROM 
					".MF_TABLE_PREFIX."dependency_logic A LEFT JOIN ".MF_TABLE_PREFIX."form_elements B
				  ON 
				  	A.form_id = B.form_id and A.element_id=B.element_id and B.element_status = 1
			   WHERE
					A.form_id = ?";
		$query .= $page_number ? " and B.element_page_number = ?" : "";
		$query .= "ORDER BY 
					B.element_position asc";

		$params = $page_number ? array($form_id,$page_number) : array($form_id);

		$sth = mf_do_query($query,$params,$dbh);

		$logic_elements_array = array();
		
		while($row = mf_do_fetch_result($sth)){
			//$element_id = (int) $row['element_id'];
			$id = (int) $row['id'];
			if($row['integration_logic'] or !$integration){
				$logic_elements_array[$id]['element_id']  = $row['element_id'];
				$logic_elements_array[$id]['integration_logic']  = $row['integration_logic'];
				$logic_elements_array[$id]['element_title']  = $row['element_title'];
				$logic_elements_array[$id]['rule_all_any']   = $row['rule_all_any'];
				$logic_elements_array[$id]['element_type']   = $row['element_type'];
			}
		}
		return $logic_elements_array;
	}
    
    /**
     * Get plan UPI number
     */
    public function getApplicationUPI($form_id,$application_id){
        try{
        $appdatas = Doctrine_Manager::getInstance()->getCurrentConnection()->execute("SELECT * FROM ap_form_".$form_id." WHERE id = " . $application_id);
      //  $formmanager = new FormManager();
        $lais_integration_logic = $this->getCustomLogicFields(False, $form_id,True);
        $element_data = array_shift($lais_integration_logic);
        //
        $upi = 'N/A';
        foreach($appdatas as $appdata)
          {
            $upi = $appdata['element_'.$element_data['element_id']]; 
          }
          return $upi;
        }catch(Exception $ex){
            error_log("Debug: Error with getApplicationUPI() ".$ex->getMessage());
            return false;
        }
        
    }
    
     /**
     * Check if invoice allows manual/online payments options
     * this function for mombasa case is used as an override
     */
    public function invoicePermitsMultiplePayment($invoice_id) {
       // error_log("Invoice to check >>> ".$invoice_id);
        $q = Doctrine_Query::create()
                ->from('MfInvoice f')
                ->where('f.id = ? ', $invoice_id);
        $res = $q->fetchOne();
        if ($res) {
            return $res->getAllowOnlineOrManualPayment(); // 1 is override, 0 is no override
        } else {
            return 0; //no override 0
        }
    }
     /**
     * Get Form set payment Merchant
     */
    public function getFormMerchant($form_id) {
        $q = Doctrine_Query::create()
                ->from("ApForms a")
                ->where("a.form_active = 1")
                ->andWhere("a.payment_enable_merchant = 1")
                ->andWhere("a.form_id = ?", $form_id);
        $form = $q->fetchOne();
        if ($form) {
            return $form->getPaymentMerchantType();
        } else {
            return false;
        }
    }
    /**
     * Check if there are pending tasks 
     * 
     */
    public function checkPendingTasks($application_id){
        try{
              $q = Doctrine_Query::create()
                ->from('Task t')
                ->where('t.application_id = ?',$application_id) 
                ->andWhere('t.status = ?', 1); //pending
              $q_res = $q->execute();
              if(count($q_res) > 0){
                  return true;
              }else {
                  return false;
              }
        }catch(Exception $ex){
            error_log("Error checking pending tasks ".$ex->getMessage());
            return false;
        }
        
    }
    /**
     * Validate supllied UPI
     * Check if we have an application with existing value of upi and blok user submission
     * 
     */
    public function validateUPI($upi,$form_id,$element_id){
        
             $query = "select id from ap_form_".$form_id." where ".$element_id." like '%$upi%' order by id desc limit 1 " ;
             error_log("Dumb! >> ".$query);
            //prevent possibility of sql injections / strip the query
            $query_results = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($query) ;
            $entry_id = null ;
            //
            foreach($query_results as $r){
                //error_log(" Existing entries with the plot >>>> ".$r['id']);
                $entry_id = $r['id'] ;
            }
            //get application stage using entry id provided.
            $q2 = Doctrine_Query::create()
                    ->from('FormEntry f')
                    ->where('f.entry_id = ?',$entry_id)
                    ->andWhere('f.form_id = ?',$form_id);
            $q2_r = $q2->fetchOne();
            //get the approved, check type of this stage , if 6 rejected, allow the user to submit
            if($q2_r){
                 $stage_id = $q2_r->getApproved();
          //  error_log("entry id ".$entry_id);
           // error_log("form id ".$form_id);
            }
           
            error_log("Stage ID >>> ".$stage_id);
           $validatity = null; 
            //drafts
            if($stage_id == 0){
                $validatity = "valid" ; // allow user to submit
            }else {
                    $q3 = Doctrine_Query::create()
                            ->from('SubMenus s')
                            ->where('s.id = ?', $stage_id) ;
                    $q3_r = $q3->fetchOne() ;
                    //check type
                    //|| $q3_r->getStageType() == 5
                    if($q3_r->getStageType() == 6 ){
						error_log("Stage type >>> ".$q3_r->getStageType());
                        $validatity = "valid" ;
                    }else {
                        $validatity = "invalid";
                    }
            }
          //  error_log("Validating UPI submitted by user >>>> ".$validatity) ;
            return $validatity ;
    }
    
    /**
     * Validate UPI format
     */
    public function validateUPIFormat($upi)
    {
        //Example format is = 1/02/04/01/2298
        //more at http://stackoverflow.com/questions/3145264/regular-expression-and-forward-slash
        //match UPI format
	 if(preg_match('@^([0-9]+)/(\d+)/(\d+)/(\d+)/\d+@', $upi))
	 { 
             return true;
         }
	 else {
	 return false;		 
	 }
        
    }
    /**
     * Get a List of of submenus with extra property set
     * can be used to get inspections and occupation stages
     */
    public function getStageWithRequestExtraType($type = 0 ){
        $q = Doctrine_Query::create()
                ->from('SubMenus s')
                ->where('s.extra_type = ?', $type) ;
        $r_res = $q->execute();
        $sub_menus = array() ;
        foreach ($r_res as $r){
           // error_log("The stage is ".$r['id']) ;
            array_push($sub_menus, $r['id']) ;
        }
        return $sub_menus ;
    }
    
    /**
     * Thomas function to format data
     */
    public function bd_nice_number($n) {
    // first strip any formatting;
    $n = (0+str_replace(",","",$n));

    // is this a number?
    if(!is_numeric($n)) return false;

    // now filter it;
    if($n>1000000000000) return round(($n/1000000000000),1).' T';
    else if($n>1000000000) return round(($n/1000000000),1).' B';
    else if($n>1000000) return round(($n/1000000),1).' M';

    return number_format($n);
    }
    
    /**
     * 
     * @param type $user_id
     * @return type
     * Function to convert interger to equivalent roman number
     * 
     */
    public function integerToRoman($integer)
     {
             // Convert the integer into an integer (just to make sure)
             $integer = intval($integer);
             $result = '';

             // Create a lookup array that contains all of the Roman numerals.
             $lookup = array('M' => 1000,
             'CM' => 900,
             'D' => 500,
             'CD' => 400,
             'C' => 100,
             'XC' => 90,
             'L' => 50,
             'XL' => 40,
             'X' => 10,
             'IX' => 9,
             'V' => 5,
             'IV' => 4,
             'I' => 1);

             foreach($lookup as $roman => $value){
              // Determine the number of matches
              $matches = intval($integer/$value);

              // Add the same number of characters to the string
              $result .= str_repeat($roman,$matches);

              // Set the integer to be the remainder of the integer and the value
              $integer = $integer % $value;
             }

             // The Roman numeral should be built, return it
             return $result;
            }
    
    public function getReviewerDetails2($user_id) {
        try {
            $q = Doctrine_Query::create()
                    ->from("CfUser u")
                    ->where("u.nid = ? ", $user_id);
            $q_res = $q->fetchOne();
            return $q_res;
        } catch (Exception $ex) {
            error_log("getReviewerDetails() Error " . $ex->getMessage());
        }
    }
    
    /**
     * Get Application previous submitted  entry_id to 
     * display files previously attached
     */
    public function getPreviousSubmittedAppEntryId($entry_id,$form_id){
        //First get previous application number / id
        $previous_sub_q = Doctrine_Query::create()
                    ->from("FormEntry f")
                    ->where("f.entry_id = ?",$entry_id)
                     ->andWhere("f.form_id = ?", $form_id) ;
        $previous_sub_res = $previous_sub_q->fetchOne();
        //get entry id of the previous record
        $entry_id_q = Doctrine_Query::create()
            ->from("FormEntry f")
                ->where("f.id = ? ",$previous_sub_res->getPreviousSubmission()) ;
        $entry_id_res = $entry_id_q->fetchOne();
        if($entry_id_res){
             //
             return $entry_id_res->getEntryId();
        }else {
            return 0;
        }
       
                
    }
    //Duplicating a Record - Webmasters way
    public function DuplicateMySQLRecord ($table, $id_field, $id) {
        $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

        // load the original record into an array
        $result = mysql_query("SELECT * FROM {$table} WHERE {$id_field}={$id}");
        $original_record = mysql_fetch_assoc($result);

        // insert the new record and get the new auto_increment id
        mysql_query("INSERT INTO {$table} (`{$id_field}`) VALUES (NULL)",$db_connection);
        $new_id = mysql_insert_id();

        // generate the query to update the new record with the previous values
        $query = "UPDATE {$table} SET ";
        foreach ($original_record as $key => $value) {
            if ($key != $id_field) {
                $query .= '`'.$key.'` = "'.str_replace('"','\"',$value).'", ';
            }
        }
        $query = substr($query,0,strlen($query)-2); // lop off the extra trailing comma
        $query .= " WHERE {$id_field}={$new_id}";
        mysql_query($query,$db_connection) or die(mysql_error());

        // return the new id
        return $new_id;
    }
     //Duplicating a Record - The OTB way - tHIS FUNCTION IS INCOMPLETE!!! Todo
    public function DuplicateMySQLRecordOTB ($table, $id_field, $id) {
        $db_connection = mysqli_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysqli_select_db($db_connection,sfConfig::get('app_mysql_db'));
        //
        $connection = Doctrine_Manager::getInstance()->getCurrentConnection();     
        // load the original record into an array
        $query1 = "SELECT * FROM $table WHERE $id_field = $id ";
       // $result = mysqli_query("SELECT * FROM $table WHERE $id_field = $id ");
        $original_record = $connection->execute($query1);
        //$original_record = mysqli_fetch_assoc($result);
        // insert the new record and get the new auto_increment id
       // mysqli_query("INSERT INTO {$table} (`{$id_field}`) VALUES (NULL)",$db_connection);
       // $new_id = mysqli_insert_id($db_connection);
       error_log("Select Query >>> ".$query1);
        $columns = array() ;
       
        /** 
         * Customs
         */
         //create temp table
         $create_temp = "CREATE TEMPORARY TABLE tmptable_1 $query1";
         $update_temp = "UPDATE tmptable_1 SET id = NULL" ;
         //execute the queries
         $connection->execute($create_temp);
         $connection->execute($update_temp);
         //insert
         $insert_stat = "INSERT INTO table SELECT * FROM tmptable_1 " ;
         //DROP TEMPORARY TABLE IF EXISTS tmptable_1;
          error_log("New insert >>>> ".$insert_stat);
        
        
        
        
        
        
        
        
        foreach ($original_record as $key => $value) {
          
            if ($key != $id_field) {
                array_push($columns, $key) ;
            }
        }     
         $query_error_fix = "INSERT INTO $table (".implode(',',$columns).") $query1" ; 
         error_log("Test insert selecting from another table >>> ".$query_error_fix);
        $query2 = "INSERT INTO $table $id_field VALUES (NULL)" ;
        $new_id = $connection->execute($query2);
        //test
       
        error_log("Query 2 >>> ".$query2);
      

        // generate the query to update the new record with the previous values
        $query = "UPDATE {$table} SET ";
        foreach ($original_record as $key => $value) {
            if ($key != $id_field) {
                $query .= '`'.$key.'` = "'.str_replace('"','\"',$value).'", ';
            }
        }
        $query = substr($query,0,strlen($query)-2); // lop off the extra trailing comma
        $query .= " WHERE {$id_field}={$new_id}";
         error_log("Passed >>> ".$query) ;
        mysqli_query($db_connection,$query) or die(mysqli_error($db_connection));
          error_log("Passed 2 >>> ".$query) ;    
        // return the new id
        return $new_id;
    }
    
      //Returns an existing application
    public function get_application_by_id($application_id)
    {
        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.id = ?', $application_id)
            ->orderBy("a.application_id DESC")
            ->limit(1);
        $existing_app = $q->fetchOne();
        if($existing_app)//Already submitted then tell client its already submitted
        {
            return $existing_app;
        }
        else
        {
            return false;
        }
    }
    
    //OTB patch - Create previously submitted files and content on re-submission request
     public function create_parent_on_resubmission_request($application_id){
         //only execute this if an application was declined
          $submission = $this->get_application_by_id($application_id);
          if($submission->getDeclined() == "1" && $submission->getParentSubmission() == "0")
           {
            error_log("Have been executed now >>>>>>>>>>>>>>>>>>> table = "."ap_form_".$submission->getFormId()." Entry id = ".$submission->getEntryId()) ;
            $new_id = $this->DuplicateMySQLRecord ("ap_form_".$submission->getFormId(), "id", $submission->getEntryId());

            $new_entry = new FormEntry();
            $new_entry->setFormId($submission->getFormId());
            $new_entry->setEntryId($submission->getEntryId());
            $new_entry->setApproved($submission->getApproved());
            $new_entry->setApplicationId($submission->getApplicationId());
            $new_entry->setUserId($submission->getUserId());
            $new_entry->setParentSubmission($submission->getId());
            $new_entry->setDeclined("1");
            $new_entry->setDateOfSubmission($submission->getDateOfSubmission());
            $new_entry->setDateOfResponse($submission->getDateOfResponse());
            $new_entry->setDateOfIssue($submission->getDateOfIssue());
            $new_entry->setObservation($submission->getObservation());
            $new_entry->save();

            $submission->setEntryId($new_id);

            $submission->setPreviousSubmission($new_entry->getId());
            $submission->save();
        }
     }
    /**
     * Get Reviewer firstname , lastname
     */
    public function getReviewerNames($reviewer_id){
        $q = Doctrine_Query::create()
                ->from('CfUser u')
                ->where('u.nid = ? ',$reviewer_id) ;
        $res = $q->fetchOne();
        return $res->getStrfirstname()." ".$res->getStrlastname();
    }
    
    /**
     * Check if Reviewer on leave
     */
    public function checkIfReviewerOnLeave($reviewer_id){
        $q = Doctrine_Query::create()
                ->from('LeaveRequest r')
                ->where('r.reviewer_id = ?',$reviewer_id)
                ->andWhere('r.status = ?','ongoing');
        $res = $q->execute();
        if(count($res) > 0 ){
            return true;
        }else {
            return false;
        }
        
    }

            
    /**
     * Get user allowed Agencies
     */
    public function getUserAllowedAgencies($user_id){
        $agencies = array();
        $q = Doctrine_Query::create()
                ->from('AgencyUser a')
                ->where('a.user_id = ? ',$user_id) ;
        $res = $q->execute();
        foreach($res as $r){
            array_push($agencies, $r['agency_id']) ;
        }
        return $agencies ;
    }
    /**
     * Get All users with below groups
     * 200017 - RHA Admin
     * 200015 - RHA Non-Objection
     * 200000 - System Administrators
     */
    public function getAdminRHAUsers(){
        $q = Doctrine_Query::create()
                ->from('MfGuardUserGroup m')
                ->where('m.group_id in (200017,200015,200000)');
        $res = $q->execute();
        $users = array();
        foreach ($res as $r){
            array_push($users, $r['user_id']) ;
        }
        return $users;
                
    }
    
    /**
     * Validate Reviewer Leave Request
     */
    public function validateLeaveRequestDates($user_id,$start_date,$end_date){
        //check the date selection
        $today = date('Y-m-d') ;
        $start_date_a  = date('Y-m-d', strtotime($start_date));
        $end_date_a  = date('Y-m-d', strtotime($end_date));    
        
        //error_log("Start Date Received >>>> ".strtotime($start_date_a));
        //error_log("Todays Date is >>> ".strtotime($today));
        if(strtotime($start_date_a) >= strtotime($today) && strtotime($start_date_a) <= strtotime($end_date_a)) {
            //check if this user has an ongoing leave for selected dates
            if($this->validateLeaveDuration($user_id, $start_date, $end_date)){
                return false ; //user has live leave 
            }else{
             return true;
            }
           
        }
        else {
            return false;
        }
        
    } 
    
    /**
     * Leave Validation - Prevent double entry for leave period.
     * Managers should not confirm leave if a user previous leave request is ongoing and lies in the same period.
     */
    public function validateLeaveDuration($user_id,$start_date,$end_date){
        //$status = 'ongoing' ; //status to check
        $query = "SELECT * FROM leave_request WHERE (end_date BETWEEN '$start_date' AND '$end_date') AND reviewer_id = $user_id  AND status = 'ongoing' " ;
        error_log("Debug: My Query >>>> ".$query);
        $res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($query) ;
        if(count($res) > 0){
            return true ; // a record exists, 
        }else {
            //cleared
            return false;
        }
    }
    
    /**
     * Check if A user is registered under a certain user category
     */
    public function checkUserRegisteredAsPropertyOwner($user_id){
        $q = Doctrine_Query::create()
                ->from('SfGuardUserProfile p')
                ->where('p.user_id = ?',$user_id) ;
        $res = $q->fetchOne();
        //1 = Property Owners (can apply for category 1A and 1B Only)
        if($res->getRegisteras() == 1){
            error_log("This is a Property Owner >>".$user_id) ;
            return true ; //Property owner
        }else{
            error_log("Not a Property Owner ".$user_id) ;
            return false;
        }
        
               
    }
    
     /**
     * Check if a form is a valid application form
     */
    public function checkApplicationForm($form_id,$check_type = 1){
        $q = Doctrine_Query::create()
                ->from('ApForms f')
                ->where('f.form_id = ?', $form_id) ;
        $res = $q->fetchOne() ;
        //
        if($res->getFormType() == $check_type){
            return true ; //application form
        }else {
            return false;
        }
    }
    
    /**
     * Get application number using form_id and entry id
     */
    public function getAppNumber($form_id,$entry_id){
        $q = Doctrine_Query::create()
                ->from('FormEntry f')
                ->where('f.form_id = ? ',$form_id)
                ->andWhere('f.entry_id = ?',$entry_id);
        return $q->fetchOne();
    }
    
    /**
     * Get Data from URL
     */
    public function get_data($url) {
        error_log("URL TO ".$url);
	$ch = curl_init();
	$timeout = 5;
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	$data = curl_exec($ch);
	curl_close($ch);
       // error_log($data);
	return $data;
       }

    /** Compare values set for application queuing
     * This function checks if value set for queuing is equal to menus value otherwise the system picks
     * the set value on submenu.
     * used to implement override of applicaton queuing set in menus for backend application listing
     */
    public function getAppQueuing($submenu_queuing, $menu_queuing) {
        if ($submenu_queuing == "default") {
            //user prefers to use menu setting for queuing applications - so return menu queuing as the value
            return $menu_queuing;
        } else {
            //user has set a value so return the value
            return $submenu_queuing; //override menu
        }
    }

    /** Get currency ISO code */
    public function getCurrencyISOCode($currency_id) {
        error_log("Currency ISO code >>> " . $currency_id);
        $q = Doctrine_Query::create()
                ->from('Currencies c')
                ->where('c.id = ? ', $currency_id);
        $query_r = $q->execute();
        $iso_code = null;
        ///
        // error_log(print_r($query_r,True));
        foreach ($query_r as $r) {
            $iso_code = $r->getCode();
        }
        //error_log("Currency >>> ".$iso_code) ;
        return $iso_code;
    }

    /** function to return an array of countries available */
    public function getCountries() {

        $sql = "SELECT name FROM countries ";
        //we are protected by using manager
        $sql_res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($sql);
        //
        $choices = array();

        //
        foreach ($sql_res as $c) {
            //create array;
            $choices[$c['name']] = $c['name'];
        }
        return $choices;
    }

    /** get jambopay merchant * */
    public function getJamboPayMerchant() {
        $sql = "SELECT id,name FROM merchant where name='jambopay' ";
        //we are protected by using manager
        $sql_res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($sql);
        //
        $choices = array();

        //
        foreach ($sql_res as $c) {
            //create array;
            $choices[$c['id']] = $c['name'];
        }
        return $choices;
    }

    /** get jambopay merchant by name 
     * merchant_id as parameter
     * * */
    public function getJambopayMerchantByName($merchant_id) {
        $sql = "SELECT name FROM merchant where id='$merchant_id' ";
        //we are protected by using manager
        $sql_res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($sql);
        //

        $name = null;
        foreach ($sql_res as $r) {
            $name = $r['name'];
        }
        //
        return $name;
    }

    /** Get merchant currency */
    public function getMerchantCurrency($name) {
        $q = Doctrine_Query::create()
                ->from('Merchant m')
                ->where('m.name = ? ', $name)
                ->limit(1);
        $r_exec = $q->execute();
        //
        $currency = 'Uknown';
        //
        foreach ($r_exec as $r) {
            $currency = $r->getCurrencyId();
        }

        return $currency;
    }

    /** Get Ap Form Merchant Type */
    public function getApFormMerchant($form_id) {
        $q = Doctrine_Query::create()
                ->from('ApForms a')
                ->where('a.form_id = ? ', $form_id)
                ->limit(1);
        $r_exec = $q->execute();
        //
        $merchant_type = 'Undefined';
        //
        foreach ($r_exec as $r) {
            $merchant_type = $r->getPaymentMerchantType();
        }
        //
        return $merchant_type;
    }

    /**
     * Function used to set sub-county on user application submission from frontend
     * parameters - sf_user and application_id
     */
    public function setSubCounty($county_name, $application_id) {
        //OTB patch - We get the set variable county_name 
        //Then we update the subcounty value for this application. 
        //We the update FormEntry table with for subcounty. We will improve on this to work for draft applications to
        $q_form_update = Doctrine_Query::create()
                ->UPDATE('FormEntry')
                ->SET('subcounty', '?', $county_name)
                ->WHERE('id = ?', $application_id);
        $q_form_update->execute();
    }

    /**
     * Get logged reviewer subcounty value - returns county code
     */
    public function getSubcounty($user_id) {

        ///

        $q = Doctrine_Query::create()
                ->from('CfUser a')
                ->where('a.nid = ? ', $user_id);
        $results = $q->execute();
        //
        $subcounty = null;
        // 
        foreach ($results as $r) {
            $subcounty = $r->getSubcounty();
        }

        return $subcounty;
    }

    /**
     * Get subcounty by name
     * Parameter = subcounty_code
     * Important Note: This should be improved to use relations
     */
    public function getSubcountyByName($subcounty_code) {
        if ($subcounty_code) {
            $get_county_name = Doctrine_Query::create()
                    ->from('Counties c')
                    ->where('c.county_code = ? ', $subcounty_code)
                    ->limit(1);
            $res_county = $get_county_name->fetchOne();
            //
            return $res_county->getCountyName();
        } else {
            return "Uknown";
        }
    }

    /**
     * Get subcounty by id
     * Parameter = subcounty_name
     * Important Note: This should be improved to use relations
     */
    public function getSubcountyById($name) {

        $get_county_id = Doctrine_Query::create()
                ->from('Counties c')
                ->where('c.county_code = ? ', $name)
                ->limit(1);
        $res_county = $get_county_id->fetchOne();
        if ($res_county) {
            return $res_county->getId();
        } else {
            return false;
        }
        //
    }

    /**
     * Get sub-county number
     */
    public function getSubcountyNumber($subcounty_code) {
        try {
            $get_county_name = Doctrine_Query::create()
                    ->from('Counties c')
                    ->where('c.county_code = ? ', $subcounty_code)
                    ->limit(1);
            $res_county = $get_county_name->fetchOne();
            //
            return $res_county->getNumber();
        } catch (Exception $ex) {
            error_log("getSubcountyNumber() Error " . $ex->getMessage());
        }
    }

    /**
     * Get sub-county code
     */
    public function getSubcountycode($name) {
        try {
            $get_county_name = Doctrine_Query::create()
                    ->from('Counties c')
                    ->where('c.county_code = ? ', $name)
                    ->limit(1);
            $res_county = $get_county_name->fetchOne();
            //
            // error_log("County to select ".$name);
            //
            return $res_county->getCountyCode();
        } catch (Exception $ex) {
            error_log("getSubcountycode() Error " . $ex->getMessage());
        }
    }

    /** Update Generated number and append subcounty code at the start of the string */
    public function updateAppNumber($application_id, $subcounty_code) {
        //get the submission identification (Application) number of the form entry 
        $app_q = Doctrine_Query::create()
                ->from('FormEntry f')
                ->WHERE('id = ?', $application_id)
                ->limit(1);
        $res = $app_q->fetchOne();
        //
        $old_number = $res->getApplicationId();
        //we append a string prefix for the application number
        $new_number = "INV-" . $subcounty_code . "-" . $old_number;
        //update number
        $update_number = Doctrine_Query::create()
                ->UPDATE('FormEntry')
                ->SET('application_id', '?', $new_number)
                ->WHERE('id = ?', $application_id);
        $update_number->execute();
        //
        return $new_number;
    }

    /**
     * Change application number after payment from INV
     * This funtion removes INV- from an application number
     * 
     */
    public function changeAppNumber($app_number) {
        //change application number for applications with prefix INV - This prevents change of numbers from resubmissions of 
        //application sent back after circulations
        if (strpos($app_number, "INV-") !== false) {
            $new_no = str_replace("INV-", "", $app_number);
            return $new_no;
        } else {
            //just return the same name
            return $app_number;
        }
    }

    /** OTB patch - Return an array of existing counties
     *  */
    public function getCounties() {
        //query
        $q = Doctrine_Query::create()
                ->from('Counties c');
        //execute
        $counties = $q->execute();
        //return results
        return $counties;
    }

    /**
     * Get Application Sub-county value
     */
    public function getApplicationSubcounty($app_id) {
        try {
            //error_log("App id ".$app_id) ;
            $query = Doctrine_Query::create()
                    ->from("FormEntry f")
                    ->where("f.id = ? ", $app_id);
            //
            $query_res = $query->fetchOne();
            //
            return $this->getSubcountycode($query_res->getSubcounty());
        } catch (Exception $ex) {
            error_log("getApplicationSubcounty() Error " . $ex->getMessage());
        }
    }

    /**
     * Get Elements the user should edit on the frontend
     * after submissions
     */
    public function getEditFields($app_id) {
        $edit_fields = null;
        //
        $q = Doctrine_Query::create()
                ->from("EntryDecline a")
                ->where("a.entry_id = ?", $app_id)
                ->andWhere("a.resolved = 0");
        $comments = $q->execute();
        foreach ($comments as $comment) {
            $edit_fields = json_decode($comment->getEditFields(), TRUE);
        }
        //return an array of fields
        error_log("Fields:::::>>>>>");
        error_log(print_R($edit_fields));
        return $edit_fields;
    }

    /** Function to get an invoice for an application with settings as invoicing
     * 
     */
    public function getInvoiceFromApplication($application_id) {
        try {
            $q = Doctrine_Query::create()
                    ->from("MfInvoice m")
                    ->where("m.app_id = ? ", $application_id)
                    ->addwhere("m.paid = ? ", 1);
            $invoice_details = $q->fetchOne();
            //
            //$invoice_id = $invoice_details->getId() ;
            //pass the invoice details to function resposible for sending information to county pro system
            $this->sendInvoiceToCountyPro($invoice_details, $application_id);
        } catch (Exception $ex) {
            error_log("getInvoiceFromApplication() Error " . $ex->getMessage());
        }
    }

    /**
     * function to get invoice from application and call save function responsible for saving 
     * details that will be sent to countypro
     */
    public function getInvoiceFromApplicationAndSave($application_id) {
        try {
            $q = Doctrine_Query::create()
                    ->from("MfInvoice m")
                    ->where("m.app_id = ? ", $application_id)
                    ->addwhere("m.paid = ? ", 1);
            $invoice_details = $q->fetchOne();
            //
            //just save the invoice 
            $this->saveInfoToSendToCountyPro($invoice_details, $application_id); //we create a table that we will do an 
            //insert and save data that we can retrieve and send later. This is to avoid delays with countypro processing. We want to allow 
            //reviewers continue with processing other applications without having to wait untill the system sends the invoice.
        } catch (Exception $ex) {
            error_log("getInvoiceFromApplication() Error " . $ex->getMessage());
        }
    }

    /**
     * Get user_id or the user who submitted a particular application 
     */
    public function getApplicationOwner($app_id) {
        try {
            $q = Doctrine_Query::create()
                    ->from("FormEntry f")
                    ->where("f.id = ? ", $app_id);
            $q_res = $q->fetchOne();
            //
            return $q_res->getUserId();
        } catch (Exception $ex) {
            error_log("getApplicationOwner() Error " . $ex->getMessage());
        }
    }

    /**
     * Get Application owner details
     * 
     */
    public function getApplicationOwnerDetails($owner_id) {
        try {
            $q = Doctrine_Query::create()
                    ->from("SfGuardUserProfile u")
                    ->where("u.user_id = ? ", $owner_id);
            $q_res = $q->fetchOne();
            return $q_res;
        } catch (Exception $ex) {
            error_log("getApplicationOwnerDetails() Error " . $ex->getMessage());
        }
    }

    /**
     * Get Reviewer User Details
     * 
     */
    public function getReviewerDetails($user_id) {
        try {
            $q = Doctrine_Query::create()
                    ->from("CfUser u")
                    ->where("u.nid = ? ", $user_id);
            $q_res = $q->execute();
            return $q_res;
        } catch (Exception $ex) {
            error_log("getReviewerDetails() Error " . $ex->getMessage());
        }
    }
    /**
     * Return one record - Reviewer information
     */
    /**
     * Get Reviewer User Details
     * 
     */
    public function getReviewerInfor($user_id) {
        try {
            $q = Doctrine_Query::create()
                    ->from("CfUser u")
                    ->where("u.nid = ? ", $user_id);
            $q_res = $q->fetchOne();
            return $q_res;
        } catch (Exception $ex) {
            error_log("getReviewerDetails() Error " . $ex->getMessage());
        }
    }

    /**
     * get Application details
     */
    public function getApplicationDetails($app_id) {
        try {
            $q = Doctrine_Query::create()
                    ->from('FormEntry f')
                    ->where('f.id = ? ', $app_id)
                    ->limit(1);
            $res = $q->fetchOne();
            return $res;
        } catch (Exception $ex) {
            error_log("getApplicationDetails() Error " . $ex->getMessage());
        }
    }

    /**
     * Helper for calculating days. Borrowed from thomas work
     */
    public function GetDays($sStartDate, $sEndDate) {
        $aDays[] = $start_date;
        $start_date = $sStartDate;
        $end_date = $sEndDate;
        $current_date = $start_date;
        while (strtotime($current_date) <= strtotime($end_date)) {
            $aDays[] = gmdate("Y-m-d", strtotime("+1 day", strtotime($current_date)));
            $current_date = gmdate("Y-m-d", strtotime("+2 day", strtotime($current_date)));
        }

        // error_log(print_r($aDays, true));
        return $aDays;
    }

    /**
     * Get days an application has stayed in a particular stage
     */
    public function getAppStageStayedDays($stage_id, $app_id) {
        $q = Doctrine_Query::create()
                ->from('ApplicationReference b')
                ->where('b.stage_id = ?', $stage_id)
                ->andWhere('b.application_id = ?', $app_id);
        $application_reference = $q->fetchOne();
        //
        if ($application_reference) {
            // $days = sizeOf($this->GetDays($application_reference->getStartDate(), date('Y-m-d')));
            //
            $current_date = time();
            $start_date = strtotime($application_reference->getStartDate());
            $mydays = $current_date - $start_date;
            $days = floor($mydays / 86400);
            // $dys = round($mydays / 86400, 0);
            //  error_log("getAppStageStayedDays() My days ".  floor($mydays/86400));
            return $days;
        } else {
            return 0;
        }
    }

    /**
     * Get stage set max duration an application is allowed to stay
     */
    public function getStageMaxDays($stage_id) {
        $q = Doctrine_Query::create()
                ->from('SubMenus a')
                ->where('a.id = ?', $stage_id);
        $stage = $q->fetchOne();
        //
        return $stage->getMaxDuration();
    }

    /**
     * get stage name using stage_id
     */
    public function getStageName($stage_id) {
        $q = Doctrine_Query::create()
                ->from('SubMenus a')
                ->where('a.id = ?', $stage_id);
        $stage = $q->fetchOne();
        //
        return $stage->getTitle();
    }

    /**
     * Insert or update form details in ap_form_field_preload_settings table
     */
    public function updateOrInsertLogic($form_id, $base_element_id, $affected_element_id, $data_source_table_name, $data_source_option_column_name_value, $data_source_option_column_name_label) {

        //first do a select and determine if its an update
        $select_query = "select * from ap_forms_field_preload_settings where form_id = '$form_id' ";
        $select_query_res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($select_query);
        //
        if (count($select_query_res) > 0) {
            //update existing
            $statement_up = "UPDATE ap_forms_field_preload_settings SET  base_element_id = '$base_element_id', "
                    . "affected_element_id = '$affected_element_id' , table_name = '$data_source_table_name', table_optional_value = '$data_source_option_column_name_value',table_optional_label = '$data_source_option_column_name_label' ";
            $this->myDBWrapper($statement_up);
        } else {
            //insert new
            $statement_in = "INSERT INTO ap_forms_field_preload_settings (form_id,base_element_id,affected_element_id,table_name,table_optional_value,table_optional_label) "
                    . "VALUES($form_id,$base_element_id,$affected_element_id,'$data_source_table_name','$data_source_option_column_name_value','$data_source_option_column_name_label')";
            $this->myDBWrapper($statement_in);
            error_log("Query executed >>>> " . $statement_in);
        }
    }

    /**
     * Get Element Base field affected used for our custom form logic to pre-load a field data
     */
    public function getFormLogicBaseField($form_id) {

        $base_field_id = null;
        $base_field_title = "None";
        $statement = "SELECT base_element_id FROM ap_forms_field_preload_settings WHERE form_id = $form_id ";
        //
        $res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($statement);
        foreach ($res as $r) {
            $base_field_id = $r['base_element_id'];
        }
        if ($base_field_id) {
            //select element title
            $statement2 = "SELECT element_title FROM ap_form_elements WHERE element_id = $base_field_id AND form_id = $form_id";
            $res2 = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($statement2);
            foreach ($res2 as $r) {
                $base_field_title = $r['element_title'];
            }
        }

        return $base_field_title;
    }

    /**
     * Get Element Affected field info
     */
    public function getFormLogicAffectedField($form_id) {

        $affected_field_id = null;
        $affected_field_title = "None";
        $statement = "SELECT affected_element_id FROM ap_forms_field_preload_settings WHERE form_id = $form_id ";
        //
        $res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($statement);
        foreach ($res as $r) {
            $affected_field_id = $r['affected_element_id'];
        }
        if ($affected_field_id) {
            //select element title
            $statement2 = "SELECT element_title FROM ap_form_elements WHERE element_id = $affected_field_id AND form_id = $form_id";
            $res2 = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($statement2);
            foreach ($res2 as $r) {
                $affected_field_title = $r['element_title'];
            }
        }
        return $affected_field_title;
    }

    /**
     * Get table info that stores data for pre-loading fields data (affected fields data source)
     */
    public function getFormLogicTableInfo($form_id) {
        $statement = "SELECT table_name as name,table_optional_value as value,table_optional_label as label FROM ap_forms_field_preload_settings WHERE form_id = $form_id ";
        $res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($statement);
        return $res;
    }

    /**
     * return a list of zones for a specified sub-county
     */
    public function getZones($subcounty_id) {

        $statement = "SELECT id,name FROM zone WHERE sub_county = $subcounty_id";
        $res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($statement);
        return $res;
    }

    /**
     * return permitteduser for a selected zone
     * this should return permitted plot ratio and permitted ground coverage
     */
    public function getPermittedUser($zone_id) {
        $statement = "SELECT id,name FROM permitted_user WHERE zone = $zone_id";
        $res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($statement);
        return $res;
    }

    /**
     * return permitted plot ratio for a certain use for a zone
     */
    public function getPermittedPlotRatio($permitted_user_id) {
        $statement = "SELECT plot_ratio FROM permitted_user WHERE id = $permitted_user_id";
        error_log("Debug >>>>>" . $statement);
        $res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($statement);
        return $res;
    }

    /**
     * return permitted plot ratio for a certain use for a zone
     */
    public function getPermittedGroundCoverage($permitted_user_id) {
        $statement = "SELECT ground_coverange as ground_coverage FROM permitted_user WHERE id = $permitted_user_id";
        $res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($statement);
        return $res;
    }

    /**
     * get details from table ap_forms_field_preload_settings
     */
    public function getOnchangeDropdownFieldsSettings($form_id) {
        $statement = "SELECT form_id as form, base_element_id as base_field,affected_element_id as affected_field,table_name as table_name,"
                . "table_optional_value as value, table_optional_label as label FROM ap_forms_field_preload_settings WHERE form_id = $form_id ";
        $results = array();
        $values = array();
        //
        $res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($statement);
        foreach ($res as $r) {
            $values['form_id'] = $r['form'];
            $values['base_field_id'] = $r['base_field'];
            $values['affected_field_id'] = $r['affected_field'];
            $values['table_name'] = $r['table_name'];
            $values['option_value'] = $r['value'];
            $values['option_label'] = $r['label'];
            //error_log("BaseField >>>> ". $r['base_field']);
            //
        array_push($results, $values);
        }
        return $results;
    }

    /**
     * get details from table ap_forms_field_preload_settings difference is that this
     * function uses form_id and base_field_id
     */
    public function getOnchangeDropdownFieldsSettings2($form_id, $base_field) {
        $statement = "SELECT form_id as form, base_element_id as base_field,affected_element_id as affected_field,table_name as table_name,"
                . "table_optional_value as value, table_optional_label as label FROM ap_forms_field_preload_settings WHERE form_id = $form_id ";
        $results = array();
        $values = array();
        //
        $res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($statement);
        foreach ($res as $r) {
            $values['form_id'] = $r['form'];
            $values['base_field_id'] = $r['base_field'];
            $values['affected_field_id'] = $r['affected_field'];
            $values['table_name'] = $r['table_name'];
            $values['option_value'] = $r['value'];
            $values['option_label'] = $r['label'];
            //error_log("BaseField >>>> ". $r['base_field']);
            //
        array_push($results, $values);
        }
        return $results;
    }

    /*     * *
     * get data to display on affected field element
     */

    public function getPreloadInfoData($tabletoselectfrom, $option_value, $option_label, $fk_id, $affected_element_id) {
        //make fk column configurable
        $statement = "SELECT $option_value,$option_label FROM $tabletoselectfrom WHERE zone = $fk_id ";
        $results = array();
        $values = array();
        //
        $res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($statement);
        foreach ($res as $r) {
            $values['value'] = $r["" . $option_value . ""];
            $values['label'] = $r["" . $option_label . ""];
            $values['affected_element_id'] = $affected_element_id;
            array_push($results, $values);
        }
        return $results;
    }

    /**
     * OTB function for database connection
     */
    public function myDBWrapper($query, $type = 0) {

        try {
            // Get Connection of Database
            $connection = Doctrine_Manager::getInstance()->getCurrentConnection()->getDbh();
            // Make Statement
            $statement = $connection->prepare($query);
            // Execute Query
            $res = $statement->execute();
            //
            return $res;
        } catch (Exception $ex) {
            error_log("Ooops!! Something is wrong " . $ex->getMessage());
            error_log("Debug Query is " . $query);
            return false;
        }
    }

    /**
     * Function to convert a month number to month name
     */
    public function convertMonthNumberToMonthName($num) {
        $monthNum = $num;
        $monthName = "Uknown";
        if ($monthNum) {
            $dateObj = DateTime::createFromFormat('!m', $monthNum);
            $monthName = $dateObj->format('F');
        } else {
            //do nothing
            error_log("convertMonthNumberToMonthName() failed. Month Number supplied is " . $monthNum);
        }
        return $monthName;
    }

    /**
     *  Kibes Idea
     * @param type $number
     * @return boolean
     * 
     */
    public function convert_number_to_words($number) {

        $hyphen = '-';
        $conjunction = ' and ';
        $separator = ', ';
        $negative = 'negative ';
        $decimal = ' point ';
        $dictionary = array(
            0 => 'zero',
            1 => 'one',
            2 => 'two',
            3 => 'three',
            4 => 'four',
            5 => 'five',
            6 => 'six',
            7 => 'seven',
            8 => 'eight',
            9 => 'nine',
            10 => 'ten',
            11 => 'eleven',
            12 => 'twelve',
            13 => 'thirteen',
            14 => 'fourteen',
            15 => 'fifteen',
            16 => 'sixteen',
            17 => 'seventeen',
            18 => 'eighteen',
            19 => 'nineteen',
            20 => 'twenty',
            30 => 'thirty',
            40 => 'fourty',
            50 => 'fifty',
            60 => 'sixty',
            70 => 'seventy',
            80 => 'eighty',
            90 => 'ninety',
            100 => 'hundred',
            1000 => 'thousand',
            1000000 => 'million',
            1000000000 => 'billion',
            1000000000000 => 'trillion',
            1000000000000000 => 'quadrillion',
            1000000000000000000 => 'quintillion'
        );

        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                    'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX, E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . $this->convert_number_to_words(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens = ((int) ($number / 10)) * 10;
                $units = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . $this->convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = $this->convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= $this->convert_number_to_words($remainder);
                }
                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }

        return $this->uppercaseString($string); //Return string capitalized for all first letters in each string
    }

    /**
     * function to convert each character of each word in a string
     * http://php.net/manual/en/function.ucwords.php
     */
    public function uppercaseString($string) {

        $word = ucwords(strtolower($string));
        return $word;
    }

    /**
     * function to add thousands separator
     */
    public function formatFigures($number) {
        $formatted = number_format($number);
        return $formatted;
    }

    /**
     * check if an application is already shared to avoid double entry in form_entry_shares
     * 
     */
    public function checkifAppShared($receiverid, $app_id) {
        $q = Doctrine_Query::create()
                ->from("FormEntryShares f")
                ->where("f.receiverid = ? ", $receiverid)
                ->addWhere("f.formentryid = ? ", $app_id)
                ->limit(1);
        $res = $q->execute();

        if (count($res) > 0) {
            return "shared";
        } else {
            return "not_shared";
        }
    }

    /**
     * Method to return job role from a user category of a registered frontend user
     */
    public function getRole($user_id) {
        error_log("User Id " . $user_id);
        $statement = "SELECT r.role from sf_guard_user_profile p LEFT JOIN sf_guard_user_categories r ON p.registeras = r.id WHERE p.user_id = $user_id ";
        $role = 'others';
        $res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($statement);
        foreach ($res as $r) {
            $role = $r['role'];
        }
        return $role;
//            try{
//            $q = Doctrine_Query::create()
//                    ->from('SfGuardUserProfile p')
//                    ->leftJoin('SfGuardUserCategories c')
//                    ->where('p.user_id = ?',$user_id) 
//                     ->limit(1);
//            $res = $q->execute() ;
//            return $res ;
//                    
//            }  catch (Exception $ex){
//                
//            }
    }

    /**
     * check if linked form is already submitted
     */
    public function checkLinkedFormIsSubmitted($app_id) {
        $q = Doctrine_Query::create()
                ->from("FormEntryLinks a")
                ->where("a.formentryid = ?", $app_id);
        $links = $q->execute();
        //
        $submitted = false;
        foreach ($links as $link) {
            $q = Doctrine_Query::create()
                    ->from("ApForms a")
                    ->where("a.form_id = ?", $link->getFormId());
            $linkedform = $q->fetchOne();
            if ($linkedform) {
                $submitted = true;
            } else {
                //do nothing
            }
        }
        return $submitted;
    }

    /**
     * Convert date to human readable mode
     * 
     */
    public function convertDateToHumanFriendlyMode($date_string) {
        $date = date_create($date_string);
        return date_format($date, 'l jS \of F Y');
    }

    /**
     * Convert date for invoice
     */
    public function convertDateForInvoice($date_string) {
        $date = date_create($date_string);
        return date_format($date, 'jS F Y');
    }

    /**
     * get backend user details
     */
    public function getBackendUserDetails($user_id) {
        $q = Doctrine_Query::create()
                ->from('CfUser c')
                ->where('nid = ? ', $user_id);
        $res = $q->fetchOne();

        return $res;
    }

    /**
     * Get Submenu by ID
     */
    public function getStageDetails($stage_id) {
        $q = Doctrine_Query::create()
                ->from('Submenus s')
                ->where('s.id = ? ', $stage_id)
                ->limit(1);
        $res = $q->execute();
        return $res;
    }

    /**
     * Get invoices templates
     */
    public function getInvoiceTemplates() {
        $q = Doctrine_Query::create()
                ->from('Invoicetemplates t');
        $res = $q->execute();
        //
        $invoice_temps = array();
        $invoice_temps['0'] = 'NUll';
        foreach ($res as $value) {

            $invoice_temps[$value->getId()] = $value->getTitle();
        }
        return $invoice_temps;
    }

    /**
     * Check if user has pending invoicing tasks for selected application
     * 
     */
    public function hasPendingInvoicingTask($user_id, $app_id) {
        $user_id = $user_id;
        $app_id = $app_id;
        $type = 3; //invoicing task
        //
        $q = Doctrine_Query::create()
                ->from('Task t')
                ->where('t.owner_user_id = ? ', $user_id)
                ->addWhere('t.application_id = ?', $app_id)
                ->andWhere('t.type = ? ', $type);
        $results = $q->execute();
        //
        if (count($results)) {
            return 'yes';
        } else {
            return 'no';
        }
    }

    /**
     * Get Logged user Task id
     */
    public function getUserTaskId($user, $app) {
        $user_id = $user;
        $app_id = $app;
        error_log("App >>>> " . $app_id);
        error_log("User >>>> " . $user_id);
        $type = 3; //invoicing task
        //
        $q = Doctrine_Query::create()
                ->from('Task t')
                ->where('t.owner_user_id = ? ', $user_id)
                ->addWhere('t.application_id = ?', $app_id)
                ->andWhere('t.type = ? ', $type);
        $results = $q->execute();
        //
        $task_id = null;
        foreach ($results as $r) {
            $task_id = $r->getId();
        }
        return $task_id;
    }

    /* OTB - Start Form builder useful functions */

    public function getFieldLabel($form_id, $field_id) {
        $q = Doctrine_Query::create()
                ->from('ApFormElements a')
                ->where('a.form_id = ? AND a.element_id = ?', array($form_id, $field_id))
                ->limit(1);
        $element = $q->fetchOne();
        return $element->getElementTitle();
    }

    public function getFormElements($form_id) {
        $q = Doctrine_Query::create()
                ->from('ApFormElements a')
                ->where('a.form_id = ?', array($form_id));
        return $q->execute();
    }

    //OTB patch - Function to check if server is https or http and return the appropriate variable 
    public function isSecure() 
    {
        $isSecure = false;
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') 
            {
              $isSecure = true;
            }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') 
            {
             $isSecure = true;
            }
        $REQUEST_PROTOCOL = $isSecure ? 'https' : 'http';
        return $REQUEST_PROTOCOL ;
    }

    public function get_fields_html_markup($form_id, $element_ids) {
        //$prefix_folder = dirname(__FILE__) . "/../../../../../lib/vendor/cp_machform/";
        $prefix_folder = "vendor/cp_machform/";
        require($prefix_folder . 'includes/view-functions.php');
        require($prefix_folder . 'includes/db-core.php');
        require($prefix_folder . 'config.php');
        $dbh = mf_connect_db();

        $form_html = "";
		foreach($element_ids as $estimation_element_id){
			//Get Options
			$params = array($form_id, $estimation_element_id);
			$query = "SELECT
					aeo_id,
							element_id,
							option_id,
							`position`,
							`option`,
							option_is_default
						FROM
							" . MF_TABLE_PREFIX . "element_options
					   where
							form_id = ? and live=1 and element_id = ?
					order by
							element_id asc,`option` asc";

			$sth = mf_do_query($query, $params, $dbh);
			while ($row = mf_do_fetch_result($sth)) {
				$element_id = $row['element_id'];
				$option_id = $row['option_id'];
				$options_lookup[$element_id][$option_id]['aeo_id'] = $row['aeo_id'];
				$options_lookup[$element_id][$option_id]['position'] = $row['position'];
				$options_lookup[$element_id][$option_id]['option'] = $row['option'];



				$sql = "SELECT * FROM ext_translations WHERE field_id = ? AND option_id = ? AND field_name = 'option' AND table_class = 'ap_element_options' AND locale = ?";
				$params = array($row['aeo_id'], $option_id, $locale);
				$translation_sth = mf_do_query($sql, $params, $dbh);
				$translation_row = mf_do_fetch_result($translation_sth);
				if ($translation_row) {
					$options_lookup[$element_id][$option_id]['option'] = $translation_row['trl_content'];
				}

				$options_lookup[$element_id][$option_id]['option_is_default'] = $row['option_is_default'];

				if (isset($element_prices_array[$element_id][$option_id])) {
					$options_lookup[$element_id][$option_id]['price_definition'] = $element_prices_array[$element_id][$option_id];
				}
			}
			//End Get Options

			$params = array($form_id, $estimation_element_id);
			$query = "SELECT *
						FROM ap_form_elements
					   WHERE
							form_id = ? and element_status='1' and element_type <> 'page_break' and element_id = ?
					ORDER BY
							element_position asc";

			$sth = mf_do_query($query, $params, $dbh);
			while ($row = mf_do_fetch_result($sth)) {
				$element_data = new stdClass();
				//lookup element options first
				if (!empty($options_lookup[$element_id])) {
					$element_options = array();
					$i = 0;
					foreach ($options_lookup[$element_id] as $option_id => $data) {
						$element_options[$i] = new stdClass();
						$element_options[$i]->id = $option_id;
						$element_options[$i]->option = $data['option'];

						$sql = "SELECT * FROM ext_translations WHERE field_id = ? AND option_id = ? AND field_name = 'option' AND table_class = 'ap_element_options' AND locale = ?";
						$params = array($data['aeo_id'], $element_options[$i]->id, $locale);
						$translation_sth = mf_do_query($sql, $params, $dbh);
						$translation_row = mf_do_fetch_result($translation_sth);
						if ($translation_row) {
							$element_options[$i]->option = $translation_row['trl_content'];
						}

						$element_options[$i]->is_default = $data['option_is_default'];
						$element_options[$i]->is_db_live = 1;

						if (isset($data['price_definition'])) {
							$element_options[$i]->price_definition = $data['price_definition'];
						}

						$i++;
					}
				}
				if (!empty($element_options)) {
					$element_data->options = $element_options;
				} else {
					$element_data->options = '';
				}
				$element_data->title = nl2br($row['element_title']);
				$element_data->guidelines = $row['element_guidelines'];
				$element_data->size = $row['element_size'];
				$element_data->is_required = $row['element_is_required'];
				$element_data->is_unique = $row['element_is_unique'];
				$element_data->is_private = $row['element_is_private'];
				$element_data->type = $row['element_type'];
				$element_data->position = $row['element_position'];
				$element_data->id = $row['element_id'];
				$element_data->is_db_live = 1;
				$element_data->form_id = $form_id;
				$element_data->choice_has_other = (int) $row['element_choice_has_other'];
				$element_data->choice_other_label = $row['element_choice_other_label'];
				$element_data->choice_columns = (int) $row['element_choice_columns'];
				$element_data->time_showsecond = (int) $row['element_time_showsecond'];
				$element_data->time_24hour = (int) $row['element_time_24hour'];
				$element_data->address_hideline2 = (int) $row['element_address_hideline2'];
				$element_data->address_us_only = (int) $row['element_address_us_only'];
				$element_data->date_enable_range = (int) $row['element_date_enable_range'];
				$element_data->date_range_min = $row['element_date_range_min'];
				$element_data->date_range_max = $row['element_date_range_max'];
				$element_data->date_enable_selection_limit = (int) $row['element_date_enable_selection_limit'];
				$element_data->date_selection_max = (int) $row['element_date_selection_max'];
				$element_data->date_disable_past_future = (int) $row['element_date_disable_past_future'];
				$element_data->date_past_future = $row['element_date_past_future'];
				$element_data->date_disable_weekend = (int) $row['element_date_disable_weekend'];
				$element_data->date_disable_specific = (int) $row['element_date_disable_specific'];
				$element_data->date_disabled_list = $row['element_date_disabled_list'];
				$element_data->file_enable_type_limit = (int) $row['element_file_enable_type_limit'];
				$element_data->file_block_or_allow = $row['element_file_block_or_allow'];
				$element_data->file_type_list = $row['element_file_type_list'];
				$element_data->file_as_attachment = (int) $row['element_file_as_attachment'];
				$element_data->file_enable_advance = (int) $row['element_file_enable_advance'];
				$element_data->table_name = $row['element_table_name'];
				$element_data->field_value = $row['element_field_value'];
				$element_data->field_error_message = $row['element_field_error_message'];
				$element_data->field_name = $row['element_field_name'];
				$element_data->option_query = $row['element_option_query'];
				$element_data->existing_form = $row['element_existing_form'];
				$element_data->existing_stage = $row['element_existing_stage'];
				$element_data->file_auto_upload = (int) $row['element_file_auto_upload'];
				$element_data->file_enable_multi_upload = (int) $row['element_file_enable_multi_upload'];
				$element_data->file_max_selection = (int) $row['element_file_max_selection'];
				$element_data->file_enable_size_limit = (int) $row['element_file_enable_size_limit'];
				$element_data->file_size_max = (int) $row['element_file_size_max'];
				$element_data->matrix_allow_multiselect = (int) $row['element_matrix_allow_multiselect'];
				$element_data->matrix_parent_id = (int) $row['element_matrix_parent_id'];
				$element_data->upload_dir = $mf_settings['upload_dir'];
				$element_data->range_min = $row['element_range_min'];
				$element_data->range_max = $row['element_range_max'];
				$element_data->range_limit_by = $row['element_range_limit_by'];
				$element_data->jsondef = $row['element_jsondef'];
				$element_data->css_class = $row['element_css_class'];
				$element_data->machform_path = $machform_path;
				$element_data->machform_data_path = $machform_data_path;
				$element_data->section_display_in_email = (int) $row['element_section_display_in_email'];
				$element_data->section_enable_scroll = (int) $row['element_section_enable_scroll'];
				$form_html .= call_user_func('mf_display_' . $element_data->type, $element_data);
			}
		}
        return $form_html;
    }

    /* OTB - End Form builder useful functions */

    /**
     * Special Function to return connections
     * Remove the dirty mysql connections - Changed to mysqli since mysql is no longer supported
     * We replace all points wher Thomas did direct calls to mysql connection
     */
    public function getDbConnection() {
        $dbconn = mysqli_connect(sfConfig::get('app_mysql_host'), sfConfig::get('app_mysql_user'), sfConfig::get('app_mysql_pass'));
        mysqli_select_db($dbconn, sfConfig::get('app_mysql_db'));
        //
        return $dbconn;
    }
    
    /** 
     * Return current connection the symfony way
     * 
     */
    public function getDB(){
        return Doctrine_Manager::getInstance()->connection();
    }
    
           
      /**
     * Check if stage type is dispatch - 8
     */
    public function checkDispatchStage($submenu_id){
        
        $q = Doctrine_Query::create()
		   ->from('SubMenus a')
		   ->where('a.id = ?', $submenu_id);
         $sub_menu = $q->fetchOne();
         //check
        // error_log("Dispatch stage >>> ".$sub_menu->getStageType());
         if($sub_menu){
             if($sub_menu->getStageType() == 8){ // check if its dispatch
             return true;
         }else {
             //not
             return false;
            }
         }else {
             return false;
         }
         
    }
    /**
     * Check if an application has pending tasks assigned in the current stage
     */
    public function hasPendingTasks($application_id){
        $q = Doctrine_Query::create()
                ->from('Task t')
                ->where('t.application_id = ?',$application_id)
                ->addWhere('t.status = ?', 1) ;
        $res = $q->execute();
        return $res ;
                
        
    }
    /**
     * Check if a Task is in completed state and the application is in-review stage
     */
    public function checkTaskInReviewAndCompleted($task_id){
        
        $q = Doctrine_Query::create()
                ->from('Task t')
                ->where('t.id = ?',$task_id)
                ->addwhere('t.status = ?', 25); //completed status
        $task_status = $q->fetchOne();
        if($task_status){ //its in pending status           
            //now check that the application is in review stage
            $application_id = $task_status->getApplicationId();
           return $this->isStageReview($application_id);
            //return true;
        }else {
            return false;
        }
                
    }
    /**
     * Check if application has completed tasks if so, show the button of director resetting tasks
     * Also check if the application is in-review stage
     */
    public function checkAppHasTasksCompletedAndIsInReview($application_id){
        $q = Doctrine_Query::create()
                ->from('Task t')
                ->where('t.application_id = ?', $application_id)
                ->addwhere('t.status = ?', 25); //completed status
        $task_status = $q->fetchOne();
         if($task_status){ //its in pending status           
            //now check that the application is in review stage
            $application_id = $task_status->getApplicationId();
           return $this->isStageReview($application_id);
            //return true;
        }else {
            return false;
        }
                
    }
    /**
     * Check if application is in-review stage > 200001
     */
    public function isStageReview($application_id){
        $q = Doctrine_Query::create()
                ->from('FormEntry f')
                ->where('f.id = ?',$application_id);
        $res = $q->fetchOne();
        $q = Doctrine_Query::create()
                ->from('SubMenus s')
                ->where('s.id = ?',$res->getApproved())
                ->andWhere('s.stage_type = 2');
        $stage = $q->fetchOne();
        if($stage){
            return true;
        }else {
            return false;
        }
                
    }
     /**
     * Get Application details using form_id and entryid
     */
    public function getApplicationDetailsUsingCompositeDetails($form_id,$entryid){
        $q = Doctrine_Query::create()
                ->from('FormEntry f')
                ->where('f.form_id = ? ',$form_id)
                ->andWhere('f.entry_id = ? ',$entryid) ;
        $res = $q->fetchOne();
        return $res ;
                
    }
    /**
     * Get applications menu id using submenu id
     */
    public function getMenuId($submenu_id){
        error_log("Submenu to search for ".$submenu_id);
        $q = Doctrine_Query::create()
                ->from('SubMenus sm')
                ->where('sm.id = ? ',$submenu_id) ;
        //
        $res = $q->fetchOne();
        return $res ;
    }
    /**
     * Get stage with stage type approved for a selected menu
     */
    public function getStageWithSetStageType($stage_type,$menu_id){
      //  error_log("Stage Type >>> ".$stage_type);
       // error_log("Menu >>>>>> ".$menu_id);
        $q = Doctrine_Query::create()
                ->from('SubMenus s')
                ->where('s.stage_type = ?',$stage_type)
                ->andWhere('s.deleted = ?', 0)  //check only stages not deleted
            //This will cause errors if an application moves accross different submenus on different menus in which submenus have no stage type approved..
                ->andWhere('s.menu_id = ?', $menu_id) ;
        $res = $q->fetchOne();
        //error_log("Approved Stage is >>>> ".$res->getId());
        return $res;
                
    }
    /**
     * Check if an application was approved
     * This is done by checking if the application has a entry of submenu supplied which is of stage type 4 = approved
     */
    public function checkApplicationApproved($submenu_id,$app_id){
        $q = Doctrine_Query::create()
                ->from('ApplicationReference ref')
                ->where('ref.stage_id = ? ',$submenu_id)
                ->andWhere('ref.application_id = ?',$app_id) ;
        //
        $res = $q->execute();
        if(count($res)){
            return true; //approved
        }else{
            return false; //not approved
        }
    }

    public function ApplicationHasPermit($app_id){
        $q = Doctrine_Query::create()
                ->from('SavedPermit sp')
                ->leftJoin('sp.FormEntry a')
				->leftJoin('sp.Template t')
                ->where('t.parttype = 1 ')
                ->andWhere('a.id = ?',$app_id) ;
        return $q->count();
    }

	//OTB Start - Password change from old system - also used for password expire
    // validate a user password using old md5
    public function checkPassOldMethod($user_id,$password){
        
         $q = Doctrine_Query::create()
             ->from("CfUser a")
             ->where("a.nid = ?",$user_id)
             ->andWhere('a.bdeleted = ?', 0);
          $available_user = $q->fetchOne();
          //
          if($available_user)
          {
               //$hash = $available_user->getStrpassword();
			   $strMd5Password = md5($password);
               if ($strMd5Password == $available_user->getStrpassword()) {
                   //valid password
                   return true ;
               }else {
                   //invalid password
                   return false;
               }
          }
    }
    /**
     * check if user has changed password
     */
    public function hasUserChangedPassword($user_id){     
        $q = Doctrine_Query::create()
             ->from("CfUser a")
             ->where("a.nid = ?",$user_id)
             ->andWhere('a.bdeleted = ?', 0);
          $available_user = $q->fetchOne();
          if($available_user->getPassChange() == 0){
              return false;
          }
          else {
              return true;
          }
    }
	//OTB End - Password change from old system - also used for password expire
}
