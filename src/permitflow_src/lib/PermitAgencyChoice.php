<?php

/**
 * Permits form.
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PermitAgencyChoice extends sfWidgetFormDoctrineChoice
{
  public function getChoices()
   {
      $agencyManager = new AgencyManager();
      $choices = parent::getChoices();
      $allowed_permits = array();
        $q = Doctrine_Query::create()
                ->from('Permits p')
                ->whereIn('p.id', array_keys($choices))
                ->innerJoin('p.Stage s')
                ->orderBy('s.menu_id');
        $pts = $q->execute();
      foreach($pts as $pt){
            if($agencyManager->checkAgencyStageAccess($_SESSION["SESSION_CUTEFLOW_USERID"], $pt->getStage()->getId())){
                $allowed_permits[$pt->getId()] = $pt->getTitle();
            }
      
      }
      
      return $allowed_permits;
   }
}
