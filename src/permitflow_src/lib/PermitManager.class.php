<?php

require_once 'autoload.php';

use Rhumsaa\Uuid\Uuid;
use Rhumsaa\Uuid\Exception\UnsatisfiedDependencyException;

/**
 *
 * Permit class that manages the creation and modification of permits
 *
 * Created by PhpStorm.
 * User: thomasjuma
 * Date: 11/19/14
 * Time: 2:15 AM
 */

class PermitManager {

    //Public construction for the permit manager class
    public function PermitManager()
    {

    }

    //output permit to html
    public function generate_permit_template($permit_id, $pdf=false)
    {
        $templateparser = new TemplateParser();

        $q = Doctrine_Query::create()
            ->from('SavedPermit a')
            ->where('a.id = ?', $permit_id)
            ->limit(1);
        $savedpermit = $q->fetchOne();

        $application = $savedpermit->getApplication();

        $html = "<html>
        <body>
        ";

        $cancelled = false;
        $expired = false;

        if($savedpermit->getPermitStatus() == 3)
        {
            $cancelled = true;
        }

        $db_date_event = str_replace('/', '-', $savedpermit->getDateOfExpiry());

        $db_date_event = strtotime($db_date_event);

        if (time() > $db_date_event && !$cancelled)
        {
            $expired = true;
        }

        if($cancelled)
        {
            $html = $html. "<style type='text/css'>
          .watermark {
            color: #680000;
            font-size: 50pt;
            -webkit-transform: rotate(-45deg);
            -moz-transform: rotate(-45deg);
            position: absolute;
            width: 100%;
            height: 200px;
            margin: 0;
            left:100px;
            top:400px;
            z-index: 1;
          }
        </style>";

            $html = $html."<div class='watermark'>Cancelled</div>";
        }


        $servername = "localhost";
        $username = "admin";
        $password = "password";
        $dbname = "maseru_cp_v1";

//CBS version 23-09-2021
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
        
        // Check connection
        if ($conn->connect_error) {
          die("Connection failed: " . $conn->connect_error);
        }
        error_log("Connected successfully");

        $sql = "SELECT * FROM permits WHERE id =".$savedpermit->getTypeId();
        $result = $conn->query($sql);
        
        $content= "";
        if ($result->num_rows > 0) {
          // output data of each row
          while($row = $result->fetch_assoc()) {
            $content = $row["content"];
            //error_log("~~~~~~~~~~~~~~PERMIT (ID): ".$content."\n\n");
          }
        } else {
          error_log("0 results\n\n\n");
        }
        $conn->close();


        
        
        $q = Doctrine_Query::create()
            ->from("Permits a")
            ->where("a.id = ?", $savedpermit->getTypeId())
            ->limit(1);
        $permit_template = $q->fetchOne();

        error_log("debug: PERMIT ID ENA ke: ".$savedpermit->getTypeId());

        $permit_content = "";

        $permit_template->setContent($content);

        if($permit_template)
        {
            
            //error_log("TEMPLATE FOUND--------@@@@@@--------:".$permit_template->getContent());
            if($permit_template->getContent()) {
              if(sfConfig::get('app_old_parser'))
              {
                $permit_content = $templateparser->parsePermitOld($application->getId(), $application->getFormId(), $application->getEntryId(), $savedpermit->getId(), $content);
              }
              else
              {
                try {
                    $permit_content = $templateparser->parsePermit($application->getId(), $application->getFormId(), $application->getEntryId(), $savedpermit->getId(), $content);
                    //$permit_content = $templateparser->parsePermit($application->getId(), $application->getFormId(), $application->getEntryId(), $savedpermit->getId(), $permit_template->getContent()); using Doctrine ORM
                }
                catch(Exception $ex)
                {
                    error_log("Debug-p: Could not use new template parser: ".$ex);
                    $permit_content = $templateparser->parsePermitOld($application->getId(), $application->getFormId(), $application->getEntryId(), $savedpermit->getId(), $permit_template->getContent());
                }
              }
            }
            else
            {
                $permit_content = "<h3>This permit template is blank. Please contact system administrator.</h3>";
            }
        }
        else
        {
            $q = Doctrine_Query::create()
                ->from("Permits a")
                ->where("a.applicationform = ?", $application->getFormId())
                ->limit(1);
            $permit_template = $q->fetchOne();

            if($permit_template)
            {
                $permit_content = $templateparser->parsePermit($application->getId(), $application->getFormId(), $application->getEntryId(), $savedpermit->getId(), $permit_template->getContent());
            }
        }

        if($pdf == false)
        {
            $ssl_suffix = "s";

            if (empty($_SERVER['HTTPS'])) {
                $ssl_suffix = "";
            }

            //replace src=" for images with src="http://localhost
            $permit_content = str_replace('<img src="', '<img src="http'.$ssl_suffix.'://'.$_SERVER['HTTP_HOST'].'/', $permit_content);
        }

        $html .= $permit_content;

        if($cancelled)
        {
            $html = "This permit has been cancelled";
        }

        $html .= "
        </body>
        </html>";

        if(empty($savedpermit->getRemoteUpdateUuid()))
        {
            $permit_manager = new PermitManager();
            $uuid = $permit_manager->generate_uuid();
            error_log("UUID Log: ".$uuid);

            $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
            mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

            $sql = "UPDATE saved_permit SET remote_update_uuid = '".$uuid."' WHERE id = ".$savedpermit->getId();
            $result = mysql_query($sql, $db_connection);
        }

        return html_entity_decode($html);
    }
    
    //output permit to html
    public function generate_archive_permit_template($permit_id, $pdf=false)
    {
        $templateparser = new TemplateParser();

        $q = Doctrine_Query::create()
            ->from('SavedPermitArchive a')
            ->where('a.id = ?', $permit_id)
            ->limit(1);
        $savedpermit = $q->fetchOne();

        $application = $savedpermit->getApplication();

        $html = "<html>
        <body>
        ";

        $cancelled = false;
        $expired = false;

        if($savedpermit->getPermitStatus() == 3)
        {
            $cancelled = true;
        }

        $db_date_event = str_replace('/', '-', $savedpermit->getDateOfExpiry());

        $db_date_event = strtotime($db_date_event);

        if (time() > $db_date_event && !$cancelled)
        {
            $expired = true;
        }

        if($cancelled)
        {
            $html = $html. "<style type='text/css'>
          .watermark {
            color: #680000;
            font-size: 50pt;
            -webkit-transform: rotate(-45deg);
            -moz-transform: rotate(-45deg);
            position: absolute;
            width: 100%;
            height: 200px;
            margin: 0;
            left:100px;
            top:400px;
            z-index: 1;
          }
        </style>";

            $html = $html."<div class='watermark'>Cancelled</div>";
        }

        $q = Doctrine_Query::create()
            ->from("Permits a")
            ->where("a.id = ?", $savedpermit->getTypeId())
            ->limit(1);
        $permit_template = $q->fetchOne();

        $permit_content = "";

        if($permit_template)
        {
            if($permit_template->getContent()) {
                try {
                    $permit_content = $templateparser->parseArchivePermit($application->getId(), $application->getFormId(), $application->getEntryId(), $savedpermit->getId(), $permit_template->getContent());
                }
                catch(Exception $ex)
                {
                    error_log("Debug-p: Could not use new template parser: ".$ex);
                    $permit_content = $ex;
                }
            }
            else
            {
                $permit_content = "<h3>This permit template is blank. Please contact system administrator.</h3>";
            }
        }
        else
        {
            $q = Doctrine_Query::create()
                ->from("Permits a")
                ->where("a.applicationform = ?", $application->getFormId())
                ->limit(1);
            $permit_template = $q->fetchOne();

            if($permit_template)
            {
                $permit_content = $templateparser->parsePermit($application->getId(), $application->getFormId(), $application->getEntryId(), $savedpermit->getId(), $permit_template->getContent());
            }
        }

        if($pdf == false)
        {
            $ssl_suffix = "s";

            if (empty($_SERVER['HTTPS'])) {
                $ssl_suffix = "";
            }

            //replace src=" for images with src="http://localhost
            $permit_content = str_replace('<img src="', '<img src="http'.$ssl_suffix.'://'.$_SERVER['HTTP_HOST'].'/', $permit_content);
        }

        $html .= $permit_content;

        if($cancelled)
        {
            $html = "This permit has been cancelled";
        }

        $html .= "
        </body>
        </html>";

        if(empty($savedpermit->getRemoteUpdateUuid()))
        {
            $permit_manager = new PermitManager();
            $uuid = $permit_manager->generate_uuid();
            error_log("UUID Log: ".$uuid);

            $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
            mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

            $sql = "UPDATE saved_permit SET remote_update_uuid = '".$uuid."' WHERE id = ".$savedpermit->getId();
            $result = mysql_query($sql, $db_connection);
        }

        return html_entity_decode($html);
    }

    //output invoice to pdf
    public function save_to_pdf($permit_id)
    {
        $q = Doctrine_Query::create()
            ->from('SavedPermit a')
            ->where('a.id = ?', $permit_id)
            ->limit(1);
        $permit = $q->fetchOne();

        $q = Doctrine_Query::create()
            ->from("Permits a")
            ->where("a.id = ?", $permit->getTypeId())
            ->limit(1);
        $template = $q->fetchOne();

        $html = $this->generate_permit_template($permit_id, true);

        require_once(dirname(__FILE__)."/vendor/dompdf/dompdf_config.inc.php");

        $dompdf = new DOMPDF();
        //error_log("DEBUG>>>>>>>>>>>>>>>----------------<<<<<<<<<<<<<<<<<<".$html);
        try {
        $dompdf->load_html($html);
        }catch(Exception $ex)
        {
            error_log("Debug-p:".$ex);

        }
        //Define the PDF page settings
        if($template->getPageType() == "A5")
        {
            if($template->getPageOrientation() == "landscape")
            {
                $dompdf->set_paper("A5", "landscape");
            }
            else
            {
                $dompdf->set_paper("A5", "potrait");
            }
        }
        else
        {
            if($template->getPageOrientation() == "landscape")
            {
                $dompdf->set_paper("A4", "landscape");
            }
            else
            {
                $dompdf->set_paper("A4", "potrait");
            }
        }

        $dompdf->render();
        $dompdf->stream($permit->getFormEntry()->getApplicationId().".pdf");
    }
    
     //output invoice to pdf
    public function save_archive_to_pdf($permit_id)
    {
        $q = Doctrine_Query::create()
            ->from('SavedPermitArchive a')
            ->where('a.id = ?', $permit_id)
            ->limit(1);
        $permit = $q->fetchOne();

        $html = $this->generate_archive_permit_template($permit_id, true);

        require_once(dirname(__FILE__)."/vendor/dompdf/dompdf_config.inc.php");

        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->render();
        $dompdf->stream($permit->getApplication()->getApplicationId().".pdf");
    }

    //output invoice to pdf but save as file locally and return link
    public function save_to_pdf_locally($permit_id)
    {
        /**$q = Doctrine_Query::create()
            ->from('SavedPermit a')
            ->where('a.id = ?', $permit_id)
            ->limit(1);
        $permit = $q->fetchOne();

        $html = $this->generate_permit_template($permit_id, true);

        require_once(dirname(__FILE__)."/vendor/dompdf/dompdf_config.inc.php");

        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->render();

        $output = $dompdf->output();

        $filename = md5($permit->getFormEntry()->getApplicationId()."-".date("Y-m-d g:i:s")).'.pdf';

        $q = Doctrine_Query::create()
            ->from("ApSettings a");

        $settings = $q->fetchOne();
        if($settings) {
            try {
                if (substr($settings->getUploadDir(), 1) == "/") {
                    $file_to_save = $settings . '/' . $filename;
                    error_log("Debug-p: ".$file_to_save);
                    file_put_contents($file_to_save, $output);
                } else {
                    $file_to_save = $settings->getUploadDir() . '/' . $filename;
                    error_log("Debug-p: ".$file_to_save);
                    file_put_contents($file_to_save, $output);
                }
            }catch(Exception $ex)
            {
                error_log("Debug-p: ".$ex);
            }
        }
        **/
        return $filename;
    }
    
    //output invoice to pdf but save as file locally and return link
    public function save_archive_to_pdf_locally($permit_id)
    {
        $q = Doctrine_Query::create()
            ->from('SavedPermitArchive a')
            ->where('a.id = ?', $permit_id)
            ->limit(1);
        $permit = $q->fetchOne();

        $html = $this->generate_archive_permit_template($permit_id, true);

        require_once(dirname(__FILE__)."/vendor/dompdf/dompdf_config.inc.php");

        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->render();

        $output = $dompdf->output();

        $filename = md5($permit->getFormEntry()->getApplicationId()."-".date("Y-m-d g:i:s")).'.pdf';

        $q = Doctrine_Query::create()
            ->from("ApSettings a");

        $settings = $q->fetchOne();
        if($settings) {
            try {
                if (substr($settings->getUploadDir(), 1) == "/") {
                    $file_to_save = $settings . '/' . $filename;
                    error_log("Debug-p: ".$file_to_save);
                    file_put_contents($file_to_save, $output);
                } else {
                    $file_to_save = $settings->getUploadDir() . '/' . $filename;
                    error_log("Debug-p: ".$file_to_save);
                    file_put_contents($file_to_save, $output);
                }
            }catch(Exception $ex)
            {
                error_log("Debug-p: ".$ex);
            }
        }

        return $filename;
    }

    //Generate a new permit
    public function create_permit($application_id)
    {
        $submission = $this->get_application_by_id($application_id);

        $q = Doctrine_Query::create()
            ->from("Permits a")
            ->where("a.applicationstage = ?", $submission->getApproved())
            ->andWhere("a.applicationform = ?", $submission->getFormId())
            ->limit(1);
        $permit_template = $q->fetchOne();

        if ($permit_template) {
            $date_of_response = date("Y-m-d H:i:s");
            $date_of_issue = $date_of_response;

            $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
            mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

            $sql = "UPDATE form_entry SET date_of_response = '".date("Y-m-d H:i:s")."', date_of_issue = '".date("Y-m-d H:i:s")."' WHERE id = ".$submission->getId();
            mysql_query($sql, $db_connection);

            $new_permit = new SavedPermit();
            $new_permit->setTypeId($permit_template->getId());
            $new_permit->setApproverInfoId($permit_template->getApproverInfoId());//OTB patch
            $new_permit->setApplicationId($submission->getId());
            $new_permit->setDateOfIssue($date_of_issue);

            /*if ($permit_template->getMaxDuration() > 0) {
                $date = strtotime("+" . $permit_template->getMaxDuration() . " day");
                $new_permit->setDateOfExpiry(date('Y-m-d', $date));
            }*///OTB - Commented so we can get override logic for validity period of permit

			//OTB - Start option for authorized user to set max duration for each permit instance
            if ($permit_template->getMaxDuration() > 0 or $permit_template->getUserSetDuration()) {
				$date = $this->get_permit_expiry_date($submission->getId(), $permit_template->getId(), $permit_template->getMaxDuration());
                $new_permit->setDateOfExpiry(date('Y-m-d', $date));
            }
			//OTB - End option for authorized user to set max duration for each permit instance

			//OTB Start Patch - Set Permit number
			  //if permit template has its own unique identifier then assign it
			  if($permit_template->getFooter())
			  {
				 $q = Doctrine_Query::create()
					->from('SavedPermit a')
					->where('a.type_id = ?', $permit_template->getId())
					->orderBy("a.permit_id DESC");
				 $last_permit = $q->fetchOne();

				 $new_permit_id = ""; //submission identifier
				 $identifier_start = ""; //first stage

				 if($last_permit && $last_permit->getPermitId())
				 {
					 $new_permit_id = $last_permit->getPermitId();
					 $new_permit_id = ++$new_permit_id;
				 }
				 else
				 {
					 $new_permit_id = $permit_template->getFooter();
				 }

				 $new_permit->setPermitId($new_permit_id);
			  }
			  /*else
			  {
				 $new_permit->setPermitId($submission->getApplicationId());
			  }*/
			//OTB End Patch - Set Permit number

            $new_permit->setCreatedBy(0);
            $new_permit->setLastUpdated($date_of_issue);
            $new_permit->save();

            if(empty($new_permit->getRemoteUpdateUuid()))
            {
                $permit_manager = new PermitManager();
                $uuid = $permit_manager->generate_uuid();
                error_log("UUID Log: ".$uuid);

                $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
                mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

                $sql = "UPDATE saved_permit SET remote_update_uuid = '".$uuid."' WHERE id = ".$new_permit->getId();
                $result = mysql_query($sql, $db_connection);
            }
        }
    }

    //Returns an existing application
    public function get_application_by_id($application_id)
    {
        $q = Doctrine_Query::create()
            ->from('FormEntry a')
            ->where('a.id = ?', $application_id)
            ->orderBy("a.application_id DESC")
            ->limit(1);
        $existing_app = $q->fetchOne();
        if($existing_app)//Already submitted then tell client its already submitted
        {
            return $existing_app;
        }
        else
        {
            return false;
        }
    }

    //Check if the application has a permit
    public function has_permit($application_id)
    {
        $q = Doctrine_Query::create()
            ->from("SavedPermit a")
            ->where("a.application_id = ?", $application_id)
            ->andWhere("a.permit_status <> 3")
            ->limit(1);
        $saved_permit = $q->fetchOne();

        if($saved_permit)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //Check if the application has a permit for the current stage in the workflow
    public function needs_permit_for_current_stage($application_id)
    {
        $submission = $this->get_application_by_id($application_id);

        $q = Doctrine_Query::create()
            ->from("Permits a")
            ->where("a.applicationstage = ?", $submission->getApproved())
            ->andWhere("a.applicationform = ?", $submission->getFormId())
            ->limit(1);
        $permit_template = $q->fetchOne();

        if($q->count())
        {
            $q = Doctrine_Query::create()
                ->from("SavedPermit a")
                ->where("a.application_id = ?", $application_id)
                ->andWhere("a.type_id = ?", $permit_template->getId())
                ->andWhere("a.permit_status <> 3")
                ->limit(1);
            $saved_permit = $q->fetchOne();

            if ($q->count() == 0) {
                //No permits found so we need to generate a permit
                return true;
            } else {
                //Permit found so no need for a new permit
                return false;
            }
        }
        else {
            //return true coz there is no need generate a permit on stage that doesn't require a permit
            return true;
        }
    }

    //Check if a permit has public access
    public function has_public_permissions($template_id)
    {
        $public_permits_settings = sfConfig::get('app_public_permits');
        $public_permits = explode(",", $public_permits_settings);

        if(in_array($template_id, $public_permits))
        {
            return true;
        }
        else
        {
            if($template_id == sfConfig::get('app_public_permits'))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    //Check if a permit has public access
    public function get_public_template($template_id)
    {
        $public_permits_settings = sfConfig::get('app_public_permits');
        return $public_permits_settings;
    }

    //Generate an adhoc (not-attached to any account) PDF if a permit has public access
    public function generate_public_pdf($template_id, $reference)
    {
        if($reference == '')
        {
            echo "<h3>No records found for your reference number</h3>";
            exit;
        }

        error_log("Debug-t: Attempting to generate permit for ".$template_id." and reference ".$reference);

        $q = Doctrine_Query::create()
            ->from("Permits a")
            ->where("a.id = ?", $template_id)
            ->limit(1);
        $permit_template = $q->fetchOne();

        if ($permit_template) {
            error_log("Debug-t: Found template");

            $template = $permit_template->getContent();

            //If form has remote settings, try and parse remote content
            if($permit_template->getRemoteUrl())
            {
                error_log("Debug-t: Found remote fetch");

                $results = null;

                $templateparser = new templateparser();

                $remote_url = $permit_template->getRemoteUrl();
                $remote_fields = $permit_template->getRemoteField();
                $remote_username = $permit_template->getRemoteUsername();
                $remote_password = $permit_template->getRemotePassword();

                $remote_url = str_replace('$reference',$reference, $remote_url);

                error_log("Debug-t: Remote URL: ".$remote_url);

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $remote_url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                if(!empty($remote_username) && !empty($remote_password))
                {
                    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                    curl_setopt($ch, CURLOPT_USERPWD, "$remote_username:$remote_password");
                }
                curl_setopt($ch, CURLOPT_ENCODING ,"");

                $results = curl_exec($ch);

                if($error = curl_error($ch))
                {
                    $error = "curl_error:" . $error . "<br />";
                    error_log("Debug-t: ".$error);
                }
                else
                {
                    error_log("Debug-t: Results found ".$results);

                    if($results == '{"count":0,"records":[]}')
                    {
                        echo "<h3>No records found for your reference number</h3>";
                        exit;
                    }
                    else
                    {
                        $template = $templateparser->parsePublicPermit($template, $reference, $results);
                    }
                }
            }

            require_once(dirname(__FILE__)."/vendor/dompdf/dompdf_config.inc.php");

            $dompdf = new DOMPDF();
            $dompdf->load_html($template);
            $dompdf->render();
            $dompdf->stream($reference.".pdf");
        }
        else
        {
            error_log("Debug-t: No template found");
            echo "<h3>Unauthorized - No template found</h3>";
            exit;
        }
    }

    //Generate an adhoc (not-attached to any account) HTML if a permit has public access
    public function generate_public_html($template_id, $reference)
    {
        if($reference == '')
        {
            echo "<h3>No records found for your reference number</h3>";
            exit;
        }

        error_log("Debug-t: Attempting to generate permit for ".$template_id." and reference ".$reference);

        $q = Doctrine_Query::create()
            ->from("Permits a")
            ->where("a.id = ?", $template_id)
            ->limit(1);
        $permit_template = $q->fetchOne();

        if ($permit_template) {
            error_log("Debug-t: Found template");

            $template = $permit_template->getContent();

            //If form has remote settings, try and parse remote content
            if($permit_template->getRemoteUrl())
            {
                error_log("Debug-t: Found remote fetch");

                $results = null;

                $templateparser = new templateparser();

                $remote_url = $permit_template->getRemoteUrl();
                $remote_fields = $permit_template->getRemoteField();
                $remote_username = $permit_template->getRemoteUsername();
                $remote_password = $permit_template->getRemotePassword();

                $remote_url = str_replace('$reference',$reference, $remote_url);

                error_log("Debug-t: Remote URL: ".$remote_url);

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $remote_url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                if(!empty($remote_username) && !empty($remote_password))
                {
                    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                    curl_setopt($ch, CURLOPT_USERPWD, "$remote_username:$remote_password");
                }
                curl_setopt($ch, CURLOPT_ENCODING ,"");

                $results = curl_exec($ch);

                if($error = curl_error($ch))
                {
                    $error = "curl_error:" . $error . "<br />";
                    error_log("Debug-t: ".$error);
                }
                else
                {
                    error_log("Debug-t: Results found ".$results);

                    if($results == '{"count":0,"records":[]}')
                    {
                        echo "<h3>No records found for your reference number</h3>";
                        exit;
                    }
                    else
                    {
			$template = $templateparser->parsePublicPermit($template, $reference, $results);

			//Temporary fix to remove expiry date from permits that don't have an expiry date
			$template = str_replace('until
<strong></strong>
.','',$template);
                    }
                }
            }

            $ssl_suffix = "s";

            if (empty($_SERVER['HTTPS'])) {
                $ssl_suffix = "";
            }

            $template = str_replace('<img src="', '<img src="http'.$ssl_suffix.'://'.$_SERVER['HTTP_HOST'].'/', $template);

            $template = $template.'<button class="btn btn-primary" onClick="window.location=\'http://'.$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI].'&print=1\';">Print PDF</button>';

            return $template;
        }
        else
        {
            error_log("Debug-t: No template found");
            echo "<h3>Unauthorized - No template found</h3>";
            exit;
        }
    }

    //Get remote data and save to the permit
    public function get_remote_result($permit_id)
    {
        $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

        $q = Doctrine_Query::create()
            ->from("SavedPermit a")
            ->where("a.id = ?", $permit_id)
            ->limit(1);
        $saved_permit = $q->fetchOne();

        $q = Doctrine_Query::create()
            ->from("FormEntry a")
            ->where("a.id = ?", $saved_permit->getFormEntry()->getId())
            ->limit(1);
        $application = $q->fetchOne();

        $q = Doctrine_Query::create()
            ->from("ApFormElements a")
            ->where("a.form_id = ?", $application->getFormId())
            ->andWhere("a.element_option_query <> ?", "");
        $form_elements = $q->execute();

        foreach($form_elements as $element)
        {
            $sql = "SELECT * FROM ap_form_".$application->getFormId()." WHERE id = ".$application->getEntryId();
            $results = mysql_query($sql, $dbconn);

            $entry_data  = mysql_fetch_assoc($results);

            $remote_url = $element->getElementOptionQuery();
            $criteria = $element->getElementFieldName();
            $remote_template = $element->getElementFieldValue();
            $remote_username = $element->getElementRemoteUsername();
            $remote_password = $element->getElementRemotePassword();


            $ch = curl_init();

            $pos = strpos($remote_url, '$value');

            if($pos === false)
            {
                //dont' do anything
            }
            else
            {
                $remote_url = str_replace('$value', curl_escape($ch, $entry_data['element_'.$element->getElementId()]), $remote_url);
            }

            curl_setopt($ch, CURLOPT_URL, $remote_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));

            if(!empty($remote_username) && !empty($remote_password))
            {
                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                curl_setopt($ch, CURLOPT_USERPWD, "$remote_username:$remote_password");
            }

            $results = curl_exec($ch);

            error_log("Remote Results: ".$results);

            if(curl_error($ch))
            {
                $error = "error:" . curl_error($ch) . "<br />";
            }

            $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            curl_close($ch);

            if(empty($error))
            {
                $values = json_decode($results);
                if($criteria == "records")
                {
                    //If count is = 0, fail
                    if($values->{'count'} == 0)
                    {
                        $error = "No records found on server";
                    }
                }
                else if($criteria == "norecords")
                {
                    //If count is greater than 0, then pass
                    if($values->{'count'} > 0)
                    {
                        $error = "Existing records found on server";
                    }
                }

                return $results;
            }
        }
    }

    //generate new uuid code for a record
    public function generate_uuid()
    {
      try {

        // Generate a version 1 (time-based) UUID object
        $uuid1 = Uuid::uuid1();
        return $uuid1->toString(); // e4eaaaf2-d142-11e1-b3e4-080027620cdd

      } catch (UnsatisfiedDependencyException $e) {

        // Some dependency was not met. Either the method cannot be called on a
        // 32-bit system, or it can, but it relies on Moontoast\Math to be present.
        error_log('Caught exception: ' . $e->getMessage());

      }
    }

	//OTB - Start Option for authorized user to set max duration for each permit instance - cache any details that need to be applied to a permit when it is generated
    public function get_permit_template_to_cache($application, $stage)
    {
        $q = Doctrine_Query::create()
            ->from('PermitCache a')
            ->where('a.application_id = ?', $application->getId())
            ->andWhere('a.status = ?', 'pending')
            ->limit(1);
        $cache_permit = $q->fetchOne();
        $q = Doctrine_Query::create()
            ->from('Permits a')
            ->where('a.stage_set_duration = ?', $stage)
            ->andWhere('a.applicationform = ?', $application->getFormId())
            ->andWhere('a.user_set_duration = ?', True)
            ->limit(1);
        $permit = $q->fetchOne();
        if($permit and !$cache_permit)
        {
            return $permit->getId();
        }
        else
        {
            return false;
        }
	}

    public function get_permit_expiry_date($application_id, $template_id, $defaul_max_duration)
    {
        $q = Doctrine_Query::create()
            ->from('PermitCache a')
            ->where('a.application_id = ?', $application_id)
            ->andWhere('a.template_id = ?', $template_id)
            ->andWhere('a.status = ?', 'pending')
            ->orderBy('a.id DESC')// when selecting, return the latest value incase of double entries in permit cache
            ->limit(1);
        $cache_permit = $q->fetchOne();
		if($cache_permit){
			$cache_permit->setStatus('done');
			$cache_permit->save();
			if($cache_permit->getPeriodUom() == "years"){
				return strtotime("+" . $cache_permit->getValidityPeriod() . " year");
			}else if($cache_permit->getPeriodUom() == "months"){
				return strtotime("+" . $cache_permit->getValidityPeriod() . " month");
			}else if($cache_permit->getPeriodUom() == "days"){
				return strtotime("+" . $cache_permit->getValidityPeriod() . " day");
			}			
		}else{
			return strtotime("+" . $defaul_max_duration . " day");
		}
	}
	//OTB - Start Option for authorized user to set max duration for each permit instance - cache any details that need to be applied to a permit when it is generated
}
