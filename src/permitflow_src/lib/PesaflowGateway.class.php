<?php
/**
 *
 * Manages Pesaflow Payments Gateway
 *
 * User: thomasjuma
 * Date: 11/19/14
 * Time: 12:26 AM
 */

class PesaflowGateway {

    private $suffix = "s";
    public $invoice_manager = null;

    //Constructor for PesaflowGateway class
    public function PesaflowGateway()
    {
        $this->invoice_manager = new InvoiceManager();

        if (empty($_SERVER['HTTPS'])) {
            $this->suffix = "";
        }
    }

    //Display a checkout for the current gateway
    public function checkout($invoice_id, $payment_settings, $backend = false)
    {
        $invoice = $this->invoice_manager->get_invoice_by_id($invoice_id);
        $application = $invoice->getFormEntry();

        $user = Doctrine_Core::getTable('SfGuardUser')->find(array($application->getUserId()));

        $fullname = $user->getProfile()->getFullname();
        
        $fullname = str_replace("'", "", $fullname);
        
        $idnumber = $user->getUsername();
        $email = $user->getEmailAddress();

        //One of email or phonenumber is required
        $phonenumber = $user->getProfile()->getMobile();

        //Get Payment Details
        $payment_description = $invoice->getFormEntry()->getForm()->getFormName();
        $payment_amount = $invoice->getTotalAmount();
        $merchant_reference = $this->invoice_manager->get_merchant_reference($invoice->getId());

        //Params
        $apiClientID = $payment_settings['payment_cellulant_checkout_url']; //API Client ID Number - Test 1
        $key = $payment_settings['payment_cellulant_merchant_username']; //Key  -- For authentication - Test qpZkwRCm1d10784
        $secret = $payment_settings['payment_cellulant_merchant_password']; //Secret -- For authentication - Test wyDjxWr9A1SX850KtMn
        $serviceID = $payment_settings['payment_cellulant_service_id'];
        $payment_currency = $payment_settings['payment_currency'];

        $amountExpected = $payment_amount;

        $q = Doctrine_Query::create()
           ->from("ApFormPayments a")
           ->where("a.form_id = ? AND a.record_id = ?", array($invoice->getFormEntry()->getFormId(), $invoice->getFormEntry()->getEntryId()))
           ->andWhere("a.status <> ? or a.payment_status <> ?", array(2, 'paid'));
        $transaction = $q->fetchOne();

        if($transaction)
        {
          //Should we update any existing transactions at this point?
        }
        else
        {
          //Add transaction details
          $transaction = new ApFormPayments();
          $transaction->setFormId($invoice->getFormEntry()->getFormId());
          $transaction->setRecordId($invoice->getFormEntry()->getEntryId());
          $transaction->setPaymentId($merchant_reference);
          $transaction->setDateCreated(date("Y-m-d H:i:s"));
          $transaction->setPaymentFullname($fullname);
          $transaction->setPaymentAmount($amountExpected);
          $transaction->setPaymentCurrency($payment_currency);
          $transaction->setPaymentMerchantType('Pesaflow');
          $transaction->setPaymentTestMode("0");

          $transaction->setPaymentStatus("pending");
          $transaction->setStatus(15);

          $transaction->save();
        }

        $callBackURLOnSuccess = null;
        $callBackURLOnFail = null;

        if($backend)
        {
            $callBackURLOnSuccess = 'http'.$this->suffix.'://'.$_SERVER['HTTP_HOST'].'/backend.php/applications/confirmpayment?id='.$invoice->getFormEntry()->getFormId().'&entryid='.$invoice->getFormEntry()->getEntryId().'&done=1&invoiceid='.$invoice->getId().'&status=201'; //redirect url, the page that will handle the response from pesaflow.
            $callBackURLOnFail = 'http'.$this->suffix.'://'.$_SERVER['HTTP_HOST'].'/backend.php/applications/invalidpayment';
        }
        else
        {
            $callBackURLOnSuccess = 'http'.$this->suffix.'://'.$_SERVER['HTTP_HOST'].'/index.php/forms/confirmpayment?id='.$invoice->getFormEntry()->getFormId().'&entryid='.$invoice->getFormEntry()->getEntryId().'&done=1&invoiceid='.$invoice->getId().'&status=201'; //redirect url, the page that will handle the response from pesaflow.
            $callBackURLOnFail = 'http'.$this->suffix.'://'.$_SERVER['HTTP_HOST'].'/index.php/forms/invalidpayment';
        }

        if(sfConfig::get('app_pesaflow_demo'))
        {
            $iframe_url = sfConfig::get('app_pesaflow_demo');
        }
        else
        {
            $iframe_url = "https://pesaflow.ecitizen.go.ke/PaymentAPI/iframev2.php?";
        }

  	    $updateURL = 'http'.$this->suffix.'://'.$_SERVER['HTTP_HOST'].'/index.php/payment/updateinvoice';

  	    if($_SERVER['HTTP_HOST'] == "ntsa.ecitizen.go.ke" || $_SERVER['HTTP_HOST'] == "197.248.4.139")
  	    {
  	  	  $updateURL = 'http://197.248.4.139/index.php/payment/updateinvoice';
  	    }

        $account_type = "";

        if($user->getProfile()->getRegisteras() == 1)
        {
          $account_type = "citizen";
        }
        elseif($user->getProfile()->getRegisteras() == 3)
        {
          $account_type = "alien";
        }
        elseif($user->getProfile()->getRegisteras() == 4)
        {
          $account_type = "visitor";
        }

        $checkout = <<<EOT
<iframe name="iframe" width="100%" height="900px" scrolling="no" frameBorder="0">
<p>Browser unable to load iFrame</p> </iframe>
<form id="details" method="post" action="{$iframe_url}" target="iframe">
<input type="hidden" name="apiClientID" value="{$apiClientID}"/>
<input type="hidden" name="key" value="{$key}"/>
<input type="hidden" name="secret" value="{$secret}"/>
<input type="hidden" name="currency" value="{$payment_currency}"/>
<input type="hidden" name="billDesc" value="{$payment_description}"/>
<input type="hidden" name="billRefNumber" value="{$merchant_reference}"/>
<input type="hidden" name="serviceID" value="{$serviceID}"/>
<input type="hidden" name="clientMSISDN" value="{$phonenumber}"/>
<input type="hidden" name="clientName" value="{$fullname}"/>
<input type="hidden" name="clientEmail" value="{$email}"/>
<input type="hidden" name="clientIDNumber" value="{$idnumber}"/>
<input type="hidden" name="clientType" value="{$account_type}"/>
<input type="hidden" name="amountExpected" value="{$payment_amount}"/>
<input type="hidden" name="callBackURLOnSuccess" value="{$callBackURLOnSuccess}"/>
<input type="hidden" name="callBackURLOnFail" value="{$callBackURLOnFail}"/>
<input type="hidden" name="notificationURL" value="{$updateURL}"/>
</form>
<script> document.getElementById("details").submit(); </script>
EOT;

        return $checkout;
    }

    //Validate payment details received after redirect from checkout
    public function validate($invoice_id, $request_details, $payment_settings)
    {
        $invoice = $this->invoice_manager->get_invoice_by_id($invoice_id);

        //Check the status using the API and not the status from the URL
        $payment_status = "pending";

        $payment_status = $this->invoice_manager->remote_reconcile($invoice->getFormEntry()->getFormId()."/".$invoice->getFormEntry()->getEntryId()."/".$invoice->getId());

        if (isset($payment_status)) {

            $q = Doctrine_Query::create()
               ->from("ApFormPayments a")
               ->where("a.form_id = ? AND a.record_id = ?", array($invoice->getFormEntry()->getFormId(), $invoice->getFormEntry()->getEntryId()))
               ->andWhere("a.status <> ?", 2)
               ->andWhere("a.payment_amount = ?", $invoice->getTotalAmount());
            $transaction = $q->fetchOne();

            if($transaction)
            {
                //Update transaction details
                $transaction->setBillingState($request_details['transaction_id']);
                $transaction->setPaymentDate(date("Y-m-d H:i:s"));

                if($payment_status == "paid")
                {
                    $transaction->setStatus(2);
                    $transaction->setPaymentStatus("paid");
                }
                else
                {
                    $transaction->setStatus(15);
                    $transaction->setPaymentStatus("pending");
                }

                $transaction->save();
            }
            else
            {
                /** $application = $invoice->getFormEntry();

                $user = Doctrine_Core::getTable('SfGuardUser')->find(array($application->getUserId()));

                $fullname = $user->getProfile()->getFullname();

                //Add a new transaction if one doesn't exist
                $transaction = new ApFormPayments();
                $transaction->setFormId($invoice->getFormEntry()->getFormId());
                $transaction->setRecordId($invoice->getFormEntry()->getEntryId());
                $transaction->setPaymentId($invoice->getFormEntry()->getFormId()."/".$invoice->getFormEntry()->getEntryId()."/".$invoice->getId());
                $transaction->setDateCreated(date("Y-m-d H:i:s"));
                $transaction->setPaymentFullname($fullname);
                $transaction->setPaymentAmount($invoice->getTotalAmount());
                $transaction->setPaymentCurrency($invoice->getCurrency());
                $transaction->setPaymentMerchantType('Pesaflow');
                $transaction->setPaymentTestMode("0");

                $transaction->setBillingState($request_details['transactionid']);
                $transaction->setPaymentDate(date("Y-m-d H:i:s"));

                if($payment_status == "paid")
                {
                    $transaction->setStatus(2);
                    $transaction->setPaymentStatus("paid");
                }
                else
                {
                    $transaction->setStatus(15);
                    $transaction->setPaymentStatus("pending");
                }

                $transaction->save(); **/
            }

            error_log("Pesaflow: ".$request_details['status']."/".$this->invoice_manager->get_invoice_total_owed($invoice->getId()));

            if($payment_status == "paid")
            {
                //Payment is successful
                $invoice->setPaid(2);
                $invoice->setUpdatedAt(date("Y-m-d H:i:s"));
            }
            else
            {
                //Payment is incomplete
                $invoice->setPaid(15);
                $invoice->setUpdatedAt(date("Y-m-d H:i:s"));
            }

            //Update invoice and allow any triggers to take place
            $invoice->save();

            error_log("Pesaflow - Debug-x: Successful Validation - ".$invoice->getFormEntry()->getApplicationId());

            return true;
        }
        else
        {
            error_log("Pesaflow - Debug-x: Missing Status - ".$invoice->getFormEntry()->getApplicationId());
            return false;
        }
    }

    //Process payment notifications received from external payment server
    public function ipn($request_details)
    {
      try
      {
        $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

        $update_details = array();

        $transaction_id = $request_details["invoiceNumber"];
        $amount_paid = $request_details["amount"];

        if($request_details["transactiondate"])
        {
          $transaction_date = $request_details["transactiondate"];
        }
        elseif($request_details["transaction_date"])
        {
          $transaction_date = $request_details["transaction_date"];
        }

        if($request_details["transactionstatus"])
        {
          $transaction_status = $request_details["transactionstatus"];
        }
        elseif($request_details["transaction_status"])
        {
          $transaction_status = $request_details["transaction_status"];
        }

        if(empty($transaction_id))
        {
            $update_details['update_status'] = "invalid transaction id";
            return $update_details;
        }

        if(empty($amount_paid))
        {
            $update_details['update_status'] = "invalid transaction amount";
            return $update_details;
        }

        $amount_paid = $request_details["amount"];
        $paid_by = $request_details["paidby"];

        $invoice = $this->invoice_manager->get_invoice_by_reference($transaction_id);

        $application = $invoice->getFormEntry();
        $user = Doctrine_Core::getTable('SfGuardUser')->find(array($application->getUserId()));
        $fullname = $user->getProfile()->getFullname();

        if($invoice->getPaid() <> 2)
        {
          $q = Doctrine_Query::create()
             ->from("ApFormPayments a")
             ->where("a.form_id = ? AND a.record_id = ?", array($invoice->getFormEntry()->getFormId(), $invoice->getFormEntry()->getEntryId()))
             ->andWhere("a.status <> ?", 2)
             ->andWhere("a.payment_amount = ?", $invoice->getTotalAmount());
          $transaction = $q->fetchOne();

          if($transaction)
          {
              //Update transaction details
              $transaction->setBillingState($request_details['transaction_id']);
              $transaction->setPaymentDate(date("Y-m-d H:i:s"));

              if($transaction_status == 'completed')
              {
                  $transaction->setStatus(2);
                  $transaction->setPaymentStatus("paid");
              }
              elseif($transaction_status == 'failed')
              {
                  $transaction->setStatus(1);
                  $transaction->setPaymentStatus("failed");
              }
              else
              {
                  $transaction->setStatus(15);
                  $transaction->setPaymentStatus("pending");
              }

              $transaction->save();
          }
          else
          {
              $application = $invoice->getFormEntry();

              $user = Doctrine_Core::getTable('SfGuardUser')->find(array($application->getUserId()));

              $fullname = $user->getProfile()->getFullname();

              //Add a new transaction if one doesn't exist
              $transaction = new ApFormPayments();
              $transaction->setFormId($invoice->getFormEntry()->getFormId());
              $transaction->setRecordId($invoice->getFormEntry()->getEntryId());
              $transaction->setPaymentId($invoice->getFormEntry()->getFormId()."/".$invoice->getFormEntry()->getEntryId()."/".$invoice->getId());
              $transaction->setDateCreated(date("Y-m-d H:i:s"));
              $transaction->setPaymentFullname($fullname);
              $transaction->setPaymentAmount($invoice->getTotalAmount());
              $transaction->setPaymentCurrency($invoice->getCurrency());
              $transaction->setPaymentMerchantType('Pesaflow');
              $transaction->setPaymentTestMode("0");

              $transaction->setBillingState($transaction_id);
              $transaction->setPaymentDate(date("Y-m-d H:i:s"));

              if($transaction_status == 'completed')
              {
                  $transaction->setStatus(2);
                  $transaction->setPaymentStatus("paid");
              }
              elseif($transaction_status == 'failed')
              {
                  $transaction->setStatus(1);
                  $transaction->setPaymentStatus("failed");
              }
              else
              {
                  $transaction->setStatus(15);
                  $transaction->setPaymentStatus("pending");
              }

              $transaction->save();
          }

          if($transaction_status == 'completed' && $this->invoice_manager->get_invoice_total_owed($invoice->getId()) <= 0)
          {
              //Payment is successful
              $invoice->setPaid(2);
              $invoice->setUpdatedAt(date("Y-m-d H:i:s"));
          }
          elseif($transaction_status == 'failed')
          {
              //Payment has failed
              $invoice->setPaid(3);
              $invoice->setUpdatedAt(date("Y-m-d H:i:s"));
          }
          else
          {
              //Payment is incomplete
              $invoice->setPaid(15);
              $invoice->setUpdatedAt(date("Y-m-d H:i:s"));
          }

          //Update invoice and allow any triggers to take place
          $invoice->save();
        }
        else
        {
          $update_details['update_status'] = "already paid";
        }

        error_log("Pesaflow IPN: ".$request_details['status']."/".$this->invoice_manager->get_invoice_total_owed($invoice->getId()));


        //return invoice/payment details for confirmation
        if($invoice->getPaid() == 1)
        {
            $update_details['invoice_status'] = "pending";
        }
        elseif($invoice->getPaid() == 15)
        {
            $update_details['invoice_status'] = "pending confirmation";
        }
        elseif($invoice->getPaid() == 2)
        {
            $update_details['invoice_status'] = "paid";
        }
        elseif($invoice->getPaid() == 3)
        {
            $update_details['invoice_status'] = "cancelled";
        }

        $update_details['total_amount'] = $invoice->getTotalAmount();
        $update_details['currency'] = $invoice->getCurrency();
        $update_details['date_of_invoice'] = $invoice->getCreatedAt();
        $update_details['application_id'] = $application->getApplicationId();
        $update_details['user_email'] = $user->getEmailAddress();
        $update_details['user_mobile'] = $user->getProfile()->getMobile();
        $update_details['user_fullname'] = $user->getProfile()->getFullname();
      }
      catch(Exception $ex)
      {
        error_log("Debug-pesa: ".$ex);
      }

        return $update_details;
    }
}
