<?php
class PriceCalculator
{
  public function PriceCalculator()
  {

  }

  public function AGStampDuty($field_value)
  {
    return (($field_value * 0.01) + 2020);
  }

  public function LandsStampDuty($field_value)
  {
    return $field_value * 0.04;
  }

  /**
   * Retrieve a fee from dataflow
   *
   * $dataset - The dataset containing the records
   * $remote_username - HTTP authentication username
   * $remote_password - HTTP authentication password
   * $search_field - The field to search
   * $retrieve_field - The field that contains the fee
   * $value - The value from the application to use in the search field
   *
   * */
  public function DataflowCalculator($remote_url, $dataset, $remote_username, $remote_password, $search_field, $retrieve_field, $value)
  {
    $request_url = $remote_url."/".$dataset."/records?query={\"".$search_field."\":\"".$value."\"}";

    error_log("Dataflow URL: ".$request_url);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $request_url);
    curl_setopt($this->curl_request, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($this->curl_request, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($this->curl_request, CURLOPT_HTTPHEADER, array('Expect:'));

    //If username and password are set, use http authentication
    if(!empty($remote_username) && !empty($remote_password))
    {
      curl_setopt($this->curl_request, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
      curl_setopt($this->curl_request, CURLOPT_USERPWD, "$remote_username:$remote_password");
    }

    $response = curl_exec($ch);

    error_log("Dataflow Price Calculator: Sent - ".$remote_url.", Response: ".$request_url);

    return $response;
  }

  /**
   * Retrieve a fee from bill engine
   *
   * $dataset - The dataset containing the records
   * $remote_username - HTTP authentication username
   * $remote_password - HTTP authentication password
   * $search_field - The field to search
   * $retrieve_field - The field that contains the fee
   * $value - The value from the application to use in the search field
   *
   * */
  public function BillEngineCalculator($remote_url, $remote_username, $remote_password, $retrieve_field, $value)
  {
    $remote_url  = str_replace('$value', $value, $remote_url);

    error_log("BillEngine URL: ".$remote_url);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $remote_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));

    //If username and password are set, use http authentication
    if(!empty($remote_username) && !empty($remote_password))
    {
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
      curl_setopt($ch, CURLOPT_USERPWD, "$remote_username:$remote_password");
    }

    $response = curl_exec($ch);

    error_log("BillEngine Price Calculator: Sent - ".$remote_url.", Response: ".$response);

    curl_close($ch);

    $error = curl_error($ch);

    if(empty($error))
    {
      //Should we filter the field here
      $json_array = json_decode($response, true);

      if(is_array($json_array['billing']))
      {
        error_log("BillEngine Value: ". $json_array['billing'][$retrieve_field]);
        return $json_array['billing'][$retrieve_field];
      }
      else
      {
        error_log("BillEngine: Could not find retrieve value");
        header("Location: /index.php/forms/view?id=".$_GET['id']."&bill_error=1");
        exit;
      }
    }
    else
    {
      error_log("BillEngine: Error: ".$error);
      header("Location: /index.php/forms/view?id=".$_GET['id']."&bill_error=1");
      exit;
    }

    return $response;
  }
}
