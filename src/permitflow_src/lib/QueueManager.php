<?php

class QueueManager {

    //Constructor for QueueManager class
    public function QueueManager()
    {
    }

    public function queue_data($data)
    {
		require_once __DIR__ . '/rabbitmqphp/autoload.php';
		#use PhpAmqpLib\Connection\AMQPStreamConnection;
		#use PhpAmqpLib\Message\AMQPMessage;

		$connection = new PhpAmqpLib\Connection\AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
		$channel = $connection->channel();

		$channel->queue_declare('live_irembo_task_queue', false, true, false, false);

		if(empty($data)) $data = "empty";
		$data = json_encode($data);
		$msg = new PhpAmqpLib\Message\AMQPMessage($data,
								array('delivery_mode' => 2) # make message persistent
							  );

		$channel->basic_publish($msg, '', 'live_irembo_task_queue');

		error_log(" [x] Sent ".print_R($data, true));

		$channel->close();
		$connection->close();

	}

}
