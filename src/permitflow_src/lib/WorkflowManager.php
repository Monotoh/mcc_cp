<?php

/* 
 * Class to Manage process of duplicating an existing workflow and 
 * creating a new one.
 * This class will define a set of functions responsible for creating copies of existing workflow 
 * information
 */

class WorkflowManager {
    
    /**
    * Duplicate menu function
   */
    public function duplicateMenu($menu_id)
    {
        //call our helper class
        $otbhelper = new OTBHelper();
      //create temporay table to hold your record
       $tmp_q = "CREATE TEMPORARY TABLE tmp SELECT id, concat('copy_',title) as title,order_no,app_queuing,category_id,concat('copy_',sms_sender) as sms_sender,concat('copy_',service_code) as service_code FROM menus where id = ".$menu_id; 
       //create record
       $tmp_table = $otbhelper->myDBWrapper($tmp_q) ;
       //update the id 
       $new_value = $menu_id + 1 ;
       $tmp_table_update_q = "UPDATE tmp SET id = 0 WHERE id =  ".$menu_id ;
       //execute the update
       $tmp_table_update_q_res = $otbhelper->myDBWrapper($tmp_table_update_q) ;
       //insert the created record
       $tmp_q_insert = "INSERT INTO menus SELECT * FROM tmp " ;
       //execute insert statement
       $tmp_insert_details = $otbhelper->myDBWrapper($tmp_q_insert) ;
       //select latest id from db
       $q = Doctrine_Query::create()
               ->from('Menus m')
               ->orderBy('m.id DESC')
               ->limit(1) ;
       $r = $q->fetchOne();
       //
       $new_menu_id = $r->getId();
       //Insert permission for the menu
       $tmp_menu_permission = "INSERT INTO mf_guard_permission VALUES(NULL,'accessmenu$new_menu_id','Access Menu $new_menu_id')" ;
       //execute permission insert request
       $tmp_insert_permission = $otbhelper->myDBWrapper($tmp_menu_permission) ;
       //drop table tmp
       $tmp_q_drop = "DROP TABLE IF EXISTS tmp " ;
       //execute drop 
       $tmp_execute_drop = $otbhelper->myDBWrapper($tmp_q_drop) ;
       //CALL FUNCTION RESPOSIBLE FOR CREATING SUB MENUS
       $this->duplicateSubmenus($menu_id,$new_menu_id);
      
    } 
    /**
     * Duplicate or submenus on a workflow
     */
    public function duplicateSubmenus($parent_menu_id,$new_menu_id){
       // error_log("Parent Menu id >>>>>>>>>> ".$parent_menu_id);
       // error_log("New Menu id >>>>>>>>>> ".$new_menu_id);
        //call our helper class
        $otbhelper = new OTBHelper();
        //create temporay table to hold your sub_menus records
       $tmp_q = "CREATE TEMPORARY TABLE tmp2 SELECT id, concat('copy_',title) as title,"
               . "app_queuing,order_no,max_duration,deleted,change_identifier,new_identifier,new_identifier_start,menu_id,"
               . "hide_comments,allow_edit,stage_type,stage_property,stage_expired_movement,stage_type_movement,stage_type_notification,stage_type_movement_fail,stage_payment_confirmation"
               . ",extra_type FROM sub_menus where menu_id = ".$parent_menu_id; 
        //create record
      // error_log("Query to create tmp2 table ".$tmp_q);
       $tmp_table = $otbhelper->myDBWrapper($tmp_q) ;
       //Duplicate buttons of all sub_menus created here
      /* $list_of_buttons_query = "select button_id from sub_menu_buttons where sub_menu_id in (select id from tmp2) " ;
       $list_of_buttons_res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($list_of_buttons_query) ;
       $btn_list = array() ;
       foreach ($list_of_buttons_res as $results){
           array_push($btn_list, $results['button_id']) ;
       }*/
       //update the id 
       $tmp_table_update_q = "UPDATE tmp2 SET id = 0 WHERE menu_id =  ".$parent_menu_id ;
    //   error_log("Query to update ids >> ".$tmp_table_update_q);
       //execute the update
       $tmp_table_update_q_res = $otbhelper->myDBWrapper($tmp_table_update_q) ;
       //change the menu_id
       $tmp_update_menu_ids = "UPDATE tmp2 SET menu_id = $new_menu_id WHERE id =  0 " ;
     //  error_log("Update the menu ids to new query ".$tmp_update_menu_ids) ;
       $$tmp_update_menu_ids_res = $otbhelper->myDBWrapper($tmp_update_menu_ids) ;
       //insert the created record
       $tmp_q_insert = "INSERT INTO sub_menus SELECT * FROM tmp2 " ;
       //execute insert statement
      // error_log("Insert new submenus query >> ".$tmp_q_insert);
       $tmp_insert_details = $otbhelper->myDBWrapper($tmp_q_insert) ;
       ////////// Permissions management
        //select latest id from db
       $q = Doctrine_Query::create()
               ->from('SubMenus s')
               ->where('s.menu_id = ?', $new_menu_id) ;
       $r = $q->execute();
       //for loop
       foreach($r as $res){
          //Insert permission for the menu
          $new_sub_menu_id =  $res->getId();
          $tmp_menu_permission = "INSERT INTO mf_guard_permission VALUES(NULL,'accessmenu$new_sub_menu_id','Access Menu $new_sub_menu_id')" ;
          //execute permission insert request
          $tmp_insert_permission = $otbhelper->myDBWrapper($tmp_menu_permission) ;  
       }
      
       //drop table tmp
       $tmp_q_drop = "DROP TABLE IF EXISTS tmp2 " ;
       //execute drop 
       $tmp_execute_drop = $otbhelper->myDBWrapper($tmp_q_drop) ;
    }
    /**
     * Duplicate submenu buttons
     * This is a bit tricky - TODO
     */
    public function duplicateButtons($button_ids){
         //call our helper class
        $otbhelper = new OTBHelper();
        $tmp_buttons_query = "CREATE TEMPORARY TABLE tmp_btns SELECT id, img , concat('copy_',title) as title, tooltip, link FROM buttons WHERE id in (".implode(',',$button_ids).")"; 
        //execute query
        $duplicate_btns_res = $otbhelper->myDBWrapper($tmp_buttons_query) ;  
        
        
    }
    /**
     * Duplicate Invoices
     */
    public function duplicateInvoices($menu_id){
        //call our helper class
        $otbhelper = new OTBHelper();
        //select form ids to duplicate
        $sql_permits = "select id from invoicetemplates where applicationstage in (select id from sub_menus where menu_id = $menu_id )";
        $sql_permis_res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($sql_permits) ;
        //
        $invoice_ids = array();
        foreach($sql_permis_res as $r){
            array_push($invoice_ids, $r['id']) ;
        }
        //
        $tmp_invoices = "CREATE TEMPORARY TABLE tmp_invoicetemplates SELECT id,concat('copy_',title) as title,applicationform,applicationstage,content, max_duration , due_duration, "
                . "invoice_number, expiration_type, payment_type, use_application_number from invoicetemplates where id in (".implode(',', $invoice_ids).")" ;
        //execute
       $res1 =  $otbhelper->myDBWrapper($tmp_invoices) ;
        //Update form id and set to 0 
       $tmp_table_update_q = "UPDATE tmp_invoicetemplates SET id = 0 " ; // reset all 
       //execute query 
        $tmp_table_res = $otbhelper->myDBWrapper($tmp_table_update_q) ;
         //insert the created record
       $tmp_q_insert = "INSERT INTO invoicetemplates SELECT * FROM tmp_invoicetemplates " ;
       //execute insert statement
       $tmp_insert_details = $otbhelper->myDBWrapper($tmp_q_insert) ;
        //drop table tmp
       $tmp_q_drop = "DROP TABLE IF EXISTS tmp_invoicetemplates " ;
       //execute drop 
       $tmp_execute_drop = $otbhelper->myDBWrapper($tmp_q_drop) ;
    }
    /**
     * Duplicate Permits
     */
    public function duplicatePermits($menu_id){
        //call our helper class
        $otbhelper = new OTBHelper();
        //select form ids to duplicate
        $sql_permits = "select id from permits where applicationstage in (select id from sub_menus where menu_id = $menu_id )";
        $sql_permis_res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($sql_permits) ;
        //
        $permit_ids = array();
        foreach($sql_permis_res as $r){
            array_push($permit_ids, $r['id']) ;
        }
        //
        $tmp_permits = "CREATE TEMPORARY TABLE tmp_permits SELECT id,concat('copy_',title) as title,applicationform,applicationstage,parttype,content,footer,max_duration,remote_url,remote_field,remote_username,remote_password,"
                . "remote_request_type,page_type,page_orientation,user_set_duration,stage_set_duration,content_Part1,content_Part2,content_Part3,content_Part1En,content_Part2En,content_Part3En,contentEn,approver_info_id from permits where id in (".implode(',', $permit_ids).")" ;
        //execute
       $res1 =  $otbhelper->myDBWrapper($tmp_permits) ;
        //Update form id and set to 0 
       $tmp_table_update_q = "UPDATE tmp_permits SET id = 0 " ; // reset all 
       //execute query 
        $tmp_table_res = $otbhelper->myDBWrapper($tmp_table_update_q) ;
         //insert the created record
       $tmp_q_insert = "INSERT INTO permits SELECT * FROM tmp_permits " ;
       //execute insert statement
       $tmp_insert_details = $otbhelper->myDBWrapper($tmp_q_insert) ;
         //drop table tmp
       $tmp_q_drop = "DROP TABLE IF EXISTS tmp_permits " ;
       //execute drop 
       $tmp_execute_drop = $otbhelper->myDBWrapper($tmp_q_drop) ;
    }
    
   

}
