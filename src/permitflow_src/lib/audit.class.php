<?php
class audit
{
	public function audit()
	{
	}
	public function saveAudit($entryid, $action)
	{
		try{
			$audit = new AuditTrail();
			$audit->setUserId($_SESSION['SESSION_CUTEFLOW_USERID']);
                        if($entryid){
                            $audit->setFormEntryId($entryid);
                        }			
			$audit->setAction("<a href='http://".$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI]."'>".$action."</a>");
			$audit->setActionTimestamp(date('Y-m-d g:i:s'));
			$audit->setIpaddress($_SERVER['REMOTE_ADDR']);
			$audit->setHttpAgent($_SERVER['HTTP_USER_AGENT']);

			$ip = $_SERVER['REMOTE_ADDR'];

			$details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));

            if($details && $details->city) {
                $audit->setUserLocation($details->city);
            }
            else{
                $audit->setUserLocation("-");
            }
			$audit->save();
		}
		catch(Exception $ex)
		{
			error_log("Audit Error: ".$ex);
		}
	}
	public function saveFullAudit($action,$id,$table,$before,$after, $appid)
	{
		try{
			$audit = new AuditTrail();
			$audit->setUserId($_SESSION['SESSION_CUTEFLOW_USERID']);
			$audit->setFormEntryId($appid);
			$audit->setAction($action);
			$audit->setActionTimestamp(date('Y-m-d g:i:s'));
			$audit->setObjectId($id);
			$audit->setObjectName($table);
			$audit->setBeforeValues($before);
			$audit->setAfterValues($after);
			$audit->setIpaddress($_SERVER['REMOTE_ADDR']);
			$audit->setHttpAgent($_SERVER['HTTP_USER_AGENT']);

			$ip = $_SERVER['REMOTE_ADDR'];

			$details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));

            if($details && $details->city) {
                $audit->setUserLocation($details->city);
            }
            else{
                $audit->setUserLocation("-");
            }
			$audit->save();
		}catch(Exception $ex)
		{
			error_log("Audit Error: ".$ex);
		}
	}
}
?>
