<?php

/**
 * ApForm11Review filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseApForm11ReviewFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'date_created' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'date_updated' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'ip_address'   => new sfWidgetFormFilterInput(),
      'element_1'    => new sfWidgetFormFilterInput(),
      'element_2'    => new sfWidgetFormFilterInput(),
      'element_3'    => new sfWidgetFormFilterInput(),
      'element_4'    => new sfWidgetFormFilterInput(),
      'element_5'    => new sfWidgetFormFilterInput(),
      'element_6'    => new sfWidgetFormFilterInput(),
      'element_7'    => new sfWidgetFormFilterInput(),
      'element_8'    => new sfWidgetFormFilterInput(),
      'element_9'    => new sfWidgetFormFilterInput(),
      'element_10'   => new sfWidgetFormFilterInput(),
      'element_11'   => new sfWidgetFormFilterInput(),
      'element_12'   => new sfWidgetFormFilterInput(),
      'element_13'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'element_14'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'element_15'   => new sfWidgetFormFilterInput(),
      'element_16'   => new sfWidgetFormFilterInput(),
      'element_17'   => new sfWidgetFormFilterInput(),
      'element_18'   => new sfWidgetFormFilterInput(),
      'element_19'   => new sfWidgetFormFilterInput(),
      'element_20'   => new sfWidgetFormFilterInput(),
      'element_21'   => new sfWidgetFormFilterInput(),
      'session_id'   => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'date_created' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'date_updated' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'ip_address'   => new sfValidatorPass(array('required' => false)),
      'element_1'    => new sfValidatorPass(array('required' => false)),
      'element_2'    => new sfValidatorPass(array('required' => false)),
      'element_3'    => new sfValidatorPass(array('required' => false)),
      'element_4'    => new sfValidatorPass(array('required' => false)),
      'element_5'    => new sfValidatorPass(array('required' => false)),
      'element_6'    => new sfValidatorPass(array('required' => false)),
      'element_7'    => new sfValidatorPass(array('required' => false)),
      'element_8'    => new sfValidatorPass(array('required' => false)),
      'element_9'    => new sfValidatorPass(array('required' => false)),
      'element_10'   => new sfValidatorPass(array('required' => false)),
      'element_11'   => new sfValidatorPass(array('required' => false)),
      'element_12'   => new sfValidatorPass(array('required' => false)),
      'element_13'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'element_14'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'element_15'   => new sfValidatorPass(array('required' => false)),
      'element_16'   => new sfValidatorPass(array('required' => false)),
      'element_17'   => new sfValidatorPass(array('required' => false)),
      'element_18'   => new sfValidatorPass(array('required' => false)),
      'element_19'   => new sfValidatorPass(array('required' => false)),
      'element_20'   => new sfValidatorPass(array('required' => false)),
      'element_21'   => new sfValidatorPass(array('required' => false)),
      'session_id'   => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ap_form11_review_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ApForm11Review';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'date_created' => 'Date',
      'date_updated' => 'Date',
      'ip_address'   => 'Text',
      'element_1'    => 'Text',
      'element_2'    => 'Text',
      'element_3'    => 'Text',
      'element_4'    => 'Text',
      'element_5'    => 'Text',
      'element_6'    => 'Text',
      'element_7'    => 'Text',
      'element_8'    => 'Text',
      'element_9'    => 'Text',
      'element_10'   => 'Text',
      'element_11'   => 'Text',
      'element_12'   => 'Text',
      'element_13'   => 'Number',
      'element_14'   => 'Number',
      'element_15'   => 'Text',
      'element_16'   => 'Text',
      'element_17'   => 'Text',
      'element_18'   => 'Text',
      'element_19'   => 'Text',
      'element_20'   => 'Text',
      'element_21'   => 'Text',
      'session_id'   => 'Text',
    );
  }
}
