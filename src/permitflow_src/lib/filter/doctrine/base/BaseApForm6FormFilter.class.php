<?php

/**
 * ApForm6 filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseApForm6FormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'date_created' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'date_updated' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'ip_address'   => new sfWidgetFormFilterInput(),
      'element_1_1'  => new sfWidgetFormFilterInput(),
      'element_1_2'  => new sfWidgetFormFilterInput(),
      'element_2'    => new sfWidgetFormFilterInput(),
      'element_3'    => new sfWidgetFormFilterInput(),
      'element_4_1'  => new sfWidgetFormFilterInput(),
      'element_4_2'  => new sfWidgetFormFilterInput(),
      'element_4_3'  => new sfWidgetFormFilterInput(),
      'element_4_4'  => new sfWidgetFormFilterInput(),
      'element_4_5'  => new sfWidgetFormFilterInput(),
      'element_4_6'  => new sfWidgetFormFilterInput(),
      'element_5'    => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'date_created' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'date_updated' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'ip_address'   => new sfValidatorPass(array('required' => false)),
      'element_1_1'  => new sfValidatorPass(array('required' => false)),
      'element_1_2'  => new sfValidatorPass(array('required' => false)),
      'element_2'    => new sfValidatorPass(array('required' => false)),
      'element_3'    => new sfValidatorPass(array('required' => false)),
      'element_4_1'  => new sfValidatorPass(array('required' => false)),
      'element_4_2'  => new sfValidatorPass(array('required' => false)),
      'element_4_3'  => new sfValidatorPass(array('required' => false)),
      'element_4_4'  => new sfValidatorPass(array('required' => false)),
      'element_4_5'  => new sfValidatorPass(array('required' => false)),
      'element_4_6'  => new sfValidatorPass(array('required' => false)),
      'element_5'    => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ap_form6_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ApForm6';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'date_created' => 'Date',
      'date_updated' => 'Date',
      'ip_address'   => 'Text',
      'element_1_1'  => 'Text',
      'element_1_2'  => 'Text',
      'element_2'    => 'Text',
      'element_3'    => 'Text',
      'element_4_1'  => 'Text',
      'element_4_2'  => 'Text',
      'element_4_3'  => 'Text',
      'element_4_4'  => 'Text',
      'element_4_5'  => 'Text',
      'element_4_6'  => 'Text',
      'element_5'    => 'Text',
    );
  }
}
