<?php

/**
 * ApFormElements filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseApFormElementsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'element_title'         => new sfWidgetFormFilterInput(),
      'element_guidelines'    => new sfWidgetFormFilterInput(),
      'element_size'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'element_is_required'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'element_is_unique'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'element_is_private'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'element_type'          => new sfWidgetFormFilterInput(),
      'element_position'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'element_default_value' => new sfWidgetFormFilterInput(),
      'element_constraint'    => new sfWidgetFormFilterInput(),
      'element_total_child'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'element_title'         => new sfValidatorPass(array('required' => false)),
      'element_guidelines'    => new sfValidatorPass(array('required' => false)),
      'element_size'          => new sfValidatorPass(array('required' => false)),
      'element_is_required'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'element_is_unique'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'element_is_private'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'element_type'          => new sfValidatorPass(array('required' => false)),
      'element_position'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'element_default_value' => new sfValidatorPass(array('required' => false)),
      'element_constraint'    => new sfValidatorPass(array('required' => false)),
      'element_total_child'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('ap_form_elements_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ApFormElements';
  }

  public function getFields()
  {
    return array(
      'form_id'               => 'Number',
      'element_id'            => 'Number',
      'element_title'         => 'Text',
      'element_guidelines'    => 'Text',
      'element_size'          => 'Text',
      'element_is_required'   => 'Number',
      'element_is_unique'     => 'Number',
      'element_is_private'    => 'Number',
      'element_type'          => 'Text',
      'element_position'      => 'Number',
      'element_default_value' => 'Text',
      'element_constraint'    => 'Text',
      'element_total_child'   => 'Number',
    );
  }
}
