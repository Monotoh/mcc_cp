<?php

/**
 * ApFormGroups filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseApFormGroupsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'group_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Group'), 'add_empty' => true)),
      'form_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Form'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'group_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Group'), 'column' => 'group_id')),
      'form_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Form'), 'column' => 'form_id')),
    ));

    $this->widgetSchema->setNameFormat('ap_form_groups_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ApFormGroups';
  }

  public function getFields()
  {
    return array(
      'id'       => 'Number',
      'group_id' => 'ForeignKey',
      'form_id'  => 'ForeignKey',
    );
  }
}
