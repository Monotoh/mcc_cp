<?php

/**
 * ApForms filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseApFormsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'form_name'                => new sfWidgetFormFilterInput(),
      'form_description'         => new sfWidgetFormFilterInput(),
      'form_email'               => new sfWidgetFormFilterInput(),
      'form_redirect'            => new sfWidgetFormFilterInput(),
      'form_success_message'     => new sfWidgetFormFilterInput(),
      'form_password'            => new sfWidgetFormFilterInput(),
      'form_unique_ip'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'form_frame_height'        => new sfWidgetFormFilterInput(),
      'form_has_css'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'form_captcha'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'form_active'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'form_review'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'form_default_application' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'esl_from_name'            => new sfWidgetFormFilterInput(),
      'esl_from_email_address'   => new sfWidgetFormFilterInput(),
      'esl_subject'              => new sfWidgetFormFilterInput(),
      'esl_content'              => new sfWidgetFormFilterInput(),
      'esl_plain_text'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'esr_email_address'        => new sfWidgetFormFilterInput(),
      'esr_from_name'            => new sfWidgetFormFilterInput(),
      'esr_from_email_address'   => new sfWidgetFormFilterInput(),
      'esr_subject'              => new sfWidgetFormFilterInput(),
      'esr_content'              => new sfWidgetFormFilterInput(),
      'esr_plain_text'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'form_name'                => new sfValidatorPass(array('required' => false)),
      'form_description'         => new sfValidatorPass(array('required' => false)),
      'form_email'               => new sfValidatorPass(array('required' => false)),
      'form_redirect'            => new sfValidatorPass(array('required' => false)),
      'form_success_message'     => new sfValidatorPass(array('required' => false)),
      'form_password'            => new sfValidatorPass(array('required' => false)),
      'form_unique_ip'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'form_frame_height'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'form_has_css'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'form_captcha'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'form_active'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'form_review'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'form_default_application' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'esl_from_name'            => new sfValidatorPass(array('required' => false)),
      'esl_from_email_address'   => new sfValidatorPass(array('required' => false)),
      'esl_subject'              => new sfValidatorPass(array('required' => false)),
      'esl_content'              => new sfValidatorPass(array('required' => false)),
      'esl_plain_text'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'esr_email_address'        => new sfValidatorPass(array('required' => false)),
      'esr_from_name'            => new sfValidatorPass(array('required' => false)),
      'esr_from_email_address'   => new sfValidatorPass(array('required' => false)),
      'esr_subject'              => new sfValidatorPass(array('required' => false)),
      'esr_content'              => new sfValidatorPass(array('required' => false)),
      'esr_plain_text'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('ap_forms_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ApForms';
  }

  public function getFields()
  {
    return array(
      'form_id'                  => 'Number',
      'form_name'                => 'Text',
      'form_description'         => 'Text',
      'form_email'               => 'Text',
      'form_redirect'            => 'Text',
      'form_success_message'     => 'Text',
      'form_password'            => 'Text',
      'form_unique_ip'           => 'Number',
      'form_frame_height'        => 'Number',
      'form_has_css'             => 'Number',
      'form_captcha'             => 'Number',
      'form_active'              => 'Number',
      'form_review'              => 'Number',
      'form_default_application' => 'Number',
      'esl_from_name'            => 'Text',
      'esl_from_email_address'   => 'Text',
      'esl_subject'              => 'Text',
      'esl_content'              => 'Text',
      'esl_plain_text'           => 'Number',
      'esr_email_address'        => 'Text',
      'esr_from_name'            => 'Text',
      'esr_from_email_address'   => 'Text',
      'esr_subject'              => 'Text',
      'esr_content'              => 'Text',
      'esr_plain_text'           => 'Number',
    );
  }
}
