<?php

/**
 * ApplicationFee filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseApplicationFeeFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'applicationform' => new sfWidgetFormFilterInput(),
      'feeamount'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'title'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'applicationform' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'feeamount'       => new sfValidatorPass(array('required' => false)),
      'title'           => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('application_fee_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ApplicationFee';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'applicationform' => 'Number',
      'feeamount'       => 'Text',
      'title'           => 'Text',
    );
  }
}
