<?php

/**
 * ApplicationOverviewColumns filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseApplicationOverviewColumnsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'form_id'       => new sfWidgetFormFilterInput(),
      'element_id'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'position'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'custom_header' => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'form_id'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'element_id'    => new sfValidatorPass(array('required' => false)),
      'position'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'custom_header' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('application_overview_columns_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ApplicationOverviewColumns';
  }

  public function getFields()
  {
    return array(
      'id'            => 'Number',
      'form_id'       => 'Number',
      'element_id'    => 'Text',
      'position'      => 'Number',
      'custom_header' => 'Text',
    );
  }
}
