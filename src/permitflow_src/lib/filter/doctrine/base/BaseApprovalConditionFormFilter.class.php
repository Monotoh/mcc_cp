<?php

/**
 * ApprovalCondition filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseApprovalConditionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'entry_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('User'), 'add_empty' => true)),
      'condition_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Condition'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'entry_id'     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('User'), 'column' => 'id')),
      'condition_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Condition'), 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('approval_condition_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ApprovalCondition';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'entry_id'     => 'ForeignKey',
      'condition_id' => 'ForeignKey',
    );
  }
}
