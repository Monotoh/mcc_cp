<?php

/**
 * Buttons filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseButtonsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'img'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'title'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'tooltip'       => new sfWidgetFormFilterInput(),
      'link'          => new sfWidgetFormFilterInput(),
      'submenus_list' => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'SubMenus')),
    ));

    $this->setValidators(array(
      'img'           => new sfValidatorPass(array('required' => false)),
      'title'         => new sfValidatorPass(array('required' => false)),
      'tooltip'       => new sfValidatorPass(array('required' => false)),
      'link'          => new sfValidatorPass(array('required' => false)),
      'submenus_list' => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'SubMenus', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('buttons_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addSubmenusListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.SubMenuButtons SubMenuButtons')
      ->andWhereIn('SubMenuButtons.sub_menu_id', $values)
    ;
  }

  public function getModelName()
  {
    return 'Buttons';
  }

  public function getFields()
  {
    return array(
      'id'            => 'Number',
      'img'           => 'Text',
      'title'         => 'Text',
      'tooltip'       => 'Text',
      'link'          => 'Text',
      'submenus_list' => 'ManyKey',
    );
  }
}
