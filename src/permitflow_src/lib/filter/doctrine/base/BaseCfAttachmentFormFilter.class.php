<?php

/**
 * CfAttachment filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCfAttachmentFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'strpath'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ncirculationhistoryid' => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'strpath'               => new sfValidatorPass(array('required' => false)),
      'ncirculationhistoryid' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('cf_attachment_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CfAttachment';
  }

  public function getFields()
  {
    return array(
      'nid'                   => 'Number',
      'strpath'               => 'Text',
      'ncirculationhistoryid' => 'Number',
    );
  }
}
