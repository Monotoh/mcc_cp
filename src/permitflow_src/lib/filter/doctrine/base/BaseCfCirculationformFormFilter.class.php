<?php

/**
 * CfCirculationform filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCfCirculationformFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nsenderid'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'strname'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'nmailinglistid' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'bisarchived'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'nendaction'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'bdeleted'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'banonymize'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'nsenderid'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'strname'        => new sfValidatorPass(array('required' => false)),
      'nmailinglistid' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'bisarchived'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'nendaction'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'bdeleted'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'banonymize'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('cf_circulationform_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CfCirculationform';
  }

  public function getFields()
  {
    return array(
      'nid'            => 'Number',
      'nsenderid'      => 'Number',
      'strname'        => 'Text',
      'nmailinglistid' => 'Number',
      'bisarchived'    => 'Number',
      'nendaction'     => 'Number',
      'bdeleted'       => 'Number',
      'banonymize'     => 'Number',
    );
  }
}
