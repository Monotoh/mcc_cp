<?php

/**
 * CfCirculationhistory filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCfCirculationhistoryFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nrevisionnumber'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'datesending'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'stradditionaltext'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ncirculationformid' => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'nrevisionnumber'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'datesending'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'stradditionaltext'  => new sfValidatorPass(array('required' => false)),
      'ncirculationformid' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('cf_circulationhistory_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CfCirculationhistory';
  }

  public function getFields()
  {
    return array(
      'nid'                => 'Number',
      'nrevisionnumber'    => 'Number',
      'datesending'        => 'Number',
      'stradditionaltext'  => 'Text',
      'ncirculationformid' => 'Number',
    );
  }
}
