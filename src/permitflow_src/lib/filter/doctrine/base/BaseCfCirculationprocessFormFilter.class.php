<?php

/**
 * CfCirculationprocess filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCfCirculationprocessFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'ncirculationformid'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'nslotid'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'nuserid'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'dateinprocesssince'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ndecissionstate'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'datedecission'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'nissubstitiuteof'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ncirculationhistoryid' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'nresendcount'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'ncirculationformid'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'nslotid'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'nuserid'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'dateinprocesssince'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ndecissionstate'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'datedecission'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'nissubstitiuteof'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ncirculationhistoryid' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'nresendcount'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('cf_circulationprocess_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CfCirculationprocess';
  }

  public function getFields()
  {
    return array(
      'nid'                   => 'Number',
      'ncirculationformid'    => 'Number',
      'nslotid'               => 'Number',
      'nuserid'               => 'Number',
      'dateinprocesssince'    => 'Number',
      'ndecissionstate'       => 'Number',
      'datedecission'         => 'Number',
      'nissubstitiuteof'      => 'Number',
      'ncirculationhistoryid' => 'Number',
      'nresendcount'          => 'Number',
    );
  }
}
