<?php

/**
 * CfFieldvalue filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCfFieldvalueFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'ninputfieldid'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'strfieldvalue'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'nslotid'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'nformid'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ncirculationhistoryid' => new sfWidgetFormFilterInput(),
      'nuserid'               => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('User'), 'add_empty' => true)),
      'nrevisionid'           => new sfWidgetFormFilterInput(),
      'nrevisiondate'         => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'ninputfieldid'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'strfieldvalue'         => new sfValidatorPass(array('required' => false)),
      'nslotid'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'nformid'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ncirculationhistoryid' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'nuserid'               => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('User'), 'column' => 'nid')),
      'nrevisionid'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'nrevisiondate'         => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cf_fieldvalue_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CfFieldvalue';
  }

  public function getFields()
  {
    return array(
      'nid'                   => 'Number',
      'ninputfieldid'         => 'Number',
      'strfieldvalue'         => 'Text',
      'nslotid'               => 'Number',
      'nformid'               => 'Number',
      'ncirculationhistoryid' => 'Number',
      'nuserid'               => 'ForeignKey',
      'nrevisionid'           => 'Number',
      'nrevisiondate'         => 'Text',
    );
  }
}
