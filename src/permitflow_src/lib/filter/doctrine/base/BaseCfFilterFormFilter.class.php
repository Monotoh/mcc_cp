<?php

/**
 * CfFilter filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCfFilterFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nuserid'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'strlabel'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'strname'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'nstationid'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ndaysinprogress_start' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ndaysinprogress_end'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'strsenddate_start'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'strsenddate_end'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'nmailinglistid'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ntemplateid'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'strcustomfilter'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'nsenderid'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'nuserid'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'strlabel'              => new sfValidatorPass(array('required' => false)),
      'strname'               => new sfValidatorPass(array('required' => false)),
      'nstationid'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ndaysinprogress_start' => new sfValidatorPass(array('required' => false)),
      'ndaysinprogress_end'   => new sfValidatorPass(array('required' => false)),
      'strsenddate_start'     => new sfValidatorPass(array('required' => false)),
      'strsenddate_end'       => new sfValidatorPass(array('required' => false)),
      'nmailinglistid'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ntemplateid'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'strcustomfilter'       => new sfValidatorPass(array('required' => false)),
      'nsenderid'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('cf_filter_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CfFilter';
  }

  public function getFields()
  {
    return array(
      'nid'                   => 'Number',
      'nuserid'               => 'Number',
      'strlabel'              => 'Text',
      'strname'               => 'Text',
      'nstationid'            => 'Number',
      'ndaysinprogress_start' => 'Text',
      'ndaysinprogress_end'   => 'Text',
      'strsenddate_start'     => 'Text',
      'strsenddate_end'       => 'Text',
      'nmailinglistid'        => 'Number',
      'ntemplateid'           => 'Number',
      'strcustomfilter'       => 'Text',
      'nsenderid'             => 'Number',
    );
  }
}
