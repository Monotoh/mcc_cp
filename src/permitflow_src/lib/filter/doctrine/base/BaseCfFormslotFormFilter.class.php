<?php

/**
 * CfFormslot filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCfFormslotFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'strname'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ntemplateid' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'nslotnumber' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'nsendtype'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'strname'     => new sfValidatorPass(array('required' => false)),
      'ntemplateid' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'nslotnumber' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'nsendtype'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('cf_formslot_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CfFormslot';
  }

  public function getFields()
  {
    return array(
      'nid'         => 'Number',
      'strname'     => 'Text',
      'ntemplateid' => 'Number',
      'nslotnumber' => 'Number',
      'nsendtype'   => 'Number',
    );
  }
}
