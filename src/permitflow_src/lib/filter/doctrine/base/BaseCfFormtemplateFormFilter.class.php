<?php

/**
 * CfFormtemplate filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCfFormtemplateFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'strname'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'bdeleted' => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'strname'  => new sfValidatorPass(array('required' => false)),
      'bdeleted' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('cf_formtemplate_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CfFormtemplate';
  }

  public function getFields()
  {
    return array(
      'nid'      => 'Number',
      'strname'  => 'Text',
      'bdeleted' => 'Number',
    );
  }
}
