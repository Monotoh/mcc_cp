<?php

/**
 * CfInputfield filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCfInputfieldFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'strname'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ntype'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'strstandardvalue' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'breadonly'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'strbgcolor'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'commenttype'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'commentrelation'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'strname'          => new sfValidatorPass(array('required' => false)),
      'ntype'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'strstandardvalue' => new sfValidatorPass(array('required' => false)),
      'breadonly'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'strbgcolor'       => new sfValidatorPass(array('required' => false)),
      'commenttype'      => new sfValidatorPass(array('required' => false)),
      'commentrelation'  => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cf_inputfield_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CfInputfield';
  }

  public function getFields()
  {
    return array(
      'nid'              => 'Number',
      'strname'          => 'Text',
      'ntype'            => 'Number',
      'strstandardvalue' => 'Text',
      'breadonly'        => 'Number',
      'strbgcolor'       => 'Text',
      'commenttype'      => 'Text',
      'commentrelation'  => 'Text',
    );
  }
}
