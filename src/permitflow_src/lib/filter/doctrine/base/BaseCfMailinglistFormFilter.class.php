<?php

/**
 * CfMailinglist filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCfMailinglistFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'strname'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ntemplateid' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'bisedited'   => new sfWidgetFormFilterInput(),
      'bisdefault'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'bdeleted'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'strname'     => new sfValidatorPass(array('required' => false)),
      'ntemplateid' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'bisedited'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'bisdefault'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'bdeleted'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('cf_mailinglist_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CfMailinglist';
  }

  public function getFields()
  {
    return array(
      'nid'         => 'Number',
      'strname'     => 'Text',
      'ntemplateid' => 'Number',
      'bisedited'   => 'Number',
      'bisdefault'  => 'Number',
      'bdeleted'    => 'Number',
    );
  }
}
