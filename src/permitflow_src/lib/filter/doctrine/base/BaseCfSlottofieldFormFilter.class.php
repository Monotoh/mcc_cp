<?php

/**
 * CfSlottofield filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCfSlottofieldFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nslotid'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'nfieldid'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'nposition' => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'nslotid'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'nfieldid'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'nposition' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('cf_slottofield_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CfSlottofield';
  }

  public function getFields()
  {
    return array(
      'nid'       => 'Number',
      'nslotid'   => 'Number',
      'nfieldid'  => 'Number',
      'nposition' => 'Number',
    );
  }
}
