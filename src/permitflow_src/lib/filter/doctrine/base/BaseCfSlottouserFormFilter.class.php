<?php

/**
 * CfSlottouser filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCfSlottouserFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nslotid'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'nmailinglistid' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'nuserid'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'status'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'nposition'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'nslotid'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'nmailinglistid' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'nuserid'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'status'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'nposition'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('cf_slottouser_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CfSlottouser';
  }

  public function getFields()
  {
    return array(
      'nid'            => 'Number',
      'nslotid'        => 'Number',
      'nmailinglistid' => 'Number',
      'nuserid'        => 'Number',
      'status'         => 'Number',
      'nposition'      => 'Number',
    );
  }
}
