<?php

/**
 * CfUser filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCfUserFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'strlastname'                 => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'strfirstname'                => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'stremail'                    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'naccesslevel'                => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'struserid'                   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'strpassword'                 => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'stremail_format'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'stremail_values'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'nsubstitudeid'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'tslastaction'                => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'bdeleted'                    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'strstreet'                   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'strcountry'                  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'strzipcode'                  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'strcity'                     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'strphone_main1'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'strphone_main2'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'strphone_mobile'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'strfax'                      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'strorganisation'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'strdepartment'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'strcostcenter'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'userdefined1_value'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'userdefined2_value'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'nsubstitutetimevalue'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'strsubstitutetimeunit'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'busegeneralsubstituteconfig' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'busegeneralemailconfig'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'groups_list'                 => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'mfGuardGroup')),
    ));

    $this->setValidators(array(
      'strlastname'                 => new sfValidatorPass(array('required' => false)),
      'strfirstname'                => new sfValidatorPass(array('required' => false)),
      'stremail'                    => new sfValidatorPass(array('required' => false)),
      'naccesslevel'                => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'struserid'                   => new sfValidatorPass(array('required' => false)),
      'strpassword'                 => new sfValidatorPass(array('required' => false)),
      'stremail_format'             => new sfValidatorPass(array('required' => false)),
      'stremail_values'             => new sfValidatorPass(array('required' => false)),
      'nsubstitudeid'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'tslastaction'                => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'bdeleted'                    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'strstreet'                   => new sfValidatorPass(array('required' => false)),
      'strcountry'                  => new sfValidatorPass(array('required' => false)),
      'strzipcode'                  => new sfValidatorPass(array('required' => false)),
      'strcity'                     => new sfValidatorPass(array('required' => false)),
      'strphone_main1'              => new sfValidatorPass(array('required' => false)),
      'strphone_main2'              => new sfValidatorPass(array('required' => false)),
      'strphone_mobile'             => new sfValidatorPass(array('required' => false)),
      'strfax'                      => new sfValidatorPass(array('required' => false)),
      'strorganisation'             => new sfValidatorPass(array('required' => false)),
      'strdepartment'               => new sfValidatorPass(array('required' => false)),
      'strcostcenter'               => new sfValidatorPass(array('required' => false)),
      'userdefined1_value'          => new sfValidatorPass(array('required' => false)),
      'userdefined2_value'          => new sfValidatorPass(array('required' => false)),
      'nsubstitutetimevalue'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'strsubstitutetimeunit'       => new sfValidatorPass(array('required' => false)),
      'busegeneralsubstituteconfig' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'busegeneralemailconfig'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'groups_list'                 => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'mfGuardGroup', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cf_user_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addGroupsListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.mfGuardUserGroup mfGuardUserGroup')
      ->andWhereIn('mfGuardUserGroup.group_id', $values)
    ;
  }

  public function getModelName()
  {
    return 'CfUser';
  }

  public function getFields()
  {
    return array(
      'nid'                         => 'Number',
      'strlastname'                 => 'Text',
      'strfirstname'                => 'Text',
      'stremail'                    => 'Text',
      'naccesslevel'                => 'Number',
      'struserid'                   => 'Text',
      'strpassword'                 => 'Text',
      'stremail_format'             => 'Text',
      'stremail_values'             => 'Text',
      'nsubstitudeid'               => 'Number',
      'tslastaction'                => 'Number',
      'bdeleted'                    => 'Number',
      'strstreet'                   => 'Text',
      'strcountry'                  => 'Text',
      'strzipcode'                  => 'Text',
      'strcity'                     => 'Text',
      'strphone_main1'              => 'Text',
      'strphone_main2'              => 'Text',
      'strphone_mobile'             => 'Text',
      'strfax'                      => 'Text',
      'strorganisation'             => 'Text',
      'strdepartment'               => 'Text',
      'strcostcenter'               => 'Text',
      'userdefined1_value'          => 'Text',
      'userdefined2_value'          => 'Text',
      'nsubstitutetimevalue'        => 'Number',
      'strsubstitutetimeunit'       => 'Text',
      'busegeneralsubstituteconfig' => 'Number',
      'busegeneralemailconfig'      => 'Number',
      'groups_list'                 => 'ManyKey',
    );
  }
}
