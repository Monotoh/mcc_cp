<?php

/**
 * Checksum filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseChecksumFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'entry_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Entry'), 'add_empty' => true)),
      'checksum' => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'entry_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Entry'), 'column' => 'id')),
      'checksum' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('checksum_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Checksum';
  }

  public function getFields()
  {
    return array(
      'id'       => 'Number',
      'entry_id' => 'ForeignKey',
      'checksum' => 'Text',
    );
  }
}
