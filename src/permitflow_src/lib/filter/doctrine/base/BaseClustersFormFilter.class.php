<?php

/**
 * Clusters filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseClustersFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'form_id' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'field1'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'field2'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'form_id' => new sfValidatorPass(array('required' => false)),
      'field1'  => new sfValidatorPass(array('required' => false)),
      'field2'  => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('clusters_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Clusters';
  }

  public function getFields()
  {
    return array(
      'id'      => 'Number',
      'form_id' => 'Text',
      'field1'  => 'Text',
      'field2'  => 'Text',
    );
  }
}
