<?php

/**
 * Comments filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCommentsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'circulation_id' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'slot_id'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'form_id'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'field_id'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'comment'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'circulation_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'slot_id'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'form_id'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'field_id'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'comment'        => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('comments_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Comments';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'circulation_id' => 'Number',
      'slot_id'        => 'Number',
      'form_id'        => 'Number',
      'field_id'       => 'Number',
      'comment'        => 'Text',
    );
  }
}
