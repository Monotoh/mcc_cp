<?php

/**
 * ConditionsOfApproval filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseConditionsOfApprovalFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'slot_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Slot'), 'add_empty' => true)),
      'short_name'  => new sfWidgetFormFilterInput(),
      'description' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'slot_id'     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Slot'), 'column' => 'nid')),
      'short_name'  => new sfValidatorPass(array('required' => false)),
      'description' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('conditions_of_approval_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ConditionsOfApproval';
  }

  public function getFields()
  {
    return array(
      'id'          => 'Number',
      'slot_id'     => 'ForeignKey',
      'short_name'  => 'Text',
      'description' => 'Text',
    );
  }
}
