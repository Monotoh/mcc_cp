<?php

/**
 * Content filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseContentFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'type_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ContentType'), 'add_empty' => true)),
      'title'      => new sfWidgetFormFilterInput(),
      'longtitle'  => new sfWidgetFormFilterInput(),
      'parent'     => new sfWidgetFormFilterInput(),
      'summary'    => new sfWidgetFormFilterInput(),
      'article'    => new sfWidgetFormFilterInput(),
      'created_on' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'published'  => new sfWidgetFormFilterInput(),
      'menu_index' => new sfWidgetFormFilterInput(),
      'url'        => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'type_id'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('ContentType'), 'column' => 'id')),
      'title'      => new sfValidatorPass(array('required' => false)),
      'longtitle'  => new sfValidatorPass(array('required' => false)),
      'parent'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'summary'    => new sfValidatorPass(array('required' => false)),
      'article'    => new sfValidatorPass(array('required' => false)),
      'created_on' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'published'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'menu_index' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'url'        => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('content_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Content';
  }

  public function getFields()
  {
    return array(
      'id'         => 'Number',
      'type_id'    => 'ForeignKey',
      'title'      => 'Text',
      'longtitle'  => 'Text',
      'parent'     => 'Number',
      'summary'    => 'Text',
      'article'    => 'Text',
      'created_on' => 'Date',
      'published'  => 'Number',
      'menu_index' => 'Number',
      'url'        => 'Text',
    );
  }
}
