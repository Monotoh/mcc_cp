<?php

/**
 * DefaultForms filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseDefaultFormsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'occupancy_form_id'       => new sfWidgetFormFilterInput(),
      'application_form_id'     => new sfWidgetFormFilterInput(),
      'contact_form_id'         => new sfWidgetFormFilterInput(),
      'feedback_form_id'        => new sfWidgetFormFilterInput(),
      'profile_form_id'         => new sfWidgetFormFilterInput(),
      'structural_form_id'      => new sfWidgetFormFilterInput(),
      'payment_form_id'         => new sfWidgetFormFilterInput(),
      'additional_docs_form_id' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'occupancy_form_id'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'application_form_id'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'contact_form_id'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'feedback_form_id'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'profile_form_id'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'structural_form_id'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'payment_form_id'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'additional_docs_form_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('default_forms_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'DefaultForms';
  }

  public function getFields()
  {
    return array(
      'id'                      => 'Number',
      'occupancy_form_id'       => 'Number',
      'application_form_id'     => 'Number',
      'contact_form_id'         => 'Number',
      'feedback_form_id'        => 'Number',
      'profile_form_id'         => 'Number',
      'structural_form_id'      => 'Number',
      'payment_form_id'         => 'Number',
      'additional_docs_form_id' => 'Number',
    );
  }
}
