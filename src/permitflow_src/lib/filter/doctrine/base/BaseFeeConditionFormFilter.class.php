<?php

/**
 * FeeCondition filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseFeeConditionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'feeid'          => new sfWidgetFormFilterInput(),
      'fieldid'        => new sfWidgetFormFilterInput(),
      'conditiontype'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'conditionvalue' => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'feeid'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'fieldid'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'conditiontype'  => new sfValidatorPass(array('required' => false)),
      'conditionvalue' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('fee_condition_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'FeeCondition';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'feeid'          => 'Number',
      'fieldid'        => 'Number',
      'conditiontype'  => 'Text',
      'conditionvalue' => 'Text',
    );
  }
}
