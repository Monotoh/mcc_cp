<?php

/**
 * FormEntry filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseFormEntryFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'form_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Form'), 'add_empty' => true)),
      'entry_id'          => new sfWidgetFormFilterInput(),
      'user_id'           => new sfWidgetFormFilterInput(),
      'circulation_id'    => new sfWidgetFormFilterInput(),
      'occupancy_id'      => new sfWidgetFormFilterInput(),
      'occupation_status' => new sfWidgetFormFilterInput(),
      'structural_id'     => new sfWidgetFormFilterInput(),
      'structural_status' => new sfWidgetFormFilterInput(),
      'circulation_type'  => new sfWidgetFormFilterInput(),
      'approved'          => new sfWidgetFormFilterInput(),
      'application_id'    => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'form_id'           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Form'), 'column' => 'form_id')),
      'entry_id'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'user_id'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'circulation_id'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'occupancy_id'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'occupation_status' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'structural_id'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'structural_status' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'circulation_type'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'approved'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'application_id'    => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('form_entry_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'FormEntry';
  }

  public function getFields()
  {
    return array(
      'id'                => 'Number',
      'form_id'           => 'ForeignKey',
      'entry_id'          => 'Number',
      'user_id'           => 'Number',
      'circulation_id'    => 'Number',
      'occupancy_id'      => 'Number',
      'occupation_status' => 'Number',
      'structural_id'     => 'Number',
      'structural_status' => 'Number',
      'circulation_type'  => 'Number',
      'approved'          => 'Number',
      'application_id'    => 'Text',
    );
  }
}
