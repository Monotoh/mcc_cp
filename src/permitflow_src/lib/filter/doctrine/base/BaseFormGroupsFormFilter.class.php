<?php

/**
 * FormGroups filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseFormGroupsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'group_name'        => new sfWidgetFormFilterInput(),
      'group_description' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'group_name'        => new sfValidatorPass(array('required' => false)),
      'group_description' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('form_groups_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'FormGroups';
  }

  public function getFields()
  {
    return array(
      'group_id'          => 'Number',
      'group_name'        => 'Text',
      'group_description' => 'Text',
    );
  }
}
