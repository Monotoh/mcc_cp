<?php

/**
 * NotificationHistory filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseNotificationHistoryFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'user_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('User'), 'add_empty' => true)),
      'notification'      => new sfWidgetFormFilterInput(),
      'notification_type' => new sfWidgetFormFilterInput(),
      'sent_on'           => new sfWidgetFormFilterInput(),
      'confirmed_receipt' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'user_id'           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('User'), 'column' => 'id')),
      'notification'      => new sfValidatorPass(array('required' => false)),
      'notification_type' => new sfValidatorPass(array('required' => false)),
      'sent_on'           => new sfValidatorPass(array('required' => false)),
      'confirmed_receipt' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('notification_history_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'NotificationHistory';
  }

  public function getFields()
  {
    return array(
      'id'                => 'Number',
      'user_id'           => 'ForeignKey',
      'notification'      => 'Text',
      'notification_type' => 'Text',
      'sent_on'           => 'Text',
      'confirmed_receipt' => 'Number',
    );
  }
}
