<?php

/**
 * Notifications filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseNotificationsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'submenu_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Submenu'), 'add_empty' => true)),
      'title'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'content'    => new sfWidgetFormFilterInput(),
      'sms'        => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'submenu_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Submenu'), 'column' => 'id')),
      'title'      => new sfValidatorPass(array('required' => false)),
      'content'    => new sfValidatorPass(array('required' => false)),
      'sms'        => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('notifications_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Notifications';
  }

  public function getFields()
  {
    return array(
      'id'         => 'Number',
      'submenu_id' => 'ForeignKey',
      'title'      => 'Text',
      'content'    => 'Text',
      'sms'        => 'Text',
    );
  }
}
