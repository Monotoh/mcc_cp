<?php

/**
 * OtherApprovalCondition filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseOtherApprovalConditionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'entry_id'           => new sfWidgetFormFilterInput(),
      'field_id'           => new sfWidgetFormFilterInput(),
      'approval_condition' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'entry_id'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'field_id'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'approval_condition' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('other_approval_condition_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'OtherApprovalCondition';
  }

  public function getFields()
  {
    return array(
      'id'                 => 'Number',
      'entry_id'           => 'Number',
      'field_id'           => 'Number',
      'approval_condition' => 'Text',
    );
  }
}
