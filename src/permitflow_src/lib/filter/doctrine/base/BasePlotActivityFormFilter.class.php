<?php

/**
 * PlotActivity filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BasePlotActivityFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'plot_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Plot'), 'add_empty' => true)),
      'entry_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Application'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'plot_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Plot'), 'column' => 'id')),
      'entry_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Application'), 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('plot_activity_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlotActivity';
  }

  public function getFields()
  {
    return array(
      'id'       => 'Number',
      'plot_id'  => 'ForeignKey',
      'entry_id' => 'ForeignKey',
    );
  }
}
