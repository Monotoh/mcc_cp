<?php

/**
 * PlotOcAreaMatrix filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BasePlotOcAreaMatrixFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'building_type' => new sfWidgetFormFilterInput(),
      'min'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'max'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'fee'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'building_type' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'min'           => new sfValidatorPass(array('required' => false)),
      'max'           => new sfValidatorPass(array('required' => false)),
      'fee'           => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('plot_oc_area_matrix_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlotOcAreaMatrix';
  }

  public function getFields()
  {
    return array(
      'id'            => 'Number',
      'building_type' => 'Number',
      'min'           => 'Text',
      'max'           => 'Text',
      'fee'           => 'Text',
    );
  }
}
