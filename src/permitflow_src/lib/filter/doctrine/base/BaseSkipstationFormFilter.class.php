<?php

/**
 * Skipstation filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseSkipstationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'approval_stage' => new sfWidgetFormFilterInput(),
      'entry_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Application'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'approval_stage' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'entry_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Application'), 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('skipstation_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Skipstation';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'approval_stage' => 'Number',
      'entry_id'       => 'ForeignKey',
    );
  }
}
