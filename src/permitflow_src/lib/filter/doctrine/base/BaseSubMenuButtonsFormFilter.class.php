<?php

/**
 * SubMenuButtons filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseSubMenuButtonsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'sub_menu_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Submenu'), 'add_empty' => true)),
      'button_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Button'), 'add_empty' => true)),
      'order_no'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'backend'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'sub_menu_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Submenu'), 'column' => 'id')),
      'button_id'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Button'), 'column' => 'id')),
      'order_no'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'backend'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('sub_menu_buttons_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SubMenuButtons';
  }

  public function getFields()
  {
    return array(
      'id'          => 'Number',
      'sub_menu_id' => 'ForeignKey',
      'button_id'   => 'ForeignKey',
      'order_no'    => 'Number',
      'backend'     => 'Number',
    );
  }
}
