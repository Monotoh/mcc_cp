<?php

/**
 * SubMenus filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseSubMenusFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'menu_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Menus'), 'add_empty' => true)),
      'title'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'tooltip'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'order_no'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'buttonsubs_list' => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Buttons')),
    ));

    $this->setValidators(array(
      'menu_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Menus'), 'column' => 'id')),
      'title'           => new sfValidatorPass(array('required' => false)),
      'tooltip'         => new sfValidatorPass(array('required' => false)),
      'order_no'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'buttonsubs_list' => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Buttons', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('sub_menus_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addButtonsubsListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.SubMenuButtons SubMenuButtons')
      ->andWhereIn('SubMenuButtons.button_id', $values)
    ;
  }

  public function getModelName()
  {
    return 'SubMenus';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'menu_id'         => 'ForeignKey',
      'title'           => 'Text',
      'tooltip'         => 'Text',
      'order_no'        => 'Number',
      'buttonsubs_list' => 'ManyKey',
    );
  }
}
