<?php

/**
 * Task filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseTaskFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'type'             => new sfWidgetFormFilterInput(),
      'creator_user_id'  => new sfWidgetFormFilterInput(),
      'owner_user_id'    => new sfWidgetFormFilterInput(),
      'duration'         => new sfWidgetFormFilterInput(),
      'start_date'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'end_date'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'priority'         => new sfWidgetFormFilterInput(),
      'is_transferrable' => new sfWidgetFormFilterInput(),
      'active'           => new sfWidgetFormFilterInput(),
      'status'           => new sfWidgetFormFilterInput(),
      'last_update'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'date_created'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'remarks'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'application_id'   => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'type'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'creator_user_id'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'owner_user_id'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'duration'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'start_date'       => new sfValidatorPass(array('required' => false)),
      'end_date'         => new sfValidatorPass(array('required' => false)),
      'priority'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_transferrable' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'active'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'status'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'last_update'      => new sfValidatorPass(array('required' => false)),
      'date_created'     => new sfValidatorPass(array('required' => false)),
      'remarks'          => new sfValidatorPass(array('required' => false)),
      'application_id'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('task_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Task';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'type'             => 'Number',
      'creator_user_id'  => 'Number',
      'owner_user_id'    => 'Number',
      'duration'         => 'Number',
      'start_date'       => 'Text',
      'end_date'         => 'Text',
      'priority'         => 'Number',
      'is_transferrable' => 'Number',
      'active'           => 'Number',
      'status'           => 'Number',
      'last_update'      => 'Text',
      'date_created'     => 'Text',
      'remarks'          => 'Text',
      'application_id'   => 'Number',
    );
  }
}
