<?php

/**
 * Workflow filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseWorkflowFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'workflow_title' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'workflow_type'  => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'workflow_title' => new sfValidatorPass(array('required' => false)),
      'workflow_type'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('workflow_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Workflow';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'workflow_title' => 'Text',
      'workflow_type'  => 'Number',
    );
  }
}
