<?php

/**
 * WorkflowReviewers filter form base class.
 *
 * @package    permit
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseWorkflowReviewersFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'workflow_id' => new sfWidgetFormFilterInput(),
      'reviewer_id' => new sfWidgetFormFilterInput(),
      'task_type'   => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'workflow_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'reviewer_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'task_type'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('workflow_reviewers_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'WorkflowReviewers';
  }

  public function getFields()
  {
    return array(
      'id'          => 'Number',
      'workflow_id' => 'Number',
      'reviewer_id' => 'Number',
      'task_type'   => 'Number',
    );
  }
}
