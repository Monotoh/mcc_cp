<?php

class BackendResetForm extends BaseForm
{
  public function configure()
  {
    $this->setWidgets(array(
      'password1'    => new sfWidgetFormInputPassword(),
      'password2'   => new sfWidgetFormInputPassword()
    ));
    $this->setValidators(array(
        'password1' => new sfValidatorString(),
        'password2' => new sfValidatorString()
    ));
    $this->widgetSchema->setNameFormat('reset[%s]');
  }
}