<?php

/**
 * Buttons form.
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ButtonsForm extends BaseButtonsForm
{
  public function configure()
  {
	  $this->widgetSchema['img'] = new sfWidgetFormInputFile(array('label' => 'Button Image',
));
	  $this->validatorSchema['img'] = new sfValidatorFile(array(
'required' => false,
'path' => '/srv/www/htdocs/asset_uplds/buttons',
'mime_types' => 'web_images',
));
  }
}
