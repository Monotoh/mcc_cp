<?php

/**
 * Publication form.
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PublicationForm extends BasePublicationForm
{
  public function configure()
  {
	$this->useFields(array('title', 'article'));
  }
}
