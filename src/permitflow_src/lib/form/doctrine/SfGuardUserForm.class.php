<?php

/**
 * SfGuardUser form.
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class SfGuardUserForm extends BaseSfGuardUserForm
{
  public function configure()
  {
  

	unset(
      $this['is_active'],
      $this['is_super_admin'],
      $this['updated_at'],
      $this['created_at'],
      $this['last_login'],
      $this['salt'],
      $this['algorithm'],
      $this['groups_list'],
      $this['permissions_list']
    );
  }
}
