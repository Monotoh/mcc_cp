<?php

/**
 * SubMenus form.
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class SubMenusForm extends BaseSubMenusForm
{
  public function configure()
  {
	 unset($this['max_duration'], $this['menu_id'], $this['change_identifier']);
  }
}
