<?php

/**
 * Workflow form.
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class WorkflowForm extends BaseWorkflowForm
{
  public function configure()
  {
    $this->setWidgets(array(
		'workflow_title' => new sfWidgetFormInputText()
	));
	$this->widgetSchema['workflow_type'] = new sfWidgetFormChoice(array(
		'choices' => array('1' => 'Concurrent', '2' => 'Sequential'),
		'expanded' => false,
	));
  }
}
