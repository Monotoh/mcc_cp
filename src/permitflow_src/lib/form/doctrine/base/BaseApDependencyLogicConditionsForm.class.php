<?php

abstract class BaseApDependencyLogicConditionsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      #'form_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ApForms'), 'add_empty' => true)),
      #'target_element_id'     => new sfWidgetFormChoice(array('choices' => array())),
      'element_name'           => new sfWidgetFormInputText(),
      'rule_keyword'           => new sfWidgetFormInputText(),
      'logic_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('DependencyLogic'), 'add_empty' => true)),
      'rule_condition'      => new sfWidgetFormSelect(array('choices' => array('is' => 'Is' , 'is_not' => 'Is Not'))),
      'rule_keyword_option_id'           => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      #'form_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('ApForms'), 'required' => false)),
      #'target_element_id' => new CustomDynamicChoiceValidator(array('choices' => array(), 'required' => false)),
      'element_name'      => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'rule_keyword'      => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'logic_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('DependencyLogic'), 'required' => false)),
      'rule_condition'      => new sfValidatorChoice(array('choices' => array('is', 'is_not'))),
      'rule_keyword_option_id' => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ap_dependency_logic_conditions[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ApDependencyLogicConditions';
  }

}
