<?php

abstract class BaseApDependencyLogicForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'form_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Form'), 'add_empty' => true)),
      'element_id'     => new sfWidgetFormChoice(array('choices' => array())),
      'rule_all_any'      => new sfWidgetFormSelect(array('choices' => array('all' => 'Apply if all of the conditions are met' , 'any' => 'Apply if any of the conditions are met'))),
      'integration_logic'      => new sfWidgetFormSelect(array('choices' => array(0 => 'Do not Fill based on values from LAIS' , 1 => 'Fill based on values from LAIS'))),
      'integration_url'      => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'form_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Form'), 'required' => false)),
      'element_id'  => new CustomDynamicChoiceValidator(array('choices' => array(), 'required' => false)),
      'rule_all_any'      => new sfValidatorChoice(array('choices' => array('all', 'any'))),
      'integration_logic'      => new sfValidatorChoice(array('choices' => array(0, 1))),
      'integration_url'      => new sfValidatorString(array('max_length' => 500, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ap_dependency_logic[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ApDependencyLogic';
  }

}
