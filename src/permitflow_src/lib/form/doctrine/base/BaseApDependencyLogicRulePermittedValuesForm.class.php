<?php

abstract class BaseApDependencyLogicRulePermittedValuesForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      #'form_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ApForms'), 'add_empty' => true)),
      'element_id'   => new sfWidgetFormChoice(array('choices' => array())),
      'element_name'           => new sfWidgetFormInputText(),
      'permitted_value'           => new sfWidgetFormInputText(),
      'logic_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('DependencyLogic'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      #'form_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('ApForms'), 'required' => false)),
      'element_id'   => new CustomDynamicChoiceValidator(array('choices' => array(), 'required' => false)),
      'element_name'      => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'permitted_value'      => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'logic_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('DependencyLogic'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ap_dependency_logic_rule_permitted_values[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ApDependencyLogicRulePermittedValues';
  }

}
