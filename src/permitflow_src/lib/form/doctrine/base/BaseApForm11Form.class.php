<?php

/**
 * ApForm11 form base class.
 *
 * @method ApForm11 getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseApForm11Form extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'date_created' => new sfWidgetFormDateTime(),
      'date_updated' => new sfWidgetFormDateTime(),
      'ip_address'   => new sfWidgetFormInputText(),
      'element_1'    => new sfWidgetFormTextarea(),
      'element_2'    => new sfWidgetFormTextarea(),
      'element_3'    => new sfWidgetFormTextarea(),
      'element_4'    => new sfWidgetFormTextarea(),
      'element_5'    => new sfWidgetFormTextarea(),
      'element_6'    => new sfWidgetFormTextarea(),
      'element_7'    => new sfWidgetFormTextarea(),
      'element_8'    => new sfWidgetFormTextarea(),
      'element_9'    => new sfWidgetFormTextarea(),
      'element_10'   => new sfWidgetFormTextarea(),
      'element_11'   => new sfWidgetFormTextarea(),
      'element_12'   => new sfWidgetFormTextarea(),
      'element_13'   => new sfWidgetFormInputText(),
      'element_14'   => new sfWidgetFormInputText(),
      'element_15'   => new sfWidgetFormTextarea(),
      'element_16'   => new sfWidgetFormTextarea(),
      'element_17'   => new sfWidgetFormTextarea(),
      'element_18'   => new sfWidgetFormTextarea(),
      'element_19'   => new sfWidgetFormTextarea(),
      'element_20'   => new sfWidgetFormTextarea(),
      'element_21'   => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'date_created' => new sfValidatorDateTime(array('required' => false)),
      'date_updated' => new sfValidatorDateTime(array('required' => false)),
      'ip_address'   => new sfValidatorString(array('max_length' => 15, 'required' => false)),
      'element_1'    => new sfValidatorString(array('required' => false)),
      'element_2'    => new sfValidatorString(array('required' => false)),
      'element_3'    => new sfValidatorString(array('required' => false)),
      'element_4'    => new sfValidatorString(array('required' => false)),
      'element_5'    => new sfValidatorString(array('required' => false)),
      'element_6'    => new sfValidatorString(array('required' => false)),
      'element_7'    => new sfValidatorString(array('required' => false)),
      'element_8'    => new sfValidatorString(array('required' => false)),
      'element_9'    => new sfValidatorString(array('required' => false)),
      'element_10'   => new sfValidatorString(array('required' => false)),
      'element_11'   => new sfValidatorString(array('required' => false)),
      'element_12'   => new sfValidatorString(array('required' => false)),
      'element_13'   => new sfValidatorInteger(array('required' => false)),
      'element_14'   => new sfValidatorInteger(array('required' => false)),
      'element_15'   => new sfValidatorString(array('required' => false)),
      'element_16'   => new sfValidatorString(array('required' => false)),
      'element_17'   => new sfValidatorString(array('required' => false)),
      'element_18'   => new sfValidatorString(array('required' => false)),
      'element_19'   => new sfValidatorString(array('required' => false)),
      'element_20'   => new sfValidatorString(array('required' => false)),
      'element_21'   => new sfValidatorString(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ap_form11[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ApForm11';
  }

}
