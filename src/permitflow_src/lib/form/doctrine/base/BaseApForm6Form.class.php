<?php

/**
 * ApForm6 form base class.
 *
 * @method ApForm6 getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseApForm6Form extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'date_created' => new sfWidgetFormDateTime(),
      'date_updated' => new sfWidgetFormDateTime(),
      'ip_address'   => new sfWidgetFormInputText(),
      'element_1_1'  => new sfWidgetFormInputText(),
      'element_1_2'  => new sfWidgetFormInputText(),
      'element_2'    => new sfWidgetFormTextarea(),
      'element_3'    => new sfWidgetFormTextarea(),
      'element_4_1'  => new sfWidgetFormInputText(),
      'element_4_2'  => new sfWidgetFormInputText(),
      'element_4_3'  => new sfWidgetFormInputText(),
      'element_4_4'  => new sfWidgetFormInputText(),
      'element_4_5'  => new sfWidgetFormInputText(),
      'element_4_6'  => new sfWidgetFormInputText(),
      'element_5'    => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'date_created' => new sfValidatorDateTime(array('required' => false)),
      'date_updated' => new sfValidatorDateTime(array('required' => false)),
      'ip_address'   => new sfValidatorString(array('max_length' => 15, 'required' => false)),
      'element_1_1'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'element_1_2'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'element_2'    => new sfValidatorString(array('required' => false)),
      'element_3'    => new sfValidatorString(array('required' => false)),
      'element_4_1'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'element_4_2'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'element_4_3'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'element_4_4'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'element_4_5'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'element_4_6'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'element_5'    => new sfValidatorString(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ap_form6[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ApForm6';
  }

}
