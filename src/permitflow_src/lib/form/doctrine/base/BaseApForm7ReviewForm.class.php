<?php

/**
 * ApForm7Review form base class.
 *
 * @method ApForm7Review getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseApForm7ReviewForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'date_created' => new sfWidgetFormDateTime(),
      'date_updated' => new sfWidgetFormDateTime(),
      'ip_address'   => new sfWidgetFormInputText(),
      'element_1_1'  => new sfWidgetFormInputText(),
      'element_1_2'  => new sfWidgetFormInputText(),
      'element_2'    => new sfWidgetFormTextarea(),
      'element_3'    => new sfWidgetFormTextarea(),
      'session_id'   => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'date_created' => new sfValidatorDateTime(array('required' => false)),
      'date_updated' => new sfValidatorDateTime(array('required' => false)),
      'ip_address'   => new sfValidatorString(array('max_length' => 15, 'required' => false)),
      'element_1_1'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'element_1_2'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'element_2'    => new sfValidatorString(array('required' => false)),
      'element_3'    => new sfValidatorString(array('required' => false)),
      'session_id'   => new sfValidatorString(array('max_length' => 100, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ap_form7_review[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ApForm7Review';
  }

}
