<?php

/**
 * ApFormElements form base class.
 *
 * @method ApFormElements getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseApFormElementsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'form_id'               => new sfWidgetFormInputHidden(),
      'element_id'            => new sfWidgetFormInputHidden(),
      'element_title'         => new sfWidgetFormTextarea(),
      'element_guidelines'    => new sfWidgetFormTextarea(),
      'element_size'          => new sfWidgetFormInputText(),
      'element_is_required'   => new sfWidgetFormInputText(),
      'element_is_unique'     => new sfWidgetFormInputText(),
      'element_is_private'    => new sfWidgetFormInputText(),
      'element_type'          => new sfWidgetFormInputText(),
      'element_position'      => new sfWidgetFormInputText(),
      'element_default_value' => new sfWidgetFormTextarea(),
      'element_constraint'    => new sfWidgetFormInputText(),
      'element_total_child'   => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'form_id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('form_id')), 'empty_value' => $this->getObject()->get('form_id'), 'required' => false)),
      'element_id'            => new sfValidatorChoice(array('choices' => array($this->getObject()->get('element_id')), 'empty_value' => $this->getObject()->get('element_id'), 'required' => false)),
      'element_title'         => new sfValidatorString(array('required' => false)),
      'element_guidelines'    => new sfValidatorString(array('required' => false)),
      'element_size'          => new sfValidatorString(array('max_length' => 6, 'required' => false)),
      'element_is_required'   => new sfValidatorInteger(array('required' => false)),
      'element_is_unique'     => new sfValidatorInteger(array('required' => false)),
      'element_is_private'    => new sfValidatorInteger(array('required' => false)),
      'element_type'          => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'element_position'      => new sfValidatorInteger(array('required' => false)),
      'element_default_value' => new sfValidatorString(array('required' => false)),
      'element_constraint'    => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'element_total_child'   => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ap_form_elements[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ApFormElements';
  }

}
