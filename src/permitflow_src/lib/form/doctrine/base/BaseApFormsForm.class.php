<?php

/**
 * ApForms form base class.
 *
 * @method ApForms getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseApFormsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'form_id'                  => new sfWidgetFormInputHidden(),
      'form_name'                => new sfWidgetFormTextarea(),
      'form_description'         => new sfWidgetFormTextarea(),
      'form_email'               => new sfWidgetFormInputText(),
      'form_redirect'            => new sfWidgetFormTextarea(),
      'form_success_message'     => new sfWidgetFormTextarea(),
      'form_password'            => new sfWidgetFormInputText(),
      'form_unique_ip'           => new sfWidgetFormInputText(),
      'form_frame_height'        => new sfWidgetFormInputText(),
      'form_has_css'             => new sfWidgetFormInputText(),
      'form_captcha'             => new sfWidgetFormInputText(),
      'form_active'              => new sfWidgetFormInputText(),
      'form_review'              => new sfWidgetFormInputText(),
      'form_default_application' => new sfWidgetFormInputText(),
      'esl_from_name'            => new sfWidgetFormTextarea(),
      'esl_from_email_address'   => new sfWidgetFormInputText(),
      'esl_subject'              => new sfWidgetFormTextarea(),
      'esl_content'              => new sfWidgetFormTextarea(),
      'esl_plain_text'           => new sfWidgetFormInputText(),
      'esr_email_address'        => new sfWidgetFormInputText(),
      'esr_from_name'            => new sfWidgetFormTextarea(),
      'esr_from_email_address'   => new sfWidgetFormInputText(),
      'esr_subject'              => new sfWidgetFormTextarea(),
      'esr_content'              => new sfWidgetFormTextarea(),
      'esr_plain_text'           => new sfWidgetFormInputText(),
      //OTB additional of missing fields
      'form_review_title'           => new sfWidgetFormInputText(),
      'form_review_description'           => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'form_id'                  => new sfValidatorChoice(array('choices' => array($this->getObject()->get('form_id')), 'empty_value' => $this->getObject()->get('form_id'), 'required' => false)),
      'form_name'                => new sfValidatorString(array('required' => false)),
      'form_description'         => new sfValidatorString(array('required' => false)),
      'form_email'               => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'form_redirect'            => new sfValidatorString(array('required' => false)),
      'form_success_message'     => new sfValidatorString(array('required' => false)),
      'form_password'            => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'form_unique_ip'           => new sfValidatorInteger(array('required' => false)),
      'form_frame_height'        => new sfValidatorInteger(array('required' => false)),
      'form_has_css'             => new sfValidatorInteger(array('required' => false)),
      'form_captcha'             => new sfValidatorInteger(array('required' => false)),
      'form_active'              => new sfValidatorInteger(array('required' => false)),
      'form_review'              => new sfValidatorInteger(array('required' => false)),
      'form_default_application' => new sfValidatorInteger(array('required' => false)),
      'esl_from_name'            => new sfValidatorString(array('required' => false)),
      'esl_from_email_address'   => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'esl_subject'              => new sfValidatorString(array('required' => false)),
      'esl_content'              => new sfValidatorString(array('required' => false)),
      'esl_plain_text'           => new sfValidatorInteger(array('required' => false)),
      'esr_email_address'        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'esr_from_name'            => new sfValidatorString(array('required' => false)),
      'esr_from_email_address'   => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'esr_subject'              => new sfValidatorString(array('required' => false)),
      'esr_content'              => new sfValidatorString(array('required' => false)),
      'esr_plain_text'           => new sfValidatorInteger(array('required' => false)),
       //OTB patch
      'form_review_title'   => new sfValidatorString(array('max_length' => 1000, 'required' => false)),  
      'form_review_description'   => new sfValidatorString(array('max_length' => 1000, 'required' => false)),    
    ));

    $this->widgetSchema->setNameFormat('ap_forms[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ApForms';
  }

}
