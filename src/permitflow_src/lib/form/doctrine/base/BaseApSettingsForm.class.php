<?php

/**
 * ApSettings form base class.
 *
 * @method ApSettings getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseApSettingsForm extends BaseFormDoctrine
{
  public function setup()
  {

    $siteconfig = Doctrine_Core::getTable('ApSettings')->find(array(1));

    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'organisation_name'       => new sfWidgetFormInputText(array(), array('class' => 'form-control')),
      'organisation_email'       => new sfWidgetFormInputText(array(), array('class' => 'form-control')),
      'organisation_description'       => new sfWidgetFormTextarea(array(), array('class' => 'form-control')),
      'organisation_help'       => new sfWidgetFormTextarea(array(), array('class' => 'form-control')),
      'organisation_sidebar'       => new sfWidgetFormTextarea(array(), array('class' => 'form-control')),
      'upload_dir'       => new sfWidgetFormInputText(array(), array('class' => 'form-control')),
      'data_dir'       => new sfWidgetFormInputText(array(), array('class' => 'form-control')),
      'upload_dir_web'       => new sfWidgetFormInputText(array(), array('class' => 'form-control')),
      'data_dir_web'       => new sfWidgetFormInputText(array(), array('class' => 'form-control')),
      'profile_dir'       => new sfWidgetFormInputText(array(), array('class' => 'form-control')), //OTB patch 
      'smtp_host'       => new sfWidgetFormInputText(array(), array('class' => 'form-control')),
      'smtp_port'       => new sfWidgetFormInputText(array(), array('class' => 'form-control')),
      'smtp_username'       => new sfWidgetFormInputText(array(), array('class' => 'form-control')),
      'smtp_password'       => new sfWidgetFormInputText(array(), array('class' => 'form-control')),
      'smtp_enable'       => new sfWidgetFormChoice(array('choices' => array('1' => 'Enabled', '0' => 'Disabled'))),
      'smtp_auth'       => new sfWidgetFormChoice(array('choices' => array('0' => 'Plain', '1' => 'Login'))),
      'smtp_secure'       => new sfWidgetFormChoice(array('choices' => array('0' => 'SSL', '1' => 'TLS'))),
      'admin_image_url'       => new sfWidgetFormInputFile(array('label' => 'Agency Logo Image'), array('class' => 'form-control'))
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'organisation_name'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'organisation_email' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'organisation_description' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'organisation_help' => new sfValidatorString(array('max_length' => 2000, 'required' => false)),
      'organisation_sidebar' => new sfValidatorString(array('max_length' => 2000, 'required' => false)),
      'upload_dir' => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'profile_dir' => new sfValidatorString(array('max_length' => 1000, 'required' => false)),  //otb patch
      'data_dir' => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'upload_dir_web' => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'data_dir_web' => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'smtp_host' => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'smtp_port' => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'smtp_username' => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'smtp_password' => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'smtp_enable'          => new sfValidatorChoice(array('choices' => array_keys(array('1' => 'Enabled', '0' => 'Disabled')))),
      'smtp_auth'          => new sfValidatorChoice(array('choices' => array_keys(array('0' => 'Plain', '1' => 'Login')))),
      'smtp_secure'          => new sfValidatorChoice(array('choices' => array_keys(array('0' => 'SSL', '1' => 'TLS')))),
      'admin_image_url'       => new sfValidatorFile(array(
        'required' => false,
        'path' => $siteconfig->getUploadDir(),
        'mime_types' => 'web_images',
        ))
    ));

    $this->widgetSchema->setNameFormat('ap_settings[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ApSettings';
  }

}
