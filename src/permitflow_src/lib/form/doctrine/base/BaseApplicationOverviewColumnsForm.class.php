<?php

/**
 * ApplicationOverviewColumns form base class.
 *
 * @method ApplicationOverviewColumns getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseApplicationOverviewColumnsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'form_id'       => new sfWidgetFormInputText(),
      'element_id'    => new sfWidgetFormInputText(),
      'custom_header' => new sfWidgetFormInputText(),
      'position'      => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'form_id'       => new sfValidatorInteger(array('required' => false)),
      'element_id'    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'custom_header' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'position'      => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('application_overview_columns[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ApplicationOverviewColumns';
  }

}
