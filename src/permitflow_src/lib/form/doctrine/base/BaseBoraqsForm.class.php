<?php

/**
 * Plot form base class.
 *
 * @method Plot getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseBoraqsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'members_no'   => new sfWidgetFormInputText(),
      'full_name' => new sfWidgetFormInputText(),
      'email'   => new sfWidgetFormInputText(),
      'address'    => new sfWidgetFormInputText(),
      'town'   => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'members_no'   => new sfValidatorString(array('max_length' => 250, 'required' => false)),
      'full_name' => new sfValidatorString(array('max_length' => 250, 'required' => false)),
      'email'   => new sfValidatorString(array('max_length' => 250, 'required' => false)),
      'address'    => new sfValidatorString(array('max_length' => 250, 'required' => false)),
      'town'   => new sfValidatorString(array('max_length' => 250, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('plot[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Boraqs';
  }

}
