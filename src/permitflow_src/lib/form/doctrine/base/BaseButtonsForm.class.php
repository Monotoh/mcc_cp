<?php

/**
 * Buttons form base class.
 *
 * @method Buttons getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseButtonsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'img'           => new sfWidgetFormTextarea(),
      'title'         => new sfWidgetFormTextarea(),
      'tooltip'       => new sfWidgetFormTextarea(),
      'link'          => new sfWidgetFormTextarea(),
      'submenus_list' => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'SubMenus')),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'img'           => new sfValidatorString(),
      'title'         => new sfValidatorString(),
      'tooltip'       => new sfValidatorString(array('required' => false)),
      'link'          => new sfValidatorString(array('required' => false)),
      'submenus_list' => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'SubMenus', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('buttons[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Buttons';
  }

  public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['submenus_list']))
    {
      $this->setDefault('submenus_list', $this->object->Submenus->getPrimaryKeys());
    }

  }

  protected function doSave($con = null)
  {
    $this->saveSubmenusList($con);

    parent::doSave($con);
  }

  public function saveSubmenusList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['submenus_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Submenus->getPrimaryKeys();
    $values = $this->getValue('submenus_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Submenus', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Submenus', array_values($link));
    }
  }

}
