<?php

/**
 * CfAttachment form base class.
 *
 * @method CfAttachment getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCfAttachmentForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nid'                   => new sfWidgetFormInputHidden(),
      'strpath'               => new sfWidgetFormTextarea(),
      'ncirculationhistoryid' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'nid'                   => new sfValidatorChoice(array('choices' => array($this->getObject()->get('nid')), 'empty_value' => $this->getObject()->get('nid'), 'required' => false)),
      'strpath'               => new sfValidatorString(),
      'ncirculationhistoryid' => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cf_attachment[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CfAttachment';
  }

}
