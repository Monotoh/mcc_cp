<?php

/**
 * CfCirculationform form base class.
 *
 * @method CfCirculationform getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCfCirculationformForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nid'            => new sfWidgetFormInputHidden(),
      'nsenderid'      => new sfWidgetFormInputText(),
      'strname'        => new sfWidgetFormTextarea(),
      'nmailinglistid' => new sfWidgetFormInputText(),
      'bisarchived'    => new sfWidgetFormInputText(),
      'nendaction'     => new sfWidgetFormInputText(),
      'bdeleted'       => new sfWidgetFormInputText(),
      'banonymize'     => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'nid'            => new sfValidatorChoice(array('choices' => array($this->getObject()->get('nid')), 'empty_value' => $this->getObject()->get('nid'), 'required' => false)),
      'nsenderid'      => new sfValidatorInteger(array('required' => false)),
      'strname'        => new sfValidatorString(),
      'nmailinglistid' => new sfValidatorInteger(array('required' => false)),
      'bisarchived'    => new sfValidatorInteger(array('required' => false)),
      'nendaction'     => new sfValidatorInteger(array('required' => false)),
      'bdeleted'       => new sfValidatorInteger(),
      'banonymize'     => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cf_circulationform[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CfCirculationform';
  }

}
