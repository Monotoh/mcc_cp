<?php

/**
 * CfCirculationhistory form base class.
 *
 * @method CfCirculationhistory getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCfCirculationhistoryForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nid'                => new sfWidgetFormInputHidden(),
      'nrevisionnumber'    => new sfWidgetFormInputText(),
      'datesending'        => new sfWidgetFormInputText(),
      'stradditionaltext'  => new sfWidgetFormTextarea(),
      'ncirculationformid' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'nid'                => new sfValidatorChoice(array('choices' => array($this->getObject()->get('nid')), 'empty_value' => $this->getObject()->get('nid'), 'required' => false)),
      'nrevisionnumber'    => new sfValidatorInteger(array('required' => false)),
      'datesending'        => new sfValidatorInteger(array('required' => false)),
      'stradditionaltext'  => new sfValidatorString(),
      'ncirculationformid' => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cf_circulationhistory[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CfCirculationhistory';
  }

}
