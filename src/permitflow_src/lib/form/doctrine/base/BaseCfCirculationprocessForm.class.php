<?php

/**
 * CfCirculationprocess form base class.
 *
 * @method CfCirculationprocess getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCfCirculationprocessForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nid'                   => new sfWidgetFormInputHidden(),
      'ncirculationformid'    => new sfWidgetFormInputText(),
      'nslotid'               => new sfWidgetFormInputText(),
      'nuserid'               => new sfWidgetFormInputText(),
      'dateinprocesssince'    => new sfWidgetFormInputText(),
      'ndecissionstate'       => new sfWidgetFormInputText(),
      'datedecission'         => new sfWidgetFormInputText(),
      'nissubstitiuteof'      => new sfWidgetFormInputText(),
      'ncirculationhistoryid' => new sfWidgetFormInputText(),
      'nresendcount'          => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'nid'                   => new sfValidatorChoice(array('choices' => array($this->getObject()->get('nid')), 'empty_value' => $this->getObject()->get('nid'), 'required' => false)),
      'ncirculationformid'    => new sfValidatorInteger(array('required' => false)),
      'nslotid'               => new sfValidatorInteger(array('required' => false)),
      'nuserid'               => new sfValidatorInteger(array('required' => false)),
      'dateinprocesssince'    => new sfValidatorInteger(array('required' => false)),
      'ndecissionstate'       => new sfValidatorInteger(array('required' => false)),
      'datedecission'         => new sfValidatorInteger(array('required' => false)),
      'nissubstitiuteof'      => new sfValidatorInteger(array('required' => false)),
      'ncirculationhistoryid' => new sfValidatorInteger(array('required' => false)),
      'nresendcount'          => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cf_circulationprocess[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CfCirculationprocess';
  }

}
