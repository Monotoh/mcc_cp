<?php

/**
 * CfFieldvalue form base class.
 *
 * @method CfFieldvalue getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCfFieldvalueForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nid'                   => new sfWidgetFormInputHidden(),
      'ninputfieldid'         => new sfWidgetFormInputText(),
      'strfieldvalue'         => new sfWidgetFormTextarea(),
      'nslotid'               => new sfWidgetFormInputText(),
      'nformid'               => new sfWidgetFormInputText(),
      'ncirculationhistoryid' => new sfWidgetFormInputText(),
      'nuserid'               => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('User'), 'add_empty' => true)),
      'nrevisionid'           => new sfWidgetFormInputText(),
      'nrevisiondate'         => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'nid'                   => new sfValidatorChoice(array('choices' => array($this->getObject()->get('nid')), 'empty_value' => $this->getObject()->get('nid'), 'required' => false)),
      'ninputfieldid'         => new sfValidatorInteger(array('required' => false)),
      'strfieldvalue'         => new sfValidatorString(),
      'nslotid'               => new sfValidatorInteger(array('required' => false)),
      'nformid'               => new sfValidatorInteger(array('required' => false)),
      'ncirculationhistoryid' => new sfValidatorInteger(array('required' => false)),
      'nuserid'               => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('User'), 'required' => false)),
      'nrevisionid'           => new sfValidatorInteger(array('required' => false)),
      'nrevisiondate'         => new sfValidatorString(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cf_fieldvalue[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CfFieldvalue';
  }

}
