<?php

/**
 * CfFilter form base class.
 *
 * @method CfFilter getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCfFilterForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nid'                   => new sfWidgetFormInputHidden(),
      'nuserid'               => new sfWidgetFormInputText(),
      'strlabel'              => new sfWidgetFormTextarea(),
      'strname'               => new sfWidgetFormTextarea(),
      'nstationid'            => new sfWidgetFormInputText(),
      'ndaysinprogress_start' => new sfWidgetFormTextarea(),
      'ndaysinprogress_end'   => new sfWidgetFormTextarea(),
      'strsenddate_start'     => new sfWidgetFormTextarea(),
      'strsenddate_end'       => new sfWidgetFormTextarea(),
      'nmailinglistid'        => new sfWidgetFormInputText(),
      'ntemplateid'           => new sfWidgetFormInputText(),
      'strcustomfilter'       => new sfWidgetFormTextarea(),
      'nsenderid'             => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'nid'                   => new sfValidatorChoice(array('choices' => array($this->getObject()->get('nid')), 'empty_value' => $this->getObject()->get('nid'), 'required' => false)),
      'nuserid'               => new sfValidatorInteger(array('required' => false)),
      'strlabel'              => new sfValidatorString(),
      'strname'               => new sfValidatorString(),
      'nstationid'            => new sfValidatorInteger(array('required' => false)),
      'ndaysinprogress_start' => new sfValidatorString(),
      'ndaysinprogress_end'   => new sfValidatorString(),
      'strsenddate_start'     => new sfValidatorString(),
      'strsenddate_end'       => new sfValidatorString(),
      'nmailinglistid'        => new sfValidatorInteger(array('required' => false)),
      'ntemplateid'           => new sfValidatorInteger(array('required' => false)),
      'strcustomfilter'       => new sfValidatorString(),
      'nsenderid'             => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cf_filter[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CfFilter';
  }

}
