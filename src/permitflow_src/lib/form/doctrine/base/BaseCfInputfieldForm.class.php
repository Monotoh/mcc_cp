<?php

/**
 * CfInputfield form base class.
 *
 * @method CfInputfield getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCfInputfieldForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nid'              => new sfWidgetFormInputHidden(),
      'strname'          => new sfWidgetFormTextarea(),
      'ntype'            => new sfWidgetFormInputText(),
      'strstandardvalue' => new sfWidgetFormTextarea(),
      'breadonly'        => new sfWidgetFormInputText(),
      'strbgcolor'       => new sfWidgetFormTextarea(),
      'commenttype'      => new sfWidgetFormTextarea(),
      'commentrelation'  => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'nid'              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('nid')), 'empty_value' => $this->getObject()->get('nid'), 'required' => false)),
      'strname'          => new sfValidatorString(),
      'ntype'            => new sfValidatorInteger(array('required' => false)),
      'strstandardvalue' => new sfValidatorString(),
      'breadonly'        => new sfValidatorInteger(array('required' => false)),
      'strbgcolor'       => new sfValidatorString(),
      'commenttype'      => new sfValidatorString(),
      'commentrelation'  => new sfValidatorString(),
    ));

    $this->widgetSchema->setNameFormat('cf_inputfield[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CfInputfield';
  }

}
