<?php

/**
 * CfMailinglist form base class.
 *
 * @method CfMailinglist getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCfMailinglistForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nid'         => new sfWidgetFormInputHidden(),
      'strname'     => new sfWidgetFormTextarea(),
      'ntemplateid' => new sfWidgetFormInputText(),
      'bisedited'   => new sfWidgetFormInputText(),
      'bisdefault'  => new sfWidgetFormInputText(),
      'bdeleted'    => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'nid'         => new sfValidatorChoice(array('choices' => array($this->getObject()->get('nid')), 'empty_value' => $this->getObject()->get('nid'), 'required' => false)),
      'strname'     => new sfValidatorString(),
      'ntemplateid' => new sfValidatorInteger(array('required' => false)),
      'bisedited'   => new sfValidatorInteger(array('required' => false)),
      'bisdefault'  => new sfValidatorInteger(array('required' => false)),
      'bdeleted'    => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cf_mailinglist[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CfMailinglist';
  }

}
