<?php

/**
 * CfSlottofield form base class.
 *
 * @method CfSlottofield getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCfSlottofieldForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nid'       => new sfWidgetFormInputHidden(),
      'nslotid'   => new sfWidgetFormInputText(),
      'nfieldid'  => new sfWidgetFormInputText(),
      'nposition' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'nid'       => new sfValidatorChoice(array('choices' => array($this->getObject()->get('nid')), 'empty_value' => $this->getObject()->get('nid'), 'required' => false)),
      'nslotid'   => new sfValidatorInteger(array('required' => false)),
      'nfieldid'  => new sfValidatorInteger(array('required' => false)),
      'nposition' => new sfValidatorInteger(),
    ));

    $this->widgetSchema->setNameFormat('cf_slottofield[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CfSlottofield';
  }

}
