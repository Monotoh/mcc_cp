<?php

/**
 * CfSlottouser form base class.
 *
 * @method CfSlottouser getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCfSlottouserForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nid'            => new sfWidgetFormInputHidden(),
      'nslotid'        => new sfWidgetFormInputText(),
      'nmailinglistid' => new sfWidgetFormInputText(),
      'nuserid'        => new sfWidgetFormInputText(),
      'status'         => new sfWidgetFormInputText(),
      'nposition'      => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'nid'            => new sfValidatorChoice(array('choices' => array($this->getObject()->get('nid')), 'empty_value' => $this->getObject()->get('nid'), 'required' => false)),
      'nslotid'        => new sfValidatorInteger(array('required' => false)),
      'nmailinglistid' => new sfValidatorInteger(array('required' => false)),
      'nuserid'        => new sfValidatorInteger(array('required' => false)),
      'status'         => new sfValidatorInteger(array('required' => false)),
      'nposition'      => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cf_slottouser[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CfSlottouser';
  }

}
