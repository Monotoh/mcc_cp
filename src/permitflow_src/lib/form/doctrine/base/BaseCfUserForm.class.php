<?php

/**
 * CfUser form base class.
 *
 * @method CfUser getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCfUserForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nid'                         => new sfWidgetFormInputHidden(),
      'strlastname'                 => new sfWidgetFormTextarea(),
      'strfirstname'                => new sfWidgetFormTextarea(),
      'stremail'                    => new sfWidgetFormTextarea(),
      'naccesslevel'                => new sfWidgetFormInputText(),
      'struserid'                   => new sfWidgetFormTextarea(),
      'strpassword'                 => new sfWidgetFormTextarea(),
      'stremail_format'             => new sfWidgetFormInputText(),
      'stremail_values'             => new sfWidgetFormInputText(),
      'nsubstitudeid'               => new sfWidgetFormInputText(),
      'tslastaction'                => new sfWidgetFormInputText(),
      'bdeleted'                    => new sfWidgetFormInputText(),
      'strstreet'                   => new sfWidgetFormTextarea(),
      'strcountry'                  => new sfWidgetFormTextarea(),
      'strzipcode'                  => new sfWidgetFormTextarea(),
      'strcity'                     => new sfWidgetFormTextarea(),
      'strphone_main1'              => new sfWidgetFormTextarea(),
      'strphone_main2'              => new sfWidgetFormTextarea(),
      'strphone_mobile'             => new sfWidgetFormTextarea(),
      'strfax'                      => new sfWidgetFormTextarea(),
      'strorganisation'             => new sfWidgetFormTextarea(),
      'strdepartment'               => new sfWidgetFormTextarea(),
      'strcostcenter'               => new sfWidgetFormTextarea(),
      'userdefined1_value'          => new sfWidgetFormTextarea(),
      'userdefined2_value'          => new sfWidgetFormTextarea(),
      'nsubstitutetimevalue'        => new sfWidgetFormInputText(),
      'strsubstitutetimeunit'       => new sfWidgetFormTextarea(),
      'busegeneralsubstituteconfig' => new sfWidgetFormInputText(),
      'busegeneralemailconfig'      => new sfWidgetFormInputText(),
      'groups_list'                 => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'mfGuardGroup')),
	  'pass_change'      => new sfWidgetFormInputText(),//OTB - Password change from old system - also used for password expire
    ));

    $this->setValidators(array(
      'nid'                         => new sfValidatorChoice(array('choices' => array($this->getObject()->get('nid')), 'empty_value' => $this->getObject()->get('nid'), 'required' => false)),
      'strlastname'                 => new sfValidatorString(),
      'strfirstname'                => new sfValidatorString(),
      'stremail'                    => new sfValidatorString(),
      'naccesslevel'                => new sfValidatorInteger(array('required' => false)),
      'struserid'                   => new sfValidatorString(),
      'strpassword'                 => new sfValidatorString(),
      'stremail_format'             => new sfValidatorString(array('max_length' => 8, 'required' => false)),
      'stremail_values'             => new sfValidatorString(array('max_length' => 8, 'required' => false)),
      'nsubstitudeid'               => new sfValidatorInteger(array('required' => false)),
      'tslastaction'                => new sfValidatorInteger(),
      'bdeleted'                    => new sfValidatorInteger(),
      'strstreet'                   => new sfValidatorString(),
      'strcountry'                  => new sfValidatorString(),
      'strzipcode'                  => new sfValidatorString(),
      'strcity'                     => new sfValidatorString(),
      'strphone_main1'              => new sfValidatorString(),
      'strphone_main2'              => new sfValidatorString(),
      'strphone_mobile'             => new sfValidatorString(),
      'strfax'                      => new sfValidatorString(),
      'strorganisation'             => new sfValidatorString(),
      'strdepartment'               => new sfValidatorString(),
      'strcostcenter'               => new sfValidatorString(),
      'userdefined1_value'          => new sfValidatorString(),
      'userdefined2_value'          => new sfValidatorString(),
      'nsubstitutetimevalue'        => new sfValidatorInteger(),
      'strsubstitutetimeunit'       => new sfValidatorString(),
      'busegeneralsubstituteconfig' => new sfValidatorInteger(),
      'busegeneralemailconfig'      => new sfValidatorInteger(),
      'groups_list'                 => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'mfGuardGroup', 'required' => false)),
	  'pass_change'      => new sfValidatorInteger(),//OTB - Password change from old system - also used for password expire
    ));

    $this->widgetSchema->setNameFormat('cf_user[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CfUser';
  }

  public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['groups_list']))
    {
      $this->setDefault('groups_list', $this->object->Groups->getPrimaryKeys());
    }

  }

  protected function doSave($con = null)
  {
    $this->saveGroupsList($con);

    parent::doSave($con);
  }

  public function saveGroupsList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['groups_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Groups->getPrimaryKeys();
    $values = $this->getValue('groups_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Groups', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Groups', array_values($link));
    }
  }

}
