<?php

/**
 * Clusters form base class.
 *
 * @method Clusters getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseClustersForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'      => new sfWidgetFormInputHidden(),
      'form_id' => new sfWidgetFormInputText(),
      'field1'  => new sfWidgetFormInputText(),
      'field2'  => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'      => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'form_id' => new sfValidatorString(array('max_length' => 250)),
      'field1'  => new sfValidatorString(array('max_length' => 250)),
      'field2'  => new sfValidatorString(array('max_length' => 250)),
    ));

    $this->widgetSchema->setNameFormat('clusters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Clusters';
  }

}
