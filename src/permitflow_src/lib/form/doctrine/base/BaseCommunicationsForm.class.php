<?php

/**
 * Communications form base class.
 *
 * @method Communications getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCommunicationsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'architect_id'     => new sfWidgetFormInputText(),
      'reviewer_id'      => new sfWidgetFormInputText(),
      'application_id'   => new sfWidgetFormInputText(),
      'messageread'      => new sfWidgetFormInputText(),
      'content'          => new sfWidgetFormTextarea(),
         //OTB patch - Attachment option
      'attachment'             => new sfWidgetFormInputFileEditable(array(
                                'label' => 'Browse File',
                                'file_src' => sfConfig::get('sf_upload_dir').'/'.$this->getObject()->getBanner(),
                                'is_image' => false,
                                'edit_mode' => !$this->getObject()->isNew(),
                                'delete_label' => 'Delete?',
                                'template' => '<div>%file%<br />%input%<br />%delete%%delete_label%</div>',
                                'with_delete' => true )) ,
      'action_timestamp' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'architect_id'     => new sfValidatorInteger(array('required' => false)),
      'reviewer_id'      => new sfValidatorInteger(array('required' => false)),
      'application_id'   => new sfValidatorInteger(array('required' => false)),
      'messageread'      => new sfValidatorInteger(array('required' => false)),
      'content'          => new sfValidatorString(array('required' => false)),
      'action_timestamp' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));
    
     //OTB patch - Uploading of files
     $this->validatorSchema['attachment'] = new sfValidatorFile(array(
                                     'required' => true,
                                     'path' => sfConfig::get('sf_upload_dir').'/' ,
                                    // 'validated_file_class' => 'CustomValidatedFile',
                                     'mime_types' => 'web_images,pdf,zip,doc,docx',
                                  ),  array('invalid' => 'Invalid file.','required' => 'Select a file to upload.',
                                      'mime_types' => 'Attach only images,pdf,docx or zip file types')) ;

    $this->widgetSchema->setNameFormat('communications[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Communications';
  }

}
