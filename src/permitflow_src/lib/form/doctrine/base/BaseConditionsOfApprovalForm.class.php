<?php

/**
 * ConditionsOfApproval form base class.
 *
 * @method ConditionsOfApproval getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseConditionsOfApprovalForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'permit_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Permit'), 'add_empty' => true)),
      'short_name'  => new sfWidgetFormInputText(),
      'description' => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'permit_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Permit'), 'required' => false)),
      'short_name'  => new sfValidatorString(array('max_length' => 250, 'required' => false)),
      'description' => new sfValidatorString(array('max_length' => 4000, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('conditions_of_approval[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ConditionsOfApproval';
  }

}
