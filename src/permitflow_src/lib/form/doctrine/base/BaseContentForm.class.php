<?php

/**
 * Content form base class.
 *
 * @method Content getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseContentForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'top_article'      => new sfWidgetFormTextarea(),
      'bottom_article'   => new sfWidgetFormTextarea(),
      'top_style'        => new sfWidgetFormInputText(),
      'bottom_style'     => new sfWidgetFormInputText(),
      'top_slideshow'    => new sfWidgetFormInputText(),
      'bottom_slideshow' => new sfWidgetFormInputText(),
      'top_sidebar'      => new sfWidgetFormTextarea(),
      'bottom_sidebar'   => new sfWidgetFormTextarea(),
      'custom_footer'    => new sfWidgetFormTextarea(),
      'custom_css'       => new sfWidgetFormTextarea(),
      'top_image'        => new sfWidgetFormInputText(),
      'bottom_image'     => new sfWidgetFormInputText(),
      'menu_title'       => new sfWidgetFormInputText(),
      'breadcrumb_title' => new sfWidgetFormInputText(),
      'content_type'     => new sfWidgetFormChoice(array('choices'  => array('default' => 'default', 'homepage' => 'Homepage Content',), 'expanded' => false,'multiple' => false,)),
      'created_on'       => new sfWidgetFormDateTime(),
      'last_modified_on' => new sfWidgetFormDateTime(),
      'published'        => new sfWidgetFormInputText(),
      'menu_index'       => new sfWidgetFormInputText(),
      'url'              => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'top_article'      => new sfValidatorString(array('required' => false)),
      'bottom_article'   => new sfValidatorString(array('required' => false)),
      'top_style'        => new sfValidatorInteger(array('required' => false)),
      'bottom_style'     => new sfValidatorInteger(array('required' => false)),
      'top_slideshow'    => new sfValidatorInteger(array('required' => false)),
      'bottom_slideshow' => new sfValidatorInteger(array('required' => false)),
      'top_sidebar'      => new sfValidatorString(array('required' => false)),
      'bottom_sidebar'   => new sfValidatorString(array('required' => false)),
      'custom_footer'    => new sfValidatorString(array('required' => false)),
      'custom_css'       => new sfValidatorString(array('required' => false)),
      'top_image'        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'bottom_image'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'menu_title'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'breadcrumb_title' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'content_type'     => new sfValidatorString(array('required' => false)),
      'created_on'       => new sfValidatorDateTime(array('required' => false)),
      'last_modified_on' => new sfValidatorDateTime(array('required' => false)),
      'published'        => new sfValidatorInteger(array('required' => false)),
      'menu_index'       => new sfValidatorInteger(array('required' => false)),
      'url'              => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('content[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Content';
  }

}
