<?php

/**
 * DefaultForms form base class.
 *
 * @method DefaultForms getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseDefaultFormsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                      => new sfWidgetFormInputHidden(),
      'occupancy_form_id'       => new sfWidgetFormInputText(),
      'application_form_id'     => new sfWidgetFormInputText(),
      'contact_form_id'         => new sfWidgetFormInputText(),
      'feedback_form_id'        => new sfWidgetFormInputText(),
      'profile_form_id'         => new sfWidgetFormInputText(),
      'structural_form_id'      => new sfWidgetFormInputText(),
      'payment_form_id'         => new sfWidgetFormInputText(),
      'additional_docs_form_id' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                      => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'occupancy_form_id'       => new sfValidatorInteger(array('required' => false)),
      'application_form_id'     => new sfValidatorInteger(array('required' => false)),
      'contact_form_id'         => new sfValidatorInteger(array('required' => false)),
      'feedback_form_id'        => new sfValidatorInteger(array('required' => false)),
      'profile_form_id'         => new sfValidatorInteger(array('required' => false)),
      'structural_form_id'      => new sfValidatorInteger(array('required' => false)),
      'payment_form_id'         => new sfValidatorInteger(array('required' => false)),
      'additional_docs_form_id' => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('default_forms[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'DefaultForms';
  }

}
