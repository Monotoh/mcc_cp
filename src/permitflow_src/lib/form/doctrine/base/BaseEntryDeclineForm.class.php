<?php

/**
 * EntryDecline form base class.
 *
 * @method EntryDecline getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseEntryDeclineForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'entry_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('User'), 'add_empty' => true)),
      'description' => new sfWidgetFormTextarea(),
       'declined_by' => new sfWidgetFormTextarea(),
        'created_at'  => new sfWidgetFormDateTime(),
      'updated_at'  => new sfWidgetFormDateTime(),
        //OTB patch - Attachment option
      'attachment'             => new sfWidgetFormInputFileEditable(array(
                                'label' => 'Browse File',
                                'file_src' => sfConfig::get('sf_upload_dir').'/'.$this->getObject()->getBanner(),
                                'is_image' => false,
                                'edit_mode' => !$this->getObject()->isNew(),
                                'delete_label' => 'Delete?',
                                'template' => '<div>%file%<br />%input%<br />%delete%%delete_label%</div>',
                                'with_delete' => true )) ,
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'entry_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('User'), 'required' => false)),
      'description' => new sfValidatorString(array('max_length' => 4000, 'required' => false)),
       'declined_by' => new sfValidatorString(array('max_length' => 4000, 'required' => false)), 
      'created_at'  => new sfValidatorDateTime(),
      'updated_at'  => new sfValidatorDateTime(),
        
    ));
     //OTB patch - Uploading of files
     $this->validatorSchema['attachment'] = new sfValidatorFile(array(
                                     'required' => true,
                                     'path' => sfConfig::get('sf_upload_dir').'/' ,
                                    // 'validated_file_class' => 'CustomValidatedFile',
                                     'mime_types' => 'web_images,pdf,zip,doc,docx',
                                  ),  array('invalid' => 'Invalid file.','required' => 'Select a file to upload.',
                                      'mime_types' => 'Attach only images,pdf,docx or zip file types')) ;
    $this->widgetSchema->setNameFormat('entry_decline[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EntryDecline';
  }

}
