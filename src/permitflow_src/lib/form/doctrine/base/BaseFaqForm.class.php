<?php

/**
 * Faq form base class.
 *
 * @method Faq getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseFaqForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'question'   => new sfWidgetFormInputText(),
      'answer'     => new sfWidgetFormTextarea(),
      'published'  => new sfWidgetFormInputText(),
      'posted_by'  => new sfWidgetFormInputText(),
      'email'      => new sfWidgetFormInputText(),
      'created_on' => new sfWidgetFormDate(),
      'deleted'    => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'question'   => new sfValidatorString(array('max_length' => 250)),
      'answer'     => new sfValidatorString(array('required' => false)),
      'published'  => new sfValidatorInteger(array('required' => false)),
      'posted_by'  => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'email'      => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'created_on' => new sfValidatorDate(),
      'deleted'    => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('faq[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Faq';
  }

}
