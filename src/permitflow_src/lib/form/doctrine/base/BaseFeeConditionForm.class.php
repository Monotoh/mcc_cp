<?php

/**
 * FeeCondition form base class.
 *
 * @method FeeCondition getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseFeeConditionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'feeid'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Applicationfee'), 'add_empty' => true)),
      'fieldid'        => new sfWidgetFormInputText(),
      'conditiontype'  => new sfWidgetFormInputText(),
      'conditionvalue' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'feeid'          => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Applicationfee'), 'required' => false)),
      'fieldid'        => new sfValidatorInteger(array('required' => false)),
      'conditiontype'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'conditionvalue' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('fee_condition[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'FeeCondition';
  }

}
