<?php

/**
 * Fee form base class.
 *
 * @method Fee getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseFeeForm extends BaseFormDoctrine

{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'invoiceid' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Invoicetemplate'), 'add_empty' => true)),
      'fee_code'      => new sfWidgetFormInputText(),
      'description' => new sfWidgetFormTextarea(),
      'amount'      => new sfWidgetFormTextarea(),
	   //OTB Start Patch - For Implementing Finance Bills
      'percentage' => new sfWidgetFormInput(),
      'fee_type'      => new sfWidgetFormSelect(array('choices' => array('fixed' => 'Fixed Amount' , 'percentage' => 'Percentage of Base Field' ,'range' => 'Range Fixed' ,'range_percentage' => 'Range Percentage of Base Field','formula' => 'Mathematical Formula'))),
	  'base_field' => new sfWidgetFormChoice(array('choices' => array())),
      'minimum_fee'      => new sfWidgetFormInputText(),
	   //OTB End Patch - For Implementing Finance Bills
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'invoiceid' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Invoicetemplate'), 'required' => false)),
      'fee_code'      => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'description' => new sfValidatorString(array('max_length' => 1000)),
      'amount'      => new sfValidatorString(array('max_length' => 65536, 'required' => false)),
	   //OTB Start Patch - For Implementing Finance Bills
      'percentage' => new sfValidatorString(array('max_length' => 100,'required' => false)),
      'fee_type'      => new sfValidatorChoice(array('choices' => array('fixed', 'percentage','range','range_percentage','formula'))),
	  'base_field'	  => new CustomDynamicChoiceValidator(array('choices' => array(), 'required' => false)),
      'minimum_fee'      => new sfValidatorString(array('max_length' => 100, 'required' => false)),
	   //OTB End Patch - For Implementing Finance Bills
    ));

    $this->widgetSchema->setNameFormat('fee[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Fee';
  }

}
