<?php

/**
 * Feedback form base class.
 *
 * @method Feedback getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseFeedbackForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'subject'    => new sfWidgetFormInputText(),
      'type'       => new sfWidgetFormInputText(),
      'names'      => new sfWidgetFormInputText(),
      'email'      => new sfWidgetFormInputText(),
      'message'    => new sfWidgetFormTextarea(),
      'created_on' => new sfWidgetFormDate(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'subject'    => new sfValidatorString(array('max_length' => 60, 'required' => false)),
      'type'       => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'names'      => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'email'      => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'message'    => new sfValidatorString(array('required' => false)),
      'created_on' => new sfValidatorDate(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('feedback[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Feedback';
  }

}
