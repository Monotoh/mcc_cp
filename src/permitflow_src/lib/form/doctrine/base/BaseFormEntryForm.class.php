<?php

/**
 * FormEntry form base class.
 *
 * @method FormEntry getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseFormEntryForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'form_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Form'), 'add_empty' => true)),
      'entry_id'       => new sfWidgetFormInputText(),
      'user_id'        => new sfWidgetFormInputText(),
      'circulation_id' => new sfWidgetFormInputText(),
      'declined'       => new sfWidgetFormInputText(),
      'saved_permit'   => new sfWidgetFormTextarea(),
      'approved'       => new sfWidgetFormInputText(),
      'application_id' => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'form_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Form'), 'required' => false)),
      'entry_id'       => new sfValidatorInteger(array('required' => false)),
      'user_id'        => new sfValidatorInteger(array('required' => false)),
      'circulation_id' => new sfValidatorInteger(array('required' => false)),
      'declined'       => new sfValidatorInteger(array('required' => false)),
      'saved_permit'   => new sfValidatorString(array('required' => false)),
      'approved'       => new sfValidatorInteger(array('required' => false)),
      'application_id' => new sfValidatorString(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('form_entry[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'FormEntry';
  }

}
