<?php

/**
 * FormGroups form base class.
 *
 * @method FormGroups getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseFormGroupsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'group_id'          => new sfWidgetFormInputHidden(),
      'group_name'        => new sfWidgetFormTextarea(),
      'group_description' => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'group_id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('group_id')), 'empty_value' => $this->getObject()->get('group_id'), 'required' => false)),
      'group_name'        => new sfValidatorString(array('required' => false)),
      'group_description' => new sfValidatorString(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('form_groups[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'FormGroups';
  }

}
