<?php

/**
 * Geographical_Area form base class.
 *
 * @method Geographical_Area getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseGeographical_AreaForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'name'          => new sfWidgetFormInputText(),
      'plot_area_min' => new sfWidgetFormInputText(),
      'plot_area_max' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'          => new sfValidatorString(array('max_length' => 250, 'required' => false)),
      'plot_area_min' => new sfValidatorInteger(array('required' => false)),
      'plot_area_max' => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('geographical_area[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Geographical_Area';
  }

}
