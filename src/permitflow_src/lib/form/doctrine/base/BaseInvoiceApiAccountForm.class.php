<?php

/**
 * Invoice API Account form base class.
 *
 * @method Invoice API Account getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseInvoiceApiAccountForm extends BaseFormDoctrine
{
    public function setup()
    {
        $this->setWidgets(array(
            'id'         => new sfWidgetFormInputHidden(),
            'mda_name'    => new sfWidgetFormInputText(),
            'mda_branch'   => new sfWidgetFormInputText(),
            'api_key'      => new sfWidgetFormInputText(),
            'api_secret'      => new sfWidgetFormInputText(),
        ));

        $this->setValidators(array(
            'id'         => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
            'mda_name'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
            'mda_branch'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
            'api_key'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
            'api_secret'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
        ));

        $this->widgetSchema->setNameFormat('invoiceapiaccount[%s]');

        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

        $this->setupInheritance();

        parent::setup();
    }

    public function getModelName()
    {
        return 'InvoiceApiAccount';
    }

}
