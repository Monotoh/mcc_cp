<?php

/**
 * Permits form base class.
 *
 * @method Permits getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseInvoicetemplatesForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'title'           => new sfWidgetFormInputText(),
      'applicationform' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Application'), 'add_empty' => true)),
      'applicationstage' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('SubMenus'), 'add_empty' => true)),
      'content'         => new sfWidgetFormTextarea(),
      'max_duration'           => new sfWidgetFormInputText(),
      'due_duration'           => new sfWidgetFormInputText(),
      'invoice_number'           => new sfWidgetFormInputText(),
      'payment_type'           => new sfWidgetFormInputText(),
      'expiration_type'           => new sfWidgetFormInputText(),
      'use_application_number'             => new sfWidgetFormChoice(array('choices' =>array(0 =>'No',1 => 'Yes') )),//OTB Start - option to set invoice number to application number
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'title'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'applicationform' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Application'), 'required' => false)),
      'applicationstage' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('SubMenus'), 'required' => false)),
      'content'         => new sfValidatorString(array('max_length' => 40000, 'required' => false)),
      'max_duration'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'due_duration'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'invoice_number'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'payment_type'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'expiration_type'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'use_application_number'             => new sfValidatorNumber(array('max' => 1, 'required' => false)),//OTB Start - option to set invoice number to application number
    ));

    $this->widgetSchema->setNameFormat('invoicetemplates[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Invoicetemplates';
  }

}
