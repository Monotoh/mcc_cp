<?php

/**
 * LeaveRequest form base class.
 *
 * @method Activity getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseLeaveRequestForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'reviewer_id'          => new sfWidgetFormInputText(),
      'start_date'    => new sfWidgetFormInputText(),
      'end_date'           => new sfWidgetFormInputText(),
      'status' => new sfWidgetFormInputText(),
      'name' => new sfWidgetFormInputText(),
      'approved_by' => new sfWidgetFormInputText(),
      'approved_at' => new sfWidgetFormInputText(),
      'created_at' => new sfWidgetFormInputText(),
      'leave_type' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'reviewer_id'          => new sfValidatorInteger(array('required' => false)),
      'start_date'    => new sfValidatorDate(array('required' => true)),
      'end_date'           => new sfValidatorDate(array('required' => true)),
      'status' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'name' => new sfValidatorString(array('max_length' => 255, 'required' => true)),
      'approved_by' => new sfValidatorInteger(array('required' => false)),
      'approved_at'           => new sfValidatorDateTime(array('required' => false)),
      'created_at'           => new sfValidatorDateTime(array('required' => false)),
      'leave_type' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('leave_request[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'LeaveRequest';
  }

}
