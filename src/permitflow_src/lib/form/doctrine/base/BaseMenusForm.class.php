<?php

/**
 * Menus form base class.
 *
 * @method Menus getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseMenusForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'       => new sfWidgetFormInputHidden(),
      'title'    => new sfWidgetFormInputText(),
      'app_queuing'       => new sfWidgetFormChoice(array('choices' =>array('ASC' => 'Ascending','DESC' =>'Descending') )), //OTB patch add app queuing 
      'sms_sender'    => new sfWidgetFormInputText(),//OTB - sms sender ID
      'service_code'    => new sfWidgetFormInputText(),//OTB - service code
    ));

    $this->setValidators(array(
      'id'       => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'title'    => new sfValidatorString(),
      'app_queuing'    => new sfValidatorString(), //OTB patch add app queuing 
      'sms_sender'    => new sfValidatorString(),//OTB - sms sender ID
      'service_code'    => new sfValidatorString(),//OTB - service code
    ));

    $this->widgetSchema->setNameFormat('menus[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Menus';
  }

}
