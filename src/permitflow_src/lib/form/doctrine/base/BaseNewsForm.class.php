<?php

/**
 * News form base class.
 *
 * @method News getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseNewsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'title'      => new sfWidgetFormInputText(),
      'article'    => new sfWidgetFormTextarea(),
      'created_by' => new sfWidgetFormInputText(),
      'created_on' => new sfWidgetFormDate(),
      'published'  => new sfWidgetFormInputText(),
      'hits'       => new sfWidgetFormInputText(),
      'agency_id'  => new sfWidgetFormInputText(),
      'deleted'    => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'title'      => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'article'    => new sfValidatorString(array('required' => false)),
      'created_by' => new sfValidatorInteger(array('required' => false)),
      'created_on' => new sfValidatorDate(array('required' => false)),
      'published'  => new sfValidatorInteger(array('required' => false)),
      'hits'       => new sfValidatorInteger(array('required' => false)),
      'agency_id'  => new sfValidatorInteger(array('required' => false)),
      'deleted'    => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('news[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'News';
  }

}
