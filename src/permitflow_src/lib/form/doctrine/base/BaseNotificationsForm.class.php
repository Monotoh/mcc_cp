<?php

/**
 * Notifications form base class.
 *
 * @method Notifications getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseNotificationsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'submenu_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Submenu'), 'add_empty' => true)),
      'form_id'    => new sfWidgetFormInputText(),
      'autosend'   => new sfWidgetFormInputText(),
      'title'      => new sfWidgetFormInputText(),
      'content'    => new sfWidgetFormTextarea(),
      'sms'        => new sfWidgetFormTextarea(),
          //custom
      'title_reviewer'        => new sfWidgetFormInputText(),
      'content_reviewer'        => new sfWidgetFormTextarea(),
      'content_sms_reviewer'        => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'submenu_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Submenu'), 'required' => false)),
      'form_id'    => new sfValidatorInteger(array('required' => false)),
      'autosend'   => new sfValidatorInteger(array('required' => false)),
      'title'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'content'    => new sfValidatorString(array('required' => false)),
      'sms'        => new sfValidatorString(array('required' => false)),
          //custom
      'title_reviewer'        => new sfValidatorString(array('required' => false)),
      'content_reviewer'        => new sfValidatorString(array('required' => false)),
      'content_sms_reviewer'        => new sfValidatorString(array('required' => false)), 
    ));

    $this->widgetSchema->setNameFormat('notifications[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Notifications';
  }

}
