<?php

/**
 * OtherApprovalCondition form base class.
 *
 * @method OtherApprovalCondition getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseOtherApprovalConditionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'entry_id'           => new sfWidgetFormInputText(),
      'field_id'           => new sfWidgetFormInputText(),
      'approval_condition' => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'entry_id'           => new sfValidatorInteger(array('required' => false)),
      'field_id'           => new sfValidatorInteger(array('required' => false)),
      'approval_condition' => new sfValidatorString(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('other_approval_condition[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'OtherApprovalCondition';
  }

}
