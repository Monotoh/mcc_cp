<?php

/**
 * PermitsApproverInfo form base class.
 *
 * @method PermitsApproverInfo getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePermitApproverInfoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'approval_text'           => new sfWidgetFormTextarea(),
      'approval_signature'           => new sfWidgetFormInputText(),
      'title'           => new sfWidgetFormInputText(),
      'valid_from'           => new sfWidgetFormInputText(),
      'valid_to'           => new sfWidgetFormInputText(),
       'permit_list'      =>    new PermitAgencyChoice(array('model' => $this->getRelatedModelName('Permits'), 'add_empty' => true,'multiple' => true)),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'approval_text'           => new sfValidatorString(array('max_length' => 2000, 'required' => false)),
        'title'           => new sfValidatorString(array('max_length' => 2000, 'required' => false)),
      'approval_signature'           => new sfValidatorString(array('max_length' => 2000, 'required' => false)),
      'valid_from'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'valid_to'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
        'permit_list'       => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Permits', 'required' => false)),
        ));

    $this->widgetSchema->setNameFormat('permit_approver_info[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }
  
  
  
  public function getModelName()
  {
    return 'PermitApproverInfo';
  }
  
public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['permit_list']))
    {
      $this->setDefault('permit_list', $this->object->Permits->getPrimaryKeys());
    }

  }

  protected function doSave($con = null)
  {
    parent::doSave($con);
    $this->savePermitList($con);
  }

  public function savePermitList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['permit_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Permits->getPrimaryKeys();
    foreach($existing as $template_id){
        $q = Doctrine_Query::create()
                ->from('Permits p')
                ->where('p.id = ? ', $template_id);
        $template = $q->fetchOne();
        $template->setApproverInfoId(0);
        $template->save();
    }
    $values = $this->getValue('permit_list');
    foreach($values as $template_id){
        $q = Doctrine_Query::create()
                ->from('Permits p')
                ->where('p.id = ? ', $template_id);
        $template = $q->fetchOne();
        $template->setApproverInfoId($this->object->getId());
        $template->save();
    }
  }

}
