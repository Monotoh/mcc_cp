<?php

/**
 * PermitCheckerConfigForm form base class.
 * @author     OTB Africa
 */
abstract class BasePermitCheckerConfigForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'permit_template_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Permit'), 'add_empty' => true)),
      'reference_object'       => new sfWidgetFormChoice(array('choices' =>array(null=>'Choose reference object','form' => 'Application Form', 'invoice' =>'Invoice') )),
      'label_to_show'  => new sfWidgetFormInputText(),
      'value_to_show'  => new sfWidgetFormInputText(),
      'sequence_no' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'permit_template_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Permit'), 'required' => false)),
      'reference_object' => new sfValidatorString(array('max_length' => 256, 'required' => false)),
      'label_to_show'  => new sfValidatorString(array('max_length' => 250, 'required' => false)),
      'value_to_show'  => new sfValidatorString(array('max_length' => 250, 'required' => false)),
      'sequence_no'       => new sfValidatorNumber(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('permit_checker_config_details[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PermitCheckerConfig';
  }

}
