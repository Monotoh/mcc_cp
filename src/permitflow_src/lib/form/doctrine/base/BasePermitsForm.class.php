<?php

/**
 * Permits form base class.
 *
 * @method Permits getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePermitsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'title'           => new sfWidgetFormInputText(),
      'parttype'           => new sfWidgetFormInputText(),
      'page_type'           => new sfWidgetFormInputText(),
      'page_orientation'           => new sfWidgetFormInputText(),
      'applicationform' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Application'), 'add_empty' => true)),
      'applicationstage' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Stage'), 'add_empty' => true)),
      'content'         => new sfWidgetFormTextarea(),
      'footer'         => new sfWidgetFormTextarea(),
      'max_duration'           => new sfWidgetFormInputText(),
      'remote_url'           => new sfWidgetFormInputText(),
      'remote_field'           => new sfWidgetFormInputText(),
      'remote_username'           => new sfWidgetFormInputText(),
      'remote_password'           => new sfWidgetFormInputText(),
        'remote_request_type'           => new sfWidgetFormInputText(),
      'user_set_duration' => new sfWidgetFormChoice(array('choices' =>array(0 =>'No',1 => 'Yes') )),//OTB Start - Option for authorized user to set max duration for each permit instance
      'stage_set_duration' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Stage'), 'add_empty' => true)),//OTB Start - Option for authorized user to set max duration for each permit instance
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'title'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'parttype'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'page_type'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'page_orientation'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'applicationform' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Application'), 'required' => false)),
      'applicationstage' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Stage'), 'required' => false)),
      'content'         => new sfValidatorString(array('max_length' => 80000, 'required' => false)),
      'footer'         => new sfValidatorString(array('max_length' => 40000, 'required' => false)),
      'max_duration'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'remote_url'           => new sfValidatorString(array('max_length' => 2000, 'required' => false)),
      'remote_field'           => new sfValidatorString(array('max_length' => 40000, 'required' => false)),
      'remote_username'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'remote_password'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
        'remote_request_type'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'user_set_duration'             => new sfValidatorNumber(array('max' => 1, 'required' => false)),//OTB Start - Option for authorized user to set max duration for each permit instance
      'stage_set_duration' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Stage'), 'required' => false)),//OTB Start - Option for authorized user to set max duration for each permit instance
    ));

    $this->widgetSchema->setNameFormat('permits[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Permits';
  }

}
