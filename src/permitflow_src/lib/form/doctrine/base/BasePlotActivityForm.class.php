<?php

/**
 * PlotActivity form base class.
 *
 * @method PlotActivity getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePlotActivityForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'       => new sfWidgetFormInputHidden(),
      'plot_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Plot'), 'add_empty' => true)),
      'entry_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Application'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'id'       => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'plot_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Plot'), 'required' => false)),
      'entry_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Application'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('plot_activity[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlotActivity';
  }

}
