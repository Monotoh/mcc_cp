<?php

/**
 * PlotFeeMatrix form base class.
 *
 * @method PlotFeeMatrix getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePlotFeeMatrixForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'application_type' => new sfWidgetFormInputText(),
      'building_type'    => new sfWidgetFormInputText(),
      'min'              => new sfWidgetFormInputText(),
      'max'              => new sfWidgetFormInputText(),
      'fee'              => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'application_type' => new sfValidatorInteger(array('required' => false)),
      'building_type'    => new sfValidatorInteger(array('required' => false)),
      'min'              => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'max'              => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'fee'              => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('plot_fee_matrix[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlotFeeMatrix';
  }

}
