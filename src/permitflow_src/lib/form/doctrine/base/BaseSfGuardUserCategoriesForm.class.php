<?php

/**
 * SfGuardUserCategories form base class.
 *
 * @method SfGuardUserCategories getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseSfGuardUserCategoriesForm extends BaseFormDoctrine
{
  public function setup()
  {
    $forms = "";
    $orders = "";
    $q = Doctrine_Query::create()
       ->from("ApForms a")
       ->orderBy("a.form_name ASC");
    $apforms = $q->execute();
    foreach($apforms as $form)
    {
      if($form->getFormType())
      {
        //application forms or comment sheets (ignore)
      }
      else
      {
        $forms[$form->getFormId()] = $form->getFormName();
      }
    }
    
    $q = Doctrine_Query::create()
       ->from("SfGuardUserCategories a");
    $cats = $q->execute();
    $count = 1;
    foreach($cats as $cat)
    {
    	$orders[$count] = $count;
    	$count++;
    }
    	$orders[$count] = $count;
    
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'formid'      => new sfWidgetFormChoice(array('choices' => $forms)),
      'name'        => new sfWidgetFormInputText(),
      'description' => new sfWidgetFormInputText(),
      'orderid'     => new sfWidgetFormChoice(array('choices' => $orders)),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'formid'      => new sfValidatorInteger(array('required' => true)),
      'name'        => new sfValidatorString(array('max_length' => 250, 'required' => true)),
      'description' => new sfValidatorString(array('max_length' => 250, 'required' => true)),
      'orderid'     => new sfValidatorInteger(),
    ));

    $this->widgetSchema->setNameFormat('sf_guard_user_categories[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SfGuardUserCategories';
  }

}
