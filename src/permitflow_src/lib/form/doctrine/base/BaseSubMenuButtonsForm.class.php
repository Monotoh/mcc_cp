<?php

/**
 * SubMenuButtons form base class.
 *
 * @method SubMenuButtons getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseSubMenuButtonsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'sub_menu_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Submenu'), 'add_empty' => false)),
      'button_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Button'), 'add_empty' => false)),
      'order_no'    => new sfWidgetFormInputText(),
      'backend'     => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'sub_menu_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Submenu'))),
      'button_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Button'))),
      'order_no'    => new sfValidatorInteger(),
      'backend'     => new sfValidatorInteger(),
    ));

    $this->widgetSchema->setNameFormat('sub_menu_buttons[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SubMenuButtons';
  }

}
