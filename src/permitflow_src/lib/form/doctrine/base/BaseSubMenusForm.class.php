<?php

/**
 * SubMenus form base class.
 *
 * @method SubMenus getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseSubMenusForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'max_duration'      => new sfWidgetFormInputText(),
      'title'             => new sfWidgetFormTextarea(),
      'menu_id'           => new sfWidgetFormInputText(),
      'change_identifier' => new sfWidgetFormInputText(),
      'deleted'           => new sfWidgetFormInputText(),
      'hide_comments'           => new sfWidgetFormInputText(),
      'allow_edit'           => new sfWidgetFormInputText() ,
      'extra_type'           => new sfWidgetFormChoice(array('choices' =>array('0' => 'Default','1' => 'Inspection','2' =>'Occupation') )) , // OTB patch, Extra type for further customizations
      'app_queuing'       => new sfWidgetFormChoice(array('choices' =>array('default' => 'Default','ASC' => 'Ascending','DESC' =>'Descending') )) //OTB patch add app queuing   
        
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'max_duration'      => new sfValidatorInteger(array('required' => false)),
      'title'             => new sfValidatorString(),
      'menu_id'           => new sfValidatorInteger(array('required' => false)),
      'change_identifier' => new sfValidatorInteger(array('required' => false)),
      'extra_type' => new sfValidatorInteger(array('required' => false)),
      'deleted'           => new sfValidatorInteger(array('required' => false)),
      'hide_comments'           => new sfValidatorInteger(array('required' => false)),
      'allow_edit'           => new sfValidatorInteger(array('required' => false)) ,
      'app_queuing'    => new sfValidatorString() //OTB patch add app queuing   
    ));

    $this->widgetSchema->setNameFormat('sub_menus[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SubMenus';
  }

  public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['buttonsubs_list']))
    {
      $this->setDefault('buttonsubs_list', $this->object->Buttonsubs->getPrimaryKeys());
    }

  }

  protected function doSave($con = null)
  {
    $this->saveButtonsubsList($con);

    parent::doSave($con);
  }

  public function saveButtonsubsList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['buttonsubs_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Buttonsubs->getPrimaryKeys();
    $values = $this->getValue('buttonsubs_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Buttonsubs', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Buttonsubs', array_values($link));
    }
  }

}
