<?php

/**
 * Task form base class.
 *
 * @method Task getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseTaskForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'type'             => new sfWidgetFormInputText(),
      'application_id'   => new sfWidgetFormInputText(),
      'creator_user_id'  => new sfWidgetFormInputText(),
      'owner_user_id'    => new sfWidgetFormInputText(),
      'duration'         => new sfWidgetFormInputText(),
      'start_date'       => new sfWidgetFormTextarea(),
      'end_date'         => new sfWidgetFormTextarea(),
      'priority'         => new sfWidgetFormInputText(),
      'is_transferrable' => new sfWidgetFormInputText(),
      'active'           => new sfWidgetFormInputText(),
      'status'           => new sfWidgetFormInputText(),
      'last_update'      => new sfWidgetFormTextarea(),
      'date_created'     => new sfWidgetFormTextarea(),
      'remarks'          => new sfWidgetFormTextarea(),
        //Skipped by - OTB patch
       'skipped_by'  => new sfWidgetFormInputText(),
       'site_visit_status'  => new sfWidgetFormInputText(),// site visit
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'type'             => new sfValidatorInteger(array('required' => false)),
      'application_id'   => new sfValidatorInteger(array('required' => false)),
      'creator_user_id'  => new sfValidatorInteger(array('required' => false)),
      'owner_user_id'    => new sfValidatorInteger(array('required' => false)),
      'duration'         => new sfValidatorInteger(array('required' => false)),
      'start_date'       => new sfValidatorString(array('required' => false)),
      'end_date'         => new sfValidatorString(array('required' => false)),
      'priority'         => new sfValidatorInteger(array('required' => false)),
      'is_transferrable' => new sfValidatorInteger(array('required' => false)),
      'active'           => new sfValidatorInteger(array('required' => false)),
      'status'           => new sfValidatorInteger(array('required' => false)),
      'last_update'      => new sfValidatorString(array('required' => false)),
      'date_created'     => new sfValidatorString(array('required' => false)),
      'remarks'          => new sfValidatorString(array('required' => false)),
        //skipped by - OTB patch
        'skipped_by'    => new sfValidatorInteger(array('required' => false)),
        //site visit OTB patch
        'site_visit_status'    => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('task[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Task';
  }

}
