<?php

/**
 * sfGuardUserProfile form base class.
 *
 * @method sfGuardUserProfile getObject() Returns the current form's model object
 *
 * @package    permit
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasesfGuardUserProfileForm extends BaseFormDoctrine

{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'user_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Profilita'), 'add_empty' => false)),
      'email'      => new sfWidgetFormInputText(),
      'fullname'   => new sfWidgetFormInputText(),
      'validate'   => new sfWidgetFormInputText(),
      'mobile'   => new sfWidgetFormInputText(),
      'registeras'   => new sfWidgetFormInputText(),
      'created_at' => new sfWidgetFormDateTime(),
      'updated_at' => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'user_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Profilita'))),
      'email'      => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'fullname'   => new sfValidatorString(array('max_length' => 80, 'required' => false)),
      'validate'   => new sfValidatorString(array('max_length' => 17, 'required' => false)),
      'mobile'   => new sfValidatorString(array('max_length' => 17, 'required' => false)),
      'registeras'   => new sfValidatorString(array('max_length' => 17, 'required' => false)),
      'created_at' => new sfValidatorDateTime(),
      'updated_at' => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('sf_guard_user_profile[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'sfGuardUserProfile';
  }

}
