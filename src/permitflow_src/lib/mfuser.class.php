<?php
class mfuser extends sfGuardSecurityUser
{
	public function mfHasCredential($credential)
	{
		$q = Doctrine_Query::create()
			->from('mfGuardUserGroup a')
			->leftJoin('a.Group b')
			->leftJoin('b.mfGuardGroupPermission c') //Left Join group permissions
			->leftJoin('c.Permission d') //Left Join permissions
			->where('a.user_id = ?', $this->getAttribute('userid'))
			->andWhere('d.name = ?', $credential);
		$usergroups = $q->execute();
		if(sizeof($usergroups) > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function hasMfGroup($id, $user_id)
	{
		$q = Doctrine_Query::create()
			->from('mfGuardUserGroup a')
			->where('a.user_id = ?', $user_id)
			->andWhere('a.group_id = ?', $id);
		$usergroups = $q->execute();
		if(sizeof($usergroups) > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
?>