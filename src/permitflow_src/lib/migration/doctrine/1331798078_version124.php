<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version124 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->dropForeignKey('sub_menus', 'sub_menus_menu_id_menus_id');
        $this->dropForeignKey('sf_guard_user_profile', 'sf_guard_user_profile_user_id_sf_guard_user_id');
        $this->createForeignKey('sf_guard_user_profile', 'sf_guard_user_profile_user_id_sf_guard_user_id_1', array(
             'name' => 'sf_guard_user_profile_user_id_sf_guard_user_id_1',
             'local' => 'user_id',
             'foreign' => 'id',
             'foreignTable' => 'sf_guard_user',
             'onUpdate' => '',
             'onDelete' => 'cascade',
             ));
        $this->addIndex('sf_guard_user_profile', 'sf_guard_user_profile_user_id', array(
             'fields' => 
             array(
              0 => 'user_id',
             ),
             ));
    }

    public function down()
    {
        $this->createForeignKey('sub_menus', 'sub_menus_menu_id_menus_id', array(
             'name' => 'sub_menus_menu_id_menus_id',
             'local' => 'menu_id',
             'foreign' => 'id',
             'foreignTable' => 'menus',
             'onUpdate' => '',
             'onDelete' => 'CASCADE',
             ));
        $this->createForeignKey('sf_guard_user_profile', 'sf_guard_user_profile_user_id_sf_guard_user_id', array(
             'name' => 'sf_guard_user_profile_user_id_sf_guard_user_id',
             'local' => 'user_id',
             'foreign' => 'id',
             'foreignTable' => 'sf_guard_user',
             'onUpdate' => '',
             'onDelete' => 'CASCADE',
             ));
        $this->dropForeignKey('sf_guard_user_profile', 'sf_guard_user_profile_user_id_sf_guard_user_id_1');
        $this->removeIndex('sf_guard_user_profile', 'sf_guard_user_profile_user_id', array(
             'fields' => 
             array(
              0 => 'user_id',
             ),
             ));
    }
}