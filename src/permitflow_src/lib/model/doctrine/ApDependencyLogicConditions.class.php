<?php
class ApDependencyLogicConditions extends BaseApDependencyLogicConditions
{
	public function save(Doctrine_Connection $conn = null)
	{
		$this->setTargetElementId($this->getDependencyLogic()->getElementId());
		$this->setFormId($this->getDependencyLogic()->getFormId());
		//$this->setElementName("element_".$this->getElementName());
		return parent::save($conn);
	}
}
?>