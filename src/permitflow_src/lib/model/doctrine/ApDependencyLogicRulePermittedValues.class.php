<?php
class ApDependencyLogicRulePermittedValues extends BaseApDependencyLogicRulePermittedValues
{
	public function save(Doctrine_Connection $conn = null)
	{
		$this->setFormId($this->getDependencyLogic()->getFormId());
		return parent::save($conn);
	}
}
?>