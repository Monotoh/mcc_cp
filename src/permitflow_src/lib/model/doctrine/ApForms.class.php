<?php

/**
 * ApForms
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    permit
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class ApForms extends BaseApForms
{
    public function __toString()
    {
        return $this->getFormName();
    }

	public function getFormName()
	{
		$translation = new Translation();
		if($translation->getTranslation('ap_forms','form_name',$this->getFormId()))
        {
          	return $translation->getTranslation('ap_forms','form_name',$this->getFormId());
        }
        else
        {
        	return parent::_get("form_name");
        }
	}

	public function getFormDescription()
	{
		$translation = new Translation();
		if($translation->getTranslation('ap_forms','form_description',$this->getFormId()))
        {
          	return $translation->getTranslation('ap_forms','form_description',$this->getFormId());
        }
        else
        {
        	return parent::_get("form_description");
        }
	}
}