<?php

/**
 * ApprovalConditionTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class ApprovalConditionTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object ApprovalConditionTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('ApprovalCondition');
    }
}