<?php

/**
 * ConditionsOfApproval
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    permit
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class ConditionsOfApproval extends BaseConditionsOfApproval
{
	public function save(Doctrine_Connection $conn = null)
	{
		parent::save($conn);

		$translation = new Translation();
		$translation->setTranslation('conditions_of_approval','short_name',parent::_get("id"), parent::_get("short_name"));

		$translation = new Translation();
		$translation->setTranslation('conditions_of_approval','description',parent::_get("id"), parent::_get("description"));
	}

	public function getShortName()
	{
		$translation = new Translation();
		if($translation->getTranslation('conditions_of_approval','short_name',$this->getId()))
        {
          	return $translation->getTranslation('conditions_of_approval','short_name',$this->getId());
        }
        else
        {
        	return parent::_get("short_name");
        }
	}

	public function getDescription()
	{
		$translation = new Translation();
		if($translation->getTranslation('conditions_of_approval','description',$this->getId()))
        {
          	return $translation->getTranslation('conditions_of_approval','description',$this->getId());
        }
        else
        {
        	return parent::_get("description");
        }
	}
}