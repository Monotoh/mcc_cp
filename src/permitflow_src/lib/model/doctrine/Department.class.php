<?php

/**
 * Department
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    permit
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class Department extends BaseDepartment
{
	public function save(Doctrine_Connection $conn = null)
	{
		parent::save($conn);

		$translation = new Translation();
		$translation->setTranslation('department','department_name',parent::_get("id"), parent::_get("department_name"));
	}

	public function getDepartmentName()
	{
		$translation = new Translation();
		if($translation->getTranslation('department','department_name',$this->getId()))
        {
          	return $translation->getTranslation('department','department_name',$this->getId());
        }
        else
        {
        	return parent::_get("department_name");
        }
	}
	//OTB Start - Multi Agency Implementation
	public function __toString()
	{
		return $this->getDepartmentName();
	}
	//OTB End - Multi Agency Implementation
}