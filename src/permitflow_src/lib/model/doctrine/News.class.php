<?php

/**
 * News
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    permit
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class News extends BaseNews
{
	public function save(Doctrine_Connection $conn = null)
	{
		parent::save($conn);

		$translation = new Translation();
		$translation->setTranslation('news','title',parent::_get("id"), parent::_get("title"));

		$translation = new Translation();
		$translation->setTranslation('news','article',parent::_get("id"), parent::_get("article"));
	}

	public function getTitle()
	{
		$translation = new Translation();
		if($translation->getTranslation('news','title',$this->getId()))
        {
          	return $translation->getTranslation('news','title',$this->getId());
        }
        else
        {
        	return parent::_get("title");
        }
	}

	public function getArticle()
	{
		$translation = new Translation();
		if($translation->getTranslation('news','article',$this->getId()))
        {
          	return $translation->getTranslation('news','article',$this->getId());
        }
        else
        {
        	return parent::_get("article");
        }
	}
}