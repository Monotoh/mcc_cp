<?php

/**
 * Plot
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    permit
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class Plot extends BasePlot
{
	public function save(Doctrine_Connection $conn = null)
	{
		parent::save($conn);

		$translation = new Translation();
		$translation->setTranslation('plot','plot_location',parent::_get("id"), parent::_get("plot_location"));
	}

	public function getPlotLocation()
	{
		$translation = new Translation();
		if($translation->getTranslation('plot','plot_location',$this->getId()))
        {
          	return $translation->getTranslation('plot','plot_location',$this->getId());
        }
        else
        {
        	return parent::_get("plot_location");
        }
	}
}