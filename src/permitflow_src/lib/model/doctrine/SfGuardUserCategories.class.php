<?php

/**
 * SfGuardUserCategories
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    permit
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class SfGuardUserCategories extends BaseSfGuardUserCategories
{
	public function save(Doctrine_Connection $conn = null)
	{
		parent::save($conn);

		$translation = new Translation();
		$translation->setTranslation('sf_guard_user_categories','name',parent::_get("id"), parent::_get("name"));

		$translation = new Translation();
		$translation->setTranslation('sf_guard_user_categories','description',parent::_get("id"), parent::_get("description"));
	}

	public function getName()
	{
		$translation = new Translation();
		if($translation->getTranslation('sf_guard_user_categories','name',$this->getId()))
        {
          	return $translation->getTranslation('sf_guard_user_categories','name',$this->getId());
        }
        else
        {
        	return parent::_get("name");
        }
	}

	public function getDescription()
	{
		$translation = new Translation();
		if($translation->getTranslation('sf_guard_user_categories','description',$this->getId()))
        {
          	return $translation->getTranslation('sf_guard_user_categories','description',$this->getId());
        }
        else
        {
        	return parent::_get("description");
        }
	}
}
