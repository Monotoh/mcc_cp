<?php

/**
 * TaskQueueTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class TaskQueueTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object TaskQueueTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('TaskQueue');
    }
}