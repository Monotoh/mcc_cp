<?php


class WorkflowCategory extends BaseWorkflowCategory
{
    public function __toString()
    {
        return $this->getTitle();
    }
}