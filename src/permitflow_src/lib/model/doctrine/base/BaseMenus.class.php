<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('Menus', 'doctrine');

/**
 * BaseMenus
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $title
 * @property integer $order_no
 * 
 * @method integer getId()       Returns the current record's "id" value
 * @method string  getTitle()    Returns the current record's "title" value
 * @method integer getOrderNo()  Returns the current record's "order_no" value
 * @method Menus   setId()       Sets the current record's "id" value
 * @method Menus   setTitle()    Sets the current record's "title" value
 * @method Menus   setOrderNo()  Sets the current record's "order_no" value
 * 
 * @package    permit
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseMenus extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('menus');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => true,
             'length' => 4,
             ));
        $this->hasColumn('title', 'string', null, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => '',
             ));
        //OTB patch add app queuing
         $this->hasColumn('app_queuing', 'string', 256, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 256,
             ));
		//Start OTB workflow category
        $this->hasColumn('category_id', 'integer', 4, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
		//End OTB workflow category
		//Start OTB SMS Sender ID
         $this->hasColumn('sms_sender', 'string', 256, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 256,
             ));
		//End OTB SMS Sender ID
		//Start OTB Service Code
         $this->hasColumn('service_code', 'string', 256, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 256,
             ));
		//End OTB Service Code
        $this->hasColumn('order_no', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 4,
             'default' => 0,
             ));
    }

    public function setUp()
    {
        parent::setUp();

		//OTB Start - Multi Agency Implementation
        $this->hasMany('AgencyMenu', array(
             'local' => 'id',
             'foreign' => 'menu_id'));
		//OTB End - Multi Agency Implementation
		//OTB workflow category
        $this->hasOne('WorkflowCategory', array(
             'local' => 'category_id',
             'foreign' => 'id'));
		//OTB END
		//OTB Start
		$this->hasMany('SubMenus',array(
			'local' => 'id',
			'foreign' => 'menu_id'
		));
		//OTB END
    }
}
