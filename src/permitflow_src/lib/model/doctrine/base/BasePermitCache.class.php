<?php
Doctrine_Manager::getInstance()->bindComponent('PermitCache', 'doctrine');

abstract class BasePermitCache extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('permit_cache');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => true,
             'length' => 4,
             ));
        $this->hasColumn('application_id', 'integer', 4, array(
            'type' => 'integer',
            'fixed' => 0,
            'unsigned' => false,
            'primary' => false,
            'notnull' => false,
            'autoincrement' => false,
            'length' => 4,
        ));
        $this->hasColumn('template_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 4,
           ));
        $this->hasColumn('period_uom', 'string', 50, array(
            'type' => 'string',
            'fixed' => 0,
            'unsigned' => false,
            'primary' => false,
            'default' => '',
            'notnull' => true,
            'autoincrement' => false,
            'length' => 50,
        ));
        $this->hasColumn('validity_period', 'integer', 4, array(
            'type' => 'integer',
            'fixed' => 0,
            'unsigned' => false,
            'primary' => false,
            'notnull' => false,
            'autoincrement' => false,
            'length' => 4,
        ));
        $this->hasColumn('status', 'string', 50, array(
            'type' => 'string',
            'fixed' => 0,
            'unsigned' => false,
            'primary' => false,
            'default' => '',
            'notnull' => true,
            'autoincrement' => false,
            'length' => 50,
        ));
        $this->hasColumn('sp_condition', 'string', 50, array(
            'type' => 'string',
            'fixed' => 0,
            'unsigned' => false,
            'primary' => false,
            'default' => '',
            'notnull' => true,
            'autoincrement' => false,
            'length' => 50,
        ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('FormEntry as Application', array(
             'local' => 'application_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));
       $this->hasOne('Permits as PermitTemplate', array(
            'local' => 'template_id',
            'foreign' => 'id',
            'onDelete' => 'CASCADE'));
    }
}
