<?php

abstract class BasePermitCheckerConfig extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('permit_checker_config');
        $this->hasColumn('permit_template_id', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('reference_object', 'string', 250, array(
             'type' => 'string',
             'length' => 250,
             ));
        $this->hasColumn('label_to_show', 'string', 250, array(
             'type' => 'string',
             'length' => 250,
             ));
        $this->hasColumn('value_to_show', 'string', 250, array(
             'type' => 'string',
             'length' => 250,
             ));
        $this->hasColumn('sequence_no', 'integer', null, array(
             'type' => 'integer',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Permits as Permit', array(
             'local' => 'permit_template_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));
    }
}