<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('Publication', 'doctrine');

/**
 * BasePublication
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $title
 * @property string $article
 * @property integer $created_by
 * @property date $created_on
 * @property integer $published
 * @property integer $hits
 * @property integer $agency_id
 * @property integer $deleted
 * 
 * @method integer     getId()         Returns the current record's "id" value
 * @method string      getTitle()      Returns the current record's "title" value
 * @method string      getArticle()    Returns the current record's "article" value
 * @method integer     getCreatedBy()  Returns the current record's "created_by" value
 * @method date        getCreatedOn()  Returns the current record's "created_on" value
 * @method integer     getPublished()  Returns the current record's "published" value
 * @method integer     getHits()       Returns the current record's "hits" value
 * @method integer     getAgencyId()   Returns the current record's "agency_id" value
 * @method integer     getDeleted()    Returns the current record's "deleted" value
 * @method Publication setId()         Sets the current record's "id" value
 * @method Publication setTitle()      Sets the current record's "title" value
 * @method Publication setArticle()    Sets the current record's "article" value
 * @method Publication setCreatedBy()  Sets the current record's "created_by" value
 * @method Publication setCreatedOn()  Sets the current record's "created_on" value
 * @method Publication setPublished()  Sets the current record's "published" value
 * @method Publication setHits()       Sets the current record's "hits" value
 * @method Publication setAgencyId()   Sets the current record's "agency_id" value
 * @method Publication setDeleted()    Sets the current record's "deleted" value
 * 
 * @package    permit
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BasePublication extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('publication');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => true,
             'length' => 4,
             ));
        $this->hasColumn('title', 'string', 100, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 100,
             ));
        $this->hasColumn('article', 'string', null, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => '',
             ));
        $this->hasColumn('created_by', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('created_on', 'date', 25, array(
             'type' => 'date',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 25,
             ));
        $this->hasColumn('published', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('hits', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '0',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('agency_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('deleted', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '0',
             'notnull' => false,
             'autoincrement' => false,
             'length' => 4,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        
    }
}