<?php

/**
 * BaseUploadReceipt
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $invoice_id
 * @property integer $form_id
 * @property integer $entry_id
 * @property mfInvoice $User
 * 
 * @method integer       getInvoiceId()  Returns the current record's "invoice_id" value
 * @method integer       getFormId()     Returns the current record's "form_id" value
 * @method integer       getEntryId()    Returns the current record's "entry_id" value
 * @method mfInvoice     getUser()       Returns the current record's "User" value
 * @method UploadReceipt setInvoiceId()  Sets the current record's "invoice_id" value
 * @method UploadReceipt setFormId()     Sets the current record's "form_id" value
 * @method UploadReceipt setEntryId()    Sets the current record's "entry_id" value
 * @method UploadReceipt setUser()       Sets the current record's "User" value
 * 
 * @package    permit
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseUploadReceipt extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('upload_receipt');
        $this->hasColumn('invoice_id', 'integer', 4, array(
             'type' => 'integer',
             'length' => 4,
             ));
        $this->hasColumn('form_id', 'integer', 4, array(
             'type' => 'integer',
             'length' => 4,
             ));
        $this->hasColumn('entry_id', 'integer', 4, array(
             'type' => 'integer',
             'length' => 4,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('mfInvoice as User', array(
             'local' => 'invoice_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));
    }
}