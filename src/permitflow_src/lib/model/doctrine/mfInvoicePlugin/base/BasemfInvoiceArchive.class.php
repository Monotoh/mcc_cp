<?php

/**
 * BasemfInvoice
 *
 * This class has been auto-generated by the Doctrine ORM Framework
 *
 * @property integer $app_id
 * @property string $invoice_number
 * @property integer $paid
 * @property FormEntry $User
 * @property Doctrine_Collection $mfInvoiceDetail
 * @property Doctrine_Collection $UploadReceipt
 *
 * @method integer             getAppId()           Returns the current record's "app_id" value
 * @method string              getInvoiceNumber()   Returns the current record's "invoice_number" value
 * @method integer             getPaid()            Returns the current record's "paid" value
 * @method FormEntry           getUser()            Returns the current record's "User" value
 * @method Doctrine_Collection getMfInvoiceDetail() Returns the current record's "mfInvoiceDetail" collection
 * @method Doctrine_Collection getUploadReceipt()   Returns the current record's "UploadReceipt" collection
 * @method mfInvoice           setAppId()           Sets the current record's "app_id" value
 * @method mfInvoice           setInvoiceNumber()   Sets the current record's "invoice_number" value
 * @method mfInvoice           setPaid()            Sets the current record's "paid" value
 * @method mfInvoice           setUser()            Sets the current record's "User" value
 * @method mfInvoice           setMfInvoiceDetail() Sets the current record's "mfInvoiceDetail" collection
 * @method mfInvoice           setUploadReceipt()   Sets the current record's "UploadReceipt" collection
 *
 * @package    permit
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BasemfInvoiceArchive extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('mf_invoice_archive');
        $this->hasColumn('app_id', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('invoice_number', 'string', 255, array(
             'type' => 'string',
             'length' => 255,
             ));
        $this->hasColumn('paid', 'integer', null, array(
             'type' => 'integer',
             'default' => '0',
           ));
        $this->hasColumn('created_at', 'timestamp', 25, array(
             'type' => 'timestamp',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 25,
             ));
        $this->hasColumn('updated_at', 'timestamp', 25, array(
             'type' => 'timestamp',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 25,
           ));
      $this->hasColumn('expires_at', 'timestamp', 25, array(
           'type' => 'timestamp',
           'fixed' => 0,
           'unsigned' => false,
           'primary' => false,
           'notnull' => true,
           'autoincrement' => false,
           'length' => 25,
         ));
        $this->hasColumn('mda_code', 'string', 255, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 255,
             ));
        $this->hasColumn('service_code', 'string', 255, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 255,
             ));
        $this->hasColumn('branch', 'string', 255, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 255,
             ));
        $this->hasColumn('due_date', 'string', 255, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 255,
             ));
        $this->hasColumn('payer_id', 'string', 255, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 255,
             ));
        $this->hasColumn('payer_name', 'string', 255, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 255,
             ));
        $this->hasColumn('total_amount', 'string', 255, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 255,
             ));
        $this->hasColumn('currency', 'string', 255, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 255,
             ));
        $this->hasColumn('doc_ref_number', 'string', 255, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 255,
             ));
        $this->hasColumn('template_id', 'integer', null, array(
             'type' => 'integer',
             'default' => '0',
           ));
        $this->hasColumn('document_key', 'string', 255, array(
            'type' => 'string',
            'fixed' => 0,
            'unsigned' => false,
            'primary' => false,
            'notnull' => false,
            'autoincrement' => false,
            'length' => 255,
        ));
       $this->hasColumn('remote_validate', 'string', 255, array(
           'type' => 'string',
           'fixed' => 0,
           'unsigned' => false,
           'primary' => false,
           'notnull' => false,
           'autoincrement' => false,
           'length' => 255,
       ));
        $this->hasColumn('pdf_path', 'string', 255, array(
            'type' => 'string',
            'fixed' => 0,
            'unsigned' => false,
            'primary' => false,
            'notnull' => false,
            'autoincrement' => false,
            'length' => 255,
        ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('FormEntryArchive', array(
             'local' => 'app_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $this->hasMany('mfInvoiceDetailArchive', array(
             'local' => 'id',
             'foreign' => 'invoice_id'));

        $this->hasMany('UploadReceipt', array(
             'local' => 'id',
             'foreign' => 'invoice_id'));

        $timestampable0 = new Doctrine_Template_Timestampable(array(
             ));
        $this->actAs($timestampable0);
    }
}
