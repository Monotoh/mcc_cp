#!/usr/bin/php
<?php
require_once __DIR__ . '/rabbitmqphp/autoload.php';
//
require_once 'notifications.class.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;

//instance of messaging class
$messaging = new mailnotifications() ;

error_log(' [*] Waiting for messages. To exit press CTRL+C');

$message = $argv[1];
$original = base64_decode($message);

  $received_data = json_decode($original);
  error_log(" [x] Received Email Worker".print_R($received_data,true));
  //
  $messaging->sendSwiftMail($message->from, $message->to, $message->organisation_name, $message->subject, $message->body) ;
  error_log(" [x] Done sending queued emails");

?>
