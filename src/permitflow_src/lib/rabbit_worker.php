#!/usr/bin/php
<?php
require_once __DIR__ . '/rabbitmqphp/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;


error_log(' [*] Waiting for messages. To exit press CTRL+C');

$message = $argv[1];
$original = base64_decode($message);

  $received_data = json_decode($original);
  error_log(" [x] Received ".print_R($received_data,true));
  //Comment out try catch so that task is requeued by rabbitmq_cli_consumer on error
  try {
    error_log(" [x] Try Send Invoice details ".print_R($received_data, true));//Although rabbitmq_cli_consumer will automatically requeue on error, we log the error
    sendInvoiceDetails($received_data->invoice_id, $received_data->payment_settings);
  }catch (Exception $ex){
        error_log (" [x] Send Invoice details failed ".$ex);//Although rabbitmq_cli_consumer will automatically requeue on error, we log the error
		usleep(600000000);//Sleep for about 10 minutes so that irembo service does not pick next task as an error
		$queue_data = array('invoice_id'=>$received_data->invoice_id, 'payment_settings'=>$received_data->payment_settings);
		$queuemanager = new QueueManager();
		$queuemanager->queue_data($queue_data);
  }
  error_log(" [x] Done");

    function sendInvoiceDetails($invoice_id, $payment_settings, $backend = false)
    {
		require_once(dirname(__FILE__).'/../config/ProjectConfiguration.class.php');
		require_once(dirname(__FILE__).'/vendor/symfony/lib/vendor/swiftmailer/swift_required.php');
		$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', 'prod', false);
		new sfDatabaseManager($configuration);

		$irembo_gateway = new IremboGateway();
        $invoice = $irembo_gateway->invoice_manager->get_invoice_by_id($invoice_id);

        $q = Doctrine_Query::create()
           ->from("ApFormPayments a")
           ->where("a.form_id = ? AND a.record_id = ?", array($invoice->getFormEntry()->getFormId(), $invoice->getFormEntry()->getEntryId()))
           ->andWhere("a.status <> ? or a.payment_status <> ?", array(2, 'paid'))
           ->andWhere("a.status <> ? or a.payment_status <> ?", array(3, 'cancelled'))
		   ->andWhere("a.invoice_id = ?", $invoice->getId())
		   ->orderBy('a.afp_id desc');
        $transaction = $q->fetchOne();

		$application = $invoice->getFormEntry();

		$user = Doctrine_Core::getTable('SfGuardUser')->find(array($application->getUserId()));
		$stage = Doctrine_Core::getTable('SubMenus')->find(array($invoice->getFormEntry()->getApproved()));

		$fullname = $user->getProfile()->getFullname();
		$idnumber = $user->getUsername();
		$email = $user->getProfile()->getEmail();

		//One of email or phonenumber is required
		$phonenumber = $user->getProfile()->getMobile();
		//Clean Phone Number
		$phonenumber = str_replace(" ", "", $phonenumber);
		$phonenumber = str_replace("+", "", $phonenumber);
		//Ensure cleaned Phone number is number
		if (ctype_digit($phonenumber)) {
		    $phonenumber = $phonenumber;
		} else {
		    $phonenumber="";
		}
		//Ensure cleaned Phone number is a Rwandan phone number. ROL service is strict on phone number validation
		if(substr($phonenumber,0,3) == "250"){
				$phonenumber = strlen($phonenumber) == 12 ? $phonenumber : "";
		}else{
				$phonenumber = strlen($phonenumber) == 10 ? $phonenumber : "";
		}

		//Get Payment Details
		$payment_description = $invoice->getFormEntry()->getForm()->getFormName().$invoice->getFormEntry()->getForm()->getFormDescription();
		$payment_amount = $invoice->getTotalAmount();
		if($stage){
			$districtCode = $stage->getMenus()->getServiceCode();
			//Start - some hard code for CoK to get district code based on district selected in form.
			if($districtCode == "COK"){
				$form_manager = new FormManager();
				$district_val = $form_manager->getElementValue($invoice->getFormEntry()->getForm()->getFormId(),$invoice->getFormEntry()->getEntryId(),81);
				if(strcasecmp ( $district_val , "gasabo" )==0){
					$districtCode = "0102";
				}else if(strcasecmp ( $district_val , "kicukiro" )==0){
					$districtCode = "0103";
				}else if(strcasecmp ( $district_val , "nyarugenge" )==0){
					$districtCode = "0101";
				}else{
					$districtCode = "0102";//Default to Gasabo if error occurs
				}
			}
			error_log("Irembo districtCode Requeust for Billing: ".$districtCode);
			//End - some hard code for CoK to get district code based on district selected in form.
		}
		//$payment_currency = $payment_settings->payment_currency;
		$payment_currency = "RWF";//Pass this as currency to irembo. FRW or any other value will through an error on Irembo

        if($transaction){//If transaction exists, then application details have already been sent to irembo, so we cancel previous one and make sure they pay for this transaction
			$transaction->setPaymentStatus("cancelled");
			$transaction->setStatus("3");
			$transaction->save();
		}

		if($payment_amount > 0){
			$client = new SoapClient("https://172.16.20.10/cxf/sendApplicationDetails/?wsdl");//Irembo Payment API
			$paymentDetails = new paymentDetails($payment_currency, $payment_amount);
			$notificationDetails = new notificationDetails($phonenumber, $email);
			$app_request = new ApplicationRequest("EPYMNT", $invoice->getInvoiceNumber(), "RHA", $districtCode, $paymentDetails, $notificationDetails, "RHA", "3eufueuc", "Test", "Test");
			error_log("Irembo Application Requeust for Billing: ".print_R($app_request, true));
			$response = $client->sendApplicationDetails($app_request);
			$transaction = $irembo_gateway->addTransactionDetails($invoice, $response->BillId, $fullname, $payment_amount, $payment_currency, 15);

			$rol_bill_number = $transaction->getPaymentId();
			$application_manager = new ApplicationManager();
			$subject = "Payment for Bill Id ".$rol_bill_number;
			$message = "Dear ".$fullname.", Kindly pay for your bill through irembo.gov.rw using Bill number ".$rol_bill_number.". Amount to pay: ".$payment_currency." ".$payment_amount.". Project reference number for this payment is: ".$application->getApplicationId();
			$application_manager->queue_notifications($application->getId(), $application->getFormId(), $application->getEntryId(), $application->getUserId(), False, $subject, $message,$stage->getMenus()->getSmsSender());

			return $transaction;
		}
	}
?>
