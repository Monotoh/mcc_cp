<?php

class fixApplicationReferenceTask extends sfBaseTask
{
  protected function configure()
  {
    // // add your own arguments here
    // $this->addArguments(array(
    //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
    // ));

    /*$this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
      // add your own options here
    ));*/

    $this->namespace        = 'permitflow';
    $this->name             = 'fixApplicationReference';
    $this->briefDescription = 'update the current stage of approval for an application';
    $this->detailedDescription = <<<EOF
The [fixApplicationReference|INFO] task does things.
Call it with:

  [php symfony fixApplicationReference|INFO]
EOF;
  }

  protected function execute($arguments = array(), $options = array())
  {
	  session_start();
  $_SESSION["SESSION_CUTEFLOW_USERID"]= "63"; //63 is the id of Mr/Mrs cronjob
	$_SERVER['REMOTE_ADDR']="cron ip";
	$_SERVER['HTTP_USER_AGENT']= "cron's agent";
	  
    // initialize the database connection
    $databaseManager = new sfDatabaseManager($this->configuration);
	
		$fetch_application = Doctrine_Query::create()
			 ->from('FormEntry a');
	     $current_application = $fetch_application->execute();
		// error_log(" CURRENT APPLICATION IS ".$current_application->getApproved()) ;
		 
		 $count_fixed=0;
		 $count_processed=0;
		 if($current_application)
		 { 
			foreach($current_application as $application){
			 
			
					$stage_id=$application->getApproved();
					$app_id=$application->getId();
					 
					 
				$q = Doctrine_Query::create()
					->from('ApplicationReference a')
					->where('a.application_id = ?', $app_id)
					->orderBy('a.id DESC')
					->limit(1);
					$oldappref = $q->fetchOne();
					
					if($oldappref){
					foreach($oldappref as $old){
					
					$ref_application_id= $oldappref->getApplicationId();
					$ref_stage_id= $oldappref->getStageId();
						
						
					}
					}
			
	if($ref_stage_id!=$stage_id){

      if($stage_id==13 || $stage_id==18){

        $q1 = Doctrine_Query::create()
					->from('Task t')
					->where('t.application_id = ?', $app_id)
					->orderBy('t.id DESC')
					->limit(1);
          $tasks = $q1->fetchOne();
          if($tasks){   
          foreach($tasks as $task){
            $start_date= $tasks->getStartDate();
          }

          }

      }

      else {
       // $start_date=date('Y-m-d');
       $start_date="";

      }
    
				echo "\n application: ".$app_id." ref: ".$ref_stage_id."  stage_id: ".$stage_id."\n";
					$appref = new ApplicationReference();
					$appref->setStageId($stage_id);
					$appref->setApplicationId($app_id);
					$appref->setApprovedBy($_SESSION['SESSION_CUTEFLOW_USERID']);
					$appref->setStartDate($start_date);
					$appref->setEndDate("");
					$appref->save(); 
					$count_fixed++;
				
			}
			
		$count_processed++;
			}
		}	
		echo "\n\n processed: ".$count_processed."  fixed: ".$count_fixed."\n\n";
		
		 echo "\r\n****************************ENDING A SESSION****************\r\n";
   
    // destroy the session 
    
    session_destroy();  
	}
}
