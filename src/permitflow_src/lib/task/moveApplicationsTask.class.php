<?php

class moveApplicationsTask extends sfBaseTask
{
  protected function configure()
  {
    // // add your own arguments here
    // $this->addArguments(array(
    //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
    // ));

    /*$this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
      // add your own options here
    ));*/

    $this->namespace = "permitflow";
    $this->name = "move-due-applications";
    $this->briefDescription    = 'move applications';
    $this->detailedDescription = <<<EOF
The [moveApplications|INFO] task does things.
Call it with:

  [php symfony moveApplications|INFO]
EOF;

	$this->addArgument('src_stage_id', sfCommandArgument::REQUIRED, 'Src Stage Id', null);
    $this->addArgument('dest_stage_id', sfCommandArgument::REQUIRED, 'Dest Stage Id', null);
	$this->addArgument('max_days', sfCommandArgument::REQUIRED, 'Max Days', null);


  }
  


  protected function execute($arguments = array(), $options = array())
  {
	  
	  
	  session_start();
    $_SESSION["SESSION_CUTEFLOW_USERID"]= "63"; //63 is the id of Mr/Mrs cronjob
	$_SERVER['REMOTE_ADDR']="cron ip";
	$_SERVER['HTTP_USER_AGENT']= "cron's agent";
    
    
    echo "\r\nThe session id is:----------> ".$_SESSION["SESSION_CUTEFLOW_USERID"]."\r\n";
   


	  $databaseManager = new sfDatabaseManager($this->configuration);
	
	

	 $this->logSection('permitflow', 'move applications to next stage');
        
        $app_manager = new ApplicationManager();
        $databaseManager = new sfDatabaseManager($this->configuration);// initialize the database connection
         //get src stage id passed
        $src_stage_id = $arguments['src_stage_id'] ;
        //get dest stage id passed
        $dest_stage_id = $arguments['dest_stage_id'] ;
        $max_days=$arguments['max_days'] ;
        error_log("Src Stage ".$src_stage_id);
        error_log("Dest Stage ".$dest_stage_id);
        error_log("Max Days ".$max_days);
       // $sql = "SELECT f.application_id,f.approved,f.id FROM form_entry f LEFT JOIN mf_invoice m ON f.id = m.app_id WHERE f.approved = $src_stage_id AND paid = 1 ";
        //sql2="select max duration from sub_menus where stage = order_no=$src_stage_id";
        $sql3 = "SELECT * from form_entry where approved=$src_stage_id";
        //$res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($sql);
        $res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($sql3);
      
        $otbhelper = new OTBHelper();
        $application_manager=new applicationManager();
        $notify = new mailnotifications();
		$count_checked=0;
		$count_moved=0;
        
		
		
		
		foreach($res as $r){
			$app_id=$r['id'];
              //error_log("move application to next stage >>>> ".$r['application_id'] );
			  //echo "HELLO COMMENTS REVIEW";
			  if($src_stage_id==13 || $src_stage_id==19){
               $tasks = Doctrine_Query::create()
               ->from('task t')
               ->where('t.application_id = ?', $app_id)
               ->orderBy('t.id DESC')
               ->limit(1);
			   
			  $date_provider = $tasks->fetchOne();
			  }
			  else if($src_stage_id==14){
				  
				  echo "HELLO COMMENTS REVIEW";
				  $q = Doctrine_Query::create()
					->from('ApplicationReference a')
					->where('a.application_id = ?', $app_id)
					->orderBy('a.id DESC')
					->limit(1);
				  $date_provider = $q->fetchOne();
			  }

        if($date_provider){
				   
				   if($src_stage_id==13 || $src_stage_id==19){
				   echo "\n\nTask: ".$date_provider->getId()." For: ".$app_id."\n";
				   }
				   else if($src_stage_id==14){
				   echo "\n\nReference: ".$date_provider->getId()." For: ".$app_id."\n";
					   
					   
				   }
                   //error_log("Debug: Move application >>> ".$applications_res->getId());
                   
                   //CBS patch, calculating number of days the application has spent in since task assigned
                        /*$now = time(); // current system time in seconds
						$start_date = strtotime($date_provider->getStartDate()); //in seconds*/
						
						
						$date_today=date("Y-m-d");
						$start_date=$date_provider->getStartDate();
                        
						echo "\n\nSTART DATE: ".$start_date."\n\nNOW DATE: ".$date_today."\n";
                        //$datediff = $now - $start_date; in seconds
                        //$spent_days=round($datediff / (60 * 60 * 24));//this one is for (working+non-working)days
						$current_year=date("Y");
						$holidays=array("$current_year-01-01","$current_year-03-11","$current_year-05-01","$current_year-05-25","$current_year-05-30","$current_year-07-17","$current_year-10-04","$current_year-12-25","$current_year-12-26" );
						//echo getWorkingDays("2008-12-22","2009-01-02",$holidays)
						$spent_days=$app_manager->getWorkingDays($start_date,$date_today,$holidays);
						
                        echo "\n\n".$date_provider->getApplicationId()." Spent days=".$spent_days."\n";

            
            
            
            //IF APPLICATION OVER DUE MOVE IT TO NEXT STAGE
            if($spent_days>$max_days){
                          echo "APPLICATION TASK OVER DUE\n\n";
						  
						 $q = Doctrine_Query::create()
                            ->from('Task a')
                            ->where('a.application_id = ?', $app_id)
                            ->andWhere('a.status = 1 OR a.status = 2 OR a.status = 3 OR a.status = 4 OR a.status = 5');
                    $tasks = $q->execute();
					
					//complete any pending tasks for this application
					echo "SIZE OF TASKS....... ".sizeof(tasks);
					
					if(sizeof($tasks && $tasks>0)){
						
						foreach($tasks as $task){
              echo "XXXXXXXXXXXXXXXXXXXXXXXXX CANCELLING TASK: ".$task->getId()." ON APPLICATION: ".$app_id."\n\n";
						//	$task->setStatus("55");
						//	$task->save();
							}
						}
							//$application_manager->application_onhold($r[id],$dest_stage_id);
							$count_moved++;
                            
                        }

               }

              $count_checked++; 
              echo "-----------------------------------------------------------------------------------------------------\n\n";
        }
		
			  echo "\nTotal processed: ".$count_checked."  Total moved: ".$count_moved."\n\n";
	
 echo "\r\n****************************ENDING A SESSION****************\r\n";
        session_unset();
    // destroy the session 
    
    session_destroy();  
	 
  }
  

  
  
}
