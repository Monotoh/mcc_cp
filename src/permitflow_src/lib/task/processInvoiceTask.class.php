<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class processInvoiceTask extends sfBaseTask {
    
     //put your code here
    public function configure()
    {
        $this->namespace = "permitflow";
        $this->name = "process-pending-invoices";
        $this->briefDescription    = 'Process Invoices';
 
        $this->detailedDescription = <<<EOF
The [permitflow:process-pending-invoices|INFO]  Send Invoices(pending) in Billing Stage to client with email notification.
 
  [./symfony permitflow:process-pending-invoices|INFO]

  src_stage_id : specify source stage id
  dest_stage_id : specify destination stage id
  example usage: php symfony permitflow:process-pending-invoices 11(src) 50(dest)
EOF;
            
       $this->addArgument('src_stage_id', sfCommandArgument::REQUIRED, 'Src Stage Id', null);
       $this->addArgument('dest_stage_id', sfCommandArgument::REQUIRED, 'Dest Stage Id', null);
       
    }
    
    public function execute($arguments = array(), $options = array()) {
        
        $this->logSection('permitflow', 'Send Bills to Clients');
        //
        $app_manager = new ApplicationManager();
        $databaseManager = new sfDatabaseManager($this->configuration);
         //get src stage id passed
        $src_stage_id = $arguments['src_stage_id'] ;
        //get dest stage id passed
        $dest_stage_id = $arguments['dest_stage_id'] ;
        error_log("Src Stage ".$src_stage_id);
        error_log("Dest Stage ".$dest_stage_id);
        $sql = "SELECT f.application_id,f.approved,f.id FROM form_entry f LEFT JOIN mf_invoice m ON f.id = m.app_id WHERE f.approved = $src_stage_id AND paid = 1 ";
        
        $res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($sql);
       // error_log(">>>>>>>".$sql);
        //get these applications
        /**$q = Doctrine_Query::create() 
            ->from('MfInvoice m')
            ->leftJoin('m.FormEntry f')
            ->where('f.approved = ? ', $src_stage_id)
            ->andWhere('m.paid = ?',1) ;
        //
        $res = $q->execute() ;*/
        $otbhelper = new OTBHelper();
        $notify = new mailnotifications();
        foreach($res as $r){
              //error_log("Send bill for application >>>> ".$r['application_id'] );
               $applications = Doctrine_Query::create()
               ->from('FormEntry f')
               ->where('f.id = ?', $r['id']);
               //
               $applications_res = $applications->fetchOne();
               if($applications_res){
                   error_log("Debug: Move application >>> ".$applications_res->getId());

                        //get template
                    $q = Doctrine_Query::create()
                    ->from('Invoicetemplates a')
                    ->where("a.applicationform = ?", $applications_res->getFormId())
                    ->limit(1);
                    $invoicetemplate = $q->fetchOne();
                        //expiration times
                        if ($invoicetemplate->getMaxDuration()) {
                            $date = strtotime("+" . $invoicetemplate->getMaxDuration() . " day", time());
                            $expires_at = date('Y-m-d', $date);
                        }

                        if ($invoicetemplate->getDueDuration()) {
                            $date = strtotime("+" . $invoicetemplate->getDueDuration() . " day", time());
                            $expires_at = date('Y-m-d', $date);
                        }
                        $q_update = Doctrine_Query::create()
                                    ->update('MfInvoice m')
                                    ->set('m.expires_at','?',$expires_at)
                                    ->where('m.app_id = ?',$applications_res->getId());
                        $q_update_res = $q_update->execute();
                        //move the application and send an email
                        $q_update_stage = Doctrine_Query::create()
                        ->update('FormEntry f')
                        ->set('f.approved','?',$dest_stage_id)
                        ->where('f.id = ?',$applications_res->getId());
                         $q_update_res_stage = $q_update_stage->execute();
                         /////////////
                         /**$client_email =  $otbhelper->getApplicationOwnerDetails($applications_res->getUserId());
                        //error_log("Owner Email >>>".$client_email->getEmail()." App is ".$applications_res->getId());
                        //$to = $client_email->getEmail() ;
                       $invoice_no = $otbhelper->getInvoiceInfo($applications_res->getId()) ;
                        $to = "boniface@otbafrica.com" ;
                        $subject = "Maseru City Council - Invoice Issued " ;
                        $message = "Dear ".$client_email->getFullname()." an Invoice ".$invoice_no->getInvoiceNumber()." for application ".$applications_res->getApplicationId()." for amount  M.  ".$invoice_no->getTotalAmount()." has been generated. Please make necessary arrangements for payment.
                        Thank you for using Maseru Construction Permit System .       
                        Best Regards,
                         Maseru Construction Permit Support Team
                        
                        
                        " ;
                        
                        $from = "buildingcontrol@mcc.org.ls" ;
                        $org = "Maseru City Council" ;
                        ////
                        error_log("From >>>>>>>>>>>".$from);
                        error_log("to >>>>>>>>>>>".$to);
                        error_log("org >>>>>>>>>>>".$org);
                        error_log("From >>>>>>>>>>>".$subject);
                        error_log("From >>>>>>>>>>>".$message);
                        //NOTE: For this to work, add swiftlib call but its not required*/
                        //$notify->sendemail($from, $to, $subject, $message);
                       
 
               }

               
        }
        
        
    } 
}