<?php

require_once 'autoload.php';

class DateFilter implements \Dust\Filter\Filter {
    public function apply($value) {
        return date('d/m/Y', (new DateTime($value))->getTimestamp());
    }
}

class templateparser
{
    public function setup($application_id, $form_id, $entry_id)
    {
        $this->application = Doctrine_Core::getTable("FormEntry")->find($application_id);
        $this->user = Doctrine_Core::getTable("SfGuardUser")->find($this->application->getUserId());
        $this->user_profile = $this->user->getProfile();
        $this->dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$this->dbconn);
        $q = Doctrine_Query::create()
            ->from('mfUserProfile a')
            ->where('a.user_id = ?', $this->application->getUserId())
            ->limit(1);
        $this->formprofile = $q->fetchOne();
        $this->form_id = $form_id;
        $this->entry_id = $entry_id;
        if($this->formprofile)
        {
            $this->prof_entry_id = $this->formprofile->getEntryId();

            $sql = "SELECT * FROM ap_form_15 WHERE id = ".$this->prof_entry_id;
            $prof_results = mysql_query($sql,$this->dbconn);

            $this->profileform = mysql_fetch_assoc($prof_results);
        }

        $this->app_entry_id = $this->application->getEntryId();

        $sql = "SELECT * FROM ap_form_".$this->form_id." WHERE id = ".$this->entry_id;
        $app_results = mysql_query($sql,$this->dbconn);

        $this->apform = mysql_fetch_assoc($app_results);

        $this->conditions = "";
        $this->miniconditions = "";

        $this->invoice_total = 0;

        $q = Doctrine_Query::create()
            ->from('approvalCondition a')
            ->where('a.entry_id = ?', $this->application->getId());
        $this->approvalconditions = $q->execute();
        $this->conditions = "<ul>";
        foreach($this->approvalconditions as $approval)
        {
            $this->conditions = $this->conditions."<li>".$approval->getCondition()->getShortName().". ".$approval->getCondition()->getDescription()."</li>";
            $this->miniconditions = $this->miniconditions.$approval->getCondition()->getShortName().", ";
        }
        $q = Doctrine_Query::create()
            ->from('Conditions a')
            ->where('a.circulation_id = ?', $this->application->getCirculationId());
        $conds = $q->execute();
        foreach($conds as $cond)
        {
            $this->conditions = $this->conditions."<li>- ".$cond->getConditionText()."</li>";
        }
        $this->conditions = $this->conditions."</ul>";

        $q = Doctrine_Query::create()
            ->from('EntryDecline a')
            ->where('a.entry_id = ?', $this->application->getId());
        $this->comments = $q->execute();
        $this->decline = "<ul>";
        foreach($this->comments as $comment)
        {
            $this->decline = $this->decline."<li>-".$comment->getDescription()."</li>";
        }
        $this->decline = $this->decline."</ul>";

        $this->invoices = $this->application->getMfInvoice();
        foreach($this->invoices as $invoice)
        {
            $q = Doctrine_Query::create()
                ->from('mfInvoiceDetail a')
                ->where('a.invoice_id = ? AND a.description = ?', array($invoice->getId(),'Total'))
                ->limit(1);
            $this->details = $q->fetchOne();
            //OTB Fix - Get amount if we have invoice lines
            if($this->details){
                $this->invoice_total = $this->invoice_total + $this->details->getAmount();
            }
            
        }

        $q = Doctrine_Query::create()
            ->from('PlotActivity a')
            ->where('a.entry_id = ?', $this->application->getId())
            ->limit(1);
        $this->plotactivity = $q->fetchOne();

        $q = Doctrine_Query::create()
            ->from('apFormElements a')
            ->where('a.form_id = ?', $this->profile_form);

        $this->profileelements = $q->execute();

        $q = Doctrine_Query::create()
            ->from('apFormElements a')
            ->where('a.form_id = ?', $this->form_id);

        $this->formelements = $q->execute();
    }

    public function parseReport($content)
    {
        //Get User Information (anything starting with sf_ )
        //sf_email, sf_fullname, sf_username, ... other fields in the dynamic user profile form e.g sf_element_1
        if($this->find('{sf_username}', $content))
        {
            $content = str_replace('{sf_username}',$this->user->getUsername(), $content);
        }
        if($this->find('{sf_email}', $content))
        {
            $content = str_replace('{sf_email}',$this->user_profile->getEmail(), $content);
        }
        if($this->find('{sf_fullname}', $content))
        {
            $content = str_replace('{sf_fullname}',$this->user_profile->getFullname(), $content);
        }

        if($this->find('{ap_application_status}', $content))
        {
            $content = str_replace('{ap_application_status}',$this->application->getStatusName(), $content);
        }

        if($this->find('{ap_date_of_submission}', $content))
        {
            if($this->application->getDateOfSubmission())
            {
                $content = str_replace('{ap_date_of_submission}', date('d F Y', strtotime($this->application->getDateOfSubmission())), $content);
            }
            else
            {
                $content = str_replace('{ap_date_of_submission}', "", $content);
            }
        }

        if($this->find('{ap_date_of_approval}', $content))
        {
            if($this->application->getDateOfIssue())
            {
                $content = str_replace('{ap_date_of_approval}', date('d F Y', strtotime($this->application->getDateOfIssue())), $content);
            }
            else
            {
                $content = str_replace('{ap_date_of_approval}', "", $content);
            }
        }


        if($this->plotactivity)
        {
            $q = Doctrine_Query::create()
                ->from('Plot a')
                ->where('a.id = ?', $this->plotactivity->getPlotId())
                ->limit(1);
            $plot = $q->fetchOne();
            if($plot)
            {
                if($this->find('{plot_size}', $content))
                {
                    $content = str_replace('{plot_size}',$plot->getPlotSize(), $content);
                }
            }
        }

        $profile_form = '15';

        foreach($this->profileelements as $element)
        {
            $childs = $element->getElementTotalChild();
            if($childs == 0)
            {
                if($this->find('{sf_element_'.$element->getElementId().'}', $content))
                {
                    $content = str_replace('{sf_element_'.$element->getElementId().'}',$this->profileform['element_'.$element->getElementId()], $content);
                }
            }
            else
            {
                if($element->getElementType() == "select")
                {
                    if($this->find('{sf_element_'.$element->getElementId().'}', $content))
                    {
                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($profile_form,$element->getElementId(),$this->profileform['element_'.$element->getElementId()]))
                            ->limit(1);
                        $option = $q->fetchOne();

                        if($option)
                        {
                            $content = str_replace('{sf_element_'.$element->getElementId().'}',$option->getOption(), $content);
                        }
                    }
                }
                else
                {
                    for($x = 0; $x < ($childs + 1); $x++)
                    {
                        if($this->find('{sf_element_'.$element->getElementId().'_'.($x+1).'}', $content))
                        {
                            $content = str_replace('{sf_element_'.$element->getElementId().'_'.($x+1).'}',$this->profileform['element_'.$element->getElementId()."_".($x+1)], $content);
                        }
                    }

                    if($this->find('{sf_element_'.$element->getElementId().'}', $content))
                    {
                        $content = str_replace('{sf_element_'.$element->getElementId().'}',$this->profileform['element_'.$element->getElementId()], $content);
                    }
                }


            }


        }

        //Get Application Information (anything starting with ap_ )
        //ap_application_id
        if($this->find('{ap_application_id}', $content))
        {
            $content = str_replace('{ap_application_id}',$this->application->getApplicationId(), $content);
        }

        //Get Form Details (anything starting with fm_ )
        //fm_created_at, fm_updated_at.....fm_element_1


        if($this->find('{fm_created_at}', $content))
        {
            if($this->application->getDateOfSubmission())
            {
                $content = str_replace('{fm_created_at}', date('d F Y', strtotime(substr($this->application->getDateOfSubmission(), 0, 11))), $content);
            }
            else
            {
                $content = str_replace('{fm_created_at}', "", $content);
            }
        }
        if($this->find('{fm_updated_at}', $content))
        {
            if($this->application->getDateOfResponse())
            {
                $content = str_replace('{fm_updated_at}', date('d F Y', strtotime(substr($this->application->getDateOfResponse(), 0, 11))), $content);
            }
            else
            {
                $content = str_replace('{fm_updated_at}', "", $content);
            }
        }
        if($this->find('{current_date}', $content))
        {
            if(date('Y-m-d'))
            {
                $content = str_replace('{current_date}', date('d F Y', strtotime(date('Y-m-d'))), $content);
            }
            else
            {
                $content = str_replace('{current_date}', "", $content);
            }
        }



        foreach($this->formelements as $element)
        {
            $childs = $element->getElementTotalChild();

            if($childs == 0)
            {
                if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                {
                    $content = str_replace('{fm_element_'.$element->getElementId().'}',$this->apform['element_'.$element->getElementId()], $content);
                }
                else
                {
                    for($x = 0; $x < ($childs + 1); $x++)
                    {
                        if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId()."}",$this->apform['element_'.$element->getElementId()."_".($x+1)], $content);
                        }
                    }

                }
            }
            else
            {

                if($element->getElementType() == "select")
                {
                    if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                    {
                        $opt_value = 0;
                        if($this->apform['element_'.$element->getElementId()] == "0")
                        {
                            $opt_value++;
                        }
                        else
                        {
                            $opt_value = $this->apform['element_'.$element->getElementId()];
                        }

                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($this->form_id,$element->getElementId(),$opt_value))
                            ->limit(1);
                        $option = $q->fetchOne();


                        if($option)
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId().'}',$option->getOption(), $content);
                        }
                    }
                }
                else
                {
                    for($x = 0; $x < ($childs + 1); $x++)
                    {
                        if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId()."}",$this->apform['element_'.$element->getElementId()."_".($x+1)], $content);
                        }
                    }

                }




            }
            for($x = 0; $x < ($childs + 1); $x++)
            {
                if($this->find('{fm_element_'.$element->getElementId()."_".($x+1).'}', $content))
                {
                    $content = str_replace('{fm_element_'.$element->getElementId()."_".($x+1).'}',$this->apform['element_'.$element->getElementId()."_".($x+1)], $content);
                }
            }

        }

        //Get Conditions of Approval (anything starting with ca_ )
        //ca_conditions
        if($this->find('{ca_conditions}', $content))
        {
            $content = str_replace('{ca_conditions}', $this->conditions , $content);
        }

        //ca_conditions
        if($this->find('{ap_comments}', $content))
        {
            $content = str_replace('{ap_comments}', $this->decline , $content);
        }

        //mini_ca_conditions
        if($this->find('{mini_ca_conditions}', $content))
        {
            $content = str_replace('{mini_ca_conditions}', $this->miniconditions , $content);
        }
        //OTB patch - Addition of invoice number
        //in_number
        if($this->find('{in_number}', $content))
        {
            $content = str_replace('{in_number}', $this->invoice_number , $content);
        }
        //Get Invoice Details (anything starting with in_ )
        //in_total

        if($this->find('{in_total}', $content))
        {
            $content = str_replace('{in_total}', $this->invoice_total , $content);
        }

        return $content;

    }

    public function parse($application_id, $form_id, $entry_id, $content)
    {
        $saved_application = Doctrine_Core::getTable("FormEntry")->find($application_id);

        if($this->find('{fm_id}', $content))
        {
            $content = str_replace('{fm_id}',$saved_application->getId(), $content);
        }

        if($this->find('{fm_application_id}', $content))
        {
            $content = str_replace('{fm_application_id}',$saved_application->getApplicationId(), $content);
        }

        if($this->find('{fm_date_of_notice}', $content))
        {
            $q = Doctrine_Query::create()
                ->from("ApplicationReference a")
                ->where("a.application_id = ?", $application_id)
                ->orderBy("a.id DESC")
                ->limit(1);
            $app_ref = $q->fetchOne();

            if($app_ref)
            {
                $content = str_replace('{fm_date_of_notice}', date('d F Y', strtotime(substr($app_ref->getStartDate(), 0, 11))), $content);
            }
            else
            {
                $content = str_replace('{fm_date_of_notice}',"", $content);
            }
        }

        if($this->find('{current_stage_date}', $content))
        {
            $q = Doctrine_Query::create()
                ->from("ApplicationReference a")
                ->where("a.application_id = ?", $application_id)
                ->orderBy("a.id DESC")
                ->limit(1);
            $app_ref = $q->fetchOne();

            if($app_ref)
            {
                $content = str_replace('{current_stage_date}', date('d F Y', strtotime(substr($app_ref->getStartDate(), 0, 11))), $content);
            }
            else
            {
                $content = str_replace('{current_stage_date}',"", $content);
            }
        }

        if($this->find('{fm_notice_no}', $content))
        {
            $content = str_replace('{fm_notice_no}',$saved_application->getApplicationId(), $content);
        }

        $user = Doctrine_Core::getTable("SfGuardUser")->find($saved_application->getUserId());

        $user_profile = $user->getProfile();

        $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);


        $q = Doctrine_Query::create()
            ->from('mfUserProfile a')
            ->where('a.user_id = ?', $saved_application->getUserId())
            ->limit(1);
        $formprofile = $q->fetchOne();


        if($formprofile)
        {
            $prof_entry_id = $formprofile->getEntryId();

            $sql = "SELECT * FROM ap_form_15 WHERE id = ".$prof_entry_id;
            $prof_results = mysql_query($sql,$dbconn);

            $profile_form = mysql_fetch_assoc($prof_results);
        }

        $app_entry_id = $saved_application->getEntryId();

        $sql = "SELECT * FROM ap_form_".$form_id." WHERE id = ".$entry_id;
        $app_results = mysql_query($sql,$dbconn);

        $apform = mysql_fetch_assoc($app_results);

        $conditions = "";
        $miniconditions = "";

        $invoice_total = 0;

        $q = Doctrine_Query::create()
            ->from('approvalCondition a')
            ->where('a.entry_id = ?', $saved_application->getId());
        $approvalconditions = $q->execute();
        $conditions = "<ul>";
        foreach($approvalconditions as $approval)
        {
            $conditions = $conditions."<li>".$approval->getCondition()->getShortName().". ".$approval->getCondition()->getDescription()."</li>";
            $miniconditions = $miniconditions.$approval->getCondition()->getShortName().", ";
        }
        $q = Doctrine_Query::create()
            ->from('Conditions a')
            ->where('a.circulation_id = ?', $saved_application->getCirculationId());
        $conds = $q->execute();
        foreach($conds as $cond)
        {
            $conditions = $conditions."<li>- ".$cond->getConditionText()."</li>";
        }
        $conditions = $conditions."</ul>";

        $q = Doctrine_Query::create()
            ->from('EntryDecline a')
            ->where('a.entry_id = ?', $saved_application->getId());
        $comments = $q->execute();
        $decline = "<ul>";
        foreach($comments as $comment)
        {
            $decline = $decline."<li>-".$comment->getDescription()."</li>";
        }
        $decline = $decline."</ul>";

        $invoices = $saved_application->getMfInvoice();
        foreach($invoices as $invoice)
        {
            $q = Doctrine_Query::create()
                ->from('mfInvoiceDetail a')
                ->where('a.invoice_id = ? AND a.description = ?', array($invoice->getId(),'Total'))
                ->limit(1);
            $details = $q->fetchOne();
            if($details)
            {
                $invoice_total = $invoice_total + $details->getAmount();
            }
             //Invoice number - OTB patch
            $q2 = Doctrine_Query::create()
                    ->from('mfInvoice a')
                    ->where('a.id = ? ',$invoice->getId()) ;
            //
            $this->details2 = $q2->fetchOne();
            //
            $invoice_number = $this->details2->getInvoiceNumber();
        }


        //Get User Information (anything starting with sf_ )
        //sf_email, sf_fullname, sf_username, ... other fields in the dynamic user profile form e.g sf_element_1
        if($this->find('{sf_username}', $content))
        {
            $content = str_replace('{sf_username}',$user->getUsername(), $content);
        }
        if($this->find('{sf_email}', $content))
        {
            $content = str_replace('{sf_email}',$user_profile->getEmail(), $content);
        }
        if($this->find('{sf_fullname}', $content))
        {
            $content = str_replace('{sf_fullname}',$user_profile->getFullname(), $content);
        }
        //in_number -- OTB patch
        if($this->find('{in_number}', $content))
        {
            $content = str_replace('{in_number}', $invoice_number , $content);
            error_log("Debug2 OTB Invoice Number: ".$invoice_number);   
            
        }
      
        $q = Doctrine_Query::create()
            ->from('PlotActivity a')
            ->where('a.entry_id = ?', $saved_application->getId())
            ->limit(1);
        $plotactivity = $q->fetchOne();

        if($plotactivity)
        {
            $q = Doctrine_Query::create()
                ->from('Plot a')
                ->where('a.id = ?', $plotactivity->getPlotId())
                ->limit(1);
            $plot = $q->fetchOne();
            if($plot)
            {
                if($this->find('{plot_size}', $content))
                {
                    $content = str_replace('{plot_size}',$plot->getPlotSize(), $content);
                }
            }
        }

        $profile_form = '15';

        $q = Doctrine_Query::create()
            ->from('apFormElements a')
            ->where('a.form_id = ?', $profile_form);

        $elements = $q->execute();

        foreach($elements as $element)
        {
            $childs = $element->getElementTotalChild();
            if($childs == 0)
            {
                if($this->find('{sf_element_'.$element->getElementId().'}', $content))
                {
                    $content = str_replace('{sf_element_'.$element->getElementId().'}',$profile_form['element_'.$element->getElementId()], $content);
                }
            }
            else
            {
                if($element->getElementType() == "select")
                {
                    if($this->find('{sf_element_'.$element->getElementId().'}', $content))
                    {
                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($profile_form,$element->getElementId(),$profile_form['element_'.$element->getElementId()]))
                            ->limit(1);
                        $option = $q->fetchOne();

                        if($option)
                        {
                            $content = str_replace('{sf_element_'.$element->getElementId().'}',$option->getOption(), $content);
                        }
                    }
                }
                else
                {
                    for($x = 0; $x < ($childs + 1); $x++)
                    {
                        if($this->find('{sf_element_'.$element->getElementId().'_'.($x+1).'}', $content))
                        {
                            $content = str_replace('{sf_element_'.$element->getElementId().'_'.($x+1).'}',$profile_form['element_'.$element->getElementId()."_".($x+1)], $content);
                        }
                    }

                    if($this->find('{sf_element_'.$element->getElementId().'}', $content))
                    {
                        $content = str_replace('{sf_element_'.$element->getElementId().'}',$profile_form['element_'.$element->getElementId()], $content);
                    }
                }


            }


        }

        //Get Application Information (anything starting with ap_ )
        //ap_application_id
        if($this->find('{ap_application_id}', $content))
        {
            $content = str_replace('{ap_application_id}',$saved_application->getApplicationId(), $content);
        }
//OTB - added links to application in parser
        if($this->find('{ap_backend_application_link}', $content))
        {
            $content = str_replace('{ap_backend_application_link}',"http://".$_SERVER['HTTP_HOST']."/backend.php/applications/view/id/".$application_id, $content);
        }

        if($this->find('{ap_frontend_application_link}', $content))
        {
            $content = str_replace('{ap_frontend_application_link}',"http://".$_SERVER['HTTP_HOST']."/index.php/application/view/id/".$application_id, $content);
        }
//OTB END - added links to application in parser
        //Get Form Details (anything starting with fm_ )
        //fm_created_at, fm_updated_at.....fm_element_1


        if($this->find('{fm_created_at}', $content))
        {
            if($saved_application->getDateOfSubmission())
            {
                $content = str_replace('{fm_created_at}', date('d F Y', strtotime(substr($saved_application->getDateOfSubmission(), 0, 11))), $content);
            }
            else
            {
                $content = str_replace('{fm_created_at}', "", $content);
            }
        }
        if($this->find('{fm_updated_at}', $content))
        {
            if($saved_application->getDateOfResponse())
            {
                $content = str_replace('{fm_updated_at}', date('d F Y', strtotime(substr($saved_application->getDateOfResponse(), 0, 11))), $content);
            }
            else
            {
                $content = str_replace('{fm_updated_at}', "", $content);
            }
        }

        if($this->find('{fm_date_created}', $content))
        {
            if($saved_application->getDateOfSubmission())
            {
                $content = str_replace('{fm_date_created}', date('d F Y', strtotime(substr($saved_application->getDateOfSubmission(), 0, 11))), $content);
            }
            else
            {
                $content = str_replace('{fm_date_created}', "", $content);
            }
        }
        if($this->find('{fm_last_updated}', $content))
        {
            if($saved_application->getDateOfResponse())
            {
                $content = str_replace('{fm_last_updated}', date('d F Y', strtotime(substr($saved_application->getDateOfResponse(), 0, 11))), $content);
            }
            else
            {
                $content = str_replace('{fm_last_updated}', "", $content);
            }
        }
        if($this->find('{current_date}', $content))
        {
            $content = str_replace('{current_date}',date('d F Y', strtotime(date('Y-m-d'))), $content);
        }

        $q = Doctrine_Query::create()
            ->from('apFormElements a')
            ->where('a.form_id = ?', $form_id);

        $elements = $q->execute();

        foreach($elements as $element)
        {
            $childs = $element->getElementTotalChild();

            if($childs == 0)
            {
                if($element->getElementType() == "select")
                {
                    if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                    {
                        $opt_value = 0;
                        if($apform['element_'.$element->getElementId()] == "0")
                        {
                            $opt_value++;
                        }
                        else
                        {
                            $opt_value = $apform['element_'.$element->getElementId()];
                        }


                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($form_id,$element->getElementId(),$opt_value))
                            ->limit(1);
                        $option = $q->fetchOne();

                        if($option)
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId().'}',$option->getOption(), $content);
                        }
                    }
                }
                if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                {
                    if($element->getElementId()==1){
                        $content = str_replace('{fm_element_'.$element->getElementId().'}',$apform['element_'.$element->getElementId().'_1'].' '.$apform['element_'.$element->getElementId().'_2'], $content);
                    }
                    else{
                        $content = str_replace('{fm_element_'.$element->getElementId().'}',$apform['element_'.$element->getElementId()], $content);
                    }
                }
                elseif($this->find('{fm_element_'.$element->getElementId().'_2}', $content))
                {
                    $content = str_replace('{fm_element_'.$element->getElementId().'_2}',$apform['element_'.$element->getElementId()."_2"], $content);
                }
				//OTB Patch Start - Get actual plot number from UPI
                else if($this->find('{upi_plot_fm_element_'.$element->getElementId().'}', $content))
                {
					$upi_plot = explode("/", $apform['element_'.$element->getElementId()]);
					$upi_plot = $upi_plot[4] ? $upi_plot[4] : "-";
                    $content = str_replace('{upi_plot_fm_element_'.$element->getElementId().'}', $upi_plot, $content);
                }
				//OTB Patch End - Get actual plot number from UPI
                else
                {
                    for($x = 0; $x < ($childs + 1); $x++)
                    {
                        if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                        }
                    }

                }
            }
            else
            {

                if($element->getElementType() == "select")
                {
                    if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                    {
                        $opt_value = 0;
                        if($apform['element_'.$element->getElementId()] == "0")
                        {
                            $opt_value++;
                        }
                        else
                        {
                            $opt_value = $apform['element_'.$element->getElementId()];
                        }

                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($form_id,$element->getElementId(),$opt_value))
                            ->limit(1);
                        $option = $q->fetchOne();


                        if($option)
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId().'}',$option->getOption(), $content);
                        }
                    }
                }
                else
                {
                    for($x = 0; $x < ($childs + 1); $x++)
                    {
                        if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                        }
                    }

                }




            }
            for($x = 0; $x < ($childs + 1); $x++)
            {
                if($this->find('{fm_element_'.$element->getElementId()."_".($x+1).'}', $content))
                {
                    $content = str_replace('{fm_element_'.$element->getElementId()."_".($x+1).'}',$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                }
				//OTB Patch Start - Get actual plot number from UPI
				else if($this->find('{upi_plot_fm_element_'.$element->getElementId().'}', $content))
				{
					$upi_plot = explode("/", $apform['element_'.$element->getElementId()]);
					$upi_plot = $upi_plot[4] ? $upi_plot[4] : "-";
					$content = str_replace('{upi_plot_fm_element_'.$element->getElementId().'}', $upi_plot, $content);
				}
				//OTB Patch End - Get actual plot number from UPI
            }

        }

        //Get Conditions of Approval (anything starting with ca_ )
        //ca_conditions
        if($this->find('{ca_conditions}', $content))
        {
            $content = str_replace('{ca_conditions}', $conditions , $content);
        }

        //ca_conditions
        if($this->find('{ap_comments}', $content))
        {
            $content = str_replace('{ap_comments}', $decline , $content);
        }

        //mini_ca_conditions
        if($this->find('{mini_ca_conditions}', $content))
        {
            $content = str_replace('{mini_ca_conditions}', $miniconditions , $content);
        }

        //Get Invoice Details (anything starting with in_ )
        //in_total

        if($this->find('{in_total}', $content))
        {
            $content = str_replace('{in_total}', $invoice_total , $content);
        }

        return $content;

    }


    /***
     * Used to parse values into a permit template
     *
     * @param $content string the string that contains placeholders to be replaced
     * @param $application_id the id of the application
     * @return string
     */
    public function parseApplication($application_id, $content)
    {
        $application = Doctrine_Core::getTable("FormEntry")->find($application_id);

        $user_id = $application->getUserId();

        //User Details
        $user_details = $this->getUserDetails($user_id);

        //Application Details
        $application_details = $this->getApplicationDetails($application_id);

        $values = array_merge($user_details, $application_details);

        $content = static::parseWithDust($content, $values);

        return $content;

    }


    /***
     * Used to parse values into a permit template
     *
     * @param $content string the string that contains placeholders to be replaced
     * @param $application_id the id of the application
     * @param $form_id the id of the form
     * @param $entry_id the id of the entry in the form table
     * @param $permit_id the id of the permit
     * @return string
     */
    public function parsePermit($application_id, $form_id, $entry_id, $permit_id, $content)
    {
        $application = Doctrine_Core::getTable("FormEntry")->find($application_id);
        $saved_permit = Doctrine_Core::getTable("SavedPermit")->find($permit_id);

        $user_id = $application->getUserId();

        //User Details
        $user_details = $this->getUserDetails($user_id);

        //Application Details
        $application_details = $this->getApplicationDetails($application_id);

        //Permit Details
        $permit_details = $this->getPermitDetails($permit_id);

        $values = array_merge($user_details, $application_details);
        $values = array_merge($values, $permit_details);

		//Start OTB Patch - Get comment sheet data when parsing permit
        $comment_sheet = $this->getCommentSheetDetails($application_id);
		$values = array_merge($values, $comment_sheet);
		//End OTB Patch - Get comment sheet data when parsing permit

        //Start OTB Patch -  Add the permit conditions that have parameters from all over
        $this->setup($application_id, $form_id, $entry_id);
        $values['ca_conditions'] = static::parseWithDust($this->conditions, $values);
        //End OTB Patch -  Add the permit conditions that have parameters from all over
		
        $content = static::parseWithDust($content, $values);

        return $content;

    }

	//Start OTB Patch - Get comment sheet data when parsing permit
    /**
     * Given the relevant parameters get the additional form details that should
     * be rendered in the result set
     * 
     * @param   application_id
     * @return  Array
     */
    public function getCommentSheetDetails($application_id) {
        $values = array();
        
        // Get the conditions to apply
        $query = Doctrine_Query::create()
            ->from("TaskForms a")
            ->where("a.task_id IN (SELECT b.id FROM Task b WHERE b.application_id = ?)", $application_id)
            ->execute();

        foreach ( $query as $row ) {
            $stmt = Doctrine_Query::create()->getConnection()->execute (
                "SELECT * FROM ap_form_{$row->form_id} a WHERE a.id = :id", 
                array('id'=>$row->entry_id)
            );

            while ( $cell = $stmt->fetch(\PDO::FETCH_ASSOC) ) {
                foreach ( $cell as $key=>$val ) {
                    if ( strlen($key)>8 && substr($key, 0, 8)=="element_" )
                        $values["fm_c{$row->form_id}_{$key}"] = $val;
                }
            }
        }
        
        // Return the parameters for further processing
        return $values;
    }
	//End OTB Patch - Get comment sheet data when parsing permit

    /***
     * Used to parse values into a permit template
     *
     * @param $content string the string that contains placeholders to be replaced
     * @param $application_id the id of the application
     * @param $form_id the id of the form
     * @param $entry_id the id of the entry in the form table
     * @param $permit_id the id of the permit
     * @return string
     */
    public function parseArchivePermit($application_id, $form_id, $entry_id, $permit_id, $content)
    {
        $application = Doctrine_Core::getTable("FormEntryArchive")->find($application_id);
        $saved_permit = Doctrine_Core::getTable("SavedPermitArchive")->find($permit_id);

        $user_id = $application->getUserId();

        //User Details
        $user_details = $this->getUserDetails($user_id);

        //Application Details
        $application_details = $this->getArchiveApplicationDetails($application_id);

        //Permit Details
        $permit_details = $this->getArchivePermitDetails($permit_id);

        $values = array_merge($user_details, $application_details);
        $values = array_merge($values, $permit_details);

        $content = static::parseWithDust($content, $values);

        return $content;

    }

    public function parsePermitOld($application_id, $form_id, $entry_id, $permit_id, $content)
    {
        $saved_application = Doctrine_Core::getTable("FormEntry")->find($application_id);

        $saved_permit = Doctrine_Core::getTable("SavedPermit")->find($permit_id);

        if($this->find('{ap_issue_date}', $content))
        {
            if($saved_permit->getDateOfIssue())
            {
                $content = str_replace('{ap_issue_date}', date('d F Y', strtotime($saved_permit->getDateOfIssue())), $content);
            }
            else
            {
                $content = str_replace('{ap_issue_date}', "", $content);
            }
        }

        if($this->find('{ap_permit_id}', $content))
        {
            $content = str_replace('{ap_permit_id}', $saved_permit->getPermitId(), $content);
        }

        if($this->find('{ap_expire_date}', $content))
        {
            if($saved_permit->getDateOfExpiry())
            {
                $content = str_replace('{ap_expire_date}', date('d F Y', strtotime($saved_permit->getDateOfExpiry())), $content);
            }
            else
            {
                $content = str_replace('{ap_expire_date}', "", $content);
            }
        }

        $user = Doctrine_Core::getTable("SfGuardUser")->find($saved_application->getUserId());

        $user_profile = $user->getProfile();

        $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

        $q = Doctrine_Query::create()
            ->from('mfUserProfile a')
            ->where('a.user_id = ?', $user->getId())
            ->limit(1);
        $formprofile = $q->fetchOne();

        $profile_form = "";

        if($formprofile)
        {
            $prof_entry_id = $formprofile->getEntryId();

            $sql = "SELECT * FROM ap_form_15 WHERE id = ".$prof_entry_id;
            $prof_results = mysql_query($sql,$dbconn);

            $profile_form = mysql_fetch_assoc($prof_results);
        }

        $app_entry_id = $saved_application->getEntryId();

        $sql = "SELECT * FROM ap_form_".$form_id." WHERE id = ".$app_entry_id;
        $app_results = mysql_query($sql,$dbconn);

        $apform = mysql_fetch_assoc($app_results);

        $conditions = "";

        $q = Doctrine_Query::create()
            ->from('approvalCondition a')
            ->where('a.entry_id = ?', $saved_application->getId());
        $approvalconditions = $q->execute();
        $conditions = "<ul>";
        foreach($approvalconditions as $approval)
        {
            $conditions = $conditions."<li>".$approval->getCondition()->getShortName().". ".$approval->getCondition()->getDescription()."</li>";
        }
        $q = Doctrine_Query::create()
            ->from('Conditions a')
            ->where('a.circulation_id = ?', $saved_application->getCirculationId());
        $conds = $q->execute();
        foreach($conds as $cond)
        {
            $conditions = $conditions."<li>- ".$cond->getConditionText()."</li>";
        }
        $conditions = $conditions."</ul>";


        //Get User Information (anything starting with sf_ )
        //sf_email, sf_fullname, sf_username, ... other fields in the dynamic user profile form e.g sf_element_1
        if($this->find('{sf_username}', $content))
        {
            $content = str_replace('{sf_username}',$user->getUsername(), $content);
        }
        if($this->find('{sf_mobile}', $content))
        {
            $content = str_replace('{sf_mobile}',$user_profile->getMobile(), $content);
        }
        if($this->find('{sf_email}', $content))
        {
            $content = str_replace('{sf_email}',$user_profile->getEmail(), $content);
        }
        if($this->find('{sf_fullname}', $content))
        {
            $content = str_replace('{sf_fullname}',$user_profile->getFullname(), $content);
        }

        $q = Doctrine_Query::create()
            ->from('apFormElements a')
            ->where('a.form_id = ?', 15);

        $elements = $q->execute();

        foreach($elements as $element)
        {
            $childs = $element->getElementTotalChild();
            if($childs == 0)
            {
                if($this->find('{sf_element_'.$element->getElementId().'}', $content))
                {
                    $content = str_replace('{sf_element_'.$element->getElementId().'}',$profile_form['element_'.$element->getElementId()], $content);
                }
            }
            else
            {
                if($element->getElementType() == "select")
                {
                    if($this->find('{sf_element_'.$element->getElementId().'}', $content))
                    {
                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($profile_form,$element->getElementId(),$profile_form['element_'.$element->getElementId()]))
                            ->limit(1);
                        $option = $q->fetchOne();

                        if($option)
                        {
                            $content = str_replace('{sf_element_'.$element->getElementId().'}',$option->getOption(), $content);
                        }
                    }
                }
                else
                {
                    for($x = 0; $x < ($childs + 1); $x++)
                    {
                        if($this->find('{sf_element_'.$element->getElementId().'_'.($x+1).'}', $content))
                        {
                            $content = str_replace('{sf_element_'.$element->getElementId().'_'.($x+1).'}',$profile_form['element_'.$element->getElementId()."_".($x+1)], $content);
                        }
                    }
                }
            }


        }

        //Get Application Information (anything starting with ap_ )
        //ap_application_id
        if($this->find('{ap_application_id}', $content))
        {
            $content = str_replace('{ap_application_id}',$saved_application->getApplicationId(), $content);
        }

        //Get Form Details (anything starting with fm_ )
        //fm_created_at, fm_updated_at.....fm_element_1


        if($this->find('{fm_created_at}', $content))
        {
            if($saved_application->getDateOfSubmission())
            {
                $content = str_replace('{fm_created_at}', date('d F Y', strtotime($saved_application->getDateOfSubmission())), $content);
            }
            else
            {
                $content = str_replace('{fm_created_at}', "", $content);
            }
        }
        if($this->find('{fm_updated_at}', $content))
        {
            if($saved_application->getDateOfResponse())
            {
                $content = str_replace('{fm_updated_at}', date('d F Y', strtotime(substr($saved_application->getDateOfResponse(), 0, 11))), $content);
            }
            else
            {
                $content = str_replace('{fm_updated_at}', "", $content);
            }
        }
        if($this->find('{current_date}', $content))
        {
            $content = str_replace('{current_date}',date('d F Y', strtotime(date('Y-m-d'))), $content);
        }

        $q = Doctrine_Query::create()
            ->from('apFormElements a')
            ->where('a.form_id = ?', $form_id);

        $elements = $q->execute();

        foreach($elements as $element)
        {
            if($element->getElementType() != "select")
            {
                //Check remote URL and post values
                $remote_url = $element->getElementOptionQuery();
                if(!empty($remote_url))
                {
                    $criteria = $element->getElementFieldName();
                    $remote_value = $element->getElementFieldValue();
                    $remote_username = $element->getElementRemoteUsername();
                    $remote_password = $element->getElementRemotePassword();

                    $pos = strpos($remote_url, '$value');

                    if($pos === false)
                    {
                        //dont' do anything
                    }
                    else
                    {
                        $remote_url = str_replace('$value', $apform['element_'.$element->getElementId()], $remote_url);
                    }

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $remote_url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                    curl_setopt($ch, CURLOPT_USERPWD, "$remote_username:$remote_password");

                    $results = curl_exec($ch);

                    if($error = curl_error($ch))
                    {
                        $error = "remote request error: " .$error . "<br />";
                    }

                    curl_close($ch);

                    if(empty($error))
                    {
                        $values = json_decode($results);
                        if($criteria == "records")
                        {
                            //If count is = 0, fail
                            if($values->{'count'} == 0)
                            {
                                $error = "No records found on server";
                            }
                        }
                        else if($criteria == "norecords")
                        {
                            //If count is greater than 0, then pass
                            if($values->{'count'} > 0)
                            {
                                $error = "Existing records found on server";
                            }
                        }
                        else if($criteria == "value")
                        {
                            if($result != $result_value)
                            {
                                $error = "You input doesn't match any records";
                            }
                        }

                        $entry_value .= "<br>";

                        foreach($values->{'records'} as $record)
                        {
                            foreach($record as $key => $value)
                            {
                                if($this->find('{'.$key.'}', $content))
                                {
                                    $content = str_replace('{'.$key.'}',$value, $content);
                                }
                                if(!is_array($value))
                                {
                                    if($this->find('{'.$key.'}', $content))
                                    {
                                        $content = str_replace('{'.$key.'}',$value, $content);
                                    }
                                }
                                else
                                {
                                    $result_value = "";
                                    foreach($value as $lkey => $lvalue)
                                    {
                                        $result_value = $result_value.$lvalue;
                                    }

                                    if($this->find('{'.$key.'}', $content))
                                    {
                                        $content = str_replace('{'.$key.'}',$result_value, $content);
                                    }
                                }
                            }
                        }


                    }
                    else
                    {
                        error_log($error);
                    }
                }
            }

            if($element->getElementType() == "simple_name")
            {
                if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                {
                    $content = str_replace('{fm_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_1"]." ".$apform['element_'.$element->getElementId()."_2"], $content);
                }
                continue;
            }

            if($element->getElementType() == "simple_name_wmiddle")
            {
                if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                {
                    $content = str_replace('{fm_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_1"]." ".$apform['element_'.$element->getElementId()."_2"]." ".$apform['element_'.$element->getElementId()."_3"], $content);
                }
                continue;
            }

            if($element->getElementType() == "checkbox")
            {
                if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                {
                    $q = Doctrine_Query::create()
                        ->from('ApElementOptions a')
                        ->where('a.form_id = ? AND a.element_id = ?', array($form_id,$element->getElementId()));
                    $options = $q->execute();

                    $options_text = "";
                    foreach($options as $option)
                    {
                        if($apform['element_'.$element->getElementId()."_".$option->getOptionId()])
                        {
                            $options_text .= "".$option->getOption()."";
                        }
                    }


                    if ($apform['element_' . $element->getElementId() . "_other"] != "") {
                        $options_text .= "" . $apform['element_'.$element->getElementId().'_other'] . "";
                    }

                    $options_text .= "";

                    $content = str_replace('{fm_element_'.$element->getElementId()."}",$options_text, $content);
                }
                continue;
            }

            if($element->getElementType() == "select")
            {
                if($element->getElementExistingForm() && $element->getElementExistingStage())
                {
                    $q = Doctrine_Query::create()
                        ->from("FormEntry a")
                        ->where("a.id = ?", $apform['element_'.$element->getElementId()])
                        ->limit(1);
                    $linked_application = $q->fetchOne();
                    if($linked_application)
                    {

                        $q = Doctrine_Query::create()
                            ->from("SavedPermit a")
                            ->leftJoin("a.FormEntry b")
                            ->where("b.form_id = ?", $linked_application->getFormId());
                        $permits = $q->execute();

                        foreach($permits as $saved_permit)
                        {
                            if($this->find("{ap_permit_id_".$saved_permit->getTypeId()."_element_child}", $content))
                            {
                                $content = str_replace("{ap_permit_id_".$saved_permit->getTypeId()."_element_child}", ($saved_permit->getPermitId()?$saved_permit->getPermitId():$linked_application->getApplicationId()), $content);
                            }
                        }

                        if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId().'}', $linked_application->getApplicationId(), $content);
                        }

                        $q = Doctrine_Query::create()
                            ->from('apFormElements a')
                            ->where('a.form_id = ?', $element->getElementExistingForm())
                            ->andWhere('a.element_status = ?', 1);

                        $child_elements = $q->execute();

                        foreach($child_elements as $child_element)
                        {
                            $sql = "SELECT * FROM ap_form_".$linked_application->getFormId()." WHERE id = ".$linked_application->getEntryId();
                            $child_app_results = mysql_query($sql,$dbconn);

                            $child_apform = mysql_fetch_assoc($child_app_results);

                            if($this->find('{ap_child_application_id}', $content))
                            {
                                $content = str_replace('{ap_child_application_id}', $linked_application->getApplicationId(), $content);
                            }

                            if($this->find('{fm_child_created_at}', $content))
                            {
                                if($linked_application->getDateOfSubmission())
                                {
                                    $content = str_replace('{fm_child_created_at}', date('d F Y', strtotime($linked_application->getDateOfSubmission())), $content);
                                }
                                else
                                {
                                    $content = str_replace('{fm_child_created_at}', "", $content);
                                }
                            }

                            if($this->find('{fm_child_updated_at}', $content))
                            {
                                if($linked_application->getDateOfResponse())
                                {
                                    $content = str_replace('{fm_child_updated_at}', date('d F Y', strtotime(substr($linked_application->getDateOfResponse(), 0, 11))), $content);
                                }
                                else
                                {
                                    $content = str_replace('{fm_child_updated_at}', "", $content);
                                }
                            }

                            //START CHILD ELEMENTS
                            $childs = $child_element->getElementTotalChild();
                            if($childs == 0)
                            {
                                if($child_element->getElementType() == "select")
                                {
                                    if($child_element->getElementExistingForm() && $child_element->getElementExistingStage())
                                    {

                                        $q = Doctrine_Query::create()
                                            ->from("FormEntry a")
                                            ->where("a.id = ?", $child_apform['element_'.$child_element->getElementId()])
                                            ->limit(1);
                                        $linked_grand_application = $q->fetchOne();
                                        if($linked_grand_application)
                                        {

                                            $q = Doctrine_Query::create()
                                                ->from("SavedPermit a")
                                                ->leftJoin("a.FormEntry b")
                                                ->where("b.form_id = ?", $linked_grand_application->getFormId());
                                            $permits = $q->execute();

                                            foreach($permits as $saved_permit)
                                            {
                                                if($this->find("{ap_permit_id_".$saved_permit->getTypeId()."_element_grand_child}", $content))
                                                {
                                                    $content = str_replace("{ap_permit_id_".$saved_permit->getTypeId()."_element_grand_child}", ($saved_permit->getPermitId()?$saved_permit->getPermitId():$linked_grand_application->getApplicationId()), $content);
                                                }
                                            }

                                            if($this->find('{fm_child_element_'.$child_element->getElementId().'}', $content))
                                            {
                                                $content = str_replace('{fm_child_element_'.$child_element->getElementId().'}', $linked_grand_application->getApplicationId(), $content);
                                            }

                                            $q = Doctrine_Query::create()
                                                ->from('apFormElements a')
                                                ->where('a.form_id = ?', $child_element->getElementExistingForm())
                                                ->andWhere('a.element_status = ?', 1);

                                            $grand_child_elements = $q->execute();

                                            foreach($grand_child_elements as $grand_child_element)
                                            {

                                                $sql = "SELECT * FROM ap_form_".$child_element->getElementExistingForm()." WHERE id = ".$linked_grand_application->getEntryId();
                                                $child_app_results = mysql_query($sql,$dbconn);

                                                $grand_child_apform = mysql_fetch_assoc($child_app_results);

                                                if($this->find('{ap_grand_child_application_id}', $content))
                                                {
                                                    $content = str_replace('{ap_grand_child_application_id}', $linked_grand_application->getApplicationId(), $content);
                                                }

                                                if($this->find('{fm_grand_child_created_at}', $content))
                                                {
                                                    if($linked_grand_application->getDateOfSubmission())
                                                    {
                                                        $content = str_replace('{fm_grand_child_created_at}', date('d F Y', strtotime($linked_grand_application->getDateOfSubmission())), $content);
                                                    }
                                                    else
                                                    {
                                                        $content = str_replace('{fm_grand_child_created_at}', "", $content);
                                                    }
                                                }

                                                if($this->find('{fm_grand_child_updated_at}', $content))
                                                {
                                                    if($linked_grand_application->getDateOfResponse())
                                                    {
                                                        $content = str_replace('{fm_grand_child_updated_at}', date('d F Y', strtotime(substr($linked_grand_application->getDateOfResponse(), 0, 11))), $content);
                                                    }
                                                    else
                                                    {
                                                        $content = str_replace('{fm_grand_child_updated_at}', "", $content);
                                                    }
                                                }

                                                //START GRAND CHILD ELEMENTS
                                                $childs = $grand_child_element->getElementTotalChild();
                                                if($childs == 0)
                                                {
                                                    if($grand_child_element->getElementType() == "select")
                                                    {//select
                                                        if($this->find('{fm_grand_child_element_'.$grand_child_element->getElementId().'}', $content))
                                                        {
                                                            $opt_value = 0;
                                                            if($grand_child_apform['element_'.$grand_child_element->getElementId()] == "0")
                                                            {
                                                                $opt_value++;
                                                            }
                                                            else
                                                            {
                                                                $opt_value = $grand_child_apform['element_'.$grand_child_element->getElementId()];
                                                            }

                                                            $q = Doctrine_Query::create()
                                                                ->from('ApElementOptions a')
                                                                ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($child_element->getElementExistingForm(),$grand_child_element->getElementId(),$opt_value))
                                                                ->limit(1);
                                                            $option = $q->fetchOne();

                                                            if($option)
                                                            {
                                                                $content = str_replace('{fm_grand_child_element_'.$grand_child_element->getElementId().'}',$option->getOption(), $content);
                                                            }
                                                        }
                                                    }
                                                    elseif($grand_child_element->getElementType() == "checkbox")
                                                    {
                                                        if($this->find('{fm_grand_child_element_'.$grand_child_element->getElementId().'}', $content))
                                                        {
                                                            $q = Doctrine_Query::create()
                                                                ->from('ApElementOptions a')
                                                                ->where('a.form_id = ? AND a.element_id = ?', array($child_element->getElementExistingForm(),$grand_child_element->getElementId()));
                                                            $options = $q->execute();

                                                            $options_text = "";
                                                            foreach($options as $option)
                                                            {
                                                                if($grand_child_apform['element_'.$grand_child_element->getElementId()."_".$option->getOptionId()])
                                                                {
                                                                    $options_text .= "".$option->getOption()."";
                                                                }
                                                            }
                                                            $options_text .= "";

                                                            $content = str_replace('{fm_grand_child_element_'.$grand_child_element->getElementId()."}",$options_text, $content);
                                                        }
                                                    }
                                                    else
                                                    {//text
                                                        if($this->find('{fm_grand_child_element_'.$grand_child_element->getElementId().'}', $content))
                                                        {
                                                            $content = str_replace('{fm_grand_child_element_'.$grand_child_element->getElementId().'}', $grand_child_apform['element_'.$grand_child_element->getElementId()], $content);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    for($x = 0; $x < ($childs + 1); $x++)
                                                    {
                                                        if($this->find('{fm_grand_child_element_'.$grand_child_element->getElementId().'}', $content))
                                                        {
                                                            $content = str_replace('{fm_grand_child_element_'.$grand_child_element->getElementId()."}",$grand_child_apform['element_'.$grand_child_element->getElementId()."_".($x+1)], $content);
                                                        }
                                                    }
                                                }
                                                //END GRAND CHILD ELEMENTS
                                            }

                                        }

                                    }
                                    else
                                    { //select
                                        if($this->find('{fm_child_element_'.$child_element->getElementId().'}', $content))
                                        {
                                            $opt_value = 0;
                                            if($child_apform['element_'.$child_element->getElementId()] == "0")
                                            {
                                                $opt_value++;
                                            }
                                            else
                                            {
                                                $opt_value = $child_apform['element_'.$child_element->getElementId()];
                                            }

                                            $q = Doctrine_Query::create()
                                                ->from('ApElementOptions a')
                                                ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($linked_application->getFormId(),$child_element->getElementId(),$opt_value))
                                                ->limit(1);
                                            $option = $q->fetchOne();

                                            if($option)
                                            {
                                                $content = str_replace('{fm_child_element_'.$child_element->getElementId().'}',$option->getOption(), $content);
                                            }
                                        }
                                    }
                                }
                                elseif($child_element->getElementType() == "checkbox")
                                {
                                    if($this->find('{fm_child_element_'.$child_element->getElementId().'}', $content))
                                    {
                                        $q = Doctrine_Query::create()
                                            ->from('ApElementOptions a')
                                            ->where('a.form_id = ? AND a.element_id = ?', array($linked_application->getFormId(),$child_element->getElementId()));
                                        $options = $q->execute();

                                        $options_text = "";
                                        foreach($options as $option)
                                        {
                                            if($child_apform['element_'.$child_element->getElementId()."_".$option->getOptionId()])
                                            {
                                                $options_text .= "".$option->getOption()."";
                                            }
                                        }
                                        $options_text .= "";

                                        $content = str_replace('{fm_child_element_'.$child_element->getElementId()."}",$options_text, $content);
                                    }
                                    continue;
                                }
                                else
                                { //text
                                    if($this->find('{fm_child_element_'.$child_element->getElementId().'}', $content))
                                    {
                                        if($child_apform['element_'.$child_element->getElementId()])
                                        {
                                            $content = str_replace('{fm_child_element_'.$child_element->getElementId().'}', $child_apform['element_'.$child_element->getElementId()], $content);
                                        }
                                        else
                                        {
                                            $content = str_replace('{fm_child_element_'.$child_element->getElementId().'}', $child_apform['element_'.$child_element->getElementId().'_1'], $content);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                for($x = 0; $x < ($childs + 1); $x++)
                                {
                                    if($this->find('{fm_child_element_'.$child_element->getElementId().'}', $content))
                                    {
                                        $content = str_replace('{fm_child_element_'.$child_element->getElementId()."}",$child_apform['element_'.$child_element->getElementId()."_".($x+1)], $content);
                                    }
                                }
                            }
                            //END CHILD ELEMENTS
                        }


                        continue;
                    }
                }
                else
                {
                    if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                    {
                        $opt_value = 0;
                        if($apform['element_'.$element->getElementId()] == "0")
                        {
                            //$opt_value++;
                        }
                        else
                        {
                            $opt_value = $apform['element_'.$element->getElementId()];
                        }


                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($form_id,$element->getElementId(),$opt_value))
                            ->limit(1);
                        $option = $q->fetchOne();

                        if($option)
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId().'}',$option->getOption(), $content);
                        }
                    }
                }
            }

            $childs = $element->getElementTotalChild();

            if($childs == 0)
            {
                if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                {
                    if($element->getElementType() == "date")
                    {
                        $date = "";
                        if($apform['element_'.$element->getElementId()] && $apform['element_'.$element->getElementId()] != "0000-00-00")
                        {
                            $date = date('d F Y', strtotime($apform['element_'.$element->getElementId()]));
                        }
                        $content = str_replace('{fm_element_'.$element->getElementId().'}', $date, $content);
                    }
                    elseif($element->getElementType() == "europe_date")
                    {
                        $date = "";
                        if($apform['element_'.$element->getElementId()])
                        {
                            $date = date('d F Y', strtotime($apform['element_'.$element->getElementId()]));
                        }
                        $content = str_replace('{fm_element_'.$element->getElementId().'}', $date, $content);
                    }
                    elseif($element->getElementType() == "select" || $element->getElementType() == "radio")
                    {
                        $opt_value = $apform['element_'.$element->getElementId()];

                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($form_id,$element->getElementId(),$opt_value))
                            ->limit(1);
                        $option = $q->fetchOne();

                        if($option)
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId()."}",$option->getOption(), $content);
                        }
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId()."}","-", $content);
                        }
                    }
                    else
                    {
                        $content = str_replace('{fm_element_'.$element->getElementId().'}',$apform['element_'.$element->getElementId()], $content);
                    }
                }
                else
                {
                    for($x = 0; $x < ($childs + 1); $x++)
                    {
                        if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                        }
                    }

                }
            }
            else
            {

                if($element->getElementType() == "select")
                {
                    if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                    {
                        $opt_value = 0;
                        if($apform['element_'.$element->getElementId()] == "0")
                        {
                            $opt_value++;
                        }
                        else
                        {
                            $opt_value = $apform['element_'.$element->getElementId()];
                        }


                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($form_id,$element->getElementId(),$opt_value))
                            ->limit(1);
                        $option = $q->fetchOne();

                        if($option)
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId().'}',$option->getOption(), $content);
                        }
                    }
                }
                else
                {
                    for($x = 0; $x < ($childs + 1); $x++)
                    {
                        if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                        }
                    }

                }




            }
            for($x = 0; $x < ($childs + 1); $x++)
            {
                if($this->find('{fm_element_'.$element->getElementId()."_".($x+1).'}', $content))
                {
                    $content = str_replace('{fm_element_'.$element->getElementId()."_".($x+1).'}',$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                }
            }

        }

        //Comment Sheets
        $q = Doctrine_Query::create()
            ->from("Task a")
            ->where("a.application_id = ?", $saved_application->getId());
        $tasks = $q->execute();

        foreach($tasks as $task)
        {
            $q = Doctrine_Query::create()
                ->from("TaskForms a")
                ->where("a.task_id = ?", $task->getId())
                ->limit(1);
            $taskform = $q->fetchOne();
            if($taskform)
            {
                $sql = "SELECT * FROM ap_form_".$taskform->getFormId()." WHERE id = ".$taskform->getEntryId();
                $app_results = mysql_query($sql,$dbconn);

                $apform = mysql_fetch_assoc($app_results);

                $q = Doctrine_Query::create()
                    ->from('apFormElements a')
                    ->where('a.form_id = ?', $taskform->getFormId());

                $elements = $q->execute();

                foreach($elements as $element)
                {
                    if($element->getElementType() == "simple_name")
                    {
                        if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_1"]." ".$apform['element_'.$element->getElementId()."_2"], $content);
                        }
                        continue;
                    }
                    if($element->getElementType() == "simple_name_wmiddle")
                    {
                        if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_1"]." ".$apform['element_'.$element->getElementId()."_2"]." ".$apform['element_'.$element->getElementId()."_3"], $content);
                        }
                        continue;
                    }
                    if($element->getElementType() == "checkbox")
                    {
                        if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$taskform->getFormId().'}', $content))
                        {
                            $q = Doctrine_Query::create()
                                ->from('ApElementOptions a')
                                ->where('a.form_id = ? AND a.element_id = ?', array($taskform->getFormId(),$element->getElementId()));
                            $options = $q->execute();

                            $options_text = "";
                            foreach($options as $option)
                            {
                                if($apform['element_'.$element->getElementId()."_".$option->getOptionId()])
                                {
                                    $options_text .= "".$option->getOption()."";
                                }
                            }
                            $options_text .= "";

                            $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."}",$options_text, $content);
                        }
                        continue;
                    }

                    if($element->getElementType() == "select")
                    {
                        $opt_value = 0;
                        if($apform['element_'.$element->getElementId()] == "0")
                        {
                            $opt_value++;
                        }
                        else
                        {
                            $opt_value = $apform['element_'.$element->getElementId()];
                        }


                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($taskform->getFormId(),$element->getElementId(),$opt_value))
                            ->limit(1);
                        $option = $q->fetchOne();

                        if($option)
                        {
                            $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}',$option->getOption(), $content);
                        }
                    }


                    $childs = $element->getElementTotalChild();

                    if($childs == 0)
                    {
                        if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}',$apform['element_'.$element->getElementId()], $content);
                        }
                        else
                        {
                            for($x = 0; $x < ($childs + 1); $x++)
                            {
                                if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}', $content))
                                {
                                    $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                                }
                            }

                        }
                    }
                    else
                    {

                        if($element->getElementType() == "select")
                        {
                            if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}', $content))
                            {
                                $opt_value = 0;
                                if($apform['element_'.$element->getElementId()] == "0")
                                {
                                    $opt_value++;
                                }
                                else
                                {
                                    $opt_value = $apform['element_'.$element->getElementId()];
                                }


                                $q = Doctrine_Query::create()
                                    ->from('ApElementOptions a')
                                    ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($taskform->getFormId(),$element->getElementId(),$opt_value))
                                    ->limit(1);
                                $option = $q->fetchOne();

                                if($option)
                                {
                                    $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}',$option->getOption(), $content);
                                }
                            }
                        }
                        else
                        {
                            for($x = 0; $x < ($childs + 1); $x++)
                            {
                                if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}', $content))
                                {
                                    $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                                }
                            }

                        }




                    }
                    for($x = 0; $x < ($childs + 1); $x++)
                    {
                        if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."_".($x+1).'}', $content))
                        {
                            $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."_".($x+1).'}',$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                        }
                    }

                }

            }
        }

        //Get Conditions of Approval (anything starting with ca_ )
        //ca_conditions
        if($this->find('{ca_conditions}', $content))
        {
            $content = str_replace('{ca_conditions}', $conditions , $content);
        }

        if($this->find('{inv_total}', $content))
        {
            $inv_total_amount = 0;

            $invoices = $saved_application->getMfInvoice();
            foreach($invoices as $invoice)
            {
                $inv_total_amount = 0;

                $q = Doctrine_Query::create()
                    ->from('mfInvoiceDetail a')
                    ->where('a.invoice_id = ?', $invoice->getId());
                $details = $q->execute();

                foreach($details as $detail)
                {
                    if(!$this->find('Total', $detail->getDescription()) && !$this->find('Convenience fee', $detail->getDescription()))
                    {
                        $inv_total_amount = $inv_total_amount + $detail->getAmount();
                    }
                }

            }

            $content = str_replace('{inv_total}', $inv_total_amount, $content);
        }

        //Check if permit has a saved_result, if it does then try parsing its variables
        if($saved_permit->getRemoteResult())
        {
            $results = $saved_permit->getRemoteResult();

            $values = json_decode($results, true); // decode to arrays not stdclass to allow parsing nested loops

            $content = static::parseWithDust($content, $values);

        }

        return $content;

    }


    public function parseArchivePermitOld($application_id, $form_id, $entry_id, $permit_id, $content)
    {
        $saved_application = Doctrine_Core::getTable("FormEntryArchive")->find($application_id);

        $saved_permit = Doctrine_Core::getTable("SavedPermitArchive")->find($permit_id);

        if($this->find('{ap_issue_date}', $content))
        {
            if($saved_permit->getDateOfIssue())
            {
                $content = str_replace('{ap_issue_date}', date('d F Y', strtotime($saved_permit->getDateOfIssue())), $content);
            }
            else
            {
                $content = str_replace('{ap_issue_date}', "", $content);
            }
        }

        if($this->find('{ap_permit_id}', $content))
        {
            $content = str_replace('{ap_permit_id}', $saved_permit->getPermitId(), $content);
        }

        if($this->find('{ap_expire_date}', $content))
        {
            if($saved_permit->getDateOfExpiry())
            {
                $content = str_replace('{ap_expire_date}', date('d F Y', strtotime($saved_permit->getDateOfExpiry())), $content);
            }
            else
            {
                $content = str_replace('{ap_expire_date}', "", $content);
            }
        }

        $user = Doctrine_Core::getTable("SfGuardUser")->find($saved_application->getUserId());

        $user_profile = $user->getProfile();

        $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

        $q = Doctrine_Query::create()
            ->from('mfUserProfile a')
            ->where('a.user_id = ?', $user->getId())
            ->limit(1);
        $formprofile = $q->fetchOne();

        $profile_form = "";

        if($formprofile)
        {
            $prof_entry_id = $formprofile->getEntryId();

            $sql = "SELECT * FROM ap_form_15 WHERE id = ".$prof_entry_id;
            $prof_results = mysql_query($sql,$dbconn);

            $profile_form = mysql_fetch_assoc($prof_results);
        }

        $app_entry_id = $saved_application->getEntryId();

        $sql = "SELECT * FROM ap_form_".$form_id." WHERE id = ".$app_entry_id;
        $app_results = mysql_query($sql,$dbconn);

        $apform = mysql_fetch_assoc($app_results);

        $conditions = "";

        $q = Doctrine_Query::create()
            ->from('approvalCondition a')
            ->where('a.entry_id = ?', $saved_application->getId());
        $approvalconditions = $q->execute();
        $conditions = "<ul>";
        foreach($approvalconditions as $approval)
        {
            $conditions = $conditions."<li>".$approval->getCondition()->getShortName().". ".$approval->getCondition()->getDescription()."</li>";
        }
        $q = Doctrine_Query::create()
            ->from('Conditions a')
            ->where('a.circulation_id = ?', $saved_application->getCirculationId());
        $conds = $q->execute();
        foreach($conds as $cond)
        {
            $conditions = $conditions."<li>- ".$cond->getConditionText()."</li>";
        }
        $conditions = $conditions."</ul>";


        //Get User Information (anything starting with sf_ )
        //sf_email, sf_fullname, sf_username, ... other fields in the dynamic user profile form e.g sf_element_1
        if($this->find('{sf_username}', $content))
        {
            $content = str_replace('{sf_username}',$user->getUsername(), $content);
        }
        if($this->find('{sf_mobile}', $content))
        {
            $content = str_replace('{sf_mobile}',$user_profile->getMobile(), $content);
        }
        if($this->find('{sf_email}', $content))
        {
            $content = str_replace('{sf_email}',$user_profile->getEmail(), $content);
        }
        if($this->find('{sf_fullname}', $content))
        {
            $content = str_replace('{sf_fullname}',$user_profile->getFullname(), $content);
        }

        $q = Doctrine_Query::create()
            ->from('apFormElements a')
            ->where('a.form_id = ?', 15);

        $elements = $q->execute();

        foreach($elements as $element)
        {
            $childs = $element->getElementTotalChild();
            if($childs == 0)
            {
                if($this->find('{sf_element_'.$element->getElementId().'}', $content))
                {
                    $content = str_replace('{sf_element_'.$element->getElementId().'}',$profile_form['element_'.$element->getElementId()], $content);
                }
            }
            else
            {
                if($element->getElementType() == "select")
                {
                    if($this->find('{sf_element_'.$element->getElementId().'}', $content))
                    {
                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($profile_form,$element->getElementId(),$profile_form['element_'.$element->getElementId()]))
                            ->limit(1);
                        $option = $q->fetchOne();

                        if($option)
                        {
                            $content = str_replace('{sf_element_'.$element->getElementId().'}',$option->getOption(), $content);
                        }
                    }
                }
                else
                {
                    for($x = 0; $x < ($childs + 1); $x++)
                    {
                        if($this->find('{sf_element_'.$element->getElementId().'_'.($x+1).'}', $content))
                        {
                            $content = str_replace('{sf_element_'.$element->getElementId().'_'.($x+1).'}',$profile_form['element_'.$element->getElementId()."_".($x+1)], $content);
                        }
                    }
                }
            }


        }

        //Get Application Information (anything starting with ap_ )
        //ap_application_id
        if($this->find('{ap_application_id}', $content))
        {
            $content = str_replace('{ap_application_id}',$saved_application->getApplicationId(), $content);
        }

        //Get Form Details (anything starting with fm_ )
        //fm_created_at, fm_updated_at.....fm_element_1


        if($this->find('{fm_created_at}', $content))
        {
            if($saved_application->getDateOfSubmission())
            {
                $content = str_replace('{fm_created_at}', date('d F Y', strtotime($saved_application->getDateOfSubmission())), $content);
            }
            else
            {
                $content = str_replace('{fm_created_at}', "", $content);
            }
        }
        if($this->find('{fm_updated_at}', $content))
        {
            if($saved_application->getDateOfResponse())
            {
                $content = str_replace('{fm_updated_at}', date('d F Y', strtotime(substr($saved_application->getDateOfResponse(), 0, 11))), $content);
            }
            else
            {
                $content = str_replace('{fm_updated_at}', "", $content);
            }
        }
        if($this->find('{current_date}', $content))
        {
            $content = str_replace('{current_date}',date('d F Y', strtotime(date('Y-m-d'))), $content);
        }

        $q = Doctrine_Query::create()
            ->from('apFormElements a')
            ->where('a.form_id = ?', $form_id);

        $elements = $q->execute();

        foreach($elements as $element)
        {
            if($element->getElementType() != "select")
            {
                //Check remote URL and post values
                $remote_url = $element->getElementOptionQuery();
                if(!empty($remote_url))
                {
                    $criteria = $element->getElementFieldName();
                    $remote_value = $element->getElementFieldValue();
                    $remote_username = $element->getElementRemoteUsername();
                    $remote_password = $element->getElementRemotePassword();

                    $pos = strpos($remote_url, '$value');

                    if($pos === false)
                    {
                        //dont' do anything
                    }
                    else
                    {
                        $remote_url = str_replace('$value', $apform['element_'.$element->getElementId()], $remote_url);
                    }

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $remote_url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                    curl_setopt($ch, CURLOPT_USERPWD, "$remote_username:$remote_password");

                    $results = curl_exec($ch);

                    if($error = curl_error($ch))
                    {
                        $error = "remote request error: " .$error . "<br />";
                    }

                    curl_close($ch);

                    if(empty($error))
                    {
                        $values = json_decode($results);
                        if($criteria == "records")
                        {
                            //If count is = 0, fail
                            if($values->{'count'} == 0)
                            {
                                $error = "No records found on server";
                            }
                        }
                        else if($criteria == "norecords")
                        {
                            //If count is greater than 0, then pass
                            if($values->{'count'} > 0)
                            {
                                $error = "Existing records found on server";
                            }
                        }
                        else if($criteria == "value")
                        {
                            if($result != $result_value)
                            {
                                $error = "You input doesn't match any records";
                            }
                        }

                        $entry_value .= "<br>";

                        foreach($values->{'records'} as $record)
                        {
                            foreach($record as $key => $value)
                            {
                                if($this->find('{'.$key.'}', $content))
                                {
                                    $content = str_replace('{'.$key.'}',$value, $content);
                                }
                                if(!is_array($value))
                                {
                                    if($this->find('{'.$key.'}', $content))
                                    {
                                        $content = str_replace('{'.$key.'}',$value, $content);
                                    }
                                }
                                else
                                {
                                    $result_value = "";
                                    foreach($value as $lkey => $lvalue)
                                    {
                                        $result_value = $result_value.$lvalue;
                                    }

                                    if($this->find('{'.$key.'}', $content))
                                    {
                                        $content = str_replace('{'.$key.'}',$result_value, $content);
                                    }
                                }
                            }
                        }


                    }
                    else
                    {
                        error_log($error);
                    }
                }
            }

            if($element->getElementType() == "simple_name")
            {
                if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                {
                    $content = str_replace('{fm_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_1"]." ".$apform['element_'.$element->getElementId()."_2"], $content);
                }
                continue;
            }

            if($element->getElementType() == "simple_name_wmiddle")
            {
                if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                {
                    $content = str_replace('{fm_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_1"]." ".$apform['element_'.$element->getElementId()."_2"]." ".$apform['element_'.$element->getElementId()."_3"], $content);
                }
                continue;
            }

            if($element->getElementType() == "checkbox")
            {
                if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                {
                    $q = Doctrine_Query::create()
                        ->from('ApElementOptions a')
                        ->where('a.form_id = ? AND a.element_id = ?', array($form_id,$element->getElementId()));
                    $options = $q->execute();

                    $options_text = "";
                    foreach($options as $option)
                    {
                        if($apform['element_'.$element->getElementId()."_".$option->getOptionId()])
                        {
                            $options_text .= "".$option->getOption()."";
                        }
                    }


                    if ($apform['element_' . $element->getElementId() . "_other"] != "") {
                        $options_text .= "" . $apform['element_'.$element->getElementId().'_other'] . "";
                    }

                    $options_text .= "";

                    $content = str_replace('{fm_element_'.$element->getElementId()."}",$options_text, $content);
                }
                continue;
            }

            if($element->getElementType() == "select")
            {
                if($element->getElementExistingForm() && $element->getElementExistingStage())
                {
                    $q = Doctrine_Query::create()
                        ->from("FormEntry a")
                        ->where("a.id = ?", $apform['element_'.$element->getElementId()])
                        ->limit(1);
                    $linked_application = $q->fetchOne();
                    if($linked_application)
                    {

                        $q = Doctrine_Query::create()
                            ->from("SavedPermit a")
                            ->leftJoin("a.FormEntry b")
                            ->where("b.form_id = ?", $linked_application->getFormId());
                        $permits = $q->execute();

                        foreach($permits as $saved_permit)
                        {
                            if($this->find("{ap_permit_id_".$saved_permit->getTypeId()."_element_child}", $content))
                            {
                                $content = str_replace("{ap_permit_id_".$saved_permit->getTypeId()."_element_child}", ($saved_permit->getPermitId()?$saved_permit->getPermitId():$linked_application->getApplicationId()), $content);
                            }
                        }

                        if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId().'}', $linked_application->getApplicationId(), $content);
                        }

                        $q = Doctrine_Query::create()
                            ->from('apFormElements a')
                            ->where('a.form_id = ?', $element->getElementExistingForm())
                            ->andWhere('a.element_status = ?', 1);

                        $child_elements = $q->execute();

                        foreach($child_elements as $child_element)
                        {
                            $sql = "SELECT * FROM ap_form_".$linked_application->getFormId()." WHERE id = ".$linked_application->getEntryId();
                            $child_app_results = mysql_query($sql,$dbconn);

                            $child_apform = mysql_fetch_assoc($child_app_results);

                            if($this->find('{ap_child_application_id}', $content))
                            {
                                $content = str_replace('{ap_child_application_id}', $linked_application->getApplicationId(), $content);
                            }

                            if($this->find('{fm_child_created_at}', $content))
                            {
                                if($linked_application->getDateOfSubmission())
                                {
                                    $content = str_replace('{fm_child_created_at}', date('d F Y', strtotime($linked_application->getDateOfSubmission())), $content);
                                }
                                else
                                {
                                    $content = str_replace('{fm_child_created_at}', "", $content);
                                }
                            }

                            if($this->find('{fm_child_updated_at}', $content))
                            {
                                if($linked_application->getDateOfResponse())
                                {
                                    $content = str_replace('{fm_child_updated_at}', date('d F Y', strtotime(substr($linked_application->getDateOfResponse(), 0, 11))), $content);
                                }
                                else
                                {
                                    $content = str_replace('{fm_child_updated_at}', "", $content);
                                }
                            }

                            //START CHILD ELEMENTS
                            $childs = $child_element->getElementTotalChild();
                            if($childs == 0)
                            {
                                if($child_element->getElementType() == "select")
                                {
                                    if($child_element->getElementExistingForm() && $child_element->getElementExistingStage())
                                    {

                                        $q = Doctrine_Query::create()
                                            ->from("FormEntry a")
                                            ->where("a.id = ?", $child_apform['element_'.$child_element->getElementId()])
                                            ->limit(1);
                                        $linked_grand_application = $q->fetchOne();
                                        if($linked_grand_application)
                                        {

                                            $q = Doctrine_Query::create()
                                                ->from("SavedPermit a")
                                                ->leftJoin("a.FormEntry b")
                                                ->where("b.form_id = ?", $linked_grand_application->getFormId());
                                            $permits = $q->execute();

                                            foreach($permits as $saved_permit)
                                            {
                                                if($this->find("{ap_permit_id_".$saved_permit->getTypeId()."_element_grand_child}", $content))
                                                {
                                                    $content = str_replace("{ap_permit_id_".$saved_permit->getTypeId()."_element_grand_child}", ($saved_permit->getPermitId()?$saved_permit->getPermitId():$linked_grand_application->getApplicationId()), $content);
                                                }
                                            }

                                            if($this->find('{fm_child_element_'.$child_element->getElementId().'}', $content))
                                            {
                                                $content = str_replace('{fm_child_element_'.$child_element->getElementId().'}', $linked_grand_application->getApplicationId(), $content);
                                            }

                                            $q = Doctrine_Query::create()
                                                ->from('apFormElements a')
                                                ->where('a.form_id = ?', $child_element->getElementExistingForm())
                                                ->andWhere('a.element_status = ?', 1);

                                            $grand_child_elements = $q->execute();

                                            foreach($grand_child_elements as $grand_child_element)
                                            {

                                                $sql = "SELECT * FROM ap_form_".$child_element->getElementExistingForm()." WHERE id = ".$linked_grand_application->getEntryId();
                                                $child_app_results = mysql_query($sql,$dbconn);

                                                $grand_child_apform = mysql_fetch_assoc($child_app_results);

                                                if($this->find('{ap_grand_child_application_id}', $content))
                                                {
                                                    $content = str_replace('{ap_grand_child_application_id}', $linked_grand_application->getApplicationId(), $content);
                                                }

                                                if($this->find('{fm_grand_child_created_at}', $content))
                                                {
                                                    if($linked_grand_application->getDateOfSubmission())
                                                    {
                                                        $content = str_replace('{fm_grand_child_created_at}', date('d F Y', strtotime($linked_grand_application->getDateOfSubmission())), $content);
                                                    }
                                                    else
                                                    {
                                                        $content = str_replace('{fm_grand_child_created_at}', "", $content);
                                                    }
                                                }

                                                if($this->find('{fm_grand_child_updated_at}', $content))
                                                {
                                                    if($linked_grand_application->getDateOfResponse())
                                                    {
                                                        $content = str_replace('{fm_grand_child_updated_at}', date('d F Y', strtotime(substr($linked_grand_application->getDateOfResponse(), 0, 11))), $content);
                                                    }
                                                    else
                                                    {
                                                        $content = str_replace('{fm_grand_child_updated_at}', "", $content);
                                                    }
                                                }

                                                //START GRAND CHILD ELEMENTS
                                                $childs = $grand_child_element->getElementTotalChild();
                                                if($childs == 0)
                                                {
                                                    if($grand_child_element->getElementType() == "select")
                                                    {//select
                                                        if($this->find('{fm_grand_child_element_'.$grand_child_element->getElementId().'}', $content))
                                                        {
                                                            $opt_value = 0;
                                                            if($grand_child_apform['element_'.$grand_child_element->getElementId()] == "0")
                                                            {
                                                                $opt_value++;
                                                            }
                                                            else
                                                            {
                                                                $opt_value = $grand_child_apform['element_'.$grand_child_element->getElementId()];
                                                            }

                                                            $q = Doctrine_Query::create()
                                                                ->from('ApElementOptions a')
                                                                ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($child_element->getElementExistingForm(),$grand_child_element->getElementId(),$opt_value))
                                                                ->limit(1);
                                                            $option = $q->fetchOne();

                                                            if($option)
                                                            {
                                                                $content = str_replace('{fm_grand_child_element_'.$grand_child_element->getElementId().'}',$option->getOption(), $content);
                                                            }
                                                        }
                                                    }
                                                    elseif($grand_child_element->getElementType() == "checkbox")
                                                    {
                                                        if($this->find('{fm_grand_child_element_'.$grand_child_element->getElementId().'}', $content))
                                                        {
                                                            $q = Doctrine_Query::create()
                                                                ->from('ApElementOptions a')
                                                                ->where('a.form_id = ? AND a.element_id = ?', array($child_element->getElementExistingForm(),$grand_child_element->getElementId()));
                                                            $options = $q->execute();

                                                            $options_text = "";
                                                            foreach($options as $option)
                                                            {
                                                                if($grand_child_apform['element_'.$grand_child_element->getElementId()."_".$option->getOptionId()])
                                                                {
                                                                    $options_text .= "".$option->getOption()."";
                                                                }
                                                            }
                                                            $options_text .= "";

                                                            $content = str_replace('{fm_grand_child_element_'.$grand_child_element->getElementId()."}",$options_text, $content);
                                                        }
                                                    }
                                                    else
                                                    {//text
                                                        if($this->find('{fm_grand_child_element_'.$grand_child_element->getElementId().'}', $content))
                                                        {
                                                            $content = str_replace('{fm_grand_child_element_'.$grand_child_element->getElementId().'}', $grand_child_apform['element_'.$grand_child_element->getElementId()], $content);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    for($x = 0; $x < ($childs + 1); $x++)
                                                    {
                                                        if($this->find('{fm_grand_child_element_'.$grand_child_element->getElementId().'}', $content))
                                                        {
                                                            $content = str_replace('{fm_grand_child_element_'.$grand_child_element->getElementId()."}",$grand_child_apform['element_'.$grand_child_element->getElementId()."_".($x+1)], $content);
                                                        }
                                                    }
                                                }
                                                //END GRAND CHILD ELEMENTS
                                            }

                                        }

                                    }
                                    else
                                    { //select
                                        if($this->find('{fm_child_element_'.$child_element->getElementId().'}', $content))
                                        {
                                            $opt_value = 0;
                                            if($child_apform['element_'.$child_element->getElementId()] == "0")
                                            {
                                                $opt_value++;
                                            }
                                            else
                                            {
                                                $opt_value = $child_apform['element_'.$child_element->getElementId()];
                                            }

                                            $q = Doctrine_Query::create()
                                                ->from('ApElementOptions a')
                                                ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($linked_application->getFormId(),$child_element->getElementId(),$opt_value))
                                                ->limit(1);
                                            $option = $q->fetchOne();

                                            if($option)
                                            {
                                                $content = str_replace('{fm_child_element_'.$child_element->getElementId().'}',$option->getOption(), $content);
                                            }
                                        }
                                    }
                                }
                                elseif($child_element->getElementType() == "checkbox")
                                {
                                    if($this->find('{fm_child_element_'.$child_element->getElementId().'}', $content))
                                    {
                                        $q = Doctrine_Query::create()
                                            ->from('ApElementOptions a')
                                            ->where('a.form_id = ? AND a.element_id = ?', array($linked_application->getFormId(),$child_element->getElementId()));
                                        $options = $q->execute();

                                        $options_text = "";
                                        foreach($options as $option)
                                        {
                                            if($child_apform['element_'.$child_element->getElementId()."_".$option->getOptionId()])
                                            {
                                                $options_text .= "".$option->getOption()."";
                                            }
                                        }
                                        $options_text .= "";

                                        $content = str_replace('{fm_child_element_'.$child_element->getElementId()."}",$options_text, $content);
                                    }
                                    continue;
                                }
                                else
                                { //text
                                    if($this->find('{fm_child_element_'.$child_element->getElementId().'}', $content))
                                    {
                                        if($child_apform['element_'.$child_element->getElementId()])
                                        {
                                            $content = str_replace('{fm_child_element_'.$child_element->getElementId().'}', $child_apform['element_'.$child_element->getElementId()], $content);
                                        }
                                        else
                                        {
                                            $content = str_replace('{fm_child_element_'.$child_element->getElementId().'}', $child_apform['element_'.$child_element->getElementId().'_1'], $content);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                for($x = 0; $x < ($childs + 1); $x++)
                                {
                                    if($this->find('{fm_child_element_'.$child_element->getElementId().'}', $content))
                                    {
                                        $content = str_replace('{fm_child_element_'.$child_element->getElementId()."}",$child_apform['element_'.$child_element->getElementId()."_".($x+1)], $content);
                                    }
                                }
                            }
                            //END CHILD ELEMENTS
                        }


                        continue;
                    }
                }
                else
                {
                    if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                    {
                        $opt_value = 0;
                        if($apform['element_'.$element->getElementId()] == "0")
                        {
                            //$opt_value++;
                        }
                        else
                        {
                            $opt_value = $apform['element_'.$element->getElementId()];
                        }


                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($form_id,$element->getElementId(),$opt_value))
                            ->limit(1);
                        $option = $q->fetchOne();

                        if($option)
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId().'}',$option->getOption(), $content);
                        }
                    }
                }
            }

            $childs = $element->getElementTotalChild();

            if($childs == 0)
            {
                if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                {
                    if($element->getElementType() == "date")
                    {
                        $date = "";
                        if($apform['element_'.$element->getElementId()] && $apform['element_'.$element->getElementId()] != "0000-00-00")
                        {
                            $date = date('d F Y', strtotime($apform['element_'.$element->getElementId()]));
                        }
                        $content = str_replace('{fm_element_'.$element->getElementId().'}', $date, $content);
                    }
                    elseif($element->getElementType() == "europe_date")
                    {
                        $date = "";
                        if($apform['element_'.$element->getElementId()])
                        {
                            $date = date('d F Y', strtotime($apform['element_'.$element->getElementId()]));
                        }
                        $content = str_replace('{fm_element_'.$element->getElementId().'}', $date, $content);
                    }
                    elseif($element->getElementType() == "select" || $element->getElementType() == "radio")
                    {
                        $opt_value = $apform['element_'.$element->getElementId()];

                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($form_id,$element->getElementId(),$opt_value))
                            ->limit(1);
                        $option = $q->fetchOne();

                        if($option)
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId()."}",$option->getOption(), $content);
                        }
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId()."}","-", $content);
                        }
                    }
                    else
                    {
                        $content = str_replace('{fm_element_'.$element->getElementId().'}',$apform['element_'.$element->getElementId()], $content);
                    }
                }
                else
                {
                    for($x = 0; $x < ($childs + 1); $x++)
                    {
                        if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                        }
                    }

                }
            }
            else
            {

                if($element->getElementType() == "select")
                {
                    if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                    {
                        $opt_value = 0;
                        if($apform['element_'.$element->getElementId()] == "0")
                        {
                            $opt_value++;
                        }
                        else
                        {
                            $opt_value = $apform['element_'.$element->getElementId()];
                        }


                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($form_id,$element->getElementId(),$opt_value))
                            ->limit(1);
                        $option = $q->fetchOne();

                        if($option)
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId().'}',$option->getOption(), $content);
                        }
                    }
                }
                else
                {
                    for($x = 0; $x < ($childs + 1); $x++)
                    {
                        if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                        }
                    }

                }




            }
            for($x = 0; $x < ($childs + 1); $x++)
            {
                if($this->find('{fm_element_'.$element->getElementId()."_".($x+1).'}', $content))
                {
                    $content = str_replace('{fm_element_'.$element->getElementId()."_".($x+1).'}',$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                }
            }

        }

        //Comment Sheets
        $q = Doctrine_Query::create()
            ->from("Task a")
            ->where("a.application_id = ?", $saved_application->getId());
        $tasks = $q->execute();

        foreach($tasks as $task)
        {
            $q = Doctrine_Query::create()
                ->from("TaskForms a")
                ->where("a.task_id = ?", $task->getId())
                ->limit(1);
            $taskform = $q->fetchOne();
            if($taskform)
            {
                $sql = "SELECT * FROM ap_form_".$taskform->getFormId()." WHERE id = ".$taskform->getEntryId();
                $app_results = mysql_query($sql,$dbconn);

                $apform = mysql_fetch_assoc($app_results);

                $q = Doctrine_Query::create()
                    ->from('apFormElements a')
                    ->where('a.form_id = ?', $taskform->getFormId());

                $elements = $q->execute();

                foreach($elements as $element)
                {
                    if($element->getElementType() == "simple_name")
                    {
                        if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_1"]." ".$apform['element_'.$element->getElementId()."_2"], $content);
                        }
                        continue;
                    }
                    if($element->getElementType() == "simple_name_wmiddle")
                    {
                        if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_1"]." ".$apform['element_'.$element->getElementId()."_2"]." ".$apform['element_'.$element->getElementId()."_3"], $content);
                        }
                        continue;
                    }
                    if($element->getElementType() == "checkbox")
                    {
                        if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$taskform->getFormId().'}', $content))
                        {
                            $q = Doctrine_Query::create()
                                ->from('ApElementOptions a')
                                ->where('a.form_id = ? AND a.element_id = ?', array($taskform->getFormId(),$element->getElementId()));
                            $options = $q->execute();

                            $options_text = "";
                            foreach($options as $option)
                            {
                                if($apform['element_'.$element->getElementId()."_".$option->getOptionId()])
                                {
                                    $options_text .= "".$option->getOption()."";
                                }
                            }
                            $options_text .= "";

                            $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."}",$options_text, $content);
                        }
                        continue;
                    }

                    if($element->getElementType() == "select")
                    {
                        $opt_value = 0;
                        if($apform['element_'.$element->getElementId()] == "0")
                        {
                            $opt_value++;
                        }
                        else
                        {
                            $opt_value = $apform['element_'.$element->getElementId()];
                        }


                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($taskform->getFormId(),$element->getElementId(),$opt_value))
                            ->limit(1);
                        $option = $q->fetchOne();

                        if($option)
                        {
                            $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}',$option->getOption(), $content);
                        }
                    }


                    $childs = $element->getElementTotalChild();

                    if($childs == 0)
                    {
                        if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}',$apform['element_'.$element->getElementId()], $content);
                        }
                        else
                        {
                            for($x = 0; $x < ($childs + 1); $x++)
                            {
                                if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}', $content))
                                {
                                    $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                                }
                            }

                        }
                    }
                    else
                    {

                        if($element->getElementType() == "select")
                        {
                            if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}', $content))
                            {
                                $opt_value = 0;
                                if($apform['element_'.$element->getElementId()] == "0")
                                {
                                    $opt_value++;
                                }
                                else
                                {
                                    $opt_value = $apform['element_'.$element->getElementId()];
                                }


                                $q = Doctrine_Query::create()
                                    ->from('ApElementOptions a')
                                    ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($taskform->getFormId(),$element->getElementId(),$opt_value))
                                    ->limit(1);
                                $option = $q->fetchOne();

                                if($option)
                                {
                                    $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}',$option->getOption(), $content);
                                }
                            }
                        }
                        else
                        {
                            for($x = 0; $x < ($childs + 1); $x++)
                            {
                                if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}', $content))
                                {
                                    $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                                }
                            }

                        }




                    }
                    for($x = 0; $x < ($childs + 1); $x++)
                    {
                        if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."_".($x+1).'}', $content))
                        {
                            $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."_".($x+1).'}',$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                        }
                    }

                }

            }
        }

        //Get Conditions of Approval (anything starting with ca_ )
        //ca_conditions
        if($this->find('{ca_conditions}', $content))
        {
            $content = str_replace('{ca_conditions}', $conditions , $content);
        }

        if($this->find('{inv_total}', $content))
        {
            $inv_total_amount = 0;

            $invoices = $saved_application->getMfInvoice();
            foreach($invoices as $invoice)
            {
                $inv_total_amount = 0;

                $q = Doctrine_Query::create()
                    ->from('mfInvoiceDetail a')
                    ->where('a.invoice_id = ?', $invoice->getId());
                $details = $q->execute();

                foreach($details as $detail)
                {
                    if(!$this->find('Total', $detail->getDescription()) && !$this->find('Convenience fee', $detail->getDescription()))
                    {
                        $inv_total_amount = $inv_total_amount + $detail->getAmount();
                    }
                }

            }

            $content = str_replace('{inv_total}', $inv_total_amount, $content);
        }

        //Check if permit has a saved_result, if it does then try parsing its variables
        if($saved_permit->getRemoteResult())
        {
            $results = $saved_permit->getRemoteResult();

            $values = json_decode($results, true); // decode to arrays not stdclass to allow parsing nested loops

            $content = static::parseWithDust($content, $values);

        }

        return $content;

    }

    public function parsePublicPermit($content, $reference, $remote_data)
    {
        if($this->find('{current_date}', $content))
        {
            $content = str_replace('{current_date}', date('d F Y'), $content);
        }

        $values = json_decode($remote_data, true);
        $content = static::parseWithDust($content, $values['records'][0]);
        return $content;
    }

    /***
     * Use Dust template engine to parse the template
     *
     * @param $content string the string that contains placeholders to be replaced
     * @param $values array Array of values to be used to replace placeholders
     * @return string
     */
    public static function parseWithDust($content, $values)
    {
        //create object
        $dust = new \Dust\Dust();

        // attach a date filter
        $dust->filters['date'] = new DateFilter();

        //compile a template
        $template = $dust->compile($content);

        //render the template
        return $dust->renderTemplate($template, $values);
    }

    public function parseRemote($application_id, $form_id, $entry_id, $permit_id, $content){
        $saved_application = Doctrine_Core::getTable("FormEntry")->find($application_id);

        $saved_permit = Doctrine_Core::getTable("SavedPermit")->find($permit_id);

        $app_entry_id = $saved_application->getEntryId();

        $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

        $sql = "SELECT * FROM ap_form_".$form_id." WHERE id = ".$app_entry_id;
        $app_results = mysql_query($sql,$dbconn);

        $apform = mysql_fetch_assoc($app_results);

        $sql = "SELECT * FROM ap_settings WHERE id = 1";
        $app_results = mysql_query($sql,$dbconn);

        $apsettings = mysql_fetch_assoc($app_results);

        if($this->find('{ap_issue_date}', $content))
        {
            if($saved_permit->getDateOfIssue())
            {
                $content = str_replace('{ap_issue_date}', date('d F Y', strtotime($saved_permit->getDateOfIssue())), $content);
            }
            else
            {
                $content = str_replace('{ap_issue_date}', "", $content);
            }
        }

        if($this->find('{ap_application_id}', $content))
        {
            $content = str_replace('{ap_application_id}', $saved_application->getApplicationId(), $content);
        }

        if($this->find('{ap_permit_id}', $content))
        {
            $content = str_replace('{ap_permit_id}', $saved_permit->getPermitId(), $content);
        }

        if($this->find('{ap_expire_date}', $content))
        {
            if($saved_permit->getDateOfExpiry())
            {
                $content = str_replace('{ap_expire_date}', date('d F Y', strtotime($saved_permit->getDateOfExpiry())), $content);
            }
            else
            {
                $content = str_replace('{ap_expire_date}', "", $content);
            }
        }

        $user = Doctrine_Core::getTable("SfGuardUser")->find($saved_application->getUserId());

        $user_profile = $user->getProfile();

        //Get User Information (anything starting with sf_ )
        //sf_email, sf_fullname, sf_username, ... other fields in the dynamic user profile form e.g sf_element_1
        if($this->find('{sf_username}', $content))
        {
            $content = str_replace('{sf_username}',$user->getUsername(), $content);
        }
        if($this->find('{sf_email}', $content))
        {
            $content = str_replace('{sf_email}',$user_profile->getEmail(), $content);
        }
        if($this->find('{sf_mobile}', $content))
        {
            $content = str_replace('{sf_mobile}',$user_profile->getMobile(), $content);
        }
        if($this->find('{sf_fullname}', $content))
        {
            $content = str_replace('{sf_fullname}',$user_profile->getFullname(), $content);
        }
        if($this->find('{sf_user_category}', $content))
        {
            $content = str_replace('{sf_user_category}',$user_profile->getRegisteras(), $content);
        }


        if($this->find('{current_date}', $content))
        {
            $content = str_replace('{current_date}',date('d F Y', strtotime(date('Y-m-d'))), $content);
        }

        $q = Doctrine_Query::create()
            ->from('apFormElements a')
            ->where('a.form_id = ?', $form_id);

        $elements = $q->execute();

        foreach($elements as $element)
        {
            if($element->getElementType() == "simple_name")
            {
                if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                {
                    $content = str_replace('{fm_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_1"]." ".$apform['element_'.$element->getElementId()."_2"], $content);
                }
                continue;
            }

            if($element->getElementType() == "simple_name_wmiddle")
            {
                if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                {
                    $content = str_replace('{fm_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_1"]." ".$apform['element_'.$element->getElementId()."_2"]." ".$apform['element_'.$element->getElementId()."_3"], $content);
                }
                continue;
            }

            if($element->getElementType() == "file")
            {
                if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                {
                  $content = str_replace('{fm_element_'.$element->getElementId()."}", "https://".$_SERVER['HTTP_HOST']."/".$apsettings['data_dir']."/form_".$element->getFormId()."/files/".$apform['element_'.$element->getElementId()], $content);
                }
                continue;
            }

            if($element->getElementType() == "checkbox")
            {
                if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                {
                    $q = Doctrine_Query::create()
                        ->from('ApElementOptions a')
                        ->where('a.form_id = ? AND a.element_id = ?', array($form_id,$element->getElementId()));
                    $options = $q->execute();

                    $options_text = "";
                    foreach($options as $option)
                    {
                        if($apform['element_'.$element->getElementId()."_".$option->getOptionId()])
                        {
                            $options_text .= "".$option->getOption()."";
                        }
                    }
                    $options_text .= "";

                    $content = str_replace('{fm_element_'.$element->getElementId()."}",$options_text, $content);
                }
                continue;
            }

            if($element->getElementType() == "select")
            {
                if($element->getElementExistingForm() && $element->getElementExistingStage())
                {
                    $q = Doctrine_Query::create()
                        ->from("FormEntry a")
                        ->where("a.id = ?", $apform['element_'.$element->getElementId()])
                        ->limit(1);
                    $linked_application = $q->fetchOne();
                    if($q->count() > 0)
                    {

                        if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId().'}', $linked_application->getApplicationId(), $content);
                        }

                        $q = Doctrine_Query::create()
                            ->from('apFormElements a')
                            ->where('a.form_id = ?', $element->getElementExistingForm())
                            ->andWhere('a.element_status = ?', 1);

                        $child_elements = $q->execute();

                        foreach($child_elements as $child_element)
                        {
                            $sql = "SELECT * FROM ap_form_".$linked_application->getFormId()." WHERE id = ".$linked_application->getEntryId();
                            $child_app_results = mysql_query($sql,$dbconn);

                            $child_apform = mysql_fetch_assoc($child_app_results);

                            if($this->find('{ap_child_application_id}', $content))
                            {
                                $content = str_replace('{ap_child_application_id}', $linked_application->getApplicationId(), $content);
                            }

                            if($this->find('{fm_child_created_at}', $content))
                            {
                                if($linked_application->getDateOfSubmission())
                                {
                                    $content = str_replace('{fm_child_created_at}', date('d F Y', strtotime($linked_application->getDateOfSubmission())), $content);
                                }
                                else
                                {
                                    $content = str_replace('{fm_child_created_at}', "", $content);
                                }
                            }

                            if($this->find('{fm_child_updated_at}', $content))
                            {
                                if($linked_application->getDateOfResponse())
                                {
                                    $content = str_replace('{fm_child_updated_at}', date('d F Y', strtotime(substr($linked_application->getDateOfResponse(), 0, 11))), $content);
                                }
                                else
                                {
                                    $content = str_replace('{fm_child_updated_at}', "", $content);
                                }
                            }

                            //START CHILD ELEMENTS
                            $childs = $child_element->getElementTotalChild();
                            if($childs == 0)
                            {
                                if($child_element->getElementType() == "select")
                                {
                                    if($child_element->getElementExistingForm() && $child_element->getElementExistingStage())
                                    {

                                        $q = Doctrine_Query::create()
                                            ->from("FormEntry a")
                                            ->where("a.id = ?", $child_apform['element_'.$child_element->getElementId()])
                                            ->limit(1);
                                        $linked_grand_application = $q->fetchOne();
                                        if($linked_grand_application)
                                        {

                                            if($this->find('{fm_child_element_'.$child_element->getElementId().'}', $content))
                                            {
                                                $content = str_replace('{fm_child_element_'.$child_element->getElementId().'}', $linked_grand_application->getApplicationId(), $content);
                                            }

                                            $q = Doctrine_Query::create()
                                                ->from('apFormElements a')
                                                ->where('a.form_id = ?', $child_element->getElementExistingForm())
                                                ->andWhere('a.element_status = ?', 1);

                                            $grand_child_elements = $q->execute();

                                            foreach($grand_child_elements as $grand_child_element)
                                            {

                                                $sql = "SELECT * FROM ap_form_".$child_element->getElementExistingForm()." WHERE id = ".$linked_grand_application->getEntryId();
                                                $child_app_results = mysql_query($sql,$dbconn);

                                                $grand_child_apform = mysql_fetch_assoc($child_app_results);

                                                if($this->find('{ap_grand_child_application_id}', $content))
                                                {
                                                    $content = str_replace('{ap_grand_child_application_id}', $linked_grand_application->getApplicationId(), $content);
                                                }

                                                if($this->find('{fm_grand_child_created_at}', $content))
                                                {
                                                    if($linked_grand_application->getDateOfSubmission())
                                                    {
                                                        $content = str_replace('{fm_grand_child_created_at}', date('d F Y', strtotime($linked_grand_application->getDateOfSubmission())), $content);
                                                    }
                                                    else
                                                    {
                                                        $content = str_replace('{fm_grand_child_created_at}', "", $content);
                                                    }
                                                }

                                                if($this->find('{fm_grand_child_updated_at}', $content))
                                                {
                                                    if($linked_grand_application->getDateOfResponse())
                                                    {
                                                        $content = str_replace('{fm_grand_child_updated_at}', date('d F Y', strtotime(substr($linked_grand_application->getDateOfResponse(), 0, 11))), $content);
                                                    }
                                                    else
                                                    {
                                                        $content = str_replace('{fm_grand_child_updated_at}', "", $content);
                                                    }
                                                }

                                                //START GRAND CHILD ELEMENTS
                                                $childs = $grand_child_element->getElementTotalChild();
                                                if($childs == 0)
                                                {
                                                    if($grand_child_element->getElementType() == "select")
                                                    {//select
                                                        if($this->find('{fm_grand_child_element_'.$grand_child_element->getElementId().'}', $content))
                                                        {
                                                            $opt_value = 0;
                                                            if($grand_child_apform['element_'.$grand_child_element->getElementId()] == "0")
                                                            {
                                                                $opt_value++;
                                                            }
                                                            else
                                                            {
                                                                $opt_value = $grand_child_apform['element_'.$grand_child_element->getElementId()];
                                                            }

                                                            $q = Doctrine_Query::create()
                                                                ->from('ApElementOptions a')
                                                                ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($child_element->getElementExistingForm(),$grand_child_element->getElementId(),$opt_value))
                                                                ->limit(1);
                                                            $option = $q->fetchOne();

                                                            if($option)
                                                            {
                                                                $content = str_replace('{fm_grand_child_element_'.$grand_child_element->getElementId().'}',$option->getOption(), $content);
                                                            }
                                                        }
                                                    }
                                                    elseif($grand_child_element->getElementType() == "checkbox")
                                                    {
                                                        if($this->find('{fm_grand_child_element_'.$grand_child_element->getElementId().'}', $content))
                                                        {
                                                            $q = Doctrine_Query::create()
                                                                ->from('ApElementOptions a')
                                                                ->where('a.form_id = ? AND a.element_id = ?', array($child_element->getElementExistingForm(),$grand_child_element->getElementId()));
                                                            $options = $q->execute();

                                                            $last_option = "";

                                                            $options_text = "";
                                                            foreach($options as $option)
                                                            {
                                                                if($grand_child_apform['element_'.$grand_child_element->getElementId()."_".$option->getOptionId()])
                                                                {
                                                                    $options_text .= "".$option->getOption()."";
                                                                }
                                                                $last_option = $option->getOption();
                                                            }

                                                            //if other option is filled then get last option and increment by 1
                                                            if($this->find('{fm_grand_child_element_'.$grand_child_element->getElementId().'_other}', $content))
                                                            {
                                                                //Ensure other field is not empty before adding option
                                                                if($grand_child_apform['element_'.$grand_child_element->getElementId().'_other'])
                                                                {
                                                                  $options_text .= "I";
                                                                }
                                                            }


                                                            $options_text .= "";

                                                            $content = str_replace('{fm_grand_child_element_'.$grand_child_element->getElementId()."}",$options_text, $content);
                                                        }

                                                        if($this->find('{fm_grand_child_element_'.$grand_child_element->getElementId().'_other}', $content))
                                                        {
                                                          $content = str_replace('{fm_grand_child_element_'.$grand_child_element->getElementId()."_other}",$grand_child_apform['element_'.$grand_child_element->getElementId()."_other"], $content);
                                                        }
                                                    }
                                                    else
                                                    {//text
                                                        if($this->find('{fm_grand_child_element_'.$grand_child_element->getElementId().'}', $content))
                                                        {
                                                            $content = str_replace('{fm_grand_child_element_'.$grand_child_element->getElementId().'}', $grand_child_apform['element_'.$grand_child_element->getElementId()], $content);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    for($x = 0; $x < ($childs + 1); $x++)
                                                    {
                                                        if($this->find('{fm_grand_child_element_'.$grand_child_element->getElementId().'}', $content))
                                                        {
                                                            $content = str_replace('{fm_grand_child_element_'.$grand_child_element->getElementId()."}",$grand_child_apform['element_'.$grand_child_element->getElementId()."_".($x+1)], $content);
                                                        }
                                                    }
                                                }
                                                //END GRAND CHILD ELEMENTS
                                            }

                                        }

                                    }
                                    else
                                    { //select
                                        if($this->find('{fm_child_element_'.$child_element->getElementId().'}', $content))
                                        {
                                            $opt_value = 0;
                                            if($child_apform['element_'.$child_element->getElementId()] == "0")
                                            {
                                                $opt_value++;
                                            }
                                            else
                                            {
                                                $opt_value = $child_apform['element_'.$child_element->getElementId()];
                                            }

                                            $q = Doctrine_Query::create()
                                                ->from('ApElementOptions a')
                                                ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($linked_application->getFormId(),$child_element->getElementId(),$opt_value))
                                                ->limit(1);
                                            $option = $q->fetchOne();

                                            if($option)
                                            {
                                                $content = str_replace('{fm_child_element_'.$child_element->getElementId().'}',$option->getOption(), $content);
                                            }
                                        }
                                    }
                                }
                                elseif($child_element->getElementType() == "checkbox")
                                {
                                    if($this->find('{fm_child_element_'.$child_element->getElementId().'}', $content))
                                    {
                                        $q = Doctrine_Query::create()
                                            ->from('ApElementOptions a')
                                            ->where('a.form_id = ? AND a.element_id = ?', array($linked_application->getFormId(),$child_element->getElementId()));
                                        $options = $q->execute();

                                        $options_text = "";
                                        foreach($options as $option)
                                        {
                                            if($child_apform['element_'.$child_element->getElementId()."_".$option->getOptionId()])
                                            {
                                                $options_text .= "".$option->getOption().",";
                                            }
                                        }
                                        $options_text .= "";

                                        $content = str_replace('{fm_child_element_'.$child_element->getElementId()."}",$options_text, $content);
                                    }
                                    continue;
                                }
                                else
                                { //text
                                    if($this->find('{fm_child_element_'.$child_element->getElementId().'}', $content))
                                    {
                                        if($child_apform['element_'.$child_element->getElementId()])
                                        {
                                            $content = str_replace('{fm_child_element_'.$child_element->getElementId().'}', $child_apform['element_'.$child_element->getElementId()], $content);
                                        }
                                        else
                                        {
                                            $content = str_replace('{fm_child_element_'.$child_element->getElementId().'}', $child_apform['element_'.$child_element->getElementId().'_1'], $content);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                for($x = 0; $x < ($childs + 1); $x++)
                                {
                                    if($this->find('{fm_child_element_'.$child_element->getElementId().'}', $content))
                                    {
                                        $content = str_replace('{fm_child_element_'.$child_element->getElementId()."}",$child_apform['element_'.$child_element->getElementId()."_".($x+1)], $content);
                                    }
                                }
                            }
                            //END CHILD ELEMENTS
                        }


                        continue;
                    }
                }
                elseif($element->getElementTableName())
                {
                   $content = str_replace('{fm_element_'.$element->getElementId().'}',$apform['element_'.$element->getElementId()], $content);
                }
                else
                {
                    if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                    {
                        $opt_value = 0;
                        if($apform['element_'.$element->getElementId()] == "0")
                        {
                            $opt_value++;
                        }
                        else
                        {
                            $opt_value = $apform['element_'.$element->getElementId()];
                        }


                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($form_id,$element->getElementId(),$opt_value))
                            ->limit(1);
                        $option = $q->fetchOne();

                        if($option)
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId().'}',$option->getOption(), $content);
                        }
                    }
                }
            }

            $childs = $element->getElementTotalChild();

            if($childs == 0)
            {
                if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                {
                    if($element->getElementType() == "date")
                    {
                        $date = "";
                        if($apform['element_'.$element->getElementId()])
                        {
                            $date = date('d F Y', strtotime($apform['element_'.$element->getElementId()]));
                        }
                        $content = str_replace('{fm_element_'.$element->getElementId().'}', $date, $content);
                    }
                    elseif($element->getElementType() == "europe_date")
                    {
                        $date = "";
                        if($apform['element_'.$element->getElementId()])
                        {
                            $date = date('d F Y', strtotime($apform['element_'.$element->getElementId()]));
                        }
                        $content = str_replace('{fm_element_'.$element->getElementId().'}', $date, $content);
                    }
                    elseif($element->getElementType() == "select" || $element->getElementType() == "radio")
                    {
                        $opt_value = $apform['element_'.$element->getElementId()];

                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($form_id,$element->getElementId(),$opt_value))
                            ->limit(1);
                        $option = $q->fetchOne();

                        if($option)
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId()."}",$option->getOption(), $content);
                        }
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId()."}","-", $content);
                        }
                    }
                    else
                    {
                        $content = str_replace('{fm_element_'.$element->getElementId().'}',$apform['element_'.$element->getElementId()], $content);
                    }
                }
                else
                {
                    for($x = 0; $x < ($childs + 1); $x++)
                    {
                        if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                        }
                    }

                }
            }
            else
            {

                if($element->getElementType() == "select")
                {
                    if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                    {
                        $opt_value = 0;
                        if($apform['element_'.$element->getElementId()] == "0")
                        {
                            $opt_value++;
                        }
                        else
                        {
                            $opt_value = $apform['element_'.$element->getElementId()];
                        }


                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($form_id,$element->getElementId(),$opt_value))
                            ->limit(1);
                        $option = $q->fetchOne();

                        if($option)
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId().'}',$option->getOption(), $content);
                        }
                    }
                }
                else
                {
                    for($x = 0; $x < ($childs + 1); $x++)
                    {
                        if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                        }
                    }

                }




            }
            for($x = 0; $x < ($childs + 1); $x++)
            {
                if($this->find('{fm_element_'.$element->getElementId()."_".($x+1).'}', $content))
                {
                    $content = str_replace('{fm_element_'.$element->getElementId()."_".($x+1).'}',$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                }
            }

        }

        foreach($elements as $element)
        {
            if($element->getElementType() == "text")
            {
                $error_found = false;

                if($this->find("Error", $saved_application->getObservation()) || $this->find("Error", $saved_application->getObservation()))
                {
                    $error_found = true;
                }

                if($saved_application->getObservation() == "" || $error_found) {
                    //If this field has remote validation configured then parse results
                    $updates_manager = new UpdatesManager();

                    $remote_url = $element->getElementOptionQuery();
                    $remote_username = $element->getElementRemoteUsername();
                    $remote_password = $element->getElementRemotePassword();
                    $remote_template = $element->getElementFieldValue();
                    $remote_criteria = $element->getElementFieldName();
                    $remote_post = $element->getElementRemotePost();
                    $remote_value = $apform['element_' . $element->getElementId()];

                    $q = Doctrine_Query::create()
                        ->from("MfInvoice a")
                        ->where("a.app_id = ?", $application_id)
                        ->andWhere("a.paid = 2");
                    $invoices = $q->execute();
                    foreach ($invoices as $invoice) {
                        if ($this->find('$invoice_number', $remote_post)) {
                            $remote_post = str_replace('$invoice_number', $invoice->getFormEntry()->getFormId() . "/" . $invoice->getFormEntry()->getEntryId() . "/" . $invoice->getId(), $remote_post);
                            error_log("Updates Manager -. Pull Info: Found Billing Reference for " . $invoice->getId());
                            break;
                        }
                    }

                    $client_username = $user->getUsername();

                    $results = $updates_manager->pull_update_raw($remote_url, $remote_username, $remote_password, $remote_template, $remote_criteria, $remote_value, $remote_post, $client_username);
                    error_log("Updates Manager -> Pull Info: Pre-Push 1: " . $results);

                    if ($saved_application->getObservation() == "" || $saved_application->getObservation() == "error") {
                        if ($results != "error") {
                            error_log("Updates Manager -> Pull Info 2: Updated FormEntry Results: " . $results);
                            $saved_application->setObservation($results);
                            $saved_application->save();
                        }
                    }

                    $results_array = json_decode($results, true);

                    if (sizeof($results_array['results']) > 0) {
                        $content = static::parseWithDust($content, $results_array['results'][0]);
                        error_log("Updates Manager -> Pull Info: Pre-Push 2: " . $content);
                    }
                }
                else
                {

                }
            }
        }

        //Comment Sheets
        $q = Doctrine_Query::create()
            ->from("Task a")
            ->where("a.application_id = ?", $saved_application->getId());
        $tasks = $q->execute();

        foreach($tasks as $task)
        {
            $q = Doctrine_Query::create()
                ->from("TaskForms a")
                ->where("a.task_id = ?", $task->getId())
                ->limit(1);
            $taskform = $q->fetchOne();
            if($taskform)
            {
                $sql = "SELECT * FROM ap_form_".$taskform->getFormId()." WHERE id = ".$taskform->getEntryId();
                $app_results = mysql_query($sql,$dbconn);

                $apform = mysql_fetch_assoc($app_results);

                $q = Doctrine_Query::create()
                    ->from('apFormElements a')
                    ->where('a.form_id = ?', $taskform->getFormId());

                $elements = $q->execute();

                foreach($elements as $element)
                {
                    if($element->getElementType() == "simple_name")
                    {
                        if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_1"]." ".$apform['element_'.$element->getElementId()."_2"], $content);
                        }
                        continue;
                    }
                    if($element->getElementType() == "simple_name_wmiddle")
                    {
                        if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_1"]." ".$apform['element_'.$element->getElementId()."_2"]." ".$apform['element_'.$element->getElementId()."_3"], $content);
                        }
                        continue;
                    }
                    if($element->getElementType() == "checkbox")
                    {
                        if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$taskform->getFormId().'}', $content))
                        {
                            $q = Doctrine_Query::create()
                                ->from('ApElementOptions a')
                                ->where('a.form_id = ? AND a.element_id = ?', array($taskform->getFormId(),$element->getElementId()));
                            $options = $q->execute();

                            $options_text = "";
                            foreach($options as $option)
                            {
                                if($apform['element_'.$element->getElementId()."_".$option->getOptionId()])
                                {
                                    $options_text .= "".$option->getOption().",";
                                }
                            }
                            $options_text .= "";

                            $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."}",$options_text, $content);
                        }
                        continue;
                    }

                    if($element->getElementType() == "select")
                    {
                        $opt_value = 0;
                        if($apform['element_'.$element->getElementId()] == "0")
                        {
                            $opt_value++;
                        }
                        else
                        {
                            $opt_value = $apform['element_'.$element->getElementId()];
                        }


                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($taskform->getFormId(),$element->getElementId(),$opt_value))
                            ->limit(1);
                        $option = $q->fetchOne();

                        if($option)
                        {
                            $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}',$option->getOption(), $content);
                        }
                    }


                    $childs = $element->getElementTotalChild();

                    if($childs == 0)
                    {
                        if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}',$apform['element_'.$element->getElementId()], $content);
                        }
                        else
                        {
                            for($x = 0; $x < ($childs + 1); $x++)
                            {
                                if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}', $content))
                                {
                                    $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                                }
                            }

                        }
                    }
                    else
                    {

                        if($element->getElementType() == "select")
                        {
                            if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}', $content))
                            {
                                $opt_value = 0;
                                if($apform['element_'.$element->getElementId()] == "0")
                                {
                                    $opt_value++;
                                }
                                else
                                {
                                    $opt_value = $apform['element_'.$element->getElementId()];
                                }


                                $q = Doctrine_Query::create()
                                    ->from('ApElementOptions a')
                                    ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($taskform->getFormId(),$element->getElementId(),$opt_value))
                                    ->limit(1);
                                $option = $q->fetchOne();

                                if($option)
                                {
                                    $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}',$option->getOption(), $content);
                                }
                            }
                        }
                        else
                        {
                            for($x = 0; $x < ($childs + 1); $x++)
                            {
                                if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}', $content))
                                {
                                    $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                                }
                            }

                        }




                    }
                    for($x = 0; $x < ($childs + 1); $x++)
                    {
                        if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."_".($x+1).'}', $content))
                        {
                            $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."_".($x+1).'}',$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                        }
                    }

                }

            }
        }

        //Get Conditions of Approval (anything starting with ca_ )
        //ca_conditions
        if($this->find('{ca_conditions}', $content))
        {
            $content = str_replace('{ca_conditions}', $conditions , $content);
        }

        if($this->find('{uuid}', $content))
        {
            if(empty($saved_permit->getRemoteUpdateUuid()))
            {
                $permit_manager = new PermitManager();
                $uuid = $permit_manager->generate_uuid();
                error_log("UUID Log: ".$uuid);

                $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
                mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

                $sql = "UPDATE saved_permit SET remote_update_uuid = '".$uuid."' WHERE id = ".$saved_permit->getId();
                $result = mysql_query($sql, $db_connection);
            }

            $content = str_replace('{uuid}', $saved_permit->getRemoteUpdateUuid(), $content);
        }

        $inv_total_amount = 0;

        $invoices = $saved_application->getMfInvoice();
        foreach($invoices as $invoice)
        {
            $inv_total_amount = 0;

            $q = Doctrine_Query::create()
                ->from('mfInvoiceDetail a')
                ->where('a.invoice_id = ?', $invoice->getId());
            $details = $q->execute();

            foreach($details as $detail)
            {
                if($detail->getDescription() != "Total" && $detail->getDescription() != "Convenience fee")
                {
                    $inv_total_amount = $inv_total_amount + $detail->getAmount();
                }
            }

            $content = str_replace('{inv_no}', $invoice->getInvoiceNumber(), $content);
            $content = str_replace('{inv_date_created}', date('d F Y', strtotime(substr($invoice->getCreatedAt(), 0, 10))), $content);
            $content = str_replace('{inv_expires_at}', date('d F Y', strtotime(substr($invoice->getExpiresAt(), 0, 10))), $content);
            $content = str_replace('{inv_paid_at}', date('d F Y', strtotime(substr($invoice->getUpdatedAt(), 0, 10))), $content);

            $q = Doctrine_Query::create()
                ->from("ApFormPayments a")
                ->where("a.payment_id = ?", $invoice->getFormEntry()->getFormId()."/".$invoice->getFormEntry()->getEntryId()."/".$invoice->getId());
            $transaction = $q->fetchOne();

            if($transaction) {
                $content = str_replace('{inv_payment_id}', $transaction->getPaymentId(), $content);
                $content = str_replace('{inv_transaction_id}', $transaction->getBillingState(), $content);
            }
        }

        if($this->find('{inv_total}', $content))
        {
            $content = str_replace('{inv_total}', $inv_total_amount, $content);
        }



        $content = str_replace('First option', "", $content);
        return $content;

    }

    public function parseRemoteArchive($application_id, $form_id, $entry_id, $permit_id, $content){
        $saved_application = Doctrine_Core::getTable("FormEntryArchive")->find($application_id);

        $saved_permit = Doctrine_Core::getTable("SavedPermitArchive")->find($permit_id);

        $app_entry_id = $saved_application->getEntryId();

        $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

        $sql = "SELECT * FROM ap_form_".$form_id." WHERE id = ".$app_entry_id;
        $app_results = mysql_query($sql,$dbconn);

        $apform = mysql_fetch_assoc($app_results);

        $sql = "SELECT * FROM ap_settings WHERE id = 1";
        $app_results = mysql_query($sql,$dbconn);

        $apsettings = mysql_fetch_assoc($app_results);

        if($this->find('{ap_issue_date}', $content))
        {
            if($saved_permit->getDateOfIssue())
            {
                $content = str_replace('{ap_issue_date}', date('d F Y', strtotime($saved_permit->getDateOfIssue())), $content);
            }
            else
            {
                $content = str_replace('{ap_issue_date}', "", $content);
            }
        }

        if($this->find('{ap_application_id}', $content))
        {
            $content = str_replace('{ap_application_id}', $saved_application->getApplicationId(), $content);
        }

        if($this->find('{ap_permit_id}', $content))
        {
            $content = str_replace('{ap_permit_id}', $saved_permit->getPermitId(), $content);
        }

        if($this->find('{ap_expire_date}', $content))
        {
            if($saved_permit->getDateOfExpiry())
            {
                $content = str_replace('{ap_expire_date}', date('d F Y', strtotime($saved_permit->getDateOfExpiry())), $content);
            }
            else
            {
                $content = str_replace('{ap_expire_date}', "", $content);
            }
        }

        $user = Doctrine_Core::getTable("SfGuardUser")->find($saved_application->getUserId());

        $user_profile = $user->getProfile();

        //Get User Information (anything starting with sf_ )
        //sf_email, sf_fullname, sf_username, ... other fields in the dynamic user profile form e.g sf_element_1
        if($this->find('{sf_username}', $content))
        {
            $content = str_replace('{sf_username}',$user->getUsername(), $content);
        }
        if($this->find('{sf_email}', $content))
        {
            $content = str_replace('{sf_email}',$user_profile->getEmail(), $content);
        }
        if($this->find('{sf_mobile}', $content))
        {
            $content = str_replace('{sf_mobile}',$user_profile->getMobile(), $content);
        }
        if($this->find('{sf_fullname}', $content))
        {
            $content = str_replace('{sf_fullname}',$user_profile->getFullname(), $content);
        }


        if($this->find('{current_date}', $content))
        {
            $content = str_replace('{current_date}',date('d F Y', strtotime(date('Y-m-d'))), $content);
        }

        $q = Doctrine_Query::create()
            ->from('apFormElements a')
            ->where('a.form_id = ?', $form_id);

        $elements = $q->execute();

        foreach($elements as $element)
        {
            if($element->getElementType() == "simple_name")
            {
                if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                {
                    $content = str_replace('{fm_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_1"]." ".$apform['element_'.$element->getElementId()."_2"], $content);
                }
                continue;
            }

            if($element->getElementType() == "simple_name_wmiddle")
            {
                if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                {
                    $content = str_replace('{fm_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_1"]." ".$apform['element_'.$element->getElementId()."_2"]." ".$apform['element_'.$element->getElementId()."_3"], $content);
                }
                continue;
            }

            if($element->getElementType() == "file")
            {
                if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                {
                  $content = str_replace('{fm_element_'.$element->getElementId()."}", "https://".$_SERVER['HTTP_HOST']."/".$apsettings['data_dir']."/form_".$element->getFormId()."/files/".$apform['element_'.$element->getElementId()], $content);
                }
                continue;
            }

            if($element->getElementType() == "checkbox")
            {
                if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                {
                    $q = Doctrine_Query::create()
                        ->from('ApElementOptions a')
                        ->where('a.form_id = ? AND a.element_id = ?', array($form_id,$element->getElementId()));
                    $options = $q->execute();

                    $options_text = "";
                    foreach($options as $option)
                    {
                        if($apform['element_'.$element->getElementId()."_".$option->getOptionId()])
                        {
                            $options_text .= "".$option->getOption()."";
                        }
                    }
                    $options_text .= "";

                    $content = str_replace('{fm_element_'.$element->getElementId()."}",$options_text, $content);
                }
                continue;
            }

            if($element->getElementType() == "select")
            {
                if($element->getElementExistingForm() && $element->getElementExistingStage())
                {
                    $q = Doctrine_Query::create()
                        ->from("FormEntry a")
                        ->where("a.id = ?", $apform['element_'.$element->getElementId()])
                        ->limit(1);
                    $linked_application = $q->fetchOne();
                    if($q->count() > 0)
                    {

//            $q = Doctrine_Query::create()
//               ->from("SavedPermit a")
//               ->leftJoin("a.FormEntry b")
//               ->where("b.form_id = ?", $linked_application->getFormId());
//            $permits = $q->execute();
//
//            foreach($permits as $saved_permit)
//            {
//                if($this->find("{ap_permit_id_".$saved_permit->getTypeId()."_element_child}", $content))
//                {
//                  $content = str_replace("{ap_permit_id_".$saved_permit->getTypeId()."_element_child}", ($saved_permit->getPermitId()?$saved_permit->getPermitId():$linked_application->getApplicationId()), $content);
//                }
//            }

                        if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId().'}', $linked_application->getApplicationId(), $content);
                        }

                        $q = Doctrine_Query::create()
                            ->from('apFormElements a')
                            ->where('a.form_id = ?', $element->getElementExistingForm())
                            ->andWhere('a.element_status = ?', 1);

                        $child_elements = $q->execute();

                        foreach($child_elements as $child_element)
                        {
                            $sql = "SELECT * FROM ap_form_".$linked_application->getFormId()." WHERE id = ".$linked_application->getEntryId();
                            $child_app_results = mysql_query($sql,$dbconn);

                            $child_apform = mysql_fetch_assoc($child_app_results);

                            if($this->find('{ap_child_application_id}', $content))
                            {
                                $content = str_replace('{ap_child_application_id}', $linked_application->getApplicationId(), $content);
                            }

                            if($this->find('{fm_child_created_at}', $content))
                            {
                                if($linked_application->getDateOfSubmission())
                                {
                                    $content = str_replace('{fm_child_created_at}', date('d F Y', strtotime($linked_application->getDateOfSubmission())), $content);
                                }
                                else
                                {
                                    $content = str_replace('{fm_child_created_at}', "", $content);
                                }
                            }

                            if($this->find('{fm_child_updated_at}', $content))
                            {
                                if($linked_application->getDateOfResponse())
                                {
                                    $content = str_replace('{fm_child_updated_at}', date('d F Y', strtotime(substr($linked_application->getDateOfResponse(), 0, 11))), $content);
                                }
                                else
                                {
                                    $content = str_replace('{fm_child_updated_at}', "", $content);
                                }
                            }

                            //START CHILD ELEMENTS
                            $childs = $child_element->getElementTotalChild();
                            if($childs == 0)
                            {
                                if($child_element->getElementType() == "select")
                                {
                                    if($child_element->getElementExistingForm() && $child_element->getElementExistingStage())
                                    {

                                        $q = Doctrine_Query::create()
                                            ->from("FormEntry a")
                                            ->where("a.id = ?", $child_apform['element_'.$child_element->getElementId()])
                                            ->limit(1);
                                        $linked_grand_application = $q->fetchOne();
                                        if($linked_grand_application)
                                        {

//                           $q = Doctrine_Query::create()
//                              ->from("SavedPermit a")
//                              ->leftJoin("a.FormEntry b")
//                              ->where("b.form_id = ?", $linked_grand_application->getFormId());
//                           $permits = $q->execute();
//
//                           foreach($permits as $saved_permit)
//                           {
//                               if($this->find("{ap_permit_id_".$saved_permit->getTypeId()."_element_grand_child}", $content))
//                               {
//                                 $content = str_replace("{ap_permit_id_".$saved_permit->getTypeId()."_element_grand_child}", ($saved_permit->getPermitId()?$saved_permit->getPermitId():$linked_grand_application->getApplicationId()), $content);
//                               }
//                           }

                                            if($this->find('{fm_child_element_'.$child_element->getElementId().'}', $content))
                                            {
                                                $content = str_replace('{fm_child_element_'.$child_element->getElementId().'}', $linked_grand_application->getApplicationId(), $content);
                                            }

                                            $q = Doctrine_Query::create()
                                                ->from('apFormElements a')
                                                ->where('a.form_id = ?', $child_element->getElementExistingForm())
                                                ->andWhere('a.element_status = ?', 1);

                                            $grand_child_elements = $q->execute();

                                            foreach($grand_child_elements as $grand_child_element)
                                            {

                                                $sql = "SELECT * FROM ap_form_".$child_element->getElementExistingForm()." WHERE id = ".$linked_grand_application->getEntryId();
                                                $child_app_results = mysql_query($sql,$dbconn);

                                                $grand_child_apform = mysql_fetch_assoc($child_app_results);

                                                if($this->find('{ap_grand_child_application_id}', $content))
                                                {
                                                    $content = str_replace('{ap_grand_child_application_id}', $linked_grand_application->getApplicationId(), $content);
                                                }

                                                if($this->find('{fm_grand_child_created_at}', $content))
                                                {
                                                    if($linked_grand_application->getDateOfSubmission())
                                                    {
                                                        $content = str_replace('{fm_grand_child_created_at}', date('d F Y', strtotime($linked_grand_application->getDateOfSubmission())), $content);
                                                    }
                                                    else
                                                    {
                                                        $content = str_replace('{fm_grand_child_created_at}', "", $content);
                                                    }
                                                }

                                                if($this->find('{fm_grand_child_updated_at}', $content))
                                                {
                                                    if($linked_grand_application->getDateOfResponse())
                                                    {
                                                        $content = str_replace('{fm_grand_child_updated_at}', date('d F Y', strtotime(substr($linked_grand_application->getDateOfResponse(), 0, 11))), $content);
                                                    }
                                                    else
                                                    {
                                                        $content = str_replace('{fm_grand_child_updated_at}', "", $content);
                                                    }
                                                }

                                                //START GRAND CHILD ELEMENTS
                                                $childs = $grand_child_element->getElementTotalChild();
                                                if($childs == 0)
                                                {
                                                    if($grand_child_element->getElementType() == "select")
                                                    {//select
                                                        if($this->find('{fm_grand_child_element_'.$grand_child_element->getElementId().'}', $content))
                                                        {
                                                            $opt_value = 0;
                                                            if($grand_child_apform['element_'.$grand_child_element->getElementId()] == "0")
                                                            {
                                                                $opt_value++;
                                                            }
                                                            else
                                                            {
                                                                $opt_value = $grand_child_apform['element_'.$grand_child_element->getElementId()];
                                                            }

                                                            $q = Doctrine_Query::create()
                                                                ->from('ApElementOptions a')
                                                                ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($child_element->getElementExistingForm(),$grand_child_element->getElementId(),$opt_value))
                                                                ->limit(1);
                                                            $option = $q->fetchOne();

                                                            if($option)
                                                            {
                                                                $content = str_replace('{fm_grand_child_element_'.$grand_child_element->getElementId().'}',$option->getOption(), $content);
                                                            }
                                                        }
                                                    }
                                                    elseif($grand_child_element->getElementType() == "checkbox")
                                                    {
                                                        if($this->find('{fm_grand_child_element_'.$grand_child_element->getElementId().'}', $content))
                                                        {
                                                            $q = Doctrine_Query::create()
                                                                ->from('ApElementOptions a')
                                                                ->where('a.form_id = ? AND a.element_id = ?', array($child_element->getElementExistingForm(),$grand_child_element->getElementId()));
                                                            $options = $q->execute();

                                                            $last_option = "";

                                                            $options_text = "";
                                                            foreach($options as $option)
                                                            {
                                                                if($grand_child_apform['element_'.$grand_child_element->getElementId()."_".$option->getOptionId()])
                                                                {
                                                                    $options_text .= "".$option->getOption()."";
                                                                }
                                                                $last_option = $option->getOption();
                                                            }

                                                            //if other option is filled then get last option and increment by 1
                                                            if($this->find('{fm_grand_child_element_'.$grand_child_element->getElementId().'_other}', $content))
                                                            {
                                                                //Ensure other field is not empty before adding option
                                                                if($grand_child_apform['element_'.$grand_child_element->getElementId().'_other'])
                                                                {
                                                                  $options_text .= "I";
                                                                }
                                                            }


                                                            $options_text .= "";

                                                            $content = str_replace('{fm_grand_child_element_'.$grand_child_element->getElementId()."}",$options_text, $content);
                                                        }

                                                        if($this->find('{fm_grand_child_element_'.$grand_child_element->getElementId().'_other}', $content))
                                                        {
                                                          $content = str_replace('{fm_grand_child_element_'.$grand_child_element->getElementId()."_other}",$grand_child_apform['element_'.$grand_child_element->getElementId()."_other"], $content);
                                                        }
                                                    }
                                                    else
                                                    {//text
                                                        if($this->find('{fm_grand_child_element_'.$grand_child_element->getElementId().'}', $content))
                                                        {
                                                            $content = str_replace('{fm_grand_child_element_'.$grand_child_element->getElementId().'}', $grand_child_apform['element_'.$grand_child_element->getElementId()], $content);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    for($x = 0; $x < ($childs + 1); $x++)
                                                    {
                                                        if($this->find('{fm_grand_child_element_'.$grand_child_element->getElementId().'}', $content))
                                                        {
                                                            $content = str_replace('{fm_grand_child_element_'.$grand_child_element->getElementId()."}",$grand_child_apform['element_'.$grand_child_element->getElementId()."_".($x+1)], $content);
                                                        }
                                                    }
                                                }
                                                //END GRAND CHILD ELEMENTS
                                            }

                                        }

                                    }
                                    else
                                    { //select
                                        if($this->find('{fm_child_element_'.$child_element->getElementId().'}', $content))
                                        {
                                            $opt_value = 0;
                                            if($child_apform['element_'.$child_element->getElementId()] == "0")
                                            {
                                                $opt_value++;
                                            }
                                            else
                                            {
                                                $opt_value = $child_apform['element_'.$child_element->getElementId()];
                                            }

                                            $q = Doctrine_Query::create()
                                                ->from('ApElementOptions a')
                                                ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($linked_application->getFormId(),$child_element->getElementId(),$opt_value))
                                                ->limit(1);
                                            $option = $q->fetchOne();

                                            if($option)
                                            {
                                                $content = str_replace('{fm_child_element_'.$child_element->getElementId().'}',$option->getOption(), $content);
                                            }
                                        }
                                    }
                                }
                                elseif($child_element->getElementType() == "checkbox")
                                {
                                    if($this->find('{fm_child_element_'.$child_element->getElementId().'}', $content))
                                    {
                                        $q = Doctrine_Query::create()
                                            ->from('ApElementOptions a')
                                            ->where('a.form_id = ? AND a.element_id = ?', array($linked_application->getFormId(),$child_element->getElementId()));
                                        $options = $q->execute();

                                        $options_text = "";
                                        foreach($options as $option)
                                        {
                                            if($child_apform['element_'.$child_element->getElementId()."_".$option->getOptionId()])
                                            {
                                                $options_text .= "".$option->getOption().",";
                                            }
                                        }
                                        $options_text .= "";

                                        $content = str_replace('{fm_child_element_'.$child_element->getElementId()."}",$options_text, $content);
                                    }
                                    continue;
                                }
                                else
                                { //text
                                    if($this->find('{fm_child_element_'.$child_element->getElementId().'}', $content))
                                    {
                                        if($child_apform['element_'.$child_element->getElementId()])
                                        {
                                            $content = str_replace('{fm_child_element_'.$child_element->getElementId().'}', $child_apform['element_'.$child_element->getElementId()], $content);
                                        }
                                        else
                                        {
                                            $content = str_replace('{fm_child_element_'.$child_element->getElementId().'}', $child_apform['element_'.$child_element->getElementId().'_1'], $content);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                for($x = 0; $x < ($childs + 1); $x++)
                                {
                                    if($this->find('{fm_child_element_'.$child_element->getElementId().'}', $content))
                                    {
                                        $content = str_replace('{fm_child_element_'.$child_element->getElementId()."}",$child_apform['element_'.$child_element->getElementId()."_".($x+1)], $content);
                                    }
                                }
                            }
                            //END CHILD ELEMENTS
                        }


                        continue;
                    }
                }
                elseif($element->getElementTableName())
                {
                   $content = str_replace('{fm_element_'.$element->getElementId().'}',$apform['element_'.$element->getElementId()], $content);
                }
                else
                {
                    if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                    {
                        $opt_value = 0;
                        if($apform['element_'.$element->getElementId()] == "0")
                        {
                            $opt_value++;
                        }
                        else
                        {
                            $opt_value = $apform['element_'.$element->getElementId()];
                        }


                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($form_id,$element->getElementId(),$opt_value))
                            ->limit(1);
                        $option = $q->fetchOne();

                        if($option)
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId().'}',$option->getOption(), $content);
                        }
                    }
                }
            }

            $childs = $element->getElementTotalChild();

            if($childs == 0)
            {
                if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                {
                    if($element->getElementType() == "date")
                    {
                        $date = "";
                        if($apform['element_'.$element->getElementId()])
                        {
                            $date = date('d F Y', strtotime($apform['element_'.$element->getElementId()]));
                        }
                        $content = str_replace('{fm_element_'.$element->getElementId().'}', $date, $content);
                    }
                    elseif($element->getElementType() == "europe_date")
                    {
                        $date = "";
                        if($apform['element_'.$element->getElementId()])
                        {
                            $date = date('d F Y', strtotime($apform['element_'.$element->getElementId()]));
                        }
                        $content = str_replace('{fm_element_'.$element->getElementId().'}', $date, $content);
                    }
                    elseif($element->getElementType() == "select" || $element->getElementType() == "radio")
                    {
                        $opt_value = $apform['element_'.$element->getElementId()];

                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($form_id,$element->getElementId(),$opt_value))
                            ->limit(1);
                        $option = $q->fetchOne();

                        if($option)
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId()."}",$option->getOption(), $content);
                        }
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId()."}","-", $content);
                        }
                    }
                    else
                    {
                        $content = str_replace('{fm_element_'.$element->getElementId().'}',$apform['element_'.$element->getElementId()], $content);
                    }
                }
                else
                {
                    for($x = 0; $x < ($childs + 1); $x++)
                    {
                        if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                        }
                    }

                }
            }
            else
            {

                if($element->getElementType() == "select")
                {
                    if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                    {
                        $opt_value = 0;
                        if($apform['element_'.$element->getElementId()] == "0")
                        {
                            $opt_value++;
                        }
                        else
                        {
                            $opt_value = $apform['element_'.$element->getElementId()];
                        }


                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($form_id,$element->getElementId(),$opt_value))
                            ->limit(1);
                        $option = $q->fetchOne();

                        if($option)
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId().'}',$option->getOption(), $content);
                        }
                    }
                }
                else
                {
                    for($x = 0; $x < ($childs + 1); $x++)
                    {
                        if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                        }
                    }

                }




            }
            for($x = 0; $x < ($childs + 1); $x++)
            {
                if($this->find('{fm_element_'.$element->getElementId()."_".($x+1).'}', $content))
                {
                    $content = str_replace('{fm_element_'.$element->getElementId()."_".($x+1).'}',$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                }
            }

        }

        //Comment Sheets
        $q = Doctrine_Query::create()
            ->from("Task a")
            ->where("a.application_id = ?", $saved_application->getId());
        $tasks = $q->execute();

        foreach($tasks as $task)
        {
            $q = Doctrine_Query::create()
                ->from("TaskForms a")
                ->where("a.task_id = ?", $task->getId())
                ->limit(1);
            $taskform = $q->fetchOne();
            if($taskform)
            {
                $sql = "SELECT * FROM ap_form_".$taskform->getFormId()." WHERE id = ".$taskform->getEntryId();
                $app_results = mysql_query($sql,$dbconn);

                $apform = mysql_fetch_assoc($app_results);

                $q = Doctrine_Query::create()
                    ->from('apFormElements a')
                    ->where('a.form_id = ?', $taskform->getFormId());

                $elements = $q->execute();

                foreach($elements as $element)
                {
                    if($element->getElementType() == "simple_name")
                    {
                        if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_1"]." ".$apform['element_'.$element->getElementId()."_2"], $content);
                        }
                        continue;
                    }
                    if($element->getElementType() == "simple_name_wmiddle")
                    {
                        if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_1"]." ".$apform['element_'.$element->getElementId()."_2"]." ".$apform['element_'.$element->getElementId()."_3"], $content);
                        }
                        continue;
                    }
                    if($element->getElementType() == "checkbox")
                    {
                        if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$taskform->getFormId().'}', $content))
                        {
                            $q = Doctrine_Query::create()
                                ->from('ApElementOptions a')
                                ->where('a.form_id = ? AND a.element_id = ?', array($taskform->getFormId(),$element->getElementId()));
                            $options = $q->execute();

                            $options_text = "";
                            foreach($options as $option)
                            {
                                if($apform['element_'.$element->getElementId()."_".$option->getOptionId()])
                                {
                                    $options_text .= "".$option->getOption().",";
                                }
                            }
                            $options_text .= "";

                            $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."}",$options_text, $content);
                        }
                        continue;
                    }

                    if($element->getElementType() == "select")
                    {
                        $opt_value = 0;
                        if($apform['element_'.$element->getElementId()] == "0")
                        {
                            $opt_value++;
                        }
                        else
                        {
                            $opt_value = $apform['element_'.$element->getElementId()];
                        }


                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($taskform->getFormId(),$element->getElementId(),$opt_value))
                            ->limit(1);
                        $option = $q->fetchOne();

                        if($option)
                        {
                            $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}',$option->getOption(), $content);
                        }
                    }


                    $childs = $element->getElementTotalChild();

                    if($childs == 0)
                    {
                        if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}',$apform['element_'.$element->getElementId()], $content);
                        }
                        else
                        {
                            for($x = 0; $x < ($childs + 1); $x++)
                            {
                                if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}', $content))
                                {
                                    $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                                }
                            }

                        }
                    }
                    else
                    {

                        if($element->getElementType() == "select")
                        {
                            if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}', $content))
                            {
                                $opt_value = 0;
                                if($apform['element_'.$element->getElementId()] == "0")
                                {
                                    $opt_value++;
                                }
                                else
                                {
                                    $opt_value = $apform['element_'.$element->getElementId()];
                                }


                                $q = Doctrine_Query::create()
                                    ->from('ApElementOptions a')
                                    ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($taskform->getFormId(),$element->getElementId(),$opt_value))
                                    ->limit(1);
                                $option = $q->fetchOne();

                                if($option)
                                {
                                    $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}',$option->getOption(), $content);
                                }
                            }
                        }
                        else
                        {
                            for($x = 0; $x < ($childs + 1); $x++)
                            {
                                if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId().'}', $content))
                                {
                                    $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                                }
                            }

                        }




                    }
                    for($x = 0; $x < ($childs + 1); $x++)
                    {
                        if($this->find('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."_".($x+1).'}', $content))
                        {
                            $content = str_replace('{fm_c'.$taskform->getFormId().'_element_'.$element->getElementId()."_".($x+1).'}',$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                        }
                    }

                }

            }
        }

        //Get Conditions of Approval (anything starting with ca_ )
        //ca_conditions
        if($this->find('{ca_conditions}', $content))
        {
            $content = str_replace('{ca_conditions}', $conditions , $content);
        }

        if($this->find('{uuid}', $content))
        {
            if(empty($saved_permit->getRemoteUpdateUuid()))
            {
                $permit_manager = new PermitManager();
                $uuid = $permit_manager->generate_uuid();
                error_log("UUID Log: ".$uuid);

                $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
                mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

                $sql = "UPDATE saved_permit SET remote_update_uuid = '".$uuid."' WHERE id = ".$saved_permit->getId();
                $result = mysql_query($sql, $db_connection);
            }

            $content = str_replace('{uuid}', $saved_permit->getRemoteUpdateUuid(), $content);
        }

        if($this->find('{inv_total}', $content))
        {
            $inv_total_amount = 0;

            $invoices = $saved_application->getMfInvoiceArchive();
            foreach($invoices as $invoice)
            {
                $inv_total_amount = 0;

                $q = Doctrine_Query::create()
                    ->from('mfInvoiceDetailArchive a')
                    ->where('a.invoice_id = ?', $invoice->getId());
                $details = $q->execute();

                foreach($details as $detail)
                {
                    if($detail->getDescription() != "Total" && $detail->getDescription() != "Convenience fee")
                    {
                        $inv_total_amount = $inv_total_amount + $detail->getAmount();
                    }
                }

            }

            $content = str_replace('{inv_total}', $inv_total_amount, $content);
        }



        $content = str_replace('First option', "", $content);

        return $content;

    }

    /***
     * Retrieves all the user details as an array
     *
     * @param $user_id the id of the user
     * @return array
     */
    function getUserDetails($user_id)
    {
        $values = array();

        $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

        $user = Doctrine_Core::getTable("SfGuardUser")->find($user_id);

        $values['sf_username'] = $user->getUsername();
        $values['sf_mobile'] = $user->getProfile()->getMobile();
        $values['sf_email'] = $user->getProfile()->getEmail();
        $values['sf_fullname'] = $user->getProfile()->getFullname();

        $q = Doctrine_Query::create()
            ->from('sfGuardUserCategories a')
            ->where('a.id = ?', $user->getProfile()->getRegisteras())
            ->limit(1);
        $category = $q->fetchOne();

        $q = Doctrine_Query::create()
            ->from('mfUserProfile a')
            ->where('a.user_id = ?', $user->getId())
            ->limit(1);
        $form_profile = $q->fetchOne();

        if($category && $form_profile)
        {
            $q = Doctrine_Query::create()
                ->from('apFormElements a')
                ->where('a.form_id = ?', $category->getFormId());

            $elements = $q->execute();

            $sql = "SELECT * FROM ap_form_".$category->getFormId()." WHERE id = ".$form_profile->getEntryId();
            $prof_results = mysql_query($sql,$dbconn);

            $user_profile_details = mysql_fetch_assoc($prof_results);

            foreach($elements as $element)
            {
                $childs = $element->getElementTotalChild();
                if($childs == 0)
                {
                    $values['sf_element_'.$element->getElementId()] = $user_profile_details['element_'.$element->getElementId()];
                }
                else
                {
                    if($element->getElementType() == "select")
                    {
                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($category->getFormId(),$element->getElementId(),$user_profile_details['element_'.$element->getElementId()]))
                            ->limit(1);
                        $option = $q->fetchOne();

                        if($option)
                        {
                            $values['sf_element_'.$element->getElementId()] = $option->getOption();
                        }
                    }
                    else
                    {
                        for($x = 0; $x < ($childs + 1); $x++)
                        {
                            $values['sf_element_'.$element->getElementId()] = $user_profile_details['element_'.$element->getElementId()."_".($x+1)];
                        }
                    }
                }


            }
        }

        return $values;
    }

    /***
     * Retrieves all the application details as an array
     *
     * @param $application_id the id of the application (form_entry)
     * @return array
     */
    function getApplicationDetails($application_id, $prefix = "")
    {
        $values = array();

        $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

        $application = Doctrine_Core::getTable("FormEntry")->find($application_id);

        if(empty($application))
        {
            $application = Doctrine_Core::getTable("FormEntryArchive")->find($application_id);
            if(empty($application))
            {
                error_log("Debug-t: Could not find linked application ".$application_id." for prefix ".$prefix);
                return $values;
            }
        }

        $form_id = $application->getFormId();
        $entry_id = $application->getEntryId();

        //Get Application Information (anything starting with ap_ )
        //ap_application_id
        $values['ap'.$prefix.'_application_id'] = $application->getApplicationId();

        //Get Form Details (anything starting with fm_ )
        //fm_created_at, fm_updated_at.....fm_element_1

        if($application->getDateOfSubmission())
        {
            $values['fm'.$prefix.'_created_at'] = date('d F Y', strtotime($application->getDateOfSubmission()));
        }
        else
        {
            $values['fm'.$prefix.'_created_at'] = "";
        }


        if($application->getDateOfResponse())
        {
            $values['fm'.$prefix.'_updated_at'] = date('d F Y', strtotime(substr($application->getDateOfResponse(), 0, 11)));
        }
        else
        {
            $values['fm'.$prefix.'_updated_at'] = "";
        }

        $values['current_date'.$prefix] = date('d F Y', strtotime(date('Y-m-d')));

        $sql = "SELECT * FROM ap_form_".$application->getFormId()." WHERE id = ".$application->getEntryId();
        $app_results = mysql_query($sql,$dbconn);

        $apform = mysql_fetch_assoc($app_results);

        $q = Doctrine_Query::create()
            ->from('apFormElements a')
            ->where('a.form_id = ?', $application->getFormId())
            ->andWhere("a.element_status = ?", 1);

        $elements = $q->execute();

        foreach($elements as $element)
        {
            if($element->getElementType() == "simple_name")
            {
                $values['fm'.$prefix.'_element_'.$element->getElementId().$prefix] = $apform['element_'.$element->getElementId()."_1"]." ".$apform['element_'.$element->getElementId()."_2"];
                continue;
            }

            if($element->getElementType() == "textarea")
            {
                if($element->getElementJsondef())
                {
                  $values['fm'.$prefix.'_element_'.$element->getElementId().$prefix] = json_decode($apform['element_'.$element->getElementId()], true);
                  continue;
                }
                else
                {
                  $values['fm'.$prefix.'_element_'.$element->getElementId().$prefix] = $apform['element_'.$element->getElementId()];
                  continue;
                }
            }

            if($element->getElementType() == "simple_name_wmiddle")
            {
                $values['fm'.$prefix.'_element_'.$element->getElementId().$prefix] = $apform['element_'.$element->getElementId()."_1"]." ".$apform['element_'.$element->getElementId()."_2"]." ".$apform['element_'.$element->getElementId().'_3'];
                continue;
            }

            if($element->getElementType() == "file")
            {
                $q = Doctrine_Query::create()
                   ->from("ApSettings a")
                   ->where("a.id = 1")
                   ->orderBy("a.id DESC");
                $aplogo = $q->fetchOne();

                $values['fm'.$prefix.'_element_'.$element->getElementId().$prefix] = $aplogo->getDataDirWeb()."form_".$application->getFormId()."/files/".$apform['element_'.$element->getElementId()];
                continue;
            }

            if($element->getElementType() == "checkbox")
            {
                $q = Doctrine_Query::create()
                    ->from('ApElementOptions a')
                    ->where('a.form_id = ? AND a.element_id = ?', array($form_id,$element->getElementId()));
                $options = $q->execute();

                $options_text = "";
                foreach($options as $option)
                {
                    if($apform['element_'.$element->getElementId()."_".$option->getOptionId()])
                    {
                        $options_text .= "".$option->getOption()."";
                    }
                }


                if ($apform['element_' . $element->getElementId() . "_other"] != "") {
                    $options_text .= "" . $apform['element_'.$element->getElementId().'_other'] . "";
                }

                $options_text .= "";

                $values['fm'.$prefix.'_element_'.$element->getElementId()] = $options_text;
                continue;
            }
			//OTB Patch Start - Get actual plot number from UPI
				$upi_plot = explode("/", $apform['element_'.$element->getElementId()]);
				$upi_plot = $upi_plot[4] ? $upi_plot[4] : "-";
				$values['upi_plot_fm_element_'.$element->getElementId()] = $upi_plot;
			//OTB Patch End - Get actual plot number from UPI
            $childs = $element->getElementTotalChild();

            //if($childs == 0)//OTB Comment - don't seem to be using element_total_child
            {
                if($element->getElementType() == "date")
                {
                    $date = "";
                    if($apform['element_'.$element->getElementId()] && $apform['element_'.$element->getElementId()] != "0000-00-00")
                    {
                        $date = date('d F Y', strtotime($apform['element_'.$element->getElementId()]));
                    }
                    else
                    {
                        $date = "";
                    }

                    $values['fm'.$prefix.'_element_'.$element->getElementId()] = $date;
                }
                elseif($element->getElementType() == "europe_date")
                {
                    $date = "";
                    if($apform['element_'.$element->getElementId()] && $apform['element_'.$element->getElementId()] != "0000-00-00")
                    {
                        $date = date('d F Y', strtotime($apform['element_'.$element->getElementId()]));
                    }
                    else
                    {
                        $date = "";
                    }

                    $values['fm'.$prefix.'_element_'.$element->getElementId()] = $date;
                }
                elseif($element->getElementType() == "select" || $element->getElementType() == "radio")
                {
                    if($element->getElementExistingForm() && $element->getElementExistingStage() && $element->getElementType() == "select")
                    {
                        $application_id = $apform['element_'.$element->getElementId()];

                        if($prefix == "_child")
                        {
                            $child_values = $this->getApplicationDetails($application_id,"_grand_child");
                            $values = array_merge($values, $child_values);
                        }
                        elseif($prefix == "_grand_child")
                        {
                            $child_values = $this->getApplicationDetails($application_id,"_great_grand_child");
                            $values = array_merge($values, $child_values);
                        }
                        else{
                            $child_values = $this->getApplicationDetails($application_id,"_child");
                            $values = array_merge($values, $child_values);
                        }
                    }
                    else {
                        $opt_value = $apform['element_' . $element->getElementId()];

                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($form_id, $element->getElementId(), $opt_value))
                            ->limit(1);
                        $option = $q->fetchOne();

                        if ($option) {
                            $values['fm'.$prefix.'_element_' . $element->getElementId()] = $option->getOption();
                        } else {
                            $values['fm'.$prefix.'_element_' . $element->getElementId()] = "-";
                        }
                    }
                }
                else
                {
                    $values['fm'.$prefix.'_element_'.$element->getElementId()] = $apform['element_'.$element->getElementId()]." ";
                }
            }
            /*else//OTB Comment - don't seem to be using element_total_child
            {
                for($x = 0; $x < ($childs + 1); $x++)
                {
                    $content = str_replace('{fm_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                    $values['fm'.$prefix.'_element_'.$element->getElementId()] = $apform['element_'.$element->getElementId()."_".($x+1)];
                }

            }*/

        }

        return $values;
    }

    /***
     * Retrieves all the application details as an array
     *
     * @param $application_id the id of the application (form_entry)
     * @return array
     */
    function getArchiveApplicationDetails($application_id, $prefix = "")
    {
        $values = array();

        $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

        $application = Doctrine_Core::getTable("FormEntryArchive")->find($application_id);

        if(empty($application))
        {
            error_log("Debug-t: Could not find linked application ".$application_id." for prefix ".$prefix);
            return $values;
        }

        $form_id = $application->getFormId();
        $entry_id = $application->getEntryId();

        //Get Application Information (anything starting with ap_ )
        //ap_application_id
        $values['ap'.$prefix.'_application_id'] = $application->getApplicationId();

        //Get Form Details (anything starting with fm_ )
        //fm_created_at, fm_updated_at.....fm_element_1

        if($application->getDateOfSubmission())
        {
            $values['fm'.$prefix.'_created_at'] = date('d F Y', strtotime($application->getDateOfSubmission()));
        }
        else
        {
            $values['fm'.$prefix.'_created_at'] = "";
        }


        if($application->getDateOfResponse())
        {
            $values['fm'.$prefix.'_updated_at'] = date('d F Y', strtotime(substr($application->getDateOfResponse(), 0, 11)));
        }
        else
        {
            $values['fm'.$prefix.'_updated_at'] = "";
        }

        $values['current_date'.$prefix] = date('d F Y', strtotime(date('Y-m-d')));

        $sql = "SELECT * FROM ap_form_".$application->getFormId()." WHERE id = ".$application->getEntryId();
        $app_results = mysql_query($sql,$dbconn);

        $apform = mysql_fetch_assoc($app_results);

        $q = Doctrine_Query::create()
            ->from('apFormElements a')
            ->where('a.form_id = ?', $application->getFormId())
            ->andWhere("a.element_status = ?", 1);

        $elements = $q->execute();

        foreach($elements as $element)
        {
            if($element->getElementType() == "simple_name")
            {
                $values['fm'.$prefix.'_element_'.$element->getElementId().$prefix] = $apform['element_'.$element->getElementId()."_1"]." ".$apform['element_'.$element->getElementId()."_2"];
                continue;
            }

            if($element->getElementType() == "simple_name_wmiddle")
            {
                $values['fm'.$prefix.'_element_'.$element->getElementId().$prefix] = $apform['element_'.$element->getElementId()."_1"]." ".$apform['element_'.$element->getElementId()."_2"]." ".$apform['element_'.$element->getElementId().'_3'];
                continue;
            }

            if($element->getElementType() == "file")
            {
                $q = Doctrine_Query::create()
                   ->from("ApSettings a")
                   ->where("a.id = 1")
                   ->orderBy("a.id DESC");
                $aplogo = $q->fetchOne();

                $values['fm'.$prefix.'_element_'.$element->getElementId().$prefix] = $aplogo->getDataDirWeb()."form_".$application->getFormId()."/files/".$apform['element_'.$element->getElementId()];
                continue;
            }

            if($element->getElementType() == "checkbox")
            {
                $q = Doctrine_Query::create()
                    ->from('ApElementOptions a')
                    ->where('a.form_id = ? AND a.element_id = ?', array($form_id,$element->getElementId()));
                $options = $q->execute();

                $options_text = "";
                foreach($options as $option)
                {
                    if($apform['element_'.$element->getElementId()."_".$option->getOptionId()])
                    {
                        $options_text .= "".$option->getOption()."";
                    }
                }


                if ($apform['element_' . $element->getElementId() . "_other"] != "") {
                    $options_text .= "" . $apform['element_'.$element->getElementId().'_other'] . "";
                }

                $options_text .= "";

                $values['fm'.$prefix.'_element_'.$element->getElementId()] = $options_text;
                continue;
            }

            $childs = $element->getElementTotalChild();

            if($childs == 0)
            {
                if($element->getElementType() == "date")
                {
                    $date = "";
                    if($apform['element_'.$element->getElementId()] && $apform['element_'.$element->getElementId()] != "0000-00-00")
                    {
                        $date = date('d F Y', strtotime($apform['element_'.$element->getElementId()]));
                    }
                    else
                    {
                        $date = "";
                    }

                    $values['fm'.$prefix.'_element_'.$element->getElementId()] = $date;
                }
                elseif($element->getElementType() == "europe_date")
                {
                    $date = "";
                    if($apform['element_'.$element->getElementId()] && $apform['element_'.$element->getElementId()] != "0000-00-00")
                    {
                        $date = date('d F Y', strtotime($apform['element_'.$element->getElementId()]));
                    }
                    else
                    {
                        $date = "";
                    }

                    $values['fm'.$prefix.'_element_'.$element->getElementId()] = $date;
                }
                elseif($element->getElementType() == "select" || $element->getElementType() == "radio")
                {
                    if($element->getElementExistingForm() && $element->getElementExistingStage() && $element->getElementType() == "select")
                    {
                        $application_id = $apform['element_'.$element->getElementId()];

                        if($prefix == "_child")
                        {
                            $child_values = $this->getApplicationDetails($application_id,"_grand_child");
                            $values = array_merge($values, $child_values);
                        }
                        elseif($prefix == "_grand_child")
                        {
                            $child_values = $this->getApplicationDetails($application_id,"_great_grand_child");
                            $values = array_merge($values, $child_values);
                        }
                        else{
                            $child_values = $this->getApplicationDetails($application_id,"_child");
                            $values = array_merge($values, $child_values);
                        }
                    }
                    else {
                        $opt_value = $apform['element_' . $element->getElementId()];

                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($form_id, $element->getElementId(), $opt_value))
                            ->limit(1);
                        $option = $q->fetchOne();

                        if ($option) {
                            $values['fm'.$prefix.'_element_' . $element->getElementId()] = $option->getOption();
                        } else {
                            $values['fm'.$prefix.'_element_' . $element->getElementId()] = "-";
                        }
                    }
                }
                else
                {
                    $values['fm'.$prefix.'_element_'.$element->getElementId()] = $apform['element_'.$element->getElementId()]." ";
                }
            }
            else
            {
                for($x = 0; $x < ($childs + 1); $x++)
                {
                    $content = str_replace('{fm_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                    $values['fm'.$prefix.'_element_'.$element->getElementId()] = $apform['element_'.$element->getElementId()."_".($x+1)];
                }

            }

        }

        return $values;
    }

    /***
     * Retrieves all the invoice details as an array
     *
     * @param $invoice_id the id of the invoice
     * @return array
     */
    function getInvoiceDetails($invoice_id)
    {
        $values = array();

        $invoice_manager = new InvoiceManager();

        $invoice = Doctrine_Core::getTable("MfInvoice")->find($invoice_id);
        $application = Doctrine_Core::getTable("FormEntry")->find($invoice->getAppId());

        $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

        $query = "select * from ap_form_payments where form_id = ".$application->getFormId()." and record_id = ".$application->getEntryId();
        $payment_results = mysql_query($query,$dbconn);
        $payment_row = mysql_fetch_assoc($payment_results);

        $inv_title = $invoice->getInvoiceNumber();
        $inv_created_at = substr($invoice->getCreatedAt(), 0, 10);
        $inv_expires_at = "";

        if($invoice->getExpiresAt())
        {
            $inv_expires_at = substr($invoice->getExpiresAt(), 0, 10);
        }
        else
        {
            $q = Doctrine_Query::create()
                ->from("Invoicetemplates a")
                ->where("a.applicationform = ?", $application->getFormId())
                ->limit(1);
            $template = $q->fetchOne();
            if($template)
            {
                if($template->getMaxDuration() > 0)
                {
                    $date = strtotime("+".$template->getMaxDuration()." day");
                    $inv_expires_at = date('Y-m-d', $date);
                }
            }

        }
		//OTB Start Patch - Parse Irembo Payment bill id
		$q = Doctrine_Query::create()
			->from("ApFormPayments a")
			->where("a.invoice_id = ? and a.payment_status in ('pending','paid')", $invoice->getId());
		$transactions = $q->execute();

		foreach($transactions as $transaction){
			$values['merchant_billing_number'] .= $transaction->getPaymentId()."<br/>";
		}
		//OTB End Patch - Parse Irembo Payment bill id
        $currency = "";

        $q = Doctrine_Query::create()
            ->from("Invoicetemplates a")
            ->where("a.applicationform = ?", $application->getFormId())
            ->limit(1);
        $invoice_template = $q->fetchOne();

        $q = Doctrine_Query::create()
            ->from("ApForms a")
            ->where("a.form_id = ?", $invoice_template->getApplicationform());
        $form = $q->fetchOne();

        if($form)
        {
          $currency = $form->getPaymentCurrency();
        }
        else
        {
          $currency = sfConfig::get("app_currency");
        }

        $inv_fees = "";

        $inv_fees = $inv_fees.'
                 <table width="2000px" cellspacing="0" cellpadding="0" border-spacing="0" style="border: 1px solid #d2d2d2; margin-bottom: 20px; width:100%;">
                 <thead><tr><th width="20%" style="border-right: 1px solid #d2d2d2; padding:5px 12px; text-align:left;" nowrap>Service Code</th><th width="80%" style="border-right: 1px solid #d2d2d2; padding:5px 12px; text-align:left;" nowrap>Service Description</th><th style="padding:5px 12px; text-align:left;" nowrap>Amount ('.$currency.')</th></tr></thead><tbody>';
        $grand_total = 0;
        $inv_details = $invoice->getMfInvoiceDetail();
        $total_found = false;
        foreach($inv_details as $inv_detail)
        {
            if($inv_detail->getDescription() == "Attached Invoice")
            {
                $inv_fees = $inv_fees.'<tr><td align="left" width="20%" style="border-right: 1px solid #d2d2d2; padding:5px 12px; border-top: 1px solid #d2d2d2;">-</td><td align="left" width="80%" style="border-right: 1px solid #d2d2d2; padding:5px 12px; border-top: 1px solid #d2d2d2;">'.$inv_detail->getDescription().'</td><td align="left" style="border-top: 1px solid #d2d2d2; padding:5px 12px;">'.$inv_detail->getAmount().'</td></tr>';
            }
            else
            {
                if($inv_detail->getDescription() == "Fee" && $inv_detail->getAmount() == "0")
                {
                    //Dont display
                }
                else
                {
                    $pieces = explode(" - ", $inv_detail->getDescription());
                    if(sizeof($pieces) > 1)
                    {
                        $description = $pieces[1];

                        $q = Doctrine_Query::create()
                            ->from("ApForms a")
                            ->where("a.form_code = ?", $pieces[0])
                            ->limit(1);
                        $apform = $q->fetchOne();

                        if($apform && $pieces[0] != "1420202-007")
                        {
                            $description = $apform->getFormName();
                        }

                        $inv_fees = $inv_fees.'<tr><td align="left" width="20%" style="border-right: 1px solid #d2d2d2; padding:5px 12px; border-top: 1px solid #d2d2d2;">'.$pieces[0].'</td><td align="left" width="80%" style="border-right: 1px solid #d2d2d2; padding:5px 12px; border-top: 1px solid #d2d2d2;">'.$description.'</td><td align="left" style="border-top: 1px solid #d2d2d2; padding:5px 12px;">'.$inv_detail->getAmount().'</td></tr>';
                    }
                    else
                    {
                        $inv_fees = $inv_fees.'<tr><td align"left" width="20%" style="border-right: 1px solid #d2d2d2; padding:5px 12px; border-top: 1px solid #d2d2d2;">-</td><td align="left" width="80%" style="border-right: 1px solid #d2d2d2; padding:5px 12px; border-top: 1px solid #d2d2d2;">'.$inv_detail->getDescription().'</td><td align="left" style="border-top: 1px solid #d2d2d2; padding:5px 12px;">'.$inv_detail->getAmount().'</td></tr>';
                    }
                    $grand_total += $inv_detail->getAmount();
                }
            }

            if($inv_detail->getDescription() == "Total")
            {
                $total_found = true;
            }
        }

        if($total_found == false)
        {
            $currency = "";

            $q = Doctrine_Query::create()
                ->from("Invoicetemplates a")
                ->where("a.applicationform = ?", $application->getFormId())
                ->limit(1);
            $invoice_template = $q->fetchOne();

            $q = Doctrine_Query::create()
                ->from("ApForms a")
                ->where("a.form_id = ?", $invoice_template->getApplicationform());
            $form = $q->fetchOne();

            if($form)
            {
              $currency = $form->getPaymentCurrency();
            }
            else
            {
              $currency = sfConfig::get("app_currency");
            }

            $inv_fees = $inv_fees.'<tr><td align="left" width="20%" style="border-right: 1px solid #d2d2d2; padding:5px 12px; border-top: 1px solid #d2d2d2;"></td><td align="right" width="80%" style="border-right: 1px solid #d2d2d2; padding:5px 12px; border-top: 1px solid #d2d2d2;"><b>Total ('.$currency.')</b></td><td align="left" style="border-top: 1px solid #d2d2d2; padding:5px 12px;">'.$grand_total.'</td></tr>';
        }

        $inv_fees = $inv_fees.'</tbody>
                 </table>';

        $status = "";

        $db_date_event = str_replace('/', '-', $invoice->getExpiresAt());

        $db_date_event = strtotime($db_date_event);

        if (time() > $db_date_event && !($invoice->getPaid() == "15" || $invoice->getPaid() == "2" || $invoice->getPaid() == "3"))
        {
            $status = '<font color="#D00000">Expired</font>';
        }
        else if($invoice->getPaid() == "2")
        {
            $status = '<font color="#00CC00">Paid</font>';
        }
        else if($invoice->getPaid() == "15")
        {
            $status = "Part Payment";
        }
        else if($invoice->getPaid() == "1")
        {
            $status = '<font color="#D00000">Not Paid</font>';
        }

        if($invoice->getPaid() == "3")
        {
            $status = '<font color="#D00000">Cancelled</font>';
        }

        $values['inv_status'] = $status;

        $values['inv_no'] = $inv_title;

        $values['in_total'] = $invoice->getTotalAmount();

        $values['inv_total'] = $invoice->getTotalAmount();

        $values['inv_balance'] = $invoice_manager->get_invoice_total_owed($invoice->getId());

        $values['inv_date_created'] = date('d F Y', strtotime($inv_created_at));

        if($inv_expires_at)
        {
            $values['inv_expires_at'] = date('d F Y', strtotime($inv_expires_at));
        }
        else
        {
            $values['inv_expires_at'] = "";
        }

        $values['inv_fee_table'] = $inv_fees;

        $reference = $invoice->getFormEntry()->getFormId()."/".$invoice->getFormEntry()->getEntryId()."/".$invoice->getId();
        $values['inv_payment_id'] = $reference;

        $ssl_suffix = "s";

        if (empty($_SERVER['HTTPS'])) {
            $ssl_suffix = "";
        }

        require_once dirname(__FILE__).'/../../html/barcode/barcode.class.php';
        $bar	= new BARCODE();

        $qr_values[0] 	= $invoice->getFormEntry()->getApplicationId();
        $values['qr_code'] = '<img src="http://'.$_SERVER['HTTP_HOST'].'/'.$bar->QRCode_link('text', $qr_values, 100, 2).'">';

        $values['qr_code_small'] = '<img src="http://'.$_SERVER['HTTP_HOST'].'/'.$bar->QRCode_link('text', $qr_values, 30, 2).'">';

        $values['bar_code'] = '<img src="http://'.$_SERVER['HTTP_HOST'].'/'.$bar->BarCode_link("CODE128",$invoice->getFormEntry()->getApplicationId(), 40, "#000000", "#ff6600").'">';

        $values['bar_code_small'] = '<img src="http://'.$_SERVER['HTTP_HOST'].'/'.$bar->BarCode_link("CODE128",$invoice->getFormEntry()->getApplicationId(), 20, "#000000", "#ff6600").'">';

        return $values;
    }

    /***
     * Retrieves all the invoice details as an array
     *
     * @param $invoice_id the id of the invoice
     * @return array
     */
    function getArchiveInvoiceDetails($invoice_id)
    {
        $values = array();

        $invoice_manager = new InvoiceManager();

        $invoice = Doctrine_Core::getTable("MfInvoiceArchive")->find($invoice_id);
        $application = Doctrine_Core::getTable("FormEntryArchive")->find($invoice->getAppId());

        $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

        $query = "select * from ap_form_payments where form_id = ".$application->getFormId()." and record_id = ".$application->getEntryId();
        $payment_results = mysql_query($query,$dbconn);
        $payment_row = mysql_fetch_assoc($payment_results);

        $inv_title = $invoice->getInvoiceNumber();
        $inv_created_at = substr($invoice->getCreatedAt(), 0, 10);
        $inv_expires_at = "";

        if($invoice->getExpiresAt())
        {
            $inv_expires_at = substr($invoice->getExpiresAt(), 0, 10);
        }
        else
        {
            $q = Doctrine_Query::create()
                ->from("Invoicetemplates a")
                ->where("a.applicationform = ?", $application->getFormId())
                ->limit(1);
            $template = $q->fetchOne();
            if($template)
            {
                if($template->getMaxDuration() > 0)
                {
                    $date = strtotime("+".$template->getMaxDuration()." day");
                    $inv_expires_at = date('Y-m-d', $date);
                }
            }

        }

        $currency = "";

        $q = Doctrine_Query::create()
            ->from("Invoicetemplates a")
            ->where("a.applicationform = ?", $application->getFormId())
            ->limit(1);
        $invoice_template = $q->fetchOne();

        $q = Doctrine_Query::create()
            ->from("ApForms a")
            ->where("a.form_id = ?", $invoice_template->getApplicationform());
        $form = $q->fetchOne();

        if($form)
        {
          $currency = $form->getPaymentCurrency();
        }
        else
        {
          $currency = sfConfig::get("app_currency");
        }

        $inv_fees = "";

        $inv_fees = $inv_fees.'
                 <table width="2000px" cellspacing="0" cellpadding="0" border-spacing="0" style="border: 1px solid #d2d2d2; margin-bottom: 20px; width:100%;">
                 <thead><tr><th width="20%" style="border-right: 1px solid #d2d2d2; padding:5px 12px; text-align:left;" nowrap>Service Code</th><th width="80%" style="border-right: 1px solid #d2d2d2; padding:5px 12px; text-align:left;" nowrap>Service Description</th><th style="padding:5px 12px; text-align:left;" nowrap>Amount ('.$currency.')</th></tr></thead><tbody>';
        $grand_total = 0;
        $inv_details = $invoice->getMfInvoiceDetail();
        $total_found = false;
        foreach($inv_details as $inv_detail)
        {
            if($inv_detail->getDescription() == "Attached Invoice")
            {
                $inv_fees = $inv_fees.'<tr><td align="left" width="20%" style="border-right: 1px solid #d2d2d2; padding:5px 12px; border-top: 1px solid #d2d2d2;">-</td><td align="left" width="80%" style="border-right: 1px solid #d2d2d2; padding:5px 12px; border-top: 1px solid #d2d2d2;">'.$inv_detail->getDescription().'</td><td align="left" style="border-top: 1px solid #d2d2d2; padding:5px 12px;">'.$inv_detail->getAmount().'</td></tr>';
            }
            else
            {
                if($inv_detail->getDescription() == "Fee" && $inv_detail->getAmount() == "0")
                {
                    //Dont display
                }
                else
                {
                    $pieces = explode(" - ", $inv_detail->getDescription());
                    if(sizeof($pieces) > 1)
                    {
                        $description = $pieces[1];

                        $q = Doctrine_Query::create()
                            ->from("ApForms a")
                            ->where("a.form_code = ?", $pieces[0])
                            ->limit(1);
                        $apform = $q->fetchOne();

                        if($apform)
                        {
                            $description = $apform->getFormName();
                        }

                        $inv_fees = $inv_fees.'<tr><td align="left" width="20%" style="border-right: 1px solid #d2d2d2; padding:5px 12px; border-top: 1px solid #d2d2d2;">'.$pieces[0].'</td><td align="left" width="80%" style="border-right: 1px solid #d2d2d2; padding:5px 12px; border-top: 1px solid #d2d2d2;">'.$description.'</td><td align="left" style="border-top: 1px solid #d2d2d2; padding:5px 12px;">'.$inv_detail->getAmount().'</td></tr>';
                    }
                    else
                    {
                        $inv_fees = $inv_fees.'<tr><td align"left" width="20%" style="border-right: 1px solid #d2d2d2; padding:5px 12px; border-top: 1px solid #d2d2d2;">-</td><td align="left" width="80%" style="border-right: 1px solid #d2d2d2; padding:5px 12px; border-top: 1px solid #d2d2d2;">'.$inv_detail->getDescription().'</td><td align="left" style="border-top: 1px solid #d2d2d2; padding:5px 12px;">'.$inv_detail->getAmount().'</td></tr>';
                    }
                    $grand_total += $inv_detail->getAmount();
                }
            }

            if($inv_detail->getDescription() == "Total")
            {
                $total_found = true;
            }
        }

        if($total_found == false)
        {
            $currency = "";

            $q = Doctrine_Query::create()
                ->from("Invoicetemplates a")
                ->where("a.applicationform = ?", $application->getFormId())
                ->limit(1);
            $invoice_template = $q->fetchOne();

            $q = Doctrine_Query::create()
                ->from("ApForms a")
                ->where("a.form_id = ?", $invoice_template->getApplicationform());
            $form = $q->fetchOne();

            if($form)
            {
              $currency = $form->getPaymentCurrency();
            }
            else
            {
              $currency = sfConfig::get("app_currency");
            }

            $inv_fees = $inv_fees.'<tr><td align="left" width="20%" style="border-right: 1px solid #d2d2d2; padding:5px 12px; border-top: 1px solid #d2d2d2;"></td><td align="right" width="80%" style="border-right: 1px solid #d2d2d2; padding:5px 12px; border-top: 1px solid #d2d2d2;"><b>Total ('.$currency.')</b></td><td align="left" style="border-top: 1px solid #d2d2d2; padding:5px 12px;">'.$grand_total.'</td></tr>';
        }

        $inv_fees = $inv_fees.'</tbody>
                 </table>';

        $status = "";

        $db_date_event = str_replace('/', '-', $invoice->getExpiresAt());

        $db_date_event = strtotime($db_date_event);

        if (time() > $db_date_event && !($invoice->getPaid() == "15" || $invoice->getPaid() == "2" || $invoice->getPaid() == "3"))
        {
            $status = '<font color="#D00000">Expired</font>';
        }
        else if($invoice->getPaid() == "2")
        {
            $status = '<font color="#00CC00">Paid</font>';
        }
        else if($invoice->getPaid() == "15")
        {
            $status = "Part Payment";
        }
        else if($invoice->getPaid() == "1")
        {
            $status = '<font color="#D00000">Not Paid</font>';
        }

        if($invoice->getPaid() == "3")
        {
            $status = '<font color="#D00000">Cancelled</font>';
        }

        $values['inv_status'] = $status;

        $values['inv_no'] = $inv_title;

        $values['in_total'] = $invoice->getTotalAmount();

        $values['inv_total'] = $invoice->getTotalAmount();

        $values['inv_balance'] = $invoice_manager->get_invoice_total_owed($invoice->getId());

        $values['inv_date_created'] = date('d F Y', strtotime($inv_created_at));

        if($inv_expires_at)
        {
            $values['inv_expires_at'] = date('d F Y', strtotime($inv_expires_at));
        }
        else
        {
            $values['inv_expires_at'] = "";
        }

        $values['inv_fee_table'] = $inv_fees;

        $reference = $invoice->getFormEntry()->getFormId()."/".$invoice->getFormEntry()->getEntryId()."/".$invoice->getId();
        $values['inv_payment_id'] = $reference;


        return $values;
    }

    /***
     * Retrieves all the permit details as an array
     *
     * @param $permit_id the id of the permit
     * @return array
     */
    function getPermitDetails($permit_id)
    {
        $saved_permit = Doctrine_Core::getTable("SavedPermit")->find($permit_id);

        if($saved_permit->getDateOfIssue())
        {
            $values['ap_issue_date'] = date('d F Y', strtotime($saved_permit->getDateOfIssue()));
        }
        else
        {
            $values['ap_issue_date'] = "";
        }

        $values['ap_permit_id'] = $saved_permit->getPermitId();
        $values['uuid'] = $saved_permit->getRemoteUpdateUuid();
        //OTB pacth special condition
        $values['sp_condition'] = "N/A";
        //OTB patch signature link 
                    $values['c_signature'] = $saved_permit->getPermitApproverInfo()->getApprovalSignature();
                     //director
                    $values['c_approval_text'] = $saved_permit->getPermitApproverInfo()->getApprovalText();
        //Add extra field for Temporary authorization validity
          $values['ap_permit_valid'] = Null ;          
         
        //get special condition value saved.
        $q1 = Doctrine_Query::create()
                ->from('PermitCache c')
                ->where('c.application_id = ? ',$saved_permit->getApplicationId())
                ->limit(1);
        $q1_res = $q1->fetchOne();
        //
        if($q1_res){
            $values['sp_condition'] = $q1_res->getSpCondition() ; //get special condition value
            $values['ap_permit_valid'] = $q1_res->getValidityPeriod()." ".$q1_res->getPeriodUom() ;
        }else {
            $datetime1 = new DateTime($saved_permit->getDateOfIssue());
            $datetime2 = new DateTime($saved_permit->getDateOfExpiry());
            $interval = $datetime1->diff($datetime2);
            //http://php.net/manual/en/dateinterval.format.php
            $values['ap_permit_valid'] = $interval->format('%m month(s), %d days') ;
         
            
        }

        if(empty($saved_permit->getRemoteUpdateUuid()))
        {
            $permit_manager = new PermitManager();
            $values['uuid'] = $permit_manager->generate_uuid();
            error_log("UUID Log: ".$uuid);

            $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
            mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

            $sql = "UPDATE saved_permit SET remote_update_uuid = '".$uuid."' WHERE id = ".$saved_permit->getId();
            $result = mysql_query($sql, $db_connection);
        }
        else
        {
            $values['uuid'] = $saved_permit->getRemoteUpdateUuid();
        }

        if($saved_permit->getDateOfExpiry())
        {
            $values['ap_expire_date'] = date('d F Y', strtotime($saved_permit->getDateOfExpiry()));
        }
        else
        {
            $values['ap_expire_date'] = "";
        }

        $ssl_suffix = "s";

        if (empty($_SERVER['HTTPS'])) {
            $ssl_suffix = "";
        }

        require_once dirname(__FILE__).'/../../html/barcode/barcode.class.php';
        $bar	= new BARCODE();

        //$qr_values[0] 	= $saved_permit->getFormEntry()->getApplicationId();//OTB Commented this to allow for Permit QR Code Check
		$qr_values[0] = "http://".$_SERVER[HTTP_HOST]."/index.php/permitchecker/openrequest?permitref=".$saved_permit->getId();//OTB Permit QR Code Check
        //$values['qr_code'] = '<img src="http://'.$_SERVER['HTTP_HOST'].'/'.$bar->QRCode_link('text', $qr_values, 100, 2).'">';//OTB Commented this to allow for Permit QR Code Check
        $values['qr_code'] = '<img src="http://'.$_SERVER['HTTP_HOST'].'/'.$bar->QRCode_link('link', $qr_values, 100, 2).'">';//OTB Permit QR Code Check

        //$values['qr_code_small'] = '<img src="http://'.$_SERVER['HTTP_HOST'].'/'.$bar->QRCode_link('text', $qr_values, 30, 2).'">';//OTB Commented this to allow for Permit QR Code Check
        $values['qr_code_small'] = '<img src="http://'.$_SERVER['HTTP_HOST'].'/'.$bar->QRCode_link('link', $qr_values, 30, 2).'">';//OTB Permit QR Code Check

        $values['bar_code'] = '<img src="http://'.$_SERVER['HTTP_HOST'].'/'.$bar->BarCode_link("CODE128",$saved_permit->getFormEntry()->getApplicationId(), 40, "#000000", "#ff6600").'">';

        $values['bar_code_small'] = '<img src="http://'.$_SERVER['HTTP_HOST'].'/'.$bar->BarCode_link("CODE128",$saved_permit->getFormEntry()->getApplicationId(), 20, "#000000", "#ff6600").'">';

        $q = Doctrine_Query::create()
             ->from('SfGuardUser a')
             ->where('a.id = ?', $saved_permit->getFormEntry()->getUserId());
        $user = $q->fetchOne();

        if(sfConfig::get('app_enable_categories') == "no")
        {
          $account_type = "";

          if($user->getProfile()->getRegisteras() == 1)
          {
            $account_type = "citizen";
          }
          elseif($user->getProfile()->getRegisteras() == 3)
          {
            $account_type = "alien";
          }
          elseif($user->getProfile()->getRegisteras() == 4)
          {
            $account_type = "visitor";
          }

          $values['profile_pic'] = '<img src="https://account.ecitizen.go.ke/profile-picture/'.$user->getUsername().'?t='.$account_type.'" width="120px" style="border-radius: 5px;border: 2px solid;">';
        }

        //Get invoice total
        $application = $saved_permit->getFormEntry();

        foreach($application->getMfInvoice() as $invoice)
        {
            if($invoice->getPaid() == "2")
            {
                $values['inv_total'] = $invoice->getTotalAmount();
            }
        }

        $found_html = false;

        if($this->find("<html", $saved_permit->getRemoteResult()))
        {
            $found_html = true;
        }

        //Check if permit has a saved_result, if it does then try parsing its variables
        if($saved_permit->getRemoteResult() && strlen($saved_permit->getRemoteResult()) > 10 && $found_html == false)
        {
            if($saved_permit->getRemoteResult() == '{"message":"Cannot update more than one record"}')
            {
                $permit_manager = new PermitManager();

                $results = $permit_manager->get_remote_result($saved_permit->getId());

                $remote_values = json_decode($results, true); // decode to arrays not stdclass to allow parsing nested loops

                $values = array_merge($values, $remote_values['records'][0]);
            }
            elseif($saved_permit->getRemoteResult() == '"Provide a uuid when creating a new record"')
            {

            }
            elseif($saved_permit->getRemoteResult() == '"The server is under maintenance, please check again ina few minutes."')
            {

            }
            elseif(strlen($saved_permit->getRemoteResult()) > 5000)
            {

            }
            else {
                $results = $saved_permit->getRemoteResult();

                $remote_values = json_decode($results, true); // decode to arrays not stdclass to allow parsing nested loops

                $remote_values = json_decode($remote_values, true);

                if(!is_array($remote_values))
                {
                  $remote_values = json_decode($remote_values, true);
                }

                error_log("Remote Values ".$saved_permit->getId().": " . $results);

                if($remote_values == null)
                {
                    $results = $saved_permit->getRemoteResult();
                    $remote_values = json_decode($results, true);

                    if(is_array($remote_values['records']))
                    {
                        foreach($remote_values['records'] as $record)
                        {
                            $values = array_merge($values, $record);
                        }
                    }
                    elseif(is_array($remote_values))
                    {
                        $values = array_merge($values, $remote_values);
                    }
                    else
                    {
                        error_log("Remote Details: No Array Found.");
                    }


                    if(strlen($saved_permit->getRemoteResult()) > 5 && $_SESSION['SESSION_CUTEFLOW_USERID'] == 1) {
                        echo "<pre>Remote details: ", var_dump($saved_permit->getRemoteResult()), "</pre>";
                    }
                }
                else {
                    $values = array_merge($values, $remote_values);

                    if(strlen($saved_permit->getRemoteResult()) > 5 && $_SESSION['SESSION_CUTEFLOW_USERID'] == 1) {
                        echo "<pre>Error in remote details: ", var_dump($saved_permit->getRemoteResult()), "</pre>";

                        echo "<pre>Attempted decode values: ", var_dump($values), "</pre>";

                        error_log("Remote Details: ".$saved_permit->getRemoteResult());
                    }
                }
            }
        }

        //If observation has a json result then try and parse it
        if($saved_permit->getFormEntry()->getObservation())
        {
            $json_array = json_decode($saved_permit->getFormEntry()->getObservation(), true);
            if(is_array($json_array)) {
                $values = array_merge($values, $json_array);
            }
        }

        return $values;
    }

    /***
     * Retrieves all the permit details as an array
     *
     * @param $permit_id the id of the permit
     * @return array
     */
    function getArchivePermitDetails($permit_id)
    {
        $saved_permit = Doctrine_Core::getTable("SavedPermitArchive")->find($permit_id);

        if($saved_permit->getDateOfIssue())
        {
            $values['ap_issue_date'] = date('d F Y', strtotime($saved_permit->getDateOfIssue()));
        }
        else
        {
            $values['ap_issue_date'] = "";
        }

        $values['ap_permit_id'] = $saved_permit->getPermitId();
        $values['uuid'] = $saved_permit->getRemoteUpdateUuid();

        if(empty($saved_permit->getRemoteUpdateUuid()))
        {
            $permit_manager = new PermitManager();
            $values['uuid'] = $permit_manager->generate_uuid();
            error_log("UUID Log: ".$uuid);

            $db_connection = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
            mysql_select_db(sfConfig::get('app_mysql_db'),$db_connection);

            $sql = "UPDATE saved_permit_archive SET remote_update_uuid = '".$uuid."' WHERE id = ".$saved_permit->getId();
            $result = mysql_query($sql, $db_connection);
        }
        else
        {
            $values['uuid'] = $saved_permit->getRemoteUpdateUuid();
        }

        if($saved_permit->getDateOfExpiry())
        {
            $values['ap_expire_date'] = date('d F Y', strtotime($saved_permit->getDateOfExpiry()));
        }
        else
        {
            $values['ap_expire_date'] = "";
        }

        $q = Doctrine_Query::create()
             ->from('SfGuardUser a')
             ->where('a.id = ?', $saved_permit->getFormEntry()->getUserId());
        $user = $q->fetchOne();

        if(sfConfig::get('app_enable_categories') == "no")
        {
          $account_type = "";

          if($user->getProfile()->getRegisteras() == 1)
          {
            $account_type = "citizen";
          }
          elseif($user->getProfile()->getRegisteras() == 3)
          {
            $account_type = "alien";
          }
          elseif($user->getProfile()->getRegisteras() == 4)
          {
            $account_type = "visitor";
          }

          $values['profile_pic'] = '<img src="https://account.ecitizen.go.ke/profile-picture/'.$user->getUsername().'?t='.$account_type.'" width="120px" style="border-radius: 5px;border: 2px solid;">';
        }

        //Get invoice total
        $application = $saved_permit->getFormEntry();

        foreach($application->getMfInvoiceArchive() as $invoice)
        {
            if($invoice->getPaid() == "2")
            {
                $values['inv_total'] = $invoice->getTotalAmount();
            }
        }

        //Check if permit has a saved_result, if it does then try parsing its variables
        if($saved_permit->getRemoteResult() && strlen($saved_permit->getRemoteResult()) > 10)
        {
            if($saved_permit->getRemoteResult() == '{"message":"Cannot update more than one record"}')
            {
                $permit_manager = new PermitManager();

                $results = $permit_manager->get_remote_result($saved_permit->getId());

                $remote_values = json_decode($results, true); // decode to arrays not stdclass to allow parsing nested loops

                $values = array_merge($values, $remote_values['records'][0]);
            }
            elseif($saved_permit->getRemoteResult() == '"Provide a uuid when creating a new record"')
            {

            }
            elseif($saved_permit->getRemoteResult() == '"The server is under maintenance, please check again ina few minutes."')
            {

            }
            elseif(strlen($saved_permit->getRemoteResult()) > 5000)
            {

            }
            else {
                $results = $saved_permit->getRemoteResult();

                $remote_values = json_decode($results, true); // decode to arrays not stdclass to allow parsing nested loops

                $remote_values = json_decode($remote_values, true);

                if(!is_array($remote_values))
                {
                  $remote_values = json_decode($remote_values, true);
                }

                error_log("Remote Values: " . $results);
                $values = array_merge($values, $remote_values);
            }
        }

        return $values;
    }

    /***
     * Used to parse values into a user string e.g automatic submission
     *
     * @param $content string the string that contains placeholders to be replaced
     * @param $user_id the id of the user
     * @return string
     */
    public function parseUser($user_id, $content)
    {
        //User Details
        $user_details = $this->getUserDetails($user_id);

        $content = static::parseWithDust($content, $user_details);

        return $content;
    }

    /***
     * Used to parse values into an invoice template
     *
     * @param $content string the string that contains placeholders to be replaced
     * @param $application_id the id of the application
     * @param $form_id the id of the form
     * @param $entry_id the id of the entry in the form table
     * @param $invoice_id the id of the invoice
     * @return string
     */
    public function parseInvoice($application_id, $form_id, $entry_id, $invoice_id, $content)
    {
        $application = Doctrine_Core::getTable("FormEntry")->find($application_id);
        $user_id = $application->getUserId();

        //User Details
        $user_details = $this->getUserDetails($user_id);

        //Application Details
        $application_details = $this->getApplicationDetails($application_id);

        //Invoice Details
        $invoice_details = $this->getInvoiceDetails($invoice_id);

        $values = array_merge($user_details, $application_details);
        $values = array_merge($values, $invoice_details);

        $content = static::parseWithDust($content, $values);

        return $content;
    }

    /***
     * Used to parse values into an invoice template
     *
     * @param $content string the string that contains placeholders to be replaced
     * @param $application_id the id of the application
     * @param $form_id the id of the form
     * @param $entry_id the id of the entry in the form table
     * @param $invoice_id the id of the invoice
     * @return string
     */
    public function parseArchiveInvoice($application_id, $form_id, $entry_id, $invoice_id, $content)
    {
        $application = Doctrine_Core::getTable("FormEntryArchive")->find($application_id);
        $user_id = $application->getUserId();

        //User Details
        $user_details = $this->getUserDetails($user_id);

        //Application Details
        $application_details = $this->getArchiveApplicationDetails($application_id);

        //Invoice Details
        $invoice_details = $this->getArchiveInvoiceDetails($invoice_id);

        $values = array_merge($user_details, $application_details);
        $values = array_merge($values, $invoice_details);

        $content = static::parseWithDust($content, $values);

        return $content;
    }

    public function parseInvoicePDF($application_id, $form_id, $entry_id, $invoice_id, $content)
    {
        $saved_application = Doctrine_Core::getTable("FormEntry")->find($application_id);
		$application = $saved_application;//OTB to avoid null reference at line 6781 below
        $user = Doctrine_Core::getTable("SfGuardUser")->find($saved_application->getUserId());

        $user_profile = $user->getProfile();

        $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
        mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

        $q = Doctrine_Query::create()
            ->from('mfUserProfile a')
            ->where('a.user_id = ?', $user->getId())
            ->limit(1);
        $formprofile = $q->fetchOne();

        $profile_form = "";

        if($formprofile)
        {
            $prof_entry_id = $formprofile->getEntryId();

            $sql = "SELECT * FROM ap_form_15 WHERE id = ".$prof_entry_id;
            $prof_results = mysql_query($sql,$dbconn);

            $profile_form = mysql_fetch_assoc($prof_results);
        }

        $app_entry_id = $saved_application->getEntryId();

        $sql = "SELECT * FROM ap_form_".$form_id." WHERE id = ".$app_entry_id;
        $app_results = mysql_query($sql,$dbconn);

        $apform = mysql_fetch_assoc($app_results);

        $conditions = "";

        $invoice_total = 0;

        $q = Doctrine_Query::create()
            ->from('approvalCondition a')
            ->where('a.entry_id = ?', $saved_application->getId());
        $approvalconditions = $q->execute();
        $conditions = "<ul>";
        foreach($approvalconditions as $approval)
        {
            $conditions = $conditions."<li>".$approval->getCondition()->getShortName().". ".$approval->getCondition()->getDescription()."</li>";
        }

        $q = Doctrine_Query::create()
            ->from('mfInvoiceDetail a')
            ->where('a.invoice_id = ? AND a.description = ?', array($invoice_id,'Total'));
        $details = $q->execute();
        foreach($details as $detail)
        {
            $invoice_total = $invoice_total + $detail->getAmount();
        }

        //Get User Information (anything starting with sf_ )
        //sf_email, sf_fullname, sf_username, ... other fields in the dynamic user profile form e.g sf_element_1
        if($this->find('{sf_username}', $content))
        {
            $content = str_replace('{sf_username}',$user->getUsername(), $content);
        }
        if($this->find('{sf_email}', $content))
        {
            $content = str_replace('{sf_email}',$user_profile->getEmail(), $content);
        }
        if($this->find('{sf_mobile}', $content))
        {
            $content = str_replace('{sf_mobile}',$user_profile->getMobile(), $content);
        }
        if($this->find('{sf_fullname}', $content))
        {
            $content = str_replace('{sf_fullname}',$user_profile->getFullname(), $content);
        }

        $q = Doctrine_Query::create()
            ->from('apFormElements a')
            ->where('a.form_id = ?', 15);

        $elements = $q->execute();

        foreach($elements as $element)
        {
            $childs = $element->getElementTotalChild();
            if($childs == 0)
            {
                if($this->find('{sf_element_'.$element->getElementId().'}', $content))
                {
                    $content = str_replace('{sf_element_'.$element->getElementId().'}',$profile_form['element_'.$element->getElementId()], $content);
                }
            }
            else
            {
                if($element->getElementType() == "select")
                {
                    if($this->find('{sf_element_'.$element->getElementId().'}', $content))
                    {
                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($profile_form,$element->getElementId(),$profile_form['element_'.$element->getElementId()]))
                            ->limit(1);
                        $option = $q->fetchOne();

                        if($option)
                        {
                            $content = str_replace('{sf_element_'.$element->getElementId().'}',$option->getOption(), $content);
                        }
                    }
                }
                else
                {
                    for($x = 0; $x < ($childs + 1); $x++)
                    {
                        if($this->find('{sf_element_'.$element->getElementId().'_'.($x+1).'}', $content))
                        {
                            $content = str_replace('{sf_element_'.$element->getElementId().'_'.($x+1).'}',$profile_form['element_'.$element->getElementId()."_".($x+1)], $content);
                        }
                    }
                }
            }


        }

        //Get Application Information (anything starting with ap_ )
        //ap_application_id
        if($this->find('ap_application_id', $content))
        {
            $content = str_replace('ap_application_id',$saved_application->getApplicationId(), $content);
        }

        //Get Form Details (anything starting with fm_ )
        //fm_created_at, fm_updated_at.....fm_element_1


        if($this->find('fm_created_at', $content))
        {
            if($saved_application->getDateOfSubmission())
            {
                $content = str_replace('fm_created_at', date('d F Y', strtotime(substr($saved_application->getDateOfSubmission(), 0, 11))), $content);
            }
            else
            {
                $content = str_replace('fm_created_at', "", $content);
            }
        }
        if($this->find('fm_updated_at', $content))
        {
            if($saved_application->getDateOfResponse())
            {
                $content = str_replace('fm_updated_at', date('d F Y', strtotime(substr($saved_application->getDateOfResponse(), 0, 11))), $content);
            }
            else
            {
                $content = str_replace('fm_updated_at', "", $content);
            }
        }
        if($this->find('current_date', $content))
        {
            $content = str_replace('current_date',date('d F Y', strtotime(date('Y-m-d'))), $content);
        }

        $q = Doctrine_Query::create()
            ->from('apFormElements a')
            ->where('a.form_id = ?', $form_id);

        $elements = $q->execute();

        foreach($elements as $element)
        {
            $childs = $element->getElementTotalChild();

            if($childs == 0)
            {
                if($element->getElementType() == "select")
                {
                    if($element->getElementExistingForm() && $element->getElementExistingStage())
                    {
                        $q = Doctrine_Query::create()
                            ->from("FormEntry a")
                            ->where("a.id = ?", $apform['element_'.$element->getElementId()])
                            ->limit(1);
                        $linked_application = $q->fetchOne();
                        if($linked_application)
                        {

                            $q = Doctrine_Query::create()
                                ->from("SavedPermit a")
                                ->leftJoin("a.FormEntry b")
                                ->where("b.form_id = ?", $linked_application->getFormId());
                            $permits = $q->execute();

                            foreach($permits as $saved_permit)
                            {
                                if($this->find("{ap_permit_id_".$saved_permit->getTypeId()."_element_child}", $content))
                                {
                                    $content = str_replace("{ap_permit_id_".$saved_permit->getTypeId()."_element_child}", ($saved_permit->getPermitId()?$saved_permit->getPermitId():$linked_application->getApplicationId()), $content);
                                }
                            }

                            if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                            {
                                $content = str_replace('{fm_element_'.$element->getElementId().'}', $linked_application->getApplicationId(), $content);
                            }

                            $q = Doctrine_Query::create()
                                ->from('apFormElements a')
                                ->where('a.form_id = ?', $element->getElementExistingForm())
                                ->andWhere('a.element_status = ?', 1);

                            $child_elements = $q->execute();

                            foreach($child_elements as $child_element)
                            {
                                $sql = "SELECT * FROM ap_form_".$linked_application->getFormId()." WHERE id = ".$linked_application->getEntryId();
                                $child_app_results = mysql_query($sql,$dbconn);

                                $child_apform = mysql_fetch_assoc($child_app_results);

                                if($this->find('{ap_child_application_id}', $content))
                                {
                                    $content = str_replace('{ap_child_application_id}', $linked_application->getApplicationId(), $content);
                                }

                                if($this->find('{fm_child_created_at}', $content))
                                {
                                    if($linked_application->getDateOfSubmission())
                                    {
                                        $content = str_replace('{fm_child_created_at}', date('d F Y', strtotime($linked_application->getDateOfSubmission())), $content);
                                    }
                                    else
                                    {
                                        $content = str_replace('{fm_child_created_at}', "", $content);
                                    }
                                }

                                if($this->find('{fm_child_updated_at}', $content))
                                {
                                    if($linked_application->getDateOfResponse())
                                    {
                                        $content = str_replace('{fm_child_updated_at}', date('d F Y', strtotime(substr($linked_application->getDateOfResponse(), 0, 11))), $content);
                                    }
                                    else
                                    {
                                        $content = str_replace('{fm_child_updated_at}', "", $content);
                                    }
                                }

                                //START CHILD ELEMENTS
                                $childs = $child_element->getElementTotalChild();
                                if($childs == 0)
                                {
                                    if($child_element->getElementType() == "select")
                                    {
                                        if($child_element->getElementExistingForm() && $child_element->getElementExistingStage())
                                        {

                                            $q = Doctrine_Query::create()
                                                ->from("FormEntry a")
                                                ->where("a.id = ?", $child_apform['element_'.$child_element->getElementId()])
                                                ->limit(1);
                                            $linked_grand_application = $q->fetchOne();
                                            if($linked_grand_application)
                                            {

                                                $q = Doctrine_Query::create()
                                                    ->from("SavedPermit a")
                                                    ->leftJoin("a.FormEntry b")
                                                    ->where("b.form_id = ?", $linked_grand_application->getFormId());
                                                $permits = $q->execute();

                                                foreach($permits as $saved_permit)
                                                {
                                                    if($this->find("{ap_permit_id_".$saved_permit->getTypeId()."_element_grand_child}", $content))
                                                    {
                                                        $content = str_replace("{ap_permit_id_".$saved_permit->getTypeId()."_element_grand_child}", ($saved_permit->getPermitId()?$saved_permit->getPermitId():$linked_grand_application->getApplicationId()), $content);
                                                    }
                                                }

                                                if($this->find('{fm_child_element_'.$child_element->getElementId().'}', $content))
                                                {
                                                    $content = str_replace('{fm_child_element_'.$child_element->getElementId().'}', $linked_grand_application->getApplicationId(), $content);
                                                }

                                                $q = Doctrine_Query::create()
                                                    ->from('apFormElements a')
                                                    ->where('a.form_id = ?', $child_element->getElementExistingForm())
                                                    ->andWhere('a.element_status = ?', 1);

                                                $grand_child_elements = $q->execute();

                                                foreach($grand_child_elements as $grand_child_element)
                                                {

                                                    $sql = "SELECT * FROM ap_form_".$child_element->getElementExistingForm()." WHERE id = ".$linked_grand_application->getEntryId();
                                                    $child_app_results = mysql_query($sql,$dbconn);

                                                    $grand_child_apform = mysql_fetch_assoc($child_app_results);

                                                    if($this->find('{ap_grand_child_application_id}', $content))
                                                    {
                                                        $content = str_replace('{ap_grand_child_application_id}', $linked_grand_application->getApplicationId(), $content);
                                                    }

                                                    if($this->find('{fm_grand_child_created_at}', $content))
                                                    {
                                                        $date = "";
                                                        if($linked_grand_application->getDateOfSubmission())
                                                        {
                                                            $date = date('d F Y', strtotime($linked_grand_application->getDateOfSubmission()));
                                                        }
                                                        $content = str_replace('{fm_grand_child_created_at}', $date, $content);
                                                    }

                                                    if($this->find('{fm_grand_child_updated_at}', $content))
                                                    {
                                                        $date = "";
                                                        if($linked_grand_application->getDateOfResponse())
                                                        {
                                                            $date = date('d F Y', strtotime(substr($linked_grand_application->getDateOfResponse(), 0, 11)));
                                                        }
                                                        $content = str_replace('{fm_grand_child_updated_at}', $date, $content);
                                                    }

                                                    //START GRAND CHILD ELEMENTS
                                                    $childs = $grand_child_element->getElementTotalChild();
                                                    if($childs == 0)
                                                    {
                                                        if($grand_child_element->getElementType() == "select")
                                                        {//select
                                                            if($this->find('{fm_grand_child_element_'.$grand_child_element->getElementId().'}', $content))
                                                            {
                                                                $opt_value = 0;
                                                                if($grand_child_apform['element_'.$grand_child_element->getElementId()] == "0")
                                                                {
                                                                    $opt_value++;
                                                                }
                                                                else
                                                                {
                                                                    $opt_value = $grand_child_apform['element_'.$grand_child_element->getElementId()];
                                                                }

                                                                $q = Doctrine_Query::create()
                                                                    ->from('ApElementOptions a')
                                                                    ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($child_element->getElementExistingForm(),$grand_child_element->getElementId(),$opt_value))
                                                                    ->limit(1);
                                                                $option = $q->fetchOne();

                                                                if($option)
                                                                {
                                                                    $content = str_replace('{fm_grand_child_element_'.$grand_child_element->getElementId().'}',$option->getOption(), $content);
                                                                }
                                                            }
                                                        }
                                                        elseif($grand_child_element->getElementType() == "checkbox")
                                                        {
                                                            if($this->find('{fm_grand_child_element_'.$grand_child_element->getElementId().'}', $content))
                                                            {
                                                                $q = Doctrine_Query::create()
                                                                    ->from('ApElementOptions a')
                                                                    ->where('a.form_id = ? AND a.element_id = ?', array($child_element->getElementExistingForm(),$grand_child_element->getElementId()));
                                                                $options = $q->execute();

                                                                $options_text = "<ul>";
                                                                foreach($options as $option)
                                                                {
                                                                    if($grand_child_apform['element_'.$grand_child_element->getElementId()."_".$option->getOptionId()])
                                                                    {
                                                                        $options_text .= "<li>".$option->getOption()."</li>";
                                                                    }
                                                                }
                                                                $options_text .= "</ul>";

                                                                $content = str_replace('{fm_grand_child_element_'.$grand_child_element->getElementId()."}",$options_text, $content);
                                                            }
                                                        }
                                                        else
                                                        {//text
                                                            if($this->find('{fm_grand_child_element_'.$grand_child_element->getElementId().'}', $content))
                                                            {
                                                                $content = str_replace('{fm_grand_child_element_'.$grand_child_element->getElementId().'}', $grand_child_apform['element_'.$grand_child_element->getElementId()], $content);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        for($x = 0; $x < ($childs + 1); $x++)
                                                        {
                                                            if($this->find('{fm_grand_child_element_'.$grand_child_element->getElementId().'}', $content))
                                                            {
                                                                $content = str_replace('{fm_grand_child_element_'.$grand_child_element->getElementId()."}",$grand_child_apform['element_'.$grand_child_element->getElementId()."_".($x+1)], $content);
                                                            }
                                                        }
                                                    }
                                                    //END GRAND CHILD ELEMENTS
                                                }

                                            }

                                        }
                                        else
                                        { //select
                                            if($this->find('{fm_child_element_'.$child_element->getElementId().'}', $content))
                                            {
                                                $opt_value = 0;
                                                if($child_apform['element_'.$child_element->getElementId()] == "0")
                                                {
                                                    $opt_value++;
                                                }
                                                else
                                                {
                                                    $opt_value = $child_apform['element_'.$child_element->getElementId()];
                                                }

                                                $q = Doctrine_Query::create()
                                                    ->from('ApElementOptions a')
                                                    ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($linked_application->getFormId(),$child_element->getElementId(),$opt_value))
                                                    ->limit(1);
                                                $option = $q->fetchOne();

                                                if($option)
                                                {
                                                    $content = str_replace('{fm_child_element_'.$child_element->getElementId().'}',$option->getOption(), $content);
                                                }
                                            }
                                        }
                                    }
                                    elseif($child_element->getElementType() == "checkbox")
                                    {
                                        if($this->find('{fm_child_element_'.$child_element->getElementId().'}', $content))
                                        {
                                            $q = Doctrine_Query::create()
                                                ->from('ApElementOptions a')
                                                ->where('a.form_id = ? AND a.element_id = ?', array($linked_application->getFormId(),$child_element->getElementId()));
                                            $options = $q->execute();

                                            $options_text = "<ul>";
                                            foreach($options as $option)
                                            {
                                                if($child_apform['element_'.$child_element->getElementId()."_".$option->getOptionId()])
                                                {
                                                    $options_text .= "<li>".$option->getOption()."</li>";
                                                }
                                            }
                                            $options_text .= "</ul>";

                                            $content = str_replace('{fm_child_element_'.$child_element->getElementId()."}",$options_text, $content);
                                        }
                                        continue;
                                    }
                                    else
                                    { //text
                                        if($this->find('{fm_child_element_'.$child_element->getElementId().'}', $content))
                                        {
                                            $content = str_replace('{fm_child_element_'.$child_element->getElementId().'}', $child_apform['element_'.$child_element->getElementId()], $content);
                                        }
                                    }
                                }
                                else
                                {
                                    for($x = 0; $x < ($childs + 1); $x++)
                                    {
                                        if($this->find('{fm_child_element_'.$child_element->getElementId().'}', $content))
                                        {
                                            $content = str_replace('{fm_child_element_'.$child_element->getElementId()."}",$child_apform['element_'.$child_element->getElementId()."_".($x+1)], $content);
                                        }
                                    }
                                }
                                //END CHILD ELEMENTS
                            }


                            continue;
                        }
                    }
                    else
                    {
                        if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                        {
                            $opt_value = 0;
                            if($apform['element_'.$element->getElementId()] == "0")
                            {
                                $opt_value++;
                            }
                            else
                            {
                                $opt_value = $apform['element_'.$element->getElementId()];
                            }

                            $q = Doctrine_Query::create()
                                ->from('ApElementOptions a')
                                ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($form_id,$element->getElementId(),$opt_value))
                                ->limit(1);
                            $option = $q->fetchOne();


                            if($option)
                            {
                                $content = str_replace('{fm_element_'.$element->getElementId().'}',$option->getOption(), $content);
                            }
                        }
                    }
                }

                if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                {
                    $content = str_replace('{fm_element_'.$element->getElementId().'}',$apform['element_'.$element->getElementId()], $content);
                }
                else
                {
                    for($x = 0; $x < ($childs + 1); $x++)
                    {
                        if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                        }
                    }

                }
            }
            else
            {

                if($element->getElementType() == "select")
                {
                    if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                    {
                        $opt_value = 0;
                        if($apform['element_'.$element->getElementId()] == "0")
                        {
                            $opt_value++;
                        }
                        else
                        {
                            $opt_value = $apform['element_'.$element->getElementId()];
                        }

                        $q = Doctrine_Query::create()
                            ->from('ApElementOptions a')
                            ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($form_id,$element->getElementId(),$opt_value))
                            ->limit(1);
                        $option = $q->fetchOne();


                        if($option)
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId().'}',$option->getOption(), $content);
                        }
                    }
                }
                else
                {
                    for($x = 0; $x < ($childs + 1); $x++)
                    {
                        if($this->find('{fm_element_'.$element->getElementId().'}', $content))
                        {
                            $content = str_replace('{fm_element_'.$element->getElementId()."}",$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                        }
                    }

                }




            }
            for($x = 0; $x < ($childs + 1); $x++)
            {
                if($this->find('{fm_element_'.$element->getElementId()."_".($x+1).'}', $content))
                {
                    $content = str_replace('{fm_element_'.$element->getElementId()."_".($x+1).'}',$apform['element_'.$element->getElementId()."_".($x+1)], $content);
                }
            }

        }

        //Get Conditions of Approval (anything starting with ca_ )
        //ca_conditions
        if($this->find('{ca_conditions}', $content))
        {
            $content = str_replace('{ca_conditions}', $conditions , $content);
        }

        //Get Invoice Details (anything starting with in_ )
        //in_total
        $q = Doctrine_Query::create()
            ->from('MfInvoice a')
            ->where('a.id = ?', $invoice_id)
            ->limit(1);
        $invoice = $q->fetchOne();

        $inv_title = $invoice->getInvoiceNumber();
        $inv_created_at = substr($invoice->getCreatedAt(), 0, 10);
        if($invoice->getExpiresAt())
        {
            $inv_expires_at = substr($invoice->getExpiresAt(), 0, 10);
        }
        else
        {
            $q = Doctrine_Query::create()
                ->from("Invoicetemplates a")
                ->where("a.applicationform = ?", $saved_application->getFormId())
                ->limit(1);
            $template = $q->fetchOne();
            if($template)
            {
                if($template->getMaxDuration() > 0)
                {
                    $date = strtotime("+".$template->getMaxDuration()." day");
                    $inv_expires_at = date('Y-m-d', $date);
                }
            }

        }

        $currency = "";

        $q = Doctrine_Query::create()
            ->from("Invoicetemplates a")
            ->where("a.applicationform = ?", $application->getFormId())
            ->limit(1);
        $invoice_template = $q->fetchOne();

        $q = Doctrine_Query::create()
            ->from("ApForms a")
            ->where("a.form_id = ?", $invoice_template->getApplicationform());
        $form = $q->fetchOne();

        if($form)
        {
          $currency = $form->getPaymentCurrency();
        }
        else
        {
          $currency = sfConfig::get("app_currency");
        }

        $inv_fees = "";

        $inv_fees = $inv_fees.'
             <table width="2000px" cellspacing="0" cellpadding="0" border-spacing="0" style="border: 1px solid #d2d2d2; margin-bottom: 20px; width:100%;">
             <thead><tr><th width="20%" style="border-right: 1px solid #d2d2d2; padding:5px 12px; text-align:left;" nowrap>Service Code</th><th width="80%" style="border-right: 1px solid #d2d2d2; padding:5px 12px; text-align:left;" nowrap>Service Description</th><th style="padding:5px 12px; text-align:left;" nowrap>Amount ('.$currency.')</th></tr></thead><tbody>';
        $grand_total = 0;
        $inv_details = $invoice->getMfInvoiceDetail();
        $total_found = false;
        foreach($inv_details as $inv_detail)
        {
            if($inv_detail->getDescription() == "Attached Invoice")
            {
                $url = html_entity_decode($detail->getAmount());
                $inv_fees = $inv_fees."<tr><td align='left' width='20%' style='border-right: 1px solid #d2d2d2; padding:5px 12px; border-top: 1px solid #d2d2d2;'>-</td><td align='left' width='80%' style='border-right: 1px solid #d2d2d2; padding:5px 12px; border-top: 1px solid #d2d2d2;'>".$inv_detail->getDescription()."</td><td align='left' style='border-top: 1px solid #d2d2d2; padding:5px 12px;'>".$inv_detail->getAmount()."</td></tr>";
            }
            else
            {
                if($inv_detail->getDescription() == "Fee" && $inv_detail->getAmount() == "0")
                {
                    //Dont display
                }
                else
                {
                    $pieces = explode(" - ", $inv_detail->getDescription());
                    if(sizeof($pieces) > 1)
                    {
                        $description = $pieces[1];

                        $q = Doctrine_Query::create()
                            ->from("ApForms a")
                            ->where("a.form_code = ?", $pieces[0])
                            ->limit(1);
                        $apform = $q->fetchOne();

                        if($apform)
                        {
                            $description = $apform->getFormName();
                        }

                        $inv_fees = $inv_fees."<tr><td align='left' width='20%' style='border-right: 1px solid #d2d2d2; padding:5px 12px; border-top: 1px solid #d2d2d2;'>".$pieces[0]."</td><td align='left' width='80%' style='border-right: 1px solid #d2d2d2; padding:5px 12px; border-top: 1px solid #d2d2d2;'>".$description."</td><td align='left' style='border-top: 1px solid #d2d2d2; padding:5px 12px;'>".$inv_detail->getAmount()."</td></tr>";
                    }
                    else
                    {
                        $inv_fees = $inv_fees."<tr><td align='left' width='20%' style='border-right: 1px solid #d2d2d2; padding:5px 12px; border-top: 1px solid #d2d2d2;'>-</td><td align='left' width='80%' style='border-right: 1px solid #d2d2d2; padding:5px 12px; border-top: 1px solid #d2d2d2;'>".$inv_detail->getDescription()."</td><td align='left' style='border-top: 1px solid #d2d2d2; padding:5px 12px;'>".$inv_detail->getAmount()."</td></tr>";
                    }
                    $grand_total += $inv_detail->getAmount();
                }
            }

            if($inv_detail->getDescription() == "Total")
            {
                $total_found = true;
            }
        }

        if($total_found == false)
        {
            $currency = "";

            $q = Doctrine_Query::create()
                ->from("Invoicetemplates a")
                ->where("a.applicationform = ?", $application->getFormId())
                ->limit(1);
            $invoice_template = $q->fetchOne();

            $q = Doctrine_Query::create()
                ->from("ApForms a")
                ->where("a.form_id = ?", $invoice_template->getApplicationform());
            $form = $q->fetchOne();

            if($form)
            {
              $currency = $form->getPaymentCurrency();
            }
            else
            {
              $currency = sfConfig::get("app_currency");
            }

            $inv_fees = $inv_fees."<tr><td align='left' width='20%' style='border-right: 1px solid #d2d2d2; padding:5px 12px; border-top: 1px solid #d2d2d2;'></td><td align='right' width='80%' style='border-right: 1px solid #d2d2d2; padding:5px 12px; border-top: 1px solid #d2d2d2;'><b>Total (".$currency.")</b></td><td align='left' style='border-top: 1px solid #d2d2d2; padding:5px 12px;'>".$grand_total."</td></tr>";
        }

        $inv_fees = $inv_fees."</tbody>
             </table>";


        if($this->find('{in_total}', $content))
        {
            $content = str_replace('{in_total}', $invoice_total , $content);
        }

        if($this->find('{inv_no}', $content))
        {
            $content = str_replace('{inv_no}', $inv_title , $content);
        }

        if($this->find('{inv_date_created}', $content))
        {
            $content = str_replace('{inv_date_created}', date('d F Y', strtotime($inv_created_at)) , $content);
        }

        if($this->find('{inv_expires_at}', $content))
        {
            if($inv_expires_at)
            {
                $content = str_replace('{inv_expires_at}', date('d F Y', strtotime($inv_expires_at)) , $content);
            }
            else
            {
                $content = str_replace('{inv_expires_at}', "", $content);
            }
        }

        if($this->find('{inv_fee_table}', $content))
        {
            $content = str_replace('{inv_fee_table}', $inv_fees , $content);
        }

        if($this->find('{inv_status}', $content))
        {
            $status = "";

            $expired = false;
            $cancelled = false;

            $db_date_event = str_replace('/', '-', $invoice->getExpiresAt());

            $db_date_event = strtotime($db_date_event);

            if (time() > $db_date_event && !($invoice->getPaid() == "15" || $invoice->getPaid() == "2" || $invoice->getPaid() == "3"))
            {
                $status = "Expired";
            }
            else if($invoice->getPaid() == "2")
            {
                $status = "Paid";
            }
            else if($invoice->getPaid() == "15")
            {
                $status = "Pending Confirmation";
            }
            else if($invoice->getPaid() == "1")
            {
                $status = "Not Paid";
            }

            if($invoice->getPaid() == "3")
            {
                $status = 'Cancelled';
            }

            $content = str_replace('{inv_status}', $status , $content);
        }

        $query = "select * from ap_form_payments where form_id = ".$form_id." and record_id = ".$entry_id;
        $payment_results = mysql_query($query,$dbconn);
        $payment_row = mysql_fetch_assoc($payment_results);

        if($this->find('{inv_payment_id}', $content))
        {
            $payment_id = "";

            if($payment_row)
            {
                $payment_id = ucwords($payment_row['payment_id']);
            }

            $content = str_replace('{inv_payment_id}', $payment_id, $content);
        }

        if($this->find('{inv_payment_merchant_type}', $content))
        {
            $merchant_type = "";

            if($payment_row)
            {
                $merchant_type = ucwords($payment_row['payment_merchant_type']);
            }

            $content = str_replace('{inv_payment_merchant_type}', $merchant_type, $content);
        }

        return $content;

    }

    public function parseHeaders($form_id, $content)
    {

        //Get User Information (anything starting with sf_ )
        //sf_email, sf_fullname, sf_username, ... other fields in the dynamic user profile form e.g sf_element_1
        if($this->find('{sf_username}', $content))
        {
            $content = str_replace('{sf_username}',"Architect's Username", $content);
        }
        if($this->find('{sf_email}', $content))
        {
            $content = str_replace('{sf_email}',"Architect's Email", $content);
        }
        if($this->find('{sf_fullname}', $content))
        {
            $content = str_replace('{sf_fullname}',"Architect's Fullname", $content);
        }

        $q = Doctrine_Query::create()
            ->from('apFormElements a')
            ->where('a.form_id = ?', 15);

        $elements = $q->execute();

        foreach($elements as $element)
        {
            if($this->find('{sf_element_'.$element->getElementId().'}', $content))
            {
                $content = str_replace('{sf_element_'.$element->getElementId().'}',$element->getElementTitle(), $content);
            }
        }

        //Get Application Information (anything starting with ap_ )
        //ap_application_id
        if($this->find('{ap_application_id}', $content))
        {
            $content = str_replace('{ap_application_id}',"Application Number", $content);
        }

        //Get Form Details (anything starting with fm_ )
        //fm_created_at, fm_updated_at.....fm_element_1


        if($this->find('{fm_created_at}', $content))
        {
            $content = str_replace('{fm_created_at}', "Submitted On", $content);
        }
        if($this->find('{fm_updated_at}', $content))
        {
            $content = str_replace('{fm_updated_at}', "Last Updated", $content);
        }
        if($this->find('{current_date}', $content))
        {
            $content = str_replace('{current_date}', "Current Date", $content);
        }

        $q = Doctrine_Query::create()
            ->from('apFormElements a')
            ->where('a.form_id = ?', $form_id);

        $elements = $q->execute();

        foreach($elements as $element)
        {
            if($this->find('{fm_element_'.$element->getElementId().'}', $content))
            {
                $content = str_replace('{fm_element_'.$element->getElementId().'}',$element->getElementTitle(), $content);
            }
        }

        //Get Conditions of Approval (anything starting with ca_ )
        //ca_conditions
        if($this->find('{ca_conditions}', $content))
        {
            $content = str_replace('{ca_conditions}', "Conditions Of Approval" , $content);
        }

        //mini_ca_conditions
        if($this->find('{mini_ca_conditions}', $content))
        {
            $content = str_replace('{mini_ca_conditions}', "Conditions Of Approval" , $content);
        }
        
        //Get Invoice Details (anything starting with in_ )
        //in_total

        if($this->find('{in_total}', $content))
        {
            $content = str_replace('{in_total}', "Submission Fee" , $content);
        }

        return $content;

    }

    public function merge_array($element) {
        foreach ($element as $key1 => $value1){
            if(is_array($value1)) $this->merge_array($value1);
            else $this->final_array[] = $value1;
        }
    }

    public function find($needle, $haystack)
    {
        $pos = strpos($haystack, $needle);
        if($pos === false)
        {
            return false;
        }
        else
        {
            return true;
        }
    }


}
?>
