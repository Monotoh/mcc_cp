<?php
class translation
{
	public function IsLeftAligned()
	{
		try
		{
			$locale = sfContext::getInstance()->getUser()->getCulture();

			$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
			mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

			$sql = "SELECT * FROM ext_locales WHERE locale_identifier LIKE '".$locale."'";
			$results = mysql_query($sql, $dbconn) or die($sql);
			$language = mysql_fetch_assoc($results);

			if($language)
			{
				if($language['text_align'])
				{
					return false;
				}
				else
				{
					return true;
				}
			}
			else
			{
				return true;
			}
		}catch(Exception $ex)
		{
			error_log($ex);
		}
	}
        //OTB patch - Get field form id from ext_translations table
        public function getFieldFormId($tablename,$fieldname,$optionid)
	{
		try
		{
			$fieldid = ($fieldid) ? $fieldid : 0;

			$locale = sfContext::getInstance()->getUser()->getCulture();

			$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
			mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

			$sql = "SELECT field_id as formid FROM ext_translations WHERE field_name = '".$fieldname."' AND table_class = '".$tablename."' AND locale = '".$locale."'"." AND option_id = ".$optionid;
                        $rows = mysql_query($sql, $dbconn) or die($sql);
			if($row = mysql_fetch_assoc($rows))
			{
				return $row['formid'];
			}
			else
			{
				return false;
			}
		}catch(Exception $ex)
		{
			error_log($ex);
		}
	}
        //OTB patch - Get field translation
        public function getFieldTranslation($tablename,$fieldname,$formid,$optionid)
	{
		try
		{
			$fieldid = ($fieldid) ? $fieldid : 0;

			$locale = sfContext::getInstance()->getUser()->getCulture();

			$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
			mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

			$sql = "SELECT * FROM ext_translations WHERE field_id = ".$formid." AND field_name = '".$fieldname."' AND table_class = '".$tablename."' AND locale = '".$locale."'"." AND option_id = ".$optionid;
                        $rows = mysql_query($sql, $dbconn) or die($sql);
			if($row = mysql_fetch_assoc($rows))
			{
				return $row['trl_content'];
			}
			else
			{
				return false;
			}
		}catch(Exception $ex)
		{
			error_log($ex);
		}
	}

	public function getTranslation($tablename,$fieldname,$fieldid)
	{
		try
		{
			$fieldid = ($fieldid) ? $fieldid : 0;

			$locale = sfContext::getInstance()->getUser()->getCulture();

			$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
			mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

			$sql = "SELECT * FROM ext_translations WHERE field_id = ".$fieldid." AND field_name = '".$fieldname."' AND table_class = '".$tablename."' AND locale = '".$locale."'";
			$rows = mysql_query($sql, $dbconn) or die($sql);
			if($row = mysql_fetch_assoc($rows))
			{
				return $row['trl_content'];
			}
			else
			{
				return false;
			}
		}catch(Exception $ex)
		{
			error_log($ex);
		}
	}

	public function getOptionTranslation($tablename,$fieldname,$fieldid,$optionid)
	{
		try
		{
			$fieldid = ($fieldid) ? $fieldid : 0;

			$locale = sfContext::getInstance()->getUser()->getCulture();

			$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
			mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

			$sql = "SELECT * FROM ext_translations WHERE option_id = ".$optionid." AND field_id = ".$fieldid." AND field_name = '".$fieldname."' AND table_class = '".$tablename."' AND locale = '".$locale."'";
			$rows = mysql_query($sql, $dbconn) or die($sql);
			if($row = mysql_fetch_assoc($rows))
			{
				return $row['trl_content'];
			}
			else
			{
				return false;
			}
		}catch(Exception $ex)
		{
			error_log($ex);
		}
	}

	public function setTranslation($tablename,$fieldname,$fieldid,$value)
	{
		try
		{
			$locale = sfContext::getInstance()->getUser()->getCulture();

			$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
			mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

			$sql = "SELECT * FROM ext_translations WHERE field_id = ".$fieldid." AND field_name = '".$fieldname."' AND table_class = '".$tablename."' AND locale = '".$locale."'";
		    $translation_sth = mysql_query($sql,$dbconn) or die($sql);
		    $translation_row = mysql_fetch_assoc($translation_sth);
		    if($translation_row)
		    {
		      //update existing translation
		      $sql = "UPDATE ext_translations SET trl_content = '".mysql_real_escape_string($value)."' WHERE id = ".$translation_row['id'];
		      mysql_query($sql,$dbconn) or die($sql);
		    }
		    else
		    {
		      //insert new translation
		      $sql = "INSERT INTO ext_translations (`locale`, `table_class`, `field_name`, `field_id`, `trl_content`) VALUES('".$locale."','".$tablename."','".$fieldname."',".$fieldid.",'".mysql_real_escape_string($value)."')";
		      mysql_query($sql,$dbconn) or die($sql);
		    }
			}catch(Exception $ex)
			{
				error_log($ex);
			}
	}

	public function setOptionTranslation($tablename,$fieldname,$fieldid,$optionid,$value)
	{
		try
		{
			$locale = sfContext::getInstance()->getUser()->getCulture();

			$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
			mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

			$sql = "SELECT * FROM ext_translations WHERE field_id = ".$fieldid." AND option_id = ".$optionid." AND field_name = '".$fieldname."' AND table_class = '".$tablename."' AND locale = '".$locale."'";
		    $translation_sth = mysql_query($sql,$dbconn) or die($sql);
		    $translation_row = mysql_fetch_assoc($translation_sth);
		    if($translation_row)
		    {
		      //update existing translation
		      $sql = "UPDATE ext_translations SET trl_content = '".mysql_real_escape_string($value)."' WHERE id = ".$translation_row['id'];
		      mysql_query($sql,$dbconn) or die($sql);
		    }
		    else
		    {
		      //insert new translation
		      $sql = "INSERT INTO ext_translations (`locale`, `table_class`, `field_name`, `field_id`, `option_id`, `trl_content`) VALUES('".$locale."','".$tablename."','".$fieldname."',".$fieldid.", ".$optionid.",'".mysql_real_escape_string($value)."')";
		      mysql_query($sql,$dbconn) or die($sql);
		    }
			}catch(Exception $ex)
			{
				error_log($ex);
			}
	}
    
    public function setTranslationManual($tablename,$fieldname,$fieldid,$value, $locale)
	{
		try
		{
			$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
			mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

			$sql = "SELECT * FROM ext_translations WHERE field_id = ".$fieldid." AND field_name = '".$fieldname."' AND table_class = '".$tablename."' AND locale = '".$locale."'";
		    $translation_sth = mysql_query($sql,$dbconn) or die($sql);
		    $translation_row = mysql_fetch_assoc($translation_sth);
		    if($translation_row)
		    {
		      //update existing translation
		      $sql = "UPDATE ext_translations SET trl_content = '".mysql_real_escape_string($value)."' WHERE id = ".$translation_row['id'];
		      mysql_query($sql,$dbconn) or die($sql);
		    }
		    else
		    {
		      //insert new translation
		      $sql = "INSERT INTO ext_translations (`locale`, `table_class`, `field_name`, `field_id`, `trl_content`) VALUES('".$locale."','".$tablename."','".$fieldname."',".$fieldid.",'".mysql_real_escape_string($value)."')";
		      mysql_query($sql,$dbconn) or die($sql);
		    }
			}catch(Exception $ex)
			{
				error_log($ex);
			}
	}

	public function setOptionTranslationManual($tablename,$fieldname,$fieldid,$optionid,$value, $locale)
	{
		try
		{
			$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
			mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

			$sql = "SELECT * FROM ext_translations WHERE field_id = ".$fieldid." AND option_id = ".$optionid." AND field_name = '".$fieldname."' AND table_class = '".$tablename."' AND locale = '".$locale."'";
		    $translation_sth = mysql_query($sql,$dbconn) or die($sql);
		    $translation_row = mysql_fetch_assoc($translation_sth);
		    if($translation_row)
		    {
		      //update existing translation
		      $sql = "UPDATE ext_translations SET trl_content = '".mysql_real_escape_string($value)."' WHERE id = ".$translation_row['id'];
		      mysql_query($sql,$dbconn) or die($sql);
		    }
		    else
		    {
		      //insert new translation
		      $sql = "INSERT INTO ext_translations (`locale`, `table_class`, `field_name`, `field_id`, `option_id`, `trl_content`) VALUES('".$locale."','".$tablename."','".$fieldname."',".$fieldid.", ".$optionid.",'".mysql_real_escape_string($value)."')";
		      mysql_query($sql,$dbconn) or die($sql);
		    }
			}catch(Exception $ex)
			{
				error_log($ex);
			}
	}
}
?>
