<?php
require_once dirname(__FILE__).'/../cp_workflow/config/db_config.inc.php';	
/*
PermitFlow Form Configuration File
*/

/** MySQL settings **/
define('MF_DB_NAME', $DATABASE_DB); //The name of your database. Note that this database must exist before running installer.php
define('MF_DB_USER', $DATABASE_UID); //Your database username
define('MF_DB_PASSWORD', $DATABASE_PWD); //Your database users password
define('MF_DB_HOST', $DATABASE_HOST); //The hostname for your database




/** YOU CAN LEAVE THE SETTINGS BELOW THIS LINE UNCHANGED **/

/** Optional Settings **/
/** All settings below this line are optionals, you can leave them as they are now **/
define('MF_TABLE_PREFIX', 'ap_'); //The prefix for all machform tables

//by default, deleting field from the form won't actually remove all the data within the table, so that we can manually recover it
//by setting this value to 'true' the data will be removed completely, unrecoverable
define('MF_CONF_TRUE_DELETE',false);

/** reCAPTCHA settings **/
/** Below is a global key. If you prefer to use your own reCAPTCHA key, get an API key from https://www.google.com/recaptcha/admin/create **/
define('RECAPTCHA_PUBLIC_KEY','6LdDtMcSAAAAAL0O2fhNlYObanlKlbQzSfYsdHRY');
define('RECAPTCHA_PRIVATE_KEY','6LdDtMcSAAAAACXVxR-niVXMe-5KnVQQkvaZP_dw');
define('RECAPTCHA_THEME','red'); //available themes: red, white, blackglass, clean
define('RECAPTCHA_LANGUAGE','en'); //available languages: en, nl, fr, de, pt, ru, es, tr


$_SESSION['mf_logged_in'] = true;
$_SESSION['mf_user_id']   = 1;
$_SESSION['mf_user_privileges']['priv_administer'] = 1;
$_SESSION['mf_user_privileges']['priv_new_forms']  = 1;
$_SESSION['mf_user_privileges']['priv_new_themes'] = 1;

?>
