<?php
	//check for magic quotes, if on, remove all slashes from input
	function mf_sanitize($input){
		if(get_magic_quotes_gpc() && !empty($input)){
			 $input = is_array($input) ?
	                array_map('mf_stripslashes_deep', $input) :
	                stripslashes($input);
		}
		
		return $input;
	}
	
	function mf_stripslashes_deep($value){
		
	    $value = is_array($value) ?
	                array_map('mf_stripslashes_deep', $value) :
	                stripslashes($value);
	
	    return $value;
	}

?>