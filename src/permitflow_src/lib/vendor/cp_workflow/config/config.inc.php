<?php
	//checking if file exists
	$inner_prefix_folder = dirname(__FILE__)."/../..";
	$path = $inner_prefix_folder.'/cp_workflow/';
	$file = 'config/db_config.inc.php';
	if (file_exists($file))
	{
		$path = '';
	}
	else
	{
		$file = '../config/db_config.inc.php';
		if (file_exists($file))
		{
			$path = '../';
		}
		else
		{
			$file = '../../config/db_config.inc.php';
			if (file_exists($file))
			{
				$path = '../../';
			}
		}
	}
	
	require_once dirname(__FILE__).'/db_config.inc.php';
	require_once dirname(__FILE__).'/../lib/RPL/Encryption/Blowfish.class.php';
	require_once dirname(__FILE__).'/../lib/RPL/Encryption/Url.class.php';
	
	/**
	 * Automatic Class including
	 *
	 * @param String $class
	 */
	function __autoload($class)
	{
		global $path;
		
		$file = str_replace('_', '/', $class).'.php';
		require_once $path.'classes/'.$file;
	}

	$nConnection = mysql_connect($DATABASE_HOST, $DATABASE_UID, $DATABASE_PWD);
	if ($nConnection)
	{
		if (mysql_select_db($DATABASE_DB, $nConnection))
		{
            $query = "SELECT * from cf_config";
			$nResult = mysql_query($query, $nConnection);
			
			
			if ($nResult)
			{
				if (mysql_num_rows($nResult) > 0) 
				{
					$arrConfig = mysql_fetch_array($nResult);				
				}
			}
			
			//--- server settings
			$CUTEFLOW_SERVER 		= $arrConfig["strcf_server"];
			$SMTP_USE_AUTH 			= $arrConfig["strsmtp_use_auth"];		//--- "y" for using smtp authentification otherwise ""
			$SMTP_SERVER 			= $arrConfig["strsmtp_server"];
			$SMTP_PORT 				= $arrConfig["strsmtp_port"];
			$SMTP_USERID 			= $arrConfig["strsmtp_userid"];
			$SMTP_PWD 				= $arrConfig["strsmtp_pwd"];
			$SHOWROWS 				= $arrConfig["nshowrows"];
			$AUTO_RELOAD_SEC 		= $arrConfig["nautoreload"];
			$URL_ENCODING_PASSWORD 	= $arrConfig["strurlpassword"];
			$URL_ENCODING_TS 		= $arrConfig["tslastupdate"];
			$ARR_COL_SPLIT 			= split('---', $arrConfig['strcirculation_cols']);
			
			
			$objURL = new RPL_Encryption_Url();
			$objURL->setPassword($URL_ENCODING_PASSWORD);
			$objURL->checkBoxes($path.'lib/RPL/Encryption/boxes.js', $URL_ENCODING_TS);
						
			if ($_REQUEST['key'] != '')
			{
				$objURL->setParams($_REQUEST['key']);
			}
			
			
			//--- Substitute person
			$SUBTITUTE_PERSON_UNIT 	= $arrConfig['strsubstituteperson_unit'];
			$SUBTITUTE_PERSON_VALUE	= $arrConfig['nsubstituteperson_hours'];
			switch($arrConfig['strsubstituteperson_unit'])
			{
				case 'DAYS':
					$SUBSTITUTE_PERSON_MINUTES = $arrConfig['nsubstituteperson_hours'] * 24 * 60;
					break;
				case 'HOURS':
					$SUBSTITUTE_PERSON_MINUTES = $arrConfig['nsubstituteperson_hours'] * 60;
					break;
				case 'MINUTES':
					$SUBSTITUTE_PERSON_MINUTES = $arrConfig['nsubstituteperson_hours'];
					break;
			}
			
			
			$arrColsSplit = split('---', $arrConfig['strcirculation_cols']);
			
			$nColsRunningNumber = 0;
			$nAllColsRunningNumber = 0;
			$nColsMax = sizeof($arrColsSplit);
			for ($nColsIndex = 0; $nColsIndex < $nColsMax; $nColsIndex++)
			{
				$strCurCol 	= $arrColsSplit[$nColsIndex];
				$nColsIndex = $nColsIndex + 1;
				$bActive 	= $arrColsSplit[$nColsIndex];
				
				switch($strCurCol)
				{
					case 'NAME':
						$arrCONF_AllCols[$nAllColsRunningNumber]['strtitle']		= 'COL_CIRCULATION_NAME';
						$arrCONF_AllCols[$nAllColsRunningNumber]['strscreentitle']	= $CIRCORDER_NAME;
						if ($bActive)
						{
							$arrCirculation_Cols[] 		= 'COL_CIRCULATION_NAME';
						}
						break;
					case 'STATION':
						$arrCONF_AllCols[$nAllColsRunningNumber]['strtitle']	= 'COL_CIRCULATION_STATION';
						$arrCONF_AllCols[$nAllColsRunningNumber]['strscreentitle']	= $CIRCORDER_STATION;
						if ($bActive)
						{
							$arrCirculation_Cols[] 		= 'COL_CIRCULATION_STATION';
						}
						break;
					case 'DAYS':
						$arrCONF_AllCols[$nAllColsRunningNumber]['strtitle']	= 'COL_CIRCULATION_PROCESS_DAYS';
						$arrCONF_AllCols[$nAllColsRunningNumber]['strscreentitle']	= $CIRCORDER_DAYS;
						if ($bActive)
						{
							$arrCirculation_Cols[] 		= 'COL_CIRCULATION_PROCESS_DAYS';
						}
						break;
					case 'START':
						$arrCONF_AllCols[$nAllColsRunningNumber]['strtitle']	= 'COL_CIRCULATION_PROCESS_START';
						$arrCONF_AllCols[$nAllColsRunningNumber]['strscreentitle']	= $CIRCORDER_START;
						if ($bActive)
						{
							$arrCirculation_Cols[] 		= 'COL_CIRCULATION_PROCESS_START';
						}
						break;
					case 'SENDER':
						$arrCONF_AllCols[$nAllColsRunningNumber]['strtitle']	= 'COL_CIRCULATION_SENDER';
						$arrCONF_AllCols[$nAllColsRunningNumber]['strscreentitle']	= $CIRCORDER_SENDER;
						if ($bActive)
						{
							$arrCirculation_Cols[] 		= 'COL_CIRCULATION_SENDER';
						}
						break;
					case 'MAILLIST':
						$arrCONF_AllCols[$nAllColsRunningNumber]['strtitle']	= 'COL_CIRCULATION_MAILLIST';
						$arrCONF_AllCols[$nAllColsRunningNumber]['strscreentitle']	= $SHOW_CIRCULATION_MAILLIST;
						if ($bActive)
						{
							$arrCirculation_Cols[] 		= 'COL_CIRCULATION_MAILLIST';
						}
						break;
					case 'TEMPLATE':
						$arrCONF_AllCols[$nAllColsRunningNumber]['strtitle']	= 'COL_CIRCULATION_TEMPLATE';
						$arrCONF_AllCols[$nAllColsRunningNumber]['strscreentitle']	= $SHOW_CIRCULATION_TEMPLATE;
						if ($bActive)
						{
							$arrCirculation_Cols[] 		= 'COL_CIRCULATION_TEMPLATE';
						}
						break;
					case 'WHOLETIME':
						$arrCONF_AllCols[$nAllColsRunningNumber]['strtitle']	= 'COL_CIRCULATION_WHOLETIME';
						$arrCONF_AllCols[$nAllColsRunningNumber]['strscreentitle']	= $SHOW_CIRCULATION_WHOLETIME;
						if ($bActive)
						{
							$arrCirculation_Cols[] 		= 'COL_CIRCULATION_WHOLETIME';
						}
						break;
				}
				if ($bActive)
				{				
					$nColsRunningNumber++;
				}
				$arrCONF_AllCols[$nAllColsRunningNumber]['bactive']	= $bActive;
				$nAllColsRunningNumber++;
			}
			
			$TStoday =  mktime(date("H"),date("i"),date("s"),date("m"), date("d"), date("Y"));
			$TSsendSP = mktime(date("H"),date("i")-$SUBSTITUTE_PERSON_MINUTES,date("s"),date("m"), date("d"), date("Y"));
			
			//--- mail informations
			$SYSTEM_REPLY_ADDRESS = $arrConfig["strsysteplyaddr"];
			$MAIL_ADDTIONALTEXT_DEFAULT = $arrConfig["strmailaddtextdef"];
			
			//--- gui settings
			$DEFAULT_LANGUAGE				 = $arrConfig["strdeflang"];
			$OPEN_DETAILS_IN_SEPERATE_WINDOW = $arrConfig["bdetailseperatewindow"];
			$DEFAULT_SORT_COL 				= $arrConfig["strdefsortcol"];
			$SHOW_POSITION_IN_MAIL 			= $arrConfig["bshowposmail"];
			$FILTER_AUTO_REGEX_WORDSTART 	= $arrConfig["bfilter_ar_wordstart"];
			$EMAIL_FORMAT 					= $arrConfig["stremail_format"];
			$EMAIL_VALUES 					= $arrConfig["stremail_values"];
			$CIRCULATION_COLUMNS 			= $arrCirculation_Cols;
			$SORTDIRECTION					= $arrConfig["strsortdirection"];
			$ALLOW_UNENCRYPTED_REQUEST		= $arrConfig['ballowunencryptedrequest'];
			$USERDEFINED_TITLE1				= $arrConfig['userdefined1_title'];
			$USERDEFINED_TITLE2				= $arrConfig['userdefined2_title'];
			$DATE_FORMAT					= $arrConfig['strdateformat'];
						
			$SEND_WORKFLOW_MAIL				= $arrConfig['bsendworkflowmail'];
			$SEND_REMINDER_MAIL				= $arrConfig['bsendremindermail'];
			
			//--- days of delay
		   	$DELAY_NORMAL = $arrConfig["ndelay_norm"];
		   	$DELAY_INDERMIDIATE = $arrConfig["ndelay_interm"];
		   	$DELAY_LATE = $arrConfig["ndelay_late"];
		   	
		   	$SLOT_VISIBILITY = $arrConfig['strslotvisibility'];
		   	
		   	$MAIL_SEND_TYPE = $arrConfig['strmailsendtype'];
		   	$MTA_PATH = $arrConfig['strmtapath'];
		   	
		   	$SMPT_ENCRYPTION = $arrConfig['strsmtpencryption'];
		   	
		   	//$CUTEFLOW_VERSION = $arrConfig["strVersion"];

		   	// User Timeout = 10 minutes
			$USER_TIMEOUT = 60*10;
			
			if ($_SESSION['SESSION_CUTEFLOW_USERID'] != '')
			{	// user is logged in - let's update the user's last action
				$strQuery 	= "UPDATE cf_user SET tslastaction = '".time()."' WHERE nid = '".$_SESSION['SESSION_CUTEFLOW_USERID']."' LIMIT 1;";
				$nResult 	= mysql_query($strQuery, $nConnection) or die(mysql_error());
			}
		}
	}

?>
