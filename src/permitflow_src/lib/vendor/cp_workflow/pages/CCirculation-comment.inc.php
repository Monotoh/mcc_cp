<?php

class CCirculation
{
	function CCirculation()
	{
		
	}
	
	function getRadioGroup($nInputfieldID, $strValue, $bIsEnabled, $keyId, $nRunningCounter)
	{
		$strBuffer = '';
		
		$arrSplit = split('---',$strValue);
		
		$arrInputFieldValues = $this->getInputFieldValue($nInputfieldID);
		$arrInputFieldTypes = $this->getInputFieldTypes($nInputfieldID);
		$arrInputFieldRelations = $this->getInputFieldRelation($nInputfieldID);
		
		$nMax = sizeof($arrInputFieldValues);
		
		for ($nMyIndex = 0; $nMyIndex < $nMax; $nMyIndex++)
		{
			$nCurState 	= $arrSplit[$nMyIndex];		// state of Radiobutton either '0' or '1'
			
			$strBuffer = $strBuffer.'<input type="radio" name="'.$keyId.'_nRadiogroup_'.$nRunningCounter.'" value="'.$nMyIndex.'"';
			if(!$bIsEnabled) 
			{ 
				$strBuffer = $strBuffer.' ';
			}
			if ($nCurState) 
			{
				$strBuffer = $strBuffer.' checked';
			}
			if($arrInputFieldTypes[$nMyIndex] == "2")
			{
				$value = "";
				//Check If Field has value, if none then populate with default
				
      			//fetch slot details from keyid(fieldid_slotid_circulationid), user split
				$circ_details = split('_',$keyId);
				
				$q = Doctrine_Query::create()
				   ->from('Comments a')
				   ->where('a.field_id = ? AND a.slot_id = ? AND a.circulation_id = ?', array($circ_details[0],$circ_details[1],$circ_details[2]));
				$comment = $q->fetchOne();
				if($comment)
				{
					$value = $comment->getComment();
				}
				
				if($value == "")
				{
							$q = Doctrine_Query::create()
							   ->from('cfInputfield a')
							   ->where('a.nid = ?', $circ_details[0]);
							$field = $q->fetchOne();
							if($field){
								$arrComments = split("---",$field->getCommentrelation());
								$value = $arrComments[$nMyIndex+2];
								$data = $arrComments[$nMyIndex+2];
							}
				}
				
				$divstate = "none";
				
				if($nCurState)
				{
					$divstate = "block";
					//check for comments matching this field
				}
				else
				{
					$value = "";	
				}
				
				if($data == "0")
				{
					$value = "";
					$data = "";
				}
				
				$strBuffer = $strBuffer.' onclick="var commentdiv = document.getElementById(\'comment_'.$keyId.'_div\'); commentdiv.style.display = \'block\';  var txt = document.getElementById(\'comment_'.$keyId.'_nRadiogroup\'); txt.value = \''.$data.'\';"> '.$arrInputFieldValues[$nMyIndex].'<br> <div id="comment_'.$keyId.'_div" name="comment_'.$keyId.'_div" style="display: '.$divstate.'; padding-left: 20px; padding-top: 5px;">
				<textarea class="InputText" style="width: 90%; height: 60px;" id="comment_'.$keyId.'_nRadiogroup" name="comment_'.$keyId.'_nRadiogroup" onfocus="this.value = remove_underscore(this.value);">'.$value.'</textarea>
				
				</div> ';
			}
			else if($arrInputFieldTypes[$nMyIndex] == "3")
			{
				$value = "";
				//Check If Field has value, if none then populate with default
				
      				//fetch slot details from keyid(fieldid_slotid_circulationid), user split
					$circ_details = split('_',$keyId);
				
				$q = Doctrine_Query::create()
				   ->from('Conditions a')
				   ->where('a.field_id = ? AND a.slot_id = ? AND a.circulation_id = ?', array($circ_details[0],$circ_details[1],$circ_details[2]));
				$condition = $q->fetchOne();
				if($condition)
				{
					$value = $condition->getConditionText();
				}
				
				if($value == "")
				{
							$q = Doctrine_Query::create()
							   ->from('cfInputfield a')
							   ->where('a.nid = ?', $circ_details[0]);
							$field = $q->fetchOne();
							if($field)
							{
							$arrComments = split("---",$field->getCommentrelation());
							$value = $arrComments[$nMyIndex+2];
							$data = $arrComments[$nMyIndex+2];
							}
				}
				
				$divstate = "none";
				
				if($nCurState)
				{
					$divstate = "block";
				}
				else
				{
					$value = "";	
				}
				
				
				if($data == "0")
				{
					$value = "";
					$data = "";
				}
				
			
				$strBuffer = $strBuffer.' onclick="var conditiondiv = document.getElementById(\'condition_'.$keyId.'_div\'); conditiondiv.style.display = \'block\'; var txt = document.getElementById(\'condition_'.$keyId.'_nRadiogroup\'); txt.value = \''.$data.'\';"> '.$arrInputFieldValues[$nMyIndex].'<br> <div id="condition_'.$keyId.'_div" name="condition_'.$keyId.'_div" style="display: '.$divstate.'; padding-left: 20px; padding-top: 5px;">
				<textarea class="InputText" style="width: 90%; height: 60px;" id="condition_'.$keyId.'_nRadiogroup" name="condition_'.$keyId.'_nRadiogroup" onfocus="this.value = remove_underscore(this.value);">'.$value.'</textarea>
				
				</div> ';
			}
			else
			{
				$strBuffer = $strBuffer.' onclick="var commentdiv = document.getElementById(\'comment_'.$keyId.'_div\'); commentdiv.style.display = \'none\'; var commentfield = document.getElementById(\'comment_'.$keyId.'_nRadiogroup\'); commentfield.value = \'\';var conditiondiv = document.getElementById(\'condition_'.$keyId.'_div\'); conditiondiv.style.display = \'none\'; var conditionfield = document.getElementById(\'condition_'.$keyId.'_nRadiogroup\'); conditionfield.value = \'\';">';
				$strBuffer = $strBuffer.''.$arrInputFieldValues[$nMyIndex].'<br>';
			}
			
			if ($bIsEnabled) 
			{ // slot is allowed to edit
				$strBuffer = $strBuffer.'<input type="hidden" name="RBName_'.$keyId.'_nRadiogroup_'.$nRunningCounter.'_'.$nMyIndex.'" value="'.$arrInputFieldValues[$nMyIndex].'">'; 
			}
		}
		return $strBuffer;
	}
	
	
	function getRadioGroup2($nInputfieldID, $circ_id, $slot_id, $strValue, $bIsEnabled, $keyId, $nRunningCounter)
	{
		$strBuffer = '';
		
		$arrSplit = split('---',$strValue);
		
		$arrInputFieldValues = $this->getInputFieldValue($nInputfieldID);
		$arrInputFieldTypes = $this->getInputFieldTypes($nInputfieldID);
		$arrInputFieldRelations = $this->getInputFieldRelation($nInputfieldID);
		
		$nMax = sizeof($arrInputFieldValues);
		
		for ($nMyIndex = 0; $nMyIndex < $nMax; $nMyIndex++)
		{
			$nCurState 	= $arrSplit[$nMyIndex];		// state of Radiobutton either '0' or '1'
			
			$strBuffer = $strBuffer.'<input type="radio" name="'.$keyId.'_nRadiogroup_'.$nRunningCounter.'" value="'.$nMyIndex.'"';
			if(!$bIsEnabled) 
			{ 
				$strBuffer = $strBuffer.' ';
			}
			if ($nCurState) 
			{
				$strBuffer = $strBuffer.' checked';
			}
			if($arrInputFieldTypes[$nMyIndex] == "2")
			{
				$value = "";
				//Check If Field has value, if none then populate with default
				
      			//fetch slot details from keyid(fieldid_slotid_circulationid), user split
				$circ_details = split('_',$keyId);
				
				$q = Doctrine_Query::create()
				   ->from('Comments a')
				   ->where('a.field_id = ? AND a.slot_id = ? AND a.circulation_id = ?', array($nInputfieldID,$slot_id, $circ_id));
				$comment = $q->fetchOne();
				if($comment)
				{
					$value = $comment->getComment();
				}
				
				if($value == "")
				{
							$q = Doctrine_Query::create()
							   ->from('cfInputfield a')
							   ->where('a.nid = ?', $circ_details[0]);
							$field = $q->fetchOne();
							if($field){
								$arrComments = split("---",$field->getCommentrelation());
								$value = $arrComments[$nMyIndex+2];
								$data = $arrComments[$nMyIndex+2];
							}
				}
				
				$divstate = "none";
				
				if($nCurState)
				{
					$divstate = "block";
					//check for comments matching this field
				}
				else
				{
					$value = "";	
				}
				
				if($data == "0")
				{
					$value = "";
					$data = "";
				}
				
				$strBuffer = $strBuffer.' onclick="var commentdiv = document.getElementById(\'comment_'.$keyId.'_div\'); commentdiv.style.display = \'block\';  var txt = document.getElementById(\'comment_'.$keyId.'_nRadiogroup\'); txt.value = \''.$data.'\';"> '.$arrInputFieldValues[$nMyIndex].'<br> <div id="comment_'.$keyId.'_div" name="comment_'.$keyId.'_div" style="display: '.$divstate.'; padding-left: 20px; padding-top: 5px;">
				<textarea  class="InputText" style="width: 90%; height: 60px;" id="comment_'.$keyId.'_nRadiogroup" name="comment_'.$keyId.'_nRadiogroup" onfocus="this.value = remove_underscore(this.value);">'.$value.'</textarea>
				
				</div> ';
			}
			else if($arrInputFieldTypes[$nMyIndex] == "3")
			{
				$value = "";
				//Check If Field has value, if none then populate with default
				
      				//fetch slot details from keyid(fieldid_slotid_circulationid), user split
					$circ_details = split('_',$keyId);
				
				$q = Doctrine_Query::create()
				   ->from('Conditions a')
				   ->where('a.field_id = ? AND a.slot_id = ? AND a.circulation_id = ?', array($circ_details[0],$circ_details[1],$circ_details[2]));
				$condition = $q->fetchOne();
				if($condition)
				{
					$value = $condition->getConditionText();
				}
				
				if($value == "")
				{
							$q = Doctrine_Query::create()
							   ->from('cfInputfield a')
							   ->where('a.nid = ?', $circ_details[0]);
							$field = $q->fetchOne();
							if($field)
							{
							$arrComments = split("---",$field->getCommentrelation());
							$value = $arrComments[$nMyIndex+2];
							$data = $arrComments[$nMyIndex+2];
							}
				}
				
				$divstate = "none";
				
				if($nCurState)
				{
					$divstate = "block";
				}
				else
				{
					$value = "";	
				}
				
				
				if($data == "0")
				{
					$value = "";
					$data = "";
				}
				
			
				$strBuffer = $strBuffer.' onclick="var conditiondiv = document.getElementById(\'condition_'.$keyId.'_div\'); conditiondiv.style.display = \'block\'; var txt = document.getElementById(\'condition_'.$keyId.'_nRadiogroup\'); txt.value = \''.$data.'\';"> '.$arrInputFieldValues[$nMyIndex].'<br> <div id="condition_'.$keyId.'_div" name="condition_'.$keyId.'_div" style="display: '.$divstate.'; padding-left: 20px; padding-top: 5px;">
				<textarea class="InputText" style="width: 90%; height: 60px;" id="condition_'.$keyId.'_nRadiogroup" name="condition_'.$keyId.'_nRadiogroup" onfocus="this.value = remove_underscore(this.value);">'.$value.'</textarea>
				
				</div> ';
			}
			else
			{
				$strBuffer = $strBuffer.' onclick="var commentdiv = document.getElementById(\'comment_'.$keyId.'_div\'); commentdiv.style.display = \'none\'; var commentfield = document.getElementById(\'comment_'.$keyId.'_nRadiogroup\'); commentfield.value = \'\';var conditiondiv = document.getElementById(\'condition_'.$keyId.'_div\'); conditiondiv.style.display = \'none\'; var conditionfield = document.getElementById(\'condition_'.$keyId.'_nRadiogroup\'); conditionfield.value = \'\';">';
				$strBuffer = $strBuffer.''.$arrInputFieldValues[$nMyIndex].'<br>';
			}
			
			if ($bIsEnabled) 
			{ // slot is allowed to edit
				$strBuffer = $strBuffer.'<input type="hidden" name="RBName_'.$keyId.'_nRadiogroup_'.$nRunningCounter.'_'.$nMyIndex.'" value="'.$arrInputFieldValues[$nMyIndex].'">'; 
			}
		}
		return $strBuffer;
	}
	
	function getCheckBoxGroup($nInputfieldID, $strValue, $bIsEnabled, $keyId, $nRunningCounter)
	{
		$strBuffer = '';
		
		$arrSplit = split('---',$strValue);
		
		$arrInputFieldValues = $this->getInputFieldValue($nInputfieldID);
		$arrInputFieldTypes = $this->getInputFieldTypes($nInputfieldID);
		$arrInputFieldRelations = $this->getInputFieldRelation($nInputfieldID);
		
		$nMax = sizeof($arrInputFieldValues);
		for ($nMyIndex = 0; $nMyIndex < $nMax; $nMyIndex++)
		{
			$nCurState 	= $arrSplit[$nMyIndex];		// state of Radiobutton either '0' or '1'

			$strBuffer = $strBuffer.'<input type="checkbox" name="'.$keyId.'_nCheckboxGroup_'.$nRunningCounter.'_'.$nMyIndex.'" value="1"';
			if(!$bIsEnabled) 
			{ // slot is allowed to edit
				$strBuffer = $strBuffer.' '; 
			}
			if ($nCurState) 
			{ 
				$strBuffer = $strBuffer.' checked'; 
			}
			
			
			if($arrInputFieldTypes[$nMyIndex] == "2")
			{
				$value = "";
				//Check If Field has value, if none then populate with default
				
      				//fetch slot details from keyid(fieldid_slotid_circulationid), user split
					$circ_details = split('_',$keyId);
				
				$q = Doctrine_Query::create()
				   ->from('Comments a')
				   ->where('a.field_id = ? AND a.slot_id = ? AND a.circulation_id = ?', array($circ_details[0],$circ_details[1],$circ_details[2]));
				$comment = $q->fetchOne();
				if($comment)
				{
					$value = $comment->getComment();
				}
				
				if($value == "")
				{
					$q = Doctrine_Query::create()
					   ->from('cfInputfield a')
					   ->where('a.nid = ?', $nInputfieldID);
					$inputfield = $q->fetchOne();
					$value = $inputfield->getCommentrelation();
				}
				
				$divstate = "none";
				
				if($nCurState)
				{
					$divstate = "block";
				}
				else
				{
					$value = "";	
				}
				
				$strBuffer = $strBuffer.' onclick="var commentdiv = document.getElementById(\'comment_'.$keyId.'_div\'); commentdiv.style.display = \'block\';"> '.$arrInputFieldValues[$nMyIndex].'<br> <div id="comment_'.$keyId.'_div" name="comment_'.$keyId.'_div" style="display: '.$divstate.'; padding-left: 20px; padding-top: 5px;">
				<textarea class="InputText" style="width: 90%; height: 60px;" id="comment_'.$keyId.'_nCheckboxGroup" name="comment_'.$keyId.'_nCheckboxGroup" onfocus="this.value = remove_underscore(this.value);">'.$value.'</textarea>
				
				</div> ';
			}
			else if($arrInputFieldTypes[$nMyIndex] == "3")
			{
				$value = "";
				//Check If Field has value, if none then populate with default
				
      				//fetch slot details from keyid(fieldid_slotid_circulationid), user split
					$circ_details = split('_',$keyId);
				
				$q = Doctrine_Query::create()
				   ->from('Conditions a')
				   ->where('a.field_id = ? AND a.slot_id = ? AND a.circulation_id = ?', array($circ_details[0],$circ_details[1],$circ_details[2]));
				$condition = $q->fetchOne();
				if($condition)
				{
					$value = $condition->getConditionText();
				}
				
				if($value == "")
				{
					$q = Doctrine_Query::create()
					   ->from('cfInputfield a')
					   ->where('a.nid = ?', $nInputfieldID);
					$inputfield = $q->fetchOne();
					$value = $inputfield->getCommentrelation();
				}
				
				$divstate = "none";
				
				if($nCurState)
				{
					$divstate = "block";
				}
				else
				{
					$value = "";	
				}
				
				$strBuffer = $strBuffer.' onclick="var conditiondiv = document.getElementById(\'condition_'.$keyId.'_div\'); conditiondiv.style.display = \'block\';"> '.$arrInputFieldValues[$nMyIndex].'<br> <div id="condition_'.$keyId.'_div" name="condition_'.$keyId.'_div" style="display: '.$divstate.'; padding-left: 20px; padding-top: 5px;">
				<textarea class="InputText" style="width: 90%; height: 60px;" id="condition_'.$keyId.'_nCheckboxGroup" name="condition_'.$keyId.'_nCheckboxGroup" onfocus="this.value = remove_underscore(this.value);">'.$value.'</textarea>
				
				</div> ';
			}
			else
			{
				$strBuffer = $strBuffer.' onclick="var commentdiv = document.getElementById(\'comment_'.$keyId.'_div\'); commentdiv.style.display = \'none\'; var commentfield = document.getElementById(\'comment_'.$keyId.'_nCheckboxGroup\'); commentfield.value = \'\';var conditiondiv = document.getElementById(\'condition_'.$keyId.'_div\'); conditiondiv.style.display = \'none\'; var conditionfield = document.getElementById(\'condition_'.$keyId.'_nCheckboxGroup\'); conditionfield.value = \'\';">';
				$strBuffer = $strBuffer.''.$arrInputFieldValues[$nMyIndex].'<br>';
			}
			
			$strBuffer = $strBuffer.'>'.$arrInputFieldValues[$nMyIndex].'<br>';
			if ($bIsEnabled) 
			{ // slot is allowed to edit
				$strBuffer = $strBuffer.'<input type="hidden" name="CBName_'.$keyId.'_nCheckboxGroup_'.$nRunningCounter.'_'.$nMyIndex.'" value="'.$arrInputFieldValues[$nMyIndex].'">';
			}
		}
		
		return $strBuffer;
	}
	
	function getCheckBoxGroup2($nInputfieldID, $circ_id, $slot_id, $strValue, $bIsEnabled, $keyId, $nRunningCounter)
	{
		$strBuffer = '';
		
		$arrSplit = split('---',$strValue);
		
		$arrInputFieldValues = $this->getInputFieldValue($nInputfieldID);
		$arrInputFieldTypes = $this->getInputFieldTypes($nInputfieldID);
		$arrInputFieldRelations = $this->getInputFieldRelation($nInputfieldID);
		
		$nMax = sizeof($arrInputFieldValues);
		for ($nMyIndex = 0; $nMyIndex < $nMax; $nMyIndex++)
		{
			$nCurState 	= $arrSplit[$nMyIndex];		// state of Radiobutton either '0' or '1'

			$strBuffer = $strBuffer.'<input type="checkbox" name="'.$keyId.'_nCheckboxGroup_'.$nRunningCounter.'_'.$nMyIndex.'" value="1"';
			if(!$bIsEnabled) 
			{ // slot is allowed to edit
				$strBuffer = $strBuffer.' '; 
			}
			if ($nCurState) 
			{ 
				$strBuffer = $strBuffer.' checked'; 
			}
			
			
			if($arrInputFieldTypes[$nMyIndex] == "2")
			{
				$value = "";
				//Check If Field has value, if none then populate with default
				
      				//fetch slot details from keyid(fieldid_slotid_circulationid), user split
					$circ_details = split('_',$keyId);
				
				$q = Doctrine_Query::create()
				   ->from('Comments a')
				   ->where('a.field_id = ? AND a.slot_id = ? AND a.circulation_id = ?', array($nInputfieldID,$circ_id,$slot_id));
				$comment = $q->fetchOne();
				if($comment)
				{
					$value = $comment->getComment();
				}
				
				if($value == "")
				{
					$q = Doctrine_Query::create()
					   ->from('cfInputfield a')
					   ->where('a.nid = ?', $nInputfieldID);
					$inputfield = $q->fetchOne();
					$value = $inputfield->getCommentrelation();
				}
				
				$divstate = "none";
				
				if($nCurState)
				{
					$divstate = "block";
				}
				else
				{
					$value = "";	
				}
				
				$strBuffer = $strBuffer.' onclick="var commentdiv = document.getElementById(\'comment_'.$keyId.'_div\'); commentdiv.style.display = \'block\';"> '.$arrInputFieldValues[$nMyIndex].'<br> <div id="comment_'.$keyId.'_div" name="comment_'.$keyId.'_div" style="display: '.$divstate.'; padding-left: 20px; padding-top: 5px;">
				<textarea class="InputText" style="width: 90%; height: 60px;" id="comment_'.$keyId.'_nCheckboxGroup" name="comment_'.$keyId.'_nCheckboxGroup" onfocus="this.value = remove_underscore(this.value);">'.$value.'</textarea>
				
				</div> ';
			}
			else if($arrInputFieldTypes[$nMyIndex] == "3")
			{
				$value = "";
				//Check If Field has value, if none then populate with default
				
      				//fetch slot details from keyid(fieldid_slotid_circulationid), user split
					$circ_details = split('_',$keyId);
				
				$q = Doctrine_Query::create()
				   ->from('Conditions a')
				   ->where('a.field_id = ? AND a.slot_id = ? AND a.circulation_id = ?', array($circ_details[0],$circ_details[1],$circ_details[2]));
				$condition = $q->fetchOne();
				if($condition)
				{
					$value = $condition->getConditionText();
				}
				
				if($value == "")
				{
					$q = Doctrine_Query::create()
					   ->from('cfInputfield a')
					   ->where('a.nid = ?', $nInputfieldID);
					$inputfield = $q->fetchOne();
					$value = $inputfield->getCommentrelation();
				}
				
				$divstate = "none";
				
				if($nCurState)
				{
					$divstate = "block";
				}
				else
				{
					$value = "";	
				}
				
				$strBuffer = $strBuffer.' onclick="var conditiondiv = document.getElementById(\'condition_'.$keyId.'_div\'); conditiondiv.style.display = \'block\';"> '.$arrInputFieldValues[$nMyIndex].'<br> <div id="condition_'.$keyId.'_div" name="condition_'.$keyId.'_div" style="display: '.$divstate.'; padding-left: 20px; padding-top: 5px;">
				<textarea class="InputText" style="width: 90%; height: 60px;" id="condition_'.$keyId.'_nCheckboxGroup" name="condition_'.$keyId.'_nCheckboxGroup" onfocus="this.value = remove_underscore(this.value);">'.$value.'</textarea>
				
				</div> ';
			}
			else
			{
				$strBuffer = $strBuffer.' onclick="var commentdiv = document.getElementById(\'comment_'.$keyId.'_div\'); commentdiv.style.display = \'none\'; var commentfield = document.getElementById(\'comment_'.$keyId.'_nCheckboxGroup\'); commentfield.value = \'\';var conditiondiv = document.getElementById(\'condition_'.$keyId.'_div\'); conditiondiv.style.display = \'none\'; var conditionfield = document.getElementById(\'condition_'.$keyId.'_nCheckboxGroup\'); conditionfield.value = \'\';">';
				$strBuffer = $strBuffer.''.$arrInputFieldValues[$nMyIndex].'<br>';
			}
			
			$strBuffer = $strBuffer.'>'.$arrInputFieldValues[$nMyIndex].'<br>';
			if ($bIsEnabled) 
			{ // slot is allowed to edit
				$strBuffer = $strBuffer.'<input type="hidden" name="CBName_'.$keyId.'_nCheckboxGroup_'.$nRunningCounter.'_'.$nMyIndex.'" value="'.$arrInputFieldValues[$nMyIndex].'">';
			}
		}
		
		return $strBuffer;
	}
	
	function getComboBoxGroup($nInputfieldID, $strValue, $bIsEnabled, $keyId, $nRunningCounter)
	{
		$strBuffer = '';
		
		$arrSplit = split('---',$strValue);
		
		$arrInputFieldValues = $this->getInputFieldValue($nInputfieldID);
		$arrInputFieldTypes = $this->getInputFieldTypes($nInputfieldID);
		$arrInputFieldRelations = $this->getInputFieldRelation($nInputfieldID);
		
		
		$strBuffer = $strBuffer.'<select name="'.$keyId.'_nComboboxV_'.$nRunningCounter.'" id="'.$keyId.'_nComboboxV_'.$nRunningCounter.'" size="1"';
		if(!$bIsEnabled) 
		{ 
			$strBuffer = $strBuffer.' '; 
		}		
		
		
		
		
			
			if($arrInputFieldTypes[$nMyIndex] == "2")
			{
				$value = "";
				//Check If Field has value, if none then populate with default
				
      				//fetch slot details from keyid(fieldid_slotid_circulationid), user split
					$circ_details = split('_',$keyId);
				
				$q = Doctrine_Query::create()
				   ->from('Comments a')
				   ->where('a.field_id = ? AND a.slot_id = ? AND a.circulation_id = ?', array($circ_details[0],$circ_details[1],$circ_details[2]));
				$comment = $q->fetchOne();
				if($comment)
				{
					$value = $comment->getComment();
				}
				
				if($value == "")
				{
					$q = Doctrine_Query::create()
					   ->from('cfInputfield a')
					   ->where('a.nid = ?', $nInputfieldID);
					$inputfield = $q->fetchOne();
					$value = $inputfield->getCommentrelation();
				}
				
				$strBuffer = $strBuffer.' onclick="var commentdiv = document.getElementById(\'comment_'.$keyId.'_div\'); commentdiv.style.display = \'block\';"> '.$arrInputFieldValues[$nMyIndex].'<br> <div id="comment_'.$keyId.'_div" name="comment_'.$keyId.'_div" style="display: none; padding-left: 20px; padding-top: 5px;">
				<textarea class="InputText" style="width: 90%; height: 60px;" id="comment_'.$keyId.'_nComboboxV" name="comment_'.$keyId.'_nComboboxV" onfocus="this.value = remove_underscore(this.value);">'.$value.'</textarea>
				
				</div> ';
			}
			else if($arrInputFieldTypes[$nMyIndex] == "3")
			{
				$value = "";
				//Check If Field has value, if none then populate with default
				
      				//fetch slot details from keyid(fieldid_slotid_circulationid), user split
					$circ_details = split('_',$keyId);
				
				$q = Doctrine_Query::create()
				   ->from('Conditions a')
				   ->where('a.field_id = ? AND a.slot_id = ? AND a.circulation_id = ?', array($circ_details[0],$circ_details[1],$circ_details[2]));
				$condition = $q->fetchOne();
				if($condition)
				{
					$value = $condition->getConditionText();
				}
				
				if($value == "")
				{
					$q = Doctrine_Query::create()
					   ->from('cfInputfield a')
					   ->where('a.nid = ?', $nInputfieldID);
					$inputfield = $q->fetchOne();
					$value = $inputfield->getCommentrelation();
				}
				
				$strBuffer = $strBuffer.' onclick="var conditiondiv = document.getElementById(\'condition_'.$keyId.'_div\'); conditiondiv.style.display = \'block\';"> '.$arrInputFieldValues[$nMyIndex].'<br> <div id="condition_'.$keyId.'_div" name="condition_'.$keyId.'_div" style="display: none; padding-left: 20px; padding-top: 5px;">
				<textarea class="InputText" style="width: 90%; height: 60px;" id="condition_'.$keyId.'_nComboboxV" name="condition_'.$keyId.'_nComboboxV" onfocus="this.value = remove_underscore(this.value);">'.$value.'</textarea>
				
				</div> ';
			}
			else
			{
				$strBuffer = $strBuffer.' onclick="var commentdiv = document.getElementById(\'comment_'.$keyId.'_div\'); commentdiv.style.display = \'none\'; var commentfield = document.getElementById(\'comment_'.$keyId.'_nComboboxV\'); commentfield.value = \'\';var conditiondiv = document.getElementById(\'condition_'.$keyId.'_div\'); conditiondiv.style.display = \'none\'; var conditionfield = document.getElementById(\'condition_'.$keyId.'_nComboboxV\'); conditionfield.value = \'\';">';
				$strBuffer = $strBuffer.''.$arrInputFieldValues[$nMyIndex].'<br>';
			}
		
		
		$strBuffer = $strBuffer.'>';
		
		$nMax = sizeof($arrInputFieldValues);
		for ($nMyIndex = 0; $nMyIndex < $nMax; $nMyIndex++)
		{
			$nCurState 	= $arrSplit[$nMyIndex];		// state of Radiobutton either '0' or '1'

			if ($nCurState)
			{
				$strBuffer = $strBuffer.'<option value="'.$nMyIndex.'" selected>'.$arrInputFieldValues[$nMyIndex].'</option>';
			}
			else
			{
				$strBuffer = $strBuffer.'<option value="'.$nMyIndex.'">'.$arrInputFieldValues[$nMyIndex].'</option>';
			}
		}
		$strBuffer .= '</select>';
		
		if ($bIsEnabled) 
		{ // slot is allowed to edit
			for ($nMyIndex = 0; $nMyIndex < $nMax; $nMyIndex++)
			{
				$nCurState 	= $arrSplit[$nMyIndex];		// state of Radiobutton either '0' or '1'
				
				$strBuffer = $strBuffer.'<input type="hidden" name="COMBOName_'.$keyId.'_nCombobox_'.$nRunningCounter.'_'.$nMyIndex.'" value="'.$arrInputFieldValues[$nMyIndex].'">';			
			}
		}
		
		return $strBuffer;
	}
	
	function getComboBoxGroup2($nInputfieldID, $circ_id, $slot_id, $strValue, $bIsEnabled, $keyId, $nRunningCounter)
	{
		$strBuffer = '';
		
		$arrSplit = split('---',$strValue);
		
		$arrInputFieldValues = $this->getInputFieldValue($nInputfieldID);
		$arrInputFieldTypes = $this->getInputFieldTypes($nInputfieldID);
		$arrInputFieldRelations = $this->getInputFieldRelation($nInputfieldID);
		
		
		$strBuffer = $strBuffer.'<select name="'.$keyId.'_nComboboxV_'.$nRunningCounter.'" id="'.$keyId.'_nComboboxV_'.$nRunningCounter.'" size="1"';
		if(!$bIsEnabled) 
		{ 
			$strBuffer = $strBuffer.' '; 
		}		
		
		
		
		
			
			if($arrInputFieldTypes[$nMyIndex] == "2")
			{
				$value = "";
				//Check If Field has value, if none then populate with default
				
      				//fetch slot details from keyid(fieldid_slotid_circulationid), user split
					$circ_details = split('_',$keyId);
				
				$q = Doctrine_Query::create()
				   ->from('Comments a')
				   ->where('a.field_id = ? AND a.slot_id = ? AND a.circulation_id = ?', array($nInputfieldID,$circ_id,$slot_id));
				$comment = $q->fetchOne();
				if($comment)
				{
					$value = $comment->getComment();
				}
				
				if($value == "")
				{
					$q = Doctrine_Query::create()
					   ->from('cfInputfield a')
					   ->where('a.nid = ?', $nInputfieldID);
					$inputfield = $q->fetchOne();
					$value = $inputfield->getCommentrelation();
				}
				
				$strBuffer = $strBuffer.' onclick="var commentdiv = document.getElementById(\'comment_'.$keyId.'_div\'); commentdiv.style.display = \'block\';"> '.$arrInputFieldValues[$nMyIndex].'<br> <div id="comment_'.$keyId.'_div" name="comment_'.$keyId.'_div" style="display: none; padding-left: 20px; padding-top: 5px;">
				<textarea class="InputText" style="width: 90%; height: 60px;" id="comment_'.$keyId.'_nComboboxV" name="comment_'.$keyId.'_nComboboxV" onfocus="this.value = remove_underscore(this.value);">'.$value.'</textarea>
				
				</div> ';
			}
			else if($arrInputFieldTypes[$nMyIndex] == "3")
			{
				$value = "";
				//Check If Field has value, if none then populate with default
				
      				//fetch slot details from keyid(fieldid_slotid_circulationid), user split
					$circ_details = split('_',$keyId);
				
				$q = Doctrine_Query::create()
				   ->from('Conditions a')
				   ->where('a.field_id = ? AND a.slot_id = ? AND a.circulation_id = ?', array($circ_details[0],$circ_details[1],$circ_details[2]));
				$condition = $q->fetchOne();
				if($condition)
				{
					$value = $condition->getConditionText();
				}
				
				if($value == "")
				{
					$q = Doctrine_Query::create()
					   ->from('cfInputfield a')
					   ->where('a.nid = ?', $nInputfieldID);
					$inputfield = $q->fetchOne();
					$value = $inputfield->getCommentrelation();
				}
				
				$strBuffer = $strBuffer.' onclick="var conditiondiv = document.getElementById(\'condition_'.$keyId.'_div\'); conditiondiv.style.display = \'block\';"> '.$arrInputFieldValues[$nMyIndex].'<br> <div id="condition_'.$keyId.'_div" name="condition_'.$keyId.'_div" style="display: none; padding-left: 20px; padding-top: 5px;">
				<textarea class="InputText" style="width: 90%; height: 60px;" id="condition_'.$keyId.'_nComboboxV" name="condition_'.$keyId.'_nComboboxV" onfocus="this.value = remove_underscore(this.value);">'.$value.'</textarea>
				
				</div> ';
			}
			else
			{
				$strBuffer = $strBuffer.' onclick="var commentdiv = document.getElementById(\'comment_'.$keyId.'_div\'); commentdiv.style.display = \'none\'; var commentfield = document.getElementById(\'comment_'.$keyId.'_nComboboxV\'); commentfield.value = \'\';var conditiondiv = document.getElementById(\'condition_'.$keyId.'_div\'); conditiondiv.style.display = \'none\'; var conditionfield = document.getElementById(\'condition_'.$keyId.'_nComboboxV\'); conditionfield.value = \'\';">';
				$strBuffer = $strBuffer.''.$arrInputFieldValues[$nMyIndex].'<br>';
			}
		
		
		$strBuffer = $strBuffer.'>';
		
		$nMax = sizeof($arrInputFieldValues);
		for ($nMyIndex = 0; $nMyIndex < $nMax; $nMyIndex++)
		{
			$nCurState 	= $arrSplit[$nMyIndex];		// state of Radiobutton either '0' or '1'

			if ($nCurState)
			{
				$strBuffer = $strBuffer.'<option value="'.$nMyIndex.'" selected>'.$arrInputFieldValues[$nMyIndex].'</option>';
			}
			else
			{
				$strBuffer = $strBuffer.'<option value="'.$nMyIndex.'">'.$arrInputFieldValues[$nMyIndex].'</option>';
			}
		}
		$strBuffer .= '</select>';
		
		if ($bIsEnabled) 
		{ // slot is allowed to edit
			for ($nMyIndex = 0; $nMyIndex < $nMax; $nMyIndex++)
			{
				$nCurState 	= $arrSplit[$nMyIndex];		// state of Radiobutton either '0' or '1'
				
				$strBuffer = $strBuffer.'<input type="hidden" name="COMBOName_'.$keyId.'_nCombobox_'.$nRunningCounter.'_'.$nMyIndex.'" value="'.$arrInputFieldValues[$nMyIndex].'">';			
			}
		}
		
		return $strBuffer;
	}
	
	function getInputFieldValue($nInputfieldID)
	{
		$strQuery 	= "SELECT strstandardvalue FROM cf_inputfield WHERE nid = '$nInputfieldID' LIMIT 1";
		$nResult 	= @mysql_query($strQuery);
		
		$arrRow = mysql_fetch_array($nResult, MYSQL_ASSOC);
				
		$arrSplit = split('---',$arrRow['strstandardvalue']);
		
		$nMax = $arrSplit[1]+1;
		
		for ($nMyIndex = 1; $nMyIndex < $nMax; $nMyIndex++)
		{
			$splitIndex = $nMyIndex + $nMyIndex;
			$retArrIndex = $nMyIndex - 1;
			
			$arrInputFieldValues[$retArrIndex] = $arrSplit[$splitIndex];
		}
		return $arrInputFieldValues;
	}
	
	function getInputFieldRelation($nInputfieldID)
	{
		$strQuery 	= "SELECT commentrelation FROM cf_inputfield WHERE nid = '$nInputfieldID' LIMIT 1";
		$nResult 	= @mysql_query($strQuery);
		
		$arrRow = mysql_fetch_array($nResult, MYSQL_ASSOC);
				
		$arrSplit = split('---',$arrRow['commentrelation']);
		
		$nMax = $arrSplit[1]+1;
		
		for ($nMyIndex = 1; $nMyIndex < $nMax; $nMyIndex++)
		{
			$splitIndex = $nMyIndex + $nMyIndex;
			$retArrIndex = $nMyIndex - 1;
			
			$arrInputFieldValues[$retArrIndex] = $arrSplit[$splitIndex];
		}
		return $arrInputFieldValues;
	}
	
	function getInputFieldTypes($nInputfieldID)
	{
		$strQuery 	= "SELECT commenttype FROM cf_inputfield WHERE nid = '$nInputfieldID' LIMIT 1";
		$nResult 	= @mysql_query($strQuery);
		
		$arrRow = mysql_fetch_array($nResult, MYSQL_ASSOC);
				
		$arrSplit = split('---',$arrRow['commenttype']);
		
		$nMax = $arrSplit[1]+1;
		
		for ($nMyIndex = 1; $nMyIndex < $nMax; $nMyIndex++)
		{
			$splitIndex = $nMyIndex + $nMyIndex;
			$retArrIndex = $nMyIndex - 1;
			
			$arrInputFieldValues[$retArrIndex] = $arrSplit[$splitIndex];
		}
		return $arrInputFieldValues;
	}
	
	/**
	**	@param $nCirculationFormID
	*	@return the circulationhistory ID
	*/
	function getMaxCirculationHistoryID($nCirculationFormID)
	{
		$strQuery 	= "SELECT MAX(nid) FROM cf_circulationhistory WHERE ncirculationformid = '$nCirculationFormID'";
		$nResult 	= @mysql_query($strQuery);
		
		$arrRow = mysql_fetch_array($nResult);
		return $arrRow[0]; // returns the circulationhistory ID		
	}
	
	/**
	*	@param $nCirculationFormID
	*	@return the circulationForm
	*/
	function getCirculationForm($nCirculationFormID)
	{
		$strQuery 	= "SELECT * FROM cf_circulationform WHERE nid = '$nCirculationFormID' LIMIT 1;";
		$nResult 	= @mysql_query($strQuery);
		
		return mysql_fetch_array($nResult, MYSQL_ASSOC);
	}
	
	/**
	*	adds a new Circulation Form
	*	@param $strCirculationName, $nMailinglistID, $nSenderID, $SuccessMail, $SuccessArchive
	*/
	function addCirculationForm($strCirculationName, $nMailinglistID, $nSenderID, $SuccessMail, $SuccessArchive, $SuccessDelete, $bAnonymize)
	{
		global $_REQUEST;
		
		$dateSending = mktime(date("H"),date("i"),date("s"),date("m"),date("d"),date("Y"));
		
		$nEndAction = 0;
		
		if ($SuccessMail != 0) $nEndAction += $SuccessMail;
		if ($SuccessArchive == 'on') $nEndAction += 2;
		if ($SuccessDelete == 'on') $nEndAction += 4;
		
		// check the hook CF_ENDACTION
			$endActions	= $this->getExtensionsByHookId('CF_ENDACTION');
			if ($endActions)
			{
				foreach ($endActions as $endAction)
				{
					$params = $this->getEndActionParams($endAction);
					
					if ($_REQUEST[$params['checkboxName']] == 'on') $nEndAction += $params['hookValue'];
				}
			}
		
		
		$strQuery 	= "INSERT INTO cf_circulationform values (null, '$nSenderID', '$strCirculationName', '$nMailinglistID', 0, '$nEndAction', 0, $bAnonymize)";
		$nResult 	= mysql_query($strQuery) or die(mysql_error()."- $strQuery");
		
		$strQuery 	= "SELECT MAX(nid) FROM cf_circulationform WHERE bdeleted = 0";
		$nResult 	= @mysql_query($strQuery);
		
		$arrRow 	= @mysql_fetch_array($nResult);
		
		return $arrRow[0]; //returns the circulationform ID
	}
	
	/**
	*	adds a new Circulation History
	*	@param $nCirculationFormID, $strAdditionalText
	*/
	function addCirculationHistory($nCirculationFormID, $strAdditionalText)
	{	
		$dateSending = mktime(date("H"),date("i"),date("s"),date("m"),date("d"),date("Y"));
		
		$strQuery 	= "INSERT INTO cf_circulationhistory VALUES(null, 1, '$dateSending', '$strAdditionalText', $nCirculationFormID)";
		$nResult 	= @mysql_query($strQuery);
		
		$strQuery 	= "SELECT MAX(nid) FROM cf_circulationhistory";
		$nResult 	= @mysql_query($strQuery);
		
		$arrRow = mysql_fetch_array($nResult);
		return $arrRow[0]; // returns the circulationhistory ID		
	}
	
	/**
	*	@param $nCirculationProcessID
	*	@return the circulationhistory ID
	*/
	function getCirculationHistoryID($nCirculationProcessID)
	{
		$strQuery 	= "SELECT ncirculationhistoryid FROM cf_circulationprocess WHERE nid = '$nCirculationProcessID' LIMIT 1;";
		$nResult 	= @mysql_query($strQuery);
		
		$arrResult = mysql_fetch_array($nResult, MYSQL_ASSOC);
		
		return $arrResult['ncirculationhistoryid'];
	}
	
	/**
	*	@param $nCirculationFormID, $nCirculationHistoryID
	*	@return the circulation process informations
	*/
	function getCirculationProcess($nCirculationFormID, $nCirculationHistoryID)
	{
		$strQuery 	= "	SELECT *
						FROM cf_circulationprocess
						WHERE ncirculationformid = '$nCirculationFormID' AND ncirculationhistoryid = '$nCirculationHistoryID' AND nissubstitiuteof = '0'
						ORDER BY dateinprocesssince ASC;";
		$nResult 	= mysql_query($strQuery);
		
		if ($nResult)
		{
			$nIndex = 0;
			while (	$arrRow = mysql_fetch_array($nResult, MYSQL_ASSOC))
			{
				$arrRows[$nIndex] = $arrRow;
				$nIndex++;						
			}
		}
		return $arrRows;
	}
	
	/**
	*	@param $nCirculationFormID, $nCirculationHistoryID, $tsMyDateInProcessSince
	*/
	function getLaterEntries($nCirculationFormID, $nCirculationHistoryID, $tsMyDateInProcessSince)	
	{
		$strQuery 	= "	SELECT *
						FROM cf_circulationprocess
						WHERE ncirculationformid = '$nCirculationFormID' AND ncirculationhistoryid = '$nCirculationHistoryID' AND dateinprocesssince > '$tsMyDateInProcessSince'
						ORDER BY dateinprocesssince ASC;";
		$nResult 	= mysql_query($strQuery) or die(mysql_error());
		
		if ($nResult)
		{
			$nIndex = 0;
			while (	$arrRow = mysql_fetch_array($nResult, MYSQL_ASSOC))
			{
				$arrRows[$nIndex] = $arrRow;
				$nIndex++;						
			}
		}
		return $arrRows;
	}
	
	/**
	*	sets the current station
	*	@param $nMyCirculationProcessID
	*/
	function setCurrentStation($nMyCirculationProcessID)
	{
		$strQuery = "UPDATE `cf_circulationprocess` SET	`dateinprocesssince`	= '".time()."',
														`ndecissionstate` 		= '0',
														`datedecission` 		= '0'
														WHERE `nid` = '$nMyCirculationProcessID' LIMIT 1 ;";
		$result		= mysql_query($strQuery) or die (mysql_error());
	}
	
	/**
	*	sets the current station state to skipped
	*	@param $nCURCirculationProcessID
	*/
	function setStationToSkipped($nCURCirculationProcessID)
	{
		$strQuery = "UPDATE `cf_circulationprocess` SET	`ndecissionstate` 		= '4',
														`datedecission` 		= '".(time()-60)."'
														WHERE `nid` = '$nCURCirculationProcessID' LIMIT 1 ;";
		$result		= mysql_query($strQuery) or die (mysql_error());
	}
	
	/**
	*	adds a new Circulation Process
	*	@param $nCirculationFormID, $nCirculationHistoryID, $nCurSlotID, $nCurUserID, $tsDateInProcessSince, $tsDateDecission
	*/
	function addCirculationProcess($nCirculationFormID, $nCirculationHistoryID, $nCurSlotID, $nCurUserID, $tsDateInProcessSince, $tsDateDecission)
	{
		$strQuery 	= "INSERT INTO `cf_circulationprocess` ( `nid` , `ncirculationformid` , `nslotid`, `nuserid` , `dateinprocesssince` , `ndecissionstate`, `datedecission` , `nissubstitiuteof` , `ncirculationhistoryid`)
									VALUES ( NULL , '$nCirculationFormID' , '$nCurSlotID', '$nCurUserID', '$tsDateInProcessSince', '4', '$tsDateDecission', '0', '$nCirculationHistoryID');";
		$result		= mysql_query($strQuery) or die (mysql_error());
	}
	
	/**
	*	adds a new Circulation Process
	*	@param $nCirculationFormID, $nCirculationHistoryID, $nCurSlotID, $nCurUserID, $tsDateInProcessSince
	*/
	function addCurCirculationProcess($nCirculationFormID, $nCirculationHistoryID, $nCurSlotID, $nCurUserID, $tsDateInProcessSince)
	{
		$strQuery 	= "INSERT INTO `cf_circulationprocess` ( `nid` , `ncirculationformid` , `nslotid`, `nuserid` , `dateinprocesssince` , `ndecissionstate`, `datedecission` , `nissubstitiuteof` , `ncirculationhistoryid`)
									VALUES ( NULL , '$nCirculationFormID' , '$nCurSlotID', '$nCurUserID', '$tsDateInProcessSince', '0', '0', '0', '$nCirculationHistoryID');";
		$result		= mysql_query($strQuery) or die (mysql_error());
	}
	
	/**
	*	@param $nCirculationProcessID
	*	@return the circulation process informations
	*/
	function getMyCirculationProcess($nCirculationProcessID)
	{
		$strQuery 	= "SELECT * FROM cf_circulationprocess WHERE nid = '$nCirculationProcessID' LIMIT 1;";
		$nResult 	= @mysql_query($strQuery);

		return mysql_fetch_array($nResult, MYSQL_ASSOC);
	}
	
	/**
	*	@param $nDELCirculationProcessID
	*/
	function deleteMyCirculationProcess($nDELCirculationProcessID)
	{
		$strQuery 	= "DELETE FROM cf_circulationprocess WHERE nid = '$nDELCirculationProcessID' LIMIT 1;";
		$nResult 	= @mysql_query($strQuery);
	}
	
	/**
	*	@param $nCirculationHistoryID
	*	@return the circulation history informations
	*/
	function getCirculationHistory($nCirculationHistoryID)
	{
		$strQuery 	= "SELECT * FROM cf_circulationhistory WHERE nid = '$nCirculationHistoryID' LIMIT 1;";
		$nResult 	= @mysql_query($strQuery);

		return mysql_fetch_array($nResult, MYSQL_ASSOC);
	}
	
	/**
	*	@return the input fields
	*/
	function getAllInputFields()
	{
		$strQuery 	= "SELECT * FROM cf_inputfield ORDER BY strname;";
		$nResult 	= @mysql_query($strQuery);
		
		if ($nResult)
		{
			$nIndex = 0;
			while (	$arrRow = mysql_fetch_array($nResult, MYSQL_ASSOC))
			{
				$arrRows[$nIndex] = $arrRow;
				$nIndex++;						
			}
		}
		return $arrRows;
	}
	
	/**
	*	@return the input fields
	*/
	function getMyInputFields()
	{
		$strQuery 	= "SELECT * FROM cf_inputfield WHERE ntype <> '4' AND ntype <> '9' AND ntype <> '7' ORDER BY strname;";
		$nResult 	= @mysql_query($strQuery);
		
		if ($nResult)
		{
			$nIndex = 0;
			while (	$arrRow = mysql_fetch_array($nResult, MYSQL_ASSOC))
			{
				$arrRows[$nIndex] = $arrRow;
				$nIndex++;						
			}
		}
		return $arrRows;
	}
	
	/**
	*	@param $nCirculationFormID, $nCirculationHistoryID
	*	@return the field values
	*/
	function getFieldValues($nCirculationFormID, $nCirculationHistoryID)
	{
        $strQuery 	= "SELECT * FROM cf_fieldvalue WHERE nformid = '$nCirculationFormID' AND ncirculationhistoryid = '$nCirculationHistoryID'";
		$nResult 	= @mysql_query($strQuery);
		
		while (	$arrRow = mysql_fetch_array($nResult))
		{
			$arrValues[$arrRow['ninputfieldid'].'_'.$arrRow['nslotid'].'_'.$arrRow['nformid']] = $arrRow;
		}
		
		return $arrValues;
	}
	
	/**
	*	@param $nCirculationFormID, $nCirculationHistoryID, $nInputFieldID
	*	@return the field values
	*/
	function getMyFieldValue($nCirculationFormID, $nCirculationHistoryID, $nInputFieldID)
	{
        $strQuery 	= "SELECT * FROM cf_fieldvalue WHERE ninputfieldid = '$nInputFieldID' AND nformid = '$nCirculationFormID' AND ncirculationhistoryid = '$nCirculationHistoryID'";
		
        $nResult 	= mysql_query($strQuery);
		
		if ($nResult)
		{
			$nIndex = 0;
			while (	$arrRow = mysql_fetch_array($nResult, MYSQL_ASSOC))
			{
				$arrRows[$nIndex] = $arrRow;
				$nIndex++;						
			}
		}
		
		$strFieldValue 	= $arrRows[0]['strfieldvalue'];
		$nFieldType 	= $this->getFieldType($nInputFieldID);
		
		if ($nFieldType == 1)
		{
			$arrValue = split('rrrrr',$strFieldValue);
			return  $arrValue[0];
		}
		else if ($nFieldType == 2)
		{
			if ($strFieldValue != "on")
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
		else if ($nFieldType == 3)
		{
			$arrValue = split('xx',$strFieldValue);								
			$nNumGroup 	= $arrValue[1];														
			$arrValue1 = split('rrrrr',$arrValue[2]);														
			$strMyValue	= $arrValue1[0];

			return $strMyValue;
		}
		else if ($nFieldType == 5)
		{
			return  $strFieldValue;
		}
		else if ($nFieldType == 6)
		{
			$arrRBGroup = '';
			$arrGroup = '';
			
			$arrSplit = split('---',$strFieldValue);
			
			$arrRBGroup[]	= $arrSplit['2'];
			$arrGroup[]		= $arrSplit['3'];
			$arrRBGroup[]	= $arrSplit['4'];
			$arrGroup[]		= $arrSplit['5'];
			$arrRBGroup[]	= $arrSplit['6'];
			$arrGroup[]		= $arrSplit['7'];
			$arrRBGroup[]	= $arrSplit['8'];
			$arrGroup[]		= $arrSplit['9'];
			$arrRBGroup[]	= $arrSplit['10'];
			$arrGroup[]		= $arrSplit['11'];
			$arrRBGroup[]	= $arrSplit['12'];
			$arrGroup[]		= $arrSplit['13'];
																
			for ($nMyIndex = 0; $nMyIndex < $arrSplit['1']; $nMyIndex++)
			{
				$CurStrRBGroup 	= $arrRBGroup[$nMyIndex];	// content of corresponding Radiobutton
				$CurRBGroup 	= $arrGroup[$nMyIndex];		// state of Radiobutton either '0' or '1'
				
				if ($CurRBGroup)
				{ 
					return $CurStrRBGroup;
				}														
			}	
		}
		elseif($nFieldType == 8)
		{
			$arrComboGroup = '';
			$arrGroup = '';
			
			$arrSplit = split('---',$strFieldValue);
																
			$arrComboGroup[]	= $arrSplit['2'];
			$arrGroup[]			= $arrSplit['3'];
			$arrComboGroup[]	= $arrSplit['4'];
			$arrGroup[]			= $arrSplit['5'];
			$arrComboGroup[]	= $arrSplit['6'];
			$arrGroup[]			= $arrSplit['7'];
			$arrComboGroup[]	= $arrSplit['8'];
			$arrGroup[]			= $arrSplit['9'];
			$arrComboGroup[]	= $arrSplit['10'];
			$arrGroup[]			= $arrSplit['11'];
			$arrComboGroup[]	= $arrSplit['12'];
			$arrGroup[]			= $arrSplit['13'];
									
			for ($nMyIndex = 0; $nMyIndex < $arrSplit['1']; $nMyIndex++)
			{
				$CurStrCBGroup 	= $arrComboGroup[$nMyIndex];	
				$CurCBGroup 	= $arrGroup[$nMyIndex];
				
				if ($CurCBGroup)
				{
					return $CurStrCBGroup;
				}
			}
		}
	}
	
	function getMyFilters($nID)
	{
		$strQuery 	= "SELECT * FROM cf_filter WHERE nuserid = '$nID'";
		$nResult 	= @mysql_query($strQuery);
		
		if ($nResult)
		{
			$nIndex = 0;
			while (	$arrRow = mysql_fetch_array($nResult, MYSQL_ASSOC))
			{
				$arrRows[$nIndex] = $arrRow;
				$nIndex++;						
			}
		}		
		return $arrRows;
	}
	
	function getFilter($nID)
	{
		$strQuery 	= "SELECT * FROM cf_filter WHERE nid = '$nID' LIMIT 1";
		$nResult 	= @mysql_query($strQuery);
		
		return mysql_fetch_array($nResult, MYSQL_ASSOC);
	}
	
	/**
	*	@param $nInputFieldID
	*	@return the field type
	*/
	function getFieldType($nInputFieldID)
	{
		$strQuery 	= "SELECT ntype FROM cf_inputfield WHERE nid = '$nInputFieldID'";
		$nResult 	= @mysql_query($strQuery);
		
		if ($nResult)
		{
			$nIndex = 0;
			while (	$arrRow = mysql_fetch_array($nResult, MYSQL_ASSOC))
			{
				$arrRows[$nIndex] = $arrRow;
				$nIndex++;						
			}
		}
		
		return $arrRows[0]['ntype'];
	}
	
	/**
	*	@return the Users
	*/
	function getUsers()
	{
		$strQuery = "SELECT * FROM cf_user  WHERE bdeleted <> 1 ORDER BY struserid;";
		$nResult = @mysql_query($strQuery);
		if ($nResult)
		{
			while (	$arrRow = mysql_fetch_array($nResult, MYSQL_ASSOC))
			{
				$arrUsers[$arrRow["nid"]] = $arrRow;						
			}
		}
		
		return $arrUsers;
	}
	
	/**
	*	@param $nUserID
	*	@return the username
	*/
	function getUsername($nUserID)
	{
		$strQuery = "SELECT * FROM cf_user WHERE nid = '$nUserID' LIMIT 1;";
		$nResult = @mysql_query($strQuery);
		if ($nResult)
		{
			$nIndex = 0;
			while (	$arrRow = mysql_fetch_array($nResult, MYSQL_ASSOC))
			{
				$arrRows[$nIndex] = $arrRow;
				$nIndex++;						
			}
		}
		return $arrRows[0]['struserid']." (".$arrRows[0]['strlastname'].", ". $arrRows[0]['strfirstname'].")";
	}
		
	/**
	*	@return the Users
	*/
	function getAllUsers($include_deleted = true)
	{
		if ($include_deleted) {
			$strQuery = "SELECT * FROM cf_user ORDER BY struserid ASC;";	
		}
		else {
			$strQuery = "SELECT * FROM cf_user WHERE bdeleted=0 ORDER BY struserid ASC;";
		}
		
		$nResult = @mysql_query($strQuery);
		if ($nResult)
		{
			$nIndex = 0;
			while (	$arrRow = mysql_fetch_array($nResult, MYSQL_ASSOC))
			{
				$arrRows[$arrRow['nid']] = $arrRow;
				$nIndex++;						
			}
		}
		return $arrRows;
	}
	
	function getAllTemplates()
	{
		$strQuery = "SELECT * FROM cf_formtemplate WHERE bdeleted=0 ORDER BY strname ASC;";
		$nResult = @mysql_query($strQuery);
		if ($nResult)
		{
			$nIndex = 0;
			while (	$arrRow = mysql_fetch_array($nResult, MYSQL_ASSOC))
			{
				$arrRows[$nIndex] = $arrRow;
				$nIndex++;						
			}
		}
		return $arrRows;
	}
	
	function getTemplate($nID)
	{
		$strQuery 	= "SELECT * FROM cf_formtemplate WHERE nid = '$nID' LIMIT 1;";
		$nResult 	= @mysql_query($strQuery);
		
		$arrResult	= mysql_fetch_array($nResult, MYSQL_ASSOC);
		return $arrResult;
	}
	
	function getWholeTime($nCirculationFormID)
	{
		$strQuery 	= "SELECT datesending FROM cf_circulationhistory WHERE ncirculationformid = '$nCirculationFormID' LIMIT 1;";
		$nResult 	= @mysql_query($strQuery);
		
		$arrRow		= mysql_fetch_array($nResult, MYSQL_ASSOC);
		
		$tsDateSending 	= $arrRow['datesending'];
		$tsNow			= time();
		
		$secDif = $tsNow - $tsDateSending;
		
		if ($secDif > 86400)
		{
			$daysDif = (int) ($secDif / (60 * 60 * 24));
			return $daysDif;
		}
		else
		{
			return 0;
		}		
	}
	
	/**
	*	@param $nID
	*	@return the mailinglist
	*/
	function getMailinglist($nID)
	{
		$strQuery 	= "SELECT * FROM cf_mailinglist WHERE nid = '$nID' LIMIT 1;";
		$nResult 	= @mysql_query($strQuery);
		
		return mysql_fetch_array($nResult, MYSQL_ASSOC);
	}
	
	/**
	*	@return the mailinglists
	*/
	function getAllMailingLists()
	{
		$strQuery 	= "SELECT * FROM cf_mailinglist WHERE bisedited = '0' ORDER BY strname ASC;";
		$nResult 	= @mysql_query($strQuery);
		
		if ($nResult)
		{
			$nIndex = 0;
			while (	$arrRow = mysql_fetch_array($nResult, MYSQL_ASSOC))
			{
				$arrRows[$nIndex] = $arrRow;
				$nIndex++;						
			}
		}
		return $arrRows;
	}
	
	/**
	*	@param $nTemplateID
	*	@return the formslots
	*/
	function getFormslots($nTemplateID)
	{
		$strQuery = "SELECT * FROM cf_formslot WHERE ntemplateid = '$nTemplateID' ORDER BY nslotnumber ASC";
		$nResult = mysql_query($strQuery);
		
		if ($nResult)
		{
			$nIndex = 0;
			while (	$arrRow = mysql_fetch_array($nResult))
			{
				$arrSlots[$nIndex] = $arrRow;
				
				$nIndex++;
			}
		}
		
		return $arrSlots;
	}
	
	/**
	*	@param $start, $sortby, $sortDirection, $archivemode, $nShowRows, $bFilterOn
	*	@return the Circulation Overview
	*/
	function getOwnCirculationOverview($start, $sortby, $sortDirection, $archivemode, $nShowRows, $bFilterOn, $FILTER_Name = '', $FILTER_Sender = false, $FILTER_Mailinglist = false, $FILTER_Station = false, $FILTER_DAYS_IN_PROGRESS_START = '', $FILTER_DAYS_IN_PROGRESS_END = '', $FILTER_DATE_START = '', $FILTER_DATE_END = '', $FILTER_TEMPLATE = false, $FILTER_CUSTOM = false)
	{
		
		global $_REQUEST;
		
		if ($FILTER_CUSTOM)
		{
			$strFilterCustomQuery = "";
			
			
			// only filter the first entry
			// TODO
			$arrFilter = $FILTER_CUSTOM[0];
				
			$nInputfieldId 	= $arrFilter['ninputfieldid'];
			$strOperator 	= $arrFilter['stroperator'];
			$strValue		= $arrFilter['strvalue'];
			
			if (($nInputfieldId != '') && ($strOperator != '') && ($strValue != ''))
			{
				$strFilterCustomQuery .= " AND ninputfieldid = '$nInputfieldId' AND strfieldvalue LIKE '%$strValue%'";
			}
		}
		
		if (($FILTER_DATE_START != '') || ($FILTER_DATE_END != ''))
		{
			$FILTER_DATE = 1;
			$arrDateStart 	= explode(".", $FILTER_DATE_START);
			$arrDateEnd 	= explode(".", $FILTER_DATE_END);
			
			$tsDateMin  	= @mktime(0,0,0,$arrDateStart[1],$arrDateStart[0],$arrDateStart[2]);
			if ($FILTER_DATE_START == '')
			{
				$tsDateMin = -9999999999;
			}
			
			$tsDateMax  	= @mktime(0,0,0,$arrDateEnd[1],$arrDateEnd[0],$arrDateEnd[2]);
			if ($FILTER_DATE_END == '')
			{
				$tsDateMax = 9999999999;
			}
		}
		
		$secondsDay		= (24 * 60 * 60);
		
		if (($FILTER_DAYS_IN_PROGRESS_START != '') || ($FILTER_DAYS_IN_PROGRESS_END != ''))
		{
			$FILTER_DIP = 1;
			$givenMin = $FILTER_DAYS_IN_PROGRESS_START;
			$givenMax = $FILTER_DAYS_IN_PROGRESS_END;
			
			$tsDIPMin = time() - ($givenMin * $secondsDay);
			if ($FILTER_DAYS_IN_PROGRESS_START == '')
			{
				$tsDIPMin = 9999999999;
			}
			
			$tsDIPMax = time() - ($givenMax * $secondsDay);
			if ($FILTER_DAYS_IN_PROGRESS_END == '')
			{
				$tsDIPMax = -9999999999;
			}
		}
		
		$nNumberOfRows = $nShowRows;
		$start--;
		if ($FILTER_CUSTOM)
		{
			$start = 0;
			$nNumberOfRows = 50;
		}
		switch($sortby)
		{
			case 'COL_CIRCULATION_NAME':
				$strQuery 	= "		SELECT";
				if ($FILTER_DATE || $FILTER_DIP || $FILTER_CUSTOM)
				{
					$strQuery .= " DISTINCT";
				}
				$strQuery .= "				cf.*
									FROM cf_circulationform cf";
				if ($FILTER_Station || $FILTER_DIP || $FILTER_DATE || $FILTER_CUSTOM)
				{
					$strQuery .= "	INNER JOIN cf_circulationprocess cp
									ON cp.ncirculationformid = cf.nid";
				}
				if ($FILTER_Station || $FILTER_DIP)
				{
					$strQuery .= " AND (cp.ndecissionstate = '0' OR cp.ndecissionstate = '2' OR cp.ndecissionstate = '16')";
					if ($_REQUEST['bOwnCirculations'])
					{
						$strQuery .= " AND cp.ndecissionstate <> 16 AND cp.ndecissionstate <> 2";
					}
				}
				if ($FILTER_DIP)
				{
					$strQuery .= " AND cp.dateinprocesssince < '$tsDIPMin' AND cp.dateinprocesssince > '$tsDIPMax'";
				}
				if ($FILTER_DATE || $FILTER_CUSTOM)
				{
					$strQuery .= " 	INNER JOIN cf_circulationhistory ch
									ON ch.nid = cp.ncirculationhistoryid";
				}
				if ($FILTER_DATE)
				{
					$strQuery .= " AND ch.datesending > '$tsDateMin' AND ch.datesending < '$tsDateMax'";
				}
				if ($FILTER_CUSTOM)
				{
					$strQuery .= " 	INNER JOIN cf_fieldvalue fv
									ON fv.ncirculationhistoryid = ch.nid
									$strFilterCustomQuery";
				}
				if ($FILTER_TEMPLATE || $FILTER_Mailinglist)
				{
					$strQuery .= "	INNER JOIN cf_mailinglist m
									ON cf.nmailinglistid = m.nid";
					if ($FILTER_Mailinglist)
					{
						$strQuery .= " AND m.strname = '$FILTER_Mailinglist'";
					}
					if ($FILTER_TEMPLATE)
					{
						$strQuery .= "	INNER JOIN cf_formtemplate t
										ON t.nid = m.ntemplateid AND t.nid = '$FILTER_TEMPLATE'";
					}
				}
				
				$strQuery .= " WHERE bdeleted = 0";
				if ($FILTER_Name != '')
				{	// extended Filter is active
					$strQuery .= " AND cf.strname LIKE '".$FILTER_Name."%'";
				}
				if ($FILTER_Station)
				{
					$strQuery .= " AND (cp.nuserid = '$FILTER_Station' OR (cp.nuserid = -2 AND cf.nsenderid = $FILTER_Station) )";
				}
				if ($FILTER_Sender != false)
				{	// extended Filter is active
					$strQuery .= " AND cf.nsenderid = '$FILTER_Sender'";
				}
				$strQuery .= " 	ORDER BY cf.strname $sortDirection";
				if (!$bFilterOn || $FILTER_CUSTOM)
				{
					$strQuery .= " LIMIT $start, $nNumberOfRows";
				}
				break;
			case 'COL_CIRCULATION_STATION':
				$strQuery 	= "SELECT DISTINCT cf.*
								FROM cf_circulationform cf
								INNER JOIN cf_circulationprocess cp
								ON cf.nid = cp.ncirculationformid AND (cp.ndecissionstate = '0' OR cp.ndecissionstate = '2' OR cp.ndecissionstate = '16')";
				if ($_REQUEST['bOwnCirculations'])
				{
					$strQuery .= " AND cp.ndecissionstate <> 16 AND cp.ndecissionstate <> 2";
				}
				if ($FILTER_DIP)
				{
					$strQuery .= " AND cp.dateinprocesssince < '$tsDIPMin' AND cp.dateinprocesssince > '$tsDIPMax'";
				}
				if ($FILTER_DATE)
				{
					$strQuery .= " 	INNER JOIN cf_circulationhistory ch
									ON ch.nid = cp.ncirculationhistoryid";
				}
				if ($FILTER_DATE)
				{
					$strQuery .= " AND ch.datesending > '$tsDateMin' AND ch.datesending < '$tsDateMax'";
				}
				$strQuery .= "	INNER JOIN cf_user u
								ON cp.nuserid = u.nid";
				if ($FILTER_TEMPLATE || $FILTER_Mailinglist)
				{
					$strQuery .= "	INNER JOIN cf_mailinglist m
									ON cf.nmailinglistid = m.nid";
					if ($FILTER_Mailinglist)
					{
						$strQuery .= " AND m.strname = '$FILTER_Mailinglist'";
					}
					if ($FILTER_TEMPLATE)
					{
						$strQuery .= "	INNER JOIN cf_formtemplate t
										ON t.nid = m.ntemplateid AND t.nid = '$FILTER_TEMPLATE'";
					}
				}
				$strQuery .= " WHERE cf.bdeleted = 0";
				if ($FILTER_Name != '')
				{	// extended Filter is active
					$strQuery .= " AND cf.strname LIKE '".$FILTER_Name."%'";
				}
				if ($FILTER_Station)
				{
					$strQuery .= " AND (cp.nuserid = '$FILTER_Station' OR (cp.nuserid = -2 AND cf.nsenderid = $FILTER_Station) )";
				}
				if ($FILTER_Sender != false)
				{	// extended Filter is active
					$strQuery .= " AND cf.nsenderid = '$FILTER_Sender'";
				}
				$strQuery .= " 	ORDER BY u.struserid $sortDirection";
				if (!$bFilterOn)
				{
					$strQuery .= " LIMIT $start, $nNumberOfRows";
				}
				break;
			case 'COL_CIRCULATION_PROCESS_DAYS':
				$mySortDirection = 'DESC'; //
				if ($sortDirection == 'DESC')
				{
					$mySortDirection = 'ASC'; // upside down
				}
				
				$strQuery 	= "SELECT MAX(cp.dateinprocesssince) as tsdateinprocesssince, cf.*
								FROM cf_circulationform cf
								INNER JOIN cf_circulationprocess cp
								ON cf.nid = cp.ncirculationformid";
				if ($FILTER_Station || $FILTER_DIP)
				{
					$strQuery .= " AND (cp.ndecissionstate = '0' OR cp.ndecissionstate = '2' OR cp.ndecissionstate = '16')";
					if ($_REQUEST['bOwnCirculations'])
					{
						$strQuery .= " AND cp.ndecissionstate <> 16 AND cp.ndecissionstate <> 2";
					}
				}
				if ($FILTER_DIP)
				{
					$strQuery .= " AND cp.dateinprocesssince < '$tsDIPMin' AND cp.dateinprocesssince > '$tsDIPMax'";
				}
				if ($FILTER_DATE || $FILTER_CUSTOM)
				{
					$strQuery .= " 	INNER JOIN cf_circulationhistory ch
									ON ch.nid = cp.ncirculationhistoryid";
				}
				if ($FILTER_DATE)
				{
					$strQuery .= " AND ch.datesending > '$tsDateMin' AND ch.datesending < '$tsDateMax'";
				}
				if ($FILTER_CUSTOM)
				{
					$strQuery .= " 	INNER JOIN cf_fieldvalue fv
									ON fv.ncirculationhistoryid = ch.nid
									$strFilterCustomQuery";
				}
				if ($FILTER_TEMPLATE || $FILTER_Mailinglist)
				{
					$strQuery .= "	INNER JOIN cf_mailinglist m
									ON cf.nmailinglistid = m.nid";
					if ($FILTER_Mailinglist)
					{
						$strQuery .= " AND m.strname = '$FILTER_Mailinglist'";
					}
					if ($FILTER_TEMPLATE)
					{
						$strQuery .= "	INNER JOIN cf_formtemplate t
										ON t.nid = m.ntemplateid AND t.nid = '$FILTER_TEMPLATE'";
					}
				}
				$strQuery .=	" WHERE cf.bdeleted = 0";
				if ($FILTER_Name != '')
				{	// extended Filter is active
					$strQuery .= " AND cf.strname LIKE '".$FILTER_Name."%'";
				}
				if ($FILTER_Station)
				{
					$strQuery .= " AND (cp.nuserid = '$FILTER_Station' OR (cp.nuserid = -2 AND cf.nsenderid = $FILTER_Station) )";
				}
				if ($FILTER_Sender != false)
				{	// extended Filter is active
					$strQuery .= " AND cf.nsenderid = '$FILTER_Sender'";
				}
				$strQuery .= "	GROUP BY cf.nid
								ORDER BY tsdateinprocesssince $mySortDirection";
				if (!$bFilterOn || $FILTER_CUSTOM)
				{
					$strQuery .= " LIMIT $start, $nNumberOfRows";
				}
				break;
			case 'COL_CIRCULATION_PROCESS_START':
				$strQuery 	= "SELECT DISTINCT";
				//if ($FILTER_DATE || $FILTER_TEMPLATE|| $FILTER_DIP || $FILTER_Mailinglist || $FILTER_CUSTOM)
				//{
				//	$strQuery .= " DISTINCT";
				//}
				$strQuery .= " 		cf.*
								FROM cf_circulationform cf
								INNER JOIN cf_circulationprocess cp
								ON cf.nid = cp.ncirculationformid";
				if ($FILTER_Station || $FILTER_DIP)
				{
					$strQuery .= " AND (cp.ndecissionstate = '0' OR cp.ndecissionstate = '2' OR cp.ndecissionstate = '16')";
					if ($_REQUEST['bOwnCirculations'])
					{
						$strQuery .= " AND cp.ndecissionstate <> 16 AND cp.ndecissionstate <> 2";
					}
				}
				if ($FILTER_DIP)
				{
					$strQuery .= " AND cp.dateinprocesssince < '$tsDIPMin' AND cp.dateinprocesssince > '$tsDIPMax'";
				}
				$strQuery .= "	INNER JOIN cf_circulationhistory ch
								ON ch.nid = cp.ncirculationhistoryid AND ch.ncirculationformid = cf.nid";
				if ($FILTER_DATE)
				{
					$strQuery .= " 	AND ch.datesending > '$tsDateMin' AND ch.datesending < '$tsDateMax'";
				}
				if ($FILTER_CUSTOM)
				{
					$strQuery .= " 	INNER JOIN cf_fieldvalue fv
									ON fv.ncirculationhistoryid = ch.nid
									$strFilterCustomQuery";
				}
				if ($FILTER_TEMPLATE || $FILTER_Mailinglist)
				{
					$strQuery .= "	INNER JOIN cf_mailinglist m
									ON cf.nmailinglistid = m.nid";
					if ($FILTER_Mailinglist)
					{
						$strQuery .= " AND m.strname = '$FILTER_Mailinglist'";
					}
					if ($FILTER_TEMPLATE)
					{
						$strQuery .= "	INNER JOIN cf_formtemplate t
										ON t.nid = m.ntemplateid AND t.nid = '$FILTER_TEMPLATE'";
					}
				}
				$strQuery .=	" WHERE cf.bdeleted = 0";
				if ($FILTER_Name != '')
				{	// extended Filter is active
					$strQuery .= " AND cf.strname LIKE '".$FILTER_Name."%'";
				}
				if ($FILTER_Station)
				{
					$strQuery .= " AND cp.nuserid = '$FILTER_Station'";
				}
				if ($FILTER_Sender != false)
				{	// extended Filter is active
					$strQuery .= " AND cf.nsenderid = '$FILTER_Sender'";
				}
				$strQuery .= " 	ORDER BY ch.datesending $sortDirection";
				if (!$bFilterOn || $FILTER_CUSTOM)
				{
					$strQuery .= " LIMIT $start, $nNumberOfRows";
				}
				break;
			case 'COL_CIRCULATION_SENDER':
				$strQuery	= "SELECT";
				if ($FILTER_DATE || $FILTER_DIP)
				{
					$strQuery .= " DISTINCT";
				}
				$strQuery .= "	cf.*
								FROM cf_circulationform cf
								INNER JOIN cf_user u
								ON cf.nsenderid = u.nid";
				if ($FILTER_Station || $FILTER_DIP || $FILTER_DATE)
				{
					$strQuery .= "	INNER JOIN cf_circulationprocess cp
									ON cp.ncirculationformid = cf.nid";
				}
				if ($FILTER_Station || $FILTER_DIP)
				{
					$strQuery .= " AND (cp.ndecissionstate = '0' OR cp.ndecissionstate = '2' OR cp.ndecissionstate = '16')";
					if ($_REQUEST['bOwnCirculations'])
					{
						$strQuery .= " AND cp.ndecissionstate <> 16 AND cp.ndecissionstate <> 2";
					}
				}
				if ($FILTER_DIP)
				{
					$strQuery .= " AND cp.dateinprocesssince < '$tsDIPMin' AND cp.dateinprocesssince > '$tsDIPMax'";
				}
				if ($FILTER_DATE)
				{
					$strQuery .= " 	INNER JOIN cf_circulationhistory ch
									ON ch.nid = cp.ncirculationhistoryid";
				}
				if ($FILTER_DATE)
				{
					$strQuery .= " AND ch.datesending > '$tsDateMin' AND ch.datesending < '$tsDateMax'";
				}
				if ($FILTER_TEMPLATE || $FILTER_Mailinglist)
				{
					$strQuery .= "	INNER JOIN cf_mailinglist m
									ON cf.nmailinglistid = m.nid";
					if ($FILTER_Mailinglist)
					{
						$strQuery .= " AND m.strname = '$FILTER_Mailinglist'";
					}
					if ($FILTER_TEMPLATE)
					{
						$strQuery .= "	INNER JOIN cf_formtemplate t
										ON t.nid = m.ntemplateid AND t.nid = '$FILTER_TEMPLATE'";
					}
				}
				$strQuery .=	" WHERE cf.bdeleted = 0";
				if ($FILTER_Name != '')
				{	// extended Filter is active
					$strQuery .= " AND cf.strname LIKE '".$FILTER_Name."%'";
				}
				if ($FILTER_Station)
				{
					$strQuery .= " AND (cp.nuserid = '$FILTER_Station' OR (cp.nuserid = -2 AND cf.nsenderid = $FILTER_Station) )";
				}
				if ($FILTER_Sender != false)
				{	// extended Filter is active
					$strQuery .= " AND cf.nsenderid = '$FILTER_Sender'";
				}
				$strQuery .= "	ORDER BY u.struserid $sortDirection";
				if (!$bFilterOn)
				{
					$strQuery .= " LIMIT $start, $nNumberOfRows";
				}
				break;
			case 'COL_CIRCULATION_MAILLIST':
				$mySortDirection = 'DESC'; //
				if ($sortDirection == 'DESC')
				{
					$mySortDirection = 'ASC'; // upside down
				}
				$strQuery 	= "SELECT m.strname as strmaillistname, cf.*
								FROM cf_circulationform cf
								INNER JOIN cf_mailinglist m
								ON m.nid = cf.nmailinglistid";
				if ($FILTER_Mailinglist)
				{	// extended Filter is active
					$strQuery .= " AND m.strname = '$FILTER_Mailinglist'";
				}
				if ($FILTER_Station || $FILTER_DIP || $FILTER_DATE)
				{
					$strQuery .= "	INNER JOIN cf_circulationprocess cp
									ON cp.ncirculationformid = cf.nid";
				}
				if ($FILTER_Station)
				{
					$strQuery .= " AND (cp.ndecissionstate = '0' OR cp.ndecissionstate = '2' OR cp.ndecissionstate = '16')";
					if ($_REQUEST['bOwnCirculations'])
					{
						$strQuery .= " AND cp.ndecissionstate <> 16 AND cp.ndecissionstate <> 2";
					}
				}
				if ($FILTER_DIP)
				{
					$strQuery .= " AND (cp.ndecissionstate = '0' OR cp.ndecissionstate = '2' OR cp.ndecissionstate = '16')";
					$strQuery .= " AND cp.dateinprocesssince < '$tsDIPMin' AND cp.dateinprocesssince > '$tsDIPMax'";
				}
				if ($FILTER_DATE)
				{
					$strQuery .= " 	INNER JOIN cf_circulationhistory ch
									ON ch.ncirculationformid = cf.nid AND ch.nid = cp.ncirculationhistoryid
									AND ch.datesending > '$tsDateMin' AND ch.datesending < '$tsDateMax'";
				}
				if ($FILTER_TEMPLATE)
				{
					$strQuery .= "	INNER JOIN cf_formtemplate t
									ON t.nid = m.ntemplateid AND t.nid = '$FILTER_TEMPLATE'";
				}
				$strQuery .=	" WHERE cf.bdeleted = 0";
				if ($FILTER_Name != '')
				{	// extended Filter is active
					$strQuery .= " AND cf.strname LIKE '".$FILTER_Name."%'";
				}
				if ($FILTER_Station)
				{
					$strQuery .= " AND cp.nuserid = '$FILTER_Station'";
				}
				if ($FILTER_Sender != false)
				{	// extended Filter is active
					$strQuery .= " AND cf.nsenderid = '$FILTER_Sender'";
				}
				$strQuery .= "	GROUP BY cf.nid
								ORDER BY strmaillistname $sortDirection";
				if (!$bFilterOn)
				{
					$strQuery .= " LIMIT $start, $nNumberOfRows";
				}
				break;
			case 'COL_CIRCULATION_TEMPLATE':
				$strQuery = "SELECT";
				if ($FILTER_DATE || $FILTER_DIP)
				{
					$strQuery .= " DISTINCT";
				}
				$strQuery .= " cf.*
							FROM cf_circulationform cf
							INNER JOIN cf_mailinglist m
							ON cf.nmailinglistid = m.nid";
				if ($FILTER_Mailinglist)
				{	// extended Filter is active
					$strQuery .= " AND m.strname = '$FILTER_Mailinglist'";
				}
				$strQuery .= "	INNER JOIN cf_formtemplate t
								ON t.nid = m.ntemplateid";
				if ($FILTER_TEMPLATE)
				{
					$strQuery .= "	AND t.nid = '$FILTER_TEMPLATE'";
				}
				if ($FILTER_Station || $FILTER_DIP || $FILTER_DATE)
				{
					$strQuery .= "	INNER JOIN cf_circulationprocess cp
									ON cp.ncirculationformid = cf.nid";
				}
				if ($FILTER_Station || $FILTER_DIP)
				{
					$strQuery .= " AND (cp.ndecissionstate = '0' OR cp.ndecissionstate = '2' OR cp.ndecissionstate = '16')";
					if ($_REQUEST['bOwnCirculations'])
					{
						$strQuery .= " AND cp.ndecissionstate <> 16 AND cp.ndecissionstate <> 2";
					}
				}
				if ($FILTER_DIP)
				{
					$strQuery .= " AND cp.dateinprocesssince < '$tsDIPMin' AND cp.dateinprocesssince > '$tsDIPMax'";
				}
				if ($FILTER_DATE)
				{
					$strQuery .= " 	INNER JOIN cf_circulationhistory ch
									ON ch.nid = cp.ncirculationhistoryid";
				}
				if ($FILTER_DATE)
				{
					$strQuery .= " AND ch.datesending > '$tsDateMin' AND ch.datesending < '$tsDateMax'";
				}
				$strQuery .=	" WHERE cf.bdeleted = 0";
				if ($FILTER_Name != '')
				{	// extended Filter is active
					$strQuery .= " AND cf.strname LIKE '".$FILTER_Name."%'";
				}
				if ($FILTER_Station)
				{
					$strQuery .= " AND (cp.nuserid = '$FILTER_Station' OR (cp.nuserid = -2 AND cf.nsenderid = $FILTER_Station) )";
				}
				if ($FILTER_Sender != false)
				{	// extended Filter is active
					$strQuery .= " AND cf.nsenderid = '$FILTER_Sender'";
				}
				$strQuery .= "	ORDER BY t.strname $sortDirection";
				if (!$bFilterOn)
				{
					$strQuery .= " LIMIT $start, $nNumberOfRows";
				}
				break;
			
			case 'COL_CIRCULATION_WHOLETIME':
				$mySortDirection = 'DESC'; //
				if ($sortDirection == 'DESC')
				{
					$mySortDirection = 'ASC'; // upside down
				}
				$strQuery 	= "SELECT";
				if ($FILTER_DATE || $FILTER_TEMPLATE || $FILTER_DIP || $FILTER_Mailinglist || $FILTER_CUSTOM)
				{ 
					$strQuery .= " DISTINCT";
				}
				$strQuery .= " cf.*
								FROM cf_circulationform cf
								INNER JOIN cf_circulationprocess cp
								ON cf.nid = cp.ncirculationformid";
				if ($FILTER_Station || $FILTER_DIP)
				{
					$strQuery .= " AND (cp.ndecissionstate = '0' OR cp.ndecissionstate = '2' OR cp.ndecissionstate = '16')";
					if ($_REQUEST['bOwnCirculations'])
					{
						$strQuery .= " AND cp.ndecissionstate <> 16 AND cp.ndecissionstate <> 2";
					}
				}
				if ($FILTER_DIP)
				{
					$strQuery .= " AND cp.dateinprocesssince < '$tsDIPMin' AND cp.dateinprocesssince > '$tsDIPMax'";
				}
				if ($FILTER_TEMPLATE || $FILTER_Mailinglist)
				{
					$strQuery .= "	INNER JOIN cf_mailinglist m
									ON cf.nmailinglistid = m.nid";
					if ($FILTER_Mailinglist)
					{
						$strQuery .= " AND m.strname = '$FILTER_Mailinglist'";
					}
					if ($FILTER_TEMPLATE)
					{
						$strQuery .= "	INNER JOIN cf_formtemplate t
										ON t.nid = m.ntemplateid AND t.nid = '$FILTER_TEMPLATE'";
					}
				}
				$strQuery .= "	INNER JOIN cf_circulationhistory ch
								ON ch.nid = cp.ncirculationhistoryid AND ch.ncirculationformid = cf.nid";
				if ($FILTER_DATE)
				{
					$strQuery .= " 	AND ch.datesending > '$tsDateMin' AND ch.datesending < '$tsDateMax'";
				}
				if ($FILTER_CUSTOM)
				{
					$strQuery .= " 	INNER JOIN cf_fieldvalue fv
									ON fv.ncirculationhistoryid = ch.nid
									$strFilterCustomQuery";
				}
				$strQuery .=	" WHERE cf.bdeleted = 0";
				if ($FILTER_Name != '')
				{	// extended Filter is active
					$strQuery .= " AND cf.strname LIKE '".$FILTER_Name."%'";
				}
				if ($FILTER_Station)
				{
					$strQuery .= " AND (cp.nuserid = '$FILTER_Station' OR (cp.nuserid = -2 AND cf.nsenderid = $FILTER_Station) )";
				}
				if (($FILTER_Sender != false))
				{	// extended Filter is active
					$strQuery .= " AND cf.nsenderid = '$FILTER_Sender'";
				}
				$strQuery .= "	ORDER BY ch.datesending $mySortDirection";
				if (!$bFilterOn || $FILTER_CUSTOM)
				{
					$strQuery .= " LIMIT $start, $nNumberOfRows";
				}
				break;
		}
		
		$nResult 	= mysql_query($strQuery) or die(mysql_error());
		if ($nResult)
		{
			$nIndex = 0;
			while (	$arrRow = mysql_fetch_array($nResult, MYSQL_ASSOC))
			{
				$arrRows[$nIndex] = $arrRow;					
				$nIndex++;
			}
		}
		
		return $arrRows;
	}
	
	function getCirculationOverview($start, $sortby, $sortDirection, $archivemode, $nShowRows, $bFilterOn, $FILTER_Name = '', $FILTER_Sender = false, $FILTER_Mailinglist = false, $FILTER_Station = false, $FILTER_DAYS_IN_PROGRESS_START = '', $FILTER_DAYS_IN_PROGRESS_END = '', $FILTER_DATE_START = '', $FILTER_DATE_END = '', $FILTER_TEMPLATE = false, $FILTER_CUSTOM = false)
	{
		global $_REQUEST;
		
		if ($FILTER_CUSTOM)
		{
			$strFilterCustomQuery = "";
			
			
			// only filter the first entry
			// TODO
			$arrFilter = $FILTER_CUSTOM[0];
				
			$nInputfieldId 	= $arrFilter['ninputfieldid'];
			$strOperator 	= $arrFilter['stroperator'];
			$strValue		= $arrFilter['strvalue'];
			
			if (($nInputfieldId != '') && ($strOperator != '') && ($strValue != ''))
			{
				$strFilterCustomQuery .= " AND ninputfieldid = '$nInputfieldId' AND strfieldvalue LIKE '%$strValue%'";
			}
		}
		
		if (($FILTER_DATE_START != '') || ($FILTER_DATE_END != ''))
		{
			$FILTER_DATE = 1;
			$arrDateStart 	= explode(".", $FILTER_DATE_START);
			$arrDateEnd 	= explode(".", $FILTER_DATE_END);
			
			$tsDateMin  	= @mktime(0,0,0,$arrDateStart[1],$arrDateStart[0],$arrDateStart[2]);
			if ($FILTER_DATE_START == '')
			{
				$tsDateMin = -9999999999;
			}
			
			$tsDateMax  	= @mktime(0,0,0,$arrDateEnd[1],$arrDateEnd[0],$arrDateEnd[2]);
			if ($FILTER_DATE_END == '')
			{
				$tsDateMax = 9999999999;
			}
		}
		
		$secondsDay		= (24 * 60 * 60);
		
		if (($FILTER_DAYS_IN_PROGRESS_START != '') || ($FILTER_DAYS_IN_PROGRESS_END != ''))
		{
			$FILTER_DIP = 1;
			$givenMin = $FILTER_DAYS_IN_PROGRESS_START;
			$givenMax = $FILTER_DAYS_IN_PROGRESS_END;
			
			$tsDIPMin = time() - ($givenMin * $secondsDay);
			if ($FILTER_DAYS_IN_PROGRESS_START == '')
			{
				$tsDIPMin = 9999999999;
			}
			
			$tsDIPMax = time() - ($givenMax * $secondsDay);
			if ($FILTER_DAYS_IN_PROGRESS_END == '')
			{
				$tsDIPMax = -9999999999;
			}
		}
		
		$nNumberOfRows = $nShowRows;
		$start--;
		if ($FILTER_CUSTOM)
		{
			$start = 0;
			$nNumberOfRows = 50;
		}
		switch($sortby)
		{
			case 'COL_CIRCULATION_NAME':
				$strQuery 	= "		SELECT";
				if ($FILTER_DATE || $FILTER_DIP || $FILTER_CUSTOM)
				{
					$strQuery .= " DISTINCT";
				}
				$strQuery .= "				cf.*
									FROM cf_circulationform cf";
				if ($FILTER_Station || $FILTER_DIP || $FILTER_DATE || $FILTER_CUSTOM)
				{
					$strQuery .= "	INNER JOIN cf_circulationprocess cp
									ON cp.ncirculationformid = cf.nid";
				}
				if ($FILTER_Station || $FILTER_DIP)
				{
					$strQuery .= " AND (cp.ndecissionstate = '0' OR cp.ndecissionstate = '2' OR cp.ndecissionstate = '16')";
					if ($_REQUEST['bOwnCirculations'])
					{
						$strQuery .= " AND cp.ndecissionstate <> 16 AND cp.ndecissionstate <> 2";
					}
				}
				if ($FILTER_DIP)
				{
					$strQuery .= " AND cp.dateinprocesssince < '$tsDIPMin' AND cp.dateinprocesssince > '$tsDIPMax'";
				}
				if ($FILTER_DATE || $FILTER_CUSTOM)
				{
					$strQuery .= " 	INNER JOIN cf_circulationhistory ch
									ON ch.nid = cp.ncirculationhistoryid";
				}
				if ($FILTER_DATE)
				{
					$strQuery .= " AND ch.datesending > '$tsDateMin' AND ch.datesending < '$tsDateMax'";
				}
				if ($FILTER_CUSTOM)
				{
					$strQuery .= " 	INNER JOIN cf_fieldvalue fv
									ON fv.ncirculationhistoryid = ch.nid
									$strFilterCustomQuery";
				}
				if ($FILTER_TEMPLATE || $FILTER_Mailinglist)
				{
					$strQuery .= "	INNER JOIN cf_mailinglist m
									ON cf.nmailinglistid = m.nid";
					if ($FILTER_Mailinglist)
					{
						$strQuery .= " AND m.strname = '$FILTER_Mailinglist'";
					}
					if ($FILTER_TEMPLATE)
					{
						$strQuery .= "	INNER JOIN cf_formtemplate t
										ON t.nid = m.ntemplateid AND t.nid = '$FILTER_TEMPLATE'";
					}
				}
				
				$strQuery .= " WHERE cf.bisarchived = '$archivemode' AND bdeleted = 0";
				if ($FILTER_Name != '')
				{	// extended Filter is active
					$strQuery .= " AND cf.strname LIKE '".$FILTER_Name."%'";
				}
				if ($FILTER_Station)
				{
					$strQuery .= " AND (cp.nuserid = '$FILTER_Station' OR (cp.nuserid = -2 AND cf.nsenderid = $FILTER_Station) )";
				}
				if ($FILTER_Sender != false)
				{	// extended Filter is active
					$strQuery .= " AND cf.nsenderid = '$FILTER_Sender'";
				}
				$strQuery .= " 	ORDER BY cf.strname $sortDirection";
				if (!$bFilterOn || $FILTER_CUSTOM)
				{
					$strQuery .= " LIMIT $start, $nNumberOfRows";
				}
				break;
			case 'COL_CIRCULATION_STATION':
				$strQuery 	= "SELECT DISTINCT cf.*
								FROM cf_circulationform cf
								INNER JOIN cf_circulationprocess cp
								ON cf.nid = cp.ncirculationformid AND (cp.ndecissionstate = '0' OR cp.ndecissionstate = '2' OR cp.ndecissionstate = '16')";
				if ($_REQUEST['bOwnCirculations'])
				{
					$strQuery .= " AND cp.ndecissionstate <> 16 AND cp.ndecissionstate <> 2";
				}
				if ($FILTER_DIP)
				{
					$strQuery .= " AND cp.dateinprocesssince < '$tsDIPMin' AND cp.dateinprocesssince > '$tsDIPMax'";
				}
				if ($FILTER_DATE)
				{
					$strQuery .= " 	INNER JOIN cf_circulationhistory ch
									ON ch.nid = cp.ncirculationhistoryid";
				}
				if ($FILTER_DATE)
				{
					$strQuery .= " AND ch.datesending > '$tsDateMin' AND ch.datesending < '$tsDateMax'";
				}
				$strQuery .= "	INNER JOIN cf_user u
								ON cp.nuserid = u.nid";
				if ($FILTER_TEMPLATE || $FILTER_Mailinglist)
				{
					$strQuery .= "	INNER JOIN cf_mailinglist m
									ON cf.nmailinglistid = m.nid";
					if ($FILTER_Mailinglist)
					{
						$strQuery .= " AND m.strname = '$FILTER_Mailinglist'";
					}
					if ($FILTER_TEMPLATE)
					{
						$strQuery .= "	INNER JOIN cf_formtemplate t
										ON t.nid = m.ntemplateid AND t.nid = '$FILTER_TEMPLATE'";
					}
				}
				$strQuery .= " WHERE cf.bisarchived = '$archivemode' AND cf.bdeleted = 0";
				if ($FILTER_Name != '')
				{	// extended Filter is active
					$strQuery .= " AND cf.strname LIKE '".$FILTER_Name."%'";
				}
				if ($FILTER_Station)
				{
					$strQuery .= " AND (cp.nuserid = '$FILTER_Station' OR (cp.nuserid = -2 AND cf.nsenderid = $FILTER_Station) )";
				}
				if ($FILTER_Sender != false)
				{	// extended Filter is active
					$strQuery .= " AND cf.nsenderid = '$FILTER_Sender'";
				}
				$strQuery .= " 	ORDER BY u.struserid $sortDirection";
				if (!$bFilterOn)
				{
					$strQuery .= " LIMIT $start, $nNumberOfRows";
				}
				break;
			case 'COL_CIRCULATION_PROCESS_DAYS':
				$mySortDirection = 'DESC'; //
				if ($sortDirection == 'DESC')
				{
					$mySortDirection = 'ASC'; // upside down
				}
				
				$strQuery 	= "SELECT MAX(cp.dateinprocesssince) as tsdateinprocesssince, cf.*
								FROM cf_circulationform cf
								INNER JOIN cf_circulationprocess cp
								ON cf.nid = cp.ncirculationformid";
				if ($FILTER_Station || $FILTER_DIP)
				{
					$strQuery .= " AND (cp.ndecissionstate = '0' OR cp.ndecissionstate = '2' OR cp.ndecissionstate = '16')";
					if ($_REQUEST['bOwnCirculations'])
					{
						$strQuery .= " AND cp.ndecissionstate <> 16 AND cp.ndecissionstate <> 2";
					}
				}
				if ($FILTER_DIP)
				{
					$strQuery .= " AND cp.dateinprocesssince < '$tsDIPMin' AND cp.dateinprocesssince > '$tsDIPMax'";
				}
				if ($FILTER_DATE || $FILTER_CUSTOM)
				{
					$strQuery .= " 	INNER JOIN cf_circulationhistory ch
									ON ch.nid = cp.ncirculationhistoryid";
				}
				if ($FILTER_DATE)
				{
					$strQuery .= " AND ch.datesending > '$tsDateMin' AND ch.datesending < '$tsDateMax'";
				}
				if ($FILTER_CUSTOM)
				{
					$strQuery .= " 	INNER JOIN cf_fieldvalue fv
									ON fv.ncirculationhistoryid = ch.nid
									$strFilterCustomQuery";
				}
				if ($FILTER_TEMPLATE || $FILTER_Mailinglist)
				{
					$strQuery .= "	INNER JOIN cf_mailinglist m
									ON cf.nmailinglistid = m.nid";
					if ($FILTER_Mailinglist)
					{
						$strQuery .= " AND m.strname = '$FILTER_Mailinglist'";
					}
					if ($FILTER_TEMPLATE)
					{
						$strQuery .= "	INNER JOIN cf_formtemplate t
										ON t.nid = m.ntemplateid AND t.nid = '$FILTER_TEMPLATE'";
					}
				}
				$strQuery .=	" WHERE cf.bisarchived = '$archivemode' AND cf.bdeleted = 0";
				if ($FILTER_Name != '')
				{	// extended Filter is active
					$strQuery .= " AND cf.strname LIKE '".$FILTER_Name."%'";
				}
				if ($FILTER_Station)
				{
					$strQuery .= " AND (cp.nuserid = '$FILTER_Station' OR (cp.nuserid = -2 AND cf.nsenderid = $FILTER_Station) )";
				}
				if ($FILTER_Sender != false)
				{	// extended Filter is active
					$strQuery .= " AND cf.nsenderid = '$FILTER_Sender'";
				}
				$strQuery .= "	GROUP BY cf.nid
								ORDER BY tsdateinprocesssince $mySortDirection";
				if (!$bFilterOn || $FILTER_CUSTOM)
				{
					$strQuery .= " LIMIT $start, $nNumberOfRows";
				}
				break;
			case 'COL_CIRCULATION_PROCESS_START':
				$strQuery 	= "SELECT DISTINCT";
				//if ($FILTER_DATE || $FILTER_TEMPLATE|| $FILTER_DIP || $FILTER_Mailinglist || $FILTER_CUSTOM)
				//{
				//	$strQuery .= " DISTINCT";
				//}
				$strQuery .= " 		cf.*
								FROM cf_circulationform cf
								INNER JOIN cf_circulationprocess cp
								ON cf.nid = cp.ncirculationformid";
				if ($FILTER_Station || $FILTER_DIP)
				{
					$strQuery .= " AND (cp.ndecissionstate = '0' OR cp.ndecissionstate = '2' OR cp.ndecissionstate = '16')";
					if ($_REQUEST['bOwnCirculations'])
					{
						$strQuery .= " AND cp.ndecissionstate <> 16 AND cp.ndecissionstate <> 2";
					}
				}
				if ($FILTER_DIP)
				{
					$strQuery .= " AND cp.dateinprocesssince < '$tsDIPMin' AND cp.dateinprocesssince > '$tsDIPMax'";
				}
				$strQuery .= "	INNER JOIN cf_circulationhistory ch
								ON ch.nid = cp.ncirculationhistoryid AND ch.ncirculationformid = cf.nid";
				if ($FILTER_DATE)
				{
					$strQuery .= " 	AND ch.datesending > '$tsDateMin' AND ch.datesending < '$tsDateMax'";
				}
				if ($FILTER_CUSTOM)
				{
					$strQuery .= " 	INNER JOIN cf_fieldvalue fv
									ON fv.ncirculationhistoryid = ch.nid
									$strFilterCustomQuery";
				}
				if ($FILTER_TEMPLATE || $FILTER_Mailinglist)
				{
					$strQuery .= "	INNER JOIN cf_mailinglist m
									ON cf.nmailinglistid = m.nid";
					if ($FILTER_Mailinglist)
					{
						$strQuery .= " AND m.strname = '$FILTER_Mailinglist'";
					}
					if ($FILTER_TEMPLATE)
					{
						$strQuery .= "	INNER JOIN cf_formtemplate t
										ON t.nid = m.ntemplateid AND t.nid = '$FILTER_TEMPLATE'";
					}
				}
				$strQuery .=	" WHERE cf.bisarchived = '$archivemode' AND cf.bdeleted = 0";
				if ($FILTER_Name != '')
				{	// extended Filter is active
					$strQuery .= " AND cf.strname LIKE '".$FILTER_Name."%'";
				}
				if ($FILTER_Station)
				{
					$strQuery .= " AND cp.nuserid = '$FILTER_Station'";
				}
				if ($FILTER_Sender != false)
				{	// extended Filter is active
					$strQuery .= " AND cf.nsenderid = '$FILTER_Sender'";
				}
				$strQuery .= " 	ORDER BY ch.datesending $sortDirection";
				if (!$bFilterOn || $FILTER_CUSTOM)
				{
					$strQuery .= " LIMIT $start, $nNumberOfRows";
				}
				break;
			case 'COL_CIRCULATION_SENDER':
				$strQuery	= "SELECT";
				if ($FILTER_DATE || $FILTER_DIP)
				{
					$strQuery .= " DISTINCT";
				}
				$strQuery .= "	cf.*
								FROM cf_circulationform cf
								INNER JOIN cf_user u
								ON cf.nsenderid = u.nid";
				if ($FILTER_Station || $FILTER_DIP || $FILTER_DATE)
				{
					$strQuery .= "	INNER JOIN cf_circulationprocess cp
									ON cp.ncirculationformid = cf.nid";
				}
				if ($FILTER_Station || $FILTER_DIP)
				{
					$strQuery .= " AND (cp.ndecissionstate = '0' OR cp.ndecissionstate = '2' OR cp.ndecissionstate = '16')";
					if ($_REQUEST['bOwnCirculations'])
					{
						$strQuery .= " AND cp.ndecissionstate <> 16 AND cp.ndecissionstate <> 2";
					}
				}
				if ($FILTER_DIP)
				{
					$strQuery .= " AND cp.dateinprocesssince < '$tsDIPMin' AND cp.dateinprocesssince > '$tsDIPMax'";
				}
				if ($FILTER_DATE)
				{
					$strQuery .= " 	INNER JOIN cf_circulationhistory ch
									ON ch.nid = cp.ncirculationhistoryid";
				}
				if ($FILTER_DATE)
				{
					$strQuery .= " AND ch.datesending > '$tsDateMin' AND ch.datesending < '$tsDateMax'";
				}
				if ($FILTER_TEMPLATE || $FILTER_Mailinglist)
				{
					$strQuery .= "	INNER JOIN cf_mailinglist m
									ON cf.nmailinglistid = m.nid";
					if ($FILTER_Mailinglist)
					{
						$strQuery .= " AND m.strname = '$FILTER_Mailinglist'";
					}
					if ($FILTER_TEMPLATE)
					{
						$strQuery .= "	INNER JOIN cf_formtemplate t
										ON t.nid = m.ntemplateid AND t.nid = '$FILTER_TEMPLATE'";
					}
				}
				$strQuery .=	" WHERE cf.bisarchived = '$archivemode' AND cf.bdeleted = 0";
				if ($FILTER_Name != '')
				{	// extended Filter is active
					$strQuery .= " AND cf.strname LIKE '".$FILTER_Name."%'";
				}
				if ($FILTER_Station)
				{
					$strQuery .= " AND (cp.nuserid = '$FILTER_Station' OR (cp.nuserid = -2 AND cf.nsenderid = $FILTER_Station) )";
				}
				if ($FILTER_Sender != false)
				{	// extended Filter is active
					$strQuery .= " AND cf.nsenderid = '$FILTER_Sender'";
				}
				$strQuery .= "	ORDER BY u.struserid $sortDirection";
				if (!$bFilterOn)
				{
					$strQuery .= " LIMIT $start, $nNumberOfRows";
				}
				break;
			case 'COL_CIRCULATION_MAILLIST':
				$mySortDirection = 'DESC'; //
				if ($sortDirection == 'DESC')
				{
					$mySortDirection = 'ASC'; // upside down
				}
				$strQuery 	= "SELECT m.strname as strmaillistname, cf.*
								FROM cf_circulationform cf
								INNER JOIN cf_mailinglist m
								ON m.nid = cf.nmailinglistid";
				if ($FILTER_Mailinglist)
				{	// extended Filter is active
					$strQuery .= " AND m.strname = '$FILTER_Mailinglist'";
				}
				if ($FILTER_Station || $FILTER_DIP || $FILTER_DATE)
				{
					$strQuery .= "	INNER JOIN cf_circulationprocess cp
									ON cp.ncirculationformid = cf.nid";
				}
				if ($FILTER_Station)
				{
					$strQuery .= " AND (cp.ndecissionstate = '0' OR cp.ndecissionstate = '2' OR cp.ndecissionstate = '16')";
					if ($_REQUEST['bOwnCirculations'])
					{
						$strQuery .= " AND cp.ndecissionstate <> 16 AND cp.ndecissionstate <> 2";
					}
				}
				if ($FILTER_DIP)
				{
					$strQuery .= " AND (cp.ndecissionstate = '0' OR cp.ndecissionstate = '2' OR cp.ndecissionstate = '16')";
					$strQuery .= " AND cp.dateinprocesssince < '$tsDIPMin' AND cp.dateinprocesssince > '$tsDIPMax'";
				}
				if ($FILTER_DATE)
				{
					$strQuery .= " 	INNER JOIN cf_circulationhistory ch
									ON ch.ncirculationformid = cf.nid AND ch.nid = cp.ncirculationhistoryid
									AND ch.datesending > '$tsDateMin' AND ch.datesending < '$tsDateMax'";
				}
				if ($FILTER_TEMPLATE)
				{
					$strQuery .= "	INNER JOIN cf_formtemplate t
									ON t.nid = m.ntemplateid AND t.nid = '$FILTER_TEMPLATE'";
				}
				$strQuery .=	" WHERE cf.bisarchived = '$archivemode' AND cf.bdeleted = 0";
				if ($FILTER_Name != '')
				{	// extended Filter is active
					$strQuery .= " AND cf.strname LIKE '".$FILTER_Name."%'";
				}
				if ($FILTER_Station)
				{
					$strQuery .= " AND cp.nuserid = '$FILTER_Station'";
				}
				if ($FILTER_Sender != false)
				{	// extended Filter is active
					$strQuery .= " AND cf.nsenderid = '$FILTER_Sender'";
				}
				$strQuery .= "	GROUP BY cf.nid
								ORDER BY strmaillistname $sortDirection";
				if (!$bFilterOn)
				{
					$strQuery .= " LIMIT $start, $nNumberOfRows";
				}
				break;
			case 'COL_CIRCULATION_TEMPLATE':
				$strQuery = "SELECT";
				if ($FILTER_DATE || $FILTER_DIP)
				{
					$strQuery .= " DISTINCT";
				}
				$strQuery .= " cf.*
							FROM cf_circulationform cf
							INNER JOIN cf_mailinglist m
							ON cf.nmailinglistid = m.nid";
				if ($FILTER_Mailinglist)
				{	// extended Filter is active
					$strQuery .= " AND m.strname = '$FILTER_Mailinglist'";
				}
				$strQuery .= "	INNER JOIN cf_formtemplate t
								ON t.nid = m.ntemplateid";
				if ($FILTER_TEMPLATE)
				{
					$strQuery .= "	AND t.nid = '$FILTER_TEMPLATE'";
				}
				if ($FILTER_Station || $FILTER_DIP || $FILTER_DATE)
				{
					$strQuery .= "	INNER JOIN cf_circulationprocess cp
									ON cp.ncirculationformid = cf.nid";
				}
				if ($FILTER_Station || $FILTER_DIP)
				{
					$strQuery .= " AND (cp.ndecissionstate = '0' OR cp.ndecissionstate = '2' OR cp.ndecissionstate = '16')";
					if ($_REQUEST['bOwnCirculations'])
					{
						$strQuery .= " AND cp.ndecissionstate <> 16 AND cp.ndecissionstate <> 2";
					}
				}
				if ($FILTER_DIP)
				{
					$strQuery .= " AND cp.dateinprocesssince < '$tsDIPMin' AND cp.dateinprocesssince > '$tsDIPMax'";
				}
				if ($FILTER_DATE)
				{
					$strQuery .= " 	INNER JOIN cf_circulationhistory ch
									ON ch.nid = cp.ncirculationhistoryid";
				}
				if ($FILTER_DATE)
				{
					$strQuery .= " AND ch.datesending > '$tsDateMin' AND ch.datesending < '$tsDateMax'";
				}
				$strQuery .=	" WHERE cf.bisarchived = '$archivemode' AND cf.bdeleted = 0";
				if ($FILTER_Name != '')
				{	// extended Filter is active
					$strQuery .= " AND cf.strname LIKE '".$FILTER_Name."%'";
				}
				if ($FILTER_Station)
				{
					$strQuery .= " AND (cp.nuserid = '$FILTER_Station' OR (cp.nuserid = -2 AND cf.nsenderid = $FILTER_Station) )";
				}
				if ($FILTER_Sender != false)
				{	// extended Filter is active
					$strQuery .= " AND cf.nsenderid = '$FILTER_Sender'";
				}
				$strQuery .= "	ORDER BY t.strname $sortDirection";
				if (!$bFilterOn)
				{
					$strQuery .= " LIMIT $start, $nNumberOfRows";
				}
				break;
			
			case 'COL_CIRCULATION_WHOLETIME':
				$mySortDirection = 'DESC'; //
				if ($sortDirection == 'DESC')
				{
					$mySortDirection = 'ASC'; // upside down
				}
				$strQuery 	= "SELECT";
				if ($FILTER_DATE || $FILTER_TEMPLATE || $FILTER_DIP || $FILTER_Mailinglist || $FILTER_CUSTOM)
				{ 
					$strQuery .= " DISTINCT";
				}
				$strQuery .= " cf.*
								FROM cf_circulationform cf
								INNER JOIN cf_circulationprocess cp
								ON cf.nid = cp.ncirculationformid";
				if ($FILTER_Station || $FILTER_DIP)
				{
					$strQuery .= " AND (cp.ndecissionstate = '0' OR cp.ndecissionstate = '2' OR cp.ndecissionstate = '16')";
					if ($_REQUEST['bOwnCirculations'])
					{
						$strQuery .= " AND cp.ndecissionstate <> 16 AND cp.ndecissionstate <> 2";
					}
				}
				if ($FILTER_DIP)
				{
					$strQuery .= " AND cp.dateinprocesssince < '$tsDIPMin' AND cp.dateinprocesssince > '$tsDIPMax'";
				}
				if ($FILTER_TEMPLATE || $FILTER_Mailinglist)
				{
					$strQuery .= "	INNER JOIN cf_mailinglist m
									ON cf.nmailinglistid = m.nid";
					if ($FILTER_Mailinglist)
					{
						$strQuery .= " AND m.strname = '$FILTER_Mailinglist'";
					}
					if ($FILTER_TEMPLATE)
					{
						$strQuery .= "	INNER JOIN cf_formtemplate t
										ON t.nid = m.ntemplateid AND t.nid = '$FILTER_TEMPLATE'";
					}
				}
				$strQuery .= "	INNER JOIN cf_circulationhistory ch
								ON ch.nid = cp.ncirculationhistoryid AND ch.ncirculationformid = cf.nid";
				if ($FILTER_DATE)
				{
					$strQuery .= " 	AND ch.datesending > '$tsDateMin' AND ch.datesending < '$tsDateMax'";
				}
				if ($FILTER_CUSTOM)
				{
					$strQuery .= " 	INNER JOIN cf_fieldvalue fv
									ON fv.ncirculationhistoryid = ch.nid
									$strFilterCustomQuery";
				}
				$strQuery .=	" WHERE cf.bisarchived = '$archivemode' AND cf.bdeleted = 0";
				if ($FILTER_Name != '')
				{	// extended Filter is active
					$strQuery .= " AND cf.strname LIKE '".$FILTER_Name."%'";
				}
				if ($FILTER_Station)
				{
					$strQuery .= " AND (cp.nuserid = '$FILTER_Station' OR (cp.nuserid = -2 AND cf.nsenderid = $FILTER_Station) )";
				}
				if (($FILTER_Sender != false))
				{	// extended Filter is active
					$strQuery .= " AND cf.nsenderid = '$FILTER_Sender'";
				}
				$strQuery .= "	ORDER BY ch.datesending $mySortDirection";
				if (!$bFilterOn || $FILTER_CUSTOM)
				{
					$strQuery .= " LIMIT $start, $nNumberOfRows";
				}
				break;
		}
		
		$nResult 	= mysql_query($strQuery) or die(mysql_error());
		if ($nResult)
		{
			$nIndex = 0;
			while (	$arrRow = mysql_fetch_array($nResult, MYSQL_ASSOC))
			{
				$arrRows[$nIndex] = $arrRow;					
				$nIndex++;
			}
		}
		
		return $arrRows;
	}
	
	function cmpCirculations($arr1, $arr2)
	{
		return @strcmp($arr1['strcurstation'], $arr2['strcurstation']);
	}
	
	/**
	*	@param $nFormId
	*	@return the Circ History
	*/
	function getMaxHistoryData($nFormId)
	    {
	    	$arrResult = array();
	    	
	        $query = "SELECT MAX(nid) FROM `cf_circulationhistory` WHERE `ncirculationformid`=".$nFormId;
	        $nResult = mysql_query($query);
	
	        if ($nResult)
	        {
	            if (mysql_num_rows($nResult) > 0)
	            {
	                $arrRow = mysql_fetch_array($nResult);
	                
	                if ($arrRow)
	                {
	                    return $arrRow[0];
	                }           
	            }   
	        }
	    }
		function getMaxProcessId($nHistoryId)
	    {
	        $query = "SELECT MIN(ndecissionstate) FROM `cf_circulationprocess` WHERE `ncirculationhistoryid`=".$nHistoryId;
	        $nResult = mysql_query($query);
	
	        if ($nResult)
	        {
	            if (mysql_num_rows($nResult) > 0)
	            {
	                $arrRow = mysql_fetch_array($nResult);
	                
	                if ($arrRow)
	                {
	                    $nMaxId = $arrRow[0];
	                    return $nMaxId;
	                }           
	            }   
	        }
	    }
		function getProcessInformation($nMaxId)
	    {
	        $query = "SELECT * FROM `cf_circulationprocess` WHERE `nid`=".$nMaxId;
	        $nResult = mysql_query($query);
	
	        if ($nResult)
	        {
	            if (mysql_num_rows($nResult) > 0)
	            {
	                $arrRow = mysql_fetch_array($nResult);
	                
	                if ($arrRow)
	                {
	                    return $arrRow;
	                }           
	            }   
	        }        
	    }
	
	/**
	*	@param $nCirculationFormID
	*	@return the decissionstate
	*/
	function getDecissionState($nCirculationFormID)
	{		
		$arrHistoryData 		= $this->getMaxHistoryData($nCirculationFormID);
		$nMaxId 				= $this->getMaxProcessId($arrHistoryData);
        $arrProcessInformation 	= $this->getProcessInformation($nMaxId);
        
        if (($arrProcessInformation["ndecissionstate"] == 0) || ($arrProcessInformation["ndecissionstate"] == 8) )
		{
			$tsDateInProcessSince = $arrProcessInformation["dateinprocesssince"];
			$tsNow = time();
			$diff = (int)((($tsNow-$tsDateInProcessSince))/(24*60*60));
		}
		else
		{
			$diff = '-';
		}
        			
		$arrResults['ndecissionstate']	= $arrProcessInformation["ndecissionstate"];
		$arrResults['ndaysinprogress']	= $diff;
		if ($arrProcessInformation["nuserid"] != -2)
		{
			$arrResults['strcurstation']	= $this->getUsername($arrProcessInformation["nuserid"]);
			$arrResults['ncurstationid']	= $arrProcessInformation["nuserid"];
		}
		else
		{
			$strQuery = "SELECT nsenderid FROM cf_circulationform WHERE nid = '".$arrProcessInformation['ncirculationformid']."' LIMIT 1;";
			$nResult = mysql_query($strQuery);
			$arrResult = mysql_fetch_array($nResult, MYSQL_ASSOC);
			
			$nCurUserId = $arrResult['nsenderid'];
			
			$arrResults['strcurstation']	= $this->getUsername($nCurUserId);
			$arrResults['ncurstationid']	= $nCurUserId;
		}
			
		return $arrResults;
	}
	
	/**
	*	@param $nCirculationFormID
	*	@return the starting date
	*/
	function getStartDate($nCirculationFormID)
	{
		$strQuery = 	"SELECT MAX(datesending) as 'tsdatesending'
						FROM cf_circulationhistory
						WHERE ncirculationformid = '$nCirculationFormID'
						GROUP BY ncirculationformid;";
									
						
		$nResult = mysql_query($strQuery);
		
		if ($nResult)
		{
			$nIndex = 0;
			while (	$arrRow = mysql_fetch_array($nResult, MYSQL_ASSOC))
			{
				$arrRows[$nIndex] = $arrRow;					
				$nIndex++;
			}
			
				
			
			return date("d-m-Y", $arrRows[0]['tsdatesending']);
		}
	}
	
	/**
	*	@param $nCirculationFormID
	*	@return the sender
	*/
	function getSender($nCirculationFormID)
	{
		$strQuery	= "SELECT u.struserid as 'strsender'
						FROM cf_circulationform c
						INNER JOIN cf_user u
						ON c.nsenderid = u.nid
						WHERE c.nid = '$nCirculationFormID'
						LIMIT 1;";
		$nResult = mysql_query($strQuery);
		if ($nResult)
		{
			$nIndex = 0;
			while (	$arrRow = mysql_fetch_array($nResult, MYSQL_ASSOC))
			{
				$arrRows[$nIndex] = $arrRow;					
				$nIndex++;
			}
			
			return $arrRows[0]['strsender'];
		}
	}
	
	function getSenderDetails($nCirculationFormID)
	{
		$strQuery	= "SELECT u.*
						FROM cf_circulationform c
						INNER JOIN cf_user u
						ON c.nsenderid = u.nid
						WHERE c.nid = '$nCirculationFormID'
						LIMIT 1;";
		$nResult = mysql_query($strQuery);
		if ($nResult)
		{
			$nIndex = 0;
			while (	$arrRow = mysql_fetch_array($nResult, MYSQL_ASSOC))
			{
				$arrRows[$nIndex] = $arrRow;					
				$nIndex++;
			}
			
			return $arrRows[0];
		}
	}
	
	/**
	*	@param $nMyCirculationFormID, $nMyMailinglistID
	*	@return the width
	*/
	function getWidth($nMyCirculationFormID, $nMyMailinglistID)
	{		
		$width = 0;
		
		$strQuery = "SELECT * FROM cf_circulationprocess WHERE ncirculationformid = ".$nMyCirculationFormID;
		$nResult = mysql_query($strQuery);
		$totalsize = mysql_num_rows($nResult);
		
		$strQuery = "SELECT * FROM cf_circulationprocess WHERE ncirculationformid = ".$nMyCirculationFormID." AND ndecissionstate <> 0";
		$nResult = mysql_query($strQuery);
		$completesize = mysql_num_rows($nResult);
		
		$width = ($completesize/$totalsize) * 100;
		
		$width = round($width,0);
	
		if (($width > 100) || ($width < 0))
		{
			$width = -1;
		}
		
		$arrProgressbar['width'] = $width;
		
		if($width < 33)
		{
			$arrProgressbar['color'] = '#ef0000';
		}
		else
		{
			if($width < 66)
			{
				$arrProgressbar['color'] = '#ffa000';
			}
			else
			{
				$arrProgressbar['color'] = '#00ef00';
			}
		}
		
		return $arrProgressbar;
	}
	
	/**
	 * @param $archivemode
	 * @return amount of circulations
	 */
	function getAmountOfAllCirculations($archivemode)
	{
		$strQuery = "SELECT COUNT(nid) as 'nmyresult' FROM cf_circulationform WHERE bisarchived = '$archivemode' AND bdeleted = 0;";
		$nResult = mysql_query($strQuery);
		
		if ($nResult)
		{
			while (	$arrRow = mysql_fetch_array($nResult, MYSQL_ASSOC))
			{
				$arrRows = $arrRow;
			}
			return $arrRows['nmyresult'];
		}
	}
	
	
	function getUserById($user_id = false)
	{
		if ($user_id)
		{
			$strQuery 	= "SELECT * FROM cf_user WHERE nid = '$user_id' LIMIT 1;";
			$nResult 	= mysql_query($strQuery);
			$arrResult	= mysql_fetch_array($nResult, MYSQL_ASSOC);
			
			return $arrResult;
		}
	}
	
	function filterUsers($filter = false)
	{
		if ($filter != 'false')
		{
			$arrSplit = explode(" ", $filter);
			
			$strQuery = "SELECT user_id FROM cf_user_index";
			
			
			if ($arrSplit[0] != '')
			{
				$strQuery .= " WHERE";
			}
			
			$nMax = sizeof($arrSplit);
			for ($nIndex = 0; $nIndex < $nMax; $nIndex++)
			{
				$strSplit = $arrSplit[$nIndex];
				
				if ($strSplit != '')
				{
					if ($nIndex > 0) $strQuery .= " AND";
					$strQuery .= " `indexe` LIKE '%".$strSplit."%'";
				}
			}
			
			$strQuery .= " ORDER BY `indexe` ASC";
			$nResult 	= mysql_query($strQuery) or die (mysql_error());
			
			if ($nResult)
			{
				while($arrRow = mysql_fetch_array($nResult, MYSQL_ASSOC))
				{
					$arrResult[] =  $arrRow;
				}
				return $arrResult;
			}
		}
	}
	
	/**
	 * returns the substitutes of a user
	 *
	 * @param Integer $user_id
	 * @return Array $result
	 */
	function getSubstitutes($user_id = false)
	{
		if ($user_id != 'false')
		{
			$strQuery 	= "SELECT substitute_id FROM cf_substitute WHERE user_id = '$user_id' ORDER BY position ASC";
			$nResult 	= mysql_query($strQuery) or die (mysql_error());
			
			if ($nResult)
			{
				while($arrRow = mysql_fetch_array($nResult, MYSQL_ASSOC))
				{
					$arrResult[] =  $arrRow;
				}
				return $arrResult;
			}
		}
		return false;
	}
	
	/**
	 * returns an array containing the Extension Objects with the Extension details
	 * returns only the Extensions of the corresponding hookId
	 *
	 * @param String $hookId
	 * @return Array
	 */
	function getExtensionsByHookId($hookId = false)
	{
		if ($hookId)
		{
			// first scan the directory for extensions
			$direcories		= array();
			$extensions		= array();
			$addedIndex		= 0;
			$rootPath 		= '/workflow/extensions/';
			$definitionFile = 'extdef.xml';
			
			/*$directory	= opendir($rootPath);
			while (false !== ($file = readdir ($directory)))
			{
				if (($file != '.') && ($file != '..') && ($file != '.svn'))
				{
					$direcories[] = $file;
				}
			}
			closedir($directory);*/
			
			// go on if there are extensions
			if (sizeof($direcories))
			{
				// read the xml definition files
				$max = sizeof($direcories);
				for ($index = 0; $index < $max; $index++)
				{
					$directory 		= $direcories[$index].'/';
					$curFilename	= $rootPath.$directory.$definitionFile;
					
					$Xml = @simplexml_load_file($curFilename);
					if ($Xml)
					{
						$isActive	= $Xml->attributes()->isActive;
						$curHookId	= $Xml->hook->attributes()->id;
						
						if (($hookId == $curHookId) && ($isActive == 'true'))
						{	// save the extension details to the array only if it's hook matches and it's active
							if ($hookId != 'CF_ENDACTION')
							{
								$extensions[$addedIndex]['path'] 		= $rootPath.$directory;
								$extensions[$addedIndex]['Extension'] 	= $Xml;
								$addedIndex++;
							}
							else
							{	
								$params	= $Xml->hook->param;
								foreach ($params as $param)
								{
									$name	= (String) $param->attributes()->name;
									$value	= (String) $param->attributes()->value;
									$hookParams[$name] = $value;
								}
								
								$extensions[$hookParams['position']]['path'] 		= $rootPath.$directory;
								$extensions[$hookParams['position']]['Extension'] 	= $Xml;
							}
						}
					}
				}
				if ($hookId == 'CF_ENDACTION') ksort($extensions);
				
				if ($extensions) return $extensions;
			}
		}
		return false;
	}
	
	function getMenuGroupExtensions($menuGroup = false, $extensions = false)
	{
		if ($menuGroup && $extensions)
		{
			$menuGroupExtensions 	= array();
			$addedIndex 			= 0;
			
			$max = sizeof($extensions);
			for ($index = 0; $index < $max; $index++)
			{
				$Extension 	= $extensions[$index]['Extension'];
				$hooks 		= $Extension->hook;
				$addGroup 	= false;
				
				$max2 = sizeof($hooks);
				for ($index2 = 0; $index2 < $max2; $index2++)
				{
					$hook 			= $hooks[$index2];
					$curMenuGroup	= $hook->group;
					$splitCurGroup	= split('_', $curMenuGroup);
					
					if ($menuGroup == $curMenuGroup)
					{
						$addGroup = true;
					}
					elseif(($splitCurGroup[0] != 'CF') && ($menuGroup == 'CF_GROUP_USERDEFINED'))
					{	// in this case it's a userdefined menugroup 
						$addGroup = true;
					}
				}
				
				if ($addGroup)
				{
					$menuGroupExtensions[$addedIndex] = $extensions[$index];
					$addedIndex++;
				}
			}
			if ($menuGroupExtensions) return $menuGroupExtensions;
		}
		return false;
	}
	
	function getExtensionParams($hook = false)
	{
		global $_REQUEST;
		
		if ($hook)
		{
			$params = $hook->param;
			
			$max2 = sizeof($params);
			for ($index2 = 0; $index2 < $max2; $index2++)
			{
				$param 	= $params[$index2];
				$name 	= $param->attributes()->name;
				$value 	= $param->attributes()->value;
				
				if ($index2 < 1) $destinationParams .= '?'.$name.'='.$value;
				if ($index2 > 0) $destinationParams .= '&'.$name.'='.$value;
			}
			
			if ($destinationParams != '')
			{
				// replace the Placeholders
				$destinationParams = str_replace('CF_LANGUAGE', $_REQUEST['language'], $destinationParams);
				
				return $destinationParams;
			}
		}
		return false;
	}
	
	function getEndActionParams($endAction = false)
	{
		if ($endAction)
		{	
			$params	= $endAction['Extension']->hook->param;
			$path	= $endAction['path'];
			
			foreach ($params as $param)
			{
				$name	= (String) $param->attributes()->name;
				$value	= (String) $param->attributes()->value;
				
				if ($name == 'filename') $value = $path.$value;
				$hookParams[$name] = $value;
			}
			
			if ($hookParams) return $hookParams;
		}
	}
	
	function getEndActions($endActions = false)
	{
		if ($endActions)
		{
			$runningIndex	= 0;
			foreach ($endActions as $endAction)
			{
				$curEndAction	= $endAction['Extension'];				
				$path			= $endAction['path'];
				$hook			= $curEndAction->hook;
				$params			= $hook->param;
				
				foreach ($params as $param)
				{
					$name	= (String) $param->attributes()->name;
					$value	= (String) $param->attributes()->value;
					
					$curEndActions[$runningIndex][$name] = $path.$value;
				}
				
				$runningIndex++;
			}
			if ($curEndActions) return $curEndActions;
		}
	}
	
	function getUserdefinedGroups($extensions = false)
	{
		if ($extensions)
		{
			$addedIndex 		= 0;
			$userDefinedGroups	= array();
			$existingGroups 	= array();
			
			$max = sizeof($extensions);
			for ($index = 0; $index < $max; $index++)
			{
				$Extension	= $extensions[$index]['Extension'];
				$hooks 		= $Extension->hook;
				
				$max2 = sizeof($hooks);
				for ($index2 = 0; $index2 < $max2; $index2++)
				{
					$hook 		= $hooks[$index2];
					$curGroup	= ucwords($hook->group);
					$splitGroup	= split('_', $curGroup);
					
					if ($splitGroup[0] != 'CF')
					{
						if ($existingGroups[$curGroup] == '')
						{
							$userDefinedGroups[$addedIndex] = $curGroup;
							$existingGroups[$curGroup]		= 'false';
							$addedIndex++;
						}
					}
				}
			}
			
			return $userDefinedGroups;
		}
	}
	
}	
?>
