<?php
	$prefix_folder = dirname(__FILE__)."/../../../..";
	require_once $prefix_folder."/lib/vendor/cp_workflow/config/config.inc.php";
	require_once $prefix_folder."/lib/vendor/cp_workflow/pages/version.inc.php";
	require_once $prefix_folder."/lib/vendor/cp_workflow/language_files/language.inc.php";
	require_once $prefix_folder.'/lib/vendor/cp_workflow/config/db_config.inc.php';
	
	
	
	function getNextUserInList($nCurUserId, $nMailingListId, $nSlotId)
	{
	    $prefix_folder = dirname(__FILE__)."/../../../..";
	    require $prefix_folder.'/lib/vendor/cp_workflow/config/db_config.inc.php';
	
		
		$arrUserInfo = array();
		
		$nConnection = mysql_connect($DATABASE_HOST, $DATABASE_UID, $DATABASE_PWD);
		$nConnection2 = mysql_connect($DATABASE_HOST, $DATABASE_UID, $DATABASE_PWD);
		
		if ( ($nConnection) && ($nConnection2) ) 
		{
			if (mysql_select_db($DATABASE_DB, $nConnection))
			{
				mysql_select_db($DATABASE_DB, $nConnection2);
				
				$query = "SELECT * FROM cf_formslot WHERE nid={$nSlotId}";
				$result = mysql_query($query, $nConnection);
				if ($result) {
					$arrSlotInfo = mysql_fetch_array($result);
				}
				
				$strQuery = "SELECT * FROM cf_slottouser INNER JOIN cf_formslot ON cf_slottouser.nslotid  = cf_formslot.nid WHERE cf_slottouser.nmailinglistid=$nMailingListId ORDER BY cf_formslot.nslotnumber ASC, cf_slottouser.nposition ASC";
				$nResult = mysql_query($strQuery, $nConnection);
				
        		if ($nResult)
        		{
        			if (mysql_num_rows($nResult) > 0)
        			{
						$bFoundOne == false;
        				while (	$arrRow = mysql_fetch_array($nResult))
        				{
        					if ($nCurUserId == -1)
							{
								//--- lets take the first user
								$arrUserInfo[0] = $arrRow["nuserid"];
								$arrUserInfo[1] = $arrRow["nslotid"];
								
								return $arrUserInfo;
							}
							else if ($bFoundOne == true)
							{
								$arrUserInfo[0] = $arrRow["nuserid"];
								$arrUserInfo[1] = $arrRow["nslotid"];
								
								// Slot has changed
								$arrUserInfo[2] = $nSlotId != $arrRow['nslotid'] ? $arrRow['nslotid'] : false; 
								
								return $arrUserInfo;
							}
							else
							{
								if ( ($arrRow["nuserid"] == $nCurUserId) && 
										($arrRow["nslotid"] == $nSlotId))
								{
									$bFoundOne = true; //--- next loop returns user	
								}
							}
						}
					}
				}
			}
		}
		
		return $arrUserInfo;
	}

	function sendToUser($nUserId, $nCirculationId, $nSlotId, $nCirculationProcessId, $nCirculationHistoryId, $tsDateInProcessSince = '')
	{
		global  $MAIL_HEADER_PRE, $CUTEFLOW_SERVER;
		global $SMTP_SERVER, $SMTP_PORT, $SMTP_USERID, $SMTP_PWD, $SMTP_USE_AUTH;
		global $SYSTEM_REPLY_ADDRESS, $CUTEFLOW_VERSION, $TStoday, $objURL, $EMAIL_FORMAT, $EMAIL_VALUES, $MAIL_SEND_TYPE, $MTA_PATH, $SMPT_ENCRYPTION;
		
		global $CUTEFLOW_SERVER, $CUTEFLOW_VERSION, $EMAIL_BROWSERVIEW, $MAIL_LINK_DESCRIPTION, $MAIL_HEADER_PRE;
		global $CIRCULATION_DONE_MESSSAGE_REJECT, $CIRCULATION_DONE_MESSSAGE_SUCCESS, $CIRCDETAIL_SENDER, $CIRCDETAIL_SENDDATE, $MAIL_ADDITION_INFORMATIONS;
		
		global $DEFAULT_CHARSET, $SEND_WORKFLOW_MAIL;
		
	    $prefix_folder = dirname(__FILE__)."/../..";
	    require $prefix_folder.'/lib/vendor/cp_workflow/config/db_config.inc.php';
		
		$TStoday =  mktime(date("H"),date("i"),date("s"),date("m"), date("d"), date("Y"));
		
		$nConnection = mysql_connect($DATABASE_HOST, $DATABASE_UID, $DATABASE_PWD);
		if ($nConnection)
		{
			if (mysql_select_db($DATABASE_DB, $nConnection))
			{	
				// Create the Transport
				/*if ($MAIL_SEND_TYPE == 'SMTP') {
					$transport = Swift_SmtpTransport::newInstance($SMTP_SERVER, $SMTP_PORT)
			  					->setUsername($SMTP_USERID)
			  					->setPassword($SMTP_PWD);
			  					
			  		if ($SMPT_ENCRYPTION != 'NONE') {
			  			$transport = $transport->setEncryption(strtolower($SMPT_ENCRYPTION));
			  		}
				}
				else if ($MAIL_SEND_TYPE == 'PHP') {
					$transport = Swift_MailTransport::newInstance();
				}
				else if ($MAIL_SEND_TYPE == 'MTA') {
					$transport = Swift_SendmailTransport::newInstance($MTA_PATH);
				}
				
				// Create the Mailer using the created Transport
				$mailer = Swift_Mailer::newInstance($transport);
				
				$message = Swift_Message::newInstance()
									->setCharset($DEFAULT_CHARSET);*/
	
				//------------------------------------------------------
				//--- get the needed informations
				//------------------------------------------------------
				
				//--- circulation form
				$arrForm = array();
				$strQuery = "SELECT * FROM cf_circulationform WHERE nid=$nCirculationId";
				$nResult = mysql_query($strQuery, $nConnection);
				if ($nResult)
	    		{
	    			if (mysql_num_rows($nResult) > 0)
	    			{
	    				$arrForm = mysql_fetch_array($nResult);
					}
				}
				
				//--- circulation history
				$arrHistory = array();
				$strQuery = "SELECT * FROM cf_circulationhistory WHERE nid=$nCirculationHistoryId";
				$nResult = mysql_query($strQuery, $nConnection);
				if ($nResult)
	    		{
	    			if (mysql_num_rows($nResult) > 0)
	    			{
	    				$arrHistory = mysql_fetch_array($nResult);
					}
				}
				
				//--- the attachments
				$strQuery = "SELECT * FROM cf_attachment WHERE ncirculationhistoryid=$nCirculationHistoryId";
				$nResult = mysql_query($strQuery, $nConnection);
	    		if ($nResult)
	    		{
	    			if (mysql_num_rows($nResult) > 0)
	    			{
	    				while (	$arrRow = mysql_fetch_array($nResult))
	    				{
							$strFileName = basename($arrRow['strpath']);
							/*$mimetype = new mimetype();
					      	$filemime = $mimetype->getType($strFileName);*/
							/*$message->attach(Swift_Attachment::fromPath(
													$arrRow["strpath"],
													$filemime)->setFilename($strFileName));*/
						}
					}
				}
				
				//------------------------------------------------------
				//--- update status in circulationprocess table
				//------------------------------------------------------				
				if ($tsDateInProcessSince == '')
				{
				    if($nCirculationProcessId == '')
					{
					$strQuery = "INSERT INTO cf_circulationprocess values (null, $nCirculationId, $nSlotId, $nUserId, $TStoday, 0, 0, 0, $nCirculationHistoryId, 0)";
					}
					else
					{
					$strQuery = "INSERT INTO cf_circulationprocess values (null, $nCirculationId, $nSlotId, $nUserId, $TStoday, 0, 0, $nCirculationProcessId, $nCirculationHistoryId, 0)";
					}
					mysql_query($strQuery, $nConnection) or die ($strQuery.mysql_error());
				}
				else
				{
									//( `nID` , `nCirculationFormId` , `nSlotId`, `nUserId` , `dateInProcessSince` , `nDecissionState`, `dateDecission` , `nIsSubstitiuteOf` , `nCirculationHistoryId`)
					$strQuery = "INSERT INTO cf_circulationprocess values (null, $nCirculationId, $nSlotId, $nUserId, $tsDateInProcessSince, 0, 0, 0, $nCirculationHistoryId, 0)";
					mysql_query($strQuery, $nConnection) or die ($strQuery.mysql_error());
				}
				
				//------------------------------------------------------
				//--- generate email message
				//------------------------------------------------------	
				if ($SEND_WORKFLOW_MAIL == true) 
				{		
					$strQuery = "SELECT nid FROM cf_circulationprocess WHERE nslotid=$nSlotId AND nuserid=$nUserId AND ncirculationformid=$nCirculationId AND ncirculationhistoryid=$nCirculationHistoryId";
					$nResult = mysql_query($strQuery, $nConnection);
		    		if ($nResult)
		    		{
		    			if (mysql_num_rows($nResult) > 0)
		    			{
		    				$arrLastRow = array();
		    				
		    				while ($arrRow = mysql_fetch_array($nResult))
		    				{
		    					$arrLastRow = $arrRow;
		    				}
							$Circulation_cpid = $arrLastRow[0];
						}
					}				
					
					//switching Email Format
					if ($nUserId != -2)
					{	
						$strQuery = "SELECT * FROM `cf_user` WHERE nid = $nUserId;";
					}
					else
					{	// in this case the next user is the sender of this circulation
						$strQuery = "SELECT * FROM `cf_user` WHERE nid = ".$arrForm['nsenderid'].";";
					}
					$nResult = mysql_query($strQuery, $nConnection);
					if ($nResult)
		    		{
			    		$user						= mysql_fetch_array($nResult, MYSQL_ASSOC);
			    		
			    		$useGeneralEmailConfig		= $user['busegeneralemailconfig'];
			    		
			    		if (!$useGeneralEmailConfig)
			    		{
				    		$emailFormat	= $user['stremail_format'];
				    		$emailValues	= $user['stremail_values'];
			    		}
			    		else
			    		{
				    		$emailFormat	= $EMAIL_FORMAT;
				    		$emailValues	= $EMAIL_VALUES;
			    		}
			    		
			    		$Circulation_Name			= $arrForm['strname'];
						$Circulation_AdditionalText = str_replace("\n", "<br>", $arrHistory['stradditionaltext']);
	    				
	    				//--- create mail
	    				        $prefix_folder = dirname(__FILE__)."/../../../../..";
						require_once $prefix_folder.'/lib/vendor/cp_workflow/mail/mail_'.$emailFormat.$emailValues.'.inc.php';
	
						/*switch ($emailFormat)
						{
							case PLAIN:
								$message->setBody($strMessage, 'text/plain');
								break;
							case HTML:
								$message->setBody($strMessage, 'text/html');
								break;
		    			}	*/	    		
		    		}				
					
					//------------------------------------------------------
					//--- send email to user
					//------------------------------------------------------
					if ($nUserId != -2)
					{
						$strQuery = "SELECT * FROM cf_user WHERE nid = $nUserId";
					}
					else
					{	// in this case the next user is the sender of this circulation
						$strQuery = "SELECT * FROM cf_user WHERE nid = ".$arrForm['nsenderid']."";
					}
					$nResult = mysql_query($strQuery, $nConnection);
	        		if ($nResult)
	        		{
	        			if (mysql_num_rows($nResult) > 0)
	        			{/*
							$arrRow = mysql_fetch_array($nResult);
							$SYSTEM_REPLY_ADDRESS = str_replace (' ', '_', $SYSTEM_REPLY_ADDRESS);
							
							$message->setFrom(array($SYSTEM_REPLY_ADDRESS=>'CuteFlow'));
							$message->setSubject($MAIL_HEADER_PRE.$arrForm["strname"]);
							
							$message->setTo(array($arrRow["stremail"]));
							
							$result = $mailer->send($message);
							
							if (!$result)
							{
								$fp = @fopen ("mailerror.log", "a");
								if ($fp)
								{
									@fputs ($fp, date("d.m.Y", time())." - sendToUser\n");
									fclose($fp);
								}
							}
							else
							{
								return true;
							}
						*/}
					}
				}
			}
		}
		
		return false;
	}
	
	function sendMessageToSender($nSenderId, $nLastStationId, $strMessageFile, $strCirculationName, $strEndState, $Circulation_cpid, $slotname="")
	{
		global $DATABASE_HOST, $DATABASE_UID, $DATABASE_PWD, $DATABASE_DB, $MAIL_HEADER_PRE, $CUTEFLOW_SERVER;
		global $SMTP_SERVER, $SMTP_PORT, $SMTP_USERID, $SMTP_PWD, $SMTP_USE_AUTH, $MAIL_ENDACTION_DONE_REJECT, $MAIL_ENDACTION_DONE_SUCCESS;
		global $SYSTEM_REPLY_ADDRESS, $CIRCULATION_DONE_MESSSAGE_REJECT, $CIRCULATION_DONE_MESSSAGE_SUCCESS, $CUTEFLOW_VERSION, $EMAIL_FORMAT;
		global $MAIL_SEND_TYPE, $MTA_PATH, $SMPT_ENCRYPTION, $CIRCULATION_SLOTEND_MESSSAGE_SUCCESS, $MAIL_ENDACTION_DONE_ENDSLOT;
		
		global $CUTEFLOW_SERVER, $CUTEFLOW_VERSION, $EMAIL_BROWSERVIEW, $MAIL_LINK_DESCRIPTION, $MAIL_HEADER_PRE;
		global $CIRCULATION_DONE_MESSSAGE_REJECT, $CIRCULATION_DONE_MESSSAGE_SUCCESS, $CIRCDETAIL_SENDER, $CIRCDETAIL_SENDDATE, $MAIL_ADDITION_INFORMATIONS, $objURL;
		
		global $DEFAULT_CHARSET;
		
		$nConnection = mysql_connect($DATABASE_HOST, $DATABASE_UID, $DATABASE_PWD);
		if ($nConnection)
		{
			if (mysql_select_db($DATABASE_DB, $nConnection))
			{
				// Create the Transport
				if ($MAIL_SEND_TYPE == 'SMTP') {
					$transport = Swift_SmtpTransport::newInstance($SMTP_SERVER, $SMTP_PORT)
			  					->setUsername($SMTP_USERID)
			  					->setPassword($SMTP_PWD);
			  					
					if ($SMPT_ENCRYPTION != 'NONE') {
			  			$transport = $transport->setEncryption(strtolower($SMPT_ENCRYPTION));
			  		}
				}
				else if ($MAIL_SEND_TYPE == 'PHP') {
					$transport = Swift_MailTransport::newInstance();
				}
				else if ($MAIL_SEND_TYPE == 'MTA') {
					$transport = Swift_SendmailTransport::newInstance($MTA_PATH);
				}
			  					
			  	// Create the Mailer using the created Transport
				$mailer = Swift_Mailer::newInstance($transport);
				
				$mail_message = Swift_Message::newInstance()
									->setCharset($DEFAULT_CHARSET);
				
				//switching Email Format
				$strQuery	= "SELECT * FROM `cf_user` WHERE nid = $nSenderId;";
				$nResult	= mysql_query($strQuery, $nConnection) or die ("<b>A fatal MySQL error occured</b>.\n<br />Query: " . $strQuery . "<br />\nError: (" . mysql_errno() . ") " . mysql_error());

	    		$user					= mysql_fetch_array($nResult, MYSQL_ASSOC);
	    		
	    		$useGeneralEmailConfig	= $user['busegeneralemailConfig'];
	    		
	    		if (!$useGeneralEmailConfig)
	    		{
		    		$emailFormat	= $user['stremail_format'];
	    		}
	    		else
	    		{
		    		$emailFormat	= $EMAIL_FORMAT;
	    		}
		    		$prefix_folder = dirname(__FILE__)."/../../../../..";   	
    			require_once $prefix_folder.'/lib/vendor/cp_workflow/mail/mail_'.$emailFormat.'_done.inc.php';					
	    					
				switch ($emailFormat)
				{
					case PLAIN:
						$mail_message->setBody($strMessage, 'text/plain');
						break;
					case HTML:
						$mail_message->setBody($strMessage, 'text/html');
						break;
    			}	
	    		
				$mail_message->setFrom(array($SYSTEM_REPLY_ADDRESS=>'CuteFlow'));
				eval ("\$strEndSubject = \"\$MAIL_ENDACTION_DONE_$strEndState\";");
				
				$mail_message->setSubject($MAIL_HEADER_PRE.$strCirculationName.
											$strEndSubject);
				
				$mail_message->setTo(array($user["stremail"]));
				$result = $mailer->send($mail_message);
				if (!$result)
				{
					$fp = @fopen ("/lib/vendor/cp_workflow/mail/mailerror.log", "a");
					if ($fp)
					{
						@fputs ($fp, date("d.m.Y", time())." - sendToUser\n");
						fclose($fp);
					}
				}
				else
				{
					return true;
				}
			}
		}
	}
	function sendMessageToArchitect($user, $strMessage)
	{
		global $DATABASE_HOST, $DATABASE_UID, $DATABASE_PWD, $DATABASE_DB, $MAIL_HEADER_PRE, $CUTEFLOW_SERVER;
		global $SMTP_SERVER, $SMTP_PORT, $SMTP_USERID, $SMTP_PWD, $SMTP_USE_AUTH, $MAIL_ENDACTION_DONE_REJECT, $MAIL_ENDACTION_DONE_SUCCESS;
		global $SYSTEM_REPLY_ADDRESS, $CIRCULATION_DONE_MESSSAGE_REJECT, $CIRCULATION_DONE_MESSSAGE_SUCCESS, $CUTEFLOW_VERSION, $EMAIL_FORMAT;
		global $MAIL_SEND_TYPE, $MTA_PATH, $SMPT_ENCRYPTION, $CIRCULATION_SLOTEND_MESSSAGE_SUCCESS, $MAIL_ENDACTION_DONE_ENDSLOT;
		
		global $CUTEFLOW_SERVER, $CUTEFLOW_VERSION, $EMAIL_BROWSERVIEW, $MAIL_LINK_DESCRIPTION, $MAIL_HEADER_PRE;
		global $CIRCULATION_DONE_MESSSAGE_REJECT, $CIRCULATION_DONE_MESSSAGE_SUCCESS, $CIRCDETAIL_SENDER, $CIRCDETAIL_SENDDATE, $MAIL_ADDITION_INFORMATIONS, $objURL;
		
		global $DEFAULT_CHARSET;
		
		$nConnection = mysql_connect($DATABASE_HOST, $DATABASE_UID, $DATABASE_PWD);
		if ($nConnection)
		{
			if (mysql_select_db($DATABASE_DB, $nConnection))
			{
				// Create the Transport
				if ($MAIL_SEND_TYPE == 'SMTP') {
					$transport = Swift_SmtpTransport::newInstance($SMTP_SERVER, $SMTP_PORT)
			  					->setUsername($SMTP_USERID)
			  					->setPassword($SMTP_PWD);
			  					
					if ($SMPT_ENCRYPTION != 'NONE') {
			  			$transport = $transport->setEncryption(strtolower($SMPT_ENCRYPTION));
			  		}
				}
				else if ($MAIL_SEND_TYPE == 'PHP') {
					$transport = Swift_MailTransport::newInstance();
				}
				else if ($MAIL_SEND_TYPE == 'MTA') {
					$transport = Swift_SendmailTransport::newInstance($MTA_PATH);
				}
			  					
			  	// Create the Mailer using the created Transport
				$mailer = Swift_Mailer::newInstance($transport);
				
				$mail_message = Swift_Message::newInstance()
									->setCharset($DEFAULT_CHARSET);
				
	    		$emailFormat	= $EMAIL_FORMAT;
		    		$prefix_folder = dirname(__FILE__)."/../../../../..";   	
    			require_once $prefix_folder.'/lib/vendor/cp_workflow/mail/mail_'.$emailFormat.'_done.inc.php';					
	    					
				switch ($emailFormat)
				{
					case PLAIN:
						$mail_message->setBody($strMessage, 'text/plain');
						break;
					case HTML:
						$mail_message->setBody($strMessage, 'text/html');
						break;
    			}	
	    		
				$mail_message->setFrom(array($SYSTEM_REPLY_ADDRESS=>'Masterflow'));
				eval ("\$strEndSubject = \"\$MAIL_ENDACTION_DONE_$strEndState\";");
				
				$mail_message->setSubject($MAIL_HEADER_PRE.$strCirculationName.
											$strEndSubject);
				
				$mail_message->setTo(array($user->getProfile()->getEmail()));
				$result = $mailer->send($mail_message);
				if (!$result)
				{
					$fp = @fopen ("mailerror.log", "a");
					if ($fp)
					{
						@fputs ($fp, date("d.m.Y", time())." - sendToUser\n");
						fclose($fp);
					}
				}
				else
				{
					return true;
				}
			}
		}
	}
?>
