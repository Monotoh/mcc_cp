<?php
/**
 * Description of CokMigrationUserManager
 *
 * @author OTB Africa
 */
class CokMigrationUserManager{

    public function CokMigrationUserManager()
    {
    }

	public function checkMigratedPassword($username, $password)
	{
		$q = Doctrine_Query::create()
			->from("SfGuardUser a")
			->where("a.username = ?", $username)
			->limit(1);
		$user = $q->fetchOne();
		error_log($username." >>> conflict with kigali cpmis >> ".$user->getConflictsWithKcps()." >> and bpmis username before cok >>>> ".$user->getBpmisUsernameB4Cok());
		if ($user->checkPasswordByGuard($password)){
			return true;
		}else{
			//If username and password authentication fails, attempt to login in to conflict bpmis account
			$statement = "select id from sf_guard_user where conflicts_with_kcps=1 and bpmis_username_b4_cok='".$username."'";
			$sql_res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($statement);
			if($sql_res){
				$q = Doctrine_Query::create()
					->from("SfGuardUser a")
					->where("a.id = ?", $sql_res[0]['id'])
					->limit(1);
				$user_conflict = $q->fetchOne();
				if ($user_conflict and $user_conflict->checkPasswordByGuard($password)){
					error_log($user_conflict->getUsername()."we can change this username");
					echo "<script language='javascript'>window.parent.location.href = '/index.php/conflictuser/change/userid/" . $user_conflict->getId()."';</script>";
					exit;
				}
			}
			error_log("we can change nothing");

			//Try the log in with the email for a BPMIS user
			$statement = "select sfu.id ,sfu.username from sf_guard_user sfu inner join sf_guard_user_profile sfp on sfp.user_id=sfu.id where sfp.email='".$user->getProfile()->getEmail()."' and user_id>=200000";
			$sql_res = Doctrine_Manager::getInstance()->getCurrentConnection()->fetchAssoc($statement);
			if($sql_res){
				$q = Doctrine_Query::create()
					->from("SfGuardUser a")
					->where("a.id = ?", $sql_res[0]['id'])
					->limit(1);
				$user = $q->fetchOne();
				if ($user and $user->checkPasswordByGuard($password)){
					return true;
				}
			}
			return false;
		}
	}

}