<?php

class BasechangeUserNameForm extends BaseForm
{
  public function setup()
  {
    $this->setWidgets(array(
      'newusername' => new sfWidgetFormInputText(),
      'password' => new sfWidgetFormInputPassword(array('type' => 'password')),
    ));

    $this->setValidators(array(
      'newusername' => new sfValidatorString(),
      'password' => new sfValidatorString(),
    ));

    $this->widgetSchema->setNameFormat('changeusername[%s]');
  }

  public function isValid()
  {
	  //Ensure the username is not used by another user
    $valid = parent::isValid();
    if ($valid)
    {
      $values = $this->getValues();
      $this->user = Doctrine_Core::getTable('sfGuardUser')
        ->createQuery('u')
        ->where('u.username = ?', $values['newusername'])
        ->fetchOne();

      if ($this->user)
      {
        return false;
      } else {
        return true;
      }
    } else {
      return false;
    }
  }
}