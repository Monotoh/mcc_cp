<?php

abstract class BaseAccountDataTransfer extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('account_data_transfer');
        $this->hasColumn('application_id', 'integer', 4, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => 4,
             ));
        $this->hasColumn('transferred_to_id', 'integer', 4, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => 4,
             ));
        $this->hasColumn('transferred_from_id', 'integer', 4, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => 4,
             ));
        $this->hasColumn('validate', 'string', 17, array(
             'type' => 'string',
             'length' => 17,
             ));
        $this->hasColumn('transfer_complete', 'boolean', null, array(
             'type' => 'boolean',
             'default' => false,
             ));
        $this->hasColumn('email_request_sent', 'string', 65536, array(
             'type' => 'string',
             'length' => 65536,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('FormEntry as Application', array(
             'local' => 'application_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $this->hasOne('sfGuardUser as UserTransferredTo', array(
             'local' => 'transferred_to_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $this->hasOne('sfGuardUser as UserTransferredFrom', array(
             'local' => 'transferred_from_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $timestampable0 = new Doctrine_Template_Timestampable(array(
             ));
        $this->actAs($timestampable0);
    }
}