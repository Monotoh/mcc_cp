<?php

class conflictuserActions extends sfActions
{
    /**
	 * Executes 'Index' action
	 *
	 * Displays list of all of the currently logged in client's permits
	 *
	 * @param sfRequest $request A request object
	 */
    public function executeChange(sfWebRequest $request)
    {
		$this->form = new changeUserNameForm();
		//error_log(" my form >> ".print_R($request->getParameter('changeusername'),true));
		$q = Doctrine_Query::create()
			->from("SfGuardUser a")
			->where("a.id = ?", $request->getParameter('userid'))
			->limit(1);
		$user = $q->fetchOne();

		if ($request->isMethod('post'))
		{
			$this->form->bind($request->getParameter('changeusername'));
			$values = $this->form->getValues();
		//error_log(" my password form >> ".$values['password']);
			if($this->form->isValid() and $user->checkPasswordByGuard($values['password'])){
				$user->setUsername($values['newusername']);
				$user->setConflictsWithKcps(false);
				$user->save();
				$success_message = "You have successfully updated your username. Kindly proceed to login with your new username";
				//echo "<script language='javascript'>alert('".$success_message."');</script>";
				$this->getUser()->setFlash("change_username_success", $success_message);
				return $this->redirect('@sf_guard_signin');
			}else if(!$user->checkPasswordByGuard($values['password'])){
				$this->error_message = "The password you have entered is incorrect.";
			}else if (!$this->form->isValid()){
				$this->error_message = "The username you have entered is already in use.";
			}
			$this->getUser()->setFlash("notice", $this->error_message);
		}

		$this->old_username = $user ? $user->getBpmisUsernameB4Cok() : "";
		$this->userid = $request->getParameter('userid');

		$template = $this->getContext()->getConfiguration()->getTemplateDir('conflictuser', 'layoutirembochangeusename.php');
		$this->setLayout($template . '/layoutirembochangeusename');
    }

}
