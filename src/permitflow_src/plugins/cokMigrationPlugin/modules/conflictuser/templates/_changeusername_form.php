 <?php echo $form->renderGlobalErrors() ?>
<?php use_helper('I18N'); ?>
<form id="fm1" class="form-horizontal" action="<?php echo url_for('@conflictuser') ?>" method="post">
     <?php if(isset($form['_csrf_token'])): ?>
     <?php echo $form['_csrf_token']->render(); ?>
    <?php endif; ?>
    <div class="col-md-12">
         <?php if ($sf_user->getFlash('notice')):?>
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert"></button> 
                    <?php echo $sf_user->getFlash('notice') ?>
           </div>
          <?php endif; ?>

        <div class="form-group">
            <label for="" class="col-md-4 control-label"><?php echo __('Old Username') ?>:</label>
            <div class="col-md-8">
                <input class="required form-control" tabindex="1" type="text" value="<?php echo $old_username; ?>" size="25" autocomplete="off" readonly />
            </div>
        </div>
	 
        <div class="form-group">
            <label for="" class="col-md-4 control-label"><?php echo __('New Username') ?>:</label>
            <div class="col-md-8">
                <?php echo $form['newusername']->renderError() ?>
                <input id="changeusername_newusername" name="changeusername[newusername]"  class="required form-control" tabindex="1" type="text" value="" size="25" autocomplete="off"/>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-md-4 control-label"><?php echo __("Password"); ?>:</label>
            <div class="col-md-8">
                <input name="changeusername[password]" id="changeusername_password" class="required form-control" tabindex="2" type="password" value="" size="25" autocomplete="off"/>
            </div>
        </div>
        <input name="userid" id="userid" type="hidden" value="<?php echo $userid; ?>"/>		
        <div class="form-group">
            <label for="" class="col-md-4 control-label"></label>
            <div class="col-md-8">

                <input class="btn btn-default" name="submit" accesskey="l" value="<?php echo __('Save Change') ?>" tabindex="4" type="submit" />
               
            </div>
        </div>


    </div>

</form>


