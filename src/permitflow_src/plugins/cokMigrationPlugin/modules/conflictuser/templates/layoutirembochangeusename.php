<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
 <title><?php echo __('Login - Rwanda Building Permitting System') ?></title> 
<!-- Bootstrap -->
<link href="<?php echo public_path(); ?>irembo/themes/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo public_path(); ?>irembo/themes/css/bootstrap.min.css" rel="stylesheet"/>
<link href="<?php echo public_path(); ?>irembo/themes/css/style.css" rel="stylesheet"/>
<link href="<?php echo public_path(); ?>irembo/themes/css/custom.css" rel="stylesheet"/>
<link href="<?php echo public_path(); ?>irembo/themes/css/home.css" rel="stylesheet"/>
<link href="<?php echo public_path(); ?>irembo/themes/css/print.css" rel="stylesheet"/>
<link href="<?php echo public_path(); ?>otb_assets/less/theme.less" />
<link href="<?php echo public_path(); ?>otb_assets/less/alerts.less"/>
<link href='<?php echo public_path(); ?>irembo/<?php echo public_path(); ?>irembo/fonts.googleapis.com/css5b3b.css?family=Open+Sans:400,800,300,600' rel='stylesheet' type='text/css'/>
<link rel="shortcut icon" href="favicon.ico"/>

</head>

<body class="home-page">
<div class="navbar navbar-default" role="navigation">
  <div class="home-page-top-nav">
    <div class="container relative">
       <?php include_component('index', 'logoheader') ?>
      <div class="login-icon margin-top-10">
        <ul>
		     <!--<li class="hidden-xs"><a href="/index.php/news"><?php //echo __('News'); ?></a></li> -->
	    		    <li class="hidden-xs"><a href="/index.php/help/faq"><?php echo __('FAQs'); ?></a></li>
	    		    <li class="hidden-xs"><a href="/index.php/help/contact"><?php echo __('Contact Us'); ?></a></li>

      <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Language <i class="fa fa-angle-down"></i> </a> 
                               
                                 <?php
                                    $dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
                                    mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

                                    $sql = "SELECT * FROM ext_locales ORDER BY local_title ASC";
                                    $rows = mysql_query($sql, $dbconn);

                                    if(mysql_num_rows($rows) > 1)
                                    {
                                    ?>
                                           
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                            <?php
                                            while($row = mysql_fetch_assoc($rows))
                                            {?>
                                                    <li><a href="/index.php/index/setlocale/code/<?php echo $row['locale_identifier']; ?>">
                                            <?php
                                              if($row['locale_identifier'] == "en_US"){
                                              ?>
                                                    <i><img src="<?php echo public_path(); ?>rwanda_frontend/images/english.jpg" alt=""></i>
                                              <?php
                                              }else if($row['locale_identifier'] == "fr_FR"){
                                              ?>
                                                    <i><img src="<?php echo public_path(); ?>rwanda_frontend/images/french.jpg" alt=""></i>
                                              <?php
                                              }else if($row['locale_identifier'] == "kin"){
                                              ?>
                                                    <i><img src="<?php echo public_path(); ?>rwanda_frontend/images/rwanda.jpg" alt=""></i>
                                              <?php
                                              }
                                              ?>
                                                    <?php echo $row['local_title'];?></a></li>
                                              <?php
                                            }
                                            ?>
                                    </ul>
                                    <?php
                                    }
                              ?>
                             
                            </li>
        </ul>
      </div>
    </div>
  </div>

  <div id="stickey">
    <div class="navbar-collapse">
  <div class="container">
      <ul class="nav navbar-nav">
        <li class="navbar-collapse-close"><img src="<?php echo public_path(); ?>irembo/themes/images/close-icon.jpg"  alt=""/></li>
        <div class="container"> 
              <?php include_component('index', 'irembomenu') ?>
        </div> 
                <div class="container relative"> 
                    <div class="nav-right"> 
                        <ul class="nav navbar-nav"> 
                            <li class="register">
                                <a href="<?php echo url_for('@sf_guard_register'); ?>"><i class="fa fa-pencil-square-o"></i> <?php echo __('Register'); ?></a> </li> 
                            <li class="dropdown-login"><a href="<?php echo url_for('signon/login') ?>" class="last"><?php echo __('Sign In'); ?></a> 


                            </li>
                        </ul>
                    </div>
                </div>
      </ul>
    </div>
  </div>
  </div>

</div>



<div class="page-container login-bg"> 

  <div class="container">
  <div class="row">
  <div class="col-md-12 center-block">
  
  <div class="col-md-6">
  <div class="welcome-message">
      <h2 style="color:#00FF00;"> <u>Change of Username Notice !</u> </h2>
      <h4>
      <span style="color:#00FF00;">
          Dear user, following the upgrade of the BPMIS to include the City of Kigali, you are required to 
      change your username as it was previously registered by another user on the old City of Kigali system.
      Kindly note: <br/><br/>
	  <ul>
	  <li>1. You do not have to change your password</li>
	  <li>2. Remember your new username as this is the username you will use for login in moving forward</li>
	  </ul>
          </span>
      </h4>
  </div>
  </div>
     
  <div class="col-md-6">
  <div class="welcome-message login-box">
  <h1><?php echo __('Change your username') ?></h1>
     <?php echo $sf_content ?>
   <div class="clearfix"></div>
  </div>
  
  
  </div>

  
  </div>
  
  
  </div>
    
  </div>
    
    
</div>
  <div class="footer">
    <div class="container">
      <div class="row">
        <div class="col-xs-7">
          <ul class="social-footer">
           <li><a href='#'>
                                                            <img src="<?php echo public_path(); ?>irembo/themes/images/facebook-icon.png" alt=""/></a></li> 
                                                            <li><a href='#'><img src="<?php echo public_path(); ?>irembo/themes/images/twiter-icon.png" alt=""/></a></li> 
                                                    <li><a href='#'>
                                                    <img src="<?php echo public_path(); ?>irembo/themes/images/youtube-icon.png" alt=""/></a>
                                                    </li>
          </ul>
        </div>
        <div class="col-xs-5 text-right"> <a><img src="<?php echo public_path(); ?>irembo/themes/images/entrust.jpg"  alt=""/> </div>
      </div>
    </div>
  </div>
  <div class="copyright">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-sm-8">
          <ul class="footer-links"> <!--<li><a href="/rolportal/web/rol/contact"> </a> </li>--> 
                                 <li><a href='#'> <?php echo __('Privacy Statement') ?></a> </li> 
                                 <li><a href='#'><?php echo __('Terms of Service') ?></a> </li> 
                                 <li><a href='#'><?php echo __('Disclaimer') ?></a> </li> 
         </ul> 
        </div>
        <div class="col-md-4 col-sm-4"> <span class=" pull-right"><?php echo __('Rwanda Housing Authority') ?></span> </div>
      </div>
    </div>
  </div>
</div>

<script src="<?php echo public_path(); ?>irembo/themes/js/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<?php echo public_path(); ?>irembo/themes/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="<?php echo public_path(); ?>irembo/themes/js/jquery.sticky.js"></script> 
<script src="<?php echo public_path(); ?>irembo/themes/js/custom.js"></script> 
<script>
    $(window).load(function(){
      $("#stickey").sticky({ topSpacing: 0 });
    });
</script>
<script>function openwindow(a){window.open(a,"_target","width=1200, height=900,directories=no, menubar=no,locationbar=no,resizable=no,scrollbars=yes,status=yes")};</script>
</body>

<!-- Mirrored from irembo.gov.rw/cas-web/login?service=https%3A%2F%2Firembo.gov.rw%2Frolportal%2Fc%2Fportal%2Flogin%3Fredirect%3D%252Frolportal%252Fweb%252Frol%252Freplacement-of-definitive-driving-license%26p_l_id%3D20184&locale=fr by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 27 Aug 2016 21:29:11 GMT -->
</html>
