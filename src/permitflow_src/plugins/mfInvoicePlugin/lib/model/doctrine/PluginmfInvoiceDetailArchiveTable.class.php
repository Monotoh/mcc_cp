<?php

/**
 * PluginmfInvoiceDetailArchiveTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class PluginmfInvoiceDetailArchiveTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object PluginmfInvoiceDetailArchiveTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PluginmfInvoiceDetailArchive');
    }
}