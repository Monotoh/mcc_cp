<?php

/**
 * PluginmfInvoiceTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class PluginmfInvoiceTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object PluginmfInvoiceTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PluginmfInvoice');
    }
}