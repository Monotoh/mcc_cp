<?php
$prefix_folder = dirname(__FILE__)."/../../../../..";
require_once($prefix_folder.'/tcpdf/config/lang/eng.php');
require_once($prefix_folder.'/tcpdf/tcpdf.php');


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

	//Page header
	public function Header() {
		// Set font
		$this->Ln();
		$this->Ln();
		$this->SetFont('helvetica', 'B', 16);
		// Title
		$this->Cell(0, 10, 'Plan Application Invoice', 0, false, 'C', 0, '', 0, false, 'M', 'M');				$image_file = K_PATH_IMAGES.'logo.png';				$this->Image($image_file, 80, 10, 50, '', 'PNG', '', 'T', false, 500, '', false, false, 0, false, false, false);
	}

	// Page footer
	public function Footer() {
		// Position at 15 mm from bottom
		$this->SetY(-15);
		// Set font
		$this->SetFont('helvetica', 'I', 11);
		// Page number
		$this->Ln();
		
		$this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
	
	public function ColoredTable($header,$data) {
		// Colors, line width and bold font
		$this->SetFillColor(190, 190, 190);
		$this->SetTextColor(0);
		$this->SetDrawColor(90, 90, 90);
		$this->SetLineWidth(0.3);
		$this->SetFont('', 'B');
		// Header
		$w = array(126, 52);
		$num_headers = count($header);
		for($i = 0; $i < $num_headers; ++$i) {
			$this->Cell($w[$i], 7, $header[$i], 1, 0, 'C', 1);
		}
		$this->Ln();
		// Color and font restoration
		$this->SetFillColor(224, 235, 255);
		$this->SetTextColor(0);
		$this->SetFont('');
		// Data
		$fill = 0;
		foreach($data as $row) {
			$this->Cell($w[0], 6, $row[0], 'LR', 0, 'L', $fill);
			$this->Cell($w[1], 6, 'KSH.'.$row[1], 'LR', 0, 'R', $fill);
			$this->Ln();
			$fill=!$fill;
		}
		$this->Cell(array_sum($w), 0, '', 'T');
		
		
		$this->SetFillColor(255, 255, 255);
		$this->SetTextColor(0);
		$this->SetDrawColor(255, 255, 255);
		
		
		$this->Ln();
		// Page number
		
		$this->Cell(0, 10, 'Approval Fee Code:   1-9112', 0, false, 'L', 0, '', 0, false, 'T', 'M');
		
		$this->Ln();
		
		$this->Cell(0, 10, 'Occupation Fee Code:   1-9134', 0, false, 'L', 0, '', 0, false, 'T', 'M');
		
		$this->Ln();
		
		$this->Cell(0, 10, 'Infrastructure Fee Code:  1-3297', 'L', 0, '', 0, false, 'T', 'M');
		
		$this->Ln();
		
		$this->Cell(0, 10, 'Construction and site board Fee Code:   1-3243', 0, false, 'L', 0, '', 0, false, 'T', 'M');
		
		$this->Ln();
		
		
		$this->Cell(0, 10, '', 0, false, 'C', 0, '', 0, false, 'T', 'M');
		
		$this->Ln();
	}
	
	public function TopRightTable($data) {
		$w = array(140, 40);
		// Color and font restoration
		$this->SetFillColor(255, 255, 255);
		$this->SetDrawColor(255,  255, 255);
		$this->SetTextColor(0);
		$this->SetFont('');						// Logo		
		// Data
		$fill = 0;
		foreach($data as $row) {
			$this->Cell($w[0], 6, $row[0], 'LR', 0, 'R', $fill);
			$this->Cell($w[1], 6, $row[1], 'LR', 0, 'L', $fill);
			$this->Ln();
			$fill=!$fill;
		}
		$this->Cell(array_sum($w), 0, '', 'T');
		
		$this->Ln();
	}
	
	public function TopLeftTable($data) {
		$w = array(40, 40);
		// Color and font restoration
		$this->SetFillColor(255, 255, 255);
		$this->SetDrawColor(255,  255, 255);
		$this->SetTextColor(0);
		$this->SetFont('');
		// Data
		$fill = 0;
		foreach($data as $row) {
			$this->Cell($w[0], 6, $row[0], 'LR', 0, 'R', $fill);
			$this->Cell($w[1], 6, $row[1], 'LR', 0, 'L', $fill);
			$this->Ln();
			$fill=!$fill;
		}
		$this->Cell(array_sum($w), 0, '', 'T');		
		$this->Ln();				$this->Cell(array_sum($w), 0, '	Before we comment on your plan, please pay the below fees.', 'T');				$this->Ln();				$this->Cell(array_sum($w), 0, '	', 'T');				$this->Ln();
	}
}
class mfInvoiceActions extends sfActions
{
 	public function executeGenerate(sfWebRequest $request)
	{
		$this->setLayout('layout-content');
	}
	public function executeGenerate2(sfWebRequest $request)
	{
		$this->setLayout('layout-content');
	}
 	public function executeGeneratemobile(sfWebRequest $request)
	{
		$this->setLayout('layout-mobile');
	}
	public function executeSaveinvoice(sfWebRequest $request)
	{
		$this->setLayout('layout-content');
		
		$entry = Doctrine_Core::getTable('FormEntry')->find(array($request->getPostParameter('entryid')));
		
		$invoice = new MfInvoice();
		$invoice->setAppId($entry->getId());
		$invoice->setInvoiceNumber("CP-".$entry->getId());
		$invoice->save();
		
		$descriptions = $request->getPostParameter("description");
		$amounts = $request->getPostParameter("amount");
		
		$index = 0;
		while (list ($key,$val) = @each ($descriptions)) {
			$invoicedetail = new MfInvoiceDetail();
			$invoicedetail->setDescription($val);
			$invoicedetail->setAmount($amounts[$index]);
			$invoicedetail->setInvoiceId($invoice->getId());
			$invoicedetail->save();
			$index++;
		}
		
		$entry->setApproved($request->getPostParameter("moveto"));
		$entry->save();
		
		
	}
	public function executeSavemobileinvoice(sfWebRequest $request)
	{
		$this->setLayout('layout-mobile');
		
		$entry = Doctrine_Core::getTable('FormEntry')->find(array($request->getPostParameter('entryid')));
		
		$invoice = new MfInvoice();
		$invoice->setAppId($entry->getId());
		$invoice->setInvoiceNumber("CP-".$entry->getId());
		$invoice->save();
		
		$descriptions = $request->getPostParameter("description");
		$amounts = $request->getPostParameter("amount");
		
		$index = 0;
		while (list ($key,$val) = @each ($descriptions)) {
			$invoicedetail = new MfInvoiceDetail();
			$invoicedetail->setDescription($val);
			$invoicedetail->setAmount($amounts[$index]);
			$invoicedetail->setInvoiceId($invoice->getId());
			$invoicedetail->save();
			$index++;
		}
		
		
		//Save Audit
		$audit = new Audit();
		$audit->saveAudit($entry->getId(), "approved_invoice");
		
		$notifier = new Notifications($this->getMailer());
		
		$user = Doctrine_Core::getTable('sfGuardUser')->find(array($entry->getUserId()));
		
		$to = $user->getProfile()->getEmail();
		$from = sfConfig::get('app_fromemail');
		$subject = "Pending Payment";
		$layout = Doctrine_Core::getTable("WebLayout")->find("7");
				
		$body = $layout->getContent();
		
		
				$templateparser = new Templateparser();
		        
				$body = $templateparser->parse($entry->getId(),$entry->getFormId(), $entry->getEntryId(), $body);
		
		$notifier->sendemail($from,$to,$subject,$body);
		
	}
	public function executeView(sfWebRequest $request)
	{
		
    

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('City Council Of Nairobi');
$pdf->SetTitle('Invoice');
$pdf->SetSubject('Architect Invoice');
$pdf->SetKeywords('Invoice, Architect, Plan');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
//$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
$pdf->SetFont('times', '', 11);

// add a page
$pdf->AddPage();

// set some text to print
$invoice = Doctrine_Core::getTable('MfInvoice')->find(array($request->getGetParameter('id')));

//Get Form Entry ID
$theform = Doctrine_Core::getTable('FormEntry')->find(array($invoice->getAppId()));

if($theform){


$data[] = array("Date:",date('Y-m-d'));
$data[] = array("PRN.:",$theform->getApplicationId());

$pdf->TopRightTable($data);

$txt = <<<EOD
To
EOD;

$pdf->Write($h=0, $txt, $link='', $fill=0, $align='L', $ln=true, $stretch=0, $firstline=false, $firstblock=false, $maxh=0);

$userid = $theform->getUserId();

$q = Doctrine_Query::create()
					   ->from('sfGuardUser a')
					   ->where('a.id = ?', $userid);
$users = $q->execute();
$theuser = "";
foreach($users as $user)
{
	$theuser = $user;
}

if($theuser){

//Get User Profile Form Entry
$q = Doctrine_Query::create()
					   ->from('MfUserProfile a')
					   ->where('a.user_id = ?', $theuser->getId());
$userprofiles = $q->execute();
$userprofile = "";
foreach($userprofiles as $profile)
{
	$userprofile = $profile;
}
//Get Form ID
$form_id = $userprofile->getFormId();
//Get Entry ID
$entry_id = $userprofile->getEntryId();
//Get Choosen/Preferred Columns (ApColumnPreferences)
$q = Doctrine_Query::create()
					   ->from('ApColumnPreferences a')
					   ->where('a.form_id = ?', $form_id);
$columnprefs = $q->execute();
$columns = "";
foreach($columnprefs as $pref)
{
	$columns[] = $pref->getElementName();
}
//Get Data
$conn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
mysql_select_db(sfConfig::get('app_mysql_db'),$conn);
$query = "SELECT * FROM ap_form_".$form_id." WHERE id=".$entry_id;
$result = mysql_query($query,$conn);
$row = mysql_fetch_assoc($result);

$query2 = "SELECT * FROM ap_form_elements WHERE form_id = ".$form_id;
$result2 = mysql_query($query2, $conn);
$columnassoc = "";
$columnassocids = "";
while($rows2 = mysql_fetch_assoc($result2))
{
	$columnassoc['element_'.$rows2['element_id']] = $rows2['element_title'];
	$columnassocids['element_'.$rows2['element_id']] = $rows2['element_id'];
	$columnassoc['element_'.$rows2['element_id'].'_1'] = $rows2['element_title'];
	$columnassoc['element_'.$rows2['element_id'].'_2'] = $rows2['element_title'];
	$columnassoc['element_'.$rows2['element_id'].'_3'] = $rows2['element_title'];
	$columnassoc['element_'.$rows2['element_id'].'_4'] = $rows2['element_title'];
}

    $data2[] = array("Name:",$theuser->getProfile()->getFullname());			$query = "SELECT * FROM ap_form_".$theform->getFormId()." WHERE id=".$theform->getEntryId();		$app_result = mysql_query($query,$conn);		$app_row = mysql_fetch_assoc($app_result);						$query2 = "SELECT * FROM ap_form_elements WHERE form_id = ".$theform->getFormId();		$app_result2 = mysql_query($query2, $conn);		$app_columnassoc = "";		$app_columnassocids = "";		while($app_rows2 = mysql_fetch_assoc($app_result2))		{			$app_columnassoc['element_'.$app_rows2['element_id']] = $app_rows2['element_title'];			$app_columnassocids['element_'.$app_rows2['element_id']] = $app_rows2['element_id'];		}	
	foreach($columns as $column)
	{
	    $element_id = $columnassocids[$column];
		$q = Doctrine_Query::create()
						   ->from('ApElementOptions a')
						   ->where('a.form_id = ? AND a.element_id = ? AND a.option_id = ?', array($form_id, $element_id,$row[$column]));
	    $option = $q->fetchOne();
		
		if($option)
		{
			$data2[] = array($columnassoc[$column].":",$option->getOption());
		}
		else					   
		{			   
			$data2[] = array($columnassoc[$column].":",$row[$column]);
		}
	}		    $data2[] = array($app_columnassoc['element_34'].":",$app_row['element_34']); 		$data2[] = array($app_columnassoc['element_4'].":",$app_row['element_4']); 		$data2[] = array($app_columnassoc['element_66'].":",$app_row['element_66']); 		
}

$pdf->TopLeftTable($data2);

$header = array("Description","Amount");

$invoicedetails = $invoice->getMfInvoiceDetail();

foreach($invoicedetails as $detail)
{
  $data3[] = array($detail->getDescription(),$detail->getAmount());
}

//$data3[] = array("Sample Description","KSH.2000");


$pdf->ColoredTable($header, $data3);

// ---------------------------------------------------------
}
//Close and output PDF document
$pdf->Output('invoice.pdf', 'I');
  
	}
}
?>