<?php 
$prefix_folder = dirname(__FILE__)."/../../../../..";
require($prefix_folder.'/lib/vendor/cp_form/includes/check-session.php');
 ?>
<table cellspacing="0" cellpadding="0" width="700">
<tr>
	<td align="left" style="padding-right: 20px;">
	<span style="float: left;font-size: 14pt; color: #ffa000; font-family: Verdana; font-weight: bold;">
		Generate Invoice
	</span>
	<div style="display: none; margin-top: 5px;" id="loading">
		<table cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" valign="middle">
				<img src="/cuteflow/images/loading_moz.gif" hspace="3">
			</td>
			<td align="left" valign="top">
				<?php echo $LOADING_DATA;?>
			</td>
		</tr>
		</table>
	</div>
	</td>
</tr>
</table>
<br>
<?php
$theform = Doctrine_Core::getTable('FormEntry')->find(array($_GET['entryid']));

$q = Doctrine_Query::create()
   ->from('ApFormElements a')
   ->where('a.element_type = ?', "area_fee")
   ->andWhere('a.form_id = ?',$theform->getFormId())
   ->andWhere('a.element_title LIKE ?', "%total%");
   
$elements = $q->execute();
$theelement = "";
foreach($elements as $element)
{
	$theelement = $element;
}
$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
$query = "SELECT * FROM ap_form_".$theform->getFormId()." WHERE id = ".$theform->getEntryId();
$results = mysql_query($query,$dbconn);
$dbrow = mysql_fetch_assoc($results);

$q = Doctrine_Query::create()
    ->from('PlotFeeMatrix a')
	->where('a.building_type = ?', $dbrow["element_10"])
	->andWhere('a.max < ?', $dbrow["element_".$theelement->getElementId()."_1"]);
$isfees = $q->execute();

$theisfee = 0;
if(sizeof($isfees) > 0)
{
    $q = Doctrine_Query::create()
    ->from('PlotFeeMatrix a')
	->where('a.building_type = ?', $dbrow["element_10"])
	->andWhere('? BETWEEN a.min AND a.max', $dbrow["element_".$theelement->getElementId()."_1"]);
    $isfees = $q->execute();
	foreach($isfees as $isfee)
	{
		//echo $isfee->getMax();
		$theisfee = $isfee->getFee();
		if($isfee->getMax() == "~")
		{
			$theisfee = round(($isfee->getMin() / 93) * $theisfee);
		}
		break;
	}
}
else
{
    $q = Doctrine_Query::create()
    ->from('PlotFeeMatrix a')
	->where('a.building_type = ?', $dbrow["element_10"])
	->andWhere('a.max = ?', "~");
    $isfees = $q->execute();
	foreach($isfees as $isfee)
	{
		$theisfee = $isfee->getFee();
		if($isfee->getMax() == "~")
		{
			$theisfee = round(($dbrow["element_".$theelement->getElementId()."_1"] / 93) * $theisfee);
		}
		break;
	}
}

$q = Doctrine_Query::create()
    ->from('PlotOcAreaMatrix a')
	->where('a.building_type = ?', $dbrow["element_10"])
	->andWhere('a.max < ?', $dbrow["element_".$theelement->getElementId()."_1"]);
$ocfees = $q->execute();

$theocfee = 0;

if(sizeof($ocfees) > 0)
{
	$q = Doctrine_Query::create()
    ->from('PlotOcAreaMatrix a')
	->where('a.building_type = ?', $dbrow["element_10"])
	->andWhere('? BETWEEN a.min AND a.max', $dbrow["element_".$theelement->getElementId()."_1"]);
	$ocfees = $q->execute();
	foreach($ocfees as $ocfee)
	{
		$theocfee = round($ocfee->getFee());
		break;
	}
}
else
{
	$q = Doctrine_Query::create()
    ->from('PlotOcAreaMatrix a')
	->where('a.building_type = ?', $dbrow["element_10"])
	->andWhere('a.max = ?', "~");
	$ocfees = $q->execute();
	foreach($ocfees as $ocfee)
	{
		$theocfee = $ocfee->getFee();
		if($ocfee->getMax() == "~")
		{
			$theocfee = round(($dbrow["element_".$theelement->getElementId()."_1"] / 93) * $theocfee);
		}
		break;
	}
}

$totalfee = 0;

?>
<br />

<form method="post" action="/backend.php/mfInvoice/saveinvoice">
<input type="hidden" name="entryid" id="entryid" value="<?php echo $_GET['entryid']; ?>" />
<table width="100%" border="0">

<tr>
      <th width="70%">Description</th><th>Amount</th>
      </tr>
      <tr>
      <td width="70%"><input type='text' name='description[]' value='' style="width:100%; font-weight:bolder;"/></td><td><input type='text' name='amount[]' value='' style="font-weight:bolder;"/></td>
      </tr><tr>
      <td width="70%"><input type='text' name='description[]' value='' style="width:100%; font-weight:bolder;"/></td><td><input type='text' name='amount[]' value='' style="font-weight:bolder;"/></td>
      </tr><tr>
      <td width="70%"><input type='text' name='description[]' value='' style="width:100%; font-weight:bolder;"/></td><td><input type='text' name='amount[]' value='' style="font-weight:bolder;"/></td>
      </tr><tr>
      <td width="70%"><input type='text' name='description[]' value='' style="width:100%; font-weight:bolder;"/></td><td><input type='text' name='amount[]' value='' style="font-weight:bolder;"/></td>
      </tr>
      <tr>
      <td width="70%"><input type='text' name='description[]' value='Total' readonly="readonly" style="width:100%; font-weight:bolder;"/></td><td><input type='text' name='amount[]' value='' style="font-weight:bolder;"/></td>
      </tr>
</table>

<div class="extrabottom">
<div class="bulkactions">
<input type="submit" value="Save And Send" />
</div>
</div>

</form>

