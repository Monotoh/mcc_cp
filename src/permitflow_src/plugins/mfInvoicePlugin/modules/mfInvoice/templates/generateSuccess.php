<?php 
$prefix_folder = dirname(__FILE__)."/../../../../..";
require($prefix_folder.'/lib/vendor/cp_form/includes/check-session.php');
 ?>
<table cellspacing="0" cellpadding="0" width="700">
<tr>
	<td align="left" style="padding-right: 20px;">
	<span style="float: left;font-size: 14pt; color: #ffa000; font-family: Verdana; font-weight: bold;">
		Generate Invoice
	</span>
	<div style="display: none; margin-top: 5px;" id="loading">
		<table cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" valign="middle">
				<img src="/cuteflow/images/loading_moz.gif" hspace="3">
			</td>
			<td align="left" valign="top">
				<?php echo $LOADING_DATA;?>
			</td>
		</tr>
		</table>
	</div>
	</td>
</tr>
</table>
<br>
<?php
$theform = Doctrine_Core::getTable('FormEntry')->find(array($_GET['entryid']));

$q = Doctrine_Query::create()
   ->from('ApFormElements a')
   ->where('a.element_type = ?', "area_fee")
   ->andWhere('a.form_id = ?',$theform->getFormId())
   ->andWhere('a.element_title LIKE ?', "%total%");
   
$elements = $q->execute();
$theelement = "";
foreach($elements as $element)
{
	$theelement = $element;
}
$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
$query = "SELECT * FROM ap_form_".$theform->getFormId()." WHERE id = ".$theform->getEntryId();
$results = mysql_query($query,$dbconn);
$dbrow = mysql_fetch_assoc($results);

$_POST['building_type'] = $dbrow["element_10"];
$_POST['application_type'] = $dbrow["element_44"];
$_POST['plotsize'] = $dbrow["element_".$theelement->getElementId()."_1"];

$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);

$query = "SELECT * FROM plot_fee_matrix WHERE building_type = ".$_POST['building_type']." AND application_type = ".$_POST['application_type']." AND ( ".$_POST['plotsize']." BETWEEN min and max)";

$result = mysql_query($query,$dbconn);

if($inspectionfee = mysql_fetch_assoc($result))
{
	$theisfee = $inspectionfee['fee'];
}
else
{
	    $query = "SELECT * FROM plot_fee_matrix WHERE application_type = ".$_POST['application_type']." AND building_type = ".$_POST['building_type']." AND ".$_POST['plotsize']." > min AND max = '~'";
		$result = mysql_query($query,$dbconn);
		if($inspectionfee1 = mysql_fetch_assoc($result))
		{
			$incr_fee = $inspectionfee1['fee'];
			
			//get the second last fee and use these as the base fee
			$base_fee = 0;
			$base_max = 0;
			$query = "SELECT * FROM plot_fee_matrix WHERE application_type = ".$_POST['application_type']." AND building_type = ".$_POST['building_type']." AND max <> '~' ORDER BY FEE ASC";
			$result2 = mysql_query($query,$dbconn);
			while($thefee = mysql_fetch_assoc($result2))
			{
				$base_fee = $thefee['fee'];
				$base_max = $thefee['max'];
			}
			
			//subtract second last max area from total area
			$exc_area = $_POST['plotsize'] - $base_max;
			
			//divide by 93 and clear remainders
			$exc_area = round($exc_area/93, 0);
			if($exc_area == 0)
			{
				$exc_area = 1;
			}
			
			//multiply remainder by incr_fee
			$add_fee = $exc_area * $incr_fee;
			
			//add result to base fee
			$total_fee = $base_fee + $add_fee;
			
			$theisfee = $total_fee;
		}
}

	$query = "SELECT * FROM plot_oc_area_matrix WHERE building_type = ".$_POST['building_type']." AND ( ".$_POST['plotsize']." BETWEEN min and max)";
    $result1 = mysql_query($query,$dbconn);
    if($occupationfee = mysql_fetch_assoc($result1))
	{
		$theisfee = $occupationfee['fee'];
	}
	else
	{
		$query = "SELECT * FROM plot_oc_area_matrix WHERE building_type = ".$_POST['building_type']." AND ".$_POST['plotsize']." > min AND max = '~'";
		$result2 = mysql_query($query,$dbconn);
		if($occupationfee1 = mysql_fetch_assoc($result2))
		{
			$incr_fee = $occupationfee1['fee'];
			
			//get the second last fee and use these as the base fee
			$base_fee = 0;
			$base_max = 0;
			$query = "SELECT * FROM plot_oc_area_matrix WHERE building_type = ".$_POST['building_type']." AND max <> '~' ORDER BY FEE ASC";
			$result2 = mysql_query($query,$dbconn);
			while($thefee = mysql_fetch_assoc($result2))
			{
				$base_fee = $thefee['fee'];
				$base_max = $thefee['max'];
			}
			
			//subtract second last max area from total area
			$exc_area = $_POST['plotsize'] - $base_max;
			
			//divide by 93 and clear remainders
			$exc_area = round($exc_area/93, 0);
			if($exc_area == 0)
			{
				$exc_area = 1;
			}
			
			//multiply remainder by incr_fee
			$add_fee = $exc_area * $incr_fee;
			
			//add result to base fee
			$total_fee = $base_fee + $add_fee;
			$theocfee = $total_fee;
		}
	}

$totalfee = 0;

?>
<br />

<form method="post" action="/backend.php/mfInvoice/saveinvoice">
<input type="hidden" name="entryid" id="entryid" value="<?php echo $_GET['entryid']; ?>" />
<input type="hidden" name="moveto" id="moveto" value="<?php echo $_GET['moveto']; ?>" />
<table width="100%" border="0">

<tr>
      <th width="70%">Description</th><th>Amount</th>
      </tr>
      <tr>
      <td width="70%"><input type='text' name='description[]' value='Inspection (Submission) Fee' style="width:100%;" /></td><td><input type='text' name='amount[]' value='<?php echo $theisfee; ?>' /></td>
      <?php
	  $totalfee = $totalfee + $theisfee;
	  ?>
      </tr>
      <tr>
      <td width="70%"><input type='text' name='description[]' value='Occupation Certificate Fee' style="width:100%;" /></td><td><input type='text' name='amount[]' value='<?php echo $theocfee; ?>' /></td>
      <?php
	  $totalfee = $totalfee + $theocfee;
	  ?>
      </tr>
      <tr>
      <td width="70%"><input type='text' name='description[]' value='Infrastructure Development Levy' style="width:100%;" /></td><td><input type='text' name='amount[]' value='<?php echo 0.0005 * $dbrow['element_6']; ?>' /></td>
      <?php
	  $totalfee = $totalfee + (0.0005 * $dbrow['element_6']); 
	  ?>
      </tr>
<?php
  $charges = 0;
  $totalAmount = 0;
?>
      
<?php
  $q = Doctrine_Query::create()
       ->from('Fee a')
	   ->orderBy('a.id DESC');
  $fees = $q->execute();
  foreach($fees as $fee)
  {
      ?>
      <tr>
      <td width="70%"><input type='text' name='description[]' value='<?php echo $fee->getDescription(); ?>' style="width:100%;" /></td><td><input type='text' name='amount[]' value='<?php echo $fee->getAmount(); ?>' /></td>
      <?php
	  $totalfee = $totalfee + $fee->getAmount();
	  ?>
      </tr>
      <?php
  }
?>

      <tr>
      <td width="70%"><input type='text' name='description[]' value='Total' style="width:100%; font-weight:bolder;" /></td><td><input type='text' name='amount[]' value='<?php echo $totalfee; ?>' style="font-weight:bolder;"/></td>
      </tr>
</table>

<div class="extrabottom">
<div class="bulkactions">
<input type="submit" value="Save And Send" />
</div>
</div>

</form>

