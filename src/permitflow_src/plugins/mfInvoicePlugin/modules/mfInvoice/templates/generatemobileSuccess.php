<?php 
$prefix_folder = dirname(__FILE__)."/../../../../..";
require($prefix_folder.'/lib/vendor/cp_form/includes/check-session.php');
 ?>
<table cellspacing="0" cellpadding="0" width="700">
<tr>
	<td align="left" style="padding-right: 20px;">
	<span style="float: left;font-size: 14pt; color: #ffa000; font-family: Verdana; font-weight: bold;">
		Generate Invoice
	</span>
	<div style="display: none; margin-top: 5px;" id="loading">
		<table cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" valign="middle">
				<img src="/cuteflow/images/loading_moz.gif" hspace="3">
			</td>
			<td align="left" valign="top">
				<?php echo $LOADING_DATA;?>
			</td>
		</tr>
		</table>
	</div>
	</td>
</tr>
</table>
<br>
<?php
$theform = Doctrine_Core::getTable('FormEntry')->find(array($_GET['entryid']));

$q = Doctrine_Query::create()
   ->from('ApFormElements a')
   ->where('a.element_type = ?', "area_fee")
   ->andWhere('a.form_id = ?',$theform->getFormId())
   ->andWhere('a.element_title LIKE ?', "%total%");
   
$elements = $q->execute();
$theelement = "";
foreach($elements as $element)
{
	$theelement = $element;
}
$dbconn = mysql_connect(sfConfig::get('app_mysql_host'),sfConfig::get('app_mysql_user'),sfConfig::get('app_mysql_pass'));
mysql_select_db(sfConfig::get('app_mysql_db'),$dbconn);
$query = "SELECT * FROM ap_form_".$theform->getFormId()." WHERE id = ".$theform->getEntryId();
$results = mysql_query($query,$dbconn);
$dbrow = mysql_fetch_assoc($results);

$q = Doctrine_Query::create()
    ->from('PlotFeeMatrix a')
	->where('a.building_type = ?', $dbrow["element_10"])
	->andWhere('? BETWEEN a.min AND a.max', $dbrow["element_".$theelement->getElementId()."_1"]);
$isfees = $q->execute();
$theisfee = 0;
foreach($isfees as $isfee)
{
	$theisfee = $isfee->getFee();
}

$q = Doctrine_Query::create()
    ->from('PlotOcAreaMatrix a')
	->where('a.building_type = ?', $dbrow["element_10"])
	->andWhere('? BETWEEN a.min AND a.max', $dbrow["element_".$theelement->getElementId()."_1"]);
$ocfees = $q->execute();
$theocfee = 0;
foreach($ocfees as $ocfee)
{
	$theocfee = $ocfee->getFee();
}

$totalfee = 0;

?>
<br />

<form method="post" action="/backend.php/mfInvoice/savemobileinvoice">
<input type="hidden" name="entryid" id="entryid" value="<?php echo $_GET['entryid']; ?>" />
<table width="100%" border="0">

<tr>
      <th width="70%">Description</th><th>Amount</th>
      </tr>
      <tr>
      <td width="70%"><input type='text' name='description[]' value='Inspection (Submission) Fee'  readonly="readonly"/></td><td><input type='text' name='amount[]' value='<?php echo $theisfee; ?>'/></td>
      <?php
	  $totalfee = $totalfee + $theisfee;
	  ?>
      </tr>
      <tr>
      <td width="70%"><input type='text' name='description[]' value='Occupation Certificate Fee'  readonly="readonly"/></td><td><input type='text' name='amount[]' value='<?php echo $theocfee; ?>'/></td>
      <?php
	  $totalfee = $totalfee + $theocfee;
	  ?>
      </tr>
      <tr>
      <td width="70%"><input type='text' name='description[]' value='Infrastructure Development Levy'  readonly="readonly"/></td><td><input type='text' name='amount[]' value='<?php echo 0.0005 * $dbrow['element_6']; ?>'/></td>
      <?php
	  $totalfee = $totalfee + (0.0005 * $dbrow['element_6']); 
	  ?>
      </tr>
<?php
  $charges = 0;
  $totalAmount = 0;
?>
      
<?php
  $q = Doctrine_Query::create()
       ->from('Fee a')
	   ->orderBy('a.id DESC');
  $fees = $q->execute();
  foreach($fees as $fee)
  {
      ?>
      <tr>
      <td width="70%"><input type='text' name='description[]' value='<?php echo $fee->getDescription(); ?>' /></td><td><input type='text' name='amount[]' value='<?php echo $fee->getAmount(); ?>'/></td>
      <?php
	  $totalfee = $totalfee + $fee->getAmount();
	  ?>
      </tr>
      <?php
  }
?>

      <tr>
      <td width="70%"><input type='text' name='description[]' value='Total' style="font-weight:bolder;"/></td><td><input type='text' name='amount[]' value='<?php echo $totalfee; ?>' style="font-weight:bolder;"/></td>
      </tr>
</table>

<div class="extrabottom">
<div class="bulkactions">
<input type="submit" value="Save And Send" />
</div>
</div>

</form>

