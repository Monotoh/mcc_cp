<?php use_helper('I18N') ?>
<?php slot('sf_apply_login') ?>
<?php end_slot() ?>


<div class="contentpanel">

    <div class="row">
        <div class="col-sm-6 col-lg-8">
            <div class="card-box">
                 <h4 class="text-dark  header-title m-t-0"><?php echo __('Change password') ?></h4>
                    <p class="text-muted m-b-25 font-13">
                        <?php echo __('You can now change your password. Please provide below details') ?>
                    </p>
                    <form  class="form-bordered form-horizontal" action="<?php echo url_for("sfApply/changePassword") ?>" name="sf_apply_reset_form" id="sf_apply_reset_form" method="post" enctype="multipart/form-data"   autocomplete="off" data-ajax="false">
                        <div class="panel-body panel-body-nopadding"> 
                          <ul style="list-style: none; margin:0;">
                        <?php echo $form ?>
                        <li>
                        <input type="submit" class="btn btn-info" value="<?php echo __("Reset My Password") ?>">
                        <?php echo __("or") ?> 
                        <?php echo link_to(__('Cancel'), 'sfApply/resetCancel') ?>
                        </li>
</ul>
                        </div>
                    </form>
            </div>
            
        </div>
    </div>
    
  </div>
