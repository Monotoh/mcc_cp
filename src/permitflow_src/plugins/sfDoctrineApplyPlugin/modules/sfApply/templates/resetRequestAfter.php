<?php use_helper('I18N') ?>
<?php slot('sf_apply_login') ?>
<?php end_slot() ?>
  <div class="content">
<div class="breadcrumb-box">
<div class="container">
<ul class="breadcrumb">
              <li><a href="#"><?php echo __('Home'); ?></a> <span class="divider">/</span></li>
              <li class="active"><?php echo __('Forgot Your Password?'); ?></li>
            </ul>    <!-- Docs nav
    ================================================== -->
            </div>
            </div>
<div class="row">
<div class="container">
<div class="twelve columns offset-by-three">
          <div class="panel panel-default">
            <div class="panel-heading">
			<h4 class="panel-title"><?php echo __('Check Your E-Mail'); ?></h4>
			</div><!-- panel-heading -->
            <div class="panel-body">
              <div class="alert alert-info">
<?php echo __('For security reasons, a confirmation message has been sent to 
the email address associated with this account. Please check your
email for that message. You will need to click on a link provided
in that email in order to change your password. If you do not see
the message, be sure to check your "spam" and "bulk" email folders.'); ?>
<br>
<br>
</div>
            </div>
          </div><!-- panel -->
	
</div>
</div>


  

