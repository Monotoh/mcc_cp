<?php
	$user = $sf_user->getGuarduser();
?>
<?php use_helper('I18N') ?>
<div class="pageheader">
  <h2><i class="fa fa-user"></i>&nbsp; <span><?php echo __('My Profile'); ?></span></h2>
<!--  <div class="breadcrumb-wrapper">-->
<!--    <span class="label">--><?php //echo __('You are here'); ?><!--:</span>-->
<!--    <ol class="breadcrumb">-->
<!--      <li><a href="--><?php //echo public_path(); ?><!--index.php">--><?php //echo __('Home'); ?><!--</a></li>-->
<!--      <li><a href="--><?php //echo public_path(); ?><!--index.php/settings">--><?php //echo __('My Account'); ?><!--</a></li>-->
<!--    </ol>-->
<!--  </div>-->
</div>
<div class="row">

<?php //include_partial('index/sidemenu', array('' => '')) ?>

<div class="contentpanel">
    <div class="row">
<div class="col-sm-12">

    <div class="panel panel-default">
         <div class="panel-body panel-body-nopadding">

           <!-- BASIC WIZARD -->
            <div id="basicWizard" class="basic-wizard bordered-wizard">
              <ul class="nav nav-tabs nav-justified">
						<li class="active"><a href="#tabs-1"  data-toggle="tab"><?php echo __('Edit Basic Details'); ?></a></li>
						<li><a href="#tabs-2"  data-toggle="tab"><?php echo __('Edit Additional Details'); ?></a></li>
						<li><a href="#tabs-3"  data-toggle="tab"><?php echo __('My Activity'); ?></a></li>
					</ul>
		      <div class="tab-content tab-content-nopadding">
					<div class="tab-pane active" id="tabs-1">
					 <form class="form-bordered form-horizontal">
						<div class="form-group">
					</div>
						<div class="form-group">
       				 	<label class="col-sm-2 control-label">
           				<i class="bold-label-2"><?php echo __('Full Name'); ?></i>
        				 </label>
    				<div class="col-sm-10">
    					<?php echo $user->getProfile()->getFullname(); ?>
    				</div>
					</div>
						<div class="form-group">
       				 	<label class="col-sm-2 control-label">
           				<i class="bold-label-2"><?php echo __('Email Address'); ?></i>
        				 </label>
    				<div class="col-sm-10">
    					<?php echo $user->getProfile()->getEmail(); ?>
    				</div>
					</div>
						<div class="form-group">
       				 	<label class="col-sm-2 control-label">
           				<i class="bold-label-2"><?php echo __('Username'); ?></i>
        				 </label>
    				<div class="col-sm-10">
    					<?php echo $user->getUsername(); ?>
    				</div>
					</div>
						<div class="form-group">
       				 	<label class="col-sm-2 control-label">
           				<i class="bold-label-2"><?php if($user->getLastLogin()){ echo $user->getLastLogin(); }else{ echo "<i>Never logged in</i>"; } ?></i>
        				 </label>
    				<div class="col-sm-10">
    					<?php echo $user->getUsername(); ?>
    				</div>
					</div>
						<div class="form-group">
       				 	<label class="col-sm-2 control-label">
           				<i class="bold-label-2"><?php echo __('Is Active?'); ?></i>
        				 </label>
    				<div class="col-sm-10">
    					<?php if($user->getIsActive() == "1"){ echo __("Yes"); }else{ echo __("No"); } ?>
    				</div>
					</div>
						<div class="form-group">
       				 	<label class="col-sm-2 control-label">
           				<i class="bold-label-2"><?php echo __('Email is Validated?'); ?></i>
        				 </label>
    				<div class="col-sm-10">
    					<?php if($user->getIsSuperAdmin() == "1"){ echo __("Yes"); }else{ echo __("No"); } ?>
    				</div>
					</div>
                    </form>
                   	</div>


                    <div class="tab-pane fade" id="tabs-2">
                       <?php
$q = Doctrine_Query::create()
	      ->from('mfUserProfile a')
		  ->where('a.user_id = ?', $user->getId());
$profile = $q->fetchOne();
?>
<form class="form-bordered form-horizontal">
<?php
if($profile)
{
?>

	<div class="form-group">
   		<div class="col-sm-12">
   			<a class="btn btn-default pull-right" href="/index.php/frusers/editadditional/formid/15/entryid/<?php echo $profile->getEntryId(); ?>" > <?php echo __('Edit Additional Details'); ?></a>
		</div>
	</div>

		<?php
		include_partial('frusers/listinfo', array('form_id' => $profile->getFormId(),'entry_id' => $profile->getEntryId()));
		?>
		<?php
		}
		else
		{
		$form_id = 15;
		if($user->getProfile()->getRegisteras())
		{
		$q = Doctrine_Query::create()
			->from("SfGuardUserCategories a")
			->where("a.id = ?", $user->getProfile()->getRegisteras());
	    $category = $q->fetchOne();
	    if($category)
	    {
			$form_id = $category->getFormId();
		}
	}
	?>
	<div class="form-group">
   		<div class="col-sm-12">
   			<a class="btn btn-default pull-right" href="/index.php/mfRegister/registerDetails2?formid=<?php echo $form_id; ?>" > <?php echo __('Add Additional Details'); ?></a>
		</div>
	</div>

	<?php
}
?>
 </form>
                    </div>
                    <div class="tab-pane fade" id="tabs-3">
	                 <table class="table table-hover mb0">
	<thead>
		<tr>
			<th><?php echo __('ID'); ?></th><th width="200px"><?php echo __('Date/Time'); ?></th><th><?php echo __('Action'); ?></th>
		</tr>
	</thead>
	<tbody>
	<?php
			$q = Doctrine_Query::create()
					->from('Activity a')
					->where('a.user_id = ?', $user->getId());
			$activities = $q->execute();
			$count = 1;
			foreach($activities as $activity)
			{
	?>
		<tr class="gradeX">
			<td><?php echo $count++; ?></td>
			<td><?php
			echo $activity->getActionTimestamp();
			?></td>
			<td><?php echo $activity->getAction(); ?></td>
		</tr>
		<?php
			}
	?>
	</tbody>
</table>
</div>
                  </div>
            </div>

        </div>
    </div>


</div><!--span12-->

    </div><!--row-->
</div>
