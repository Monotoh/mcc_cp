<?php

/*
 * This file is part of the symfony package.
 * (c) 2004-2006 Fabien Potencier <fabien.potencier@symfony-project.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 *
 * @package    symfony
 * @subpackage plugin
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: BasesfGuardAuthActions.class.php 23800 2009-11-11 23:30:50Z Kris.Wallsmith $
 */
class BasesfGuardAuthActions extends sfActions
{
  public function executeSignin($request)
  {
    $user = $this->getUser();
    if ($user->isAuthenticated())
    {
      return $this->redirect('@homepage');
    }

    $class = sfConfig::get('app_sf_guard_plugin_signin_form', 'sfGuardFormSignin');
    $this->form = new $class();

    if ($request->isMethod('post'))
    {
      $this->form->bind($request->getParameter('signin'));
      if ($this->form->isValid())
      {
        $values = $this->form->getValues();

    		if($values['user'])
    		{
             $sorry = $this->getContext()->getI18N()->__("Sorry! The username or password you entered is incorrect.", $args, 'messages');
             $forget = $this->getContext()->getI18N()->__("Did you forget?", $args, 'messages');
    		     if($values['user']->getIsSuperAdmin() == FALSE){
    			   $this->getUser()->setFlash("notice", $sorry."<a onClick=\"window.location='/index.php/reset-request';\">".$forget."</a> ");
    				 return $this->redirect("/index.php/login");
    			}
    		}
        $this->getUser()->signin($values['user'], array_key_exists('remember', $values) ? $values['remember'] : false);

        // always redirect to a URL set in app.yml
        // or to the referer
        // or to the homepage
        $signinUrl = sfConfig::get('app_sf_guard_plugin_success_signin_url', $user->getReferer($request->getReferer()));



        return $this->redirect('@homepage');
      }
  	  else
  	  {
  			     $this->getUser()->setFlash("notice", "<strong>Sorry!</strong> The username or password you entered is incorrect. <a onClick=\"window.location='http://www.kcps.gov.rw/index.php/reset-request';\">Did you forget?</a> ");
  				 return $this->redirect("/");
  	  }
    }
    else
    {

      if ($request->isXmlHttpRequest())
      {
        $this->getResponse()->setHeaderOnly(true);
        $this->getResponse()->setStatusCode(401);

        return sfView::NONE;
      }

      // if we have been forwarded, then the referer is the current URL
      // if not, this is the referer of the current request
      $user->setReferer($this->getContext()->getActionStack()->getSize() > 1 ? $request->getUri() : $request->getReferer());



      $module = sfConfig::get('sf_login_module');
      if ($this->getModuleName() != $module)
      {
        return $this->redirect($module.'/'.sfConfig::get('sf_login_action'));
      }

      $this->getResponse()->setStatusCode(401);
    }
     $this->setLayout("layout");
  }

  public function executeSignout($request)
  {
    $this->getUser()->signOut();


    $this->redirect('@homepage');
  }

  public function executeSecure($request)
  {
    $this->getResponse()->setStatusCode(403);
  }

  public function executePassword($request)
  {
    throw new sfException('This method is not yet implemented.');
  }
}
