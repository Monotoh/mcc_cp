<?php use_helper('I18N') ?>

<form action="<?php echo url_for('@sf_guard_signin') ?>" method="post" autocomplete="off" data-ajax="false">
<fieldset>
<label>User Login</label>
<section>
     <label class="description">Username</label>
	 <div>
	 <input type="text" class="element text" name="signin[username]" id="signin_username" style="width: 500px;" value=""></span>
     </div>
</section>
<section>
    <label class="description">Password</label>
	<div>
    <input type="password" class="element text" name="signin[password]" id="signin_password" style="width: 500px;"></span>
    </div>
</section>
<section>
    <div style="width: 95%;">
		Remember me? <input type="checkbox" name="signin[remember]" id="signin_remember">
	</div>
</section>
<section>
	<div id="element_div_1" name="element_div_1" style="width: 95%;" class="highlighted">
		<a href="/index.php/reset-request">Forgot Password?</a> | <a href="/index.php/apply">Not a member? Sign-up here.</a>
	</div>
</section>
<section>
	<div style="width: 95%;">
		<input id="saveForm" class="button_text" type="submit" name="submit" value="Continue" style="padding: 5px;">
	</div>
</section>
</form>