<?php

class BasesfGuardRegisterActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    if ($this->getUser()->isAuthenticated())
    {
      $this->getUser()->setFlash('notice', 'You are already registered and signed in!');
      $this->redirect('@homepage');
    }

    $this->form = new sfGuardRegisterForm();

    if ($request->isMethod('post'))
    {
      $this->form->bind($request->getParameter($this->form->getName()));
      if ($this->form->isValid())
      {
		include '/securimage/securimage.php';
		$image = new Securimage();
        if ($image->check($request->getPostParameter('sf_guard_user[security_code]')) != true) {
			$this->getUser()->setFlash("notice","Error. Security code doesnt match.");
		}
		else
		{
			$user = $this->form->save();
			//$this->getUser()->signIn($user);
	
			//$this->redirect('@homepage');
			$this->getUser()->setAttribute('new_user_id',$user->getId());
			$this->redirect('/index.php/mfRegister/registerDetails?formid=15');
		}
      }
    }
     $this->setLayout("layout");
  }
}