<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

$browser = new PermitflowTestFunctional(new sfBrowser());
$browser->loadData();

$browser->
	info('1 - The FAQ')->
	get('/help/faq')->
	with('request')->begin()->
		isParameter('module', 'help')->
		isParameter('action', 'faq')->
	end()->

	info('1.1 - Content of the faq page')->
	with('response')->begin()->
		checkElement('.container:contains("FAQ One")', true)->
		checkElement('.container:contains("First Frequently Asked Question")', true)->
		checkElement('.container:contains("FAQ Two")', true)->
		checkElement('.container:contains("Second Frequently Asked Question")', true)->
	end()->

	info('2 - The Contact Us Page')->
	get('/help/contact')->
	with('request')->begin()->
		isParameter('module', 'help')->
		isParameter('action', 'contact')->
	end()->

	info('2.1 - Content of the contact us page')->
	with('response')->begin()->
		checkElement('.container:contains("Name")', true)->
		checkElement('.container:contains("Email")', true)->
		checkElement('.container:contains("Your Message")', true)->
	end() ;
