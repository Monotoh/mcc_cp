<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

$browser = new PermitflowTestFunctional(new sfBrowser());
$browser->loadData();

$browser->
	info('1 - Check News Index')->
	get('/news')->
	with('request')->begin()->
		isParameter('module', 'news')->
		isParameter('action', 'index')->
	end()->

	info('1.1 - Content of the News Index')->
	with('response')->begin()->
		checkElement('.container:contains("News Article 1")', true)->
		checkElement('.container:contains("This is a sample news article 1")', true)->
		checkElement('.container:contains("News Article 2")', true)->
		checkElement('.container:contains("This is a sample news article 2")', true)->
	end()->

	info('2 - Check News Article Page')->
	get('/news/article/id/2')->
	with('request')->begin()->
		isParameter('module', 'news')->
		isParameter('action', 'article')->
		isParameter('id', '2')->
	end()->

	info('2.1 - Check News Article Page Content')->
	with('response')->begin()->
		checkElement('.container:contains("News Article 2")', true)->
		checkElement('.container:contains("This is a sample news article 2")', true)->
	end() ;