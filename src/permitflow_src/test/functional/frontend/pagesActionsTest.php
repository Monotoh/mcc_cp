<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

$browser = new PermitflowTestFunctional(new sfBrowser());
$browser->loadData();

$browser->
	info('1 - The homepage')->
	get('/')->
	with('request')->begin()->
		isParameter('module', 'pages')->
		isParameter('action', 'index')->
	end()->

	info('1.1 - Content of the home page should match test data')->
	with('response')->begin()->
		checkElement('.container:contains("Welcome to the Government eCitizen portal")', true)->
	end()->

	info('2 - The SetLocale')->
	get('/pages/setlocale/code/bt_BD')->
	with('request')->begin()->
		isParameter('module', 'pages')->
		isParameter('action', 'setlocale')->
		isParameter('code', 'bt_BD')->
	end()->

	info('2.1 - Check if user is redirected after locale is set')->
	with('response')->begin()->
		isRedirected()->
	end() ;