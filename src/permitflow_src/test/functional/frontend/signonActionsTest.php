<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

$browser = new PermitflowTestFunctional(new sfBrowser());
$browser->loadData();

//Generate test data
$data = array('email' => 'user@example.com', 'id_number' => '12345678', 'first_name' => 'Jenny', 'last_name' => 'Doe', 'mobile_number' => '254123456', 'at' => new DateTime());
$signature = base64_encode(hash_hmac('sha256', json_encode($data), sfConfig::get('app_sso_secret'), true));
$data['signature'] = $signature;
$payload = base64_encode(json_encode($data));

$browser->
    //Check if single sign on login link is correct
	info('1.1 - The Single Sign On Login Page')->
	get('/signon/login')->
	with('request')->begin()->
		isParameter('module', 'signon')->
		isParameter('action', 'login')->
	end()->

	//Check if single sign on login link redirects to remote single sign on page
	info('1.2 - Check Single Sign On Login Redirect')->
	with('response')->begin()->
		isRedirected()->
	end()->

    //Check if single sign on logout link is correct
	info('1.3 - The Single Sign On Logout Page')->
	get('/signon/logout')->
	with('request')->begin()->
		isParameter('module', 'signon')->
		isParameter('action', 'logout')->
	end()->

	//Check if single sign on logout link redirects to remote single sign on page
	info('1.4 - Check Single Sign On Logout Redirect')->
	with('response')->begin()->
		isRedirected()->
	end()->

    //Check if single sign on registration link is correct
	info('1.5 - The Single Sign On Register Page')->
	get('/signon/register')->
	with('request')->begin()->
		isParameter('module', 'signon')->
		isParameter('action', 'register')->
	end()->

	//Check if single sign on registration link redirects to remote single sign on page
	info('1.6 - Check Single Sign On Register Redirect')->
	with('response')->begin()->
		isRedirected()->
	end()->

	//Check if single sign on registration link is correct
	info('2 - The Single Sign On Signin Page')->
	get('/signon/signin/data/'.$payload)->
	with('request')->begin()->
		isParameter('module', 'signon')->
		isParameter('action', 'signin')->
		isParameter('data', $payload)->
	end()->

	info('2.1 - Check Single Sign On Signin Redirect')->
	with('response')->begin()->
		isRedirected()->
	end() ;
