<?php
// test/unit/AuditTest.php

require_once dirname(__FILE__).'/../bootstrap/unit.php';

$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', 'test', true);
new sfDatabaseManager($configuration);

Doctrine_Core::loadData(sfConfig::get('sf_test_dir').'/fixtures');

require_once dirname(__FILE__).'/../../lib/audit.class.php';

error_reporting(0);

$t = new lime_test(14);

$t->comment('Audit trail library tests');

//simulate a user session
$_SESSION['SESSION_CUTEFLOW_USERID'] = 123;

//initialized audit trail class
$audit = new audit();

//test simple audit class
$audit->saveAudit(1, 'test action 1');

$q = Doctrine_Query::create()
   ->from('AuditTrail a')
   ->orderBy('a.id DESC');

$latest_audit =  $q->fetchOne();

//check simple audit fields
$t->is($latest_audit->getUserId(), 123,'->getUserId() returns the user id of the action');
$t->is($latest_audit->getFormEntryId(), 1,'->getFormEntryId() returns the application id of the action');
$t->is($latest_audit->getAction(), "<a href='http://'>test action 1</a>",'->getAction() returns the content of the action');
$t->is($latest_audit->getIpaddress(), $_SERVER['REMOTE_ADDR'],'->getIpaddress() returns the ip address of the user');
$t->is($latest_audit->getHttpAgent(), $_SERVER['HTTP_USER_AGENT'],'->getHttpAgent() returns the browser/device of the user');


//test full audit class
$audit->saveFullAudit('test action 2',2,'groups','title:users1','title:users2', 3);

$q = Doctrine_Query::create()
   ->from('AuditTrail a')
   ->orderBy('a.id DESC');

$latest_audit =  $q->fetchOne();

//check full audit fields
$t->is($latest_audit->getUserId(), 123,'->getUserId() returns the user id of the action');
$t->is($latest_audit->getFormEntryId(), 3,'->getFormEntryId() returns the application id of the action');
$t->is($latest_audit->getAction(), 'test action 2','->getAction() returns the content of the action');
$t->is($latest_audit->getObjectId(), 2,'->getAction() returns the content of the action');
$t->is($latest_audit->getObjectName(), 'groups','->getAction() returns the content of the action');
$t->is($latest_audit->getBeforeValues(), 'title:users1','->getAction() returns the content of the action');
$t->is($latest_audit->getAfterValues(), 'title:users2','->getAction() returns the content of the action');
$t->is($latest_audit->getIpaddress(), $_SERVER['REMOTE_ADDR'],'->getIpaddress() returns the ip address of the user');
$t->is($latest_audit->getHttpAgent(), $_SERVER['HTTP_USER_AGENT'],'->getHttpAgent() returns the browser/device of the user');
