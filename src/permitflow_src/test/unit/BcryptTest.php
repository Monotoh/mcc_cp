<?php
// test/unit/BcryptTest.php

require_once dirname(__FILE__).'/../bootstrap/unit.php'; 
include(dirname(__FILE__).'/../../lib/bcrypt.php'); 

$t = new lime_test(6);


$t->comment('Bcrypt ->password_hash() and ->password_verify() function tests');

//hash the password using bcrypt. This is the version that gets stored in the database
$hash1 = password_hash("admin", PASSWORD_BCRYPT);
	//Re-hash again to simulate a second login attempt
	$hash_try1 = password_hash("admin", PASSWORD_BCRYPT);

$hash2 = password_hash("test1234", PASSWORD_BCRYPT);
	//Re-hash again to simulate a second login attempt
	$hash_try2 = password_hash("test1234", PASSWORD_BCRYPT);

//verify the hash to confirm bcrypt is working
$t->is(password_verify("admin", $hash1), true,'->password_verify(admin, '.$hash_try1.') the first password');
$t->is(password_verify("test1234", $hash2), true,'->password_verify(test1234, '.$hash_try2.') the second password');
$t->is(password_verify("admin", $hash_try1), true,'->password_verify(admin, '.$hash_try1.') the first try password');
$t->is(password_verify("test1234", $hash_try2), true,'->password_verify(test1234, '.$hash_try2.') the second try password');

//verify the hash to confirm bcrypt doesn't work for different password that aren't correct
$t->is(password_verify("admin1234", $hash1), false,'->password_verify('.$hash1.', '.$fakehash1.') the first password with a wrong hash');
$t->is(password_verify("1234test", $hash2), false,'->password_verify('.$hash2.', '.$fakehash2.') the second password with a wrong hash');