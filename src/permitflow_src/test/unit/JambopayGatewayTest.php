<?php
// test/unit/AuditTest.php

require_once dirname(__FILE__).'/../bootstrap/unit.php';

$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', 'test', true);
new sfDatabaseManager($configuration);

Doctrine_Core::loadData(sfConfig::get('sf_test_dir').'/fixtures');

error_reporting(0);

$t = new lime_test(11);

$t->comment('Jambopay gateway test of checkout, validation and ipn functions');

$payments_manager = new PaymentsManager();
$application_manager = new ApplicationManager();
$invoice_manager = new InvoiceManager();
$jambopay_gateway = new JambopayGateway();

//Variables and data to be tested
  $q = Doctrine_Query::create()
     ->from("SfGuardUser a")
     ->where("a.username = ?", "12345678");
  $sfGuardUser = $q->fetchOne();

  if($sfGuardUser)
  {
    //Do Nothing
  }
  else
  {
    // create user
    $sfGuardUser = new sfGuardUser();
    $sfGuardUser->username = "12345678";
    $sfGuardUser->email_address = "campfossa@gmail.com";
    $sfGuardUser->save();

    // create user profile
    $profile = new sfGuardUserProfile();
    $profile->user_id = $sfGuardUser->id;
    $profile->fullname = "John Doe";
    $profile->email = "campfossa@gmail.com";
    $profile->mobile = "0701123123";
    $profile->registeras = $account_type ? $account_type : 1;
    $profile->save();
  }

  $q = Doctrine_Query::create()
     ->from("ApForms a")
     ->where("a.form_name LIKE ?", "%1. Application For Provisional Driving License%");
  $form = $q->fetchOne();

  $form_id = $form->getFormId();
  $new_record_id = 1;

  $submission = $application_manager->create_application($form_id, $new_record_id, $sfGuardUser->getId(), true);

  $application_manager->update_invoices($submission->getId());

  $invoice_id = $invoice_manager->get_unpaid_invoice($submission->getId());

  $payment_settings = $payments_manager->get_payment_settings($invoice_id);

//Test Checkout

  //Frontend
  $frontend_checkout = $jambopay_gateway->checkout($invoice_id, $payment_settings, false);

  $reference = $form_id."/".$new_record_id."/".$invoice_id;

  $callback_url = 'https://' . $_SERVER['HTTP_HOST'] . '/index.php/forms/confirmpayment?id=' . $submission->getFormId() . '&entryid=' . $submission->getEntryId() . "&done=1&invoiceid=" . $invoice_id;

  $targets = array();
  $targets[] = 'name="jp_item_type" value="cart"';
  $targets[] = 'name="jp_item_name" value="1. Application For Provisional Driving License"';
  $targets[] = 'name="order_id" value="'.$reference.'"';
  $targets[] = 'name="jp_business" value="'.$payment_settings['payment_jambopay_business'].'"';
  $targets[] = 'name="jp_amount_1" value="650"';
  $targets[] = 'name="jp_amount_2" value="0"';
  $targets[] = 'name="jp_amount_5" value="0"';
  $targets[] = 'name="jp_payee" value="campfossa@gmail.com"';
  $targets[] = 'name="jp_shipping" value="eCitizen"';
  $targets[] = 'name="jp_rurl" value="'.$callback_url.'&status=2"';
  $targets[] = 'name="jp_furl" value="'.$callback_url.'&status=1"';
  $targets[] = 'name="jp_curl" value="'.$callback_url.'&status=0"';

  $found_count = 0;

  foreach($targets as $tg)
  {
      if (strpos($frontend_checkout,$tg) !== false) {
          $found_count++;
      }
      else
      {
        error_log("Unmatched Frontend Checkout Item: ".$tg);
      }
  }

  $t->is($found_count, 12,'->checkout() Frontend checkout iframe rendered');

  //Backend
  $backend_checkout = $jambopay_gateway->checkout($invoice_id, $payment_settings, true);

  $callback_url = 'https://' . $_SERVER['HTTP_HOST'] . '/index.php/forms/confirmpayment?id=' . $submission->getFormId() . '&entryid=' . $submission->getEntryId() . "&done=1&invoiceid=" . $invoice_id;

  $targets = array();
  $targets[] = 'name="jp_item_type" value="cart"';
  $targets[] = 'name="jp_item_name" value="1. Application For Provisional Driving License"';
  $targets[] = 'name="order_id" value="'.$reference.'"';
  $targets[] = 'name="jp_business" value="'.$payment_settings['payment_jambopay_business'].'"';
  $targets[] = 'name="jp_amount_1" value="650"';
  $targets[] = 'name="jp_amount_2" value="0"';
  $targets[] = 'name="jp_amount_5" value="0"';
  $targets[] = 'name="jp_payee" value="campfossa@gmail.com"';
  $targets[] = 'name="jp_shipping" value="eCitizen"';
  $targets[] = 'name="jp_rurl" value="'.$callback_url.'&status=2"';
  $targets[] = 'name="jp_furl" value="'.$callback_url.'&status=1"';
  $targets[] = 'name="jp_curl" value="'.$callback_url.'&status=0"';

  $found_count = 0;

  foreach($targets as $tg)
  {
      if (strpos($frontend_checkout,$tg) !== false) {
          $found_count++;
      }
      else
      {
        error_log("Unmatched Frontend Checkout Item: ".$tg);
      }
  }

  $t->is($found_count, 12,'->checkout() Backend checkout iframe rendered');

//Test Validation

  $sharedkey = $payment_settings['payment_jambopay_shared_key'];

  //Successful Payments
  $request_details = array();
  $request_details["JP_TRANID"] = $reference;
  $request_details["status"] = 2;
  $request_details['JP_MERCHANT_ORDERID'] = $reference;
  $request_details['JP_ITEM_NAME'] = "1. Application For Provisional Driving License";
  $request_details['JP_AMOUNT'] = "650";
  $request_details['JP_CURRENCY'] = "KES";
  $request_details['JP_TIMESTAMP'] = "2015-04-22";
  $request_details['JP_PASSWORD'] = md5((utf8_encode($reference."650"."KES".$sharedkey."2015-04-22")));
  $request_details['JP_CHANNEL'] = "1";

  $q = Doctrine_Query::create()
     ->from("MfInvoice a")
     ->where("a.id = ?", $invoice_id);
  $invoice = $q->fetchOne();
  $invoice->setPaid(1);
  $invoice->save();

  $expected_validation_results = true;
  $validation_results = $jambopay_gateway->validate($invoice_id, $request_details, $payment_settings);
  $t->is($validation_results, $expected_validation_results,'->validate() Validate successful payments after redirect from checkout');
    $q = Doctrine_Query::create()
       ->from("MfInvoice a")
       ->where("a.id = ?", $invoice_id);
    $invoice = $q->fetchOne();
  $t->is($invoice->getPaid(), 2,'->validate() Invoice status after successful payment');

  //Failed Payments
  $request_details = array();
  $request_details["JP_TRANID"] = $reference;
  $request_details["status"] = 1;
  $request_details['JP_MERCHANT_ORDERID'] = $reference;
  $request_details['JP_ITEM_NAME'] = "1. Application For Provisional Driving License";
  $request_details['JP_AMOUNT'] = "650";
  $request_details['JP_CURRENCY'] = "KES";
  $request_details['JP_TIMESTAMP'] = "2015-04-22";
  $request_details['JP_PASSWORD'] = md5((utf8_encode($reference."650"."KES".$sharedkey."2015-04-22")));
  $request_details['JP_CHANNEL'] = "1";

  $q = Doctrine_Query::create()
     ->from("MfInvoice a")
     ->where("a.id = ?", $invoice_id);
  $invoice = $q->fetchOne();
  $invoice->setPaid(1);
  $invoice->save();

  $expected_validation_results = true;
  $validation_results = $jambopay_gateway->validate($invoice_id, $request_details, $payment_settings);
  $t->is($validation_results, $expected_validation_results,'->validate() Validate failed payments after redirect from checkout');
    $q = Doctrine_Query::create()
       ->from("MfInvoice a")
       ->where("a.id = ?", $invoice_id);
    $invoice = $q->fetchOne();
  $t->is($invoice->getPaid(), 3,'->validate() Invoice status after failed payment');

  //Pending Payments
  $request_details = array();
  $request_details["JP_TRANID"] = $reference;
  $request_details["status"] = 0;
  $request_details['JP_MERCHANT_ORDERID'] = $reference;
  $request_details['JP_ITEM_NAME'] = "1. Application For Provisional Driving License";
  $request_details['JP_AMOUNT'] = "650";
  $request_details['JP_CURRENCY'] = "KES";
  $request_details['JP_TIMESTAMP'] = "2015-04-22";
  $request_details['JP_PASSWORD'] = md5((utf8_encode($reference."650"."KES".$sharedkey."2015-04-22")));
  $request_details['JP_CHANNEL'] = "1";

  $q = Doctrine_Query::create()
     ->from("MfInvoice a")
     ->where("a.id = ?", $invoice_id);
  $invoice = $q->fetchOne();
  $invoice->setPaid(1);
  $invoice->save();

  $expected_validation_results = true;
  $validation_results = $jambopay_gateway->validate($invoice_id, $request_details, $payment_settings);
  $t->is($validation_results, $expected_validation_results,'->validate() Validate pending payments after redirect from checkout');
    $q = Doctrine_Query::create()
       ->from("MfInvoice a")
       ->where("a.id = ?", $invoice_id);
    $invoice = $q->fetchOne();
  $t->is($invoice->getPaid(), 15,'->validate() Invoice status after pending payment');

//Test IPN

  //Successful Payments
  $request_details = array();
  $request_details["JP_TRANID"] = $reference;
  $request_details["status"] = 2;
  $request_details['JP_MERCHANT_ORDERID'] = $reference;
  $request_details['JP_ITEM_NAME'] = "1. Application For Provisional Driving License";
  $request_details['JP_AMOUNT'] = "650";
  $request_details['JP_CURRENCY'] = "KES";
  $request_details['JP_TIMESTAMP'] = "2015-04-22";
  $request_details['JP_PASSWORD'] = md5((utf8_encode($reference."650"."KES".$sharedkey."2015-04-22")));
  $request_details['JP_CHANNEL'] = "1";

  $q = Doctrine_Query::create()
     ->from("MfInvoice a")
     ->where("a.id = ?", $invoice_id);
  $invoice = $q->fetchOne();
  $invoice->setPaid(1);
  $invoice->save();

  $expected_ipn_results = array();
  $expected_ipn_results['invoice_status'] = 'paid';
  $expected_ipn_results['total_amount'] = '650';
  $expected_ipn_results['currency'] = 'KES';
  $expected_ipn_results['date_of_invoice'] = $invoice->getCreatedAt();
  $expected_ipn_results['application_id'] = $invoice->getFormEntry()->getApplicationId();
  $expected_ipn_results['user_email'] = 'campfossa@gmail.com';
  $expected_ipn_results['user_mobile'] = '0701123123';
  $expected_ipn_results['user_fullname'] = 'John Doe';

  $ipn_results = $jambopay_gateway->ipn($request_details);
  $t->is($ipn_results, $expected_ipn_results,'->ipn() IPN for successful payments');

  //Failed Payments
  $request_details = array();
  $request_details["JP_TRANID"] = $reference;
  $request_details["status"] = 1;
  $request_details['JP_MERCHANT_ORDERID'] = $reference;
  $request_details['JP_ITEM_NAME'] = "1. Application For Provisional Driving License";
  $request_details['JP_AMOUNT'] = "650";
  $request_details['JP_CURRENCY'] = "KES";
  $request_details['JP_TIMESTAMP'] = "2015-04-22";
  $request_details['JP_PASSWORD'] = md5((utf8_encode($reference."650"."KES".$sharedkey."2015-04-22")));
  $request_details['JP_CHANNEL'] = "1";

  $q = Doctrine_Query::create()
     ->from("MfInvoice a")
     ->where("a.id = ?", $invoice_id);
  $invoice = $q->fetchOne();
  $invoice->setPaid(1);
  $invoice->save();

  $expected_ipn_results = array();
  $expected_ipn_results['invoice_status'] = 'cancelled';
  $expected_ipn_results['total_amount'] = '650';
  $expected_ipn_results['currency'] = 'KES';
  $expected_ipn_results['date_of_invoice'] = $invoice->getCreatedAt();
  $expected_ipn_results['application_id'] = $invoice->getFormEntry()->getApplicationId();
  $expected_ipn_results['user_email'] = 'campfossa@gmail.com';
  $expected_ipn_results['user_mobile'] = '0701123123';
  $expected_ipn_results['user_fullname'] = 'John Doe';

  $ipn_results = $jambopay_gateway->ipn($request_details);
  $t->is($ipn_results, $expected_ipn_results,'->ipn() IPN for failed payments');

  //Pending Payments
  $request_details = array();
  $request_details["JP_TRANID"] = $reference;
  $request_details["status"] = 0;
  $request_details['JP_MERCHANT_ORDERID'] = $reference;
  $request_details['JP_ITEM_NAME'] = "1. Application For Provisional Driving License";
  $request_details['JP_AMOUNT'] = "650";
  $request_details['JP_CURRENCY'] = "KES";
  $request_details['JP_TIMESTAMP'] = "2015-04-22";
  $request_details['JP_PASSWORD'] = md5((utf8_encode($reference."650"."KES".$sharedkey."2015-04-22")));
  $request_details['JP_CHANNEL'] = "1";

  $q = Doctrine_Query::create()
     ->from("MfInvoice a")
     ->where("a.id = ?", $invoice_id);
  $invoice = $q->fetchOne();
  $invoice->setPaid(1);
  $invoice->save();

  $expected_ipn_results = array();
  $expected_ipn_results['invoice_status'] = 'pending confirmation';
  $expected_ipn_results['total_amount'] = '650';
  $expected_ipn_results['currency'] = 'KES';
  $expected_ipn_results['date_of_invoice'] = $invoice->getCreatedAt();
  $expected_ipn_results['application_id'] = $invoice->getFormEntry()->getApplicationId();
  $expected_ipn_results['user_email'] = 'campfossa@gmail.com';
  $expected_ipn_results['user_mobile'] = '0701123123';
  $expected_ipn_results['user_fullname'] = 'John Doe';

  $ipn_results = $jambopay_gateway->ipn($request_details);
  $t->is($ipn_results, $expected_ipn_results,'->ipn() IPN for pending payments');
