<?php
// test/unit/AuditTest.php

require_once dirname(__FILE__).'/../bootstrap/unit.php';

$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', 'test', true);
new sfDatabaseManager($configuration);

Doctrine_Core::loadData(sfConfig::get('sf_test_dir').'/fixtures');

$t = new lime_test(1);

$t->comment('MfUser library tests for checking reviewer credentials');

$q = Doctrine_Query::create()
   ->from("CfUser a")
   ->where("a.bdeleted = 0")
   ->orderBy("a.nid ASC");

$admin_reviewer = $q->fetchOne(); //From test fixtures

$_SESSION['SESSION_CUTEFLOW_USERID'] = $admin_reviewer->getNid();

//Complications with symfony forced me to copy the entire function here for testing
function mfHasCredential($credential)
{
	$q = Doctrine_Query::create()
		->from('mfGuardUserGroup a')
		->leftJoin('a.MfGuardGroup b')
		->leftJoin('b.MfGuardGroupPermission c') //Left Join group permissions
		->leftJoin('c.MfGuardPermission d') //Left Join permissions
		->where('a.user_id = ?', $_SESSION['SESSION_CUTEFLOW_USERID'])
		->andWhere('d.name = ?', $credential);
	$usergroups = $q->execute();
	if(sizeof($usergroups) > 0)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

$t->ok(mfHasCredential('test_role'),'->mfHadCredential() checks if the user has the specified credential');