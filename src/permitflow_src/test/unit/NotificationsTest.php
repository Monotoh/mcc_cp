<?php
// test/unit/AuditTest.php

require_once dirname(__FILE__).'/../bootstrap/unit.php';

$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', 'test', true);
new sfDatabaseManager($configuration);

Doctrine_Core::loadData(sfConfig::get('sf_test_dir').'/fixtures');

require_once dirname(__FILE__).'/../../lib/notifications.class.php';

$t = new lime_test(2);

$t->comment('Notifications library tests for checking Mail and SMS');

$notifications = new mailnotifications();

//mail test
$t->ok($notifications->sendemail("info@masterlmis.com","thomasjgx@gmail.com","test", "test email"),'->sendemail() sends an email notification');

//sms test
$t->ok($notifications->sendsms("+254703138826", "test message"),'->sendsms() sends an sms');