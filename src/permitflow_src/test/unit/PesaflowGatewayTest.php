<?php
// test/unit/AuditTest.php

require_once dirname(__FILE__).'/../bootstrap/unit.php';

$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', 'test', true);
new sfDatabaseManager($configuration);

Doctrine_Core::loadData(sfConfig::get('sf_test_dir').'/fixtures');

error_reporting(0);

$t = new lime_test(11);

$t->comment('Pesaflow gateway test of checkout, validation and ipn functions');

$payments_manager = new PaymentsManager();
$application_manager = new ApplicationManager();
$invoice_manager = new InvoiceManager();
$pesaflow_gateway = new PesaflowGateway();

//Variables and data to be tested
  $q = Doctrine_Query::create()
     ->from("SfGuardUser a")
     ->where("a.username = ?", "12345678");
  $sfGuardUser = $q->fetchOne();

  if($sfGuardUser)
  {
    //Do Nothing
  }
  else
  {
    // create user
    $sfGuardUser = new sfGuardUser();
    $sfGuardUser->username = "12345678";
    $sfGuardUser->email_address = "campfossa@gmail.com";
    $sfGuardUser->save();

    // create user profile
    $profile = new sfGuardUserProfile();
    $profile->user_id = $sfGuardUser->id;
    $profile->fullname = "John Doe";
    $profile->email = "campfossa@gmail.com";
    $profile->mobile = "0701123123";
    $profile->registeras = $account_type ? $account_type : 1;
    $profile->save();
  }

  $q = Doctrine_Query::create()
     ->from("ApForms a")
     ->where("a.form_name LIKE ?", "%1. Application For Provisional Driving License%");
  $form = $q->fetchOne();

  $form_id = $form->getFormId();
  $new_record_id = 1;

  $submission = $application_manager->create_application($form_id, $new_record_id, $sfGuardUser->getId(), true);

  $application_manager->update_invoices($submission->getId());

  $invoice_id = $invoice_manager->get_unpaid_invoice($submission->getId());

  $payment_settings = $payments_manager->get_payment_settings($invoice_id);

//Test Checkout

  //Frontend
  $frontend_checkout = $pesaflow_gateway->checkout($invoice_id, $payment_settings, false);

  $reference = $form_id."/".$new_record_id."/".$invoice_id;

  $targets = array();
  $targets[] = 'name="apiClientID" value="1"';
  $targets[] = 'name="key" value="6678979C75A70BDA85762F4D488AFB6F"/>';
  $targets[] = 'name="secret" value="wyDjxWr9A1SX850KtMn"/>';
  $targets[] = 'name="currency" value="KES"/>';
  $targets[] = 'name="billDesc" value="1. Application For Provisional Driving License"/>';
  $targets[] = 'name="billRefNumber" value="'.$reference.'"/>';
  $targets[] = 'name="serviceID" value="7"/>';
  $targets[] = 'name="clientMSISDN" value="0701123123"/>';
  $targets[] = 'name="clientName" value="John Doe"/>';
  $targets[] = 'name="clientEmail" value="campfossa@gmail.com"/>';
  $targets[] = 'name="clientIDNumber" value="12345678"/>';
  $targets[] = 'name="amountExpected" value="650"/>';
  $targets[] = 'name="callBackURLOnSuccess" value="http:///index.php/forms/confirmpayment?id='.$form_id.'&entryid='.$new_record_id.'&done=1&invoiceid='.$invoice_id.'&status=201"/>';
  $targets[] = 'name="callBackURLOnFail" value="http:///index.php/forms/invalidpayment"/>';
  $targets[] = 'name="updateURL" value="http:///index.php/payment/updateinvoice"/>';

  $found_count = 0;

  foreach($targets as $tg)
  {
      if (strpos($frontend_checkout,$tg) !== false) {
          $found_count++;
      }
      else
      {
        error_log("Unmatched Frontend Checkout Item: ".$tg);
      }
  }

  $t->is($found_count, 15,'->checkout() Frontend checkout iframe rendered');

  //Backend
  $backend_checkout = $pesaflow_gateway->checkout($invoice_id, $payment_settings, true);

  $targets = array();
  $targets[] = 'name="apiClientID" value="1"';
  $targets[] = 'name="key" value="6678979C75A70BDA85762F4D488AFB6F"/>';
  $targets[] = 'name="secret" value="wyDjxWr9A1SX850KtMn"/>';
  $targets[] = 'name="currency" value="KES"/>';
  $targets[] = 'name="billDesc" value="1. Application For Provisional Driving License"/>';
  $targets[] = 'name="billRefNumber" value="'.$reference.'"/>';
  $targets[] = 'name="serviceID" value="7"/>';
  $targets[] = 'name="clientMSISDN" value="0701123123"/>';
  $targets[] = 'name="clientName" value="John Doe"/>';
  $targets[] = 'name="clientEmail" value="campfossa@gmail.com"/>';
  $targets[] = 'name="clientIDNumber" value="12345678"/>';
  $targets[] = 'name="amountExpected" value="650"/>';
  $targets[] = 'name="callBackURLOnSuccess" value="http:///backend.php/applications/confirmpayment?id='.$form_id.'&entryid='.$new_record_id.'&done=1&invoiceid='.$invoice_id.'&status=201"/>';
  $targets[] = 'name="callBackURLOnFail" value="http:///backend.php/applications/invalidpayment"/>';
  $targets[] = 'name="updateURL" value="http:///index.php/payment/updateinvoice"/>';

  $found_count = 0;

  foreach($targets as $tg)
  {
      if (strpos($backend_checkout,$tg) !== false) {
          $found_count++;
      }
      else
      {
        error_log("Unmatched Backend Checkout Item: ".$tg);
      }
  }

  $t->is($found_count, 15,'->checkout() Backend checkout iframe rendered');

//Test Validation

  //Successful Payments
  $request_details = array();
  $request_details["transactionid"] = $reference;
  $request_details["status"] = 201;

  $q = Doctrine_Query::create()
     ->from("MfInvoice a")
     ->where("a.id = ?", $invoice_id);
  $invoice = $q->fetchOne();
  $invoice->setPaid(1);
  $invoice->save();

  $expected_validation_results = true;
  $validation_results = $pesaflow_gateway->validate($invoice_id, $request_details, $payment_settings);
  $t->is($validation_results, $expected_validation_results,'->validate() Validate successful payments after redirect from checkout');
    $q = Doctrine_Query::create()
       ->from("MfInvoice a")
       ->where("a.id = ?", $invoice_id);
    $invoice = $q->fetchOne();
  $t->is($invoice->getPaid(), 2,'->validate() Invoice status after successful payment');

  //Failed Payments
  $request_details = array();
  $request_details["transactionid"] = $reference;
  $request_details["status"] = 203;

  $q = Doctrine_Query::create()
     ->from("MfInvoice a")
     ->where("a.id = ?", $invoice_id);
  $invoice = $q->fetchOne();
  $invoice->setPaid(1);
  $invoice->save();

  $expected_validation_results = true;
  $validation_results = $pesaflow_gateway->validate($invoice_id, $request_details, $payment_settings);
  $t->is($validation_results, $expected_validation_results,'->validate() Validate failed payments after redirect from checkout');
    $q = Doctrine_Query::create()
       ->from("MfInvoice a")
       ->where("a.id = ?", $invoice_id);
    $invoice = $q->fetchOne();
  $t->is($invoice->getPaid(), 3,'->validate() Invoice status after failed payment');

  //Pending Payments
  $request_details = array();
  $request_details["transactionid"] = $reference;
  $request_details["status"] = 0;

  $q = Doctrine_Query::create()
     ->from("MfInvoice a")
     ->where("a.id = ?", $invoice_id);
  $invoice = $q->fetchOne();
  $invoice->setPaid(1);
  $invoice->save();

  $expected_validation_results = true;
  $validation_results = $pesaflow_gateway->validate($invoice_id, $request_details, $payment_settings);
  $t->is($validation_results, $expected_validation_results,'->validate() Validate pending payments after redirect from checkout');
    $q = Doctrine_Query::create()
       ->from("MfInvoice a")
       ->where("a.id = ?", $invoice_id);
    $invoice = $q->fetchOne();
  $t->is($invoice->getPaid(), 15,'->validate() Invoice status after pending payment');

//Test IPN

  //Successful Payments
  $request_details = array();
  $request_details["invoiceNumber"] = $reference;
  $request_details["transactiondate"] = date('Y-m-d');
  $request_details["transaction_date"] = date('Y-m-d');
  $request_details["transactionstatus"] = "completed";
  $request_details["transaction_status"] = "completed";
  $request_details["amount"] = "650";
  $request_details["paidby"] = "John Doe";

  $q = Doctrine_Query::create()
     ->from("MfInvoice a")
     ->where("a.id = ?", $invoice_id);
  $invoice = $q->fetchOne();
  $invoice->setPaid(1);
  $invoice->save();

  $expected_ipn_results = array();
  $expected_ipn_results['invoice_status'] = 'paid';
  $expected_ipn_results['total_amount'] = '650';
  $expected_ipn_results['currency'] = 'KES';
  $expected_ipn_results['date_of_invoice'] = $invoice->getCreatedAt();
  $expected_ipn_results['application_id'] = $invoice->getFormEntry()->getApplicationId();
  $expected_ipn_results['user_email'] = 'campfossa@gmail.com';
  $expected_ipn_results['user_mobile'] = '0701123123';
  $expected_ipn_results['user_fullname'] = 'John Doe';

  $ipn_results = $pesaflow_gateway->ipn($request_details);
  $t->is($ipn_results, $expected_ipn_results,'->ipn() IPN for successful payments');

  //Failed Payments
  $request_details = array();
  $request_details["invoiceNumber"] = $reference;
  $request_details["transactiondate"] = date('Y-m-d');
  $request_details["transaction_date"] = date('Y-m-d');
  $request_details["transactionstatus"] = "failed";
  $request_details["transaction_status"] = "failed";
  $request_details["amount"] = "650";
  $request_details["paidby"] = "John Doe";

  $q = Doctrine_Query::create()
     ->from("MfInvoice a")
     ->where("a.id = ?", $invoice_id);
  $invoice = $q->fetchOne();
  $invoice->setPaid(1);
  $invoice->save();

  $expected_ipn_results = array();
  $expected_ipn_results['invoice_status'] = 'cancelled';
  $expected_ipn_results['total_amount'] = '650';
  $expected_ipn_results['currency'] = 'KES';
  $expected_ipn_results['date_of_invoice'] = $invoice->getCreatedAt();
  $expected_ipn_results['application_id'] = $invoice->getFormEntry()->getApplicationId();
  $expected_ipn_results['user_email'] = 'campfossa@gmail.com';
  $expected_ipn_results['user_mobile'] = '0701123123';
  $expected_ipn_results['user_fullname'] = 'John Doe';

  $ipn_results = $pesaflow_gateway->ipn($request_details);
  $t->is($ipn_results, $expected_ipn_results,'->ipn() IPN for failed payments');

  //Pending Payments
  $request_details = array();
  $request_details["invoiceNumber"] = $reference;
  $request_details["transactiondate"] = date('Y-m-d');
  $request_details["transaction_date"] = date('Y-m-d');
  $request_details["transactionstatus"] = "pending";
  $request_details["transaction_status"] = "pending";
  $request_details["amount"] = "650";
  $request_details["paidby"] = "John Doe";

  $q = Doctrine_Query::create()
     ->from("MfInvoice a")
     ->where("a.id = ?", $invoice_id);
  $invoice = $q->fetchOne();
  $invoice->setPaid(1);
  $invoice->save();

  $expected_ipn_results = array();
  $expected_ipn_results['invoice_status'] = 'pending confirmation';
  $expected_ipn_results['total_amount'] = '650';
  $expected_ipn_results['currency'] = 'KES';
  $expected_ipn_results['date_of_invoice'] = $invoice->getCreatedAt();
  $expected_ipn_results['application_id'] = $invoice->getFormEntry()->getApplicationId();
  $expected_ipn_results['user_email'] = 'campfossa@gmail.com';
  $expected_ipn_results['user_mobile'] = '0701123123';
  $expected_ipn_results['user_fullname'] = 'John Doe';

  $ipn_results = $pesaflow_gateway->ipn($request_details);
  $t->is($ipn_results, $expected_ipn_results,'->ipn() IPN for pending payments');
