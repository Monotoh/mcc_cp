<?php
// test/unit/AuditTest.php

require_once dirname(__FILE__).'/../bootstrap/unit.php';

$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', 'test', true);
new sfDatabaseManager($configuration);

Doctrine_Core::loadData(sfConfig::get('sf_test_dir').'/fixtures');

require_once dirname(__FILE__).'/../../lib/notifications.class.php';

$t = new lime_test(6);

$t->comment('Template parser library tests for invoice and permit template parsers');

$templateparser = new TemplateParser();

$report_template_raw = "{ap_application_id}: Date Of Submission: {fm_created_at}";
$invoice_template_raw = "{ap_application_id}: Invoice Total: {in_total}, Invoice Sent On: {inv_date_created}";
$permit_template_raw = "{ap_application_id}: Permit No: {ap_permit_id}, Permit Issued On: {ap_issue_date}";

$application_id = 26;
$form_id = 14;
$entry_id = 10;

$invoice_id = 5;
$permit_id = 6;

//For reports
  $templateparser->setup($application_id, $form_id, $entry_id);
  $report_content = $templateparser->parseReport($report_template_raw);

//For Invoices
  $templateparser = new TemplateParser();
  $invoice_content = $templateparser->parseInvoice($application_id, $form_id, $entry_id, $invoice_id, $invoice_template_raw);

//For Permits
  $templateparser = new TemplateParser();
  $permit_content = $templateparser->parsePermit($application_id, $form_id, $entry_id, $permit_id, $permit_template_raw);

$t->isnt($report_content, "{ap_application_id}: Date Of Submission: {fm_created_at}",'->parseReport() parser for reports');
$t->isnt($invoice_content, "{ap_application_id}: Invoice Total: {in_total}, Invoice Sent On: {inv_date_created}",'->parseInvoice() parser for invoices');
$t->isnt($permit_content, "{ap_application_id}: Permit No: {ap_permit_id}, Permit Issued On: {ap_issue_date}",'->parsePermit() parser for permits');

$t->is($report_content, "AAA001: Date Of Submission: 01 August 2014",'->parseReport() parser for reports');
$t->is($invoice_content, "AAA001: Invoice Total: 1000, Invoice Sent On: 01 August 2014",'->parseInvoice() parser for invoices');
$t->is($permit_content, "AAA001: Permit No: SLP-AAA001, Permit Issued On: 02 August 2014",'->parsePermit() parser for permits');