<?php
// test/unit/AuditTest.php

require_once dirname(__FILE__).'/../bootstrap/unit.php';

$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', 'test', true);
new sfDatabaseManager($configuration);

Doctrine_Core::loadData(sfConfig::get('sf_test_dir').'/fixtures');

$t = new lime_test(2);

$_SESSION['unit_test'] = true;

require_once dirname(__FILE__).'/../../lib/translation.class.php';

$t->comment('Translation library tests');

$translator = new Translation();



//Test Set Translations on a table

$tablename = "Groups";
$fieldname = "title";
$fieldid = "1";
$fieldvalue1 = "Bangla Groups";

$translator->setTranslation($tablename,$fieldname,$fieldid,$fieldvalue1);

//Test Get Translation from what you set above
$content1 = $translator->getTranslation($tablename,$fieldname,$fieldid);

//Test Set Translations on a table
$tablename = "Choices";
$fieldname = "option";
$fieldid = "1";
$optionid = "2";
$fieldvalue2 = "Bangla B";

$translator->setOptionTranslation($tablename,$fieldname,$fieldid,$optionid,$fieldvalue2);

//Test Get Translation from what you set above
$content2 = $translator->getOptionTranslation($tablename,$fieldname,$fieldid,$optionid);

$t->is($content1, "Bangla Groups",'->getTranslation() return translation content set using ->setTranslation()');
$t->is($content2, "Bangla B",'->getOptionTranslation() return translation content set using ->setOptionTranslation()');