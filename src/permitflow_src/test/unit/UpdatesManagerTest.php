<?php
// test/unit/AuditTest.php

require_once dirname(__FILE__).'/../bootstrap/unit.php';

$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', 'test', true);
new sfDatabaseManager($configuration);

Doctrine_Core::loadData(sfConfig::get('sf_test_dir').'/fixtures');

error_reporting(0);

$t = new lime_test(3);

$t->comment('UpdatesManager Test. Checking remote validator, pulling data and pushing data');

$updates_manager = new UpdatesManager();
$application_manager = new ApplicationManager();
$invoice_manager = new InvoiceManager();

//An application wth a valid invoice is needed
$q = Doctrine_Query::create()
   ->from("SfGuardUser a")
   ->where("a.username = ?", "12345678");
$sfGuardUser = $q->fetchOne();

if($sfGuardUser)
{
  //Do Nothing
}
else
{
  // create user
  $sfGuardUser = new sfGuardUser();
  $sfGuardUser->username = "12345678";
  $sfGuardUser->email_address = "campfossa@gmail.com";
  $sfGuardUser->save();

  // create user profile
  $profile = new sfGuardUserProfile();
  $profile->user_id = $sfGuardUser->id;
  $profile->fullname = "John Doe";
  $profile->email = "campfossa@gmail.com";
  $profile->mobile = "0701123123";
  $profile->registeras = $account_type ? $account_type : 1;
  $profile->save();
}

$q = Doctrine_Query::create()
   ->from("ApForms a")
   ->where("a.form_name LIKE ?", "%4. Driving License Renewal %");
$form = $q->fetchOne();

$form_id = $form->getFormId();
$new_record_id = 1;

$submission = $application_manager->create_application($form_id, $new_record_id, $sfGuardUser->getId(), true);

$application_manager->update_invoices($submission->getId());

$invoice_id = $invoice_manager->get_unpaid_invoice($submission->getId());

$q = Doctrine_Query::create()
   ->from("MfInvoice a")
   ->where("a.id = ?", $invoice_id);
$invoice = $q->fetchOne();

$invoice->setPaid(2);
$invoice->save();

$q = Doctrine_Query::create()
   ->from("SavedPermit a")
   ->where("a.application_id = ?", $submission->getId());
$permit = $q->fetchOne();

//Form Validator for the form builder. Will only allow submission of form based on different criteria e.g. records found on remote server
$remote_url = "http://localhost/tests/remotevalidate.php";
$remote_username = "test";
$remote_password = "test";
$remote_template = "{data:'{count}'}";
$remote_criteria = "records";
$remote_value = "SCZ";

$validator_results = $updates_manager->pull_validator($remote_url, $remote_username, $remote_password, $remote_template, $remote_criteria, $remote_value);
$t->is($validator_results['test'], true,'->pull_validator() Test Form Remove Validator');

//Pulling updates from a remote database. This occurs during form submission and is not linked to a permit nor are the results stored locally (yet)
$remote_url = "http://localhost/tests/remotepull.php";
$remote_username = "test";
$remote_password = "test";
$remote_template = "{data:'{count}'}";
$remote_criteria = "records";
$remote_value = "SCZ";

$pull_results = $updates_manager->pull_update($remote_url, $remote_username, $remote_password, $remote_template, $remote_criteria, $remote_value);
$t->is($pull_results, 'Success','->pull_update() Test Pulling of Data from Remove Database');

//Pushing updates to a remote database. This occurs during permit generation. The results are stored locally.
$updates_manager->push_update($permit->getId());

$q = Doctrine_Query::create()
   ->from("SavedPermit a")
   ->where("a.application_id = ?", $submission->getId());
$permit = $q->fetchOne();

$t->is($permit->getRemoteResult(), 'Success','->push_update() Test Pushing of Data to Remote Database');
