<?php
// test/unit/model/FaqTest.php

include(dirname(__FILE__).'/../../bootstrap/doctrine.php'); 

$t = new lime_test(12);
$t->comment('ApForms Doctrine Model fields');
$form = Doctrine_Core::getTable('ApForms')->createQuery()->fetchOne();

//Test fields using test data stored in fixtures
$t->is($form->getFormName(), 'Application for special license','->getFormName() return the name of the form');
$t->is($form->getFormDescription(), 'Special unit test license for symfony pros','->getFormDescription() return the description of the form');
$t->is($form->getFormActive(), 1,'->getFormActive() return the active status of the form (1 - Active, 0 - Not Active)');
$t->is($form->getFormStage(), 1,'->getFormStage() return the stage of submission of the form');
$t->is($form->getFormType(), 1,'->getFormType() return the type of the form (0 - General, 1 - Application form, 2 - Comment Sheet)');
$t->is($form->getFormCode(), 1001,'->getFormCode() return the deleted status of the form');

//Test save function for Locale
$t->comment('ApForms Doctrine Model ->save function');


function create_form($defaults = array())
{
  $form = new ApForms();
  $form->fromArray(array_merge(array(
    'form_name'  => 'Contact Us',
    'form_description' => 'A deactivated contact us form',
    'form_active'        => 0,
    'form_stage'        => 5,
    'form_type'        => 0,
    'form_code'        => 20011101
  ), $defaults));
  return $form;
}


$newform = create_form();
$newform->save();


$t->is($newform->getFormName(), 'Contact Us','->getFormName() return the name of the new form');
$t->is($newform->getFormDescription(), 'A deactivated contact us form','->getFormDescription() return the description of the new form');
$t->is($newform->getFormActive(), 0,'->getFormActive() return the active status of the new form (1 - Active, 0 - Not Active)');
$t->is($newform->getFormStage(), 5,'->getFormStage() return the stage of submission of the new form');
$t->is($newform->getFormType(), 0,'->getFormType() return the type of the new form (0 - General, 1 - Application form, 2 - Comment Sheet)');
$t->is($newform->getFormCode(), 20011101,'->getFormCode() return the deleted status of the new form');