<?php
// test/unit/model/CfUserTest.php

include(dirname(__FILE__).'/../../bootstrap/doctrine.php'); 
include(dirname(__FILE__).'/../../../lib/bcrypt.php'); 

$t = new lime_test(12);
$t->comment('CfUser Doctrine Model fields');
$reviewer = Doctrine_Core::getTable('CfUser')->createQuery()->fetchOne();

//Test fields using test data stored in fixtures
$t->is($reviewer->getStrfirstname(), 'System','->getStrfirstname() return the first name of the reviewer');
$t->is($reviewer->getStrlastname(), 'Administrator','->getStrlastname() return the last name of the reviewer');
$t->is($reviewer->getStremail(), 'thomasjgx@gmail.com','->getStremail() return the email of the reviewer');
$t->is($reviewer->getStruserid(), 'admin','->getStruserid() return the username of the reviewer');
$t->is(password_verify("manticore",$reviewer->getStrpassword()), true, '->getStrpassword() return the bcrypt encrypted password of the reviewer');
$t->is($reviewer->getBdeleted(), 0,'->getBdeleted() return the deleted status of the reviewer (1 - Deleted, 0 - Not Deleted)');

//Test save function for CfUser
$t->comment('CfUser Doctrine Model ->save function');

function create_reviewer($defaults = array())
{
  $reviewer = new CfUser();
  $reviewer->fromArray(array_merge(array(
    'strfirstname'  => 'Thomas',
    'strlastname' => 'Juma',
    'stremail' => 'thomastest@gmail.com',
    'struserid' => 'thomas',
    'strpassword' => password_hash("thomas", PASSWORD_BCRYPT),
    'bdeleted'        => 1
  ), $defaults));
  return $reviewer;
}


$newreviewer = create_reviewer();
$newreviewer->save();

$t->is($newreviewer->getStrfirstname(), 'Thomas','->getStrfirstname() return the first name of the new reviewer');
$t->is($newreviewer->getStrlastname(), 'Juma','->getStrlastname() return the last name of the new reviewer');
$t->is($newreviewer->getStremail(), 'thomastest@gmail.com','->getStremail() return the email of the new reviewer');
$t->is($newreviewer->getStruserid(), 'thomas','->getStruserid() return the username of the new reviewer');
$t->is(password_verify("thomas", $newreviewer->getStrpassword()), true,'->getStrpassword() return the bcrypt encrypted password of the new reviewer');
$t->is($newreviewer->getBdeleted(), 1,'->getBdeleted() return the deleted status of the new reviewer (1 - Deleted, 0 - Not Deleted)');
