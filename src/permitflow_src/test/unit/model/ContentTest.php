<?php
// test/unit/model/ContentTest.php

include(dirname(__FILE__).'/../../bootstrap/doctrine.php'); 

$t = new lime_test(27);
$t->comment('Content Doctrine Model fields');
$page = Doctrine_Core::getTable('Content')->createQuery()->fetchOne();

//Test fields using test data stored in fixtures
$t->is($page->getMenuTitle(), 'Home','->getMenuTitle() return the title for the home page');
$t->is($page->getBreadcrumbTitle(), 'Home Page','->getBreadcrumbTitle() return the breadcrumb for the home page');
$t->is($page->getTopArticle(), '<section class="container"><div class="icon-top-title aligncenter sixteen columns"><h3><strong>Welcome to the Government eCitizen portal</strong></h3><h3 class="slight">One account all eServices</h3></div></section>','->getTopArticle() return the content for the home page');
$t->is($page->getVisibility(), 1,'->getVisibility() return the visibility of the page (1 - Logged out and Logged in, 2 - Logged in only)');
$t->is($page->getPublished(), true,'->getPublished() return the published stage for the home page (false - Not Published, true - Published)');
$t->is($page->getDeleted(), false,'->getDeleted() return the deleted state for the home page (false - Deleted, true - Not Deleted)');
$t->is($page->getMenuIndex(), 1,'->getMenuIndex() return the order for the home page');
$t->is($page->getCreatedBy(), 1,'->getCreatedBy() return the user id for the creator of the home page');
$t->is($page->getTypeId(), 1,'->getTypeId() return the type for the home page (1 - Web Page, 2 - URL)');

//Test save function for (Web Page without URL)
$t->comment('Content Doctrine Model ->save function for a Web Page without URL');


function create_page_without_url($defaults = array())
{
  $page = new Content();
  $page->fromArray(array_merge(array(
    'menu_title'  => 'Page 1',
    'breadcrumb_title' => 'Page 1 Breadcrumb',
    'top_article'        => 'Page 1 Content',
    'visibility'        => 1,
    'published'        => true,
    'deleted'        => false,
    'menu_index'        => 2,
    'created_by'        => 1,
    'type_id'        => 1
  ), $defaults));
  return $page;
}


$newpage = create_page_without_url();
$newpage->save();

$t->is($newpage->getMenuTitle(), 'Page 1','->getMenuTitle() return the title for the new page');
$t->is($newpage->getBreadcrumbTitle(), 'Page 1 Breadcrumb','->getBreadcrumbTitle() return the breadcrumb for the new page');
$t->is($newpage->getTopArticle(), 'Page 1 Content','->getTopArticle() return the content for the new page');
$t->is($newpage->getVisibility(), 1,'->getVisibility() return the visibility of the page (1 - Logged out, 2 - Logged out and Logged in)');
$t->is($newpage->getPublished(), true,'->getPublished() return the published stage for the home page (false - Not Published, true - Published)');
$t->is($newpage->getDeleted(), false,'->getDeleted() return the deleted state for the home page (false - Deleted, true - Not Deleted)');
$t->is($newpage->getMenuIndex(), 2,'->getMenuIndex() return the order for the home page');
$t->is($newpage->getCreatedBy(), 1,'->getCreatedBy() return the user id for the creator of the home page');
$t->is($newpage->getTypeId(), 1,'->getTypeId() return the type for the home page (1 - Web Page, 2 - URL)');

//Test save function for (Web Page with a URL)
$t->comment('Content Doctrine Model ->save function for a Web Page with a URL');


function create_page_with_url($defaults = array())
{
  $page = new Content();
  $page->fromArray(array_merge(array(
    'menu_title'  => 'Page 2',
    'breadcrumb_title' => 'Page 2 Breadcrumb',
    'url'        => 'www.sample.com',
    'visibility'        => 2,
    'published'        => false,
    'deleted'        => true,
    'menu_index'        => 3,
    'created_by'        => 1,
    'type_id'        => 2
  ), $defaults));
  return $page;
}


$newpage = create_page_with_url();
$newpage->save();

$t->is($newpage->getMenuTitle(), 'Page 2','->getMenuTitle() return the title for the new page');
$t->is($newpage->getBreadcrumbTitle(), 'Page 2 Breadcrumb','->getBreadcrumbTitle() return the breadcrumb for the new page');
$t->is($newpage->getUrl(), 'www.sample.com', '->getURL() return the url for the new page');
$t->is($newpage->getVisibility(), 2,'->getVisibility() return the visibility of the page (1 - Logged out, 2 - Logged out and Logged in)');
$t->is($newpage->getPublished(), false,'->getPublished() return the published stage for the home page (false - Not Published, true - Published)');
$t->is($newpage->getDeleted(), true,'->getDeleted() return the deleted state for the home page (false - Deleted, true - Not Deleted)');
$t->is($newpage->getMenuIndex(), 3,'->getMenuIndex() return the order for the home page');
$t->is($newpage->getCreatedBy(), 1,'->getCreatedBy() return the user id for the creator of the home page');
$t->is($newpage->getTypeId(), 2,'->getTypeId() return the type for the home page (1 - Web Page, 2 - URL)');