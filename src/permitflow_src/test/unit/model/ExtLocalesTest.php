<?php
// test/unit/model/CfUserTest.php

include(dirname(__FILE__).'/../../bootstrap/doctrine.php'); 

$t = new lime_test(6);
$t->comment('ExtLocales Doctrine Model fields');
$locale = Doctrine_Core::getTable('ExtLocales')->createQuery()->fetchOne();

//Test fields using test data stored in fixtures
$t->is($locale->getLocaleIdentifier(), 'en_US','->getLocaleIdentifier() return the identifier of the locale');
$t->is($locale->getLocaleTitle(), 'English','->getLocaleTitle() return the title of the locale');
$t->is($locale->getIsDefault(), 1,'->getIsDefault() return the default status of the locale (1 - Default, 0 - Not Default)');

//Test save function for Locale
$t->comment('ExtLocales Doctrine Model ->save function');


function create_locale($defaults = array())
{
  $locale = new ExtLocales();
  $locale->fromArray(array_merge(array(
    'locale_identifier'  => 'bt_BD',
    'locale_title' => 'Bangla',
    'is_default'        => 0
  ), $defaults));
  return $locale;
}


$newlocale = create_locale();
$newlocale->save();

$t->is($newlocale->getLocaleIdentifier(), 'bt_BD','->getLocaleIdentifier() return the identifier of the new locale');
$t->is($newlocale->getLocaleTitle(), 'Bangla','->getLocaleTitle() return the title of the new locale');
$t->is($newlocale->getIsDefault(), 0,'->getIsDefault() return the default status of the new locale (1 - Default, 0 - Not Default)');