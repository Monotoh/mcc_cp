<?php
// test/unit/model/CfUserTest.php

include(dirname(__FILE__).'/../../bootstrap/doctrine.php'); 

$t = new lime_test(12);
$t->comment('ExtTranslations Doctrine Model fields');
$translation = Doctrine_Core::getTable('ExtTranslations')->createQuery()->fetchOne();

//Test fields using test data stored in fixtures
$t->is($translation->getLocale(), 'bt_BD','->getLocale() return the locale of the translation');
$t->is($translation->getTableClass(), 'Sample Table','->getTableClass() return the table name of the translation');
$t->is($translation->getFieldName(), 'Sample Field','->getFieldName() return the field name of the translation');
$t->is($translation->getFieldId(), 1,'->getFieldId() return the field id of the translation');
$t->is($translation->getOptionId(), 2,'->getFieldId() return the option id of the translation');
$t->is($translation->getTrlContent(), 'Sample Value','->getTrlContent() return the content of the translation');

//Test save function for Locale
$t->comment('ExtTranslations Doctrine Model ->save function');


function create_translation($defaults = array())
{
  $translation = new ExtTranslations();
  $translation->fromArray(array_merge(array(
    'locale'  => 'bt_BD',
    'table_class' => 'Another Table',
    'field_name' => 'Another Field',
    'field_id' => 2,
    'option_id' => 3,
    'trl_content' => 'Another Value'
  ), $defaults));
  return $translation;
}


$newtranslation = create_translation();
$newtranslation->save();

$t->is($newtranslation->getLocale(), 'bt_BD','->getLocale() return the locale of the new translation');
$t->is($newtranslation->getTableClass(), 'Another Table','->getTableClass() return the table name of the new translation');
$t->is($newtranslation->getFieldName(), 'Another Field','->getFieldName() return the field name of the new translation');
$t->is($newtranslation->getFieldId(), 2,'->getFieldId() return the field id of the new translation');
$t->is($newtranslation->getOptionId(), 3,'->getFieldId() return the option id of the new translation');
$t->is($newtranslation->getTrlContent(), 'Another Value','->getTrlContent() return the content of the new translation');