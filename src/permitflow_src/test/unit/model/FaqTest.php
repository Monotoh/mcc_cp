<?php
// test/unit/model/FaqTest.php

include(dirname(__FILE__).'/../../bootstrap/doctrine.php'); 

$t = new lime_test(8);
$t->comment('Faq Doctrine Model fields');
$faq = Doctrine_Core::getTable('Faq')->createQuery()->fetchOne();

//Test fields using test data stored in fixtures
$t->is($faq->getQuestion(), 'FAQ One','->getQuestion() return the question of the faq');
$t->is($faq->getAnswer(), 'First Frequently Asked Question','->getAnswer() return the answer of the faq');
$t->is($faq->getPublished(), 1,'->getPublished() return the published status of the faq (1 - Published, 0 - Not Published)');
$t->is($faq->getDeleted(), 0,'->getDeleted() return the deleted status of the faq (1 - Deleted, 0 - Not Deleted)');

//Test save function for Locale
$t->comment('Faq Doctrine Model ->save function');


function create_faq($defaults = array())
{
  $faq = new Faq();
  $faq->fromArray(array_merge(array(
    'question'  => 'FAQ Two',
    'answer' => 'Second Frequently Asked Question',
    'published'        => 0,
    'deleted'        => 1
  ), $defaults));
  return $faq;
}


$newfaq = create_faq();
$newfaq->save();

$t->is($newfaq->getQuestion(), 'FAQ Two','->getQuestion() return the question of the new faq');
$t->is($newfaq->getAnswer(), 'Second Frequently Asked Question','->getAnswer() return the answer of the new faq');
$t->is($newfaq->getPublished(), 0,'->getPublished() return the published status of the new faq (1 - Published, 0 - Not Published)');
$t->is($newfaq->getDeleted(), 1,'->getDeleted() return the deleted status of the new faq (1 - Deleted, 0 - Not Deleted)');