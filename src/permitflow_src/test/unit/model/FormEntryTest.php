<?php
// test/unit/model/FaqTest.php

include(dirname(__FILE__).'/../../bootstrap/doctrine.php'); 

$t = new lime_test(28);
$t->comment('FormEntry Doctrine Model fields');
$submission = Doctrine_Core::getTable('FormEntry')->createQuery()->fetchOne();

//Test fields using test data stored in fixtures
$t->is($submission->getFormId(), 14,'->getFormId() return the form_id of the form_entry');
$t->is($submission->getEntryId(), 10,'->getEntryId() return the entry_id of the form_entry');
$t->is($submission->getUserId(), 1,'->getUserId() return the user_id of the form_entry');
$t->is($submission->getApproved(), 2,'->getApproved() return the approved status of the form_entry');
$t->is($submission->getApplicationId(), 'AAA001','->getApplicationId() return the application_id of the form_entry');
$t->is($submission->getDeclined(), 0,'->getDeclined() return the declined of the form_entry');
$t->is($submission->getDeletedStatus(), 0,'->getDeletedStatus() return the deleted_status of the form_entry');
$t->is($submission->getSavedPermit(), '{ap_application_id}: Date Of Submission: {ap_submitted_on}','->getSavedPermit() return the saved_permit of the form_entry');
$t->is($submission->getPreviousSubmission(), 0,'->getPreviousSubmission() return the previous_submission of the form_entry');
$t->is($submission->getParentSubmission(), 0,'->getParentSubmission() return the parent_submission of the form_entry');
$t->is($submission->getDateOfSubmission(), '2014-08-01','->getDateOfSubmission() return the date_of_submission of the form_entry');
$t->is($submission->getDateOfResponse(), '2014-08-02','->getDateOfResponse() return the date_of_response of the form_entry');
$t->is($submission->getDateOfIssue(), '2014-08-03','->getDateOfIssue() return the date_of_issue of the form_entry');
$t->is($submission->getObservation(), 'N/A','->getObservation() return the observation of the form_entry');

//Test save function for Locale
$t->comment('FormEntry Doctrine Model ->save function');


function create_submission($defaults = array())
{
  $submission = new FormEntry();
  $submission->fromArray(array_merge(array(
    'form_id'  => '14',
    'entry_id'  => '5',
    'user_id'  => '10',
    'approved'  => '5',
    'application_id'  => 'BAA006',
    'declined'  => '1',
    'deleted_status'  => '1',
    'saved_permit'  => "Application: {ap_application_id}: Date Of Submission: {ap_submitted_on}",
    'previous_submission'  => '1',
    'parent_submission'  => '1',
    'date_of_submission'  => "2014-09-01",
    'date_of_response'  => "2014-09-02",
    'date_of_issue'  => "2014-09-03",
    'observation'  => "good!"
  ), $defaults));
  return $submission;
}


$newsubmission = create_submission();
$newsubmission->save();

$t->is($newsubmission->getFormId(), 14,'->getFormId() return the form_id of the new form_entry');
$t->is($newsubmission->getEntryId(), 5,'->getEntryId() return the entry_id of the new form_entry');
$t->is($newsubmission->getUserId(), 10,'->getUserId() return the user_id of the new form_entry');
$t->is($newsubmission->getApproved(), 5,'->getApproved() return the approved status of the new form_entry');
$t->is($newsubmission->getApplicationId(), 'BAA006','->getApplicationId() return the application_id of the new form_entry');
$t->is($newsubmission->getDeclined(), 1,'->getDeclined() return the declined of the new form_entry');
$t->is($newsubmission->getDeletedStatus(), 1,'->getDeletedStatus() return the deleted_status of the new form_entry');
$t->is($newsubmission->getSavedPermit(), 'Application: {ap_application_id}: Date Of Submission: {ap_submitted_on}','->getSavedPermit() return the saved_permit of the new form_entry');
$t->is($newsubmission->getPreviousSubmission(), 1,'->getPreviousSubmission() return the previous_submission of the new form_entry');
$t->is($newsubmission->getParentSubmission(), 1,'->getParentSubmission() return the parent_submission of the new form_entry');
$t->is($newsubmission->getDateOfSubmission(), '2014-09-01','->getDateOfSubmission() return the date_of_submission of the new form_entry');
$t->is($newsubmission->getDateOfResponse(), '2014-09-02','->getDateOfResponse() return the date_of_response of the new form_entry');
$t->is($newsubmission->getDateOfIssue(), '2014-09-03','->getDateOfIssue() return the date_of_issue of the new form_entry');
$t->is($newsubmission->getObservation(), 'good!','->getObservation() return the observation of the new form_entry');