<?php
// test/unit/model/FaqTest.php

include(dirname(__FILE__).'/../../bootstrap/doctrine.php'); 

$t = new lime_test(42);
$t->comment('MfInvoice Doctrine Model fields');
$invoice = Doctrine_Core::getTable('MfInvoice')->createQuery()->fetchOne();

//Test fields using test data stored in fixtures
$t->is($invoice->getAppId(), 26,'->getAppId() return the form_id of the invoice');
$t->is($invoice->getInvoiceNumber(), 'INV-AA001','->getInvoiceNumber() return the entry_id of the invoice');
$t->is($invoice->getPaid(), 1,'->getPaid() return the user_id of the invoice');
$t->is($invoice->getCreatedAt(), '2014-08-01 00:00:00','->getCreatedAt() return the approved status of the invoice');
$t->is($invoice->getUpdatedAt(), '2014-08-02 00:00:00','->getUpdatedAt() return the application_id of the invoice');
$t->is($invoice->getExpiresAt(), '2014-08-05 00:00:00','->getExpiresAt() return the declined of the invoice');
$t->is($invoice->getMdaCode(), 'SL-001','->getMdaCode() return the deleted_status of the invoice');
$t->is($invoice->getServiceCode(), '110001011','->getServiceCode() return the saved_permit of the invoice');
$t->is($invoice->getBranch(), 'Nairobi','->getBranch() return the previous_submission of the invoice');
$t->is($invoice->getDueDate(), '2014-08-04','->getDueDate() return the parent_submission of the invoice');
$t->is($invoice->getPayerId(), 1,'->getPayerId() return the date_of_submission of the invoice');
$t->is($invoice->getPayerName(), 'John Doe','->getPayerName() return the date_of_response of the invoice');
$t->is($invoice->getTotalAmount(), 1000,'->getTotalAmount() return the date_of_issue of the invoice');
$t->is($invoice->getCurrency(), 'BDT','->getCurrency() return the observation of the invoice');
$t->is($invoice->getDocRefNumber(), 'AAA001','->getDocRefNumber() return the observation of the invoice');
$t->is($invoice->getTemplateId(), 1,'->getTemplateId() return the observation of the invoice');

foreach($invoice->getMfInvoiceDetail() as $detail)
{
	$t->comment('MfInvoiceDetail doctrine model relationship test');

	$t->is($detail->getInvoiceId(), 5,'->getInvoiceId() return the invoice_id of the invoice detail');
	$t->is($detail->getDescription(), 'Total','->getDescription() return the description of the invoice detail');
	$t->is($detail->getAmount(), 1000,'->getAmount() return the amount of the invoice detail');
	$t->is($detail->getCreatedAt(), '2014-08-01 00:00:00','->getCreatedAt() return the created_at of the invoice detail');
	$t->is($detail->getUpdatedAt(), '2014-08-02 00:00:00','->getUpdatedAt() return the updated_at of the invoice detail');
}

//Test save function for Locale
$t->comment('MfInvoice Doctrine Model ->save function');


function create_invoice($defaults = array())
{
  $invoice = new MfInvoice();
  $invoice->fromArray(array_merge(array(
  	'id' => 7,
    'app_id' => 26,
    'invoice_number' => INV-AA002,
    'paid' => 2,
    'created_at' => "2014-08-02",
    'updated_at' => "2014-08-03",
    'expires_at' => "2014-08-06",
    'mda_code' => 'SL-002',
    'service_code' => '110001012',
    'branch' => 'Kakamega',
    'due_date' => "2014-08-05",
    'payer_id' => 2,
    'payer_name' => 'Jenny Doe',
    'total_amount' => 1001,
    'currency' => 'KSH',
    'doc_ref_number' => 'AAA002',
    'template_id' => 2
  ), $defaults));
  return $invoice;
}


$newinvoice = create_invoice();
$newinvoice->save();

$newinvoicedetail = new MfInvoiceDetail();
$newinvoicedetail->setDescription('Submission Fee');
$newinvoicedetail->setAmount('1001');
$newinvoicedetail->setInvoiceId($newinvoice->getId());
$newinvoicedetail->setCreatedAt('2014-08-02');
$newinvoicedetail->setUpdatedAt('2014-08-03');
$newinvoicedetail->save();

//Test fields using test data stored in fixtures
$t->is($newinvoice->getAppId(), 26,'->getAppId() return the form_id of the new invoice');
$t->is($newinvoice->getInvoiceNumber(), 'INV-AA002','->getInvoiceNumber() return the entry_id of the new invoice');
$t->is($newinvoice->getPaid(), 2,'->getPaid() return the user_id of the new invoice');
$t->is($newinvoice->getCreatedAt(), '2014-08-02','->getCreatedAt() return the approved status of the new invoice');
$t->is($newinvoice->getUpdatedAt(), '2014-08-03','->getUpdatedAt() return the application_id of the new invoice');
$t->is($newinvoice->getExpiresAt(), '2014-08-06','->getExpiresAt() return the declined of the new invoice');
$t->is($newinvoice->getMdaCode(), 'SL-002','->getMdaCode() return the deleted_status of the new invoice');
$t->is($newinvoice->getServiceCode(), '110001012','->getServiceCode() return the saved_permit of the new invoice');
$t->is($newinvoice->getBranch(), 'Kakamega','->getBranch() return the previous_submission of the new invoice');
$t->is($newinvoice->getDueDate(), '2014-08-05','->getDueDate() return the parent_submission of the new invoice');
$t->is($newinvoice->getPayerId(), 2,'->getPayerId() return the date_of_submission of the new invoice');
$t->is($newinvoice->getPayerName(), 'Jenny Doe','->getPayerName() return the date_of_response of the inew nvoice');
$t->is($newinvoice->getTotalAmount(), 1001,'->getTotalAmount() return the date_of_issue of the new invoice');
$t->is($newinvoice->getCurrency(), 'KSH','->getCurrency() return the observation of the new invoice');
$t->is($newinvoice->getDocRefNumber(), 'AAA002','->getDocRefNumber() return the observation of the new invoice');
$t->is($newinvoice->getTemplateId(), 2,'->getTemplateId() return the observation of the new invoice');

foreach($newinvoice->getMfInvoiceDetail() as $newdetail)
{
	$t->comment('MfInvoiceDetail doctrine model relationship test');

	$t->is($newdetail->getInvoiceId(), 7,'->getInvoiceId() return the invoice_id of the new invoice detail');
	$t->is($newdetail->getDescription(), 'Submission Fee','->getDescription() return the description of the new invoice detail');
	$t->is($newdetail->getAmount(), 1001,'->getAmount() return the amount of the new invoice detail');
	$t->is($newdetail->getCreatedAt(), '2014-08-02 00:00:00','->getCreatedAt() return the created_at of the new invoice detail');
	$t->is($newdetail->getUpdatedAt(), '2014-08-03 00:00:00','->getUpdatedAt() return the updated_at of the new invoice detail');
}