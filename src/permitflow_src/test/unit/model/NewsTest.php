<?php
// test/unit/model/NewsTest.php

include(dirname(__FILE__).'/../../bootstrap/doctrine.php'); 

$t = new lime_test(12);
$t->comment('News Doctrine Model fields');
$news = Doctrine_Core::getTable('News')->createQuery()->fetchOne();

//Test fields using test data stored in fixtures
$t->is($news->getTitle(), 'News Article 1','->getTitle() return the title of the news article');
$t->is($news->getArticle(), 'This is a sample news article 1','->getArticle() return the content of the news article');
$t->is($news->getHits(), 0,'->getHits() return the hits of the news article');
$t->is($news->getPublished(), true,'->getPublished() return the published status of the news article (True - Published, False - Not Published)');
$t->is($news->getDeleted(), false,'->getDeleted() return the deleted status of the news article (True - Deleted, False - Not Deleted)');
$t->is($news->getCreatedOn(), '2014-08-01','->getCreatedOn() return the date of creation of the news article');

//Test save function for News
$t->comment('News Doctrine Model ->save function');


function create_news($defaults = array())
{
  $news = new News();
  $news->fromArray(array_merge(array(
    'title'  => 'News Article 2',
    'article' => 'This is a sample news article 2',
    'hits' => 2,
    'published' => false,
    'deleted' => true,
    'created_on'        => '2014-08-03'
  ), $defaults));
  return $news;
}


$newnews = create_news();
$newnews->save();

$t->is($newnews->getTitle(), 'News Article 2','->getTitle() return the title of the new news article');
$t->is($newnews->getArticle(), 'This is a sample news article 2','->getArticle() return the content of the new news article');
$t->is($newnews->getHits(), 2,'->getHits() return the hits of the new news article');
$t->is($newnews->getPublished(), false,'->getPublished() return the published status of the new news article (True - Published, False - Not Published)');
$t->is($newnews->getDeleted(), true,'->getDeleted() return the deleted status of the new news article (True - Deleted, False - Not Deleted)');
$t->is($newnews->getCreatedOn(), '2014-08-03','->getCreatedOn() return the date of creation of the new news article');