<?php
// test/unit/model/FaqTest.php

include(dirname(__FILE__).'/../../bootstrap/doctrine.php'); 

$t = new lime_test(18);
$t->comment('SavedPermit Doctrine Model fields');
$permit = Doctrine_Core::getTable('SavedPermit')->createQuery()->fetchOne();

//Test fields using test data stored in fixtures
$t->is($permit->getTypeId(), 1,'->getTypeId() return the type_id of the saved_permit');
$t->is($permit->getApplicationId(), 26,'->getApplicationId() return the application_id of the saved_permit');
$t->is($permit->getPermit(), "{ap_application_id}: Permit No: {permit_id}, Permit Issued On: {ap_issued_on}",'->getPermit() return the content of the saved_permit');
$t->is($permit->getDateOfIssue(), "2014-08-02",'->getDateOfIssue() return the date_of_issue of the saved_permit');
$t->is($permit->getDateOfExpiry(), "2015-08-02",'->getDateOfExpiry() return the application_id of the saved_permit');
$t->is($permit->getCreatedBy(), 1,'->getCreatedBy() return the declined of the date_of_expiry');
$t->is($permit->getLastUpdated(), "2014-08-02",'->getLastUpdated() return the last_updated date of the saved_permit');
$t->is($permit->getPermitId(), 'SLP-AAA001','->getPermitId() return the permit_id of the saved_permit');
$t->is($permit->getDocumentKey(), 'N/A','->getDocumentKey() return the document_key of the saved_permit');

//Test save function for Locale
$t->comment('SavedPermit Doctrine Model ->save function');


function create_permit($defaults = array())
{
  $permit = new SavedPermit();
  $permit->fromArray(array_merge(array(
    'type_id' => 2,
    'application_id' => 26,
    'permit' => "Application: {ap_application_id}: Permit No: {permit_id}, Permit Issued On: {ap_issued_on}",
    'date_of_issue' => "2014-08-03",
    'date_of_expiry' => "2015-08-04",
    'created_by' => 2,
    'last_updated' => "2014-08-03",
    'permit_id' => 'SLP-AAA002',
    'document_key' => "10010"
  ), $defaults));
  return $permit;
}


$newpermit = create_permit();
$newpermit->save();

//Test fields using test data stored in fixtures
$t->is($newpermit->getTypeId(), 2,'->getTypeId() return the type_id of the saved_permit');
$t->is($newpermit->getApplicationId(), 26,'->getApplicationId() return the application_id of the saved_permit');
$t->is($newpermit->getPermit(), "Application: {ap_application_id}: Permit No: {permit_id}, Permit Issued On: {ap_issued_on}",'->getPermit() return the content of the saved_permit');
$t->is($newpermit->getDateOfIssue(), "2014-08-03",'->getDateOfIssue() return the date_of_issue of the saved_permit');
$t->is($newpermit->getDateOfExpiry(), "2015-08-04",'->getDateOfExpiry() return the application_id of the saved_permit');
$t->is($newpermit->getCreatedBy(), 2,'->getCreatedBy() return the declined of the date_of_expiry');
$t->is($newpermit->getLastUpdated(), "2014-08-03",'->getLastUpdated() return the last_updated date of the saved_permit');
$t->is($newpermit->getPermitId(), 'SLP-AAA002','->getPermitId() return the permit_id of the saved_permit');
$t->is($newpermit->getDocumentKey(), '10010','->getDocumentKey() return the document_key of the saved_permit');