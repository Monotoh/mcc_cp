<?php
// test/unit/model/FaqTest.php

include(dirname(__FILE__).'/../../bootstrap/doctrine.php'); 

$t = new lime_test(12);
$t->comment('SfGuardUserProfile Doctrine Model fields');
$user_profile = Doctrine_Core::getTable('SfGuardUserProfile')->createQuery()->fetchOne();

//Test fields using test data stored in fixtures
$t->is($user_profile->getUserId(), 1,'->getUserId() return the user_id of the sf_guard_user_profile');
$t->is($user_profile->getEmail(), 'johndoe@gmail.com','->getEmail() return the email of the sf_guard_user_profile');
$t->is($user_profile->getFullname(), "John Doe",'->getFullname() return the fullname of the sf_guard_user_profile');
$t->is($user_profile->getMobile(), "123456",'->getMobile() return the mobile of the sf_guard_user_profile');
$t->is($user_profile->getValidate(), "val1234",'->getValidate() return the validate of the sf_guard_user_profile');
$t->is($user_profile->getRegisteras(), 1,'->getRegisteras() return the registeras of the sf_guard_user_profile');

//Test save function for Locale
$t->comment('SfGuardUserProfile Doctrine Model ->save function');


function create_user_profile($defaults = array())
{
  $profile = new SfGuardUserProfile();
  $profile->fromArray(array_merge(array(
    'user_id' => 1,
    'email' => 'johndoe1@gmail.com',
    'fullname' => 'John Doe1',
    'mobile' => '654321',
    'validate' => '4321val',
    'registeras' => 2
  ), $defaults));
  return $profile;
}


$newprofile = create_user_profile();
$newprofile->save();

//Test fields using test data stored in fixtures
$t->is($newprofile->getUserId(), 1,'->getUserId() return the user_id of the sf_guard_user_profile');
$t->is($newprofile->getEmail(), 'johndoe1@gmail.com','->getEmail() return the email of the sf_guard_user_profile');
$t->is($newprofile->getFullname(), "John Doe1",'->getFullname() return the fullname of the sf_guard_user_profile');
$t->is($newprofile->getMobile(), "654321",'->getMobile() return the mobile of the sf_guard_user_profile');
$t->is($newprofile->getValidate(), "4321val",'->getValidate() return the validate of the sf_guard_user_profile');
$t->is($newprofile->getRegisteras(), 2,'->getRegisteras() return the registeras of the sf_guard_user_profile');