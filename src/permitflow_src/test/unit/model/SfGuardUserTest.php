<?php
// test/unit/model/SfGuardTest.php

include(dirname(__FILE__).'/../../bootstrap/doctrine.php'); 

$t = new lime_test(12);
$t->comment('SfGuardUser Doctrine Model fields');
$user = Doctrine_Core::getTable('SfGuardUser')->createQuery()->fetchOne();

//Test fields using test data stored in fixtures
$t->is($user->getFirstName(), 'John','->getFirstName() return the first name of the user');
$t->is($user->getLastName(), 'Doe','->getLastName() return the last name of the user');
$t->is($user->getEmailAddress(), 'johndoe@gmail.com','->getEmailAddress() return the email address of the user)');
$t->is($user->getUsername(), 'johndoe','->getUsername() return the username of the user');
//$t->is($user->getPassword(), sha1('johndoe'),'->getPassword() return the password of the user'); //Figure out a test for the password
$t->is($user->getIsActive(), true,'->getIsActive() return the active status of the user (true - Active, false - Not Active)');
$t->is($user->getIsSuperAdmin(), true,'->getIsSuperAdmin() return the super_admin status of the user (true - Super Admin, false - Not Super Admin)');

//Test save function for SfGuardUser
$t->comment('SfGuardUser Doctrine Model ->save function');


function create_user($defaults = array())
{
  $user = new SfGuardUser();
  $user->fromArray(array_merge(array(
    'first_name'  => 'Thomas',
    'last_name' => 'Juma',
    'email_address' => 'thomastest@gmail.com',
    'username' => 'thomas',
    'password' => 'thomas',
    'is_active' => false,
    'is_super_admin' => false
  ), $defaults));
  return $user;
}


$newuser = create_user();
$newuser->save();

$t->is($newuser->getFirstName(), 'Thomas','->getFirstName() return the first name of the new user');
$t->is($newuser->getLastName(), 'Juma','->getLastName() return the last name of the new user');
$t->is($newuser->getEmailAddress(), 'thomastest@gmail.com','->getEmailAddress() return the email address of the new user)');
$t->is($newuser->getUsername(), 'thomas','->getUsername() return the username of the new user');
//$t->is($newuser->getPassword(), sha1('thomas'),'->getPassword() return the password of the new user'); //Figure out a test for the password
$t->is($newuser->getIsActive(), false,'->getIsActive() return the active status of the new user (true - Active, false - Not Active)');
$t->is($newuser->getIsSuperAdmin(), false,'->getIsSuperAdmin() return the super_admin status of the new user (true - Super Admin, false - Not Super Admin)');