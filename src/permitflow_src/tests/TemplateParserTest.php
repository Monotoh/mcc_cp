<?php

class TemplateParserTest extends \PHPUnit_Framework_TestCase {

    public function testParseWithRust()
    {
        $content = 'My first name is {names.first} and my last name is {names.last}, and I was born on {names.dob|date}';
        $values = [
            'names' => [
                'first' => 'John',
                'last'  => 'Smith',
                'dob'   => '05-10-1985'
            ]
        ];
        $result = templateparser::parseWithDust($content, $values);
        $this->assertEquals($result, 'My first name is John and my last name is Smith, and I was born on 05/10/1985');
    }
}
